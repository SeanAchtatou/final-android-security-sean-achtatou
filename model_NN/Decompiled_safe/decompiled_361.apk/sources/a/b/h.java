package a.b;

import java.io.IOException;
import java.io.PipedOutputStream;

final class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ a f94a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ PipedOutputStream f95b;
    private final /* synthetic */ j c;

    h(a aVar, PipedOutputStream pipedOutputStream, j jVar) {
        this.f94a = aVar;
        this.f95b = pipedOutputStream;
        this.c = jVar;
    }

    public final void run() {
        try {
            this.c.a(this.f94a.c, this.f94a.d, this.f95b);
            try {
                this.f95b.close();
            } catch (IOException e) {
            }
        } catch (IOException e2) {
            try {
                this.f95b.close();
            } catch (IOException e3) {
            }
        } catch (Throwable th) {
            try {
                this.f95b.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }
}
