package a.b;

import com.a.b.a.b;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class d extends t {

    /* renamed from: a  reason: collision with root package name */
    private static b f90a = null;

    /* renamed from: b  reason: collision with root package name */
    private b[] f91b;

    public d() {
        b e;
        ArrayList arrayList = new ArrayList(5);
        arrayList.add(null);
        com.a.b.a.d.a("MailcapCommandMap: load HOME");
        try {
            String property = System.getProperty("user.home");
            if (!(property == null || (e = e(String.valueOf(property) + File.separator + ".mailcap")) == null)) {
                arrayList.add(e);
            }
        } catch (SecurityException e2) {
        }
        com.a.b.a.d.a("MailcapCommandMap: load SYS");
        try {
            b e3 = e(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "mailcap");
            if (e3 != null) {
                arrayList.add(e3);
            }
        } catch (SecurityException e4) {
        }
        com.a.b.a.d.a("MailcapCommandMap: load JAR");
        a(arrayList, "mailcap");
        com.a.b.a.d.a("MailcapCommandMap: load DEF");
        synchronized (d.class) {
            if (f90a == null) {
                f90a = d("mailcap.default");
            }
        }
        if (f90a != null) {
            arrayList.add(f90a);
        }
        this.f91b = new b[arrayList.size()];
        this.f91b = (b[]) arrayList.toArray(this.f91b);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x0078 A[Catch:{ all -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x008c A[SYNTHETIC, Splitter:B:37:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0096 A[SYNTHETIC, Splitter:B:43:0x0096] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:22:0x0050=Splitter:B:22:0x0050, B:32:0x0072=Splitter:B:32:0x0072} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.a.b.a.b d(java.lang.String r7) {
        /*
            r6 = this;
            r4 = 0
            java.lang.Class r0 = r6.getClass()     // Catch:{ IOException -> 0x004e, SecurityException -> 0x0070, all -> 0x0092 }
            java.io.InputStream r0 = a.b.r.a(r0, r7)     // Catch:{ IOException -> 0x004e, SecurityException -> 0x0070, all -> 0x0092 }
            if (r0 == 0) goto L_0x002f
            com.a.b.a.b r1 = new com.a.b.a.b     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            r1.<init>(r0)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            boolean r2 = com.a.b.a.d.a()     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            if (r2 == 0) goto L_0x0028
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            java.lang.String r3 = "MailcapCommandMap: successfully loaded mailcap file: "
            r2.<init>(r3)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            com.a.b.a.d.a(r2)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
        L_0x0028:
            if (r0 == 0) goto L_0x002d
            r0.close()     // Catch:{ IOException -> 0x009a }
        L_0x002d:
            r0 = r1
        L_0x002e:
            return r0
        L_0x002f:
            boolean r1 = com.a.b.a.d.a()     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            if (r1 == 0) goto L_0x0047
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            java.lang.String r2 = "MailcapCommandMap: not loading mailcap file: "
            r1.<init>(r2)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
            com.a.b.a.d.a(r1)     // Catch:{ IOException -> 0x00ac, SecurityException -> 0x00a7, all -> 0x00a0 }
        L_0x0047:
            if (r0 == 0) goto L_0x004c
            r0.close()     // Catch:{ IOException -> 0x009e }
        L_0x004c:
            r0 = r4
            goto L_0x002e
        L_0x004e:
            r0 = move-exception
            r1 = r4
        L_0x0050:
            boolean r2 = com.a.b.a.d.a()     // Catch:{ all -> 0x00a5 }
            if (r2 == 0) goto L_0x0068
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a5 }
            java.lang.String r3 = "MailcapCommandMap: can't load "
            r2.<init>(r3)     // Catch:{ all -> 0x00a5 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00a5 }
            com.a.b.a.d.a(r2, r0)     // Catch:{ all -> 0x00a5 }
        L_0x0068:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ IOException -> 0x006e }
            goto L_0x004c
        L_0x006e:
            r0 = move-exception
            goto L_0x004c
        L_0x0070:
            r0 = move-exception
            r1 = r4
        L_0x0072:
            boolean r2 = com.a.b.a.d.a()     // Catch:{ all -> 0x00a5 }
            if (r2 == 0) goto L_0x008a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a5 }
            java.lang.String r3 = "MailcapCommandMap: can't load "
            r2.<init>(r3)     // Catch:{ all -> 0x00a5 }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00a5 }
            com.a.b.a.d.a(r2, r0)     // Catch:{ all -> 0x00a5 }
        L_0x008a:
            if (r1 == 0) goto L_0x004c
            r1.close()     // Catch:{ IOException -> 0x0090 }
            goto L_0x004c
        L_0x0090:
            r0 = move-exception
            goto L_0x004c
        L_0x0092:
            r0 = move-exception
            r1 = r4
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ IOException -> 0x009c }
        L_0x0099:
            throw r0
        L_0x009a:
            r0 = move-exception
            goto L_0x002d
        L_0x009c:
            r1 = move-exception
            goto L_0x0099
        L_0x009e:
            r0 = move-exception
            goto L_0x004c
        L_0x00a0:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0094
        L_0x00a5:
            r0 = move-exception
            goto L_0x0094
        L_0x00a7:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0072
        L_0x00ac:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: a.b.d.d(java.lang.String):com.a.b.a.b");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00c3, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c4, code lost:
        r8 = r5;
        r5 = r2;
        r2 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cb, code lost:
        if (com.a.b.a.d.a() != false) goto L_0x00cd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00cd, code lost:
        com.a.b.a.d.a("MailcapCommandMap: can't load " + r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00df, code lost:
        if (r4 != null) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00e4, code lost:
        r2 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e7, code lost:
        r2 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00e9, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00ea, code lost:
        r8 = r5;
        r5 = r2;
        r2 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00f1, code lost:
        if (com.a.b.a.d.a() != false) goto L_0x00f3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00f3, code lost:
        com.a.b.a.d.a("MailcapCommandMap: can't load " + r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0105, code lost:
        if (r4 != null) goto L_0x0107;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x010a, code lost:
        r2 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x010d, code lost:
        r2 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x011f, code lost:
        com.a.b.a.d.a("MailcapCommandMap: can't load " + r11, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0139, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x013a, code lost:
        r1 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x013c, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x013d, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x013f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0140, code lost:
        r1 = r4;
        r2 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0143, code lost:
        r2 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00c3 A[ExcHandler: IOException (r5v6 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:27:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00e9 A[ExcHandler: SecurityException (r5v4 'e' java.lang.SecurityException A[CUSTOM_DECLARE]), Splitter:B:27:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0113 A[SYNTHETIC, Splitter:B:69:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0139 A[ExcHandler: Exception (e java.lang.Exception), PHI: r5 
      PHI: (r5v3 boolean) = (r5v5 boolean), (r5v5 boolean), (r5v7 boolean), (r5v7 boolean) binds: [B:61:0x0107, B:62:?, B:49:0x00e1, B:50:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:49:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:99:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.List r10, java.lang.String r11) {
        /*
            r9 = this;
            r2 = 0
            java.lang.ClassLoader r0 = a.b.r.a()     // Catch:{ Exception -> 0x0117 }
            if (r0 != 0) goto L_0x000f
            java.lang.Class r0 = r9.getClass()     // Catch:{ Exception -> 0x0117 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ Exception -> 0x0117 }
        L_0x000f:
            if (r0 == 0) goto L_0x0054
            a.b.p r1 = new a.b.p     // Catch:{ Exception -> 0x0117 }
            r1.<init>(r0, r11)     // Catch:{ Exception -> 0x0117 }
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r1)     // Catch:{ Exception -> 0x0117 }
            java.net.URL[] r0 = (java.net.URL[]) r0     // Catch:{ Exception -> 0x0117 }
        L_0x001c:
            if (r0 == 0) goto L_0x0146
            boolean r1 = com.a.b.a.d.a()     // Catch:{ Exception -> 0x0117 }
            if (r1 == 0) goto L_0x0029
            java.lang.String r1 = "MailcapCommandMap: getResources"
            com.a.b.a.d.a(r1)     // Catch:{ Exception -> 0x0117 }
        L_0x0029:
            r1 = r2
        L_0x002a:
            int r3 = r0.length     // Catch:{ Exception -> 0x0117 }
            if (r1 < r3) goto L_0x0060
            r0 = r2
        L_0x002e:
            if (r0 != 0) goto L_0x0053
            boolean r0 = com.a.b.a.d.a()
            if (r0 == 0) goto L_0x003b
            java.lang.String r0 = "MailcapCommandMap: !anyLoaded"
            com.a.b.a.d.a(r0)
        L_0x003b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "/"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r11)
            java.lang.String r0 = r0.toString()
            com.a.b.a.b r0 = r9.d(r0)
            if (r0 == 0) goto L_0x0053
            r10.add(r0)
        L_0x0053:
            return
        L_0x0054:
            a.b.q r0 = new a.b.q     // Catch:{ Exception -> 0x0117 }
            r0.<init>(r11)     // Catch:{ Exception -> 0x0117 }
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r0)     // Catch:{ Exception -> 0x0117 }
            java.net.URL[] r0 = (java.net.URL[]) r0     // Catch:{ Exception -> 0x0117 }
            goto L_0x001c
        L_0x0060:
            r3 = r0[r1]     // Catch:{ Exception -> 0x0117 }
            r4 = 0
            boolean r5 = com.a.b.a.d.a()     // Catch:{ Exception -> 0x0117 }
            if (r5 == 0) goto L_0x007b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0117 }
            java.lang.String r6 = "MailcapCommandMap: URL "
            r5.<init>(r6)     // Catch:{ Exception -> 0x0117 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x0117 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0117 }
            com.a.b.a.d.a(r5)     // Catch:{ Exception -> 0x0117 }
        L_0x007b:
            java.io.InputStream r4 = a.b.r.a(r3)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x010f }
            if (r4 == 0) goto L_0x00aa
            com.a.b.a.b r5 = new com.a.b.a.b     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            r5.<init>(r4)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            r10.add(r5)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            r2 = 1
            boolean r5 = com.a.b.a.d.a()     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            if (r5 == 0) goto L_0x00a2
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            java.lang.String r6 = "MailcapCommandMap: successfully loaded mailcap file from URL: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            com.a.b.a.d.a(r5)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
        L_0x00a2:
            if (r4 == 0) goto L_0x00a7
            r4.close()     // Catch:{ IOException -> 0x0136 }
        L_0x00a7:
            int r1 = r1 + 1
            goto L_0x002a
        L_0x00aa:
            boolean r5 = com.a.b.a.d.a()     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            if (r5 == 0) goto L_0x00a2
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            java.lang.String r6 = "MailcapCommandMap: not loading mailcap file from URL: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            com.a.b.a.d.a(r5)     // Catch:{ IOException -> 0x00c3, SecurityException -> 0x00e9, all -> 0x013c }
            goto L_0x00a2
        L_0x00c3:
            r5 = move-exception
            r8 = r5
            r5 = r2
            r2 = r8
            boolean r6 = com.a.b.a.d.a()     // Catch:{ all -> 0x013f }
            if (r6 == 0) goto L_0x00df
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x013f }
            java.lang.String r7 = "MailcapCommandMap: can't load "
            r6.<init>(r7)     // Catch:{ all -> 0x013f }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ all -> 0x013f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x013f }
            com.a.b.a.d.a(r3, r2)     // Catch:{ all -> 0x013f }
        L_0x00df:
            if (r4 == 0) goto L_0x0143
            r4.close()     // Catch:{ IOException -> 0x00e6, Exception -> 0x0139 }
            r2 = r5
            goto L_0x00a7
        L_0x00e6:
            r2 = move-exception
            r2 = r5
            goto L_0x00a7
        L_0x00e9:
            r5 = move-exception
            r8 = r5
            r5 = r2
            r2 = r8
            boolean r6 = com.a.b.a.d.a()     // Catch:{ all -> 0x013f }
            if (r6 == 0) goto L_0x0105
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x013f }
            java.lang.String r7 = "MailcapCommandMap: can't load "
            r6.<init>(r7)     // Catch:{ all -> 0x013f }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ all -> 0x013f }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x013f }
            com.a.b.a.d.a(r3, r2)     // Catch:{ all -> 0x013f }
        L_0x0105:
            if (r4 == 0) goto L_0x0143
            r4.close()     // Catch:{ IOException -> 0x010c, Exception -> 0x0139 }
            r2 = r5
            goto L_0x00a7
        L_0x010c:
            r2 = move-exception
            r2 = r5
            goto L_0x00a7
        L_0x010f:
            r0 = move-exception
            r1 = r4
        L_0x0111:
            if (r1 == 0) goto L_0x0116
            r1.close()     // Catch:{ IOException -> 0x0134 }
        L_0x0116:
            throw r0     // Catch:{ Exception -> 0x0117 }
        L_0x0117:
            r0 = move-exception
            r1 = r2
        L_0x0119:
            boolean r2 = com.a.b.a.d.a()
            if (r2 == 0) goto L_0x0131
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "MailcapCommandMap: can't load "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r11)
            java.lang.String r2 = r2.toString()
            com.a.b.a.d.a(r2, r0)
        L_0x0131:
            r0 = r1
            goto L_0x002e
        L_0x0134:
            r1 = move-exception
            goto L_0x0116
        L_0x0136:
            r3 = move-exception
            goto L_0x00a7
        L_0x0139:
            r0 = move-exception
            r1 = r5
            goto L_0x0119
        L_0x013c:
            r0 = move-exception
            r1 = r4
            goto L_0x0111
        L_0x013f:
            r0 = move-exception
            r1 = r4
            r2 = r5
            goto L_0x0111
        L_0x0143:
            r2 = r5
            goto L_0x00a7
        L_0x0146:
            r0 = r2
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: a.b.d.a(java.util.List, java.lang.String):void");
    }

    private static b e(String str) {
        try {
            return new b(str);
        } catch (IOException e) {
            return null;
        }
    }

    public final synchronized void a(String str) {
        com.a.b.a.d.a("MailcapCommandMap: add to PROG");
        if (this.f91b[0] == null) {
            this.f91b[0] = new b();
        }
        this.f91b[0].c(str);
    }

    public final synchronized j b(String str) {
        String str2;
        j jVar;
        List list;
        List list2;
        if (com.a.b.a.d.a()) {
            com.a.b.a.d.a("MailcapCommandMap: createDataContentHandler for " + str);
        }
        if (str != null) {
            str2 = str.toLowerCase(Locale.ENGLISH);
        } else {
            str2 = str;
        }
        int i = 0;
        while (true) {
            if (i >= this.f91b.length) {
                int i2 = 0;
                while (true) {
                    if (i2 >= this.f91b.length) {
                        jVar = null;
                        break;
                    }
                    if (this.f91b[i2] != null) {
                        if (com.a.b.a.d.a()) {
                            com.a.b.a.d.a("  search fallback DB #" + i2);
                        }
                        Map b2 = this.f91b[i2].b(str2);
                        if (!(b2 == null || (list = (List) b2.get("content-handler")) == null || (jVar = f((String) list.get(0))) == null)) {
                            break;
                        }
                    }
                    i2++;
                }
            } else {
                if (this.f91b[i] != null) {
                    if (com.a.b.a.d.a()) {
                        com.a.b.a.d.a("  search DB #" + i);
                    }
                    Map a2 = this.f91b[i].a(str2);
                    if (!(a2 == null || (list2 = (List) a2.get("content-handler")) == null || (jVar = f((String) list2.get(0))) == null)) {
                        break;
                    }
                }
                i++;
            }
        }
        return jVar;
    }

    private j f(String str) {
        Class<?> cls;
        if (com.a.b.a.d.a()) {
            com.a.b.a.d.a("    got content-handler");
        }
        if (com.a.b.a.d.a()) {
            com.a.b.a.d.a("      class " + str);
        }
        try {
            ClassLoader a2 = r.a();
            if (a2 == null) {
                a2 = getClass().getClassLoader();
            }
            try {
                cls = a2.loadClass(str);
            } catch (Exception e) {
                cls = Class.forName(str);
            }
            if (cls != null) {
                return (j) cls.newInstance();
            }
        } catch (IllegalAccessException e2) {
            if (com.a.b.a.d.a()) {
                com.a.b.a.d.a("Can't load DCH " + str, e2);
            }
        } catch (ClassNotFoundException e3) {
            if (com.a.b.a.d.a()) {
                com.a.b.a.d.a("Can't load DCH " + str, e3);
            }
        } catch (InstantiationException e4) {
            if (com.a.b.a.d.a()) {
                com.a.b.a.d.a("Can't load DCH " + str, e4);
            }
        }
        return null;
    }
}
