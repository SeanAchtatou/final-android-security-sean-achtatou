package a.b;

import java.security.PrivilegedAction;

final class n implements PrivilegedAction {
    n() {
    }

    public final Object run() {
        try {
            return Thread.currentThread().getContextClassLoader();
        } catch (SecurityException e) {
            return null;
        }
    }
}
