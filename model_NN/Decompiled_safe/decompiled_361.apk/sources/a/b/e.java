package a.b;

import b.a.a.d;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

final class e implements j {

    /* renamed from: a  reason: collision with root package name */
    private d[] f92a = null;

    /* renamed from: b  reason: collision with root package name */
    private Object f93b;
    private String c;
    private j d = null;

    public e(j jVar, Object obj, String str) {
        this.f93b = obj;
        this.c = str;
        this.d = jVar;
    }

    public final j a() {
        return this.d;
    }

    public final Object a(g gVar) {
        return this.f93b;
    }

    public final void a(Object obj, String str, OutputStream outputStream) {
        if (this.d != null) {
            this.d.a(obj, str, outputStream);
        } else if (obj instanceof byte[]) {
            outputStream.write((byte[]) obj);
        } else if (obj instanceof String) {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
            outputStreamWriter.write((String) obj);
            outputStreamWriter.flush();
        } else {
            throw new f("no object DCH for MIME type " + this.c);
        }
    }
}
