package a.b;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Locale;

public final class s implements Externalizable {

    /* renamed from: a  reason: collision with root package name */
    private String f103a;

    /* renamed from: b  reason: collision with root package name */
    private String f104b;
    private b c;

    public s() {
        this.f103a = "application";
        this.f104b = "*";
        this.c = new b();
    }

    public s(String str) {
        a(str);
    }

    private void a(String str) {
        int indexOf = str.indexOf(47);
        int indexOf2 = str.indexOf(59);
        if (indexOf < 0 && indexOf2 < 0) {
            throw new i("Unable to find a sub type.");
        } else if (indexOf >= 0 || indexOf2 < 0) {
            if (indexOf >= 0 && indexOf2 < 0) {
                this.f103a = str.substring(0, indexOf).trim().toLowerCase(Locale.ENGLISH);
                this.f104b = str.substring(indexOf + 1).trim().toLowerCase(Locale.ENGLISH);
                this.c = new b();
            } else if (indexOf < indexOf2) {
                this.f103a = str.substring(0, indexOf).trim().toLowerCase(Locale.ENGLISH);
                this.f104b = str.substring(indexOf + 1, indexOf2).trim().toLowerCase(Locale.ENGLISH);
                this.c = new b(str.substring(indexOf2));
            } else {
                throw new i("Unable to find a sub type.");
            }
            if (!b(this.f103a)) {
                throw new i("Primary type is invalid.");
            } else if (!b(this.f104b)) {
                throw new i("Sub type is invalid.");
            }
        } else {
            throw new i("Unable to find a sub type.");
        }
    }

    public final String toString() {
        return String.valueOf(a()) + this.c.toString();
    }

    public final String a() {
        return String.valueOf(this.f103a) + "/" + this.f104b;
    }

    public final void writeExternal(ObjectOutput objectOutput) {
        objectOutput.writeUTF(toString());
        objectOutput.flush();
    }

    public final void readExternal(ObjectInput objectInput) {
        try {
            a(objectInput.readUTF());
        } catch (i e) {
            throw new IOException(e.toString());
        }
    }

    private static boolean b(String str) {
        boolean z;
        int length = str.length();
        if (length <= 0) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= ' ' || charAt >= 127 || "()<>@,;:/[]?=\\\"".indexOf(charAt) >= 0) {
                z = false;
            } else {
                z = true;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }
}
