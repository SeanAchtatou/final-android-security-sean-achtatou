package a.b;

import java.io.IOException;
import java.net.URL;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.Enumeration;

final class q implements PrivilegedAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f102a;

    q(String str) {
        this.f102a = str;
    }

    public final Object run() {
        try {
            ArrayList arrayList = new ArrayList();
            Enumeration<URL> systemResources = ClassLoader.getSystemResources(this.f102a);
            while (systemResources != null && systemResources.hasMoreElements()) {
                URL nextElement = systemResources.nextElement();
                if (nextElement != null) {
                    arrayList.add(nextElement);
                }
            }
            if (arrayList.size() > 0) {
                return (URL[]) arrayList.toArray(new URL[arrayList.size()]);
            }
            return null;
        } catch (IOException | SecurityException e) {
            return null;
        }
    }
}
