package a.b;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedActionException;

final class r {
    private r() {
    }

    public static ClassLoader a() {
        return (ClassLoader) AccessController.doPrivileged(new n());
    }

    public static InputStream a(Class cls, String str) {
        try {
            return (InputStream) AccessController.doPrivileged(new o(cls, str));
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }

    public static InputStream a(URL url) {
        try {
            return (InputStream) AccessController.doPrivileged(new l(url));
        } catch (PrivilegedActionException e) {
            throw ((IOException) e.getException());
        }
    }
}
