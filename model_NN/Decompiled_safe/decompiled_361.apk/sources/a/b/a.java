package a.b;

import b.a.a.d;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public final class a {
    private static final d[] f = new d[0];
    private static k j = null;

    /* renamed from: a  reason: collision with root package name */
    private g f85a = null;

    /* renamed from: b  reason: collision with root package name */
    private g f86b = null;
    /* access modifiers changed from: private */
    public Object c = null;
    /* access modifiers changed from: private */
    public String d = null;
    private t e = null;
    private d[] g = f;
    private j h = null;
    private j i = null;
    private k k = null;
    private String l = null;

    public a(g gVar) {
        this.f85a = gVar;
        this.k = j;
    }

    public a(Object obj, String str) {
        this.c = obj;
        this.d = str;
        this.k = j;
    }

    private synchronized t f() {
        t a2;
        if (this.e != null) {
            a2 = this.e;
        } else {
            a2 = t.a();
        }
        return a2;
    }

    public final g a() {
        if (this.f85a != null) {
            return this.f85a;
        }
        if (this.f86b == null) {
            this.f86b = new m(this);
        }
        return this.f86b;
    }

    public final String b() {
        if (this.f85a != null) {
            return this.f85a.c();
        }
        return null;
    }

    public final String c() {
        if (this.f85a != null) {
            return this.f85a.b();
        }
        return this.d;
    }

    public final InputStream d() {
        if (this.f85a != null) {
            return this.f85a.a();
        }
        j g2 = g();
        if (g2 == null) {
            throw new f("no DCH for MIME type " + h());
        } else if (!(g2 instanceof e) || ((e) g2).a() != null) {
            PipedOutputStream pipedOutputStream = new PipedOutputStream();
            PipedInputStream pipedInputStream = new PipedInputStream(pipedOutputStream);
            new Thread(new h(this, pipedOutputStream, g2), "DataHandler.getInputStream").start();
            return pipedInputStream;
        } else {
            throw new f("no object DCH for MIME type " + h());
        }
    }

    public final void a(OutputStream outputStream) {
        if (this.f85a != null) {
            byte[] bArr = new byte[8192];
            InputStream a2 = this.f85a.a();
            while (true) {
                try {
                    int read = a2.read(bArr);
                    if (read > 0) {
                        outputStream.write(bArr, 0, read);
                    } else {
                        return;
                    }
                } finally {
                    a2.close();
                }
            }
        } else {
            g().a(this.c, this.d, outputStream);
        }
    }

    public final Object e() {
        if (this.c != null) {
            return this.c;
        }
        return g().a(a());
    }

    private synchronized j g() {
        j jVar;
        if (j != this.k) {
            this.k = j;
            this.i = null;
            this.h = null;
            this.g = f;
        }
        if (this.h != null) {
            jVar = this.h;
        } else {
            String h2 = h();
            if (this.i == null && j != null) {
                this.i = j.a();
            }
            if (this.i != null) {
                this.h = this.i;
            }
            if (this.h == null) {
                if (this.f85a != null) {
                    this.h = f().c(h2);
                } else {
                    this.h = f().b(h2);
                }
            }
            if (this.f85a != null) {
                this.h = new c(this.h, this.f85a);
            } else {
                this.h = new e(this.h, this.c, this.d);
            }
            jVar = this.h;
        }
        return jVar;
    }

    private synchronized String h() {
        if (this.l == null) {
            String c2 = c();
            try {
                this.l = new s(c2).a();
            } catch (i e2) {
                this.l = c2;
            }
        }
        return this.l;
    }
}
