package a.b;

public abstract class t {

    /* renamed from: a  reason: collision with root package name */
    private static t f105a = null;

    public abstract j b(String str);

    public static t a() {
        if (f105a == null) {
            f105a = new d();
        }
        return f105a;
    }

    public static void a(t tVar) {
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) {
            try {
                securityManager.checkSetFactory();
            } catch (SecurityException e) {
                if (t.class.getClassLoader() != tVar.getClass().getClassLoader()) {
                    throw e;
                }
            }
        }
        f105a = tVar;
    }

    public final j c(String str) {
        return b(str);
    }
}
