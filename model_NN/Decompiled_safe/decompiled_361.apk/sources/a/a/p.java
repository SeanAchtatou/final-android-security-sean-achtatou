package a.a;

import a.a.b.b;
import java.util.Vector;

public abstract class p {

    /* renamed from: a  reason: collision with root package name */
    private j f71a;

    /* renamed from: b  reason: collision with root package name */
    private g f72b;
    private boolean c;
    private boolean d;
    private Vector e;
    private u f;
    private Object g;

    public final void a() {
        a(null, -1, null, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0084 A[Catch:{ SecurityException -> 0x00cd }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e A[Catch:{ SecurityException -> 0x00cd }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a0 A[SYNTHETIC, Splitter:B:28:0x00a0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(java.lang.String r12, int r13, java.lang.String r14, java.lang.String r15) {
        /*
            r11 = this;
            r8 = 0
            monitor-enter(r11)
            boolean r0 = r11.c()     // Catch:{ all -> 0x0010 }
            if (r0 == 0) goto L_0x0013
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0010 }
            java.lang.String r1 = "already connected"
            r0.<init>(r1)     // Catch:{ all -> 0x0010 }
            throw r0     // Catch:{ all -> 0x0010 }
        L_0x0010:
            r0 = move-exception
            monitor-exit(r11)
            throw r0
        L_0x0013:
            a.a.g r0 = r11.f72b     // Catch:{ all -> 0x0010 }
            if (r0 == 0) goto L_0x00ec
            a.a.g r0 = r11.f72b     // Catch:{ all -> 0x0010 }
            java.lang.String r0 = r0.b()     // Catch:{ all -> 0x0010 }
            a.a.g r1 = r11.f72b     // Catch:{ all -> 0x0010 }
            java.lang.String r1 = r1.d()     // Catch:{ all -> 0x0010 }
            a.a.g r2 = r11.f72b     // Catch:{ all -> 0x0010 }
            int r2 = r2.a()     // Catch:{ all -> 0x0010 }
            a.a.g r3 = r11.f72b     // Catch:{ all -> 0x0010 }
            java.lang.String r3 = r3.e()     // Catch:{ all -> 0x0010 }
            a.a.g r4 = r11.f72b     // Catch:{ all -> 0x0010 }
            java.lang.String r4 = r4.f()     // Catch:{ all -> 0x0010 }
            a.a.g r5 = r11.f72b     // Catch:{ all -> 0x0010 }
            java.lang.String r5 = r5.c()     // Catch:{ all -> 0x0010 }
            r9 = r5
            r5 = r1
            r1 = r0
            r0 = r4
            r4 = r9
            r10 = r2
            r2 = r3
            r3 = r10
        L_0x0043:
            if (r1 == 0) goto L_0x00e8
            if (r5 != 0) goto L_0x0062
            a.a.j r5 = r11.f71a     // Catch:{ all -> 0x0010 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0010 }
            java.lang.String r7 = "mail."
            r6.<init>(r7)     // Catch:{ all -> 0x0010 }
            java.lang.StringBuilder r6 = r6.append(r1)     // Catch:{ all -> 0x0010 }
            java.lang.String r7 = ".host"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0010 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0010 }
            java.lang.String r5 = r5.a(r6)     // Catch:{ all -> 0x0010 }
        L_0x0062:
            if (r2 != 0) goto L_0x00e8
            a.a.j r2 = r11.f71a     // Catch:{ all -> 0x0010 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0010 }
            java.lang.String r7 = "mail."
            r6.<init>(r7)     // Catch:{ all -> 0x0010 }
            java.lang.StringBuilder r6 = r6.append(r1)     // Catch:{ all -> 0x0010 }
            java.lang.String r7 = ".user"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0010 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0010 }
            java.lang.String r2 = r2.a(r6)     // Catch:{ all -> 0x0010 }
            r9 = r2
            r2 = r5
            r5 = r9
        L_0x0082:
            if (r2 != 0) goto L_0x008c
            a.a.j r2 = r11.f71a     // Catch:{ all -> 0x0010 }
            java.lang.String r6 = "mail.host"
            java.lang.String r2 = r2.a(r6)     // Catch:{ all -> 0x0010 }
        L_0x008c:
            if (r5 != 0) goto L_0x0096
            a.a.j r5 = r11.f71a     // Catch:{ all -> 0x0010 }
            java.lang.String r6 = "mail.user"
            java.lang.String r5 = r5.a(r6)     // Catch:{ all -> 0x0010 }
        L_0x0096:
            if (r5 != 0) goto L_0x009e
            java.lang.String r6 = "user.name"
            java.lang.String r5 = java.lang.System.getProperty(r6)     // Catch:{ SecurityException -> 0x00cd }
        L_0x009e:
            if (r0 != 0) goto L_0x00e3
            a.a.g r0 = r11.f72b     // Catch:{ all -> 0x0010 }
            if (r0 == 0) goto L_0x00e3
            a.a.g r0 = new a.a.g     // Catch:{ all -> 0x0010 }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0010 }
            r11.a(r0)     // Catch:{ all -> 0x0010 }
            a.a.j r0 = r11.f71a     // Catch:{ all -> 0x0010 }
            a.a.g r4 = r11.e()     // Catch:{ all -> 0x0010 }
            a.a.i r0 = r0.a(r4)     // Catch:{ all -> 0x0010 }
            if (r0 == 0) goto L_0x00e3
            if (r5 != 0) goto L_0x00dc
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x0010 }
        L_0x00be:
            java.net.InetAddress r2 = java.net.InetAddress.getByName(r2)     // Catch:{ UnknownHostException -> 0x00e5 }
        L_0x00c2:
            a.a.j r4 = r11.f71a     // Catch:{ all -> 0x0010 }
            r4.a(r2, r3, r1, r0)     // Catch:{ all -> 0x0010 }
            a.a.c r0 = new a.a.c     // Catch:{ all -> 0x0010 }
            r0.<init>()     // Catch:{ all -> 0x0010 }
            throw r0     // Catch:{ all -> 0x0010 }
        L_0x00cd:
            r6 = move-exception
            boolean r7 = r11.c     // Catch:{ all -> 0x0010 }
            if (r7 == 0) goto L_0x009e
            a.a.j r7 = r11.f71a     // Catch:{ all -> 0x0010 }
            java.io.PrintStream r7 = r7.a()     // Catch:{ all -> 0x0010 }
            r6.printStackTrace(r7)     // Catch:{ all -> 0x0010 }
            goto L_0x009e
        L_0x00dc:
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x0010 }
            r5.equals(r0)     // Catch:{ all -> 0x0010 }
        L_0x00e3:
            r0 = r5
            goto L_0x00be
        L_0x00e5:
            r2 = move-exception
            r2 = r8
            goto L_0x00c2
        L_0x00e8:
            r9 = r2
            r2 = r5
            r5 = r9
            goto L_0x0082
        L_0x00ec:
            r4 = r8
            r1 = r8
            r0 = r15
            r2 = r14
            r3 = r13
            r5 = r12
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.p.a(java.lang.String, int, java.lang.String, java.lang.String):void");
    }

    private synchronized boolean c() {
        return this.d;
    }

    private synchronized void d() {
        this.d = false;
    }

    public final synchronized void b() {
        d();
        f();
    }

    private synchronized g e() {
        g gVar;
        if (this.f72b == null || (this.f72b.f() == null && this.f72b.c() == null)) {
            gVar = this.f72b;
        } else {
            gVar = new g(this.f72b.b(), this.f72b.d(), this.f72b.a(), null, this.f72b.e());
        }
        return gVar;
    }

    private synchronized void a(g gVar) {
        this.f72b = gVar;
    }

    private synchronized void f() {
        if (this.e != null) {
            b bVar = new b(this);
            Vector vector = this.e;
            synchronized (this.g) {
                if (this.f == null) {
                    this.f = new u();
                }
            }
            this.f.a(bVar, (Vector) vector.clone());
        }
        g();
    }

    public String toString() {
        g e2 = e();
        if (e2 != null) {
            return e2.toString();
        }
        return super.toString();
    }

    private void g() {
        synchronized (this.g) {
            if (this.f != null) {
                Vector vector = new Vector();
                vector.setSize(1);
                this.f.a(new b(), vector);
                this.f = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        g();
    }
}
