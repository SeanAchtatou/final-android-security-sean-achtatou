package a.a;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private x f61a;

    /* renamed from: b  reason: collision with root package name */
    private String f62b;
    private String c;
    private String d;
    private String e;

    public h(x xVar, String str, String str2, String str3, String str4) {
        this.f61a = xVar;
        this.f62b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
    }

    public final x a() {
        return this.f61a;
    }

    public final String b() {
        return this.f62b;
    }

    public final String c() {
        return this.c;
    }

    public final String toString() {
        String str = "javax.mail.Provider[" + this.f61a + "," + this.f62b + "," + this.c;
        if (this.d != null) {
            str = String.valueOf(str) + "," + this.d;
        }
        if (this.e != null) {
            str = String.valueOf(str) + "," + this.e;
        }
        return String.valueOf(str) + "]";
    }
}
