package a.a;

public abstract class t implements k {

    /* renamed from: a  reason: collision with root package name */
    protected j f75a = null;

    /* renamed from: b  reason: collision with root package name */
    private int f76b = 0;
    private boolean c = false;
    private f d = null;

    public abstract void a(ag agVar, q[] qVarArr);

    public abstract q[] a(ag agVar);

    public abstract void d();

    protected t() {
    }

    protected t(j jVar) {
        this.f75a = jVar;
    }

    public q[] c() {
        int i;
        int i2;
        int i3;
        q[] a2 = a(ag.f47b);
        q[] a3 = a(ag.c);
        q[] a4 = a(ag.d);
        if (a3 == null && a4 == null) {
            return a2;
        }
        int length = a2 != null ? a2.length : 0;
        if (a3 != null) {
            i = a3.length;
        } else {
            i = 0;
        }
        int i4 = length + i;
        if (a4 != null) {
            i2 = a4.length;
        } else {
            i2 = 0;
        }
        q[] qVarArr = new q[(i4 + i2)];
        if (a2 != null) {
            System.arraycopy(a2, 0, qVarArr, 0, a2.length);
            i3 = a2.length + 0;
        } else {
            i3 = 0;
        }
        if (a3 != null) {
            System.arraycopy(a3, 0, qVarArr, i3, a3.length);
            i3 += a3.length;
        }
        if (a4 != null) {
            System.arraycopy(a4, 0, qVarArr, i3, a4.length);
        }
        return qVarArr;
    }
}
