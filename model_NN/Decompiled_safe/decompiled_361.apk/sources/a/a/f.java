package a.a;

import java.util.Vector;

public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    private u f57a;

    /* renamed from: b  reason: collision with root package name */
    private Object f58b;

    public abstract String a();

    /* access modifiers changed from: protected */
    public void finalize() {
        super.finalize();
        synchronized (this.f58b) {
            if (this.f57a != null) {
                Vector vector = new Vector();
                vector.setSize(1);
                this.f57a.a(new o(), vector);
                this.f57a = null;
            }
        }
    }

    public String toString() {
        String a2 = a();
        return a2 != null ? a2 : super.toString();
    }
}
