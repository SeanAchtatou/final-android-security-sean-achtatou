package a.a;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

public abstract class w extends p {
    public static void a(t tVar) {
        q[] qVarArr;
        q[] qVarArr2;
        q[] qVarArr3;
        tVar.d();
        q[] c = tVar.c();
        if (c == null || c.length == 0) {
            throw new n("No recipient addresses");
        }
        Hashtable hashtable = new Hashtable();
        Vector vector = new Vector();
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        for (int i = 0; i < c.length; i++) {
            if (hashtable.containsKey(c[i].a())) {
                ((Vector) hashtable.get(c[i].a())).addElement(c[i]);
            } else {
                Vector vector4 = new Vector();
                vector4.addElement(c[i]);
                hashtable.put(c[i].a(), vector4);
            }
        }
        int size = hashtable.size();
        if (size == 0) {
            throw new n("No recipient addresses");
        }
        j b2 = tVar.f75a != null ? tVar.f75a : j.b(System.getProperties(), (ah) null);
        if (size == 1) {
            w a2 = b2.a(c[0]);
            try {
                a2.a();
            } finally {
                a2.b();
            }
        } else {
            Enumeration elements = hashtable.elements();
            boolean z = false;
            n nVar = null;
            while (elements.hasMoreElements()) {
                Vector vector5 = (Vector) elements.nextElement();
                q[] qVarArr4 = new q[vector5.size()];
                vector5.copyInto(qVarArr4);
                w a3 = b2.a(qVarArr4[0]);
                if (a3 == null) {
                    for (q addElement : qVarArr4) {
                        vector.addElement(addElement);
                    }
                } else {
                    try {
                        a3.a();
                        a3.b();
                    } catch (n e) {
                        if (nVar == null) {
                            nVar = e;
                        } else {
                            nVar.a(e);
                        }
                        q[] c2 = e.c();
                        if (c2 != null) {
                            for (q addElement2 : c2) {
                                vector.addElement(addElement2);
                            }
                        }
                        q[] a4 = e.a();
                        if (a4 != null) {
                            for (q addElement3 : a4) {
                                vector2.addElement(addElement3);
                            }
                        }
                        q[] b3 = e.b();
                        if (b3 != null) {
                            for (q addElement4 : b3) {
                                vector3.addElement(addElement4);
                            }
                        }
                        a3.b();
                        z = true;
                    } catch (af e2) {
                        e = e2;
                        if (nVar != null) {
                            nVar.a(e);
                            e = nVar;
                        }
                        a3.b();
                        nVar = e;
                        z = true;
                    } catch (Throwable th) {
                        a3.b();
                        throw th;
                    }
                }
            }
            if (z || vector.size() != 0 || vector3.size() != 0) {
                if (vector2.size() > 0) {
                    q[] qVarArr5 = new q[vector2.size()];
                    vector2.copyInto(qVarArr5);
                    qVarArr = qVarArr5;
                } else {
                    qVarArr = null;
                }
                if (vector3.size() > 0) {
                    q[] qVarArr6 = new q[vector3.size()];
                    vector3.copyInto(qVarArr6);
                    qVarArr2 = qVarArr6;
                } else {
                    qVarArr2 = null;
                }
                if (vector.size() > 0) {
                    q[] qVarArr7 = new q[vector.size()];
                    vector.copyInto(qVarArr7);
                    qVarArr3 = qVarArr7;
                } else {
                    qVarArr3 = null;
                }
                throw new n("Sending failed", nVar, qVarArr, qVarArr2, qVarArr3);
            }
        }
    }
}
