package a.a;

import java.security.PrivilegedAction;

final class ac implements PrivilegedAction {
    ac() {
    }

    public final Object run() {
        try {
            return Thread.currentThread().getContextClassLoader();
        } catch (SecurityException e) {
            return null;
        }
    }
}
