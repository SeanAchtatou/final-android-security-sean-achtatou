package a.a;

import java.security.PrivilegedExceptionAction;

final class ab implements PrivilegedExceptionAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Class f42a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f43b;

    ab(Class cls, String str) {
        this.f42a = cls;
        this.f43b = str;
    }

    public final Object run() {
        return this.f42a.getResourceAsStream(this.f43b);
    }
}
