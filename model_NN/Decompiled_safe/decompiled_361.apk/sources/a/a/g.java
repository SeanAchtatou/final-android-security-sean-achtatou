package a.a;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.BitSet;
import java.util.Locale;

public class g {
    private static boolean l;
    private static BitSet m = new BitSet(256);

    /* renamed from: a  reason: collision with root package name */
    private String f59a;

    /* renamed from: b  reason: collision with root package name */
    private String f60b;
    private String c;
    private String d;
    private String e;
    private InetAddress f;
    private boolean g = false;
    private int h = -1;
    private String i;
    private String j;
    private int k = 0;

    static {
        boolean z;
        l = true;
        try {
            if (Boolean.getBoolean("mail.URLName.dontencode")) {
                z = false;
            } else {
                z = true;
            }
            l = z;
        } catch (Exception e2) {
        }
        for (int i2 = 97; i2 <= 122; i2++) {
            m.set(i2);
        }
        for (int i3 = 65; i3 <= 90; i3++) {
            m.set(i3);
        }
        for (int i4 = 48; i4 <= 57; i4++) {
            m.set(i4);
        }
        m.set(32);
        m.set(45);
        m.set(95);
        m.set(46);
        m.set(42);
    }

    public g(String str, String str2, int i2, String str3, String str4) {
        String str5;
        String str6;
        int indexOf;
        this.f60b = str;
        this.e = str2;
        this.h = i2;
        if (str3 == null || (indexOf = str3.indexOf(35)) == -1) {
            this.i = str3;
            this.j = null;
        } else {
            this.i = str3.substring(0, indexOf);
            this.j = str3.substring(indexOf + 1);
        }
        if (l) {
            str5 = a(str4);
        } else {
            str5 = str4;
        }
        this.c = str5;
        if (l) {
            str6 = a(null);
        } else {
            str6 = null;
        }
        this.d = str6;
    }

    public String toString() {
        if (this.f59a == null) {
            StringBuffer stringBuffer = new StringBuffer();
            if (this.f60b != null) {
                stringBuffer.append(this.f60b);
                stringBuffer.append(":");
            }
            if (!(this.c == null && this.e == null)) {
                stringBuffer.append("//");
                if (this.c != null) {
                    stringBuffer.append(this.c);
                    if (this.d != null) {
                        stringBuffer.append(":");
                        stringBuffer.append(this.d);
                    }
                    stringBuffer.append("@");
                }
                if (this.e != null) {
                    stringBuffer.append(this.e);
                }
                if (this.h != -1) {
                    stringBuffer.append(":");
                    stringBuffer.append(Integer.toString(this.h));
                }
                if (this.i != null) {
                    stringBuffer.append("/");
                }
            }
            if (this.i != null) {
                stringBuffer.append(this.i);
            }
            if (this.j != null) {
                stringBuffer.append("#");
                stringBuffer.append(this.j);
            }
            this.f59a = stringBuffer.toString();
        }
        return this.f59a;
    }

    public final int a() {
        return this.h;
    }

    public final String b() {
        return this.f60b;
    }

    public final String c() {
        return this.i;
    }

    public final String d() {
        return this.e;
    }

    public final String e() {
        return l ? c(this.c) : this.c;
    }

    public final String f() {
        return l ? c(this.d) : this.d;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof g)) {
            return false;
        }
        g gVar = (g) obj;
        if (gVar.f60b == null || !gVar.f60b.equals(this.f60b)) {
            return false;
        }
        InetAddress g2 = g();
        InetAddress g3 = gVar.g();
        if (g2 == null || g3 == null) {
            if (this.e == null || gVar.e == null) {
                if (this.e != gVar.e) {
                    return false;
                }
            } else if (!this.e.equalsIgnoreCase(gVar.e)) {
                return false;
            }
        } else if (!g2.equals(g3)) {
            return false;
        }
        if (this.c != gVar.c && (this.c == null || !this.c.equals(gVar.c))) {
            return false;
        }
        if (!(this.i == null ? "" : this.i).equals(gVar.i == null ? "" : gVar.i)) {
            return false;
        }
        if (this.h != gVar.h) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.k != 0) {
            return this.k;
        }
        if (this.f60b != null) {
            this.k += this.f60b.hashCode();
        }
        InetAddress g2 = g();
        if (g2 != null) {
            this.k = g2.hashCode() + this.k;
        } else if (this.e != null) {
            this.k += this.e.toLowerCase(Locale.ENGLISH).hashCode();
        }
        if (this.c != null) {
            this.k += this.c.hashCode();
        }
        if (this.i != null) {
            this.k += this.i.hashCode();
        }
        this.k += this.h;
        return this.k;
    }

    private synchronized InetAddress g() {
        InetAddress inetAddress;
        if (this.g) {
            inetAddress = this.f;
        } else if (this.e == null) {
            inetAddress = null;
        } else {
            try {
                this.f = InetAddress.getByName(this.e);
            } catch (UnknownHostException e2) {
                this.f = null;
            }
            this.g = true;
            inetAddress = this.f;
        }
        return inetAddress;
    }

    private static String a(String str) {
        if (str == null) {
            return null;
        }
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (charAt == ' ' || !m.get(charAt)) {
                return b(str);
            }
        }
        return str;
    }

    private static String b(String str) {
        StringBuffer stringBuffer = new StringBuffer(str.length());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(10);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(byteArrayOutputStream);
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (m.get(charAt)) {
                if (charAt == ' ') {
                    charAt = '+';
                }
                stringBuffer.append((char) charAt);
            } else {
                try {
                    outputStreamWriter.write(charAt);
                    outputStreamWriter.flush();
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    for (int i3 = 0; i3 < byteArray.length; i3++) {
                        stringBuffer.append('%');
                        char forDigit = Character.forDigit((byteArray[i3] >> 4) & 15, 16);
                        if (Character.isLetter(forDigit)) {
                            forDigit = (char) (forDigit - ' ');
                        }
                        stringBuffer.append(forDigit);
                        char forDigit2 = Character.forDigit(byteArray[i3] & 15, 16);
                        if (Character.isLetter(forDigit2)) {
                            forDigit2 = (char) (forDigit2 - ' ');
                        }
                        stringBuffer.append(forDigit2);
                    }
                    byteArrayOutputStream.reset();
                } catch (IOException e2) {
                    byteArrayOutputStream.reset();
                }
            }
        }
        return stringBuffer.toString();
    }

    private static String c(String str) {
        if (str == null) {
            return null;
        }
        if (a(str, "+%") == -1) {
            return str;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = 0;
        while (i2 < str.length()) {
            char charAt = str.charAt(i2);
            switch (charAt) {
                case '%':
                    try {
                        stringBuffer.append((char) Integer.parseInt(str.substring(i2 + 1, i2 + 3), 16));
                        i2 += 2;
                        break;
                    } catch (NumberFormatException e2) {
                        throw new IllegalArgumentException();
                    }
                case '+':
                    stringBuffer.append(' ');
                    break;
                default:
                    stringBuffer.append(charAt);
                    break;
            }
            i2++;
        }
        String stringBuffer2 = stringBuffer.toString();
        try {
            return new String(stringBuffer2.getBytes("8859_1"));
        } catch (UnsupportedEncodingException e3) {
            return stringBuffer2;
        }
    }

    private static int a(String str, String str2) {
        try {
            int length = str.length();
            for (int i2 = 0; i2 < length; i2++) {
                if (str2.indexOf(str.charAt(i2)) >= 0) {
                    return i2;
                }
            }
            return -1;
        } catch (StringIndexOutOfBoundsException e2) {
            return -1;
        }
    }
}
