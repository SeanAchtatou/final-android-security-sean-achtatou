package a.a;

import java.util.Vector;

public abstract class m {

    /* renamed from: a  reason: collision with root package name */
    protected Vector f67a = new Vector();

    /* renamed from: b  reason: collision with root package name */
    protected String f68b = "multipart/mixed";
    private k c;

    protected m() {
    }

    public final String c() {
        return this.f68b;
    }

    public synchronized int a() {
        int size;
        if (this.f67a == null) {
            size = 0;
        } else {
            size = this.f67a.size();
        }
        return size;
    }

    public synchronized void a(int i) {
        if (this.f67a == null) {
            throw new IndexOutOfBoundsException("No such BodyPart");
        }
        this.f67a.removeElementAt(i);
        ((a) this.f67a.elementAt(i)).a((m) null);
    }

    public synchronized void a(a aVar) {
        if (this.f67a == null) {
            this.f67a = new Vector();
        }
        this.f67a.addElement(aVar);
        aVar.a(this);
    }

    public final synchronized void a(k kVar) {
        this.c = kVar;
    }
}
