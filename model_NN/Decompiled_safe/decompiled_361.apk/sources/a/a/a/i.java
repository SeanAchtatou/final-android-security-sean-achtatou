package a.a.a;

public final class i {

    /* renamed from: a  reason: collision with root package name */
    private String f14a;

    /* renamed from: b  reason: collision with root package name */
    private String f15b;
    private y c;

    public i() {
    }

    public i(String str, String str2) {
        this.f14a = str;
        this.f15b = str2;
        this.c = null;
    }

    public i(String str) {
        q qVar = new q(str, "()<>@,;:\\\"\t []/?=");
        k a2 = qVar.a();
        if (a2.a() != -1) {
            throw new aa();
        }
        this.f14a = a2.b();
        if (((char) qVar.a().a()) != '/') {
            throw new aa();
        }
        k a3 = qVar.a();
        if (a3.a() != -1) {
            throw new aa();
        }
        this.f15b = a3.b();
        String b2 = qVar.b();
        if (b2 != null) {
            this.c = new y(b2);
        }
    }

    public final String a(String str) {
        if (this.c == null) {
            return null;
        }
        return this.c.a(str);
    }

    public final void a(String str, String str2) {
        if (this.c == null) {
            this.c = new y();
        }
        this.c.a(str, str2);
    }

    public final String toString() {
        if (this.f14a == null || this.f15b == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f14a).append('/').append(this.f15b);
        if (this.c != null) {
            stringBuffer.append(this.c.a(stringBuffer.length() + 14));
        }
        return stringBuffer.toString();
    }

    public final boolean b(String str) {
        try {
            i iVar = new i(str);
            if (!this.f14a.equalsIgnoreCase(iVar.f14a)) {
                return false;
            }
            String str2 = iVar.f15b;
            if (this.f15b.charAt(0) == '*' || str2.charAt(0) == '*') {
                return true;
            }
            return this.f15b.equalsIgnoreCase(str2);
        } catch (aa e) {
            return false;
        }
    }
}
