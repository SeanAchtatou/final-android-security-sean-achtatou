package a.a.a;

public final class v extends a.a.v {

    /* renamed from: b  reason: collision with root package name */
    String f33b;

    public v(String str) {
        super("", "");
        int indexOf = str.indexOf(58);
        if (indexOf < 0) {
            this.f79a = str.trim();
        } else {
            this.f79a = str.substring(0, indexOf).trim();
        }
        this.f33b = str;
    }

    public v(String str, String str2) {
        super(str, "");
        if (str2 != null) {
            this.f33b = String.valueOf(str) + ": " + str2;
        } else {
            this.f33b = null;
        }
    }

    public final String b() {
        char charAt;
        int indexOf = this.f33b.indexOf(58);
        if (indexOf < 0) {
            return this.f33b;
        }
        while (true) {
            indexOf++;
            if (indexOf < this.f33b.length() && ((charAt = this.f33b.charAt(indexOf)) == ' ' || charAt == 9 || charAt == 13 || charAt == 10)) {
            }
        }
        return this.f33b.substring(indexOf);
    }
}
