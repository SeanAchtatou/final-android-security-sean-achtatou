package a.a.a;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public final class y {
    private static boolean e;
    private static boolean f;
    private static boolean g;
    private static boolean h;
    private static final char[] i = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a  reason: collision with root package name */
    private Map f37a;

    /* renamed from: b  reason: collision with root package name */
    private Set f38b;
    private Map c;
    private String d;

    static {
        boolean z;
        boolean z2;
        boolean z3;
        e = false;
        f = false;
        g = false;
        h = false;
        try {
            String property = System.getProperty("mail.mime.encodeparameters");
            e = property != null && property.equalsIgnoreCase("true");
            String property2 = System.getProperty("mail.mime.decodeparameters");
            if (property2 == null || !property2.equalsIgnoreCase("true")) {
                z = false;
            } else {
                z = true;
            }
            f = z;
            String property3 = System.getProperty("mail.mime.decodeparameters.strict");
            if (property3 == null || !property3.equalsIgnoreCase("true")) {
                z2 = false;
            } else {
                z2 = true;
            }
            g = z2;
            String property4 = System.getProperty("mail.mime.applefilenames");
            if (property4 == null || !property4.equalsIgnoreCase("true")) {
                z3 = false;
            } else {
                z3 = true;
            }
            h = z3;
        } catch (SecurityException e2) {
        }
    }

    public y() {
        this.f37a = new LinkedHashMap();
        this.d = null;
        if (f) {
            this.f38b = new HashSet();
            this.c = new HashMap();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:36:0x012c, code lost:
        throw new a.a.a.aa("Expected ';', got \"" + r2.b() + "\"");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public y(java.lang.String r8) {
        /*
            r7 = this;
            r6 = -4
            r5 = -1
            r7.<init>()
            a.a.a.q r1 = new a.a.a.q
            java.lang.String r0 = "()<>@,;:\\\"\t []/?="
            r1.<init>(r8, r0)
        L_0x000c:
            a.a.a.k r2 = r1.a()
            int r0 = r2.a()
            if (r0 == r6) goto L_0x012d
            char r3 = (char) r0
            r4 = 59
            if (r3 != r4) goto L_0x00c2
            a.a.a.k r0 = r1.a()
            int r2 = r0.a()
            if (r2 == r6) goto L_0x012d
            int r2 = r0.a()
            if (r2 == r5) goto L_0x004a
            a.a.a.aa r1 = new a.a.a.aa
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected parameter name, got \""
            r2.<init>(r3)
            java.lang.String r0 = r0.b()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x004a:
            java.lang.String r0 = r0.b()
            java.util.Locale r2 = java.util.Locale.ENGLISH
            java.lang.String r0 = r0.toLowerCase(r2)
            a.a.a.k r2 = r1.a()
            int r3 = r2.a()
            char r3 = (char) r3
            r4 = 61
            if (r3 == r4) goto L_0x0080
            a.a.a.aa r0 = new a.a.a.aa
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected '=', got \""
            r1.<init>(r3)
            java.lang.String r2 = r2.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0080:
            a.a.a.k r2 = r1.a()
            int r3 = r2.a()
            if (r3 == r5) goto L_0x00ac
            r4 = -2
            if (r3 == r4) goto L_0x00ac
            a.a.a.aa r0 = new a.a.a.aa
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected parameter value, got \""
            r1.<init>(r3)
            java.lang.String r2 = r2.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00ac:
            java.lang.String r2 = r2.b()
            r7.d = r0
            boolean r3 = a.a.a.y.f
            if (r3 == 0) goto L_0x00bb
            r7.b(r0, r2)
            goto L_0x000c
        L_0x00bb:
            java.util.Map r3 = r7.f37a
            r3.put(r0, r2)
            goto L_0x000c
        L_0x00c2:
            boolean r3 = a.a.a.y.h
            if (r3 == 0) goto L_0x010e
            if (r0 != r5) goto L_0x010e
            java.lang.String r0 = r7.d
            if (r0 == 0) goto L_0x010e
            java.lang.String r0 = r7.d
            java.lang.String r3 = "name"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x00e0
            java.lang.String r0 = r7.d
            java.lang.String r3 = "filename"
            boolean r0 = r0.equals(r3)
            if (r0 == 0) goto L_0x010e
        L_0x00e0:
            java.util.Map r0 = r7.f37a
            java.lang.String r3 = r7.d
            java.lang.Object r0 = r0.get(r3)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r3.<init>(r0)
            java.lang.String r0 = " "
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r2 = r2.b()
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.util.Map r2 = r7.f37a
            java.lang.String r3 = r7.d
            r2.put(r3, r0)
            goto L_0x000c
        L_0x010e:
            a.a.a.aa r0 = new a.a.a.aa
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Expected ';', got \""
            r1.<init>(r3)
            java.lang.String r2 = r2.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\""
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x012d:
            boolean r0 = a.a.a.y.f
            if (r0 == 0) goto L_0x0135
            r0 = 0
            r7.a(r0)
        L_0x0135:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.y.<init>(java.lang.String):void");
    }

    private void b(String str, String str2) {
        m mVar;
        String str3;
        int indexOf = str.indexOf(42);
        if (indexOf < 0) {
            this.f37a.put(str, str2);
        } else if (indexOf == str.length() - 1) {
            this.f37a.put(str.substring(0, indexOf), c(str2));
        } else {
            String substring = str.substring(0, indexOf);
            this.f38b.add(substring);
            this.f37a.put(substring, "");
            if (str.endsWith("*")) {
                m mVar2 = new m();
                mVar2.c = str2;
                mVar2.f20a = str2;
                m mVar3 = mVar2;
                str3 = str.substring(0, str.length() - 1);
                mVar = mVar3;
            } else {
                mVar = str2;
                str3 = str;
            }
            this.c.put(str3, mVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:93:0x00df A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x00f0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0101 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(boolean r15) {
        /*
            r14 = this;
            r12 = 0
            r11 = 0
            java.util.Set r0 = r14.f38b     // Catch:{ all -> 0x00a0 }
            java.util.Iterator r2 = r0.iterator()     // Catch:{ all -> 0x00a0 }
        L_0x0008:
            boolean r0 = r2.hasNext()     // Catch:{ all -> 0x00a0 }
            if (r0 != 0) goto L_0x0038
            java.util.Map r0 = r14.c
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x002d
            java.util.Map r0 = r14.c
            java.util.Collection r0 = r0.values()
            java.util.Iterator r1 = r0.iterator()
        L_0x0020:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0135
            java.util.Map r0 = r14.f37a
            java.util.Map r1 = r14.c
            r0.putAll(r1)
        L_0x002d:
            java.util.Set r0 = r14.f38b
            r0.clear()
            java.util.Map r0 = r14.c
            r0.clear()
            return
        L_0x0038:
            java.lang.Object r0 = r2.next()     // Catch:{ all -> 0x00a0 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x00a0 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ all -> 0x00a0 }
            r3.<init>()     // Catch:{ all -> 0x00a0 }
            a.a.a.p r4 = new a.a.a.p     // Catch:{ all -> 0x00a0 }
            r4.<init>()     // Catch:{ all -> 0x00a0 }
            r5 = r11
            r6 = r12
        L_0x004a:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a0 }
            java.lang.String r7 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x00a0 }
            r1.<init>(r7)     // Catch:{ all -> 0x00a0 }
            java.lang.String r7 = "*"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x00a0 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ all -> 0x00a0 }
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x00a0 }
            java.util.Map r1 = r14.c     // Catch:{ all -> 0x00a0 }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x0097
            r4.add(r1)     // Catch:{ all -> 0x00a0 }
            boolean r8 = r1 instanceof a.a.a.m     // Catch:{ all -> 0x00a0 }
            if (r8 == 0) goto L_0x010b
            a.a.a.m r1 = (a.a.a.m) r1     // Catch:{ NumberFormatException -> 0x0161, UnsupportedEncodingException -> 0x0158, StringIndexOutOfBoundsException -> 0x014f }
            java.lang.String r8 = r1.c     // Catch:{ NumberFormatException -> 0x0161, UnsupportedEncodingException -> 0x0158, StringIndexOutOfBoundsException -> 0x014f }
            if (r5 != 0) goto L_0x0090
            a.a.a.m r9 = c(r8)     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
            java.lang.String r10 = r9.f21b     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
            r1.f21b = r10     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
            java.lang.String r6 = r9.f20a     // Catch:{ NumberFormatException -> 0x00d8, UnsupportedEncodingException -> 0x00e9, StringIndexOutOfBoundsException -> 0x00fa }
            r1.f20a = r6     // Catch:{ NumberFormatException -> 0x00d8, UnsupportedEncodingException -> 0x00e9, StringIndexOutOfBoundsException -> 0x00fa }
            r1 = r6
            r6 = r10
        L_0x0084:
            r3.append(r1)     // Catch:{ all -> 0x00a0 }
            java.util.Map r1 = r14.c     // Catch:{ all -> 0x00a0 }
            r1.remove(r7)     // Catch:{ all -> 0x00a0 }
            int r1 = r5 + 1
            r5 = r1
            goto L_0x004a
        L_0x0090:
            if (r6 != 0) goto L_0x00d0
            java.util.Set r1 = r14.f38b     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
            r1.remove(r0)     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
        L_0x0097:
            if (r5 != 0) goto L_0x010f
            java.util.Map r1 = r14.f37a     // Catch:{ all -> 0x00a0 }
            r1.remove(r0)     // Catch:{ all -> 0x00a0 }
            goto L_0x0008
        L_0x00a0:
            r0 = move-exception
            r1 = r0
            if (r15 != 0) goto L_0x00a6
            if (r11 == 0) goto L_0x00cf
        L_0x00a6:
            java.util.Map r0 = r14.c
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x00c5
            java.util.Map r0 = r14.c
            java.util.Collection r0 = r0.values()
            java.util.Iterator r2 = r0.iterator()
        L_0x00b8:
            boolean r0 = r2.hasNext()
            if (r0 != 0) goto L_0x011c
            java.util.Map r0 = r14.f37a
            java.util.Map r2 = r14.c
            r0.putAll(r2)
        L_0x00c5:
            java.util.Set r0 = r14.f38b
            r0.clear()
            java.util.Map r0 = r14.c
            r0.clear()
        L_0x00cf:
            throw r1
        L_0x00d0:
            java.lang.String r9 = c(r8, r6)     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
            r1.f20a = r9     // Catch:{ NumberFormatException -> 0x0166, UnsupportedEncodingException -> 0x015c, StringIndexOutOfBoundsException -> 0x0153 }
            r1 = r9
            goto L_0x0084
        L_0x00d8:
            r1 = move-exception
            r6 = r8
            r8 = r10
        L_0x00db:
            boolean r9 = a.a.a.y.g     // Catch:{ all -> 0x00a0 }
            if (r9 == 0) goto L_0x016c
            a.a.a.aa r0 = new a.a.a.aa     // Catch:{ all -> 0x00a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00a0 }
            r0.<init>(r1)     // Catch:{ all -> 0x00a0 }
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x00e9:
            r1 = move-exception
            r6 = r8
            r8 = r10
        L_0x00ec:
            boolean r9 = a.a.a.y.g     // Catch:{ all -> 0x00a0 }
            if (r9 == 0) goto L_0x016c
            a.a.a.aa r0 = new a.a.a.aa     // Catch:{ all -> 0x00a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00a0 }
            r0.<init>(r1)     // Catch:{ all -> 0x00a0 }
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x00fa:
            r1 = move-exception
            r6 = r8
            r8 = r10
        L_0x00fd:
            boolean r9 = a.a.a.y.g     // Catch:{ all -> 0x00a0 }
            if (r9 == 0) goto L_0x016c
            a.a.a.aa r0 = new a.a.a.aa     // Catch:{ all -> 0x00a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00a0 }
            r0.<init>(r1)     // Catch:{ all -> 0x00a0 }
            throw r0     // Catch:{ all -> 0x00a0 }
        L_0x010b:
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00a0 }
            goto L_0x0084
        L_0x010f:
            java.lang.String r1 = r3.toString()     // Catch:{ all -> 0x00a0 }
            r4.f26a = r1     // Catch:{ all -> 0x00a0 }
            java.util.Map r1 = r14.f37a     // Catch:{ all -> 0x00a0 }
            r1.put(r0, r4)     // Catch:{ all -> 0x00a0 }
            goto L_0x0008
        L_0x011c:
            java.lang.Object r0 = r2.next()
            boolean r3 = r0 instanceof a.a.a.m
            if (r3 == 0) goto L_0x00b8
            a.a.a.m r0 = (a.a.a.m) r0
            java.lang.String r3 = r0.c
            a.a.a.m r3 = c(r3)
            java.lang.String r4 = r3.f21b
            r0.f21b = r4
            java.lang.String r3 = r3.f20a
            r0.f20a = r3
            goto L_0x00b8
        L_0x0135:
            java.lang.Object r0 = r1.next()
            boolean r2 = r0 instanceof a.a.a.m
            if (r2 == 0) goto L_0x0020
            a.a.a.m r0 = (a.a.a.m) r0
            java.lang.String r2 = r0.c
            a.a.a.m r2 = c(r2)
            java.lang.String r3 = r2.f21b
            r0.f21b = r3
            java.lang.String r2 = r2.f20a
            r0.f20a = r2
            goto L_0x0020
        L_0x014f:
            r1 = move-exception
            r8 = r6
            r6 = r12
            goto L_0x00fd
        L_0x0153:
            r1 = move-exception
            r13 = r8
            r8 = r6
            r6 = r13
            goto L_0x00fd
        L_0x0158:
            r1 = move-exception
            r8 = r6
            r6 = r12
            goto L_0x00ec
        L_0x015c:
            r1 = move-exception
            r13 = r8
            r8 = r6
            r6 = r13
            goto L_0x00ec
        L_0x0161:
            r1 = move-exception
            r8 = r6
            r6 = r12
            goto L_0x00db
        L_0x0166:
            r1 = move-exception
            r13 = r8
            r8 = r6
            r6 = r13
            goto L_0x00db
        L_0x016c:
            r1 = r6
            r6 = r8
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.y.a(boolean):void");
    }

    public final String a(String str) {
        Object obj = this.f37a.get(str.trim().toLowerCase(Locale.ENGLISH));
        if (obj instanceof p) {
            return ((p) obj).f26a;
        }
        if (obj instanceof m) {
            return ((m) obj).f20a;
        }
        return (String) obj;
    }

    public final void a(String str, String str2) {
        if (str != null || str2 == null || !str2.equals("DONE")) {
            String lowerCase = str.trim().toLowerCase(Locale.ENGLISH);
            if (f) {
                try {
                    b(lowerCase, str2);
                } catch (aa e2) {
                    this.f37a.put(lowerCase, str2);
                }
            } else {
                this.f37a.put(lowerCase, str2);
            }
        } else if (f && this.f38b.size() > 0) {
            try {
                a(true);
            } catch (aa e3) {
            }
        }
    }

    public final String toString() {
        return a(0);
    }

    public final String a(int i2) {
        t tVar = new t(i2);
        for (String str : this.f37a.keySet()) {
            Object obj = this.f37a.get(str);
            if (obj instanceof p) {
                p pVar = (p) obj;
                String str2 = String.valueOf(str) + "*";
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 >= pVar.size()) {
                        break;
                    }
                    Object obj2 = pVar.get(i4);
                    if (obj2 instanceof m) {
                        tVar.a(String.valueOf(str2) + i4 + "*", ((m) obj2).c);
                    } else {
                        tVar.a(String.valueOf(str2) + i4, (String) obj2);
                    }
                    i3 = i4 + 1;
                }
            } else if (obj instanceof m) {
                tVar.a(String.valueOf(str) + "*", ((m) obj).c);
            } else {
                tVar.a(str, (String) obj);
            }
        }
        return tVar.toString();
    }

    private static m c(String str) {
        m mVar = new m();
        mVar.c = str;
        mVar.f20a = str;
        try {
            int indexOf = str.indexOf(39);
            if (indexOf > 0) {
                String substring = str.substring(0, indexOf);
                int indexOf2 = str.indexOf(39, indexOf + 1);
                if (indexOf2 >= 0) {
                    str.substring(indexOf + 1, indexOf2);
                    String substring2 = str.substring(indexOf2 + 1);
                    mVar.f21b = substring;
                    mVar.f20a = c(substring2, substring);
                } else if (g) {
                    throw new aa("Missing language in encoded value: " + str);
                }
            } else if (g) {
                throw new aa("Missing charset in encoded value: " + str);
            }
        } catch (NumberFormatException e2) {
            if (g) {
                throw new aa(e2.toString());
            }
        } catch (UnsupportedEncodingException e3) {
            if (g) {
                throw new aa(e3.toString());
            }
        } catch (StringIndexOutOfBoundsException e4) {
            if (g) {
                throw new aa(e4.toString());
            }
        }
        return mVar;
    }

    private static String c(String str, String str2) {
        byte[] bArr = new byte[str.length()];
        int i2 = 0;
        int i3 = 0;
        while (i3 < str.length()) {
            char charAt = str.charAt(i3);
            if (charAt == '%') {
                charAt = (char) Integer.parseInt(str.substring(i3 + 1, i3 + 3), 16);
                i3 += 2;
            }
            bArr[i2] = (byte) charAt;
            i3++;
            i2++;
        }
        return new String(bArr, 0, i2, b.d(str2));
    }
}
