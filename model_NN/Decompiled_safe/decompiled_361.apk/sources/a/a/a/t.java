package a.a.a;

final class t {

    /* renamed from: a  reason: collision with root package name */
    private int f30a;

    /* renamed from: b  reason: collision with root package name */
    private StringBuffer f31b = new StringBuffer();

    public t(int i) {
        this.f30a = i;
    }

    public final void a(String str, String str2) {
        String b2 = b.a(str2, "()<>@,;:\\\"\t []/?=");
        this.f31b.append("; ");
        this.f30a += 2;
        if (str.length() + b2.length() + 1 + this.f30a > 76) {
            this.f31b.append("\r\n\t");
            this.f30a = 8;
        }
        this.f31b.append(str).append('=');
        this.f30a += str.length() + 1;
        if (this.f30a + b2.length() > 76) {
            String a2 = b.a(this.f30a, b2);
            this.f31b.append(a2);
            int lastIndexOf = a2.lastIndexOf(10);
            if (lastIndexOf >= 0) {
                this.f30a = ((a2.length() - lastIndexOf) - 1) + this.f30a;
                return;
            }
            this.f30a = a2.length() + this.f30a;
            return;
        }
        this.f31b.append(b2);
        this.f30a = b2.length() + this.f30a;
    }

    public final String toString() {
        return this.f31b.toString();
    }
}
