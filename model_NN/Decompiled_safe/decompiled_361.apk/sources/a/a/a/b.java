package a.a.a;

import a.a.af;
import a.b.a;
import com.a.a.a.c;
import com.a.a.a.d;
import com.a.a.a.e;
import com.a.a.a.f;
import com.a.a.a.g;
import com.a.a.a.j;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f3b;
    private static boolean c;
    private static boolean d;
    private static String e;
    private static String f;
    private static Hashtable g = new Hashtable(10);
    private static Hashtable h = new Hashtable(40);

    private b() {
    }

    static {
        g gVar;
        Throwable th;
        boolean z;
        boolean z2;
        boolean z3;
        f2a = true;
        f3b = false;
        c = false;
        d = true;
        try {
            String property = System.getProperty("mail.mime.decodetext.strict");
            f2a = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.encodeeol.strict");
            if (property2 == null || !property2.equalsIgnoreCase("true")) {
                z = false;
            } else {
                z = true;
            }
            f3b = z;
            String property3 = System.getProperty("mail.mime.foldencodedwords");
            if (property3 == null || !property3.equalsIgnoreCase("true")) {
                z2 = false;
            } else {
                z2 = true;
            }
            c = z2;
            String property4 = System.getProperty("mail.mime.foldtext");
            if (property4 == null || !property4.equalsIgnoreCase("false")) {
                z3 = true;
            } else {
                z3 = false;
            }
            d = z3;
        } catch (SecurityException e2) {
        }
        try {
            InputStream resourceAsStream = b.class.getResourceAsStream("/META-INF/javamail.charset.map");
            if (resourceAsStream != null) {
                try {
                    gVar = new g(resourceAsStream);
                    try {
                        a(gVar, h);
                        a(gVar, g);
                        try {
                            gVar.close();
                        } catch (Exception e3) {
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        try {
                            gVar.close();
                        } catch (Exception e4) {
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    gVar = resourceAsStream;
                    th = th4;
                    gVar.close();
                    throw th;
                }
            }
        } catch (Exception e5) {
        }
        if (h.isEmpty()) {
            h.put("8859_1", "ISO-8859-1");
            h.put("iso8859_1", "ISO-8859-1");
            h.put("iso8859-1", "ISO-8859-1");
            h.put("8859_2", "ISO-8859-2");
            h.put("iso8859_2", "ISO-8859-2");
            h.put("iso8859-2", "ISO-8859-2");
            h.put("8859_3", "ISO-8859-3");
            h.put("iso8859_3", "ISO-8859-3");
            h.put("iso8859-3", "ISO-8859-3");
            h.put("8859_4", "ISO-8859-4");
            h.put("iso8859_4", "ISO-8859-4");
            h.put("iso8859-4", "ISO-8859-4");
            h.put("8859_5", "ISO-8859-5");
            h.put("iso8859_5", "ISO-8859-5");
            h.put("iso8859-5", "ISO-8859-5");
            h.put("8859_6", "ISO-8859-6");
            h.put("iso8859_6", "ISO-8859-6");
            h.put("iso8859-6", "ISO-8859-6");
            h.put("8859_7", "ISO-8859-7");
            h.put("iso8859_7", "ISO-8859-7");
            h.put("iso8859-7", "ISO-8859-7");
            h.put("8859_8", "ISO-8859-8");
            h.put("iso8859_8", "ISO-8859-8");
            h.put("iso8859-8", "ISO-8859-8");
            h.put("8859_9", "ISO-8859-9");
            h.put("iso8859_9", "ISO-8859-9");
            h.put("iso8859-9", "ISO-8859-9");
            h.put("sjis", "Shift_JIS");
            h.put("jis", "ISO-2022-JP");
            h.put("iso2022jp", "ISO-2022-JP");
            h.put("euc_jp", "euc-jp");
            h.put("koi8_r", "koi8-r");
            h.put("euc_cn", "euc-cn");
            h.put("euc_tw", "euc-tw");
            h.put("euc_kr", "euc-kr");
        }
        if (g.isEmpty()) {
            g.put("iso-2022-cn", "ISO2022CN");
            g.put("iso-2022-kr", "ISO2022KR");
            g.put("utf-8", "UTF8");
            g.put("utf8", "UTF8");
            g.put("ja_jp.iso2022-7", "ISO2022JP");
            g.put("ja_jp.eucjp", "EUCJIS");
            g.put("euc-kr", "KSC5601");
            g.put("euckr", "KSC5601");
            g.put("us-ascii", "ISO-8859-1");
            g.put("x-us-ascii", "ISO-8859-1");
        }
    }

    private static String a(a.b.g gVar) {
        String str;
        try {
            i iVar = new i(gVar.b());
            InputStream a2 = gVar.a();
            switch (a(a2, !iVar.b("text/*"))) {
                case 1:
                    str = "7bit";
                    break;
                case 2:
                    str = "quoted-printable";
                    break;
                default:
                    str = "base64";
                    break;
            }
            try {
                a2.close();
                return str;
            } catch (IOException e2) {
                return str;
            }
        } catch (Exception e3) {
            return "base64";
        }
    }

    public static String a(a aVar) {
        if (aVar.b() != null) {
            return a(aVar.a());
        }
        try {
            if (new i(aVar.c()).b("text/*")) {
                d dVar = new d(false, false);
                try {
                    aVar.a(dVar);
                } catch (IOException e2) {
                }
                switch (dVar.a()) {
                    case 1:
                        return "7bit";
                    case 2:
                        return "quoted-printable";
                    default:
                        return "base64";
                }
            } else {
                d dVar2 = new d(true, f3b);
                try {
                    aVar.a(dVar2);
                } catch (IOException e3) {
                }
                if (dVar2.a() == 1) {
                    return "7bit";
                }
                return "base64";
            }
        } catch (Exception e4) {
            return "base64";
        }
    }

    public static InputStream a(InputStream inputStream, String str) {
        if (str.equalsIgnoreCase("base64")) {
            return new e(inputStream);
        }
        if (str.equalsIgnoreCase("quoted-printable")) {
            return new j(inputStream);
        }
        if (str.equalsIgnoreCase("uuencode") || str.equalsIgnoreCase("x-uuencode") || str.equalsIgnoreCase("x-uue")) {
            return new f(inputStream);
        }
        if (str.equalsIgnoreCase("binary") || str.equalsIgnoreCase("7bit") || str.equalsIgnoreCase("8bit")) {
            return inputStream;
        }
        throw new af("Unknown encoding: " + str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.b.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.b.a(java.io.InputStream, boolean):int
      a.a.a.b.a(java.io.InputStream, java.lang.String):java.io.InputStream
      a.a.a.b.a(int, java.lang.String):java.lang.String
      a.a.a.b.a(java.lang.String, java.lang.String):java.lang.String
      a.a.a.b.a(com.a.a.a.g, java.util.Hashtable):void
      a.a.a.b.a(java.lang.String, boolean):java.lang.String */
    public static String a(String str) {
        return a(str, false);
    }

    public static String b(String str) {
        String str2;
        boolean z;
        if (str.indexOf("=?") == -1) {
            return str;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, " \t\n\r", true);
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        boolean z2 = false;
        while (stringTokenizer.hasMoreTokens()) {
            String nextToken = stringTokenizer.nextToken();
            char charAt = nextToken.charAt(0);
            if (charAt == ' ' || charAt == 9 || charAt == 13 || charAt == 10) {
                stringBuffer2.append(charAt);
            } else {
                try {
                    String f2 = f(nextToken);
                    if (!z2 && stringBuffer2.length() > 0) {
                        stringBuffer.append(stringBuffer2);
                    }
                    str2 = f2;
                    z = true;
                } catch (aa e2) {
                    if (!f2a) {
                        String g2 = g(nextToken);
                        if (g2 != nextToken) {
                            if ((!z2 || !nextToken.startsWith("=?")) && stringBuffer2.length() > 0) {
                                stringBuffer.append(stringBuffer2);
                            }
                            z = nextToken.endsWith("?=");
                            str2 = g2;
                        } else {
                            if (stringBuffer2.length() > 0) {
                                stringBuffer.append(stringBuffer2);
                            }
                            str2 = nextToken;
                            z = false;
                        }
                    } else {
                        if (stringBuffer2.length() > 0) {
                            stringBuffer.append(stringBuffer2);
                        }
                        str2 = nextToken;
                        z = false;
                    }
                }
                stringBuffer.append(str2);
                stringBuffer2.setLength(0);
                z2 = z;
            }
        }
        stringBuffer.append(stringBuffer2);
        return stringBuffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.b.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.b.a(java.io.InputStream, boolean):int
      a.a.a.b.a(java.io.InputStream, java.lang.String):java.io.InputStream
      a.a.a.b.a(int, java.lang.String):java.lang.String
      a.a.a.b.a(java.lang.String, java.lang.String):java.lang.String
      a.a.a.b.a(com.a.a.a.g, java.util.Hashtable):void
      a.a.a.b.a(java.lang.String, boolean):java.lang.String */
    public static String c(String str) {
        return a(str, true);
    }

    private static String a(String str, boolean z) {
        String str2;
        boolean z2;
        int e2 = e(str);
        if (e2 == 1) {
            return str;
        }
        String b2 = b();
        String a2 = a();
        if (e2 != 3) {
            str2 = "Q";
        } else {
            str2 = "B";
        }
        if (str2.equalsIgnoreCase("B")) {
            z2 = true;
        } else if (str2.equalsIgnoreCase("Q")) {
            z2 = false;
        } else {
            throw new UnsupportedEncodingException("Unknown transfer encoding: " + str2);
        }
        StringBuffer stringBuffer = new StringBuffer();
        a(str, z2, b2, 68 - a2.length(), "=?" + a2 + "?" + str2 + "?", true, z, stringBuffer);
        return stringBuffer.toString();
    }

    private static void a(String str, boolean z, String str2, int i, String str3, boolean z2, boolean z3, StringBuffer stringBuffer) {
        byte[] bytes;
        int a2;
        OutputStream cVar;
        int length;
        StringBuffer stringBuffer2 = stringBuffer;
        boolean z4 = z3;
        boolean z5 = z2;
        String str4 = str3;
        int i2 = i;
        String str5 = str2;
        boolean z6 = z;
        while (true) {
            bytes = str.getBytes(str5);
            if (z6) {
                a2 = ((bytes.length + 2) / 3) * 4;
            } else {
                a2 = c.a(bytes, z4);
            }
            if (a2 <= i2 || (length = str.length()) <= 1) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            } else {
                a(str.substring(0, length / 2), z6, str5, i2, str4, z5, z4, stringBuffer2);
                str = str.substring(length / 2, length);
                z5 = false;
            }
        }
        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        if (z6) {
            cVar = new com.a.a.a.b(byteArrayOutputStream2);
        } else {
            cVar = new c(byteArrayOutputStream2, z4);
        }
        cVar.write(bytes);
        cVar.close();
        byte[] byteArray = byteArrayOutputStream2.toByteArray();
        if (!z5) {
            if (c) {
                stringBuffer2.append("\r\n ");
            } else {
                stringBuffer2.append(" ");
            }
        }
        stringBuffer2.append(str4);
        for (byte b2 : byteArray) {
            stringBuffer2.append((char) b2);
        }
        stringBuffer2.append("?=");
    }

    private static String f(String str) {
        String str2;
        InputStream dVar;
        if (!str.startsWith("=?")) {
            throw new aa("encoded word does not start with \"=?\": " + str);
        }
        int indexOf = str.indexOf(63, 2);
        if (indexOf == -1) {
            throw new aa("encoded word does not include charset: " + str);
        }
        String d2 = d(str.substring(2, indexOf));
        int i = indexOf + 1;
        int indexOf2 = str.indexOf(63, i);
        if (indexOf2 == -1) {
            throw new aa("encoded word does not include encoding: " + str);
        }
        String substring = str.substring(i, indexOf2);
        int i2 = indexOf2 + 1;
        int indexOf3 = str.indexOf("?=", i2);
        if (indexOf3 == -1) {
            throw new aa("encoded word does not end with \"?=\": " + str);
        }
        String substring2 = str.substring(i2, indexOf3);
        try {
            if (substring2.length() > 0) {
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(com.a.a.a.a.a(substring2));
                if (substring.equalsIgnoreCase("B")) {
                    dVar = new e(byteArrayInputStream);
                } else if (substring.equalsIgnoreCase("Q")) {
                    dVar = new d(byteArrayInputStream);
                } else {
                    throw new UnsupportedEncodingException("unknown encoding: " + substring);
                }
                int available = byteArrayInputStream.available();
                byte[] bArr = new byte[available];
                int read = dVar.read(bArr, 0, available);
                if (read <= 0) {
                    str2 = "";
                } else {
                    str2 = new String(bArr, 0, read, d2);
                }
            } else {
                str2 = "";
            }
            if (indexOf3 + 2 >= str.length()) {
                return str2;
            }
            String substring3 = str.substring(indexOf3 + 2);
            if (!f2a) {
                substring3 = g(substring3);
            }
            return String.valueOf(str2) + substring3;
        } catch (UnsupportedEncodingException e2) {
            throw e2;
        } catch (IOException e3) {
            throw new aa(e3.toString());
        } catch (IllegalArgumentException e4) {
            throw new UnsupportedEncodingException(d2);
        }
    }

    private static String g(String str) {
        int indexOf;
        int indexOf2;
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int indexOf3 = str.indexOf("=?", i);
            if (indexOf3 >= 0) {
                stringBuffer.append(str.substring(i, indexOf3));
                int indexOf4 = str.indexOf(63, indexOf3 + 2);
                if (indexOf4 < 0 || (indexOf = str.indexOf(63, indexOf4 + 1)) < 0 || (indexOf2 = str.indexOf("?=", indexOf + 1)) < 0) {
                    break;
                }
                String substring = str.substring(indexOf3, indexOf2 + 2);
                try {
                    substring = f(substring);
                } catch (aa e2) {
                }
                stringBuffer.append(substring);
                i = indexOf2 + 2;
            } else {
                break;
            }
        }
        if (i == 0) {
            return str;
        }
        if (i < str.length()) {
            stringBuffer.append(str.substring(i));
        }
        return stringBuffer.toString();
    }

    public static String a(String str, String str2) {
        int length = str.length();
        int i = 0;
        boolean z = false;
        while (i < length) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == '\\' || charAt == 13 || charAt == 10) {
                StringBuffer stringBuffer = new StringBuffer(length + 3);
                stringBuffer.append('\"');
                stringBuffer.append(str.substring(0, i));
                char c2 = 0;
                while (i < length) {
                    char charAt2 = str.charAt(i);
                    if ((charAt2 == '\"' || charAt2 == '\\' || charAt2 == 13 || charAt2 == 10) && !(charAt2 == 10 && c2 == 13)) {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt2);
                    i++;
                    c2 = charAt2;
                }
                stringBuffer.append('\"');
                return stringBuffer.toString();
            }
            if (charAt < ' ' || charAt >= 127 || str2.indexOf(charAt) >= 0) {
                z = true;
            }
            i++;
        }
        if (!z) {
            return str;
        }
        StringBuffer stringBuffer2 = new StringBuffer(length + 2);
        stringBuffer2.append('\"').append(str).append('\"');
        return stringBuffer2.toString();
    }

    public static String a(int i, String str) {
        String str2;
        String str3;
        if (!d) {
            return str;
        }
        int length = str.length() - 1;
        while (length >= 0) {
            char charAt = str.charAt(length);
            if (charAt != ' ' && charAt != 9 && charAt != 13 && charAt != 10) {
                break;
            }
            length--;
        }
        if (length != str.length() - 1) {
            str2 = str.substring(0, length + 1);
        } else {
            str2 = str;
        }
        if (str2.length() + i <= 76) {
            return str2;
        }
        StringBuffer stringBuffer = new StringBuffer(str2.length() + 4);
        String str4 = str2;
        int i2 = i;
        char c2 = 0;
        while (true) {
            if (str4.length() + i2 <= 76) {
                str3 = str4;
                break;
            }
            int i3 = -1;
            char c3 = c2;
            int i4 = 0;
            while (i4 < str4.length() && (i3 == -1 || i2 + i4 <= 76)) {
                char charAt2 = str4.charAt(i4);
                if (!((charAt2 != ' ' && charAt2 != 9) || c3 == ' ' || c3 == 9)) {
                    i3 = i4;
                }
                i4++;
                c3 = charAt2;
            }
            if (i3 == -1) {
                stringBuffer.append(str4);
                str3 = "";
                break;
            }
            stringBuffer.append(str4.substring(0, i3));
            stringBuffer.append("\r\n");
            c2 = str4.charAt(i3);
            stringBuffer.append(c2);
            str4 = str4.substring(i3 + 1);
            i2 = 1;
        }
        stringBuffer.append(str3);
        return stringBuffer.toString();
    }

    public static String d(String str) {
        if (g == null || str == null) {
            return str;
        }
        String str2 = (String) g.get(str.toLowerCase(Locale.ENGLISH));
        return str2 == null ? str : str2;
    }

    private static String b() {
        if (e == null) {
            String str = null;
            try {
                str = System.getProperty("mail.mime.charset");
            } catch (SecurityException e2) {
            }
            if (str == null || str.length() <= 0) {
                try {
                    e = System.getProperty("file.encoding", "8859_1");
                } catch (SecurityException e3) {
                    String encoding = new InputStreamReader(new a()).getEncoding();
                    e = encoding;
                    if (encoding == null) {
                        e = "8859_1";
                    }
                }
            } else {
                String d2 = d(str);
                e = d2;
                return d2;
            }
        }
        return e;
    }

    static String a() {
        String str;
        if (f == null) {
            try {
                f = System.getProperty("mail.mime.charset");
            } catch (SecurityException e2) {
            }
        }
        if (f == null) {
            String b2 = b();
            if (h == null || b2 == null) {
                str = b2;
            } else {
                str = (String) h.get(b2.toLowerCase(Locale.ENGLISH));
                if (str == null) {
                    str = b2;
                }
            }
            f = str;
        }
        return f;
    }

    private static void a(g gVar, Hashtable hashtable) {
        while (true) {
            try {
                String a2 = gVar.a();
                if (a2 == null) {
                    return;
                }
                if (a2.startsWith("--") && a2.endsWith("--")) {
                    return;
                }
                if (a2.trim().length() != 0 && !a2.startsWith("#")) {
                    StringTokenizer stringTokenizer = new StringTokenizer(a2, " \t");
                    try {
                        String nextToken = stringTokenizer.nextToken();
                        hashtable.put(nextToken.toLowerCase(Locale.ENGLISH), stringTokenizer.nextToken());
                    } catch (NoSuchElementException e2) {
                    }
                }
            } catch (IOException e3) {
                return;
            }
        }
    }

    static int e(String str) {
        int length = str.length();
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            if (a(str.charAt(i3))) {
                i++;
            } else {
                i2++;
            }
        }
        if (i == 0) {
            return 1;
        }
        if (i2 > i) {
            return 2;
        }
        return 3;
    }

    private static int a(InputStream inputStream, boolean z) {
        boolean z2;
        boolean z3;
        int i;
        int i2;
        boolean z4;
        int i3;
        boolean z5;
        int i4;
        int i5;
        int i6 = 0;
        boolean z6 = f3b && z;
        byte[] bArr = new byte[4096];
        int i7 = 0;
        boolean z7 = false;
        boolean z8 = false;
        int i8 = 0;
        while (true) {
            try {
                int read = inputStream.read(bArr, 0, 4096);
                if (read == -1) {
                    z2 = z7;
                    z3 = z8;
                    i = i8;
                    i2 = i7;
                    break;
                }
                int i9 = i7;
                int i10 = i6;
                boolean z9 = z7;
                int i11 = 0;
                int i12 = i8;
                boolean z10 = z8;
                byte b2 = 0;
                while (i11 < read) {
                    try {
                        byte b3 = bArr[i11] & 255;
                        if (!z6 || ((b2 != 13 || b3 == 10) && (b2 == 13 || b3 != 10))) {
                            z4 = z9;
                        } else {
                            z4 = true;
                        }
                        if (b3 == 13 || b3 == 10) {
                            boolean z11 = z10;
                            i3 = 0;
                            z5 = z11;
                        } else {
                            int i13 = i10 + 1;
                            if (i13 > 998) {
                                i3 = i13;
                                z5 = true;
                            } else {
                                boolean z12 = z10;
                                i3 = i13;
                                z5 = z12;
                            }
                        }
                        try {
                            if (!a(b3)) {
                                int i14 = i12;
                                i4 = i9 + 1;
                                i5 = i14;
                            } else if (z) {
                                return 3;
                            } else {
                                i5 = i12 + 1;
                                i4 = i9;
                            }
                            i11++;
                            i9 = i4;
                            i12 = i5;
                            i10 = i3;
                            z10 = z5;
                            z9 = z4;
                            b2 = b3;
                        } catch (IOException e2) {
                            z2 = z4;
                            z3 = z5;
                            i = i12;
                            i2 = i9;
                        }
                    } catch (IOException e3) {
                        z2 = z9;
                        z3 = z10;
                        i = i12;
                        i2 = i9;
                    }
                }
                z7 = z9;
                z8 = z10;
                i6 = i10;
                i8 = i12;
                i7 = i9;
            } catch (IOException e4) {
                z2 = z7;
                z3 = z8;
                i = i8;
                i2 = i7;
            }
        }
        if (i == 0) {
            if (z2) {
                return 3;
            }
            if (z3) {
                return 2;
            }
            return 1;
        } else if (i2 > i) {
            return 2;
        } else {
            return 3;
        }
    }

    static final boolean a(int i) {
        return i >= 127 || !(i >= 32 || i == 13 || i == 10 || i == 9);
    }
}
