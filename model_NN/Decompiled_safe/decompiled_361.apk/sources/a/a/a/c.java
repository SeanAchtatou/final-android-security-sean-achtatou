package a.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private String f4a;

    /* renamed from: b  reason: collision with root package name */
    private y f5b;

    public c() {
    }

    public c(String str) {
        q qVar = new q(str, "()<>@,;:\\\"\t []/?=");
        k a2 = qVar.a();
        if (a2.a() != -1) {
            throw new aa();
        }
        this.f4a = a2.b();
        String b2 = qVar.b();
        if (b2 != null) {
            this.f5b = new y(b2);
        }
    }

    public final String a(String str) {
        if (this.f5b == null) {
            return null;
        }
        return this.f5b.a(str);
    }

    public final String toString() {
        if (this.f4a == null) {
            return null;
        }
        if (this.f5b == null) {
            return this.f4a;
        }
        StringBuffer stringBuffer = new StringBuffer(this.f4a);
        stringBuffer.append(this.f5b.a(stringBuffer.length() + 21));
        return stringBuffer.toString();
    }
}
