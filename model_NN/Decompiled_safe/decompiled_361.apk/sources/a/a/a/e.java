package a.a.a;

import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public final class e extends SimpleDateFormat {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f8a = false;

    /* renamed from: b  reason: collision with root package name */
    private static TimeZone f9b = TimeZone.getTimeZone("GMT");
    private static Calendar c = new GregorianCalendar(f9b);

    public e() {
        super("EEE, d MMM yyyy HH:mm:ss 'XXXXX' (z)", Locale.US);
    }

    public final StringBuffer format(Date date, StringBuffer stringBuffer, FieldPosition fieldPosition) {
        int i;
        int length = stringBuffer.length();
        super.format(date, stringBuffer, fieldPosition);
        int i2 = length + 25;
        while (stringBuffer.charAt(i2) != 'X') {
            i2++;
        }
        this.calendar.clear();
        this.calendar.setTime(date);
        int i3 = this.calendar.get(15) + this.calendar.get(16);
        if (i3 < 0) {
            stringBuffer.setCharAt(i2, '-');
            i3 = -i3;
            i = i2 + 1;
        } else {
            stringBuffer.setCharAt(i2, '+');
            i = i2 + 1;
        }
        int i4 = (i3 / 60) / 1000;
        int i5 = i4 / 60;
        int i6 = i4 % 60;
        int i7 = i + 1;
        stringBuffer.setCharAt(i, Character.forDigit(i5 / 10, 10));
        int i8 = i7 + 1;
        stringBuffer.setCharAt(i7, Character.forDigit(i5 % 10, 10));
        stringBuffer.setCharAt(i8, Character.forDigit(i6 / 10, 10));
        stringBuffer.setCharAt(i8 + 1, Character.forDigit(i6 % 10, 10));
        return stringBuffer;
    }

    public final Date parse(String str, ParsePosition parsePosition) {
        return a(str.toCharArray(), parsePosition, isLenient());
    }

    private static Date a(char[] cArr, ParsePosition parsePosition, boolean z) {
        g gVar;
        int i = 0;
        int i2 = 0;
        try {
            gVar = new g(cArr);
            while (true) {
                switch (gVar.f11b[gVar.f10a]) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        int b2 = gVar.b();
                        if (!gVar.a('-')) {
                            gVar.a();
                        }
                        int c2 = gVar.c();
                        if (!gVar.a('-')) {
                            gVar.a();
                        }
                        int b3 = gVar.b();
                        if (b3 < 50) {
                            b3 += 2000;
                        } else if (b3 < 100) {
                            b3 += 1900;
                        }
                        gVar.a();
                        int b4 = gVar.b();
                        if (gVar.f10a >= gVar.f11b.length) {
                            throw new ParseException("No more characters", gVar.f10a);
                        } else if (gVar.f11b[gVar.f10a] == ':') {
                            gVar.f10a++;
                            int b5 = gVar.b();
                            if (gVar.a(':')) {
                                i = gVar.b();
                            }
                            try {
                                gVar.a();
                                if (gVar.f10a >= gVar.f11b.length) {
                                    throw new ParseException("No more characters", gVar.f10a);
                                }
                                char c3 = gVar.f11b[gVar.f10a];
                                if (c3 == '+' || c3 == '-') {
                                    boolean z2 = false;
                                    char[] cArr2 = gVar.f11b;
                                    int i3 = gVar.f10a;
                                    gVar.f10a = i3 + 1;
                                    char c4 = cArr2[i3];
                                    if (c4 == '+') {
                                        z2 = true;
                                    } else if (c4 != '-') {
                                        throw new ParseException("Bad Numeric TimeZone", gVar.f10a);
                                    }
                                    int b6 = gVar.b();
                                    i2 = (b6 % 100) + ((b6 / 100) * 60);
                                    if (z2) {
                                        i2 = -i2;
                                    }
                                    parsePosition.setIndex(gVar.f10a);
                                    return a(b3, c2, b2, b4, b5, i, i2, z);
                                }
                                i2 = gVar.d();
                                parsePosition.setIndex(gVar.f10a);
                                return a(b3, c2, b2, b4, b5, i, i2, z);
                            } catch (ParseException e) {
                                if (f8a) {
                                    System.out.println("No timezone? : '" + new String(cArr) + "'");
                                }
                            }
                        } else {
                            throw new ParseException("Wrong char", gVar.f10a);
                        }
                        break;
                    default:
                        gVar.f10a++;
                }
            }
        } catch (ArrayIndexOutOfBoundsException e2) {
            throw new ParseException("No Number Found", gVar.f10a);
        } catch (Exception e3) {
            if (f8a) {
                System.out.println("Bad date: '" + new String(cArr) + "'");
                e3.printStackTrace();
            }
            parsePosition.setIndex(1);
            return null;
        }
    }

    private static synchronized Date a(int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z) {
        Date time;
        synchronized (e.class) {
            c.clear();
            c.setLenient(z);
            c.set(1, i);
            c.set(2, i2);
            c.set(5, i3);
            c.set(11, i4);
            c.set(12, i5 + i7);
            c.set(13, i6);
            time = c.getTime();
        }
        return time;
    }

    public final void setCalendar(Calendar calendar) {
        throw new RuntimeException("Method setCalendar() shouldn't be called");
    }

    public final void setNumberFormat(NumberFormat numberFormat) {
        throw new RuntimeException("Method setNumberFormat() shouldn't be called");
    }
}
