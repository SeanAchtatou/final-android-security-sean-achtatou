package a.a.a;

import a.a.af;
import a.a.ag;
import a.a.d;
import a.a.j;
import a.a.m;
import a.a.q;
import a.a.s;
import a.a.t;
import a.b.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

public final class x extends t implements f {
    private static e j = new e();
    private static final d l = new d(s.f73a);

    /* renamed from: b  reason: collision with root package name */
    Object f36b;
    private a c;
    private byte[] d;
    private InputStream e;
    private s f;
    private d g;
    private boolean h;
    private boolean i;
    private boolean k;

    public x(j jVar) {
        super(jVar);
        boolean z;
        this.h = false;
        this.i = false;
        this.k = true;
        this.h = true;
        this.f = new s();
        this.g = new d();
        if (this.f75a != null) {
            String a2 = this.f75a.a("mail.mime.address.strict");
            if (a2 == null || !a2.equalsIgnoreCase("false")) {
                z = true;
            } else {
                z = false;
            }
            this.k = z;
        }
    }

    public final void a(q qVar) {
        if (qVar == null) {
            c("From");
        } else {
            b("From", qVar.toString());
        }
    }

    public final void b(q qVar) {
        if (qVar == null) {
            c("Sender");
        } else {
            b("Sender", qVar.toString());
        }
    }

    public final q[] a(ag agVar) {
        if (agVar == u.f32a) {
            String a2 = a("Newsgroups", ",");
            if (a2 == null) {
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(a2, ",");
            Vector vector = new Vector();
            while (stringTokenizer.hasMoreTokens()) {
                vector.addElement(new h(stringTokenizer.nextToken(), (byte) 0));
            }
            int size = vector.size();
            h[] hVarArr = new h[size];
            if (size > 0) {
                vector.copyInto(hVarArr);
            }
            return hVarArr;
        }
        String a3 = a(b(agVar), ",");
        if (a3 == null) {
            return null;
        }
        return w.a(a3, this.k);
    }

    public final q[] c() {
        q[] c2 = super.c();
        q[] a2 = a(u.f32a);
        if (a2 == null) {
            return c2;
        }
        if (c2 == null) {
            return a2;
        }
        q[] qVarArr = new q[(c2.length + a2.length)];
        System.arraycopy(c2, 0, qVarArr, 0, c2.length);
        System.arraycopy(a2, 0, qVarArr, c2.length, a2.length);
        return qVarArr;
    }

    public final void a(ag agVar, q[] qVarArr) {
        String str;
        if (agVar != u.f32a) {
            String b2 = b(agVar);
            String a2 = w.a(qVarArr);
            if (a2 == null) {
                c(b2);
            } else {
                b(b2, a2);
            }
        } else if (qVarArr == null || qVarArr.length == 0) {
            c("Newsgroups");
        } else {
            if (qVarArr == null || qVarArr.length == 0) {
                str = null;
            } else {
                StringBuffer stringBuffer = new StringBuffer(((h) qVarArr[0]).toString());
                int i2 = 1;
                while (true) {
                    int i3 = i2;
                    if (i3 >= qVarArr.length) {
                        break;
                    }
                    stringBuffer.append(",").append(((h) qVarArr[i3]).toString());
                    i2 = i3 + 1;
                }
                str = stringBuffer.toString();
            }
            b("Newsgroups", str);
        }
    }

    public final void d(String str) {
        if (str == null) {
            c("Subject");
            return;
        }
        try {
            b("Subject", b.a(9, b.a(str)));
        } catch (UnsupportedEncodingException e2) {
            throw new af("Encoding error", e2);
        }
    }

    public final void a(Date date) {
        if (date == null) {
            c("Date");
            return;
        }
        synchronized (j) {
            b("Date", j.format(date));
        }
    }

    public final String b() {
        String a2 = a("Content-Type", (String) null);
        if (a2 == null) {
            return "text/plain";
        }
        return a2;
    }

    public final String a() {
        return n.a(this);
    }

    private static String b(ag agVar) {
        if (agVar == ag.f47b) {
            return "To";
        }
        if (agVar == ag.c) {
            return "Cc";
        }
        if (agVar == ag.d) {
            return "Bcc";
        }
        if (agVar == u.f32a) {
            return "Newsgroups";
        }
        throw new af("Invalid Recipient Type");
    }

    /* access modifiers changed from: protected */
    public final InputStream f() {
        if (this.e != null) {
            return ((r) this.e).a(0, -1);
        }
        if (this.d != null) {
            return new a.a.c.a(this.d);
        }
        throw new af("No content");
    }

    public final synchronized a e() {
        if (this.c == null) {
            this.c = new a(new o(this));
        }
        return this.c;
    }

    public final synchronized void a(a aVar) {
        this.c = aVar;
        this.f36b = null;
        n.c(this);
    }

    public final void a(Object obj, String str) {
        if (obj instanceof m) {
            a((m) obj);
        } else {
            a(new a(obj, str));
        }
    }

    public final void a(String str) {
        n.a(this, str, "plain");
    }

    public final void a(m mVar) {
        a(new a(mVar, mVar.c()));
        mVar.a(this);
    }

    public final String[] b(String str) {
        return this.f.a(str);
    }

    public final String a(String str, String str2) {
        return this.f.a(str, str2);
    }

    public final void b(String str, String str2) {
        this.f.b(str, str2);
    }

    public final void c(String str) {
        this.f.b(str);
    }

    public final void d() {
        this.h = true;
        this.i = true;
        n.b(this);
        b("MIME-Version", "1.0");
        b("Message-ID", "<" + z.a(this.f75a) + ">");
        if (this.f36b != null) {
            this.c = new a(this.f36b, b());
            this.f36b = null;
            this.d = null;
            if (this.e != null) {
                try {
                    this.e.close();
                } catch (IOException e2) {
                }
            }
            this.e = null;
        }
    }
}
