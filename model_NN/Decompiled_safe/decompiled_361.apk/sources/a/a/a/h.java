package a.a.a;

import a.a.q;
import java.util.Locale;

public final class h extends q {

    /* renamed from: a  reason: collision with root package name */
    private String f12a;

    /* renamed from: b  reason: collision with root package name */
    private String f13b;

    public h() {
    }

    public h(String str, byte b2) {
        this(str);
    }

    private h(String str) {
        this.f12a = str;
        this.f13b = null;
    }

    public final String a() {
        return "news";
    }

    public final String toString() {
        return this.f12a;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        return this.f12a.equals(hVar.f12a) && ((this.f13b == null && hVar.f13b == null) || !(this.f13b == null || hVar.f13b == null || !this.f13b.equalsIgnoreCase(hVar.f13b)));
    }

    public final int hashCode() {
        int i = 0;
        if (this.f12a != null) {
            i = this.f12a.hashCode() + 0;
        }
        if (this.f13b != null) {
            return i + this.f13b.toLowerCase(Locale.ENGLISH).hashCode();
        }
        return i;
    }
}
