package a.a.a;

import a.a.a;
import a.a.af;
import a.a.k;
import a.a.m;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public final class n extends a implements f {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f22a;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f23b;
    private static boolean c;
    private static boolean d;
    private static boolean e;
    private a.b.a f;
    private byte[] g;
    private InputStream h;
    private s i;
    private Object j;

    static {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        f22a = true;
        f23b = true;
        c = false;
        d = false;
        e = true;
        try {
            String property = System.getProperty("mail.mime.setdefaulttextcharset");
            f22a = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.setcontenttypefilename");
            if (property2 == null || !property2.equalsIgnoreCase("false")) {
                z = true;
            } else {
                z = false;
            }
            f23b = z;
            String property3 = System.getProperty("mail.mime.encodefilename");
            if (property3 == null || property3.equalsIgnoreCase("false")) {
                z2 = false;
            } else {
                z2 = true;
            }
            c = z2;
            String property4 = System.getProperty("mail.mime.decodefilename");
            if (property4 == null || property4.equalsIgnoreCase("false")) {
                z3 = false;
            } else {
                z3 = true;
            }
            d = z3;
            String property5 = System.getProperty("mail.mime.cachemultipart");
            if (property5 == null || !property5.equalsIgnoreCase("false")) {
                z4 = true;
            } else {
                z4 = false;
            }
            e = z4;
        } catch (SecurityException e2) {
        }
    }

    public n() {
        this.i = new s();
    }

    public n(InputStream inputStream) {
        InputStream inputStream2;
        if ((inputStream instanceof ByteArrayInputStream) || (inputStream instanceof BufferedInputStream) || (inputStream instanceof r)) {
            inputStream2 = inputStream;
        } else {
            inputStream2 = new BufferedInputStream(inputStream);
        }
        this.i = new s(inputStream2);
        if (inputStream2 instanceof r) {
            r rVar = (r) inputStream2;
            this.h = rVar.a(rVar.a(), -1);
            return;
        }
        try {
            this.g = com.a.a.a.a.a(inputStream2);
        } catch (IOException e2) {
            throw new af("Error reading input stream", e2);
        }
    }

    public n(s sVar, byte[] bArr) {
        this.i = sVar;
        this.g = bArr;
    }

    public final String b() {
        String a2 = a("Content-Type", (String) null);
        if (a2 == null) {
            return "text/plain";
        }
        return a2;
    }

    public final String a() {
        return a(this);
    }

    public final String c() {
        return d(this);
    }

    /* access modifiers changed from: protected */
    public final InputStream d() {
        if (this.h != null) {
            return ((r) this.h).a(0, -1);
        }
        if (this.g != null) {
            return new ByteArrayInputStream(this.g);
        }
        throw new af("No content");
    }

    public final a.b.a e() {
        if (this.f == null) {
            this.f = new a.b.a(new o(this));
        }
        return this.f;
    }

    private void a(a.b.a aVar) {
        this.f = aVar;
        this.j = null;
        c(this);
    }

    public final void a(Object obj, String str) {
        if (obj instanceof m) {
            m mVar = (m) obj;
            a(new a.b.a(mVar, mVar.c()));
            mVar.a((k) this);
            return;
        }
        a(new a.b.a(obj, str));
    }

    public final void a(String str) {
        a(this, str, "plain");
    }

    public final String[] b(String str) {
        return this.i.a(str);
    }

    public final String a(String str, String str2) {
        return this.i.a(str, str2);
    }

    public final void b(String str, String str2) {
        this.i.b(str, str2);
    }

    public final void c(String str) {
        this.i.b(str);
    }

    /* access modifiers changed from: protected */
    public final void f() {
        b(this);
        if (this.j != null) {
            this.f = new a.b.a(this.j, b());
            this.j = null;
            this.g = null;
            if (this.h != null) {
                try {
                    this.h.close();
                } catch (IOException e2) {
                }
            }
            this.h = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.k.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.f.a(java.lang.String, java.lang.String):java.lang.String
      a.a.k.a(java.lang.Object, java.lang.String):void */
    static void a(f fVar, String str, String str2) {
        String str3;
        if (b.e(str) != 1) {
            str3 = b.a();
        } else {
            str3 = "us-ascii";
        }
        fVar.a((Object) str, "text/" + str2 + "; charset=" + b.a(str3, "()<>@,;:\\\"\t []/?="));
    }

    private static String d(f fVar) {
        String str;
        String a2;
        String a3 = fVar.a("Content-Disposition", (String) null);
        if (a3 != null) {
            str = new c(a3).a("filename");
        } else {
            str = null;
        }
        if (str == null && (a2 = fVar.a("Content-Type", (String) null)) != null) {
            try {
                str = new i(a2).a("name");
            } catch (aa e2) {
            }
        }
        if (!d || str == null) {
            return str;
        }
        try {
            return b.b(str);
        } catch (UnsupportedEncodingException e3) {
            throw new af("Can't decode filename", e3);
        }
    }

    static String a(f fVar) {
        k a2;
        int a3;
        String a4 = fVar.a("Content-Transfer-Encoding", (String) null);
        if (a4 == null) {
            return null;
        }
        String trim = a4.trim();
        if (trim.equalsIgnoreCase("7bit") || trim.equalsIgnoreCase("8bit") || trim.equalsIgnoreCase("quoted-printable") || trim.equalsIgnoreCase("binary") || trim.equalsIgnoreCase("base64")) {
            return trim;
        }
        q qVar = new q(trim, "()<>@,;:\\\"\t []/?=");
        do {
            a2 = qVar.a();
            a3 = a2.a();
            if (a3 == -4) {
                return trim;
            }
        } while (a3 != -1);
        return a2.b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x007f A[Catch:{ IOException -> 0x00a5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void b(a.a.a.f r8) {
        /*
            r6 = 0
            r7 = 1
            a.b.a r2 = r8.e()
            if (r2 != 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.lang.String r3 = r2.c()     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = "Content-Type"
            java.lang.String[] r1 = r8.b(r1)     // Catch:{ IOException -> 0x00a5 }
            if (r1 != 0) goto L_0x00ae
            r4 = r7
        L_0x0016:
            a.a.a.i r5 = new a.a.a.i     // Catch:{ IOException -> 0x00a5 }
            r5.<init>(r3)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = "multipart/*"
            boolean r1 = r5.b(r1)     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x0100
            boolean r1 = r8 instanceof a.a.a.n     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x00b7
            r0 = r8
            a.a.a.n r0 = (a.a.a.n) r0     // Catch:{ IOException -> 0x00a5 }
            r1 = r0
            java.lang.Object r6 = r1.j     // Catch:{ IOException -> 0x00a5 }
            if (r6 == 0) goto L_0x00b1
            java.lang.Object r1 = r1.j     // Catch:{ IOException -> 0x00a5 }
        L_0x0031:
            boolean r6 = r1 instanceof a.a.a.l     // Catch:{ IOException -> 0x00a5 }
            if (r6 == 0) goto L_0x00d3
            a.a.a.l r1 = (a.a.a.l) r1     // Catch:{ IOException -> 0x00a5 }
            r1.b()     // Catch:{ IOException -> 0x00a5 }
            r1 = r7
        L_0x003b:
            if (r1 != 0) goto L_0x0111
            java.lang.String r1 = "Content-Transfer-Encoding"
            java.lang.String[] r1 = r8.b(r1)     // Catch:{ IOException -> 0x00a5 }
            if (r1 != 0) goto L_0x004e
            java.lang.String r1 = a.a.a.b.a(r2)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r2 = "Content-Transfer-Encoding"
            r8.b(r2, r1)     // Catch:{ IOException -> 0x00a5 }
        L_0x004e:
            if (r4 == 0) goto L_0x0111
            boolean r1 = a.a.a.n.f22a     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x0111
            java.lang.String r1 = "text/*"
            boolean r1 = r5.b(r1)     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x0111
            java.lang.String r1 = "charset"
            java.lang.String r1 = r5.a(r1)     // Catch:{ IOException -> 0x00a5 }
            if (r1 != 0) goto L_0x0111
            java.lang.String r1 = r8.a()     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x010b
            java.lang.String r2 = "7bit"
            boolean r1 = r1.equalsIgnoreCase(r2)     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x010b
            java.lang.String r1 = "us-ascii"
        L_0x0074:
            java.lang.String r2 = "charset"
            r5.a(r2, r1)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = r5.toString()     // Catch:{ IOException -> 0x00a5 }
        L_0x007d:
            if (r4 == 0) goto L_0x0008
            java.lang.String r2 = "Content-Disposition"
            r3 = 0
            java.lang.String r2 = r8.a(r2, r3)     // Catch:{ IOException -> 0x00a5 }
            if (r2 == 0) goto L_0x009e
            a.a.a.c r3 = new a.a.a.c     // Catch:{ IOException -> 0x00a5 }
            r3.<init>(r2)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r2 = "filename"
            java.lang.String r2 = r3.a(r2)     // Catch:{ IOException -> 0x00a5 }
            if (r2 == 0) goto L_0x009e
            java.lang.String r1 = "name"
            r5.a(r1, r2)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = r5.toString()     // Catch:{ IOException -> 0x00a5 }
        L_0x009e:
            java.lang.String r2 = "Content-Type"
            r8.b(r2, r1)     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0008
        L_0x00a5:
            r1 = move-exception
            a.a.af r2 = new a.a.af
            java.lang.String r3 = "IOException updating headers"
            r2.<init>(r3, r1)
            throw r2
        L_0x00ae:
            r4 = r6
            goto L_0x0016
        L_0x00b1:
            java.lang.Object r1 = r2.e()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0031
        L_0x00b7:
            boolean r1 = r8 instanceof a.a.a.x     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x00cd
            r0 = r8
            a.a.a.x r0 = (a.a.a.x) r0     // Catch:{ IOException -> 0x00a5 }
            r1 = r0
            java.lang.Object r6 = r1.f36b     // Catch:{ IOException -> 0x00a5 }
            if (r6 == 0) goto L_0x00c7
            java.lang.Object r1 = r1.f36b     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0031
        L_0x00c7:
            java.lang.Object r1 = r2.e()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0031
        L_0x00cd:
            java.lang.Object r1 = r2.e()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0031
        L_0x00d3:
            a.a.af r2 = new a.a.af     // Catch:{ IOException -> 0x00a5 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r5 = "MIME part of type \""
            r4.<init>(r5)     // Catch:{ IOException -> 0x00a5 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r4 = "\" contains object of type "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x00a5 }
            java.lang.Class r1 = r1.getClass()     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = r1.getName()     // Catch:{ IOException -> 0x00a5 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r3 = " instead of MimeMultipart"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IOException -> 0x00a5 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x00a5 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00a5 }
            throw r2     // Catch:{ IOException -> 0x00a5 }
        L_0x0100:
            java.lang.String r1 = "message/rfc822"
            boolean r1 = r5.b(r1)     // Catch:{ IOException -> 0x00a5 }
            if (r1 == 0) goto L_0x0114
            r1 = r7
            goto L_0x003b
        L_0x010b:
            java.lang.String r1 = a.a.a.b.a()     // Catch:{ IOException -> 0x00a5 }
            goto L_0x0074
        L_0x0111:
            r1 = r3
            goto L_0x007d
        L_0x0114:
            r1 = r6
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.n.b(a.a.a.f):void");
    }

    static void c(f fVar) {
        fVar.c("Content-Type");
        fVar.c("Content-Transfer-Encoding");
    }
}
