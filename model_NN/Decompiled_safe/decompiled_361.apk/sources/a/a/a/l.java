package a.a.a;

import a.a.a;
import a.a.m;
import a.b.g;
import java.io.EOFException;
import java.io.InputStream;

public final class l extends m {
    private static boolean c;
    private static boolean d;
    private static boolean e;
    private g f;
    private boolean g;
    private boolean h;
    private String i;

    static {
        boolean z;
        boolean z2;
        c = true;
        d = true;
        e = true;
        try {
            String property = System.getProperty("mail.mime.multipart.ignoremissingendboundary");
            c = property == null || !property.equalsIgnoreCase("false");
            String property2 = System.getProperty("mail.mime.multipart.ignoremissingboundaryparameter");
            if (property2 == null || !property2.equalsIgnoreCase("false")) {
                z = true;
            } else {
                z = false;
            }
            d = z;
            String property3 = System.getProperty("mail.mime.multipart.bmparse");
            if (property3 == null || !property3.equalsIgnoreCase("false")) {
                z2 = true;
            } else {
                z2 = false;
            }
            e = z2;
        } catch (SecurityException e2) {
        }
    }

    public l() {
        this("mixed");
    }

    private l(String str) {
        this.f = null;
        this.g = true;
        this.h = true;
        this.i = null;
        String a2 = z.a();
        i iVar = new i("multipart", str);
        iVar.a("boundary", a2);
        this.f68b = iVar.toString();
    }

    public final synchronized int a() {
        d();
        return super.a();
    }

    public final void a(int i2) {
        d();
        super.a(i2);
    }

    public final synchronized void a(a aVar) {
        d();
        super.a(aVar);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f67a.size()) {
                ((n) this.f67a.elementAt(i3)).f();
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* JADX WARN: Type inference failed for: r20v7, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void d() {
        /*
            r25 = this;
            monitor-enter(r25)
            r0 = r25
            boolean r0 = r0.g     // Catch:{ all -> 0x0012 }
            r5 = r0
            if (r5 == 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r25)
            return
        L_0x000a:
            boolean r5 = a.a.a.l.e     // Catch:{ all -> 0x0012 }
            if (r5 == 0) goto L_0x0015
            r25.e()     // Catch:{ all -> 0x0012 }
            goto L_0x0008
        L_0x0012:
            r5 = move-exception
            monitor-exit(r25)
            throw r5
        L_0x0015:
            r5 = 0
            r7 = 0
            r9 = 0
            r0 = r25
            a.b.g r0 = r0.f     // Catch:{ Exception -> 0x008b }
            r6 = r0
            java.io.InputStream r6 = r6.a()     // Catch:{ Exception -> 0x008b }
            boolean r11 = r6 instanceof java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x008b }
            if (r11 != 0) goto L_0x0035
            boolean r11 = r6 instanceof java.io.BufferedInputStream     // Catch:{ Exception -> 0x008b }
            if (r11 != 0) goto L_0x0035
            boolean r11 = r6 instanceof a.a.a.r     // Catch:{ Exception -> 0x008b }
            if (r11 != 0) goto L_0x0035
            java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x008b }
            r11.<init>(r6)     // Catch:{ Exception -> 0x008b }
            r6 = r11
        L_0x0035:
            boolean r11 = r6 instanceof a.a.a.r     // Catch:{ all -> 0x0012 }
            if (r11 == 0) goto L_0x003d
            r0 = r6
            a.a.a.r r0 = (a.a.a.r) r0     // Catch:{ all -> 0x0012 }
            r5 = r0
        L_0x003d:
            a.a.a.i r11 = new a.a.a.i     // Catch:{ all -> 0x0012 }
            r0 = r25
            java.lang.String r0 = r0.f68b     // Catch:{ all -> 0x0012 }
            r12 = r0
            r11.<init>(r12)     // Catch:{ all -> 0x0012 }
            r12 = 0
            java.lang.String r13 = "boundary"
            java.lang.String r11 = r11.a(r13)     // Catch:{ all -> 0x0012 }
            if (r11 == 0) goto L_0x0094
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0012 }
            java.lang.String r13 = "--"
            r12.<init>(r13)     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r11 = r12.append(r11)     // Catch:{ all -> 0x0012 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0012 }
        L_0x005f:
            com.a.a.a.g r12 = new com.a.a.a.g     // Catch:{ IOException -> 0x007d }
            r12.<init>(r6)     // Catch:{ IOException -> 0x007d }
            r13 = 0
            r14 = 0
            r22 = r14
            r14 = r13
            r13 = r22
        L_0x006b:
            java.lang.String r15 = r12.a()     // Catch:{ IOException -> 0x007d }
            if (r15 != 0) goto L_0x00a0
            r13 = r11
            r11 = r15
        L_0x0073:
            if (r11 != 0) goto L_0x0114
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x007d }
            java.lang.String r7 = "Missing start boundary"
            r5.<init>(r7)     // Catch:{ IOException -> 0x007d }
            throw r5     // Catch:{ IOException -> 0x007d }
        L_0x007d:
            r5 = move-exception
            a.a.af r7 = new a.a.af     // Catch:{ all -> 0x0086 }
            java.lang.String r8 = "IO Error"
            r7.<init>(r8, r5)     // Catch:{ all -> 0x0086 }
            throw r7     // Catch:{ all -> 0x0086 }
        L_0x0086:
            r5 = move-exception
            r6.close()     // Catch:{ IOException -> 0x02df }
        L_0x008a:
            throw r5     // Catch:{ all -> 0x0012 }
        L_0x008b:
            r5 = move-exception
            a.a.af r6 = new a.a.af     // Catch:{ all -> 0x0012 }
            java.lang.String r7 = "No inputstream from datasource"
            r6.<init>(r7, r5)     // Catch:{ all -> 0x0012 }
            throw r6     // Catch:{ all -> 0x0012 }
        L_0x0094:
            boolean r11 = a.a.a.l.d     // Catch:{ all -> 0x0012 }
            if (r11 != 0) goto L_0x02f1
            a.a.af r5 = new a.a.af     // Catch:{ all -> 0x0012 }
            java.lang.String r6 = "Missing boundary parameter"
            r5.<init>(r6)     // Catch:{ all -> 0x0012 }
            throw r5     // Catch:{ all -> 0x0012 }
        L_0x00a0:
            int r16 = r15.length()     // Catch:{ IOException -> 0x007d }
            r17 = 1
            int r16 = r16 - r17
        L_0x00a8:
            if (r16 >= 0) goto L_0x00c2
        L_0x00aa:
            r17 = 0
            int r16 = r16 + 1
            r0 = r15
            r1 = r17
            r2 = r16
            java.lang.String r15 = r0.substring(r1, r2)     // Catch:{ IOException -> 0x007d }
            if (r11 == 0) goto L_0x00d9
            boolean r16 = r15.equals(r11)     // Catch:{ IOException -> 0x007d }
            if (r16 == 0) goto L_0x00e4
            r13 = r11
            r11 = r15
            goto L_0x0073
        L_0x00c2:
            char r17 = r15.charAt(r16)     // Catch:{ IOException -> 0x007d }
            r18 = 32
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x00d6
            r18 = 9
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00aa
        L_0x00d6:
            int r16 = r16 + -1
            goto L_0x00a8
        L_0x00d9:
            java.lang.String r16 = "--"
            boolean r16 = r15.startsWith(r16)     // Catch:{ IOException -> 0x007d }
            if (r16 == 0) goto L_0x00e4
            r11 = r15
            r13 = r15
            goto L_0x0073
        L_0x00e4:
            int r16 = r15.length()     // Catch:{ IOException -> 0x007d }
            if (r16 <= 0) goto L_0x006b
            if (r13 != 0) goto L_0x00f7
            java.lang.String r13 = "line.separator"
            java.lang.String r16 = "\n"
            r0 = r13
            r1 = r16
            java.lang.String r13 = java.lang.System.getProperty(r0, r1)     // Catch:{ SecurityException -> 0x0110 }
        L_0x00f7:
            if (r14 != 0) goto L_0x0107
            java.lang.StringBuffer r14 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x007d }
            int r16 = r15.length()     // Catch:{ IOException -> 0x007d }
            int r16 = r16 + 2
            r0 = r14
            r1 = r16
            r0.<init>(r1)     // Catch:{ IOException -> 0x007d }
        L_0x0107:
            java.lang.StringBuffer r15 = r14.append(r15)     // Catch:{ IOException -> 0x007d }
            r15.append(r13)     // Catch:{ IOException -> 0x007d }
            goto L_0x006b
        L_0x0110:
            r13 = move-exception
            java.lang.String r13 = "\n"
            goto L_0x00f7
        L_0x0114:
            if (r14 == 0) goto L_0x011f
            java.lang.String r11 = r14.toString()     // Catch:{ IOException -> 0x007d }
            r0 = r11
            r1 = r25
            r1.i = r0     // Catch:{ IOException -> 0x007d }
        L_0x011f:
            byte[] r11 = com.a.a.a.a.a(r13)     // Catch:{ IOException -> 0x007d }
            int r13 = r11.length     // Catch:{ IOException -> 0x007d }
            r14 = 0
            r22 = r14
            r14 = r7
            r7 = r22
            r23 = r9
            r8 = r23
        L_0x012e:
            if (r7 == 0) goto L_0x013b
        L_0x0130:
            r6.close()     // Catch:{ IOException -> 0x02e2 }
        L_0x0133:
            r5 = 1
            r0 = r5
            r1 = r25
            r1.g = r0     // Catch:{ all -> 0x0012 }
            goto L_0x0008
        L_0x013b:
            r10 = 0
            if (r5 == 0) goto L_0x0163
            long r14 = r5.a()     // Catch:{ IOException -> 0x007d }
        L_0x0142:
            java.lang.String r16 = r12.a()     // Catch:{ IOException -> 0x007d }
            if (r16 == 0) goto L_0x014e
            int r17 = r16.length()     // Catch:{ IOException -> 0x007d }
            if (r17 > 0) goto L_0x0142
        L_0x014e:
            if (r16 != 0) goto L_0x0167
            boolean r5 = a.a.a.l.c     // Catch:{ IOException -> 0x007d }
            if (r5 != 0) goto L_0x015c
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x007d }
            java.lang.String r7 = "missing multipart end boundary"
            r5.<init>(r7)     // Catch:{ IOException -> 0x007d }
            throw r5     // Catch:{ IOException -> 0x007d }
        L_0x015c:
            r5 = 0
            r0 = r5
            r1 = r25
            r1.h = r0     // Catch:{ IOException -> 0x007d }
            goto L_0x0130
        L_0x0163:
            a.a.a.s r10 = a(r6)     // Catch:{ IOException -> 0x007d }
        L_0x0167:
            boolean r16 = r6.markSupported()     // Catch:{ IOException -> 0x007d }
            if (r16 != 0) goto L_0x0175
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x007d }
            java.lang.String r7 = "Stream doesn't support mark"
            r5.<init>(r7)     // Catch:{ IOException -> 0x007d }
            throw r5     // Catch:{ IOException -> 0x007d }
        L_0x0175:
            r16 = 0
            if (r5 != 0) goto L_0x01e8
            java.io.ByteArrayOutputStream r16 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x007d }
            r16.<init>()     // Catch:{ IOException -> 0x007d }
            r22 = r16
            r16 = r8
            r8 = r22
        L_0x0184:
            r9 = 1
            r18 = -1
            r19 = -1
            r22 = r19
            r23 = r18
            r18 = r16
            r17 = r9
            r16 = r23
            r9 = r22
        L_0x0195:
            if (r17 == 0) goto L_0x025f
            int r17 = r13 + 4
            r0 = r17
            int r0 = r0 + 1000
            r17 = r0
            r0 = r6
            r1 = r17
            r0.mark(r1)     // Catch:{ IOException -> 0x007d }
            r17 = 0
        L_0x01a7:
            r0 = r17
            r1 = r13
            if (r0 < r1) goto L_0x01f3
        L_0x01ac:
            r0 = r17
            r1 = r13
            if (r0 != r1) goto L_0x023f
            int r17 = r6.read()     // Catch:{ IOException -> 0x007d }
            r20 = 45
            r0 = r17
            r1 = r20
            if (r0 != r1) goto L_0x020c
            int r20 = r6.read()     // Catch:{ IOException -> 0x007d }
            r21 = 45
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x020c
            r7 = 1
            r0 = r7
            r1 = r25
            r1.h = r0     // Catch:{ IOException -> 0x007d }
            r7 = 1
        L_0x01d0:
            if (r5 == 0) goto L_0x02d5
            r0 = r5
            r1 = r14
            r3 = r18
            java.io.InputStream r8 = r0.a(r1, r3)     // Catch:{ IOException -> 0x007d }
            a.a.a.n r8 = b(r8)     // Catch:{ IOException -> 0x007d }
        L_0x01de:
            r0 = r25
            r1 = r8
            super.a(r1)     // Catch:{ IOException -> 0x007d }
            r8 = r18
            goto L_0x012e
        L_0x01e8:
            long r8 = r5.a()     // Catch:{ IOException -> 0x007d }
            r22 = r16
            r16 = r8
            r8 = r22
            goto L_0x0184
        L_0x01f3:
            int r20 = r6.read()     // Catch:{ IOException -> 0x007d }
            byte r21 = r11[r17]     // Catch:{ IOException -> 0x007d }
            r0 = r21
            r0 = r0 & 255(0xff, float:3.57E-43)
            r21 = r0
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x01ac
            int r17 = r17 + 1
            goto L_0x01a7
        L_0x0208:
            int r17 = r6.read()     // Catch:{ IOException -> 0x007d }
        L_0x020c:
            r20 = 32
            r0 = r17
            r1 = r20
            if (r0 == r1) goto L_0x0208
            r20 = 9
            r0 = r17
            r1 = r20
            if (r0 == r1) goto L_0x0208
            r20 = 10
            r0 = r17
            r1 = r20
            if (r0 == r1) goto L_0x01d0
            r20 = 13
            r0 = r17
            r1 = r20
            if (r0 != r1) goto L_0x023f
            r9 = 1
            r6.mark(r9)     // Catch:{ IOException -> 0x007d }
            int r9 = r6.read()     // Catch:{ IOException -> 0x007d }
            r16 = 10
            r0 = r9
            r1 = r16
            if (r0 == r1) goto L_0x01d0
            r6.reset()     // Catch:{ IOException -> 0x007d }
            goto L_0x01d0
        L_0x023f:
            r6.reset()     // Catch:{ IOException -> 0x007d }
            if (r8 == 0) goto L_0x025f
            r17 = -1
            r0 = r16
            r1 = r17
            if (r0 == r1) goto L_0x025f
            r0 = r8
            r1 = r16
            r0.write(r1)     // Catch:{ IOException -> 0x007d }
            r16 = -1
            r0 = r9
            r1 = r16
            if (r0 == r1) goto L_0x025c
            r8.write(r9)     // Catch:{ IOException -> 0x007d }
        L_0x025c:
            r9 = -1
            r16 = r9
        L_0x025f:
            int r17 = r6.read()     // Catch:{ IOException -> 0x007d }
            if (r17 >= 0) goto L_0x027a
            boolean r7 = a.a.a.l.c     // Catch:{ IOException -> 0x007d }
            if (r7 != 0) goto L_0x0271
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x007d }
            java.lang.String r7 = "missing multipart end boundary"
            r5.<init>(r7)     // Catch:{ IOException -> 0x007d }
            throw r5     // Catch:{ IOException -> 0x007d }
        L_0x0271:
            r7 = 0
            r0 = r7
            r1 = r25
            r1.h = r0     // Catch:{ IOException -> 0x007d }
            r7 = 1
            goto L_0x01d0
        L_0x027a:
            r20 = 13
            r0 = r17
            r1 = r20
            if (r0 == r1) goto L_0x028a
            r20 = 10
            r0 = r17
            r1 = r20
            if (r0 != r1) goto L_0x02c7
        L_0x028a:
            r16 = 1
            if (r5 == 0) goto L_0x0296
            long r18 = r5.a()     // Catch:{ IOException -> 0x007d }
            r20 = 1
            long r18 = r18 - r20
        L_0x0296:
            r20 = 13
            r0 = r17
            r1 = r20
            if (r0 != r1) goto L_0x02e9
            r20 = 1
            r0 = r6
            r1 = r20
            r0.mark(r1)     // Catch:{ IOException -> 0x007d }
            int r20 = r6.read()     // Catch:{ IOException -> 0x007d }
            r21 = 10
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x02bc
            r9 = r20
            r22 = r17
            r17 = r16
            r16 = r22
            goto L_0x0195
        L_0x02bc:
            r6.reset()     // Catch:{ IOException -> 0x007d }
            r22 = r17
            r17 = r16
            r16 = r22
            goto L_0x0195
        L_0x02c7:
            r20 = 0
            if (r8 == 0) goto L_0x02e5
            r0 = r8
            r1 = r17
            r0.write(r1)     // Catch:{ IOException -> 0x007d }
            r17 = r20
            goto L_0x0195
        L_0x02d5:
            byte[] r8 = r8.toByteArray()     // Catch:{ IOException -> 0x007d }
            a.a.a.n r8 = a(r10, r8)     // Catch:{ IOException -> 0x007d }
            goto L_0x01de
        L_0x02df:
            r6 = move-exception
            goto L_0x008a
        L_0x02e2:
            r5 = move-exception
            goto L_0x0133
        L_0x02e5:
            r17 = r20
            goto L_0x0195
        L_0x02e9:
            r22 = r17
            r17 = r16
            r16 = r22
            goto L_0x0195
        L_0x02f1:
            r11 = r12
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.l.d():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:206:0x03c5, code lost:
        r9 = r7;
        r7 = r23;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void e() {
        /*
            r30 = this;
            monitor-enter(r30)
            r0 = r30
            boolean r0 = r0.g     // Catch:{ all -> 0x0080 }
            r5 = r0
            if (r5 == 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r30)
            return
        L_0x000a:
            r5 = 0
            r7 = 0
            r9 = 0
            r0 = r30
            a.b.g r0 = r0.f     // Catch:{ Exception -> 0x0083 }
            r6 = r0
            java.io.InputStream r6 = r6.a()     // Catch:{ Exception -> 0x0083 }
            boolean r11 = r6 instanceof java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0083 }
            if (r11 != 0) goto L_0x002a
            boolean r11 = r6 instanceof java.io.BufferedInputStream     // Catch:{ Exception -> 0x0083 }
            if (r11 != 0) goto L_0x002a
            boolean r11 = r6 instanceof a.a.a.r     // Catch:{ Exception -> 0x0083 }
            if (r11 != 0) goto L_0x002a
            java.io.BufferedInputStream r11 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0083 }
            r11.<init>(r6)     // Catch:{ Exception -> 0x0083 }
            r6 = r11
        L_0x002a:
            boolean r11 = r6 instanceof a.a.a.r     // Catch:{ all -> 0x0080 }
            if (r11 == 0) goto L_0x0032
            r0 = r6
            a.a.a.r r0 = (a.a.a.r) r0     // Catch:{ all -> 0x0080 }
            r5 = r0
        L_0x0032:
            a.a.a.i r11 = new a.a.a.i     // Catch:{ all -> 0x0080 }
            r0 = r30
            java.lang.String r0 = r0.f68b     // Catch:{ all -> 0x0080 }
            r12 = r0
            r11.<init>(r12)     // Catch:{ all -> 0x0080 }
            r12 = 0
            java.lang.String r13 = "boundary"
            java.lang.String r11 = r11.a(r13)     // Catch:{ all -> 0x0080 }
            if (r11 == 0) goto L_0x008c
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0080 }
            java.lang.String r13 = "--"
            r12.<init>(r13)     // Catch:{ all -> 0x0080 }
            java.lang.StringBuilder r11 = r12.append(r11)     // Catch:{ all -> 0x0080 }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x0080 }
        L_0x0054:
            com.a.a.a.g r12 = new com.a.a.a.g     // Catch:{ IOException -> 0x0072 }
            r12.<init>(r6)     // Catch:{ IOException -> 0x0072 }
            r13 = 0
            r14 = 0
            r27 = r14
            r14 = r13
            r13 = r27
        L_0x0060:
            java.lang.String r15 = r12.a()     // Catch:{ IOException -> 0x0072 }
            if (r15 != 0) goto L_0x0098
            r13 = r11
            r11 = r15
        L_0x0068:
            if (r11 != 0) goto L_0x010c
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x0072 }
            java.lang.String r7 = "Missing start boundary"
            r5.<init>(r7)     // Catch:{ IOException -> 0x0072 }
            throw r5     // Catch:{ IOException -> 0x0072 }
        L_0x0072:
            r5 = move-exception
            a.a.af r7 = new a.a.af     // Catch:{ all -> 0x007b }
            java.lang.String r8 = "IO Error"
            r7.<init>(r8, r5)     // Catch:{ all -> 0x007b }
            throw r7     // Catch:{ all -> 0x007b }
        L_0x007b:
            r5 = move-exception
            r6.close()     // Catch:{ IOException -> 0x03bf }
        L_0x007f:
            throw r5     // Catch:{ all -> 0x0080 }
        L_0x0080:
            r5 = move-exception
            monitor-exit(r30)
            throw r5
        L_0x0083:
            r5 = move-exception
            a.a.af r6 = new a.a.af     // Catch:{ all -> 0x0080 }
            java.lang.String r7 = "No inputstream from datasource"
            r6.<init>(r7, r5)     // Catch:{ all -> 0x0080 }
            throw r6     // Catch:{ all -> 0x0080 }
        L_0x008c:
            boolean r11 = a.a.a.l.d     // Catch:{ all -> 0x0080 }
            if (r11 != 0) goto L_0x03ce
            a.a.af r5 = new a.a.af     // Catch:{ all -> 0x0080 }
            java.lang.String r6 = "Missing boundary parameter"
            r5.<init>(r6)     // Catch:{ all -> 0x0080 }
            throw r5     // Catch:{ all -> 0x0080 }
        L_0x0098:
            int r16 = r15.length()     // Catch:{ IOException -> 0x0072 }
            r17 = 1
            int r16 = r16 - r17
        L_0x00a0:
            if (r16 >= 0) goto L_0x00ba
        L_0x00a2:
            r17 = 0
            int r16 = r16 + 1
            r0 = r15
            r1 = r17
            r2 = r16
            java.lang.String r15 = r0.substring(r1, r2)     // Catch:{ IOException -> 0x0072 }
            if (r11 == 0) goto L_0x00d1
            boolean r16 = r15.equals(r11)     // Catch:{ IOException -> 0x0072 }
            if (r16 == 0) goto L_0x00dc
            r13 = r11
            r11 = r15
            goto L_0x0068
        L_0x00ba:
            char r17 = r15.charAt(r16)     // Catch:{ IOException -> 0x0072 }
            r18 = 32
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x00ce
            r18 = 9
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x00a2
        L_0x00ce:
            int r16 = r16 + -1
            goto L_0x00a0
        L_0x00d1:
            java.lang.String r16 = "--"
            boolean r16 = r15.startsWith(r16)     // Catch:{ IOException -> 0x0072 }
            if (r16 == 0) goto L_0x00dc
            r11 = r15
            r13 = r15
            goto L_0x0068
        L_0x00dc:
            int r16 = r15.length()     // Catch:{ IOException -> 0x0072 }
            if (r16 <= 0) goto L_0x0060
            if (r13 != 0) goto L_0x00ef
            java.lang.String r13 = "line.separator"
            java.lang.String r16 = "\n"
            r0 = r13
            r1 = r16
            java.lang.String r13 = java.lang.System.getProperty(r0, r1)     // Catch:{ SecurityException -> 0x0108 }
        L_0x00ef:
            if (r14 != 0) goto L_0x00ff
            java.lang.StringBuffer r14 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0072 }
            int r16 = r15.length()     // Catch:{ IOException -> 0x0072 }
            int r16 = r16 + 2
            r0 = r14
            r1 = r16
            r0.<init>(r1)     // Catch:{ IOException -> 0x0072 }
        L_0x00ff:
            java.lang.StringBuffer r15 = r14.append(r15)     // Catch:{ IOException -> 0x0072 }
            r15.append(r13)     // Catch:{ IOException -> 0x0072 }
            goto L_0x0060
        L_0x0108:
            r13 = move-exception
            java.lang.String r13 = "\n"
            goto L_0x00ef
        L_0x010c:
            if (r14 == 0) goto L_0x0117
            java.lang.String r11 = r14.toString()     // Catch:{ IOException -> 0x0072 }
            r0 = r11
            r1 = r30
            r1.i = r0     // Catch:{ IOException -> 0x0072 }
        L_0x0117:
            byte[] r11 = com.a.a.a.a.a(r13)     // Catch:{ IOException -> 0x0072 }
            int r13 = r11.length     // Catch:{ IOException -> 0x0072 }
            r14 = 256(0x100, float:3.59E-43)
            int[] r14 = new int[r14]     // Catch:{ IOException -> 0x0072 }
            r15 = 0
        L_0x0121:
            if (r15 < r13) goto L_0x014a
            int[] r15 = new int[r13]     // Catch:{ IOException -> 0x0072 }
            r16 = r13
        L_0x0127:
            if (r16 > 0) goto L_0x0153
            r16 = 1
            int r16 = r13 - r16
            r17 = 1
            r15[r16] = r17     // Catch:{ IOException -> 0x0072 }
            r16 = 0
            r27 = r16
            r16 = r7
            r7 = r27
            r28 = r9
            r8 = r28
        L_0x013d:
            if (r7 == 0) goto L_0x017c
        L_0x013f:
            r6.close()     // Catch:{ IOException -> 0x03c2 }
        L_0x0142:
            r5 = 1
            r0 = r5
            r1 = r30
            r1.g = r0     // Catch:{ all -> 0x0080 }
            goto L_0x0008
        L_0x014a:
            byte r16 = r11[r15]     // Catch:{ IOException -> 0x0072 }
            int r17 = r15 + 1
            r14[r16] = r17     // Catch:{ IOException -> 0x0072 }
            int r15 = r15 + 1
            goto L_0x0121
        L_0x0153:
            r17 = 1
            int r17 = r13 - r17
        L_0x0157:
            r0 = r17
            r1 = r16
            if (r0 >= r1) goto L_0x0162
        L_0x015d:
            if (r17 > 0) goto L_0x0177
        L_0x015f:
            int r16 = r16 + -1
            goto L_0x0127
        L_0x0162:
            byte r18 = r11[r17]     // Catch:{ IOException -> 0x0072 }
            int r19 = r17 - r16
            byte r19 = r11[r19]     // Catch:{ IOException -> 0x0072 }
            r0 = r18
            r1 = r19
            if (r0 != r1) goto L_0x015f
            r18 = 1
            int r18 = r17 - r18
            r15[r18] = r16     // Catch:{ IOException -> 0x0072 }
            int r17 = r17 + -1
            goto L_0x0157
        L_0x0177:
            int r17 = r17 + -1
            r15[r17] = r16     // Catch:{ IOException -> 0x0072 }
            goto L_0x015d
        L_0x017c:
            r10 = 0
            if (r5 == 0) goto L_0x01a4
            long r16 = r5.a()     // Catch:{ IOException -> 0x0072 }
        L_0x0183:
            java.lang.String r18 = r12.a()     // Catch:{ IOException -> 0x0072 }
            if (r18 == 0) goto L_0x018f
            int r19 = r18.length()     // Catch:{ IOException -> 0x0072 }
            if (r19 > 0) goto L_0x0183
        L_0x018f:
            if (r18 != 0) goto L_0x01a8
            boolean r5 = a.a.a.l.c     // Catch:{ IOException -> 0x0072 }
            if (r5 != 0) goto L_0x019d
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x0072 }
            java.lang.String r7 = "missing multipart end boundary"
            r5.<init>(r7)     // Catch:{ IOException -> 0x0072 }
            throw r5     // Catch:{ IOException -> 0x0072 }
        L_0x019d:
            r5 = 0
            r0 = r5
            r1 = r30
            r1.h = r0     // Catch:{ IOException -> 0x0072 }
            goto L_0x013f
        L_0x01a4:
            a.a.a.s r10 = a(r6)     // Catch:{ IOException -> 0x0072 }
        L_0x01a8:
            boolean r18 = r6.markSupported()     // Catch:{ IOException -> 0x0072 }
            if (r18 != 0) goto L_0x01b6
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x0072 }
            java.lang.String r7 = "Stream doesn't support mark"
            r5.<init>(r7)     // Catch:{ IOException -> 0x0072 }
            throw r5     // Catch:{ IOException -> 0x0072 }
        L_0x01b6:
            r18 = 0
            if (r5 != 0) goto L_0x020b
            java.io.ByteArrayOutputStream r18 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0072 }
            r18.<init>()     // Catch:{ IOException -> 0x0072 }
            r27 = r18
            r18 = r8
            r8 = r27
        L_0x01c5:
            byte[] r9 = new byte[r13]     // Catch:{ IOException -> 0x0072 }
            r0 = r13
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0072 }
            r20 = r0
            r21 = 0
            r22 = 1
            r27 = r22
            r28 = r21
            r21 = r18
            r19 = r20
            r18 = r28
            r20 = r9
            r9 = r27
        L_0x01de:
            int r23 = r13 + 4
            r0 = r23
            int r0 = r0 + 1000
            r23 = r0
            r0 = r6
            r1 = r23
            r0.mark(r1)     // Catch:{ IOException -> 0x0072 }
            r23 = 0
            r24 = 0
            r0 = r6
            r1 = r20
            r2 = r24
            r3 = r13
            int r24 = a(r0, r1, r2, r3)     // Catch:{ IOException -> 0x0072 }
            r0 = r24
            r1 = r13
            if (r0 >= r1) goto L_0x0240
            boolean r7 = a.a.a.l.c     // Catch:{ IOException -> 0x0072 }
            if (r7 != 0) goto L_0x0216
            a.a.af r5 = new a.a.af     // Catch:{ IOException -> 0x0072 }
            java.lang.String r7 = "missing multipart end boundary"
            r5.<init>(r7)     // Catch:{ IOException -> 0x0072 }
            throw r5     // Catch:{ IOException -> 0x0072 }
        L_0x020b:
            long r8 = r5.a()     // Catch:{ IOException -> 0x0072 }
            r27 = r18
            r18 = r8
            r8 = r27
            goto L_0x01c5
        L_0x0216:
            if (r5 == 0) goto L_0x021c
            long r21 = r5.a()     // Catch:{ IOException -> 0x0072 }
        L_0x021c:
            r7 = 0
            r0 = r7
            r1 = r30
            r1.h = r0     // Catch:{ IOException -> 0x0072 }
            r7 = 1
            r9 = r7
            r7 = r23
        L_0x0226:
            if (r5 == 0) goto L_0x0391
            r0 = r5
            r1 = r16
            r3 = r21
            java.io.InputStream r7 = r0.a(r1, r3)     // Catch:{ IOException -> 0x0072 }
            a.a.a.n r7 = b(r7)     // Catch:{ IOException -> 0x0072 }
        L_0x0235:
            r0 = r30
            r1 = r7
            super.a(r1)     // Catch:{ IOException -> 0x0072 }
            r7 = r9
            r8 = r21
            goto L_0x013d
        L_0x0240:
            r23 = 1
            int r23 = r13 - r23
        L_0x0244:
            if (r23 >= 0) goto L_0x02be
        L_0x0246:
            if (r23 >= 0) goto L_0x03ca
            r23 = 0
            if (r9 != 0) goto L_0x0284
            r25 = 1
            int r25 = r18 - r25
            byte r25 = r19[r25]     // Catch:{ IOException -> 0x0072 }
            r26 = 13
            r0 = r25
            r1 = r26
            if (r0 == r1) goto L_0x0262
            r26 = 10
            r0 = r25
            r1 = r26
            if (r0 != r1) goto L_0x0284
        L_0x0262:
            r23 = 1
            r26 = 10
            r0 = r25
            r1 = r26
            if (r0 != r1) goto L_0x0284
            r25 = 2
            r0 = r18
            r1 = r25
            if (r0 < r1) goto L_0x0284
            r25 = 2
            int r25 = r18 - r25
            byte r25 = r19[r25]     // Catch:{ IOException -> 0x0072 }
            r26 = 13
            r0 = r25
            r1 = r26
            if (r0 != r1) goto L_0x0284
            r23 = 2
        L_0x0284:
            if (r9 != 0) goto L_0x0288
            if (r23 <= 0) goto L_0x0303
        L_0x0288:
            if (r5 == 0) goto L_0x029b
            long r21 = r5.a()     // Catch:{ IOException -> 0x0072 }
            r0 = r13
            long r0 = (long) r0     // Catch:{ IOException -> 0x0072 }
            r25 = r0
            long r21 = r21 - r25
            r0 = r23
            long r0 = (long) r0     // Catch:{ IOException -> 0x0072 }
            r25 = r0
            long r21 = r21 - r25
        L_0x029b:
            int r9 = r6.read()     // Catch:{ IOException -> 0x0072 }
            r25 = 45
            r0 = r9
            r1 = r25
            if (r0 != r1) goto L_0x02d0
            int r25 = r6.read()     // Catch:{ IOException -> 0x0072 }
            r26 = 45
            r0 = r25
            r1 = r26
            if (r0 != r1) goto L_0x02d0
            r7 = 1
            r0 = r7
            r1 = r30
            r1.h = r0     // Catch:{ IOException -> 0x0072 }
            r7 = 1
            r9 = r7
            r7 = r23
            goto L_0x0226
        L_0x02be:
            byte r25 = r20[r23]     // Catch:{ IOException -> 0x0072 }
            byte r26 = r11[r23]     // Catch:{ IOException -> 0x0072 }
            r0 = r25
            r1 = r26
            if (r0 != r1) goto L_0x0246
            int r23 = r23 + -1
            goto L_0x0244
        L_0x02cc:
            int r9 = r6.read()     // Catch:{ IOException -> 0x0072 }
        L_0x02d0:
            r25 = 32
            r0 = r9
            r1 = r25
            if (r0 == r1) goto L_0x02cc
            r25 = 9
            r0 = r9
            r1 = r25
            if (r0 == r1) goto L_0x02cc
            r25 = 10
            r0 = r9
            r1 = r25
            if (r0 == r1) goto L_0x03c5
            r25 = 13
            r0 = r9
            r1 = r25
            if (r0 != r1) goto L_0x0303
            r9 = 1
            r6.mark(r9)     // Catch:{ IOException -> 0x0072 }
            int r9 = r6.read()     // Catch:{ IOException -> 0x0072 }
            r25 = 10
            r0 = r9
            r1 = r25
            if (r0 == r1) goto L_0x03c5
            r6.reset()     // Catch:{ IOException -> 0x0072 }
            r9 = r7
            r7 = r23
            goto L_0x0226
        L_0x0303:
            r9 = 0
        L_0x0304:
            int r23 = r9 + 1
            byte r24 = r20[r9]     // Catch:{ IOException -> 0x0072 }
            r24 = r24 & 127(0x7f, float:1.78E-43)
            r24 = r14[r24]     // Catch:{ IOException -> 0x0072 }
            int r23 = r23 - r24
            r9 = r15[r9]     // Catch:{ IOException -> 0x0072 }
            r0 = r23
            r1 = r9
            int r9 = java.lang.Math.max(r0, r1)     // Catch:{ IOException -> 0x0072 }
            r23 = 2
            r0 = r9
            r1 = r23
            if (r0 >= r1) goto L_0x0371
            if (r5 != 0) goto L_0x0334
            r9 = 1
            r0 = r18
            r1 = r9
            if (r0 <= r1) goto L_0x0334
            r9 = 0
            r23 = 1
            int r23 = r18 - r23
            r0 = r8
            r1 = r19
            r2 = r9
            r3 = r23
            r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0072 }
        L_0x0334:
            r6.reset()     // Catch:{ IOException -> 0x0072 }
            r23 = 1
            r0 = r6
            r1 = r23
            a(r0, r1)     // Catch:{ IOException -> 0x0072 }
            if (r18 <= 0) goto L_0x0364
            r9 = 0
            r23 = 1
            int r18 = r18 - r23
            byte r18 = r19[r18]     // Catch:{ IOException -> 0x0072 }
            r19[r9] = r18     // Catch:{ IOException -> 0x0072 }
            r9 = 1
            r18 = 0
            byte r18 = r20[r18]     // Catch:{ IOException -> 0x0072 }
            r19[r9] = r18     // Catch:{ IOException -> 0x0072 }
            r9 = 2
            r18 = r19
            r19 = r20
        L_0x0356:
            r20 = 0
            r27 = r20
            r20 = r19
            r19 = r18
            r18 = r9
            r9 = r27
            goto L_0x01de
        L_0x0364:
            r9 = 0
            r18 = 0
            byte r18 = r20[r18]     // Catch:{ IOException -> 0x0072 }
            r19[r9] = r18     // Catch:{ IOException -> 0x0072 }
            r9 = 1
            r18 = r19
            r19 = r20
            goto L_0x0356
        L_0x0371:
            if (r18 <= 0) goto L_0x0381
            if (r5 != 0) goto L_0x0381
            r23 = 0
            r0 = r8
            r1 = r19
            r2 = r23
            r3 = r18
            r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0072 }
        L_0x0381:
            r6.reset()     // Catch:{ IOException -> 0x0072 }
            r0 = r9
            long r0 = (long) r0     // Catch:{ IOException -> 0x0072 }
            r23 = r0
            r0 = r6
            r1 = r23
            a(r0, r1)     // Catch:{ IOException -> 0x0072 }
            r18 = r20
            goto L_0x0356
        L_0x0391:
            int r23 = r18 - r7
            if (r23 <= 0) goto L_0x03a2
            r23 = 0
            int r7 = r18 - r7
            r0 = r8
            r1 = r19
            r2 = r23
            r3 = r7
            r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0072 }
        L_0x03a2:
            r0 = r30
            boolean r0 = r0.h     // Catch:{ IOException -> 0x0072 }
            r7 = r0
            if (r7 != 0) goto L_0x03b5
            if (r24 <= 0) goto L_0x03b5
            r7 = 0
            r0 = r8
            r1 = r20
            r2 = r7
            r3 = r24
            r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0072 }
        L_0x03b5:
            byte[] r7 = r8.toByteArray()     // Catch:{ IOException -> 0x0072 }
            a.a.a.n r7 = a(r10, r7)     // Catch:{ IOException -> 0x0072 }
            goto L_0x0235
        L_0x03bf:
            r6 = move-exception
            goto L_0x007f
        L_0x03c2:
            r5 = move-exception
            goto L_0x0142
        L_0x03c5:
            r9 = r7
            r7 = r23
            goto L_0x0226
        L_0x03ca:
            r9 = r23
            goto L_0x0304
        L_0x03ce:
            r11 = r12
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.l.e():void");
    }

    private static int a(InputStream inputStream, byte[] bArr, int i2, int i3) {
        int i4 = 0;
        if (i3 == 0) {
            return 0;
        }
        int i5 = i3;
        int i6 = i2;
        while (i5 > 0) {
            int read = inputStream.read(bArr, i6, i5);
            if (read <= 0) {
                break;
            }
            i6 += read;
            i4 += read;
            i5 -= read;
        }
        if (i4 <= 0) {
            return -1;
        }
        return i4;
    }

    private static void a(InputStream inputStream, long j) {
        long j2 = j;
        while (j2 > 0) {
            long skip = inputStream.skip(j2);
            if (skip <= 0) {
                throw new EOFException("can't skip");
            }
            j2 -= skip;
        }
    }

    private static s a(InputStream inputStream) {
        return new s(inputStream);
    }

    private static n a(s sVar, byte[] bArr) {
        return new n(sVar, bArr);
    }

    private static n b(InputStream inputStream) {
        return new n(inputStream);
    }
}
