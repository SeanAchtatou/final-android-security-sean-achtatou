package a.a.a;

public final class j extends aa {

    /* renamed from: a  reason: collision with root package name */
    private String f16a = null;

    /* renamed from: b  reason: collision with root package name */
    private int f17b = -1;

    public j() {
    }

    public j(String str, String str2) {
        super(str);
        this.f16a = str2;
    }

    public j(String str, String str2, int i) {
        super(str);
        this.f16a = str2;
        this.f17b = i;
    }

    public final String toString() {
        String aaVar = super.toString();
        if (this.f16a == null) {
            return aaVar;
        }
        String str = String.valueOf(aaVar) + " in string ``" + this.f16a + "''";
        return this.f17b >= 0 ? String.valueOf(str) + " at position " + this.f17b : str;
    }
}
