package a.a.a;

import java.text.ParseException;

final class g {

    /* renamed from: a  reason: collision with root package name */
    int f10a = 0;

    /* renamed from: b  reason: collision with root package name */
    char[] f11b = null;

    public g(char[] cArr) {
        this.f11b = cArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0008  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r3 = this;
            char[] r0 = r3.f11b
            int r0 = r0.length
        L_0x0003:
            int r1 = r3.f10a
            if (r1 < r0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            char[] r1 = r3.f11b
            int r2 = r3.f10a
            char r1 = r1[r2]
            switch(r1) {
                case 9: goto L_0x0012;
                case 10: goto L_0x0012;
                case 13: goto L_0x0012;
                case 32: goto L_0x0012;
                default: goto L_0x0011;
            }
        L_0x0011:
            goto L_0x0007
        L_0x0012:
            int r1 = r3.f10a
            int r1 = r1 + 1
            r3.f10a = r1
            goto L_0x0003
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.g.a():void");
    }

    public final boolean a(char c) {
        if (this.f10a >= this.f11b.length) {
            throw new ParseException("No more characters", this.f10a);
        } else if (this.f11b[this.f10a] != c) {
            return false;
        } else {
            this.f10a++;
            return true;
        }
    }

    public final int b() {
        int i = 0;
        int length = this.f11b.length;
        boolean z = false;
        while (this.f10a < length) {
            switch (this.f11b[this.f10a]) {
                case '0':
                    i *= 10;
                    break;
                case '1':
                    i = (i * 10) + 1;
                    break;
                case '2':
                    i = (i * 10) + 2;
                    break;
                case '3':
                    i = (i * 10) + 3;
                    break;
                case '4':
                    i = (i * 10) + 4;
                    break;
                case '5':
                    i = (i * 10) + 5;
                    break;
                case '6':
                    i = (i * 10) + 6;
                    break;
                case '7':
                    i = (i * 10) + 7;
                    break;
                case '8':
                    i = (i * 10) + 8;
                    break;
                case '9':
                    i = (i * 10) + 9;
                    break;
                default:
                    if (z) {
                        return i;
                    }
                    throw new ParseException("No Number found", this.f10a);
            }
            z = true;
            this.f10a++;
        }
        if (z) {
            return i;
        }
        throw new ParseException("No Number found", this.f10a);
    }

    public final int c() {
        try {
            char[] cArr = this.f11b;
            int i = this.f10a;
            this.f10a = i + 1;
            switch (cArr[i]) {
                case 'A':
                case 'a':
                    char[] cArr2 = this.f11b;
                    int i2 = this.f10a;
                    this.f10a = i2 + 1;
                    char c = cArr2[i2];
                    if (c == 'P' || c == 'p') {
                        char[] cArr3 = this.f11b;
                        int i3 = this.f10a;
                        this.f10a = i3 + 1;
                        char c2 = cArr3[i3];
                        if (c2 == 'R' || c2 == 'r') {
                            return 3;
                        }
                        throw new ParseException("Bad Month", this.f10a);
                    }
                    if (c == 'U' || c == 'u') {
                        char[] cArr4 = this.f11b;
                        int i4 = this.f10a;
                        this.f10a = i4 + 1;
                        char c3 = cArr4[i4];
                        if (c3 == 'G' || c3 == 'g') {
                            return 7;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'D':
                case 'd':
                    char[] cArr5 = this.f11b;
                    int i5 = this.f10a;
                    this.f10a = i5 + 1;
                    char c4 = cArr5[i5];
                    if (c4 == 'E' || c4 == 'e') {
                        char[] cArr6 = this.f11b;
                        int i6 = this.f10a;
                        this.f10a = i6 + 1;
                        char c5 = cArr6[i6];
                        if (c5 == 'C' || c5 == 'c') {
                            return 11;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'F':
                case 'f':
                    char[] cArr7 = this.f11b;
                    int i7 = this.f10a;
                    this.f10a = i7 + 1;
                    char c6 = cArr7[i7];
                    if (c6 == 'E' || c6 == 'e') {
                        char[] cArr8 = this.f11b;
                        int i8 = this.f10a;
                        this.f10a = i8 + 1;
                        char c7 = cArr8[i8];
                        if (c7 == 'B' || c7 == 'b') {
                            return 1;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'J':
                case 'j':
                    char[] cArr9 = this.f11b;
                    int i9 = this.f10a;
                    this.f10a = i9 + 1;
                    switch (cArr9[i9]) {
                        case 'A':
                        case 'a':
                            char[] cArr10 = this.f11b;
                            int i10 = this.f10a;
                            this.f10a = i10 + 1;
                            char c8 = cArr10[i10];
                            if (c8 == 'N' || c8 == 'n') {
                                return 0;
                            }
                        case 'U':
                        case 'u':
                            char[] cArr11 = this.f11b;
                            int i11 = this.f10a;
                            this.f10a = i11 + 1;
                            char c9 = cArr11[i11];
                            if (c9 == 'N' || c9 == 'n') {
                                return 5;
                            }
                            if (c9 == 'L' || c9 == 'l') {
                                return 6;
                            }
                            break;
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'M':
                case 'm':
                    char[] cArr12 = this.f11b;
                    int i12 = this.f10a;
                    this.f10a = i12 + 1;
                    char c10 = cArr12[i12];
                    if (c10 == 'A' || c10 == 'a') {
                        char[] cArr13 = this.f11b;
                        int i13 = this.f10a;
                        this.f10a = i13 + 1;
                        char c11 = cArr13[i13];
                        if (c11 == 'R' || c11 == 'r') {
                            return 2;
                        }
                        if (c11 == 'Y' || c11 == 'y') {
                            return 4;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'N':
                case 'n':
                    char[] cArr14 = this.f11b;
                    int i14 = this.f10a;
                    this.f10a = i14 + 1;
                    char c12 = cArr14[i14];
                    if (c12 == 'O' || c12 == 'o') {
                        char[] cArr15 = this.f11b;
                        int i15 = this.f10a;
                        this.f10a = i15 + 1;
                        char c13 = cArr15[i15];
                        if (c13 == 'V' || c13 == 'v') {
                            return 10;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'O':
                case 'o':
                    char[] cArr16 = this.f11b;
                    int i16 = this.f10a;
                    this.f10a = i16 + 1;
                    char c14 = cArr16[i16];
                    if (c14 == 'C' || c14 == 'c') {
                        char[] cArr17 = this.f11b;
                        int i17 = this.f10a;
                        this.f10a = i17 + 1;
                        char c15 = cArr17[i17];
                        if (c15 == 'T' || c15 == 't') {
                            return 9;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                case 'S':
                case 's':
                    char[] cArr18 = this.f11b;
                    int i18 = this.f10a;
                    this.f10a = i18 + 1;
                    char c16 = cArr18[i18];
                    if (c16 == 'E' || c16 == 'e') {
                        char[] cArr19 = this.f11b;
                        int i19 = this.f10a;
                        this.f10a = i19 + 1;
                        char c17 = cArr19[i19];
                        if (c17 == 'P' || c17 == 'p') {
                            return 8;
                        }
                    }
                    throw new ParseException("Bad Month", this.f10a);
                default:
                    throw new ParseException("Bad Month", this.f10a);
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
    }

    public final int d() {
        int i;
        boolean z;
        try {
            char[] cArr = this.f11b;
            int i2 = this.f10a;
            this.f10a = i2 + 1;
            switch (cArr[i2]) {
                case 'C':
                case 'c':
                    i = 360;
                    z = true;
                    break;
                case 'E':
                case 'e':
                    i = 300;
                    z = true;
                    break;
                case 'G':
                case 'g':
                    char[] cArr2 = this.f11b;
                    int i3 = this.f10a;
                    this.f10a = i3 + 1;
                    char c = cArr2[i3];
                    if (c == 'M' || c == 'm') {
                        char[] cArr3 = this.f11b;
                        int i4 = this.f10a;
                        this.f10a = i4 + 1;
                        char c2 = cArr3[i4];
                        if (c2 == 'T' || c2 == 't') {
                            z = false;
                            i = 0;
                            break;
                        }
                    }
                    throw new ParseException("Bad Alpha TimeZone", this.f10a);
                case 'M':
                case 'm':
                    i = 420;
                    z = true;
                    break;
                case 'P':
                case 'p':
                    i = 480;
                    z = true;
                    break;
                case 'U':
                case 'u':
                    char[] cArr4 = this.f11b;
                    int i5 = this.f10a;
                    this.f10a = i5 + 1;
                    char c3 = cArr4[i5];
                    if (c3 == 'T' || c3 == 't') {
                        z = false;
                        i = 0;
                        break;
                    } else {
                        throw new ParseException("Bad Alpha TimeZone", this.f10a);
                    }
                default:
                    throw new ParseException("Bad Alpha TimeZone", this.f10a);
            }
            if (z) {
                char[] cArr5 = this.f11b;
                int i6 = this.f10a;
                this.f10a = i6 + 1;
                char c4 = cArr5[i6];
                if (c4 == 'S' || c4 == 's') {
                    char[] cArr6 = this.f11b;
                    int i7 = this.f10a;
                    this.f10a = i7 + 1;
                    char c5 = cArr6[i7];
                    if (!(c5 == 'T' || c5 == 't')) {
                        throw new ParseException("Bad Alpha TimeZone", this.f10a);
                    }
                } else if (c4 == 'D' || c4 == 'd') {
                    char[] cArr7 = this.f11b;
                    int i8 = this.f10a;
                    this.f10a = i8 + 1;
                    char c6 = cArr7[i8];
                    if (c6 == 'T' || c6 != 't') {
                        return i - 60;
                    }
                    throw new ParseException("Bad Alpha TimeZone", this.f10a);
                }
            }
            return i;
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new ParseException("Bad Alpha TimeZone", this.f10a);
        }
    }
}
