package a.a.a;

import a.a.af;
import a.b.g;
import java.io.IOException;
import java.io.InputStream;

public final class o implements g {

    /* renamed from: b  reason: collision with root package name */
    private static boolean f24b;

    /* renamed from: a  reason: collision with root package name */
    private f f25a;

    static {
        f24b = true;
        try {
            String property = System.getProperty("mail.mime.ignoremultipartencoding");
            f24b = property == null || !property.equalsIgnoreCase("false");
        } catch (SecurityException e) {
        }
    }

    public o(f fVar) {
        this.f25a = fVar;
    }

    public final InputStream a() {
        InputStream f;
        try {
            if (this.f25a instanceof n) {
                f = ((n) this.f25a).d();
            } else if (this.f25a instanceof x) {
                f = ((x) this.f25a).f();
            } else {
                throw new af("Unknown part");
            }
            String a2 = a(this.f25a.a(), this.f25a);
            if (a2 != null) {
                return b.a(f, a2);
            }
            return f;
        } catch (af e) {
            throw new IOException(e.getMessage());
        }
    }

    private static String a(String str, f fVar) {
        if (!f24b || str == null) {
            return str;
        }
        if (str.equalsIgnoreCase("7bit") || str.equalsIgnoreCase("8bit") || str.equalsIgnoreCase("binary")) {
            return str;
        }
        String b2 = fVar.b();
        if (b2 == null) {
            return str;
        }
        try {
            i iVar = new i(b2);
            if (iVar.b("multipart/*") || iVar.b("message/*")) {
                return null;
            }
        } catch (aa e) {
        }
        return str;
    }

    public final String b() {
        try {
            return this.f25a.b();
        } catch (af e) {
            return "application/octet-stream";
        }
    }

    public final String c() {
        try {
            if (this.f25a instanceof n) {
                return ((n) this.f25a).c();
            }
        } catch (af e) {
        }
        return "";
    }
}
