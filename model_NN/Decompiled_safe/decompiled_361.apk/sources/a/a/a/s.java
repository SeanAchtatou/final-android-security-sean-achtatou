package a.a.a;

import a.a.af;
import com.a.a.a.g;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public final class s {

    /* renamed from: a  reason: collision with root package name */
    private List f29a = new ArrayList(40);

    public s() {
        this.f29a.add(new v("Return-Path", null));
        this.f29a.add(new v("Received", null));
        this.f29a.add(new v("Resent-Date", null));
        this.f29a.add(new v("Resent-From", null));
        this.f29a.add(new v("Resent-Sender", null));
        this.f29a.add(new v("Resent-To", null));
        this.f29a.add(new v("Resent-Cc", null));
        this.f29a.add(new v("Resent-Bcc", null));
        this.f29a.add(new v("Resent-Message-Id", null));
        this.f29a.add(new v("Date", null));
        this.f29a.add(new v("From", null));
        this.f29a.add(new v("Sender", null));
        this.f29a.add(new v("Reply-To", null));
        this.f29a.add(new v("To", null));
        this.f29a.add(new v("Cc", null));
        this.f29a.add(new v("Bcc", null));
        this.f29a.add(new v("Message-Id", null));
        this.f29a.add(new v("In-Reply-To", null));
        this.f29a.add(new v("References", null));
        this.f29a.add(new v("Subject", null));
        this.f29a.add(new v("Comments", null));
        this.f29a.add(new v("Keywords", null));
        this.f29a.add(new v("Errors-To", null));
        this.f29a.add(new v("MIME-Version", null));
        this.f29a.add(new v("Content-Type", null));
        this.f29a.add(new v("Content-Transfer-Encoding", null));
        this.f29a.add(new v("Content-MD5", null));
        this.f29a.add(new v(":", null));
        this.f29a.add(new v("Content-Length", null));
        this.f29a.add(new v("Status", null));
    }

    public s(InputStream inputStream) {
        a(inputStream);
    }

    private void a(InputStream inputStream) {
        String a2;
        g gVar = new g(inputStream);
        StringBuffer stringBuffer = new StringBuffer();
        String str = null;
        do {
            try {
                a2 = gVar.a();
                if (a2 == null || (!a2.startsWith(" ") && !a2.startsWith("\t"))) {
                    if (str != null) {
                        c(str);
                    } else if (stringBuffer.length() > 0) {
                        c(stringBuffer.toString());
                        stringBuffer.setLength(0);
                    }
                    str = a2;
                } else {
                    if (str != null) {
                        stringBuffer.append(str);
                        str = null;
                    }
                    stringBuffer.append("\r\n");
                    stringBuffer.append(a2);
                }
                if (a2 == null) {
                    return;
                }
            } catch (IOException e) {
                throw new af("Error in input stream", e);
            }
        } while (a2.length() > 0);
    }

    public final String[] a(String str) {
        ArrayList arrayList = new ArrayList();
        for (v vVar : this.f29a) {
            if (str.equalsIgnoreCase(vVar.a()) && vVar.f33b != null) {
                arrayList.add(vVar.b());
            }
        }
        if (arrayList.size() == 0) {
            return null;
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public final String a(String str, String str2) {
        String[] a2 = a(str);
        if (a2 == null) {
            return null;
        }
        if (a2.length == 1 || str2 == null) {
            return a2[0];
        }
        StringBuffer stringBuffer = new StringBuffer(a2[0]);
        for (int i = 1; i < a2.length; i++) {
            stringBuffer.append(str2);
            stringBuffer.append(a2[i]);
        }
        return stringBuffer.toString();
    }

    public final void b(String str, String str2) {
        int i;
        boolean z;
        int indexOf;
        int i2 = 0;
        boolean z2 = false;
        while (i2 < this.f29a.size()) {
            v vVar = (v) this.f29a.get(i2);
            if (!str.equalsIgnoreCase(vVar.a())) {
                i = i2;
                z = z2;
            } else if (!z2) {
                if (vVar.f33b == null || (indexOf = vVar.f33b.indexOf(58)) < 0) {
                    vVar.f33b = String.valueOf(str) + ": " + str2;
                } else {
                    vVar.f33b = String.valueOf(vVar.f33b.substring(0, indexOf + 1)) + " " + str2;
                }
                int i3 = i2;
                z = true;
                i = i3;
            } else {
                this.f29a.remove(i2);
                i = i2 - 1;
                z = z2;
            }
            z2 = z;
            i2 = i + 1;
        }
        if (!z2) {
            c(str, str2);
        }
    }

    private void c(String str, String str2) {
        int i;
        int size = this.f29a.size();
        boolean z = str.equalsIgnoreCase("Received") || str.equalsIgnoreCase("Return-Path");
        if (z) {
            size = 0;
        }
        int size2 = this.f29a.size() - 1;
        int i2 = size;
        while (size2 >= 0) {
            v vVar = (v) this.f29a.get(size2);
            if (str.equalsIgnoreCase(vVar.a())) {
                if (z) {
                    i2 = size2;
                } else {
                    this.f29a.add(size2 + 1, new v(str, str2));
                    return;
                }
            }
            if (vVar.a().equals(":")) {
                i = size2;
            } else {
                i = i2;
            }
            size2--;
            i2 = i;
        }
        this.f29a.add(i2, new v(str, str2));
    }

    public final void b(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f29a.size()) {
                v vVar = (v) this.f29a.get(i2);
                if (str.equalsIgnoreCase(vVar.a())) {
                    vVar.f33b = null;
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    private void c(String str) {
        try {
            char charAt = str.charAt(0);
            if (charAt == ' ' || charAt == 9) {
                v vVar = (v) this.f29a.get(this.f29a.size() - 1);
                vVar.f33b = String.valueOf(vVar.f33b) + "\r\n" + str;
                return;
            }
            this.f29a.add(new v(str));
        } catch (StringIndexOutOfBoundsException | NoSuchElementException e) {
        }
    }
}
