package a.a.a;

public final class q {
    private static final k h = new k(-4, null);

    /* renamed from: a  reason: collision with root package name */
    private String f27a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f28b;
    private String c;
    private int d;
    private int e;
    private int f;
    private int g;

    private q(String str, String str2, byte b2) {
        this.f27a = str == null ? "" : str;
        this.f28b = true;
        this.c = str2;
        this.g = 0;
        this.f = 0;
        this.d = 0;
        this.e = this.f27a.length();
    }

    public q(String str, String str2) {
        this(str, str2, (byte) 0);
    }

    public final k a() {
        k kVar;
        this.d = this.f;
        if (this.d >= this.e) {
            kVar = h;
        } else if (c() == -4) {
            kVar = h;
        } else {
            boolean z = false;
            while (true) {
                char charAt = this.f27a.charAt(this.d);
                if (charAt == '(') {
                    int i = this.d + 1;
                    this.d = i;
                    boolean z2 = z;
                    int i2 = 1;
                    while (i2 > 0 && this.d < this.e) {
                        char charAt2 = this.f27a.charAt(this.d);
                        if (charAt2 == '\\') {
                            this.d++;
                            z2 = true;
                        } else if (charAt2 == 13) {
                            z2 = true;
                        } else if (charAt2 == '(') {
                            i2++;
                        } else if (charAt2 == ')') {
                            i2--;
                        }
                        this.d++;
                    }
                    if (i2 != 0) {
                        throw new aa("Unbalanced comments");
                    } else if (!this.f28b) {
                        kVar = new k(-3, z2 ? a(this.f27a, i, this.d - 1) : this.f27a.substring(i, this.d - 1));
                    } else if (c() == -4) {
                        kVar = h;
                        break;
                    } else {
                        z = z2;
                    }
                } else if (charAt == '\"') {
                    int i3 = this.d + 1;
                    this.d = i3;
                    while (this.d < this.e) {
                        char charAt3 = this.f27a.charAt(this.d);
                        if (charAt3 == '\\') {
                            this.d++;
                            z = true;
                        } else if (charAt3 == 13) {
                            z = true;
                        } else if (charAt3 == '\"') {
                            this.d++;
                            kVar = new k(-2, z ? a(this.f27a, i3, this.d - 1) : this.f27a.substring(i3, this.d - 1));
                        }
                        this.d++;
                    }
                    throw new aa("Unbalanced quoted string");
                } else if (charAt < ' ' || charAt >= 127 || this.c.indexOf(charAt) >= 0) {
                    this.d++;
                    kVar = new k(charAt, new String(new char[]{charAt}));
                } else {
                    int i4 = this.d;
                    while (this.d < this.e && (r1 = this.f27a.charAt(this.d)) >= ' ' && r1 < 127 && r1 != '(' && r1 != ' ' && r1 != '\"' && this.c.indexOf(r1) < 0) {
                        this.d++;
                    }
                    kVar = new k(-1, this.f27a.substring(i4, this.d));
                }
            }
        }
        int i5 = this.d;
        this.g = i5;
        this.f = i5;
        return kVar;
    }

    public final String b() {
        return this.f27a.substring(this.f);
    }

    private int c() {
        while (this.d < this.e) {
            char charAt = this.f27a.charAt(this.d);
            if (charAt != ' ' && charAt != 9 && charAt != 13 && charAt != 10) {
                return this.d;
            }
            this.d++;
        }
        return -4;
    }

    private static String a(String str, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = false;
        boolean z2 = false;
        for (int i3 = i; i3 < i2; i3++) {
            char charAt = str.charAt(i3);
            if (charAt == 10 && z) {
                z = false;
            } else if (z2) {
                stringBuffer.append(charAt);
                z = false;
                z2 = false;
            } else if (charAt == '\\') {
                z = false;
                z2 = true;
            } else if (charAt == 13) {
                z = true;
            } else {
                stringBuffer.append(charAt);
                z = false;
            }
        }
        return stringBuffer.toString();
    }
}
