package a.a.a;

import a.a.j;

final class z {

    /* renamed from: a  reason: collision with root package name */
    private static int f39a = 0;

    z() {
    }

    public static String a() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("----=_Part_").append(b()).append("_").append(stringBuffer.hashCode()).append('.').append(System.currentTimeMillis());
        return stringBuffer.toString();
    }

    public static String a(j jVar) {
        String str;
        w a2 = w.a(jVar);
        if (a2 != null) {
            str = a2.b();
        } else {
            str = "javamailuser@localhost";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(stringBuffer.hashCode()).append('.').append(b()).append('.').append(System.currentTimeMillis()).append('.').append("JavaMail.").append(str);
        return stringBuffer.toString();
    }

    private static synchronized int b() {
        int i;
        synchronized (z.class) {
            i = f39a;
            f39a = i + 1;
        }
        return i;
    }
}
