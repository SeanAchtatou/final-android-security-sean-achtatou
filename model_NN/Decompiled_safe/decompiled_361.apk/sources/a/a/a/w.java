package a.a.a;

import a.a.j;
import a.a.q;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Locale;

public final class w extends q implements Cloneable {
    private static final String d = "()<>@,;:\\\"\t .[]".replace(' ', 0).replace(9, 0);

    /* renamed from: a  reason: collision with root package name */
    private String f34a;

    /* renamed from: b  reason: collision with root package name */
    private String f35b;
    private String c;

    public w() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[]
     arg types: [java.lang.String, int, int]
     candidates:
      a.a.a.w.a(java.lang.String, java.lang.String, int):int
      a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[] */
    public w(String str) {
        w[] a2 = a(str, true, false);
        if (a2.length != 1) {
            throw new j("Illegal address", str);
        }
        this.f34a = a2[0].f34a;
        this.f35b = a2[0].f35b;
        this.c = a2[0].c;
    }

    public final Object clone() {
        try {
            return (w) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public final String a() {
        return "rfc822";
    }

    public final String b() {
        return this.f34a;
    }

    public final String toString() {
        boolean z;
        if (this.c == null && this.f35b != null) {
            try {
                this.c = b.c(this.f35b);
            } catch (UnsupportedEncodingException e) {
            }
        }
        if (this.c != null) {
            return String.valueOf(b(this.c)) + " <" + this.f34a + ">";
        }
        if (this.f34a == null || !this.f34a.endsWith(";") || this.f34a.indexOf(58) <= 0) {
            z = false;
        } else {
            z = true;
        }
        if (!z) {
            if (!(this.f34a == null || a(this.f34a, "()<>,;:\\\"[]", 0) < 0)) {
                return "<" + this.f34a + ">";
            }
        }
        return this.f34a;
    }

    private static String b(String str) {
        int length = str.length();
        boolean z = false;
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt == '\"' || charAt == '\\') {
                StringBuffer stringBuffer = new StringBuffer(length + 3);
                stringBuffer.append('\"');
                for (int i2 = 0; i2 < length; i2++) {
                    char charAt2 = str.charAt(i2);
                    if (charAt2 == '\"' || charAt2 == '\\') {
                        stringBuffer.append('\\');
                    }
                    stringBuffer.append(charAt2);
                }
                stringBuffer.append('\"');
                return stringBuffer.toString();
            }
            if ((charAt < ' ' && charAt != 13 && charAt != 10 && charAt != 9) || charAt >= 127 || d.indexOf(charAt) >= 0) {
                z = true;
            }
        }
        if (!z) {
            return str;
        }
        StringBuffer stringBuffer2 = new StringBuffer(length + 2);
        stringBuffer2.append('\"').append(str).append('\"');
        return stringBuffer2.toString();
    }

    private static String c(String str) {
        if (!str.startsWith("\"") || !str.endsWith("\"")) {
            return str;
        }
        String substring = str.substring(1, str.length() - 1);
        if (substring.indexOf(92) < 0) {
            return substring;
        }
        StringBuffer stringBuffer = new StringBuffer(substring.length());
        int i = 0;
        while (i < substring.length()) {
            char charAt = substring.charAt(i);
            if (charAt == '\\' && i < substring.length() - 1) {
                i++;
                charAt = substring.charAt(i);
            }
            char c2 = charAt;
            stringBuffer.append(c2);
            i++;
        }
        return stringBuffer.toString();
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof w)) {
            return false;
        }
        String str = ((w) obj).f34a;
        if (str == this.f34a) {
            return true;
        }
        if (this.f34a == null || !this.f34a.equalsIgnoreCase(str)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        if (this.f34a == null) {
            return 0;
        }
        return this.f34a.toLowerCase(Locale.ENGLISH).hashCode();
    }

    public static String a(q[] qVarArr) {
        if (qVarArr == null || qVarArr.length == 0) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        for (int i2 = 0; i2 < qVarArr.length; i2++) {
            if (i2 != 0) {
                stringBuffer.append(", ");
                i += 2;
            }
            String qVar = qVarArr[i2].toString();
            int indexOf = qVar.indexOf("\r\n");
            if (indexOf == -1) {
                indexOf = qVar.length();
            }
            if (indexOf + i > 76) {
                stringBuffer.append("\r\n\t");
                i = 8;
            }
            stringBuffer.append(qVar);
            int lastIndexOf = qVar.lastIndexOf("\r\n");
            i = lastIndexOf != -1 ? (qVar.length() - lastIndexOf) - 2 : i + qVar.length();
        }
        return stringBuffer.toString();
    }

    public static w a(j jVar) {
        String a2;
        String str;
        String str2;
        InetAddress localHost;
        if (jVar == null) {
            try {
                String property = System.getProperty("user.name");
                str = InetAddress.getLocalHost().getHostName();
                str2 = property;
                a2 = null;
            } catch (j | SecurityException | UnknownHostException e) {
            }
        } else {
            a2 = jVar.a("mail.from");
            if (a2 == null) {
                String a3 = jVar.a("mail.user");
                if (a3 == null || a3.length() == 0) {
                    a3 = jVar.a("user.name");
                }
                if (a3 == null || a3.length() == 0) {
                    a3 = System.getProperty("user.name");
                }
                String a4 = jVar.a("mail.host");
                if ((a4 == null || a4.length() == 0) && (localHost = InetAddress.getLocalHost()) != null) {
                    str2 = a3;
                    str = localHost.getHostName();
                } else {
                    String str3 = a4;
                    str2 = a3;
                    str = str3;
                }
            } else {
                str = null;
                str2 = null;
            }
        }
        if (!(a2 != null || str2 == null || str2.length() == 0 || str == null || str.length() == 0)) {
            a2 = String.valueOf(str2) + "@" + str;
        }
        if (a2 != null) {
            return new w(a2);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[]
     arg types: [java.lang.String, int, int]
     candidates:
      a.a.a.w.a(java.lang.String, java.lang.String, int):int
      a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[] */
    public static w[] a(String str) {
        return a(str, true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[]
     arg types: [java.lang.String, boolean, int]
     candidates:
      a.a.a.w.a(java.lang.String, java.lang.String, int):int
      a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[] */
    public static w[] a(String str, boolean z) {
        return a(str, z, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:153:0x015b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x0088 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static a.a.a.w[] a(java.lang.String r19, boolean r20, boolean r21) {
        /*
            r4 = -1
            r5 = -1
            int r6 = r19.length()
            r7 = 0
            r8 = 0
            r9 = 0
            java.util.Vector r10 = new java.util.Vector
            r10.<init>()
            r11 = -1
            r12 = 0
            r13 = r11
            r16 = r8
            r8 = r5
            r5 = r16
            r17 = r4
            r4 = r9
            r9 = r17
            r18 = r11
            r11 = r12
            r12 = r18
        L_0x0020:
            if (r11 < r6) goto L_0x007a
            if (r13 < 0) goto L_0x0069
            r6 = -1
            if (r12 != r6) goto L_0x0297
            r6 = r11
        L_0x0028:
            r0 = r19
            r1 = r13
            r2 = r6
            java.lang.String r6 = r0.substring(r1, r2)
            java.lang.String r6 = r6.trim()
            if (r4 != 0) goto L_0x003a
            if (r20 != 0) goto L_0x003a
            if (r21 == 0) goto L_0x026e
        L_0x003a:
            if (r20 != 0) goto L_0x003e
            if (r21 != 0) goto L_0x0041
        L_0x003e:
            b(r6, r5)
        L_0x0041:
            a.a.a.w r20 = new a.a.a.w
            r20.<init>()
            r0 = r6
            r1 = r20
            r1.f34a = r0
            if (r9 < 0) goto L_0x0063
            r0 = r19
            r1 = r9
            r2 = r8
            java.lang.String r19 = r0.substring(r1, r2)
            java.lang.String r19 = r19.trim()
            java.lang.String r19 = c(r19)
            r0 = r19
            r1 = r20
            r1.c = r0
        L_0x0063:
            r0 = r10
            r1 = r20
            r0.addElement(r1)
        L_0x0069:
            int r19 = r10.size()
            r0 = r19
            a.a.a.w[] r0 = new a.a.a.w[r0]
            r19 = r0
            r0 = r10
            r1 = r19
            r0.copyInto(r1)
            return r19
        L_0x007a:
            r0 = r19
            r1 = r11
            char r14 = r0.charAt(r1)
            switch(r14) {
                case 9: goto L_0x0088;
                case 10: goto L_0x0088;
                case 13: goto L_0x0088;
                case 32: goto L_0x0088;
                case 34: goto L_0x0150;
                case 40: goto L_0x008b;
                case 41: goto L_0x00d1;
                case 44: goto L_0x019e;
                case 58: goto L_0x021e;
                case 59: goto L_0x0237;
                case 60: goto L_0x00e0;
                case 62: goto L_0x0141;
                case 91: goto L_0x017a;
                default: goto L_0x0084;
            }
        L_0x0084:
            r14 = -1
            if (r13 != r14) goto L_0x0088
            r13 = r11
        L_0x0088:
            int r11 = r11 + 1
            goto L_0x0020
        L_0x008b:
            r4 = 1
            if (r13 < 0) goto L_0x0092
            r14 = -1
            if (r12 != r14) goto L_0x0092
            r12 = r11
        L_0x0092:
            r14 = -1
            if (r9 != r14) goto L_0x0097
            int r9 = r11 + 1
        L_0x0097:
            int r11 = r11 + 1
            r14 = 1
            r16 = r14
            r14 = r11
            r11 = r16
        L_0x009f:
            if (r14 >= r6) goto L_0x00a3
            if (r11 > 0) goto L_0x00b4
        L_0x00a3:
            if (r11 <= 0) goto L_0x00ca
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing ')'"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r14
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x00b4:
            r0 = r19
            r1 = r14
            char r15 = r0.charAt(r1)
            switch(r15) {
                case 40: goto L_0x00c4;
                case 41: goto L_0x00c7;
                case 92: goto L_0x00c1;
                default: goto L_0x00be;
            }
        L_0x00be:
            int r14 = r14 + 1
            goto L_0x009f
        L_0x00c1:
            int r14 = r14 + 1
            goto L_0x00be
        L_0x00c4:
            int r11 = r11 + 1
            goto L_0x00be
        L_0x00c7:
            int r11 = r11 + -1
            goto L_0x00be
        L_0x00ca:
            int r11 = r14 + -1
            r14 = -1
            if (r8 != r14) goto L_0x0088
            r8 = r11
            goto L_0x0088
        L_0x00d1:
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing '('"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x00e0:
            r4 = 1
            if (r5 == 0) goto L_0x00f2
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Extra route-addr"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x00f2:
            if (r7 != 0) goto L_0x02a3
            if (r13 < 0) goto L_0x02a0
            r5 = r11
        L_0x00f7:
            int r8 = r11 + 1
            r9 = r8
            r8 = r13
        L_0x00fb:
            r12 = 0
        L_0x00fc:
            int r11 = r11 + 1
            if (r11 < r6) goto L_0x0113
        L_0x0100:
            if (r11 < r6) goto L_0x0139
            if (r12 == 0) goto L_0x012a
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing '\"'"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x0113:
            r0 = r19
            r1 = r11
            char r13 = r0.charAt(r1)
            switch(r13) {
                case 34: goto L_0x011e;
                case 62: goto L_0x0127;
                case 92: goto L_0x0122;
                default: goto L_0x011d;
            }
        L_0x011d:
            goto L_0x00fc
        L_0x011e:
            if (r12 == 0) goto L_0x0125
            r12 = 0
            goto L_0x00fc
        L_0x0122:
            int r11 = r11 + 1
            goto L_0x00fc
        L_0x0125:
            r12 = 1
            goto L_0x00fc
        L_0x0127:
            if (r12 == 0) goto L_0x0100
            goto L_0x00fc
        L_0x012a:
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing '>'"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x0139:
            r12 = 1
            r13 = r9
            r9 = r8
            r8 = r5
            r5 = r12
            r12 = r11
            goto L_0x0088
        L_0x0141:
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing '<'"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x0150:
            r4 = 1
            r14 = -1
            if (r13 != r14) goto L_0x0155
            r13 = r11
        L_0x0155:
            int r11 = r11 + 1
        L_0x0157:
            if (r11 < r6) goto L_0x016a
        L_0x0159:
            if (r11 < r6) goto L_0x0088
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing '\"'"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x016a:
            r0 = r19
            r1 = r11
            char r14 = r0.charAt(r1)
            switch(r14) {
                case 34: goto L_0x0159;
                case 92: goto L_0x0177;
                default: goto L_0x0174;
            }
        L_0x0174:
            int r11 = r11 + 1
            goto L_0x0157
        L_0x0177:
            int r11 = r11 + 1
            goto L_0x0174
        L_0x017a:
            r4 = 1
        L_0x017b:
            int r11 = r11 + 1
            if (r11 < r6) goto L_0x0190
        L_0x017f:
            if (r11 < r6) goto L_0x0088
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Missing ']'"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x0190:
            r0 = r19
            r1 = r11
            char r14 = r0.charAt(r1)
            switch(r14) {
                case 92: goto L_0x019b;
                case 93: goto L_0x017f;
                default: goto L_0x019a;
            }
        L_0x019a:
            goto L_0x017b
        L_0x019b:
            int r11 = r11 + 1
            goto L_0x017b
        L_0x019e:
            r14 = -1
            if (r13 != r14) goto L_0x01ac
            r4 = 0
            r5 = 0
            r12 = -1
            r13 = r12
            r16 = r4
            r4 = r5
            r5 = r16
            goto L_0x0088
        L_0x01ac:
            if (r7 == 0) goto L_0x01b1
            r5 = 0
            goto L_0x0088
        L_0x01b1:
            r14 = -1
            if (r12 != r14) goto L_0x01b5
            r12 = r11
        L_0x01b5:
            r0 = r19
            r1 = r13
            r2 = r12
            java.lang.String r12 = r0.substring(r1, r2)
            java.lang.String r12 = r12.trim()
            if (r4 != 0) goto L_0x01c7
            if (r20 != 0) goto L_0x01c7
            if (r21 == 0) goto L_0x01fd
        L_0x01c7:
            if (r20 != 0) goto L_0x01cb
            if (r21 != 0) goto L_0x01ce
        L_0x01cb:
            b(r12, r5)
        L_0x01ce:
            a.a.a.w r4 = new a.a.a.w
            r4.<init>()
            r4.f34a = r12
            if (r9 < 0) goto L_0x029c
            r0 = r19
            r1 = r9
            r2 = r8
            java.lang.String r5 = r0.substring(r1, r2)
            java.lang.String r5 = r5.trim()
            java.lang.String r5 = c(r5)
            r4.c = r5
            r5 = -1
            r8 = r5
        L_0x01eb:
            r10.addElement(r4)
            r4 = r5
            r5 = r8
        L_0x01f0:
            r8 = 0
            r9 = 0
            r12 = -1
            r13 = r12
            r16 = r8
            r8 = r4
            r4 = r9
            r9 = r5
            r5 = r16
            goto L_0x0088
        L_0x01fd:
            java.util.StringTokenizer r4 = new java.util.StringTokenizer
            r4.<init>(r12)
        L_0x0202:
            boolean r5 = r4.hasMoreTokens()
            if (r5 != 0) goto L_0x020b
            r4 = r8
            r5 = r9
            goto L_0x01f0
        L_0x020b:
            java.lang.String r5 = r4.nextToken()
            r12 = 0
            b(r5, r12)
            a.a.a.w r12 = new a.a.a.w
            r12.<init>()
            r12.f34a = r5
            r10.addElement(r12)
            goto L_0x0202
        L_0x021e:
            r4 = 1
            if (r7 == 0) goto L_0x0230
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Nested group"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x0230:
            r7 = 1
            r14 = -1
            if (r13 != r14) goto L_0x0088
            r13 = r11
            goto L_0x0088
        L_0x0237:
            r5 = -1
            if (r13 != r5) goto L_0x029a
            r5 = r11
        L_0x023b:
            if (r7 != 0) goto L_0x024c
            a.a.a.j r20 = new a.a.a.j
            java.lang.String r21 = "Illegal semicolon, not in group"
            r0 = r20
            r1 = r21
            r2 = r19
            r3 = r11
            r0.<init>(r1, r2, r3)
            throw r20
        L_0x024c:
            r7 = 0
            r12 = -1
            if (r5 != r12) goto L_0x0251
            r5 = r11
        L_0x0251:
            a.a.a.w r12 = new a.a.a.w
            r12.<init>()
            int r13 = r11 + 1
            r0 = r19
            r1 = r5
            r2 = r13
            java.lang.String r5 = r0.substring(r1, r2)
            java.lang.String r5 = r5.trim()
            r12.f34a = r5
            r10.addElement(r12)
            r5 = 0
            r12 = -1
            r13 = r12
            goto L_0x0088
        L_0x026e:
            java.util.StringTokenizer r19 = new java.util.StringTokenizer
            r0 = r19
            r1 = r6
            r0.<init>(r1)
        L_0x0276:
            boolean r20 = r19.hasMoreTokens()
            if (r20 == 0) goto L_0x0069
            java.lang.String r20 = r19.nextToken()
            r21 = 0
            b(r20, r21)
            a.a.a.w r21 = new a.a.a.w
            r21.<init>()
            r0 = r20
            r1 = r21
            r1.f34a = r0
            r0 = r10
            r1 = r21
            r0.addElement(r1)
            goto L_0x0276
        L_0x0297:
            r6 = r12
            goto L_0x0028
        L_0x029a:
            r5 = r13
            goto L_0x023b
        L_0x029c:
            r5 = r8
            r8 = r9
            goto L_0x01eb
        L_0x02a0:
            r5 = r8
            goto L_0x00f7
        L_0x02a3:
            r5 = r8
            r8 = r9
            r9 = r13
            goto L_0x00fb
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.w.a(java.lang.String, boolean, boolean):a.a.a.w[]");
    }

    private static void b(String str, boolean z) {
        int i;
        String str2;
        String str3;
        if (str.indexOf(34) < 0) {
            if (z) {
                i = 0;
                while (true) {
                    int a2 = a(str, ",:", i);
                    if (a2 < 0) {
                        break;
                    } else if (str.charAt(i) != '@') {
                        throw new j("Illegal route-addr", str);
                    } else if (str.charAt(a2) == ':') {
                        i = a2 + 1;
                        break;
                    } else {
                        i = a2 + 1;
                    }
                }
            } else {
                i = 0;
            }
            int indexOf = str.indexOf(64, i);
            if (indexOf < 0) {
                str2 = null;
                str3 = str;
            } else if (indexOf == i) {
                throw new j("Missing local name", str);
            } else if (indexOf == str.length() - 1) {
                throw new j("Missing domain", str);
            } else {
                str3 = str.substring(i, indexOf);
                str2 = str.substring(indexOf + 1);
            }
            if (a(str, " \t\n\r", 0) >= 0) {
                throw new j("Illegal whitespace in address", str);
            } else if (a(str3, "()<>,;:\\\"[]@", 0) >= 0) {
                throw new j("Illegal character in local name", str);
            } else if (str2 != null && str2.indexOf(91) < 0 && a(str2, "()<>,;:\\\"[]@", 0) >= 0) {
                throw new j("Illegal character in domain", str);
            }
        }
    }

    private static int a(String str, String str2, int i) {
        try {
            int length = str.length();
            for (int i2 = i; i2 < length; i2++) {
                if (str2.indexOf(str.charAt(i2)) >= 0) {
                    return i2;
                }
            }
            return -1;
        } catch (StringIndexOutOfBoundsException e) {
            return -1;
        }
    }
}
