package a.a.a;

import java.io.EOFException;
import java.io.OutputStream;

final class d extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private boolean f6a;

    /* renamed from: b  reason: collision with root package name */
    private int f7b = 0;
    private int c = 0;
    private int d = 0;
    private boolean e = false;
    private boolean f = false;
    private boolean g = false;
    private int h = 0;
    private int i = 0;

    public d(boolean z, boolean z2) {
        boolean z3 = false;
        this.f6a = z;
        if (z2 && z) {
            z3 = true;
        }
        this.g = z3;
    }

    public final void write(int i2) {
        a(i2);
    }

    public final void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public final void write(byte[] bArr, int i2, int i3) {
        int i4 = i3 + i2;
        for (int i5 = i2; i5 < i4; i5++) {
            a(bArr[i5]);
        }
    }

    private final void a(int i2) {
        int i3 = i2 & 255;
        if (this.g && ((this.h == 13 && i3 != 10) || (this.h != 13 && i3 == 10))) {
            this.f = true;
        }
        if (i3 == 13 || i3 == 10) {
            this.d = 0;
        } else {
            this.d++;
            if (this.d > 998) {
                this.e = true;
            }
        }
        if (b.a(i3)) {
            this.c++;
            if (this.f6a) {
                this.i = 3;
                throw new EOFException();
            }
        } else {
            this.f7b++;
        }
        this.h = i3;
    }

    public final int a() {
        if (this.i != 0) {
            return this.i;
        }
        if (this.f) {
            return 3;
        }
        if (this.c == 0) {
            if (this.e) {
                return 2;
            }
            return 1;
        } else if (this.f7b > this.c) {
            return 2;
        } else {
            return 3;
        }
    }
}
