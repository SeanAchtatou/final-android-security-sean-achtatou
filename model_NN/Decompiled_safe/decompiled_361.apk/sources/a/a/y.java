package a.a;

import java.net.URL;
import java.security.PrivilegedExceptionAction;

final class y implements PrivilegedExceptionAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ URL f83a;

    y(URL url) {
        this.f83a = url;
    }

    public final Object run() {
        return this.f83a.openStream();
    }
}
