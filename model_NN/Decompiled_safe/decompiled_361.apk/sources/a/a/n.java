package a.a;

public final class n extends af {

    /* renamed from: a  reason: collision with root package name */
    private transient q[] f69a;

    /* renamed from: b  reason: collision with root package name */
    private transient q[] f70b;
    private transient q[] c;

    public n() {
    }

    public n(String str) {
        super(str);
    }

    public n(String str, Exception exc, q[] qVarArr, q[] qVarArr2, q[] qVarArr3) {
        super(str, exc);
        this.f70b = qVarArr;
        this.c = qVarArr2;
        this.f69a = qVarArr3;
    }

    public final q[] a() {
        return this.f70b;
    }

    public final q[] b() {
        return this.c;
    }

    public final q[] c() {
        return this.f69a;
    }
}
