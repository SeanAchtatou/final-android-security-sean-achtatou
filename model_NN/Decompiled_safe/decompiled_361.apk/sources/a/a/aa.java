package a.a;

import java.io.IOException;
import java.net.URL;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.Vector;

final class aa implements PrivilegedAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ClassLoader f40a;

    /* renamed from: b  reason: collision with root package name */
    private final /* synthetic */ String f41b;

    aa(ClassLoader classLoader, String str) {
        this.f40a = classLoader;
        this.f41b = str;
    }

    public final Object run() {
        try {
            Vector vector = new Vector();
            Enumeration<URL> resources = this.f40a.getResources(this.f41b);
            while (resources != null && resources.hasMoreElements()) {
                URL nextElement = resources.nextElement();
                if (nextElement != null) {
                    vector.addElement(nextElement);
                }
            }
            if (vector.size() <= 0) {
                return null;
            }
            URL[] urlArr = new URL[vector.size()];
            vector.copyInto(urlArr);
            return urlArr;
        } catch (IOException | SecurityException e) {
            return null;
        }
    }
}
