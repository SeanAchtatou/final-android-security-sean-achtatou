package a.a.c;

import a.a.a.r;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

public final class a extends ByteArrayInputStream implements r {

    /* renamed from: a  reason: collision with root package name */
    private int f52a = 0;

    public a(byte[] bArr) {
        super(bArr);
    }

    private a(byte[] bArr, int i, int i2) {
        super(bArr, i, i2);
        this.f52a = i;
    }

    public final long a() {
        return (long) (this.pos - this.f52a);
    }

    public final InputStream a(long j, long j2) {
        long j3;
        if (j < 0) {
            throw new IllegalArgumentException("start < 0");
        }
        if (j2 == -1) {
            j3 = (long) (this.count - this.f52a);
        } else {
            j3 = j2;
        }
        return new a(this.buf, this.f52a + ((int) j), (int) (j3 - j));
    }
}
