package a.a;

import com.a.a.a.g;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.URL;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.util.Hashtable;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

public final class j {
    private static j j = null;

    /* renamed from: a  reason: collision with root package name */
    private final Properties f65a;

    /* renamed from: b  reason: collision with root package name */
    private final ah f66b;
    private final Hashtable c = new Hashtable();
    private boolean d = false;
    private PrintStream e;
    private final Vector f = new Vector();
    private final Hashtable g = new Hashtable();
    private final Hashtable h = new Hashtable();
    /* access modifiers changed from: private */
    public final Properties i = new Properties();

    private j(Properties properties, ah ahVar) {
        Class<?> cls;
        this.f65a = properties;
        this.f66b = ahVar;
        if (Boolean.valueOf(properties.getProperty("mail.debug")).booleanValue()) {
            this.d = true;
        }
        if (this.d) {
            c("DEBUG: JavaMail version 1.4.1");
        }
        if (ahVar != null) {
            cls = ahVar.getClass();
        } else {
            cls = getClass();
        }
        ae aeVar = new ae(this);
        try {
            a(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.providers", aeVar);
        } catch (SecurityException e2) {
            if (this.d) {
                c("DEBUG: can't get java.home: " + e2);
            }
        }
        b("META-INF/javamail.providers", cls, aeVar);
        a("/META-INF/javamail.default.providers", cls, aeVar);
        if (this.f.size() == 0) {
            if (this.d) {
                c("DEBUG: failed to load any providers, using defaults");
            }
            a(new h(x.f81a, "imap", "com.sun.mail.imap.IMAPStore", "Sun Microsystems, Inc.", "1.4.1"));
            a(new h(x.f81a, "imaps", "com.sun.mail.imap.IMAPSSLStore", "Sun Microsystems, Inc.", "1.4.1"));
            a(new h(x.f81a, "pop3", "com.sun.mail.pop3.POP3Store", "Sun Microsystems, Inc.", "1.4.1"));
            a(new h(x.f81a, "pop3s", "com.sun.mail.pop3.POP3SSLStore", "Sun Microsystems, Inc.", "1.4.1"));
            a(new h(x.f82b, "smtp", "com.sun.mail.smtp.SMTPTransport", "Sun Microsystems, Inc.", "1.4.1"));
            a(new h(x.f82b, "smtps", "com.sun.mail.smtp.SMTPSSLTransport", "Sun Microsystems, Inc.", "1.4.1"));
        }
        if (this.d) {
            c("DEBUG: Tables of loaded providers");
            c("DEBUG: Providers Listed By Class Name: " + this.h.toString());
            c("DEBUG: Providers Listed By Protocol: " + this.g.toString());
        }
        ad adVar = new ad(this);
        a("/META-INF/javamail.default.address.map", cls, adVar);
        b("META-INF/javamail.address.map", cls, adVar);
        try {
            a(String.valueOf(System.getProperty("java.home")) + File.separator + "lib" + File.separator + "javamail.address.map", adVar);
        } catch (SecurityException e3) {
            if (this.d) {
                c("DEBUG: can't get java.home: " + e3);
            }
        }
        if (this.i.isEmpty()) {
            if (this.d) {
                c("DEBUG: failed to load address map, using defaults");
            }
            this.i.put("rfc822", "smtp");
        }
    }

    public static j a(Properties properties, ah ahVar) {
        return new j(properties, ahVar);
    }

    public static synchronized j b(Properties properties, ah ahVar) {
        j jVar;
        synchronized (j.class) {
            if (j == null) {
                j = new j(properties, ahVar);
            } else if (j.f66b != ahVar && (j.f66b == null || ahVar == null || j.f66b.getClass().getClassLoader() != ahVar.getClass().getClassLoader())) {
                throw new SecurityException("Access to default session denied");
            }
            jVar = j;
        }
        return jVar;
    }

    public final synchronized PrintStream a() {
        PrintStream printStream;
        if (this.e == null) {
            printStream = System.out;
        } else {
            printStream = this.e;
        }
        return printStream;
    }

    private synchronized h b(String str) {
        h hVar;
        if (str != null) {
            if (str.length() > 0) {
                hVar = null;
                String property = this.f65a.getProperty("mail." + str + ".class");
                if (property != null) {
                    if (this.d) {
                        c("DEBUG: mail." + str + ".class property exists and points to " + property);
                    }
                    hVar = (h) this.h.get(property);
                }
                if (hVar == null) {
                    hVar = (h) this.g.get(str);
                    if (hVar == null) {
                        throw new r("No provider for " + str);
                    } else if (this.d) {
                        c("DEBUG: getProvider() returning " + hVar.toString());
                    }
                }
            }
        }
        throw new r("Invalid protocol: null");
        return hVar;
    }

    public final w a(q qVar) {
        String str = (String) this.i.get(qVar.a());
        if (str == null) {
            throw new r("No provider for Address type: " + qVar.a());
        }
        g gVar = new g(str, null, -1, null, null);
        return a(b(gVar.b()), gVar);
    }

    private w a(h hVar, g gVar) {
        if (hVar == null || hVar.a() != x.f82b) {
            throw new r("invalid provider");
        }
        try {
            return (w) b(hVar, gVar);
        } catch (ClassCastException e2) {
            throw new r("incorrect class");
        }
    }

    private Object b(h hVar, g gVar) {
        g gVar2;
        ClassLoader classLoader;
        Class<?> cls;
        Class<?> cls2 = null;
        if (hVar == null) {
            throw new r("null");
        }
        if (gVar == null) {
            gVar2 = new g(hVar.b(), null, -1, null, null);
        } else {
            gVar2 = gVar;
        }
        if (this.f66b != null) {
            classLoader = this.f66b.getClass().getClassLoader();
        } else {
            classLoader = getClass().getClassLoader();
        }
        try {
            ClassLoader b2 = b();
            if (b2 != null) {
                try {
                    cls2 = b2.loadClass(hVar.c());
                } catch (ClassNotFoundException e2) {
                }
            }
            if (cls2 == null) {
                cls = classLoader.loadClass(hVar.c());
            } else {
                cls = cls2;
            }
        } catch (Exception e3) {
            try {
                cls = Class.forName(hVar.c());
            } catch (Exception e4) {
                if (this.d) {
                    e4.printStackTrace(a());
                }
                throw new r(hVar.b());
            }
        }
        try {
            return cls.getConstructor(j.class, g.class).newInstance(this, gVar2);
        } catch (Exception e5) {
            if (this.d) {
                e5.printStackTrace(a());
            }
            throw new r(hVar.b());
        }
    }

    public final i a(g gVar) {
        return (i) this.c.get(gVar);
    }

    public final i a(InetAddress inetAddress, int i2, String str, String str2) {
        if (this.f66b != null) {
            return this.f66b.a(inetAddress, i2, str, str2);
        }
        return null;
    }

    public final String a(String str) {
        return this.f65a.getProperty(str);
    }

    static /* synthetic */ void a(j jVar, InputStream inputStream) {
        if (inputStream != null) {
            g gVar = new g(inputStream);
            while (true) {
                String a2 = gVar.a();
                if (a2 != null) {
                    if (!a2.startsWith("#")) {
                        StringTokenizer stringTokenizer = new StringTokenizer(a2, ";");
                        String str = null;
                        String str2 = null;
                        String str3 = null;
                        String str4 = null;
                        x xVar = null;
                        while (stringTokenizer.hasMoreTokens()) {
                            String trim = stringTokenizer.nextToken().trim();
                            int indexOf = trim.indexOf("=");
                            if (trim.startsWith("protocol=")) {
                                str4 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("type=")) {
                                String substring = trim.substring(indexOf + 1);
                                if (substring.equalsIgnoreCase("store")) {
                                    xVar = x.f81a;
                                } else if (substring.equalsIgnoreCase("transport")) {
                                    xVar = x.f82b;
                                }
                            } else if (trim.startsWith("class=")) {
                                str3 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("vendor=")) {
                                str2 = trim.substring(indexOf + 1);
                            } else if (trim.startsWith("version=")) {
                                str = trim.substring(indexOf + 1);
                            }
                        }
                        if (xVar != null && str4 != null && str3 != null && str4.length() > 0 && str3.length() > 0) {
                            jVar.a(new h(xVar, str4, str3, str2, str));
                        } else if (jVar.d) {
                            jVar.c("DEBUG: Bad provider entry: " + a2);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }

    private synchronized void a(h hVar) {
        this.f.addElement(hVar);
        this.h.put(hVar.c(), hVar);
        if (!this.g.containsKey(hVar.b())) {
            this.g.put(hVar.b(), hVar);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0030 A[Catch:{ all -> 0x009e }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0056 A[SYNTHETIC, Splitter:B:16:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0064 A[Catch:{ all -> 0x009e }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x008a A[SYNTHETIC, Splitter:B:26:0x008a] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0096 A[SYNTHETIC, Splitter:B:32:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:21:0x0060=Splitter:B:21:0x0060, B:11:0x002c=Splitter:B:11:0x002c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r6, a.a.l r7) {
        /*
            r5 = this;
            r0 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x0028, SecurityException -> 0x005c, all -> 0x0090 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0028, SecurityException -> 0x005c, all -> 0x0090 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0028, SecurityException -> 0x005c, all -> 0x0090 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0028, SecurityException -> 0x005c, all -> 0x0090 }
            r7.a(r1)     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
            boolean r0 = r5.d     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
            if (r0 == 0) goto L_0x0024
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
            java.lang.String r2 = "DEBUG: successfully loaded file: "
            r0.<init>(r2)     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
            r5.c(r0)     // Catch:{ IOException -> 0x00a2, SecurityException -> 0x00a0 }
        L_0x0024:
            r1.close()     // Catch:{ IOException -> 0x009c }
        L_0x0027:
            return
        L_0x0028:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x002c:
            boolean r2 = r5.d     // Catch:{ all -> 0x009e }
            if (r2 == 0) goto L_0x0054
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r3 = "DEBUG: not loading file: "
            r2.<init>(r3)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ all -> 0x009e }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x009e }
            r5.c(r2)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x009e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009e }
            r5.c(r0)     // Catch:{ all -> 0x009e }
        L_0x0054:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x0027
        L_0x005a:
            r0 = move-exception
            goto L_0x0027
        L_0x005c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0060:
            boolean r2 = r5.d     // Catch:{ all -> 0x009e }
            if (r2 == 0) goto L_0x0088
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r3 = "DEBUG: not loading file: "
            r2.<init>(r3)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ all -> 0x009e }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x009e }
            r5.c(r2)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x009e }
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)     // Catch:{ all -> 0x009e }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x009e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x009e }
            r5.c(r0)     // Catch:{ all -> 0x009e }
        L_0x0088:
            if (r1 == 0) goto L_0x0027
            r1.close()     // Catch:{ IOException -> 0x008e }
            goto L_0x0027
        L_0x008e:
            r0 = move-exception
            goto L_0x0027
        L_0x0090:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0094:
            if (r1 == 0) goto L_0x0099
            r1.close()     // Catch:{ IOException -> 0x009a }
        L_0x0099:
            throw r0
        L_0x009a:
            r1 = move-exception
            goto L_0x0099
        L_0x009c:
            r0 = move-exception
            goto L_0x0027
        L_0x009e:
            r0 = move-exception
            goto L_0x0094
        L_0x00a0:
            r0 = move-exception
            goto L_0x0060
        L_0x00a2:
            r0 = move-exception
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.j.a(java.lang.String, a.a.l):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0067 A[Catch:{ all -> 0x0094 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x007b A[SYNTHETIC, Splitter:B:32:0x007b] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0087 A[SYNTHETIC, Splitter:B:38:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:17:0x0041=Splitter:B:17:0x0041, B:27:0x0063=Splitter:B:27:0x0063} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.lang.String r6, java.lang.Class r7, a.a.l r8) {
        /*
            r5 = this;
            r0 = 0
            java.io.InputStream r0 = a(r7, r6)     // Catch:{ IOException -> 0x009b, SecurityException -> 0x005f, all -> 0x0081 }
            if (r0 == 0) goto L_0x0026
            r8.a(r0)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            boolean r1 = r5.d     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            if (r1 == 0) goto L_0x0020
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            java.lang.String r2 = "DEBUG: successfully loaded resource: "
            r1.<init>(r2)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            r5.c(r1)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
        L_0x0020:
            if (r0 == 0) goto L_0x0025
            r0.close()     // Catch:{ IOException -> 0x008d }
        L_0x0025:
            return
        L_0x0026:
            boolean r1 = r5.d     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            if (r1 == 0) goto L_0x0020
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            java.lang.String r2 = "DEBUG: not loading resource: "
            r1.<init>(r2)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            r5.c(r1)     // Catch:{ IOException -> 0x003d, SecurityException -> 0x0096, all -> 0x008f }
            goto L_0x0020
        L_0x003d:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0041:
            boolean r2 = r5.d     // Catch:{ all -> 0x0094 }
            if (r2 == 0) goto L_0x0057
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0094 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0094 }
            r5.c(r0)     // Catch:{ all -> 0x0094 }
        L_0x0057:
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ IOException -> 0x005d }
            goto L_0x0025
        L_0x005d:
            r0 = move-exception
            goto L_0x0025
        L_0x005f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0063:
            boolean r2 = r5.d     // Catch:{ all -> 0x0094 }
            if (r2 == 0) goto L_0x0079
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0094 }
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)     // Catch:{ all -> 0x0094 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x0094 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0094 }
            r5.c(r0)     // Catch:{ all -> 0x0094 }
        L_0x0079:
            if (r1 == 0) goto L_0x0025
            r1.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x0025
        L_0x007f:
            r0 = move-exception
            goto L_0x0025
        L_0x0081:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0085:
            if (r1 == 0) goto L_0x008a
            r1.close()     // Catch:{ IOException -> 0x008b }
        L_0x008a:
            throw r0
        L_0x008b:
            r1 = move-exception
            goto L_0x008a
        L_0x008d:
            r0 = move-exception
            goto L_0x0025
        L_0x008f:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0085
        L_0x0094:
            r0 = move-exception
            goto L_0x0085
        L_0x0096:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0063
        L_0x009b:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.j.a(java.lang.String, java.lang.Class, a.a.l):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00e9, code lost:
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x00f9, code lost:
        c("DEBUG: " + r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0113, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0114, code lost:
        r1 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00cf A[Catch:{ all -> 0x0119 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e3 A[SYNTHETIC, Splitter:B:56:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00ef A[SYNTHETIC, Splitter:B:64:0x00ef] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0113 A[ExcHandler: Exception (e java.lang.Exception), PHI: r4 
      PHI: (r4v5 boolean) = (r4v7 boolean), (r4v7 boolean), (r4v10 boolean), (r4v10 boolean) binds: [B:56:0x00e3, B:57:?, B:44:0x00be, B:45:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:44:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:98:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.lang.String r9, java.lang.Class r10, a.a.l r11) {
        /*
            r8 = this;
            r2 = 0
            java.lang.ClassLoader r0 = b()     // Catch:{ Exception -> 0x00f3 }
            if (r0 != 0) goto L_0x000b
            java.lang.ClassLoader r0 = r10.getClassLoader()     // Catch:{ Exception -> 0x00f3 }
        L_0x000b:
            if (r0 == 0) goto L_0x003d
            a.a.aa r1 = new a.a.aa     // Catch:{ Exception -> 0x00f3 }
            r1.<init>(r0, r9)     // Catch:{ Exception -> 0x00f3 }
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r1)     // Catch:{ Exception -> 0x00f3 }
            java.net.URL[] r0 = (java.net.URL[]) r0     // Catch:{ Exception -> 0x00f3 }
        L_0x0018:
            if (r0 == 0) goto L_0x012d
            r1 = r2
        L_0x001b:
            int r3 = r0.length     // Catch:{ Exception -> 0x00f3 }
            if (r1 < r3) goto L_0x0049
            r0 = r2
        L_0x001f:
            if (r0 != 0) goto L_0x003c
            boolean r0 = r8.d
            if (r0 == 0) goto L_0x002a
            java.lang.String r0 = "DEBUG: !anyLoaded"
            r8.c(r0)
        L_0x002a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "/"
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r9)
            java.lang.String r0 = r0.toString()
            r8.a(r0, r10, r11)
        L_0x003c:
            return
        L_0x003d:
            a.a.z r0 = new a.a.z     // Catch:{ Exception -> 0x00f3 }
            r0.<init>(r9)     // Catch:{ Exception -> 0x00f3 }
            java.lang.Object r0 = java.security.AccessController.doPrivileged(r0)     // Catch:{ Exception -> 0x00f3 }
            java.net.URL[] r0 = (java.net.URL[]) r0     // Catch:{ Exception -> 0x00f3 }
            goto L_0x0018
        L_0x0049:
            r3 = r0[r1]     // Catch:{ Exception -> 0x00f3 }
            r4 = 0
            boolean r5 = r8.d     // Catch:{ Exception -> 0x00f3 }
            if (r5 == 0) goto L_0x0062
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r6 = "DEBUG: URL "
            r5.<init>(r6)     // Catch:{ Exception -> 0x00f3 }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x00f3 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00f3 }
            r8.c(r5)     // Catch:{ Exception -> 0x00f3 }
        L_0x0062:
            java.io.InputStream r4 = a(r3)     // Catch:{ IOException -> 0x0123, SecurityException -> 0x00c6, all -> 0x00eb }
            if (r4 == 0) goto L_0x008a
            r11.a(r4)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            r2 = 1
            boolean r5 = r8.d     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            if (r5 == 0) goto L_0x0082
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            java.lang.String r6 = "DEBUG: successfully loaded resource: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            r8.c(r3)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
        L_0x0082:
            if (r4 == 0) goto L_0x0087
            r4.close()     // Catch:{ IOException -> 0x0110 }
        L_0x0087:
            int r1 = r1 + 1
            goto L_0x001b
        L_0x008a:
            boolean r5 = r8.d     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            if (r5 == 0) goto L_0x0082
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            java.lang.String r6 = "DEBUG: not loading resource: "
            r5.<init>(r6)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            r8.c(r3)     // Catch:{ IOException -> 0x00a1, SecurityException -> 0x011d, all -> 0x0116 }
            goto L_0x0082
        L_0x00a1:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
        L_0x00a6:
            boolean r5 = r8.d     // Catch:{ all -> 0x0119 }
            if (r5 == 0) goto L_0x00bc
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0119 }
            java.lang.String r6 = "DEBUG: "
            r5.<init>(r6)     // Catch:{ all -> 0x0119 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x0119 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0119 }
            r8.c(r2)     // Catch:{ all -> 0x0119 }
        L_0x00bc:
            if (r3 == 0) goto L_0x012a
            r3.close()     // Catch:{ IOException -> 0x00c3, Exception -> 0x0113 }
            r2 = r4
            goto L_0x0087
        L_0x00c3:
            r2 = move-exception
            r2 = r4
            goto L_0x0087
        L_0x00c6:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
        L_0x00cb:
            boolean r5 = r8.d     // Catch:{ all -> 0x0119 }
            if (r5 == 0) goto L_0x00e1
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0119 }
            java.lang.String r6 = "DEBUG: "
            r5.<init>(r6)     // Catch:{ all -> 0x0119 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ all -> 0x0119 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0119 }
            r8.c(r2)     // Catch:{ all -> 0x0119 }
        L_0x00e1:
            if (r3 == 0) goto L_0x012a
            r3.close()     // Catch:{ IOException -> 0x00e8, Exception -> 0x0113 }
            r2 = r4
            goto L_0x0087
        L_0x00e8:
            r2 = move-exception
            r2 = r4
            goto L_0x0087
        L_0x00eb:
            r0 = move-exception
            r1 = r4
        L_0x00ed:
            if (r1 == 0) goto L_0x00f2
            r1.close()     // Catch:{ IOException -> 0x010e }
        L_0x00f2:
            throw r0     // Catch:{ Exception -> 0x00f3 }
        L_0x00f3:
            r0 = move-exception
            r1 = r2
        L_0x00f5:
            boolean r2 = r8.d
            if (r2 == 0) goto L_0x010b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "DEBUG: "
            r2.<init>(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r8.c(r0)
        L_0x010b:
            r0 = r1
            goto L_0x001f
        L_0x010e:
            r1 = move-exception
            goto L_0x00f2
        L_0x0110:
            r3 = move-exception
            goto L_0x0087
        L_0x0113:
            r0 = move-exception
            r1 = r4
            goto L_0x00f5
        L_0x0116:
            r0 = move-exception
            r1 = r4
            goto L_0x00ed
        L_0x0119:
            r0 = move-exception
            r1 = r3
            r2 = r4
            goto L_0x00ed
        L_0x011d:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
            goto L_0x00cb
        L_0x0123:
            r3 = move-exception
            r7 = r3
            r3 = r4
            r4 = r2
            r2 = r7
            goto L_0x00a6
        L_0x012a:
            r2 = r4
            goto L_0x0087
        L_0x012d:
            r0 = r2
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.j.b(java.lang.String, java.lang.Class, a.a.l):void");
    }

    private void c(String str) {
        a().println(str);
    }

    private static ClassLoader b() {
        return (ClassLoader) AccessController.doPrivileged(new ac());
    }

    private static InputStream a(Class cls, String str) {
        try {
            return (InputStream) AccessController.doPrivileged(new ab(cls, str));
        } catch (PrivilegedActionException e2) {
            throw ((IOException) e2.getException());
        }
    }

    private static InputStream a(URL url) {
        try {
            return (InputStream) AccessController.doPrivileged(new y(url));
        } catch (PrivilegedActionException e2) {
            throw ((IOException) e2.getException());
        }
    }
}
