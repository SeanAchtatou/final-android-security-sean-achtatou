package a.a;

public class af extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private Exception f46a;

    public af() {
        initCause(null);
    }

    public af(String str) {
        super(str);
        initCause(null);
    }

    public af(String str, Exception exc) {
        super(str);
        this.f46a = exc;
        initCause(null);
    }

    public synchronized Throwable getCause() {
        return this.f46a;
    }

    /* JADX WARN: Type inference failed for: r1v8, types: [java.lang.Exception] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean a(java.lang.Exception r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            r2 = r3
        L_0x0002:
            boolean r1 = r2 instanceof a.a.af     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x000e
            r0 = r2
            a.a.af r0 = (a.a.af) r0     // Catch:{ all -> 0x0021 }
            r1 = r0
            java.lang.Exception r1 = r1.f46a     // Catch:{ all -> 0x0021 }
            if (r1 != 0) goto L_0x0019
        L_0x000e:
            boolean r1 = r2 instanceof a.a.af     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x001f
            a.a.af r2 = (a.a.af) r2     // Catch:{ all -> 0x0021 }
            r2.f46a = r4     // Catch:{ all -> 0x0021 }
            r1 = 1
        L_0x0017:
            monitor-exit(r3)
            return r1
        L_0x0019:
            a.a.af r2 = (a.a.af) r2     // Catch:{ all -> 0x0021 }
            java.lang.Exception r1 = r2.f46a     // Catch:{ all -> 0x0021 }
            r2 = r1
            goto L_0x0002
        L_0x001f:
            r1 = 0
            goto L_0x0017
        L_0x0021:
            r1 = move-exception
            monitor-exit(r3)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.af.a(java.lang.Exception):boolean");
    }

    public synchronized String toString() {
        String exc;
        exc = super.toString();
        Exception exc2 = this.f46a;
        if (exc2 != null) {
            if (exc == null) {
                exc = "";
            }
            StringBuffer stringBuffer = new StringBuffer(exc);
            Exception exc3 = exc2;
            while (exc3 != null) {
                stringBuffer.append(";\n  nested exception is:\n\t");
                if (exc3 instanceof af) {
                    af afVar = (af) exc3;
                    stringBuffer.append(super.toString());
                    exc3 = afVar.f46a;
                } else {
                    stringBuffer.append(exc3.toString());
                    exc3 = null;
                }
            }
            exc = stringBuffer.toString();
        }
        return exc;
    }
}
