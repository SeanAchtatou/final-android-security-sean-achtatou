package a.a;

import java.io.IOException;
import java.net.URL;
import java.security.PrivilegedAction;
import java.util.Enumeration;
import java.util.Vector;

final class z implements PrivilegedAction {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ String f84a;

    z(String str) {
        this.f84a = str;
    }

    public final Object run() {
        try {
            Vector vector = new Vector();
            Enumeration<URL> systemResources = ClassLoader.getSystemResources(this.f84a);
            while (systemResources != null && systemResources.hasMoreElements()) {
                URL nextElement = systemResources.nextElement();
                if (nextElement != null) {
                    vector.addElement(nextElement);
                }
            }
            if (vector.size() <= 0) {
                return null;
            }
            URL[] urlArr = new URL[vector.size()];
            vector.copyInto(urlArr);
            return urlArr;
        } catch (IOException | SecurityException e) {
            return null;
        }
    }
}
