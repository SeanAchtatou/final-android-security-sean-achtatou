package a.a;

import a.a.b.a;
import java.util.Vector;

final class u implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private e f77a = null;

    /* renamed from: b  reason: collision with root package name */
    private e f78b = null;
    private Thread c = new Thread(this, "JavaMail-EventQueue");

    public u() {
        this.c.setDaemon(true);
        this.c.start();
    }

    public final synchronized void a(a aVar, Vector vector) {
        e eVar = new e(aVar, vector);
        if (this.f77a == null) {
            this.f77a = eVar;
            this.f78b = eVar;
        } else {
            eVar.f55a = this.f77a;
            this.f77a.f56b = eVar;
            this.f77a = eVar;
        }
        notifyAll();
    }

    private synchronized e a() {
        e eVar;
        while (this.f78b == null) {
            wait();
        }
        eVar = this.f78b;
        this.f78b = eVar.f56b;
        if (this.f78b == null) {
            this.f77a = null;
        } else {
            this.f78b.f55a = null;
        }
        eVar.f55a = null;
        eVar.f56b = null;
        return eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0006 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0007 A[Catch:{ InterruptedException -> 0x0021 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r4 = this;
        L_0x0000:
            a.a.e r0 = r4.a()     // Catch:{ InterruptedException -> 0x0021 }
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            a.a.b.a r1 = r0.c     // Catch:{ InterruptedException -> 0x0021 }
            java.util.Vector r0 = r0.d     // Catch:{ InterruptedException -> 0x0021 }
            r2 = 0
        L_0x000c:
            int r3 = r0.size()     // Catch:{ InterruptedException -> 0x0021 }
            if (r2 >= r3) goto L_0x0000
            r0.elementAt(r2)     // Catch:{ Throwable -> 0x001b }
            r1.a()     // Catch:{ Throwable -> 0x001b }
        L_0x0018:
            int r2 = r2 + 1
            goto L_0x000c
        L_0x001b:
            r3 = move-exception
            boolean r3 = r3 instanceof java.lang.InterruptedException     // Catch:{ InterruptedException -> 0x0021 }
            if (r3 != 0) goto L_0x0006
            goto L_0x0018
        L_0x0021:
            r0 = move-exception
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.u.run():void");
    }
}
