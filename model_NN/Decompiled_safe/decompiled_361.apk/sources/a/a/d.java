package a.a;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;

public final class d implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f53a = 0;

    /* renamed from: b  reason: collision with root package name */
    private Hashtable f54b = null;

    public d() {
    }

    public d(s sVar) {
        this.f53a |= sVar.h;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (dVar.f53a != this.f53a) {
            return false;
        }
        if (dVar.f54b == null && this.f54b == null) {
            return true;
        }
        if (dVar.f54b == null || this.f54b == null || dVar.f54b.size() != this.f54b.size()) {
            return false;
        }
        Enumeration keys = dVar.f54b.keys();
        while (keys.hasMoreElements()) {
            if (!this.f54b.containsKey(keys.nextElement())) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = this.f53a;
        if (this.f54b != null) {
            Enumeration keys = this.f54b.keys();
            while (keys.hasMoreElements()) {
                i += ((String) keys.nextElement()).hashCode();
            }
        }
        return i;
    }

    public final Object clone() {
        d dVar;
        try {
            dVar = (d) super.clone();
        } catch (CloneNotSupportedException e) {
            dVar = null;
        }
        if (!(this.f54b == null || dVar == null)) {
            dVar.f54b = (Hashtable) this.f54b.clone();
        }
        return dVar;
    }
}
