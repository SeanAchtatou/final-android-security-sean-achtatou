package net.lazyer.GameSceneShowV4;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int isGoneWithoutAd = 2130771973;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int testing = 2130771968;
        public static final int textColor = 2130771970;
    }

    public static final class drawable {
        public static final int cover_1 = 2130837504;
        public static final int cover_2 = 2130837505;
        public static final int cover_3 = 2130837506;
        public static final int cover_4 = 2130837507;
        public static final int icon = 2130837508;
        public static final int p1 = 2130837509;
        public static final int p10 = 2130837510;
        public static final int p11 = 2130837511;
        public static final int p12 = 2130837512;
        public static final int p13 = 2130837513;
        public static final int p14 = 2130837514;
        public static final int p15 = 2130837515;
        public static final int p16 = 2130837516;
        public static final int p17 = 2130837517;
        public static final int p18 = 2130837518;
        public static final int p19 = 2130837519;
        public static final int p2 = 2130837520;
        public static final int p20 = 2130837521;
        public static final int p21 = 2130837522;
        public static final int p22 = 2130837523;
        public static final int p23 = 2130837524;
        public static final int p24 = 2130837525;
        public static final int p25 = 2130837526;
        public static final int p26 = 2130837527;
        public static final int p27 = 2130837528;
        public static final int p28 = 2130837529;
        public static final int p29 = 2130837530;
        public static final int p3 = 2130837531;
        public static final int p30 = 2130837532;
        public static final int p31 = 2130837533;
        public static final int p32 = 2130837534;
        public static final int p33 = 2130837535;
        public static final int p34 = 2130837536;
        public static final int p35 = 2130837537;
        public static final int p36 = 2130837538;
        public static final int p37 = 2130837539;
        public static final int p38 = 2130837540;
        public static final int p39 = 2130837541;
        public static final int p4 = 2130837542;
        public static final int p40 = 2130837543;
        public static final int p5 = 2130837544;
        public static final int p6 = 2130837545;
        public static final int p7 = 2130837546;
        public static final int p8 = 2130837547;
        public static final int p9 = 2130837548;
    }

    public static final class id {
        public static final int ItemImage = 2131034114;
        public static final int ItemText = 2131034115;
        public static final int ad = 2131034112;
        public static final int gallery = 2131034113;
        public static final int gridview = 2131034116;
    }

    public static final class layout {
        public static final int image_view = 2130903040;
        public static final int list_item = 2130903041;
        public static final int stage_list = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2130968576;
        public static final int no_way_to_share_image = 2130968589;
        public static final int ok = 2130968586;
        public static final int save_failed = 2130968584;
        public static final int save_image = 2130968581;
        public static final int save_ing = 2130968582;
        public static final int save_result = 2130968585;
        public static final int save_succ = 2130968583;
        public static final int save_to_gallery = 2130968578;
        public static final int set_wallpaper = 2130968579;
        public static final int set_wallpaper_ing = 2130968580;
        public static final int share = 2130968587;
        public static final int share_via = 2130968588;
        public static final int vol = 2130968577;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.testing, R.attr.backgroundColor, R.attr.textColor, R.attr.keywords, R.attr.refreshInterval, R.attr.isGoneWithoutAd};
        public static final int com_admob_android_ads_AdView_backgroundColor = 1;
        public static final int com_admob_android_ads_AdView_isGoneWithoutAd = 5;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_testing = 0;
        public static final int com_admob_android_ads_AdView_textColor = 2;
    }
}
