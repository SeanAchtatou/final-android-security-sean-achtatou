package net.lazyer.GameSceneShowV4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class IndexView extends Activity {
    private GridView gridview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.stage_list);
        this.gridview = (GridView) findViewById(R.id.gridview);
        int length = Config.mImageCategories.length;
        ArrayList<HashMap<String, Object>> listItem = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("ItemImage", Integer.valueOf(Config.mCoverImages[i]));
            map.put("ItemText", String.valueOf(getString(R.string.vol)) + (i + 1));
            listItem.add(map);
        }
        this.gridview.setAdapter((ListAdapter) new SimpleAdapter(this, listItem, R.layout.list_item, new String[]{"ItemImage", "ItemText"}, new int[]{R.id.ItemImage, R.id.ItemText}));
        this.gridview.setOnItemClickListener(new ItemClickListener());
    }

    class ItemClickListener implements AdapterView.OnItemClickListener {
        ItemClickListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            bundle.putInt("GalleryID", position);
            intent.putExtras(bundle);
            intent.setClass(IndexView.this, GalleryView.class);
            IndexView.this.startActivity(intent);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }
}
