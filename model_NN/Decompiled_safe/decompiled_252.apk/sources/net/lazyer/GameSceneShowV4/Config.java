package net.lazyer.GameSceneShowV4;

public class Config {
    public static int[] mCoverImages = {R.drawable.cover_1, R.drawable.cover_2, R.drawable.cover_3, R.drawable.cover_4};
    public static final int[][] mImageCategories = {mImages1, mImages2, mImages3, mImages4};
    private static final int[] mImages1 = {R.drawable.p1, R.drawable.p2, R.drawable.p3, R.drawable.p4, R.drawable.p5, R.drawable.p6, R.drawable.p7, R.drawable.p8, R.drawable.p9, R.drawable.p10};
    private static final int[] mImages2 = {R.drawable.p11, R.drawable.p12, R.drawable.p13, R.drawable.p14, R.drawable.p15, R.drawable.p16, R.drawable.p17, R.drawable.p18, R.drawable.p19, R.drawable.p20};
    private static final int[] mImages3 = {R.drawable.p21, R.drawable.p22, R.drawable.p23, R.drawable.p24, R.drawable.p25, R.drawable.p26, R.drawable.p27, R.drawable.p28, R.drawable.p29, R.drawable.p30};
    private static final int[] mImages4 = {R.drawable.p31, R.drawable.p32, R.drawable.p33, R.drawable.p34, R.drawable.p35, R.drawable.p36, R.drawable.p37, R.drawable.p38, R.drawable.p39, R.drawable.p40};
}
