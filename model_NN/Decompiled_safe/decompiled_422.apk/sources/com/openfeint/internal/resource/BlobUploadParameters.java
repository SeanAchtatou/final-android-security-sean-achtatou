package com.openfeint.internal.resource;

public class BlobUploadParameters extends Resource {
    public String AWSAccessKeyId;
    public String acl;
    public String action;
    public String key;
    public String policy;
    public String signature;

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(BlobUploadParameters.class, "blob_upload_parameters") {
            public Resource factory() {
                return new BlobUploadParameters();
            }
        };
        klass.mProperties.put("action", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((BlobUploadParameters) obj).action;
            }

            public void set(Resource obj, String val) {
                ((BlobUploadParameters) obj).action = val;
            }
        });
        klass.mProperties.put("key", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((BlobUploadParameters) obj).key;
            }

            public void set(Resource obj, String val) {
                ((BlobUploadParameters) obj).key = val;
            }
        });
        klass.mProperties.put("AWSAccessKeyId", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((BlobUploadParameters) obj).AWSAccessKeyId;
            }

            public void set(Resource obj, String val) {
                ((BlobUploadParameters) obj).AWSAccessKeyId = val;
            }
        });
        klass.mProperties.put("acl", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((BlobUploadParameters) obj).acl;
            }

            public void set(Resource obj, String val) {
                ((BlobUploadParameters) obj).acl = val;
            }
        });
        klass.mProperties.put("policy", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((BlobUploadParameters) obj).policy;
            }

            public void set(Resource obj, String val) {
                ((BlobUploadParameters) obj).policy = val;
            }
        });
        klass.mProperties.put("signature", new StringResourceProperty() {
            public String get(Resource obj) {
                return ((BlobUploadParameters) obj).signature;
            }

            public void set(Resource obj, String val) {
                ((BlobUploadParameters) obj).signature = val;
            }
        });
        return klass;
    }
}
