package com.openfeint.internal.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import com.openfeint.api.OpenFeint;
import com.openfeint.internal.ImagePicker;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util5;
import com.openfeint.internal.request.IRawRequestDelegate;
import com.openfeint.internal.ui.WebNav;
import java.util.List;
import java.util.Map;
import org.idesignco.memorymatchesfree.DataProvider;

public class IntroFlow extends WebNav {
    Bitmap cachedImage;

    /* access modifiers changed from: protected */
    public String initialContentPath() {
        String contentName = getIntent().getStringExtra("content_name");
        if (contentName != null) {
            return "intro/" + contentName;
        }
        return "intro/index";
    }

    public void onActivityResult(int requestCode, int resultCode, Intent returnedIntent) {
        if (ImagePicker.isImagePickerActivityResult(requestCode)) {
            this.cachedImage = ImagePicker.onImagePickerActivityResult(this, resultCode, 152, returnedIntent);
        }
    }

    /* access modifiers changed from: protected */
    public WebNav.ActionHandler createActionHandler(WebNav webNav) {
        return new IntroFlowActionHandler(webNav);
    }

    private class IntroFlowActionHandler extends WebNav.ActionHandler {
        public IntroFlowActionHandler(WebNav webNav) {
            super(webNav);
        }

        /* access modifiers changed from: protected */
        public void populateActionList(List<String> actionList) {
            super.populateActionList(actionList);
            actionList.add("createUser");
            actionList.add("loginUser");
            actionList.add("cacheImage");
            actionList.add("uploadImage");
            actionList.add("clearImage");
            actionList.add("decline");
            actionList.add("getEmail");
        }

        public final void createUser(final Map<String, String> options) {
            OpenFeintInternal.getInstance().createUser(options.get(DataProvider.NAME), options.get("email"), options.get("password"), options.get("password_confirmation"), new IRawRequestDelegate() {
                public void onResponse(int status, String response) {
                    IntroFlowActionHandler.this.mWebNav.executeJavascript(String.format("%s('%d', %s)", options.get("callback"), Integer.valueOf(status), response.trim()));
                }
            });
        }

        public final void loginUser(final Map<String, String> options) {
            OpenFeintInternal.getInstance().loginUser(options.get("email"), options.get("password"), options.get("user_id"), new IRawRequestDelegate() {
                public void onResponse(int status, String response) {
                    IntroFlowActionHandler.this.mWebNav.executeJavascript(String.format("%s('%d', %s)", options.get("callback"), Integer.valueOf(status), response.trim()));
                }
            });
        }

        public final void cacheImage(Map<String, String> map) {
            ImagePicker.show(IntroFlow.this);
        }

        public final void uploadImage(Map<String, String> map) {
            if (IntroFlow.this.cachedImage != null) {
                ImagePicker.compressAndUpload(IntroFlow.this.cachedImage, "/xp/users/" + OpenFeintInternal.getInstance().getCurrentUser().resourceID() + "/profile_picture", null);
            }
        }

        public final void clearImage(Map<String, String> map) {
            IntroFlow.this.cachedImage = null;
        }

        public void decline(Map<String, String> map) {
            OpenFeint.userDeclinedFeint();
            IntroFlow.this.finish();
        }

        public void getEmail(Map<String, String> options) {
            String account = Util5.getAccountNameEclair(IntroFlow.this);
            if (account != null) {
                IntroFlow.this.executeJavascript(String.format("%s('%s');", options.get("callback"), account));
            }
        }
    }
}
