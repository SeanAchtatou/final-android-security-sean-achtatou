package com.openfeint.internal.ui;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.webkit.WebView;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.Util;
import org.idesignco.memorymatchesfree.R;

public class NestedWindow extends Activity {
    private boolean mIsVisible = false;
    protected View mLogoImage;
    protected WebView mWebView;
    private final String tag = "NestedWindow";

    /* access modifiers changed from: protected */
    public int layoutResource() {
        return R.layout.of_nested_window;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.setOrientation(this);
        beforeSetContentView();
        setContentView(layoutResource());
        this.mWebView = (WebView) findViewById(R.id.web_view);
        this.mLogoImage = findViewById(R.id.of_ll_logo_image);
        OpenFeintInternal.log("NestedWindow", "onCreate");
    }

    private boolean isBigScreen() {
        Display d = getWindowManager().getDefaultDisplay();
        int width = d.getWidth();
        int height = d.getHeight();
        if (height > width && width >= 800 && height >= 1000) {
            return true;
        }
        if (width < 1000 || height < 800) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void beforeSetContentView() {
        if (isBigScreen()) {
            getWindow().requestFeature(1);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void fade(boolean toVisible) {
        int i;
        float f = 0.0f;
        if (this.mWebView != null && this.mIsVisible != toVisible) {
            this.mIsVisible = toVisible;
            float f2 = toVisible ? 0.0f : 1.0f;
            if (toVisible) {
                f = 1.0f;
            }
            AlphaAnimation anim = new AlphaAnimation(f2, f);
            if (toVisible) {
                i = 200;
            } else {
                i = 0;
            }
            anim.setDuration((long) i);
            anim.setFillAfter(true);
            this.mWebView.startAnimation(anim);
            if (this.mWebView.getVisibility() == 4) {
                this.mWebView.setVisibility(0);
                findViewById(R.id.frameLayout).setVisibility(0);
            }
        }
    }
}
