package com.openfeint.api.resource;

import com.openfeint.internal.APICallback;
import com.openfeint.internal.OpenFeintInternal;
import com.openfeint.internal.request.JSONRequest;
import com.openfeint.internal.request.OrderedArgList;
import com.openfeint.internal.resource.ArrayResourceProperty;
import com.openfeint.internal.resource.BooleanResourceProperty;
import com.openfeint.internal.resource.NestedResourceProperty;
import com.openfeint.internal.resource.Resource;
import com.openfeint.internal.resource.ResourceClass;
import com.openfeint.internal.resource.ServerException;
import com.openfeint.internal.resource.StringResourceProperty;
import java.util.List;
import org.idesignco.memorymatchesfree.DataProvider;
import org.idesignco.memorymatchesfree.R;

public class Leaderboard extends Resource {
    public boolean allowsWorseScores;
    public boolean descendingSortOrder = true;
    public List<Score> highScores;
    public Score localUserScore;
    public String name;

    public static abstract class GetScoresCB extends APICallback {
        public abstract void onSuccess(List<Score> list);
    }

    public static abstract class GetUserScoreCB extends APICallback {
        public abstract void onSuccess(Score score);
    }

    public static abstract class ListCB extends APICallback {
        public abstract void onSuccess(List<Leaderboard> list);
    }

    public Leaderboard(String resourceID) {
        setResourceID(resourceID);
    }

    public void getScores(GetScoresCB cb) {
        getScores(false, cb);
    }

    public void getFriendScores(GetScoresCB cb) {
        getScores(true, cb);
    }

    private void getScores(boolean friends, GetScoresCB cb) {
        final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards/" + resourceID() + "/high_scores";
        OrderedArgList args = new OrderedArgList();
        if (friends) {
            args.put("friends_leaderboard", "true");
        }
        final boolean z = friends;
        final GetScoresCB getScoresCB = cb;
        new JSONRequest(args) {
            public boolean wantsLogin() {
                return z;
            }

            public String method() {
                return "GET";
            }

            public String path() {
                return path;
            }

            public void onSuccess(Object responseBody) {
                if (getScoresCB != null) {
                    getScoresCB.onSuccess((List) responseBody);
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (getScoresCB != null) {
                    getScoresCB.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public void getUserScore(User forUser, final GetUserScoreCB cb) {
        final String path = "/xp/users/" + forUser.resourceID() + "/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards/" + resourceID() + "/current_score";
        new JSONRequest(new OrderedArgList()) {
            public String method() {
                return "GET";
            }

            public String path() {
                return path;
            }

            public void onResponse(int responseCode, Object responseBody) {
                if (cb == null) {
                    return;
                }
                if (200 <= responseCode && responseCode < 300) {
                    cb.onSuccess((Score) responseBody);
                } else if (404 == responseCode) {
                    cb.onSuccess(null);
                } else if (responseBody instanceof ServerException) {
                    onFailure(((ServerException) responseBody).message);
                } else {
                    onFailure(OpenFeintInternal.getRString(R.string.of_unknown_server_error));
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public static void list(final ListCB cb) {
        final String path = "/xp/games/" + OpenFeintInternal.getInstance().getAppID() + "/leaderboards";
        new JSONRequest() {
            public String method() {
                return "GET";
            }

            public String path() {
                return path;
            }

            public void onSuccess(Object responseBody) {
                if (cb != null) {
                    try {
                        cb.onSuccess((List) responseBody);
                    } catch (Exception e) {
                        onFailure(OpenFeintInternal.getRString(R.string.of_unexpected_response_format));
                    }
                }
            }

            public void onFailure(String exceptionMessage) {
                super.onFailure(exceptionMessage);
                if (cb != null) {
                    cb.onFailure(exceptionMessage);
                }
            }
        }.launch();
    }

    public Leaderboard() {
    }

    public static ResourceClass getResourceClass() {
        ResourceClass klass = new ResourceClass(Leaderboard.class, "leaderboard") {
            public Resource factory() {
                return new Leaderboard();
            }
        };
        klass.mProperties.put(DataProvider.NAME, new StringResourceProperty() {
            public String get(Resource obj) {
                return ((Leaderboard) obj).name;
            }

            public void set(Resource obj, String val) {
                ((Leaderboard) obj).name = val;
            }
        });
        klass.mProperties.put("current_user_high_score", new NestedResourceProperty(Score.class) {
            public Resource get(Resource obj) {
                return ((Leaderboard) obj).localUserScore;
            }

            public void set(Resource obj, Resource val) {
                ((Leaderboard) obj).localUserScore = (Score) val;
            }
        });
        klass.mProperties.put("descending_sort_order", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((Leaderboard) obj).descendingSortOrder;
            }

            public void set(Resource obj, boolean val) {
                ((Leaderboard) obj).descendingSortOrder = val;
            }
        });
        klass.mProperties.put("allow_posting_lower_scores", new BooleanResourceProperty() {
            public boolean get(Resource obj) {
                return ((Leaderboard) obj).allowsWorseScores;
            }

            public void set(Resource obj, boolean val) {
                ((Leaderboard) obj).allowsWorseScores = val;
            }
        });
        klass.mProperties.put("high_scores", new ArrayResourceProperty(Score.class) {
            public List<? extends Resource> get(Resource obj) {
                return ((Leaderboard) obj).highScores;
            }

            /* JADX WARN: Type inference failed for: r2v0, types: [java.util.List<com.openfeint.api.resource.Score>, java.util.List<?>] */
            /* JADX WARNING: Unknown variable types count: 1 */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void set(com.openfeint.internal.resource.Resource r1, java.util.List<?> r2) {
                /*
                    r0 = this;
                    com.openfeint.api.resource.Leaderboard r1 = (com.openfeint.api.resource.Leaderboard) r1
                    r1.highScores = r2
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.openfeint.api.resource.Leaderboard.AnonymousClass9.set(com.openfeint.internal.resource.Resource, java.util.List):void");
            }
        });
        return klass;
    }
}
