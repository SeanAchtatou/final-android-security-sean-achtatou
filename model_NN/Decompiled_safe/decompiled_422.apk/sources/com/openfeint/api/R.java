package com.openfeint.api;

public final class R {

    public static final class array {
        public static final int delaysText = 2131230720;
        public static final int layoutText = 2131230721;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771969;
        public static final int isGoneWithoutAd = 2130771973;
        public static final int keywords = 2130771971;
        public static final int refreshInterval = 2130771972;
        public static final int testing = 2130771968;
        public static final int textColor = 2130771970;
    }

    public static final class color {
        public static final int highscores_seperator = 2131034113;
        public static final int mmatches_text = 2131034114;
        public static final int of_transparent = 2131034112;
    }

    public static final class drawable {
        public static final int button = 2130837504;
        public static final int c06 = 2130837505;
        public static final int c24 = 2130837506;
        public static final int cardback = 2130837507;
        public static final int cardfront = 2130837508;
        public static final int d1 = 2130837509;
        public static final int d10 = 2130837510;
        public static final int d11 = 2130837511;
        public static final int d12 = 2130837512;
        public static final int d13 = 2130837513;
        public static final int d14 = 2130837514;
        public static final int d15 = 2130837515;
        public static final int d16 = 2130837516;
        public static final int d17 = 2130837517;
        public static final int d18 = 2130837518;
        public static final int d2 = 2130837519;
        public static final int d3 = 2130837520;
        public static final int d4 = 2130837521;
        public static final int d5 = 2130837522;
        public static final int d6 = 2130837523;
        public static final int d7 = 2130837524;
        public static final int d8 = 2130837525;
        public static final int d9 = 2130837526;
        public static final int dot1 = 2130837527;
        public static final int dot10 = 2130837528;
        public static final int dot11 = 2130837529;
        public static final int dot12 = 2130837530;
        public static final int dot13 = 2130837531;
        public static final int dot14 = 2130837532;
        public static final int dot15 = 2130837533;
        public static final int dot16 = 2130837534;
        public static final int dot17 = 2130837535;
        public static final int dot18 = 2130837536;
        public static final int dot2 = 2130837537;
        public static final int dot3 = 2130837538;
        public static final int dot4 = 2130837539;
        public static final int dot5 = 2130837540;
        public static final int dot6 = 2130837541;
        public static final int dot7 = 2130837542;
        public static final int dot8 = 2130837543;
        public static final int dot9 = 2130837544;
        public static final int frame_layout_shape = 2130837545;
        public static final int idcback = 2130837546;
        public static final int idcwin = 2130837547;
        public static final int mmbackground = 2130837548;
        public static final int mmbutton = 2130837549;
        public static final int mmbuttongrey = 2130837550;
        public static final int mmbuttonsmall = 2130837551;
        public static final int mmbuttonyellow = 2130837552;
        public static final int mmbuttonyellowsmall = 2130837553;
        public static final int mmicon = 2130837554;
        public static final int mmlogo = 2130837555;
        public static final int num1 = 2130837556;
        public static final int num10 = 2130837557;
        public static final int num11 = 2130837558;
        public static final int num12 = 2130837559;
        public static final int num13 = 2130837560;
        public static final int num14 = 2130837561;
        public static final int num15 = 2130837562;
        public static final int num16 = 2130837563;
        public static final int num17 = 2130837564;
        public static final int num18 = 2130837565;
        public static final int num2 = 2130837566;
        public static final int num3 = 2130837567;
        public static final int num4 = 2130837568;
        public static final int num5 = 2130837569;
        public static final int num6 = 2130837570;
        public static final int num7 = 2130837571;
        public static final int num8 = 2130837572;
        public static final int num9 = 2130837573;
        public static final int of_achievement_icon_frame = 2130837574;
        public static final int of_achievement_icon_locked = 2130837575;
        public static final int of_achievement_icon_unlocked = 2130837576;
        public static final int of_achievement_notification_bkg = 2130837577;
        public static final int of_achievement_notification_locked = 2130837578;
        public static final int of_feint_points_white = 2130837579;
        public static final int of_icon_dashboard_exit = 2130837580;
        public static final int of_icon_dashboard_home = 2130837581;
        public static final int of_icon_dashboard_settings = 2130837582;
        public static final int of_icon_highscore_notification = 2130837583;
        public static final int of_ll_logo = 2130837584;
        public static final int of_native_loader = 2130837585;
        public static final int of_native_loader_frame = 2130837586;
        public static final int of_native_loader_leaf = 2130837587;
        public static final int of_native_loader_progress = 2130837588;
        public static final int of_native_loader_progress_01 = 2130837589;
        public static final int of_native_loader_progress_02 = 2130837590;
        public static final int of_native_loader_progress_03 = 2130837591;
        public static final int of_native_loader_progress_04 = 2130837592;
        public static final int of_native_loader_progress_05 = 2130837593;
        public static final int of_native_loader_progress_06 = 2130837594;
        public static final int of_native_loader_progress_07 = 2130837595;
        public static final int of_native_loader_progress_08 = 2130837596;
        public static final int of_native_loader_progress_09 = 2130837597;
        public static final int of_native_loader_progress_10 = 2130837598;
        public static final int of_native_loader_progress_11 = 2130837599;
        public static final int of_native_loader_progress_12 = 2130837600;
        public static final int of_notification_bkg = 2130837601;
        public static final int s15 = 2130837602;
        public static final int s6 = 2130837603;
    }

    public static final class id {
        public static final int Button01 = 2131361831;
        public static final int FrameLayout01 = 2131361884;
        public static final int ImageView01 = 2131361870;
        public static final int ImageView02 = 2131361876;
        public static final int ImageView03 = 2131361875;
        public static final int ImageView04 = 2131361873;
        public static final int ImageView05 = 2131361872;
        public static final int ImageView06 = 2131361879;
        public static final int ImageView07 = 2131361880;
        public static final int LinearLayout1 = 2131361883;
        public static final int btn00 = 2131361802;
        public static final int btn01 = 2131361803;
        public static final int btn02 = 2131361804;
        public static final int btn03 = 2131361805;
        public static final int btn04 = 2131361807;
        public static final int btn05 = 2131361808;
        public static final int btn06 = 2131361809;
        public static final int btn07 = 2131361810;
        public static final int btn08 = 2131361812;
        public static final int btn09 = 2131361813;
        public static final int btn10 = 2131361814;
        public static final int btn11 = 2131361815;
        public static final int btn12 = 2131361817;
        public static final int btn13 = 2131361818;
        public static final int btn14 = 2131361819;
        public static final int btn15 = 2131361820;
        public static final int btn16 = 2131361821;
        public static final int btn17 = 2131361822;
        public static final int btn18 = 2131361823;
        public static final int btn19 = 2131361824;
        public static final int btnCancel = 2131361797;
        public static final int btnSave = 2131361796;
        public static final int button1 = 2131361828;
        public static final int button2 = 2131361832;
        public static final int button3 = 2131361830;
        public static final int button4 = 2131361829;
        public static final int chkNewGame = 2131361839;
        public static final int chkWebUpload = 2131361840;
        public static final int editName = 2131361837;
        public static final int exit_feint = 2131361888;
        public static final int firstPlay = 2131361794;
        public static final int frameLayout = 2131361850;
        public static final int frameLayout1 = 2131361859;
        public static final int home = 2131361886;
        public static final int imgPreview1 = 2131361869;
        public static final int layoutAd = 2131361798;
        public static final int linearLayout0 = 2131361792;
        public static final int linearLayout1 = 2131361795;
        public static final int linearLayout2 = 2131361838;
        public static final int linearLayout3 = 2131361793;
        public static final int linearLayout4 = 2131361858;
        public static final int linlayColors = 2131361878;
        public static final int linlayDefault = 2131361868;
        public static final int linlayDots = 2131361871;
        public static final int linlayNumDots = 2131361874;
        public static final int linlayShapes = 2131361877;
        public static final int mainLayout = 2131361825;
        public static final int mmlogo = 2131361826;
        public static final int nested_window_root = 2131361849;
        public static final int newsline = 2131361834;
        public static final int of_achievement_icon = 2131361842;
        public static final int of_achievement_icon_frame = 2131361843;
        public static final int of_achievement_notification = 2131361841;
        public static final int of_achievement_progress_icon = 2131361847;
        public static final int of_achievement_score = 2131361845;
        public static final int of_achievement_score_icon = 2131361846;
        public static final int of_achievement_text = 2131361844;
        public static final int of_icon = 2131361853;
        public static final int of_ll_logo_image = 2131361852;
        public static final int of_text = 2131361854;
        public static final int of_text1 = 2131361855;
        public static final int of_text2 = 2131361856;
        public static final int progress = 2131361848;
        public static final int radioColors = 2131361867;
        public static final int radioDefault = 2131361863;
        public static final int radioDots = 2131361864;
        public static final int radioNumDots = 2131361865;
        public static final int radioShapes = 2131361866;
        public static final int rdgCardSet = 2131361862;
        public static final int scrollView1 = 2131361857;
        public static final int settings = 2131361887;
        public static final int spinner = 2131361885;
        public static final int spinner1 = 2131361861;
        public static final int table = 2131361800;
        public static final int tblRow0 = 2131361801;
        public static final int tblRow1 = 2131361806;
        public static final int tblRow2 = 2131361811;
        public static final int tblRow3 = 2131361816;
        public static final int tbtnSound = 2131361881;
        public static final int textLayout = 2131361860;
        public static final int textView1 = 2131361836;
        public static final int toggleButton = 2131361882;
        public static final int txtSeconds = 2131361799;
        public static final int txtWinMessage = 2131361835;
        public static final int view0 = 2131361827;
        public static final int view1 = 2131361833;
        public static final int web_view = 2131361851;
    }

    public static final class layout {
        public static final int firstplay = 2130903040;
        public static final int game4x4 = 2130903041;
        public static final int game4x5 = 2130903042;
        public static final int highscores = 2130903043;
        public static final int main = 2130903044;
        public static final int newscore = 2130903045;
        public static final int of_achievement_notification = 2130903046;
        public static final int of_native_loader = 2130903047;
        public static final int of_nested_window = 2130903048;
        public static final int of_simple_notification = 2130903049;
        public static final int of_two_line_notification = 2130903050;
        public static final int options = 2130903051;
        public static final int spinner_layout = 2130903052;
    }

    public static final class menu {
        public static final int of_dashboard = 2131296256;
    }

    public static final class raw {
        public static final int buttonclick = 2130968576;
        public static final int cardclick = 2130968577;
        public static final int winner = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131099692;
        public static final int cancel = 2131099708;
        public static final int cardSetup = 2131099729;
        public static final int clearScores = 2131099725;
        public static final int colors = 2131099719;
        public static final int confirmClear = 2131099731;
        public static final int defaultName = 2131099711;
        public static final int defaultSet = 2131099714;
        public static final int dots = 2131099717;
        public static final int enter_name = 2131099710;
        public static final int firstNew = 2131099723;
        public static final int fivebysix = 2131099705;
        public static final int fourbyfive = 2131099704;
        public static final int fours = 2131099703;
        public static final int getFull = 2131099736;
        public static final int hello = 2131099691;
        public static final int main_menu = 2131099694;
        public static final int need_openfeint = 2131099727;
        public static final int new_game = 2131099693;
        public static final int new_game_question = 2131099701;
        public static final int new_record = 2131099709;
        public static final int newsline_url = 2131099730;
        public static final int no = 2131099700;
        public static final int nothing = 2131099695;
        public static final int numDots = 2131099715;
        public static final int of_achievement_load_null = 2131099656;
        public static final int of_achievement_unlock_null = 2131099655;
        public static final int of_achievement_unlocked = 2131099671;
        public static final int of_banned_dialog = 2131099684;
        public static final int of_bitmap_decode_error = 2131099673;
        public static final int of_cancel = 2131099667;
        public static final int of_cant_compress_blob = 2131099669;
        public static final int of_crash_report_query = 2131099681;
        public static final int of_device = 2131099662;
        public static final int of_error_parsing_error_message = 2131099675;
        public static final int of_exit_feint = 2131099686;
        public static final int of_file_not_found = 2131099674;
        public static final int of_home = 2131099683;
        public static final int of_id_cannot_be_null = 2131099650;
        public static final int of_io_exception_on_download = 2131099668;
        public static final int of_ioexception_reading_body = 2131099677;
        public static final int of_key_cannot_be_null = 2131099648;
        public static final int of_loading_feint = 2131099663;
        public static final int of_low_memory_profile_pic = 2131099658;
        public static final int of_malformed_request_error = 2131099690;
        public static final int of_name_cannot_be_null = 2131099651;
        public static final int of_no = 2131099664;
        public static final int of_no_blob = 2131099670;
        public static final int of_no_video = 2131099682;
        public static final int of_nodisk = 2131099660;
        public static final int of_now_logged_in_as_format = 2131099679;
        public static final int of_null_icon_url = 2131099654;
        public static final int of_offline_notification = 2131099687;
        public static final int of_offline_notification_line2 = 2131099688;
        public static final int of_ok = 2131099666;
        public static final int of_profile_pic_changed = 2131099680;
        public static final int of_profile_picture_download_failed = 2131099659;
        public static final int of_profile_url_null = 2131099657;
        public static final int of_score_submitted_notification = 2131099689;
        public static final int of_sdcard = 2131099661;
        public static final int of_secret_cannot_be_null = 2131099649;
        public static final int of_server_error_code_format = 2131099676;
        public static final int of_settings = 2131099685;
        public static final int of_switched_accounts = 2131099678;
        public static final int of_timeout = 2131099672;
        public static final int of_unexpected_response_format = 2131099652;
        public static final int of_unknown_server_error = 2131099653;
        public static final int of_yes = 2131099665;
        public static final int options = 2131099702;
        public static final int paidOnly = 2131099734;
        public static final int paidUrl = 2131099735;
        public static final int quit = 2131099696;
        public static final int returnStr = 2131099724;
        public static final int save = 2131099707;
        public static final int scores = 2131099713;
        public static final int seconds = 2131099698;
        public static final int shapes = 2131099718;
        public static final int sharedID = 2131099737;
        public static final int sharedID_Name = 2131099738;
        public static final int sixes = 2131099706;
        public static final int soundOff = 2131099733;
        public static final int soundOn = 2131099732;
        public static final int spinner_text = 2131099728;
        public static final int timer = 2131099716;
        public static final int timerOff = 2131099721;
        public static final int timerOn = 2131099720;
        public static final int web = 2131099726;
        public static final int webUpload = 2131099712;
        public static final int win = 2131099697;
        public static final int win_normal = 2131099722;
        public static final int yes = 2131099699;
    }

    public static final class style {
        public static final int Font = 2131165190;
        public static final int LargeFont = 2131165188;
        public static final int OFLoading = 2131165184;
        public static final int OFNestedWindow = 2131165185;
        public static final int SmallFont = 2131165189;
        public static final int buttons = 2131165186;
        public static final int cards = 2131165187;
    }

    public static final class styleable {
        public static final int[] com_zestadz_android_ZestadzAd = {org.idesignco.memorymatchesfree.R.attr.testing, org.idesignco.memorymatchesfree.R.attr.backgroundColor, org.idesignco.memorymatchesfree.R.attr.textColor, org.idesignco.memorymatchesfree.R.attr.keywords, org.idesignco.memorymatchesfree.R.attr.refreshInterval, org.idesignco.memorymatchesfree.R.attr.isGoneWithoutAd};
        public static final int com_zestadz_android_ZestadzAd_backgroundColor = 1;
        public static final int com_zestadz_android_ZestadzAd_isGoneWithoutAd = 5;
        public static final int com_zestadz_android_ZestadzAd_keywords = 3;
        public static final int com_zestadz_android_ZestadzAd_refreshInterval = 4;
        public static final int com_zestadz_android_ZestadzAd_testing = 0;
        public static final int com_zestadz_android_ZestadzAd_textColor = 2;
    }
}
