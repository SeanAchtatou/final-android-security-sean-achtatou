package com.adwhirl;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.adwhirl.util.AdWhirlUtil;

public class AdWhirlActivity extends Activity {
    public static final int MSG_ONDESTROY = 3;
    public static final int MSG_ONPAUSE = 1;
    public static final int MSG_ONRESUME = 2;
    public static boolean consoleSpam = false;

    public void onPause() {
        super.onPause();
        if (consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "System OnPause: System event dispatched!");
        }
        doMessageAdWhirl(1);
    }

    public void onDestroy() {
        super.onDestroy();
        if (consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "System OnDestroy: System event dispatched!");
        }
        doMessageAdWhirl(3);
    }

    public void onResume() {
        super.onResume();
        if (consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "System OnResume: System event dispatched!");
        }
        doMessageAdWhirl(2);
    }

    private void doMessageAdWhirl(int message) {
        ViewGroup vg = (ViewGroup) getWindow().getDecorView().getRootView();
        int c = vg.getChildCount();
        if (consoleSpam) {
            Log.i(AdWhirlUtil.ADWHIRL, "System Event: Dispatch message to all AdWhirlLayouts.");
        }
        for (int k = 0; k < c; k++) {
            messageViewsInGroup(vg, message);
        }
    }

    private void messageViewsInGroup(ViewGroup group, int message) {
        int cnt = group.getChildCount();
        for (int k = 0; k < cnt; k++) {
            View v = group.getChildAt(k);
            if (v instanceof AdWhirlLayout) {
                messageAdWhirlLayout((AdWhirlLayout) v, message);
            } else if (v instanceof ViewGroup) {
                messageViewsInGroup((ViewGroup) v, message);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void messageAdWhirlLayout(AdWhirlLayout layout, int message) {
        String tmp;
        if (consoleSpam) {
            if (message == 1) {
                tmp = "ONPAUSE";
            } else if (message == 2) {
                tmp = "ONRESUME";
            } else if (message == 3) {
                tmp = "ONDESTROY";
            } else {
                tmp = "Unknown Message:" + message;
            }
            Log.i(AdWhirlUtil.ADWHIRL, "System Event: Dispatching message to AdWhirlLayout:" + tmp);
        }
        layout.handleActivityMessage(message, isFinishing());
    }
}
