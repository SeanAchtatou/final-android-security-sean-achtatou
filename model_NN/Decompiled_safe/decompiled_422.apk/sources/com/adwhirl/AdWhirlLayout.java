package com.adwhirl;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.adwhirl.adapters.AdWhirlAdapter;
import com.adwhirl.obj.Custom;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class AdWhirlLayout extends RelativeLayout {
    public static final String ADWHIRL_KEY = "ADWHIRL_KEY";
    public static boolean consoleSpam = false;
    public static final String legacyStr = "*********************************************************\n* Warning - Adwhirl Layout being created by an Activity *\n* that is not an AdWhirlActivity subclass!              *\n*                                                       *\n*              Switching to legacy mode .               *\n*                                                       *\n*  Ad rotation should be explicitly suspended/resumed   *\n*  using the pause/unpause methods as reqired by the    *\n*  Activity.                                            *\n*********************************************************\n";
    public static boolean warnLegacy = true;
    public Ration activeRation;
    public WeakReference<Activity> activityReference;
    public AdWhirlInterface adWhirlInterface;
    public AdWhirlManager adWhirlManager;
    public Custom custom;
    public Extra extra;
    public final Handler handler = new Handler();
    /* access modifiers changed from: private */
    public boolean hasWindow;
    private boolean isDestroyed = false;
    /* access modifiers changed from: private */
    public boolean isScheduled;
    private boolean isStarting = false;
    private String keyAdWhirl;
    public boolean legacyMode = false;
    private int maxHeight;
    private int maxWidth;
    public Ration nextRation;
    public ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    public WeakReference<RelativeLayout> superViewReference;

    public interface AdWhirlInterface {
        void adWhirlGeneric();
    }

    public void setMaxWidth(int width) {
        this.maxWidth = width;
    }

    public void setMaxHeight(int height) {
        this.maxHeight = height;
    }

    public AdWhirlLayout(Activity context, String keyAdWhirl2) {
        super(context);
        init(context, keyAdWhirl2);
    }

    public AdWhirlLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init((Activity) context, getAdWhirlKey(context));
        this.isStarting = true;
    }

    /* access modifiers changed from: protected */
    public String getAdWhirlKey(Context context) {
        String packageName = context.getPackageName();
        String activityName = context.getClass().getName();
        PackageManager pm = context.getPackageManager();
        try {
            Bundle bundle = pm.getActivityInfo(new ComponentName(packageName, activityName), 128).metaData;
            if (bundle != null) {
                return bundle.getString(ADWHIRL_KEY);
            }
            try {
                Bundle bundle2 = pm.getApplicationInfo(packageName, 128).metaData;
                if (bundle2 != null) {
                    return bundle2.getString(ADWHIRL_KEY);
                }
                return null;
            } catch (PackageManager.NameNotFoundException e) {
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Activity context, String keyAdWhirl2) {
        this.activityReference = new WeakReference<>(context);
        this.superViewReference = new WeakReference<>(this);
        this.keyAdWhirl = keyAdWhirl2;
        this.hasWindow = true;
        this.isScheduled = true;
        this.scheduler.schedule(new InitRunnable(this, keyAdWhirl2), 0, TimeUnit.SECONDS);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        this.maxWidth = 0;
        this.maxHeight = 0;
        if (!(context instanceof AdWhirlActivity)) {
            if (warnLegacy) {
                Log.e(AdWhirlUtil.ADWHIRL, legacyStr);
            }
            this.legacyMode = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (this.maxWidth > 0 && widthSize > this.maxWidth) {
            widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxWidth, Integer.MIN_VALUE);
        }
        if (this.maxHeight > 0 && heightSize > this.maxHeight) {
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.maxHeight, Integer.MIN_VALUE);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        if (this.legacyMode) {
            if (visibility == 0) {
                this.hasWindow = true;
                if (!this.isScheduled || this.scheduler == null) {
                    if (this.scheduler == null) {
                        this.scheduler = Executors.newScheduledThreadPool(1);
                        if (consoleSpam) {
                            Log.w(AdWhirlUtil.ADWHIRL, "Visibility: scheduler created.");
                        }
                    }
                    this.isScheduled = true;
                    if (this.extra != null) {
                        rotateThreadedNow();
                    } else {
                        this.scheduler.schedule(new InitRunnable(this, this.keyAdWhirl), 0, TimeUnit.SECONDS);
                    }
                }
            } else {
                this.hasWindow = false;
                this.isDestroyed = true;
                if (this.scheduler != null) {
                    this.scheduler.shutdownNow();
                    this.scheduler = null;
                    if (consoleSpam) {
                        Log.w(AdWhirlUtil.ADWHIRL, "Visibility(hide): scheduler destroyed.");
                    }
                } else if (consoleSpam) {
                    Log.w(AdWhirlUtil.ADWHIRL, "Visibility(hide): no scheduler.");
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void rotateAd() {
        if (!this.hasWindow) {
            if (consoleSpam) {
                Log.w(AdWhirlUtil.ADWHIRL, "RotateAd() - AdWhirlLayout paused. Exiting.");
            }
            this.isScheduled = false;
            return;
        }
        Log.i(AdWhirlUtil.ADWHIRL, "Rotating Ad");
        this.nextRation = this.adWhirlManager.getRation();
        this.handler.post(new HandleAdRunnable(this));
    }

    /* access modifiers changed from: private */
    public void handleAd() {
        if (this.nextRation == null) {
            Log.e(AdWhirlUtil.ADWHIRL, "nextRation is null!");
            rotateThreadedDelayed();
            return;
        }
        Log.d(AdWhirlUtil.ADWHIRL, String.format("Showing ad:\n\tnid: %s\n\tname: %s\n\ttype: %d\n\tkey: %s\n\tkey2: %s", this.nextRation.nid, this.nextRation.name, Integer.valueOf(this.nextRation.type), this.nextRation.key, this.nextRation.key2));
        try {
            AdWhirlAdapter.handle(this, this.nextRation);
        } catch (Throwable th) {
            Log.w(AdWhirlUtil.ADWHIRL, "Caught an exception in adapter:", th);
            rollover();
        }
    }

    public void rotateThreadedNow() {
        if (this.isDestroyed && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "rotateThreadedNow() called after destruction. Exiting.");
        }
        if (!this.isDestroyed && this.scheduler == null && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "rotateThreadedNow() called while not destroyed, but no scheduler? Exiting.");
        }
        if (this.isDestroyed || this.scheduler == null || !this.hasWindow) {
            this.isScheduled = false;
        } else {
            this.scheduler.schedule(new RotateAdRunnable(this), 0, TimeUnit.SECONDS);
        }
    }

    public void rotateThreadedDelayed() {
        if (this.isDestroyed && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "rotateThreadedNow() called after destruction. Exiting.");
        }
        if (!this.isDestroyed && this.scheduler == null && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "rotateThreadedNow() called while not destroyed, but no scheduler? Exiting.");
        }
        if (this.isDestroyed || this.scheduler == null || !this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        Log.d(AdWhirlUtil.ADWHIRL, "Will call rotateAd() in " + this.extra.cycleTime + " seconds");
        this.scheduler.schedule(new RotateAdRunnable(this), (long) this.extra.cycleTime, TimeUnit.SECONDS);
    }

    public void pushSubView(ViewGroup subView) {
        if (this.isDestroyed && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "SubView pushed after destruction. Exiting.");
        }
        if (!this.isDestroyed && this.scheduler == null && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "SubView Pushed while not destroyed, but no scheduler? Exiting.");
        }
        if (this.isDestroyed || this.scheduler == null || !this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        RelativeLayout superView = this.superViewReference.get();
        if (superView != null) {
            superView.removeAllViews();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            superView.addView(subView, layoutParams);
            Log.d(AdWhirlUtil.ADWHIRL, "Added subview");
            this.activeRation = this.nextRation;
            countImpression();
        }
    }

    public void rollover() {
        if (this.isDestroyed && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "Rollover called after destruction. Exiting.");
        }
        if (!this.isDestroyed && this.scheduler == null && consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "Rollover called while not destroyed, but no scheduler? Exiting.");
        }
        if (this.isDestroyed || this.scheduler == null || !this.hasWindow) {
            this.isScheduled = false;
            return;
        }
        this.nextRation = this.adWhirlManager.getRollover();
        this.handler.post(new HandleAdRunnable(this));
    }

    private void countImpression() {
        if ((this.activeRation != null && this.scheduler != null && !this.isDestroyed) || !this.hasWindow) {
            this.scheduler.schedule(new PingUrlRunnable(String.format(AdWhirlUtil.urlImpression, this.adWhirlManager.keyAdWhirl, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adWhirlManager.deviceIDHash, this.adWhirlManager.localeString, Integer.valueOf((int) AdWhirlUtil.VERSION))), 0, TimeUnit.SECONDS);
        }
    }

    private void countClick() {
        if (this.activeRation != null && this.scheduler != null && !this.isDestroyed) {
            this.scheduler.schedule(new PingUrlRunnable(String.format(AdWhirlUtil.urlClick, this.adWhirlManager.keyAdWhirl, this.activeRation.nid, Integer.valueOf(this.activeRation.type), this.adWhirlManager.deviceIDHash, this.adWhirlManager.localeString, Integer.valueOf((int) AdWhirlUtil.VERSION))), 0, TimeUnit.SECONDS);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                Log.d(AdWhirlUtil.ADWHIRL, "Intercepted ACTION_DOWN event");
                if (this.activeRation != null) {
                    countClick();
                    if (this.activeRation.type == 9) {
                        if (this.custom != null && this.custom.link != null) {
                            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(this.custom.link));
                            intent.addFlags(268435456);
                            try {
                                if (this.activityReference != null) {
                                    Activity activity = this.activityReference.get();
                                    if (activity != null) {
                                        activity.startActivity(intent);
                                        break;
                                    } else {
                                        return false;
                                    }
                                } else {
                                    return false;
                                }
                            } catch (Exception e) {
                                Log.w(AdWhirlUtil.ADWHIRL, "Could not handle click to " + this.custom.link, e);
                                break;
                            }
                        } else {
                            Log.w(AdWhirlUtil.ADWHIRL, "In onInterceptTouchEvent(), but custom or custom.link is null");
                            break;
                        }
                    }
                }
                break;
        }
        return false;
    }

    public void setAdWhirlInterface(AdWhirlInterface i) {
        this.adWhirlInterface = i;
    }

    private static class InitRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;
        private String keyAdWhirl;

        public InitRunnable(AdWhirlLayout adWhirlLayout, String keyAdWhirl2) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
            this.keyAdWhirl = keyAdWhirl2;
        }

        public void run() {
            Activity activity;
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null && (activity = adWhirlLayout.activityReference.get()) != null) {
                if (adWhirlLayout.adWhirlManager == null) {
                    adWhirlLayout.adWhirlManager = new AdWhirlManager(new WeakReference(activity.getApplicationContext()), this.keyAdWhirl);
                }
                if (!adWhirlLayout.hasWindow) {
                    adWhirlLayout.isScheduled = false;
                    return;
                }
                adWhirlLayout.adWhirlManager.fetchConfig();
                adWhirlLayout.extra = adWhirlLayout.adWhirlManager.getExtra();
                if (adWhirlLayout.extra == null) {
                    adWhirlLayout.scheduler.schedule(this, 30, TimeUnit.SECONDS);
                } else {
                    adWhirlLayout.rotateAd();
                }
            }
        }
    }

    private static class HandleAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;

        public HandleAdRunnable(AdWhirlLayout adWhirlLayout) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.handleAd();
            }
        }
    }

    public static class ViewAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;
        private ViewGroup nextView;

        public ViewAdRunnable(AdWhirlLayout adWhirlLayout, ViewGroup nextView2) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
            this.nextView = nextView2;
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.pushSubView(this.nextView);
            }
        }
    }

    private static class RotateAdRunnable implements Runnable {
        private WeakReference<AdWhirlLayout> adWhirlLayoutReference;

        public RotateAdRunnable(AdWhirlLayout adWhirlLayout) {
            this.adWhirlLayoutReference = new WeakReference<>(adWhirlLayout);
        }

        public void run() {
            AdWhirlLayout adWhirlLayout = this.adWhirlLayoutReference.get();
            if (adWhirlLayout != null) {
                adWhirlLayout.rotateAd();
            }
        }
    }

    private static class PingUrlRunnable implements Runnable {
        private String url;

        public PingUrlRunnable(String url2) {
            this.url = url2;
        }

        public void run() {
            Log.d(AdWhirlUtil.ADWHIRL, "Pinging URL: " + this.url);
            try {
                new DefaultHttpClient().execute(new HttpGet(this.url));
            } catch (ClientProtocolException e) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught ClientProtocolException in PingUrlRunnable", e);
            } catch (IOException e2) {
                Log.e(AdWhirlUtil.ADWHIRL, "Caught IOException in PingUrlRunnable", e2);
            }
        }
    }

    public void setVisibility(int visibility) {
        if (consoleSpam) {
            Log.w(AdWhirlUtil.ADWHIRL, "AdWhirlLayout - Setting visibility:" + visibility);
        }
        if (this.legacyMode) {
            onWindowVisibilityChanged(visibility);
            return;
        }
        if (visibility == 0) {
            onResume();
        } else {
            onPause(false);
        }
        super.setVisibility(visibility);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        String str = "Null Scheduler";
        if (this.scheduler != null) {
            str = this.scheduler.isShutdown() ? "No" : "Yes";
            onDestroy(false);
        }
        if (consoleSpam) {
            Log.e(AdWhirlUtil.ADWHIRL, "Adwhirl layout detaching .... is Scheduler alive? " + str + " ***");
        }
    }

    public boolean handleActivityMessage(int message, boolean finishFlag) {
        String tmp;
        boolean rv;
        if (consoleSpam) {
            Log.w("System Event", "Message Received by AdWhirlLayout!");
        }
        if (message == 1) {
            onPause(finishFlag);
            tmp = "ONPAUSE";
            rv = true;
        } else if (message == 2) {
            onResume();
            tmp = "ONRESUME";
            rv = true;
        } else if (message == 3) {
            onDestroy(finishFlag);
            tmp = "ONDESTROY";
            rv = true;
        } else {
            tmp = "Unknown Message:" + message;
            rv = false;
        }
        if (consoleSpam) {
            Log.w("System Event", "Message processed:" + tmp);
        }
        return rv;
    }

    /* access modifiers changed from: protected */
    public void onPause(boolean isFinishing) {
        if (!this.legacyMode) {
            if (consoleSpam) {
                Log.i(AdWhirlUtil.ADWHIRL, "System Event: AdWhirlLayout onPause(" + isFinishing + ")");
            }
            this.hasWindow = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!this.legacyMode) {
            if (this.isStarting) {
                this.isStarting = false;
                if (consoleSpam) {
                    Log.i(AdWhirlUtil.ADWHIRL, "System Event: AdWhirlLayout - just created onResume() ignored.");
                    return;
                }
                return;
            }
            if (consoleSpam) {
                Log.i(AdWhirlUtil.ADWHIRL, "System Event: AdWhirlLayout onResume()");
            }
            this.hasWindow = true;
            if (!this.isScheduled) {
                this.isScheduled = true;
                if (this.scheduler == null) {
                    this.scheduler = Executors.newScheduledThreadPool(1);
                    if (consoleSpam) {
                        Log.w(AdWhirlUtil.ADWHIRL, "OnResume: scheduler created.");
                    }
                }
                if (this.extra != null) {
                    rotateThreadedNow();
                } else {
                    this.scheduler.schedule(new InitRunnable(this, this.keyAdWhirl), 0, TimeUnit.SECONDS);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy(boolean isFinishing) {
        if (!this.legacyMode) {
            if (consoleSpam) {
                Log.i("System Event", "AdWhirlLayout onDestroy(" + isFinishing + ")");
            }
            this.hasWindow = false;
            this.isDestroyed = true;
            if (this.scheduler != null) {
                this.scheduler.shutdownNow();
                this.scheduler = null;
                if (consoleSpam) {
                    Log.w(AdWhirlUtil.ADWHIRL, "OnDestroy: scheduler destroyed.");
                }
            } else if (consoleSpam) {
                Log.i(AdWhirlUtil.ADWHIRL, "OnDestroy: no scheduler.");
            }
        }
    }

    public boolean isPaused() {
        return !this.hasWindow;
    }

    public void pause() {
        onPause(false);
    }

    public void unpause() {
        onResume();
    }
}
