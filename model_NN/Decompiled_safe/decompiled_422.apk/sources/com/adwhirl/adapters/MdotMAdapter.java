package com.adwhirl.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.obj.Extra;
import com.adwhirl.obj.Ration;
import com.adwhirl.util.AdWhirlUtil;
import com.mdotm.android.ads.MdotMManager;
import com.mdotm.android.ads.MdotMView;

public class MdotMAdapter extends AdWhirlAdapter implements MdotMView.MdotMActionListener {
    public MdotMAdapter(AdWhirlLayout adWhirlLayout, Ration ration) {
        super(adWhirlLayout, ration);
    }

    public void handle() {
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            try {
                MdotMManager.setPublisherId(this.ration.key);
                MdotMManager.setMediationLayerName(AdWhirlUtil.ADWHIRL);
                MdotMManager.setMediationLayerVersion(AdWhirlUtil.VERSION);
                Activity activity = adWhirlLayout.activityReference.get();
                if (activity != null) {
                    MdotMView mdotm = new MdotMView(activity, this);
                    mdotm.setListener(this);
                    Extra extra = adWhirlLayout.extra;
                    int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
                    int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
                    mdotm.setBackgroundColor(bgColor);
                    mdotm.setTextColor(fgColor);
                }
            } catch (IllegalArgumentException e) {
                adWhirlLayout.rollover();
            }
        }
    }

    public void adRequestCompletedSuccessfully(MdotMView adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "MdotM success");
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adView.setListener(null);
            adView.setVisibility(0);
            adWhirlLayout.adWhirlManager.resetRollover();
            adWhirlLayout.handler.post(new AdWhirlLayout.ViewAdRunnable(adWhirlLayout, adView));
            adWhirlLayout.rotateThreadedDelayed();
        }
    }

    public void adRequestFailed(MdotMView adView) {
        Log.d(AdWhirlUtil.ADWHIRL, "MdotM failure");
        adView.setListener(null);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) this.adWhirlLayoutReference.get();
        if (adWhirlLayout != null) {
            adWhirlLayout.rollover();
        }
    }
}
