package com.adwhirl;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.adwhirl.AdWhirlTargeting;
import com.adwhirl.adapters.AdWhirlAdapter;
import com.adwhirl.util.AdWhirlUtil;
import java.util.Arrays;
import java.util.HashSet;
import org.idesignco.memorymatchesfree.R;

public class Invoker extends Activity implements AdWhirlLayout.AdWhirlInterface {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.firstplay);
        LinearLayout layout = (LinearLayout) findViewById(R.color.of_transparent);
        if (layout == null) {
            Log.e("AdWhirl", "Layout is null!");
            return;
        }
        float density = getResources().getDisplayMetrics().density;
        int width = (int) (((float) 320) * density);
        int height = (int) (((float) 52) * density);
        AdWhirlTargeting.setAge(23);
        AdWhirlTargeting.setGender(AdWhirlTargeting.Gender.MALE);
        AdWhirlTargeting.setKeywordSet(new HashSet(Arrays.asList("online", "games", "gaming")));
        AdWhirlTargeting.setPostalCode("94123");
        AdWhirlTargeting.setTestMode(false);
        AdWhirlAdapter.setGoogleAdSenseAppName("AdWhirl Test App");
        AdWhirlAdapter.setGoogleAdSenseCompanyName("AdWhirl");
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlLayout adWhirlLayout = (AdWhirlLayout) findViewById(R.color.highscores_seperator);
        adWhirlLayout.setAdWhirlInterface(this);
        adWhirlLayout.setMaxWidth(width);
        adWhirlLayout.setMaxHeight(height);
        AdWhirlLayout adWhirlLayout2 = new AdWhirlLayout(this, "46a9e26bb1f5499ab7b00c9807ae034b");
        adWhirlLayout2.setAdWhirlInterface(this);
        adWhirlLayout2.setMaxWidth(width);
        adWhirlLayout2.setMaxHeight(height);
        RelativeLayout.LayoutParams adWhirlLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
        adWhirlLayoutParams.addRule(14);
        layout.setGravity(1);
        layout.addView(adWhirlLayout2, adWhirlLayoutParams);
        TextView textView = new TextView(this);
        textView.setText("Below AdWhirlLayout from code");
        layout.addView(textView, adWhirlLayoutParams);
        layout.setGravity(1);
        layout.invalidate();
    }

    public void adWhirlGeneric() {
        Log.e(AdWhirlUtil.ADWHIRL, "In adWhirlGeneric()");
    }
}
