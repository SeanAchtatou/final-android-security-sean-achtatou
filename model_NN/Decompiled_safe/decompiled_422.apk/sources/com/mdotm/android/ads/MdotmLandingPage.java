package com.mdotm.android.ads;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MdotmLandingPage extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(2);
        WebView wvLandingPage = new WebView(this);
        setContentView(wvLandingPage);
        wvLandingPage.getSettings().setJavaScriptEnabled(true);
        wvLandingPage.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                this.setProgress(progress * 1000);
            }
        });
        wvLandingPage.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(this, "Oh no! " + description, 0).show();
            }
        });
        wvLandingPage.loadUrl(getIntent().getExtras().getString("bStrLandingPage"));
    }
}
