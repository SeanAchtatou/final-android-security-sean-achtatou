package org.idesignco.memorymatchesfree;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

public class SimpleSoundManager {
    /* access modifiers changed from: private */
    public static String LOGTAG = "SimpleSoundManager";
    public static boolean debug = false;
    public static boolean enabled = true;
    /* access modifiers changed from: private */
    public static MediaPlayer mp = null;

    public static void play(Context ctx, int resID) {
        if (enabled) {
            mp = MediaPlayer.create(ctx.getApplicationContext(), resID);
            if (mp != null) {
                mp.setAudioStreamType(3);
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        if (SimpleSoundManager.debug) {
                            Log.i(SimpleSoundManager.LOGTAG, "Stopping sound by releasing player");
                        }
                        mp.release();
                        SimpleSoundManager.mp = null;
                    }
                });
                if (debug) {
                    Log.i(LOGTAG, "playing res: " + ctx.getResources().getResourceName(resID));
                }
                mp.start();
                return;
            }
            Log.e(LOGTAG, "Could not create media player for res: " + ctx.getResources().getResourceName(resID));
        }
    }
}
