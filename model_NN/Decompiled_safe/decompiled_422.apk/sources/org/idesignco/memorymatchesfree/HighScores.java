package org.idesignco.memorymatchesfree;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.adwhirl.AdWhirlTabActivity;
import com.flurry.android.FlurryAgent;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.ui.Dashboard;

public class HighScores extends AdWhirlTabActivity implements TabHost.TabContentFactory {
    private static final int DLG_APPROVE_OPENFEINT = 0;
    private static final String LOGTAG = "MMatches:HighScores";
    private static final int padding = 2;

    private class WebButton implements View.OnClickListener {
        private String lboardID;

        public WebButton(String leaderBoardID) {
            this.lboardID = leaderBoardID;
        }

        public void onClick(View v) {
            FlurryAgent.onEvent("Web scores clicked");
            SimpleSoundManager.play(HighScores.this, R.raw.buttonclick);
            if (!OpenFeint.isUserLoggedIn()) {
                HighScores.this.showDialog(0);
            } else {
                Dashboard.openLeaderboard(this.lboardID);
            }
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.highscores);
        setVolumeControlStream(3);
        TabHost tabHost = getTabHost();
        String[] layoutNames = getResources().getStringArray(R.array.layoutText);
        for (int i = 0; i < layoutNames.length; i++) {
            tabHost.addTab(tabHost.newTabSpec(layoutNames[i]).setIndicator(layoutNames[i]).setContent(this));
        }
        SharedPreferences data = getSharedPreferences("data", 2);
        tabHost.setCurrentTabByTag(layoutNames[data.getInt(DataProvider.LAYOUT, 0)]);
        SimpleSoundManager.enabled = data.getBoolean("sound", true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Game.FLURRY_API_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStart();
        FlurryAgent.onEndSession(this);
    }

    public View createTabContent(String layout) {
        String[] layoutNames = getResources().getStringArray(R.array.layoutText);
        int layoutPos = 0;
        int buttonHeight = (int) (50.0f * getResources().getDisplayMetrics().density);
        for (int i = 0; i < layoutNames.length; i++) {
            if (layout.equals(layoutNames[i])) {
                layoutPos = i;
            }
        }
        ScrollView scrollView = new ScrollView(this);
        TableLayout tableLayout = new TableLayout(this);
        tableLayout.setColumnStretchable(1, true);
        scrollView.addView(tableLayout);
        String[] projection = new String[2];
        String[] selectionArgs = new String[2];
        for (int i2 = 0; i2 < CardSets.setKeyNames.length; i2++) {
            projection[0] = DataProvider.NAME;
            projection[1] = DataProvider.SCORE;
            selectionArgs[0] = Integer.toString(i2);
            selectionArgs[1] = Integer.toString(layoutPos);
            Cursor c = getContentResolver().query(DataProvider.SCORES_URI, projection, "cardset=? AND layout=?", selectionArgs, null);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setOrientation(0);
            linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
            TextView textView = new TextView(this);
            textView.setText(CardSets.setHumanNames[i2]);
            textView.setTextSize(22.0f);
            textView.setTextColor((int) R.color.mmatches_text);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.weight = 1.0f;
            layoutParams.gravity = 81;
            textView.setLayoutParams(layoutParams);
            linearLayout.addView(textView);
            Button webScore = new Button(this);
            linearLayout.addView(webScore);
            String boardID = CardSets.leaderboardIDs[layoutPos][i2];
            webScore.setBackgroundResource(R.drawable.button);
            webScore.setTextColor(-16777216);
            webScore.setTypeface(Typeface.create("sans", 1));
            webScore.setTextSize(16.0f);
            webScore.setGravity(17);
            webScore.setOnClickListener(new WebButton(boardID));
            webScore.setText(R.string.web);
            webScore.setLayoutParams(new LinearLayout.LayoutParams(-2, buttonHeight));
            webScore.forceLayout();
            tableLayout.addView(linearLayout);
            while (c.moveToNext()) {
                String name = c.getString(0);
                float score = ((float) c.getLong(1)) / 1000.0f;
                TextView textView2 = new TextView(this);
                textView2.setText(name);
                textView2.setPadding(2, 2, 2, 2);
                textView2.setTextColor(R.color.mmatches_text);
                TextView textView3 = new TextView(this);
                textView3.setText(Game.formatter.format((double) score));
                textView3.setGravity(5);
                textView3.setPadding(2, 2, 2, 2);
                textView3.setTextColor(R.color.mmatches_text);
                TableRow tableRow = new TableRow(this);
                TextView textView4 = new TextView(this);
                textView4.setText("X");
                textView4.setVisibility(4);
                tableRow.addView(textView4);
                tableRow.addView(textView2);
                tableRow.addView(textView3);
                tableLayout.addView(tableRow);
            }
            c.close();
            View view = new View(this);
            view.setBackgroundColor(R.color.highscores_seperator);
            view.setLayoutParams(new ViewGroup.LayoutParams(-1, 2));
            tableLayout.addView(view);
        }
        scrollView.requestLayout();
        return scrollView;
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                AlertDialog.Builder d = new AlertDialog.Builder(this);
                d.setMessage((int) R.string.need_openfeint).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OpenFeint.userApprovedFeint();
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                return d.create();
            default:
                String msg = "Unknown dialog id: " + Integer.toString(id);
                Log.e(LOGTAG, msg);
                FlurryAgent.onError(LOGTAG, msg, "onCreateDialog");
                return null;
        }
    }
}
