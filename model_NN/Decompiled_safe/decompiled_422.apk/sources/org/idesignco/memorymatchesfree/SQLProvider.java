package org.idesignco.memorymatchesfree;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import java.util.HashMap;

public abstract class SQLProvider extends ContentProvider {
    protected static String AUTHORITY = null;
    protected static String[][] COLUMN_NAMES = null;
    protected static int[][] COLUMN_TYPES = null;
    public static String[] CONTENT_ITEM_TYPES = null;
    public static String[] CONTENT_TYPES = null;
    public static Uri CONTENT_URI = null;
    protected static String DATABASE_NAME = "provider.db";
    protected static int DATABASE_VERSION = 2;
    public static int DEBUG = 4;
    private static final String LOGTAG = "SQLProvider";
    protected static String[] TABLE_NAMES = null;
    public static final int TYPE_BLOB = 4;
    public static final int TYPE_INTEGER = 1;
    public static final int TYPE_NULL = 3;
    public static final int TYPE_REAL = 2;
    public static final int TYPE_TEXT = 0;
    public static final String _ID = "_id";
    protected static String[] defaultSort = null;
    protected static DBOpenHelper openHelper;
    private static HashMap<String, HashMap<String, String>> projectionMap = null;
    private static UriMatcher uriMatcher = null;

    protected static class DBOpenHelper extends SQLiteOpenHelper {
        public DBOpenHelper(Context context) {
            super(context, SQLProvider.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, SQLProvider.DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            if (SQLProvider.TABLE_NAMES == null) {
                throw new IllegalStateException("Table names not set");
            } else if (SQLProvider.COLUMN_NAMES == null) {
                throw new IllegalStateException("Column names not set");
            } else if (SQLProvider.COLUMN_TYPES == null) {
                throw new IllegalStateException("Column types not set");
            } else {
                int t = 0;
                while (t < SQLProvider.TABLE_NAMES.length) {
                    String tname = SQLProvider.TABLE_NAMES[t];
                    try {
                        if (SQLProvider.COLUMN_TYPES[t].length != SQLProvider.COLUMN_NAMES[t].length) {
                            throw new IllegalStateException("Column types for table '" + tname + "' length does not match the length of Column names");
                        }
                        String prefix = "CREATE TABLE " + SQLProvider.TABLE_NAMES[t] + " (" + SQLProvider._ID + " INTEGER PRIMARY KEY,";
                        String query = "";
                        for (int c = 0; c < SQLProvider.COLUMN_NAMES[t].length; c++) {
                            switch (SQLProvider.COLUMN_TYPES[t][c]) {
                                case 0:
                                    query = String.valueOf(query) + SQLProvider.COLUMN_NAMES[t][c] + " TEXT";
                                    break;
                                case 1:
                                    query = String.valueOf(query) + SQLProvider.COLUMN_NAMES[t][c] + " INTEGER";
                                    break;
                                case 2:
                                    query = String.valueOf(query) + SQLProvider.COLUMN_NAMES[t][c] + " REAL";
                                    break;
                                case 3:
                                    query = String.valueOf(query) + SQLProvider.COLUMN_NAMES[t][c] + " NULL";
                                    break;
                                case 4:
                                    query = String.valueOf(query) + SQLProvider.COLUMN_NAMES[t][c] + " BLOB";
                                    break;
                                default:
                                    throw new IllegalStateException("Invalid column type for table '" + tname + "' at column " + Integer.toString(c));
                            }
                            if (c != SQLProvider.COLUMN_NAMES[t].length - 1) {
                                query = String.valueOf(query) + ",";
                            }
                        }
                        String query2 = String.valueOf(prefix) + query + ");";
                        if (SQLProvider.DEBUG > 3) {
                            Log.v(SQLProvider.LOGTAG, "Executing query: '" + query2 + "'");
                        }
                        db.execSQL(query2);
                        t++;
                    } catch (Exception e) {
                        throw new IllegalStateException("Problem with Column Types or Column names", e);
                    }
                }
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(SQLProvider.LOGTAG, "Upgrading database '" + SQLProvider.DATABASE_NAME + "' to version " + Integer.toString(newVersion) + ", this will delete and recreate the database losing all information");
            db.execSQL("DROP TABLE *");
            onCreate(db);
        }
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int table;
        int count;
        if (DEBUG > 3) {
            Log.v(LOGTAG, "Delete uri: " + uri);
        }
        SQLiteDatabase db = openHelper.getWritableDatabase();
        int table2 = uriMatcher.match(uri);
        if (table2 % 2 == 0) {
            table = table2 / 2;
            if (table >= TABLE_NAMES.length) {
                throw new IllegalArgumentException("Unknown URI + " + uri);
            }
            count = db.delete(TABLE_NAMES[table], selection, selectionArgs);
        } else {
            table = (table2 - 1) / 2;
            if (table >= TABLE_NAMES.length || table < 0) {
                throw new IllegalArgumentException("Unknown URI + " + uri);
            }
            count = db.delete(TABLE_NAMES[table], "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
        }
        if (DEBUG > 0) {
            Log.i(LOGTAG, "Deleted " + Integer.toString(count) + " rows from table: " + TABLE_NAMES[table]);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public String getType(Uri uri) {
        if (DEBUG > 3) {
            Log.v(LOGTAG, "GetType uri: " + uri);
        }
        int table = uriMatcher.match(uri);
        if (table % 2 == 0) {
            int table2 = table / 2;
            if (table2 < TABLE_NAMES.length) {
                return CONTENT_TYPES[table2];
            }
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        int table3 = (table - 1) / 2;
        if (table3 < TABLE_NAMES.length) {
            return CONTENT_ITEM_TYPES[table3];
        }
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }

    public Uri insert(Uri uri, ContentValues values) {
        if (DEBUG > 3) {
            Log.v(LOGTAG, "Insert uri: " + uri);
        }
        int table = uriMatcher.match(uri);
        if (table % 2 == 1) {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        int table2 = table / 2;
        if (table2 >= TABLE_NAMES.length) {
            throw new IllegalArgumentException("Unknown URI: " + uri);
        } else if (values == null) {
            throw new IllegalArgumentException("Values cannot be null");
        } else {
            Uri returnUri = null;
            for (int c = 0; c < COLUMN_NAMES[table2].length; c++) {
                if (values.containsKey(COLUMN_NAMES[table2][c])) {
                    if (returnUri != null) {
                        continue;
                    } else {
                        long rowId = openHelper.getWritableDatabase().insert(TABLE_NAMES[table2], null, values);
                        if (rowId > 0) {
                            returnUri = ContentUris.withAppendedId(uri, rowId);
                            getContext().getContentResolver().notifyChange(returnUri, null);
                            if (DEBUG > 0) {
                                Log.i(LOGTAG, "Successfully inserted row: " + returnUri);
                            }
                            if (DEBUG <= 2) {
                                return returnUri;
                            }
                        } else {
                            continue;
                        }
                    }
                } else if (DEBUG > 2) {
                    Log.d(LOGTAG, "No value for '" + COLUMN_NAMES[table2][c] + "' specified for table: " + TABLE_NAMES[table2]);
                }
            }
            if (returnUri != null) {
                return returnUri;
            }
            throw new IllegalArgumentException("Values must contain atleast one value for table: " + TABLE_NAMES[table2]);
        }
    }

    public boolean onCreate() {
        openHelper = new DBOpenHelper(getContext());
        if (TABLE_NAMES == null) {
            throw new IllegalStateException("Table names not set");
        } else if (CONTENT_ITEM_TYPES == null) {
            throw new IllegalStateException("Content item types not set");
        } else if (CONTENT_TYPES == null) {
            throw new IllegalStateException("Content types not set");
        } else if (COLUMN_NAMES == null) {
            throw new IllegalStateException("Column names not set");
        } else if (COLUMN_TYPES == null) {
            throw new IllegalStateException("Column types not set");
        } else {
            if (defaultSort == null) {
                defaultSort = new String[TABLE_NAMES.length];
                for (int t = 0; t < TABLE_NAMES.length; t++) {
                    defaultSort[t] = null;
                }
            }
            projectionMap = new HashMap<>();
            uriMatcher = new UriMatcher(-1);
            if (DEBUG > 3) {
                Log.v(LOGTAG, "Creating Provider " + CONTENT_URI);
                Log.v(LOGTAG, "with " + Integer.toString(TABLE_NAMES.length) + " tables");
            }
            for (int t2 = 0; t2 < TABLE_NAMES.length; t2++) {
                uriMatcher.addURI(AUTHORITY, TABLE_NAMES[t2], t2 * 2);
                uriMatcher.addURI(AUTHORITY, String.valueOf(TABLE_NAMES[t2]) + "/#", (t2 * 2) + 1);
                HashMap<String, String> tableMap = new HashMap<>();
                tableMap.put(_ID, _ID);
                for (int c = 0; c < COLUMN_NAMES[t2].length; c++) {
                    tableMap.put(COLUMN_NAMES[t2][c], COLUMN_NAMES[t2][c]);
                }
                projectionMap.put(TABLE_NAMES[t2], tableMap);
            }
            return true;
        }
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int table;
        String orderBy;
        if (DEBUG > 3) {
            Log.v(LOGTAG, "Query uri: " + uri);
        }
        int table2 = uriMatcher.match(uri);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        if (table2 % 2 == 0) {
            table = table2 / 2;
            if (table >= TABLE_NAMES.length) {
                throw new IllegalArgumentException("Unknown URI: " + uri);
            }
            qb.setTables(TABLE_NAMES[table]);
            qb.setProjectionMap(projectionMap.get(TABLE_NAMES[table]));
        } else {
            table = (table2 - 1) / 2;
            if (table >= TABLE_NAMES.length || table < 0) {
                throw new IllegalArgumentException("Unknown URI: " + uri);
            }
            qb.setTables(TABLE_NAMES[table]);
            qb.setProjectionMap(projectionMap.get(TABLE_NAMES[table]));
            qb.appendWhere("_id=" + uri.getPathSegments().get(1));
        }
        if (TextUtils.isEmpty(sortOrder)) {
            orderBy = defaultSort[table];
        } else {
            orderBy = sortOrder;
        }
        Cursor c = qb.query(openHelper.getReadableDatabase(), projection, selection, selectionArgs, null, null, orderBy);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        if (DEBUG > 2) {
            Log.d(LOGTAG, "Query on " + TABLE_NAMES[table] + " returned " + Integer.toString(c.getCount()) + " results");
        }
        return c;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int table;
        int count;
        if (DEBUG > 3) {
            Log.v(LOGTAG, "Update uri: " + uri);
        }
        SQLiteDatabase db = openHelper.getWritableDatabase();
        int table2 = uriMatcher.match(uri);
        if (table2 % 2 == 0) {
            table = table2 / 2;
            if (table >= TABLE_NAMES.length) {
                throw new IllegalArgumentException("Unknown URI: " + uri);
            }
            count = db.update(TABLE_NAMES[table], values, selection, selectionArgs);
        } else {
            table = (table2 - 1) / 2;
            if (table >= TABLE_NAMES.length || table < 0) {
                throw new IllegalArgumentException("Unknown URI: " + uri);
            }
            count = db.update(TABLE_NAMES[table], values, "_id=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ')' : ""), selectionArgs);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        if (DEBUG > 1) {
            Log.i(LOGTAG, "Update on " + TABLE_NAMES[table] + " affecting " + Integer.toString(count) + " rows");
        }
        return count;
    }
}
