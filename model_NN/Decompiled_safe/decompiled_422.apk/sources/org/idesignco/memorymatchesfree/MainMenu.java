package org.idesignco.memorymatchesfree;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import com.adwhirl.AdWhirlActivity;
import com.adwhirl.AdWhirlManager;
import com.adwhirl.AdWhirlTargeting;
import com.flurry.android.FlurryAgent;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;

public class MainMenu extends AdWhirlActivity {
    private static final String LOGTAG = "MMatches:Main";
    public static final String sharedfile = "data";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        requestWindowFeature(1);
        setContentView((int) R.layout.main);
        AdWhirlManager.setConfigExpireTimeout(300000);
        AdWhirlTargeting.setKeywords("online games gaming");
        AdWhirlTargeting.setTestMode(false);
        WebView newsline = (WebView) findViewById(R.id.newsline);
        newsline.setBackgroundColor(0);
        try {
            newsline.loadUrl(String.valueOf(getResources().getString(R.string.newsline_url)) + getPackageManager().getPackageInfo(getPackageName(), 0).versionName + ".html");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(LOGTAG, "Unable to form Newsline URL - " + e.getLocalizedMessage());
        }
        SharedPreferences data = getSharedPreferences("data", 2);
        SimpleSoundManager.enabled = data.getBoolean("sound", true);
        OpenFeint.initialize(this, new OpenFeintSettings(Game.OFNAME, Game.OFKEY, Game.OFSECRET, Game.OFID), new OpenFeintDelegate() {
        });
        if (!data.getBoolean("dataUpdated_v13", false)) {
            updateData_v13();
        }
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Game.FLURRY_API_KEY);
    }

    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    public void newGame(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        boolean firstplay = getSharedPreferences("data", 2).getBoolean("firstplay_key", true);
        FlurryAgent.onEvent("New Game Clicked");
        if (firstplay) {
            Log.v(LOGTAG, "firstplay: true");
            startActivity(new Intent(this, FirstPlay.class));
            return;
        }
        Log.v(LOGTAG, "firstplay: false");
        startActivity(new Intent(this, Game.class));
    }

    public void scoresClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        FlurryAgent.onEvent("High Scores Clicked");
        startActivity(new Intent(this, HighScores.class));
    }

    public void optionsClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        FlurryAgent.onEvent("Options Clicked");
        startActivity(new Intent(this, options.class));
    }

    public void getFullClick(View v) {
        String uri = getString(R.string.paidUrl);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    public void quitClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        Process.killProcess(Process.myPid());
        finish();
    }

    private void updateData_v13() {
        SharedPreferences data = getSharedPreferences("data", 0);
        String[] layoutNames = getResources().getStringArray(R.array.layoutText);
        SharedPreferences.Editor editor = data.edit();
        Log.i(LOGTAG, "Updating data to version 1.3 standard with data provider");
        String[] projection = {SQLProvider._ID};
        String[] selectionArgs = new String[2];
        for (int set = 0; set < CardSets.setKeyNames.length; set++) {
            String key = "won4x4_" + CardSets.setKeyNames[set];
            boolean won4x4 = data.getBoolean(key, false);
            editor.remove(key);
            String key2 = "won6x6_" + CardSets.setKeyNames[set];
            boolean won6x6 = data.getBoolean(key2, false);
            editor.remove(key2);
            if (won4x4) {
                MMAchievements.set4x4Won(this, set);
            }
            if (won6x6) {
                MMAchievements.set6x6Won(this, set);
            }
            selectionArgs[0] = Integer.toString(set);
            for (int l = 0; l < layoutNames.length; l++) {
                for (int i = 0; i < 5; i++) {
                    String key3 = String.valueOf(layoutNames[l]) + "_" + CardSets.setKeyNames[set] + "_score" + Integer.toString(i);
                    String namekey = String.valueOf(layoutNames[l]) + "_" + CardSets.setKeyNames[set] + "_name" + Integer.toString(i);
                    if (data.getFloat(key3, -1.0f) != -1.0f) {
                        float score = data.getFloat(key3, -1.0f);
                        String name = data.getString(namekey, getString(R.string.defaultName));
                        ContentValues values = new ContentValues();
                        values.put(DataProvider.CARDSET, Integer.valueOf(set));
                        values.put(DataProvider.LAYOUT, Integer.valueOf(l));
                        values.put(DataProvider.SCORE, Long.valueOf((long) (1000.0f * score)));
                        values.put(DataProvider.NAME, name);
                        getContentResolver().insert(DataProvider.SCORES_URI, values);
                    }
                    editor.remove(key3).remove(namekey);
                }
                selectionArgs[1] = Integer.toString(l);
                Cursor c = getContentResolver().query(DataProvider.SCORES_URI, projection, "cardset=? AND layout=?", selectionArgs, null);
                while (c.getCount() > 5) {
                    c.moveToLast();
                    getContentResolver().delete(ContentUris.withAppendedId(DataProvider.SCORES_URI, c.getLong(0)), null, null);
                    c.close();
                    c = getContentResolver().query(DataProvider.SCORES_URI, projection, "cardset=? AND layout=?", selectionArgs, null);
                }
                c.close();
            }
        }
        editor.putBoolean("dataUpdated_v13", true);
        editor.commit();
    }
}
