package org.idesignco.memorymatchesfree;

import android.net.Uri;

public class DataProvider extends SQLProvider {
    public static final Uri ACHIEVEMENTS = Uri.parse("content://org.idesignco.memorymatchesfree.dataprovider/achievements");
    public static final String CARDSET = "cardset";
    public static final String LAYOUT = "layout";
    public static final String NAME = "name";
    public static final String SCORE = "score";
    public static final Uri SCORES_URI = Uri.parse("content://org.idesignco.memorymatchesfree.dataprovider/scores");
    public static final String WON4X4 = "won4x4";
    public static final String WON6X6 = "won6x6";

    static {
        DATABASE_NAME = "scores.db";
        CONTENT_URI = Uri.parse("content://org.idesignco.memorymatchesfree.dataprovider");
        AUTHORITY = "org.idesignco.memorymatchesfree.dataprovider";
        TABLE_NAMES = new String[2];
        TABLE_NAMES[0] = "scores";
        TABLE_NAMES[1] = "achievements";
        CONTENT_TYPES = new String[2];
        CONTENT_TYPES[0] = "vnd.android.cursor.dir/vnd.idesignco.memorymatches.score";
        CONTENT_TYPES[1] = "vnd.android.cursor.dir/vnd.idesignco.memorymatches.achievements";
        CONTENT_ITEM_TYPES = new String[2];
        CONTENT_ITEM_TYPES[0] = "vnd.android.cursor.item/vnd.idesignco.memorymatches.score";
        CONTENT_ITEM_TYPES[1] = "vnd.android.cursor.item/vnd.idesignco.memorymatches.achievements";
        String[] columns = {LAYOUT, CARDSET, NAME, SCORE};
        String[] ach_Columns = {CARDSET, WON4X4, WON6X6};
        int[] columnTypes = new int[4];
        columnTypes[0] = 1;
        columnTypes[1] = 1;
        columnTypes[3] = 1;
        COLUMN_NAMES = new String[2][];
        COLUMN_NAMES[0] = columns;
        COLUMN_NAMES[1] = ach_Columns;
        COLUMN_TYPES = new int[2][];
        COLUMN_TYPES[0] = columnTypes;
        COLUMN_TYPES[1] = new int[]{1, 1, 1};
        defaultSort = new String[2];
        defaultSort[0] = "score ASC";
        defaultSort[1] = null;
    }
}
