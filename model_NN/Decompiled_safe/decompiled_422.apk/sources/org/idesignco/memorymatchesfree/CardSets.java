package org.idesignco.memorymatchesfree;

import android.util.Log;

public final class CardSets {
    public static final int LAYOUT_4X4 = 0;
    public static final int LAYOUT_4X5 = 1;
    public static final int LAYOUT_5X6 = 2;
    public static final int LAYOUT_6X6 = 3;
    private static final String LOGTAG = "MMatches:CardSets";
    public static final int[] drawSetD_0 = {R.drawable.d1, R.drawable.d2, R.drawable.d3, R.drawable.d4, R.drawable.d5, R.drawable.d6, R.drawable.d7, R.drawable.d8, R.drawable.d9, R.drawable.d10, R.drawable.d11, R.drawable.d12, R.drawable.d13, R.drawable.d14, R.drawable.d15, R.drawable.d16, R.drawable.d17, R.drawable.d18};
    public static final int[] drawSetD_1 = drawSetD_0;
    public static final int[] drawSetDots_0 = drawSetNumDots_0;
    public static final int[] drawSetDots_1 = drawSetNumDots_0;
    public static final int[] drawSetNumDots_0 = {R.drawable.dot1, R.drawable.dot2, R.drawable.dot3, R.drawable.dot4, R.drawable.dot5, R.drawable.dot6, R.drawable.dot7, R.drawable.dot8, R.drawable.dot9, R.drawable.dot10, R.drawable.dot11, R.drawable.dot12, R.drawable.dot13, R.drawable.dot14, R.drawable.dot15, R.drawable.dot16, R.drawable.dot17, R.drawable.dot18};
    public static final int[] drawSetNumDots_1 = {R.drawable.num1, R.drawable.num2, R.drawable.num3, R.drawable.num4, R.drawable.num5, R.drawable.num6, R.drawable.num7, R.drawable.num8, R.drawable.num9, R.drawable.num10, R.drawable.num11, R.drawable.num12, R.drawable.num13, R.drawable.num14, R.drawable.num15, R.drawable.num16, R.drawable.num17, R.drawable.num18};
    public static final String[][] leaderboardIDs = {new String[]{"629954", "629964", "629984", "629944", "630004"}, new String[]{"630024", "630064", "630084", "630044", "630104"}, new String[]{"630034", "630074", "630094", "630054", "630114"}, new String[]{"613894", "629974", "629994", "629934", "630014"}};
    public static final int[] setHumanNames = {R.string.defaultSet, R.string.dots, R.string.numDots, R.string.shapes, R.string.colors};
    public static final int[] setIDs = {R.id.radioDefault, R.id.radioDots, R.id.radioNumDots, R.id.radioShapes, R.id.radioColors};
    public static final String[] setKeyNames = {"Default", "Dots", "NumDots", "Shapes", "Colors"};

    public static final int getSetIDPos(int id) {
        for (int i = 0; i < setIDs.length; i++) {
            if (id == setIDs[i]) {
                return i;
            }
        }
        Log.e(LOGTAG, "getSetIDPos - invalid id: " + Integer.toString(id));
        return -1;
    }
}
