package org.idesignco.memorymatchesfree;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;
import com.openfeint.api.resource.Achievement;

public final class MMAchievements {
    public static Achievement DEFAULT_MASTER = new Achievement("778503");
    private static final String LOGTAG = "MMAchievements";
    public static Achievement THEME_MASTER_4X4 = new Achievement("778663");
    public static Achievement THEME_MASTER_6X6 = new Achievement("778733");

    private static class progressCB extends Achievement.UpdateProgressionCB {
        private String achName;
        private Context context;

        public progressCB(String aName, Context ctx) {
            this.achName = aName;
            this.context = ctx;
        }

        public void onSuccess(boolean complete) {
        }

        public void onFailure(String message) {
            Log.e(MMAchievements.LOGTAG, "Error updating achievement: " + this.achName + "(" + message + ")");
            Toast.makeText(this.context, "Error (" + message + ") updating achievement.", 0).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void set4x4Won(Context ctx, int cardset) {
        Log.d(LOGTAG, "Set 4x4 Won" + Integer.toString(cardset));
        String[] projection = {SQLProvider._ID, DataProvider.WON4X4};
        String[] selectionArgs = {Integer.toString(cardset)};
        Cursor c = ctx.getContentResolver().query(DataProvider.ACHIEVEMENTS, projection, "cardset=?", selectionArgs, null);
        if (c.getCount() == 0) {
            ContentValues values = new ContentValues();
            values.put(DataProvider.CARDSET, Integer.valueOf(cardset));
            values.put(DataProvider.WON4X4, (Integer) 1);
            values.put(DataProvider.WON6X6, (Integer) 0);
            ctx.getContentResolver().insert(DataProvider.ACHIEVEMENTS, values);
        } else {
            c.moveToFirst();
            if (c.getInt(1) != 1) {
                ContentValues values2 = new ContentValues();
                values2.put(DataProvider.WON4X4, (Integer) 1);
                ctx.getContentResolver().update(ContentUris.withAppendedId(DataProvider.ACHIEVEMENTS, c.getLong(0)), values2, "cardset=?", selectionArgs);
            }
        }
        c.close();
    }

    public static void update4x4Achievement(Context ctx) {
        Cursor c = ctx.getContentResolver().query(DataProvider.ACHIEVEMENTS, new String[]{SQLProvider._ID, DataProvider.WON4X4}, "won4x4=?", new String[]{"1"}, null);
        THEME_MASTER_4X4.updateProgression((((float) c.getCount()) / ((float) CardSets.setHumanNames.length)) * 100.0f, new progressCB("Theme Master 4x4", ctx));
        c.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void set6x6Won(Context ctx, int cardset) {
        String[] projection = {SQLProvider._ID, DataProvider.WON6X6};
        String[] selectionArgs = {Integer.toString(cardset)};
        Cursor c = ctx.getContentResolver().query(DataProvider.ACHIEVEMENTS, projection, "cardset=?", selectionArgs, null);
        if (c.getCount() == 0) {
            ContentValues values = new ContentValues();
            values.put(DataProvider.CARDSET, Integer.valueOf(cardset));
            values.put(DataProvider.WON6X6, (Integer) 1);
            values.put(DataProvider.WON4X4, (Integer) 0);
            ctx.getContentResolver().insert(DataProvider.ACHIEVEMENTS, values);
        } else {
            c.moveToFirst();
            if (c.getInt(1) != 1) {
                ContentValues values2 = new ContentValues();
                values2.put(DataProvider.WON6X6, (Integer) 1);
                ctx.getContentResolver().update(ContentUris.withAppendedId(DataProvider.ACHIEVEMENTS, c.getLong(0)), values2, "cardset=?", selectionArgs);
            }
        }
        c.close();
    }

    public static void update6x6Achievement(Context ctx) {
        Cursor c = ctx.getContentResolver().query(DataProvider.ACHIEVEMENTS, new String[]{SQLProvider._ID, DataProvider.WON4X4}, "won6x6=?", new String[]{"1"}, null);
        THEME_MASTER_4X4.updateProgression((((float) c.getCount()) / ((float) CardSets.setHumanNames.length)) * 100.0f, new progressCB("Theme Master 6x6", ctx));
        c.close();
    }
}
