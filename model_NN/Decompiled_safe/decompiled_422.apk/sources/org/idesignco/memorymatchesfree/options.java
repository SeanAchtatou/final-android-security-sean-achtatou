package org.idesignco.memorymatchesfree;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ToggleButton;
import com.adwhirl.AdWhirlActivity;
import com.flurry.android.FlurryAgent;

public class options extends AdWhirlActivity {
    private static final int DLG_CONFIRM_CLEARHS = 0;
    private static final int DLG_PAID_ONLY = 1;
    private static final String LOGTAG = "MMatches:options";
    public static final String sharedfile = "data";
    SharedPreferences data;
    /* access modifiers changed from: private */
    public boolean initialized = false;
    private int layout;
    private int mmdelay;
    /* access modifiers changed from: private */
    public Boolean timercarry;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.options);
        setVolumeControlStream(3);
        this.data = getSharedPreferences("data", 2);
        this.layout = this.data.getInt(DataProvider.LAYOUT, 0);
        this.mmdelay = this.data.getInt("mmdelay_key", 2);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        Spinner spinner1 = (Spinner) findViewById(R.id.spinner1);
        SimpleSoundManager.enabled = this.data.getBoolean("sound", true);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.delaysText, R.layout.spinner_layout);
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter) adapter);
        spinner.setOnItemSelectedListener(new SelectedDelay());
        spinner.setSelection(this.mmdelay, false);
        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.layoutText, R.layout.spinner_layout);
        adapter1.setDropDownViewResource(17367049);
        spinner1.setAdapter((SpinnerAdapter) adapter1);
        spinner1.setOnItemSelectedListener(new SelectedLayout());
        spinner1.setSelection(this.layout, false);
        int cardSetPos = this.data.getInt("cardSetPos", 0);
        this.timercarry = Boolean.valueOf(this.data.getBoolean("timer_display_key", true));
        final ToggleButton togglebutton = (ToggleButton) findViewById(R.id.toggleButton);
        togglebutton.setChecked(this.timercarry.booleanValue());
        ((RadioGroup) findViewById(R.id.rdgCardSet)).check(CardSets.setIDs[cardSetPos]);
        ((ToggleButton) findViewById(R.id.tbtnSound)).setChecked(SimpleSoundManager.enabled);
        togglebutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SimpleSoundManager.play(options.this, R.raw.buttonclick);
                if (togglebutton.isChecked()) {
                    options.this.timercarry = true;
                } else {
                    options.this.timercarry = false;
                }
            }
        });
        this.initialized = true;
    }

    public class SelectedDelay implements AdapterView.OnItemSelectedListener {
        public SelectedDelay() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            if (options.this.initialized) {
                SimpleSoundManager.play(options.this, R.raw.buttonclick);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public class SelectedLayout implements AdapterView.OnItemSelectedListener {
        public SelectedLayout() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
            if (options.this.initialized) {
                SimpleSoundManager.play(options.this, R.raw.buttonclick);
            }
            if (pos != 0 && pos != 1) {
                ((Spinner) options.this.findViewById(R.id.spinner1)).setSelection(options.this.data.getInt(DataProvider.LAYOUT, 0));
                options.this.showDialog(1);
                Log.d(options.LOGTAG, "Paid Layout selected: " + Integer.toString(pos));
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Game.FLURRY_API_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStart();
        FlurryAgent.onEndSession(this);
    }

    public void onPause() {
        super.onPause();
        SharedPreferences.Editor editor = this.data.edit();
        editor.putInt(DataProvider.LAYOUT, ((Spinner) findViewById(R.id.spinner1)).getSelectedItemPosition());
        editor.putInt("cardSetPos", CardSets.getSetIDPos(((RadioGroup) findViewById(R.id.rdgCardSet)).getCheckedRadioButtonId()));
        editor.putBoolean("timer_display_key", this.timercarry.booleanValue());
        editor.putInt("mmdelay_key", ((Spinner) findViewById(R.id.spinner)).getSelectedItemPosition());
        editor.putBoolean("firstplay_key", false);
        editor.putBoolean("sound", ((ToggleButton) findViewById(R.id.tbtnSound)).isChecked());
        editor.commit();
    }

    public Dialog onCreateDialog(int id) {
        AlertDialog.Builder d = new AlertDialog.Builder(this);
        switch (id) {
            case 0:
                d.setMessage((int) R.string.confirmClear).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        options.this.clearScores();
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                return d.create();
            case 1:
                d.setMessage((int) R.string.paidOnly).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        String uri = options.this.getString(R.string.paidUrl);
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(uri));
                        options.this.startActivity(intent);
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                return d.create();
            default:
                String msg = "onCreateDialog - Unknown dialog ID: " + Integer.toString(id);
                FlurryAgent.onError(LOGTAG, msg, "onCreateDialog");
                Log.e(LOGTAG, msg);
                return null;
        }
    }

    public void resetClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        showDialog(0);
    }

    public void clearScores() {
        getContentResolver().delete(DataProvider.SCORES_URI, null, null);
    }

    public void soundClick(View v) {
        SimpleSoundManager.enabled = ((ToggleButton) v).isChecked();
    }

    public void paidCardSet(View v) {
        ((RadioGroup) findViewById(R.id.rdgCardSet)).check(CardSets.setIDs[this.data.getInt("cardSetPos", 0)]);
        showDialog(1);
        Log.d(LOGTAG, "Paid card set selected");
    }
}
