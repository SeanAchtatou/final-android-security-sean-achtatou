package org.idesignco.memorymatchesfree;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.adwhirl.AdWhirlLayout;
import com.flurry.android.FlurryAgent;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Random;

public class Game extends Activity {
    public static final String FLURRY_API_KEY = "FPWITYP3CDYW58EY74FP";
    private static final String LOGTAG = "MMatches:Game";
    private static final int MAXCARDS = 36;
    public static final String OFID = "225783";
    public static final String OFKEY = "XcCrQPd1A49ScNQSIiDmA";
    public static final String OFNAME = "Memory Matches";
    public static final String OFSECRET = "rofYRmjXkxUMJ0SuBuUiZvkqCljsSBDTfTL8KP8nhpc";
    private static final int PADDING = 2;
    private static final int SHUFFLEPASSES = 100;
    private static final int[] cardIDs = {R.id.btn00, R.id.btn01, R.id.btn02, R.id.btn03, R.id.btn04, R.id.btn05, R.id.btn06, R.id.btn07, R.id.btn08, R.id.btn09, R.id.btn10, R.id.btn11, R.id.btn12, R.id.btn13, R.id.btn14, R.id.btn15, R.id.btn16, R.id.btn17, R.id.btn18, R.id.btn19};
    public static int cardSet = 0;
    private static SharedPreferences data = null;
    private static int[] drawSet_0 = null;
    private static int[] drawSet_1 = null;
    public static final DecimalFormat formatter = new DecimalFormat("##0.0");
    public static int layout = 0;
    public static final String sharedfile = "data";
    private HashMap<String, String> FLURRY_PARAMS;
    int[] cardFace = new int[MAXCARDS];
    int[] cardVal = new int[MAXCARDS];
    int curCardID = -1;
    int curPos = -1;
    private int mismatchDelay;
    int numCards;
    int numMatches = 0;
    Random rnd;
    private boolean running = true;
    private float seconds = 0.0f;
    private boolean showing2 = false;
    private boolean[] shown = new boolean[MAXCARDS];
    private Timer timer = new Timer();
    private TextView txtSeconds;

    public class Timer extends Handler {
        private static final String LOGTAG = "MMatches:Game:Timer";
        private static final int MSG_MATCH = 1;
        private static final int MSG_MISMATCH = 0;
        private static final int MSG_TICK = 2;
        private int card1;
        private int card2;
        private long now;
        private long then;

        public Timer() {
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    Game.this.handleMismatch(this.card1, this.card2);
                    return;
                case 1:
                    Game.this.handleMatch(this.card1, this.card2);
                    return;
                case 2:
                    removeMessages(2);
                    this.now = SystemClock.uptimeMillis();
                    Game.this.handleTick(this.now - this.then);
                    return;
                default:
                    Log.e(LOGTAG, "Unexpected message received - what:" + Integer.toString(msg.what) + " string: " + msg.toString());
                    FlurryAgent.onError(LOGTAG, "Unexpected message received: " + msg.toString(), "handleMessage");
                    return;
            }
        }

        public void mismatchDelay(int card1ref, int card2ref, long millis) {
            this.card1 = card1ref;
            this.card2 = card2ref;
            sendMessageDelayed(obtainMessage(0), millis);
        }

        public void matchDelay(int card1ref, int card2ref, long millis) {
            this.card1 = card1ref;
            this.card2 = card2ref;
            sendMessageDelayed(obtainMessage(1), millis);
        }

        public void sendTick() {
            this.then = SystemClock.uptimeMillis();
            sendMessageDelayed(obtainMessage(2), 100);
        }

        public void clearTick() {
            removeMessages(2);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        requestWindowFeature(1);
        data = getSharedPreferences("data", 2);
        layout = data.getInt(DataProvider.LAYOUT, 0);
        boolean timercarry = data.getBoolean("timer_display_key", true);
        cardSet = data.getInt("cardSetPos", 0);
        int mmdelay = data.getInt("mmdelay_key", 2);
        if (mmdelay == 0) {
            this.mismatchDelay = SHUFFLEPASSES;
        } else if (mmdelay == 1) {
            this.mismatchDelay = 250;
        } else if (mmdelay == 2) {
            this.mismatchDelay = 500;
        } else if (mmdelay == 3) {
            this.mismatchDelay = 1000;
        }
        switch (layout) {
            case 0:
                setContentView((int) R.layout.game4x4);
                this.numCards = 16;
                break;
            case 1:
                setContentView((int) R.layout.game4x5);
                this.numCards = 20;
                break;
            default:
                Log.e(LOGTAG, "Unknown layout: '" + Integer.toString(layout) + "' defaulting to 4x4");
                setContentView((int) R.layout.game4x4);
                this.numCards = 16;
                layout = 0;
                FlurryAgent.onError(LOGTAG, "Invalid layout", "onCreate");
                break;
        }
        switch (CardSets.setIDs[cardSet]) {
            case R.id.radioDefault /*2131361863*/:
                drawSet_0 = CardSets.drawSetD_0;
                drawSet_1 = CardSets.drawSetD_1;
                break;
            case R.id.radioDots /*2131361864*/:
                drawSet_0 = CardSets.drawSetDots_0;
                drawSet_1 = CardSets.drawSetDots_1;
                break;
            case R.id.radioNumDots /*2131361865*/:
                drawSet_0 = CardSets.drawSetNumDots_0;
                drawSet_1 = CardSets.drawSetNumDots_1;
                break;
            default:
                Log.e(LOGTAG, "Unknown card set id: " + Integer.toString(cardSet) + " using default");
                drawSet_0 = CardSets.drawSetD_0;
                drawSet_1 = CardSets.drawSetD_1;
                FlurryAgent.onError(LOGTAG, "Invalid Card Set ID", "onCreate");
                break;
        }
        this.FLURRY_PARAMS = new HashMap<>();
        this.FLURRY_PARAMS.put("numCards", Integer.toString(this.numCards));
        this.FLURRY_PARAMS.put("Card Set", CardSets.setKeyNames[cardSet]);
        this.FLURRY_PARAMS.put("Delay", Integer.toString(mmdelay));
        this.txtSeconds = (TextView) findViewById(R.id.txtSeconds);
        if (timercarry) {
            this.txtSeconds.setVisibility(0);
        } else {
            this.txtSeconds.setVisibility(8);
        }
        this.rnd = new Random();
        SimpleSoundManager.enabled = data.getBoolean("sound", true);
        if (savedInstanceState == null) {
            createGame();
        } else {
            this.curCardID = -1;
            this.curPos = -1;
            this.seconds = savedInstanceState.getFloat("seconds");
            this.numMatches = savedInstanceState.getInt("numMatches");
            this.cardVal = savedInstanceState.getIntArray("cardVal");
            this.cardFace = savedInstanceState.getIntArray("cardFace");
            this.shown = savedInstanceState.getBooleanArray("shown");
            for (int i = 0; i < this.numCards; i++) {
                ImageButton card = (ImageButton) findViewById(cardIDs[i]);
                if (this.shown[i]) {
                    card.setVisibility(0);
                } else {
                    card.setVisibility(4);
                }
            }
            this.txtSeconds.setText(formatter.format((double) this.seconds));
            this.timer.sendTick();
        }
        this.timer.clearTick();
        this.timer.sendTick();
        this.running = true;
    }

    public void createGame() {
        FlurryAgent.onEvent("Game Start", this.FLURRY_PARAMS);
        int matches = this.numCards / 2;
        for (int i = 0; i < this.numCards; i++) {
            this.cardVal[i] = i % matches;
        }
        boolean[] seenCard = new boolean[18];
        for (int i2 = 0; i2 < matches; i2++) {
            seenCard[i2] = false;
        }
        for (int i3 = 0; i3 < this.numCards; i3++) {
            if (seenCard[this.cardVal[i3]]) {
                this.cardFace[i3] = drawSet_0[this.cardVal[i3]];
            } else {
                this.cardFace[i3] = drawSet_1[this.cardVal[i3]];
                seenCard[this.cardVal[i3]] = true;
            }
            ImageButton card = (ImageButton) findViewById(cardIDs[i3]);
            card.setImageResource(R.drawable.cardback);
            card.setVisibility(0);
            this.shown[i3] = true;
        }
        for (int i4 = 0; i4 < SHUFFLEPASSES; i4++) {
            int a = this.rnd.nextInt(this.numCards);
            int b = this.rnd.nextInt(this.numCards);
            int v = this.cardVal[a];
            this.cardVal[a] = this.cardVal[b];
            this.cardVal[b] = v;
            int v2 = this.cardFace[a];
            this.cardFace[a] = this.cardFace[b];
            this.cardFace[b] = v2;
        }
        this.seconds = 0.0f;
        this.txtSeconds.setText(formatter.format((double) this.seconds));
        this.txtSeconds.invalidate();
        this.numMatches = 0;
        this.curCardID = -1;
        this.curPos = -1;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("numMatches", this.numMatches);
        outState.putFloat("seconds", this.seconds);
        outState.putIntArray("cardVal", this.cardVal);
        outState.putIntArray("cardFace", this.cardFace);
        outState.putBooleanArray("shown", this.shown);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, FLURRY_API_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
        this.running = false;
        this.timer.clearTick();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        this.running = true;
        this.timer.sendTick();
    }

    public void cardClick(View v) {
        if (!this.showing2) {
            int id = v.getId();
            int pos = findCardPos(id);
            ImageButton card = (ImageButton) findViewById(id);
            card.setImageBitmap(resizeImage(this.cardFace[pos]));
            card.setScaleType(ImageView.ScaleType.CENTER);
            card.invalidate();
            SimpleSoundManager.play(this, R.raw.cardclick);
            if (this.curCardID == -1) {
                this.curCardID = id;
                this.curPos = pos;
            } else if (this.cardVal[pos] != this.cardVal[this.curPos]) {
                this.timer.mismatchDelay(id, this.curCardID, (long) this.mismatchDelay);
                this.showing2 = true;
                this.curCardID = -1;
                this.curPos = -1;
            } else if (this.curCardID != id) {
                this.numMatches++;
                this.timer.matchDelay(id, this.curCardID, (long) this.mismatchDelay);
                this.showing2 = true;
                this.shown[this.curPos] = false;
                this.shown[pos] = false;
                this.curCardID = -1;
                this.curPos = -1;
                if (this.numMatches == this.numCards / 2) {
                    doVictory();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private Bitmap resizeImage(int resID) {
        Bitmap bitmapOrg = BitmapFactory.decodeResource(getResources(), resID);
        int width = bitmapOrg.getWidth();
        int height = bitmapOrg.getHeight();
        ImageButton card = (ImageButton) findViewById(R.id.btn00);
        int newWidth = card.getWidth() - 2;
        float scaleHeight = ((float) (card.getHeight() - 2)) / ((float) height);
        float scaleWidth = ((float) newWidth) / ((float) width);
        if (scaleHeight < scaleWidth) {
            scaleWidth = scaleHeight;
        } else {
            scaleHeight = scaleWidth;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
    }

    private void doVictory() {
        this.running = false;
        this.timer.clearTick();
        FlurryAgent.onEvent("Game Win", this.FLURRY_PARAMS);
        Intent i = new Intent(this, NewScore.class);
        i.putExtra(DataProvider.CARDSET, cardSet);
        i.putExtra(DataProvider.LAYOUT, layout);
        i.putExtra(DataProvider.SCORE, this.seconds);
        startActivityForResult(i, 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data2) {
        if (requestCode != 0) {
            Log.e(LOGTAG, "onActivityResult - ignoring unknown result code: " + Integer.toString(requestCode));
            FlurryAgent.onError(LOGTAG, "Unknown Result code: " + Integer.toString(requestCode), "onActivityResult");
            return;
        }
        switch (resultCode) {
            case 1:
                createGame();
                this.running = true;
                this.timer.clearTick();
                this.timer.sendTick();
                return;
            default:
                finish();
                return;
        }
    }

    /* access modifiers changed from: private */
    public void handleMismatch(int card1ref, int card2ref) {
        ImageButton card1 = (ImageButton) findViewById(card1ref);
        ImageButton card2 = (ImageButton) findViewById(card2ref);
        if (this.curCardID != card1ref) {
            card1.setImageResource(R.drawable.cardback);
        }
        if (this.curCardID != card2ref) {
            card2.setImageResource(R.drawable.cardback);
        }
        card1.setScaleType(ImageView.ScaleType.FIT_XY);
        card2.setScaleType(ImageView.ScaleType.FIT_XY);
        card1.invalidate();
        card2.invalidate();
        this.showing2 = false;
    }

    /* access modifiers changed from: private */
    public void handleMatch(int card1ref, int card2ref) {
        ImageButton card1 = (ImageButton) findViewById(card1ref);
        ImageButton card2 = (ImageButton) findViewById(card2ref);
        if (this.curCardID == card1ref || this.curCardID == card2ref) {
            this.curCardID = -1;
            this.curPos = -1;
        }
        card1.setScaleType(ImageView.ScaleType.FIT_XY);
        card2.setScaleType(ImageView.ScaleType.FIT_XY);
        card1.setVisibility(4);
        card2.setVisibility(4);
        card1.invalidate();
        card2.invalidate();
        this.showing2 = false;
    }

    /* access modifiers changed from: private */
    public void handleTick(long delay) {
        this.seconds += ((float) delay) / 1000.0f;
        this.txtSeconds.setText(formatter.format((double) this.seconds));
        this.txtSeconds.invalidate();
        if (this.running) {
            this.timer.sendTick();
        }
    }

    private int findCardPos(int id) {
        for (int i = 0; i < this.numCards; i++) {
            if (cardIDs[i] == id) {
                return i;
            }
        }
        String entryName = getResources().getResourceEntryName(id);
        Log.e(LOGTAG, "findCardPos - Bad card id: " + entryName);
        FlurryAgent.onError(LOGTAG, "Bad card id: " + entryName, "findCardPos");
        return -1;
    }

    public void onPause() {
        super.onPause();
        AdWhirlLayout layout2 = (AdWhirlLayout) findViewById(R.id.layoutAd);
        if (layout2 != null) {
            layout2.handleActivityMessage(1, isFinishing());
        }
    }

    public void onResume() {
        super.onResume();
        AdWhirlLayout layout2 = (AdWhirlLayout) findViewById(R.id.layoutAd);
        if (layout2 != null) {
            layout2.handleActivityMessage(2, isFinishing());
        }
    }

    public void onDestroy() {
        super.onDestroy();
        AdWhirlLayout layout2 = (AdWhirlLayout) findViewById(R.id.layoutAd);
        if (layout2 != null) {
            layout2.handleActivityMessage(3, isFinishing());
        }
    }
}
