package org.idesignco.memorymatchesfree;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import com.adwhirl.AdWhirlActivity;
import com.flurry.android.FlurryAgent;

public class FirstPlay extends AdWhirlActivity {
    private static final String LOGTAG = "MMatches:FirstPlay";
    public static final String sharedfile = "data";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.firstplay);
        setVolumeControlStream(3);
        SharedPreferences data = getSharedPreferences("data", 2);
        data.edit().putBoolean("firstplay_key", false).commit();
        SimpleSoundManager.enabled = data.getBoolean("sound", true);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Game.FLURRY_API_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStart();
        FlurryAgent.onEndSession(this);
    }

    public void newClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        startActivity(new Intent(this, Game.class));
        FlurryAgent.onEvent("First Play New game hit");
        finish();
    }

    public void settingsClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        startActivityForResult(new Intent(this, options.class), 0);
        FlurryAgent.onEvent("First Play settings hit");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            startActivity(new Intent(this, Game.class));
            finish();
            return;
        }
        Log.e(LOGTAG, "onActivityResult - Unknown request code: " + Integer.toString(requestCode));
        FlurryAgent.onError(LOGTAG, "Unknown request code: " + Integer.toString(requestCode), "onActivityResult");
    }
}
