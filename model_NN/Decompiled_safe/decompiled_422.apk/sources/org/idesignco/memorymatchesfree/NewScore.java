package org.idesignco.memorymatchesfree;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.adwhirl.AdWhirlActivity;
import com.flurry.android.FlurryAgent;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Score;
import java.util.HashMap;

public class NewScore extends AdWhirlActivity {
    public static final int DLG_APPROVE_OPENFEINT = 0;
    private static final String LOGTAG = "MMatches:NewScore";
    private static SharedPreferences data = null;
    public static final String sharedfile = "data";
    private float seconds;

    private class unlockCB extends Achievement.UnlockCB {
        private String achName;

        public unlockCB(String aName) {
            this.achName = aName;
        }

        public void onSuccess(boolean newUnlock) {
            Log.v(NewScore.LOGTAG, "Achievement successfully unlocked: " + this.achName);
        }

        public void onFailure(String message) {
            Log.e(NewScore.LOGTAG, "Error updating achievement: " + this.achName + "(" + message + ")");
            Toast.makeText(NewScore.this, "Error (" + message + ") unlocking achievement.", 0).show();
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.newscore);
        setVolumeControlStream(3);
        EditText txtName = (EditText) findViewById(R.id.editName);
        TextView txtWin = (TextView) findViewById(R.id.txtWinMessage);
        data = getSharedPreferences("data", 2);
        SimpleSoundManager.enabled = data.getBoolean("sound", true);
        setTitle((int) R.string.win_normal);
        this.seconds = getIntent().getFloatExtra(DataProvider.SCORE, -1.0f);
        if (this.seconds == -1.0f) {
            Log.e(LOGTAG, "Invalid seconds received... something went wrong");
            FlurryAgent.onError(LOGTAG, "Seconds not set", "onCreate");
            finish();
            return;
        }
        txtWin.setText(String.valueOf(getString(R.string.win)) + " " + Game.formatter.format((double) this.seconds) + " " + getString(R.string.seconds));
        txtName.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == 0) {
                    switch (keyCode) {
                        case 66:
                            if (!((Button) NewScore.this.findViewById(R.id.btnSave)).requestFocus()) {
                                Log.w(NewScore.LOGTAG, "Unable to set focus to save button");
                            }
                            return true;
                    }
                }
                return false;
            }
        });
        if (savedInstanceState == null) {
            txtName.setText(data.getString("newscore_name", getString(R.string.defaultName)));
            txtName.selectAll();
            SimpleSoundManager.play(this, R.raw.winner);
        }
        setResult(0);
        checkAndUpdateAchievements();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, Game.FLURRY_API_KEY);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStart();
        FlurryAgent.onEndSession(this);
    }

    public void cancelClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        if (((CheckBox) findViewById(R.id.chkNewGame)).isChecked()) {
            setResult(1);
        } else {
            setResult(0);
        }
        finish();
    }

    public void saveClick(View v) {
        SimpleSoundManager.play(this, R.raw.buttonclick);
        boolean newGame = ((CheckBox) findViewById(R.id.chkNewGame)).isChecked();
        boolean web = ((CheckBox) findViewById(R.id.chkWebUpload)).isChecked();
        long score = (long) (this.seconds * 1000.0f);
        HashMap<String, String> FlurryParams = new HashMap<>();
        int cardset = getIntent().getIntExtra(DataProvider.CARDSET, 0);
        int layout = getIntent().getIntExtra(DataProvider.LAYOUT, 0);
        String[] projection = {SQLProvider._ID};
        Cursor result = getContentResolver().query(DataProvider.SCORES_URI, projection, "score>? AND cardset=? AND layout=?", new String[]{Long.toString(score), Integer.toString(cardset), Integer.toString(layout)}, null);
        String[] selectionArgs = {Integer.toString(cardset), Integer.toString(layout)};
        Cursor allScores = getContentResolver().query(DataProvider.SCORES_URI, projection, "cardset=? AND layout=?", selectionArgs, null);
        if (result.getCount() > 0 || allScores.getCount() < 5) {
            String name = ((EditText) findViewById(R.id.editName)).getText().toString();
            data.edit().putString("newscore_name", name).commit();
            ContentValues values = new ContentValues();
            values.put(DataProvider.SCORE, Long.valueOf(score));
            values.put(DataProvider.NAME, name);
            values.put(DataProvider.CARDSET, Integer.valueOf(cardset));
            values.put(DataProvider.LAYOUT, Integer.valueOf(layout));
            while (allScores.getCount() >= 5) {
                allScores.moveToLast();
                getContentResolver().delete(ContentUris.withAppendedId(DataProvider.SCORES_URI, allScores.getLong(0)), null, null);
                allScores.close();
                allScores = getContentResolver().query(DataProvider.SCORES_URI, projection, "cardset=? AND layout=?", selectionArgs, null);
            }
            getContentResolver().insert(DataProvider.SCORES_URI, values);
        }
        result.close();
        allScores.close();
        if (newGame) {
            setResult(1);
            FlurryParams.put("new game", "true");
        } else {
            setResult(-1);
            FlurryParams.put("new game", "false");
        }
        if (web) {
            FlurryParams.put("Upload to web", "true");
            FlurryAgent.onEvent("Save score", FlurryParams);
            if (OpenFeint.isUserLoggedIn()) {
                uploadScore();
            } else {
                showDialog(0);
            }
        } else {
            FlurryParams.put("Upload to web", "false");
            FlurryAgent.onEvent("Save score", FlurryParams);
            finish();
        }
    }

    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                AlertDialog.Builder d = new AlertDialog.Builder(this);
                d.setMessage((int) R.string.need_openfeint).setCancelable(false).setPositiveButton((int) R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OpenFeint.userApprovedFeint();
                        NewScore.this.checkAndUpdateAchievements();
                        NewScore.this.uploadScore();
                    }
                }).setNegativeButton((int) R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ((CheckBox) NewScore.this.findViewById(R.id.chkWebUpload)).setChecked(false);
                    }
                });
                return d.create();
            default:
                String msg = "Unknown dialog id: " + Integer.toString(id);
                Log.e(LOGTAG, msg);
                FlurryAgent.onError(LOGTAG, msg, "onCreateDialog");
                return null;
        }
    }

    /* access modifiers changed from: private */
    public void uploadScore() {
        String leaderBoardID = findLeaderBoardID();
        long score = (long) (this.seconds * 1000.0f);
        new Score(score, Game.formatter.format((double) this.seconds)).submitTo(new Leaderboard(leaderBoardID), new Score.SubmitToCB() {
            public void onSuccess(boolean newHighScore) {
                Log.v(NewScore.LOGTAG, "Web Upload Success");
                NewScore.this.finish();
            }

            public void onFailure(String message) {
                Log.e(NewScore.LOGTAG, "Web upload failed: " + message);
                FlurryAgent.onError("WebUpload", "Web upload failed, message: " + message, "onFailure");
                Toast.makeText(NewScore.this, "Error uploading high score: " + message, 0).show();
                ((CheckBox) NewScore.this.findViewById(R.id.chkWebUpload)).setChecked(false);
            }
        });
        Log.v(LOGTAG, "Uploaded score: " + Long.toString(score) + "to leader board: " + leaderBoardID);
    }

    private String findLeaderBoardID() {
        int curLayout = getIntent().getIntExtra(DataProvider.LAYOUT, 0);
        return CardSets.leaderboardIDs[curLayout][data.getInt("cardSetPos", 0)];
    }

    /* access modifiers changed from: private */
    public void checkAndUpdateAchievements() {
        int layout = getIntent().getIntExtra(DataProvider.LAYOUT, 0);
        int cardSet = data.getInt("cardSetPos", 0);
        Log.w(LOGTAG, "layout: " + Integer.toString(layout) + " cardSet: " + Integer.toString(cardSet));
        if (this.seconds < 20.0f && layout == 0 && CardSets.setIDs[cardSet] == R.id.radioDefault && OpenFeint.isUserLoggedIn()) {
            MMAchievements.DEFAULT_MASTER.unlock(new unlockCB("Default Master"));
        }
        if (layout == 0) {
            MMAchievements.set4x4Won(this, cardSet);
            if (OpenFeint.isUserLoggedIn()) {
                MMAchievements.update4x4Achievement(this);
            }
        } else if (layout == 3) {
            MMAchievements.set6x6Won(this, cardSet);
            if (OpenFeint.isUserLoggedIn()) {
                MMAchievements.update6x6Achievement(this);
            }
        }
    }
}
