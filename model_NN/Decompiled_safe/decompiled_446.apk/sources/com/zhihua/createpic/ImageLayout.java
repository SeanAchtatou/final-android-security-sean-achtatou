package com.zhihua.createpic;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImageLayout extends LinearLayout {
    private Context ctx;
    private ImageView[][] imageViewArr;

    public ImageLayout(Context context, LinearLayout linear, ImageView[][] imageViewArray) {
        super(context);
        this.imageViewArr = imageViewArray;
        this.ctx = context;
        setContent(linear);
    }

    private void setContent(LinearLayout linear) {
        linear.setOrientation(1);
        for (int i = 0; i < this.imageViewArr.length; i++) {
            LinearLayout linearLayout = new LinearLayout(this.ctx);
            linearLayout.setOrientation(0);
            for (ImageView imageView : this.imageViewArr[i]) {
                if (imageView != null) {
                    linearLayout.addView(imageView, new LinearLayout.LayoutParams(-2, -2));
                }
            }
            linear.addView(linearLayout);
        }
        addView(linear);
    }
}
