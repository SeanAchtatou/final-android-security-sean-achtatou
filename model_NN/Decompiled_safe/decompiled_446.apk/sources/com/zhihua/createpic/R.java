package com.zhihua.createpic;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int joshua = 2130837505;
    }

    public static final class id {
        public static final int adView = 2131099649;
        public static final int mainview = 2131099648;
        public static final int textview = 2131099650;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int oops = 2130968576;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }
}
