package com.zhihua.createpic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.lang.reflect.Array;

public class MainActivity extends Activity {
    private static final int DIALOG1 = 1;
    private static final int NUM_LINES = 3;
    private View.OnClickListener OnClickImageView = new View.OnClickListener() {
        public void onClick(View v) {
            if (MainActivity.this.clickNum == 0) {
                int i = 0;
                while (i < MainActivity.this.imageViewArray.length) {
                    boolean f = false;
                    int j = 0;
                    while (true) {
                        if (j >= MainActivity.this.imageViewArray[i].length) {
                            break;
                        } else if (MainActivity.this.imageViewArray[i][j] == v) {
                            MainActivity.this.po = new Point(i, j);
                            MainActivity mainActivity = MainActivity.this;
                            mainActivity.clickNum = mainActivity.clickNum + MainActivity.DIALOG1;
                            f = true;
                            break;
                        } else {
                            j += MainActivity.DIALOG1;
                        }
                    }
                    if (!f) {
                        i += MainActivity.DIALOG1;
                    } else {
                        return;
                    }
                }
                return;
            }
            for (int i2 = 0; i2 < MainActivity.this.imageViewArray.length; i2 += MainActivity.DIALOG1) {
                for (int j2 = 0; j2 < MainActivity.this.imageViewArray[i2].length; j2 += MainActivity.DIALOG1) {
                    if (MainActivity.this.imageViewArray[i2][j2] == v && MainActivity.this.po != null) {
                        MainActivity.this.changeBitmapPosition(i2, j2, MainActivity.this.po.x, MainActivity.this.po.y);
                        MainActivity.this.po = null;
                        MainActivity.this.clickNum = 0;
                    }
                }
            }
        }
    };
    private Bitmap bitmap;
    private Bitmap[][] bitmapArray;
    private int blockHeight;
    private int blockWidth;
    /* access modifiers changed from: private */
    public int clickNum = 0;
    private int image = R.drawable.joshua;
    private int[] imageArray;
    /* access modifiers changed from: private */
    public ImageView[][] imageViewArray;
    private MediaPlayer mp;
    /* access modifiers changed from: private */
    public Point po;
    private float scaleHeight;
    private float scaleWidth;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mp = MediaPlayer.create(getBaseContext(), (int) R.raw.oops);
        this.imageViewArray = (ImageView[][]) Array.newInstance(ImageView.class, 4, 4);
        this.bitmapArray = (Bitmap[][]) Array.newInstance(Bitmap.class, 4, 4);
        this.imageArray = new int[16];
        this.bitmap = BitmapFactory.decodeResource(getResources(), this.image);
        int bitmapWidth = this.bitmap.getWidth();
        int bitmapHeight = this.bitmap.getHeight();
        float screenWidth = (float) getResources().getDisplayMetrics().widthPixels;
        this.scaleWidth = screenWidth / ((float) bitmapWidth);
        this.scaleHeight = ((99.0f * screenWidth) / 100.0f) / ((float) bitmapHeight);
        this.blockWidth = bitmapWidth / 4;
        this.blockHeight = bitmapHeight / 4;
        produceRandomImageViewArray();
        ready4ImageContent();
        LinearLayout linear = new LinearLayout(this);
        setContentView(new ImageLayout(this, linear, this.imageViewArray));
        AdView adView = new AdView(this, AdSize.BANNER, "a14dce0e8017cf6");
        linear.addView(adView);
        adView.loadAd(new AdRequest());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private void ready4ImageContent() {
        Matrix matrix = new Matrix();
        matrix.postScale(this.scaleWidth, this.scaleHeight);
        Bitmap[][] fragment = (Bitmap[][]) Array.newInstance(Bitmap.class, 4, 4);
        for (int i = 0; i < 4; i += DIALOG1) {
            for (int j = 0; j < 4; j += DIALOG1) {
                fragment[i][j] = Bitmap.createBitmap(this.bitmap, j * this.blockWidth, i * this.blockHeight, this.blockWidth, this.blockHeight, matrix, true);
            }
        }
        for (int i2 = 0; i2 < this.imageArray.length; i2 += DIALOG1) {
            int x = this.imageArray[i2] / 4;
            int y = this.imageArray[i2] % 4;
            this.bitmapArray[i2 / 4][i2 % 4] = fragment[x][y];
        }
        for (int i3 = 0; i3 < this.bitmapArray.length; i3 += DIALOG1) {
            for (int j2 = 0; j2 < this.bitmapArray[i3].length; j2 += DIALOG1) {
                BitmapDrawable draw = new BitmapDrawable(this.bitmapArray[i3][j2]);
                ImageView imageView = new ImageView(this);
                imageView.setImageDrawable(draw);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setOnClickListener(this.OnClickImageView);
                this.imageViewArray[i3][j2] = imageView;
            }
        }
    }

    private void produceRandomImageViewArray() {
        int[] array = new int[16];
        for (int i = 0; i < 16; i += DIALOG1) {
            array[i] = i;
        }
        int length = 16;
        for (int i2 = 0; i2 < 16; i2 += DIALOG1) {
            int index = (int) Math.floor(Math.random() * ((double) length));
            this.imageArray[i2] = array[index];
            for (int j = index; j < array.length - DIALOG1; j += DIALOG1) {
                array[j] = array[j + DIALOG1];
            }
            length--;
        }
    }

    private boolean hasCompleted() {
        for (int i = 0; i < 16; i += DIALOG1) {
            if (this.imageArray[i] != i) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void changeBitmapPosition(int x, int y, int x1, int y1) {
        if (Math.abs(x - x1) + Math.abs(y - y1) != DIALOG1) {
            this.mp.start();
            return;
        }
        Bitmap bitF = null;
        Bitmap bitS = null;
        Point p1 = new Point();
        Point p2 = new Point();
        int len = this.bitmapArray.length;
        for (int i = 0; i < len; i += DIALOG1) {
            int leng = this.bitmapArray[i].length;
            for (int j = 0; j < leng; j += DIALOG1) {
                Bitmap mapi = this.bitmapArray[i][j];
                if (i == x && j == y) {
                    bitF = mapi;
                    p1.x = i;
                    p1.y = j;
                } else if (i == x1 && j == y1) {
                    bitS = mapi;
                    p2.x = i;
                    p2.y = j;
                }
            }
        }
        this.bitmapArray[p1.x][p1.y] = bitS;
        this.imageArray[(p1.x * 4) + p1.y] = (p2.x * 4) + p2.y;
        this.bitmapArray[p2.x][p2.y] = bitF;
        this.imageArray[(p2.x * 4) + p2.y] = (p1.x * 4) + p1.y;
        for (int i2 = 0; i2 < this.bitmapArray.length; i2 += DIALOG1) {
            for (int j2 = 0; j2 < this.bitmapArray[i2].length; j2 += DIALOG1) {
                ImageView imageView = new ImageView(this);
                imageView.setImageDrawable(new BitmapDrawable(this.bitmapArray[i2][j2]));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setOnClickListener(this.OnClickImageView);
                this.imageViewArray[i2][j2] = imageView;
            }
        }
        LinearLayout linearLayout = new LinearLayout(this);
        setContentView(new ImageLayout(this, linearLayout, this.imageViewArray));
        AdView adView = new AdView(this, AdSize.BANNER, "a14dce0e8017cf6");
        linearLayout.addView(adView);
        adView.loadAd(new AdRequest());
        if (hasCompleted()) {
            this.mp.start();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case DIALOG1 /*1*/:
                return buildDialog1(this);
            default:
                return null;
        }
    }

    private AlertDialog buildDialog1(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton("Woops!", (DialogInterface.OnClickListener) null);
        return builder.create();
    }
}
