package jp.co.nobot.libAdMaker;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class libAdMaker extends FrameLayout {
    private static final String DOMAIN = "images.ad-maker.info";
    private static final String SDKVERSION = "1.1";
    private ImageButton button;
    /* access modifiers changed from: private */
    public Context ct;
    /* access modifiers changed from: private */
    public String deviceId;
    private String deviceModel;
    /* access modifiers changed from: private */
    public String deviceModelCookieString;
    /* access modifiers changed from: private */
    public String didCookieString;
    /* access modifiers changed from: private */
    public boolean expandFlag;
    /* access modifiers changed from: private */
    public boolean fastLoaded;
    private LinearLayout layout;
    /* access modifiers changed from: private */
    public AdMakerListener listener;
    private String osVersion;
    /* access modifiers changed from: private */
    public String osVersionCookieString;
    private String packageName;
    /* access modifiers changed from: private */
    public String packageNameCookieString;
    /* access modifiers changed from: private */
    public boolean resume;
    public String siteId;
    private String url;
    /* access modifiers changed from: private */
    public String versionCookieString;
    private WebSettings webSettings;
    /* access modifiers changed from: private */
    public WebView webView;
    public String zoneId;

    public libAdMaker(Context context) {
        super(context);
        this.ct = context;
        prepareAdMaker();
    }

    public libAdMaker(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.ct = context;
        prepareAdMaker();
    }

    public libAdMaker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.ct = context;
        prepareAdMaker();
    }

    private void prepareAdMaker() {
        this.resume = false;
        Resources r = this.ct.getResources();
        setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        this.webView = new WebView(this.ct);
        this.layout = new LinearLayout(this.ct);
        this.button = new ImageButton(this.ct);
        this.button.setBackgroundResource(17301560);
        this.layout.setGravity(85);
        this.button.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.layout.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (r.getDisplayMetrics().density * 50.0f)));
        this.layout.addView(this.button);
        addView(this.webView);
        this.webView.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (r.getDisplayMetrics().density * 50.0f)));
        addView(this.layout);
        this.button.setVisibility(8);
        this.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                libAdMaker.this.closeExpand();
                libAdMaker.this.startView();
            }
        });
        prepareCookie(this.ct);
    }

    private void prepareCookie(Context context) {
        CookieSyncManager.createInstance(context);
        CookieSyncManager.getInstance().startSync();
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().removeExpiredCookie();
    }

    public void setAdMakerListener(AdMakerListener _listener) {
        this.listener = _listener;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    /* access modifiers changed from: private */
    public boolean isNetworkConnected() {
        NetworkInfo ni = ((ConnectivityManager) this.ct.getSystemService("connectivity")).getActiveNetworkInfo();
        if (ni == null || !ni.isConnected()) {
            if (this.listener != null) {
                this.listener.onFailedToReceiveAdMaker("internet connection not available");
            }
            setVisibility(8);
            return false;
        }
        setVisibility(0);
        return true;
    }

    public void startView() {
        this.expandFlag = false;
        this.osVersion = Build.VERSION.RELEASE;
        try {
            if (Settings.Secure.getString(this.ct.getContentResolver(), "android_id") == null) {
                this.deviceId = "nofound";
            } else {
                this.deviceId = A.a(Settings.Secure.getString(this.ct.getContentResolver(), "android_id"));
            }
        } catch (NoSuchAlgorithmException e) {
            this.deviceId = "nofound";
        }
        this.deviceModel = "Android " + Build.MODEL;
        this.packageName = this.ct.getPackageName();
        this.packageNameCookieString = "admaker_appname=" + this.packageName + "; domain=" + DOMAIN;
        this.didCookieString = "admaker_did=" + this.deviceId + "; domain=" + DOMAIN;
        this.versionCookieString = "admaker_version=1.1; domain=images.ad-maker.info";
        this.osVersionCookieString = "admaker_osVersion=" + this.osVersion + "; domain=" + DOMAIN;
        this.deviceModelCookieString = "admaker_deviceModel=" + this.deviceModel + "; domain=" + DOMAIN;
        if (isNetworkConnected()) {
            this.webView.loadUrl(this.url);
            this.webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    try {
                        view.stopLoading();
                        libAdMaker.this.ct.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return true;
                    }
                }

                public void onLoadResource(WebView view, String url) {
                    int i;
                    String sha1String;
                    if (libAdMaker.this.resume) {
                        view.stopLoading();
                    } else if (libAdMaker.this.isNetworkConnected()) {
                        if (url.indexOf("http://images.ad-maker.info/apps") == 0) {
                            try {
                                sha1String = B.a(String.format("%s%s%s", libAdMaker.this.deviceId, Long.valueOf(new Date().getTime()), "mXFTQ9fp73rqK5aaOAuQ8yP8"));
                            } catch (Exception e) {
                                sha1String = "";
                            }
                            CookieManager.getInstance().setCookie(libAdMaker.DOMAIN, libAdMaker.this.packageNameCookieString);
                            CookieManager.getInstance().setCookie(libAdMaker.DOMAIN, libAdMaker.this.didCookieString);
                            CookieManager.getInstance().setCookie(libAdMaker.DOMAIN, libAdMaker.this.versionCookieString);
                            CookieManager.getInstance().setCookie(libAdMaker.DOMAIN, libAdMaker.this.osVersionCookieString);
                            CookieManager.getInstance().setCookie(libAdMaker.DOMAIN, libAdMaker.this.deviceModelCookieString);
                            CookieManager.getInstance().setCookie(libAdMaker.DOMAIN, "admaker_sgt=" + sha1String + "; domain=" + libAdMaker.DOMAIN);
                            CookieSyncManager.getInstance().sync();
                        }
                        int expand = url.indexOf("expand.html");
                        if (url.indexOf("exp=2") != -1 && !libAdMaker.this.expandFlag) {
                            libAdMaker.this.expandFlag = true;
                            libAdMaker.this.webView.loadUrl(url);
                            libAdMaker.this.setCloseButton();
                        }
                        if (expand == -1 && (i = url.indexOf("www/delivery/ck.php?oaparams")) != -1 && i < 35) {
                            try {
                                view.stopLoading();
                                libAdMaker.this.ct.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                            } catch (Exception e2) {
                                e2.printStackTrace();
                            }
                        }
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    String title = view.getTitle();
                    int notFound = -1;
                    if (title != null) {
                        notFound = title.indexOf("404");
                    }
                    if (notFound != -1) {
                        libAdMaker.this.viewGone();
                        if (libAdMaker.this.listener != null) {
                            libAdMaker.this.listener.onFailedToReceiveAdMaker(title);
                            return;
                        }
                        return;
                    }
                    if (libAdMaker.this.listener != null && !libAdMaker.this.fastLoaded) {
                        libAdMaker.this.listener.onReceiveAdMaker();
                    }
                    libAdMaker.this.fastLoaded = true;
                    CookieSyncManager.getInstance().sync();
                    url.indexOf("exp=2");
                }

                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    libAdMaker.this.viewGone();
                    if (libAdMaker.this.listener != null) {
                        libAdMaker.this.listener.onFailedToReceiveAdMaker(description);
                    }
                }
            });
        }
    }

    public void closeExpand() {
        Resources r = this.ct.getResources();
        this.button.setVisibility(8);
        this.layout.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (r.getDisplayMetrics().density * 50.0f)));
        this.webView.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (r.getDisplayMetrics().density * 50.0f)));
        this.resume = false;
        startView();
    }

    public void setCloseButton() {
        Resources r = this.ct.getResources();
        this.button.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.layout.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (270.0f * r.getDisplayMetrics().density)));
        this.webView.setLayoutParams(new FrameLayout.LayoutParams(-1, (int) (250.0f * r.getDisplayMetrics().density)));
        this.button.setVisibility(0);
    }

    public void start() {
        this.webSettings = this.webView.getSettings();
        this.webSettings.setJavaScriptEnabled(true);
        this.webView.clearCache(true);
        if (!this.resume) {
            this.fastLoaded = false;
        }
        this.resume = false;
        startView();
    }

    /* access modifiers changed from: private */
    public void viewGone() {
        setVisibility(8);
    }

    public void destroy() {
        this.webView.destroy();
        this.webView = null;
        this.layout = null;
        this.button = null;
    }

    public void stop() {
        this.resume = true;
        this.fastLoaded = true;
    }
}
