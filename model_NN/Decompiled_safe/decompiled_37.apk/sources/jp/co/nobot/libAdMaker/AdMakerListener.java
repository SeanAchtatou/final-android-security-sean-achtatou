package jp.co.nobot.libAdMaker;

public interface AdMakerListener {
    void onFailedToReceiveAdMaker(String str);

    void onReceiveAdMaker();
}
