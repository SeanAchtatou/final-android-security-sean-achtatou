package net.ack_sys.category_notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class AlarmAdapter extends ArrayAdapter<Alarm> {
    private List<Alarm> alarms;
    private Context context;
    private LayoutInflater inflater;

    public AlarmAdapter(Context context2, int textViewResourceId, List<Alarm> alarms2) {
        super(context2, textViewResourceId, alarms2);
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.alarms = alarms2;
    }

    public void setAlarms(List<Alarm> alarms2) {
        this.alarms = alarms2;
    }

    public Alarm getItem(int position) {
        return this.alarms.get(position);
    }

    public int getCount() {
        return this.alarms.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            int minHeight = this.context.getResources().getDimensionPixelSize(R.dimen.row_alarm_height);
            view = this.inflater.inflate((int) R.layout.row_alarm, (ViewGroup) null);
            view.setMinimumHeight(minHeight);
            holder = new ViewHolder(null);
            holder.IV_CATE_ICON = (ImageView) view.findViewById(R.id.IV_CATE_ICON);
            holder.TV_DATETIME = (TextView) view.findViewById(R.id.TV_DATETIME);
            holder.TV_MEMO = (TextView) view.findViewById(R.id.TV_MEMO);
            holder.IV_CATE_DISABLE = (ImageView) view.findViewById(R.id.IV_CATE_DISABLE);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Alarm alarm = getItem(position);
        holder.IV_CATE_ICON.setImageResource(AppUtil.getCateResId(alarm.getIconId()));
        holder.TV_DATETIME.setText(alarm.getAlarmDateTime());
        holder.TV_MEMO.setText(alarm.getMemo());
        if (alarm.getEnable()) {
            holder.IV_CATE_DISABLE.setVisibility(4);
        } else {
            holder.IV_CATE_DISABLE.setVisibility(0);
        }
        return view;
    }

    private static class ViewHolder {
        ImageView IV_CATE_DISABLE;
        ImageView IV_CATE_ICON;
        TextView TV_DATETIME;
        TextView TV_MEMO;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
