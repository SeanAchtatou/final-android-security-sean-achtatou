package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class BackupDbTask extends AsyncTask<String, Void, AsyncTaskResult<?>> {
    private Activity activity = null;
    private BackupDbTaskCallback callback;
    private ProgressDialog progressDialog = null;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((AsyncTaskResult<?>) ((AsyncTaskResult) obj));
    }

    public BackupDbTask(Activity activity2, BackupDbTaskCallback callback2) {
        this.activity = activity2;
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.progressDialog = new ProgressDialog(this.activity);
        this.progressDialog.setMessage(this.activity.getString(R.string.str_msg_backup));
        this.progressDialog.show();
    }

    /* access modifiers changed from: protected */
    public AsyncTaskResult<?> doInBackground(String... params) {
        String dbName = params[0];
        try {
            String pathSd = Environment.getExternalStorageDirectory().getPath() + "/" + this.activity.getPackageName();
            File file = new File(pathSd);
            if (file.exists() || file.mkdirs()) {
                FileChannel channelSource = new FileInputStream(this.activity.getDatabasePath(dbName).getPath()).getChannel();
                FileChannel channelTarget = new FileOutputStream(pathSd + "/" + dbName).getChannel();
                channelSource.transferTo(0, channelSource.size(), channelTarget);
                channelSource.close();
                channelTarget.close();
                return AsyncTaskResult.createNormalResult(this.activity.getString(R.string.str_msg_backupok));
            }
            throw new IOException(this.activity.getString(R.string.str_msg_backuperr));
        } catch (Exception e) {
            return AsyncTaskResult.createErrorResult(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncTaskResult<?> result) {
        this.progressDialog.dismiss();
        if (result.isError()) {
            this.callback.onFailedBackupDb(result.getMessage());
        } else {
            this.callback.onSuccessBackupDb(result.getMessage());
        }
    }
}
