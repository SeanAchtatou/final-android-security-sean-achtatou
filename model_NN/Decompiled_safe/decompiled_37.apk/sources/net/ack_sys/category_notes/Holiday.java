package net.ack_sys.category_notes;

public class Holiday {
    private int holiDay;
    private int holiMonth;
    private int holiYear;
    private String title1;
    private String title2;

    public int getHoliYear() {
        return this.holiYear;
    }

    public void setHoliYear(String holiYear2) {
        this.holiYear = Integer.valueOf(holiYear2.trim()).intValue();
    }

    public int getHoliMonth() {
        return this.holiMonth;
    }

    public void setHoliMonth(String holiMonth2) {
        this.holiMonth = Integer.valueOf(holiMonth2.trim()).intValue() - 1;
    }

    public int getHoliDay() {
        return this.holiDay;
    }

    public void setHoliDay(String holiDay2) {
        this.holiDay = Integer.valueOf(holiDay2.trim()).intValue();
    }

    public String getTitle1() {
        return this.title1;
    }

    public void setTitle1(String title12) {
        this.title1 = title12.trim();
    }

    public String getTitle2() {
        return this.title2;
    }

    public void setTitle2(String title22) {
        this.title2 = title22.trim();
    }
}
