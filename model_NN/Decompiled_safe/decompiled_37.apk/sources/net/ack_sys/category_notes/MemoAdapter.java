package net.ack_sys.category_notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class MemoAdapter extends ArrayAdapter<Memo> {
    private Context context;
    private LayoutInflater inflater;
    private List<Memo> memos;

    public MemoAdapter(Context context2, int textViewResourceId, List<Memo> memos2) {
        super(context2, textViewResourceId, memos2);
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.memos = memos2;
    }

    public void setMemos(List<Memo> memos2) {
        this.memos = memos2;
    }

    public Memo getItem(int position) {
        return this.memos.get(position);
    }

    public int getCount() {
        return this.memos.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Prefs prefs = new Prefs(this.context);
        View view = convertView;
        if (view == null) {
            int minHeight = this.context.getResources().getDimensionPixelSize(R.dimen.row_memo_height);
            view = this.inflater.inflate((int) R.layout.row_memo, (ViewGroup) null);
            view.setMinimumHeight(minHeight);
            holder = new ViewHolder(null);
            holder.IV_TAG_ICON = (ImageView) view.findViewById(R.id.IV_TAG_ICON);
            holder.TV_DATETIME = (TextView) view.findViewById(R.id.TV_DATETIME);
            holder.TV_MEMO = (TextView) view.findViewById(R.id.TV_MEMO);
            holder.IV_PHOTO_ICON = (ImageView) view.findViewById(R.id.IV_PHOTO_ICON);
            holder.IV_SCHEDULE_ICON = (ImageView) view.findViewById(R.id.IV_SCHEDULE_ICON);
            holder.IV_ALARM_ICON = (ImageView) view.findViewById(R.id.IV_ALARM_ICON);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Memo memo = getItem(position);
        if (prefs.getTagUse()) {
            holder.IV_TAG_ICON.setImageResource(AppUtil.getTagResId(this.context, memo.getTagId()));
            holder.IV_TAG_ICON.setVisibility(0);
        } else {
            holder.IV_TAG_ICON.setImageDrawable(null);
            holder.IV_TAG_ICON.setVisibility(8);
        }
        switch (Integer.valueOf(prefs.getSortOrder()).intValue()) {
            case 0:
            case DBEngine.DB_VERSION:
            case 4:
            case 5:
                holder.TV_DATETIME.setText(memo.getCreateTime().subSequence(0, 16));
                break;
            case 1:
            case 3:
                holder.TV_DATETIME.setText(memo.getUpdateTime().subSequence(0, 16));
                break;
        }
        holder.TV_MEMO.setText(memo.getMemo());
        if (memo.getPhoto()) {
            holder.IV_PHOTO_ICON.setVisibility(0);
        } else {
            holder.IV_PHOTO_ICON.setVisibility(4);
        }
        if (memo.getSchedule()) {
            holder.IV_SCHEDULE_ICON.setVisibility(0);
        } else {
            holder.IV_SCHEDULE_ICON.setVisibility(4);
        }
        if (memo.getAlarm()) {
            holder.IV_ALARM_ICON.setVisibility(0);
        } else {
            holder.IV_ALARM_ICON.setVisibility(4);
        }
        if (prefs.getDateDisp()) {
            holder.TV_DATETIME.setVisibility(0);
        } else {
            holder.TV_DATETIME.setVisibility(8);
        }
        return view;
    }

    private static class ViewHolder {
        ImageView IV_ALARM_ICON;
        ImageView IV_PHOTO_ICON;
        ImageView IV_SCHEDULE_ICON;
        ImageView IV_TAG_ICON;
        TextView TV_DATETIME;
        TextView TV_MEMO;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
