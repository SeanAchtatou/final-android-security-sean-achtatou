package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class ActivityMsg extends Activity implements View.OnClickListener, View.OnLongClickListener {
    private ImageView IV_CATE_ICON;
    private ImageView IV_DEL;
    private ImageView IV_SHARE;
    private ImageView IV_TAG_ICON;
    private LinearLayout LL_SCHEDULE;
    /* access modifiers changed from: private */
    public TextView TV_CATE_TITLE;
    private TextView TV_DATETIME;
    /* access modifiers changed from: private */
    public TextView TV_MEMO;
    private TextView TV_SCHEDULE;
    /* access modifiers changed from: private */
    public AlertDialog alarmDialog = null;
    private int alarmRecno;
    private int alarmVol;
    private KeyguardManager.KeyguardLock keyguardlock;
    private KeyguardManager keyguardmanager;
    /* access modifiers changed from: private */
    public String location;
    private MediaPlayer mediaPlayer;
    /* access modifiers changed from: private */
    public int memoRecno;
    /* access modifiers changed from: private */
    public String photoUri;
    /* access modifiers changed from: private */
    public int snoozeCnt;
    /* access modifiers changed from: private */
    public TimerClass timer = new TimerClass();
    private long timerDelay = 60000;
    private Vibrator vibrator;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock wakelock;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.keyguardmanager = (KeyguardManager) getSystemService("keyguard");
        this.keyguardlock = this.keyguardmanager.newKeyguardLock("FindMyPhone");
        this.keyguardlock.disableKeyguard();
        this.keyguardmanager.exitKeyguardSecurely(new KeyguardManager.OnKeyguardExitResult() {
            public void onKeyguardExitResult(boolean success) {
                ActivityMsg.this.wakelock = ((PowerManager) ActivityMsg.this.getSystemService("power")).newWakeLock(10, "FindMyPhone");
                ActivityMsg.this.wakelock.acquire();
            }
        });
        setVolumeControlStream(3);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        AppInfo.getInstance().setScaledDensity(metrics.scaledDensity);
        setContentView((int) R.layout.act_msg);
        getWindow().addFlags(128);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.alarmRecno = extras.getInt("alarmRecno");
            this.memoRecno = AppUtil.getMemoRecno(this, this.alarmRecno);
        }
        this.TV_DATETIME = (TextView) findViewById(R.id.TV_DATETIME);
        this.IV_CATE_ICON = (ImageView) findViewById(R.id.IV_CATE_ICON);
        this.TV_CATE_TITLE = (TextView) findViewById(R.id.TV_CATE_TITLE);
        this.IV_TAG_ICON = (ImageView) findViewById(R.id.IV_TAG_ICON);
        this.TV_MEMO = (TextView) findViewById(R.id.TV_MEMO);
        this.TV_MEMO.setOnLongClickListener(this);
        this.IV_SHARE = (ImageView) findViewById(R.id.IV_SHARE);
        this.IV_SHARE.setOnClickListener(this);
        this.IV_DEL = (ImageView) findViewById(R.id.IV_DEL);
        this.IV_DEL.setOnClickListener(this);
        this.LL_SCHEDULE = (LinearLayout) findViewById(R.id.LL_SCHEDULE);
        this.TV_SCHEDULE = (TextView) findViewById(R.id.TV_SCHEDULE);
        setView();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.wakelock.isHeld()) {
            this.wakelock.release();
        }
        this.keyguardlock.reenableKeyguard();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.timer.removeMessages(0);
        this.timer = null;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.IV_DEL /*2131296609*/:
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle((int) R.string.str_check);
                alert.setMessage((int) R.string.str_msg_remove);
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppUtil.deleteMemo(ActivityMsg.this, ActivityMsg.this.memoRecno);
                        ActivityMsg.this.finish();
                    }
                });
                alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                alert.show();
                return;
            case R.id.IV_SHARE /*2131296646*/:
                final Intent intent = new Intent("android.intent.action.SEND");
                if (!AppUtil.checkNull(this.photoUri).booleanValue()) {
                    AlertDialog.Builder alert2 = new AlertDialog.Builder(this);
                    alert2.setTitle((int) R.string.str_check);
                    alert2.setMessage((int) R.string.str_msg_attached);
                    alert2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            intent.setType("image/*");
                            intent.putExtra("android.intent.extra.TITLE", ActivityMsg.this.TV_CATE_TITLE.getText().toString().trim());
                            intent.putExtra("android.intent.extra.SUBJECT", ActivityMsg.this.TV_CATE_TITLE.getText().toString().trim());
                            intent.putExtra("android.intent.extra.TEXT", ActivityMsg.this.TV_MEMO.getText().toString().trim());
                            intent.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + ActivityMsg.this.photoUri));
                            ActivityMsg.this.startActivity(Intent.createChooser(intent, ActivityMsg.this.getString(R.string.str_menu_share)));
                        }
                    });
                    alert2.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            intent.setType("text/plain");
                            intent.putExtra("android.intent.extra.TITLE", ActivityMsg.this.TV_CATE_TITLE.getText().toString().trim());
                            intent.putExtra("android.intent.extra.SUBJECT", ActivityMsg.this.TV_CATE_TITLE.getText().toString().trim());
                            intent.putExtra("android.intent.extra.TEXT", ActivityMsg.this.TV_MEMO.getText().toString().trim());
                            ActivityMsg.this.startActivity(Intent.createChooser(intent, ActivityMsg.this.getString(R.string.str_menu_share)));
                        }
                    });
                    alert2.show();
                    return;
                }
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TITLE", this.TV_CATE_TITLE.getText().toString().trim());
                intent.putExtra("android.intent.extra.SUBJECT", this.TV_CATE_TITLE.getText().toString().trim());
                intent.putExtra("android.intent.extra.TEXT", this.TV_MEMO.getText().toString().trim());
                startActivity(Intent.createChooser(intent, getString(R.string.str_menu_share)));
                return;
            default:
                return;
        }
    }

    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.TV_MEMO /*2131296642*/:
                CharSequence[] searchs = (TextUtils.isEmpty(this.photoUri) || TextUtils.isEmpty(this.location)) ? !TextUtils.isEmpty(this.photoUri) ? new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2), getString(R.string.str_preview)} : !TextUtils.isEmpty(this.location) ? new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2), getString(R.string.str_map)} : new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2)} : new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2), getString(R.string.str_preview), getString(R.string.str_map)};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            AppUtil.copyClipboard(ActivityMsg.this, ActivityMsg.this.TV_MEMO.getText().toString());
                        } else if (which == 1) {
                            List<String> listTmp = Arrays.asList(ActivityMsg.this.TV_MEMO.getText().toString().split("\n"));
                            List<String> listMemo = new ArrayList<>();
                            for (String memo : listTmp) {
                                if (!TextUtils.isEmpty(memo)) {
                                    listMemo.add(memo);
                                }
                            }
                            final CharSequence[] items = (CharSequence[]) listMemo.toArray(new CharSequence[listMemo.size()]);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMsg.this);
                            builder.setTitle((int) R.string.str_clipboard);
                            builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.copyClipboard(ActivityMsg.this, items[which].toString());
                                }
                            });
                            builder.create().show();
                        } else if (which != 2) {
                            AppUtil.startMap(ActivityMsg.this, ActivityMsg.this.location);
                        } else if (!TextUtils.isEmpty(ActivityMsg.this.photoUri)) {
                            AppUtil.startImageViewer(ActivityMsg.this, ActivityMsg.this.photoUri);
                        } else {
                            AppUtil.startMap(ActivityMsg.this, ActivityMsg.this.location);
                        }
                    }
                });
                builder.create().show();
                break;
        }
        return false;
    }

    private void setView() {
        dispMemo(this.memoRecno);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0201 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x03e6 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x043a A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x04b7 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x04c5 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0511 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0530  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0648  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0657 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0666 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0675 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0684 A[Catch:{ Exception -> 0x051e, all -> 0x0636 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void dispMemo(int r37) {
        /*
            r36 = this;
            net.ack_sys.category_notes.Prefs r33 = new net.ack_sys.category_notes.Prefs
            r0 = r33
            r1 = r36
            r0.<init>(r1)
            net.ack_sys.category_notes.DBEngine r26 = new net.ack_sys.category_notes.DBEngine
            r0 = r26
            r1 = r36
            r0.<init>(r1)
            android.database.sqlite.SQLiteDatabase r25 = r26.getReadableDatabase()
            r3 = 7
            int[] r10 = new int[r3]
            java.lang.StringBuilder r34 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r34.<init>()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " SELECT"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.cate_id,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " C.title,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " C.icon_id,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " C.music_uri,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.memo,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.photo_name,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.photo_uri,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.tag_id,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_s_year,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_s_month,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_s_day,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_s_hour,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_s_minute,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_s_time,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_e_year,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_e_month,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_e_day,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_e_hour,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_e_minute,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_e_time,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " M.sche_location,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_year,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_month,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_day,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_hour,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_minute,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_loop,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week1,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week2,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week3,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week4,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week5,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week6,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.alarm_week7,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.recno,"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " A.snooze"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " FROM MEMO M"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " INNER JOIN CATEGORY C ON"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = "       M.cate_id = C.id"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " INNER JOIN ALARM A ON"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = "       M.recno = A.memo_no"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = " WHERE M.recno = "
            r3.<init>(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r37)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = net.ack_sys.category_notes.AppUtil.sql9(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = " ORDER BY A.recno DESC"
            r0 = r34
            r1 = r3
            r0.append(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = r34.toString()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r4 = 0
            r0 = r25
            r1 = r3
            r2 = r4
            android.database.Cursor r27 = r0.rawQuery(r1, r2)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            boolean r3 = r27.moveToFirst()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            if (r3 == 0) goto L_0x04d1
            java.lang.String r3 = "audio"
            r0 = r36
            r1 = r3
            java.lang.Object r28 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.media.AudioManager r28 = (android.media.AudioManager) r28     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            int r3 = r28.getRingerMode()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            switch(r3) {
                case 0: goto L_0x04e5;
                case 1: goto L_0x04e5;
                default: goto L_0x01b8;
            }     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x01b8:
            r31 = 0
        L_0x01ba:
            if (r31 != 0) goto L_0x01c2
            boolean r3 = r33.getVibrate()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            if (r3 == 0) goto L_0x01ff
        L_0x01c2:
            java.lang.String r3 = "vibrator"
            r0 = r36
            r1 = r3
            java.lang.Object r37 = r0.getSystemService(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.os.Vibrator r37 = (android.os.Vibrator) r37     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r37
            r1 = r36
            r1.vibrator = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 6
            r0 = r3
            long[] r0 = new long[r0]     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r32 = r0
            r3 = 1
            r5 = 1000(0x3e8, double:4.94E-321)
            r32[r3] = r5     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2
            r5 = 500(0x1f4, double:2.47E-321)
            r32[r3] = r5     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 3
            r5 = 300(0x12c, double:1.48E-321)
            r32[r3] = r5     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 4
            r5 = 100
            r32[r3] = r5     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 5
            r5 = 50
            r32[r3] = r5     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.os.Vibrator r0 = r0.vibrator     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r4 = 0
            r0 = r3
            r1 = r32
            r2 = r4
            r0.vibrate(r1, r2)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x01ff:
            if (r31 != 0) goto L_0x0229
            java.lang.String r3 = "music_uri"
            r0 = r27
            r1 = r3
            int r3 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r3
            java.lang.String r3 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.Boolean r3 = net.ack_sys.category_notes.AppUtil.checkNull(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            boolean r3 = r3.booleanValue()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            if (r3 == 0) goto L_0x04fb
            r3 = 1
            android.net.Uri r35 = android.media.RingtoneManager.getDefaultUri(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x0220:
            r0 = r36
            r1 = r36
            r2 = r35
            r0.alarmPlay(r1, r2)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x0229:
            r3 = 0
            java.lang.String r4 = "alarm_week1"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 1
            java.lang.String r4 = "alarm_week2"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2
            java.lang.String r4 = "alarm_week3"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 3
            java.lang.String r4 = "alarm_week4"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 4
            java.lang.String r4 = "alarm_week5"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 5
            java.lang.String r4 = "alarm_week6"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 6
            java.lang.String r4 = "alarm_week7"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r10[r3] = r4     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.content.res.Resources r3 = r36.getResources()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "alarm_year"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r5 = "alarm_month"
            r0 = r27
            r1 = r5
            int r5 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r5
            int r5 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r6 = "alarm_day"
            r0 = r27
            r1 = r6
            int r6 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r6
            int r6 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r7 = "alarm_hour"
            r0 = r27
            r1 = r7
            int r7 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r7
            int r7 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r8 = "alarm_minute"
            r0 = r27
            r1 = r8
            int r8 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r8
            int r8 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r9 = "alarm_loop"
            r0 = r27
            r1 = r9
            int r9 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r9
            int r9 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.CharSequence r29 = net.ack_sys.category_notes.AppUtil.getDateTimeStr(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.view.LayoutInflater r30 = android.view.LayoutInflater.from(r36)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2130903052(0x7f03000c, float:1.7412911E38)
            r4 = 0
            r0 = r30
            r1 = r3
            r2 = r4
            android.view.View r4 = r0.inflate(r1, r2)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2131296658(0x7f090192, float:1.8211239E38)
            android.view.View r37 = r4.findViewById(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.widget.TextView r37 = (android.widget.TextView) r37     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r37
            r1 = r29
            r0.setText(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2131296659(0x7f090193, float:1.821124E38)
            android.view.View r37 = r4.findViewById(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.widget.Button r37 = (android.widget.Button) r37     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            net.ack_sys.category_notes.ActivityMsg$6 r3 = new net.ack_sys.category_notes.ActivityMsg$6     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r0.<init>()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r37
            r1 = r3
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2131296660(0x7f090194, float:1.8211243E38)
            android.view.View r37 = r4.findViewById(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.widget.Button r37 = (android.widget.Button) r37     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            net.ack_sys.category_notes.ActivityMsg$7 r3 = new net.ack_sys.category_notes.ActivityMsg$7     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r0.<init>()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r37
            r1 = r3
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = 2131296661(0x7f090195, float:1.8211245E38)
            android.view.View r37 = r4.findViewById(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.widget.Button r37 = (android.widget.Button) r37     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            net.ack_sys.category_notes.ActivityMsg$8 r3 = new net.ack_sys.category_notes.ActivityMsg$8     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r0.<init>()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r37
            r1 = r3
            r0.setOnClickListener(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.app.AlertDialog$Builder r3 = new android.app.AlertDialog$Builder     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r0.<init>(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = 0
            android.app.AlertDialog$Builder r3 = r3.setCancelable(r5)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = 2131099738(0x7f06005a, float:1.7811838E38)
            android.app.AlertDialog$Builder r3 = r3.setTitle(r5)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.app.AlertDialog r3 = r3.create()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r1.alarmDialog = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.app.AlertDialog r0 = r0.alarmDialog     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r3.setView(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.app.AlertDialog r0 = r0.alarmDialog     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r3.show()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.widget.ImageView r0 = r0.IV_CATE_ICON     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            java.lang.String r4 = "icon_id"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            int r4 = net.ack_sys.category_notes.AppUtil.getCateResId(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setImageResource(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.widget.TextView r0 = r0.TV_CATE_TITLE     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            java.lang.String r4 = "title"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setText(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            boolean r3 = r33.getTagUse()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            if (r3 == 0) goto L_0x0511
            r0 = r36
            android.widget.ImageView r0 = r0.IV_TAG_ICON     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            java.lang.String r4 = "tag_id"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r4 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            r1 = r4
            int r4 = net.ack_sys.category_notes.AppUtil.getTagResId(r0, r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setImageResource(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x0405:
            r0 = r36
            android.widget.TextView r0 = r0.TV_MEMO     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            java.lang.String r4 = "memo"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            java.lang.String r4 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setText(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.widget.TextView r0 = r0.TV_DATETIME     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r0 = r3
            r1 = r29
            r0.setText(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = "sche_s_year"
            r0 = r27
            r1 = r3
            int r3 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r3
            int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            if (r3 != 0) goto L_0x0530
            r0 = r36
            android.widget.LinearLayout r0 = r0.LL_SCHEDULE     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = -1
            r6 = 0
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setLayoutParams(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = ""
            r0 = r3
            r1 = r36
            r1.location = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x0450:
            java.lang.String r3 = "photo_uri"
            r0 = r27
            r1 = r3
            int r3 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r3
            java.lang.String r3 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r1.photoUri = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.content.res.Resources r3 = r36.getResources()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            java.lang.String r0 = r0.photoUri     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r4 = r0
            r0 = r36
            android.widget.TextView r0 = r0.TV_MEMO     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            net.ack_sys.category_notes.AppUtil.showPhoto2(r3, r4, r5)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = 60000(0xea60, double:2.9644E-319)
            r0 = r5
            r2 = r36
            r2.timerDelay = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = r33.getAutoStop()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            switch(r3) {
                case 0: goto L_0x0648;
                case 1: goto L_0x0657;
                case 2: goto L_0x0666;
                case 3: goto L_0x0675;
                case 4: goto L_0x0684;
                default: goto L_0x048d;
            }     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x048d:
            r0 = r36
            net.ack_sys.category_notes.ActivityMsg$TimerClass r0 = r0.timer     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r0 = r36
            long r0 = r0.timerDelay     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            r3.sleep(r5)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = "snooze"
            r0 = r27
            r1 = r3
            int r3 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r3
            int r3 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r1.snoozeCnt = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            int r0 = r0.snoozeCnt     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r4 = 1
            if (r3 != r4) goto L_0x04bd
            r3 = 0
            r0 = r3
            r1 = r36
            r1.snoozeCnt = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x04bd:
            r0 = r36
            int r0 = r0.snoozeCnt     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r4 = 2
            if (r3 < r4) goto L_0x04d1
            r0 = r36
            int r0 = r0.snoozeCnt     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r4 = 1
            int r3 = r3 - r4
            r0 = r3
            r1 = r36
            r1.snoozeCnt = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
        L_0x04d1:
            r27.close()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r25.close()
            r0 = r36
            int r0 = r0.alarmRecno
            r3 = r0
            r4 = 1
            r0 = r36
            r1 = r3
            r2 = r4
            net.ack_sys.category_notes.AppUtil.deleteAlarm(r0, r1, r2)
        L_0x04e4:
            return
        L_0x04e5:
            java.lang.String r3 = r33.getManner()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            if (r3 != 0) goto L_0x04f7
            r31 = 1
            goto L_0x01ba
        L_0x04f7:
            r31 = 0
            goto L_0x01ba
        L_0x04fb:
            java.lang.String r3 = "music_uri"
            r0 = r27
            r1 = r3
            int r3 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r3
            java.lang.String r3 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            android.net.Uri r35 = android.net.Uri.parse(r3)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x0220
        L_0x0511:
            r0 = r36
            android.widget.ImageView r0 = r0.IV_TAG_ICON     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            r4 = 2130837538(0x7f020022, float:1.7280033E38)
            r3.setImageResource(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x0405
        L_0x051e:
            r3 = move-exception
            r25.close()
            r0 = r36
            int r0 = r0.alarmRecno
            r3 = r0
            r4 = 1
            r0 = r36
            r1 = r3
            r2 = r4
            net.ack_sys.category_notes.AppUtil.deleteAlarm(r0, r1, r2)
            goto L_0x04e4
        L_0x0530:
            r0 = r36
            android.widget.LinearLayout r0 = r0.LL_SCHEDULE     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = -1
            r6 = -2
            r4.<init>(r5, r6)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setLayoutParams(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r36
            android.widget.TextView r0 = r0.TV_SCHEDULE     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3 = r0
            android.content.res.Resources r11 = r36.getResources()     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_s_year"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r12 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_s_month"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r13 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_s_day"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r14 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_s_hour"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r15 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_s_minute"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r16 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_s_time"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            java.lang.String r17 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_e_year"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r18 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_e_month"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r19 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_e_day"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r20 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_e_hour"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r21 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_e_minute"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            int r22 = r0.getInt(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_e_time"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            java.lang.String r23 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r4 = "sche_location"
            r0 = r27
            r1 = r4
            int r4 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r4
            java.lang.String r24 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.CharSequence r4 = net.ack_sys.category_notes.AppUtil.getScheduleStr(r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r3.setText(r4)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            java.lang.String r3 = "sche_location"
            r0 = r27
            r1 = r3
            int r3 = r0.getColumnIndex(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r27
            r1 = r3
            java.lang.String r3 = r0.getString(r1)     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r0 = r3
            r1 = r36
            r1.location = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x0450
        L_0x0636:
            r3 = move-exception
            r25.close()
            r0 = r36
            int r0 = r0.alarmRecno
            r4 = r0
            r5 = 1
            r0 = r36
            r1 = r4
            r2 = r5
            net.ack_sys.category_notes.AppUtil.deleteAlarm(r0, r1, r2)
            throw r3
        L_0x0648:
            r0 = r36
            long r0 = r0.timerDelay     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            r7 = 1
            long r5 = r5 * r7
            r0 = r5
            r2 = r36
            r2.timerDelay = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x048d
        L_0x0657:
            r0 = r36
            long r0 = r0.timerDelay     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            r7 = 2
            long r5 = r5 * r7
            r0 = r5
            r2 = r36
            r2.timerDelay = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x048d
        L_0x0666:
            r0 = r36
            long r0 = r0.timerDelay     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            r7 = 3
            long r5 = r5 * r7
            r0 = r5
            r2 = r36
            r2.timerDelay = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x048d
        L_0x0675:
            r0 = r36
            long r0 = r0.timerDelay     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            r7 = 4
            long r5 = r5 * r7
            r0 = r5
            r2 = r36
            r2.timerDelay = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x048d
        L_0x0684:
            r0 = r36
            long r0 = r0.timerDelay     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            r5 = r0
            r7 = 5
            long r5 = r5 * r7
            r0 = r5
            r2 = r36
            r2.timerDelay = r0     // Catch:{ Exception -> 0x051e, all -> 0x0636 }
            goto L_0x048d
        */
        throw new UnsupportedOperationException("Method not decompiled: net.ack_sys.category_notes.ActivityMsg.dispMemo(int):void");
    }

    /* access modifiers changed from: private */
    public void setAlarm(int snooze) {
        Prefs prefs = new Prefs(this);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        switch (Integer.valueOf(prefs.getSnooze()).intValue()) {
            case 0:
                cal.add(12, 3);
                break;
            case 1:
                cal.add(12, 5);
                break;
            case DBEngine.DB_VERSION /*2*/:
                cal.add(12, 10);
                break;
        }
        updateAlarm(cal.get(1), cal.get(2), cal.get(5), cal.get(11), cal.get(12), snooze);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void updateAlarm(int year, int month, int day, int hour, int minute, int snooze) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put("memo_no", Integer.valueOf(this.memoRecno));
            values.put(Alarm.ALARM_YEAR, Integer.valueOf(year));
            values.put(Alarm.ALARM_MONTH, Integer.valueOf(month));
            values.put(Alarm.ALARM_DAY, Integer.valueOf(day));
            values.put(Alarm.ALARM_HOUR, Integer.valueOf(hour));
            values.put(Alarm.ALARM_MINUTE, Integer.valueOf(minute));
            values.put("enable", (Integer) 1);
            values.put(Alarm.SNOOZE, Integer.valueOf(snooze));
            DB.insert("ALARM", null, values);
            Cursor RS = DB.rawQuery(" SELECT last_insert_rowid()", null);
            if (RS.moveToFirst()) {
                AppUtil.setAlarmManager(this, RS.getInt(0), year, month, day, hour, minute);
            }
            RS.close();
            DB.setTransactionSuccessful();
            DB.endTransaction();
            DB.close();
            Toast.makeText(this, ((Object) AppUtil.getDateTimeStr(getResources(), year, month, day, hour, minute, 0, null)) + "\n\n" + getString(R.string.str_msg_setting), 1).show();
            finish();
        } catch (Exception e) {
            DB.endTransaction();
            DB.close();
            Toast.makeText(this, ((Object) AppUtil.getDateTimeStr(getResources(), year, month, day, hour, minute, 0, null)) + "\n\n" + getString(R.string.str_msg_setting), 1).show();
            finish();
        } catch (Throwable th) {
            Throwable th2 = th;
            DB.endTransaction();
            DB.close();
            Toast.makeText(this, ((Object) AppUtil.getDateTimeStr(getResources(), year, month, day, hour, minute, 0, null)) + "\n\n" + getString(R.string.str_msg_setting), 1).show();
            finish();
            throw th2;
        }
    }

    private void alarmPlay(Context context, Uri uri) {
        Prefs prefs = new Prefs(this);
        AudioManager audio = (AudioManager) getSystemService("audio");
        float setVolume = (((float) audio.getStreamMaxVolume(4)) / 7.0f) * Float.valueOf(prefs.getVolume()).floatValue();
        this.alarmVol = audio.getStreamVolume(4);
        audio.setStreamVolume(4, Math.round(setVolume), 0);
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer.setAudioStreamType(4);
        this.mediaPlayer.setLooping(true);
        try {
            this.mediaPlayer.setDataSource(context, uri);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e2) {
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        try {
            this.mediaPlayer.prepare();
        } catch (IllegalStateException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
        this.mediaPlayer.start();
    }

    /* access modifiers changed from: private */
    public void alarmStop() {
        if (this.mediaPlayer != null) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer.reset();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
            ((AudioManager) getSystemService("audio")).setStreamVolume(4, this.alarmVol, 0);
        }
        if (this.vibrator != null) {
            this.vibrator.cancel();
        }
    }

    class TimerClass extends Handler {
        TimerClass() {
        }

        public void handleMessage(Message msg) {
            ActivityMsg.this.alarmStop();
            ActivityMsg.this.getWindow().clearFlags(128);
            Prefs prefs = new Prefs(ActivityMsg.this);
            if (prefs.getAutoSnooze() && ActivityMsg.this.snoozeCnt < Integer.valueOf(prefs.getSnoozeCnt()).intValue()) {
                ActivityMsg.this.alarmDialog.dismiss();
                ActivityMsg.this.setAlarm(ActivityMsg.this.snoozeCnt + 2);
            }
        }

        public void sleep(long delayMillis) {
            removeMessages(0);
            sendMessageDelayed(obtainMessage(0), delayMillis);
        }
    }
}
