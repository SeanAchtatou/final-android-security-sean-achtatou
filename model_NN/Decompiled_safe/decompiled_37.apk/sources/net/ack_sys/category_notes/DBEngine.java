package net.ack_sys.category_notes;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBEngine extends SQLiteOpenHelper {
    public static final String DB_NAME = "CATEGORY_NOTES_DB";
    public static final int DB_VERSION = 2;
    private Context context;

    public DBEngine(Context context2) {
        super(context2, DB_NAME, (SQLiteDatabase.CursorFactory) null, 2);
        this.context = context2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE CATEGORY" + "(" + "id         INTEGER," + "title      TEXT," + "music_name TEXT," + "music_uri  TEXT," + "icon_id    INTEGER," + "sort       INTEGER," + "enable     INTEGER," + "PRIMARY KEY(id))");
        StringBuilder sql = new StringBuilder();
        sql.append("CREATE TABLE TAG");
        sql.append("(");
        sql.append("id         INTEGER,");
        sql.append("title      TEXT,");
        sql.append("sort       INTEGER,");
        sql.append("enable     INTEGER,");
        sql.append("PRIMARY KEY(id))");
        db.execSQL(sql.toString());
        int i = 1;
        while (i <= 45) {
            ContentValues values = new ContentValues();
            values.put("id", Integer.valueOf(i));
            values.put("title", this.context.getString(AppUtil.getCateTitleResId(i)));
            values.put(Category.MUSIC_NAME, this.context.getString(R.string.str_normalringtone));
            values.put(Category.ICON_ID, Integer.valueOf(i));
            values.put("sort", Integer.valueOf(i));
            values.put("enable", Integer.valueOf(i <= 10 ? 1 : 0));
            db.insert("CATEGORY", null, values);
            i++;
        }
        for (int i2 = 1; i2 <= 16; i2++) {
            ContentValues values2 = new ContentValues();
            values2.put("id", Integer.valueOf(i2));
            values2.put("title", this.context.getString(AppUtil.getTagTitleResId(i2)));
            values2.put("sort", Integer.valueOf(i2));
            values2.put("enable", (Integer) 1);
            db.insert("TAG", null, values2);
        }
        db.execSQL("CREATE TABLE MEMO" + "(" + "recno         INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + "cate_id       INTEGER," + "tag_id        INTEGER," + "memo          TEXT," + "photo_name    TEXT," + "photo_uri     TEXT," + "sche_s_year   INTEGER," + "sche_s_month  INTEGER," + "sche_s_day    INTEGER," + "sche_s_hour   INTEGER," + "sche_s_minute INTEGER," + "sche_s_time   TEXT," + "sche_e_year   INTEGER," + "sche_e_month  INTEGER," + "sche_e_day    INTEGER," + "sche_e_hour   INTEGER," + "sche_e_minute INTEGER," + "sche_e_time   TEXT," + "sche_location TEXT," + "other_txt1    TEXT," + "other_txt2    TEXT," + "other_txt3    TEXT," + "other_txt4    TEXT," + "other_txt5    TEXT," + "other_int1    INTEGER," + "other_int2    INTEGER," + "other_int3    INTEGER," + "other_int4    INTEGER," + "other_int5    INTEGER," + "create_time   TEXT," + "update_time   TEXT)");
        StringBuilder sql2 = new StringBuilder();
        sql2.append("CREATE TABLE SCHEDULE");
        sql2.append("(");
        sql2.append("recno      INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql2.append("memo_no    INTEGER,");
        sql2.append("sche_year  INTEGER,");
        sql2.append("sche_month INTEGER,");
        sql2.append("sche_day   INTEGER,");
        sql2.append("day_flg    INTEGER)");
        db.execSQL(sql2.toString());
        StringBuilder sql3 = new StringBuilder();
        sql3.append("CREATE TABLE ALARM");
        sql3.append("(");
        sql3.append("recno        INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,");
        sql3.append("memo_no      INTEGER,");
        sql3.append("alarm_year   INTEGER,");
        sql3.append("alarm_month  INTEGER,");
        sql3.append("alarm_day    INTEGER,");
        sql3.append("alarm_hour   INTEGER,");
        sql3.append("alarm_minute INTEGER,");
        sql3.append("alarm_loop   INTEGER,");
        sql3.append("alarm_week1  INTEGER,");
        sql3.append("alarm_week2  INTEGER,");
        sql3.append("alarm_week3  INTEGER,");
        sql3.append("alarm_week4  INTEGER,");
        sql3.append("alarm_week5  INTEGER,");
        sql3.append("alarm_week6  INTEGER,");
        sql3.append("alarm_week7  INTEGER,");
        sql3.append("enable       INTEGER,");
        sql3.append("snooze       INTEGER)");
        db.execSQL(sql3.toString());
        db.execSQL("CREATE TABLE HOLIDAY" + "(" + "holi_year  INTEGER," + "holi_month INTEGER," + "holi_day   INTEGER," + "title1     TEXT," + "title2     TEXT," + "PRIMARY KEY(holi_year,holi_month,holi_day))");
        createV2(db);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion == 1 && newVersion == 2) {
            createV2(db);
        }
    }

    private void createV2(SQLiteDatabase db) {
        db.execSQL("CREATE INDEX MEMO_IDX1 ON MEMO(cate_id)");
        db.execSQL("CREATE INDEX SCHEDULE_IDX1 ON SCHEDULE(memo_no)");
        db.execSQL("CREATE INDEX SCHEDULE_IDX2 ON SCHEDULE(sche_year,sche_month,sche_day)");
        db.execSQL("CREATE INDEX ALARM_IDX1 ON ALARM(memo_no)");
        db.execSQL("CREATE INDEX ALARM_IDX2 ON ALARM(alarm_year,alarm_month,alarm_day,alarm_hour,alarm_minute)");
        SQLiteDatabase DBS = null;
        try {
            DBS = this.context.openOrCreateDatabase(DB_NAME, 0, null);
            DBS.execSQL("vacuum");
            DBS.execSQL("reindex");
            if (DBS != null) {
                try {
                    DBS.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            if (DBS != null) {
                try {
                    DBS.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (DBS != null) {
                try {
                    DBS.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }
}
