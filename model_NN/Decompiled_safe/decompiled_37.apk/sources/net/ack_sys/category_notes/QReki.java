package net.ack_sys.category_notes;

import java.lang.reflect.Array;

public class QReki {
    private static double[][] chu = ((double[][]) Array.newInstance(Double.TYPE, 4, 2));
    private static int day;
    private static int i;
    private static int[] kyureki = new int[4];
    private static int month;
    private static double[][] nibun = ((double[][]) Array.newInstance(Double.TYPE, 4, 2));
    private static double[] saku = new double[5];
    private static double tm;
    private static int year;

    public static String RokuYo(int yyyy, int mm, int dd) {
        year = yyyy;
        month = mm;
        day = dd;
        tm = YMDT2JD(year, month, day, 0, 0, 0);
        calc_kyureki(tm);
        return new String[]{"先勝", "友引", "先負", "仏滅", "大安", "赤口"}[((kyureki[2] + kyureki[3]) - 2) % 6];
    }

    static void calc_kyureki(double tm0) {
        int lap;
        kyureki[0] = year;
        kyureki[2] = month;
        kyureki[3] = day;
        before_nibun(tm0);
        chu[0][0] = nibun[0][0];
        chu[0][1] = nibun[0][1];
        i = 1;
        while (i < 4) {
            calc_chu(chu[i - 1][0] + 32.0d);
            i++;
        }
        saku[0] = calc_saku(chu[0][0]);
        i = 1;
        while (i < 5) {
            tm = saku[i - 1];
            tm += 30.0d;
            saku[i] = calc_saku(tm);
            if (abs((double) (((int) saku[i - 1]) - ((int) saku[i]))) <= 26.0d) {
                saku[i] = calc_saku(saku[i - 1] + 35.0d);
            }
            i++;
        }
        if (((int) saku[1]) <= ((int) chu[0][0])) {
            i = 0;
            while (i < 5) {
                saku[i] = saku[i + 1];
                i++;
            }
            saku[4] = calc_saku(saku[3] + 35.0d);
        } else if (((int) saku[0]) > ((int) chu[0][0])) {
            i = 4;
            while (i > 0) {
                saku[i] = saku[i - 1];
                i--;
            }
            saku[0] = calc_saku(saku[0] - 27.0d);
        }
        if (((int) saku[4]) <= ((int) chu[3][0])) {
            lap = 1;
        } else {
            lap = 0;
        }
        int[][] m = (int[][]) Array.newInstance(Integer.TYPE, 5, 3);
        m[0][0] = ((int) (chu[0][1] / 30.0d)) + 2;
        if (m[0][1] > 12) {
            int[] iArr = m[0];
            iArr[0] = iArr[0] - 12;
        }
        m[0][2] = (int) saku[0];
        m[0][1] = 0;
        i = 1;
        while (i < 5) {
            if (lap == 1 && i != 1 && (((int) chu[i - 1][0]) <= ((int) saku[i - 1]) || ((int) chu[i - 1][0]) >= ((int) saku[i]))) {
                m[i - 1][0] = m[i - 2][0];
                m[i - 1][1] = 1;
                m[i - 1][2] = (int) saku[i - 1];
                lap = 0;
            }
            m[i][0] = m[i - 1][0] + 1;
            if (m[i][0] > 12) {
                int[] iArr2 = m[i];
                iArr2[0] = iArr2[0] - 12;
            }
            m[i][2] = (int) saku[i];
            m[i][1] = 0;
            i++;
        }
        int state = 0;
        i = 0;
        while (true) {
            if (i >= 5) {
                break;
            } else if (((int) tm0) < m[i][2]) {
                state = 1;
                break;
            } else if (((int) tm0) == m[i][2]) {
                state = 2;
                break;
            } else {
                i++;
            }
        }
        if (state == 0 || state == 1) {
            i--;
        }
        kyureki[1] = m[i][1];
        kyureki[2] = m[i][0];
        kyureki[3] = (((int) tm0) - m[i][2]) + 1;
        if (kyureki[2] > 9 && kyureki[2] > month) {
            int[] iArr3 = kyureki;
            iArr3[0] = iArr3[0] - 1;
        }
    }

    /* JADX INFO: Multiple debug info for r18v1 double: [D('tm2' double), D('tm' double)] */
    /* JADX INFO: Multiple debug info for r18v24 double: [D('delta_rm' double), D('delta_t2' double)] */
    static void calc_chu(double tm2) {
        double tm1 = (double) ((int) tm2);
        double tm22 = (tm2 - tm1) - 0.375d;
        double rm_sun0 = 30.0d * ((double) ((int) (L_SUN(((0.5d + tm22) / 36525.0d) + ((tm1 - 2451545.0d) / 36525.0d)) / 30.0d)));
        double delta_t2 = 0.0d;
        double delta_t1 = 1.0d;
        while (abs(delta_t2 + delta_t1) > 1.1574074074074073E-5d) {
            double delta_rm = L_SUN(((0.5d + tm22) / 36525.0d) + ((tm1 - 2451545.0d) / 36525.0d)) - rm_sun0;
            if (delta_rm > 180.0d) {
                delta_rm -= 360.0d;
            } else if (delta_rm < -180.0d) {
                delta_rm += 360.0d;
            }
            double delta_t12 = (double) ((int) ((365.2d * delta_rm) / 360.0d));
            double delta_t22 = ((delta_rm * 365.2d) / 360.0d) - delta_t12;
            tm1 -= delta_t12;
            tm22 -= delta_t22;
            if (tm22 < 0.0d) {
                tm22 += 1.0d;
                tm1 -= 1.0d;
                double delta_t23 = delta_t22;
                delta_t2 = delta_t12;
                delta_t1 = delta_t23;
            } else {
                double delta_t24 = delta_t22;
                delta_t2 = delta_t12;
                delta_t1 = delta_t24;
            }
        }
        chu[i][0] = 0.375d + tm22;
        double[] dArr = chu[i];
        dArr[0] = dArr[0] + tm1;
        chu[i][1] = rm_sun0;
    }

    /* JADX INFO: Multiple debug info for r18v1 double: [D('tm2' double), D('tm' double)] */
    /* JADX INFO: Multiple debug info for r18v27 double: [D('delta_rm' double), D('delta_t2' double)] */
    static void before_nibun(double tm2) {
        double tm1 = (double) ((int) tm2);
        double tm22 = (tm2 - tm1) - 0.375d;
        double rm_sun0 = (double) (((int) (L_SUN(((0.5d + tm22) / 36525.0d) + ((tm1 - 2451545.0d) / 36525.0d)) / 90.0d)) * 90);
        double delta_t2 = 0.0d;
        double delta_t1 = 1.0d;
        while (abs(delta_t2 + delta_t1) > 1.1574074074074073E-5d) {
            double delta_rm = L_SUN(((0.5d + tm22) / 36525.0d) + ((tm1 - 2451545.0d) / 36525.0d)) - rm_sun0;
            if (delta_rm > 180.0d) {
                delta_rm -= 360.0d;
            } else if (delta_rm < -180.0d) {
                delta_rm += 360.0d;
            }
            double delta_t12 = (double) ((int) ((365.2d * delta_rm) / 360.0d));
            double delta_t22 = ((delta_rm * 365.2d) / 360.0d) - delta_t12;
            tm1 -= delta_t12;
            tm22 -= delta_t22;
            if (tm22 < 0.0d) {
                tm22 += 1.0d;
                tm1 -= 1.0d;
                double delta_t23 = delta_t22;
                delta_t2 = delta_t12;
                delta_t1 = delta_t23;
            } else {
                double delta_t24 = delta_t22;
                delta_t2 = delta_t12;
                delta_t1 = delta_t24;
            }
        }
        nibun[0][0] = 0.375d + tm22;
        double[] dArr = nibun[0];
        dArr[0] = dArr[0] + tm1;
        nibun[0][1] = rm_sun0;
    }

    /* JADX INFO: Multiple debug info for r0v7 double: [D('t' double), D('delta_t1' double)] */
    /* JADX INFO: Multiple debug info for r0v9 double: [D('t' double), D('delta_rm' double)] */
    /* JADX INFO: Multiple debug info for r0v15 double: [D('delta_rm' double), D('delta_t2' double)] */
    /* JADX INFO: Multiple debug info for r7v3 double: [D('tm2' double), D('tm1' double)] */
    /* JADX INFO: Multiple debug info for r15v2 double: [D('tm1' double), D('tm' double)] */
    static double calc_saku(double tm2) {
        double tm22;
        double tm1;
        int lc = 1;
        double tm12 = (double) ((int) tm2);
        double delta_t2 = 0.0d;
        double delta_t1 = 1.0d;
        double tm23 = (tm2 - tm12) - 0.375d;
        double tm24 = tm12;
        while (true) {
            if (abs(delta_t2 + delta_t1) <= 1.1574074074074073E-5d) {
                tm22 = tm23;
                tm1 = tm24;
                break;
            }
            double t = ((0.5d + tm23) / 36525.0d) + ((tm24 - 2451545.0d) / 36525.0d);
            double rm_sun = L_SUN(t);
            double rm_moon = L_MOON(t);
            double delta_rm = rm_moon - rm_sun;
            if (lc == 1 && delta_rm < 0.0d) {
                delta_rm = N_ANGLE(delta_rm);
            } else if (rm_sun >= 0.0d && rm_sun <= 20.0d && rm_moon >= 300.0d) {
                delta_rm = 360.0d - N_ANGLE(delta_rm);
            } else if (abs(delta_rm) > 40.0d) {
                delta_rm = N_ANGLE(delta_rm);
            }
            double delta_t12 = (double) ((int) ((29.530589d * delta_rm) / 360.0d));
            double delta_t22 = ((delta_rm * 29.530589d) / 360.0d) - delta_t12;
            double tm13 = tm24 - delta_t12;
            double tm25 = tm23 - delta_t22;
            if (tm25 < 0.0d) {
                tm25 += 1.0d;
                tm13 -= 1.0d;
            }
            if (lc != 15 || abs(delta_t12 + delta_t22) <= 1.1574074074074073E-5d) {
                if (lc > 30 && abs(delta_t12 + delta_t22) > 1.1574074074074073E-5d) {
                    tm22 = 0.0d;
                    tm1 = tm2;
                    break;
                }
            } else {
                tm13 = (double) ((int) (tm2 - 26.0d));
                tm25 = 0.0d;
            }
            lc++;
            tm23 = tm25;
            tm24 = tm13;
            double delta_t23 = delta_t22;
            delta_t2 = delta_t12;
            delta_t1 = delta_t23;
        }
        return tm22 + tm1 + 0.375d;
    }

    static double abs(double x) {
        if (x < 0.0d) {
            return -x;
        }
        return x;
    }

    static double N_ANGLE(double angle) {
        if (angle >= 0.0d) {
            return angle - (360.0d * ((double) ((int) (angle / 360.0d))));
        }
        double angle1 = -angle;
        return 360.0d - (angle1 - (360.0d * ((double) ((int) (angle1 / 360.0d)))));
    }

    /* JADX INFO: Multiple debug info for r8v2 double: [D('t' double), D('ang' double)] */
    /* JADX INFO: Multiple debug info for r8v6 double: [D('th' double), D('ang' double)] */
    static double L_SUN(double t) {
        double th = (0.02d * Mathcos(N_ANGLE((71998.1d * t) + 265.1d))) + (4.0E-4d * Mathcos(N_ANGLE((31557.0d * t) + 161.0d))) + (4.0E-4d * Mathcos(N_ANGLE((29930.0d * t) + 48.0d))) + (5.0E-4d * Mathcos(N_ANGLE((2281.0d * t) + 221.0d))) + (5.0E-4d * Mathcos(N_ANGLE((155.0d * t) + 118.0d))) + (6.0E-4d * Mathcos(N_ANGLE((33718.0d * t) + 316.0d))) + (7.0E-4d * Mathcos(N_ANGLE((9038.0d * t) + 64.0d))) + (7.0E-4d * Mathcos(N_ANGLE((3035.0d * t) + 110.0d))) + (7.0E-4d * Mathcos(N_ANGLE((65929.0d * t) + 45.0d))) + (0.0013d * Mathcos(N_ANGLE((22519.0d * t) + 352.0d))) + (0.0015d * Mathcos(N_ANGLE((45038.0d * t) + 254.0d))) + (0.0018d * Mathcos(N_ANGLE((445267.0d * t) + 208.0d))) + (0.0018d * Mathcos(N_ANGLE((19.0d * t) + 159.0d))) + (0.002d * Mathcos(N_ANGLE((32964.0d * t) + 158.0d)));
        double ang = N_ANGLE((35999.05d * t) + 267.52d);
        double ang2 = Mathcos(ang) * 1.9147d;
        return N_ANGLE(N_ANGLE(N_ANGLE(t * 36000.7695d) + 280.4659d) + ang2 + (th - ((0.0048d * t) * Mathcos(ang))));
    }

    /* JADX INFO: Multiple debug info for r8v2 double: [D('t' double), D('ang' double)] */
    /* JADX INFO: Multiple debug info for r8v6 double: [D('th' double), D('ang' double)] */
    static double L_MOON(double t) {
        return N_ANGLE(N_ANGLE(N_ANGLE(t * 481267.8809d) + 218.3162d) + (3.0E-4d * Mathcos(N_ANGLE((2322131.0d * t) + 191.0d))) + (3.0E-4d * Mathcos(N_ANGLE((4067.0d * t) + 70.0d))) + (3.0E-4d * Mathcos(N_ANGLE((549197.0d * t) + 220.0d))) + (3.0E-4d * Mathcos(N_ANGLE((1808933.0d * t) + 58.0d))) + (3.0E-4d * Mathcos(N_ANGLE((349472.0d * t) + 337.0d))) + (3.0E-4d * Mathcos(N_ANGLE((381404.0d * t) + 354.0d))) + (3.0E-4d * Mathcos(N_ANGLE((958465.0d * t) + 340.0d))) + (4.0E-4d * Mathcos(N_ANGLE((12006.0d * t) + 187.0d))) + (4.0E-4d * Mathcos(N_ANGLE((39871.0d * t) + 223.0d))) + (5.0E-4d * Mathcos(N_ANGLE((509131.0d * t) + 242.0d))) + (5.0E-4d * Mathcos(N_ANGLE((1745069.0d * t) + 24.0d))) + (5.0E-4d * Mathcos(N_ANGLE((1908795.0d * t) + 90.0d))) + (6.0E-4d * Mathcos(N_ANGLE((2258267.0d * t) + 156.0d))) + (6.0E-4d * Mathcos(N_ANGLE((111869.0d * t) + 38.0d))) + (7.0E-4d * Mathcos(N_ANGLE((27864.0d * t) + 127.0d))) + (7.0E-4d * Mathcos(N_ANGLE((485333.0d * t) + 186.0d))) + (7.0E-4d * Mathcos(N_ANGLE((405201.0d * t) + 50.0d))) + (7.0E-4d * Mathcos(N_ANGLE((790672.0d * t) + 114.0d))) + (8.0E-4d * Mathcos(N_ANGLE((1403732.0d * t) + 98.0d))) + (9.0E-4d * Mathcos(N_ANGLE((858602.0d * t) + 129.0d))) + (0.0011d * Mathcos(N_ANGLE((1920802.0d * t) + 186.0d))) + (0.0012d * Mathcos(N_ANGLE((1267871.0d * t) + 249.0d))) + (0.0016d * Mathcos(N_ANGLE((1856938.0d * t) + 152.0d))) + (0.0018d * Mathcos(N_ANGLE((401329.0d * t) + 274.0d))) + (0.0021d * Mathcos(N_ANGLE((341337.0d * t) + 16.0d))) + (0.0021d * Mathcos(N_ANGLE((71998.0d * t) + 85.0d))) + (0.0021d * Mathcos(N_ANGLE((990397.0d * t) + 357.0d))) + (0.0022d * Mathcos(N_ANGLE((818536.0d * t) + 151.0d))) + (0.0023d * Mathcos(N_ANGLE((922466.0d * t) + 163.0d))) + (0.0024d * Mathcos(N_ANGLE((99863.0d * t) + 122.0d))) + (0.0026d * Mathcos(N_ANGLE((1379739.0d * t) + 17.0d))) + (0.0027d * Mathcos(N_ANGLE((918399.0d * t) + 182.0d))) + (0.0028d * Mathcos(N_ANGLE((1934.0d * t) + 145.0d))) + (0.0037d * Mathcos(N_ANGLE((541062.0d * t) + 259.0d))) + (0.0038d * Mathcos(N_ANGLE((1781068.0d * t) + 21.0d))) + (0.004d * Mathcos(N_ANGLE((133.0d * t) + 29.0d))) + (0.004d * Mathcos(N_ANGLE((1844932.0d * t) + 56.0d))) + (0.004d * Mathcos(N_ANGLE((1331734.0d * t) + 283.0d))) + (0.005d * Mathcos(N_ANGLE((481266.0d * t) + 205.0d))) + (0.0052d * Mathcos(N_ANGLE((31932.0d * t) + 107.0d))) + (0.0068d * Mathcos(N_ANGLE((926533.0d * t) + 323.0d))) + (0.0079d * Mathcos(N_ANGLE((449334.0d * t) + 188.0d))) + (0.0085d * Mathcos(N_ANGLE((826671.0d * t) + 111.0d))) + (0.01d * Mathcos(N_ANGLE((1431597.0d * t) + 315.0d))) + (0.0107d * Mathcos(N_ANGLE((1303870.0d * t) + 246.0d))) + (0.011d * Mathcos(N_ANGLE((489205.0d * t) + 142.0d))) + (0.0125d * Mathcos(N_ANGLE((1443603.0d * t) + 52.0d))) + (0.0154d * Mathcos(N_ANGLE((75870.0d * t) + 41.0d))) + (0.0304d * Mathcos(N_ANGLE((513197.9d * t) + 222.5d))) + (0.0347d * Mathcos(N_ANGLE((445267.1d * t) + 27.9d))) + (0.0409d * Mathcos(N_ANGLE((441199.8d * t) + 47.4d))) + (0.0458d * Mathcos(N_ANGLE((854535.2d * t) + 148.2d))) + (0.0533d * Mathcos(N_ANGLE((1367733.1d * t) + 280.7d))) + (0.0571d * Mathcos(N_ANGLE((377336.3d * t) + 13.2d))) + (0.0588d * Mathcos(N_ANGLE((63863.5d * t) + 124.2d))) + (0.1144d * Mathcos(N_ANGLE((966404.0d * t) + 276.5d))) + (0.1851d * Mathcos(N_ANGLE((35999.05d * t) + 87.53d))) + (0.2136d * Mathcos(N_ANGLE((954397.74d * t) + 179.93d))) + (0.6583d * Mathcos(N_ANGLE((890534.22d * t) + 145.7d))) + (1.274d * Mathcos(N_ANGLE((413335.35d * t) + 10.74d))) + (6.2888d * Mathcos(N_ANGLE((477198.868d * t) + 44.963d))));
    }

    static double YMDT2JD(int year2, int month2, int day2, int hour, int min, int sec) {
        if (((double) month2) < 3.0d) {
            year2 = (int) (((double) year2) - 1.0d);
            month2 = (int) (((double) month2) + 12.0d);
        }
        return (double) ((int) (((double) (((((int) (365.25d * ((double) year2))) + ((int) (((double) year2) / 400.0d))) - ((int) (((double) year2) / 100.0d))) + ((int) (30.59d * (((double) month2) - 2.0d))) + 1721088 + day2)) + ((((((double) sec) / 3600.0d) + (((double) min) / 60.0d)) + ((double) hour)) / 24.0d)));
    }

    static double Mathcos(double deg) {
        return Math.cos((3.141592653589793d * deg) / 180.0d);
    }
}
