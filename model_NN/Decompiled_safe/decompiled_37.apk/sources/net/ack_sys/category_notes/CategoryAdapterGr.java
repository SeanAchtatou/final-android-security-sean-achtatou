package net.ack_sys.category_notes;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import java.util.List;

public class CategoryAdapterGr extends BaseAdapter {
    private List<Category> categorys;
    private Context context;

    public CategoryAdapterGr(Context context2, List<Category> categorys2) {
        this.context = context2;
        this.categorys = categorys2;
    }

    public void setCategorys(List<Category> categorys2) {
        this.categorys = categorys2;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public Category getItem(int position) {
        if (this.categorys == null) {
            return null;
        }
        if (this.categorys.size() > 0) {
            return this.categorys.get(position);
        }
        return null;
    }

    public int getCount() {
        return this.categorys.size();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            int w = this.context.getResources().getDimensionPixelSize(R.dimen.cate_image_width);
            int h = this.context.getResources().getDimensionPixelSize(R.dimen.cate_image_height);
            imageView = new ImageView(this.context);
            imageView.setLayoutParams(new Gallery.LayoutParams(w, h));
            imageView.setScaleType(ImageView.ScaleType.CENTER);
        } else {
            imageView = (ImageView) convertView;
        }
        Category category = getItem(position);
        if (category != null) {
            imageView.setImageResource(AppUtil.getCateResId(category.getIconId()));
        }
        return imageView;
    }
}
