package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;

public class HolidaySetTask extends AsyncTask<String, Void, AsyncTaskResult<?>> {
    private Activity activity = null;
    private HolidaySetTaskCallback callback;
    private ProgressDialog progressDialog = null;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((AsyncTaskResult<?>) ((AsyncTaskResult) obj));
    }

    public HolidaySetTask(Activity activity2, HolidaySetTaskCallback callback2) {
        this.activity = activity2;
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.progressDialog = new ProgressDialog(this.activity);
        this.progressDialog.setMessage(this.activity.getString(R.string.str_msg_holiday));
        this.progressDialog.show();
    }

    /* access modifiers changed from: protected */
    public AsyncTaskResult<?> doInBackground(String... params) {
        SQLiteDatabase DB = new DBEngine(this.activity.getApplicationContext()).getWritableDatabase();
        try {
            DB.beginTransaction();
            List<Integer> xmlList = new ArrayList<>();
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2011));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2012));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2013));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2014));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2015));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2016));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2017));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2018));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2019));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2020));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2021));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2022));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2023));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2024));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2025));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2026));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2027));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2028));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2029));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2030));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2031));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2032));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2033));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2034));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2035));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2036));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2037));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2038));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2039));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2040));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2041));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2042));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2043));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2044));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2045));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2046));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2047));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2048));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2049));
            xmlList.add(Integer.valueOf((int) R.xml.holiday_2050));
            for (Integer i : xmlList) {
                List<Holiday> holidays = new HolidayParser(this.activity.getResources().getXml(i.intValue())).parse();
                if (holidays == null) {
                    throw new Exception();
                }
                DB.delete("HOLIDAY", "holi_year = ?", new String[]{String.valueOf(holidays.get(0).getHoliYear())});
                for (Holiday h : holidays) {
                    ContentValues values = new ContentValues();
                    values.put("holi_year", Integer.valueOf(h.getHoliYear()));
                    values.put("holi_month", Integer.valueOf(h.getHoliMonth()));
                    values.put("holi_day", Integer.valueOf(h.getHoliDay()));
                    values.put("title1", h.getTitle1());
                    values.put("title2", h.getTitle2());
                    DB.insert("HOLIDAY", null, values);
                }
            }
            DB.setTransactionSuccessful();
            DB.endTransaction();
            DB.close();
            return AsyncTaskResult.createNormalResult(this.activity.getString(R.string.str_msg_holidayok));
        } catch (Exception e) {
            AsyncTaskResult<?> createErrorResult = AsyncTaskResult.createErrorResult(this.activity.getString(R.string.str_msg_holidayerr));
            DB.endTransaction();
            DB.close();
            return createErrorResult;
        } catch (Throwable th) {
            DB.endTransaction();
            DB.close();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncTaskResult<?> result) {
        this.progressDialog.dismiss();
        if (result.isError()) {
            this.callback.onFailedHolidaySet(result.getMessage());
        } else {
            this.callback.onSuccessHolidaySet(result.getMessage());
        }
    }
}
