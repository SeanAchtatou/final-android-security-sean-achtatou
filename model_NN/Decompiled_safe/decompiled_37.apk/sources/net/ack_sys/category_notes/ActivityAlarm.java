package net.ack_sys.category_notes;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ActivityAlarm extends ListActivity {
    private ListView LV;
    /* access modifiers changed from: private */
    public AlarmAdapter adapter = null;
    private boolean createFlg = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.createFlg = true;
        this.LV = getListView();
        this.adapter = new AlarmAdapter(this, R.layout.row_alarm, AppUtil.getAlarm(this));
        this.LV.setAdapter((ListAdapter) this.adapter);
        this.LV.setScrollingCacheEnabled(false);
        this.LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(ActivityAlarm.this, ActivityView.class);
                intent.putExtra("recno", ActivityAlarm.this.adapter.getItem(position).getMemoNo());
                ActivityAlarm.this.startActivity(intent);
            }
        });
        this.LV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                CharSequence[] items = ActivityAlarm.this.adapter.getItem(position).getAlarmLoop() ? ActivityAlarm.this.adapter.getItem(position).getEnable() ? new CharSequence[]{ActivityAlarm.this.getString(R.string.str_disable), ActivityAlarm.this.getString(R.string.str_clear)} : new CharSequence[]{ActivityAlarm.this.getString(R.string.str_enable), ActivityAlarm.this.getString(R.string.str_clear)} : new CharSequence[]{ActivityAlarm.this.getString(R.string.str_clear)};
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityAlarm.this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        boolean clear = false;
                        if (!ActivityAlarm.this.adapter.getItem(position).getAlarmLoop()) {
                            clear = true;
                        } else if (which == 0) {
                            AppUtil.setEnableAlarm(ActivityAlarm.this, ActivityAlarm.this.adapter.getItem(position).getRecno(), !ActivityAlarm.this.adapter.getItem(position).getEnable());
                            ActivityAlarm.this.updateAdapter();
                        } else {
                            clear = true;
                        }
                        if (clear) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ActivityAlarm.this);
                            alert.setTitle((int) R.string.str_check);
                            alert.setMessage((int) R.string.str_msg_clear);
                            final int i = position;
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteAlarm(ActivityAlarm.this, ActivityAlarm.this.adapter.getItem(i).getRecno());
                                    ActivityAlarm.this.updateAdapter();
                                }
                            });
                            alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert.show();
                        }
                    }
                });
                builder.create().show();
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.createFlg) {
            updateAdapter();
        }
        this.createFlg = false;
    }

    /* access modifiers changed from: private */
    public void updateAdapter() {
        this.adapter.setAlarms(AppUtil.getAlarm(this));
        this.adapter.notifyDataSetChanged();
    }
}
