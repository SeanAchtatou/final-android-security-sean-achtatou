package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.Calendar;

public class ActivityScheMemo extends Activity {
    private static final int DIALOG_CUSTOM_SCHE_MEMO = 1;
    /* access modifiers changed from: private */
    public int aDay;
    /* access modifiers changed from: private */
    public int aMonth;
    /* access modifiers changed from: private */
    public int aYear;
    private boolean widget = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(128);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.widget = extras.getBoolean("widget", false);
            if (this.widget) {
                Prefs prefs = new Prefs(this);
                if (prefs.getWidgetDay() != 0) {
                    this.aYear = prefs.getWidgetYear();
                    this.aMonth = prefs.getWidgetMonth();
                    this.aDay = prefs.getWidgetDay();
                    showDialog(DIALOG_CUSTOM_SCHE_MEMO);
                    return;
                }
                finish();
            } else if (extras.getInt("year", 0) != 0) {
                this.aYear = extras.getInt("year", 0);
                this.aMonth = extras.getInt("month", 0);
                this.aDay = extras.getInt("day", 0);
                showDialog(DIALOG_CUSTOM_SCHE_MEMO);
            } else {
                finish();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!isFinishing()) {
            removeDialog(DIALOG_CUSTOM_SCHE_MEMO);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = super.onCreateDialog(id);
        if (id != DIALOG_CUSTOM_SCHE_MEMO) {
            return dialog;
        }
        LayoutInflater inflater = LayoutInflater.from(this);
        View view = inflater.inflate((int) R.layout.dlg_sche_memo, (ViewGroup) null);
        ListView LV_MEMO = (ListView) view.findViewById(R.id.LV_MEMO);
        ScheduleAdapter scheduleAdapter = new ScheduleAdapter(this, R.layout.row_schedule, AppUtil.getScheMemo(this, this.aYear, this.aMonth, this.aDay));
        LV_MEMO.setAdapter((ListAdapter) scheduleAdapter);
        LV_MEMO.setScrollingCacheEnabled(false);
        final ScheduleAdapter scheduleAdapter2 = scheduleAdapter;
        LV_MEMO.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                ActivityScheMemo.this.removeDialog(ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO);
                Intent intent = new Intent(ActivityScheMemo.this, ActivityView.class);
                intent.putExtra("recno", scheduleAdapter2.getItem(position).getRecno());
                ActivityScheMemo.this.startActivity(intent);
            }
        });
        final ScheduleAdapter scheduleAdapter3 = scheduleAdapter;
        LV_MEMO.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                CharSequence[] searchs = new CharSequence[3];
                searchs[0] = ActivityScheMemo.this.getString(R.string.str_edit);
                searchs[ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO] = ActivityScheMemo.this.getString(R.string.str_remove);
                searchs[2] = ActivityScheMemo.this.getString(R.string.str_remove1);
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityScheMemo.this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                final ScheduleAdapter scheduleAdapter = scheduleAdapter3;
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            ActivityScheMemo.this.removeDialog(ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO);
                            Intent intent = new Intent(ActivityScheMemo.this, ActivityEdit.class);
                            intent.putExtra("recno", scheduleAdapter.getItem(position).getRecno());
                            ActivityScheMemo.this.startActivity(intent);
                        } else if (which == ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ActivityScheMemo.this);
                            alert.setTitle((int) R.string.str_check);
                            alert.setMessage((int) R.string.str_msg_remove);
                            final ScheduleAdapter scheduleAdapter = scheduleAdapter;
                            final int i = position;
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteMemo(ActivityScheMemo.this, scheduleAdapter.getItem(i).getRecno());
                                    scheduleAdapter.setMemos(AppUtil.getScheMemo(ActivityScheMemo.this, ActivityScheMemo.this.aYear, ActivityScheMemo.this.aMonth, ActivityScheMemo.this.aDay));
                                    scheduleAdapter.notifyDataSetChanged();
                                }
                            });
                            alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert.show();
                        } else {
                            AlertDialog.Builder alert2 = new AlertDialog.Builder(ActivityScheMemo.this);
                            alert2.setTitle((int) R.string.str_check);
                            alert2.setMessage((int) R.string.str_msg_remove1);
                            final ScheduleAdapter scheduleAdapter2 = scheduleAdapter;
                            final int i2 = position;
                            alert2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteSchedule(ActivityScheMemo.this, scheduleAdapter2.getItem(i2).getRecno());
                                    scheduleAdapter2.setMemos(AppUtil.getScheMemo(ActivityScheMemo.this, ActivityScheMemo.this.aYear, ActivityScheMemo.this.aMonth, ActivityScheMemo.this.aDay));
                                    scheduleAdapter2.notifyDataSetChanged();
                                }
                            });
                            alert2.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert2.show();
                        }
                    }
                });
                builder.create().show();
                return false;
            }
        });
        StringBuilder str = new StringBuilder();
        Object[] objArr = new Object[DIALOG_CUSTOM_SCHE_MEMO];
        objArr[0] = Integer.valueOf(this.aYear);
        str.append(String.valueOf(String.format("%1$04d", objArr)) + getString(R.string.str_year));
        Object[] objArr2 = new Object[DIALOG_CUSTOM_SCHE_MEMO];
        objArr2[0] = Integer.valueOf(this.aMonth + DIALOG_CUSTOM_SCHE_MEMO);
        str.append(String.valueOf(String.format("%1$02d", objArr2)) + getString(R.string.str_month));
        Object[] objArr3 = new Object[DIALOG_CUSTOM_SCHE_MEMO];
        objArr3[0] = Integer.valueOf(this.aDay);
        str.append(String.valueOf(String.format("%1$02d", objArr3)) + getString(R.string.str_day));
        Calendar cal = Calendar.getInstance();
        cal.set(DIALOG_CUSTOM_SCHE_MEMO, this.aYear);
        cal.set(2, this.aMonth);
        cal.set(5, this.aDay);
        switch (cal.get(7)) {
            case DIALOG_CUSTOM_SCHE_MEMO /*1*/:
                str.append("(<font color=\"#ffb6c1\">" + getString(R.string.str_week1) + "</font>) ");
                break;
            case DBEngine.DB_VERSION /*2*/:
                str.append("(<font color=\"White\">" + getString(R.string.str_week2) + "</font>) ");
                break;
            case 3:
                str.append("(<font color=\"White\">" + getString(R.string.str_week3) + "</font>) ");
                break;
            case 4:
                str.append("(<font color=\"White\">" + getString(R.string.str_week4) + "</font>) ");
                break;
            case 5:
                str.append("(<font color=\"White\">" + getString(R.string.str_week5) + "</font>) ");
                break;
            case 6:
                str.append("(<font color=\"White\">" + getString(R.string.str_week6) + "</font>) ");
                break;
            case 7:
                str.append("(<font color=\"#00ffff\">" + getString(R.string.str_week7) + "</font>) ");
                break;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if (this.widget) {
            View titleView = inflater.inflate((int) R.layout.dlg_sche_memo_title, (ViewGroup) null);
            ((ImageView) titleView.findViewById(R.id.IV_HOME)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ActivityScheMemo.this.removeDialog(ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO);
                    Intent intent = new Intent(ActivityScheMemo.this, ActivityMainTab.class);
                    intent.setFlags(268435456);
                    ActivityScheMemo.this.startActivity(intent);
                    ActivityScheMemo.this.finish();
                }
            });
            ((TextView) titleView.findViewById(R.id.TV_TITLE)).setText(Html.fromHtml(str.toString()));
            builder.setCustomTitle(titleView);
        } else {
            builder.setTitle(Html.fromHtml(str.toString()));
        }
        builder.setCancelable(false);
        builder.setPositiveButton((int) R.string.str_new, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ActivityScheMemo.this.removeDialog(ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO);
                Intent intent = new Intent(ActivityScheMemo.this, ActivityEdit.class);
                intent.putExtra("year", ActivityScheMemo.this.aYear);
                intent.putExtra("month", ActivityScheMemo.this.aMonth);
                intent.putExtra("day", ActivityScheMemo.this.aDay);
                ActivityScheMemo.this.startActivity(intent);
            }
        });
        builder.setNegativeButton((int) R.string.str_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ActivityScheMemo.this.removeDialog(ActivityScheMemo.DIALOG_CUSTOM_SCHE_MEMO);
                ActivityScheMemo.this.finish();
            }
        });
        AlertDialog create = builder.create();
        create.setView(view, 0, 0, 0, 0);
        return create;
    }
}
