package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import jp.co.nobot.libAdMaker.libAdMaker;

public class ActivityView extends Activity implements View.OnClickListener, View.OnLongClickListener {
    private libAdMaker AdMaker;
    private Button B_DATE;
    private Button B_TIME;
    private Button B_TIME_K;
    private Button B_WEEK_K;
    private ImageView IV_ALARM;
    private ImageView IV_CATE_ICON;
    private ImageView IV_DEL;
    private ImageView IV_EDIT;
    private ImageView IV_SHARE;
    private ImageView IV_TAG_ICON;
    private LinearLayout LL_ALARM;
    private LinearLayout LL_SCHEDULE;
    /* access modifiers changed from: private */
    public TextView TV_CATE_TITLE;
    /* access modifiers changed from: private */
    public TextView TV_DATE;
    private TextView TV_DATETIME;
    /* access modifiers changed from: private */
    public TextView TV_MEMO;
    private TextView TV_SCHEDULE;
    /* access modifiers changed from: private */
    public TextView TV_TIME;
    /* access modifiers changed from: private */
    public TextView TV_TIME_K;
    /* access modifiers changed from: private */
    public TextView TV_WEEK1;
    /* access modifiers changed from: private */
    public TextView TV_WEEK2;
    /* access modifiers changed from: private */
    public TextView TV_WEEK3;
    /* access modifiers changed from: private */
    public TextView TV_WEEK4;
    /* access modifiers changed from: private */
    public TextView TV_WEEK5;
    /* access modifiers changed from: private */
    public TextView TV_WEEK6;
    /* access modifiers changed from: private */
    public TextView TV_WEEK7;
    /* access modifiers changed from: private */
    public int alarmRecno;
    /* access modifiers changed from: private */
    public Calendar calendar = Calendar.getInstance();
    /* access modifiers changed from: private */
    public Calendar calendarK = Calendar.getInstance();
    /* access modifiers changed from: private */
    public int currentRecno = -1;
    /* access modifiers changed from: private */
    public String location;
    /* access modifiers changed from: private */
    public String photoUri;
    /* access modifiers changed from: private */
    public int sDay;
    /* access modifiers changed from: private */
    public int sHour;
    /* access modifiers changed from: private */
    public int sMinute;
    /* access modifiers changed from: private */
    public int sMonth;
    /* access modifiers changed from: private */
    public int sYear;
    private boolean schedule = false;
    /* access modifiers changed from: private */
    public int selectItem;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        AppInfo.getInstance().setScaledDensity(metrics.scaledDensity);
        setContentView((int) R.layout.act_view);
        getWindow().addFlags(128);
        this.AdMaker = (libAdMaker) findViewById(R.id.admakerview);
        this.AdMaker.siteId = "690";
        this.AdMaker.zoneId = "1739";
        this.AdMaker.setUrl("http://images.ad-maker.info/apps/3xnfedmjh91w.html");
        this.AdMaker.start();
        this.IV_CATE_ICON = (ImageView) findViewById(R.id.IV_CATE_ICON);
        this.TV_CATE_TITLE = (TextView) findViewById(R.id.TV_CATE_TITLE);
        this.IV_TAG_ICON = (ImageView) findViewById(R.id.IV_TAG_ICON);
        this.TV_MEMO = (TextView) findViewById(R.id.TV_MEMO);
        this.TV_MEMO.setOnLongClickListener(this);
        this.LL_SCHEDULE = (LinearLayout) findViewById(R.id.LL_SCHEDULE);
        this.TV_SCHEDULE = (TextView) findViewById(R.id.TV_SCHEDULE);
        this.LL_ALARM = (LinearLayout) findViewById(R.id.LL_ALARM);
        this.TV_DATETIME = (TextView) findViewById(R.id.TV_DATETIME);
        this.IV_EDIT = (ImageView) findViewById(R.id.IV_EDIT);
        this.IV_EDIT.setOnClickListener(this);
        this.IV_ALARM = (ImageView) findViewById(R.id.IV_ALARM);
        this.IV_ALARM.setOnClickListener(this);
        this.IV_SHARE = (ImageView) findViewById(R.id.IV_SHARE);
        this.IV_SHARE.setOnClickListener(this);
        this.IV_DEL = (ImageView) findViewById(R.id.IV_DEL);
        this.IV_DEL.setOnClickListener(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.currentRecno = extras.getInt("recno", -1);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.AdMaker.destroy();
        this.AdMaker = null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.AdMaker.stop();
    }

    public void onRestart() {
        super.onRestart();
        this.AdMaker.start();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.IV_EDIT /*2131296606*/:
                Intent intent = new Intent(this, ActivityEdit.class);
                intent.putExtra("recno", this.currentRecno);
                startActivity(intent);
                return;
            case R.id.IV_ALARM /*2131296607*/:
                if (!this.schedule || this.alarmRecno != 0) {
                    showAlarmDialog();
                    return;
                }
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle((int) R.string.str_check);
                alert.setMessage((int) R.string.str_msg_scheduleset);
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityView.this.updateAlarm(ActivityView.this.sYear, ActivityView.this.sMonth, ActivityView.this.sDay, ActivityView.this.sHour, ActivityView.this.sMinute, 0, null);
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityView.this.showAlarmDialog();
                    }
                });
                alert.show();
                return;
            case R.id.IV_DEL /*2131296609*/:
                AlertDialog.Builder alert2 = new AlertDialog.Builder(this);
                alert2.setTitle((int) R.string.str_check);
                alert2.setMessage((int) R.string.str_msg_remove);
                alert2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppUtil.deleteMemo(ActivityView.this, ActivityView.this.currentRecno);
                        ActivityView.this.finish();
                    }
                });
                alert2.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                alert2.show();
                return;
            case R.id.IV_SHARE /*2131296646*/:
                final Intent intentSend = new Intent("android.intent.action.SEND");
                if (!AppUtil.checkNull(this.photoUri).booleanValue()) {
                    AlertDialog.Builder alert3 = new AlertDialog.Builder(this);
                    alert3.setTitle((int) R.string.str_check);
                    alert3.setMessage((int) R.string.str_msg_attached);
                    alert3.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            intentSend.setType("image/*");
                            intentSend.putExtra("android.intent.extra.TITLE", ActivityView.this.TV_CATE_TITLE.getText().toString().trim());
                            intentSend.putExtra("android.intent.extra.SUBJECT", ActivityView.this.TV_CATE_TITLE.getText().toString().trim());
                            intentSend.putExtra("android.intent.extra.TEXT", ActivityView.this.TV_MEMO.getText().toString().trim());
                            intentSend.putExtra("android.intent.extra.STREAM", Uri.parse("file://" + ActivityView.this.photoUri));
                            ActivityView.this.startActivity(Intent.createChooser(intentSend, ActivityView.this.getString(R.string.str_menu_share)));
                        }
                    });
                    alert3.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            intentSend.setType("text/plain");
                            intentSend.putExtra("android.intent.extra.TITLE", ActivityView.this.TV_CATE_TITLE.getText().toString().trim());
                            intentSend.putExtra("android.intent.extra.SUBJECT", ActivityView.this.TV_CATE_TITLE.getText().toString().trim());
                            intentSend.putExtra("android.intent.extra.TEXT", ActivityView.this.TV_MEMO.getText().toString().trim());
                            ActivityView.this.startActivity(Intent.createChooser(intentSend, ActivityView.this.getString(R.string.str_menu_share)));
                        }
                    });
                    alert3.show();
                    return;
                }
                intentSend.setType("text/plain");
                intentSend.putExtra("android.intent.extra.TITLE", this.TV_CATE_TITLE.getText().toString().trim());
                intentSend.putExtra("android.intent.extra.SUBJECT", this.TV_CATE_TITLE.getText().toString().trim());
                intentSend.putExtra("android.intent.extra.TEXT", this.TV_MEMO.getText().toString().trim());
                startActivity(Intent.createChooser(intentSend, getString(R.string.str_menu_share)));
                return;
            case R.id.B_DATE /*2131296653*/:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        ActivityView.this.TV_DATE.setText(String.valueOf(String.format("%1$04d", Integer.valueOf(year))) + "/" + String.format("%1$02d", Integer.valueOf(monthOfYear + 1)) + "/" + String.format("%1$02d", Integer.valueOf(dayOfMonth)));
                        ActivityView.this.calendar.set(1, year);
                        ActivityView.this.calendar.set(2, monthOfYear);
                        ActivityView.this.calendar.set(5, dayOfMonth);
                    }
                }, this.calendar.get(1), this.calendar.get(2), this.calendar.get(5)).show();
                return;
            case R.id.B_TIME /*2131296654*/:
                new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ActivityView.this.TV_TIME.setText(String.valueOf(String.format("%1$02d", Integer.valueOf(hourOfDay))) + ":" + String.format("%1$02d", Integer.valueOf(minute)));
                        ActivityView.this.calendar.set(11, hourOfDay);
                        ActivityView.this.calendar.set(12, minute);
                    }
                }, this.calendar.get(11), this.calendar.get(12), true).show();
                return;
            case R.id.B_WEEK_K /*2131296656*/:
                final CharSequence[] itemsK = {getString(R.string.str_week1), getString(R.string.str_week2), getString(R.string.str_week3), getString(R.string.str_week4), getString(R.string.str_week5), getString(R.string.str_week6), getString(R.string.str_week7)};
                final boolean[] checkedItemsK = {true, true, true, true, true, true, true};
                if (this.TV_WEEK1.getCurrentTextColor() == 0) {
                    checkedItemsK[0] = false;
                }
                if (this.TV_WEEK2.getCurrentTextColor() == 0) {
                    checkedItemsK[1] = false;
                }
                if (this.TV_WEEK3.getCurrentTextColor() == 0) {
                    checkedItemsK[2] = false;
                }
                if (this.TV_WEEK4.getCurrentTextColor() == 0) {
                    checkedItemsK[3] = false;
                }
                if (this.TV_WEEK5.getCurrentTextColor() == 0) {
                    checkedItemsK[4] = false;
                }
                if (this.TV_WEEK6.getCurrentTextColor() == 0) {
                    checkedItemsK[5] = false;
                }
                if (this.TV_WEEK7.getCurrentTextColor() == 0) {
                    checkedItemsK[6] = false;
                }
                AlertDialog.Builder builderK = new AlertDialog.Builder(this);
                builderK.setTitle((int) R.string.str_setweek);
                builderK.setMultiChoiceItems(itemsK, checkedItemsK, new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        checkedItemsK[which] = isChecked;
                    }
                });
                builderK.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        boolean errFlg = true;
                        for (int i = 0; i < itemsK.length; i++) {
                            if (checkedItemsK[i]) {
                                errFlg = false;
                                if (i == 0) {
                                    ActivityView.this.TV_WEEK1.setTextColor(-18751);
                                }
                                if (i == 1) {
                                    ActivityView.this.TV_WEEK2.setTextColor(-1);
                                }
                                if (i == 2) {
                                    ActivityView.this.TV_WEEK3.setTextColor(-1);
                                }
                                if (i == 3) {
                                    ActivityView.this.TV_WEEK4.setTextColor(-1);
                                }
                                if (i == 4) {
                                    ActivityView.this.TV_WEEK5.setTextColor(-1);
                                }
                                if (i == 5) {
                                    ActivityView.this.TV_WEEK6.setTextColor(-1);
                                }
                                if (i == 6) {
                                    ActivityView.this.TV_WEEK7.setTextColor(-16711681);
                                }
                            } else {
                                if (i == 0) {
                                    ActivityView.this.TV_WEEK1.setTextColor(0);
                                }
                                if (i == 1) {
                                    ActivityView.this.TV_WEEK2.setTextColor(0);
                                }
                                if (i == 2) {
                                    ActivityView.this.TV_WEEK3.setTextColor(0);
                                }
                                if (i == 3) {
                                    ActivityView.this.TV_WEEK4.setTextColor(0);
                                }
                                if (i == 4) {
                                    ActivityView.this.TV_WEEK5.setTextColor(0);
                                }
                                if (i == 5) {
                                    ActivityView.this.TV_WEEK6.setTextColor(0);
                                }
                                if (i == 6) {
                                    ActivityView.this.TV_WEEK7.setTextColor(0);
                                }
                            }
                        }
                        if (errFlg) {
                            Toast.makeText(ActivityView.this, (int) R.string.str_msg_weeksetting, 1).show();
                        }
                        dialog.cancel();
                    }
                });
                builderK.create().show();
                return;
            case R.id.B_TIME_K /*2131296657*/:
                new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ActivityView.this.TV_TIME_K.setText(String.valueOf(String.format("%1$02d", Integer.valueOf(hourOfDay))) + ":" + String.format("%1$02d", Integer.valueOf(minute)));
                        ActivityView.this.calendarK.set(11, hourOfDay);
                        ActivityView.this.calendarK.set(12, minute);
                    }
                }, this.calendarK.get(11), this.calendarK.get(12), true).show();
                return;
            default:
                return;
        }
    }

    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.TV_MEMO /*2131296642*/:
                CharSequence[] searchs = (TextUtils.isEmpty(this.photoUri) || TextUtils.isEmpty(this.location)) ? !TextUtils.isEmpty(this.photoUri) ? new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2), getString(R.string.str_preview)} : !TextUtils.isEmpty(this.location) ? new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2), getString(R.string.str_map)} : new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2)} : new CharSequence[]{getString(R.string.str_clipboard1), getString(R.string.str_clipboard2), getString(R.string.str_preview), getString(R.string.str_map)};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            AppUtil.copyClipboard(ActivityView.this, ActivityView.this.TV_MEMO.getText().toString());
                        } else if (which == 1) {
                            List<String> listTmp = Arrays.asList(ActivityView.this.TV_MEMO.getText().toString().split("\n"));
                            List<String> listMemo = new ArrayList<>();
                            for (String memo : listTmp) {
                                if (!TextUtils.isEmpty(memo)) {
                                    listMemo.add(memo);
                                }
                            }
                            final CharSequence[] items = (CharSequence[]) listMemo.toArray(new CharSequence[listMemo.size()]);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityView.this);
                            builder.setTitle((int) R.string.str_clipboard);
                            builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.copyClipboard(ActivityView.this, items[which].toString());
                                }
                            });
                            builder.create().show();
                        } else if (which != 2) {
                            AppUtil.startMap(ActivityView.this, ActivityView.this.location);
                        } else if (!TextUtils.isEmpty(ActivityView.this.photoUri)) {
                            AppUtil.startImageViewer(ActivityView.this, ActivityView.this.photoUri);
                        } else {
                            AppUtil.startMap(ActivityView.this, ActivityView.this.location);
                        }
                    }
                });
                builder.create().show();
                break;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setView();
    }

    /* access modifiers changed from: private */
    public void setView() {
        dispMemo(this.currentRecno);
    }

    private void dispMemo(int recno) {
        Prefs prefs = new Prefs(this);
        SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
        int[] week = new int[7];
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT");
            sql.append(" M.cate_id,");
            sql.append(" C.title,");
            sql.append(" C.sort,");
            sql.append(" C.icon_id,");
            sql.append(" M.memo,");
            sql.append(" M.photo_name,");
            sql.append(" M.photo_uri,");
            sql.append(" M.tag_id,");
            sql.append(" M.sche_s_year,");
            sql.append(" M.sche_s_month,");
            sql.append(" M.sche_s_day,");
            sql.append(" M.sche_s_hour,");
            sql.append(" M.sche_s_minute,");
            sql.append(" M.sche_s_time,");
            sql.append(" M.sche_e_year,");
            sql.append(" M.sche_e_month,");
            sql.append(" M.sche_e_day,");
            sql.append(" M.sche_e_hour,");
            sql.append(" M.sche_e_minute,");
            sql.append(" M.sche_e_time,");
            sql.append(" M.sche_location,");
            sql.append(" A.alarm_year,");
            sql.append(" A.alarm_month,");
            sql.append(" A.alarm_day,");
            sql.append(" A.alarm_hour,");
            sql.append(" A.alarm_minute,");
            sql.append(" A.alarm_loop,");
            sql.append(" A.alarm_week1,");
            sql.append(" A.alarm_week2,");
            sql.append(" A.alarm_week3,");
            sql.append(" A.alarm_week4,");
            sql.append(" A.alarm_week5,");
            sql.append(" A.alarm_week6,");
            sql.append(" A.alarm_week7,");
            sql.append(" A.recno");
            sql.append(" FROM MEMO M");
            sql.append(" INNER JOIN CATEGORY C ON");
            sql.append("       M.cate_id = C.id");
            sql.append(" LEFT  JOIN ALARM A ON");
            sql.append("       M.recno = A.memo_no");
            sql.append(" AND   A.snooze = 0");
            sql.append(" WHERE M.recno = " + AppUtil.sql9(Integer.valueOf(recno)));
            Cursor RS = DB.rawQuery(sql.toString(), null);
            if (RS.moveToFirst()) {
                AppInfo.getInstance().setCurrentPosition(RS.getInt(RS.getColumnIndex("sort")) - 1);
                AppInfo.getInstance().setCurrentCateId(RS.getInt(RS.getColumnIndex(Memo.CATE_ID)));
                this.IV_CATE_ICON.setImageResource(AppUtil.getCateResId(RS.getInt(RS.getColumnIndex(Category.ICON_ID))));
                this.TV_CATE_TITLE.setText(RS.getString(RS.getColumnIndex("title")));
                if (prefs.getTagUse()) {
                    this.IV_TAG_ICON.setImageResource(AppUtil.getTagResId(this, RS.getInt(RS.getColumnIndex(Memo.TAG_ID))));
                } else {
                    this.IV_TAG_ICON.setImageResource(R.drawable.droid);
                }
                this.TV_MEMO.setText(RS.getString(RS.getColumnIndex(Memo.MEMO)));
                if (RS.getInt(RS.getColumnIndex(Memo.SCHE_S_YEAR)) == 0) {
                    this.schedule = false;
                    this.LL_SCHEDULE.setLayoutParams(new LinearLayout.LayoutParams(-1, 0));
                    this.location = "";
                } else {
                    if (!AppUtil.checkNull(RS.getString(RS.getColumnIndex(Memo.SCHE_S_TIME))).booleanValue()) {
                        this.schedule = true;
                    } else {
                        this.schedule = false;
                    }
                    this.sYear = RS.getInt(RS.getColumnIndex(Memo.SCHE_S_YEAR));
                    this.sMonth = RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MONTH));
                    this.sDay = RS.getInt(RS.getColumnIndex(Memo.SCHE_S_DAY));
                    this.sHour = RS.getInt(RS.getColumnIndex(Memo.SCHE_S_HOUR));
                    this.sMinute = RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MINUTE));
                    this.LL_SCHEDULE.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    this.TV_SCHEDULE.setText(AppUtil.getScheduleStr(getResources(), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_YEAR)), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MONTH)), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_DAY)), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_HOUR)), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MINUTE)), RS.getString(RS.getColumnIndex(Memo.SCHE_S_TIME)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_YEAR)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_MONTH)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_DAY)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_HOUR)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_MINUTE)), RS.getString(RS.getColumnIndex(Memo.SCHE_E_TIME)), RS.getString(RS.getColumnIndex(Memo.SCHE_LOCATION))));
                    this.location = RS.getString(RS.getColumnIndex(Memo.SCHE_LOCATION));
                }
                if (RS.getInt(RS.getColumnIndex(Alarm.ALARM_YEAR)) == 0) {
                    this.LL_ALARM.setLayoutParams(new LinearLayout.LayoutParams(-1, 0));
                } else {
                    week[0] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK1));
                    week[1] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK2));
                    week[2] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK3));
                    week[3] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK4));
                    week[4] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK5));
                    week[5] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK6));
                    week[6] = RS.getInt(RS.getColumnIndex(Alarm.ALARM_WEEK7));
                    this.LL_ALARM.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    this.TV_DATETIME.setText(AppUtil.getDateTimeStr(getResources(), RS.getInt(RS.getColumnIndex(Alarm.ALARM_YEAR)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_MONTH)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_DAY)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_HOUR)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_MINUTE)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_LOOP)), week));
                }
                this.alarmRecno = RS.getInt(RS.getColumnIndex("recno"));
                this.photoUri = RS.getString(RS.getColumnIndex(Memo.PHOTO_URI));
                AppUtil.showPhoto2(getResources(), this.photoUri, this.TV_MEMO);
            }
            RS.close();
        } catch (Exception e) {
        } finally {
            DB.close();
        }
    }

    /* access modifiers changed from: private */
    public void setAlarm(int which) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());
        switch (which) {
            case 0:
                View alarmView = LayoutInflater.from(this).inflate(R.layout.dlg_alarm, (ViewGroup) null);
                this.B_DATE = (Button) alarmView.findViewById(R.id.B_DATE);
                this.B_DATE.setOnClickListener(this);
                this.B_TIME = (Button) alarmView.findViewById(R.id.B_TIME);
                this.B_TIME.setOnClickListener(this);
                this.TV_DATE = (TextView) alarmView.findViewById(R.id.TV_DATE);
                this.TV_DATE.setText(String.valueOf(String.format("%1$04d", Integer.valueOf(cal.get(1)))) + "/" + String.format("%1$02d", Integer.valueOf(cal.get(2) + 1)) + "/" + String.format("%1$02d", Integer.valueOf(cal.get(5))));
                this.TV_TIME = (TextView) alarmView.findViewById(R.id.TV_TIME);
                this.TV_TIME.setText(String.valueOf(String.format("%1$02d", Integer.valueOf(cal.get(11)))) + ":" + String.format("%1$02d", Integer.valueOf(cal.get(12))));
                new AlertDialog.Builder(this).setTitle((int) R.string.str_custom).setCancelable(false).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityView.this.updateAlarm(Integer.valueOf(ActivityView.this.TV_DATE.getText().toString().substring(0, 4)).intValue(), Integer.valueOf(ActivityView.this.TV_DATE.getText().toString().substring(5, 7)).intValue() - 1, Integer.valueOf(ActivityView.this.TV_DATE.getText().toString().substring(8, 10)).intValue(), Integer.valueOf(ActivityView.this.TV_TIME.getText().toString().substring(0, 2)).intValue(), Integer.valueOf(ActivityView.this.TV_TIME.getText().toString().substring(3, 5)).intValue(), 0, null);
                    }
                }).setView(alarmView).show();
                break;
            case 1:
                cal.add(12, 3);
                break;
            case DBEngine.DB_VERSION:
                cal.add(12, 5);
                break;
            case 3:
                cal.add(12, 10);
                break;
            case 4:
                cal.add(12, 15);
                break;
            case 5:
                cal.add(12, 30);
                break;
            case 6:
                cal.add(11, 1);
                break;
            case 7:
                cal.add(5, 1);
                break;
            case 8:
                View alarmViewK = LayoutInflater.from(this).inflate(R.layout.dlg_alarm_k, (ViewGroup) null);
                this.B_WEEK_K = (Button) alarmViewK.findViewById(R.id.B_WEEK_K);
                this.B_WEEK_K.setOnClickListener(this);
                this.B_TIME_K = (Button) alarmViewK.findViewById(R.id.B_TIME_K);
                this.B_TIME_K.setOnClickListener(this);
                this.TV_WEEK1 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK1);
                this.TV_WEEK2 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK2);
                this.TV_WEEK3 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK3);
                this.TV_WEEK4 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK4);
                this.TV_WEEK5 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK5);
                this.TV_WEEK6 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK6);
                this.TV_WEEK7 = (TextView) alarmViewK.findViewById(R.id.TV_WEEK7);
                this.TV_TIME_K = (TextView) alarmViewK.findViewById(R.id.TV_TIME_K);
                this.TV_TIME_K.setText(String.valueOf(String.format("%1$02d", Integer.valueOf(cal.get(11)))) + ":" + String.format("%1$02d", Integer.valueOf(cal.get(12))));
                new AlertDialog.Builder(this).setTitle((int) R.string.str_repeat).setCancelable(false).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        int[] week = {1, 1, 1, 1, 1, 1, 1};
                        if (ActivityView.this.TV_WEEK1.getCurrentTextColor() == 0) {
                            week[0] = 0;
                        }
                        if (ActivityView.this.TV_WEEK2.getCurrentTextColor() == 0) {
                            week[1] = 0;
                        }
                        if (ActivityView.this.TV_WEEK3.getCurrentTextColor() == 0) {
                            week[2] = 0;
                        }
                        if (ActivityView.this.TV_WEEK4.getCurrentTextColor() == 0) {
                            week[3] = 0;
                        }
                        if (ActivityView.this.TV_WEEK5.getCurrentTextColor() == 0) {
                            week[4] = 0;
                        }
                        if (ActivityView.this.TV_WEEK6.getCurrentTextColor() == 0) {
                            week[5] = 0;
                        }
                        if (ActivityView.this.TV_WEEK7.getCurrentTextColor() == 0) {
                            week[6] = 0;
                        }
                        boolean errFlg = true;
                        int i = 0;
                        while (true) {
                            if (i >= week.length) {
                                break;
                            } else if (week[i] == 1) {
                                errFlg = false;
                                break;
                            } else {
                                i++;
                            }
                        }
                        if (!errFlg) {
                            int hour = Integer.valueOf(ActivityView.this.TV_TIME_K.getText().toString().substring(0, 2)).intValue();
                            int minute = Integer.valueOf(ActivityView.this.TV_TIME_K.getText().toString().substring(3, 5)).intValue();
                            Calendar alarmCal = AppUtil.getNextDay(week, hour, minute);
                            ActivityView.this.updateAlarm(alarmCal.get(1), alarmCal.get(2), alarmCal.get(5), hour, minute, 1, week);
                        }
                    }
                }).setView(alarmViewK).show();
                break;
        }
        if (which != 0 && which != 8) {
            updateAlarm(cal.get(1), cal.get(2), cal.get(5), cal.get(11), cal.get(12), 0, null);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void updateAlarm(int year, int month, int day, int hour, int minute, int loop, int[] week) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT recno FROM ALARM");
            sql.append(" WHERE memo_no = " + AppUtil.sql9(Integer.valueOf(this.currentRecno)));
            Cursor RS = DB.rawQuery(sql.toString(), null);
            while (RS.moveToNext()) {
                AppUtil.cancelAlarmManager(this, RS.getInt(0));
                StringBuilder sql2 = new StringBuilder();
                sql2.append(" DELETE FROM ALARM");
                sql2.append(" WHERE recno = " + AppUtil.sql9(Integer.valueOf(RS.getInt(0))));
                DB.execSQL(sql2.toString());
            }
            RS.close();
            ContentValues values = new ContentValues();
            values.put("memo_no", Integer.valueOf(this.currentRecno));
            values.put(Alarm.ALARM_YEAR, Integer.valueOf(year));
            values.put(Alarm.ALARM_MONTH, Integer.valueOf(month));
            values.put(Alarm.ALARM_DAY, Integer.valueOf(day));
            values.put(Alarm.ALARM_HOUR, Integer.valueOf(hour));
            values.put(Alarm.ALARM_MINUTE, Integer.valueOf(minute));
            values.put(Alarm.ALARM_LOOP, Integer.valueOf(loop));
            if (week != null) {
                for (int i = 1; i <= 7; i++) {
                    values.put("alarm_week" + String.valueOf(i), Integer.valueOf(week[i - 1]));
                }
            }
            values.put("enable", (Integer) 1);
            values.put(Alarm.SNOOZE, (Integer) 0);
            DB.insert("ALARM", null, values);
            Cursor RS2 = DB.rawQuery(" SELECT last_insert_rowid()", null);
            if (RS2.moveToFirst()) {
                AppUtil.setAlarmManager(this, RS2.getInt(0), year, month, day, hour, minute);
            }
            RS2.close();
            DB.setTransactionSuccessful();
            DB.endTransaction();
            DB.close();
            setView();
            Toast.makeText(this, ((Object) AppUtil.getDateTimeStr(getResources(), year, month, day, hour, minute, loop, week)) + "\n\n" + getString(R.string.str_msg_setting), 1).show();
        } catch (Exception e) {
            DB.endTransaction();
            DB.close();
            setView();
            Toast.makeText(this, ((Object) AppUtil.getDateTimeStr(getResources(), year, month, day, hour, minute, loop, week)) + "\n\n" + getString(R.string.str_msg_setting), 1).show();
        } catch (Throwable th) {
            Throwable th2 = th;
            DB.endTransaction();
            DB.close();
            setView();
            Toast.makeText(this, ((Object) AppUtil.getDateTimeStr(getResources(), year, month, day, hour, minute, loop, week)) + "\n\n" + getString(R.string.str_msg_setting), 1).show();
            throw th2;
        }
    }

    /* access modifiers changed from: private */
    public void showAlarmDialog() {
        CharSequence[] items = {getString(R.string.str_custom), getString(R.string.str_3minutes), getString(R.string.str_5minutes), getString(R.string.str_10minutes), getString(R.string.str_15minutes), getString(R.string.str_30minutes), getString(R.string.str_after1hour), getString(R.string.str_onedaylater), getString(R.string.str_repeat)};
        this.selectItem = 0;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.str_alarmsetting);
        builder.setSingleChoiceItems(items, this.selectItem, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                ActivityView.this.selectItem = item;
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ActivityView.this.setAlarm(ActivityView.this.selectItem);
            }
        });
        if (this.alarmRecno != 0) {
            builder.setNeutralButton((int) R.string.str_clear, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(ActivityView.this);
                    alert.setTitle((int) R.string.str_check);
                    alert.setMessage((int) R.string.str_msg_clear);
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            AppUtil.deleteAlarm(ActivityView.this, ActivityView.this.alarmRecno);
                            ActivityView.this.setView();
                        }
                    });
                    alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                    alert.show();
                }
            });
        }
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }
}
