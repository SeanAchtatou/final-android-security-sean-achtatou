package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ActivityCalendar extends Activity implements View.OnClickListener, View.OnLongClickListener, HolidaySetTaskCallback {
    private ListView LV_MEMO;
    private final int MENU_DAY = 4;
    private TextView TV_INFO;
    private TextView TV_MONTH;
    /* access modifiers changed from: private */
    public int aDay = this.cal.get(5);
    private int aMonth = this.cal.get(2);
    private int aYear = this.cal.get(1);
    /* access modifiers changed from: private */
    public ScheduleAdapter adapter = null;
    private List<Integer> bDayList = new ArrayList(43);
    private Map<Integer, Integer> bDayMap = new HashMap(43);
    /* access modifiers changed from: private */
    public Calendar cal = Calendar.getInstance();
    private boolean createFlg = false;
    private Map<Integer, String> holidayMap;
    private int index = -1;
    private List<Integer> ivCateList1 = new ArrayList(43);
    private List<Integer> ivCateList2 = new ArrayList(43);
    private List<Integer> ivCateList3 = new ArrayList(43);
    private List<Integer> ivRangeList = new ArrayList(43);
    private List<Integer> llDayList = new ArrayList(43);
    /* access modifiers changed from: private */
    public boolean moveDay = false;
    private int oldIndex = -1;
    private List<Integer> tvDayList = new ArrayList(43);

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.createFlg = true;
        setContentView((int) R.layout.act_calendar);
        this.cal.setTimeInMillis(System.currentTimeMillis());
        this.TV_INFO = (TextView) findViewById(R.id.TV_INFO);
        this.TV_MONTH = (TextView) findViewById(R.id.TV_MONTH);
        ((ImageView) findViewById(R.id.IV_BACK)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityCalendar.this.cal.add(2, -1);
                ActivityCalendar.this.setView();
                ActivityCalendar.this.updateAdapter();
            }
        });
        ((TextView) findViewById(R.id.TV_TODAY)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityCalendar.this.moveDay = true;
                ActivityCalendar.this.cal.setTimeInMillis(System.currentTimeMillis());
                ActivityCalendar.this.aDay = ActivityCalendar.this.cal.get(5);
                ActivityCalendar.this.setView();
                ActivityCalendar.this.updateAdapter();
                ActivityCalendar.this.moveDay = false;
            }
        });
        ((ImageView) findViewById(R.id.IV_NEXT)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityCalendar.this.cal.add(2, 1);
                ActivityCalendar.this.setView();
                ActivityCalendar.this.updateAdapter();
            }
        });
        this.LV_MEMO = (ListView) findViewById(R.id.LV_MEMO);
        this.adapter = new ScheduleAdapter(this, R.layout.row_schedule, AppUtil.getScheMemo(this, this.aYear, this.aMonth, this.aDay));
        this.LV_MEMO.setAdapter((ListAdapter) this.adapter);
        this.LV_MEMO.setScrollingCacheEnabled(false);
        this.LV_MEMO.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(ActivityCalendar.this, ActivityView.class);
                intent.putExtra("recno", ActivityCalendar.this.adapter.getItem(position).getRecno());
                ActivityCalendar.this.startActivity(intent);
            }
        });
        this.LV_MEMO.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                CharSequence[] searchs = {ActivityCalendar.this.getString(R.string.str_edit), ActivityCalendar.this.getString(R.string.str_remove), ActivityCalendar.this.getString(R.string.str_remove1)};
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCalendar.this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(ActivityCalendar.this, ActivityEdit.class);
                            intent.putExtra("recno", ActivityCalendar.this.adapter.getItem(position).getRecno());
                            ActivityCalendar.this.startActivity(intent);
                        } else if (which == 1) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ActivityCalendar.this);
                            alert.setTitle((int) R.string.str_check);
                            alert.setMessage((int) R.string.str_msg_remove);
                            final int i = position;
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteMemo(ActivityCalendar.this, ActivityCalendar.this.adapter.getItem(i).getRecno());
                                    ActivityCalendar.this.setView();
                                    ActivityCalendar.this.updateAdapter();
                                }
                            });
                            alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert.show();
                        } else {
                            AlertDialog.Builder alert2 = new AlertDialog.Builder(ActivityCalendar.this);
                            alert2.setTitle((int) R.string.str_check);
                            alert2.setMessage((int) R.string.str_msg_remove1);
                            final int i2 = position;
                            alert2.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteSchedule(ActivityCalendar.this, ActivityCalendar.this.adapter.getItem(i2).getRecno());
                                    ActivityCalendar.this.setView();
                                    ActivityCalendar.this.updateAdapter();
                                }
                            });
                            alert2.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert2.show();
                        }
                    }
                });
                builder.create().show();
                return false;
            }
        });
        this.tvDayList.add(0);
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY1));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY2));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY3));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY4));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY5));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY6));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY7));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY8));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY9));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY10));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY11));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY12));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY13));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY14));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY15));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY16));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY17));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY18));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY19));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY20));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY21));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY22));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY23));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY24));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY25));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY26));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY27));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY28));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY29));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY30));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY31));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY32));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY33));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY34));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY35));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY36));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY37));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY38));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY39));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY40));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY41));
        this.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY42));
        this.ivRangeList.add(0);
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE1));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE2));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE3));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE4));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE5));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE6));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE7));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE8));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE9));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE10));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE11));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE12));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE13));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE14));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE15));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE16));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE17));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE18));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE19));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE20));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE21));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE22));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE23));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE24));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE25));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE26));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE27));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE28));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE29));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE30));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE31));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE32));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE33));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE34));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE35));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE36));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE37));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE38));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE39));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE40));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE41));
        this.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE42));
        this.ivCateList1.add(0);
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_1));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_2));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_3));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_4));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_5));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_6));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_7));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_8));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_9));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_10));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_11));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_12));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_13));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_14));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_15));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_16));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_17));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_18));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_19));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_20));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_21));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_22));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_23));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_24));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_25));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_26));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_27));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_28));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_29));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_30));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_31));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_32));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_33));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_34));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_35));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_36));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_37));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_38));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_39));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_40));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_41));
        this.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_42));
        this.ivCateList2.add(0);
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_1));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_2));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_3));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_4));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_5));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_6));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_7));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_8));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_9));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_10));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_11));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_12));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_13));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_14));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_15));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_16));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_17));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_18));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_19));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_20));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_21));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_22));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_23));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_24));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_25));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_26));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_27));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_28));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_29));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_30));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_31));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_32));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_33));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_34));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_35));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_36));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_37));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_38));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_39));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_40));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_41));
        this.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_42));
        this.ivCateList3.add(0);
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_1));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_2));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_3));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_4));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_5));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_6));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_7));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_8));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_9));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_10));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_11));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_12));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_13));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_14));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_15));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_16));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_17));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_18));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_19));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_20));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_21));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_22));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_23));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_24));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_25));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_26));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_27));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_28));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_29));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_30));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_31));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_32));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_33));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_34));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_35));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_36));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_37));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_38));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_39));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_40));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_41));
        this.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_42));
        this.bDayList.add(0);
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY1));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY2));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY3));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY4));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY5));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY6));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY7));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY8));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY9));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY10));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY11));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY12));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY13));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY14));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY15));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY16));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY17));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY18));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY19));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY20));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY21));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY22));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY23));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY24));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY25));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY26));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY27));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY28));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY29));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY30));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY31));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY32));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY33));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY34));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY35));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY36));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY37));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY38));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY39));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY40));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY41));
        this.bDayList.add(Integer.valueOf((int) R.id.B_DAY42));
        int i = 0;
        for (Integer id : this.bDayList) {
            this.bDayMap.put(id, Integer.valueOf(i));
            i++;
        }
        this.llDayList.add(0);
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY1));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY2));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY3));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY4));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY5));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY6));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY7));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY8));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY9));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY10));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY11));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY12));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY13));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY14));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY15));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY16));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY17));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY18));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY19));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY20));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY21));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY22));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY23));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY24));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY25));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY26));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY27));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY28));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY29));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY30));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY31));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY32));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY33));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY34));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY35));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY36));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY37));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY38));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY39));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY40));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY41));
        this.llDayList.add(Integer.valueOf((int) R.id.LL_DAY42));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Prefs prefs = new Prefs(this);
        setView();
        if (!this.createFlg) {
            updateAdapter();
        } else if (prefs.getInitHoliday()) {
            new HolidaySetTask(this, this).execute("");
            prefs.setInitHoliday(false);
        } else {
            setDayInfo();
        }
        this.createFlg = false;
    }

    public void onClick(View v) {
        dayClick(v);
    }

    public boolean onLongClick(View v) {
        dayClick(v);
        if (this.aDay == 0) {
            return true;
        }
        Intent intent = new Intent(this, ActivityScheMemo.class);
        intent.putExtra("year", this.aYear);
        intent.putExtra("month", this.aMonth);
        intent.putExtra("day", this.aDay);
        startActivity(intent);
        return true;
    }

    private void dayClick(View v) {
        this.index = this.bDayMap.get(Integer.valueOf(v.getId())).intValue();
        if (this.oldIndex != -1) {
            ((LinearLayout) findViewById(this.llDayList.get(this.oldIndex).intValue())).setBackgroundColor(-16777216);
        }
        ((LinearLayout) findViewById(this.llDayList.get(this.index).intValue())).setBackgroundColor(-6512996);
        this.oldIndex = this.index;
        if (AppUtil.checkNull(((TextView) findViewById(this.tvDayList.get(this.index).intValue())).getText()).booleanValue()) {
            this.aDay = 0;
            this.cal.set(5, 1);
        } else {
            this.aDay = Integer.valueOf(((TextView) findViewById(this.tvDayList.get(this.index).intValue())).getText().toString()).intValue();
            this.cal.set(5, this.aDay);
        }
        updateAdapter();
    }

    /* access modifiers changed from: private */
    public void setView() {
        SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
        int s = 0;
        Calendar now = Calendar.getInstance();
        int nowYear = now.get(1);
        int nowMonth = now.get(2);
        int nowDay = now.get(5);
        this.cal.set(5, 1);
        switch (this.cal.get(7)) {
            case 1:
                s = 1;
                break;
            case DBEngine.DB_VERSION /*2*/:
                s = 2;
                break;
            case 3:
                s = 3;
                break;
            case 4:
                s = 4;
                break;
            case 5:
                s = 5;
                break;
            case 6:
                s = 6;
                break;
            case 7:
                s = 7;
                break;
        }
        int year = this.cal.get(1);
        int month = this.cal.get(2);
        this.holidayMap = new HashMap();
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT");
        sql.append(" holi_day,");
        if (Locale.JAPAN.equals(Locale.getDefault())) {
            sql.append(" title1");
        } else {
            sql.append(" title2");
        }
        sql.append(" FROM HOLIDAY");
        sql.append(" WHERE holi_year = " + String.valueOf(year));
        sql.append(" AND   holi_month = " + String.valueOf(month));
        Cursor RS = DB.rawQuery(sql.toString(), null);
        while (RS.moveToNext()) {
            this.holidayMap.put(Integer.valueOf(RS.getInt(0)), RS.getString(1));
        }
        RS.close();
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList(2);
        int tDay = 0;
        StringBuilder sql2 = new StringBuilder();
        sql2.append(" SELECT");
        sql2.append(" S.sche_day,");
        sql2.append(" S.day_flg,");
        sql2.append(" C.icon_id ");
        sql2.append(" FROM SCHEDULE S");
        sql2.append(" INNER JOIN MEMO M ON");
        sql2.append("       S.memo_no = M.recno");
        sql2.append(" INNER JOIN CATEGORY C ON");
        sql2.append("       M.cate_id = C.id");
        sql2.append(" WHERE S.sche_year = " + String.valueOf(year));
        sql2.append(" AND   S.sche_month = " + String.valueOf(month));
        sql2.append(" ORDER BY");
        sql2.append(" S.sche_day,");
        sql2.append(" S.day_flg DESC,");
        sql2.append(" M.sche_s_time,");
        sql2.append(" M.recno");
        Cursor RS2 = DB.rawQuery(sql2.toString(), null);
        while (RS2.moveToNext()) {
            if (RS2.getInt(0) != tDay) {
                if (tDay != 0) {
                    hashMap.put(Integer.valueOf(tDay), arrayList);
                }
                arrayList = new ArrayList(2);
            }
            arrayList.add(Integer.valueOf(RS2.getInt(1)));
            arrayList.add(Integer.valueOf(RS2.getInt(2)));
            tDay = RS2.getInt(0);
        }
        RS2.close();
        if (tDay != 0) {
            hashMap.put(Integer.valueOf(tDay), arrayList);
        }
        this.TV_MONTH.setText(String.valueOf(String.valueOf(year)) + getString(R.string.str_year) + String.valueOf(month + 1) + getString(R.string.str_month2));
        for (int i = 1; i <= 42; i++) {
            int cMonth = this.cal.get(2);
            int cDay = this.cal.get(5);
            int cWeek = this.cal.get(7);
            if (this.createFlg) {
                ((Button) findViewById(this.bDayList.get(i).intValue())).setOnClickListener(this);
                ((Button) findViewById(this.bDayList.get(i).intValue())).setOnLongClickListener(this);
            }
            ((TextView) findViewById(this.tvDayList.get(i).intValue())).setBackgroundColor(0);
            ((ImageView) findViewById(this.ivRangeList.get(i).intValue())).setVisibility(4);
            ((ImageView) findViewById(this.ivCateList1.get(i).intValue())).setVisibility(4);
            ((ImageView) findViewById(this.ivCateList2.get(i).intValue())).setVisibility(4);
            ((ImageView) findViewById(this.ivCateList3.get(i).intValue())).setVisibility(4);
            if (i < s || cMonth != month) {
                ((TextView) findViewById(this.tvDayList.get(i).intValue())).setText((CharSequence) null);
            } else {
                ((TextView) findViewById(this.tvDayList.get(i).intValue())).setText(String.valueOf(cDay));
                if (year == nowYear && cMonth == nowMonth && cDay == nowDay) {
                    ((TextView) findViewById(this.tvDayList.get(i).intValue())).setBackgroundColor(-13395457);
                    if (this.createFlg) {
                        ((LinearLayout) findViewById(this.llDayList.get(i).intValue())).setBackgroundColor(-6512996);
                        this.index = i;
                        this.oldIndex = i;
                    }
                }
                if (this.moveDay && cDay == this.aDay) {
                    ((LinearLayout) findViewById(this.llDayList.get(this.oldIndex).intValue())).setBackgroundColor(-16777216);
                    ((LinearLayout) findViewById(this.llDayList.get(i).intValue())).setBackgroundColor(-6512996);
                    this.index = i;
                    this.oldIndex = i;
                }
                if (cWeek != 1) {
                    if (this.holidayMap.containsKey(Integer.valueOf(cDay))) {
                        ((TextView) findViewById(this.tvDayList.get(i).intValue())).setTextColor(-18751);
                    } else if (cWeek == 7) {
                        ((TextView) findViewById(this.tvDayList.get(i).intValue())).setTextColor(-16711681);
                    } else {
                        ((TextView) findViewById(this.tvDayList.get(i).intValue())).setTextColor(-1);
                    }
                }
                if (hashMap.containsKey(Integer.valueOf(cDay))) {
                    int z = 0;
                    for (Integer sm : (List) hashMap.get(Integer.valueOf(cDay))) {
                        z++;
                        if (z % 2 != 0) {
                            if (sm.intValue() != 0) {
                                if (sm.intValue() == 1) {
                                    ((ImageView) findViewById(this.ivRangeList.get(i).intValue())).setImageResource(R.drawable.range_s);
                                } else if (sm.intValue() == 2) {
                                    ((ImageView) findViewById(this.ivRangeList.get(i).intValue())).setImageResource(R.drawable.range);
                                } else {
                                    ((ImageView) findViewById(this.ivRangeList.get(i).intValue())).setImageResource(R.drawable.range_e);
                                }
                                ((ImageView) findViewById(this.ivRangeList.get(i).intValue())).setVisibility(0);
                            }
                        } else if (z == 2) {
                            ((ImageView) findViewById(this.ivCateList1.get(i).intValue())).setImageResource(AppUtil.getCateResId15(sm.intValue()));
                            ((ImageView) findViewById(this.ivCateList1.get(i).intValue())).setVisibility(0);
                        } else if (z == 4) {
                            ((ImageView) findViewById(this.ivCateList2.get(i).intValue())).setImageResource(AppUtil.getCateResId15(sm.intValue()));
                            ((ImageView) findViewById(this.ivCateList2.get(i).intValue())).setVisibility(0);
                        } else {
                            ((ImageView) findViewById(this.ivCateList3.get(i).intValue())).setImageResource(AppUtil.getCateResId15(sm.intValue()));
                            ((ImageView) findViewById(this.ivCateList3.get(i).intValue())).setVisibility(0);
                        }
                        if (z == 6) {
                        }
                    }
                }
                this.cal.add(5, 1);
            }
        }
        this.cal.set(1, year);
        this.cal.set(2, month);
        DB.close();
        this.aYear = year;
        this.aMonth = month;
        if (AppUtil.checkNull(((TextView) findViewById(this.tvDayList.get(this.index).intValue())).getText()).booleanValue()) {
            this.aDay = 0;
            this.cal.set(5, 1);
            return;
        }
        this.aDay = Integer.valueOf(((TextView) findViewById(this.tvDayList.get(this.index).intValue())).getText().toString()).intValue();
        this.cal.set(5, this.aDay);
    }

    /* access modifiers changed from: private */
    public void updateAdapter() {
        this.adapter.setMemos(AppUtil.getScheMemo(this, this.aYear, this.aMonth, this.aDay));
        this.adapter.notifyDataSetChanged();
        setDayInfo();
    }

    public void onSuccessHolidaySet(String message) {
        Toast.makeText(this, message, 1).show();
        setView();
        updateAdapter();
    }

    public void onFailedHolidaySet(String message) {
        Toast.makeText(this, message, 1).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 4, 0, (int) R.string.str_menu_day).setIcon((int) R.drawable.me_day);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 4:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        ActivityCalendar.this.moveDay = true;
                        ActivityCalendar.this.cal.set(1, year);
                        ActivityCalendar.this.cal.set(2, monthOfYear);
                        ActivityCalendar.this.aDay = dayOfMonth;
                        ActivityCalendar.this.setView();
                        ActivityCalendar.this.updateAdapter();
                        ActivityCalendar.this.moveDay = false;
                    }
                }, this.aYear, this.aMonth, this.cal.get(5)).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setDayInfo() {
        if (this.aDay != 0) {
            Prefs prefs = new Prefs(this);
            StringBuilder str = new StringBuilder();
            str.append(String.valueOf(String.format("%1$04d", Integer.valueOf(this.aYear))) + getString(R.string.str_year));
            str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(this.aMonth + 1))) + getString(R.string.str_month));
            str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(this.aDay))) + getString(R.string.str_day));
            Calendar cal2 = Calendar.getInstance();
            cal2.set(1, this.aYear);
            cal2.set(2, this.aMonth);
            cal2.set(5, this.aDay);
            switch (cal2.get(7)) {
                case 1:
                    str.append("(<font color=\"#ffb6c1\">" + getString(R.string.str_week1) + "</font>) ");
                    break;
                case DBEngine.DB_VERSION /*2*/:
                    str.append("(<font color=\"White\">" + getString(R.string.str_week2) + "</font>) ");
                    break;
                case 3:
                    str.append("(<font color=\"White\">" + getString(R.string.str_week3) + "</font>) ");
                    break;
                case 4:
                    str.append("(<font color=\"White\">" + getString(R.string.str_week4) + "</font>) ");
                    break;
                case 5:
                    str.append("(<font color=\"White\">" + getString(R.string.str_week5) + "</font>) ");
                    break;
                case 6:
                    str.append("(<font color=\"White\">" + getString(R.string.str_week6) + "</font>) ");
                    break;
                case 7:
                    str.append("(<font color=\"#00ffff\">" + getString(R.string.str_week7) + "</font>) ");
                    break;
            }
            if (prefs.getSixWeek()) {
                str.append(String.valueOf(QReki.RokuYo(this.aYear, this.aMonth + 1, this.aDay)) + " ");
            }
            if (this.holidayMap.containsKey(Integer.valueOf(this.aDay))) {
                str.append("<font color=\"#ffb6c1\">" + this.holidayMap.get(Integer.valueOf(this.aDay)) + "</font>");
            }
            this.TV_INFO.setText(Html.fromHtml(str.toString()));
            return;
        }
        this.TV_INFO.setText((CharSequence) null);
    }
}
