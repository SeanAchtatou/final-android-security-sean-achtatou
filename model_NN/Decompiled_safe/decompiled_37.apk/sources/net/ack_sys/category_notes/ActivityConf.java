package net.ack_sys.category_notes;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Map;

public class ActivityConf extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener, HolidaySetTaskCallback, BackupDbTaskCallback, VacuumDbTaskCallback, RestoreDbTaskCallback, HolidayDelTaskCallback {
    private static final int MESSAGE_DIALOG_VACUUM = 1;
    private static final int PROGRESS_DIALOG_VACUUM = 2;
    private ListPreference autoStop;
    private Map<String, String> autoStopMap = new HashMap();
    /* access modifiers changed from: private */
    public Preference backupNotes;
    private ListPreference color;
    private Map<String, String> colorMap = new HashMap();
    private ListPreference fontSize;
    private Map<String, String> fontSizeMap = new HashMap();
    /* access modifiers changed from: private */
    public Preference holidayDel;
    /* access modifiers changed from: private */
    public Preference holidaySet;
    private ListPreference manner;
    private Map<String, String> mannerMap = new HashMap();
    /* access modifiers changed from: private */
    public Preference optimization;
    /* access modifiers changed from: private */
    public Preference restoreNotes;
    private ListPreference snooze;
    private ListPreference snoozeCnt;
    private Map<String, String> snoozeCntMap = new HashMap();
    private Map<String, String> snoozeMap = new HashMap();
    private ListPreference sortOrder;
    private Map<String, String> sortOrderMap = new HashMap();
    private ListPreference tagIcon;
    private Map<String, String> tagIconMap = new HashMap();
    private ListPreference textColor;
    private Map<String, String> textColorMap = new HashMap();
    private ListPreference transparency;
    private Map<String, String> transparencyMap = new HashMap();
    private ListPreference volume;
    private Map<String, String> volumeMap = new HashMap();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref);
        Prefs prefs = new Prefs(this);
        getWindow().addFlags(128);
        this.fontSize = (ListPreference) findPreference("fontSize");
        String[] entrys = getResources().getStringArray(R.array.arr_fontSize);
        String[] values = getResources().getStringArray(R.array.arr_fontSize_value);
        for (int i = 0; i < values.length; i += MESSAGE_DIALOG_VACUUM) {
            this.fontSizeMap.put(values[i], entrys[i]);
        }
        this.fontSize.setSummary(this.fontSizeMap.get(prefs.getFontSize()));
        this.sortOrder = (ListPreference) findPreference("sortOrder");
        String[] entrys2 = getResources().getStringArray(R.array.arr_sortOrder);
        String[] values2 = getResources().getStringArray(R.array.arr_sortOrder_value);
        for (int i2 = 0; i2 < values2.length; i2 += MESSAGE_DIALOG_VACUUM) {
            this.sortOrderMap.put(values2[i2], entrys2[i2]);
        }
        this.sortOrder.setSummary(this.sortOrderMap.get(prefs.getSortOrder()));
        this.tagIcon = (ListPreference) findPreference("tagIcon");
        String[] entrys3 = getResources().getStringArray(R.array.arr_tagIcon);
        String[] values3 = getResources().getStringArray(R.array.arr_tagIcon_value);
        for (int i3 = 0; i3 < values3.length; i3 += MESSAGE_DIALOG_VACUUM) {
            this.tagIconMap.put(values3[i3], entrys3[i3]);
        }
        this.tagIcon.setSummary(this.tagIconMap.get(prefs.getTagIcon()));
        this.volume = (ListPreference) findPreference("volume");
        String[] entrys4 = getResources().getStringArray(R.array.arr_volume);
        String[] values4 = getResources().getStringArray(R.array.arr_volume_value);
        for (int i4 = 0; i4 < values4.length; i4 += MESSAGE_DIALOG_VACUUM) {
            this.volumeMap.put(values4[i4], entrys4[i4]);
        }
        this.volume.setSummary(this.volumeMap.get(prefs.getVolume()));
        this.autoStop = (ListPreference) findPreference("autoStop");
        String[] entrys5 = getResources().getStringArray(R.array.arr_autoStop);
        String[] values5 = getResources().getStringArray(R.array.arr_autoStop_value);
        for (int i5 = 0; i5 < values5.length; i5 += MESSAGE_DIALOG_VACUUM) {
            this.autoStopMap.put(values5[i5], entrys5[i5]);
        }
        this.autoStop.setSummary(this.autoStopMap.get(prefs.getAutoStop()));
        this.snoozeCnt = (ListPreference) findPreference("snoozeCnt");
        String[] entrys6 = getResources().getStringArray(R.array.arr_snoozeCnt);
        String[] values6 = getResources().getStringArray(R.array.arr_snoozeCnt_value);
        for (int i6 = 0; i6 < values6.length; i6 += MESSAGE_DIALOG_VACUUM) {
            this.snoozeCntMap.put(values6[i6], entrys6[i6]);
        }
        this.snoozeCnt.setSummary(this.snoozeCntMap.get(prefs.getSnoozeCnt()));
        this.snooze = (ListPreference) findPreference(Alarm.SNOOZE);
        String[] entrys7 = getResources().getStringArray(R.array.arr_snooze);
        String[] values7 = getResources().getStringArray(R.array.arr_snooze_value);
        for (int i7 = 0; i7 < values7.length; i7 += MESSAGE_DIALOG_VACUUM) {
            this.snoozeMap.put(values7[i7], entrys7[i7]);
        }
        this.snooze.setSummary(this.snoozeMap.get(prefs.getSnooze()));
        this.manner = (ListPreference) findPreference("manner");
        String[] entrys8 = getResources().getStringArray(R.array.arr_manner);
        String[] values8 = getResources().getStringArray(R.array.arr_manner_value);
        for (int i8 = 0; i8 < values8.length; i8 += MESSAGE_DIALOG_VACUUM) {
            this.mannerMap.put(values8[i8], entrys8[i8]);
        }
        this.manner.setSummary(this.mannerMap.get(prefs.getManner()));
        this.color = (ListPreference) findPreference("color");
        String[] entrys9 = getResources().getStringArray(R.array.arr_color);
        String[] values9 = getResources().getStringArray(R.array.arr_color_value);
        for (int i9 = 0; i9 < values9.length; i9 += MESSAGE_DIALOG_VACUUM) {
            this.colorMap.put(values9[i9], entrys9[i9]);
        }
        this.color.setSummary(this.colorMap.get(prefs.getColor()));
        this.transparency = (ListPreference) findPreference("transparency");
        String[] entrys10 = getResources().getStringArray(R.array.arr_transparency);
        String[] values10 = getResources().getStringArray(R.array.arr_transparency_value);
        for (int i10 = 0; i10 < values10.length; i10 += MESSAGE_DIALOG_VACUUM) {
            this.transparencyMap.put(values10[i10], entrys10[i10]);
        }
        this.transparency.setSummary(this.transparencyMap.get(prefs.getTransparency()));
        this.textColor = (ListPreference) findPreference("textColor");
        String[] entrys11 = getResources().getStringArray(R.array.arr_text_color);
        String[] values11 = getResources().getStringArray(R.array.arr_text_color_value);
        for (int i11 = 0; i11 < values11.length; i11 += MESSAGE_DIALOG_VACUUM) {
            this.textColorMap.put(values11[i11], entrys11[i11]);
        }
        this.textColor.setSummary(this.textColorMap.get(prefs.getTextColor()));
        this.holidaySet = findPreference("holidaySet");
        this.holidaySet.setOnPreferenceClickListener(new PrefClickListener(this, null));
        this.holidayDel = findPreference("holidayDel");
        this.holidayDel.setOnPreferenceClickListener(new PrefClickListener(this, null));
        this.optimization = findPreference("optimization");
        this.optimization.setOnPreferenceClickListener(new PrefClickListener(this, null));
        this.backupNotes = findPreference("backupNotes");
        this.backupNotes.setOnPreferenceClickListener(new PrefClickListener(this, null));
        this.restoreNotes = findPreference("restoreNotes");
        this.restoreNotes.setOnPreferenceClickListener(new PrefClickListener(this, null));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Prefs prefs = new Prefs(this);
        if (key.equals("fontSize")) {
            this.fontSize.setSummary(this.fontSizeMap.get(prefs.getFontSize()));
        } else if (key.equals("sortOrder")) {
            this.sortOrder.setSummary(this.sortOrderMap.get(prefs.getSortOrder()));
        } else if (key.equals("tagIcon")) {
            this.tagIcon.setSummary(this.tagIconMap.get(prefs.getTagIcon()));
        } else if (key.equals("volume")) {
            this.volume.setSummary(this.volumeMap.get(prefs.getVolume()));
        } else if (key.equals("autoStop")) {
            this.autoStop.setSummary(this.autoStopMap.get(prefs.getAutoStop()));
        } else if (key.equals("snoozeCnt")) {
            this.snoozeCnt.setSummary(this.snoozeCntMap.get(prefs.getSnoozeCnt()));
        } else if (key.equals(Alarm.SNOOZE)) {
            this.snooze.setSummary(this.snoozeMap.get(prefs.getSnooze()));
        } else if (key.equals("manner")) {
            this.manner.setSummary(this.mannerMap.get(prefs.getManner()));
        } else if (key.equals("color")) {
            this.color.setSummary(this.colorMap.get(prefs.getColor()));
            AppUtil.sendWidgetAction(this, AppUtil.ACTION_CHANGE_COLOR);
        } else if (key.equals("transparency")) {
            this.transparency.setSummary(this.transparencyMap.get(prefs.getTransparency()));
            AppUtil.sendWidgetAction(this, AppUtil.ACTION_CHANGE_COLOR);
        } else if (key.equals("textColor")) {
            this.textColor.setSummary(this.textColorMap.get(prefs.getTextColor()));
            AppUtil.sendWidgetAction(this, AppUtil.ACTION_CHANGE_COLOR);
        }
    }

    private class PrefClickListener implements Preference.OnPreferenceClickListener {
        private PrefClickListener() {
        }

        /* synthetic */ PrefClickListener(ActivityConf activityConf, PrefClickListener prefClickListener) {
            this();
        }

        public boolean onPreferenceClick(Preference preference) {
            if (preference == ActivityConf.this.holidaySet) {
                ActivityConf.this.showHolidayDialog();
            } else if (preference == ActivityConf.this.holidayDel) {
                ActivityConf.this.showHolidayDelDialog();
            } else if (preference == ActivityConf.this.optimization) {
                ActivityConf.this.showDialog(ActivityConf.MESSAGE_DIALOG_VACUUM);
            } else if (preference == ActivityConf.this.backupNotes) {
                ActivityConf.this.showBackupDialog();
            } else if (preference == ActivityConf.this.restoreNotes) {
                ActivityConf.this.showRestoreDialog();
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog dialog = super.onCreateDialog(id);
        if (id == MESSAGE_DIALOG_VACUUM) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle((int) R.string.str_check);
            builder.setCancelable(false);
            builder.setMessage((int) R.string.msg_check_vacuum_db);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ActivityConf.this.showDialog(2);
                    VacuumDbTask task = new VacuumDbTask(ActivityConf.this, ActivityConf.this);
                    String[] strArr = new String[ActivityConf.MESSAGE_DIALOG_VACUUM];
                    strArr[0] = "";
                    task.execute(strArr);
                }
            });
            builder.setNegativeButton("No", (DialogInterface.OnClickListener) null);
            return builder.create();
        } else if (id != 2) {
            return dialog;
        } else {
            ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.msg_vacuum_db_wait));
            progressDialog.setCancelable(false);
            return progressDialog;
        }
    }

    public void onSuccessVacuumDb(String message) {
        dismissDialog(2);
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onFailedVacuumDb(String message) {
        dismissDialog(2);
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    /* access modifiers changed from: private */
    public void showHolidayDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle((int) R.string.str_check);
        alert.setMessage((int) R.string.str_msg_holidaycheck);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                HolidaySetTask task = new HolidaySetTask(ActivityConf.this, ActivityConf.this);
                String[] strArr = new String[ActivityConf.MESSAGE_DIALOG_VACUUM];
                strArr[0] = "";
                task.execute(strArr);
            }
        });
        alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
        alert.show();
    }

    /* access modifiers changed from: private */
    public void showHolidayDelDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle((int) R.string.str_check);
        alert.setMessage((int) R.string.str_msg_holidaydelcheck);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                HolidayDelTask task = new HolidayDelTask(ActivityConf.this, ActivityConf.this);
                String[] strArr = new String[ActivityConf.MESSAGE_DIALOG_VACUUM];
                strArr[0] = "";
                task.execute(strArr);
            }
        });
        alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
        alert.show();
    }

    /* access modifiers changed from: private */
    public void showBackupDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle((int) R.string.str_check);
        alert.setMessage(String.valueOf(getString(R.string.str_msg_backupcheck)) + "\n\n" + (Environment.getExternalStorageDirectory().getPath() + "/" + getPackageName() + "/" + DBEngine.DB_NAME));
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BackupDbTask task = new BackupDbTask(ActivityConf.this, ActivityConf.this);
                String[] strArr = new String[ActivityConf.MESSAGE_DIALOG_VACUUM];
                strArr[0] = DBEngine.DB_NAME;
                task.execute(strArr);
            }
        });
        alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
        alert.show();
    }

    /* access modifiers changed from: private */
    public void showRestoreDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle((int) R.string.str_check);
        alert.setMessage(String.valueOf(getString(R.string.str_msg_restorecheck)) + "\n\n" + (Environment.getExternalStorageDirectory().getPath() + "/" + getPackageName() + "/" + DBEngine.DB_NAME));
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                RestoreDbTask task = new RestoreDbTask(ActivityConf.this, ActivityConf.this);
                String[] strArr = new String[ActivityConf.MESSAGE_DIALOG_VACUUM];
                strArr[0] = DBEngine.DB_NAME;
                task.execute(strArr);
            }
        });
        alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
        alert.show();
    }

    public void onSuccessHolidaySet(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onFailedHolidaySet(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onSuccessHolidayDel(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onFailedHolidayDel(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onSuccessBackupDb(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onFailedBackupDb(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onSuccessRestoreDb(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }

    public void onFailedRestoreDb(String message) {
        Toast.makeText(this, message, (int) MESSAGE_DIALOG_VACUUM).show();
    }
}
