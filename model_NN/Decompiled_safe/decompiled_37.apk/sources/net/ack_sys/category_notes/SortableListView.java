package net.ack_sys.category_notes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

public class SortableListView extends ListView implements AdapterView.OnItemLongClickListener {
    private static final Bitmap.Config DRAG_BITMAP_CONFIG = Bitmap.Config.ARGB_8888;
    private static final int SCROLL_SPEED_FAST = 25;
    private static final int SCROLL_SPEED_SLOW = 8;
    private MotionEvent mActionDownEvent;
    private int mBitmapBackgroundColor = Color.argb(128, 255, 255, 255);
    private Bitmap mDragBitmap = null;
    private ImageView mDragImageView = null;
    private DragListener mDragListener = new SimpleDragListener();
    private boolean mDragging = false;
    private WindowManager.LayoutParams mLayoutParams = null;
    private int mPositionFrom = -1;
    private boolean mSortable = false;

    public interface DragListener {
        int onDuringDrag(int i, int i2);

        int onStartDrag(int i);

        boolean onStopDrag(int i, int i2);
    }

    public SortableListView(Context context) {
        super(context);
        setOnItemLongClickListener(this);
    }

    public SortableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOnItemLongClickListener(this);
    }

    public SortableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOnItemLongClickListener(this);
    }

    public void setDragListener(DragListener listener) {
        this.mDragListener = listener;
    }

    public void setSortable(boolean sortable) {
        this.mSortable = sortable;
    }

    public void setBackgroundColor(int color) {
        this.mBitmapBackgroundColor = color;
    }

    public boolean getSortable() {
        return this.mSortable;
    }

    private int eventToPosition(MotionEvent event) {
        return pointToPosition((int) event.getX(), (int) event.getY());
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.mSortable) {
            return super.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case 0:
                storeMotionEvent(event);
                break;
            case 1:
                if (stopDrag(event, true)) {
                    return true;
                }
                break;
            case DBEngine.DB_VERSION:
                if (duringDrag(event)) {
                    return true;
                }
                break;
            case 3:
            case 4:
                if (stopDrag(event, false)) {
                    return true;
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        return startDrag();
    }

    private void storeMotionEvent(MotionEvent event) {
        this.mActionDownEvent = event;
    }

    private boolean startDrag() {
        this.mPositionFrom = eventToPosition(this.mActionDownEvent);
        if (this.mPositionFrom < 0) {
            return false;
        }
        this.mDragging = true;
        View view = getChildByIndex(this.mPositionFrom);
        Canvas canvas = new Canvas();
        WindowManager wm = getWindowManager();
        this.mDragBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), DRAG_BITMAP_CONFIG);
        canvas.setBitmap(this.mDragBitmap);
        view.draw(canvas);
        if (this.mDragImageView != null) {
            wm.removeView(this.mDragImageView);
        }
        if (this.mLayoutParams == null) {
            initLayoutParams();
        }
        this.mDragImageView = new ImageView(getContext());
        this.mDragImageView.setBackgroundColor(this.mBitmapBackgroundColor);
        this.mDragImageView.setImageBitmap(this.mDragBitmap);
        wm.addView(this.mDragImageView, this.mLayoutParams);
        if (this.mDragListener != null) {
            this.mPositionFrom = this.mDragListener.onStartDrag(this.mPositionFrom);
        }
        return duringDrag(this.mActionDownEvent);
    }

    private boolean duringDrag(MotionEvent event) {
        int speed;
        if (!this.mDragging || this.mDragImageView == null) {
            return false;
        }
        int x = (int) event.getX();
        int y = (int) event.getY();
        int height = getHeight();
        int middle = height / 2;
        int fastBound = height / 9;
        int slowBound = height / 4;
        if (event.getEventTime() - event.getDownTime() < 500) {
            speed = 0;
        } else if (y < slowBound) {
            speed = y < fastBound ? -25 : -8;
        } else if (y > height - slowBound) {
            speed = y > height - fastBound ? SCROLL_SPEED_FAST : SCROLL_SPEED_SLOW;
        } else {
            speed = 0;
        }
        if (speed != 0) {
            int middlePosition = pointToPosition(0, middle);
            if (middlePosition == -1) {
                middlePosition = pointToPosition(0, getDividerHeight() + middle + 64);
            }
            View middleView = getChildByIndex(middlePosition);
            if (middleView != null) {
                setSelectionFromTop(middlePosition, middleView.getTop() - speed);
            }
        }
        if (this.mDragImageView.getHeight() < 0) {
            this.mDragImageView.setVisibility(4);
        } else {
            this.mDragImageView.setVisibility(0);
        }
        updateLayoutParams(x, y);
        getWindowManager().updateViewLayout(this.mDragImageView, this.mLayoutParams);
        if (this.mDragListener != null) {
            this.mPositionFrom = this.mDragListener.onDuringDrag(this.mPositionFrom, pointToPosition(x, y));
        }
        return true;
    }

    private boolean stopDrag(MotionEvent event, boolean isDrop) {
        if (!this.mDragging) {
            return false;
        }
        if (isDrop && this.mDragListener != null) {
            this.mDragListener.onStopDrag(this.mPositionFrom, eventToPosition(event));
        }
        this.mDragging = false;
        if (this.mDragImageView == null) {
            return false;
        }
        getWindowManager().removeView(this.mDragImageView);
        this.mDragImageView = null;
        this.mDragBitmap = null;
        return true;
    }

    private View getChildByIndex(int index) {
        return getChildAt(index - getFirstVisiblePosition());
    }

    /* access modifiers changed from: protected */
    public WindowManager getWindowManager() {
        return (WindowManager) getContext().getSystemService("window");
    }

    /* access modifiers changed from: protected */
    public void initLayoutParams() {
        this.mLayoutParams = new WindowManager.LayoutParams();
        this.mLayoutParams.gravity = 51;
        this.mLayoutParams.height = -2;
        this.mLayoutParams.width = -2;
        this.mLayoutParams.flags = 664;
        this.mLayoutParams.format = -3;
        this.mLayoutParams.windowAnimations = 0;
        this.mLayoutParams.x = getLeft();
        this.mLayoutParams.y = getTop();
    }

    /* access modifiers changed from: protected */
    public void updateLayoutParams(int x, int y) {
        this.mLayoutParams.y = (getTop() + y) - 32;
    }

    public static class SimpleDragListener implements DragListener {
        public int onStartDrag(int position) {
            return position;
        }

        public int onDuringDrag(int positionFrom, int positionTo) {
            return positionFrom;
        }

        public boolean onStopDrag(int positionFrom, int positionTo) {
            return (positionFrom != positionTo && positionFrom >= 0) || positionTo >= 0;
        }
    }
}
