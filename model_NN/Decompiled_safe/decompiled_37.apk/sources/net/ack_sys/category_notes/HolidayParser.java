package net.ack_sys.category_notes;

import android.content.res.XmlResourceParser;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class HolidayParser {
    private static final String DAY = "Day";
    private static final String HOLIDAY = "Holiday";
    private static final String MONTH = "Month";
    private static final String NAME1 = "Name1";
    private static final String NAME2 = "Name2";
    private static final String ROW = "Row";
    private static final String YEAR = "Year";
    private final XmlResourceParser parser;

    public HolidayParser(XmlResourceParser parser2) {
        this.parser = parser2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public List<Holiday> parse() throws ParseException {
        List<Holiday> list;
        List<Holiday> holidays = null;
        try {
            int eventType = this.parser.getEventType();
            Holiday currentHoliday = null;
            boolean done = false;
            while (true) {
                list = holidays;
                if (eventType == 1 || done) {
                    List<Holiday> list2 = list;
                } else {
                    switch (eventType) {
                        case 0:
                            try {
                                holidays = new ArrayList<>();
                                break;
                            } catch (Exception e) {
                                return null;
                            }
                        case 1:
                        default:
                            holidays = list;
                            break;
                        case DBEngine.DB_VERSION:
                            String name = this.parser.getName();
                            if (!name.equalsIgnoreCase(ROW)) {
                                if (currentHoliday != null) {
                                    if (!name.equalsIgnoreCase(YEAR)) {
                                        if (!name.equalsIgnoreCase(MONTH)) {
                                            if (!name.equalsIgnoreCase(DAY)) {
                                                if (!name.equalsIgnoreCase(NAME1)) {
                                                    if (name.equalsIgnoreCase(NAME2)) {
                                                        currentHoliday.setTitle2(this.parser.nextText());
                                                        holidays = list;
                                                        break;
                                                    }
                                                } else {
                                                    currentHoliday.setTitle1(this.parser.nextText());
                                                    holidays = list;
                                                    break;
                                                }
                                            } else {
                                                currentHoliday.setHoliDay(this.parser.nextText());
                                                holidays = list;
                                                break;
                                            }
                                        } else {
                                            currentHoliday.setHoliMonth(this.parser.nextText());
                                            holidays = list;
                                            break;
                                        }
                                    } else {
                                        currentHoliday.setHoliYear(this.parser.nextText());
                                        holidays = list;
                                        break;
                                    }
                                }
                                holidays = list;
                                break;
                            } else {
                                currentHoliday = new Holiday();
                                holidays = list;
                                break;
                            }
                        case 3:
                            String name2 = this.parser.getName();
                            if (name2.equalsIgnoreCase(ROW) && currentHoliday != null) {
                                list.add(currentHoliday);
                                holidays = list;
                                break;
                            } else {
                                if (name2.equalsIgnoreCase(HOLIDAY)) {
                                    done = true;
                                    holidays = list;
                                    break;
                                }
                                holidays = list;
                                break;
                            }
                    }
                    eventType = this.parser.next();
                }
            }
            List<Holiday> list22 = list;
            return list;
        } catch (Exception e2) {
            return null;
        }
    }
}
