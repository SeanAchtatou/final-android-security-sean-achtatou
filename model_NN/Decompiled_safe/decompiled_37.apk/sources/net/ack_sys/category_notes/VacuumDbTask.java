package net.ack_sys.category_notes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class VacuumDbTask extends AsyncTask<String, Void, AsyncTaskResult<?>> {
    private VacuumDbTaskCallback callback;
    private Context context;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((AsyncTaskResult<?>) ((AsyncTaskResult) obj));
    }

    public VacuumDbTask(Context context2, VacuumDbTaskCallback callback2) {
        this.context = context2;
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public AsyncTaskResult<?> doInBackground(String... params) {
        SQLiteDatabase DBS = null;
        try {
            DBS = this.context.openOrCreateDatabase(DBEngine.DB_NAME, 0, null);
            DBS.execSQL("vacuum");
            DBS.execSQL("reindex");
            if (DBS != null) {
                try {
                    DBS.close();
                } catch (Exception e) {
                }
            }
            return AsyncTaskResult.createNormalResult(this.context.getString(R.string.msg_vacuum_db_success));
        } catch (Exception e2) {
            AsyncTaskResult<?> createErrorResult = AsyncTaskResult.createErrorResult(this.context.getString(R.string.msg_vacuum_db_failed));
            if (DBS == null) {
                return createErrorResult;
            }
            try {
                DBS.close();
                return createErrorResult;
            } catch (Exception e3) {
                return createErrorResult;
            }
        } catch (Throwable th) {
            if (DBS != null) {
                try {
                    DBS.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncTaskResult<?> result) {
        if (result.isError()) {
            this.callback.onFailedVacuumDb(result.getMessage());
        } else {
            this.callback.onSuccessVacuumDb(result.getMessage());
        }
    }
}
