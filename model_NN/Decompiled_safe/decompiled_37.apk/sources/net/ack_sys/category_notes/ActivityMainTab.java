package net.ack_sys.category_notes;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

public class ActivityMainTab extends TabActivity {
    private final int MENU_CATECONF = 1;
    private final int MENU_CONF = 3;
    private final int MENU_TAGCONF = 2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.act_tab);
        getWindow().addFlags(128);
        Resources res = getResources();
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("home").setIndicator("Home", res.getDrawable(R.drawable.ic_tab_home)).setContent(new Intent().setClass(this, ActivityMain.class)));
        tabHost.addTab(tabHost.newTabSpec("search").setIndicator("Search", res.getDrawable(R.drawable.ic_tab_search)).setContent(new Intent().setClass(this, ActivitySearch.class)));
        tabHost.addTab(tabHost.newTabSpec("calendar").setIndicator("Calendar", res.getDrawable(R.drawable.ic_tab_calendar)).setContent(new Intent().setClass(this, ActivityCalendar.class)));
        tabHost.addTab(tabHost.newTabSpec("alarm").setIndicator("Alarm", res.getDrawable(R.drawable.ic_tab_alarm)).setContent(new Intent().setClass(this, ActivityAlarm.class)));
        tabHost.setCurrentTab(0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.str_menu_editcategory).setIcon((int) R.drawable.me_cateconf);
        menu.add(1, 2, 1, (int) R.string.str_menu_edittag).setIcon((int) R.drawable.me_tagconf);
        menu.add(0, 3, 2, (int) R.string.str_menu_settings).setIcon((int) R.drawable.me_conf);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (new Prefs(this).getTagUse()) {
            menu.setGroupVisible(1, true);
        } else {
            menu.setGroupVisible(1, false);
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                startActivity(new Intent(this, ActivityCateConf.class));
                return true;
            case DBEngine.DB_VERSION /*2*/:
                startActivity(new Intent(this, ActivityTagConf.class));
                return true;
            case 3:
                startActivity(new Intent(this, ActivityConf.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
