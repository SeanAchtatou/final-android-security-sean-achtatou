package net.ack_sys.category_notes;

public class Schedule {
    public static final String DAY_FLG = "day_flg";
    public static final String MEMO_NO = "memo_no";
    public static final String RECNO = "recno";
    public static final String SCHE_DAY = "sche_day";
    public static final String SCHE_MONTH = "sche_month";
    public static final String SCHE_YEAR = "sche_year";
    private int dayFlg;
    private int memoNo;
    private int recno;
    private int scheDay;
    private int scheMonth;
    private int scheYaer;

    public int getRecno() {
        return this.recno;
    }

    public void setRecno(int recno2) {
        this.recno = recno2;
    }

    public int getMemoNo() {
        return this.memoNo;
    }

    public void setMemoNo(int memoNo2) {
        this.memoNo = memoNo2;
    }

    public int getScheYaer() {
        return this.scheYaer;
    }

    public void setScheYaer(int scheYaer2) {
        this.scheYaer = scheYaer2;
    }

    public int getScheMonth() {
        return this.scheMonth;
    }

    public void setScheMonth(int scheMonth2) {
        this.scheMonth = scheMonth2;
    }

    public int getScheDay() {
        return this.scheDay;
    }

    public void setScheDay(int scheDay2) {
        this.scheDay = scheDay2;
    }

    public int getDayFlg() {
        return this.dayFlg;
    }

    public void setDayFlg(int dayFlg2) {
        this.dayFlg = dayFlg2;
    }
}
