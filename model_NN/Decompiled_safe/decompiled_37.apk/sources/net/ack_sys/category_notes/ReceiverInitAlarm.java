package net.ack_sys.category_notes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverInitAlarm extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            AppUtil.setAllAlarm(context);
        } else if ("android.intent.action.TIME_SET".equals(intent.getAction())) {
        } else {
            if ("android.intent.action.TIMEZONE_CHANGED".equals(intent.getAction())) {
                AppUtil.cancelAllAlarm(context);
                AppUtil.setAllAlarm(context);
            } else if ("android.intent.action.PACKAGE_REPLACED".equals(intent.getAction())) {
                AppUtil.setAllAlarm(context);
            }
        }
    }
}
