package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import net.ack_sys.category_notes.SortableListView;

public class ActivityTagConf extends Activity {
    private static final int REQUEST_CODE = 0;
    /* access modifiers changed from: private */
    public SortableListView LV;
    /* access modifiers changed from: private */
    public TagAdapterCf adapter = null;
    private boolean createFlg = false;
    /* access modifiers changed from: private */
    public EditText editText;
    /* access modifiers changed from: private */
    public int mDraggingPosition = -1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.act_tagconf);
        this.createFlg = true;
        getWindow().addFlags(128);
        this.adapter = new TagAdapterCf(this, R.layout.row_tagconf, AppUtil.getTag(this, false));
        this.LV = (SortableListView) findViewById(R.id.list);
        this.LV.setDragListener(new DragListener());
        this.LV.setSortable(true);
        this.LV.setAdapter((ListAdapter) this.adapter);
        this.LV.setScrollingCacheEnabled(false);
        this.LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, final int position, long id) {
                CharSequence[] searchs = {ActivityTagConf.this.getString(R.string.str_changetitle)};
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityTagConf.this);
                builder.setTitle((int) R.string.str_operations);
                builder.setPositiveButton(ActivityTagConf.this.adapter.getItem(position).getEnable() ? ActivityTagConf.this.getString(R.string.str_notuse) : ActivityTagConf.this.getString(R.string.str_using), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ActivityTagConf.this.enableChange(position, ActivityTagConf.this.adapter.getItem(position).getEnable() ? 0 : 1);
                    }
                });
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityTagConf.this.showEditDialog(position);
                    }
                });
                builder.create().show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.createFlg) {
            updateAdapter();
        }
        this.createFlg = false;
    }

    /* access modifiers changed from: private */
    public void enableChange(int position, int enable) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            int id = this.adapter.getItem(position).getId();
            ContentValues values = new ContentValues();
            values.put("enable", Integer.valueOf(enable));
            DB.update("TAG", values, "id = ?", new String[]{String.valueOf(id)});
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            updateAdapter();
        }
    }

    /* access modifiers changed from: private */
    public void titleChange(int position, String newTitle) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            int id = this.adapter.getItem(position).getId();
            ContentValues values = new ContentValues();
            values.put("title", newTitle);
            DB.update("TAG", values, "id = ?", new String[]{String.valueOf(id)});
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            updateAdapter();
        }
    }

    private void updateAdapter() {
        this.adapter.setTags(AppUtil.getTag(this, false));
        this.adapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void showEditDialog(final int position) {
        LinearLayout linearLayout = new LinearLayout(this);
        this.editText = new EditText(this);
        this.editText.setInputType(1);
        this.editText.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this.editText.setText(this.adapter.getItem(position).getTitle());
        Bundle bundleE = this.editText.getInputExtras(true);
        if (bundleE != null) {
            bundleE.putBoolean("allowEmoji", true);
        }
        ImageButton imageButton = new ImageButton(this);
        imageButton.setImageResource(R.drawable.ib_speak);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
                    intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
                    ActivityTagConf.this.startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(ActivityTagConf.this, (int) R.string.str_msg_nospeech, 1).show();
                }
            }
        });
        linearLayout.addView(this.editText);
        linearLayout.addView(imageButton);
        final AlertDialog alert = new AlertDialog.Builder(this).setTitle((int) R.string.str_changetitle).setView(linearLayout).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ActivityTagConf.this.titleChange(position, ActivityTagConf.this.editText.getText().toString());
            }
        }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).create();
        this.editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    alert.getWindow().setSoftInputMode(5);
                }
            }
        });
        alert.show();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            ArrayList<String> results = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (results.size() != 0) {
                if (results.size() > 1) {
                    final CharSequence[] items = new CharSequence[results.size()];
                    for (int i = 0; i < results.size(); i++) {
                        items[i] = results.get(i);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((int) R.string.str_recognitionresults);
                    builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityTagConf.this.editText.setText(items[which]);
                            ActivityTagConf.this.editText.setSelection(items[which].length());
                        }
                    });
                    builder.create().show();
                } else {
                    this.editText.setText(results.get(0));
                    this.editText.setSelection(results.get(0).length());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    class TagAdapterCf extends ArrayAdapter<Tag> {
        private LayoutInflater inflater;
        private int minHeight;
        private List<Tag> tags;

        public TagAdapterCf(Context context, int textViewResourceId, List<Tag> tags2) {
            super(context, textViewResourceId, tags2);
            this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
            this.tags = tags2;
            this.minHeight = context.getResources().getDimensionPixelSize(R.dimen.row_tagconf_height);
        }

        public void setTags(List<Tag> tags2) {
            this.tags = tags2;
        }

        public Tag getItem(int position) {
            return this.tags.get(position);
        }

        public int getCount() {
            return this.tags.size();
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            int i;
            View view = convertView;
            if (view == null) {
                view = this.inflater.inflate((int) R.layout.row_tagconf, (ViewGroup) null);
                view.setMinimumHeight(this.minHeight);
                holder = new ViewHolder(this, null);
                holder.IV_TAG_ICON = (ImageView) view.findViewById(R.id.IV_TAG_ICON);
                holder.TV_TAG_TITLE = (TextView) view.findViewById(R.id.TV_TAG_TITLE);
                holder.IV_TAG_DISABLE = (ImageView) view.findViewById(R.id.IV_TAG_DISABLE);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            Tag tag = getItem(position);
            holder.IV_TAG_ICON.setImageResource(AppUtil.getTagResId(ActivityTagConf.this, tag.getId()));
            holder.TV_TAG_TITLE.setText(tag.getTitle());
            if (tag.getEnable()) {
                holder.IV_TAG_DISABLE.setVisibility(4);
            } else {
                holder.IV_TAG_DISABLE.setVisibility(0);
            }
            if (position == ActivityTagConf.this.mDraggingPosition) {
                i = 4;
            } else {
                i = 0;
            }
            view.setVisibility(i);
            return view;
        }

        private class ViewHolder {
            ImageView IV_TAG_DISABLE;
            ImageView IV_TAG_ICON;
            TextView TV_TAG_TITLE;

            private ViewHolder() {
            }

            /* synthetic */ ViewHolder(TagAdapterCf tagAdapterCf, ViewHolder viewHolder) {
                this();
            }
        }
    }

    class DragListener extends SortableListView.SimpleDragListener {
        DragListener() {
        }

        public int onStartDrag(int position) {
            ActivityTagConf.this.mDraggingPosition = position;
            ActivityTagConf.this.LV.invalidateViews();
            return position;
        }

        public int onDuringDrag(int positionFrom, int positionTo) {
            if (positionFrom < 0 || positionTo < 0 || positionFrom == positionTo) {
                return positionFrom;
            }
            sortChange(positionFrom, positionTo + 1);
            ActivityTagConf.this.mDraggingPosition = positionTo;
            ActivityTagConf.this.LV.invalidateViews();
            return positionTo;
        }

        public boolean onStopDrag(int positionFrom, int positionTo) {
            ActivityTagConf.this.mDraggingPosition = -1;
            ActivityTagConf.this.LV.invalidateViews();
            return super.onStopDrag(positionFrom, positionTo);
        }

        private void sortChange(int position, int newSort) {
            SQLiteDatabase DB = new DBEngine(ActivityTagConf.this).getWritableDatabase();
            try {
                DB.beginTransaction();
                int id = ActivityTagConf.this.adapter.getItem(position).getId();
                int crSort = ActivityTagConf.this.adapter.getItem(position).getSort();
                int minSort = Math.min(crSort, newSort);
                int maxSort = Math.max(crSort, newSort);
                int stSort = minSort;
                if (crSort < newSort) {
                    stSort = minSort - 1;
                }
                ContentValues values = new ContentValues();
                values.put("sort", Integer.valueOf(newSort));
                DB.update("TAG", values, "id=?", new String[]{String.valueOf(id)});
                Cursor RS = DB.rawQuery(" SELECT id FROM TAG" + " WHERE id <> ?" + " AND   sort >= ?" + " AND   sort <= ?" + " ORDER BY sort", new String[]{String.valueOf(id), String.valueOf(minSort), String.valueOf(maxSort)});
                while (RS.moveToNext()) {
                    stSort++;
                    ContentValues values2 = new ContentValues();
                    values2.put("sort", Integer.valueOf(stSort));
                    DB.update("TAG", values2, "id = ?", new String[]{String.valueOf(RS.getInt(0))});
                }
                RS.close();
                DB.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                DB.endTransaction();
                DB.close();
                ActivityTagConf.this.adapter.setTags(AppUtil.getTag(ActivityTagConf.this, false));
            }
        }
    }
}
