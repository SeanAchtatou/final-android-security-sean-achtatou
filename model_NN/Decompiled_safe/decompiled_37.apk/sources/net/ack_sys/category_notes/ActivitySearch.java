package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivitySearch extends Activity implements View.OnClickListener {
    private static final int REQUEST_CODE = 0;
    /* access modifiers changed from: private */
    public EditText ET_WORD;
    private ImageButton IB_SPEAK;
    private ImageButton IB_TAG_ICON;
    private ListView LV_MEMO;
    /* access modifiers changed from: private */
    public SearchAdapter adapter = null;
    /* access modifiers changed from: private */
    public TagAdapter adapterTag = null;
    private boolean createFlg = false;
    /* access modifiers changed from: private */
    public int currentTagId = 0;
    /* access modifiers changed from: private */
    public AlertDialog tagDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.createFlg = true;
        setContentView((int) R.layout.act_search);
        this.adapterTag = new TagAdapter(this, R.layout.row_tag, AppUtil.getTag(this, true, true));
        this.ET_WORD = (EditText) findViewById(R.id.ET_WORD);
        Bundle bundleW = this.ET_WORD.getInputExtras(true);
        if (bundleW != null) {
            bundleW.putBoolean("allowEmoji", true);
        }
        this.IB_SPEAK = (ImageButton) findViewById(R.id.IB_SPEAK);
        this.IB_SPEAK.setOnClickListener(this);
        this.IB_TAG_ICON = (ImageButton) findViewById(R.id.IB_TAG_ICON);
        this.IB_TAG_ICON.setOnClickListener(this);
        this.LV_MEMO = (ListView) findViewById(R.id.LV_MEMO);
        this.adapter = new SearchAdapter(this, R.layout.row_search, AppUtil.getMemo(this, -1));
        this.LV_MEMO.setAdapter((ListAdapter) this.adapter);
        this.LV_MEMO.setScrollingCacheEnabled(false);
        this.LV_MEMO.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(ActivitySearch.this, ActivityView.class);
                intent.putExtra("recno", ActivitySearch.this.adapter.getItem(position).getRecno());
                ActivitySearch.this.startActivity(intent);
            }
        });
        this.LV_MEMO.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                CharSequence[] searchs = {ActivitySearch.this.getString(R.string.str_edit), ActivitySearch.this.getString(R.string.str_remove), ActivitySearch.this.getString(R.string.str_clipboard1), ActivitySearch.this.getString(R.string.str_clipboard2)};
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySearch.this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(ActivitySearch.this, ActivityEdit.class);
                            intent.putExtra("recno", ActivitySearch.this.adapter.getItem(position).getRecno());
                            ActivitySearch.this.startActivity(intent);
                        } else if (which == 1) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ActivitySearch.this);
                            alert.setTitle((int) R.string.str_check);
                            alert.setMessage((int) R.string.str_msg_remove);
                            final int i = position;
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteMemo(ActivitySearch.this, ActivitySearch.this.adapter.getItem(i).getRecno());
                                    ActivitySearch.this.updateAdapter();
                                }
                            });
                            alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert.show();
                        } else if (which == 2) {
                            AppUtil.copyClipboard(ActivitySearch.this, ActivitySearch.this.adapter.getItem(position).getMemo());
                        } else {
                            List<String> listTmp = Arrays.asList(ActivitySearch.this.adapter.getItem(position).getMemo().split("\n"));
                            List<String> listMemo = new ArrayList<>();
                            for (String memo : listTmp) {
                                if (!TextUtils.isEmpty(memo)) {
                                    listMemo.add(memo);
                                }
                            }
                            final CharSequence[] items = (CharSequence[]) listMemo.toArray(new CharSequence[listMemo.size()]);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySearch.this);
                            builder.setTitle((int) R.string.str_clipboard);
                            builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.copyClipboard(ActivitySearch.this, items[which].toString());
                                }
                            });
                            builder.create().show();
                        }
                    }
                });
                builder.create().show();
                return false;
            }
        });
        this.ET_WORD.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() != 0) {
                    ActivitySearch.this.adapter.setMemos(AppUtil.getMemo(ActivitySearch.this, s.toString(), ActivitySearch.this.currentTagId));
                } else if (ActivitySearch.this.currentTagId == 0) {
                    ActivitySearch.this.adapter.setMemos(AppUtil.getMemo(ActivitySearch.this, -1));
                } else {
                    ActivitySearch.this.adapter.setMemos(AppUtil.getMemo(ActivitySearch.this, "", ActivitySearch.this.currentTagId));
                }
                ActivitySearch.this.adapter.notifyDataSetChanged();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.createFlg) {
            updateAdapter();
        }
        this.createFlg = false;
        setView();
    }

    /* access modifiers changed from: private */
    public void updateAdapter() {
        if (this.ET_WORD.getText().toString().trim().length() != 0) {
            this.adapter.setMemos(AppUtil.getMemo(this, this.ET_WORD.getText().toString(), this.currentTagId));
        } else if (this.currentTagId == 0) {
            this.adapter.setMemos(AppUtil.getMemo(this, -1));
        } else {
            this.adapter.setMemos(AppUtil.getMemo(this, "", this.currentTagId));
        }
        this.adapter.notifyDataSetChanged();
        this.adapterTag.setTags(AppUtil.getTag(this, true, true));
        this.adapterTag.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void setView() {
        if (this.currentTagId == 0) {
            this.IB_TAG_ICON.setImageDrawable(null);
        } else {
            this.IB_TAG_ICON.setImageResource(AppUtil.getTagResId(this, this.currentTagId));
        }
        if (new Prefs(this).getTagUse()) {
            int buttonSize = getResources().getDimensionPixelSize(R.dimen.tag_button_size);
            this.IB_TAG_ICON.setLayoutParams(new LinearLayout.LayoutParams(buttonSize, buttonSize));
            return;
        }
        this.currentTagId = 0;
        this.IB_TAG_ICON.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            ArrayList<String> results = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (results.size() != 0) {
                if (results.size() > 1) {
                    final CharSequence[] items = new CharSequence[results.size()];
                    for (int i = 0; i < results.size(); i++) {
                        items[i] = results.get(i);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((int) R.string.str_recognitionresults);
                    builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivitySearch.this.ET_WORD.setText(items[which]);
                            ActivitySearch.this.ET_WORD.setSelection(items[which].length());
                        }
                    });
                    builder.create().show();
                } else {
                    this.ET_WORD.setText(results.get(0));
                    this.ET_WORD.setSelection(results.get(0).length());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.IB_TAG_ICON /*2131296620*/:
                ListView LV = new ListView(this);
                LV.setAdapter((ListAdapter) this.adapterTag);
                LV.setScrollingCacheEnabled(false);
                LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        ActivitySearch.this.tagDialog.dismiss();
                        ActivitySearch.this.currentTagId = ActivitySearch.this.adapterTag.getItem(position).getId();
                        ActivitySearch.this.setView();
                        if (ActivitySearch.this.ET_WORD.getText().toString().trim().length() != 0) {
                            ActivitySearch.this.adapter.setMemos(AppUtil.getMemo(ActivitySearch.this, ActivitySearch.this.ET_WORD.getText().toString(), ActivitySearch.this.currentTagId));
                        } else if (ActivitySearch.this.currentTagId == 0) {
                            ActivitySearch.this.adapter.setMemos(AppUtil.getMemo(ActivitySearch.this, -1));
                        } else {
                            ActivitySearch.this.adapter.setMemos(AppUtil.getMemo(ActivitySearch.this, "", ActivitySearch.this.currentTagId));
                        }
                        ActivitySearch.this.adapter.notifyDataSetChanged();
                    }
                });
                this.tagDialog = new AlertDialog.Builder(this).setTitle((int) R.string.str_tag).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setView(LV).create();
                this.tagDialog.show();
                return;
            case R.id.IB_SPEAK /*2131296648*/:
                try {
                    Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
                    intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
                    startActivityForResult(intent, 0);
                    return;
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, (int) R.string.str_msg_nospeech, 1).show();
                    return;
                }
            default:
                return;
        }
    }
}
