package net.ack_sys.category_notes;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import java.io.File;

public class ActivityImageViewer extends Activity {
    public static final String INTENT_KEY_FILE = "net.ack_sys.extra.FILE";
    private FrameLayout FL;
    /* access modifiers changed from: private */
    public ProgressBar PB_WV;
    private WebView WV;
    private String currentFile;
    private int mode;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.act_image_viewer);
        getWindow().addFlags(128);
        this.FL = (FrameLayout) findViewById(R.id.FL);
        ((ImageView) findViewById(R.id.IV_EXIT)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityImageViewer.this.finish();
            }
        });
        this.PB_WV = (ProgressBar) findViewById(R.id.PB_WV);
        this.WV = (WebView) findViewById(R.id.WV);
        this.WV.setBackgroundColor(0);
        this.WV.setScrollBarStyle(33554432);
        WebSettings settings = this.WV.getSettings();
        settings.setDefaultZoom(WebSettings.ZoomDensity.FAR);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setBuiltInZoomControls(true);
        this.WV.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                ActivityImageViewer.this.PB_WV.setVisibility(0);
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                ActivityImageViewer.this.PB_WV.setVisibility(8);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Bundle extras = getIntent().getExtras();
        this.mode = -1;
        if (extras != null && extras.containsKey(INTENT_KEY_FILE)) {
            this.mode = 1;
            this.currentFile = extras.getString(INTENT_KEY_FILE);
        }
        if (this.mode == -1) {
            finish();
            return;
        }
        File file = new File(this.currentFile);
        this.WV.loadDataWithBaseURL("file://" + Uri.encode(file.getParent()) + "/", "<html><head><meta http-equiv=\"content-type\" content=\"text/html;charset=UTF-8\"><style type=\"text/css\">body{margin: 0px;}</style></head><body><table width=\"100%\" height=\"100%\"><tr><td align=\"center\" valign=\"middle\">#data#</td></tr></table></body></html>".replaceAll("#data#", "<img src=\"" + Uri.encode(file.getName()) + "\">"), "text/html", "UTF-8", "");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.PB_WV.setBackgroundDrawable(null);
        this.PB_WV = null;
        this.WV.stopLoading();
        this.WV.setWebChromeClient(null);
        this.WV.setWebViewClient(null);
        unregisterForContextMenu(this.WV);
        this.WV.clearCache(true);
        this.WV.destroy();
        this.WV = null;
        this.FL.setBackgroundDrawable(null);
        this.FL = null;
    }
}
