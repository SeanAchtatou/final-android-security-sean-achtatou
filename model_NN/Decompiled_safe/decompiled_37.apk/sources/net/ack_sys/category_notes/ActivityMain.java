package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ActivityMain extends Activity implements View.OnClickListener {
    private Gallery GR_CATE_ICON;
    private ImageView IV_ADD;
    private ListView LV_MEMO;
    /* access modifiers changed from: private */
    public TextView TV_CATE_TITLE;
    /* access modifiers changed from: private */
    public MemoAdapter adapter = null;
    /* access modifiers changed from: private */
    public CategoryAdapterGr adapterGr = null;
    private boolean createFlg = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.createFlg = true;
        setContentView((int) R.layout.act_main);
        this.GR_CATE_ICON = (Gallery) findViewById(R.id.GR_CATE_ICON);
        this.TV_CATE_TITLE = (TextView) findViewById(R.id.TV_CATE_TITLE);
        this.LV_MEMO = (ListView) findViewById(R.id.LV_MEMO);
        this.IV_ADD = (ImageView) findViewById(R.id.IV_ADD);
        this.IV_ADD.setOnClickListener(this);
        this.adapter = new MemoAdapter(this, R.layout.row_memo, AppUtil.getMemo(this, -1));
        this.LV_MEMO.setAdapter((ListAdapter) this.adapter);
        this.LV_MEMO.setScrollingCacheEnabled(false);
        this.LV_MEMO.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(ActivityMain.this, ActivityView.class);
                intent.putExtra("recno", ActivityMain.this.adapter.getItem(position).getRecno());
                ActivityMain.this.startActivity(intent);
            }
        });
        this.LV_MEMO.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long id) {
                CharSequence[] searchs = {ActivityMain.this.getString(R.string.str_edit), ActivityMain.this.getString(R.string.str_remove), ActivityMain.this.getString(R.string.str_clipboard1), ActivityMain.this.getString(R.string.str_clipboard2)};
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
                builder.setTitle((int) R.string.str_operations);
                builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                builder.setItems(searchs, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(ActivityMain.this, ActivityEdit.class);
                            intent.putExtra("recno", ActivityMain.this.adapter.getItem(position).getRecno());
                            ActivityMain.this.startActivity(intent);
                        } else if (which == 1) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ActivityMain.this);
                            alert.setTitle((int) R.string.str_check);
                            alert.setMessage((int) R.string.str_msg_remove);
                            final int i = position;
                            alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.deleteMemo(ActivityMain.this, ActivityMain.this.adapter.getItem(i).getRecno());
                                    ActivityMain.this.updateAdapter();
                                }
                            });
                            alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                            alert.show();
                        } else if (which == 2) {
                            AppUtil.copyClipboard(ActivityMain.this, ActivityMain.this.adapter.getItem(position).getMemo());
                        } else {
                            List<String> listTmp = Arrays.asList(ActivityMain.this.adapter.getItem(position).getMemo().split("\n"));
                            List<String> listMemo = new ArrayList<>();
                            for (String memo : listTmp) {
                                if (!TextUtils.isEmpty(memo)) {
                                    listMemo.add(memo);
                                }
                            }
                            final CharSequence[] items = (CharSequence[]) listMemo.toArray(new CharSequence[listMemo.size()]);
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
                            builder.setTitle((int) R.string.str_clipboard);
                            builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                            builder.setItems(items, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    AppUtil.copyClipboard(ActivityMain.this, items[which].toString());
                                }
                            });
                            builder.create().show();
                        }
                    }
                });
                builder.create().show();
                return false;
            }
        });
        this.adapterGr = new CategoryAdapterGr(this, AppUtil.getCategory(this));
        if (this.adapterGr.getCount() == 0) {
            AppInfo.getInstance().setCurrentPosition(0);
            SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
            Cursor RS = DB.rawQuery(" SELECT * FROM CATEGORY" + " WHERE sort = 1", null);
            if (RS.moveToFirst()) {
                AppInfo.getInstance().setCurrentCateId(RS.getInt(RS.getColumnIndex("id")));
            }
            RS.close();
            DB.close();
            updateAdapter();
        }
        this.GR_CATE_ICON.setAdapter((SpinnerAdapter) this.adapterGr);
        this.GR_CATE_ICON.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
                AppInfo.getInstance().setCurrentPosition(position);
                AppInfo.getInstance().setCurrentCateId(ActivityMain.this.adapterGr.getItem(position).getId());
                ActivityMain.this.TV_CATE_TITLE.setText(ActivityMain.this.adapterGr.getItem(position).getTitle());
                ActivityMain.this.adapter.setMemos(AppUtil.getMemo(ActivityMain.this, ActivityMain.this.adapterGr.getItem(position).getId()));
                ActivityMain.this.adapter.notifyDataSetChanged();
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.createFlg) {
            updateAdapterGr();
            updateAdapter();
        }
        this.createFlg = false;
        setView();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.IV_ADD /*2131296637*/:
                startActivity(new Intent(this, ActivityEdit.class));
                return;
            default:
                return;
        }
    }

    private void updateAdapterGr() {
        this.adapterGr.setCategorys(AppUtil.getCategory(this));
        this.adapterGr.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void updateAdapter() {
        this.adapter.setMemos(AppUtil.getMemo(this, AppInfo.getInstance().getCurrentCateId()));
        this.adapter.notifyDataSetChanged();
    }

    private void setView() {
        this.GR_CATE_ICON.setSelection(AppInfo.getInstance().getCurrentPosition());
        this.TV_CATE_TITLE.setText(AppUtil.getCateTitle(this, AppInfo.getInstance().getCurrentCateId()));
    }
}
