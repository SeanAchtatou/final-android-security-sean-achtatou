package net.ack_sys.category_notes;

public class Alarm {
    public static final String ALARM_DAY = "alarm_day";
    public static final String ALARM_HOUR = "alarm_hour";
    public static final String ALARM_LOOP = "alarm_loop";
    public static final String ALARM_MINUTE = "alarm_minute";
    public static final String ALARM_MONTH = "alarm_month";
    public static final String ALARM_WEEK1 = "alarm_week1";
    public static final String ALARM_WEEK2 = "alarm_week2";
    public static final String ALARM_WEEK3 = "alarm_week3";
    public static final String ALARM_WEEK4 = "alarm_week4";
    public static final String ALARM_WEEK5 = "alarm_week5";
    public static final String ALARM_WEEK6 = "alarm_week6";
    public static final String ALARM_WEEK7 = "alarm_week7";
    public static final String ALARM_YEAR = "alarm_year";
    public static final String ENABLE = "enable";
    public static final String MEMO_NO = "memo_no";
    public static final String RECNO = "recno";
    public static final String SNOOZE = "snooze";
    private CharSequence alarmDateTime;
    private int alarmDay;
    private int alarmHour;
    private boolean alarmLoop;
    private int alarmMinute;
    private int alarmMonth;
    private boolean alarmWeek1;
    private boolean alarmWeek2;
    private boolean alarmWeek3;
    private boolean alarmWeek4;
    private boolean alarmWeek5;
    private boolean alarmWeek6;
    private boolean alarmWeek7;
    private int alarmYaer;
    private int cateId;
    private boolean enable;
    private int iconId;
    private String memo;
    private int memoNo;
    private int recno;
    private boolean snooze;

    public int getRecno() {
        return this.recno;
    }

    public void setRecno(int recno2) {
        this.recno = recno2;
    }

    public int getMemoNo() {
        return this.memoNo;
    }

    public void setMemoNo(int memoNo2) {
        this.memoNo = memoNo2;
    }

    public int getCateId() {
        return this.cateId;
    }

    public void setCateId(int cateId2) {
        this.cateId = cateId2;
    }

    public int getIconId() {
        return this.iconId;
    }

    public void setIconId(int iconId2) {
        this.iconId = iconId2;
    }

    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String memo2) {
        this.memo = memo2.trim();
    }

    public int getAlarmYaer() {
        return this.alarmYaer;
    }

    public void setAlarmYaer(int alarmYaer2) {
        this.alarmYaer = alarmYaer2;
    }

    public int getAlarmMonth() {
        return this.alarmMonth;
    }

    public void setAlarmMonth(int alarmMonth2) {
        this.alarmMonth = alarmMonth2;
    }

    public int getAlarmDay() {
        return this.alarmDay;
    }

    public void setAlarmDay(int alarmDay2) {
        this.alarmDay = alarmDay2;
    }

    public int getAlarmHour() {
        return this.alarmHour;
    }

    public void setAlarmHour(int alarmHour2) {
        this.alarmHour = alarmHour2;
    }

    public int getAlarmMinute() {
        return this.alarmMinute;
    }

    public void setAlarmMinute(int alarmMinute2) {
        this.alarmMinute = alarmMinute2;
    }

    public boolean getAlarmLoop() {
        return this.alarmLoop;
    }

    public void setAlarmLoop(boolean alarmLoop2) {
        this.alarmLoop = alarmLoop2;
    }

    public boolean getAlarmWeek1() {
        return this.alarmWeek1;
    }

    public void setAlarmWeek1(boolean alarmWeek12) {
        this.alarmWeek1 = alarmWeek12;
    }

    public boolean getAlarmWeek2() {
        return this.alarmWeek2;
    }

    public void setAlarmWeek2(boolean alarmWeek22) {
        this.alarmWeek2 = alarmWeek22;
    }

    public boolean getAlarmWeek3() {
        return this.alarmWeek3;
    }

    public void setAlarmWeek3(boolean alarmWeek32) {
        this.alarmWeek3 = alarmWeek32;
    }

    public boolean getAlarmWeek4() {
        return this.alarmWeek4;
    }

    public void setAlarmWeek4(boolean alarmWeek42) {
        this.alarmWeek4 = alarmWeek42;
    }

    public boolean getAlarmWeek5() {
        return this.alarmWeek5;
    }

    public void setAlarmWeek5(boolean alarmWeek52) {
        this.alarmWeek5 = alarmWeek52;
    }

    public boolean getAlarmWeek6() {
        return this.alarmWeek6;
    }

    public void setAlarmWeek6(boolean alarmWeek62) {
        this.alarmWeek6 = alarmWeek62;
    }

    public boolean getAlarmWeek7() {
        return this.alarmWeek7;
    }

    public void setAlarmWeek7(boolean alarmWeek72) {
        this.alarmWeek7 = alarmWeek72;
    }

    public boolean getEnable() {
        return this.enable;
    }

    public void setEnable(boolean enable2) {
        this.enable = enable2;
    }

    public boolean getSnooze() {
        return this.snooze;
    }

    public void setSnooze(boolean snooze2) {
        this.snooze = snooze2;
    }

    public CharSequence getAlarmDateTime() {
        return this.alarmDateTime;
    }

    public void setAlarmDateTime(CharSequence alarmDateTime2) {
        this.alarmDateTime = alarmDateTime2;
    }
}
