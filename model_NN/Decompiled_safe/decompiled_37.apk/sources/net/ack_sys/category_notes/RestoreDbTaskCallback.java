package net.ack_sys.category_notes;

public interface RestoreDbTaskCallback {
    void onFailedRestoreDb(String str);

    void onSuccessRestoreDb(String str);
}
