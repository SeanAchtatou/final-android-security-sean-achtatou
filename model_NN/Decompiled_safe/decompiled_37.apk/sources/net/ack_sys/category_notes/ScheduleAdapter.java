package net.ack_sys.category_notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class ScheduleAdapter extends ArrayAdapter<Memo> {
    private Context context;
    private LayoutInflater inflater;
    private List<Memo> memos;

    public ScheduleAdapter(Context context2, int textViewResourceId, List<Memo> memos2) {
        super(context2, textViewResourceId, memos2);
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.memos = memos2;
    }

    public void setMemos(List<Memo> memos2) {
        this.memos = memos2;
    }

    public Memo getItem(int position) {
        return this.memos.get(position);
    }

    public int getCount() {
        return this.memos.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            int minHeight = this.context.getResources().getDimensionPixelSize(R.dimen.row_memo_height);
            view = this.inflater.inflate((int) R.layout.row_schedule, (ViewGroup) null);
            view.setMinimumHeight(minHeight);
            holder = new ViewHolder(null);
            holder.IV_CATE_ICON = (ImageView) view.findViewById(R.id.IV_CATE_ICON);
            holder.TV_MEMO = (TextView) view.findViewById(R.id.TV_MEMO);
            holder.TV_SCHEDULE = (TextView) view.findViewById(R.id.TV_SCHEDULE);
            holder.TV_LOCATION = (TextView) view.findViewById(R.id.TV_LOCATION);
            holder.IV_PHOTO_ICON = (ImageView) view.findViewById(R.id.IV_PHOTO_ICON);
            holder.IV_ALARM_ICON = (ImageView) view.findViewById(R.id.IV_ALARM_ICON);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Memo memo = getItem(position);
        holder.IV_CATE_ICON.setImageResource(AppUtil.getCateResId(memo.getIconId()));
        holder.TV_MEMO.setText(memo.getMemo());
        holder.TV_SCHEDULE.setText(AppUtil.getScheduleStr2(this.context.getResources(), memo.getDayFlg(), memo.getScheSHour(), memo.getScheSMinute(), memo.getScheSTime(), memo.getScheEHour(), memo.getScheEMinute(), memo.getScheETime()));
        holder.TV_LOCATION.setText(memo.getScheLocation());
        if (memo.getPhoto()) {
            holder.IV_PHOTO_ICON.setVisibility(0);
        } else {
            holder.IV_PHOTO_ICON.setVisibility(4);
        }
        if (memo.getAlarm()) {
            holder.IV_ALARM_ICON.setVisibility(0);
        } else {
            holder.IV_ALARM_ICON.setVisibility(4);
        }
        return view;
    }

    private static class ViewHolder {
        ImageView IV_ALARM_ICON;
        ImageView IV_CATE_ICON;
        ImageView IV_PHOTO_ICON;
        TextView TV_LOCATION;
        TextView TV_MEMO;
        TextView TV_SCHEDULE;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
