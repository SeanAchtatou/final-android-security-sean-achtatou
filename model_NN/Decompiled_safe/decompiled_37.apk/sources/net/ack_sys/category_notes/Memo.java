package net.ack_sys.category_notes;

public class Memo {
    public static final String CATE_ID = "cate_id";
    public static final String CREATE_TIME = "create_time";
    public static final String MEMO = "memo";
    public static final String PHOTO_NAME = "photo_name";
    public static final String PHOTO_URI = "photo_uri";
    public static final String RECNO = "recno";
    public static final String SCHE_E_DAY = "sche_e_day";
    public static final String SCHE_E_HOUR = "sche_e_hour";
    public static final String SCHE_E_MINUTE = "sche_e_minute";
    public static final String SCHE_E_MONTH = "sche_e_month";
    public static final String SCHE_E_TIME = "sche_e_time";
    public static final String SCHE_E_YEAR = "sche_e_year";
    public static final String SCHE_LOCATION = "sche_location";
    public static final String SCHE_S_DAY = "sche_s_day";
    public static final String SCHE_S_HOUR = "sche_s_hour";
    public static final String SCHE_S_MINUTE = "sche_s_minute";
    public static final String SCHE_S_MONTH = "sche_s_month";
    public static final String SCHE_S_TIME = "sche_s_time";
    public static final String SCHE_S_YEAR = "sche_s_year";
    public static final String TAG_ID = "tag_id";
    public static final String UPDATE_TIME = "update_time";
    private boolean alarm;
    private int cateId;
    private String createTime;
    private int dayFlg;
    private int iconId;
    private String memo;
    private boolean photo;
    private String photoName;
    private String photoUri;
    private int recno;
    private int scheEDay;
    private int scheEHour;
    private int scheEMinute;
    private int scheEMonth;
    private String scheETime;
    private int scheEYear;
    private String scheLocation;
    private int scheSDay;
    private int scheSHour;
    private int scheSMinute;
    private int scheSMonth;
    private String scheSTime;
    private int scheSYear;
    private boolean schedule;
    private int tagId;
    private String updateTime;

    public int getRecno() {
        return this.recno;
    }

    public void setRecno(int recno2) {
        this.recno = recno2;
    }

    public int getCateId() {
        return this.cateId;
    }

    public void setCateId(int cateId2) {
        this.cateId = cateId2;
    }

    public int getIconId() {
        return this.iconId;
    }

    public void setIconId(int iconId2) {
        this.iconId = iconId2;
    }

    public int getTagId() {
        return this.tagId;
    }

    public void setTagId(int tagId2) {
        this.tagId = tagId2;
    }

    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String memo2) {
        this.memo = memo2.trim();
    }

    public String getPhotoName() {
        return this.photoName;
    }

    public void setPhotoName(String photoName2) {
        this.photoName = photoName2;
    }

    public String getPhotoUri() {
        return this.photoUri;
    }

    public void setPhotoUri(String photoUri2) {
        this.photoUri = photoUri2;
    }

    public int getScheSYear() {
        return this.scheSYear;
    }

    public void setScheSYear(int scheSYear2) {
        this.scheSYear = scheSYear2;
    }

    public int getScheSMonth() {
        return this.scheSMonth;
    }

    public void setScheSMonth(int scheSMonth2) {
        this.scheSMonth = scheSMonth2;
    }

    public int getScheSDay() {
        return this.scheSDay;
    }

    public void setScheSDay(int scheSDay2) {
        this.scheSDay = scheSDay2;
    }

    public int getScheSHour() {
        return this.scheSHour;
    }

    public void setScheSHour(int scheSHour2) {
        this.scheSHour = scheSHour2;
    }

    public int getScheSMinute() {
        return this.scheSMinute;
    }

    public void setScheSMinute(int scheSMinute2) {
        this.scheSMinute = scheSMinute2;
    }

    public String getScheSTime() {
        return this.scheSTime;
    }

    public void setScheSTime(String scheSTime2) {
        this.scheSTime = scheSTime2;
    }

    public int getScheEYear() {
        return this.scheEYear;
    }

    public void setScheEYear(int scheEYear2) {
        this.scheEYear = scheEYear2;
    }

    public int getScheEMonth() {
        return this.scheEMonth;
    }

    public void setScheEMonth(int scheEMonth2) {
        this.scheEMonth = scheEMonth2;
    }

    public int getScheEDay() {
        return this.scheEDay;
    }

    public void setScheEDay(int scheEDay2) {
        this.scheEDay = scheEDay2;
    }

    public int getScheEHour() {
        return this.scheEHour;
    }

    public void setScheEHour(int scheEHour2) {
        this.scheEHour = scheEHour2;
    }

    public int getScheEMinute() {
        return this.scheEMinute;
    }

    public void setScheEMinute(int scheEMinute2) {
        this.scheEMinute = scheEMinute2;
    }

    public String getScheETime() {
        return this.scheETime;
    }

    public void setScheETime(String scheETime2) {
        this.scheETime = scheETime2;
    }

    public String getScheLocation() {
        return this.scheLocation;
    }

    public void setScheLocation(String scheLocation2) {
        this.scheLocation = scheLocation2;
    }

    public String getCreateTime() {
        return this.createTime;
    }

    public void setCreateTime(String createTime2) {
        this.createTime = createTime2;
    }

    public String getUpdateTime() {
        return this.updateTime;
    }

    public void setUpdateTime(String updateTime2) {
        this.updateTime = updateTime2;
    }

    public boolean getPhoto() {
        return this.photo;
    }

    public void setPhoto(boolean photo2) {
        this.photo = photo2;
    }

    public boolean getSchedule() {
        return this.schedule;
    }

    public void setSchedule(boolean schedule2) {
        this.schedule = schedule2;
    }

    public boolean getAlarm() {
        return this.alarm;
    }

    public void setAlarm(boolean alarm2) {
        this.alarm = alarm2;
    }

    public int getDayFlg() {
        return this.dayFlg;
    }

    public void setDayFlg(int dayFlg2) {
        this.dayFlg = dayFlg2;
    }
}
