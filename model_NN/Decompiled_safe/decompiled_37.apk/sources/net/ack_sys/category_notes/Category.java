package net.ack_sys.category_notes;

public class Category {
    public static final String ENABLE = "enable";
    public static final String ICON_ID = "icon_id";
    public static final String ID = "id";
    public static final String MUSIC_NAME = "music_name";
    public static final String MUSIC_URI = "music_uri";
    public static final String SORT = "sort";
    public static final String TITLE = "title";
    private boolean enable;
    private int iconId;
    private int id;
    private String musicName;
    private String musicUri;
    private int sort;
    private String title;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2.trim();
    }

    public String getMusicName() {
        return this.musicName;
    }

    public void setMusicName(String musicName2) {
        this.musicName = musicName2;
    }

    public String getMusicUri() {
        return this.musicUri;
    }

    public void setMusicUri(String musicUri2) {
        this.musicUri = musicUri2;
    }

    public int getIconId() {
        return this.iconId;
    }

    public void setIconId(int iconId2) {
        this.iconId = iconId2;
    }

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort2) {
        this.sort = sort2;
    }

    public boolean getEnable() {
        return this.enable;
    }

    public void setEnable(boolean enable2) {
        this.enable = enable2;
    }
}
