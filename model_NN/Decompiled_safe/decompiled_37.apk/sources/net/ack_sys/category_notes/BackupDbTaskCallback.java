package net.ack_sys.category_notes;

public interface BackupDbTaskCallback {
    void onFailedBackupDb(String str);

    void onSuccessBackupDb(String str);
}
