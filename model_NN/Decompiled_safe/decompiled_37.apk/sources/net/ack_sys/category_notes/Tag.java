package net.ack_sys.category_notes;

public class Tag {
    public static final String ENABLE = "enable";
    public static final String ID = "id";
    public static final String SORT = "sort";
    public static final String TITLE = "title";
    private boolean enable;
    private int id;
    private int sort;
    private String title;

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2.trim();
    }

    public int getSort() {
        return this.sort;
    }

    public void setSort(int sort2) {
        this.sort = sort2;
    }

    public boolean getEnable() {
        return this.enable;
    }

    public void setEnable(boolean enable2) {
        this.enable = enable2;
    }
}
