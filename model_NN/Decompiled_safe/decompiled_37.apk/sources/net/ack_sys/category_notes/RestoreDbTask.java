package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class RestoreDbTask extends AsyncTask<String, Void, AsyncTaskResult<?>> {
    private Activity activity = null;
    private RestoreDbTaskCallback callback;
    private ProgressDialog progressDialog = null;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((AsyncTaskResult<?>) ((AsyncTaskResult) obj));
    }

    public RestoreDbTask(Activity activity2, RestoreDbTaskCallback callback2) {
        this.activity = activity2;
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.progressDialog = new ProgressDialog(this.activity);
        this.progressDialog.setMessage(this.activity.getString(R.string.str_msg_restore));
        this.progressDialog.show();
    }

    /* access modifiers changed from: protected */
    public AsyncTaskResult<?> doInBackground(String... params) {
        String dbName = params[0];
        try {
            AppUtil.cancelAllAlarm(this.activity.getApplicationContext());
            String fileSd = Environment.getExternalStorageDirectory().getPath() + "/" + this.activity.getPackageName() + "/" + dbName;
            if (!new File(fileSd).exists()) {
                throw new IOException(this.activity.getString(R.string.str_msg_restoreerr));
            }
            String fileDb = this.activity.getDatabasePath(dbName).getPath();
            FileChannel channelSource = new FileInputStream(fileSd).getChannel();
            FileChannel channelTarget = new FileOutputStream(fileDb).getChannel();
            channelSource.transferTo(0, channelSource.size(), channelTarget);
            channelSource.close();
            channelTarget.close();
            AppUtil.setAllAlarm(this.activity.getApplicationContext());
            return AsyncTaskResult.createNormalResult(this.activity.getString(R.string.str_msg_restoreok));
        } catch (Exception e) {
            AsyncTaskResult<?> createErrorResult = AsyncTaskResult.createErrorResult(e.getMessage());
            AppUtil.setAllAlarm(this.activity.getApplicationContext());
            return createErrorResult;
        } catch (Throwable th) {
            AppUtil.setAllAlarm(this.activity.getApplicationContext());
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncTaskResult<?> result) {
        this.progressDialog.dismiss();
        if (result.isError()) {
            this.callback.onFailedRestoreDb(result.getMessage());
        } else {
            this.callback.onSuccessRestoreDb(result.getMessage());
        }
    }
}
