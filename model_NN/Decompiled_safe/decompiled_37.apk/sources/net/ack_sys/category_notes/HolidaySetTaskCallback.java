package net.ack_sys.category_notes;

public interface HolidaySetTaskCallback {
    void onFailedHolidaySet(String str);

    void onSuccessHolidaySet(String str);
}
