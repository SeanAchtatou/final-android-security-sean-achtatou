package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;

public class ActivityEdit extends Activity implements View.OnClickListener {
    private static final int REQUEST_CODE = 0;
    private static final int REQUEST_PICK_CONTACT = 1;
    private static final int REQUEST_SPEAK = 2;
    /* access modifiers changed from: private */
    public Button B_EDAY;
    /* access modifiers changed from: private */
    public Button B_SDAY;
    /* access modifiers changed from: private */
    public EditText ET_LOCATION;
    /* access modifiers changed from: private */
    public EditText ET_MEMO;
    private ImageButton IB_CATE_ICON;
    private ImageButton IB_TAG_ICON;
    private ImageView IV_CAMERA;
    private ImageView IV_EDIT_SPEAK;
    private ImageView IV_PHOTO;
    private ImageView IV_SCHEDULE;
    private ImageView IV_TAG_ICON;
    private ImageView IV_UPDATE;
    /* access modifiers changed from: private */
    public LinearLayout LL_PHOTO;
    /* access modifiers changed from: private */
    public LinearLayout LL_SCHEDULE;
    private TextView TV_CATE_TITLE;
    /* access modifiers changed from: private */
    public TextView TV_EDATE;
    /* access modifiers changed from: private */
    public TextView TV_ETIME;
    private TextView TV_PHOTO;
    private TextView TV_SCHEDULE;
    /* access modifiers changed from: private */
    public TextView TV_SDATE;
    /* access modifiers changed from: private */
    public TextView TV_STIME;
    /* access modifiers changed from: private */
    public CategoryAdapter adapterCate = null;
    /* access modifiers changed from: private */
    public TagAdapter adapterTag = null;
    /* access modifiers changed from: private */
    public AlertDialog cateDialog = null;
    private boolean createFlg = false;
    /* access modifiers changed from: private */
    public int currentRecno = -1;
    /* access modifiers changed from: private */
    public int currentTagId = REQUEST_PICK_CONTACT;
    /* access modifiers changed from: private */
    public Calendar eCal = Calendar.getInstance();
    private Calendar eCalK = Calendar.getInstance();
    /* access modifiers changed from: private */
    public String eTime;
    private String eTimeK;
    private String locationK;
    /* access modifiers changed from: private */
    public boolean photo = false;
    /* access modifiers changed from: private */
    public AlertDialog photoDialog = null;
    private String photoName;
    private String photoNameK;
    private String photoUri;
    private String photoUriK;
    /* access modifiers changed from: private */
    public Calendar sCal = Calendar.getInstance();
    private Calendar sCalK = Calendar.getInstance();
    /* access modifiers changed from: private */
    public String sTime;
    private String sTimeK;
    /* access modifiers changed from: private */
    public AlertDialog scheDialog = null;
    /* access modifiers changed from: private */
    public boolean schedule = false;
    /* access modifiers changed from: private */
    public AlertDialog tagDialog = null;

    /* JADX WARNING: Removed duplicated region for block: B:26:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r15) {
        /*
            r14 = this;
            super.onCreate(r15)
            r0 = 1
            r14.createFlg = r0
            android.util.DisplayMetrics r12 = new android.util.DisplayMetrics
            r12.<init>()
            android.view.WindowManager r0 = r14.getWindowManager()
            android.view.Display r0 = r0.getDefaultDisplay()
            r0.getMetrics(r12)
            net.ack_sys.category_notes.AppInfo r0 = net.ack_sys.category_notes.AppInfo.getInstance()
            float r1 = r12.scaledDensity
            r0.setScaledDensity(r1)
            r0 = 2130903042(0x7f030002, float:1.741289E38)
            r14.setContentView(r0)
            android.view.Window r0 = r14.getWindow()
            r1 = 128(0x80, float:1.794E-43)
            r0.addFlags(r1)
            net.ack_sys.category_notes.CategoryAdapter r0 = new net.ack_sys.category_notes.CategoryAdapter
            r1 = 2130903061(0x7f030015, float:1.741293E38)
            java.util.List r2 = net.ack_sys.category_notes.AppUtil.getCategory(r14)
            r0.<init>(r14, r1, r2)
            r14.adapterCate = r0
            net.ack_sys.category_notes.TagAdapter r0 = new net.ack_sys.category_notes.TagAdapter
            r1 = 2130903066(0x7f03001a, float:1.741294E38)
            java.util.List r2 = net.ack_sys.category_notes.AppUtil.getTag(r14)
            r0.<init>(r14, r1, r2)
            r14.adapterTag = r0
            r0 = 2131296619(0x7f09016b, float:1.821116E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageButton r15 = (android.widget.ImageButton) r15
            r14.IB_CATE_ICON = r15
            android.widget.ImageButton r0 = r14.IB_CATE_ICON
            r0.setOnClickListener(r14)
            r0 = 2131296612(0x7f090164, float:1.8211146E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.TextView r15 = (android.widget.TextView) r15
            r14.TV_CATE_TITLE = r15
            r0 = 2131296620(0x7f09016c, float:1.8211162E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageButton r15 = (android.widget.ImageButton) r15
            r14.IB_TAG_ICON = r15
            android.widget.ImageButton r0 = r14.IB_TAG_ICON
            r0.setOnClickListener(r14)
            r0 = 2131296621(0x7f09016d, float:1.8211164E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            r14.IV_TAG_ICON = r15
            r0 = 2131296622(0x7f09016e, float:1.8211166E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.EditText r15 = (android.widget.EditText) r15
            r14.ET_MEMO = r15
            android.widget.EditText r0 = r14.ET_MEMO
            r0.setOnClickListener(r14)
            android.widget.EditText r0 = r14.ET_MEMO
            r1 = 1
            android.os.Bundle r7 = r0.getInputExtras(r1)
            if (r7 == 0) goto L_0x009f
            java.lang.String r0 = "allowEmoji"
            r1 = 1
            r7.putBoolean(r0, r1)
        L_0x009f:
            r0 = 2131296629(0x7f090175, float:1.821118E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            r14.IV_EDIT_SPEAK = r15
            android.widget.ImageView r0 = r14.IV_EDIT_SPEAK
            r0.setOnClickListener(r14)
            r0 = 2131296630(0x7f090176, float:1.8211182E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            r14.IV_CAMERA = r15
            android.widget.ImageView r0 = r14.IV_CAMERA
            r0.setOnClickListener(r14)
            r0 = 2131296631(0x7f090177, float:1.8211184E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            r14.IV_SCHEDULE = r15
            android.widget.ImageView r0 = r14.IV_SCHEDULE
            r0.setOnClickListener(r14)
            r0 = 2131296632(0x7f090178, float:1.8211186E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            r14.IV_UPDATE = r15
            android.widget.ImageView r0 = r14.IV_UPDATE
            r0.setOnClickListener(r14)
            r0 = 2131296623(0x7f09016f, float:1.8211168E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.LinearLayout r15 = (android.widget.LinearLayout) r15
            r14.LL_PHOTO = r15
            r0 = 2131296625(0x7f090171, float:1.8211172E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.TextView r15 = (android.widget.TextView) r15
            r14.TV_PHOTO = r15
            r0 = 2131296626(0x7f090172, float:1.8211174E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.LinearLayout r15 = (android.widget.LinearLayout) r15
            r14.LL_SCHEDULE = r15
            r0 = 2131296628(0x7f090174, float:1.8211178E38)
            android.view.View r15 = r14.findViewById(r0)
            android.widget.TextView r15 = (android.widget.TextView) r15
            r14.TV_SCHEDULE = r15
            android.content.Intent r11 = r14.getIntent()
            java.lang.String r6 = r11.getAction()
            android.os.Bundle r10 = r11.getExtras()
            if (r10 == 0) goto L_0x0137
            java.lang.String r0 = "recno"
            r1 = -1
            int r0 = r10.getInt(r0, r1)
            r14.currentRecno = r0
            java.lang.String r0 = "android.intent.action.SEND"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x0137
            java.lang.String r0 = "android.intent.extra.TEXT"
            java.lang.CharSequence r9 = r10.getCharSequence(r0)
            if (r9 == 0) goto L_0x0137
            android.widget.EditText r0 = r14.ET_MEMO
            r0.setText(r9)
        L_0x0137:
            int r0 = net.ack_sys.category_notes.AppUtil.getDefaultTagId(r14)
            r14.currentTagId = r0
            int r0 = r14.currentRecno
            r14.dispMemo(r0)
            if (r10 == 0) goto L_0x01fb
            java.lang.String r0 = "android.intent.action.SEND"
            boolean r0 = r0.equals(r6)
            if (r0 == 0) goto L_0x01fc
            java.lang.String r0 = "android.intent.extra.STREAM"
            android.os.Parcelable r13 = r11.getParcelableExtra(r0)
            android.net.Uri r13 = (android.net.Uri) r13
            if (r13 == 0) goto L_0x01fb
            java.lang.String r0 = r13.toString()
            r1 = 0
            r2 = 5
            java.lang.CharSequence r0 = r0.subSequence(r1, r2)
            java.lang.String r1 = "file:"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x025a
            android.content.ContentResolver r0 = r14.getContentResolver()
            android.net.Uri r1 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]
            r3 = 0
            java.lang.String r4 = "_id"
            r2[r3] = r4
            java.lang.String r3 = "_data = ?"
            r4 = 1
            java.lang.String[] r4 = new java.lang.String[r4]
            r5 = 0
            java.lang.String r6 = r13.getPath()
            r4[r5] = r6
            r5 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)
            if (r8 == 0) goto L_0x025a
            boolean r0 = r8.moveToFirst()
            if (r0 == 0) goto L_0x0257
            android.net.Uri r0 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            android.net.Uri$Builder r0 = r0.buildUpon()
            java.lang.String r1 = "_id"
            int r1 = r8.getColumnIndex(r1)
            long r2 = r8.getLong(r1)
            android.net.Uri$Builder r0 = android.content.ContentUris.appendId(r0, r2)
            android.net.Uri r1 = r0.build()
        L_0x01a7:
            r8.close()
        L_0x01aa:
            android.content.ContentResolver r0 = r14.getContentResolver()
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]
            r3 = 0
            java.lang.String r4 = "_display_name"
            r2[r3] = r4
            r3 = 1
            java.lang.String r4 = "_data"
            r2[r3] = r4
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r8 = r0.query(r1, r2, r3, r4, r5)
            if (r8 == 0) goto L_0x01fb
            boolean r0 = r8.moveToFirst()
            if (r0 == 0) goto L_0x01f8
            r0 = 1
            r14.photo = r0
            java.lang.String r0 = "_display_name"
            int r0 = r8.getColumnIndex(r0)
            java.lang.String r0 = r8.getString(r0)
            r14.photoNameK = r0
            java.lang.String r0 = "_data"
            int r0 = r8.getColumnIndex(r0)
            java.lang.String r0 = r8.getString(r0)
            r14.photoUriK = r0
            android.widget.TextView r0 = r14.TV_PHOTO
            java.lang.String r1 = r14.photoNameK
            r0.setText(r1)
            android.widget.LinearLayout r0 = r14.LL_PHOTO
            android.widget.LinearLayout$LayoutParams r1 = new android.widget.LinearLayout$LayoutParams
            r2 = -1
            r3 = -2
            r1.<init>(r2, r3)
            r0.setLayoutParams(r1)
        L_0x01f8:
            r8.close()
        L_0x01fb:
            return
        L_0x01fc:
            java.lang.String r0 = "year"
            r1 = 0
            int r0 = r10.getInt(r0, r1)
            if (r0 == 0) goto L_0x01fb
            java.util.Calendar r0 = r14.sCalK
            r1 = 1
            java.lang.String r2 = "year"
            r3 = 0
            int r2 = r10.getInt(r2, r3)
            r0.set(r1, r2)
            java.util.Calendar r0 = r14.sCalK
            r1 = 2
            java.lang.String r2 = "month"
            r3 = 0
            int r2 = r10.getInt(r2, r3)
            r0.set(r1, r2)
            java.util.Calendar r0 = r14.sCalK
            r1 = 5
            java.lang.String r2 = "day"
            r3 = 0
            int r2 = r10.getInt(r2, r3)
            r0.set(r1, r2)
            java.util.Calendar r0 = r14.eCalK
            r1 = 1
            java.lang.String r2 = "year"
            r3 = 0
            int r2 = r10.getInt(r2, r3)
            r0.set(r1, r2)
            java.util.Calendar r0 = r14.eCalK
            r1 = 2
            java.lang.String r2 = "month"
            r3 = 0
            int r2 = r10.getInt(r2, r3)
            r0.set(r1, r2)
            java.util.Calendar r0 = r14.eCalK
            r1 = 5
            java.lang.String r2 = "day"
            r3 = 0
            int r2 = r10.getInt(r2, r3)
            r0.set(r1, r2)
            r14.showScheDialog()
            goto L_0x01fb
        L_0x0257:
            r1 = r13
            goto L_0x01a7
        L_0x025a:
            r1 = r13
            goto L_0x01aa
        */
        throw new UnsupportedOperationException("Method not decompiled: net.ack_sys.category_notes.ActivityEdit.onCreate(android.os.Bundle):void");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.IB_CATE_ICON /*2131296619*/:
                ListView listView = new ListView(this);
                listView.setAdapter((ListAdapter) this.adapterCate);
                listView.setScrollingCacheEnabled(REQUEST_CODE);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        ActivityEdit.this.cateDialog.dismiss();
                        AppInfo.getInstance().setCurrentPosition(position);
                        AppInfo.getInstance().setCurrentCateId(ActivityEdit.this.adapterCate.getItem(position).getId());
                        ActivityEdit.this.setView();
                        if (ActivityEdit.this.ET_MEMO.getText().toString().trim().length() != 0) {
                            ActivityEdit.this.updateMemo(ActivityEdit.this.currentRecno);
                        }
                    }
                });
                this.cateDialog = new AlertDialog.Builder(this).setTitle((int) R.string.str_category).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setView(listView).create();
                this.cateDialog.show();
                return;
            case R.id.IB_TAG_ICON /*2131296620*/:
                ListView listView2 = new ListView(this);
                listView2.setAdapter((ListAdapter) this.adapterTag);
                listView2.setScrollingCacheEnabled(REQUEST_CODE);
                listView2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        ActivityEdit.this.tagDialog.dismiss();
                        ActivityEdit.this.currentTagId = ActivityEdit.this.adapterTag.getItem(position).getId();
                        ActivityEdit.this.setView();
                        if (ActivityEdit.this.ET_MEMO.getText().toString().trim().length() != 0) {
                            ActivityEdit.this.updateMemo(ActivityEdit.this.currentRecno);
                        }
                    }
                });
                this.tagDialog = new AlertDialog.Builder(this).setTitle((int) R.string.str_tag).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setView(listView2).create();
                this.tagDialog.show();
                return;
            case R.id.IV_EDIT_SPEAK /*2131296629*/:
                try {
                    Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
                    intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
                    startActivityForResult(intent, REQUEST_CODE);
                    return;
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, R.string.str_msg_nospeech, REQUEST_PICK_CONTACT).show();
                    return;
                }
            case R.id.IV_CAMERA /*2131296630*/:
                showPhotoDialog();
                return;
            case R.id.IV_SCHEDULE /*2131296631*/:
                showScheDialog();
                return;
            case R.id.IV_UPDATE /*2131296632*/:
                ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(v.getWindowToken(), REQUEST_CODE);
                if (this.ET_MEMO.getText().toString().trim().length() == 0) {
                    Toast.makeText(this, R.string.str_msg_memo, REQUEST_CODE).show();
                    return;
                }
                updateMemo(this.currentRecno);
                finish();
                return;
            case R.id.IB_SPEAK /*2131296648*/:
                try {
                    Intent intent2 = new Intent("android.speech.action.RECOGNIZE_SPEECH");
                    intent2.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
                    startActivityForResult(intent2, 2);
                    return;
                } catch (ActivityNotFoundException e2) {
                    Toast.makeText(this, R.string.str_msg_nospeech, REQUEST_PICK_CONTACT).show();
                    return;
                }
            case R.id.B_PHOTOSEL /*2131296742*/:
                Intent intent3 = new Intent("android.intent.action.PICK");
                intent3.setType("image/*");
                startActivityForResult(intent3, REQUEST_PICK_CONTACT);
                return;
            case R.id.B_PHOTOOK /*2131296744*/:
                if (AppUtil.checkNull(this.photoUri).booleanValue()) {
                    Toast.makeText(this, R.string.str_msg_photoerr, REQUEST_PICK_CONTACT).show();
                    return;
                }
                this.photo = REQUEST_PICK_CONTACT;
                this.photoNameK = this.photoName;
                this.photoUriK = this.photoUri;
                this.TV_PHOTO.setText(this.photoNameK);
                this.LL_PHOTO.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                if (this.ET_MEMO.getText().toString().trim().length() != 0) {
                    updateMemo(this.currentRecno);
                }
                this.photoDialog.dismiss();
                return;
            case R.id.B_PHOTODEL /*2131296745*/:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.str_check);
                builder.setMessage(R.string.str_msg_clear);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityEdit.this.photo = false;
                        ActivityEdit.this.LL_PHOTO.setLayoutParams(new LinearLayout.LayoutParams(-1, (int) ActivityEdit.REQUEST_CODE));
                        if (ActivityEdit.this.ET_MEMO.getText().toString().trim().length() != 0) {
                            ActivityEdit.this.updateMemo(ActivityEdit.this.currentRecno);
                        }
                        ActivityEdit.this.photoDialog.dismiss();
                    }
                });
                builder.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                builder.show();
                return;
            case R.id.B_PHOTOCANCEL /*2131296746*/:
                this.photoName = this.photoNameK;
                this.photoUri = this.photoUriK;
                this.photoDialog.dismiss();
                return;
            case R.id.B_EDATE /*2131296751*/:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        TextView access$25 = ActivityEdit.this.TV_EDATE;
                        Object[] objArr = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr[ActivityEdit.REQUEST_CODE] = Integer.valueOf(year);
                        StringBuilder append = new StringBuilder(String.valueOf(String.format("%1$04d", objArr))).append("/");
                        Object[] objArr2 = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr2[ActivityEdit.REQUEST_CODE] = Integer.valueOf(monthOfYear + ActivityEdit.REQUEST_PICK_CONTACT);
                        StringBuilder append2 = append.append(String.format("%1$02d", objArr2)).append("/");
                        Object[] objArr3 = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr3[ActivityEdit.REQUEST_CODE] = Integer.valueOf(dayOfMonth);
                        access$25.setText(append2.append(String.format("%1$02d", objArr3)).toString());
                        ActivityEdit.this.setCal(ActivityEdit.this.eCal, ActivityEdit.this.TV_EDATE.getText().toString(), ActivityEdit.this.TV_ETIME.getText().toString(), ActivityEdit.this.sCal);
                    }
                }, this.eCal.get(REQUEST_PICK_CONTACT), this.eCal.get(2), this.eCal.get(5)).show();
                return;
            case R.id.B_ETIME /*2131296752*/:
                new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        TextView access$24 = ActivityEdit.this.TV_ETIME;
                        Object[] objArr = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr[ActivityEdit.REQUEST_CODE] = Integer.valueOf(hourOfDay);
                        StringBuilder append = new StringBuilder(String.valueOf(String.format("%1$02d", objArr))).append(":");
                        Object[] objArr2 = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr2[ActivityEdit.REQUEST_CODE] = Integer.valueOf(minute);
                        access$24.setText(append.append(String.format("%1$02d", objArr2)).toString());
                        ActivityEdit.this.setCal(ActivityEdit.this.eCal, ActivityEdit.this.TV_EDATE.getText().toString(), ActivityEdit.this.TV_ETIME.getText().toString(), ActivityEdit.this.sCal);
                    }
                }, this.eCal.get(11), this.eCal.get(12), true).show();
                return;
            case R.id.B_ENOTIME /*2131296753*/:
                this.TV_ETIME.setText("");
                return;
            case R.id.B_SDATE /*2131296756*/:
                new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        TextView access$17 = ActivityEdit.this.TV_SDATE;
                        Object[] objArr = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr[ActivityEdit.REQUEST_CODE] = Integer.valueOf(year);
                        StringBuilder append = new StringBuilder(String.valueOf(String.format("%1$04d", objArr))).append("/");
                        Object[] objArr2 = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr2[ActivityEdit.REQUEST_CODE] = Integer.valueOf(monthOfYear + ActivityEdit.REQUEST_PICK_CONTACT);
                        StringBuilder append2 = append.append(String.format("%1$02d", objArr2)).append("/");
                        Object[] objArr3 = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr3[ActivityEdit.REQUEST_CODE] = Integer.valueOf(dayOfMonth);
                        access$17.setText(append2.append(String.format("%1$02d", objArr3)).toString());
                        ActivityEdit.this.setCal(ActivityEdit.this.sCal, ActivityEdit.this.TV_SDATE.getText().toString(), ActivityEdit.this.TV_STIME.getText().toString(), ActivityEdit.this.eCal);
                    }
                }, this.sCal.get(REQUEST_PICK_CONTACT), this.sCal.get(2), this.sCal.get(5)).show();
                return;
            case R.id.B_STIME /*2131296757*/:
                new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        TextView access$22 = ActivityEdit.this.TV_STIME;
                        Object[] objArr = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr[ActivityEdit.REQUEST_CODE] = Integer.valueOf(hourOfDay);
                        StringBuilder append = new StringBuilder(String.valueOf(String.format("%1$02d", objArr))).append(":");
                        Object[] objArr2 = new Object[ActivityEdit.REQUEST_PICK_CONTACT];
                        objArr2[ActivityEdit.REQUEST_CODE] = Integer.valueOf(minute);
                        access$22.setText(append.append(String.format("%1$02d", objArr2)).toString());
                        ActivityEdit.this.setCal(ActivityEdit.this.sCal, ActivityEdit.this.TV_SDATE.getText().toString(), ActivityEdit.this.TV_STIME.getText().toString(), ActivityEdit.this.eCal);
                    }
                }, this.sCal.get(11), this.sCal.get(12), true).show();
                return;
            case R.id.B_SNOTIME /*2131296758*/:
                this.TV_STIME.setText("");
                return;
            case R.id.B_SDAY /*2131296759*/:
                View sDayView = LayoutInflater.from(this).inflate((int) R.layout.dlg_schedate_s, (ViewGroup) null);
                ((Button) sDayView.findViewById(R.id.B_SDATE)).setOnClickListener(this);
                ((Button) sDayView.findViewById(R.id.B_STIME)).setOnClickListener(this);
                ((Button) sDayView.findViewById(R.id.B_SNOTIME)).setOnClickListener(this);
                this.TV_SDATE = (TextView) sDayView.findViewById(R.id.TV_SDATE);
                this.TV_STIME = (TextView) sDayView.findViewById(R.id.TV_STIME);
                TextView textView = this.TV_SDATE;
                Object[] objArr = new Object[REQUEST_PICK_CONTACT];
                objArr[REQUEST_CODE] = Integer.valueOf(this.sCal.get(REQUEST_PICK_CONTACT));
                StringBuilder append = new StringBuilder(String.valueOf(String.format("%1$04d", objArr))).append("/");
                Object[] objArr2 = new Object[REQUEST_PICK_CONTACT];
                objArr2[REQUEST_CODE] = Integer.valueOf(this.sCal.get(2) + REQUEST_PICK_CONTACT);
                StringBuilder append2 = append.append(String.format("%1$02d", objArr2)).append("/");
                Object[] objArr3 = new Object[REQUEST_PICK_CONTACT];
                objArr3[REQUEST_CODE] = Integer.valueOf(this.sCal.get(5));
                textView.setText(append2.append(String.format("%1$02d", objArr3)).toString());
                if (!AppUtil.checkNull(this.sTime).booleanValue()) {
                    TextView textView2 = this.TV_STIME;
                    Object[] objArr4 = new Object[REQUEST_PICK_CONTACT];
                    objArr4[REQUEST_CODE] = Integer.valueOf(this.sCal.get(11));
                    StringBuilder append3 = new StringBuilder(String.valueOf(String.format("%1$02d", objArr4))).append(":");
                    Object[] objArr5 = new Object[REQUEST_PICK_CONTACT];
                    objArr5[REQUEST_CODE] = Integer.valueOf(this.sCal.get(12));
                    textView2.setText(append3.append(String.format("%1$02d", objArr5)).toString());
                }
                new AlertDialog.Builder(this).setTitle(R.string.str_sday).setCancelable(false).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setNeutralButton((int) R.string.str_allday, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityEdit.this.sTime = "";
                        ActivityEdit.this.eTime = ActivityEdit.this.sTime;
                        ActivityEdit.this.eCal.set(11, ActivityEdit.REQUEST_CODE);
                        ActivityEdit.this.eCal.set(12, ActivityEdit.REQUEST_CODE);
                        ActivityEdit.this.setCal(ActivityEdit.this.sCal, ActivityEdit.this.TV_SDATE.getText().toString(), "", ActivityEdit.this.eCal);
                        ActivityEdit.this.eCal.setTimeInMillis(ActivityEdit.this.sCal.getTimeInMillis());
                        ActivityEdit.this.B_SDAY.setText(ActivityEdit.this.getDateTimeStr(ActivityEdit.this.getResources(), ActivityEdit.this.sCal.get(ActivityEdit.REQUEST_PICK_CONTACT), ActivityEdit.this.sCal.get(2), ActivityEdit.this.sCal.get(5), ActivityEdit.this.sCal.get(11), ActivityEdit.this.sCal.get(12), ActivityEdit.this.sTime));
                        ActivityEdit.this.B_EDAY.setText(ActivityEdit.this.B_SDAY.getText());
                    }
                }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityEdit.this.sTime = ActivityEdit.this.TV_STIME.getText().toString();
                        ActivityEdit.this.setCal(ActivityEdit.this.sCal, ActivityEdit.this.TV_SDATE.getText().toString(), ActivityEdit.this.TV_STIME.getText().toString(), ActivityEdit.this.eCal);
                        ActivityEdit.this.B_SDAY.setText(ActivityEdit.this.getDateTimeStr(ActivityEdit.this.getResources(), ActivityEdit.this.sCal.get(ActivityEdit.REQUEST_PICK_CONTACT), ActivityEdit.this.sCal.get(2), ActivityEdit.this.sCal.get(5), ActivityEdit.this.sCal.get(11), ActivityEdit.this.sCal.get(12), ActivityEdit.this.sTime));
                        if (ActivityEdit.this.sCal.getTimeInMillis() > ActivityEdit.this.eCal.getTimeInMillis()) {
                            ActivityEdit.this.eCal.setTimeInMillis(ActivityEdit.this.sCal.getTimeInMillis());
                            ActivityEdit.this.B_EDAY.setText(ActivityEdit.this.getDateTimeStr(ActivityEdit.this.getResources(), ActivityEdit.this.eCal.get(ActivityEdit.REQUEST_PICK_CONTACT), ActivityEdit.this.eCal.get(2), ActivityEdit.this.eCal.get(5), ActivityEdit.this.eCal.get(11), ActivityEdit.this.eCal.get(12), ActivityEdit.this.eTime));
                        }
                    }
                }).setView(sDayView).show();
                return;
            case R.id.B_EDAY /*2131296760*/:
                View eDayView = LayoutInflater.from(this).inflate((int) R.layout.dlg_schedate_e, (ViewGroup) null);
                ((Button) eDayView.findViewById(R.id.B_EDATE)).setOnClickListener(this);
                ((Button) eDayView.findViewById(R.id.B_ETIME)).setOnClickListener(this);
                ((Button) eDayView.findViewById(R.id.B_ENOTIME)).setOnClickListener(this);
                this.TV_EDATE = (TextView) eDayView.findViewById(R.id.TV_EDATE);
                this.TV_ETIME = (TextView) eDayView.findViewById(R.id.TV_ETIME);
                TextView textView3 = this.TV_EDATE;
                Object[] objArr6 = new Object[REQUEST_PICK_CONTACT];
                objArr6[REQUEST_CODE] = Integer.valueOf(this.eCal.get(REQUEST_PICK_CONTACT));
                StringBuilder append4 = new StringBuilder(String.valueOf(String.format("%1$04d", objArr6))).append("/");
                Object[] objArr7 = new Object[REQUEST_PICK_CONTACT];
                objArr7[REQUEST_CODE] = Integer.valueOf(this.eCal.get(2) + REQUEST_PICK_CONTACT);
                StringBuilder append5 = append4.append(String.format("%1$02d", objArr7)).append("/");
                Object[] objArr8 = new Object[REQUEST_PICK_CONTACT];
                objArr8[REQUEST_CODE] = Integer.valueOf(this.eCal.get(5));
                textView3.setText(append5.append(String.format("%1$02d", objArr8)).toString());
                if (!AppUtil.checkNull(this.eTime).booleanValue()) {
                    TextView textView4 = this.TV_ETIME;
                    Object[] objArr9 = new Object[REQUEST_PICK_CONTACT];
                    objArr9[REQUEST_CODE] = Integer.valueOf(this.eCal.get(11));
                    StringBuilder append6 = new StringBuilder(String.valueOf(String.format("%1$02d", objArr9))).append(":");
                    Object[] objArr10 = new Object[REQUEST_PICK_CONTACT];
                    objArr10[REQUEST_CODE] = Integer.valueOf(this.eCal.get(12));
                    textView4.setText(append6.append(String.format("%1$02d", objArr10)).toString());
                }
                new AlertDialog.Builder(this).setTitle(R.string.str_eday).setCancelable(false).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityEdit.this.eTime = ActivityEdit.this.TV_ETIME.getText().toString();
                        ActivityEdit.this.setCal(ActivityEdit.this.eCal, ActivityEdit.this.TV_EDATE.getText().toString(), ActivityEdit.this.TV_ETIME.getText().toString(), ActivityEdit.this.sCal);
                        ActivityEdit.this.B_EDAY.setText(ActivityEdit.this.getDateTimeStr(ActivityEdit.this.getResources(), ActivityEdit.this.eCal.get(ActivityEdit.REQUEST_PICK_CONTACT), ActivityEdit.this.eCal.get(2), ActivityEdit.this.eCal.get(5), ActivityEdit.this.eCal.get(11), ActivityEdit.this.eCal.get(12), ActivityEdit.this.eTime));
                    }
                }).setView(eDayView).show();
                return;
            case R.id.IB_LOCALIST /*2131296762*/:
                CharSequence[] items = getLocationList();
                if (items != null) {
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                    builder2.setTitle(R.string.str_location);
                    builder2.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                    final CharSequence[] charSequenceArr = items;
                    builder2.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityEdit.this.ET_LOCATION.setText(charSequenceArr[which]);
                            ActivityEdit.this.ET_LOCATION.setSelection(charSequenceArr[which].length());
                        }
                    });
                    builder2.create().show();
                    return;
                }
                return;
            case R.id.B_SCHEOK /*2131296763*/:
                if (this.sCal.getTimeInMillis() > this.eCal.getTimeInMillis()) {
                    Toast.makeText(this, R.string.str_msg_dayerr, REQUEST_PICK_CONTACT).show();
                    return;
                }
                this.sCalK.setTimeInMillis(this.sCal.getTimeInMillis());
                this.sTimeK = this.sTime;
                this.eCalK.setTimeInMillis(this.eCal.getTimeInMillis());
                this.eTimeK = this.eTime;
                this.locationK = this.ET_LOCATION.getText().toString();
                this.TV_SCHEDULE.setText(AppUtil.getScheduleStr(getResources(), this.sCalK.get(REQUEST_PICK_CONTACT), this.sCalK.get(2), this.sCalK.get(5), this.sCalK.get(11), this.sCalK.get(12), this.sTimeK, this.eCalK.get(REQUEST_PICK_CONTACT), this.eCalK.get(2), this.eCalK.get(5), this.eCalK.get(11), this.eCalK.get(12), this.eTimeK, this.locationK));
                this.schedule = REQUEST_PICK_CONTACT;
                this.LL_SCHEDULE.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                if (this.ET_MEMO.getText().toString().trim().length() != 0) {
                    updateMemo(this.currentRecno);
                }
                this.scheDialog.dismiss();
                return;
            case R.id.B_SCHEDEL /*2131296764*/:
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.setTitle(R.string.str_check);
                builder3.setMessage(R.string.str_msg_clear);
                builder3.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityEdit.this.schedule = false;
                        ActivityEdit.this.LL_SCHEDULE.setLayoutParams(new LinearLayout.LayoutParams(-1, (int) ActivityEdit.REQUEST_CODE));
                        if (ActivityEdit.this.ET_MEMO.getText().toString().trim().length() != 0) {
                            ActivityEdit.this.updateMemo(ActivityEdit.this.currentRecno);
                        }
                        ActivityEdit.this.scheDialog.dismiss();
                    }
                });
                builder3.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                builder3.show();
                return;
            case R.id.B_SCHECANCEL /*2131296765*/:
                this.sCal.setTimeInMillis(this.sCalK.getTimeInMillis());
                this.sTime = this.sTimeK;
                this.eCal.setTimeInMillis(this.eCalK.getTimeInMillis());
                this.eTime = this.eTimeK;
                this.scheDialog.dismiss();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.createFlg) {
            updateAdapter();
        }
        this.createFlg = false;
        setView();
    }

    private void updateAdapter() {
        this.adapterCate.setCategorys(AppUtil.getCategory(this));
        this.adapterCate.notifyDataSetChanged();
        this.adapterTag.setTags(AppUtil.getTag(this));
        this.adapterTag.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public void setView() {
        SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
        if (AppInfo.getInstance().getCurrentCateId() == 0) {
            AppInfo.getInstance().setCurrentPosition(REQUEST_CODE);
            if (this.adapterCate.getCount() == 0) {
                Cursor RS = DB.rawQuery(" SELECT * FROM CATEGORY" + " WHERE sort = 1", null);
                if (RS.moveToFirst()) {
                    AppInfo.getInstance().setCurrentCateId(RS.getInt(RS.getColumnIndex("id")));
                }
                RS.close();
            } else {
                Cursor RS2 = DB.rawQuery(" SELECT * FROM CATEGORY" + " WHERE enable = 1" + " ORDER BY sort", null);
                if (RS2.moveToFirst()) {
                    AppInfo.getInstance().setCurrentCateId(RS2.getInt(RS2.getColumnIndex("id")));
                }
                RS2.close();
            }
        }
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT * FROM CATEGORY");
        sql.append(" WHERE id = " + AppUtil.sql9(Integer.valueOf(AppInfo.getInstance().getCurrentCateId())));
        Cursor RS3 = DB.rawQuery(sql.toString(), null);
        if (RS3.moveToFirst()) {
            this.IB_CATE_ICON.setImageResource(AppUtil.getCateResId(RS3.getInt(RS3.getColumnIndex(Category.ICON_ID))));
            this.TV_CATE_TITLE.setText(RS3.getString(RS3.getColumnIndex("title")));
        }
        RS3.close();
        DB.close();
        this.IB_TAG_ICON.setImageResource(AppUtil.getTagResId(this, this.currentTagId));
        if (new Prefs(this).getTagUse()) {
            this.IB_TAG_ICON.setVisibility(REQUEST_CODE);
            this.IV_TAG_ICON.setVisibility(4);
            return;
        }
        this.currentTagId = REQUEST_PICK_CONTACT;
        this.IB_TAG_ICON.setVisibility(4);
        this.IV_TAG_ICON.setVisibility(REQUEST_CODE);
    }

    private void dispMemo(int recno) {
        SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
        this.LL_PHOTO.setLayoutParams(new LinearLayout.LayoutParams(-1, (int) REQUEST_CODE));
        this.LL_SCHEDULE.setLayoutParams(new LinearLayout.LayoutParams(-1, (int) REQUEST_CODE));
        this.sCalK.set(11, REQUEST_CODE);
        this.sCalK.set(12, REQUEST_CODE);
        this.sCalK.set(13, REQUEST_CODE);
        this.sCalK.set(14, REQUEST_CODE);
        this.eCalK.set(11, REQUEST_CODE);
        this.eCalK.set(12, REQUEST_CODE);
        this.eCalK.set(13, REQUEST_CODE);
        this.eCalK.set(14, REQUEST_CODE);
        try {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT");
            sql.append(" C.icon_id,");
            sql.append(" C.title,");
            sql.append(" M.memo,");
            sql.append(" M.cate_id,");
            sql.append(" M.tag_id,");
            sql.append(" M.photo_name,");
            sql.append(" M.photo_uri,");
            sql.append(" M.sche_s_year,");
            sql.append(" M.sche_s_month,");
            sql.append(" M.sche_s_day,");
            sql.append(" M.sche_s_hour,");
            sql.append(" M.sche_s_minute,");
            sql.append(" M.sche_s_time,");
            sql.append(" M.sche_e_year,");
            sql.append(" M.sche_e_month,");
            sql.append(" M.sche_e_day,");
            sql.append(" M.sche_e_hour,");
            sql.append(" M.sche_e_minute,");
            sql.append(" M.sche_e_time,");
            sql.append(" M.sche_location");
            sql.append(" FROM MEMO M");
            sql.append(" INNER JOIN CATEGORY C ON");
            sql.append("       M.cate_id = C.id");
            sql.append(" WHERE recno = " + AppUtil.sql9(Integer.valueOf(recno)));
            Cursor RS = DB.rawQuery(sql.toString(), null);
            if (RS.moveToFirst()) {
                this.IB_CATE_ICON.setImageResource(AppUtil.getCateResId(RS.getInt(RS.getColumnIndex(Category.ICON_ID))));
                this.TV_CATE_TITLE.setText(RS.getString(RS.getColumnIndex("title")));
                this.ET_MEMO.setText(RS.getString(RS.getColumnIndex(Memo.MEMO)));
                this.IB_TAG_ICON.setImageResource(AppUtil.getTagResId(this, RS.getInt(RS.getColumnIndex(Memo.TAG_ID))));
                AppInfo.getInstance().setCurrentCateId(RS.getInt(RS.getColumnIndex(Memo.CATE_ID)));
                this.currentTagId = RS.getInt(RS.getColumnIndex(Memo.TAG_ID));
                this.photoNameK = RS.getString(RS.getColumnIndex(Memo.PHOTO_NAME));
                this.photoUriK = RS.getString(RS.getColumnIndex(Memo.PHOTO_URI));
                if (!AppUtil.checkNull(this.photoUriK).booleanValue()) {
                    this.photo = REQUEST_PICK_CONTACT;
                    this.TV_PHOTO.setText(this.photoNameK);
                    this.LL_PHOTO.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                }
                if (RS.getInt(RS.getColumnIndex(Memo.SCHE_S_YEAR)) != 0) {
                    this.schedule = REQUEST_PICK_CONTACT;
                    this.LL_SCHEDULE.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                    this.sCalK.set(REQUEST_PICK_CONTACT, RS.getInt(RS.getColumnIndex(Memo.SCHE_S_YEAR)));
                    this.sCalK.set(2, RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MONTH)));
                    this.sCalK.set(5, RS.getInt(RS.getColumnIndex(Memo.SCHE_S_DAY)));
                    this.sCalK.set(11, RS.getInt(RS.getColumnIndex(Memo.SCHE_S_HOUR)));
                    this.sCalK.set(12, RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MINUTE)));
                    this.sTimeK = RS.getString(RS.getColumnIndex(Memo.SCHE_S_TIME));
                    this.eCalK.set(REQUEST_PICK_CONTACT, RS.getInt(RS.getColumnIndex(Memo.SCHE_E_YEAR)));
                    this.eCalK.set(2, RS.getInt(RS.getColumnIndex(Memo.SCHE_E_MONTH)));
                    this.eCalK.set(5, RS.getInt(RS.getColumnIndex(Memo.SCHE_E_DAY)));
                    this.eCalK.set(11, RS.getInt(RS.getColumnIndex(Memo.SCHE_E_HOUR)));
                    this.eCalK.set(12, RS.getInt(RS.getColumnIndex(Memo.SCHE_E_MINUTE)));
                    this.eTimeK = RS.getString(RS.getColumnIndex(Memo.SCHE_E_TIME));
                    this.locationK = RS.getString(RS.getColumnIndex(Memo.SCHE_LOCATION));
                    this.TV_SCHEDULE.setText(AppUtil.getScheduleStr(getResources(), this.sCalK.get(REQUEST_PICK_CONTACT), this.sCalK.get(2), this.sCalK.get(5), this.sCalK.get(11), this.sCalK.get(12), this.sTimeK, this.eCalK.get(REQUEST_PICK_CONTACT), this.eCalK.get(2), this.eCalK.get(5), this.eCalK.get(11), this.eCalK.get(12), this.eTimeK, this.locationK));
                }
            }
            RS.close();
            int position = REQUEST_CODE;
            StringBuilder sql2 = new StringBuilder();
            sql2.append(" SELECT * FROM CATEGORY");
            sql2.append(" WHERE enable = 1");
            sql2.append(" ORDER BY sort");
            Cursor RS2 = DB.rawQuery(sql2.toString(), null);
            while (true) {
                if (!RS2.moveToNext()) {
                    break;
                }
                if (AppInfo.getInstance().getCurrentCateId() == RS2.getInt(RS2.getColumnIndex("id"))) {
                    AppInfo.getInstance().setCurrentPosition(position);
                    break;
                }
                position += REQUEST_PICK_CONTACT;
            }
            RS2.close();
        } catch (Exception e) {
        } finally {
            DB.close();
        }
    }

    /* access modifiers changed from: private */
    public void updateMemo(int recno) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        boolean insertFlg = false;
        try {
            DB.beginTransaction();
            String dateTime = AppUtil.getDateTime();
            ContentValues values = new ContentValues();
            values.put(Memo.CATE_ID, Integer.valueOf(AppInfo.getInstance().getCurrentCateId()));
            values.put(Memo.TAG_ID, Integer.valueOf(this.currentTagId));
            values.put(Memo.MEMO, this.ET_MEMO.getText().toString().trim());
            if (this.photo) {
                values.put(Memo.PHOTO_NAME, this.photoNameK);
                values.put(Memo.PHOTO_URI, this.photoUriK);
            } else {
                values.put(Memo.PHOTO_NAME, "");
                values.put(Memo.PHOTO_URI, "");
            }
            if (this.schedule) {
                values.put(Memo.SCHE_S_YEAR, Integer.valueOf(this.sCalK.get(REQUEST_PICK_CONTACT)));
                values.put(Memo.SCHE_S_MONTH, Integer.valueOf(this.sCalK.get(2)));
                values.put(Memo.SCHE_S_DAY, Integer.valueOf(this.sCalK.get(5)));
                values.put(Memo.SCHE_S_TIME, this.sTimeK);
                values.put(Memo.SCHE_E_YEAR, Integer.valueOf(this.eCalK.get(REQUEST_PICK_CONTACT)));
                values.put(Memo.SCHE_E_MONTH, Integer.valueOf(this.eCalK.get(2)));
                values.put(Memo.SCHE_E_DAY, Integer.valueOf(this.eCalK.get(5)));
                values.put(Memo.SCHE_E_TIME, this.eTimeK);
                if (!AppUtil.checkNull(this.sTimeK).booleanValue() || !AppUtil.checkNull(this.eTimeK).booleanValue()) {
                    if (AppUtil.checkNull(this.sTimeK).booleanValue()) {
                        values.put(Memo.SCHE_S_HOUR, Integer.valueOf(this.eCalK.get(11)));
                        values.put(Memo.SCHE_S_MINUTE, Integer.valueOf(this.eCalK.get(12)));
                    } else {
                        values.put(Memo.SCHE_S_HOUR, Integer.valueOf(this.sCalK.get(11)));
                        values.put(Memo.SCHE_S_MINUTE, Integer.valueOf(this.sCalK.get(12)));
                    }
                    if (AppUtil.checkNull(this.eTimeK).booleanValue()) {
                        values.put(Memo.SCHE_E_HOUR, Integer.valueOf(this.sCalK.get(11)));
                        values.put(Memo.SCHE_E_MINUTE, Integer.valueOf(this.sCalK.get(12)));
                    } else {
                        values.put(Memo.SCHE_E_HOUR, Integer.valueOf(this.eCalK.get(11)));
                        values.put(Memo.SCHE_E_MINUTE, Integer.valueOf(this.eCalK.get(12)));
                    }
                } else {
                    values.put(Memo.SCHE_S_HOUR, Integer.valueOf((int) REQUEST_CODE));
                    values.put(Memo.SCHE_S_MINUTE, Integer.valueOf((int) REQUEST_CODE));
                    values.put(Memo.SCHE_E_HOUR, Integer.valueOf((int) REQUEST_CODE));
                    values.put(Memo.SCHE_E_MINUTE, Integer.valueOf((int) REQUEST_CODE));
                }
                values.put(Memo.SCHE_LOCATION, this.locationK.trim());
            } else {
                values.put(Memo.SCHE_S_YEAR, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_S_MONTH, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_S_DAY, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_S_HOUR, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_S_MINUTE, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_S_TIME, "");
                values.put(Memo.SCHE_E_YEAR, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_E_MONTH, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_E_DAY, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_E_HOUR, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_E_MINUTE, Integer.valueOf((int) REQUEST_CODE));
                values.put(Memo.SCHE_E_TIME, "");
                values.put(Memo.SCHE_LOCATION, "");
            }
            String[] strArr = new String[REQUEST_PICK_CONTACT];
            strArr[REQUEST_CODE] = String.valueOf(recno);
            Cursor RS = DB.rawQuery("SELECT * FROM MEMO WHERE recno = ?", strArr);
            if (!RS.moveToFirst()) {
                values.put(Memo.CREATE_TIME, dateTime);
                values.put(Memo.UPDATE_TIME, dateTime);
                DB.insert("MEMO", null, values);
                insertFlg = true;
            } else {
                values.put(Memo.UPDATE_TIME, dateTime);
                String[] strArr2 = new String[REQUEST_PICK_CONTACT];
                strArr2[REQUEST_CODE] = String.valueOf(recno);
                DB.update("MEMO", values, "recno = ?", strArr2);
            }
            RS.close();
            if (insertFlg) {
                Cursor RS2 = DB.rawQuery(" SELECT last_insert_rowid()", null);
                if (RS2.moveToFirst()) {
                    recno = RS2.getInt(REQUEST_CODE);
                }
                RS2.close();
                this.currentRecno = recno;
            }
            StringBuilder sql = new StringBuilder();
            sql.append(" DELETE FROM SCHEDULE");
            sql.append(" WHERE memo_no = " + AppUtil.sql9(Integer.valueOf(recno)));
            DB.execSQL(sql.toString());
            if (this.schedule) {
                Calendar cal = Calendar.getInstance();
                cal.set(REQUEST_PICK_CONTACT, this.sCalK.get(REQUEST_PICK_CONTACT));
                cal.set(2, this.sCalK.get(2));
                cal.set(5, this.sCalK.get(5));
                cal.set(11, this.eCalK.get(11));
                cal.set(12, this.eCalK.get(12));
                cal.set(13, this.eCalK.get(13));
                cal.set(14, this.eCalK.get(14));
                int dayFlg = REQUEST_PICK_CONTACT;
                if (this.sCalK.get(REQUEST_PICK_CONTACT) == this.eCalK.get(REQUEST_PICK_CONTACT) && this.sCalK.get(2) == this.eCalK.get(2) && this.sCalK.get(5) == this.eCalK.get(5)) {
                    dayFlg = REQUEST_CODE;
                }
                boolean loopFlg = true;
                while (loopFlg) {
                    if (dayFlg != 0) {
                        if (cal.get(REQUEST_PICK_CONTACT) == this.sCalK.get(REQUEST_PICK_CONTACT) && cal.get(2) == this.sCalK.get(2) && cal.get(5) == this.sCalK.get(5)) {
                            dayFlg = REQUEST_PICK_CONTACT;
                        } else if (cal.get(REQUEST_PICK_CONTACT) == this.eCalK.get(REQUEST_PICK_CONTACT) && cal.get(2) == this.eCalK.get(2) && cal.get(5) == this.eCalK.get(5)) {
                            dayFlg = 3;
                        } else {
                            dayFlg = 2;
                        }
                    }
                    ContentValues values2 = new ContentValues();
                    values2.put("memo_no", Integer.valueOf(recno));
                    values2.put(Schedule.SCHE_YEAR, Integer.valueOf(cal.get(REQUEST_PICK_CONTACT)));
                    values2.put(Schedule.SCHE_MONTH, Integer.valueOf(cal.get(2)));
                    values2.put(Schedule.SCHE_DAY, Integer.valueOf(cal.get(5)));
                    values2.put(Schedule.DAY_FLG, Integer.valueOf(dayFlg));
                    DB.insert("SCHEDULE", null, values2);
                    cal.add(5, REQUEST_PICK_CONTACT);
                    if (cal.getTimeInMillis() > this.eCalK.getTimeInMillis()) {
                        loopFlg = false;
                    }
                }
            }
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            AppUtil.sendWidgetAction(this, AppUtil.ACTION_UPDATE);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            ArrayList<String> results = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (results.size() != 0) {
                if (results.size() > REQUEST_PICK_CONTACT) {
                    CharSequence[] items = new CharSequence[results.size()];
                    for (int i = REQUEST_CODE; i < results.size(); i += REQUEST_PICK_CONTACT) {
                        items[i] = results.get(i);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((int) R.string.str_recognitionresults);
                    builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                    final CharSequence[] charSequenceArr = items;
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            int loc = ActivityEdit.this.ET_MEMO.getSelectionStart();
                            String text = ActivityEdit.this.ET_MEMO.getText().toString();
                            ActivityEdit.this.ET_MEMO.setText(String.valueOf(text.substring(ActivityEdit.REQUEST_CODE, loc)) + ((Object) charSequenceArr[which]) + (loc < text.length() ? text.substring(loc, text.length()) : ""));
                            ActivityEdit.this.ET_MEMO.setSelection(charSequenceArr[which].length() + loc);
                        }
                    });
                    builder.create().show();
                } else {
                    int loc = this.ET_MEMO.getSelectionStart();
                    String text = this.ET_MEMO.getText().toString();
                    this.ET_MEMO.setText(String.valueOf(text.substring(REQUEST_CODE, loc)) + results.get(REQUEST_CODE) + (loc < text.length() ? text.substring(loc, text.length()) : ""));
                    this.ET_MEMO.setSelection(results.get(REQUEST_CODE).length() + loc);
                }
            }
        }
        if (requestCode == REQUEST_PICK_CONTACT && resultCode == -1) {
            ContentResolver contentResolver = getContentResolver();
            Uri data2 = data.getData();
            String[] strArr = new String[2];
            strArr[REQUEST_CODE] = "_display_name";
            strArr[REQUEST_PICK_CONTACT] = "_data";
            Cursor c = contentResolver.query(data2, strArr, null, null, null);
            if (c.moveToFirst()) {
                this.photoName = c.getString(c.getColumnIndex("_display_name"));
                this.photoUri = c.getString(c.getColumnIndex("_data"));
                if (!AppUtil.checkNull(this.photoUri).booleanValue()) {
                    AppUtil.showPhoto(getResources(), this.photoUri, this.IV_PHOTO);
                }
            }
            c.close();
        }
        if (requestCode == 2 && resultCode == -1) {
            ArrayList<String> results2 = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (results2.size() != 0) {
                if (results2.size() > REQUEST_PICK_CONTACT) {
                    CharSequence[] items2 = new CharSequence[results2.size()];
                    for (int i2 = REQUEST_CODE; i2 < results2.size(); i2 += REQUEST_PICK_CONTACT) {
                        items2[i2] = results2.get(i2);
                    }
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                    builder2.setTitle((int) R.string.str_recognitionresults);
                    builder2.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                    final CharSequence[] charSequenceArr2 = items2;
                    builder2.setItems(items2, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityEdit.this.ET_LOCATION.setText(charSequenceArr2[which]);
                            ActivityEdit.this.ET_LOCATION.setSelection(charSequenceArr2[which].length());
                        }
                    });
                    builder2.create().show();
                } else {
                    this.ET_LOCATION.setText(results2.get(REQUEST_CODE));
                    this.ET_LOCATION.setSelection(results2.get(REQUEST_CODE).length());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* access modifiers changed from: private */
    public void setCal(Calendar cal, String date, String time, Calendar rCal) {
        cal.set(REQUEST_PICK_CONTACT, Integer.valueOf(date.substring(REQUEST_CODE, 4)).intValue());
        cal.set(2, Integer.valueOf(date.substring(5, 7)).intValue() - REQUEST_PICK_CONTACT);
        cal.set(5, Integer.valueOf(date.substring(8, 10)).intValue());
        if (AppUtil.checkNull(time).booleanValue()) {
            cal.set(11, rCal.get(11));
            cal.set(12, rCal.get(12));
            return;
        }
        cal.set(11, Integer.valueOf(time.substring(REQUEST_CODE, 2)).intValue());
        cal.set(12, Integer.valueOf(time.substring(3, 5)).intValue());
    }

    /* access modifiers changed from: private */
    public CharSequence getDateTimeStr(Resources resources, int year, int month, int day, int hour, int minute, String time) {
        StringBuilder str = new StringBuilder();
        Object[] objArr = new Object[REQUEST_PICK_CONTACT];
        objArr[REQUEST_CODE] = Integer.valueOf(year);
        str.append(String.valueOf(String.format("%1$04d", objArr)) + resources.getString(R.string.str_year));
        Object[] objArr2 = new Object[REQUEST_PICK_CONTACT];
        objArr2[REQUEST_CODE] = Integer.valueOf(month + REQUEST_PICK_CONTACT);
        str.append(String.valueOf(String.format("%1$02d", objArr2)) + resources.getString(R.string.str_month));
        Object[] objArr3 = new Object[REQUEST_PICK_CONTACT];
        objArr3[REQUEST_CODE] = Integer.valueOf(day);
        str.append(String.valueOf(String.format("%1$02d", objArr3)) + resources.getString(R.string.str_day));
        Calendar cal = Calendar.getInstance();
        cal.set(REQUEST_PICK_CONTACT, year);
        cal.set(2, month);
        cal.set(5, day);
        cal.set(11, hour);
        cal.set(12, minute);
        cal.set(13, REQUEST_CODE);
        cal.set(14, REQUEST_CODE);
        switch (cal.get(7)) {
            case REQUEST_PICK_CONTACT /*1*/:
                str.append("(<font color=\"Red\">" + resources.getString(R.string.str_week1) + "</font>) ");
                break;
            case 2:
                str.append("(<font color=\"Black\">" + resources.getString(R.string.str_week2) + "</font>) ");
                break;
            case 3:
                str.append("(<font color=\"Black\">" + resources.getString(R.string.str_week3) + "</font>) ");
                break;
            case 4:
                str.append("(<font color=\"Black\">" + resources.getString(R.string.str_week4) + "</font>) ");
                break;
            case 5:
                str.append("(<font color=\"Black\">" + resources.getString(R.string.str_week5) + "</font>) ");
                break;
            case 6:
                str.append("(<font color=\"Black\">" + resources.getString(R.string.str_week6) + "</font>) ");
                break;
            case 7:
                str.append("(<font color=\"Blue\">" + resources.getString(R.string.str_week7) + "</font>) ");
                break;
        }
        if (!AppUtil.checkNull(time).booleanValue()) {
            Object[] objArr4 = new Object[REQUEST_PICK_CONTACT];
            objArr4[REQUEST_CODE] = Integer.valueOf(hour);
            str.append(String.valueOf(String.format("%1$02d", objArr4)) + ":");
            Object[] objArr5 = new Object[REQUEST_PICK_CONTACT];
            objArr5[REQUEST_CODE] = Integer.valueOf(minute);
            str.append(String.format("%1$02d", objArr5));
        }
        return Html.fromHtml(str.toString());
    }

    private void showPhotoDialog() {
        View photoView = LayoutInflater.from(this).inflate((int) R.layout.dlg_photo, (ViewGroup) null);
        ((Button) photoView.findViewById(R.id.B_PHOTOSEL)).setOnClickListener(this);
        this.IV_PHOTO = (ImageView) photoView.findViewById(R.id.IV_PHOTO);
        ((Button) photoView.findViewById(R.id.B_PHOTOOK)).setOnClickListener(this);
        ((Button) photoView.findViewById(R.id.B_PHOTODEL)).setOnClickListener(this);
        ((Button) photoView.findViewById(R.id.B_PHOTOCANCEL)).setOnClickListener(this);
        this.photoName = this.photoNameK;
        this.photoUri = this.photoUriK;
        if (!AppUtil.checkNull(this.photoUri).booleanValue()) {
            AppUtil.showPhoto(getResources(), this.photoUri, this.IV_PHOTO);
        }
        this.photoDialog = new AlertDialog.Builder(this).setCancelable(false).setTitle((int) R.string.str_photo).create();
        this.photoDialog.setView(photoView, REQUEST_CODE, REQUEST_CODE, REQUEST_CODE, REQUEST_CODE);
        this.photoDialog.show();
    }

    private void showScheDialog() {
        View scheduleView = LayoutInflater.from(this).inflate((int) R.layout.dlg_schedule, (ViewGroup) null);
        this.B_SDAY = (Button) scheduleView.findViewById(R.id.B_SDAY);
        this.B_SDAY.setOnClickListener(this);
        this.B_EDAY = (Button) scheduleView.findViewById(R.id.B_EDAY);
        this.B_EDAY.setOnClickListener(this);
        ((ImageButton) scheduleView.findViewById(R.id.IB_SPEAK)).setOnClickListener(this);
        ((ImageButton) scheduleView.findViewById(R.id.IB_LOCALIST)).setOnClickListener(this);
        ((Button) scheduleView.findViewById(R.id.B_SCHEOK)).setOnClickListener(this);
        ((Button) scheduleView.findViewById(R.id.B_SCHEDEL)).setOnClickListener(this);
        ((Button) scheduleView.findViewById(R.id.B_SCHECANCEL)).setOnClickListener(this);
        this.sCal.setTimeInMillis(this.sCalK.getTimeInMillis());
        this.sTime = this.sTimeK;
        this.eCal.setTimeInMillis(this.eCalK.getTimeInMillis());
        this.eTime = this.eTimeK;
        this.B_SDAY.setText(getDateTimeStr(getResources(), this.sCal.get(REQUEST_PICK_CONTACT), this.sCal.get(2), this.sCal.get(5), this.sCal.get(11), this.sCal.get(12), this.sTime));
        this.B_EDAY.setText(getDateTimeStr(getResources(), this.eCal.get(REQUEST_PICK_CONTACT), this.eCal.get(2), this.eCal.get(5), this.eCal.get(11), this.eCal.get(12), this.eTime));
        this.ET_LOCATION = (EditText) scheduleView.findViewById(R.id.ET_LOCATION);
        this.ET_LOCATION.setText(this.locationK);
        Bundle bundleE = this.ET_LOCATION.getInputExtras(true);
        if (bundleE != null) {
            bundleE.putBoolean("allowEmoji", true);
        }
        this.scheDialog = new AlertDialog.Builder(this).setCancelable(false).setTitle((int) R.string.str_schedule).create();
        this.scheDialog.setView(scheduleView, REQUEST_CODE, REQUEST_CODE, REQUEST_CODE, REQUEST_CODE);
        this.scheDialog.show();
    }

    private CharSequence[] getLocationList() {
        CharSequence[] items = null;
        SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
        int i = REQUEST_CODE;
        Cursor RS = DB.rawQuery(" SELECT" + " sche_location" + " FROM MEMO" + " WHERE sche_location IS NOT NULL" + " AND   sche_location <> ''" + " GROUP BY sche_location" + " ORDER BY sche_location", null);
        if (RS.getCount() != 0) {
            items = new CharSequence[RS.getCount()];
            while (RS.moveToNext()) {
                items[i] = RS.getString(RS.getColumnIndex(Memo.SCHE_LOCATION));
                i += REQUEST_PICK_CONTACT;
            }
        }
        RS.close();
        DB.close();
        return items;
    }
}
