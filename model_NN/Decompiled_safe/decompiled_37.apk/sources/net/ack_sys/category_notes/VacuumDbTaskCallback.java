package net.ack_sys.category_notes;

public interface VacuumDbTaskCallback {
    void onFailedVacuumDb(String str);

    void onSuccessVacuumDb(String str);
}
