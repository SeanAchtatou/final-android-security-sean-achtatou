package net.ack_sys.category_notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class CategoryAdapter extends ArrayAdapter<Category> {
    private List<Category> categorys;
    private Context context;
    private LayoutInflater inflater;

    public CategoryAdapter(Context context2, int textViewResourceId, List<Category> categorys2) {
        super(context2, textViewResourceId, categorys2);
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.categorys = categorys2;
    }

    public void setCategorys(List<Category> categorys2) {
        this.categorys = categorys2;
    }

    public Category getItem(int position) {
        return this.categorys.get(position);
    }

    public int getCount() {
        return this.categorys.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            int minHeight = this.context.getResources().getDimensionPixelSize(R.dimen.row_category_height);
            view = this.inflater.inflate((int) R.layout.row_category, (ViewGroup) null);
            view.setMinimumHeight(minHeight);
            holder = new ViewHolder(null);
            holder.IV_CATE_ICON = (ImageView) view.findViewById(R.id.IV_CATE_ICON);
            holder.TV_CATE_TITLE = (TextView) view.findViewById(R.id.TV_CATE_TITLE);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Category category = getItem(position);
        holder.IV_CATE_ICON.setImageResource(AppUtil.getCateResId(category.getIconId()));
        holder.TV_CATE_TITLE.setText(category.getTitle());
        return view;
    }

    private static class ViewHolder {
        ImageView IV_CATE_ICON;
        TextView TV_CATE_TITLE;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
