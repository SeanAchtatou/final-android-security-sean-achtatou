package net.ack_sys.category_notes;

public class AppInfo {
    private static AppInfo instance = new AppInfo();
    private int currentCateId;
    private int currentPosition;
    private float scaledDensity;

    private AppInfo() {
    }

    public static AppInfo getInstance() {
        return instance;
    }

    public float getScaledDensity() {
        return this.scaledDensity;
    }

    public void setScaledDensity(float scaledDensity2) {
        this.scaledDensity = scaledDensity2;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public void setCurrentPosition(int currentPosition2) {
        this.currentPosition = currentPosition2;
    }

    public int getCurrentCateId() {
        return this.currentCateId;
    }

    public void setCurrentCateId(int currentCateId2) {
        this.currentCateId = currentCateId2;
    }
}
