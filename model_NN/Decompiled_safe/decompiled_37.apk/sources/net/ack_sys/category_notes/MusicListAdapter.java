package net.ack_sys.category_notes;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class MusicListAdapter extends SimpleCursorAdapter {
    private int layout;
    private int minHeight;

    public MusicListAdapter(Context context, int layout2, Cursor c, String[] from, int[] to) {
        super(context, layout2, c, from, to);
        this.layout = layout2;
        this.minHeight = context.getResources().getDimensionPixelSize(R.dimen.row_music_height);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View v = LayoutInflater.from(context).inflate(this.layout, parent, false);
        v.setMinimumHeight(this.minHeight);
        setMusicTitle(v, cursor);
        return v;
    }

    public void bindView(View view, Context context, Cursor cursor) {
        setMusicTitle(view, cursor);
    }

    private void setMusicTitle(View view, Cursor cursor) {
        String name = cursor.getString(cursor.getColumnIndex("_display_name"));
        TextView TV_MUSIC_TITLE = (TextView) view.findViewById(R.id.TV_MUSIC_TITLE);
        if (TV_MUSIC_TITLE != null) {
            TV_MUSIC_TITLE.setText(name);
        }
    }
}
