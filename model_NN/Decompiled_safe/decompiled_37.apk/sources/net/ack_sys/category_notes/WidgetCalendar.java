package net.ack_sys.category_notes;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Html;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class WidgetCalendar extends AppWidgetProvider {
    public static Map<Integer, Integer> DayMap = new HashMap(43);
    public static int aDay = cal.get(5);
    public static int aMonth = cal.get(2);
    public static int aYear = cal.get(1);
    public static List<Integer> bDayList = new ArrayList(43);
    public static Map<Integer, Integer> bDayMap = new HashMap(43);
    public static Calendar cal = Calendar.getInstance();
    public static boolean createFlg;
    public static Map<Integer, String> holidayMap;
    public static int index = -1;
    public static List<Integer> ivCateList1 = new ArrayList(43);
    public static List<Integer> ivCateList2 = new ArrayList(43);
    public static List<Integer> ivCateList3 = new ArrayList(43);
    public static List<Integer> ivRangeList = new ArrayList(43);
    public static List<Integer> llDayList = new ArrayList(43);
    public static int oldIndex = -1;
    public static List<Integer> tvDayList = new ArrayList(43);

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        updateAlarm(context);
        context.startService(new Intent(context, UpdateService.class));
        new Prefs(context).setWidgetFlg(true);
    }

    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
        deleteAlarm(context);
        context.stopService(new Intent(context, UpdateService.class));
        new Prefs(context).setWidgetFlg(false);
    }

    private void updateAlarm(Context context) {
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        Calendar alarmCal = AppUtil.getNextDay(new int[]{1, 1, 1, 1, 1, 1, 1}, 0, 0);
        try {
            DB.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT recno FROM ALARM");
            sql.append(" WHERE memo_no = 0");
            Cursor RS = DB.rawQuery(sql.toString(), null);
            while (RS.moveToNext()) {
                AppUtil.cancelAlarmManager(context, RS.getInt(0));
                StringBuilder sql2 = new StringBuilder();
                sql2.append(" DELETE FROM ALARM");
                sql2.append(" WHERE recno = " + AppUtil.sql9(Integer.valueOf(RS.getInt(0))));
                DB.execSQL(sql2.toString());
            }
            RS.close();
            ContentValues values = new ContentValues();
            values.put("memo_no", (Integer) null);
            values.put(Alarm.ALARM_YEAR, Integer.valueOf(alarmCal.get(1)));
            values.put(Alarm.ALARM_MONTH, Integer.valueOf(alarmCal.get(2)));
            values.put(Alarm.ALARM_DAY, Integer.valueOf(alarmCal.get(5)));
            values.put(Alarm.ALARM_HOUR, (Integer) null);
            values.put(Alarm.ALARM_MINUTE, (Integer) null);
            values.put(Alarm.ALARM_LOOP, (Integer) 1);
            for (int i = 1; i <= 7; i++) {
                values.put("alarm_week" + String.valueOf(i), (Integer) 1);
            }
            values.put("enable", (Integer) null);
            values.put(Alarm.SNOOZE, (Integer) null);
            DB.insert("ALARM", null, values);
            StringBuilder sql3 = new StringBuilder();
            sql3.append(" SELECT last_insert_rowid()");
            Cursor RS2 = DB.rawQuery(sql3.toString(), null);
            if (RS2.moveToFirst()) {
                AppUtil.setAlarmManager(context, RS2.getInt(0), alarmCal.get(1), alarmCal.get(2), alarmCal.get(5), 0, 0);
            }
            RS2.close();
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
        }
    }

    private void deleteAlarm(Context context) {
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        try {
            DB.beginTransaction();
            Cursor RS = DB.rawQuery(" SELECT recno FROM ALARM" + " WHERE memo_no = 0", null);
            while (RS.moveToNext()) {
                AppUtil.cancelAlarmManager(context, RS.getInt(0));
                StringBuilder sql = new StringBuilder();
                sql.append(" DELETE FROM ALARM");
                sql.append(" WHERE recno = " + AppUtil.sql9(Integer.valueOf(RS.getInt(0))));
                DB.execSQL(sql.toString());
            }
            RS.close();
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
        }
    }

    public static class UpdateService extends Service {
        public void onStart(Intent intent, int startId) {
            if (intent != null) {
                if (WidgetCalendar.tvDayList.size() == 0) {
                    WidgetCalendar.createFlg = true;
                    WidgetCalendar.tvDayList.add(0);
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY1));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY2));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY3));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY4));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY5));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY6));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY7));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY8));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY9));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY10));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY11));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY12));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY13));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY14));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY15));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY16));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY17));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY18));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY19));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY20));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY21));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY22));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY23));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY24));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY25));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY26));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY27));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY28));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY29));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY30));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY31));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY32));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY33));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY34));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY35));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY36));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY37));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY38));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY39));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY40));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY41));
                    WidgetCalendar.tvDayList.add(Integer.valueOf((int) R.id.TV_DAY42));
                    WidgetCalendar.ivRangeList.add(0);
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE1));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE2));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE3));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE4));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE5));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE6));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE7));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE8));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE9));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE10));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE11));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE12));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE13));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE14));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE15));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE16));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE17));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE18));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE19));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE20));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE21));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE22));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE23));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE24));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE25));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE26));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE27));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE28));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE29));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE30));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE31));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE32));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE33));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE34));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE35));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE36));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE37));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE38));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE39));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE40));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE41));
                    WidgetCalendar.ivRangeList.add(Integer.valueOf((int) R.id.IV_RANGE42));
                    WidgetCalendar.ivCateList1.add(0);
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_1));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_2));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_3));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_4));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_5));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_6));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_7));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_8));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_9));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_10));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_11));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_12));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_13));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_14));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_15));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_16));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_17));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_18));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_19));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_20));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_21));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_22));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_23));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_24));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_25));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_26));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_27));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_28));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_29));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_30));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_31));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_32));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_33));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_34));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_35));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_36));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_37));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_38));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_39));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_40));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_41));
                    WidgetCalendar.ivCateList1.add(Integer.valueOf((int) R.id.IV_CATE1_42));
                    WidgetCalendar.ivCateList2.add(0);
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_1));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_2));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_3));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_4));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_5));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_6));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_7));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_8));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_9));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_10));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_11));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_12));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_13));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_14));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_15));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_16));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_17));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_18));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_19));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_20));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_21));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_22));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_23));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_24));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_25));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_26));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_27));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_28));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_29));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_30));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_31));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_32));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_33));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_34));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_35));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_36));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_37));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_38));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_39));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_40));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_41));
                    WidgetCalendar.ivCateList2.add(Integer.valueOf((int) R.id.IV_CATE2_42));
                    WidgetCalendar.ivCateList3.add(0);
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_1));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_2));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_3));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_4));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_5));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_6));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_7));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_8));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_9));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_10));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_11));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_12));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_13));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_14));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_15));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_16));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_17));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_18));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_19));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_20));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_21));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_22));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_23));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_24));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_25));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_26));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_27));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_28));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_29));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_30));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_31));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_32));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_33));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_34));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_35));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_36));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_37));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_38));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_39));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_40));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_41));
                    WidgetCalendar.ivCateList3.add(Integer.valueOf((int) R.id.IV_CATE3_42));
                    WidgetCalendar.bDayList.add(0);
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY1));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY2));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY3));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY4));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY5));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY6));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY7));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY8));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY9));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY10));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY11));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY12));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY13));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY14));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY15));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY16));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY17));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY18));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY19));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY20));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY21));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY22));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY23));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY24));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY25));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY26));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY27));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY28));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY29));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY30));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY31));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY32));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY33));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY34));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY35));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY36));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY37));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY38));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY39));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY40));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY41));
                    WidgetCalendar.bDayList.add(Integer.valueOf((int) R.id.B_DAY42));
                    int i = 0;
                    for (Integer id : WidgetCalendar.bDayList) {
                        WidgetCalendar.bDayMap.put(id, Integer.valueOf(i));
                        i++;
                    }
                    WidgetCalendar.llDayList.add(0);
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY1));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY2));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY3));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY4));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY5));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY6));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY7));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY8));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY9));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY10));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY11));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY12));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY13));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY14));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY15));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY16));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY17));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY18));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY19));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY20));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY21));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY22));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY23));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY24));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY25));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY26));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY27));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY28));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY29));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY30));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY31));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY32));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY33));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY34));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY35));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY36));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY37));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY38));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY39));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY40));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY41));
                    WidgetCalendar.llDayList.add(Integer.valueOf((int) R.id.LL_DAY42));
                }
                RemoteViews updateViews = buildUpdate(intent);
                AppWidgetManager.getInstance(getApplicationContext()).updateAppWidget(new ComponentName(getApplicationContext(), WidgetCalendar.class), updateViews);
            }
        }

        public IBinder onBind(Intent intent) {
            return null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
        public RemoteViews buildUpdate(Intent intent) {
            RemoteViews rViews = new RemoteViews(getPackageName(), (int) R.layout.wid_calendar);
            Intent intentBack = new Intent();
            intentBack.setAction(AppUtil.ACTION_BACKCLICK);
            rViews.setOnClickPendingIntent(R.id.IV_BACK, PendingIntent.getService(getApplicationContext(), 0, intentBack, 0));
            Intent intentToday = new Intent();
            intentToday.setAction(AppUtil.ACTION_TODAYCLICK);
            rViews.setOnClickPendingIntent(R.id.TV_TODAY, PendingIntent.getService(getApplicationContext(), 0, intentToday, 0));
            Intent intentNext = new Intent();
            intentNext.setAction(AppUtil.ACTION_NEXTCLICK);
            rViews.setOnClickPendingIntent(R.id.IV_NEXT, PendingIntent.getService(getApplicationContext(), 0, intentNext, 0));
            Intent intentApp = new Intent(getApplicationContext(), ActivityScheMemo.class);
            intentApp.putExtra("widget", true);
            rViews.setOnClickPendingIntent(R.id.IV_APP, PendingIntent.getActivity(getApplicationContext(), 0, intentApp, 134217728));
            if (AppUtil.ACTION_DAYCLICK.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                if (extras != null) {
                    WidgetCalendar.index = extras.getInt("index", 1);
                    if (WidgetCalendar.oldIndex != -1) {
                        rViews.setInt(WidgetCalendar.llDayList.get(WidgetCalendar.oldIndex).intValue(), "setBackgroundColor", 0);
                    }
                    rViews.setInt(WidgetCalendar.llDayList.get(WidgetCalendar.index).intValue(), "setBackgroundColor", -2053333348);
                    WidgetCalendar.oldIndex = WidgetCalendar.index;
                    if (WidgetCalendar.DayMap.size() == 0) {
                        dayInit(rViews);
                        setRemoteViews(rViews);
                    } else if (WidgetCalendar.DayMap.get(Integer.valueOf(WidgetCalendar.index)).intValue() == 0) {
                        WidgetCalendar.aDay = 0;
                        WidgetCalendar.cal.set(5, 1);
                    } else {
                        WidgetCalendar.aDay = WidgetCalendar.DayMap.get(Integer.valueOf(WidgetCalendar.index)).intValue();
                        WidgetCalendar.cal.set(5, WidgetCalendar.aDay);
                    }
                    setDayInfo(rViews);
                }
            } else if (AppUtil.ACTION_BACKCLICK.equals(intent.getAction())) {
                if (WidgetCalendar.DayMap.size() == 0) {
                    dayInit(rViews);
                }
                WidgetCalendar.cal.add(2, -1);
                setRemoteViews(rViews);
                setDayInfo(rViews);
            } else if (AppUtil.ACTION_TODAYCLICK.equals(intent.getAction())) {
                if (WidgetCalendar.DayMap.size() == 0) {
                    dayInit(rViews);
                }
                WidgetCalendar.cal.setTimeInMillis(System.currentTimeMillis());
                WidgetCalendar.createFlg = true;
                setRemoteViews(rViews);
                setDayInfo(rViews);
            } else if (AppUtil.ACTION_NEXTCLICK.equals(intent.getAction())) {
                if (WidgetCalendar.DayMap.size() == 0) {
                    dayInit(rViews);
                }
                WidgetCalendar.cal.add(2, 1);
                setRemoteViews(rViews);
                setDayInfo(rViews);
            } else if (AppUtil.ACTION_UPDATE.equals(intent.getAction())) {
                if (WidgetCalendar.DayMap.size() == 0) {
                    dayInit(rViews);
                }
                setRemoteViews(rViews);
                setDayInfo(rViews);
            } else if (AppUtil.ACTION_CHANGE_COLOR.equals(intent.getAction())) {
                setRemoteViews(rViews);
                setDayInfo(rViews);
                setColor(rViews);
            } else {
                setRemoteViews(rViews);
                setDayInfo(rViews);
                setColor(rViews);
            }
            return rViews;
        }

        /* Debug info: failed to restart local var, previous not found, register: 33 */
        private void setRemoteViews(RemoteViews rViews) {
            int intTextColor = Integer.valueOf(new Prefs(this).getTextColor()).intValue();
            SQLiteDatabase DB = new DBEngine(this).getReadableDatabase();
            int s = 0;
            Calendar now = Calendar.getInstance();
            int nowYear = now.get(1);
            int nowMonth = now.get(2);
            int nowDay = now.get(5);
            WidgetCalendar.cal.set(5, 1);
            switch (WidgetCalendar.cal.get(7)) {
                case 1:
                    s = 1;
                    break;
                case DBEngine.DB_VERSION:
                    s = 2;
                    break;
                case 3:
                    s = 3;
                    break;
                case 4:
                    s = 4;
                    break;
                case 5:
                    s = 5;
                    break;
                case 6:
                    s = 6;
                    break;
                case 7:
                    s = 7;
                    break;
            }
            int year = WidgetCalendar.cal.get(1);
            int month = WidgetCalendar.cal.get(2);
            WidgetCalendar.holidayMap = new HashMap();
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT");
            sql.append(" holi_day,");
            if (Locale.JAPAN.equals(Locale.getDefault())) {
                sql.append(" title1");
            } else {
                sql.append(" title2");
            }
            sql.append(" FROM HOLIDAY");
            sql.append(" WHERE holi_year = " + String.valueOf(year));
            sql.append(" AND   holi_month = " + String.valueOf(month));
            Cursor RS = DB.rawQuery(sql.toString(), null);
            while (RS.moveToNext()) {
                WidgetCalendar.holidayMap.put(Integer.valueOf(RS.getInt(0)), RS.getString(1));
            }
            RS.close();
            HashMap hashMap = new HashMap();
            ArrayList arrayList = new ArrayList(2);
            int tDay = 0;
            StringBuilder sql2 = new StringBuilder();
            sql2.append(" SELECT");
            sql2.append(" S.sche_day,");
            sql2.append(" S.day_flg,");
            sql2.append(" C.icon_id ");
            sql2.append(" FROM SCHEDULE S");
            sql2.append(" INNER JOIN MEMO M ON");
            sql2.append("       S.memo_no = M.recno");
            sql2.append(" INNER JOIN CATEGORY C ON");
            sql2.append("       M.cate_id = C.id");
            sql2.append(" WHERE S.sche_year = " + String.valueOf(year));
            sql2.append(" AND   S.sche_month = " + String.valueOf(month));
            sql2.append(" ORDER BY");
            sql2.append(" S.sche_day,");
            sql2.append(" S.day_flg DESC,");
            sql2.append(" M.sche_s_time,");
            sql2.append(" M.recno");
            Cursor RS2 = DB.rawQuery(sql2.toString(), null);
            while (RS2.moveToNext()) {
                if (RS2.getInt(0) != tDay) {
                    if (tDay != 0) {
                        hashMap.put(Integer.valueOf(tDay), arrayList);
                    }
                    arrayList = new ArrayList(2);
                }
                arrayList.add(Integer.valueOf(RS2.getInt(1)));
                arrayList.add(Integer.valueOf(RS2.getInt(2)));
                tDay = RS2.getInt(0);
            }
            RS2.close();
            if (tDay != 0) {
                hashMap.put(Integer.valueOf(tDay), arrayList);
            }
            rViews.setTextViewText(R.id.TV_MONTH, String.valueOf(String.valueOf(year)) + getString(R.string.str_year) + String.valueOf(month + 1) + getString(R.string.str_month2));
            Intent[] clickIntent = new Intent[43];
            PendingIntent[] pendingIntent = new PendingIntent[43];
            int strIndex = 0;
            for (int i = 1; i <= 42; i++) {
                int cMonth = WidgetCalendar.cal.get(2);
                int cDay = WidgetCalendar.cal.get(5);
                int cWeek = WidgetCalendar.cal.get(7);
                clickIntent[i] = new Intent(AppUtil.ACTION_DAYCLICK);
                clickIntent[i].putExtra("index", i);
                pendingIntent[i] = PendingIntent.getService(getApplicationContext(), i, clickIntent[i], 134217728);
                rViews.setOnClickPendingIntent(WidgetCalendar.bDayList.get(i).intValue(), pendingIntent[i]);
                rViews.setInt(WidgetCalendar.tvDayList.get(i).intValue(), "setBackgroundColor", 0);
                rViews.setViewVisibility(WidgetCalendar.ivRangeList.get(i).intValue(), 4);
                rViews.setViewVisibility(WidgetCalendar.ivCateList1.get(i).intValue(), 4);
                rViews.setViewVisibility(WidgetCalendar.ivCateList2.get(i).intValue(), 4);
                rViews.setViewVisibility(WidgetCalendar.ivCateList3.get(i).intValue(), 4);
                if (i < s || cMonth != month) {
                    WidgetCalendar.DayMap.put(Integer.valueOf(i), 0);
                    rViews.setTextViewText(WidgetCalendar.tvDayList.get(i).intValue(), null);
                } else {
                    WidgetCalendar.DayMap.put(Integer.valueOf(i), Integer.valueOf(cDay));
                    if (strIndex == 0) {
                        strIndex = i;
                    }
                    rViews.setTextViewText(WidgetCalendar.tvDayList.get(i).intValue(), String.valueOf(cDay));
                    if (year == nowYear && cMonth == nowMonth && cDay == nowDay) {
                        rViews.setInt(WidgetCalendar.tvDayList.get(i).intValue(), "setBackgroundColor", -2060215809);
                        if (WidgetCalendar.createFlg) {
                            WidgetCalendar.index = i;
                        }
                    }
                    if (cWeek != 1) {
                        if (WidgetCalendar.holidayMap.containsKey(Integer.valueOf(cDay))) {
                            if (intTextColor == 0) {
                                rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -18751);
                            } else {
                                rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -2396013);
                            }
                        } else if (cWeek == 7) {
                            if (intTextColor == 0) {
                                rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -16711681);
                            } else {
                                rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -12490271);
                            }
                        } else if (intTextColor == 0) {
                            rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -1);
                        } else {
                            rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -16777216);
                        }
                    } else if (intTextColor == 0) {
                        rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -18751);
                    } else {
                        rViews.setTextColor(WidgetCalendar.tvDayList.get(i).intValue(), -2396013);
                    }
                    if (hashMap.containsKey(Integer.valueOf(cDay))) {
                        int z = 0;
                        for (Integer sm : (List) hashMap.get(Integer.valueOf(cDay))) {
                            z++;
                            if (z % 2 != 0) {
                                if (sm.intValue() != 0) {
                                    if (sm.intValue() == 1) {
                                        rViews.setInt(WidgetCalendar.ivRangeList.get(i).intValue(), "setImageResource", R.drawable.range_s);
                                    } else if (sm.intValue() == 2) {
                                        rViews.setInt(WidgetCalendar.ivRangeList.get(i).intValue(), "setImageResource", R.drawable.range);
                                    } else {
                                        rViews.setInt(WidgetCalendar.ivRangeList.get(i).intValue(), "setImageResource", R.drawable.range_e);
                                    }
                                    rViews.setViewVisibility(WidgetCalendar.ivRangeList.get(i).intValue(), 0);
                                }
                            } else if (z == 2) {
                                rViews.setInt(WidgetCalendar.ivCateList1.get(i).intValue(), "setImageResource", AppUtil.getCateResId15(sm.intValue()));
                                rViews.setViewVisibility(WidgetCalendar.ivCateList1.get(i).intValue(), 0);
                            } else if (z == 4) {
                                rViews.setInt(WidgetCalendar.ivCateList2.get(i).intValue(), "setImageResource", AppUtil.getCateResId15(sm.intValue()));
                                rViews.setViewVisibility(WidgetCalendar.ivCateList2.get(i).intValue(), 0);
                            } else {
                                rViews.setInt(WidgetCalendar.ivCateList3.get(i).intValue(), "setImageResource", AppUtil.getCateResId15(sm.intValue()));
                                rViews.setViewVisibility(WidgetCalendar.ivCateList3.get(i).intValue(), 0);
                            }
                            if (z == 6) {
                            }
                        }
                    }
                    WidgetCalendar.cal.add(5, 1);
                }
            }
            WidgetCalendar.cal.set(1, year);
            WidgetCalendar.cal.set(2, month);
            DB.close();
            WidgetCalendar.aYear = year;
            WidgetCalendar.aMonth = month;
            if (!WidgetCalendar.DayMap.containsKey(Integer.valueOf(WidgetCalendar.index))) {
                WidgetCalendar.index = strIndex;
            }
            if (WidgetCalendar.DayMap.get(Integer.valueOf(WidgetCalendar.index)).intValue() == 0) {
                WidgetCalendar.aDay = 0;
                WidgetCalendar.cal.set(5, 1);
            } else {
                WidgetCalendar.aDay = WidgetCalendar.DayMap.get(Integer.valueOf(WidgetCalendar.index)).intValue();
                WidgetCalendar.cal.set(5, WidgetCalendar.aDay);
            }
            if (WidgetCalendar.oldIndex != -1) {
                rViews.setInt(WidgetCalendar.llDayList.get(WidgetCalendar.oldIndex).intValue(), "setBackgroundColor", 0);
            }
            rViews.setInt(WidgetCalendar.llDayList.get(WidgetCalendar.index).intValue(), "setBackgroundColor", -2053333348);
            WidgetCalendar.oldIndex = WidgetCalendar.index;
            WidgetCalendar.createFlg = false;
        }

        private void setDayInfo(RemoteViews rViews) {
            String wColor1;
            String wColor;
            String wColor7;
            Prefs prefs = new Prefs(this);
            if (WidgetCalendar.aDay != 0) {
                if (Integer.valueOf(prefs.getTextColor()).intValue() == 0) {
                    wColor1 = "#ffb6c1";
                    wColor = "White";
                    wColor7 = "#00ffff";
                } else {
                    wColor1 = "#db7093";
                    wColor = "Black";
                    wColor7 = "#4169e1";
                }
                StringBuilder str = new StringBuilder();
                str.append(String.valueOf(String.format("%1$04d", Integer.valueOf(WidgetCalendar.aYear))) + getString(R.string.str_year));
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(WidgetCalendar.aMonth + 1))) + getString(R.string.str_month));
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(WidgetCalendar.aDay))) + getString(R.string.str_day));
                Calendar cal = Calendar.getInstance();
                cal.set(1, WidgetCalendar.aYear);
                cal.set(2, WidgetCalendar.aMonth);
                cal.set(5, WidgetCalendar.aDay);
                switch (cal.get(7)) {
                    case 1:
                        str.append("(<font color=\"" + wColor1 + "\">" + getString(R.string.str_week1) + "</font>) ");
                        break;
                    case DBEngine.DB_VERSION:
                        str.append("(<font color=\"" + wColor + "\">" + getString(R.string.str_week2) + "</font>) ");
                        break;
                    case 3:
                        str.append("(<font color=\"" + wColor + "\">" + getString(R.string.str_week3) + "</font>) ");
                        break;
                    case 4:
                        str.append("(<font color=\"" + wColor + "\">" + getString(R.string.str_week4) + "</font>) ");
                        break;
                    case 5:
                        str.append("(<font color=\"" + wColor + "\">" + getString(R.string.str_week5) + "</font>) ");
                        break;
                    case 6:
                        str.append("(<font color=\"" + wColor + "\">" + getString(R.string.str_week6) + "</font>) ");
                        break;
                    case 7:
                        str.append("(<font color=\"" + wColor7 + "\">" + getString(R.string.str_week7) + "</font>) ");
                        break;
                }
                if (prefs.getSixWeek()) {
                    str.append(String.valueOf(QReki.RokuYo(WidgetCalendar.aYear, WidgetCalendar.aMonth + 1, WidgetCalendar.aDay)) + " ");
                }
                if (WidgetCalendar.holidayMap.containsKey(Integer.valueOf(WidgetCalendar.aDay))) {
                    str.append("<font color=\"" + wColor1 + "\">" + WidgetCalendar.holidayMap.get(Integer.valueOf(WidgetCalendar.aDay)) + "</font>");
                }
                rViews.setTextViewText(R.id.TV_INFO, Html.fromHtml(str.toString()));
                rViews.setTextViewText(R.id.TV_MEMO1, null);
                rViews.setViewVisibility(R.id.IV_CATE1, 4);
                rViews.setTextViewText(R.id.TV_MEMO2, null);
                rViews.setViewVisibility(R.id.IV_CATE2, 4);
                rViews.setTextViewText(R.id.TV_MEMO3, null);
                rViews.setViewVisibility(R.id.IV_CATE3, 4);
                SQLiteDatabase DB = null;
                int z = 0;
                try {
                    DB = new DBEngine(this).getReadableDatabase();
                    StringBuilder sql = new StringBuilder();
                    sql.append(" SELECT");
                    sql.append(" M.recno,");
                    sql.append(" M.cate_id,");
                    sql.append(" C.icon_id,");
                    sql.append(" M.tag_id,");
                    sql.append(" M.memo,");
                    sql.append(" M.photo_name,");
                    sql.append(" M.photo_uri,");
                    sql.append(" M.sche_s_year,");
                    sql.append(" M.sche_s_month,");
                    sql.append(" M.sche_s_day,");
                    sql.append(" M.sche_s_hour,");
                    sql.append(" M.sche_s_minute,");
                    sql.append(" M.sche_s_time,");
                    sql.append(" M.sche_e_year,");
                    sql.append(" M.sche_e_month,");
                    sql.append(" M.sche_e_day,");
                    sql.append(" M.sche_e_hour,");
                    sql.append(" M.sche_e_minute,");
                    sql.append(" M.sche_e_time,");
                    sql.append(" M.sche_location,");
                    sql.append(" M.create_time,");
                    sql.append(" M.update_time,");
                    sql.append(" A.alarm_year,");
                    sql.append(" S.day_flg ");
                    sql.append(" FROM SCHEDULE S");
                    sql.append(" INNER JOIN MEMO M ON");
                    sql.append("       S.memo_no = M.recno");
                    sql.append(" INNER JOIN CATEGORY C ON");
                    sql.append("       M.cate_id = C.id");
                    sql.append(" LEFT JOIN ALARM A ON");
                    sql.append("       S.memo_no = A.memo_no");
                    sql.append(" AND   A.snooze = 0");
                    sql.append(" WHERE S.sche_year = " + AppUtil.sql9(Integer.valueOf(WidgetCalendar.aYear)));
                    sql.append(" AND   S.sche_month = " + AppUtil.sql9(Integer.valueOf(WidgetCalendar.aMonth)));
                    sql.append(" AND   S.sche_day = " + AppUtil.sql9(Integer.valueOf(WidgetCalendar.aDay)));
                    sql.append(" ORDER BY");
                    sql.append(" S.day_flg DESC,");
                    sql.append(" M.sche_s_time,");
                    sql.append(" M.recno");
                    Cursor RS = DB.rawQuery(sql.toString(), null);
                    while (true) {
                        if (RS.moveToNext()) {
                            CharSequence sTime = AppUtil.getScheduleStr2(getResources(), RS.getInt(RS.getColumnIndex(Schedule.DAY_FLG)), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_HOUR)), RS.getInt(RS.getColumnIndex(Memo.SCHE_S_MINUTE)), RS.getString(RS.getColumnIndex(Memo.SCHE_S_TIME)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_HOUR)), RS.getInt(RS.getColumnIndex(Memo.SCHE_E_MINUTE)), RS.getString(RS.getColumnIndex(Memo.SCHE_E_TIME)));
                            z++;
                            if (z == 1) {
                                rViews.setTextViewText(R.id.TV_MEMO1, ((Object) sTime) + " " + RS.getString(RS.getColumnIndex(Memo.MEMO)));
                                rViews.setInt(R.id.IV_CATE1, "setImageResource", AppUtil.getCateResId15(RS.getInt(RS.getColumnIndex(Category.ICON_ID))));
                                rViews.setViewVisibility(R.id.IV_CATE1, 0);
                            } else if (z == 2) {
                                rViews.setTextViewText(R.id.TV_MEMO2, ((Object) sTime) + " " + RS.getString(RS.getColumnIndex(Memo.MEMO)));
                                rViews.setInt(R.id.IV_CATE2, "setImageResource", AppUtil.getCateResId15(RS.getInt(RS.getColumnIndex(Category.ICON_ID))));
                                rViews.setViewVisibility(R.id.IV_CATE2, 0);
                            } else {
                                rViews.setTextViewText(R.id.TV_MEMO3, ((Object) sTime) + " " + RS.getString(RS.getColumnIndex(Memo.MEMO)));
                                rViews.setInt(R.id.IV_CATE3, "setImageResource", AppUtil.getCateResId15(RS.getInt(RS.getColumnIndex(Category.ICON_ID))));
                                rViews.setViewVisibility(R.id.IV_CATE3, 0);
                            }
                            if (z == 3) {
                            }
                        }
                    }
                    RS.close();
                    if (DB != null) {
                        DB.close();
                    }
                } catch (Exception e) {
                    if (DB != null) {
                        DB.close();
                    }
                } catch (Throwable th) {
                    if (DB != null) {
                        DB.close();
                    }
                    throw th;
                }
            } else {
                rViews.setTextViewText(R.id.TV_INFO, null);
                rViews.setTextViewText(R.id.TV_MEMO1, null);
                rViews.setViewVisibility(R.id.IV_CATE1, 4);
                rViews.setTextViewText(R.id.TV_MEMO2, null);
                rViews.setViewVisibility(R.id.IV_CATE2, 4);
                rViews.setTextViewText(R.id.TV_MEMO3, null);
                rViews.setViewVisibility(R.id.IV_CATE3, 4);
            }
            prefs.setWidgetYear(WidgetCalendar.aYear);
            prefs.setWidgetMonth(WidgetCalendar.aMonth);
            prefs.setWidgetDay(WidgetCalendar.aDay);
        }

        private void dayInit(RemoteViews rViews) {
            int i = 0;
            for (Integer id : WidgetCalendar.llDayList) {
                if (i != 0) {
                    rViews.setInt(id.intValue(), "setBackgroundColor", 0);
                }
                i++;
            }
        }

        private void setColor(RemoteViews rViews) {
            Prefs prefs = new Prefs(this);
            switch (Integer.valueOf(prefs.getColor()).intValue()) {
                case 0:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_black_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_black_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_black_00);
                        break;
                    }
                case 1:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_blue_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_blue_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_blue_00);
                        break;
                    }
                case DBEngine.DB_VERSION:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_green_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_green_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_green_00);
                        break;
                    }
                case 3:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_orange_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_orange_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_orange_00);
                        break;
                    }
                case 4:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_pink_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_pink_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_pink_00);
                        break;
                    }
                case 5:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_yellow_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_yellow_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_yellow_00);
                        break;
                    }
                case 6:
                    if (Integer.valueOf(prefs.getTransparency()).intValue() != 0) {
                        if (Integer.valueOf(prefs.getTransparency()).intValue() != 1) {
                            if (Integer.valueOf(prefs.getTransparency()).intValue() != 2) {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_transparent);
                                break;
                            } else {
                                rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_gray_02);
                                break;
                            }
                        } else {
                            rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_gray_01);
                            break;
                        }
                    } else {
                        rViews.setInt(R.id.IV_BACKGROUND, "setImageResource", R.drawable.back_gray_00);
                        break;
                    }
            }
            if (Integer.valueOf(prefs.getTextColor()).intValue() == 0) {
                rViews.setInt(R.id.TV_MONTH, "setTextColor", -1);
                rViews.setInt(R.id.IV_BACK, "setImageResource", R.drawable.white_back);
                rViews.setInt(R.id.IV_NEXT, "setImageResource", R.drawable.white_next);
                rViews.setInt(R.id.TV_WEEK_01, "setTextColor", -18751);
                rViews.setInt(R.id.TV_WEEK_02, "setTextColor", -1);
                rViews.setInt(R.id.TV_WEEK_03, "setTextColor", -1);
                rViews.setInt(R.id.TV_WEEK_04, "setTextColor", -1);
                rViews.setInt(R.id.TV_WEEK_05, "setTextColor", -1);
                rViews.setInt(R.id.TV_WEEK_06, "setTextColor", -1);
                rViews.setInt(R.id.TV_WEEK_07, "setTextColor", -16711681);
                rViews.setInt(R.id.IV_HORIZON01, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON02, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON03, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON04, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON05, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON06, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON07, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON08, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON09, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON10, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.IV_HORIZON11, "setImageResource", R.drawable.horizontal_white);
                rViews.setInt(R.id.TV_VERTICAL_W01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_W02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_W03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_W04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_W05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_W06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D1_01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D1_02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D1_03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D1_04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D1_05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D1_06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D2_01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D2_02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D2_03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D2_04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D2_05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D2_06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D3_01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D3_02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D3_03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D3_04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D3_05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D3_06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D4_01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D4_02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D4_03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D4_04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D4_05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D4_06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D5_01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D5_02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D5_03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D5_04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D5_05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D5_06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D6_01, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_VERTICAL_D6_02, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D6_03, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D6_04, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D6_05, "setBackgroundColor", -2046820353);
                rViews.setInt(R.id.TV_VERTICAL_D6_06, "setBackgroundColor", 1174405119);
                rViews.setInt(R.id.TV_INFO, "setTextColor", -1);
                rViews.setInt(R.id.TV_MEMO1, "setTextColor", -1);
                rViews.setInt(R.id.TV_MEMO2, "setTextColor", -1);
                rViews.setInt(R.id.TV_MEMO3, "setTextColor", -1);
                return;
            }
            rViews.setInt(R.id.TV_MONTH, "setTextColor", -16777216);
            rViews.setInt(R.id.IV_BACK, "setImageResource", R.drawable.black_back);
            rViews.setInt(R.id.IV_NEXT, "setImageResource", R.drawable.black_next);
            rViews.setInt(R.id.TV_WEEK_01, "setTextColor", -2396013);
            rViews.setInt(R.id.TV_WEEK_02, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_WEEK_03, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_WEEK_04, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_WEEK_05, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_WEEK_06, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_WEEK_07, "setTextColor", -12490271);
            rViews.setInt(R.id.IV_HORIZON01, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON02, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON03, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON04, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON05, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON06, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON07, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON08, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON09, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON10, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.IV_HORIZON11, "setImageResource", R.drawable.horizontal_black);
            rViews.setInt(R.id.TV_VERTICAL_W01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_W02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_W03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_W04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_W05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_W06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D1_01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D1_02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D1_03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D1_04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D1_05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D1_06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D2_01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D2_02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D2_03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D2_04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D2_05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D2_06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D3_01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D3_02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D3_03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D3_04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D3_05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D3_06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D4_01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D4_02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D4_03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D4_04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D4_05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D4_06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D5_01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D5_02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D5_03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D5_04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D5_05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D5_06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D6_01, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_VERTICAL_D6_02, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D6_03, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D6_04, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D6_05, "setBackgroundColor", -2063597568);
            rViews.setInt(R.id.TV_VERTICAL_D6_06, "setBackgroundColor", 1157627904);
            rViews.setInt(R.id.TV_INFO, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_MEMO1, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_MEMO2, "setTextColor", -16777216);
            rViews.setInt(R.id.TV_MEMO3, "setTextColor", -16777216);
        }
    }
}
