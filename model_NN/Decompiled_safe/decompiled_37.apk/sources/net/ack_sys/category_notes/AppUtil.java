package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.method.TextKeyListener;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.ack_sys.category_notes.WidgetCalendar;

public class AppUtil {
    public static final String ACTION_BACKCLICK = "net.ack_sys.category_notes.ACTION_BACKCLICK";
    public static final String ACTION_CHANGE_COLOR = "net.ack_sys.category_notes.ACTION_CHANGE_COLOR";
    public static final String ACTION_DAYCLICK = "net.ack_sys.category_notes.ACTION_DAYCLICK";
    public static final String ACTION_NEXTCLICK = "net.ack_sys.category_notes.ACTION_NEXTCLICK";
    public static final String ACTION_TICK = "net.ack_sys.category_notes.ACTION_TICK";
    public static final String ACTION_TODAYCLICK = "net.ack_sys.category_notes.ACTION_TODAYCLICK";
    public static final String ACTION_UPDATE = "net.ack_sys.category_notes.ACTION_UPDATE";

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<net.ack_sys.category_notes.Category> getCategory(android.content.Context r10, boolean r11) {
        /*
            r9 = 1
            java.util.ArrayList r3 = new java.util.ArrayList
            r7 = 45
            r3.<init>(r7)
            r4 = 0
            net.ack_sys.category_notes.DBEngine r1 = new net.ack_sys.category_notes.DBEngine
            r1.<init>(r10)
            r0 = 0
            r2 = 0
            android.database.sqlite.SQLiteDatabase r0 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r6.<init>()     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = " SELECT * FROM CATEGORY"
            r6.append(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            if (r11 == 0) goto L_0x0044
            java.lang.String r7 = " WHERE enable = 0"
            r6.append(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
        L_0x0025:
            java.lang.String r7 = " ORDER BY sort"
            r6.append(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = r6.toString()     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r8 = 0
            android.database.Cursor r2 = r0.rawQuery(r7, r8)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r5 = r4
        L_0x0034:
            boolean r7 = r2.moveToNext()     // Catch:{ Exception -> 0x00c6, all -> 0x00c3 }
            if (r7 != 0) goto L_0x0051
            r2.close()     // Catch:{ Exception -> 0x00c6, all -> 0x00c3 }
            if (r0 == 0) goto L_0x0042
            r0.close()
        L_0x0042:
            r4 = r5
        L_0x0043:
            return r3
        L_0x0044:
            java.lang.String r7 = " WHERE enable = 1"
            r6.append(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            goto L_0x0025
        L_0x004a:
            r7 = move-exception
        L_0x004b:
            if (r0 == 0) goto L_0x0043
            r0.close()
            goto L_0x0043
        L_0x0051:
            net.ack_sys.category_notes.Category r4 = new net.ack_sys.category_notes.Category     // Catch:{ Exception -> 0x00c6, all -> 0x00c3 }
            r4.<init>()     // Catch:{ Exception -> 0x00c6, all -> 0x00c3 }
            java.lang.String r7 = "id"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r4.setId(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = "title"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r4.setTitle(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = "music_name"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r4.setMusicName(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = "music_uri"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r4.setMusicUri(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = "icon_id"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r4.setIconId(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = "sort"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r4.setSort(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            java.lang.String r7 = "enable"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            if (r7 != r9) goto L_0x00ba
            r7 = r9
        L_0x00b1:
            r4.setEnable(r7)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r3.add(r4)     // Catch:{ Exception -> 0x004a, all -> 0x00bc }
            r5 = r4
            goto L_0x0034
        L_0x00ba:
            r7 = 0
            goto L_0x00b1
        L_0x00bc:
            r7 = move-exception
        L_0x00bd:
            if (r0 == 0) goto L_0x00c2
            r0.close()
        L_0x00c2:
            throw r7
        L_0x00c3:
            r7 = move-exception
            r4 = r5
            goto L_0x00bd
        L_0x00c6:
            r7 = move-exception
            r4 = r5
            goto L_0x004b
        */
        throw new UnsupportedOperationException("Method not decompiled: net.ack_sys.category_notes.AppUtil.getCategory(android.content.Context, boolean):java.util.List");
    }

    public static List<Category> getCategory(Context context) {
        return getCategory(context, false);
    }

    public static List<Tag> getTag(Context context, boolean enableOnly, boolean search) {
        boolean z;
        SQLiteDatabase DB = new DBEngine(context).getReadableDatabase();
        List<Tag> tags = new ArrayList<>(17);
        if (search) {
            Tag currentTag = new Tag();
            currentTag.setId(0);
            currentTag.setTitle("指定しない");
            currentTag.setSort(0);
            currentTag.setEnable(true);
            tags.add(currentTag);
        }
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT * FROM TAG");
        if (enableOnly) {
            sql.append(" WHERE enable = 1");
        }
        sql.append(" ORDER BY sort");
        Cursor RS = DB.rawQuery(sql.toString(), null);
        while (RS.moveToNext()) {
            Tag currentTag2 = new Tag();
            currentTag2.setId(RS.getInt(RS.getColumnIndex("id")));
            currentTag2.setTitle(RS.getString(RS.getColumnIndex("title")));
            currentTag2.setSort(RS.getInt(RS.getColumnIndex("sort")));
            if (RS.getInt(RS.getColumnIndex("enable")) == 1) {
                z = true;
            } else {
                z = false;
            }
            currentTag2.setEnable(z);
            tags.add(currentTag2);
        }
        RS.close();
        DB.close();
        return tags;
    }

    public static List<Tag> getTag(Context context, boolean enableOnly) {
        return getTag(context, enableOnly, false);
    }

    public static List<Tag> getTag(Context context) {
        return getTag(context, true, false);
    }

    public static int getDefaultTagId(Context context) {
        SQLiteDatabase DB = new DBEngine(context).getReadableDatabase();
        boolean errFlg = false;
        int tagId = 1;
        Cursor RS = DB.rawQuery(" SELECT * FROM TAG" + " WHERE enable = 1" + " ORDER BY sort", null);
        if (!RS.moveToFirst()) {
            errFlg = true;
        } else {
            tagId = RS.getInt(RS.getColumnIndex("id"));
        }
        RS.close();
        if (errFlg) {
            Cursor RS2 = DB.rawQuery(" SELECT * FROM TAG" + " WHERE sort = 1", null);
            if (RS2.moveToFirst()) {
                tagId = RS2.getInt(RS2.getColumnIndex("id"));
            }
            RS2.close();
        }
        DB.close();
        return tagId;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0170 A[SYNTHETIC, Splitter:B:40:0x0170] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<net.ack_sys.category_notes.Memo> getMemo(android.content.Context r12, int r13, java.lang.String r14, int r15) {
        /*
            r11 = 1
            r10 = 0
            net.ack_sys.category_notes.Prefs r6 = new net.ack_sys.category_notes.Prefs
            r6.<init>(r12)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r3 = 0
            net.ack_sys.category_notes.DBEngine r1 = new net.ack_sys.category_notes.DBEngine
            r1.<init>(r12)
            r0 = 0
            r2 = 0
            android.database.sqlite.SQLiteDatabase r0 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r7.<init>()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " SELECT"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.recno,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.cate_id,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " C.icon_id,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.tag_id,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.memo,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.photo_name,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.photo_uri,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_s_year,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_s_month,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_s_day,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_s_hour,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_s_minute,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_s_time,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_e_year,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_e_month,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_e_day,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_e_hour,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_e_minute,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_e_time,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.sche_location,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.create_time,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " M.update_time,"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " A.alarm_year"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " FROM MEMO M"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " INNER JOIN CATEGORY C ON"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "       M.cate_id = C.id"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " LEFT JOIN ALARM A ON"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "      M.recno = A.memo_no"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " AND  A.snooze = 0"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = " WHERE 0 = 0"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            if (r13 == 0) goto L_0x00d4
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r9 = " AND M.cate_id = "
            r8.<init>(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r9 = sql9(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
        L_0x00d4:
            java.lang.Boolean r8 = checkNull(r14)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            boolean r8 = r8.booleanValue()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            if (r8 != 0) goto L_0x00f4
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r9 = " AND M.memo LIKE "
            r8.<init>(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r9 = sqlLa(r14)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
        L_0x00f4:
            if (r15 == 0) goto L_0x0110
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r9 = " AND M.tag_id = "
            r8.<init>(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r15)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r9 = sql9(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
        L_0x0110:
            java.lang.String r8 = " ORDER BY"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r6.getSortOrder()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r8.intValue()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            switch(r8) {
                case 0: goto L_0x013e;
                case 1: goto L_0x014b;
                case 2: goto L_0x0158;
                case 3: goto L_0x015e;
                case 4: goto L_0x0164;
                case 5: goto L_0x016a;
                default: goto L_0x0124;
            }     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
        L_0x0124:
            java.lang.String r8 = r7.toString()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r9 = 0
            android.database.Cursor r2 = r0.rawQuery(r8, r9)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r4 = r3
        L_0x012e:
            boolean r8 = r2.moveToNext()     // Catch:{ Exception -> 0x02d7, all -> 0x02d3 }
            if (r8 != 0) goto L_0x0170
            r2.close()     // Catch:{ Exception -> 0x02d7, all -> 0x02d3 }
            if (r0 == 0) goto L_0x013c
            r0.close()
        L_0x013c:
            r3 = r4
        L_0x013d:
            return r5
        L_0x013e:
            java.lang.String r8 = " M.create_time DESC"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            goto L_0x0124
        L_0x0144:
            r8 = move-exception
        L_0x0145:
            if (r0 == 0) goto L_0x013d
            r0.close()
            goto L_0x013d
        L_0x014b:
            java.lang.String r8 = " M.update_time DESC"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            goto L_0x0124
        L_0x0151:
            r8 = move-exception
        L_0x0152:
            if (r0 == 0) goto L_0x0157
            r0.close()
        L_0x0157:
            throw r8
        L_0x0158:
            java.lang.String r8 = " M.create_time"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            goto L_0x0124
        L_0x015e:
            java.lang.String r8 = " M.update_time"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            goto L_0x0124
        L_0x0164:
            java.lang.String r8 = " M.memo"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            goto L_0x0124
        L_0x016a:
            java.lang.String r8 = " M.memo DESC"
            r7.append(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            goto L_0x0124
        L_0x0170:
            net.ack_sys.category_notes.Memo r3 = new net.ack_sys.category_notes.Memo     // Catch:{ Exception -> 0x02d7, all -> 0x02d3 }
            r3.<init>()     // Catch:{ Exception -> 0x02d7, all -> 0x02d3 }
            java.lang.String r8 = "recno"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setRecno(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "cate_id"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setCateId(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "icon_id"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setIconId(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "tag_id"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setTagId(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "memo"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setMemo(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "photo_name"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setPhotoName(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "photo_uri"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setPhotoUri(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_year"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheSYear(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_month"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheSMonth(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_day"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheSDay(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_hour"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheSHour(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_minute"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheSMinute(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_time"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheSTime(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_e_year"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheEYear(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_e_month"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheEMonth(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_e_day"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheEDay(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_e_hour"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheEHour(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_e_minute"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheEMinute(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_e_time"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheETime(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_location"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setScheLocation(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "create_time"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setCreateTime(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "update_time"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r3.setUpdateTime(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "photo_uri"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = r2.getString(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r8.length()     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            if (r8 == 0) goto L_0x02cd
            r8 = r11
        L_0x02a4:
            r3.setPhoto(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "sche_s_year"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            if (r8 == 0) goto L_0x02cf
            r8 = r11
        L_0x02b4:
            r3.setSchedule(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            java.lang.String r8 = "alarm_year"
            int r8 = r2.getColumnIndex(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            int r8 = r2.getInt(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            if (r8 == 0) goto L_0x02d1
            r8 = r11
        L_0x02c4:
            r3.setAlarm(r8)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r5.add(r3)     // Catch:{ Exception -> 0x0144, all -> 0x0151 }
            r4 = r3
            goto L_0x012e
        L_0x02cd:
            r8 = r10
            goto L_0x02a4
        L_0x02cf:
            r8 = r10
            goto L_0x02b4
        L_0x02d1:
            r8 = r10
            goto L_0x02c4
        L_0x02d3:
            r8 = move-exception
            r3 = r4
            goto L_0x0152
        L_0x02d7:
            r8 = move-exception
            r3 = r4
            goto L_0x0145
        */
        throw new UnsupportedOperationException("Method not decompiled: net.ack_sys.category_notes.AppUtil.getMemo(android.content.Context, int, java.lang.String, int):java.util.List");
    }

    public static List<Memo> getMemo(Context context, int cateId) {
        return getMemo(context, cateId, "", 0);
    }

    public static List<Memo> getMemo(Context context, String word, int tagId) {
        return getMemo(context, 0, word, tagId);
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x02b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<net.ack_sys.category_notes.Memo> getScheMemo(android.content.Context r11, int r12, int r13, int r14) {
        /*
            r10 = 1
            r9 = 0
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            r3 = 0
            net.ack_sys.category_notes.DBEngine r1 = new net.ack_sys.category_notes.DBEngine
            r1.<init>(r11)
            r0 = 0
            r2 = 0
            android.database.sqlite.SQLiteDatabase r0 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r6.<init>()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " SELECT"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.recno,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.cate_id,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " C.icon_id,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.tag_id,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.memo,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.photo_name,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.photo_uri,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_year,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_month,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_day,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_hour,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_minute,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_time,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_e_year,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_e_month,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_e_day,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_e_hour,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_e_minute,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_e_time,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_location,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.create_time,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.update_time,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " A.alarm_year,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " S.day_flg "
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " FROM SCHEDULE S"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " INNER JOIN MEMO M ON"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "       S.memo_no = M.recno"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " INNER JOIN CATEGORY C ON"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "       M.cate_id = C.id"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " LEFT JOIN ALARM A ON"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "       S.memo_no = A.memo_no"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " AND   A.snooze = 0"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r8 = " WHERE S.sche_year = "
            r7.<init>(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r12)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r8 = sql9(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r8 = " AND   S.sche_month = "
            r7.<init>(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r8 = sql9(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r8 = " AND   S.sche_day = "
            r7.<init>(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r14)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r8 = sql9(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " ORDER BY"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " S.day_flg DESC,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.sche_s_time,"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = " M.recno"
            r6.append(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r6.toString()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r8 = 0
            android.database.Cursor r2 = r0.rawQuery(r7, r8)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r4 = r3
        L_0x0129:
            boolean r7 = r2.moveToNext()     // Catch:{ Exception -> 0x02bb, all -> 0x02b8 }
            if (r7 != 0) goto L_0x0139
            r2.close()     // Catch:{ Exception -> 0x02bb, all -> 0x02b8 }
            if (r0 == 0) goto L_0x0137
            r0.close()
        L_0x0137:
            r3 = r4
        L_0x0138:
            return r5
        L_0x0139:
            net.ack_sys.category_notes.Memo r3 = new net.ack_sys.category_notes.Memo     // Catch:{ Exception -> 0x02bb, all -> 0x02b8 }
            r3.<init>()     // Catch:{ Exception -> 0x02bb, all -> 0x02b8 }
            java.lang.String r7 = "recno"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setRecno(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "cate_id"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setCateId(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "icon_id"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setIconId(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "tag_id"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setTagId(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "memo"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setMemo(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "photo_name"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setPhotoName(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "photo_uri"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setPhotoUri(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_year"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheSYear(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_month"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheSMonth(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_day"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheSDay(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_hour"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheSHour(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_minute"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheSMinute(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_time"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheSTime(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_e_year"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheEYear(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_e_month"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheEMonth(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_e_day"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheEDay(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_e_hour"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheEHour(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_e_minute"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheEMinute(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_e_time"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheETime(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_location"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setScheLocation(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "create_time"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setCreateTime(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "update_time"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setUpdateTime(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "photo_uri"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = r2.getString(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r7.length()     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            if (r7 == 0) goto L_0x02a3
            r7 = r10
        L_0x026d:
            r3.setPhoto(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "sche_s_year"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            if (r7 == 0) goto L_0x02a5
            r7 = r10
        L_0x027d:
            r3.setSchedule(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "alarm_year"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            if (r7 == 0) goto L_0x02a7
            r7 = r10
        L_0x028d:
            r3.setAlarm(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            java.lang.String r7 = "day_flg"
            int r7 = r2.getColumnIndex(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            int r7 = r2.getInt(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r3.setDayFlg(r7)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r5.add(r3)     // Catch:{ Exception -> 0x02a9, all -> 0x02b1 }
            r4 = r3
            goto L_0x0129
        L_0x02a3:
            r7 = r9
            goto L_0x026d
        L_0x02a5:
            r7 = r9
            goto L_0x027d
        L_0x02a7:
            r7 = r9
            goto L_0x028d
        L_0x02a9:
            r7 = move-exception
        L_0x02aa:
            if (r0 == 0) goto L_0x0138
            r0.close()
            goto L_0x0138
        L_0x02b1:
            r7 = move-exception
        L_0x02b2:
            if (r0 == 0) goto L_0x02b7
            r0.close()
        L_0x02b7:
            throw r7
        L_0x02b8:
            r7 = move-exception
            r3 = r4
            goto L_0x02b2
        L_0x02bb:
            r7 = move-exception
            r3 = r4
            goto L_0x02aa
        */
        throw new UnsupportedOperationException("Method not decompiled: net.ack_sys.category_notes.AppUtil.getScheMemo(android.content.Context, int, int, int):java.util.List");
    }

    public static void deleteMemo(Context context, int recno) {
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        try {
            DB.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append(" DELETE FROM SCHEDULE");
            sql.append(" WHERE memo_no = " + sql9(Integer.valueOf(recno)));
            DB.execSQL(sql.toString());
            StringBuilder sql2 = new StringBuilder();
            sql2.append(" SELECT recno FROM ALARM");
            sql2.append(" WHERE memo_no = " + sql9(Integer.valueOf(recno)));
            Cursor RS = DB.rawQuery(sql2.toString(), null);
            while (RS.moveToNext()) {
                cancelAlarmManager(context, RS.getInt(0));
                StringBuilder sql3 = new StringBuilder();
                sql3.append(" DELETE FROM ALARM");
                sql3.append(" WHERE recno = " + sql9(Integer.valueOf(RS.getInt(0))));
                DB.execSQL(sql3.toString());
            }
            RS.close();
            StringBuilder sql4 = new StringBuilder();
            sql4.append(" DELETE FROM MEMO");
            sql4.append(" WHERE recno = " + sql9(Integer.valueOf(recno)));
            DB.execSQL(sql4.toString());
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            sendWidgetAction(context, ACTION_UPDATE);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void deleteSchedule(Context context, int recno) {
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        try {
            DB.beginTransaction();
            String dateTime = getDateTime();
            ContentValues values = new ContentValues();
            values.put(Memo.SCHE_S_YEAR, (Integer) 0);
            values.put(Memo.SCHE_S_MONTH, (Integer) 0);
            values.put(Memo.SCHE_S_DAY, (Integer) 0);
            values.put(Memo.SCHE_S_HOUR, (Integer) 0);
            values.put(Memo.SCHE_S_MINUTE, (Integer) 0);
            values.put(Memo.SCHE_S_TIME, "");
            values.put(Memo.SCHE_E_YEAR, (Integer) 0);
            values.put(Memo.SCHE_E_MONTH, (Integer) 0);
            values.put(Memo.SCHE_E_DAY, (Integer) 0);
            values.put(Memo.SCHE_E_HOUR, (Integer) 0);
            values.put(Memo.SCHE_E_MINUTE, (Integer) 0);
            values.put(Memo.SCHE_E_TIME, "");
            values.put(Memo.SCHE_LOCATION, "");
            values.put(Memo.UPDATE_TIME, dateTime);
            DB.update("MEMO", values, "recno = ?", new String[]{String.valueOf(recno)});
            StringBuilder sql = new StringBuilder();
            sql.append(" DELETE FROM SCHEDULE");
            sql.append(" WHERE memo_no = " + sql9(Integer.valueOf(recno)));
            DB.execSQL(sql.toString());
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            sendWidgetAction(context, ACTION_UPDATE);
        }
    }

    public static void setEnableAlarm(Context context, int recno, boolean enable) {
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        try {
            DB.beginTransaction();
            StringBuilder sql = new StringBuilder();
            sql.append(" UPDATE ALARM SET enable = " + sql9(Integer.valueOf(enable ? 1 : 0)));
            sql.append(" WHERE recno = " + sql9(Integer.valueOf(recno)));
            DB.execSQL(sql.toString());
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
        }
    }

    /* JADX INFO: Multiple debug info for r0v10 java.util.Calendar: [D('i' int), D('alarmCal' java.util.Calendar)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void deleteAlarm(Context context, int recno, boolean nextFlg) {
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        int[] week = new int[7];
        try {
            DB.beginTransaction();
            cancelAlarmManager(context, recno);
            if (nextFlg) {
                StringBuilder sql = new StringBuilder();
                sql.append(" SELECT * FROM ALARM");
                sql.append(" WHERE recno = " + sql9(Integer.valueOf(recno)));
                sql.append(" AND   alarm_loop = 1");
                Cursor RS = DB.rawQuery(sql.toString(), null);
                if (RS.moveToFirst()) {
                    int hour = RS.getInt(RS.getColumnIndex(Alarm.ALARM_HOUR));
                    int minute = RS.getInt(RS.getColumnIndex(Alarm.ALARM_MINUTE));
                    for (int i = 1; i <= 7; i++) {
                        week[i - 1] = RS.getInt(RS.getColumnIndex("alarm_week" + String.valueOf(i)));
                    }
                    Calendar alarmCal = getNextDay(week, hour, minute);
                    ContentValues values = new ContentValues();
                    values.put("memo_no", Integer.valueOf(RS.getInt(RS.getColumnIndex("memo_no"))));
                    values.put(Alarm.ALARM_YEAR, Integer.valueOf(alarmCal.get(1)));
                    values.put(Alarm.ALARM_MONTH, Integer.valueOf(alarmCal.get(2)));
                    values.put(Alarm.ALARM_DAY, Integer.valueOf(alarmCal.get(5)));
                    values.put(Alarm.ALARM_HOUR, Integer.valueOf(hour));
                    values.put(Alarm.ALARM_MINUTE, Integer.valueOf(minute));
                    values.put(Alarm.ALARM_LOOP, (Integer) 1);
                    for (int i2 = 1; i2 <= 7; i2++) {
                        values.put("alarm_week" + String.valueOf(i2), Integer.valueOf(week[i2 - 1]));
                    }
                    values.put("enable", Integer.valueOf(RS.getInt(RS.getColumnIndex("enable"))));
                    values.put(Alarm.SNOOZE, (Integer) 0);
                    DB.insert("ALARM", null, values);
                    StringBuilder sql2 = new StringBuilder();
                    sql2.append(" SELECT last_insert_rowid()");
                    Cursor RS2 = DB.rawQuery(sql2.toString(), null);
                    if (RS2.moveToFirst()) {
                        setAlarmManager(context, RS2.getInt(0), alarmCal.get(1), alarmCal.get(2), alarmCal.get(5), hour, minute);
                    }
                    RS2.close();
                    StringBuilder sb = sql2;
                }
                RS.close();
            }
            StringBuilder sql3 = new StringBuilder();
            sql3.append(" DELETE FROM ALARM");
            sql3.append(" WHERE recno = " + sql9(Integer.valueOf(recno)));
            DB.execSQL(sql3.toString());
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
        }
    }

    public static void deleteAlarm(Context context, int recno) {
        deleteAlarm(context, recno, false);
    }

    public static String getCateTitle(Context context, int id) {
        SQLiteDatabase DB = new DBEngine(context).getReadableDatabase();
        String title = "";
        Cursor RS = DB.rawQuery(" SELECT title FROM CATEGORY" + " WHERE id = ?", new String[]{String.valueOf(id)});
        if (RS.moveToFirst()) {
            title = RS.getString(0);
        }
        RS.close();
        DB.close();
        return title;
    }

    public static int getMemoRecno(Context context, int alarmRecno) {
        SQLiteDatabase DB = new DBEngine(context).getReadableDatabase();
        int memoRecno = 0;
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT memo_no FROM ALARM");
        sql.append(" WHERE recno = " + sql9(Integer.valueOf(alarmRecno)));
        Cursor RS = DB.rawQuery(sql.toString(), null);
        if (RS.moveToNext()) {
            memoRecno = RS.getInt(0);
        }
        RS.close();
        DB.close();
        return memoRecno;
    }

    /* JADX WARNING: Removed duplicated region for block: B:63:0x02d7  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x02e4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<net.ack_sys.category_notes.Alarm> getAlarm(android.content.Context r13) {
        /*
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            r3 = 0
            net.ack_sys.category_notes.DBEngine r1 = new net.ack_sys.category_notes.DBEngine
            r1.<init>(r13)
            r0 = 0
            r2 = 0
            r4 = 7
            int[] r7 = new int[r4]
            android.database.sqlite.SQLiteDatabase r8 = r1.getReadableDatabase()     // Catch:{ Exception -> 0x02d1, all -> 0x02dc }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            r0.<init>()     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " SELECT"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.recno,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.memo_no,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " M.cate_id,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " C.icon_id,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " M.memo,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_year,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_month,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_day,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_hour,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_minute,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_loop,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week1,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week2,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week3,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week4,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week5,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week6,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_week7,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.enable,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.snooze"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " FROM ALARM A"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " INNER JOIN MEMO M ON"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = "       A.memo_no = M.recno"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " INNER JOIN CATEGORY C ON"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = "       M.cate_id = C.id"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " WHERE A.snooze = 0"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " ORDER BY"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_year,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_month,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_day,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_hour,"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r1 = " A.alarm_minute"
            r0.append(r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            r1 = 0
            android.database.Cursor r9 = r8.rawQuery(r0, r1)     // Catch:{ Exception -> 0x02fa, all -> 0x02e8 }
            r0 = r3
        L_0x00c8:
            boolean r1 = r9.moveToNext()     // Catch:{ Exception -> 0x02ff, all -> 0x02ee }
            if (r1 != 0) goto L_0x00da
            r9.close()     // Catch:{ Exception -> 0x02ff, all -> 0x02ee }
            if (r8 == 0) goto L_0x00d6
            r8.close()
        L_0x00d6:
            r13 = r8
            r1 = r0
            r0 = r9
        L_0x00d9:
            return r10
        L_0x00da:
            net.ack_sys.category_notes.Alarm r11 = new net.ack_sys.category_notes.Alarm     // Catch:{ Exception -> 0x02ff, all -> 0x02ee }
            r11.<init>()     // Catch:{ Exception -> 0x02ff, all -> 0x02ee }
            java.lang.String r0 = "recno"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setRecno(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "memo_no"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setMemoNo(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "cate_id"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setCateId(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "icon_id"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setIconId(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "memo"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = r9.getString(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setMemo(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_year"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setAlarmYaer(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_month"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setAlarmMonth(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_day"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setAlarmDay(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_hour"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setAlarmHour(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_minute"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setAlarmMinute(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_loop"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02b3
            r0 = 1
        L_0x016f:
            r11.setAlarmLoop(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week1"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02b6
            r0 = 1
        L_0x0180:
            r11.setAlarmWeek1(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week2"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02b9
            r0 = 1
        L_0x0191:
            r11.setAlarmWeek2(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week3"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02bc
            r0 = 1
        L_0x01a2:
            r11.setAlarmWeek3(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week4"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02bf
            r0 = 1
        L_0x01b3:
            r11.setAlarmWeek4(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week5"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02c2
            r0 = 1
        L_0x01c4:
            r11.setAlarmWeek5(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week6"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02c5
            r0 = 1
        L_0x01d5:
            r11.setAlarmWeek6(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "alarm_week7"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02c8
            r0 = 1
        L_0x01e6:
            r11.setAlarmWeek7(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "enable"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02cb
            r0 = 1
        L_0x01f7:
            r11.setEnable(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r0 = "snooze"
            int r0 = r9.getColumnIndex(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r1 = 1
            if (r0 != r1) goto L_0x02ce
            r0 = 1
        L_0x0208:
            r11.setSnooze(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 0
            java.lang.String r1 = "alarm_week1"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 1
            java.lang.String r1 = "alarm_week2"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 2
            java.lang.String r1 = "alarm_week3"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 3
            java.lang.String r1 = "alarm_week4"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 4
            java.lang.String r1 = "alarm_week5"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 5
            java.lang.String r1 = "alarm_week6"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = 6
            java.lang.String r1 = "alarm_week7"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r7[r0] = r1     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            android.content.res.Resources r0 = r13.getResources()     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r1 = "alarm_year"
            int r1 = r9.getColumnIndex(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r1 = r9.getInt(r1)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r2 = "alarm_month"
            int r2 = r9.getColumnIndex(r2)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r2 = r9.getInt(r2)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r3 = "alarm_day"
            int r3 = r9.getColumnIndex(r3)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r3 = r9.getInt(r3)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r4 = "alarm_hour"
            int r4 = r9.getColumnIndex(r4)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r4 = r9.getInt(r4)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r5 = "alarm_minute"
            int r5 = r9.getColumnIndex(r5)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r5 = r9.getInt(r5)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.String r6 = "alarm_loop"
            int r6 = r9.getColumnIndex(r6)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            int r6 = r9.getInt(r6)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            java.lang.CharSequence r0 = getDateTimeStr(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r11.setAlarmDateTime(r0)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r10.add(r11)     // Catch:{ Exception -> 0x0304, all -> 0x02f4 }
            r0 = r11
            goto L_0x00c8
        L_0x02b3:
            r0 = 0
            goto L_0x016f
        L_0x02b6:
            r0 = 0
            goto L_0x0180
        L_0x02b9:
            r0 = 0
            goto L_0x0191
        L_0x02bc:
            r0 = 0
            goto L_0x01a2
        L_0x02bf:
            r0 = 0
            goto L_0x01b3
        L_0x02c2:
            r0 = 0
            goto L_0x01c4
        L_0x02c5:
            r0 = 0
            goto L_0x01d5
        L_0x02c8:
            r0 = 0
            goto L_0x01e6
        L_0x02cb:
            r0 = 0
            goto L_0x01f7
        L_0x02ce:
            r0 = 0
            goto L_0x0208
        L_0x02d1:
            r13 = move-exception
            r13 = r0
            r1 = r3
            r0 = r2
        L_0x02d5:
            if (r13 == 0) goto L_0x00d9
            r13.close()
            goto L_0x00d9
        L_0x02dc:
            r13 = move-exception
            r1 = r3
            r12 = r2
            r2 = r13
            r13 = r0
            r0 = r12
        L_0x02e2:
            if (r13 == 0) goto L_0x02e7
            r13.close()
        L_0x02e7:
            throw r2
        L_0x02e8:
            r13 = move-exception
            r0 = r2
            r1 = r3
            r2 = r13
            r13 = r8
            goto L_0x02e2
        L_0x02ee:
            r13 = move-exception
            r2 = r13
            r1 = r0
            r13 = r8
            r0 = r9
            goto L_0x02e2
        L_0x02f4:
            r13 = move-exception
            r2 = r13
            r0 = r9
            r1 = r11
            r13 = r8
            goto L_0x02e2
        L_0x02fa:
            r13 = move-exception
            r0 = r2
            r13 = r8
            r1 = r3
            goto L_0x02d5
        L_0x02ff:
            r13 = move-exception
            r13 = r8
            r1 = r0
            r0 = r9
            goto L_0x02d5
        L_0x0304:
            r13 = move-exception
            r0 = r9
            r13 = r8
            r1 = r11
            goto L_0x02d5
        */
        throw new UnsupportedOperationException("Method not decompiled: net.ack_sys.category_notes.AppUtil.getAlarm(android.content.Context):java.util.List");
    }

    public static int getCateResId(int id) {
        if (id == 1) {
            return R.drawable.icon_01;
        }
        if (id == 2) {
            return R.drawable.icon_02;
        }
        if (id == 3) {
            return R.drawable.icon_03;
        }
        if (id == 4) {
            return R.drawable.icon_04;
        }
        if (id == 5) {
            return R.drawable.icon_05;
        }
        if (id == 6) {
            return R.drawable.icon_06;
        }
        if (id == 7) {
            return R.drawable.icon_07;
        }
        if (id == 8) {
            return R.drawable.icon_08;
        }
        if (id == 9) {
            return R.drawable.icon_09;
        }
        if (id == 10) {
            return R.drawable.icon_10;
        }
        if (id == 11) {
            return R.drawable.icon_11;
        }
        if (id == 12) {
            return R.drawable.icon_12;
        }
        if (id == 13) {
            return R.drawable.icon_13;
        }
        if (id == 14) {
            return R.drawable.icon_14;
        }
        if (id == 15) {
            return R.drawable.icon_15;
        }
        if (id == 16) {
            return R.drawable.icon_16;
        }
        if (id == 17) {
            return R.drawable.icon_17;
        }
        if (id == 18) {
            return R.drawable.icon_18;
        }
        if (id == 19) {
            return R.drawable.icon_19;
        }
        if (id == 20) {
            return R.drawable.icon_20;
        }
        if (id == 21) {
            return R.drawable.icon_21;
        }
        if (id == 22) {
            return R.drawable.icon_22;
        }
        if (id == 23) {
            return R.drawable.icon_23;
        }
        if (id == 24) {
            return R.drawable.icon_24;
        }
        if (id == 25) {
            return R.drawable.icon_25;
        }
        if (id == 26) {
            return R.drawable.icon_26;
        }
        if (id == 27) {
            return R.drawable.icon_27;
        }
        if (id == 28) {
            return R.drawable.icon_28;
        }
        if (id == 29) {
            return R.drawable.icon_29;
        }
        if (id == 30) {
            return R.drawable.icon_30;
        }
        if (id == 31) {
            return R.drawable.icon_31;
        }
        if (id == 32) {
            return R.drawable.icon_32;
        }
        if (id == 33) {
            return R.drawable.icon_33;
        }
        if (id == 34) {
            return R.drawable.icon_34;
        }
        if (id == 35) {
            return R.drawable.icon_35;
        }
        if (id == 36) {
            return R.drawable.icon_36;
        }
        if (id == 37) {
            return R.drawable.icon_37;
        }
        if (id == 38) {
            return R.drawable.icon_38;
        }
        if (id == 39) {
            return R.drawable.icon_39;
        }
        if (id == 40) {
            return R.drawable.icon_40;
        }
        if (id == 41) {
            return R.drawable.icon_41;
        }
        if (id == 42) {
            return R.drawable.icon_42;
        }
        if (id == 43) {
            return R.drawable.icon_43;
        }
        if (id == 44) {
            return R.drawable.icon_44;
        }
        if (id == 45) {
            return R.drawable.icon_45;
        }
        if (id == 46) {
            return R.drawable.icon_46;
        }
        if (id == 47) {
            return R.drawable.icon_47;
        }
        if (id == 48) {
            return R.drawable.icon_48;
        }
        if (id == 49) {
            return R.drawable.icon_49;
        }
        if (id == 50) {
            return R.drawable.icon_50;
        }
        if (id == 51) {
            return R.drawable.icon_51;
        }
        if (id == 52) {
            return R.drawable.icon_52;
        }
        if (id == 53) {
            return R.drawable.icon_53;
        }
        if (id == 54) {
            return R.drawable.icon_54;
        }
        if (id == 55) {
            return R.drawable.icon_55;
        }
        if (id == 56) {
            return R.drawable.icon_56;
        }
        if (id == 57) {
            return R.drawable.icon_57;
        }
        if (id == 58) {
            return R.drawable.icon_58;
        }
        if (id == 59) {
            return R.drawable.icon_59;
        }
        if (id == 60) {
            return R.drawable.icon_60;
        }
        if (id == 61) {
            return R.drawable.icon_61;
        }
        if (id == 62) {
            return R.drawable.icon_62;
        }
        if (id == 63) {
            return R.drawable.icon_63;
        }
        if (id == 64) {
            return R.drawable.icon_64;
        }
        if (id == 65) {
            return R.drawable.icon_65;
        }
        if (id == 66) {
            return R.drawable.icon_66;
        }
        if (id == 67) {
            return R.drawable.icon_67;
        }
        if (id == 68) {
            return R.drawable.icon_68;
        }
        if (id == 69) {
            return R.drawable.icon_69;
        }
        if (id == 70) {
            return R.drawable.icon_70;
        }
        if (id == 71) {
            return R.drawable.icon_71;
        }
        if (id == 72) {
            return R.drawable.icon_72;
        }
        if (id == 73) {
            return R.drawable.icon_73;
        }
        if (id == 74) {
            return R.drawable.icon_74;
        }
        if (id == 75) {
            return R.drawable.icon_75;
        }
        if (id == 76) {
            return R.drawable.icon_76;
        }
        if (id == 77) {
            return R.drawable.icon_77;
        }
        if (id == 78) {
            return R.drawable.icon_78;
        }
        if (id == 79) {
            return R.drawable.icon_79;
        }
        return R.drawable.icon_80;
    }

    public static int getCateResId15(int id) {
        if (id == 1) {
            return R.drawable.icon15_01;
        }
        if (id == 2) {
            return R.drawable.icon15_02;
        }
        if (id == 3) {
            return R.drawable.icon15_03;
        }
        if (id == 4) {
            return R.drawable.icon15_04;
        }
        if (id == 5) {
            return R.drawable.icon15_05;
        }
        if (id == 6) {
            return R.drawable.icon15_06;
        }
        if (id == 7) {
            return R.drawable.icon15_07;
        }
        if (id == 8) {
            return R.drawable.icon15_08;
        }
        if (id == 9) {
            return R.drawable.icon15_09;
        }
        if (id == 10) {
            return R.drawable.icon15_10;
        }
        if (id == 11) {
            return R.drawable.icon15_11;
        }
        if (id == 12) {
            return R.drawable.icon15_12;
        }
        if (id == 13) {
            return R.drawable.icon15_13;
        }
        if (id == 14) {
            return R.drawable.icon15_14;
        }
        if (id == 15) {
            return R.drawable.icon15_15;
        }
        if (id == 16) {
            return R.drawable.icon15_16;
        }
        if (id == 17) {
            return R.drawable.icon15_17;
        }
        if (id == 18) {
            return R.drawable.icon15_18;
        }
        if (id == 19) {
            return R.drawable.icon15_19;
        }
        if (id == 20) {
            return R.drawable.icon15_20;
        }
        if (id == 21) {
            return R.drawable.icon15_21;
        }
        if (id == 22) {
            return R.drawable.icon15_22;
        }
        if (id == 23) {
            return R.drawable.icon15_23;
        }
        if (id == 24) {
            return R.drawable.icon15_24;
        }
        if (id == 25) {
            return R.drawable.icon15_25;
        }
        if (id == 26) {
            return R.drawable.icon15_26;
        }
        if (id == 27) {
            return R.drawable.icon15_27;
        }
        if (id == 28) {
            return R.drawable.icon15_28;
        }
        if (id == 29) {
            return R.drawable.icon15_29;
        }
        if (id == 30) {
            return R.drawable.icon15_30;
        }
        if (id == 31) {
            return R.drawable.icon15_31;
        }
        if (id == 32) {
            return R.drawable.icon15_32;
        }
        if (id == 33) {
            return R.drawable.icon15_33;
        }
        if (id == 34) {
            return R.drawable.icon15_34;
        }
        if (id == 35) {
            return R.drawable.icon15_35;
        }
        if (id == 36) {
            return R.drawable.icon15_36;
        }
        if (id == 37) {
            return R.drawable.icon15_37;
        }
        if (id == 38) {
            return R.drawable.icon15_38;
        }
        if (id == 39) {
            return R.drawable.icon15_39;
        }
        if (id == 40) {
            return R.drawable.icon15_40;
        }
        if (id == 41) {
            return R.drawable.icon15_41;
        }
        if (id == 42) {
            return R.drawable.icon15_42;
        }
        if (id == 43) {
            return R.drawable.icon15_43;
        }
        if (id == 44) {
            return R.drawable.icon15_44;
        }
        if (id == 45) {
            return R.drawable.icon15_45;
        }
        if (id == 46) {
            return R.drawable.icon15_46;
        }
        if (id == 47) {
            return R.drawable.icon15_47;
        }
        if (id == 48) {
            return R.drawable.icon15_48;
        }
        if (id == 49) {
            return R.drawable.icon15_49;
        }
        if (id == 50) {
            return R.drawable.icon15_50;
        }
        if (id == 51) {
            return R.drawable.icon15_51;
        }
        if (id == 52) {
            return R.drawable.icon15_52;
        }
        if (id == 53) {
            return R.drawable.icon15_53;
        }
        if (id == 54) {
            return R.drawable.icon15_54;
        }
        if (id == 55) {
            return R.drawable.icon15_55;
        }
        if (id == 56) {
            return R.drawable.icon15_56;
        }
        if (id == 57) {
            return R.drawable.icon15_57;
        }
        if (id == 58) {
            return R.drawable.icon15_58;
        }
        if (id == 59) {
            return R.drawable.icon15_59;
        }
        if (id == 60) {
            return R.drawable.icon15_60;
        }
        if (id == 61) {
            return R.drawable.icon15_61;
        }
        if (id == 62) {
            return R.drawable.icon15_62;
        }
        if (id == 63) {
            return R.drawable.icon15_63;
        }
        if (id == 64) {
            return R.drawable.icon15_64;
        }
        if (id == 65) {
            return R.drawable.icon15_65;
        }
        if (id == 66) {
            return R.drawable.icon15_66;
        }
        if (id == 67) {
            return R.drawable.icon15_67;
        }
        if (id == 68) {
            return R.drawable.icon15_68;
        }
        if (id == 69) {
            return R.drawable.icon15_69;
        }
        if (id == 70) {
            return R.drawable.icon15_70;
        }
        if (id == 71) {
            return R.drawable.icon15_71;
        }
        if (id == 72) {
            return R.drawable.icon15_72;
        }
        if (id == 73) {
            return R.drawable.icon15_73;
        }
        if (id == 74) {
            return R.drawable.icon15_74;
        }
        if (id == 75) {
            return R.drawable.icon15_75;
        }
        if (id == 76) {
            return R.drawable.icon15_76;
        }
        if (id == 77) {
            return R.drawable.icon15_77;
        }
        if (id == 78) {
            return R.drawable.icon15_78;
        }
        if (id == 79) {
            return R.drawable.icon15_79;
        }
        return R.drawable.icon15_80;
    }

    public static int getTagResId(Context context, int id) {
        int pattern = Integer.valueOf(new Prefs(context).getTagIcon()).intValue();
        if (pattern == 0) {
            if (id == 1) {
                return R.drawable.tag_01;
            }
            if (id == 2) {
                return R.drawable.tag_02;
            }
            if (id == 3) {
                return R.drawable.tag_03;
            }
            if (id == 4) {
                return R.drawable.tag_04;
            }
            if (id == 5) {
                return R.drawable.tag_05;
            }
            if (id == 6) {
                return R.drawable.tag_06;
            }
            if (id == 7) {
                return R.drawable.tag_07;
            }
            if (id == 8) {
                return R.drawable.tag_08;
            }
            if (id == 9) {
                return R.drawable.tag_09;
            }
            if (id == 10) {
                return R.drawable.tag_10;
            }
            if (id == 11) {
                return R.drawable.tag_11;
            }
            if (id == 12) {
                return R.drawable.tag_12;
            }
            if (id == 13) {
                return R.drawable.tag_13;
            }
            if (id == 14) {
                return R.drawable.tag_14;
            }
            if (id == 15) {
                return R.drawable.tag_15;
            }
            return R.drawable.tag_16;
        } else if (pattern == 1) {
            if (id == 1) {
                return R.drawable.tag2_01;
            }
            if (id == 2) {
                return R.drawable.tag2_02;
            }
            if (id == 3) {
                return R.drawable.tag2_03;
            }
            if (id == 4) {
                return R.drawable.tag2_04;
            }
            if (id == 5) {
                return R.drawable.tag2_05;
            }
            if (id == 6) {
                return R.drawable.tag2_06;
            }
            if (id == 7) {
                return R.drawable.tag2_07;
            }
            if (id == 8) {
                return R.drawable.tag2_08;
            }
            if (id == 9) {
                return R.drawable.tag2_09;
            }
            if (id == 10) {
                return R.drawable.tag2_10;
            }
            if (id == 11) {
                return R.drawable.tag2_11;
            }
            if (id == 12) {
                return R.drawable.tag2_12;
            }
            if (id == 13) {
                return R.drawable.tag2_13;
            }
            if (id == 14) {
                return R.drawable.tag2_14;
            }
            if (id == 15) {
                return R.drawable.tag2_15;
            }
            return R.drawable.tag2_16;
        } else if (pattern == 2) {
            if (id == 1) {
                return R.drawable.tag3_01;
            }
            if (id == 2) {
                return R.drawable.tag3_02;
            }
            if (id == 3) {
                return R.drawable.tag3_03;
            }
            if (id == 4) {
                return R.drawable.tag3_04;
            }
            if (id == 5) {
                return R.drawable.tag3_05;
            }
            if (id == 6) {
                return R.drawable.tag3_06;
            }
            if (id == 7) {
                return R.drawable.tag3_07;
            }
            if (id == 8) {
                return R.drawable.tag3_08;
            }
            if (id == 9) {
                return R.drawable.tag3_09;
            }
            if (id == 10) {
                return R.drawable.tag3_10;
            }
            if (id == 11) {
                return R.drawable.tag3_11;
            }
            if (id == 12) {
                return R.drawable.tag3_12;
            }
            if (id == 13) {
                return R.drawable.tag3_13;
            }
            if (id == 14) {
                return R.drawable.tag3_14;
            }
            if (id == 15) {
                return R.drawable.tag3_15;
            }
            return R.drawable.tag3_16;
        } else if (pattern == 3) {
            if (id == 1) {
                return R.drawable.tag4_01;
            }
            if (id == 2) {
                return R.drawable.tag4_02;
            }
            if (id == 3) {
                return R.drawable.tag4_03;
            }
            if (id == 4) {
                return R.drawable.tag4_04;
            }
            if (id == 5) {
                return R.drawable.tag4_05;
            }
            if (id == 6) {
                return R.drawable.tag4_06;
            }
            if (id == 7) {
                return R.drawable.tag4_07;
            }
            if (id == 8) {
                return R.drawable.tag4_08;
            }
            if (id == 9) {
                return R.drawable.tag4_09;
            }
            if (id == 10) {
                return R.drawable.tag4_10;
            }
            if (id == 11) {
                return R.drawable.tag4_11;
            }
            if (id == 12) {
                return R.drawable.tag4_12;
            }
            if (id == 13) {
                return R.drawable.tag4_13;
            }
            if (id == 14) {
                return R.drawable.tag4_14;
            }
            if (id == 15) {
                return R.drawable.tag4_15;
            }
            return R.drawable.tag4_16;
        } else if (id == 1) {
            return R.drawable.tag5_01;
        } else {
            if (id == 2) {
                return R.drawable.tag5_02;
            }
            if (id == 3) {
                return R.drawable.tag5_03;
            }
            if (id == 4) {
                return R.drawable.tag5_04;
            }
            if (id == 5) {
                return R.drawable.tag5_05;
            }
            if (id == 6) {
                return R.drawable.tag5_06;
            }
            if (id == 7) {
                return R.drawable.tag5_07;
            }
            if (id == 8) {
                return R.drawable.tag5_08;
            }
            if (id == 9) {
                return R.drawable.tag5_09;
            }
            if (id == 10) {
                return R.drawable.tag5_10;
            }
            if (id == 11) {
                return R.drawable.tag5_11;
            }
            if (id == 12) {
                return R.drawable.tag5_12;
            }
            if (id == 13) {
                return R.drawable.tag5_13;
            }
            if (id == 14) {
                return R.drawable.tag5_14;
            }
            if (id == 15) {
                return R.drawable.tag5_15;
            }
            return R.drawable.tag5_16;
        }
    }

    public static int getCateTitleResId(int id) {
        if (id == 1) {
            return R.string.str_category_01;
        }
        if (id == 2) {
            return R.string.str_category_02;
        }
        if (id == 3) {
            return R.string.str_category_03;
        }
        if (id == 4) {
            return R.string.str_category_04;
        }
        if (id == 5) {
            return R.string.str_category_05;
        }
        if (id == 6) {
            return R.string.str_category_06;
        }
        if (id == 7) {
            return R.string.str_category_07;
        }
        if (id == 8) {
            return R.string.str_category_08;
        }
        if (id == 9) {
            return R.string.str_category_09;
        }
        if (id == 10) {
            return R.string.str_category_10;
        }
        if (id == 11) {
            return R.string.str_category_11;
        }
        if (id == 12) {
            return R.string.str_category_12;
        }
        if (id == 13) {
            return R.string.str_category_13;
        }
        if (id == 14) {
            return R.string.str_category_14;
        }
        if (id == 15) {
            return R.string.str_category_15;
        }
        if (id == 16) {
            return R.string.str_category_16;
        }
        if (id == 17) {
            return R.string.str_category_17;
        }
        if (id == 18) {
            return R.string.str_category_18;
        }
        if (id == 19) {
            return R.string.str_category_19;
        }
        if (id == 20) {
            return R.string.str_category_20;
        }
        if (id == 21) {
            return R.string.str_category_21;
        }
        if (id == 22) {
            return R.string.str_category_22;
        }
        if (id == 23) {
            return R.string.str_category_23;
        }
        if (id == 24) {
            return R.string.str_category_24;
        }
        if (id == 25) {
            return R.string.str_category_25;
        }
        if (id == 26) {
            return R.string.str_category_26;
        }
        if (id == 27) {
            return R.string.str_category_27;
        }
        if (id == 28) {
            return R.string.str_category_28;
        }
        if (id == 29) {
            return R.string.str_category_29;
        }
        if (id == 30) {
            return R.string.str_category_30;
        }
        if (id == 31) {
            return R.string.str_category_31;
        }
        if (id == 32) {
            return R.string.str_category_32;
        }
        if (id == 33) {
            return R.string.str_category_33;
        }
        if (id == 34) {
            return R.string.str_category_34;
        }
        if (id == 35) {
            return R.string.str_category_35;
        }
        if (id == 36) {
            return R.string.str_category_36;
        }
        if (id == 37) {
            return R.string.str_category_37;
        }
        if (id == 38) {
            return R.string.str_category_38;
        }
        if (id == 39) {
            return R.string.str_category_39;
        }
        if (id == 40) {
            return R.string.str_category_40;
        }
        if (id == 41) {
            return R.string.str_category_41;
        }
        if (id == 42) {
            return R.string.str_category_42;
        }
        if (id == 43) {
            return R.string.str_category_43;
        }
        if (id == 44) {
            return R.string.str_category_44;
        }
        return R.string.str_category_45;
    }

    public static int getTagTitleResId(int id) {
        if (id == 1) {
            return R.string.str_tag_01;
        }
        if (id == 2) {
            return R.string.str_tag_02;
        }
        if (id == 3) {
            return R.string.str_tag_03;
        }
        if (id == 4) {
            return R.string.str_tag_04;
        }
        if (id == 5) {
            return R.string.str_tag_05;
        }
        if (id == 6) {
            return R.string.str_tag_06;
        }
        if (id == 7) {
            return R.string.str_tag_07;
        }
        if (id == 8) {
            return R.string.str_tag_08;
        }
        if (id == 9) {
            return R.string.str_tag_09;
        }
        if (id == 10) {
            return R.string.str_tag_10;
        }
        if (id == 11) {
            return R.string.str_tag_11;
        }
        if (id == 12) {
            return R.string.str_tag_12;
        }
        if (id == 13) {
            return R.string.str_tag_13;
        }
        if (id == 14) {
            return R.string.str_tag_14;
        }
        if (id == 15) {
            return R.string.str_tag_15;
        }
        return R.string.str_tag_16;
    }

    public static Double suu(Object value) {
        if (checkNumeric(value)) {
            return Double.valueOf(Double.parseDouble(String.valueOf(value)));
        }
        return Double.valueOf(0.0d);
    }

    public static Boolean checkNull(Object value) {
        if (value == null) {
            return true;
        }
        if (value instanceof String) {
            if (((String) value).trim().length() == 0) {
                return true;
            }
            return false;
        } else if (!(value instanceof CharSequence) || ((CharSequence) value).toString().trim().length() != 0) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean checkNumeric(Object value) {
        if (checkNull(value).booleanValue()) {
            return false;
        }
        try {
            Double.parseDouble(String.valueOf(value));
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static String sql9(Object value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance();
        df.applyPattern("0.#;-0.#");
        return df.format(suu(value));
    }

    public static String sqlX(Object value) {
        String str2 = "";
        if (checkNull(value).booleanValue()) {
            return "NULL";
        }
        String str1 = String.valueOf(value).trim();
        if (str1.indexOf("'") == -1) {
            return "'" + str1 + "'";
        }
        for (int i = 0; i < str1.length(); i++) {
            str2 = String.valueOf(str2) + str1.substring(i, 1);
            if (str1.substring(i, 1) == "'") {
                str2 = String.valueOf(str2) + "'";
            }
        }
        return "'" + str2 + "'";
    }

    public static String sqlXz(Object value) {
        String str2 = "";
        if (checkNull(value).booleanValue()) {
            return "''";
        }
        String str1 = String.valueOf(value).trim();
        if (str1.indexOf("'") == -1) {
            return "'" + str1 + "'";
        }
        for (int i = 0; i < str1.length(); i++) {
            str2 = String.valueOf(str2) + str1.substring(i, 1);
            if (str1.substring(i, 1) == "'") {
                str2 = String.valueOf(str2) + "'";
            }
        }
        return "'" + str2 + "'";
    }

    public static String sqlL(Object value) {
        StringBuilder sql = new StringBuilder();
        sql.append(sqlXz(value));
        sql.insert(sql.length() - 1, "%");
        return sql.toString();
    }

    public static String sqlLa(Object value) {
        StringBuilder sql = new StringBuilder();
        sql.append(sqlXz(value));
        sql.insert(1, "%");
        sql.insert(sql.length() - 1, "%");
        return sql.toString();
    }

    public static Map<String, String> getEmailData(Activity a, Intent data) {
        Map<String, String> result = new HashMap<>();
        Cursor c = null;
        String id = "";
        String name = "";
        String email = "";
        try {
            Cursor c2 = a.managedQuery(data.getData(), null, null, null, null);
            if (c2.moveToFirst()) {
                id = c2.getString(c2.getColumnIndexOrThrow("_id"));
            }
            if (c2.moveToFirst()) {
                name = c2.getString(c2.getColumnIndexOrThrow("display_name"));
            }
            c = a.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, "contact_id = ?", new String[]{id}, null);
            if (c.moveToFirst()) {
                email = c.getString(c.getColumnIndex("data1"));
            }
            result.put("name", name);
            result.put("email", email);
            return result;
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }

    public static CharSequence getScheduleStr(Resources resources, int sYear, int sMonth, int sDay, int sHour, int sMinute, String sTime, int eYear, int eMonth, int eDay, int eHour, int eMinute, String eTime, String location) {
        Calendar cal = Calendar.getInstance();
        StringBuilder str = new StringBuilder();
        str.append(String.valueOf(String.format("%1$04d", Integer.valueOf(sYear))) + resources.getString(R.string.str_year));
        str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(sMonth + 1))) + resources.getString(R.string.str_month));
        str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(sDay))) + resources.getString(R.string.str_day));
        cal.set(1, sYear);
        cal.set(2, sMonth);
        cal.set(5, sDay);
        switch (cal.get(7)) {
            case 1:
                str.append("(<font color=\"#ffb6c1\">" + resources.getString(R.string.str_week1) + "</font>) ");
                break;
            case DBEngine.DB_VERSION /*2*/:
                str.append("(<font color=\"White\">" + resources.getString(R.string.str_week2) + "</font>) ");
                break;
            case 3:
                str.append("(<font color=\"White\">" + resources.getString(R.string.str_week3) + "</font>) ");
                break;
            case 4:
                str.append("(<font color=\"White\">" + resources.getString(R.string.str_week4) + "</font>) ");
                break;
            case 5:
                str.append("(<font color=\"White\">" + resources.getString(R.string.str_week5) + "</font>) ");
                break;
            case 6:
                str.append("(<font color=\"White\">" + resources.getString(R.string.str_week6) + "</font>) ");
                break;
            case 7:
                str.append("(<font color=\"#00ffff\">" + resources.getString(R.string.str_week7) + "</font>) ");
                break;
        }
        if (sYear != eYear || sMonth != eMonth || sDay != eDay) {
            if (checkNull(sTime).booleanValue() != 0) {
                str.append("??:??");
            } else {
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(sHour))) + ":");
                str.append(String.format("%1$02d", Integer.valueOf(sMinute)));
            }
            str.append("～");
            str.append(String.valueOf(String.format("%1$04d", Integer.valueOf(eYear))) + resources.getString(R.string.str_year));
            str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(eMonth + 1))) + resources.getString(R.string.str_month));
            str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(eDay))) + resources.getString(R.string.str_day));
            cal.set(1, eYear);
            cal.set(2, eMonth);
            cal.set(5, eDay);
            switch (cal.get(7)) {
                case 1:
                    str.append("(<font color=\"#ffb6c1\">" + resources.getString(R.string.str_week1) + "</font>) ");
                    break;
                case DBEngine.DB_VERSION /*2*/:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week2) + "</font>) ");
                    break;
                case 3:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week3) + "</font>) ");
                    break;
                case 4:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week4) + "</font>) ");
                    break;
                case 5:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week5) + "</font>) ");
                    break;
                case 6:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week6) + "</font>) ");
                    break;
                case 7:
                    str.append("(<font color=\"#00ffff\">" + resources.getString(R.string.str_week7) + "</font>) ");
                    break;
            }
            if (checkNull(eTime).booleanValue()) {
                str.append("??:??");
            } else {
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(eHour))) + ":");
                str.append(String.format("%1$02d", Integer.valueOf(eMinute)));
            }
        } else if (checkNull(sTime).booleanValue() == 0 || !checkNull(eTime).booleanValue()) {
            if (checkNull(sTime).booleanValue()) {
                str.append("??:??");
            } else {
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(sHour))) + ":");
                str.append(String.format("%1$02d", Integer.valueOf(sMinute)));
            }
            str.append("～");
            if (checkNull(eTime).booleanValue()) {
                str.append("??:??");
            } else {
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(eHour))) + ":");
                str.append(String.format("%1$02d", Integer.valueOf(eMinute)));
            }
        } else {
            str.append(resources.getString(R.string.str_allday));
        }
        if (!checkNull(location).booleanValue()) {
            str.append("<br>");
            str.append(location);
        }
        return Html.fromHtml(str.toString());
    }

    public static CharSequence getScheduleStr2(Resources resources, int dayFlg, int sHour, int sMinute, String sTime, int eHour, int eMinute, String eTime) {
        StringBuilder str = new StringBuilder();
        if (dayFlg == 0) {
            if (!checkNull(sTime).booleanValue() || !checkNull(eTime).booleanValue()) {
                if (checkNull(sTime).booleanValue()) {
                    str.append("??:??");
                } else {
                    str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(sHour))) + ":");
                    str.append(String.format("%1$02d", Integer.valueOf(sMinute)));
                }
                str.append("～");
                if (checkNull(eTime).booleanValue()) {
                    str.append("??:??");
                } else {
                    str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(eHour))) + ":");
                    str.append(String.format("%1$02d", Integer.valueOf(eMinute)));
                }
            } else {
                str.append(resources.getString(R.string.str_allday));
            }
        } else if (dayFlg == 1) {
            if (checkNull(sTime).booleanValue()) {
                str.append("??:??");
            } else {
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(sHour))) + ":");
                str.append(String.format("%1$02d", Integer.valueOf(sMinute)));
            }
            str.append("～");
        } else if (dayFlg == 2) {
            str.append("～");
        } else {
            str.append("～");
            if (checkNull(eTime).booleanValue()) {
                str.append("??:??");
            } else {
                str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(eHour))) + ":");
                str.append(String.format("%1$02d", Integer.valueOf(eMinute)));
            }
        }
        return Html.fromHtml(str.toString());
    }

    public static CharSequence getDateTimeStr(Resources resources, int year, int month, int day, int hour, int minute, int loop, int[] week) {
        StringBuilder str = new StringBuilder();
        if (loop == 0) {
            str.append(String.valueOf(String.format("%1$04d", Integer.valueOf(year))) + resources.getString(R.string.str_year));
            str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(month + 1))) + resources.getString(R.string.str_month));
            str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(day))) + resources.getString(R.string.str_day));
            Calendar cal = Calendar.getInstance();
            cal.set(1, year);
            cal.set(2, month);
            cal.set(5, day);
            cal.set(11, hour);
            cal.set(12, minute);
            cal.set(13, 0);
            cal.set(14, 0);
            switch (cal.get(7)) {
                case 1:
                    str.append("(<font color=\"#ffb6c1\">" + resources.getString(R.string.str_week1) + "</font>) ");
                    break;
                case DBEngine.DB_VERSION /*2*/:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week2) + "</font>) ");
                    break;
                case 3:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week3) + "</font>) ");
                    break;
                case 4:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week4) + "</font>) ");
                    break;
                case 5:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week5) + "</font>) ");
                    break;
                case 6:
                    str.append("(<font color=\"White\">" + resources.getString(R.string.str_week6) + "</font>) ");
                    break;
                case 7:
                    str.append("(<font color=\"#00ffff\">" + resources.getString(R.string.str_week7) + "</font>) ");
                    break;
            }
        } else {
            str.append(resources.getString(R.string.str_repeat));
            str.append("(");
            for (int i = 0; i < week.length; i++) {
                if (week[i] == 1) {
                    switch (i) {
                        case 0:
                            str.append("<font color=\"#ffb6c1\">" + resources.getString(R.string.str_week1) + "</font>");
                            break;
                        case 1:
                            str.append("<font color=\"White\">" + resources.getString(R.string.str_week2) + "</font>");
                            break;
                        case DBEngine.DB_VERSION /*2*/:
                            str.append("<font color=\"White\">" + resources.getString(R.string.str_week3) + "</font>");
                            break;
                        case 3:
                            str.append("<font color=\"White\">" + resources.getString(R.string.str_week4) + "</font>");
                            break;
                        case 4:
                            str.append("<font color=\"White\">" + resources.getString(R.string.str_week5) + "</font>");
                            break;
                        case 5:
                            str.append("<font color=\"White\">" + resources.getString(R.string.str_week6) + "</font>");
                            break;
                        case 6:
                            str.append("<font color=\"#00ffff\">" + resources.getString(R.string.str_week7) + "</font>");
                            break;
                    }
                    str.append(" ");
                }
            }
            str.deleteCharAt(str.length() - 1);
            str.append(") ");
        }
        str.append(String.valueOf(String.format("%1$02d", Integer.valueOf(hour))) + ":");
        str.append(String.format("%1$02d", Integer.valueOf(minute)));
        return Html.fromHtml(str.toString());
    }

    public static void setAlarmManager(Context context, int alarmRecno, int year, int month, int day, int hour, int minute) {
        Intent intent = new Intent(context, ReceiverTick.class);
        intent.setAction(ACTION_TICK);
        intent.setData(Uri.parse("http://" + String.valueOf(alarmRecno)));
        intent.putExtra("alarmRecno", alarmRecno);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 134217728);
        Calendar cal = Calendar.getInstance();
        cal.set(1, year);
        cal.set(2, month);
        cal.set(5, day);
        cal.set(11, hour);
        cal.set(12, minute);
        cal.set(13, 0);
        cal.set(14, 0);
        ((AlarmManager) context.getSystemService("alarm")).set(0, cal.getTimeInMillis(), sender);
    }

    public static void cancelAlarmManager(Context context, int alarmRecno) {
        Intent intent = new Intent(context, ReceiverTick.class);
        intent.setAction(ACTION_TICK);
        intent.setData(Uri.parse("http://" + String.valueOf(alarmRecno)));
        intent.putExtra("alarmRecno", alarmRecno);
        ((AlarmManager) context.getSystemService("alarm")).cancel(PendingIntent.getBroadcast(context, 0, intent, 134217728));
    }

    public static String getDateTime() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public static class LinedEditText extends EditText {
        private boolean linedText;
        private Paint paint;
        private Rect rect;

        public LinedEditText(Context context, AttributeSet attrs) {
            super(context, attrs);
            Prefs prefs = new Prefs(context);
            this.linedText = prefs.getLinedText();
            switch (Integer.valueOf(prefs.getFontSize()).intValue()) {
                case 0:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_small) / AppInfo.getInstance().getScaledDensity());
                    break;
                case 1:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_midium) / AppInfo.getInstance().getScaledDensity());
                    break;
                case DBEngine.DB_VERSION /*2*/:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_large) / AppInfo.getInstance().getScaledDensity());
                    break;
                case 3:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_extra_large) / AppInfo.getInstance().getScaledDensity());
                    break;
            }
            if (prefs.getCapitalize()) {
                setKeyListener(TextKeyListener.getInstance(false, TextKeyListener.Capitalize.SENTENCES));
            } else {
                setKeyListener(TextKeyListener.getInstance(false, TextKeyListener.Capitalize.NONE));
            }
            if (this.linedText) {
                this.rect = new Rect();
                this.paint = new Paint();
                this.paint.setStyle(Paint.Style.STROKE);
                this.paint.setColor(805306623);
            }
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            if (this.linedText) {
                int count = getLineCount();
                Rect r = this.rect;
                Paint p = this.paint;
                for (int i = 0; i < count; i++) {
                    int baseline = getLineBounds(i, r);
                    canvas.drawLine((float) r.left, (float) (baseline + 1), (float) r.right, (float) (baseline + 1), p);
                }
            }
            super.onDraw(canvas);
        }
    }

    public static class LinedTextView extends TextView {
        private boolean linedText;
        private Paint paint;
        private Rect rect;

        public LinedTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
            Prefs prefs = new Prefs(context);
            this.linedText = prefs.getLinedText();
            switch (Integer.valueOf(prefs.getFontSize()).intValue()) {
                case 0:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_small) / AppInfo.getInstance().getScaledDensity());
                    break;
                case 1:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_midium) / AppInfo.getInstance().getScaledDensity());
                    break;
                case DBEngine.DB_VERSION /*2*/:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_large) / AppInfo.getInstance().getScaledDensity());
                    break;
                case 3:
                    setTextSize(context.getResources().getDimension(R.dimen.memo_font_extra_large) / AppInfo.getInstance().getScaledDensity());
                    break;
            }
            if (prefs.getAutoLink()) {
                setAutoLinkMask(15);
            } else {
                setAutoLinkMask(0);
            }
            if (this.linedText) {
                this.rect = new Rect();
                this.paint = new Paint();
                this.paint.setStyle(Paint.Style.STROKE);
                this.paint.setColor(805306623);
            }
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            if (this.linedText) {
                int count = getLineCount();
                Rect r = this.rect;
                Paint p = this.paint;
                for (int i = 0; i < count; i++) {
                    int baseline = getLineBounds(i, r);
                    canvas.drawLine((float) r.left, (float) (baseline + 1), (float) r.right, (float) (baseline + 1), p);
                }
            }
            super.onDraw(canvas);
        }
    }

    public static Calendar getNextDay(int[] week, int hour, int minute) {
        Calendar cal = Calendar.getInstance();
        Calendar alarmCal = Calendar.getInstance();
        Map<Integer, Integer> weekMap = new HashMap<>();
        weekMap.put(1, 0);
        weekMap.put(2, 1);
        weekMap.put(3, 2);
        weekMap.put(4, 3);
        weekMap.put(5, 4);
        weekMap.put(6, 5);
        weekMap.put(7, 6);
        boolean loopFlag = true;
        cal.setTimeInMillis(System.currentTimeMillis());
        while (loopFlag) {
            if (week[((Integer) weekMap.get(Integer.valueOf(cal.get(7)))).intValue()] == 1) {
                alarmCal.set(1, cal.get(1));
                alarmCal.set(2, cal.get(2));
                alarmCal.set(5, cal.get(5));
                alarmCal.set(11, hour);
                alarmCal.set(12, minute);
                alarmCal.set(13, cal.get(13));
                alarmCal.set(14, cal.get(14));
                if (System.currentTimeMillis() < alarmCal.getTimeInMillis()) {
                    loopFlag = false;
                }
            }
            cal.add(5, 1);
        }
        return alarmCal;
    }

    public static void cancelAllAlarm(Context context) {
        SQLiteDatabase DB = new DBEngine(context).getReadableDatabase();
        Cursor RS = DB.rawQuery(" SELECT * FROM ALARM", null);
        while (RS.moveToNext()) {
            cancelAlarmManager(context, RS.getInt(RS.getColumnIndex("recno")));
        }
        RS.close();
        DB.close();
    }

    /* JADX INFO: Multiple debug info for r8v11 android.content.ContentValues: [D('i' int), D('values' android.content.ContentValues)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void setAllAlarm(Context context) {
        Throwable th;
        SQLiteDatabase DB = new DBEngine(context).getWritableDatabase();
        Calendar alarmCal = Calendar.getInstance();
        int[] week = new int[7];
        try {
            DB.beginTransaction();
            DB.execSQL(" DELETE FROM ALARM" + " WHERE snooze >= 1");
            Cursor RS = DB.rawQuery(" SELECT * FROM ALARM" + " ORDER BY" + " alarm_year," + " alarm_month," + " alarm_day," + " alarm_hour," + " alarm_minute", null);
            while (RS.moveToNext()) {
                alarmCal.set(1, RS.getInt(RS.getColumnIndex(Alarm.ALARM_YEAR)));
                alarmCal.set(2, RS.getInt(RS.getColumnIndex(Alarm.ALARM_MONTH)));
                alarmCal.set(5, RS.getInt(RS.getColumnIndex(Alarm.ALARM_DAY)));
                alarmCal.set(11, RS.getInt(RS.getColumnIndex(Alarm.ALARM_HOUR)));
                alarmCal.set(12, RS.getInt(RS.getColumnIndex(Alarm.ALARM_MINUTE)));
                alarmCal.set(13, 0);
                alarmCal.set(14, 0);
                if (System.currentTimeMillis() >= alarmCal.getTimeInMillis()) {
                    if (RS.getInt(RS.getColumnIndex(Alarm.ALARM_LOOP)) == 1) {
                        int hour = RS.getInt(RS.getColumnIndex(Alarm.ALARM_HOUR));
                        int minute = RS.getInt(RS.getColumnIndex(Alarm.ALARM_MINUTE));
                        for (int i = 1; i <= 7; i++) {
                            int i2 = i - 1;
                            week[i2] = RS.getInt(RS.getColumnIndex("alarm_week" + String.valueOf(i)));
                        }
                        Calendar alarmCal2 = getNextDay(week, hour, minute);
                        try {
                            ContentValues values = new ContentValues();
                            values.put("memo_no", Integer.valueOf(RS.getInt(RS.getColumnIndex("memo_no"))));
                            values.put(Alarm.ALARM_YEAR, Integer.valueOf(alarmCal2.get(1)));
                            values.put(Alarm.ALARM_MONTH, Integer.valueOf(alarmCal2.get(2)));
                            values.put(Alarm.ALARM_DAY, Integer.valueOf(alarmCal2.get(5)));
                            values.put(Alarm.ALARM_HOUR, Integer.valueOf(hour));
                            values.put(Alarm.ALARM_MINUTE, Integer.valueOf(minute));
                            values.put(Alarm.ALARM_LOOP, (Integer) 1);
                            for (int i3 = 1; i3 <= 7; i3++) {
                                values.put("alarm_week" + String.valueOf(i3), Integer.valueOf(week[i3 - 1]));
                            }
                            values.put("enable", Integer.valueOf(RS.getInt(RS.getColumnIndex("enable"))));
                            values.put(Alarm.SNOOZE, (Integer) 0);
                            DB.insert("ALARM", null, values);
                            Cursor RS2 = DB.rawQuery(" SELECT last_insert_rowid()", null);
                            if (RS2.moveToFirst()) {
                                setAlarmManager(context, RS2.getInt(0), alarmCal2.get(1), alarmCal2.get(2), alarmCal2.get(5), hour, minute);
                            }
                            RS2.close();
                            alarmCal = alarmCal2;
                        } catch (Exception e) {
                            DB.endTransaction();
                            DB.close();
                            return;
                        } catch (Throwable th2) {
                            th = th2;
                            DB.endTransaction();
                            DB.close();
                            throw th;
                        }
                    }
                    StringBuilder sql = new StringBuilder();
                    sql.append(" DELETE FROM ALARM");
                    sql.append(" WHERE recno = " + sql9(Integer.valueOf(RS.getInt(RS.getColumnIndex("recno")))));
                    DB.execSQL(sql.toString());
                } else {
                    Context context2 = context;
                    setAlarmManager(context2, RS.getInt(RS.getColumnIndex("recno")), RS.getInt(RS.getColumnIndex(Alarm.ALARM_YEAR)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_MONTH)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_DAY)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_HOUR)), RS.getInt(RS.getColumnIndex(Alarm.ALARM_MINUTE)));
                }
            }
            RS.close();
            DB.setTransactionSuccessful();
            DB.endTransaction();
            DB.close();
        } catch (Exception e2) {
        } catch (Throwable th3) {
            th = th3;
            DB.endTransaction();
            DB.close();
            throw th;
        }
    }

    public static void showPhoto(Resources resources, String path, ImageView imageView) {
        if (!checkNull(path).booleanValue()) {
            try {
                BitmapFactory.Options op = new BitmapFactory.Options();
                op.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, op);
                int scale = Math.max((op.outWidth / resources.getDimensionPixelSize(R.dimen.image_width)) + 1, (op.outHeight / resources.getDimensionPixelSize(R.dimen.image_height)) + 1);
                op.inJustDecodeBounds = false;
                op.inSampleSize = scale;
                Bitmap bitmap = BitmapFactory.decodeFile(path, op);
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                } else {
                    imageView.setImageResource(R.drawable.noimage);
                }
            } catch (Exception e) {
                imageView.setImageResource(R.drawable.noimage);
            }
        } else {
            imageView.setImageDrawable(null);
        }
    }

    public static void showPhoto2(Resources resources, String path, TextView textView) {
        Drawable image;
        if (!checkNull(path).booleanValue()) {
            try {
                BitmapFactory.Options op = new BitmapFactory.Options();
                op.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(path, op);
                int scale = Math.max((op.outWidth / resources.getDimensionPixelSize(R.dimen.image_width)) + 1, (op.outHeight / resources.getDimensionPixelSize(R.dimen.image_height)) + 1);
                op.inJustDecodeBounds = false;
                op.inSampleSize = scale;
                Bitmap bitmap = BitmapFactory.decodeFile(path, op);
                if (bitmap != null) {
                    image = new BitmapDrawable(bitmap);
                } else {
                    image = resources.getDrawable(R.drawable.noimage);
                }
            } catch (Exception e) {
                image = resources.getDrawable(R.drawable.noimage);
            }
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            textView.setCompoundDrawables(null, null, null, image);
            textView.setText(((Object) textView.getText()) + "\n");
        }
    }

    public static void sendWidgetAction(Context context, String action) {
        if (new Prefs(context).getWidgetFlg()) {
            Intent intentService = new Intent(context, WidgetCalendar.UpdateService.class);
            intentService.setAction(action);
            context.startService(intentService);
        }
    }

    public static void copyClipboard(Context context, String text) {
        ((ClipboardManager) context.getSystemService("clipboard")).setText(text.trim());
        Toast.makeText(context, ((Object) context.getText(R.string.str_msg_clipboard)) + "\n\n" + text.trim(), 1).show();
    }

    public static void startImageViewer(Context context, String photoUri) {
        Intent intent = new Intent(context, ActivityImageViewer.class);
        intent.putExtra(ActivityImageViewer.INTENT_KEY_FILE, photoUri);
        context.startActivity(intent);
    }

    public static void startMap(Context context, String location) {
        try {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=" + Uri.encode(location))));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, (int) R.string.str_msg_nomap, 1).show();
        }
    }
}
