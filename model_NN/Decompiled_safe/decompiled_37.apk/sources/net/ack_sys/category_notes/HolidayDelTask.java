package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.ProgressDialog;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

public class HolidayDelTask extends AsyncTask<String, Void, AsyncTaskResult<?>> {
    private Activity activity = null;
    private HolidayDelTaskCallback callback;
    private ProgressDialog progressDialog = null;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        onPostExecute((AsyncTaskResult<?>) ((AsyncTaskResult) obj));
    }

    public HolidayDelTask(Activity activity2, HolidayDelTaskCallback callback2) {
        this.activity = activity2;
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.progressDialog = new ProgressDialog(this.activity);
        this.progressDialog.setMessage(this.activity.getString(R.string.str_msg_holiday));
        this.progressDialog.show();
    }

    /* access modifiers changed from: protected */
    public AsyncTaskResult<?> doInBackground(String... params) {
        SQLiteDatabase DB = new DBEngine(this.activity.getApplicationContext()).getWritableDatabase();
        try {
            DB.beginTransaction();
            DB.delete("HOLIDAY", "", null);
            DB.setTransactionSuccessful();
            DB.endTransaction();
            DB.close();
            return AsyncTaskResult.createNormalResult(this.activity.getString(R.string.str_msg_holidaydelok));
        } catch (Exception e) {
            AsyncTaskResult<?> createErrorResult = AsyncTaskResult.createErrorResult(this.activity.getString(R.string.str_msg_holidaydelerr));
            DB.endTransaction();
            DB.close();
            return createErrorResult;
        } catch (Throwable th) {
            DB.endTransaction();
            DB.close();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(AsyncTaskResult<?> result) {
        this.progressDialog.dismiss();
        if (result.isError()) {
            this.callback.onFailedHolidayDel(result.getMessage());
        } else {
            this.callback.onSuccessHolidayDel(result.getMessage());
        }
    }
}
