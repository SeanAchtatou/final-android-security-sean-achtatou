package net.ack_sys.category_notes;

public class AsyncTaskResult<T> {
    private T content;
    private boolean isError;
    private String message;

    private AsyncTaskResult(T content2, boolean isError2, String message2) {
        this.content = content2;
        this.isError = isError2;
        this.message = message2;
    }

    public T getContent() {
        return this.content;
    }

    public boolean isError() {
        return this.isError;
    }

    public String getMessage() {
        return this.message;
    }

    public static <T> AsyncTaskResult<T> createNormalResult(Object obj) {
        return new AsyncTaskResult<>(obj, false, "");
    }

    public static <T> AsyncTaskResult<T> createNormalResult(String message2) {
        return new AsyncTaskResult<>(null, false, message2);
    }

    public static <T> AsyncTaskResult<T> createErrorResult(String message2) {
        return new AsyncTaskResult<>(null, true, message2);
    }
}
