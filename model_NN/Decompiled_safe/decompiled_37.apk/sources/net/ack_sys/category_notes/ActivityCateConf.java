package net.ack_sys.category_notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ActivityCateConf extends Activity implements View.OnClickListener {
    private static final int REQUEST_CODE = 0;
    /* access modifiers changed from: private */
    public String DATA;
    /* access modifiers changed from: private */
    public String DISPLAY_NAME;
    private Gallery GR_CATE_ICON;
    private ImageView IV_ALARM;
    private ImageView IV_DEL;
    private ImageView IV_EDIT;
    private ImageView IV_ICON;
    private ImageView IV_MOVE0;
    private ImageView IV_MOVE1;
    private ImageView IV_MOVE2;
    private ImageView IV_MOVE3;
    private final int MENU_ADD = 1;
    /* access modifiers changed from: private */
    public TextView TV_CATE_TITLE;
    /* access modifiers changed from: private */
    public TextView TV_MUSIC_NAME;
    /* access modifiers changed from: private */
    public CategoryAdapterGr adapterGr = null;
    private int alarmVol;
    /* access modifiers changed from: private */
    public AlertDialog cateDialog = null;
    private boolean createFlg = false;
    /* access modifiers changed from: private */
    public int currentPosition;
    /* access modifiers changed from: private */
    public EditText editText;
    private Map<Integer, Integer> ibIconMap = new HashMap();
    private AlertDialog iconDialog = null;
    private MediaPlayer mediaPlayer;
    private AlertDialog musicDialog = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.createFlg = true;
        setContentView((int) R.layout.act_cateconf);
        getWindow().addFlags(128);
        this.IV_EDIT = (ImageView) findViewById(R.id.IV_EDIT);
        this.IV_EDIT.setOnClickListener(this);
        this.IV_ALARM = (ImageView) findViewById(R.id.IV_ALARM);
        this.IV_ALARM.setOnClickListener(this);
        this.IV_ICON = (ImageView) findViewById(R.id.IV_ICON);
        this.IV_ICON.setOnClickListener(this);
        this.IV_DEL = (ImageView) findViewById(R.id.IV_DEL);
        this.IV_DEL.setOnClickListener(this);
        this.GR_CATE_ICON = (Gallery) findViewById(R.id.GR_CATE_ICON);
        this.TV_CATE_TITLE = (TextView) findViewById(R.id.TV_CATE_TITLE);
        this.TV_MUSIC_NAME = (TextView) findViewById(R.id.TV_MUSIC_NAME);
        this.IV_MOVE0 = (ImageView) findViewById(R.id.IV_MOVE0);
        this.IV_MOVE0.setOnClickListener(this);
        this.IV_MOVE1 = (ImageView) findViewById(R.id.IV_MOVE1);
        this.IV_MOVE1.setOnClickListener(this);
        this.IV_MOVE2 = (ImageView) findViewById(R.id.IV_MOVE2);
        this.IV_MOVE2.setOnClickListener(this);
        this.IV_MOVE3 = (ImageView) findViewById(R.id.IV_MOVE3);
        this.IV_MOVE3.setOnClickListener(this);
        this.adapterGr = new CategoryAdapterGr(this, AppUtil.getCategory(this));
        this.GR_CATE_ICON.setAdapter((SpinnerAdapter) this.adapterGr);
        this.GR_CATE_ICON.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id) {
                ActivityCateConf.this.currentPosition = position;
                ActivityCateConf.this.TV_CATE_TITLE.setText(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getTitle());
                ActivityCateConf.this.TV_MUSIC_NAME.setText(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getMusicName());
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!this.createFlg) {
            updateAdapterGr();
        }
        this.createFlg = false;
    }

    public void onClick(View v) {
        if (this.adapterGr.getCount() != 0) {
            switch (v.getId()) {
                case R.id.IV_EDIT /*2131296606*/:
                    showEditDialog();
                    return;
                case R.id.IV_ALARM /*2131296607*/:
                    showMusicDialog();
                    return;
                case R.id.IV_ICON /*2131296608*/:
                    View iconView = LayoutInflater.from(this).inflate((int) R.layout.dlg_icon, (ViewGroup) null);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON01), 1);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON02), 2);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON03), 3);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON04), 4);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON05), 5);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON06), 6);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON07), 7);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON08), 8);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON09), 9);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON10), 10);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON11), 11);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON12), 12);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON13), 13);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON14), 14);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON15), 15);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON16), 16);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON17), 17);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON18), 18);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON19), 19);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON20), 20);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON21), 21);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON22), 22);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON23), 23);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON24), 24);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON25), 25);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON26), 26);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON27), 27);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON28), 28);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON29), 29);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON30), 30);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON31), 31);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON32), 32);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON33), 33);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON34), 34);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON35), 35);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON36), 36);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON37), 37);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON38), 38);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON39), 39);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON40), 40);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON41), 41);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON42), 42);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON43), 43);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON44), 44);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON45), 45);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON46), 46);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON47), 47);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON48), 48);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON49), 49);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON50), 50);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON51), 51);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON52), 52);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON53), 53);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON54), 54);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON55), 55);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON56), 56);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON57), 57);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON58), 58);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON59), 59);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON60), 60);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON61), 61);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON62), 62);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON63), 63);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON64), 64);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON65), 65);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON66), 66);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON67), 67);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON68), 68);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON69), 69);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON70), 70);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON71), 71);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON72), 72);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON73), 73);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON74), 74);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON75), 75);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON76), 76);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON77), 77);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON78), 78);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON79), 79);
                    this.ibIconMap.put(Integer.valueOf((int) R.id.IB_ICON80), 80);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON01)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON02)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON03)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON04)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON05)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON06)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON07)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON08)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON09)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON10)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON11)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON12)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON13)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON14)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON15)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON16)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON17)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON18)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON19)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON20)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON21)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON22)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON23)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON24)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON25)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON26)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON27)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON28)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON29)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON30)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON31)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON32)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON33)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON34)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON35)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON36)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON37)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON38)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON39)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON40)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON41)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON42)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON43)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON44)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON45)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON46)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON47)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON48)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON49)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON50)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON51)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON52)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON53)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON54)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON55)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON56)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON57)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON58)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON59)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON60)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON61)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON62)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON63)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON64)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON65)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON66)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON67)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON68)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON69)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON70)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON71)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON72)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON73)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON74)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON75)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON76)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON77)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON78)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON79)).setOnClickListener(this);
                    ((ImageButton) iconView.findViewById(R.id.IB_ICON80)).setOnClickListener(this);
                    this.iconDialog = new AlertDialog.Builder(this).setTitle((int) R.string.str_changeicon).setView(iconView).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).show();
                    return;
                case R.id.IV_DEL /*2131296609*/:
                    AlertDialog.Builder alert = new AlertDialog.Builder(this);
                    alert.setTitle((int) R.string.str_check);
                    alert.setMessage((int) R.string.str_msg_cateremove);
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCateConf.this.sortChange(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getId(), ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getSort(), 45, false);
                            ActivityCateConf.this.enableChange(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getId(), 0);
                        }
                    });
                    alert.setNegativeButton("No", (DialogInterface.OnClickListener) null);
                    alert.show();
                    return;
                case R.id.P_FOOTER /*2131296610*/:
                case R.id.GR_CATE_ICON /*2131296611*/:
                case R.id.TV_CATE_TITLE /*2131296612*/:
                case R.id.IV_MUSIC_ICON /*2131296613*/:
                case R.id.TV_MUSIC_NAME /*2131296614*/:
                case R.id.IB_CATE_ICON /*2131296619*/:
                case R.id.IB_TAG_ICON /*2131296620*/:
                case R.id.IV_TAG_ICON /*2131296621*/:
                case R.id.ET_MEMO /*2131296622*/:
                case R.id.LL_PHOTO /*2131296623*/:
                case R.id.IV_PHOTO_ICON /*2131296624*/:
                case R.id.TV_PHOTO /*2131296625*/:
                case R.id.LL_SCHEDULE /*2131296626*/:
                case R.id.IV_SCHEDULE_ICON /*2131296627*/:
                case R.id.TV_SCHEDULE /*2131296628*/:
                case R.id.IV_EDIT_SPEAK /*2131296629*/:
                case R.id.IV_CAMERA /*2131296630*/:
                case R.id.IV_SCHEDULE /*2131296631*/:
                case R.id.IV_UPDATE /*2131296632*/:
                case R.id.FL /*2131296633*/:
                case R.id.PB_WV /*2131296634*/:
                case R.id.WV /*2131296635*/:
                case R.id.IV_EXIT /*2131296636*/:
                case R.id.IV_ADD /*2131296637*/:
                case R.id.LL_HEADER /*2131296638*/:
                case R.id.IV_CATE_ICON /*2131296639*/:
                case R.id.LL_FOOTER /*2131296640*/:
                case R.id.ScrollView01 /*2131296641*/:
                case R.id.TV_MEMO /*2131296642*/:
                case R.id.LL_ALARM /*2131296643*/:
                case R.id.IV_ALARM_ICON /*2131296644*/:
                case R.id.TV_DATETIME /*2131296645*/:
                case R.id.IV_SHARE /*2131296646*/:
                case R.id.ET_WORD /*2131296647*/:
                case R.id.IB_SPEAK /*2131296648*/:
                case R.id.list /*2131296649*/:
                case R.id.admakerview /*2131296650*/:
                case R.id.TV_DATE /*2131296651*/:
                case R.id.TV_TIME /*2131296652*/:
                case R.id.B_DATE /*2131296653*/:
                case R.id.B_TIME /*2131296654*/:
                case R.id.TV_TIME_K /*2131296655*/:
                case R.id.B_WEEK_K /*2131296656*/:
                case R.id.B_TIME_K /*2131296657*/:
                case R.id.TV_ADATE /*2131296658*/:
                case R.id.B_STOP_MEMO /*2131296659*/:
                case R.id.B_STOP_NOMEMO /*2131296660*/:
                case R.id.B_SNOOZE /*2131296661*/:
                default:
                    return;
                case R.id.IV_MOVE0 /*2131296615*/:
                    sortChange(this.adapterGr.getItem(this.currentPosition).getId(), this.adapterGr.getItem(this.currentPosition).getSort(), 1);
                    return;
                case R.id.IV_MOVE1 /*2131296616*/:
                    if ((this.currentPosition + 1) - 1 >= 1) {
                        sortChange(this.adapterGr.getItem(this.currentPosition).getId(), this.adapterGr.getItem(this.currentPosition).getSort(), (this.currentPosition + 1) - 1);
                        return;
                    }
                    return;
                case R.id.IV_MOVE2 /*2131296617*/:
                    if (this.currentPosition + 1 + 1 <= this.adapterGr.getCount()) {
                        sortChange(this.adapterGr.getItem(this.currentPosition).getId(), this.adapterGr.getItem(this.currentPosition).getSort(), this.currentPosition + 1 + 1);
                        return;
                    }
                    return;
                case R.id.IV_MOVE3 /*2131296618*/:
                    sortChange(this.adapterGr.getItem(this.currentPosition).getId(), this.adapterGr.getItem(this.currentPosition).getSort(), this.adapterGr.getCount());
                    return;
                case R.id.IB_ICON01 /*2131296662*/:
                case R.id.IB_ICON02 /*2131296663*/:
                case R.id.IB_ICON03 /*2131296664*/:
                case R.id.IB_ICON04 /*2131296665*/:
                case R.id.IB_ICON05 /*2131296666*/:
                case R.id.IB_ICON06 /*2131296667*/:
                case R.id.IB_ICON07 /*2131296668*/:
                case R.id.IB_ICON08 /*2131296669*/:
                case R.id.IB_ICON09 /*2131296670*/:
                case R.id.IB_ICON10 /*2131296671*/:
                case R.id.IB_ICON11 /*2131296672*/:
                case R.id.IB_ICON12 /*2131296673*/:
                case R.id.IB_ICON13 /*2131296674*/:
                case R.id.IB_ICON14 /*2131296675*/:
                case R.id.IB_ICON15 /*2131296676*/:
                case R.id.IB_ICON16 /*2131296677*/:
                case R.id.IB_ICON17 /*2131296678*/:
                case R.id.IB_ICON18 /*2131296679*/:
                case R.id.IB_ICON19 /*2131296680*/:
                case R.id.IB_ICON20 /*2131296681*/:
                case R.id.IB_ICON21 /*2131296682*/:
                case R.id.IB_ICON22 /*2131296683*/:
                case R.id.IB_ICON23 /*2131296684*/:
                case R.id.IB_ICON24 /*2131296685*/:
                case R.id.IB_ICON25 /*2131296686*/:
                case R.id.IB_ICON26 /*2131296687*/:
                case R.id.IB_ICON27 /*2131296688*/:
                case R.id.IB_ICON28 /*2131296689*/:
                case R.id.IB_ICON29 /*2131296690*/:
                case R.id.IB_ICON30 /*2131296691*/:
                case R.id.IB_ICON31 /*2131296692*/:
                case R.id.IB_ICON32 /*2131296693*/:
                case R.id.IB_ICON33 /*2131296694*/:
                case R.id.IB_ICON34 /*2131296695*/:
                case R.id.IB_ICON35 /*2131296696*/:
                case R.id.IB_ICON36 /*2131296697*/:
                case R.id.IB_ICON37 /*2131296698*/:
                case R.id.IB_ICON38 /*2131296699*/:
                case R.id.IB_ICON39 /*2131296700*/:
                case R.id.IB_ICON40 /*2131296701*/:
                case R.id.IB_ICON41 /*2131296702*/:
                case R.id.IB_ICON42 /*2131296703*/:
                case R.id.IB_ICON43 /*2131296704*/:
                case R.id.IB_ICON44 /*2131296705*/:
                case R.id.IB_ICON45 /*2131296706*/:
                case R.id.IB_ICON46 /*2131296707*/:
                case R.id.IB_ICON47 /*2131296708*/:
                case R.id.IB_ICON48 /*2131296709*/:
                case R.id.IB_ICON49 /*2131296710*/:
                case R.id.IB_ICON50 /*2131296711*/:
                case R.id.IB_ICON51 /*2131296712*/:
                case R.id.IB_ICON52 /*2131296713*/:
                case R.id.IB_ICON53 /*2131296714*/:
                case R.id.IB_ICON54 /*2131296715*/:
                case R.id.IB_ICON55 /*2131296716*/:
                case R.id.IB_ICON56 /*2131296717*/:
                case R.id.IB_ICON57 /*2131296718*/:
                case R.id.IB_ICON58 /*2131296719*/:
                case R.id.IB_ICON59 /*2131296720*/:
                case R.id.IB_ICON60 /*2131296721*/:
                case R.id.IB_ICON61 /*2131296722*/:
                case R.id.IB_ICON62 /*2131296723*/:
                case R.id.IB_ICON63 /*2131296724*/:
                case R.id.IB_ICON64 /*2131296725*/:
                case R.id.IB_ICON65 /*2131296726*/:
                case R.id.IB_ICON66 /*2131296727*/:
                case R.id.IB_ICON67 /*2131296728*/:
                case R.id.IB_ICON68 /*2131296729*/:
                case R.id.IB_ICON69 /*2131296730*/:
                case R.id.IB_ICON70 /*2131296731*/:
                case R.id.IB_ICON71 /*2131296732*/:
                case R.id.IB_ICON72 /*2131296733*/:
                case R.id.IB_ICON73 /*2131296734*/:
                case R.id.IB_ICON74 /*2131296735*/:
                case R.id.IB_ICON75 /*2131296736*/:
                case R.id.IB_ICON76 /*2131296737*/:
                case R.id.IB_ICON77 /*2131296738*/:
                case R.id.IB_ICON78 /*2131296739*/:
                case R.id.IB_ICON79 /*2131296740*/:
                case R.id.IB_ICON80 /*2131296741*/:
                    iconChange(this.adapterGr.getItem(this.currentPosition).getId(), this.ibIconMap.get(Integer.valueOf(v.getId())).intValue());
                    this.iconDialog.dismiss();
                    return;
            }
        }
    }

    private void updateAdapterGr() {
        this.adapterGr.setCategorys(AppUtil.getCategory(this));
        this.adapterGr.notifyDataSetChanged();
        if (this.adapterGr.getCount() - 1 >= this.currentPosition) {
            this.TV_CATE_TITLE.setText(this.adapterGr.getItem(this.currentPosition).getTitle());
            this.TV_MUSIC_NAME.setText(this.adapterGr.getItem(this.currentPosition).getMusicName());
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.str_menu_addcategory).setIcon((int) R.drawable.me_add);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                final CategoryAdapter adapter = new CategoryAdapter(this, R.layout.row_category, AppUtil.getCategory(this, true));
                ListView LV = new ListView(this);
                LV.setAdapter((ListAdapter) adapter);
                LV.setScrollingCacheEnabled(false);
                LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                        ActivityCateConf.this.cateDialog.dismiss();
                        ActivityCateConf.this.enableChange(adapter.getItem(position).getId(), 1);
                        ActivityCateConf.this.sortChange(adapter.getItem(position).getId(), adapter.getItem(position).getSort(), ActivityCateConf.this.adapterGr.getCount());
                    }
                });
                this.cateDialog = new AlertDialog.Builder(this).setTitle((int) R.string.str_category).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).setView(LV).create();
                this.cateDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showEditDialog() {
        LinearLayout linearLayout = new LinearLayout(this);
        this.editText = new EditText(this);
        this.editText.setInputType(1);
        this.editText.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this.editText.setText(this.adapterGr.getItem(this.currentPosition).getTitle());
        Bundle bundleE = this.editText.getInputExtras(true);
        if (bundleE != null) {
            bundleE.putBoolean("allowEmoji", true);
        }
        ImageButton imageButton = new ImageButton(this);
        imageButton.setImageResource(R.drawable.ib_speak);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
                    intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
                    ActivityCateConf.this.startActivityForResult(intent, 0);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(ActivityCateConf.this, (int) R.string.str_msg_nospeech, 1).show();
                }
            }
        });
        linearLayout.addView(this.editText);
        linearLayout.addView(imageButton);
        final AlertDialog alert = new AlertDialog.Builder(this).setTitle((int) R.string.str_changetitle).setView(linearLayout).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                ActivityCateConf.this.titleChange(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getId(), ActivityCateConf.this.editText.getText().toString());
            }
        }).setNegativeButton("Cancel", (DialogInterface.OnClickListener) null).create();
        this.editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    alert.getWindow().setSoftInputMode(5);
                }
            }
        });
        alert.show();
    }

    private void showMusicDialog() {
        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
        if (cursor != null) {
            startManagingCursor(cursor);
            ListView LV = new ListView(this);
            LV.setAdapter((ListAdapter) new MusicListAdapter(this, R.layout.row_music, cursor, new String[]{"_display_name", "_data"}, new int[]{R.id.TV_MUSIC_TITLE}));
            LV.setScrollingCacheEnabled(false);
            LV.setChoiceMode(1);
            this.DISPLAY_NAME = this.adapterGr.getItem(this.currentPosition).getMusicName();
            this.DATA = this.adapterGr.getItem(this.currentPosition).getMusicUri();
            int selectItem = 0;
            while (cursor.moveToNext() && !cursor.getString(cursor.getColumnIndex("_data")).equals(this.adapterGr.getItem(this.currentPosition).getMusicUri())) {
                selectItem++;
            }
            LV.setItemChecked(selectItem, true);
            LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> items, View view, int position, long id) {
                    Cursor c = (Cursor) items.getAdapter().getItem(position);
                    ActivityCateConf.this.DISPLAY_NAME = c.getString(c.getColumnIndex("_display_name"));
                    ActivityCateConf.this.DATA = c.getString(c.getColumnIndex("_data"));
                    ActivityCateConf.this.alarmStop();
                    ActivityCateConf.this.alarmPlay(ActivityCateConf.this, Uri.parse(ActivityCateConf.this.DATA));
                }
            });
            this.musicDialog = new AlertDialog.Builder(this).setTitle((int) R.string.str_selectsound).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCateConf.this.alarmStop();
                    ActivityCateConf.this.musicChange(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getId(), ActivityCateConf.this.DISPLAY_NAME, ActivityCateConf.this.DATA);
                }
            }).setNeutralButton((int) R.string.str_normal, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCateConf.this.alarmStop();
                    ActivityCateConf.this.musicChange(ActivityCateConf.this.adapterGr.getItem(ActivityCateConf.this.currentPosition).getId(), ActivityCateConf.this.getString(R.string.str_normalringtone), "");
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    ActivityCateConf.this.alarmStop();
                }
            }).setView(LV).create();
            this.musicDialog.show();
        }
    }

    /* access modifiers changed from: private */
    public void alarmPlay(Context context, Uri uri) {
        Prefs prefs = new Prefs(this);
        AudioManager audio = (AudioManager) getSystemService("audio");
        float setVolume = (((float) audio.getStreamMaxVolume(4)) / 7.0f) * Float.valueOf(prefs.getVolume()).floatValue();
        this.alarmVol = audio.getStreamVolume(4);
        audio.setStreamVolume(4, Math.round(setVolume), 0);
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer.setAudioStreamType(4);
        this.mediaPlayer.setLooping(true);
        try {
            this.mediaPlayer.setDataSource(context, uri);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e2) {
            e2.printStackTrace();
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
        } catch (IOException e4) {
            e4.printStackTrace();
        }
        try {
            this.mediaPlayer.prepare();
        } catch (IllegalStateException e5) {
            e5.printStackTrace();
        } catch (IOException e6) {
            e6.printStackTrace();
        }
        this.mediaPlayer.start();
    }

    /* access modifiers changed from: private */
    public void alarmStop() {
        if (this.mediaPlayer != null) {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
            this.mediaPlayer.reset();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
            ((AudioManager) getSystemService("audio")).setStreamVolume(4, this.alarmVol, 0);
        }
    }

    /* access modifiers changed from: private */
    public void enableChange(int id, int enable) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        boolean errFlg = false;
        try {
            DB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put("enable", Integer.valueOf(enable));
            DB.update("CATEGORY", values, "id = ?", new String[]{String.valueOf(id)});
            Cursor RS = DB.rawQuery(" SELECT * FROM CATEGORY" + " WHERE enable = 1" + " ORDER BY sort", null);
            if (!RS.moveToFirst()) {
                AppInfo.getInstance().setCurrentPosition(0);
                errFlg = true;
            } else {
                AppInfo.getInstance().setCurrentPosition(0);
                AppInfo.getInstance().setCurrentCateId(RS.getInt(RS.getColumnIndex("id")));
            }
            RS.close();
            if (errFlg) {
                Cursor RS2 = DB.rawQuery(" SELECT * FROM CATEGORY" + " WHERE sort = 1", null);
                if (RS2.moveToFirst()) {
                    AppInfo.getInstance().setCurrentCateId(RS2.getInt(RS2.getColumnIndex("id")));
                }
                RS2.close();
            }
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            updateAdapterGr();
        }
    }

    /* access modifiers changed from: private */
    public void titleChange(int id, String newTitle) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put("title", newTitle);
            DB.update("CATEGORY", values, "id = ?", new String[]{String.valueOf(id)});
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            updateAdapterGr();
        }
    }

    /* access modifiers changed from: private */
    public void musicChange(int id, String musicName, String musicUri) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(Category.MUSIC_NAME, musicName);
            values.put(Category.MUSIC_URI, musicUri);
            DB.update("CATEGORY", values, "id = ?", new String[]{String.valueOf(id)});
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            updateAdapterGr();
        }
    }

    private void iconChange(int id, int iconId) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            ContentValues values = new ContentValues();
            values.put(Category.ICON_ID, Integer.valueOf(iconId));
            DB.update("CATEGORY", values, "id = ?", new String[]{String.valueOf(id)});
            DB.setTransactionSuccessful();
        } catch (Exception e) {
        } finally {
            DB.endTransaction();
            DB.close();
            updateAdapterGr();
            AppUtil.sendWidgetAction(this, AppUtil.ACTION_UPDATE);
        }
    }

    /* access modifiers changed from: private */
    public void sortChange(int id, int crSort, int newSort, boolean change) {
        SQLiteDatabase DB = new DBEngine(this).getWritableDatabase();
        try {
            DB.beginTransaction();
            int minSort = Math.min(crSort, newSort);
            int maxSort = Math.max(crSort, newSort);
            int stSort = minSort;
            if (crSort < newSort) {
                stSort = minSort - 1;
            }
            ContentValues values = new ContentValues();
            values.put("sort", Integer.valueOf(newSort));
            DB.update("CATEGORY", values, "id = ?", new String[]{String.valueOf(id)});
            Cursor RS = DB.rawQuery(" SELECT * FROM CATEGORY " + " WHERE id <> ? " + " AND   sort >= ? " + " AND   sort <= ? " + " ORDER BY sort ", new String[]{String.valueOf(id), String.valueOf(minSort), String.valueOf(maxSort)});
            while (RS.moveToNext()) {
                stSort++;
                ContentValues values2 = new ContentValues();
                values2.put("sort", Integer.valueOf(stSort));
                DB.update("CATEGORY", values2, "id = ?", new String[]{String.valueOf(RS.getInt(0))});
            }
            RS.close();
            DB.setTransactionSuccessful();
            DB.endTransaction();
            DB.close();
            if (change) {
                updateAdapterGr();
                this.GR_CATE_ICON.setSelection(newSort - 1);
                AppInfo.getInstance().setCurrentPosition(newSort - 1);
                AppInfo.getInstance().setCurrentCateId(id);
            }
        } catch (Exception e) {
            DB.endTransaction();
            DB.close();
            if (change) {
                updateAdapterGr();
                this.GR_CATE_ICON.setSelection(newSort - 1);
                AppInfo.getInstance().setCurrentPosition(newSort - 1);
                AppInfo.getInstance().setCurrentCateId(id);
            }
        } catch (Throwable th) {
            DB.endTransaction();
            DB.close();
            if (change) {
                updateAdapterGr();
                this.GR_CATE_ICON.setSelection(newSort - 1);
                AppInfo.getInstance().setCurrentPosition(newSort - 1);
                AppInfo.getInstance().setCurrentCateId(id);
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void sortChange(int id, int crSort, int newSort) {
        sortChange(id, crSort, newSort, true);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == -1) {
            ArrayList<String> results = data.getStringArrayListExtra("android.speech.extra.RESULTS");
            if (results.size() != 0) {
                if (results.size() > 1) {
                    final CharSequence[] items = new CharSequence[results.size()];
                    for (int i = 0; i < results.size(); i++) {
                        items[i] = results.get(i);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle((int) R.string.str_recognitionresults);
                    builder.setNegativeButton("Cancel", (DialogInterface.OnClickListener) null);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCateConf.this.editText.setText(items[which]);
                            ActivityCateConf.this.editText.setSelection(items[which].length());
                        }
                    });
                    builder.create().show();
                } else {
                    this.editText.setText(results.get(0));
                    this.editText.setSelection(results.get(0).length());
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
