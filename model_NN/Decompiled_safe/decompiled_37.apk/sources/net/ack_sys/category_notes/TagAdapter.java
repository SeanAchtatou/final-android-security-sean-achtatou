package net.ack_sys.category_notes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

public class TagAdapter extends ArrayAdapter<Tag> {
    private Context context;
    private LayoutInflater inflater;
    private List<Tag> tags;

    public TagAdapter(Context context2, int textViewResourceId, List<Tag> tags2) {
        super(context2, textViewResourceId, tags2);
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.tags = tags2;
    }

    public void setTags(List<Tag> tags2) {
        this.tags = tags2;
    }

    public Tag getItem(int position) {
        return this.tags.get(position);
    }

    public int getCount() {
        return this.tags.size();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View view = convertView;
        if (view == null) {
            int minHeight = this.context.getResources().getDimensionPixelSize(R.dimen.row_tag_height);
            view = this.inflater.inflate((int) R.layout.row_tag, (ViewGroup) null);
            view.setMinimumHeight(minHeight);
            holder = new ViewHolder(null);
            holder.IV_TAG_ICON = (ImageView) view.findViewById(R.id.IV_TAG_ICON);
            holder.TV_TAG_TITLE = (TextView) view.findViewById(R.id.TV_TAG_TITLE);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        Tag tag = getItem(position);
        if (tag.getId() == 0) {
            holder.IV_TAG_ICON.setVisibility(4);
        } else {
            holder.IV_TAG_ICON.setVisibility(0);
            holder.IV_TAG_ICON.setImageResource(AppUtil.getTagResId(this.context, tag.getId()));
        }
        holder.TV_TAG_TITLE.setText(tag.getTitle());
        return view;
    }

    private static class ViewHolder {
        ImageView IV_TAG_ICON;
        TextView TV_TAG_TITLE;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }
}
