package net.ack_sys.category_notes;

public interface HolidayDelTaskCallback {
    void onFailedHolidayDel(String str);

    void onSuccessHolidayDel(String str);
}
