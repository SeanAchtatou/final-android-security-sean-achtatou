package net.ack_sys.category_notes;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class ReceiverTick extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            SQLiteDatabase DB = new DBEngine(context).getReadableDatabase();
            boolean ok = false;
            boolean enable = false;
            int memoNo = 0;
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT * FROM ALARM");
            sql.append(" WHERE recno = " + AppUtil.sql9(Integer.valueOf(bundle.getInt("alarmRecno"))));
            Cursor RS = DB.rawQuery(sql.toString(), null);
            if (RS.moveToFirst()) {
                ok = true;
                if (RS.getInt(RS.getColumnIndex("enable")) == 1) {
                    enable = true;
                } else {
                    enable = false;
                }
                memoNo = RS.getInt(RS.getColumnIndex("memo_no"));
            }
            RS.close();
            DB.close();
            if (enable) {
                Intent intentMsg = new Intent("android.intent.action.MAIN");
                intentMsg.setClassName(context, "net.ack_sys.category_notes.ActivityMsg");
                intentMsg.setFlags(276824064);
                intentMsg.putExtra("alarmRecno", bundle.getInt("alarmRecno"));
                context.startActivity(intentMsg);
            } else if (ok) {
                AppUtil.deleteAlarm(context, bundle.getInt("alarmRecno"), true);
                if (memoNo == 0) {
                    AppUtil.sendWidgetAction(context, AppUtil.ACTION_TODAYCLICK);
                }
            }
        }
    }
}
