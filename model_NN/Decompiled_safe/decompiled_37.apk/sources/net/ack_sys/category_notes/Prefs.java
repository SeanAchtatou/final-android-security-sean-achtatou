package net.ack_sys.category_notes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Prefs {
    private Context context = null;

    public Prefs(Context context2) {
        this.context = context2;
    }

    private String getString(String key, String def) {
        return PreferenceManager.getDefaultSharedPreferences(this.context).getString(key, def);
    }

    private boolean getBoolean(String key, boolean def) {
        return PreferenceManager.getDefaultSharedPreferences(this.context).getBoolean(key, def);
    }

    private void setBoolean(String key, boolean value) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.context).edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    private int getInt(String key, int def) {
        return PreferenceManager.getDefaultSharedPreferences(this.context).getInt(key, def);
    }

    private void setInt(String key, int value) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.context).edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public boolean getInitHoliday() {
        return getBoolean("initHoliday", true);
    }

    public void setInitHoliday(boolean value) {
        setBoolean("initHoliday", value);
    }

    public String getFontSize() {
        return getString("fontSize", "1");
    }

    public boolean getLinedText() {
        return getBoolean("linedText", true);
    }

    public boolean getAutoLink() {
        return getBoolean("autoLink", true);
    }

    public boolean getCapitalize() {
        return getBoolean("capitalize", true);
    }

    public String getSortOrder() {
        return getString("sortOrder", "0");
    }

    public boolean getDateDisp() {
        return getBoolean("dateDisp", true);
    }

    public boolean getTagUse() {
        return getBoolean("tagUse", true);
    }

    public String getTagIcon() {
        return getString("tagIcon", "0");
    }

    public boolean getSixWeek() {
        return getBoolean("sixWeek", true);
    }

    public String getVolume() {
        return getString("volume", "4");
    }

    public boolean getVibrate() {
        return getBoolean("vibrate", true);
    }

    public String getAutoStop() {
        return getString("autoStop", "0");
    }

    public boolean getAutoSnooze() {
        return getBoolean("autoSnooze", true);
    }

    public String getSnoozeCnt() {
        return getString("snoozeCnt", "1");
    }

    public String getSnooze() {
        return getString(Alarm.SNOOZE, "0");
    }

    public String getManner() {
        return getString("manner", "0");
    }

    public String getColor() {
        return getString("color", "0");
    }

    public String getTransparency() {
        return getString("transparency", "2");
    }

    public String getTextColor() {
        return getString("textColor", "0");
    }

    public boolean getWidgetFlg() {
        return getBoolean("widgetFlg", false);
    }

    public void setWidgetFlg(boolean value) {
        setBoolean("widgetFlg", value);
    }

    public int getWidgetYear() {
        return getInt("widgetYear", 0);
    }

    public void setWidgetYear(int value) {
        setInt("widgetYear", value);
    }

    public int getWidgetMonth() {
        return getInt("widgetMonth", 0);
    }

    public void setWidgetMonth(int value) {
        setInt("widgetMonth", value);
    }

    public int getWidgetDay() {
        return getInt("widgetDay", 0);
    }

    public void setWidgetDay(int value) {
        setInt("widgetDay", value);
    }
}
