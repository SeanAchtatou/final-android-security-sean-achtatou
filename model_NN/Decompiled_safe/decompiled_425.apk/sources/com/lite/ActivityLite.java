package com.lite;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.newgatto.games.WillyMemoryGameLite.R;

public class ActivityLite extends Activity {
    public static final int RESULT_ACT_OK = 1;
    String FullVersionURL;
    private View.OnClickListener GoToPageListener = new View.OnClickListener() {
        public void onClick(View v) {
            try {
                Intent launchIntent = new Intent("android.intent.action.VIEW");
                launchIntent.setData(Uri.parse(ActivityLite.this.FullVersionURL));
                ActivityLite.this.startActivity(launchIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(ActivityLite.this.getApplicationContext(), (int) R.string.error_launching_app, 0).show();
            }
        }
    };
    String PubCode;
    private View.OnClickListener TryAgainListener = new View.OnClickListener() {
        public void onClick(View v) {
            Intent i = new Intent();
            i.putExtra("actualScore", ActivityLite.this.gameScore);
            ActivityLite.this.setResult(1, i);
            ActivityLite.this.finish();
        }
    };
    int bgImageIdx = 0;
    String buy_message;
    int gameScore;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.lite);
        Bundle extras = getIntent().getExtras();
        this.buy_message = extras.getString("buy_message");
        this.FullVersionURL = extras.getString("FullVersionURL");
        this.bgImageIdx = extras.getInt("bgImageIdx");
        this.gameScore = extras.getInt("score");
        this.PubCode = extras.getString("PubCode");
        if (this.bgImageIdx > 0) {
            LinearLayout lay = (LinearLayout) findViewById(R.id.linearLayout1);
            if (this.bgImageIdx == 1) {
                lay.setBackgroundResource(R.drawable.willy);
            }
        }
        setControls();
        addADS();
    }

    private void addADS() {
        AdView adView = new AdView(this, AdSize.BANNER, this.PubCode);
        ((TableLayout) findViewById(R.id.tableLayout1)).addView(adView);
        AdRequest adRequest = new AdRequest();
        adRequest.setTesting(false);
        adView.loadAd(adRequest);
    }

    public void setControls() {
        Display display = getWindowManager().getDefaultDisplay();
        int size = Math.min(display.getWidth(), display.getHeight());
        Button btnTryAgain = (Button) findViewById(R.id.buttontryagain);
        btnTryAgain.setOnClickListener(this.TryAgainListener);
        Button btnGoToPage = (Button) findViewById(R.id.buttongotopage);
        btnGoToPage.setOnClickListener(this.GoToPageListener);
        TextView tv = (TextView) findViewById(R.id.tvlitemessage);
        tv.setTextSize((float) (size / 14));
        tv.setTextColor(-256);
        tv.setText(this.buy_message);
        btnTryAgain.setTextSize((float) (size / 15));
        btnGoToPage.setTextSize((float) (size / 13));
        btnGoToPage.setTextColor(-65536);
    }
}
