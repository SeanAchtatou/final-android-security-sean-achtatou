package com.drawing;

import android.graphics.Canvas;
import android.graphics.Paint;

public class Text {
    private int mSize;
    private int mX;
    private int mY;
    private String mtxt;

    public Text(String text, int x, int y, int size) {
        this.mX = x;
        this.mY = y;
        this.mtxt = text;
        this.mSize = size;
    }

    public void doDraw(Canvas canvas) {
        Paint p = new Paint();
        p.setColor(-65536);
        p.setFakeBoldText(true);
        p.setTextSize((float) this.mSize);
        canvas.drawText(this.mtxt, (float) this.mX, (float) this.mY, p);
    }
}
