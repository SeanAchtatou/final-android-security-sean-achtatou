package com.drawing;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

public class Element {
    private DIRECTION mDirection;
    private boolean mIsTarget = false;
    private Paint mPaint;
    private int mSpeed;
    private Bitmap mStar;
    private int mX;
    private int mY;

    public Element() {
    }

    public Element(Bitmap bitmap, int x, int y, int speed, boolean p_isTarget, DIRECTION p_direction) {
        this.mX = x;
        this.mY = y;
        this.mSpeed = speed;
        this.mPaint = new Paint();
        this.mStar = bitmap;
        this.mIsTarget = p_isTarget;
        this.mDirection = p_direction;
    }

    public Element(Bitmap bitmap, int x, int y) {
        this.mX = x;
        this.mY = y;
        this.mPaint = new Paint();
        this.mStar = bitmap;
    }

    public int getX() {
        return this.mX;
    }

    public void setX(int x) {
        this.mX = x;
    }

    public void setY(int y) {
        this.mY = y;
    }

    public int getY() {
        return this.mY;
    }

    public DIRECTION getDirection() {
        return this.mDirection;
    }

    public int nextYposition() {
        if (this.mDirection == DIRECTION.UP_TO_DOWN) {
            this.mY += this.mSpeed;
        } else if (this.mDirection == DIRECTION.DOWN_TO_UP) {
            this.mY -= this.mSpeed;
        }
        return this.mY;
    }

    public boolean IsTarget() {
        return this.mIsTarget;
    }

    public void doDraw(Canvas canvas) {
        canvas.drawBitmap(this.mStar, (float) this.mX, (float) this.mY, this.mPaint);
    }
}
