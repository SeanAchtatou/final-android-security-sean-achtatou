package com.drawing;

public enum DIRECTION {
    UP_TO_DOWN,
    DOWN_TO_UP,
    UP_DOWN_MIXED,
    LEFT_TO_RIGHT,
    RIGHT_TO_LEFT,
    LEFT_RIGHT_MIXED
}
