package com.drawing;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int a = 2130837504;
        public static final int b = 2130837505;
        public static final int backg_1 = 2130837506;
        public static final int backg_2 = 2130837507;
        public static final int backg_3 = 2130837508;
        public static final int backg_4 = 2130837509;
        public static final int backg_5 = 2130837510;
        public static final int backg_6 = 2130837511;
        public static final int backg_6_1 = 2130837512;
        public static final int backg_6_2 = 2130837513;
        public static final int backg_6_3 = 2130837514;
        public static final int backg_7 = 2130837515;
        public static final int backg_main = 2130837516;
        public static final int backg_main2 = 2130837517;
        public static final int backg_word = 2130837518;
        public static final int blank = 2130837519;
        public static final int c = 2130837520;
        public static final int card1 = 2130837521;
        public static final int card10 = 2130837522;
        public static final int card11 = 2130837523;
        public static final int card12 = 2130837524;
        public static final int card13 = 2130837525;
        public static final int card14 = 2130837526;
        public static final int card15 = 2130837527;
        public static final int card16 = 2130837528;
        public static final int card17 = 2130837529;
        public static final int card18 = 2130837530;
        public static final int card19 = 2130837531;
        public static final int card2 = 2130837532;
        public static final int card20 = 2130837533;
        public static final int card21 = 2130837534;
        public static final int card22 = 2130837535;
        public static final int card23 = 2130837536;
        public static final int card3 = 2130837537;
        public static final int card4 = 2130837538;
        public static final int card5 = 2130837539;
        public static final int card6 = 2130837540;
        public static final int card7 = 2130837541;
        public static final int card8 = 2130837542;
        public static final int card9 = 2130837543;
        public static final int card_default = 2130837544;
        public static final int d = 2130837545;
        public static final int e = 2130837546;
        public static final int egg_blu = 2130837547;
        public static final int egg_green = 2130837548;
        public static final int egg_red = 2130837549;
        public static final int egg_yellow = 2130837550;
        public static final int f = 2130837551;
        public static final int g = 2130837552;
        public static final int h = 2130837553;
        public static final int i = 2130837554;
        public static final int icon = 2130837555;
        public static final int j = 2130837556;
        public static final int k = 2130837557;
        public static final int l = 2130837558;
        public static final int linkfullimage = 2130837559;
        public static final int m = 2130837560;
        public static final int n = 2130837561;
        public static final int o = 2130837562;
        public static final int p = 2130837563;
        public static final int q = 2130837564;
        public static final int r = 2130837565;
        public static final int s = 2130837566;
        public static final int star = 2130837567;
        public static final int t = 2130837568;
        public static final int u = 2130837569;
        public static final int v = 2130837570;
        public static final int w = 2130837571;
        public static final int w_icon = 2130837572;
        public static final int willy = 2130837573;
        public static final int x = 2130837574;
        public static final int y = 2130837575;
        public static final int z = 2130837576;
    }

    public static final class id {
        public static final int buttongotopage = 2131034152;
        public static final int buttontryagain = 2131034154;
        public static final int gameLayout = 2131034112;
        public static final int i00 = 2131034118;
        public static final int i01 = 2131034119;
        public static final int i02 = 2131034120;
        public static final int i03 = 2131034121;
        public static final int i10 = 2131034123;
        public static final int i11 = 2131034124;
        public static final int i12 = 2131034125;
        public static final int i13 = 2131034126;
        public static final int i20 = 2131034128;
        public static final int i21 = 2131034129;
        public static final int i22 = 2131034130;
        public static final int i23 = 2131034131;
        public static final int i30 = 2131034133;
        public static final int i31 = 2131034134;
        public static final int i32 = 2131034135;
        public static final int i33 = 2131034136;
        public static final int i34 = 2131034138;
        public static final int i35 = 2131034139;
        public static final int i36 = 2131034140;
        public static final int i37 = 2131034141;
        public static final int i38 = 2131034143;
        public static final int i39 = 2131034144;
        public static final int i40 = 2131034145;
        public static final int i41 = 2131034146;
        public static final int linearLayout1 = 2131034147;
        public static final int relativeLayout1 = 2131034113;
        public static final int relativeLayout2 = 2131034155;
        public static final int row0 = 2131034117;
        public static final int row1 = 2131034122;
        public static final int row2 = 2131034127;
        public static final int row3 = 2131034132;
        public static final int row4 = 2131034137;
        public static final int row5 = 2131034142;
        public static final int table = 2131034116;
        public static final int tableLayout1 = 2131034148;
        public static final int tableRow1 = 2131034149;
        public static final int tableRow2 = 2131034151;
        public static final int tableRow3 = 2131034153;
        public static final int textTime = 2131034114;
        public static final int tvlitemessage = 2131034150;
        public static final int txtCanc = 2131034157;
        public static final int txtScore = 2131034115;
        public static final int txtWord = 2131034156;
    }

    public static final class layout {
        public static final int game = 2130903040;
        public static final int lite = 2130903041;
        public static final int main = 2130903042;
        public static final int words = 2130903043;
    }

    public static final class string {
        public static final int FullVersionURL = 2130968579;
        public static final int HighScore = 2130968586;
        public static final int HighScoreFile = 2130968587;
        public static final int Pairs = 2130968584;
        public static final int Score = 2130968585;
        public static final int Start = 2130968583;
        public static final int app_name = 2130968576;
        public static final int buy_message = 2130968577;
        public static final int error_launching_app = 2130968578;
        public static final int hello = 2130968580;
        public static final int infos = 2130968582;
        public static final int time = 2130968581;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {com.newgatto.games.WillyMemoryGameLite.R.attr.backgroundColor, com.newgatto.games.WillyMemoryGameLite.R.attr.primaryTextColor, com.newgatto.games.WillyMemoryGameLite.R.attr.secondaryTextColor, com.newgatto.games.WillyMemoryGameLite.R.attr.keywords, com.newgatto.games.WillyMemoryGameLite.R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
