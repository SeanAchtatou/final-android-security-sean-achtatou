package com.drawing;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.CountDownTimer;
import com.newgatto.games.WillyMemoryGameLite.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class ImagesRain {
    private final int MAX_RAIN_ELEM = 32;
    public String TargetElementName;
    public int TotalEggs = 0;
    public int TotalEggsToHit = 8;
    private DIRECTION direction;
    private int eggHeight;
    private int eggWidth;
    private ArrayList<Element> mRain = new ArrayList<>();
    Resources mResources;
    private int screenHeight;
    private int screenWidth;
    private int targetElement;
    private CountDownTimer timerRain;

    public ImagesRain(Resources r, DIRECTION rainMode, int sWidth, int sHeight) {
        this.mResources = r;
        this.screenWidth = sWidth;
        this.screenHeight = sHeight;
        inits(rainMode);
    }

    private void inits(DIRECTION rainMode) {
        this.direction = rainMode;
        Bitmap m_bgEggRed = BitmapFactory.decodeResource(this.mResources, R.drawable.egg_red);
        Bitmap m_bgEggBlu = BitmapFactory.decodeResource(this.mResources, R.drawable.egg_blu);
        Bitmap m_bgEggGreen = BitmapFactory.decodeResource(this.mResources, R.drawable.egg_green);
        Bitmap m_bgEggYellow = BitmapFactory.decodeResource(this.mResources, R.drawable.egg_yellow);
        int[] yRain = new int[32];
        int[] xRain = new int[32];
        int[] speedRain = new int[32];
        this.eggWidth = m_bgEggBlu.getWidth();
        this.eggHeight = m_bgEggBlu.getHeight();
        if (this.timerRain != null) {
            this.timerRain.cancel();
            this.timerRain = null;
        }
        int idx = 0;
        for (int x = 0; x < this.screenWidth - (this.eggWidth / 2); x += ((this.screenWidth - (this.eggHeight / 2)) / 32) * 2) {
            if (idx < 32) {
                xRain[idx] = x;
                xRain[idx + 1] = x;
                yRain[idx] = -this.eggHeight;
                yRain[idx + 1] = -this.eggHeight;
                idx += 2;
            }
        }
        ArrayList<Integer> Speeds = new ArrayList<>();
        int idx2 = 0;
        for (int x2 = 1; x2 < 64; x2++) {
            if (idx2 < 32) {
                Speeds.add(Integer.valueOf(x2));
                idx2++;
            }
        }
        Collections.shuffle(Speeds);
        for (int i = 0; i < 32; i++) {
            speedRain[i] = ((Integer) Speeds.get(i)).intValue();
        }
        ArrayList<Integer> random = new ArrayList<>();
        random.add(null);
        random.add(1);
        random.add(2);
        random.add(3);
        Collections.shuffle(random);
        this.targetElement = ((Integer) random.get(0)).intValue();
        switch (this.targetElement) {
            case 0:
                this.TargetElementName = "Blu";
                break;
            case 1:
                this.TargetElementName = "Yellow";
                break;
            case 2:
                this.TargetElementName = "Green";
                break;
            case 3:
                this.TargetElementName = "Red";
                break;
        }
        boolean toggle = false;
        for (int i2 = 0; i2 < 32; i2++) {
            Bitmap bitmap = null;
            boolean isTarget = false;
            int ii = i2 % 4;
            if (ii == this.targetElement) {
                isTarget = true;
            }
            switch (ii) {
                case 0:
                    bitmap = m_bgEggBlu;
                    break;
                case 1:
                    bitmap = m_bgEggYellow;
                    break;
                case 2:
                    bitmap = m_bgEggGreen;
                    break;
                case 3:
                    bitmap = m_bgEggRed;
                    break;
            }
            DIRECTION elemDirection = this.direction;
            if (this.direction == DIRECTION.UP_DOWN_MIXED) {
                if (toggle) {
                    elemDirection = DIRECTION.UP_TO_DOWN;
                } else {
                    elemDirection = DIRECTION.DOWN_TO_UP;
                }
                toggle = !toggle;
            }
            Element el = new Element(bitmap, xRain[i2], yRain[i2], speedRain[i2], isTarget, elemDirection);
            synchronized (this.mRain) {
                this.mRain.add(el);
            }
        }
    }

    public void startRain() {
        this.timerRain = new CountDownTimer(500000, 100) {
            public void onTick(long millisUntilFinished) {
                ImagesRain.this.updateRain();
            }

            public void onFinish() {
            }
        }.start();
    }

    public void endRain() {
        synchronized (this.mRain) {
            this.mRain.clear();
        }
    }

    /* access modifiers changed from: private */
    public void updateRain() {
        synchronized (this.mRain) {
            Iterator<Element> it = this.mRain.iterator();
            while (it.hasNext()) {
                Element element = it.next();
                if (element.getDirection() == DIRECTION.UP_TO_DOWN && element.nextYposition() > this.screenHeight) {
                    element.setY(0);
                } else if (element.getDirection() == DIRECTION.DOWN_TO_UP && element.nextYposition() < (-this.eggHeight)) {
                    element.setY(this.screenHeight);
                }
            }
        }
    }

    public void stopTimer() {
        if (this.timerRain != null) {
            this.timerRain.cancel();
        }
    }

    public void draw(Canvas canvas) {
        synchronized (this.mRain) {
            Iterator<Element> it = this.mRain.iterator();
            while (it.hasNext()) {
                it.next().doDraw(canvas);
            }
        }
    }

    public boolean fingerDown(int x, int y) {
        int minX = x - this.eggWidth;
        int maxX = x;
        int minY = y - this.eggHeight;
        int maxY = y;
        synchronized (this.mRain) {
            Iterator<Element> it = this.mRain.iterator();
            while (it.hasNext()) {
                Element element = it.next();
                if (element.IsTarget() && element.getX() >= minX && element.getX() <= maxX && element.getY() >= minY && element.getY() <= maxY) {
                    element.setX(-1000);
                    this.TotalEggs++;
                    return true;
                }
            }
            return false;
        }
    }
}
