package com.drawing;

import android.graphics.Canvas;
import android.os.CountDownTimer;
import java.util.ArrayList;
import java.util.Iterator;

public class TextVisualizer {
    /* access modifiers changed from: private */
    public ArrayList<Text> mTesti = new ArrayList<>();

    public void draw(Canvas canvas) {
        synchronized (this.mTesti) {
            Iterator<Text> it = this.mTesti.iterator();
            while (it.hasNext()) {
                Text element = it.next();
                synchronized (element) {
                    element.doDraw(canvas);
                }
            }
        }
    }

    public void addText(String text, int x, int y, int time, int size) {
        final Text t = new Text(text, x, y, size);
        synchronized (this.mTesti) {
            this.mTesti.add(t);
        }
        new CountDownTimer((long) time, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                synchronized (TextVisualizer.this.mTesti) {
                    TextVisualizer.this.mTesti.remove(t);
                }
            }
        }.start();
    }
}
