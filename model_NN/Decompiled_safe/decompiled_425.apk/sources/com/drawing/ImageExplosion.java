package com.drawing;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.CountDownTimer;
import com.newgatto.games.WillyMemoryGameLite.R;
import java.util.ArrayList;
import java.util.Iterator;

public class ImageExplosion {
    private ArrayList<Element> mElements = new ArrayList<>();
    private Bitmap m_starBitmap;
    final int radiusMax = 800;
    /* access modifiers changed from: private */
    public int radiusToDraw;
    private CountDownTimer timerStar;

    public ImageExplosion(Resources r) {
        this.m_starBitmap = BitmapFactory.decodeResource(r, R.drawable.star);
    }

    public void startLights(int x, int y) {
        final int mX = x;
        final int mY = y;
        this.radiusToDraw = 10;
        if (this.timerStar != null) {
            this.timerStar.cancel();
            this.timerStar = null;
        }
        this.timerStar = new CountDownTimer(500000, 50) {
            public void onTick(long millisUntilFinished) {
                ImageExplosion.this.endLight();
                ImageExplosion.this.radiusToDraw = (int) (((double) ImageExplosion.this.radiusToDraw) * 1.3d);
                ImageExplosion.this.updateLights(mX, mY, ImageExplosion.this.radiusToDraw);
                if (ImageExplosion.this.radiusToDraw >= 800) {
                    ImageExplosion.this.endLight();
                    onFinish();
                }
            }

            public void onFinish() {
                ImageExplosion.this.endLight();
            }
        }.start();
    }

    public void endLight() {
        synchronized (this.mElements) {
            this.mElements.clear();
        }
    }

    /* access modifiers changed from: private */
    public void updateLights(int xCenter, int yCenter, int radius) {
        synchronized (this.mElements) {
            for (double a = 0.0d; a < 6.283185307179586d; a += 0.6283185307179586d) {
                this.mElements.add(new Element(this.m_starBitmap, xCenter + ((int) (((double) radius) * Math.cos(a))), yCenter + ((int) (((double) radius) * Math.sin(a)))));
            }
        }
    }

    public void stopTimer() {
        if (this.timerStar != null) {
            this.timerStar.cancel();
        }
    }

    public void draw(Canvas canvas) {
        synchronized (this.mElements) {
            Iterator<Element> it = this.mElements.iterator();
            while (it.hasNext()) {
                Element element = it.next();
                synchronized (element) {
                    element.doDraw(canvas);
                }
            }
        }
    }
}
