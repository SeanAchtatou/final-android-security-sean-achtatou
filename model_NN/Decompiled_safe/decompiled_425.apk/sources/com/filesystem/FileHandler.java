package com.filesystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileHandler {
    public int ReadFile(File fileToRead, byte[] buffer) {
        Exception e;
        int bytesReaded = 0;
        FileInputStream fIn = null;
        try {
            if (fileToRead.exists()) {
                FileInputStream fIn2 = new FileInputStream(fileToRead);
                try {
                    bytesReaded = fIn2.read(buffer);
                    fIn = fIn2;
                } catch (Exception e2) {
                    e = e2;
                    fIn = fIn2;
                    try {
                        e.printStackTrace();
                        try {
                            fIn.close();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                        return bytesReaded;
                    } catch (Throwable th) {
                        th = th;
                        try {
                            fIn.close();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fIn = fIn2;
                    fIn.close();
                    throw th;
                }
            }
            try {
                fIn.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
        } catch (Exception e6) {
            e = e6;
            e.printStackTrace();
            fIn.close();
            return bytesReaded;
        }
        return bytesReaded;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public void WriteFile(File fileToWrite, byte[] data) {
        Exception e;
        FileOutputStream fo = null;
        try {
            if (!fileToWrite.exists()) {
                fileToWrite.getParentFile().mkdirs();
            }
            FileOutputStream fo2 = new FileOutputStream(fileToWrite, false);
            try {
                fo2.write(data);
                fo2.flush();
                try {
                    fo2.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            } catch (Exception e3) {
                e = e3;
                fo = fo2;
                try {
                    e.printStackTrace();
                    try {
                        fo.close();
                    } catch (IOException e4) {
                        e4.printStackTrace();
                    }
                } catch (Throwable th) {
                    th = th;
                    try {
                        fo.close();
                    } catch (IOException e5) {
                        e5.printStackTrace();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                fo = fo2;
                fo.close();
                throw th;
            }
        } catch (Exception e6) {
            e = e6;
            e.printStackTrace();
            fo.close();
        }
    }
}
