package com.newgatto.games.WillyMemoryGameLite;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.view.Display;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.util.ArrayList;
import java.util.Collections;

public class ActivityGenericGame extends Activity {
    final int MAX_COLS = 4;
    final int MAX_ROWS = 6;
    final int NOT_SELECTED = -1;
    protected int avaiableTime;
    protected View.OnClickListener buttonListener;
    protected int colNum;
    protected int defaultIconID;
    protected boolean easyMode = false;
    protected int fsButtonID;
    protected int gameScore;
    protected CountDownTimer gameTimer = null;
    protected boolean hideTable;
    protected int[] imagesIDs;
    protected boolean inGame = false;
    protected int initialShowAllCardstime;
    protected boolean liteMode = false;
    protected int penalties;
    protected String pubCode;
    protected CountDownTimer resetTimerButton1 = null;
    protected CountDownTimer resetTimerButton2 = null;
    protected int rowNum;
    protected int ssButtonID;
    protected int stageTime;
    protected CountDownTimer startingTimer = null;

    /* access modifiers changed from: protected */
    public void addADS() {
        AdView adView = new AdView(this, AdSize.BANNER, this.pubCode);
        ((TableLayout) findViewById(R.id.table)).addView(adView);
        AdRequest adRequest = new AdRequest();
        adRequest.setTesting(false);
        adView.loadAd(adRequest);
    }

    /* access modifiers changed from: protected */
    public void randomizeImages() {
        ArrayList<Integer> cards = new ArrayList<>();
        for (int valueOf : this.imagesIDs) {
            cards.add(Integer.valueOf(valueOf));
        }
        Collections.shuffle(cards);
        for (int i = 0; i < this.imagesIDs.length; i++) {
            this.imagesIDs[i] = ((Integer) cards.get(i)).intValue();
        }
    }

    /* access modifiers changed from: protected */
    public void setLayout() {
        int cardsCount = 0;
        Display display = getWindowManager().getDefaultDisplay();
        int cellSize = (int) Math.min(((double) (display.getWidth() / this.colNum)) * 0.9d, ((double) (display.getHeight() / this.rowNum)) * 0.65d);
        this.fsButtonID = -1;
        this.ssButtonID = -1;
        TableLayout table = (TableLayout) findViewById(R.id.table);
        for (int rowIdx = 0; rowIdx < 6; rowIdx++) {
            if (rowIdx < this.rowNum) {
                TableRow row = (TableRow) table.getChildAt(rowIdx);
                for (int idx = 0; idx < 4; idx++) {
                    if (idx < this.colNum) {
                        Integer i = new Integer(this.imagesIDs[cardsCount]);
                        ImageButton button = (ImageButton) row.getChildAt(idx);
                        button.setMinimumHeight(cellSize);
                        button.setMinimumWidth(cellSize);
                        button.setMaxHeight(cellSize);
                        button.setMaxWidth(cellSize);
                        button.setTag(i);
                        button.setImageResource(this.imagesIDs[cardsCount]);
                        button.setScaleType(ImageView.ScaleType.CENTER_CROP);
                        button.setOnClickListener(this.buttonListener);
                        cardsCount++;
                    } else {
                        row.removeViewAt(this.colNum);
                    }
                }
            } else {
                table.removeViewAt(this.rowNum);
            }
        }
    }

    public void waitAndHideCards() {
        this.startingTimer = new CountDownTimer((long) this.initialShowAllCardstime, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                ActivityGenericGame.this.hideCards();
                ActivityGenericGame.this.inGame = true;
                ActivityGenericGame.this.startGameTimer();
            }
        }.start();
    }

    public void hideCards() {
        TableLayout table = (TableLayout) findViewById(R.id.table);
        for (int rowIdx = 0; rowIdx < this.rowNum; rowIdx++) {
            TableRow row = (TableRow) table.getChildAt(rowIdx);
            for (int idx = 0; idx < this.colNum; idx++) {
                ((ImageButton) row.getChildAt(idx)).setImageResource(this.defaultIconID);
            }
        }
    }

    public void startGameTimer() {
        this.gameTimer = new CountDownTimer((long) this.stageTime, 1000) {
            public void onTick(long millisUntilFinished) {
                ((TextView) ActivityGenericGame.this.findViewById(R.id.textTime)).setText("Time: " + (millisUntilFinished / 1000));
                ((TextView) ActivityGenericGame.this.findViewById(R.id.txtScore)).setText("Score: " + ActivityGenericGame.this.gameScore);
                ActivityGenericGame.this.avaiableTime = ((int) millisUntilFinished) / 1000;
            }

            public void onFinish() {
                ActivityGenericGame.this.inGame = false;
                ((TextView) ActivityGenericGame.this.findViewById(R.id.textTime)).setText("Time: 0");
                Toast.makeText(ActivityGenericGame.this.getApplicationContext(), new String("Game Over!\r\n\r\nyour score: " + ActivityGenericGame.this.gameScore), 1).show();
                Intent i = new Intent();
                i.putExtra("actualScore", ActivityGenericGame.this.gameScore);
                ActivityGenericGame.this.setResult(0, i);
                ActivityGenericGame.this.finish();
            }
        }.start();
    }

    public void waitAndGoToNextStage() {
        this.hideTable = true;
        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
                if (ActivityGenericGame.this.hideTable) {
                    ActivityGenericGame.this.hideTable = false;
                    TableLayout table = (TableLayout) ActivityGenericGame.this.findViewById(R.id.table);
                    for (int rowIdx = 0; rowIdx < ActivityGenericGame.this.rowNum; rowIdx++) {
                        table.removeViewAt(0);
                    }
                }
            }

            public void onFinish() {
                Intent i = new Intent();
                i.putExtra("actualScore", ActivityGenericGame.this.gameScore);
                ActivityGenericGame.this.setResult(1, i);
                ActivityGenericGame.this.finish();
            }
        }.start();
    }

    public void resizeControls() {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        TextView timeTxt = (TextView) findViewById(R.id.textTime);
        timeTxt.setTextSize((float) (width / 15));
        timeTxt.setTextColor(-65536);
        TextView scoreTxt = (TextView) findViewById(R.id.txtScore);
        scoreTxt.setTextSize((float) (width / 15));
        scoreTxt.setTextColor(-16776961);
        timeTxt.setText("Time: " + (this.stageTime / 1000));
        scoreTxt.setText("Score: " + this.gameScore);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.startingTimer != null) {
            this.startingTimer.cancel();
        }
        if (this.gameTimer != null) {
            this.gameTimer.cancel();
        }
        if (this.resetTimerButton1 != null) {
            this.resetTimerButton1.cancel();
        }
        if (this.resetTimerButton2 != null) {
            this.resetTimerButton2.cancel();
        }
        finish();
    }
}
