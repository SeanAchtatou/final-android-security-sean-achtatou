package com.newgatto.games.WillyMemoryGameLite;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import com.drawing.DIRECTION;
import com.newgatto.games.WillyMemoryGameLite.panels.BonusPanel;

public class ActivityBonus extends Activity {
    BonusPanel bonusPanel;
    int gameScore;
    DIRECTION rainMode;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Bundle extras = getIntent().getExtras();
        this.gameScore = extras.getInt("score");
        this.rainMode = DIRECTION.valueOf(extras.getString("rainMode"));
        this.bonusPanel = new BonusPanel(this, this.rainMode, display.getWidth(), display.getHeight());
        requestWindowFeature(1);
        setContentView(this.bonusPanel);
    }

    public void setFinish() {
        Intent i = new Intent();
        i.putExtra("actualScore", this.gameScore + this.bonusPanel.TotalScore);
        setResult(1, i);
        finish();
    }
}
