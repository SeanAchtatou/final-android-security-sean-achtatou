package com.newgatto.games.WillyMemoryGameLite.constants;

public class Constants {
    public static final int REQ_BONUS_1 = 50;
    public static final int REQ_BONUS_2 = 51;
    public static final int REQ_BONUS_3 = 52;
    public static final int REQ_CODE_LITE = 100;
    public static final int REQ_CODE_OK_1 = 1;
    public static final int REQ_CODE_OK_2 = 2;
    public static final int REQ_CODE_OK_3 = 3;
    public static final int REQ_CODE_OK_4 = 4;
    public static final int REQ_CODE_OK_5 = 5;
    public static final int REQ_CODE_OK_6 = 6;
    public static final int REQ_CODE_OK_7 = 7;
    public static final int REQ_CODE_OK_8 = 8;
    public static final int REQ_CODE_OK_9 = 9;
    public static final int REQ_CODE_OK_LAST = 10;
    public static final int REQ_WORD_1 = 80;
    public static final int REQ_WORD_2 = 81;
    public static final int REQ_WORD_3 = 82;
    public static final int REQ_WORD_4 = 83;
    public static final int RESULT_ACT_NOT_OK = 0;
    public static final int RESULT_ACT_OK = 1;

    public enum RAINING_MODE {
        UP_TO_DOWN,
        DOWN_TO_UP,
        UP_DOWN_MIXED,
        LEFT_TO_RIGHT,
        RIGHT_TO_LEFT,
        LEFT_RIGHT_MIXED
    }
}
