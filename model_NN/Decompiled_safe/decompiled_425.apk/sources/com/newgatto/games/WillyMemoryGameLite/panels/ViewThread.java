package com.newgatto.games.WillyMemoryGameLite.panels;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class ViewThread extends Thread {
    private SurfaceHolder mHolder;
    private AbstractPanel mPanel;
    private boolean mRun = false;

    public ViewThread(AbstractPanel panel) {
        this.mPanel = panel;
        this.mHolder = this.mPanel.getHolder();
    }

    public void setRunning(boolean run) {
        this.mRun = run;
    }

    public void run() {
        while (this.mRun) {
            Canvas canvas = this.mHolder.lockCanvas();
            if (canvas != null) {
                this.mPanel.doDraw(canvas);
                this.mHolder.unlockCanvasAndPost(canvas);
            }
        }
    }
}
