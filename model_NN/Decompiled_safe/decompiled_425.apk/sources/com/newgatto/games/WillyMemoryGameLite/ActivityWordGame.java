package com.newgatto.games.WillyMemoryGameLite;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityWordGame extends ActivityGenericGame {
    private View.OnClickListener CancListener = new View.OnClickListener() {
        public void onClick(View v) {
            TextView wordTxt = (TextView) ActivityWordGame.this.findViewById(R.id.txtWord);
            String txt = wordTxt.getText().toString();
            if (txt.length() > 0) {
                wordTxt.setText(txt.substring(0, wordTxt.length() - 1).toUpperCase());
            }
        }
    };
    /* access modifiers changed from: private */
    public int[] originalImagesId;
    /* access modifiers changed from: private */
    public char[] txt_word;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.words);
        Bundle extras = getIntent().getExtras();
        this.rowNum = extras.getInt("rowNum");
        this.colNum = extras.getInt("colNum");
        this.imagesIDs = extras.getIntArray("imagesIDs");
        this.originalImagesId = (int[]) this.imagesIDs.clone();
        this.stageTime = extras.getInt("time") * 1000;
        this.gameScore = extras.getInt("score");
        this.liteMode = extras.getBoolean("liteMode");
        this.pubCode = extras.getString("PubCode");
        this.txt_word = extras.getString("wordToGuess").toCharArray();
        ((TableLayout) findViewById(R.id.table)).setBackgroundResource(extras.getInt("backgroudImageID"));
        if (this.liteMode) {
            try {
                addADS();
            } catch (Exception e) {
            }
        }
        ((TextView) findViewById(R.id.txtCanc)).setOnClickListener(this.CancListener);
        randomizeImages();
        setOnClickListener();
        setLayout();
        resizeControls();
        resizeLocalControls();
        waitAndStart();
    }

    private void resizeLocalControls() {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        TextView wordTxt = (TextView) findViewById(R.id.txtWord);
        wordTxt.setTextSize((float) (width / 12));
        wordTxt.setTextColor(-16711936);
        TextView cancTxt = (TextView) findViewById(R.id.txtCanc);
        cancTxt.setTextSize((float) (width / 12));
        cancTxt.setTextColor(-1);
    }

    public void waitAndStart() {
        Toast.makeText(getApplicationContext(), "Guess the word that Willy is thinking!", 1).show();
        this.startingTimer = new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                ActivityWordGame.this.inGame = true;
                ActivityWordGame.this.startGameTimer();
            }
        }.start();
    }

    private void setOnClickListener() {
        this.buttonListener = new View.OnClickListener() {
            public void onClick(View view) {
                if (ActivityWordGame.this.inGame) {
                    TextView wordTxt = (TextView) ActivityWordGame.this.findViewById(R.id.txtWord);
                    int idxId = ((Integer) ((ImageButton) view).getTag()).intValue();
                    int i = 0;
                    while (true) {
                        if (i >= ActivityWordGame.this.originalImagesId.length) {
                            break;
                        } else if (ActivityWordGame.this.originalImagesId[i] != idxId) {
                            i++;
                        } else if (i <= ActivityWordGame.this.txt_word.length - 1) {
                            wordTxt.setText((String.valueOf(wordTxt.getText().toString()) + ActivityWordGame.this.txt_word[i]).toUpperCase());
                        }
                    }
                    if (wordTxt.getText().toString().equalsIgnoreCase(new String(ActivityWordGame.this.txt_word))) {
                        ActivityWordGame.this.inGame = false;
                        ActivityWordGame.this.gameTimer.cancel();
                        int bonusTime = ActivityWordGame.this.avaiableTime * 100;
                        ActivityWordGame.this.gameScore += bonusTime;
                        Toast.makeText(ActivityWordGame.this.getApplicationContext(), new String("Completed!\r\nbonus time: " + bonusTime), 1).show();
                        ActivityWordGame.this.waitAndGoToNextStage();
                    }
                }
            }
        };
    }
}
