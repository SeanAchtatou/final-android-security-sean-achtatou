package com.newgatto.games.WillyMemoryGameLite.panels;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.widget.Toast;
import com.drawing.DIRECTION;
import com.drawing.ImageExplosion;
import com.drawing.ImagesRain;
import com.drawing.TextVisualizer;
import com.newgatto.games.WillyMemoryGameLite.ActivityBonus;
import com.newgatto.games.WillyMemoryGameLite.R;

public class BonusPanel extends AbstractPanel {
    public int TotalScore = 0;
    int defaultTextSize = 16;
    int eggScore = 100;
    boolean inGame = false;
    /* access modifiers changed from: private */
    public ImagesRain mCascata;
    private Context mContext;
    private ImageExplosion mEsplosione;
    /* access modifiers changed from: private */
    public Activity mParentActivity;
    private TextVisualizer mTesti;
    private Bitmap m_bgBitmap;
    int scoreShowTime = 1000;
    int stageTime = 15000;

    public BonusPanel(Activity parentActivity, DIRECTION rainMode, int width, int height) {
        super(parentActivity.getApplicationContext());
        this.mContext = parentActivity.getApplicationContext();
        this.mParentActivity = parentActivity;
        this.m_bgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.backg_4);
        this.mCascata = new ImagesRain(getResources(), rainMode, width, height);
        this.mEsplosione = new ImageExplosion(getResources());
        this.mTesti = new TextVisualizer();
        Toast.makeText(this.mContext, "Bonus level:\nHit the " + this.mCascata.TargetElementName + " eggs!\n " + (this.stageTime / 1000) + " seconds to complete!", 1).show();
        new CountDownTimer(5000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                BonusPanel.this.inGame = true;
                BonusPanel.this.startGameTimer();
                BonusPanel.this.mCascata.startRain();
            }
        }.start();
    }

    public void startGameTimer() {
        new CountDownTimer((long) this.stageTime, 300) {
            public void onTick(long millisUntilFinished) {
                if (BonusPanel.this.inGame && BonusPanel.this.mCascata.TotalEggsToHit == BonusPanel.this.mCascata.TotalEggs) {
                    BonusPanel.this.inGame = false;
                    BonusPanel.this.showTotalScoreAndReturn();
                    cancel();
                }
            }

            public void onFinish() {
                if (BonusPanel.this.inGame) {
                    BonusPanel.this.inGame = false;
                    BonusPanel.this.mCascata.stopTimer();
                    BonusPanel.this.showTotalScoreAndReturn();
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public void showTotalScoreAndReturn() {
        if (this.mCascata.TotalEggsToHit == this.mCascata.TotalEggs) {
            Toast.makeText(this.mContext, "Completed!", 1).show();
        } else {
            Toast.makeText(this.mContext, "Hits:" + this.mCascata.TotalEggs + "/" + this.mCascata.TotalEggsToHit, 1).show();
        }
        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                ((ActivityBonus) BonusPanel.this.mParentActivity).setFinish();
            }
        }.start();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.inGame) {
            int lastX = (int) event.getX();
            int lastY = (int) event.getY();
            boolean result = this.mCascata.fingerDown(lastX, lastY);
            if (this.inGame && result) {
                this.mEsplosione.startLights(lastX, lastY);
                this.mTesti.addText("+" + this.eggScore, lastX, lastY, this.scoreShowTime, this.defaultTextSize);
                this.TotalScore += this.eggScore;
            }
        }
        return super.onTouchEvent(event);
    }

    public void doDraw(Canvas canvas) {
        canvas.drawBitmap(this.m_bgBitmap, 0.0f, 0.0f, (Paint) null);
        this.mEsplosione.draw(canvas);
        this.mCascata.draw(canvas);
        this.mTesti.draw(canvas);
    }
}
