package com.newgatto.games.WillyMemoryGameLite;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.view.Display;
import android.view.View;
import android.widget.Toast;
import com.filesystem.FileHandler;
import com.lite.ActivityLite;
import com.newgatto.games.WillyMemoryGameLite.constants.Constants;
import com.newgatto.games.WillyMemoryGameLite.panels.Panel;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class ActivityDefault extends Activity {
    boolean easyMode = false;
    int gameScore = 0;
    String highScoreFilePath;
    int[] imagesSet3x3_1 = {R.drawable.card1, R.drawable.card1, R.drawable.card2, R.drawable.card2, R.drawable.card3, R.drawable.card3, R.drawable.card4, R.drawable.card4, R.drawable.card5};
    int[] imagesSet3x3_2 = {R.drawable.card6, R.drawable.card6, R.drawable.card7, R.drawable.card7, R.drawable.card8, R.drawable.card8, R.drawable.card9, R.drawable.card9, R.drawable.card10};
    int[] imagesSet3x3_3 = {R.drawable.card1, R.drawable.card1, R.drawable.card2, R.drawable.card2, R.drawable.card3, R.drawable.card3, R.drawable.card9, R.drawable.card9, R.drawable.card10};
    int[] imagesSet4x4_1 = {R.drawable.card11, R.drawable.card11, R.drawable.card12, R.drawable.card12, R.drawable.card13, R.drawable.card13, R.drawable.card14, R.drawable.card14, R.drawable.card15, R.drawable.card15, R.drawable.card16, R.drawable.card16, R.drawable.card17, R.drawable.card17, R.drawable.card18, R.drawable.card18};
    int[] imagesSet4x4_2 = {R.drawable.card19, R.drawable.card19, R.drawable.card20, R.drawable.card20, R.drawable.card21, R.drawable.card21, R.drawable.card22, R.drawable.card22, R.drawable.card15, R.drawable.card15, R.drawable.card16, R.drawable.card16, R.drawable.card17, R.drawable.card17, R.drawable.card18, R.drawable.card18};
    int[] imagesSet4x4_3 = {R.drawable.card23, R.drawable.card23, R.drawable.card1, R.drawable.card1, R.drawable.card13, R.drawable.card13, R.drawable.card14, R.drawable.card14, R.drawable.card3, R.drawable.card3, R.drawable.card16, R.drawable.card16, R.drawable.card15, R.drawable.card15, R.drawable.card2, R.drawable.card2};
    int[] imagesSet6x4_1 = {R.drawable.card23, R.drawable.card23, R.drawable.card1, R.drawable.card1, R.drawable.card13, R.drawable.card13, R.drawable.card14, R.drawable.card14, R.drawable.card3, R.drawable.card3, R.drawable.card16, R.drawable.card16, R.drawable.card17, R.drawable.card17, R.drawable.card2, R.drawable.card2, R.drawable.card6, R.drawable.card6, R.drawable.card7, R.drawable.card7, R.drawable.card8, R.drawable.card8, R.drawable.card9, R.drawable.card9};
    int[] imagesSet6x4_2 = {R.drawable.card23, R.drawable.card23, R.drawable.card1, R.drawable.card1, R.drawable.card13, R.drawable.card13, R.drawable.card14, R.drawable.card14, R.drawable.card3, R.drawable.card3, R.drawable.card16, R.drawable.card16, R.drawable.card4, R.drawable.card4, R.drawable.card2, R.drawable.card2, R.drawable.card6, R.drawable.card6, R.drawable.card7, R.drawable.card7, R.drawable.card8, R.drawable.card8, R.drawable.card5, R.drawable.card5};
    int[] imagesSet6x4_3 = {R.drawable.card23, R.drawable.card23, R.drawable.card1, R.drawable.card1, R.drawable.card18, R.drawable.card18, R.drawable.card2, R.drawable.card2, R.drawable.card3, R.drawable.card3, R.drawable.card16, R.drawable.card16, R.drawable.card17, R.drawable.card17, R.drawable.card12, R.drawable.card12, R.drawable.card6, R.drawable.card6, R.drawable.card7, R.drawable.card7, R.drawable.card8, R.drawable.card8, R.drawable.card9, R.drawable.card9};
    int[] imagesSet6x4_4 = {R.drawable.card23, R.drawable.card23, R.drawable.card1, R.drawable.card1, R.drawable.card13, R.drawable.card13, R.drawable.card14, R.drawable.card14, R.drawable.card19, R.drawable.card19, R.drawable.card21, R.drawable.card21, R.drawable.card17, R.drawable.card17, R.drawable.card2, R.drawable.card2, R.drawable.card6, R.drawable.card6, R.drawable.card7, R.drawable.card7, R.drawable.card8, R.drawable.card8, R.drawable.card9, R.drawable.card9};
    int[] images_alphabet_set = {R.drawable.a, R.drawable.b, R.drawable.c, R.drawable.d, R.drawable.e, R.drawable.f, R.drawable.g, R.drawable.g, R.drawable.i, R.drawable.j, R.drawable.k, R.drawable.l, R.drawable.m, R.drawable.n, R.drawable.o, R.drawable.p, R.drawable.q, R.drawable.r, R.drawable.s, R.drawable.t, R.drawable.u, R.drawable.v, R.drawable.w, R.drawable.x, R.drawable.y, R.drawable.z};
    boolean liteMode = true;
    String[] mediumWords_1 = {"running", "sleeping", "garden", "master", "feather", "slippers", "herbs"};
    String[] mediumWords_2 = {"postman", "paper", "kennel", "bottle", "chicken", "snake", "steak"};
    Panel panel;
    int record = 0;
    private View.OnClickListener screenListener = new View.OnClickListener() {
        public void onClick(View view) {
            ActivityDefault.this.panel.setClickable(false);
            new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    if (ActivityDefault.this.panel.PromoClicked) {
                        try {
                            ActivityDefault.this.panel.setClickable(true);
                            Intent launchIntent = new Intent("android.intent.action.VIEW");
                            launchIntent.setData(Uri.parse(ActivityDefault.this.getString(R.string.FullVersionURL)));
                            ActivityDefault.this.startActivity(launchIntent);
                        } catch (ActivityNotFoundException e) {
                            Toast.makeText(ActivityDefault.this.getApplicationContext(), (int) R.string.error_launching_app, 0).show();
                        }
                    } else {
                        ActivityDefault.this.gameScore = 0;
                        ActivityDefault.this.startLayout(ActivityDefault.this.imagesSet3x3_1, 1, 3, 3, R.drawable.backg_1, 18, 4);
                    }
                }
            }.start();
        }
    };
    String[] simpleWords_1 = {"snow", "rain", "mouse", "sun", "fox", "tree", "wind", "fish", "milk", "cat", "ball"};
    String[] simpleWords_2 = {"duck", "game", "bone", "jump", "hole", "bird", "fly", "water", "food", "bell", "dig"};

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        Display display = getWindowManager().getDefaultDisplay();
        this.panel = new Panel(getApplicationContext(), display.getWidth(), display.getHeight());
        this.panel.setOnClickListener(this.screenListener);
        if (this.liteMode) {
            this.panel.showPromoImage(true);
        }
        setContentView(this.panel);
        updateBackImage(getResources().getConfiguration());
        ReadAndSetHighScore();
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    private String getRandomWord(String[] wordsArray) {
        ArrayList<String> wordsList = new ArrayList<>();
        for (String add : wordsArray) {
            wordsList.add(add);
        }
        Collections.shuffle(wordsList);
        return (String) wordsList.get(0);
    }

    private int[] getImageSetFromWord(String word) {
        int[] ids = new int[16];
        for (int i = 0; i < word.length(); i++) {
            ids[i] = this.images_alphabet_set[word.charAt(i) - 97];
        }
        for (int i2 = word.length(); i2 < 16; i2++) {
            ids[i2] = R.drawable.blank;
        }
        return ids;
    }

    private File getScoreFile() {
        return new File(String.valueOf(getScoreFolder()) + getString(R.string.HighScoreFile));
    }

    private String getScoreFolder() {
        return Environment.getExternalStorageDirectory() + File.separator + getString(R.string.app_name) + File.separator;
    }

    private void ReadAndSetHighScore() {
        this.record = ReadScoreFromFile();
        this.panel.setHighScore(new String(String.valueOf(getResources().getString(R.string.HighScore)) + " " + new String(new StringBuilder().append(this.record).toString())));
    }

    private int ReadScoreFromFile() {
        if (!getScoreFile().exists()) {
            return 0;
        }
        byte[] data = new byte[512];
        try {
            return new Integer(new String(data).substring(0, new FileHandler().ReadFile(getScoreFile(), data))).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    private void WriteScoreToFile() {
        boolean mExternalStorageWriteable;
        boolean mExternalStorageAvailable;
        String state = Environment.getExternalStorageState();
        if ("mounted".equals(state)) {
            mExternalStorageWriteable = true;
            mExternalStorageAvailable = true;
        } else if ("mounted_ro".equals(state)) {
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            mExternalStorageWriteable = false;
            mExternalStorageAvailable = false;
        }
        if (mExternalStorageAvailable && mExternalStorageWriteable) {
            try {
                new FileHandler().WriteFile(getScoreFile(), Integer.valueOf(this.gameScore).toString().getBytes());
            } catch (Exception e) {
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null || !data.hasExtra("actualScore")) {
            this.panel.setClickable(true);
            return;
        }
        this.gameScore = data.getIntExtra("actualScore", 0);
        if (resultCode != 1 || requestCode == 100 || requestCode == 10) {
            this.panel.setClickable(true);
            if (this.gameScore > this.record) {
                WriteScoreToFile();
                ReadAndSetHighScore();
            }
        } else if (requestCode == 100) {
        } else {
            if (this.easyMode) {
                switch (requestCode) {
                    case 1:
                        startLayout(this.imagesSet3x3_2, 2, 3, 3, R.drawable.backg_2, 15, 2);
                        return;
                    case 2:
                        startLayout(this.imagesSet3x3_3, 3, 3, 3, R.drawable.backg_3, 10, 2);
                        return;
                    case 3:
                        startLayout(this.imagesSet4x4_1, 4, 4, 4, R.drawable.backg_4, 28, 4);
                        break;
                    case 4:
                        break;
                    case Constants.REQ_CODE_OK_5 /*5*/:
                        startLayout(this.imagesSet4x4_3, 6, 4, 4, R.drawable.backg_6, 16, 3);
                        return;
                    case Constants.REQ_CODE_OK_6 /*6*/:
                        startLayout(this.imagesSet6x4_1, 7, 6, 4, R.drawable.backg_6_1, 40, 8);
                        return;
                    case Constants.REQ_CODE_OK_7 /*7*/:
                        startLayout(this.imagesSet6x4_2, 8, 6, 4, R.drawable.backg_6_2, 35, 6);
                        return;
                    case Constants.REQ_CODE_OK_8 /*8*/:
                        startLayout(this.imagesSet6x4_3, 9, 6, 4, R.drawable.backg_6_3, 24, 7);
                        return;
                    case Constants.REQ_CODE_OK_9 /*9*/:
                        startLayout(this.imagesSet6x4_4, 10, 6, 4, R.drawable.backg_7, 18, 8);
                        return;
                    case Constants.REQ_CODE_OK_LAST /*10*/:
                        Toast.makeText(getApplicationContext(), "If you reached this point without chates probably you are Willy!", 1).show();
                        return;
                    default:
                        return;
                }
                startLayout(this.imagesSet4x4_2, 5, 4, 4, R.drawable.backg_5, 22, 3);
                return;
            }
            switch (requestCode) {
                case 1:
                    startLayout(this.imagesSet3x3_2, 2, 3, 3, R.drawable.backg_2, 15, 2);
                    return;
                case 2:
                    startLayout(this.imagesSet3x3_3, 3, 3, 3, R.drawable.backg_3, 10, 2);
                    return;
                case 3:
                    startWordLayout(80, 4, 4, R.drawable.backg_1, 25, getRandomWord(this.simpleWords_1));
                    return;
                case 4:
                    startLayout(this.imagesSet4x4_2, 5, 4, 4, R.drawable.backg_5, 22, 3);
                    return;
                case Constants.REQ_CODE_OK_5 /*5*/:
                    startLayout(this.imagesSet4x4_3, 6, 4, 4, R.drawable.backg_6, 16, 3);
                    return;
                case Constants.REQ_CODE_OK_6 /*6*/:
                    startWordLayout(82, 4, 4, R.drawable.backg_1, 25, getRandomWord(this.mediumWords_1));
                    return;
                case Constants.REQ_CODE_OK_7 /*7*/:
                    startLayout(this.imagesSet6x4_2, 8, 6, 4, R.drawable.backg_6_2, 35, 6);
                    return;
                case Constants.REQ_CODE_OK_8 /*8*/:
                    startLayout(this.imagesSet6x4_3, 9, 6, 4, R.drawable.backg_6_3, 24, 7);
                    return;
                case Constants.REQ_CODE_OK_9 /*9*/:
                    startBonusPanel(52, Constants.RAINING_MODE.UP_DOWN_MIXED.name());
                    return;
                case Constants.REQ_CODE_OK_LAST /*10*/:
                    Toast.makeText(getApplicationContext(), "If you reached this point without chates probably you are Willy!", 1).show();
                    return;
                case Constants.REQ_BONUS_1 /*50*/:
                    if (this.liteMode) {
                        Intent intent = new Intent(this, ActivityLite.class);
                        intent.putExtra("buy_message", getString(R.string.buy_message));
                        intent.putExtra("FullVersionURL", getString(R.string.FullVersionURL));
                        intent.putExtra("bgImageIdx", 1);
                        intent.putExtra("score", this.gameScore);
                        intent.putExtra("PubCode", "a14dcd918851e6d");
                        startActivityForResult(intent, 100);
                        return;
                    }
                    startLayout(this.imagesSet4x4_1, 4, 4, 4, R.drawable.backg_4, 28, 4);
                    return;
                case Constants.REQ_BONUS_2 /*51*/:
                    startLayout(this.imagesSet6x4_1, 7, 6, 4, R.drawable.backg_6_1, 40, 8);
                    return;
                case Constants.REQ_BONUS_3 /*52*/:
                    startLayout(this.imagesSet6x4_4, 10, 6, 4, R.drawable.backg_7, 18, 8);
                    return;
                case Constants.REQ_WORD_1 /*80*/:
                    startWordLayout(81, 4, 4, R.drawable.backg_1, 15, getRandomWord(this.simpleWords_2));
                    return;
                case Constants.REQ_WORD_2 /*81*/:
                    startBonusPanel(50, Constants.RAINING_MODE.UP_TO_DOWN.name());
                    return;
                case Constants.REQ_WORD_3 /*82*/:
                    startWordLayout(83, 4, 4, R.drawable.backg_1, 15, getRandomWord(this.mediumWords_2));
                    return;
                case Constants.REQ_WORD_4 /*83*/:
                    startBonusPanel(51, Constants.RAINING_MODE.DOWN_TO_UP.name());
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void startLayout(int[] imagesIDs, int requestCode, int rows, int cols, int backID, int time, int initialShowAllCardstime) {
        if (this.easyMode) {
            time *= 2;
            initialShowAllCardstime *= 2;
        }
        Intent intent = new Intent(this, ActivityGame.class);
        intent.putExtra("rowNum", rows);
        intent.putExtra("colNum", cols);
        intent.putExtra("backgroudImageID", backID);
        intent.putExtra("defaultIconID", (int) R.drawable.card_default);
        intent.putExtra("imagesIDs", imagesIDs);
        intent.putExtra("time", time);
        intent.putExtra("initialShowAllCardstime", initialShowAllCardstime);
        intent.putExtra("score", this.gameScore);
        intent.putExtra("liteMode", this.liteMode);
        intent.putExtra("easyMode", this.easyMode);
        intent.putExtra("PubCode", "a14dcd918851e6d");
        startActivityForResult(intent, requestCode);
    }

    private void startWordLayout(int requestCode, int rows, int cols, int backID, int time, String wordToGuess) {
        int[] imagesIDs = getImageSetFromWord(wordToGuess);
        Intent intent = new Intent(this, ActivityWordGame.class);
        intent.putExtra("wordToGuess", wordToGuess);
        intent.putExtra("rowNum", rows);
        intent.putExtra("colNum", cols);
        intent.putExtra("backgroudImageID", backID);
        intent.putExtra("imagesIDs", imagesIDs);
        intent.putExtra("time", time);
        intent.putExtra("score", this.gameScore);
        intent.putExtra("liteMode", this.liteMode);
        intent.putExtra("PubCode", "a14dcd918851e6d");
        startActivityForResult(intent, requestCode);
    }

    private void startBonusPanel(int requestCode, String string) {
        Intent intent = new Intent(this, ActivityBonus.class);
        intent.putExtra("score", this.gameScore);
        intent.putExtra("rainMode", string);
        startActivityForResult(intent, requestCode);
    }

    private void updateBackImage(Configuration newConfig) {
        if (this.panel != null) {
            Display display = getWindowManager().getDefaultDisplay();
            this.panel.mWidth = display.getWidth();
            this.panel.mHeight = display.getHeight();
            if (newConfig.orientation == 2) {
                this.panel.m_bgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.backg_main2);
                return;
            }
            this.panel.m_bgBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.backg_main);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateBackImage(newConfig);
    }
}
