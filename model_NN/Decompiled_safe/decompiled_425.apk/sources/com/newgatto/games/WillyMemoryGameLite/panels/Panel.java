package com.newgatto.games.WillyMemoryGameLite.panels;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import com.drawing.ImageExplosion;
import com.newgatto.games.WillyMemoryGameLite.R;

public class Panel extends AbstractPanel {
    public boolean PromoClicked = false;
    private ImageExplosion mEsplosione;
    public int mHeight;
    public String mRecordText;
    public int mWidth;
    public Bitmap m_bgBitmap;
    private Bitmap m_promoImage;
    private boolean m_showPromoImage = false;

    public Panel(Context context, int width, int height) {
        super(context);
        this.mWidth = width;
        this.mHeight = height;
        this.mEsplosione = new ImageExplosion(getResources());
        this.mRecordText = new String();
        this.m_promoImage = BitmapFactory.decodeResource(getResources(), R.drawable.linkfullimage);
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.m_showPromoImage || event.getY() > ((float) (this.mHeight / 10))) {
            this.PromoClicked = false;
            this.mEsplosione.startLights((int) event.getX(), (int) event.getY());
        } else {
            this.PromoClicked = true;
        }
        return super.onTouchEvent(event);
    }

    public void doDraw(Canvas canvas) {
        if (this.m_bgBitmap != null) {
            canvas.drawBitmap(this.m_bgBitmap, new Rect(0, 0, this.m_bgBitmap.getWidth(), this.m_bgBitmap.getHeight()), new RectF(0.0f, 0.0f, (float) this.mWidth, (float) this.mHeight), (Paint) null);
            Paint p = new Paint();
            p.setColor(-16776961);
            p.setTextSize((float) (this.mWidth / 15));
            canvas.drawText(this.mRecordText, 10.0f, (float) (((double) this.mHeight) * 0.85d), p);
            if (this.m_showPromoImage) {
                canvas.drawBitmap(this.m_promoImage, new Rect(0, 0, this.m_promoImage.getWidth(), this.m_promoImage.getHeight()), new RectF(0.0f, 0.0f, (float) this.mWidth, (float) (this.mHeight / 10)), (Paint) null);
            }
        }
        this.mEsplosione.draw(canvas);
    }

    public void setHighScore(String score) {
        this.mRecordText = score;
    }

    public void showPromoImage(boolean visible) {
        this.m_showPromoImage = visible;
    }
}
