package com.newgatto.games.WillyMemoryGameLite;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.Toast;

public class ActivityGame extends ActivityGenericGame {
    /* access modifiers changed from: private */
    public int fsImageID;
    /* access modifiers changed from: private */
    public int pairsFound;
    /* access modifiers changed from: private */
    public int pairsToComplete;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.game);
        Bundle extras = getIntent().getExtras();
        this.rowNum = extras.getInt("rowNum");
        this.colNum = extras.getInt("colNum");
        this.pairsToComplete = (this.rowNum * this.colNum) / 2;
        this.imagesIDs = extras.getIntArray("imagesIDs");
        this.defaultIconID = extras.getInt("defaultIconID");
        this.stageTime = extras.getInt("time") * 1000;
        this.gameScore = extras.getInt("score");
        this.initialShowAllCardstime = extras.getInt("initialShowAllCardstime") * 1000;
        this.liteMode = extras.getBoolean("liteMode");
        this.easyMode = extras.getBoolean("easyMode");
        this.pubCode = extras.getString("PubCode");
        ((TableLayout) findViewById(R.id.table)).setBackgroundResource(extras.getInt("backgroudImageID"));
        if (this.liteMode) {
            try {
                addADS();
            } catch (Exception e) {
            }
        }
        randomizeImages();
        setOnClickListener();
        setLayout();
        resizeControls();
        waitAndHideCards();
    }

    private void setOnClickListener() {
        this.buttonListener = new View.OnClickListener() {
            public void onClick(View view) {
                if (ActivityGame.this.inGame && ActivityGame.this.ssButtonID == -1 && ActivityGame.this.fsButtonID != view.getId()) {
                    int imId = ((Integer) ((ImageButton) view).getTag()).intValue();
                    if (ActivityGame.this.fsButtonID == -1) {
                        ActivityGame.this.resetImageButton1();
                        ActivityGame.this.resetImageButton2();
                        ActivityGame.this.fsButtonID = view.getId();
                        ActivityGame.this.fsImageID = imId;
                    } else {
                        ActivityGame.this.ssButtonID = view.getId();
                    }
                    ((ImageButton) view).setImageResource(imId);
                    if (ActivityGame.this.fsButtonID == -1) {
                        return;
                    }
                    if (ActivityGame.this.ssButtonID != -1) {
                        int totalTime = 1000;
                        if (ActivityGame.this.easyMode) {
                            totalTime = 2000;
                        }
                        ActivityGame.this.resetTimerButton2 = new CountDownTimer((long) totalTime, (long) totalTime) {
                            public void onTick(long millisUntilFinished) {
                            }

                            public void onFinish() {
                                ActivityGame.this.resetImageButton2();
                            }
                        }.start();
                        if (ActivityGame.this.fsImageID == imId) {
                            if (ActivityGame.this.resetTimerButton1 != null) {
                                ActivityGame.this.resetTimerButton1.cancel();
                            }
                            if (ActivityGame.this.resetTimerButton2 != null) {
                                ActivityGame.this.resetTimerButton2.cancel();
                            }
                            ((ImageButton) view).setOnClickListener(null);
                            ((ImageButton) ActivityGame.this.findViewById(ActivityGame.this.fsButtonID)).setOnClickListener(null);
                            ActivityGame.this.fsButtonID = -1;
                            ActivityGame.this.ssButtonID = -1;
                            ActivityGame activityGame = ActivityGame.this;
                            activityGame.pairsFound = activityGame.pairsFound + 1;
                            ActivityGame.this.gameScore += 100;
                            if (ActivityGame.this.pairsFound == ActivityGame.this.pairsToComplete) {
                                ActivityGame.this.inGame = false;
                                ActivityGame.this.gameTimer.cancel();
                                int bonusTime = ActivityGame.this.avaiableTime * 100;
                                int penaltiesScore = ActivityGame.this.penalties * 50;
                                ActivityGame.this.gameScore = (ActivityGame.this.gameScore + bonusTime) - penaltiesScore;
                                Toast.makeText(ActivityGame.this.getApplicationContext(), new String("Completed!\r\nbonus time: " + bonusTime + "\r\npenalties: " + penaltiesScore), 1).show();
                                ActivityGame.this.waitAndGoToNextStage();
                                return;
                            }
                            return;
                        }
                        ActivityGame.this.penalties++;
                        return;
                    }
                    int totalTime2 = 1000;
                    if (ActivityGame.this.easyMode) {
                        totalTime2 = 2000;
                    }
                    ActivityGame.this.resetTimerButton1 = new CountDownTimer((long) totalTime2, (long) totalTime2) {
                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {
                            ActivityGame.this.resetImageButton1();
                        }
                    }.start();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void resetImageButton1() {
        if (this.fsButtonID != -1) {
            ((ImageButton) findViewById(this.fsButtonID)).setImageResource(R.drawable.card_default);
        }
        this.fsButtonID = -1;
    }

    /* access modifiers changed from: private */
    public void resetImageButton2() {
        if (this.ssButtonID != -1) {
            ((ImageButton) findViewById(this.ssButtonID)).setImageResource(R.drawable.card_default);
        }
        this.ssButtonID = -1;
    }
}
