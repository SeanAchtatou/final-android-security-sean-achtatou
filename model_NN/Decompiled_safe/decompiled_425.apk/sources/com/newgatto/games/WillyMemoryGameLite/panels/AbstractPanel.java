package com.newgatto.games.WillyMemoryGameLite.panels;

import android.content.Context;
import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import java.lang.Thread;

public abstract class AbstractPanel extends SurfaceView implements SurfaceHolder.Callback {
    private ViewThread mThread = new ViewThread(this);

    public AbstractPanel(Context context) {
        super(context);
        getHolder().addCallback(this);
        setZOrderOnTop(true);
    }

    public void doDraw(Canvas canvas) {
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (this.mThread.getState() == Thread.State.TERMINATED) {
            this.mThread = new ViewThread(this);
            this.mThread.setRunning(true);
            this.mThread.start();
            return;
        }
        this.mThread.setRunning(true);
        this.mThread.start();
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mThread.isAlive()) {
            this.mThread.setRunning(false);
        }
    }
}
