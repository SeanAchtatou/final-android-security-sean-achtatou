package com.godofandroid.starwars;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.io.IOException;

public class main extends Activity {
    /* access modifiers changed from: private */
    public Integer[] Imgid = {Integer.valueOf((int) R.drawable.a1), Integer.valueOf((int) R.drawable.a2), Integer.valueOf((int) R.drawable.a3), Integer.valueOf((int) R.drawable.a4), Integer.valueOf((int) R.drawable.a5), Integer.valueOf((int) R.drawable.a6), Integer.valueOf((int) R.drawable.a7), Integer.valueOf((int) R.drawable.a8), Integer.valueOf((int) R.drawable.a9), Integer.valueOf((int) R.drawable.a10)};
    private Gallery gallery;
    /* access modifiers changed from: private */
    public ImageView imgView;
    int position;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        setRequestedOrientation(1);
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        ((Button) findViewById(R.id.button1)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(main.this).create();
                alertDialog.setTitle("Confirmation");
                alertDialog.setMessage("Do you want to set this image as wallaper?");
                alertDialog.setButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            main.this.setWallpaper(BitmapFactory.decodeResource(main.this.getResources(), main.this.Imgid[main.this.position].intValue()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        Log.d("Gallery Example", "Image setted.");
                    }
                });
                alertDialog.show();
            }
        });
        this.position = 0;
        this.imgView = (ImageView) findViewById(R.id.ImageView01);
        this.imgView.setImageResource(this.Imgid[0].intValue());
        this.gallery = (Gallery) findViewById(R.id.examplegallery);
        this.gallery.setAdapter((SpinnerAdapter) new AddImgAdp(this));
        this.gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                main.this.imgView.setImageResource(main.this.Imgid[position].intValue());
                main.this.position = position;
            }
        });
    }

    public class AddImgAdp extends BaseAdapter {
        int GalItemBg;
        private Context cont;

        public AddImgAdp(Context c) {
            this.cont = c;
            TypedArray typArray = main.this.obtainStyledAttributes(R.styleable.GalleryTheme);
            this.GalItemBg = typArray.getResourceId(0, 0);
            typArray.recycle();
        }

        public int getCount() {
            return main.this.Imgid.length;
        }

        public Object getItem(int position) {
            return Integer.valueOf(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imgView = new ImageView(this.cont);
            imgView.setImageResource(main.this.Imgid[position].intValue());
            imgView.setLayoutParams(new Gallery.LayoutParams(80, 70));
            imgView.setScaleType(ImageView.ScaleType.FIT_XY);
            imgView.setBackgroundResource(this.GalItemBg);
            return imgView;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionsmenu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("About Me");
                alertDialog.setMessage("God of Android \n Contact us : godsofandroid@gmail.com");
                alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alertDialog.setIcon((int) R.drawable.icon);
                alertDialog.show();
                return true;
            case R.id.exit:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Do you want to exit the application").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        main.this.finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.create().show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
