package com.godofandroid.starwars;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int a1 = 2130837504;
        public static final int a10 = 2130837505;
        public static final int a2 = 2130837506;
        public static final int a3 = 2130837507;
        public static final int a4 = 2130837508;
        public static final int a5 = 2130837509;
        public static final int a6 = 2130837510;
        public static final int a7 = 2130837511;
        public static final int a8 = 2130837512;
        public static final int a9 = 2130837513;
        public static final int icon = 2130837514;
    }

    public static final class id {
        public static final int BANNER = 2130968576;
        public static final int IAB_BANNER = 2130968578;
        public static final int IAB_LEADERBOARD = 2130968579;
        public static final int IAB_MRECT = 2130968577;
        public static final int ImageView01 = 2130968581;
        public static final int LinearLayout01 = 2130968580;
        public static final int about = 2130968585;
        public static final int adView = 2130968583;
        public static final int button1 = 2130968582;
        public static final int examplegallery = 2130968584;
        public static final int exit = 2130968586;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class menu {
        public static final int optionsmenu = 2131099648;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class styleable {
        public static final int[] GalleryTheme = {16842828};
        public static final int GalleryTheme_android_galleryItemBackground = 0;
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
