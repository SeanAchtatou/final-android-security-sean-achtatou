package defpackage;

import android.os.AsyncTask;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: w  reason: default package */
public final class w extends AsyncTask<String, Void, Void> {
    private static Void a(String... strArr) {
        String str = strArr[0];
        while (str != null) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                httpURLConnection.setInstanceFollowRedirects(true);
                str = httpURLConnection.getResponseCode() == 302 ? httpURLConnection.getHeaderField("Location") : null;
            } catch (IOException e) {
                t.b("Unable to check for redirect.", e);
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] x0) {
        return a((String[]) x0);
    }
}
