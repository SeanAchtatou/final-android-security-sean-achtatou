package com.mobiloids.ballgame;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.SpinnerAdapter;
import android.widget.ViewFlipper;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class SelecLevel extends Activity {
    private static final String LEVEL = "LEVEL";
    private static String MY_PREFS = "SETTINGS";
    private static String OPEN_LEVELS = "OPEN LEVELS";
    private static final String PASSED_LEVEL_FLAGS = "PASSED_LEVEL_FLAGS";
    private static String SOUND_PREF = "SOUND";
    private Activity aboutActivity;
    /* access modifiers changed from: private */
    public int courrentPictureIndex;
    private int defaultLevelsOpen = 0;
    private boolean defaultSound = true;
    private boolean firstTime;
    Intent intent;
    private boolean isSoundOn;
    private Integer[] mImageIds;
    private Integer[] mImageIdsNotPassed = {Integer.valueOf((int) R.drawable.screen1), Integer.valueOf((int) R.drawable.screen2), Integer.valueOf((int) R.drawable.screen3), Integer.valueOf((int) R.drawable.screen4), Integer.valueOf((int) R.drawable.screen5), Integer.valueOf((int) R.drawable.screen6), Integer.valueOf((int) R.drawable.screen7), Integer.valueOf((int) R.drawable.screen8), Integer.valueOf((int) R.drawable.screen9), Integer.valueOf((int) R.drawable.screen10), Integer.valueOf((int) R.drawable.screen11), Integer.valueOf((int) R.drawable.screen12), Integer.valueOf((int) R.drawable.screen13), Integer.valueOf((int) R.drawable.screen14), Integer.valueOf((int) R.drawable.screen15), Integer.valueOf((int) R.drawable.screen16), Integer.valueOf((int) R.drawable.screen17), Integer.valueOf((int) R.drawable.screen18), Integer.valueOf((int) R.drawable.screen19), Integer.valueOf((int) R.drawable.screen20), Integer.valueOf((int) R.drawable.screen21), Integer.valueOf((int) R.drawable.screen22), Integer.valueOf((int) R.drawable.screen23), Integer.valueOf((int) R.drawable.screen24), Integer.valueOf((int) R.drawable.screen25), Integer.valueOf((int) R.drawable.screen26), Integer.valueOf((int) R.drawable.screen27), Integer.valueOf((int) R.drawable.screen28), Integer.valueOf((int) R.drawable.screen29), Integer.valueOf((int) R.drawable.screen30), Integer.valueOf((int) R.drawable.screen31), Integer.valueOf((int) R.drawable.screen32), Integer.valueOf((int) R.drawable.screen33), Integer.valueOf((int) R.drawable.screen34), Integer.valueOf((int) R.drawable.screen35)};
    private Integer[] mImageIdsPassed = {Integer.valueOf((int) R.drawable.screen1_pass), Integer.valueOf((int) R.drawable.screen2_pass), Integer.valueOf((int) R.drawable.screen3_pass), Integer.valueOf((int) R.drawable.screen4_pass), Integer.valueOf((int) R.drawable.screen5_pass), Integer.valueOf((int) R.drawable.screen6_pass), Integer.valueOf((int) R.drawable.screen7_pass), Integer.valueOf((int) R.drawable.screen8_pass), Integer.valueOf((int) R.drawable.screen9_pass), Integer.valueOf((int) R.drawable.screen10_pass), Integer.valueOf((int) R.drawable.screen11_pass), Integer.valueOf((int) R.drawable.screen12_pass), Integer.valueOf((int) R.drawable.screen13_pass), Integer.valueOf((int) R.drawable.screen14_pass), Integer.valueOf((int) R.drawable.screen15_pass), Integer.valueOf((int) R.drawable.screen16_pass), Integer.valueOf((int) R.drawable.screen17_pass), Integer.valueOf((int) R.drawable.screen18_pass), Integer.valueOf((int) R.drawable.screen19_pass), Integer.valueOf((int) R.drawable.screen20_pass), Integer.valueOf((int) R.drawable.screen21_pass), Integer.valueOf((int) R.drawable.screen22_pass), Integer.valueOf((int) R.drawable.screen23_pass), Integer.valueOf((int) R.drawable.screen24_pass), Integer.valueOf((int) R.drawable.screen25_pass), Integer.valueOf((int) R.drawable.screen26_pass), Integer.valueOf((int) R.drawable.screen27_pass), Integer.valueOf((int) R.drawable.screen28_pass), Integer.valueOf((int) R.drawable.screen29_pass), Integer.valueOf((int) R.drawable.screen30_pass), Integer.valueOf((int) R.drawable.screen31_pass), Integer.valueOf((int) R.drawable.screen32_pass), Integer.valueOf((int) R.drawable.screen33_pass), Integer.valueOf((int) R.drawable.screen34_pass), Integer.valueOf((int) R.drawable.screen35_pass)};
    /* access modifiers changed from: private */
    public Integer[] mPicturesIds = {Integer.valueOf((int) R.drawable.lev1), Integer.valueOf((int) R.drawable.lev2), Integer.valueOf((int) R.drawable.lev3), Integer.valueOf((int) R.drawable.lev4), Integer.valueOf((int) R.drawable.lev5), Integer.valueOf((int) R.drawable.lev6), Integer.valueOf((int) R.drawable.lev7), Integer.valueOf((int) R.drawable.lev8), Integer.valueOf((int) R.drawable.lev9), Integer.valueOf((int) R.drawable.lev10), Integer.valueOf((int) R.drawable.lev11), Integer.valueOf((int) R.drawable.lev12), Integer.valueOf((int) R.drawable.lev13), Integer.valueOf((int) R.drawable.lev14), Integer.valueOf((int) R.drawable.lev15), Integer.valueOf((int) R.drawable.lev16), Integer.valueOf((int) R.drawable.lev17), Integer.valueOf((int) R.drawable.lev18), Integer.valueOf((int) R.drawable.lev19), Integer.valueOf((int) R.drawable.lev20), Integer.valueOf((int) R.drawable.lev21), Integer.valueOf((int) R.drawable.lev22), Integer.valueOf((int) R.drawable.lev23), Integer.valueOf((int) R.drawable.lev24), Integer.valueOf((int) R.drawable.lev25), Integer.valueOf((int) R.drawable.lev26), Integer.valueOf((int) R.drawable.lev27), Integer.valueOf((int) R.drawable.lev28), Integer.valueOf((int) R.drawable.lev29), Integer.valueOf((int) R.drawable.lev30), Integer.valueOf((int) R.drawable.lev31), Integer.valueOf((int) R.drawable.lev32), Integer.valueOf((int) R.drawable.lev33), Integer.valueOf((int) R.drawable.lev34), Integer.valueOf((int) R.drawable.lev35)};
    private int mode = 0;
    private int openLevelsCount = 0;
    private SharedPreferences pref;
    private String slidingMessage = "Just touch the pictures to move them";
    /* access modifiers changed from: private */
    public Activity thisActivity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.select_level);
        this.mImageIds = new Integer[this.mImageIdsPassed.length];
        ((AdView) findViewById(R.id.adView)).loadAd(new AdRequest());
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        this.openLevelsCount = this.mPicturesIds.length;
        int oldOpenLevelsCount = this.pref.getInt(OPEN_LEVELS, this.defaultLevelsOpen);
        long passedLevelsFlag = this.pref.getLong("PASSED_LEVEL_FLAGS", 0);
        Log.i("FLAG==", new StringBuilder(String.valueOf(passedLevelsFlag)).toString());
        for (int i = 0; i < this.mImageIds.length; i++) {
            if (i < oldOpenLevelsCount) {
                Log.i("PICTURE OPENED", new StringBuilder(String.valueOf(i)).toString());
                this.mImageIds[i] = this.mImageIdsPassed[i];
            } else if (((1 << i) & passedLevelsFlag) > 0) {
                Log.i("PICTURE OPENED", String.valueOf(i) + " " + passedLevelsFlag + "  " + (1 << i));
                this.mImageIds[i] = this.mImageIdsPassed[i];
            } else {
                Log.i("PICTURE CLOSED", new StringBuilder(String.valueOf(i)).toString());
                this.mImageIds[i] = this.mImageIdsNotPassed[i];
            }
        }
        Log.i("OPEN LEVELS", new StringBuilder(String.valueOf(this.openLevelsCount)).toString());
        setTitle("Select Level");
        this.thisActivity = this;
        Button big = (Button) this.thisActivity.findViewById(R.id.bigImage);
        big.setBackgroundResource(this.mPicturesIds[0].intValue());
        this.courrentPictureIndex = 0;
        big.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SelecLevel.this.intent = new Intent(SelecLevel.this, AccelDemo.class);
                SelecLevel.this.intent.putExtra("LEVEL", SelecLevel.this.courrentPictureIndex);
                ((Button) SelecLevel.this.findViewById(R.id.bigImage)).setText("Loading...");
                SelecLevel.this.startActivity(SelecLevel.this.intent);
                SelecLevel.this.thisActivity.finish();
            }
        });
        Gallery g = (Gallery) findViewById(R.id.gallery);
        g.setAdapter((SpinnerAdapter) new GallaryAdapter(this, this.openLevelsCount, this.mImageIds));
        g.setSelection(this.courrentPictureIndex, true);
        g.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Button big = (Button) SelecLevel.this.thisActivity.findViewById(R.id.bigImage);
                big.setText("Start Lev. " + Integer.valueOf(position + 1).toString());
                big.setBackgroundResource(SelecLevel.this.mPicturesIds[position].intValue());
                ViewFlipper flipper = (ViewFlipper) SelecLevel.this.findViewById(R.id.layoutswitcher);
                flipper.setInAnimation(AnimationUtils.loadAnimation(SelecLevel.this.thisActivity, R.anim.in));
                flipper.setOutAnimation(AnimationUtils.loadAnimation(SelecLevel.this.thisActivity, R.anim.out));
                flipper.startFlipping();
                flipper.setFlipInterval(1000);
                flipper.showNext();
                flipper.stopFlipping();
                SelecLevel.this.courrentPictureIndex = position;
            }
        });
    }
}
