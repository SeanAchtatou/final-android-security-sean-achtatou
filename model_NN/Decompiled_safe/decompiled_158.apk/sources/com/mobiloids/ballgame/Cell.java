package com.mobiloids.ballgame;

public class Cell {
    public boolean opened;
    public int pictureNum;

    public Cell(int pictureNum2, boolean opened2) {
        this.pictureNum = pictureNum2;
        this.opened = opened2;
    }
}
