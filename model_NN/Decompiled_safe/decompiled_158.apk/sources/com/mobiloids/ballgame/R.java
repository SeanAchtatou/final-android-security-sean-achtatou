package com.mobiloids.ballgame;

public final class R {

    public static final class anim {
        public static final int in = 2130968576;
        public static final int out = 2130968577;
    }

    public static final class array {
        public static final int number_of_pairs = 2131034112;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int about_us = 2130837504;
        public static final int animals = 2130837505;
        public static final int apply = 2130837506;
        public static final int arrow_left = 2130837507;
        public static final int backselect = 2130837508;
        public static final int ball = 2130837509;
        public static final int ball2 = 2130837510;
        public static final int ball_1 = 2130837511;
        public static final int ball_10 = 2130837512;
        public static final int ball_11 = 2130837513;
        public static final int ball_12 = 2130837514;
        public static final int ball_13 = 2130837515;
        public static final int ball_14 = 2130837516;
        public static final int ball_15 = 2130837517;
        public static final int ball_16 = 2130837518;
        public static final int ball_1_help = 2130837519;
        public static final int ball_2 = 2130837520;
        public static final int ball_3 = 2130837521;
        public static final int ball_4 = 2130837522;
        public static final int ball_5 = 2130837523;
        public static final int ball_6 = 2130837524;
        public static final int ball_7 = 2130837525;
        public static final int ball_8 = 2130837526;
        public static final int ball_9 = 2130837527;
        public static final int ball_old = 2130837528;
        public static final int button_back_image_mdpi = 2130837529;
        public static final int copy = 2130837530;
        public static final int danger = 2130837531;
        public static final int emotions = 2130837532;
        public static final int exit = 2130837533;
        public static final int france = 2130837534;
        public static final int game_over = 2130837535;
        public static final int help = 2130837536;
        public static final int icon = 2130837537;
        public static final int kids = 2130837538;
        public static final int kidsmath = 2130837539;
        public static final int land_back = 2130837540;
        public static final int lev1 = 2130837541;
        public static final int lev10 = 2130837542;
        public static final int lev11 = 2130837543;
        public static final int lev12 = 2130837544;
        public static final int lev13 = 2130837545;
        public static final int lev14 = 2130837546;
        public static final int lev15 = 2130837547;
        public static final int lev16 = 2130837548;
        public static final int lev17 = 2130837549;
        public static final int lev18 = 2130837550;
        public static final int lev19 = 2130837551;
        public static final int lev2 = 2130837552;
        public static final int lev20 = 2130837553;
        public static final int lev21 = 2130837554;
        public static final int lev22 = 2130837555;
        public static final int lev23 = 2130837556;
        public static final int lev24 = 2130837557;
        public static final int lev25 = 2130837558;
        public static final int lev26 = 2130837559;
        public static final int lev27 = 2130837560;
        public static final int lev28 = 2130837561;
        public static final int lev29 = 2130837562;
        public static final int lev3 = 2130837563;
        public static final int lev30 = 2130837564;
        public static final int lev31 = 2130837565;
        public static final int lev32 = 2130837566;
        public static final int lev33 = 2130837567;
        public static final int lev34 = 2130837568;
        public static final int lev35 = 2130837569;
        public static final int lev4 = 2130837570;
        public static final int lev5 = 2130837571;
        public static final int lev6 = 2130837572;
        public static final int lev7 = 2130837573;
        public static final int lev8 = 2130837574;
        public static final int lev9 = 2130837575;
        public static final int love = 2130837576;
        public static final int menu_back = 2130837577;
        public static final int mobiloids = 2130837578;
        public static final int options = 2130837579;
        public static final int puzzle = 2130837580;
        public static final int restart = 2130837581;
        public static final int sad_game_over = 2130837582;
        public static final int scores = 2130837583;
        public static final int screen1 = 2130837584;
        public static final int screen10 = 2130837585;
        public static final int screen10_pass = 2130837586;
        public static final int screen11 = 2130837587;
        public static final int screen11_pass = 2130837588;
        public static final int screen12 = 2130837589;
        public static final int screen12_pass = 2130837590;
        public static final int screen13 = 2130837591;
        public static final int screen13_pass = 2130837592;
        public static final int screen14 = 2130837593;
        public static final int screen14_pass = 2130837594;
        public static final int screen15 = 2130837595;
        public static final int screen15_pass = 2130837596;
        public static final int screen16 = 2130837597;
        public static final int screen16_pass = 2130837598;
        public static final int screen17 = 2130837599;
        public static final int screen17_pass = 2130837600;
        public static final int screen18 = 2130837601;
        public static final int screen18_pass = 2130837602;
        public static final int screen19 = 2130837603;
        public static final int screen19_pass = 2130837604;
        public static final int screen1_pass = 2130837605;
        public static final int screen2 = 2130837606;
        public static final int screen20 = 2130837607;
        public static final int screen20_pass = 2130837608;
        public static final int screen21 = 2130837609;
        public static final int screen21_pass = 2130837610;
        public static final int screen22 = 2130837611;
        public static final int screen22_pass = 2130837612;
        public static final int screen23 = 2130837613;
        public static final int screen23_pass = 2130837614;
        public static final int screen24 = 2130837615;
        public static final int screen24_pass = 2130837616;
        public static final int screen25 = 2130837617;
        public static final int screen25_pass = 2130837618;
        public static final int screen26 = 2130837619;
        public static final int screen26_pass = 2130837620;
        public static final int screen27 = 2130837621;
        public static final int screen27_pass = 2130837622;
        public static final int screen28 = 2130837623;
        public static final int screen28_pass = 2130837624;
        public static final int screen29 = 2130837625;
        public static final int screen29_pass = 2130837626;
        public static final int screen2_pass = 2130837627;
        public static final int screen3 = 2130837628;
        public static final int screen30 = 2130837629;
        public static final int screen30_pass = 2130837630;
        public static final int screen31 = 2130837631;
        public static final int screen31_pass = 2130837632;
        public static final int screen32 = 2130837633;
        public static final int screen32_pass = 2130837634;
        public static final int screen33 = 2130837635;
        public static final int screen33_pass = 2130837636;
        public static final int screen34 = 2130837637;
        public static final int screen34_pass = 2130837638;
        public static final int screen35 = 2130837639;
        public static final int screen35_pass = 2130837640;
        public static final int screen3_pass = 2130837641;
        public static final int screen4 = 2130837642;
        public static final int screen4_pass = 2130837643;
        public static final int screen5 = 2130837644;
        public static final int screen5_pass = 2130837645;
        public static final int screen6 = 2130837646;
        public static final int screen6_pass = 2130837647;
        public static final int screen7 = 2130837648;
        public static final int screen7_pass = 2130837649;
        public static final int screen8 = 2130837650;
        public static final int screen8_pass = 2130837651;
        public static final int screen9 = 2130837652;
        public static final int screen9_pass = 2130837653;
        public static final int settings = 2130837654;
        public static final int start_game = 2130837655;
        public static final int teleport_in = 2130837656;
        public static final int teleport_out = 2130837657;
        public static final int teleport_out_old = 2130837658;
        public static final int toy = 2130837659;
        public static final int undo = 2130837660;
        public static final int uspresidents = 2130837661;
        public static final int usstates = 2130837662;
        public static final int vv = 2130837663;
        public static final int walle = 2130837664;
        public static final int web = 2130837665;
        public static final int wood = 2130837666;
    }

    public static final class id {
        public static final int BANNER = 2131099648;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int LinearLayout01 = 2131099670;
        public static final int aboutButton = 2131099667;
        public static final int adOver = 2131099657;
        public static final int adView = 2131099659;
        public static final int bigImage = 2131099673;
        public static final int detail = 2131099663;
        public static final int exitButton = 2131099669;
        public static final int gallery = 2131099671;
        public static final int game_over_back = 2131099655;
        public static final int game_over_restart = 2131099656;
        public static final int globalRanking = 2131099666;
        public static final int helpButton = 2131099668;
        public static final int help_image_back = 2131099658;
        public static final int imageView1 = 2131099653;
        public static final int img = 2131099661;
        public static final int layoutswitcher = 2131099672;
        public static final int linearLayout1 = 2131099652;
        public static final int list = 2131099660;
        public static final int options = 2131099677;
        public static final int options_help = 2131099678;
        public static final int options_home = 2131099679;
        public static final int settingsButton = 2131099665;
        public static final int settings_image_back = 2131099675;
        public static final int settings_image_ok = 2131099676;
        public static final int startButton = 2131099664;
        public static final int textView1 = 2131099654;
        public static final int title = 2131099662;
        public static final int toggleVibrate = 2131099674;
    }

    public static final class layout {
        public static final int game_over = 2130903040;
        public static final int help = 2130903041;
        public static final int list = 2130903042;
        public static final int main_alter = 2130903043;
        public static final int more_games = 2130903044;
        public static final int select_level = 2130903045;
        public static final int settings = 2130903046;
    }

    public static final class menu {
        public static final int game_options_menu = 2131230720;
    }

    public static final class string {
        public static final int about_text = 2131165186;
        public static final int app_name = 2131165185;
        public static final int game_over = 2131165190;
        public static final int hello = 2131165184;
        public static final int help_text = 2131165187;
        public static final int slideMessage = 2131165189;
        public static final int timer_zero = 2131165188;
    }

    public static final class styleable {
        public static final int[] HelloGallery = {16842828};
        public static final int HelloGallery_android_galleryItemBackground = 0;
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
