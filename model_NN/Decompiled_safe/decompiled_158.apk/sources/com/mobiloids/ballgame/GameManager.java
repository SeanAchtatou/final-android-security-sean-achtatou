package com.mobiloids.ballgame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import com.mobiloids.ballgame.AccelDemo;

public class GameManager extends Thread {
    private Bitmap black;
    private Bitmap[] bubbles;
    private Context mContext;
    private Rect mField;
    private Paint mPaint;
    private boolean mRunning = false;
    private SurfaceHolder mSurfaceHolder;
    private AccelDemo.SimulationView view;
    private int viewHeight;
    private int viewWidth;

    public GameManager(SurfaceHolder surfaceHolder, Context context, AccelDemo.SimulationView simulation) {
        this.mSurfaceHolder = surfaceHolder;
        this.view = simulation;
        this.mPaint = new Paint();
        this.mPaint.setColor(-16776961);
        this.mPaint.setStrokeWidth(2.0f);
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mContext = context;
    }

    public void setRunning(boolean running) {
        this.mRunning = running;
    }

    public void run() {
        while (this.mRunning) {
            Canvas canvas = null;
            try {
                canvas = this.mSurfaceHolder.lockCanvas();
                synchronized (this.mSurfaceHolder) {
                    AccelDemo.SimulationView.ParticleSystem particleSystem = this.view.mParticleSystem;
                    long nanoTime = this.view.mSensorTimeStamp + (System.nanoTime() - this.view.mCpuTimeStamp);
                    float f = this.view.mSensorX;
                    float f2 = this.view.mSensorY;
                    canvas.drawBitmap(this.view.mWood, 0.0f, 0.0f, (Paint) null);
                    float xc = this.view.mXOrigin;
                    float yc = this.view.mYOrigin;
                    float xs = this.view.mMetersToPixelsX;
                    float ys = this.view.mMetersToPixelsY;
                    Bitmap bitmap = this.view.mBitmap;
                    if (particleSystem.isThereAnyTeleport()) {
                        float x = xc + (particleSystem.getTeleportInX() * xs);
                        float y = yc - (particleSystem.getTeleportInY() * ys);
                        canvas.drawBitmap(this.view.teleportInBitmap, x, y, (Paint) null);
                        float x2 = xc + (particleSystem.getTeleportOutX() * xs);
                        float y2 = yc - (particleSystem.getTeleportOutY() * ys);
                        canvas.drawBitmap(this.view.teleportOutBitmap, x2, y2, (Paint) null);
                    }
                    int count = particleSystem.getParticleCount();
                    for (int i = 0; i < count; i++) {
                        canvas.drawBitmap(bitmap, xc + (particleSystem.getPosX(i) * xs), yc - (particleSystem.getPosY(i) * ys), (Paint) null);
                    }
                    int targetsCount = particleSystem.getTargetsCount();
                    int firstTarget = particleSystem.getFirstUnfinishedTarget();
                    int vvCount = particleSystem.getVVCount();
                    int dangerCount = particleSystem.getDangersCount();
                    for (int i2 = firstTarget; i2 < targetsCount; i2++) {
                        canvas.drawBitmap(this.view.mTargetsBitmaps[i2], xc + (particleSystem.getTargetX(i2) * xs), yc - (particleSystem.getTargetY(i2) * ys), (Paint) null);
                    }
                    for (int i3 = 0; i3 < dangerCount; i3++) {
                        float x3 = xc + (particleSystem.getDangersX(i3) * xs);
                        float y3 = yc - (particleSystem.getDangersY(i3) * ys);
                        canvas.drawBitmap(this.view.mDangerBitmap, x3, y3, (Paint) null);
                    }
                    for (int i4 = 0; i4 < vvCount; i4++) {
                        if (this.view.mParticleSystem.mAvailableVVs[i4]) {
                            float x4 = xc + (particleSystem.getVVX(i4) * xs);
                            float y4 = yc - (particleSystem.getVVY(i4) * ys);
                            canvas.drawBitmap(this.view.mVVBitmap, x4, y4, (Paint) null);
                        }
                    }
                }
                if (canvas != null) {
                    this.mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                if (canvas != null) {
                    this.mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Throwable th) {
                if (canvas != null) {
                    this.mSurfaceHolder.unlockCanvasAndPost(canvas);
                }
                throw th;
            }
        }
    }
}
