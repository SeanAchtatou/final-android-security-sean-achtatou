package com.mobiloids.ballgame;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ToggleButton;

public class Settings extends Activity {
    private static String MY_PREFS = "SETTINGS";
    private static String SOUND_PREF = "SOUND";
    /* access modifiers changed from: private */
    public static String VIBRATE_PREF = "VIBRATE";
    private static final boolean default_value = true;
    private boolean isSoundOn;
    private boolean isVibrateOn;
    private int mode = 0;
    ToggleButton music;
    /* access modifiers changed from: private */
    public SharedPreferences pref;
    ToggleButton sound;
    /* access modifiers changed from: private */
    public Activity thisActivity;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.thisActivity = this;
        setContentView((int) R.layout.settings);
        setTitle("Settings");
        ((ImageView) findViewById(R.id.settings_image_back)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Settings.this.thisActivity.finish();
            }
        });
        ((ImageView) findViewById(R.id.settings_image_ok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences.Editor editor = Settings.this.pref.edit();
                editor.putBoolean(Settings.VIBRATE_PREF, Settings.this.music.isChecked());
                editor.commit();
                Settings.this.thisActivity.finish();
            }
        });
        this.music = (ToggleButton) findViewById(R.id.toggleVibrate);
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        this.isVibrateOn = this.pref.getBoolean(VIBRATE_PREF, true);
        this.music.setChecked(this.isVibrateOn);
    }
}
