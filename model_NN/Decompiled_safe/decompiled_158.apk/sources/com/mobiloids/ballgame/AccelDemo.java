package com.mobiloids.ballgame;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import java.util.Timer;
import java.util.TimerTask;

public class AccelDemo extends Activity {
    public static final String LEVEL = "LEVEL";
    public static String MY_PREFS = "SETTINGS";
    public static String OPEN_LEVELS = "OPEN LEVELS";
    public static String PASSED_FIRST_TIME = "PASSED_FIRST_TIME";
    public static final String PASSED_LEVEL_FLAGS = "PASSED_LEVEL_FLAGS";
    public static final String SHOWN_LEVEL = "SHOWN LEVEL";
    public static String SOUND_PREF = "SOUND";
    public static String VIBRATE_PREF = "VIBRATE";
    public static final boolean default_value = true;
    public static Object lock = new Object();
    public int TIMER_INTERVAL = 15;
    public int VIBRATION_INTERVAL = 10;
    public boolean alreadyRestarted;
    int gameLevelNumber;
    public Level[] gameLevels;
    public final int gameLevelsCount;
    public boolean isBegun = false;
    public boolean isSoundOn;
    public boolean isVibrateOn;
    int[][][] levelTables;
    int[][] levelsDimensions;
    public Display mDisplay;
    public PowerManager mPowerManager;
    public SensorManager mSensorManager;
    public SimulationView mSimulationView;
    public Vibrator mVibrateManager;
    public PowerManager.WakeLock mWakeLock;
    public WindowManager mWindowManager;
    public int mode = 0;
    public SharedPreferences pref;
    Timer t;
    Activity thisActivity;
    GoogleAnalyticsTracker tracker;
    TimerTask tt;

    public AccelDemo() {
        int[] iArr = new int[12];
        iArr[0] = -1;
        int[] iArr2 = new int[12];
        iArr2[3] = 1;
        iArr2[4] = 10;
        iArr2[11] = 9;
        int[] iArr3 = new int[12];
        iArr3[4] = 2;
        iArr3[5] = 11;
        iArr3[10] = 8;
        int[] iArr4 = new int[12];
        iArr4[5] = 3;
        iArr4[6] = 12;
        iArr4[9] = 7;
        int[] iArr5 = new int[12];
        iArr5[6] = 4;
        iArr5[7] = 13;
        iArr5[8] = 6;
        int[] iArr6 = new int[12];
        iArr6[7] = 5;
        int[][] iArr7 = {iArr, new int[12], new int[12], iArr2, iArr3, iArr4, iArr5, iArr6};
        int[] iArr8 = new int[12];
        iArr8[0] = 2;
        iArr8[4] = -5;
        iArr8[9] = -5;
        iArr8[11] = -1;
        int[] iArr9 = new int[12];
        iArr9[2] = -5;
        iArr9[4] = -5;
        iArr9[6] = -5;
        iArr9[9] = -5;
        int[] iArr10 = new int[12];
        iArr10[2] = -5;
        iArr10[4] = -5;
        iArr10[6] = -5;
        iArr10[9] = -5;
        int[] iArr11 = new int[12];
        iArr11[2] = -5;
        iArr11[4] = -5;
        iArr11[6] = -5;
        iArr11[9] = -5;
        iArr11[10] = -5;
        int[] iArr12 = new int[12];
        iArr12[0] = 1;
        iArr12[2] = -5;
        iArr12[4] = -5;
        iArr12[6] = -5;
        iArr12[10] = -5;
        int[] iArr13 = new int[12];
        iArr13[2] = -5;
        iArr13[4] = -5;
        iArr13[6] = -5;
        iArr13[10] = -5;
        int[] iArr14 = new int[12];
        iArr14[2] = -5;
        iArr14[4] = -5;
        iArr14[6] = -5;
        iArr14[8] = -5;
        iArr14[9] = -5;
        iArr14[10] = -5;
        int[] iArr15 = new int[12];
        iArr15[2] = -5;
        iArr15[6] = -5;
        int[][] iArr16 = {iArr8, iArr9, iArr10, iArr11, iArr12, iArr13, iArr14, iArr15};
        int[] iArr17 = new int[12];
        iArr17[0] = -1;
        iArr17[11] = -2;
        int[] iArr18 = new int[12];
        iArr18[9] = -5;
        iArr18[10] = -5;
        iArr18[11] = -5;
        int[] iArr19 = new int[12];
        iArr19[9] = -5;
        iArr19[10] = 1;
        int[] iArr20 = new int[12];
        iArr20[0] = 3;
        iArr20[9] = 2;
        iArr20[11] = -3;
        int[][] iArr21 = {iArr17, new int[12], new int[12], new int[12], new int[12], iArr18, iArr19, iArr20};
        int[] iArr22 = new int[12];
        iArr22[0] = 1;
        iArr22[3] = -5;
        iArr22[8] = -5;
        iArr22[11] = -1;
        int[] iArr23 = new int[12];
        iArr23[3] = -5;
        iArr23[8] = -5;
        int[] iArr24 = new int[12];
        iArr24[5] = -4;
        iArr24[8] = -5;
        int[] iArr25 = new int[12];
        iArr25[3] = -5;
        iArr25[8] = -5;
        int[] iArr26 = new int[12];
        iArr26[3] = -5;
        iArr26[8] = -5;
        int[] iArr27 = new int[12];
        iArr27[3] = -5;
        iArr27[8] = -4;
        int[] iArr28 = new int[12];
        iArr28[3] = -5;
        iArr28[8] = -4;
        int[] iArr29 = new int[12];
        iArr29[3] = -5;
        iArr29[8] = -4;
        int[][] iArr30 = {iArr22, iArr23, iArr24, iArr25, iArr26, iArr27, iArr28, iArr29};
        int[] iArr31 = new int[12];
        iArr31[0] = 6;
        iArr31[1] = -5;
        iArr31[6] = -5;
        iArr31[7] = -5;
        iArr31[8] = -5;
        iArr31[11] = -1;
        int[] iArr32 = new int[12];
        iArr32[1] = -5;
        iArr32[3] = -5;
        iArr32[4] = -5;
        iArr32[5] = 4;
        iArr32[6] = -5;
        iArr32[7] = 2;
        iArr32[8] = -5;
        iArr32[9] = 5;
        int[] iArr33 = new int[12];
        iArr33[1] = -5;
        iArr33[4] = -5;
        iArr33[5] = -5;
        iArr33[6] = -5;
        iArr33[8] = -5;
        iArr33[9] = -5;
        iArr33[10] = -5;
        int[] iArr34 = new int[12];
        iArr34[10] = -5;
        int[] iArr35 = new int[12];
        iArr35[1] = -5;
        iArr35[2] = -5;
        iArr35[3] = -5;
        iArr35[5] = -5;
        iArr35[6] = -5;
        iArr35[7] = -5;
        iArr35[9] = -5;
        iArr35[10] = -5;
        int[] iArr36 = new int[12];
        iArr36[1] = -5;
        iArr36[2] = 1;
        iArr36[3] = -5;
        iArr36[5] = -5;
        iArr36[6] = 3;
        iArr36[7] = -5;
        iArr36[9] = -5;
        int[] iArr37 = new int[12];
        iArr37[1] = -5;
        iArr37[3] = -5;
        iArr37[4] = -5;
        iArr37[5] = -5;
        iArr37[7] = -5;
        iArr37[8] = -5;
        iArr37[9] = -5;
        int[][] iArr38 = {iArr31, iArr32, iArr33, iArr34, iArr35, iArr36, iArr37, new int[12]};
        int[] iArr39 = new int[12];
        iArr39[7] = -5;
        iArr39[8] = -5;
        iArr39[9] = 5;
        iArr39[11] = -1;
        int[] iArr40 = new int[12];
        iArr40[6] = -5;
        iArr40[8] = -5;
        iArr40[10] = -5;
        int[] iArr41 = new int[12];
        iArr41[1] = -5;
        iArr41[2] = -5;
        iArr41[3] = -5;
        iArr41[6] = -5;
        iArr41[8] = -5;
        iArr41[9] = 3;
        iArr41[10] = -5;
        int[] iArr42 = new int[12];
        iArr42[1] = 2;
        iArr42[3] = -5;
        iArr42[6] = -5;
        iArr42[8] = -5;
        iArr42[9] = -3;
        iArr42[10] = -5;
        int[] iArr43 = new int[12];
        iArr43[1] = 1;
        iArr43[3] = -5;
        iArr43[6] = -5;
        iArr43[8] = -5;
        iArr43[10] = -5;
        int[] iArr44 = new int[12];
        iArr44[0] = -2;
        iArr44[3] = -5;
        iArr44[4] = -5;
        iArr44[6] = -5;
        iArr44[8] = -5;
        iArr44[9] = 4;
        iArr44[10] = -5;
        int[] iArr45 = new int[12];
        iArr45[0] = -5;
        iArr45[1] = -5;
        iArr45[2] = -5;
        iArr45[3] = -5;
        iArr45[8] = -5;
        iArr45[9] = -5;
        iArr45[10] = -5;
        int[][] iArr46 = {iArr39, iArr40, iArr41, iArr42, iArr43, iArr44, iArr45, new int[12]};
        int[] iArr47 = new int[12];
        iArr47[11] = -1;
        int[] iArr48 = new int[12];
        iArr48[0] = -5;
        iArr48[1] = -5;
        int[] iArr49 = new int[12];
        iArr49[1] = -5;
        iArr49[2] = -5;
        int[] iArr50 = new int[12];
        iArr50[2] = 3;
        iArr50[3] = -5;
        iArr50[6] = -5;
        iArr50[7] = -2;
        iArr50[8] = -5;
        int[] iArr51 = new int[12];
        iArr51[0] = -3;
        iArr51[3] = -5;
        iArr51[4] = -5;
        iArr51[6] = -5;
        iArr51[7] = -5;
        iArr51[8] = -5;
        int[] iArr52 = new int[12];
        iArr52[4] = -5;
        iArr52[5] = -5;
        iArr52[9] = -5;
        iArr52[10] = -5;
        int[] iArr53 = new int[12];
        iArr53[0] = 1;
        iArr53[5] = -5;
        iArr53[6] = -5;
        iArr53[7] = 5;
        iArr53[9] = -5;
        int[] iArr54 = new int[12];
        iArr54[4] = 2;
        iArr54[6] = -5;
        iArr54[9] = -5;
        iArr54[11] = 4;
        int[][] iArr55 = {iArr47, iArr48, iArr49, iArr50, iArr51, iArr52, iArr53, iArr54};
        int[] iArr56 = new int[12];
        iArr56[11] = -1;
        int[] iArr57 = new int[12];
        iArr57[3] = -5;
        iArr57[5] = -5;
        iArr57[7] = -5;
        iArr57[9] = -5;
        iArr57[11] = -2;
        int[] iArr58 = new int[12];
        iArr58[3] = -5;
        iArr58[4] = 6;
        iArr58[5] = -5;
        iArr58[6] = 7;
        iArr58[7] = -5;
        iArr58[8] = 8;
        iArr58[9] = -5;
        iArr58[10] = 9;
        int[] iArr59 = new int[12];
        iArr59[2] = 10;
        iArr59[3] = -5;
        iArr59[4] = 1;
        iArr59[5] = -5;
        iArr59[6] = 2;
        iArr59[7] = -5;
        iArr59[8] = 3;
        iArr59[9] = -5;
        iArr59[10] = 4;
        iArr59[11] = -5;
        int[] iArr60 = new int[12];
        iArr60[3] = -5;
        iArr60[5] = -5;
        iArr60[7] = -5;
        iArr60[9] = -5;
        iArr60[11] = -5;
        int[] iArr61 = new int[12];
        iArr61[1] = -4;
        iArr61[11] = -3;
        int[][] iArr62 = {iArr56, new int[12], iArr57, iArr58, new int[]{5, -4, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5}, iArr59, iArr60, iArr61};
        int[] iArr63 = new int[12];
        iArr63[0] = -1;
        iArr63[1] = 5;
        iArr63[2] = -5;
        iArr63[3] = -2;
        iArr63[4] = -5;
        iArr63[7] = -5;
        int[] iArr64 = new int[12];
        iArr64[2] = -5;
        iArr64[3] = -4;
        iArr64[4] = -5;
        iArr64[7] = -5;
        int[] iArr65 = new int[12];
        iArr65[0] = 4;
        iArr65[2] = -5;
        iArr65[7] = -5;
        iArr65[11] = -5;
        int[] iArr66 = new int[12];
        iArr66[2] = -5;
        iArr66[4] = -5;
        iArr66[6] = -5;
        iArr66[10] = -5;
        int[] iArr67 = new int[12];
        iArr67[2] = -5;
        iArr67[4] = -5;
        iArr67[6] = -5;
        iArr67[11] = 6;
        int[] iArr68 = new int[12];
        iArr68[2] = -5;
        iArr68[4] = -5;
        iArr68[6] = -5;
        iArr68[9] = -5;
        iArr68[11] = -5;
        int[] iArr69 = new int[12];
        iArr69[2] = -5;
        iArr69[4] = -5;
        iArr69[6] = -5;
        iArr69[9] = -5;
        iArr69[10] = 1;
        iArr69[11] = -4;
        int[] iArr70 = new int[12];
        iArr70[0] = 3;
        iArr70[4] = -5;
        iArr70[9] = 2;
        iArr70[11] = -3;
        int[][] iArr71 = {iArr63, iArr64, iArr65, iArr66, iArr67, iArr68, iArr69, iArr70};
        int[] iArr72 = new int[12];
        iArr72[8] = -5;
        iArr72[9] = -5;
        iArr72[10] = -5;
        iArr72[11] = 1;
        int[] iArr73 = new int[12];
        iArr73[8] = -5;
        iArr73[9] = -5;
        iArr73[11] = -5;
        int[] iArr74 = new int[12];
        iArr74[8] = -5;
        iArr74[10] = -5;
        iArr74[11] = -5;
        int[] iArr75 = new int[12];
        iArr75[9] = -5;
        iArr75[10] = -5;
        iArr75[11] = -5;
        int[] iArr76 = new int[12];
        iArr76[0] = -1;
        int[][] iArr77 = {iArr72, iArr73, iArr74, iArr75, new int[12], new int[12], new int[12], iArr76};
        int[] iArr78 = new int[12];
        iArr78[0] = -2;
        iArr78[1] = -5;
        iArr78[11] = -1;
        int[] iArr79 = new int[12];
        iArr79[0] = -5;
        iArr79[9] = -5;
        int[] iArr80 = new int[12];
        iArr80[0] = -5;
        iArr80[9] = -5;
        int[] iArr81 = new int[12];
        iArr81[0] = -5;
        iArr81[2] = -5;
        iArr81[3] = -5;
        iArr81[6] = -5;
        iArr81[7] = -5;
        iArr81[9] = -5;
        iArr81[10] = -5;
        iArr81[11] = -4;
        int[] iArr82 = new int[12];
        iArr82[0] = -5;
        iArr82[2] = 1;
        iArr82[3] = 2;
        iArr82[4] = -5;
        iArr82[6] = 3;
        iArr82[7] = 4;
        iArr82[9] = -5;
        int[] iArr83 = new int[12];
        iArr83[0] = -5;
        iArr83[1] = 6;
        iArr83[2] = -5;
        iArr83[3] = -5;
        iArr83[4] = -5;
        iArr83[5] = -5;
        iArr83[6] = -5;
        iArr83[7] = -5;
        iArr83[9] = -5;
        iArr83[11] = 8;
        int[] iArr84 = new int[12];
        iArr84[0] = -5;
        iArr84[4] = 5;
        iArr84[9] = -5;
        iArr84[10] = -4;
        int[][] iArr85 = {iArr78, iArr79, iArr80, iArr81, iArr82, iArr83, iArr84, new int[]{-5, -5, -5, -5, -5, -5, -5, -5, -5, -5, 7, -3}};
        int[] iArr86 = new int[12];
        iArr86[9] = -5;
        iArr86[10] = -4;
        iArr86[11] = -1;
        int[] iArr87 = new int[12];
        iArr87[0] = -5;
        iArr87[1] = -5;
        iArr87[2] = -5;
        iArr87[9] = -5;
        int[] iArr88 = new int[12];
        iArr88[0] = -5;
        iArr88[1] = 2;
        iArr88[9] = -4;
        iArr88[10] = -5;
        iArr88[11] = -5;
        int[] iArr89 = new int[12];
        iArr89[0] = -5;
        iArr89[1] = -5;
        iArr89[2] = -5;
        iArr89[7] = -5;
        int[] iArr90 = new int[12];
        iArr90[2] = -5;
        iArr90[7] = -5;
        iArr90[8] = -5;
        int[] iArr91 = new int[12];
        iArr91[0] = 3;
        iArr91[2] = -5;
        iArr91[5] = -5;
        iArr91[6] = -5;
        iArr91[7] = 1;
        iArr91[8] = -5;
        int[] iArr92 = new int[12];
        iArr92[2] = -5;
        iArr92[6] = -5;
        iArr92[7] = -5;
        iArr92[8] = -5;
        int[] iArr93 = new int[12];
        iArr93[0] = -4;
        int[][] iArr94 = {iArr86, iArr87, iArr88, iArr89, iArr90, iArr91, iArr92, iArr93};
        int[] iArr95 = new int[12];
        iArr95[0] = -5;
        iArr95[11] = -1;
        int[] iArr96 = new int[12];
        iArr96[0] = -2;
        iArr96[1] = -5;
        iArr96[2] = -4;
        iArr96[3] = -5;
        iArr96[4] = -5;
        iArr96[5] = -5;
        iArr96[6] = -4;
        iArr96[7] = -5;
        iArr96[9] = -5;
        iArr96[11] = -5;
        int[] iArr97 = new int[12];
        iArr97[1] = -5;
        iArr97[3] = -5;
        iArr97[5] = -5;
        iArr97[7] = -5;
        iArr97[8] = -5;
        iArr97[9] = -5;
        iArr97[11] = -5;
        int[] iArr98 = new int[12];
        iArr98[1] = -5;
        iArr98[3] = -5;
        iArr98[5] = 3;
        iArr98[7] = -5;
        iArr98[9] = -5;
        iArr98[11] = -5;
        int[] iArr99 = new int[12];
        iArr99[1] = -5;
        iArr99[3] = -5;
        iArr99[5] = -5;
        iArr99[6] = -3;
        iArr99[7] = -5;
        iArr99[9] = -5;
        iArr99[11] = -5;
        int[] iArr100 = new int[12];
        iArr100[1] = -5;
        iArr100[2] = 4;
        iArr100[3] = -5;
        iArr100[5] = -5;
        iArr100[6] = 2;
        iArr100[7] = -5;
        iArr100[9] = -5;
        iArr100[10] = 1;
        iArr100[11] = -5;
        int[] iArr101 = new int[12];
        iArr101[1] = -5;
        iArr101[2] = -5;
        iArr101[3] = -5;
        iArr101[5] = -5;
        iArr101[6] = -5;
        iArr101[7] = -5;
        iArr101[9] = -5;
        iArr101[10] = -5;
        iArr101[11] = -5;
        int[][] iArr102 = {iArr95, iArr96, iArr97, iArr98, iArr99, iArr100, iArr101, new int[12]};
        int[] iArr103 = new int[12];
        iArr103[0] = -2;
        iArr103[3] = -5;
        iArr103[7] = -5;
        iArr103[11] = -1;
        int[] iArr104 = new int[12];
        iArr104[2] = -5;
        iArr104[3] = 2;
        iArr104[4] = -5;
        iArr104[6] = -5;
        iArr104[7] = 4;
        iArr104[8] = -5;
        int[] iArr105 = new int[12];
        iArr105[3] = -5;
        iArr105[4] = 1;
        iArr105[5] = -5;
        iArr105[6] = 3;
        iArr105[7] = -5;
        int[] iArr106 = new int[12];
        iArr106[4] = -5;
        iArr106[6] = -5;
        int[] iArr107 = new int[12];
        iArr107[3] = -5;
        iArr107[5] = -3;
        iArr107[7] = -5;
        int[] iArr108 = new int[12];
        iArr108[2] = -5;
        iArr108[4] = -5;
        iArr108[6] = -5;
        iArr108[8] = -5;
        int[] iArr109 = new int[12];
        iArr109[1] = -5;
        iArr109[2] = 6;
        iArr109[3] = -5;
        iArr109[7] = -5;
        iArr109[8] = 5;
        iArr109[9] = -5;
        int[] iArr110 = new int[12];
        iArr110[2] = -5;
        iArr110[8] = -5;
        int[][] iArr111 = {iArr103, iArr104, iArr105, iArr106, iArr107, iArr108, iArr109, iArr110};
        int[] iArr112 = new int[12];
        iArr112[0] = -5;
        iArr112[1] = 13;
        iArr112[3] = -5;
        iArr112[4] = 14;
        iArr112[5] = -5;
        iArr112[7] = 15;
        iArr112[10] = 16;
        iArr112[11] = -1;
        int[] iArr113 = new int[12];
        iArr113[0] = -5;
        iArr113[1] = -5;
        iArr113[2] = -5;
        iArr113[9] = -4;
        iArr113[11] = -2;
        int[] iArr114 = new int[12];
        iArr114[0] = -5;
        iArr114[1] = 9;
        iArr114[4] = 10;
        iArr114[7] = 11;
        iArr114[9] = -5;
        iArr114[10] = 12;
        iArr114[11] = -5;
        int[] iArr115 = new int[12];
        iArr115[0] = -5;
        iArr115[1] = -5;
        iArr115[2] = -5;
        iArr115[3] = -5;
        iArr115[4] = -5;
        iArr115[5] = -5;
        iArr115[6] = -5;
        iArr115[7] = -5;
        iArr115[8] = -5;
        int[] iArr116 = new int[12];
        iArr116[0] = -5;
        iArr116[1] = 5;
        iArr116[2] = -5;
        iArr116[4] = 6;
        iArr116[7] = 7;
        iArr116[10] = 8;
        int[] iArr117 = new int[12];
        iArr117[0] = -5;
        iArr117[2] = -3;
        int[][] iArr118 = {iArr112, iArr113, iArr114, iArr115, iArr116, iArr117, new int[]{-5, 1, -5, -5, 2, -5, -5, 3, -5, -5, 4, -5}, new int[]{-5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5, -5}};
        int[] iArr119 = new int[12];
        iArr119[11] = -1;
        int[] iArr120 = new int[12];
        iArr120[2] = -5;
        iArr120[3] = -5;
        iArr120[4] = -5;
        iArr120[5] = -5;
        iArr120[6] = -5;
        iArr120[7] = -5;
        iArr120[8] = -5;
        iArr120[9] = -5;
        int[] iArr121 = new int[12];
        iArr121[2] = -5;
        iArr121[3] = -5;
        iArr121[4] = -3;
        iArr121[5] = -5;
        iArr121[6] = -5;
        iArr121[7] = -5;
        iArr121[8] = -5;
        iArr121[9] = -5;
        int[] iArr122 = new int[12];
        iArr122[2] = -5;
        iArr122[3] = -5;
        iArr122[5] = -5;
        iArr122[6] = -5;
        iArr122[7] = -2;
        iArr122[8] = -5;
        iArr122[9] = -5;
        int[] iArr123 = new int[12];
        iArr123[2] = -5;
        iArr123[3] = 2;
        iArr123[4] = 1;
        iArr123[5] = 3;
        iArr123[6] = -5;
        iArr123[7] = -5;
        iArr123[9] = -5;
        iArr123[10] = -5;
        int[] iArr124 = new int[12];
        iArr124[2] = -5;
        iArr124[3] = 6;
        iArr124[4] = 5;
        iArr124[5] = 4;
        iArr124[6] = -5;
        iArr124[7] = -5;
        iArr124[8] = -5;
        iArr124[10] = -5;
        iArr124[11] = -5;
        int[] iArr125 = new int[12];
        iArr125[8] = -5;
        iArr125[9] = -5;
        iArr125[11] = -5;
        int[] iArr126 = new int[12];
        iArr126[10] = -5;
        iArr126[11] = 7;
        int[][] iArr127 = {iArr119, iArr120, iArr121, iArr122, iArr123, iArr124, iArr125, iArr126};
        int[] iArr128 = new int[12];
        iArr128[0] = 2;
        iArr128[1] = -5;
        iArr128[11] = -1;
        int[] iArr129 = new int[12];
        iArr129[0] = -5;
        int[] iArr130 = new int[12];
        iArr130[4] = -5;
        iArr130[6] = -5;
        iArr130[7] = -5;
        iArr130[8] = -5;
        iArr130[9] = -5;
        iArr130[10] = -4;
        iArr130[11] = -5;
        int[] iArr131 = new int[12];
        iArr131[0] = -5;
        iArr131[1] = -5;
        iArr131[4] = -5;
        iArr131[6] = -5;
        iArr131[7] = 6;
        iArr131[8] = -5;
        iArr131[9] = -5;
        iArr131[11] = -3;
        int[] iArr132 = new int[12];
        iArr132[0] = -2;
        iArr132[4] = -5;
        iArr132[5] = 4;
        iArr132[6] = -5;
        iArr132[7] = 5;
        iArr132[8] = -5;
        iArr132[9] = -5;
        iArr132[10] = 7;
        int[] iArr133 = new int[12];
        iArr133[0] = -5;
        iArr133[1] = -5;
        iArr133[4] = -5;
        iArr133[5] = -5;
        iArr133[6] = -5;
        iArr133[8] = -5;
        iArr133[9] = -5;
        iArr133[10] = -5;
        iArr133[11] = -5;
        int[] iArr134 = new int[12];
        iArr134[0] = -5;
        iArr134[11] = -5;
        int[] iArr135 = new int[12];
        iArr135[0] = 3;
        iArr135[1] = -5;
        iArr135[10] = -5;
        iArr135[11] = 1;
        int[][] iArr136 = {iArr128, iArr129, iArr130, iArr131, iArr132, iArr133, iArr134, iArr135};
        int[] iArr137 = new int[12];
        iArr137[0] = -1;
        int[] iArr138 = new int[12];
        iArr138[6] = 3;
        int[] iArr139 = new int[12];
        iArr139[1] = -5;
        iArr139[2] = -5;
        iArr139[3] = -5;
        iArr139[4] = -5;
        iArr139[5] = -5;
        iArr139[6] = -5;
        iArr139[7] = -5;
        iArr139[8] = -5;
        iArr139[9] = -5;
        iArr139[10] = -5;
        int[] iArr140 = new int[12];
        iArr140[1] = -4;
        iArr140[6] = 1;
        iArr140[10] = -4;
        int[] iArr141 = new int[12];
        iArr141[1] = -5;
        iArr141[2] = -5;
        iArr141[3] = -5;
        iArr141[4] = -5;
        iArr141[5] = -5;
        iArr141[6] = -5;
        iArr141[7] = -5;
        iArr141[8] = -5;
        iArr141[9] = -5;
        iArr141[10] = -5;
        int[] iArr142 = new int[12];
        iArr142[6] = 2;
        int[][] iArr143 = {iArr137, iArr138, new int[12], iArr139, iArr140, iArr141, new int[12], iArr142};
        int[] iArr144 = new int[12];
        iArr144[0] = -1;
        iArr144[7] = -5;
        iArr144[8] = -2;
        iArr144[11] = -5;
        int[] iArr145 = new int[12];
        iArr145[7] = -5;
        iArr145[8] = -5;
        iArr145[9] = -5;
        iArr145[11] = -5;
        int[] iArr146 = new int[12];
        iArr146[1] = -5;
        iArr146[2] = 1;
        iArr146[3] = -5;
        iArr146[9] = -5;
        iArr146[11] = -5;
        int[] iArr147 = new int[12];
        iArr147[2] = -5;
        iArr147[3] = 2;
        iArr147[4] = -5;
        iArr147[9] = -5;
        iArr147[11] = -5;
        int[] iArr148 = new int[12];
        iArr148[3] = -5;
        iArr148[4] = 3;
        iArr148[5] = -5;
        iArr148[9] = -5;
        iArr148[10] = 9;
        iArr148[11] = -5;
        int[] iArr149 = new int[12];
        iArr149[0] = -5;
        iArr149[1] = -5;
        iArr149[2] = -5;
        iArr149[4] = -5;
        iArr149[5] = 4;
        iArr149[6] = -5;
        iArr149[8] = -5;
        iArr149[9] = 8;
        iArr149[10] = -5;
        int[] iArr150 = new int[12];
        iArr150[1] = 10;
        iArr150[2] = -5;
        iArr150[5] = -5;
        iArr150[6] = 5;
        iArr150[7] = -5;
        iArr150[8] = 7;
        iArr150[9] = -5;
        int[] iArr151 = new int[12];
        iArr151[0] = -3;
        iArr151[2] = -5;
        iArr151[6] = -5;
        iArr151[7] = 6;
        iArr151[8] = -5;
        int[][] iArr152 = {iArr144, iArr145, iArr146, iArr147, iArr148, iArr149, iArr150, iArr151};
        int[] iArr153 = new int[12];
        iArr153[0] = -5;
        iArr153[2] = -5;
        iArr153[5] = -1;
        iArr153[8] = -5;
        iArr153[10] = -5;
        int[] iArr154 = new int[12];
        iArr154[0] = -5;
        iArr154[2] = -5;
        iArr154[8] = -5;
        iArr154[10] = -5;
        int[] iArr155 = new int[12];
        iArr155[0] = -5;
        iArr155[1] = 1;
        iArr155[2] = -5;
        iArr155[8] = -5;
        iArr155[9] = 2;
        iArr155[10] = -5;
        int[] iArr156 = new int[12];
        iArr156[1] = -5;
        iArr156[2] = 3;
        iArr156[3] = -5;
        iArr156[7] = -5;
        iArr156[8] = 4;
        iArr156[9] = -5;
        int[] iArr157 = new int[12];
        iArr157[2] = -5;
        iArr157[3] = 5;
        iArr157[4] = -5;
        iArr157[6] = -5;
        iArr157[7] = 6;
        iArr157[8] = -5;
        int[] iArr158 = new int[12];
        iArr158[3] = -5;
        iArr158[4] = 7;
        iArr158[5] = -5;
        iArr158[6] = 8;
        iArr158[7] = -5;
        int[] iArr159 = new int[12];
        iArr159[4] = -5;
        iArr159[5] = 9;
        iArr159[6] = -5;
        int[][] iArr160 = {new int[12], iArr153, iArr154, iArr155, iArr156, iArr157, iArr158, iArr159};
        int[] iArr161 = new int[12];
        iArr161[0] = -3;
        iArr161[5] = 2;
        iArr161[10] = 10;
        iArr161[11] = -2;
        int[] iArr162 = new int[12];
        iArr162[5] = 4;
        iArr162[10] = -4;
        iArr162[11] = 11;
        int[] iArr163 = new int[12];
        iArr163[5] = 3;
        int[] iArr164 = new int[12];
        iArr164[5] = 5;
        int[] iArr165 = new int[12];
        iArr165[5] = 6;
        int[] iArr166 = new int[12];
        iArr166[5] = 8;
        int[] iArr167 = new int[12];
        iArr167[1] = 1;
        iArr167[5] = 7;
        int[] iArr168 = new int[12];
        iArr168[0] = -4;
        iArr168[5] = 9;
        iArr168[11] = -1;
        int[][] iArr169 = {iArr161, iArr162, iArr163, iArr164, iArr165, iArr166, iArr167, iArr168};
        int[] iArr170 = new int[12];
        iArr170[0] = -1;
        iArr170[1] = -5;
        iArr170[2] = -5;
        iArr170[3] = -5;
        iArr170[4] = -5;
        iArr170[5] = -5;
        iArr170[6] = -5;
        iArr170[7] = -5;
        iArr170[8] = -5;
        int[] iArr171 = new int[12];
        iArr171[1] = -5;
        iArr171[5] = -5;
        iArr171[6] = 1;
        iArr171[8] = -5;
        iArr171[10] = -5;
        int[] iArr172 = new int[12];
        iArr172[1] = -5;
        iArr172[3] = -5;
        iArr172[5] = -5;
        iArr172[6] = -5;
        iArr172[8] = -5;
        iArr172[10] = -5;
        int[] iArr173 = new int[12];
        iArr173[1] = -5;
        iArr173[3] = -5;
        iArr173[8] = -5;
        iArr173[10] = -5;
        int[] iArr174 = new int[12];
        iArr174[1] = -5;
        iArr174[3] = -5;
        iArr174[4] = -5;
        iArr174[5] = -5;
        iArr174[6] = -5;
        iArr174[7] = -5;
        iArr174[8] = -5;
        iArr174[10] = -5;
        int[] iArr175 = new int[12];
        iArr175[1] = -5;
        iArr175[10] = -5;
        int[] iArr176 = new int[12];
        iArr176[1] = -5;
        iArr176[2] = -5;
        iArr176[3] = -5;
        iArr176[4] = -5;
        iArr176[5] = -5;
        iArr176[6] = -5;
        iArr176[7] = -5;
        iArr176[8] = -5;
        iArr176[9] = -5;
        iArr176[10] = -5;
        int[] iArr177 = new int[12];
        iArr177[1] = -4;
        int[][] iArr178 = {iArr170, iArr171, iArr172, iArr173, iArr174, iArr175, iArr176, iArr177};
        int[] iArr179 = new int[12];
        iArr179[0] = -1;
        iArr179[11] = 7;
        int[] iArr180 = new int[12];
        iArr180[1] = -5;
        iArr180[2] = -5;
        iArr180[3] = -5;
        iArr180[4] = -5;
        iArr180[5] = -5;
        iArr180[6] = -5;
        iArr180[7] = -5;
        iArr180[8] = -5;
        iArr180[9] = -5;
        iArr180[10] = -5;
        int[] iArr181 = new int[12];
        iArr181[1] = -4;
        iArr181[2] = 1;
        iArr181[9] = 2;
        iArr181[10] = -4;
        int[] iArr182 = new int[12];
        iArr182[1] = -5;
        iArr182[2] = -5;
        iArr182[3] = -5;
        iArr182[4] = -5;
        iArr182[5] = -5;
        iArr182[6] = -5;
        iArr182[7] = -5;
        iArr182[8] = -5;
        iArr182[9] = -5;
        iArr182[10] = -5;
        int[] iArr183 = new int[12];
        iArr183[1] = -4;
        iArr183[2] = 4;
        iArr183[9] = 3;
        iArr183[10] = -4;
        int[] iArr184 = new int[12];
        iArr184[1] = -5;
        iArr184[2] = -5;
        iArr184[3] = -5;
        iArr184[4] = -5;
        iArr184[5] = -5;
        iArr184[6] = -5;
        iArr184[7] = -5;
        iArr184[8] = -5;
        iArr184[9] = -5;
        iArr184[10] = -5;
        int[] iArr185 = new int[12];
        iArr185[1] = -4;
        iArr185[2] = 5;
        iArr185[9] = 6;
        iArr185[10] = -4;
        int[] iArr186 = new int[12];
        iArr186[1] = -5;
        iArr186[2] = -5;
        iArr186[3] = -5;
        iArr186[4] = -5;
        iArr186[5] = -5;
        iArr186[6] = -5;
        iArr186[7] = -5;
        iArr186[8] = -5;
        iArr186[9] = -5;
        iArr186[10] = -5;
        int[][] iArr187 = {iArr179, iArr180, iArr181, iArr182, iArr183, iArr184, iArr185, iArr186};
        int[] iArr188 = new int[12];
        iArr188[0] = -5;
        iArr188[2] = -5;
        iArr188[4] = -5;
        iArr188[6] = -5;
        iArr188[8] = -5;
        iArr188[10] = -5;
        iArr188[11] = -1;
        int[] iArr189 = new int[12];
        iArr189[1] = -5;
        iArr189[3] = -5;
        iArr189[5] = -5;
        iArr189[7] = -5;
        iArr189[9] = -5;
        iArr189[11] = -5;
        int[] iArr190 = new int[12];
        iArr190[0] = -5;
        iArr190[2] = -5;
        iArr190[4] = -5;
        iArr190[6] = -5;
        iArr190[8] = -5;
        iArr190[10] = -5;
        int[] iArr191 = new int[12];
        iArr191[1] = -5;
        iArr191[3] = -5;
        iArr191[5] = -5;
        iArr191[7] = -5;
        iArr191[9] = -5;
        iArr191[11] = -5;
        int[] iArr192 = new int[12];
        iArr192[0] = -5;
        iArr192[2] = -5;
        iArr192[4] = -5;
        iArr192[6] = -5;
        iArr192[8] = -5;
        iArr192[10] = -5;
        int[] iArr193 = new int[12];
        iArr193[1] = -5;
        iArr193[3] = -5;
        iArr193[5] = -5;
        iArr193[7] = -5;
        iArr193[9] = -5;
        iArr193[11] = -5;
        int[] iArr194 = new int[12];
        iArr194[0] = -5;
        iArr194[2] = -5;
        iArr194[4] = -5;
        iArr194[6] = -5;
        iArr194[8] = -5;
        iArr194[10] = -5;
        int[] iArr195 = new int[12];
        iArr195[0] = 1;
        iArr195[1] = -5;
        iArr195[3] = -5;
        iArr195[5] = -5;
        iArr195[7] = -5;
        iArr195[9] = -5;
        iArr195[11] = -5;
        int[][] iArr196 = {iArr188, iArr189, iArr190, iArr191, iArr192, iArr193, iArr194, iArr195};
        int[] iArr197 = new int[12];
        iArr197[0] = -1;
        iArr197[1] = -5;
        iArr197[3] = -5;
        iArr197[5] = -5;
        iArr197[7] = -5;
        iArr197[8] = 1;
        iArr197[10] = -5;
        iArr197[11] = -2;
        int[] iArr198 = new int[12];
        iArr198[1] = -5;
        iArr198[3] = -4;
        iArr198[5] = -5;
        iArr198[7] = -5;
        iArr198[11] = -5;
        int[] iArr199 = new int[12];
        iArr199[1] = -5;
        iArr199[3] = -5;
        iArr199[5] = -5;
        iArr199[7] = -5;
        int[] iArr200 = new int[12];
        iArr200[1] = -5;
        iArr200[3] = -5;
        iArr200[5] = -5;
        iArr200[7] = -5;
        iArr200[9] = -5;
        iArr200[10] = -5;
        int[] iArr201 = new int[12];
        iArr201[1] = -5;
        iArr201[3] = -5;
        iArr201[5] = -4;
        iArr201[7] = -5;
        int[] iArr202 = new int[12];
        iArr202[1] = -4;
        iArr202[3] = -5;
        iArr202[5] = -5;
        iArr202[7] = -5;
        iArr202[8] = -5;
        iArr202[9] = -5;
        iArr202[10] = -4;
        iArr202[11] = -5;
        int[] iArr203 = new int[12];
        iArr203[0] = 2;
        iArr203[1] = -5;
        iArr203[3] = -5;
        iArr203[5] = -5;
        iArr203[7] = -4;
        int[] iArr204 = new int[12];
        iArr204[0] = -3;
        iArr204[1] = -5;
        iArr204[3] = -5;
        iArr204[5] = -5;
        iArr204[7] = -5;
        int[][] iArr205 = {iArr197, iArr198, iArr199, iArr200, iArr201, iArr202, iArr203, iArr204};
        int[] iArr206 = new int[12];
        iArr206[0] = -1;
        iArr206[1] = 1;
        iArr206[6] = -4;
        iArr206[11] = 2;
        int[] iArr207 = new int[12];
        iArr207[1] = -5;
        iArr207[2] = -5;
        iArr207[3] = -5;
        iArr207[4] = -5;
        iArr207[5] = -5;
        iArr207[6] = -5;
        iArr207[7] = -5;
        iArr207[8] = -5;
        iArr207[9] = -5;
        iArr207[10] = -5;
        int[] iArr208 = new int[12];
        iArr208[1] = -5;
        iArr208[10] = -5;
        int[] iArr209 = new int[12];
        iArr209[4] = 5;
        iArr209[8] = 6;
        iArr209[10] = -5;
        int[] iArr210 = new int[12];
        iArr210[10] = -5;
        int[] iArr211 = new int[12];
        iArr211[1] = -5;
        iArr211[10] = -5;
        int[] iArr212 = new int[12];
        iArr212[1] = -5;
        iArr212[2] = -5;
        iArr212[3] = -5;
        iArr212[4] = -5;
        iArr212[5] = -5;
        iArr212[6] = -5;
        iArr212[7] = -5;
        iArr212[8] = -5;
        iArr212[9] = -5;
        iArr212[10] = -5;
        int[] iArr213 = new int[12];
        iArr213[0] = 4;
        iArr213[11] = 3;
        int[][] iArr214 = {iArr206, iArr207, iArr208, iArr209, iArr210, iArr211, iArr212, iArr213};
        int[] iArr215 = new int[12];
        iArr215[0] = -1;
        iArr215[4] = -5;
        iArr215[6] = 3;
        iArr215[9] = -5;
        int[] iArr216 = new int[12];
        iArr216[3] = -5;
        iArr216[8] = -5;
        int[] iArr217 = new int[12];
        iArr217[2] = -5;
        iArr217[7] = -5;
        iArr217[11] = -5;
        int[] iArr218 = new int[12];
        iArr218[0] = 1;
        iArr218[1] = -5;
        iArr218[6] = -5;
        iArr218[10] = -5;
        iArr218[11] = 4;
        int[] iArr219 = new int[12];
        iArr219[0] = -5;
        iArr219[5] = -5;
        iArr219[9] = -5;
        int[] iArr220 = new int[12];
        iArr220[4] = -5;
        iArr220[8] = -5;
        int[] iArr221 = new int[12];
        iArr221[3] = -5;
        iArr221[7] = -5;
        int[] iArr222 = new int[12];
        iArr222[2] = -5;
        iArr222[5] = 2;
        iArr222[6] = -5;
        int[][] iArr223 = {iArr215, iArr216, iArr217, iArr218, iArr219, iArr220, iArr221, iArr222};
        int[] iArr224 = new int[12];
        iArr224[0] = -2;
        iArr224[1] = -5;
        iArr224[4] = -4;
        iArr224[7] = -5;
        int[] iArr225 = new int[12];
        iArr225[0] = -5;
        iArr225[2] = -5;
        iArr225[4] = -5;
        iArr225[7] = -5;
        iArr225[9] = -5;
        iArr225[10] = -5;
        int[] iArr226 = new int[12];
        iArr226[0] = -5;
        iArr226[1] = -5;
        iArr226[2] = -5;
        iArr226[4] = -5;
        iArr226[7] = -5;
        iArr226[10] = -5;
        int[] iArr227 = new int[12];
        iArr227[4] = -5;
        iArr227[7] = -5;
        iArr227[8] = -5;
        iArr227[10] = -5;
        int[] iArr228 = new int[12];
        iArr228[4] = -5;
        iArr228[7] = -5;
        iArr228[10] = -5;
        int[] iArr229 = new int[12];
        iArr229[4] = -5;
        iArr229[7] = -5;
        iArr229[9] = -5;
        iArr229[10] = -5;
        int[] iArr230 = new int[12];
        iArr230[4] = -5;
        iArr230[9] = -5;
        iArr230[10] = -5;
        iArr230[11] = 1;
        int[] iArr231 = new int[12];
        iArr231[0] = -1;
        iArr231[4] = -5;
        iArr231[9] = -5;
        iArr231[10] = -5;
        iArr231[11] = -3;
        int[][] iArr232 = {iArr224, iArr225, iArr226, iArr227, iArr228, iArr229, iArr230, iArr231};
        int[] iArr233 = new int[12];
        iArr233[0] = 2;
        iArr233[4] = 3;
        iArr233[5] = -5;
        iArr233[6] = 9;
        iArr233[7] = -5;
        iArr233[8] = -5;
        iArr233[11] = -5;
        int[] iArr234 = new int[12];
        iArr234[2] = -5;
        iArr234[4] = -5;
        iArr234[5] = -5;
        iArr234[8] = 10;
        iArr234[10] = -5;
        iArr234[11] = -5;
        int[] iArr235 = new int[12];
        iArr235[0] = -5;
        iArr235[2] = -5;
        iArr235[3] = 4;
        iArr235[5] = -5;
        iArr235[6] = -5;
        iArr235[8] = -5;
        iArr235[11] = -5;
        int[] iArr236 = new int[12];
        iArr236[2] = -5;
        iArr236[3] = -5;
        iArr236[5] = -5;
        iArr236[6] = 8;
        iArr236[8] = -5;
        iArr236[9] = -5;
        iArr236[10] = 11;
        iArr236[11] = -5;
        int[] iArr237 = new int[12];
        iArr237[0] = 1;
        iArr237[1] = -5;
        iArr237[2] = -5;
        iArr237[4] = 5;
        iArr237[5] = -5;
        iArr237[7] = -5;
        iArr237[8] = -5;
        iArr237[11] = -5;
        int[] iArr238 = new int[12];
        iArr238[2] = -5;
        iArr238[4] = -5;
        iArr238[5] = -5;
        iArr238[7] = 7;
        iArr238[8] = -5;
        iArr238[9] = 12;
        iArr238[10] = -5;
        iArr238[11] = -5;
        int[] iArr239 = new int[12];
        iArr239[0] = -5;
        iArr239[2] = -5;
        iArr239[4] = 6;
        iArr239[5] = -5;
        iArr239[6] = -5;
        iArr239[8] = -5;
        iArr239[11] = -5;
        int[] iArr240 = new int[12];
        iArr240[0] = -1;
        iArr240[2] = -5;
        iArr240[3] = -5;
        iArr240[8] = -5;
        iArr240[9] = -5;
        iArr240[11] = 13;
        int[][] iArr241 = {iArr233, iArr234, iArr235, iArr236, iArr237, iArr238, iArr239, iArr240};
        int[] iArr242 = new int[12];
        iArr242[4] = -5;
        iArr242[6] = -5;
        iArr242[11] = -1;
        int[] iArr243 = new int[12];
        iArr243[5] = -5;
        iArr243[7] = -5;
        int[] iArr244 = new int[12];
        iArr244[4] = -5;
        iArr244[6] = -5;
        int[] iArr245 = new int[12];
        iArr245[0] = -5;
        iArr245[1] = 2;
        iArr245[2] = -5;
        iArr245[5] = -5;
        iArr245[7] = -5;
        iArr245[9] = -5;
        iArr245[10] = 1;
        iArr245[11] = -5;
        int[] iArr246 = new int[12];
        iArr246[0] = -5;
        iArr246[1] = 4;
        iArr246[2] = -5;
        iArr246[4] = -5;
        iArr246[6] = -5;
        iArr246[9] = -5;
        iArr246[10] = 3;
        iArr246[11] = -5;
        int[] iArr247 = new int[12];
        iArr247[0] = -5;
        iArr247[1] = 6;
        iArr247[2] = -5;
        iArr247[5] = -5;
        iArr247[7] = -5;
        iArr247[9] = -5;
        iArr247[10] = 5;
        iArr247[11] = -5;
        int[] iArr248 = new int[12];
        iArr248[0] = -5;
        iArr248[1] = 8;
        iArr248[2] = -5;
        iArr248[4] = -5;
        iArr248[6] = -5;
        iArr248[9] = -5;
        iArr248[10] = 7;
        iArr248[11] = -5;
        int[] iArr249 = new int[12];
        iArr249[0] = -5;
        iArr249[1] = -5;
        iArr249[2] = -5;
        iArr249[5] = -5;
        iArr249[7] = -5;
        iArr249[9] = -5;
        iArr249[10] = -5;
        iArr249[11] = -5;
        int[][] iArr250 = {iArr242, iArr243, iArr244, iArr245, iArr246, iArr247, iArr248, iArr249};
        int[] iArr251 = new int[12];
        iArr251[0] = -2;
        iArr251[8] = -5;
        iArr251[9] = 5;
        int[] iArr252 = new int[12];
        iArr252[8] = -5;
        iArr252[9] = 3;
        int[] iArr253 = new int[12];
        iArr253[8] = -5;
        iArr253[9] = -5;
        int[] iArr254 = new int[12];
        iArr254[1] = -5;
        iArr254[2] = -5;
        iArr254[3] = -5;
        iArr254[4] = -5;
        iArr254[5] = -5;
        iArr254[6] = -5;
        iArr254[7] = -5;
        iArr254[8] = -5;
        iArr254[9] = 2;
        int[] iArr255 = new int[12];
        iArr255[0] = -4;
        iArr255[1] = -5;
        iArr255[2] = 4;
        iArr255[8] = -5;
        iArr255[9] = -5;
        int[] iArr256 = new int[12];
        iArr256[1] = -5;
        iArr256[2] = -5;
        iArr256[3] = -5;
        iArr256[4] = -5;
        iArr256[5] = -5;
        iArr256[6] = -5;
        iArr256[8] = -5;
        iArr256[9] = 1;
        int[] iArr257 = new int[12];
        iArr257[6] = -5;
        iArr257[8] = -5;
        iArr257[9] = -5;
        int[] iArr258 = new int[12];
        iArr258[0] = -1;
        iArr258[6] = -5;
        iArr258[11] = -3;
        int[][] iArr259 = {iArr251, iArr252, iArr253, iArr254, iArr255, iArr256, iArr257, iArr258};
        int[] iArr260 = new int[12];
        iArr260[2] = -5;
        iArr260[3] = 10;
        iArr260[4] = -5;
        iArr260[6] = -5;
        int[] iArr261 = new int[12];
        iArr261[0] = 4;
        iArr261[3] = -5;
        iArr261[6] = -2;
        iArr261[7] = -5;
        iArr261[10] = -5;
        int[] iArr262 = new int[12];
        iArr262[0] = 3;
        iArr262[1] = -5;
        iArr262[7] = -5;
        iArr262[8] = -5;
        iArr262[9] = -5;
        iArr262[10] = 6;
        int[] iArr263 = new int[12];
        iArr263[1] = -5;
        iArr263[2] = -5;
        iArr263[8] = -5;
        iArr263[9] = -5;
        int[] iArr264 = new int[12];
        iArr264[2] = 2;
        iArr264[3] = -5;
        iArr264[7] = -5;
        iArr264[8] = 8;
        int[] iArr265 = new int[12];
        iArr265[0] = -4;
        iArr265[3] = -5;
        iArr265[4] = -5;
        iArr265[6] = -5;
        iArr265[7] = -5;
        int[] iArr266 = new int[12];
        iArr266[0] = 9;
        iArr266[1] = -4;
        iArr266[4] = 1;
        iArr266[5] = -5;
        iArr266[6] = 7;
        int[] iArr267 = new int[12];
        iArr267[0] = -3;
        iArr267[1] = -4;
        iArr267[5] = 5;
        iArr267[11] = -1;
        int[][] iArr268 = {iArr260, iArr261, iArr262, iArr263, iArr264, iArr265, iArr266, iArr267};
        int[] iArr269 = new int[12];
        iArr269[2] = -4;
        iArr269[4] = -5;
        iArr269[5] = 2;
        iArr269[6] = -5;
        iArr269[8] = -4;
        iArr269[10] = -5;
        int[] iArr270 = new int[12];
        iArr270[2] = -5;
        iArr270[4] = -5;
        iArr270[6] = -5;
        iArr270[8] = -5;
        iArr270[10] = -5;
        int[] iArr271 = new int[12];
        iArr271[2] = -5;
        iArr271[3] = 1;
        iArr271[4] = -5;
        iArr271[6] = -5;
        iArr271[7] = 3;
        iArr271[8] = -5;
        iArr271[10] = -5;
        int[] iArr272 = new int[12];
        iArr272[2] = -5;
        iArr272[4] = -5;
        iArr272[6] = -5;
        iArr272[8] = -5;
        iArr272[9] = 4;
        iArr272[10] = -5;
        int[] iArr273 = new int[12];
        iArr273[2] = -5;
        iArr273[4] = -5;
        iArr273[6] = -5;
        iArr273[7] = -4;
        iArr273[8] = -5;
        iArr273[10] = -5;
        int[] iArr274 = new int[12];
        iArr274[2] = -5;
        iArr274[4] = -5;
        iArr274[6] = -5;
        iArr274[8] = -5;
        iArr274[10] = -5;
        iArr274[11] = 5;
        int[] iArr275 = new int[12];
        iArr275[2] = -5;
        iArr275[4] = -5;
        iArr275[6] = -5;
        iArr275[8] = -5;
        iArr275[10] = -5;
        int[] iArr276 = new int[12];
        iArr276[0] = -1;
        iArr276[2] = -5;
        iArr276[4] = -4;
        iArr276[8] = -5;
        iArr276[10] = -4;
        int[][] iArr277 = {iArr269, iArr270, iArr271, iArr272, iArr273, iArr274, iArr275, iArr276};
        int[] iArr278 = new int[12];
        iArr278[6] = 7;
        iArr278[7] = -5;
        int[] iArr279 = new int[12];
        iArr279[6] = -5;
        int[] iArr280 = new int[12];
        iArr280[4] = 5;
        iArr280[5] = -5;
        iArr280[6] = 6;
        int[] iArr281 = new int[12];
        iArr281[4] = -5;
        int[] iArr282 = new int[12];
        iArr282[2] = 3;
        iArr282[3] = -5;
        iArr282[4] = 4;
        iArr282[9] = -5;
        iArr282[10] = -5;
        iArr282[11] = -4;
        int[] iArr283 = new int[12];
        iArr283[2] = -5;
        iArr283[9] = -5;
        int[] iArr284 = new int[12];
        iArr284[0] = 1;
        iArr284[1] = -5;
        iArr284[2] = 2;
        iArr284[9] = -5;
        int[] iArr285 = new int[12];
        iArr285[0] = -5;
        iArr285[5] = -4;
        iArr285[9] = -5;
        iArr285[11] = -1;
        int[][] iArr286 = {iArr278, iArr279, iArr280, iArr281, iArr282, iArr283, iArr284, iArr285};
        int[] iArr287 = new int[12];
        iArr287[9] = -2;
        iArr287[11] = -1;
        int[] iArr288 = new int[12];
        iArr288[5] = -5;
        iArr288[6] = -5;
        iArr288[7] = -5;
        int[] iArr289 = new int[12];
        iArr289[5] = -5;
        iArr289[6] = 5;
        iArr289[7] = -5;
        int[] iArr290 = new int[12];
        iArr290[4] = 6;
        iArr290[5] = -5;
        iArr290[6] = -4;
        iArr290[7] = -5;
        iArr290[8] = 4;
        int[] iArr291 = new int[12];
        iArr291[4] = -5;
        iArr291[8] = -5;
        int[] iArr292 = new int[12];
        iArr292[2] = 8;
        iArr292[3] = -5;
        iArr292[4] = 7;
        iArr292[5] = -5;
        iArr292[6] = -5;
        iArr292[7] = -5;
        iArr292[8] = 3;
        iArr292[9] = -5;
        iArr292[10] = 2;
        int[] iArr293 = new int[12];
        iArr293[2] = -5;
        iArr293[3] = -4;
        iArr293[5] = -5;
        iArr293[6] = -3;
        iArr293[7] = -5;
        iArr293[9] = -4;
        iArr293[10] = -5;
        int[] iArr294 = new int[12];
        iArr294[1] = -5;
        iArr294[2] = 9;
        iArr294[5] = -4;
        iArr294[6] = -4;
        iArr294[7] = -5;
        iArr294[10] = 1;
        iArr294[11] = -5;
        this.levelTables = new int[][][]{iArr7, iArr16, iArr21, iArr30, iArr38, iArr46, iArr55, iArr62, iArr71, iArr77, iArr85, iArr94, iArr102, iArr111, iArr118, iArr127, iArr136, iArr143, iArr152, iArr160, iArr169, iArr178, iArr187, iArr196, iArr205, iArr214, iArr223, iArr232, iArr241, iArr250, iArr259, iArr268, iArr277, iArr286, new int[][]{iArr287, iArr288, iArr289, iArr290, iArr291, iArr292, iArr293, iArr294}};
        this.levelsDimensions = new int[][]{new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}, new int[]{8, 12}};
        this.gameLevelsCount = 35;
    }

    public void initLevels() {
        this.gameLevels = new Level[35];
        for (int i = 0; i < 35; i++) {
            this.gameLevels[i] = new Level(this.levelTables[i], this.levelsDimensions[i][0], this.levelsDimensions[i][1]);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initLevels();
        this.gameLevelNumber = getIntent().getExtras().getInt(LEVEL);
        Log.i("STARTING LEVEL", new StringBuilder(String.valueOf(this.gameLevelNumber + 1)).toString());
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-22925047-4", 15, this);
        this.tracker.trackPageView("/LevelPlayed" + this.gameLevelNumber + 1);
        this.thisActivity = this;
        this.alreadyRestarted = false;
        this.mVibrateManager = (Vibrator) getSystemService("vibrator");
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.mPowerManager = (PowerManager) getSystemService("power");
        this.mWindowManager = (WindowManager) getSystemService("window");
        this.mDisplay = this.mWindowManager.getDefaultDisplay();
        this.mWakeLock = this.mPowerManager.newWakeLock(10, getClass().getName());
        this.mSimulationView = new SimulationView(this, this.gameLevels[this.gameLevelNumber]);
        setContentView(this.mSimulationView);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Log.i("ACCEL--------------------------------", "STARTING");
        this.pref = getSharedPreferences(MY_PREFS, this.mode);
        this.isSoundOn = this.pref.getBoolean(SOUND_PREF, true);
        this.isVibrateOn = this.pref.getBoolean(VIBRATE_PREF, true);
        this.mWakeLock.acquire();
        this.mSimulationView.startSimulation();
        if (this.isBegun) {
            this.tt = new TimerTask() {
                public void run() {
                    AccelDemo.this.mSimulationView.updating();
                }
            };
            this.t = new Timer("timerThread", false);
            this.t.schedule(this.tt, 0, (long) this.TIMER_INTERVAL);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Log.i("ACCEL--------------------------------", "PAUSING");
        if (this.t != null) {
            this.t.cancel();
        }
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
        }
        this.mPowerManager.userActivity(SystemClock.uptimeMillis(), false);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.game_options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.options:
                startActivity(new Intent(this, Settings.class));
                return true;
            case R.id.options_help:
                startActivity(new Intent(this, Help.class));
                return true;
            case R.id.options_home:
                this.thisActivity.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Log.i("ACCEL--------------------------------", "DESTROYING");
        this.mSimulationView.mBitmap.recycle();
        this.mSimulationView.mVVBitmap.recycle();
        this.mSimulationView.teleportInBitmap.recycle();
        this.mSimulationView.teleportOutBitmap.recycle();
        this.mSimulationView.mDangerBitmap.recycle();
        this.mSimulationView.mWood.recycle();
        for (Bitmap recycle : this.mSimulationView.mTargetsBitmaps) {
            recycle.recycle();
        }
        this.mSimulationView.mBitmap = null;
        this.mSimulationView.mVVBitmap = null;
        this.mSimulationView.teleportInBitmap = null;
        this.mSimulationView.teleportOutBitmap = null;
        this.mSimulationView.mDangerBitmap = null;
        this.mSimulationView.mWood = null;
        for (int i = 0; i < this.mSimulationView.mTargetsBitmaps.length; i++) {
            this.mSimulationView.mTargetsBitmaps[i] = null;
        }
        this.mSimulationView.stopSimulation();
        this.tracker.stop();
    }

    class SimulationView extends SurfaceView implements SensorEventListener, SurfaceHolder.Callback {
        public static final float sBallDiameter = 0.004f;
        public static final float sBallDiameter2 = 1.6000002E-5f;
        public static final float sFriction = 0.997f;
        public final int MAX_TARGETS_COUNT;
        public Level gameLevel;
        public int height;
        public Sensor mAccelerometer;
        public Bitmap mBitmap;
        public Context mContext;
        public long mCpuTimeStamp;
        public Bitmap mDangerBitmap;
        public float mHorizontalBound;
        public float mLastDeltaT;
        public long mLastT;
        public float mMetersToPixelsX;
        public float mMetersToPixelsY;
        public final ParticleSystem mParticleSystem;
        public long mSensorTimeStamp;
        public float mSensorX;
        public float mSensorY;
        public SurfaceHolder mSurfaceHolder;
        public Integer[] mTaregetsIds;
        public Bitmap[] mTargetsBitmaps;
        public GameManager mThread;
        public Bitmap mVVBitmap;
        public float mVerticalBound;
        public Bitmap mWood;
        public float mXDpi;
        public float mXOrigin;
        public float mYDpi;
        public float mYOrigin;
        public Bitmap teleportInBitmap;
        public Bitmap teleportOutBitmap;
        public View thisView;
        public int width;

        class Particle {
            public float mAccelX;
            public float mAccelY;
            public float mLastPosX;
            public float mLastPosY;
            public float mOneMinusFriction = 0.003000021f;
            public float mPosX;
            public float mPosY;
            public float mVelX;
            public float mVelY;

            public float get_x() {
                return this.mPosX;
            }

            public float get_y() {
                return this.mPosY;
            }

            Particle(float x, float y) {
                this.mPosX = x;
                this.mPosY = y;
            }

            public void computePhysics(float sx, float sy, float dT) {
                float dTdT = dT * dT;
                this.mPosX = this.mPosX + (this.mVelX * dT * this.mOneMinusFriction) + ((this.mAccelX * dTdT) / 2.0f);
                this.mPosY = this.mPosY + (this.mVelY * dT * this.mOneMinusFriction) + ((this.mAccelY * dTdT) / 2.0f);
                this.mVelX += this.mAccelX * dT;
                this.mVelY += this.mAccelY * dT;
                this.mAccelX = (-sx) * 1.1f;
                this.mAccelY = (-sy) * 1.1f;
            }

            public void computePhysics(float sx, float sy, float dT, float dTC) {
                float dTdT = dT * dT;
                this.mLastPosX = this.mPosX;
                this.mLastPosY = this.mPosY;
                this.mPosX = this.mPosX + (this.mOneMinusFriction * dTC * (this.mPosX - this.mLastPosX)) + (this.mAccelX * dTdT);
                this.mPosY = this.mPosY + (this.mOneMinusFriction * dTC * (this.mPosY - this.mLastPosY)) + (this.mAccelY * dTdT);
                this.mAccelX = (-sx) * 0.5f;
                this.mAccelY = (-sy) * 0.5f;
            }

            public void resolveCollisionWithBounds() {
                float xmax = SimulationView.this.mHorizontalBound;
                float ymax = SimulationView.this.mVerticalBound;
                float x = this.mPosX;
                float y = this.mPosY;
                if (x > xmax) {
                    this.mPosX = xmax;
                    this.mVelX = 0.0f;
                } else if (x < (-xmax)) {
                    this.mPosX = -xmax;
                    this.mVelX = 0.0f;
                }
                if (y > ymax) {
                    this.mPosY = ymax;
                    this.mVelY = 0.0f;
                } else if (y < (-ymax)) {
                    this.mPosY = -ymax;
                    this.mVelY = 0.0f;
                }
            }
        }

        class Target {
            public int mNumber;
            public float mPosX;
            public float mPosY;

            Target(float x, float y, int num) {
                this.mPosX = x;
                this.mPosY = y;
                this.mNumber = num;
            }

            public float get_x() {
                return this.mPosX;
            }

            public float get_y() {
                return this.mPosY;
            }
        }

        class ParticleSystem {
            int NUM_DANGERS = 0;
            int NUM_PARTICLES = 1;
            int NUM_TARGETS = 0;
            int NUM_VV = 0;
            public int finishedTargets = 0;
            public boolean[] mAvailableVVs;
            public Particle[] mBalls = new Particle[this.NUM_PARTICLES];
            public Target[] mDangers;
            public Target[] mTargets;
            public Target[] mVVs;
            public Particle startBall;
            public Target teleportIn = null;
            public Target teleportOut = null;
            public int vvDx = 1;
            public int vvDy = 1;

            public void restartLevel() {
                AccelDemo.this.alreadyRestarted = true;
                Log.i("ACCEL--------------------------------", "RESTARTING");
                this.vvDx = 1;
                this.vvDy = 1;
                Intent intent = new Intent(AccelDemo.this, GameOver.class);
                intent.putExtra(AccelDemo.LEVEL, AccelDemo.this.gameLevelNumber);
                AccelDemo.this.startActivity(intent);
            }

            public void levelFinished() {
                boolean firstTimePassed;
                AccelDemo.this.tracker.trackPageView("/LevelFinished" + AccelDemo.this.gameLevelNumber + 1);
                AccelDemo.this.pref = AccelDemo.this.getSharedPreferences(AccelDemo.MY_PREFS, AccelDemo.this.mode);
                long prevPassedLevels = AccelDemo.this.pref.getLong(AccelDemo.PASSED_LEVEL_FLAGS, 0);
                if ((((long) (1 << AccelDemo.this.gameLevelNumber)) & prevPassedLevels) == 0) {
                    firstTimePassed = true;
                } else {
                    firstTimePassed = false;
                }
                long prevPassedLevels2 = prevPassedLevels | ((long) (1 << AccelDemo.this.gameLevelNumber));
                SharedPreferences.Editor editor = AccelDemo.this.pref.edit();
                editor.putLong(AccelDemo.PASSED_LEVEL_FLAGS, prevPassedLevels2);
                editor.commit();
                Log.i("------------FLAGS-----------", new StringBuilder(String.valueOf(prevPassedLevels2)).toString());
                this.vvDx = 1;
                this.vvDy = 1;
                Intent intent = new Intent(AccelDemo.this, SelectLevelAfterGame.class);
                intent.putExtra(AccelDemo.SHOWN_LEVEL, AccelDemo.this.gameLevelNumber + 1);
                intent.putExtra(AccelDemo.PASSED_FIRST_TIME, firstTimePassed);
                AccelDemo.this.startActivity(intent);
                AccelDemo.this.thisActivity.finish();
            }

            public void initParticleSystem(int[][] table, float tableH, float tableW, float screenH, float screenW) {
                int vvIndex;
                int vvIndex2;
                this.mBalls = new Particle[this.NUM_PARTICLES];
                float startX = -screenH;
                float startY = -screenW;
                float dx = (2.0f * screenH) / (tableH - 1.0f);
                float dy = (2.0f * screenW) / (tableW - 1.0f);
                for (int i = 0; ((float) i) < tableH; i++) {
                    for (int j = 0; ((float) j) < tableW; j++) {
                        if (table[i][j] > 0) {
                            this.NUM_TARGETS = this.NUM_TARGETS + 1;
                        }
                        if (table[i][j] == -4) {
                            this.NUM_VV = this.NUM_VV + 1;
                        }
                        if (table[i][j] == -5) {
                            this.NUM_DANGERS = this.NUM_DANGERS + 1;
                        }
                        if (table[i][j] == -1) {
                            this.mBalls[0] = new Particle((((float) i) * dx) + startX, (((float) j) * dy) + startY);
                            this.startBall = new Particle((((float) i) * dx) + startX, (((float) j) * dy) + startY);
                        }
                        if (table[i][j] == -2) {
                            this.teleportIn = new Target((((float) i) * dx) + startX, (((float) j) * dy) + startY, 0);
                        }
                        if (table[i][j] == -3) {
                            this.teleportOut = new Target((((float) i) * dx) + startX, (((float) j) * dy) + startY, 0);
                        }
                    }
                }
                this.mVVs = new Target[this.NUM_VV];
                this.mAvailableVVs = new boolean[this.NUM_VV];
                for (int i2 = 0; i2 < this.NUM_VV; i2++) {
                    this.mAvailableVVs[i2] = true;
                }
                int vvIndex3 = 0;
                int i3 = 0;
                while (((float) i3) < tableH) {
                    int j2 = 0;
                    while (true) {
                        vvIndex2 = vvIndex;
                        if (((float) j2) >= tableW) {
                            break;
                        }
                        if (table[i3][j2] == -4) {
                            vvIndex = vvIndex2 + 1;
                            this.mVVs[vvIndex2] = new Target((((float) i3) * dx) + startX, (((float) j2) * dy) + startY, vvIndex - 1);
                        } else {
                            vvIndex = vvIndex2;
                        }
                        j2++;
                    }
                    i3++;
                    vvIndex3 = vvIndex2;
                }
                this.mTargets = new Target[this.NUM_TARGETS];
                for (int targetI = 1; targetI <= this.NUM_TARGETS; targetI++) {
                    for (int i4 = 0; ((float) i4) < tableH; i4++) {
                        for (int j3 = 0; ((float) j3) < tableW; j3++) {
                            if (table[i4][j3] == targetI) {
                                this.mTargets[targetI - 1] = new Target((((float) i4) * dx) + startX, (((float) j3) * dy) + startY, targetI - 1);
                            }
                        }
                    }
                }
                this.mDangers = new Target[this.NUM_DANGERS];
                Log.i("DANGERS COUNT", new StringBuilder(String.valueOf(this.NUM_DANGERS)).toString());
                int dangerI = 0;
                for (int i5 = 0; ((float) i5) < tableH; i5++) {
                    for (int j4 = 0; ((float) j4) < tableW; j4++) {
                        if (table[i5][j4] == -5) {
                            this.mDangers[dangerI] = new Target((((float) i5) * dx) + startX, (((float) j4) * dy) + startY, dangerI);
                            dangerI++;
                        }
                    }
                }
            }

            ParticleSystem() {
                for (int i = 0; i < this.mBalls.length; i++) {
                    this.mBalls[i] = new Particle(0.0f, 0.0f);
                }
                int w = SimulationView.this.thisView.getWidth();
                int h = SimulationView.this.thisView.getHeight();
                float f = ((((float) w) / SimulationView.this.mMetersToPixelsX) - 0.004f) * 0.5f;
                float f2 = ((((float) h) / SimulationView.this.mMetersToPixelsY) - 0.004f) * 0.5f;
            }

            public void updatePositions(float sx, float sy, long timestamp) {
                long t = timestamp;
                if (SimulationView.this.mLastT != 0) {
                    float dT = ((float) (t - SimulationView.this.mLastT)) * 1.0E-9f;
                    if (SimulationView.this.mLastDeltaT != 0.0f) {
                        for (Particle ball : this.mBalls) {
                            ball.computePhysics(sx, sy, dT);
                        }
                    }
                    SimulationView.this.mLastDeltaT = dT;
                }
                SimulationView.this.mLastT = t;
            }

            public void update(float sx, float sy, long now) {
                Target ball;
                updatePositions(sx, sy, now);
                int targetsCount = this.mTargets.length;
                int VVsCount = this.mVVs.length;
                for (Particle curr : this.mBalls) {
                    for (int j = this.finishedTargets; j < targetsCount; j++) {
                        if (this.mTargets[j] != null) {
                            Target ball2 = this.mTargets[j];
                            float dx = ball2.mPosX - curr.mPosX;
                            float dy = ball2.mPosY - curr.mPosY;
                            if ((dx * dx) + (dy * dy) > 1.6000002E-5f) {
                                continue;
                            } else {
                                if (AccelDemo.this.isVibrateOn) {
                                    AccelDemo.this.mVibrateManager.vibrate((long) AccelDemo.this.VIBRATION_INTERVAL);
                                }
                                if (j == this.finishedTargets) {
                                    this.finishedTargets++;
                                    if (this.finishedTargets == targetsCount) {
                                        AccelDemo.this.alreadyRestarted = true;
                                        levelFinished();
                                        return;
                                    }
                                    return;
                                } else if (!AccelDemo.this.alreadyRestarted) {
                                    if (AccelDemo.this.isVibrateOn) {
                                        AccelDemo.this.mVibrateManager.vibrate((long) AccelDemo.this.VIBRATION_INTERVAL);
                                    }
                                    restartLevel();
                                    AccelDemo.this.thisActivity.finish();
                                }
                            }
                        }
                    }
                    if (!(this.teleportIn == null || this.teleportOut == null)) {
                        float dx2 = this.teleportIn.mPosX - curr.mPosX;
                        float dy2 = this.teleportIn.mPosY - curr.mPosY;
                        if ((dx2 * dx2) + (dy2 * dy2) <= 1.6000002E-5f) {
                            if (AccelDemo.this.isVibrateOn) {
                                AccelDemo.this.mVibrateManager.vibrate((long) (AccelDemo.this.VIBRATION_INTERVAL * 2));
                            }
                            curr.mPosX = this.teleportOut.get_x();
                            curr.mPosY = this.teleportOut.get_y();
                            curr.mAccelX = 0.0f;
                            curr.mAccelY = 0.0f;
                            curr.mLastPosX = curr.mPosX;
                            curr.mLastPosY = curr.mPosY;
                            return;
                        }
                    }
                    for (int j2 = 0; j2 < VVsCount; j2++) {
                        if (this.mAvailableVVs[j2] && (ball = this.mVVs[j2]) != null) {
                            float dx3 = ball.mPosX - curr.mPosX;
                            float dy3 = ball.mPosY - curr.mPosY;
                            if ((dx3 * dx3) + (dy3 * dy3) <= 1.6000002E-5f) {
                                this.mAvailableVVs[j2] = false;
                                if (AccelDemo.this.isVibrateOn) {
                                    AccelDemo.this.mVibrateManager.vibrate((long) AccelDemo.this.VIBRATION_INTERVAL);
                                }
                                this.vvDx *= -1;
                                this.vvDy *= -1;
                                return;
                            }
                        }
                    }
                    for (Target ball3 : this.mDangers) {
                        if (ball3 != null) {
                            float dx4 = ball3.mPosX - curr.mPosX;
                            float dy4 = ball3.mPosY - curr.mPosY;
                            if ((dx4 * dx4) + (dy4 * dy4) <= 1.6000002E-5f && !AccelDemo.this.alreadyRestarted) {
                                if (AccelDemo.this.isVibrateOn) {
                                    AccelDemo.this.mVibrateManager.vibrate((long) (AccelDemo.this.VIBRATION_INTERVAL * 3));
                                }
                                restartLevel();
                                Log.i("AFTER RESTART--------------------------------", "AFTERT RESTART");
                                AccelDemo.this.thisActivity.finish();
                            }
                        }
                    }
                    curr.resolveCollisionWithBounds();
                }
            }

            public int getParticleCount() {
                return this.mBalls.length;
            }

            public float getPosX(int i) {
                return this.mBalls[i].mPosX;
            }

            public float getPosY(int i) {
                return this.mBalls[i].mPosY;
            }

            public int getTargetsCount() {
                return this.mTargets.length;
            }

            public int getFirstUnfinishedTarget() {
                return this.finishedTargets;
            }

            public int getVVCount() {
                return this.mVVs.length;
            }

            public float getVVX(int i) {
                return this.mVVs[i].mPosX;
            }

            public float getVVY(int i) {
                return this.mVVs[i].mPosY;
            }

            public int getDangersCount() {
                return this.mDangers.length;
            }

            public float getDangersX(int i) {
                return this.mDangers[i].mPosX;
            }

            public float getDangersY(int i) {
                return this.mDangers[i].mPosY;
            }

            public float getTargetX(int i) {
                return this.mTargets[i].mPosX;
            }

            public float getTargetY(int i) {
                return this.mTargets[i].mPosY;
            }

            public float getTeleportInX() {
                return this.teleportIn.mPosX;
            }

            public float getTeleportInY() {
                return this.teleportIn.mPosY;
            }

            public float getTeleportOutX() {
                return this.teleportOut.mPosX;
            }

            public float getTeleportOutY() {
                return this.teleportOut.mPosY;
            }

            public boolean isThereAnyTeleport() {
                return (this.teleportIn == null || this.teleportOut == null) ? false : true;
            }

            public void setTargetX(int i, float a) {
                this.mTargets[i].mPosX = a;
            }

            public void setTargetY(int i, float a) {
                this.mTargets[i].mPosY = a;
            }
        }

        public void startSimulation() {
            AccelDemo.this.mSensorManager.registerListener(this, this.mAccelerometer, 0);
        }

        public void stopSimulation() {
            AccelDemo.this.mSensorManager.unregisterListener(this);
        }

        public SimulationView(Context context, Level level) {
            super(context);
            this.MAX_TARGETS_COUNT = 16;
            this.mTaregetsIds = new Integer[]{Integer.valueOf((int) R.drawable.ball_1), Integer.valueOf((int) R.drawable.ball_2), Integer.valueOf((int) R.drawable.ball_3), Integer.valueOf((int) R.drawable.ball_4), Integer.valueOf((int) R.drawable.ball_5), Integer.valueOf((int) R.drawable.ball_6), Integer.valueOf((int) R.drawable.ball_7), Integer.valueOf((int) R.drawable.ball_8), Integer.valueOf((int) R.drawable.ball_9), Integer.valueOf((int) R.drawable.ball_10), Integer.valueOf((int) R.drawable.ball_11), Integer.valueOf((int) R.drawable.ball_12), Integer.valueOf((int) R.drawable.ball_13), Integer.valueOf((int) R.drawable.ball_14), Integer.valueOf((int) R.drawable.ball_15), Integer.valueOf((int) R.drawable.ball_16)};
            this.thisView = this;
            this.mParticleSystem = new ParticleSystem();
            this.thisView = this;
            this.mContext = context;
            this.mSurfaceHolder = getHolder();
            this.mSurfaceHolder.addCallback(this);
            this.gameLevel = level;
            this.mAccelerometer = AccelDemo.this.mSensorManager.getDefaultSensor(1);
            DisplayMetrics metrics = new DisplayMetrics();
            AccelDemo.this.getWindowManager().getDefaultDisplay().getMetrics(metrics);
            this.mXDpi = metrics.xdpi;
            this.mYDpi = metrics.ydpi;
            this.mMetersToPixelsX = this.mXDpi / 0.0254f;
            this.mMetersToPixelsY = this.mYDpi / 0.0254f;
            Bitmap ball = BitmapFactory.decodeResource(getResources(), R.drawable.ball);
            int dstWidth = (int) ((0.004f * this.mMetersToPixelsX) + 0.5f);
            int dstHeight = (int) ((0.004f * this.mMetersToPixelsY) + 0.5f);
            this.mBitmap = Bitmap.createScaledBitmap(ball, dstWidth, dstHeight, true);
            this.mTargetsBitmaps = new Bitmap[16];
            for (int i = 0; i < this.mTargetsBitmaps.length; i++) {
                Bitmap temp = BitmapFactory.decodeResource(getResources(), this.mTaregetsIds[i].intValue());
                this.mTargetsBitmaps[i] = Bitmap.createScaledBitmap(temp, dstWidth, dstHeight, true);
                temp.recycle();
            }
            Bitmap temp2 = BitmapFactory.decodeResource(getResources(), R.drawable.teleport_in);
            this.teleportInBitmap = Bitmap.createScaledBitmap(temp2, dstWidth, dstHeight, true);
            temp2.recycle();
            Bitmap temp3 = BitmapFactory.decodeResource(getResources(), R.drawable.teleport_out);
            this.teleportOutBitmap = Bitmap.createScaledBitmap(temp3, dstWidth, dstHeight, true);
            temp3.recycle();
            Bitmap temp4 = BitmapFactory.decodeResource(getResources(), R.drawable.vv);
            this.mVVBitmap = Bitmap.createScaledBitmap(temp4, dstWidth, dstHeight, true);
            temp4.recycle();
            Bitmap temp5 = BitmapFactory.decodeResource(getResources(), R.drawable.danger);
            this.mDangerBitmap = Bitmap.createScaledBitmap(temp5, dstWidth, dstHeight, true);
            temp5.recycle();
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inDither = true;
            opts.inPreferredConfig = Bitmap.Config.RGB_565;
            this.mWood = BitmapFactory.decodeResource(getResources(), R.drawable.wood, opts);
            ball.recycle();
        }

        public Bitmap decodeFile(int resId, int prefSizeWidth, int prefSizeHeight) {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(AccelDemo.this.thisActivity.getResources(), resId, o);
            int width_tmp = o.outWidth;
            int height_tmp = o.outHeight;
            int scale = 1;
            while (width_tmp / 2 >= prefSizeWidth && height_tmp / 2 >= prefSizeHeight) {
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            return BitmapFactory.decodeResource(AccelDemo.this.thisActivity.getResources(), resId, new BitmapFactory.Options());
        }

        /* access modifiers changed from: protected */
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            Log.i("onSizeChanged", "Entered");
            if (!AccelDemo.this.isBegun && !AccelDemo.this.alreadyRestarted) {
                this.mXOrigin = ((float) (w - this.mBitmap.getWidth())) * 0.5f;
                this.mYOrigin = ((float) (h - this.mBitmap.getHeight())) * 0.5f;
                this.mHorizontalBound = ((((float) w) / this.mMetersToPixelsX) - 0.004f) * 0.5f;
                this.mVerticalBound = ((((float) h) / this.mMetersToPixelsY) - 0.004f) * 0.5f;
                ParticleSystem particleSystem = this.mParticleSystem;
                long nanoTime = this.mSensorTimeStamp + (System.nanoTime() - this.mCpuTimeStamp);
                float f = this.mSensorX;
                float f2 = this.mSensorY;
                particleSystem.initParticleSystem(this.gameLevel.level, (float) this.gameLevel.width, (float) this.gameLevel.height, this.mHorizontalBound, this.mVerticalBound);
                AccelDemo.this.tt = new TimerTask() {
                    public void run() {
                        AccelDemo.this.mSimulationView.updating();
                    }
                };
                AccelDemo.this.t = new Timer("timerThread", false);
                AccelDemo.this.t.schedule(AccelDemo.this.tt, 0, (long) AccelDemo.this.TIMER_INTERVAL);
                AccelDemo.this.isBegun = true;
            }
        }

        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == 1) {
                this.mSensorX = event.values[0] * ((float) this.mParticleSystem.vvDx);
                this.mSensorY = event.values[1] * ((float) this.mParticleSystem.vvDy);
                this.mSensorTimeStamp = event.timestamp;
                this.mCpuTimeStamp = System.nanoTime();
            }
        }

        /* access modifiers changed from: protected */
        public void updating() {
            this.mParticleSystem.update(this.mSensorX, this.mSensorY, this.mSensorTimeStamp + (System.nanoTime() - this.mCpuTimeStamp));
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width2, int height2) {
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.i("SURFACE  CREATED  ", "");
            this.mThread = new GameManager(this.mSurfaceHolder, this.mContext, this);
            this.mThread.setRunning(true);
            this.mThread.start();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    }
}
