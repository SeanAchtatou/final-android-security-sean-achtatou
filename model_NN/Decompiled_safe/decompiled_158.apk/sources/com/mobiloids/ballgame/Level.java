package com.mobiloids.ballgame;

import java.lang.reflect.Array;

public class Level {
    public static final int BALL = -1;
    public static final int TELEPORT_IN = -2;
    public static final int TELEPORT_OUT = -3;
    public static final int VV = -4;
    public int height;
    public int[][] level = ((int[][]) Array.newInstance(Integer.TYPE, this.width, this.height));
    public int width;

    Level(int[][] lev, int w, int h) {
        this.width = w;
        this.height = h;
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                this.level[i][j] = lev[i][j];
            }
        }
    }
}
