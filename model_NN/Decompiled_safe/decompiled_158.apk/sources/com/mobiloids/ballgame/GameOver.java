package com.mobiloids.ballgame;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.Timer;
import java.util.TimerTask;

public class GameOver extends Activity implements AdListener {
    private static final String LEVEL = "LEVEL";
    private int TIMER_INTERVAL = 800;
    /* access modifiers changed from: private */
    public Activity gameOverActivity;
    /* access modifiers changed from: private */
    public MakeVisible handlerShow = new MakeVisible();
    ImageView image;
    ImageView imageRestart;
    int looseLevel;
    private PowerManager mPowerManager;
    private PowerManager.WakeLock mWakeLock;
    AdView over;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.game_over);
        setTitle("You lose. Try Again");
        Log.i("WAKE UP:", "ACCURRED!!!!!!!!!!!!!!!!!");
        ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock("GameOver").disableKeyguard();
        this.over = (AdView) findViewById(R.id.adOver);
        this.over.setAdListener(this);
        this.over.loadAd(new AdRequest());
        this.looseLevel = getIntent().getExtras().getInt("LEVEL");
        this.gameOverActivity = this;
        this.image = (ImageView) findViewById(R.id.game_over_back);
        this.image.setKeepScreenOn(true);
        this.image.setVisibility(4);
        this.image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                GameOver.this.gameOverActivity.finish();
            }
        });
        this.imageRestart = (ImageView) findViewById(R.id.game_over_restart);
        this.imageRestart.setVisibility(4);
        this.imageRestart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(GameOver.this, AccelDemo.class);
                intent.putExtra("LEVEL", GameOver.this.looseLevel);
                GameOver.this.startActivity(intent);
                GameOver.this.gameOverActivity.finish();
            }
        });
        new Timer("timerThread", false).schedule(new TimerTask() {
            public void run() {
                GameOver.this.handlerShow.sendEmptyMessage(0);
            }
        }, (long) this.TIMER_INTERVAL);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        this.imageRestart.setVisibility(0);
        this.image.setVisibility(0);
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        this.imageRestart.setVisibility(0);
        this.image.setVisibility(0);
    }

    class MakeVisible extends Handler {
        MakeVisible() {
        }

        public void handleMessage(Message msg) {
            GameOver.this.imageRestart.setVisibility(0);
            GameOver.this.image.setVisibility(0);
        }
    }
}
