package com.mobiloids.ballgame;

import android.app.ListActivity;
import android.content.Context;
import android.net.ParseException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import java.util.Vector;

public class Help extends ListActivity {
    static final String[] detail = {"You have to collect balls in the correct sequence.", "Do not touch RED balls.If you touch them you will lose.", "Use teleport for teleporting your ball will immediately moved from one place to another", "When you use 'Teleport IN' your ball will appear in Teleport OUT", "When you touch (VV) Vice Verse your ball will change moving direction  Left to Right, Right to Left, Up to Down, Down to Up. Touch one more time (VV) and the ball moving direction will be reset to original", "In this version we have 25 levels.At the begining only the first level is open.To unlock next levels you should pass all the previous levels."};
    static final String[] title = {"Yellow balls with numbers", "Red balls", "Teleport IN", "Teleport OUT", "(VV) Vice Verse", "Levels"};
    private Vector<RowData> data;
    /* access modifiers changed from: private */
    public Integer[] imgid = {Integer.valueOf((int) R.drawable.ball_1_help), Integer.valueOf((int) R.drawable.danger), Integer.valueOf((int) R.drawable.teleport_in), Integer.valueOf((int) R.drawable.teleport_out), Integer.valueOf((int) R.drawable.vv), Integer.valueOf((int) R.drawable.lev7)};
    /* access modifiers changed from: private */
    public LayoutInflater mInflater;
    RowData rd;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.more_games);
        this.mInflater = (LayoutInflater) getSystemService("layout_inflater");
        this.data = new Vector<>();
        for (int i = 0; i < title.length; i++) {
            try {
                this.rd = new RowData(i, title[i], detail[i]);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.data.add(this.rd);
        }
        CustomAdapter adapter = new CustomAdapter(this, R.layout.list, R.id.title, this.data);
        getListView().setHeaderDividersEnabled(true);
        setListAdapter(adapter);
        getListView().setTextFilterEnabled(true);
    }

    private class RowData {
        protected String mDetail;
        protected int mId;
        protected String mTitle;

        RowData(int id, String title, String detail) {
            this.mId = id;
            this.mTitle = title;
            this.mDetail = detail;
        }

        public String toString() {
            return String.valueOf(this.mId) + " " + this.mTitle + " " + this.mDetail;
        }
    }

    private class CustomAdapter extends ArrayAdapter<RowData> {
        public CustomAdapter(Context context, int resource, int textViewResourceId, List<RowData> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            RowData rowData = (RowData) getItem(position);
            if (convertView == null) {
                convertView = Help.this.mInflater.inflate((int) R.layout.list, (ViewGroup) null);
                convertView.setTag(new ViewHolder(convertView));
            }
            ViewHolder holder = (ViewHolder) convertView.getTag();
            holder.gettitle().setText(rowData.mTitle);
            holder.getdetail().setText(rowData.mDetail);
            holder.getImage().setImageResource(Help.this.imgid[rowData.mId].intValue());
            return convertView;
        }

        private class ViewHolder {
            private TextView detail = null;
            private ImageView i11 = null;
            private View mRow;
            private TextView title = null;

            public ViewHolder(View row) {
                this.mRow = row;
            }

            public TextView gettitle() {
                if (this.title == null) {
                    this.title = (TextView) this.mRow.findViewById(R.id.title);
                }
                return this.title;
            }

            public TextView getdetail() {
                if (this.detail == null) {
                    this.detail = (TextView) this.mRow.findViewById(R.id.detail);
                }
                return this.detail;
            }

            public ImageView getImage() {
                if (this.i11 == null) {
                    this.i11 = (ImageView) this.mRow.findViewById(R.id.img);
                }
                return this.i11;
            }
        }
    }
}
