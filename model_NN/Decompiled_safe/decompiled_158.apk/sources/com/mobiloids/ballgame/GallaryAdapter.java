package com.mobiloids.ballgame;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

public class GallaryAdapter extends BaseAdapter {
    private Context mContext;
    int mGalleryItemBackground;
    private Integer[] mImageIds;
    private int openedLevelsCount;

    public GallaryAdapter(Context c, int openedLevels, Integer[] arr) {
        this.mImageIds = arr;
        this.mContext = c;
        this.openedLevelsCount = openedLevels;
        TypedArray a = this.mContext.obtainStyledAttributes(R.styleable.HelloGallery);
        this.mGalleryItemBackground = a.getResourceId(0, 0);
        a.recycle();
    }

    public int getCount() {
        return this.openedLevelsCount;
    }

    public Object getItem(int position) {
        return Integer.valueOf(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (position >= this.openedLevelsCount) {
            return null;
        }
        ImageView i = new ImageView(this.mContext);
        i.setImageResource(this.mImageIds[position].intValue());
        i.setLayoutParams(new Gallery.LayoutParams(150, 100));
        i.setScaleType(ImageView.ScaleType.FIT_XY);
        i.setBackgroundResource(this.mGalleryItemBackground);
        return i;
    }
}
