package com.xl.util;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import wall.bt.wp.P021.R;

public class XMUtilityFoldonly {
    public static final String strRoot = "/sdcard";
    public ArrayList<HashMap<String, Object>> arr = new ArrayList<>();
    private String strCurDirectory;

    public String getCurDirectory() {
        return this.strCurDirectory;
    }

    public int getIconBySuffix(String suffix) {
        if ("txt".equalsIgnoreCase(suffix)) {
            return R.drawable.icon_txt;
        }
        return R.drawable.icon_txt;
    }

    public ArrayList<HashMap<String, Object>> getFileDir(String strFilePath) {
        this.arr.clear();
        this.strCurDirectory = strFilePath;
        File file = new File(strFilePath);
        if (!file.exists()) {
            return this.arr;
        }
        File[] subFiles = file.listFiles();
        for (int iIndex = 0; iIndex < subFiles.length; iIndex++) {
            HashMap<String, Object> map = new HashMap<>();
            if (subFiles[iIndex].isDirectory() && !subFiles[iIndex].getName().equalsIgnoreCase("acebook")) {
                map.put("fdir", subFiles[iIndex].getPath());
                map.put("fname", subFiles[iIndex].getName());
                map.put("bfold", true);
                map.put("icon", Integer.valueOf((int) R.drawable.folder));
                this.arr.add(map);
            }
        }
        return this.arr;
    }
}
