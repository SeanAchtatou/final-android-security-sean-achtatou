package org.firezenk.simplylock;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;
import org.firezenk.simplylock.options.OtherOptions;

public class GmailAccounts {
    public GmailAccounts(Context context) {
        for (Account account : AccountManager.get(context).getAccounts()) {
            OtherOptions.cuentas.add(account.name);
            Log.d("CUENTA:", account.name);
        }
    }
}
