package org.firezenk.simplylock;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GeoPosicion extends Thread implements LocationListener {
    String best;
    Location currentLocation = null;
    public String lat = "36575560";
    private LocationManager lm;
    public String lng = "-04670219";

    public GeoPosicion(LocationManager locationManager) {
        this.lm = locationManager;
        try {
            Criteria criteria = new Criteria();
            criteria.setSpeedRequired(true);
            this.best = this.lm.getBestProvider(criteria, true);
            Location location = this.lm.getLastKnownLocation(this.best);
            this.lat = String.valueOf(location.getLatitude());
            this.lng = String.valueOf(location.getLongitude());
            this.lm.requestLocationUpdates(this.best, 15000, 1.0f, this);
        } catch (Exception e) {
        }
    }

    public void onLocationChanged(Location location) {
        this.lat = String.valueOf(location.getLatitude());
        this.lng = String.valueOf(location.getLongitude());
    }

    public void onProviderDisabled(String provider) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
}
