package org.firezenk.simplylock;

import android.app.Activity;
import android.app.Dialog;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.Vibrator;
import android.provider.Settings;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;
import com.flurry.android.FlurryAgent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import org.firezenk.meteo.Meteorology;
import org.firezenk.simplylock.options.Preferencias;

public class LockScreen extends Activity {
    private static String DIR = "sdcard/Android/data/org.firezenk.simplylock";
    static final int ENTER_CODE = 12894;
    protected static boolean IS_SOWN = false;
    protected static boolean SCREEN_ON = false;
    private static TextView textCalendar;
    /* access modifiers changed from: private */
    public String DIRECTORY = "sdcard/Android/data/org.firezenk.simplylock/themes/";
    BroadcastReceiver KILL = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("org.firezenk.simplylock.kill")) {
                LockScreen.this.commonUnlock();
            }
        }
    };
    private String PREFERENCES = "org.firezenk.simplylock";
    /* access modifiers changed from: private */
    public Resources RES;
    /* access modifiers changed from: private */
    public String SECRET_CODE;
    /* access modifiers changed from: private */
    public String THEME;
    private BroadcastReceiver alarmReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Process.killProcess(Process.myPid());
        }
    };
    private boolean alarmVisible = true;
    private TextView alarma;
    private ImageView alarmimage;
    /* access modifiers changed from: private */
    public AudioManager am;
    private TextView ampm;
    private RelativeLayout barra;
    /* access modifiers changed from: private */
    public ImageView bateria;
    private BroadcastReceiver batteryLife = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                try {
                    LockScreen.this.bateria.setBackgroundDrawable(new BitmapDrawable(LockScreen.this.RES, new FileInputStream(LockScreen.this.RES.getString(R.string.b0, String.valueOf(LockScreen.this.DIRECTORY) + LockScreen.this.THEME))));
                } catch (Exception e) {
                    e.printStackTrace();
                    LockScreen.this.bateria.setBackgroundResource(R.drawable.b0);
                }
            } else {
                LockScreen.this.bateria(intent.getIntExtra("level", 0));
                LockScreen.this.percentage.setText(String.valueOf(intent.getIntExtra("level", 0)) + "%");
            }
        }
    };
    /* access modifiers changed from: private */
    public BubbleTextView bub;
    private boolean calendarVisible;
    private SlidingDrawer calendario;
    /* access modifiers changed from: private */
    public TextView call;
    /* access modifiers changed from: private */
    public Camera camera;
    private int clockSize;
    private boolean clockVisible = true;
    private boolean dateVisible = true;
    private String device = "";
    public boolean done = false;
    private TextView fecha;
    private int formatSelected;
    /* access modifiers changed from: private */
    public GeoPosicion geo;
    private TextView granReloj;
    private BroadcastReceiver homeButton = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getCategories().contains("android.intent.category.HOME")) {
                Process.killProcess(intent.resolveActivityInfo(LockScreen.this.getPackageManager(), 0).applicationInfo.uid);
                LockScreen.this.closeContextMenu();
                LockScreen.this.closeOptionsMenu();
            }
        }
    };
    private boolean lockbarVisible = true;
    private boolean longHourVisible = true;
    /* access modifiers changed from: private */
    public TextView mail;
    private BroadcastReceiver notificador = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("org.firezenk.simplylock.NOTIFICADOR")) {
                LockScreen.this.setPlayerState(intent.getBooleanExtra("MUSIC", false));
                LockScreen.this.sms.setText(String.valueOf(intent.getIntExtra("SMScount", 0)));
                LockScreen.this.call.setText(String.valueOf(String.valueOf(intent.getIntExtra("CALLcount", 0))) + " Calls");
                LockScreen.this.mail.setText(String.valueOf(String.valueOf(intent.getIntExtra("MAILcount", 0))) + " New");
                String line = "Unavaible";
                if (intent.getCharSequenceExtra("SMSline").toString() != null) {
                    line = intent.getCharSequenceExtra("SMSline").toString();
                }
                LockScreen.this.smsLine.setText(line);
                if (intent.getStringExtra("track") != null) {
                    LockScreen.this.bub.setText(intent.getStringExtra("track"));
                }
                LockScreen.this.proximityForce = 0;
            }
        }
    };
    private boolean notificationVisible = true;
    /* access modifiers changed from: private */
    public TextView pais;
    private boolean percentVisible = false;
    /* access modifiers changed from: private */
    public TextView percentage;
    private Camera.PictureCallback photoCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            new SavePhotoTask().execute(data);
            camera.startPreview();
        }
    };
    private Button play;
    /* access modifiers changed from: private */
    public boolean playerState = false;
    private boolean playerVisible = true;
    /* access modifiers changed from: private */
    public Preferencias preferencias;
    private SurfaceView preview;
    /* access modifiers changed from: private */
    public SurfaceHolder previewHolder = null;
    /* access modifiers changed from: private */
    public int proximityForce = 0;
    /* access modifiers changed from: private */
    public String ringtone = "";
    private SensorManager sensorManager;
    private SharedPreferences settings;
    /* access modifiers changed from: private */
    public TextView slideHandleButton;
    private SlidingDrawer slidingDrawer;
    /* access modifiers changed from: private */
    public TextView sms;
    /* access modifiers changed from: private */
    public TextView smsLine;
    /* access modifiers changed from: private */
    public BroadcastReceiver songReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            LockScreen.this.bub.setText(intent.getStringExtra("track"));
        }
    };
    /* access modifiers changed from: private */
    public boolean sound;
    private boolean statusVisible = true;
    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        public void surfaceCreated(SurfaceHolder holder) {
            LockScreen.this.camera = Camera.open();
            try {
                LockScreen.this.camera.setPreviewDisplay(LockScreen.this.previewHolder);
            } catch (Throwable th) {
                Log.e("Photographer", "Exception in setPreviewDisplay()", th);
            }
            try {
                LockScreen.this.takePicture();
            } catch (Throwable th2) {
                Log.e("Photographer", "Exception in takePicture()", th2);
            }
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = LockScreen.this.camera.getParameters();
            Display display = LockScreen.this.getWindowManager().getDefaultDisplay();
            parameters.setPreviewSize(display.getWidth(), display.getHeight());
            parameters.setPictureFormat(256);
            LockScreen.this.camera.setParameters(parameters);
            LockScreen.this.camera.startPreview();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            LockScreen.this.camera.stopPreview();
            LockScreen.this.camera.release();
            LockScreen.this.camera = null;
        }
    };
    /* access modifiers changed from: private */
    public TextView temp;
    /* access modifiers changed from: private */
    public int tempUnit = 0;
    private Typeface tf;
    private SensorEventListener theForce;
    /* access modifiers changed from: private */
    public final Handler timeHandler = new Handler();
    /* access modifiers changed from: private */
    public final Runnable timeUpdate = new Runnable() {
        public void run() {
            LockScreen.this.reloj();
        }
    };
    private SeekBar unlock;
    private boolean useCode;
    private boolean useTheForce = false;
    /* access modifiers changed from: private */
    public boolean vibrate;
    private SeekBar volume;
    /* access modifiers changed from: private */
    public int volumeInit = 0;
    /* access modifiers changed from: private */
    public int volumeState;
    private boolean volumeVisible = true;
    /* access modifiers changed from: private */
    public boolean weatherVisible = true;
    /* access modifiers changed from: private */
    public Meteorology wht;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs2APP();
        IS_SOWN = true;
        requestWindowFeature(1);
        if (!this.preferencias.isoState()) {
            getWindow().setFlags(1024, 1024);
        } else {
            getWindow().setFlags(2048, 2048);
        }
        Window w = getWindow();
        int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
        if (sdkVersion <= 4) {
            w.setBackgroundDrawable(getWallpaper());
        } else {
            w.addFlags(4718592);
            w.addFlags(1048576);
        }
        this.THEME = String.valueOf(this.preferencias.getThState()) + "/";
        Log.d("SL::THEME", this.THEME);
        this.settings = getSharedPreferences(this.PREFERENCES, 0);
        switch (this.preferencias.getLayout()) {
            case 0:
                setContentView(R.layout.lockscreen);
                break;
            case 1:
                setContentView(R.layout.lockscreen_b);
                break;
            default:
                setContentView(R.layout.lockscreen);
                break;
        }
        this.RES = getResources();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
        intentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        intentFilter.addAction("android.intent.action.BATTERY_CHANGED");
        registerReceiver(this.batteryLife, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.intent.action.MAIN");
        intentFilter2.addCategory("android.intent.category.HOME");
        registerReceiver(this.homeButton, intentFilter2);
        registerReceiver(this.notificador, new IntentFilter("org.firezenk.simplylock.NOTIFICADOR"));
        IntentFilter intentFilter3 = new IntentFilter("com.android.deskclock.ALARM_DONE");
        intentFilter3.addAction("com.android.deskclock.ALARM_ALERT");
        intentFilter3.addAction("com.android.deskclock.ALARM_DISMISS");
        intentFilter3.addAction("com.android.deskclock.ALARM_SNOOZE");
        intentFilter3.addAction("alarm_killed");
        intentFilter3.addAction("cancel_snooze");
        registerReceiver(this.alarmReceiver, new IntentFilter(intentFilter3));
        this.vibrate = this.preferencias.isViState();
        this.sound = this.preferencias.isSoState();
        this.ringtone = this.settings.getString("ringtone", "");
        this.barra = (RelativeLayout) findViewById(R.id.barra);
        try {
            BitmapDrawable b = new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.statusbar, String.valueOf(this.DIRECTORY) + this.THEME)));
            b.setBounds(b.getBounds());
            this.barra.setBackgroundDrawable(b);
        } catch (Exception e) {
            e.printStackTrace();
            this.barra.setBackgroundResource(R.drawable.statusbar);
        }
        this.am = (AudioManager) getSystemService("audio");
        this.unlock = (SeekBar) findViewById(R.id.unblock);
        this.unlock.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int firstTouch = 100;

            public void onStopTrackingTouch(SeekBar seekBar) {
                if (seekBar.getProgress() < 100) {
                    seekBar.setProgress(0);
                } else {
                    seekBar.setProgress(100);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                this.firstTouch = seekBar.getProgress();
            }

            /* Debug info: failed to restart local var, previous not found, register: 9 */
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (this.firstTouch == 0) {
                    this.firstTouch = progress;
                    Log.d("PRIMER TOQUE", new StringBuilder().append(this.firstTouch).toString());
                }
                if (progress == 100 && this.firstTouch < 10) {
                    if (LockScreen.this.preferencias.isAcState()) {
                        SLService.clearAll();
                    }
                    LockScreen.this.commonUnlock();
                    if (LockScreen.this.sound) {
                        try {
                            Uri alert = Uri.parse(LockScreen.this.ringtone);
                            MediaPlayer player = new MediaPlayer();
                            player.setDataSource(LockScreen.this, alert);
                            if (((AudioManager) LockScreen.this.getSystemService("audio")).getStreamVolume(2) != 0) {
                                player.setAudioStreamType(2);
                                player.setLooping(false);
                                player.prepare();
                                player.start();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (LockScreen.this.vibrate) {
                        ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                    }
                } else if (this.firstTouch > 10) {
                    seekBar.setProgress(0);
                }
            }
        });
        this.unlock = (SeekBar) findViewById(R.id.unblock);
        try {
            this.unlock.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.lockscroll, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e2) {
            e2.printStackTrace();
            this.unlock.setBackgroundResource(R.drawable.lockscroll);
        }
        try {
            this.unlock.setThumb(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.lockthumb, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e3) {
            e3.printStackTrace();
            this.unlock.setThumb(this.RES.getDrawable(R.drawable.lockthumb));
        }
        this.sms = (TextView) findViewById(R.id.sms);
        try {
            this.sms.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.sms, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e4) {
            e4.printStackTrace();
            this.sms.setBackgroundResource(R.drawable.sms);
        }
        this.call = (TextView) findViewById(R.id.call0);
        this.mail = (TextView) findViewById(R.id.mail0);
        ImageView imail = (ImageView) findViewById(R.id.ImageView01);
        try {
            imail.setImageDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.mail, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e5) {
            e5.printStackTrace();
            imail.setBackgroundResource(R.drawable.mail);
        }
        ImageView iphone = (ImageView) findViewById(R.id.phone);
        try {
            iphone.setImageDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.phone, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e6) {
            e6.printStackTrace();
            iphone.setBackgroundResource(R.drawable.phone);
        }
        if (sdkVersion <= 4) {
            this.mail.setVisibility(8);
            imail.setVisibility(8);
            this.call.setPadding(3, 18, 20, 0);
            iphone.setPadding(0, 20, 0, 0);
        }
        this.smsLine = (TextView) findViewById(R.id.sms1);
        this.granReloj = (TextView) findViewById(R.id.granReloj);
        this.ampm = (TextView) findViewById(R.id.ampm);
        this.fecha = (TextView) findViewById(R.id.fecha);
        this.alarma = (TextView) findViewById(R.id.alarm);
        this.alarmimage = (ImageView) findViewById(R.id.alamimage);
        this.volume = (SeekBar) findViewById(R.id.soundbar);
        try {
            this.volume.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.volumescroll, String.valueOf(this.DIRECTORY) + this.THEME))));
            this.volume.setThumb(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.volumethumb, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e7) {
            e7.printStackTrace();
            this.volume.setBackgroundResource(R.drawable.volumescroll);
            this.volume.setThumb(this.RES.getDrawable(R.drawable.volumethumb));
        }
        if (this.am.getRingerMode() == 2) {
            this.volumeInit = 100;
            this.volume.setProgress(100);
            this.volumeState = 3;
        } else if (this.am.getRingerMode() == 1) {
            this.volumeInit = 50;
            this.volume.setProgress(50);
            this.volumeState = 2;
        } else if (this.am.getRingerMode() == 0) {
            this.volumeInit = 0;
            this.volume.setProgress(0);
            this.volumeState = 1;
        }
        this.volume.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                int p = seekBar.getProgress();
                if (p <= -1 || p >= 6 || p <= 44 || p >= 56 || p <= 94 || p >= 101) {
                    seekBar.setProgress(LockScreen.this.volumeInit);
                }
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.d("VOL!", new StringBuilder().append(seekBar.getProgress()).toString());
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                switch (progress) {
                    case 0:
                        if (LockScreen.this.volumeState != 1) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Silent mode", 0).show();
                            LockScreen.this.am.setRingerMode(0);
                            LockScreen.this.volumeState = 1;
                            LockScreen.this.volumeInit = progress;
                            break;
                        }
                        break;
                    case 48:
                        if (LockScreen.this.volumeState != 2) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Vibrate mode", 0).show();
                            LockScreen.this.am.setRingerMode(1);
                            LockScreen.this.volumeState = 2;
                            LockScreen.this.volumeInit = 50;
                            seekBar.setProgress(LockScreen.this.volumeInit);
                            break;
                        }
                        break;
                    case 49:
                        if (LockScreen.this.volumeState != 2) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Vibrate mode", 0).show();
                            LockScreen.this.am.setRingerMode(1);
                            LockScreen.this.volumeState = 2;
                            LockScreen.this.volumeInit = 50;
                            seekBar.setProgress(LockScreen.this.volumeInit);
                            break;
                        }
                        break;
                    case 50:
                        if (LockScreen.this.volumeState != 2) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Vibrate mode", 0).show();
                            LockScreen.this.am.setRingerMode(1);
                            LockScreen.this.volumeState = 2;
                            LockScreen.this.volumeInit = 50;
                            seekBar.setProgress(LockScreen.this.volumeInit);
                            break;
                        }
                        break;
                    case 51:
                        if (LockScreen.this.volumeState != 2) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Vibrate mode", 0).show();
                            LockScreen.this.am.setRingerMode(1);
                            LockScreen.this.volumeState = 2;
                            LockScreen.this.volumeInit = 50;
                            seekBar.setProgress(LockScreen.this.volumeInit);
                            break;
                        }
                        break;
                    case 52:
                        if (LockScreen.this.volumeState != 2) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Vibrate mode", 0).show();
                            LockScreen.this.am.setRingerMode(1);
                            LockScreen.this.volumeState = 2;
                            LockScreen.this.volumeInit = 50;
                            seekBar.setProgress(LockScreen.this.volumeInit);
                            break;
                        }
                        break;
                    case 100:
                        if (LockScreen.this.volumeState != 3) {
                            if (LockScreen.this.vibrate) {
                                ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                            }
                            Toast.makeText(LockScreen.this, "Normal mode", 0).show();
                            LockScreen.this.am.setRingerMode(2);
                            LockScreen.this.volumeState = 3;
                            LockScreen.this.volumeInit = progress;
                            break;
                        }
                        break;
                }
                LockScreen.this.bub.setSelected(true);
            }
        });
        Button prev = (Button) findViewById(R.id.prev);
        try {
            BitmapDrawable b2 = new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.prev, String.valueOf(this.DIRECTORY) + this.THEME)));
            prev.setHeight(b2.getBounds().height() * 2);
            prev.setWidth(b2.getBounds().width() * 2);
            prev.setBackgroundDrawable(b2);
        } catch (Exception e8) {
            e8.printStackTrace();
            prev.setBackgroundResource(R.drawable.prev);
        }
        prev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!LockScreen.this.getPlayerState()) {
                    if (LockScreen.this.vibrate) {
                        ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(20);
                    }
                    String clss = "com.android.music.musicservicecommand.previous";
                    try {
                        Class.forName(clss);
                    } catch (ClassNotFoundException e) {
                        clss = "com.miui.player.musicservicecommand.previous";
                    }
                    Intent i = new Intent(clss);
                    i.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(0, 88));
                    i.putExtra("command", "previous");
                    LockScreen.this.sendOrderedBroadcast(i, null);
                }
                LockScreen.this.bub.setSelected(true);
            }
        });
        this.play = (Button) findViewById(R.id.play);
        try {
            this.play.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.play, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e9) {
            e9.printStackTrace();
            this.play.setBackgroundResource(R.drawable.play);
        }
        this.play.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (LockScreen.this.getPlayerState()) {
                    LockScreen.this.registerReceiver(LockScreen.this.songReceiver, new IntentFilter("android.media.AUDIO_BECOMING_NOISY"));
                    Intent i = new Intent("android.media.AUDIO_BECOMING_NOISY");
                    i.putExtra("command", "play");
                    LockScreen.this.sendBroadcast(i);
                }
                if (LockScreen.this.vibrate) {
                    ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(20);
                }
                Intent i2 = new Intent("com.android.music.musicservicecommand.togglepause");
                i2.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(0, 85));
                i2.putExtra("command", "pause");
                LockScreen.this.sendOrderedBroadcast(i2, null);
                if (LockScreen.this.playerState) {
                    LockScreen.this.setPlayerState(false);
                } else {
                    LockScreen.this.setPlayerState(true);
                }
                LockScreen.this.bub.setSelected(true);
            }
        });
        Button next = (Button) findViewById(R.id.next);
        try {
            next.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.next, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e10) {
            e10.printStackTrace();
            next.setBackgroundResource(R.drawable.next);
        }
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!LockScreen.this.getPlayerState()) {
                    if (LockScreen.this.vibrate) {
                        ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(20);
                    }
                    Intent i = new Intent("com.android.music.musicservicecommand.next");
                    i.putExtra("android.intent.extra.KEY_EVENT", new KeyEvent(0, 87));
                    i.putExtra("command", "next");
                    LockScreen.this.sendOrderedBroadcast(i, null);
                }
                LockScreen.this.bub.setSelected(true);
            }
        });
        this.bateria = (ImageView) findViewById(R.id.bateria);
        try {
            this.bateria.setImageDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b0, String.valueOf(this.DIRECTORY) + this.THEME))));
        } catch (Exception e11) {
            e11.printStackTrace();
            this.bateria.setBackgroundResource(R.drawable.b0);
        }
        textCalendar = (TextView) findViewById(R.id.textCalendar);
        this.slideHandleButton = (TextView) findViewById(R.id.slideHandleButton);
        this.slidingDrawer = (SlidingDrawer) findViewById(R.id.SlidingDrawer);
        this.slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            public void onDrawerOpened() {
                LockScreen.this.slideHandleButton.setText("Close calendar");
            }
        });
        this.slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            public void onDrawerClosed() {
                LockScreen.this.slideHandleButton.setText("Open calendar");
            }
        });
        try {
            this.tf = Typeface.createFromAsset(getAssets(), this.RES.getString(R.string.font, String.valueOf(this.DIRECTORY) + this.THEME));
        } catch (Exception e12) {
            if (!this.THEME.equals("/")) {
                this.tf = Typeface.createFromAsset(getAssets(), "fonts/" + this.preferencias.getThState() + ".ttf");
            } else {
                this.tf = Typeface.createFromAsset(getAssets(), "fonts/Default.ttf");
            }
        }
        new Timer().schedule(new TimerTask() {
            public void run() {
                LockScreen.this.timeHandler.post(LockScreen.this.timeUpdate);
            }
        }, 0, 15000);
        this.temp = (TextView) findViewById(R.id.temp);
        this.pais = (TextView) findViewById(R.id.pais);
        this.percentage = (TextView) findViewById(R.id.percentage);
        this.bub = (BubbleTextView) findViewById(R.id.song);
        this.bub.setText("Touch play to start new random session.");
        this.bub.setSelected(true);
        this.calendario = (SlidingDrawer) findViewById(R.id.SlidingDrawer);
        IntentFilter iF = new IntentFilter();
        iF.addAction("com.android.music.metachanged");
        iF.addAction("com.android.music.playstatechanged");
        iF.addAction("com.android.music.playbackcomplete");
        iF.addAction("com.android.music.queuechanged");
        registerReceiver(this.songReceiver, iF);
        this.sensorManager = (SensorManager) getSystemService("sensor");
        this.theForce = new SensorEventListener() {
            /* Debug info: failed to restart local var, previous not found, register: 8 */
            public void onSensorChanged(SensorEvent event) {
                if (Build.VERSION.SDK_INT > 8) {
                    if (LockScreen.SCREEN_ON) {
                        LockScreen lockScreen = LockScreen.this;
                        lockScreen.proximityForce = lockScreen.proximityForce + 1;
                    }
                    if (LockScreen.this.proximityForce > LockScreen.this.preferencias.getForceLevel()) {
                        if (LockScreen.this.preferencias.isAcState()) {
                            SLService.clearAll();
                        }
                        LockScreen.this.proximityForce = 0;
                        LockScreen.this.commonUnlock();
                        if (LockScreen.this.sound) {
                            try {
                                Uri alert = Uri.parse(LockScreen.this.ringtone);
                                MediaPlayer player = new MediaPlayer();
                                player.setDataSource(LockScreen.this, alert);
                                if (((AudioManager) LockScreen.this.getSystemService("audio")).getStreamVolume(2) != 0) {
                                    player.setAudioStreamType(2);
                                    player.setLooping(false);
                                    player.prepare();
                                    player.start();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        if (LockScreen.this.vibrate) {
                            ((Vibrator) LockScreen.this.getSystemService("vibrator")).vibrate(50);
                        }
                    }
                }
                Log.d("SENSOR", new StringBuilder().append(event.timestamp).toString());
            }

            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
        configuration();
        new DownloadTask(this, null).execute("");
    }

    /* access modifiers changed from: protected */
    public void setPlayerState(boolean boo) {
        this.playerState = boo;
        if (boo) {
            try {
                this.play.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.play, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e) {
                e.printStackTrace();
                this.play.setBackgroundResource(R.drawable.play);
            }
        } else {
            try {
                this.play.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.pause, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e2) {
                e2.printStackTrace();
                this.play.setBackgroundResource(R.drawable.pause);
            }
        }
        Log.d("SL::PLAYER", new StringBuilder().append(boo).toString());
    }

    /* access modifiers changed from: protected */
    public boolean getPlayerState() {
        return this.playerState;
    }

    private void configuration() {
        this.tempUnit = this.preferencias.getuState();
        this.clockVisible = this.preferencias.iscState();
        this.volumeVisible = this.preferencias.isvState();
        this.statusVisible = this.preferencias.issState();
        this.device = this.preferencias.getrState();
        this.longHourVisible = this.preferencias.ishState();
        this.percentVisible = this.preferencias.ispState();
        this.dateVisible = this.preferencias.isfState();
        this.alarmVisible = this.preferencias.isaState();
        this.formatSelected = this.preferencias.getFormatState();
        this.weatherVisible = this.preferencias.iswState();
        this.clockSize = this.preferencias.getClockSizeState();
        this.notificationVisible = this.preferencias.isnState();
        this.playerVisible = this.preferencias.isPlayerState();
        this.useTheForce = this.preferencias.isTheForce();
        this.lockbarVisible = this.preferencias.islState();
        this.calendarVisible = this.preferencias.isCalendar();
        this.useCode = this.preferencias.isUseCode();
        this.SECRET_CODE = this.preferencias.getSecretCode();
        crearFondo();
        if (this.THEME.equals("/")) {
            this.granReloj.setTextSize((float) this.clockSize);
        } else {
            this.granReloj.setTextSize((float) (this.clockSize - 25));
        }
        if (this.percentVisible) {
            this.percentage.setVisibility(0);
        } else {
            this.percentage.setVisibility(4);
        }
        if (this.lockbarVisible) {
            this.unlock.setVisibility(0);
        } else {
            this.unlock.setVisibility(4);
        }
        if (this.playerVisible) {
            findViewById(R.id.player).setVisibility(0);
        } else {
            findViewById(R.id.player).setVisibility(4);
        }
        if (this.dateVisible) {
            Calendar calendario2 = new GregorianCalendar();
            switch (this.formatSelected) {
                case 0:
                    this.fecha.setText(String.valueOf(calendario2.get(5)) + "/" + (calendario2.get(2) + 1) + "/" + calendario2.get(1));
                    break;
                case 1:
                    this.fecha.setText(String.valueOf(calendario2.get(2) + 1) + "/" + calendario2.get(5) + "/" + calendario2.get(1));
                    break;
                case 2:
                    this.fecha.setText(String.valueOf(calendario2.get(1)) + "/" + (calendario2.get(2) + 1) + "/" + calendario2.get(5));
                    break;
                case 3:
                    this.fecha.setText(String.valueOf(calendario2.get(1)) + "/" + calendario2.get(5) + "/" + (calendario2.get(2) + 1));
                    break;
                case 4:
                    this.fecha.setText(String.valueOf(calendario2.get(2) + 1) + "/" + calendario2.get(1) + "/" + calendario2.get(5));
                    break;
            }
            this.fecha.setVisibility(0);
        } else {
            this.fecha.setVisibility(4);
        }
        if (this.alarmVisible) {
            String s = Settings.System.getString(getContentResolver(), "next_alarm_formatted");
            if (s.length() > 1) {
                this.alarma.setText(s);
                this.alarma.setVisibility(0);
                this.alarmimage.setVisibility(0);
            } else {
                this.alarma.setText("No alarm");
                this.alarmimage.setVisibility(4);
            }
        } else {
            this.alarma.setVisibility(4);
            this.alarmimage.setVisibility(4);
        }
        if (this.device.equals("Nexus")) {
            this.unlock.setPadding(33, 2, 33, 0);
            this.volume.setPadding(39, 0, 39, 0);
        } else if (this.device.equals("Dream")) {
            this.unlock.setPadding(24, 0, 24, 0);
            this.volume.setPadding(26, 0, 26, 0);
        }
        if (!this.clockVisible) {
            this.granReloj.setVisibility(4);
            this.ampm.setVisibility(4);
            this.temp.setVisibility(4);
            this.pais.setVisibility(4);
        } else {
            this.granReloj.setVisibility(0);
            this.ampm.setVisibility(0);
            this.temp.setVisibility(0);
            this.pais.setVisibility(0);
        }
        if (!this.volumeVisible) {
            this.volume.setVisibility(4);
        } else {
            this.volume.setVisibility(0);
        }
        if (!this.statusVisible) {
            this.barra.setVisibility(4);
        } else {
            this.barra.setVisibility(0);
        }
        if (!this.notificationVisible) {
            findViewById(R.id.Icons).setVisibility(4);
        } else {
            findViewById(R.id.Icons).setVisibility(0);
        }
        if (this.calendarVisible) {
            try {
                readCalendar(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            this.calendario.setVisibility(8);
        }
    }

    private class DownloadTask extends AsyncTask<String, Void, Object> {
        String dir;

        private DownloadTask() {
            this.dir = "";
        }

        /* synthetic */ DownloadTask(LockScreen lockScreen, DownloadTask downloadTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... args) {
            LockScreen.this.geo = new GeoPosicion((LocationManager) LockScreen.this.getSystemService("location"));
            Geocoder coder = new Geocoder(LockScreen.this.getApplicationContext(), Locale.getDefault());
            new ArrayList();
            try {
                List<Address> address = coder.getFromLocation(Double.parseDouble(LockScreen.this.geo.lat), Double.parseDouble(LockScreen.this.geo.lng), 1);
                this.dir = String.valueOf(address.get(0).getLocality()) + ", " + address.get(0).getAdminArea();
            } catch (Exception e) {
                e.printStackTrace();
                this.dir = "Not available";
            }
            LockScreen.this.wht = new Meteorology(LockScreen.this.geo.lat, LockScreen.this.geo.lng);
            LockScreen.this.wht.setUnit(LockScreen.this.tempUnit);
            return LockScreen.this.wht.loadWeather();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Object result) {
            super.onPostExecute(result);
            if (LockScreen.this.weatherVisible) {
                LockScreen.this.temp.setText(LockScreen.this.wht.getWeatherTemp());
                LockScreen.this.pais.setText(LockScreen.this.wht.getWeatherCondition());
            } else {
                LockScreen.this.temp.setText("");
                LockScreen.this.pais.setText("");
            }
            ((TextView) LockScreen.this.findViewById(R.id.location)).setText(this.dir);
            if (!LockScreen.this.weatherVisible || !result.toString().equals("OK")) {
                LockScreen.this.temp.setText("");
                LockScreen.this.pais.setText("");
                return;
            }
            LockScreen.this.temp.setText(LockScreen.this.wht.getWeatherTemp());
            LockScreen.this.pais.setText(LockScreen.this.wht.getWeatherCondition());
        }
    }

    public String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("SL::IP", e.toString());
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void reloj() {
        int hora;
        int minutos;
        Calendar calendario2 = new GregorianCalendar();
        if (!this.longHourVisible) {
            hora = calendario2.get(10);
            minutos = calendario2.get(12);
            if (calendario2.get(9) == 0) {
                this.ampm.setText("AM");
            } else {
                this.ampm.setText("PM");
            }
            if (hora == 0) {
                hora = 12;
            }
        } else {
            hora = calendario2.get(11);
            minutos = calendario2.get(12);
        }
        String h = String.valueOf(hora);
        String m = String.valueOf(minutos);
        if (this.longHourVisible && h.length() < 2) {
            h = "0" + h;
        }
        if (h.length() < 2) {
            this.granReloj.setPadding(25, 20, 0, 0);
        } else {
            this.granReloj.setPadding(15, 20, 0, 0);
        }
        if (m.length() < 2) {
            m = "0" + m;
        }
        this.granReloj.setText(String.valueOf(h) + ":" + m);
        this.granReloj.setTypeface(this.tf);
    }

    public void onDestroy() {
        super.onDestroy();
        IS_SOWN = false;
        try {
            unregisterReceiver(this.homeButton);
            unregisterReceiver(this.notificador);
            unregisterReceiver(this.songReceiver);
            unregisterReceiver(this.alarmReceiver);
            unregisterReceiver(this.batteryLife);
        } catch (Exception e) {
            Log.d("EXXX", e.getMessage());
            Process.killProcess(Process.myPid());
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == 82 && event.isLongPress()) {
            return true;
        }
        if (event.getAction() == 84 && event.isLongPress()) {
            return true;
        }
        if (event.getAction() != 0) {
            if (event.getAction() == 1) {
                switch (event.getKeyCode()) {
                    case 3:
                        return true;
                    case 4:
                        return true;
                    case 5:
                        return true;
                    case 6:
                        return true;
                    case 27:
                        return true;
                    case 84:
                        return true;
                    default:
                        if (this.preferencias.isVkState()) {
                            switch (event.getKeyCode()) {
                                case JsonParserJavaccConstants.HEX:
                                    return true;
                                case JsonParserJavaccConstants.HEX_ESC:
                                    return true;
                            }
                        }
                        break;
                }
            }
        } else {
            switch (event.getKeyCode()) {
                case 3:
                    return true;
                case 4:
                    return true;
                case 5:
                    return true;
                case 6:
                    return true;
                case 27:
                    return true;
                case 84:
                    return true;
                default:
                    if (this.preferencias.isVkState()) {
                        switch (event.getKeyCode()) {
                            case JsonParserJavaccConstants.HEX:
                                return true;
                            case JsonParserJavaccConstants.HEX_ESC:
                                return true;
                        }
                    }
                    break;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void bateria(int batteryLevel) {
        Exception e;
        this.bateria = (ImageView) findViewById(R.id.bateria);
        if (batteryLevel < 15) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b0, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e2) {
                e = e2;
                try {
                    e.printStackTrace();
                    this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b0));
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            }
        } else if (batteryLevel > 14 && batteryLevel < 25) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b1, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e4) {
                e = e4;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b1));
            }
        } else if (batteryLevel > 24 && batteryLevel < 35) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b2, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e5) {
                e = e5;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b2));
            }
        } else if (batteryLevel > 34 && batteryLevel < 45) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b4, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e6) {
                e = e6;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b4));
            }
        } else if (batteryLevel > 44 && batteryLevel < 55) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b5, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e7) {
                e = e7;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b5));
            }
        } else if (batteryLevel > 54 && batteryLevel < 65) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b6, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e8) {
                e = e8;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b6));
            }
        } else if (batteryLevel > 64 && batteryLevel < 75) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b7, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e9) {
                e = e9;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b7));
            }
        } else if (batteryLevel > 74 && batteryLevel < 85) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b8, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e10) {
                e = e10;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b8));
            }
        } else if (batteryLevel > 84) {
            try {
                this.bateria.setBackgroundDrawable(new BitmapDrawable(this.RES, new FileInputStream(this.RES.getString(R.string.b9, String.valueOf(this.DIRECTORY) + this.THEME))));
            } catch (Exception e11) {
                e = e11;
                e.printStackTrace();
                this.bateria.setBackgroundDrawable(this.RES.getDrawable(R.drawable.b9));
            }
        }
    }

    public void onStart() {
        super.onStart();
        FlurryAgent.onStartSession(this, "LG2L5S257HLAZND3DVUU");
        IS_SOWN = true;
    }

    public void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
        IS_SOWN = false;
        this.sensorManager.unregisterListener(this.theForce, this.sensorManager.getDefaultSensor(8));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.useTheForce) {
            try {
                this.sensorManager.registerListener(this.theForce, this.sensorManager.getDefaultSensor(8), 0);
            } catch (Exception e) {
                Log.d("SENSOR_REG_FAIL", e.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void createDirectory() {
        File file = new File(DIR);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    /* access modifiers changed from: protected */
    public boolean prefs2APP() {
        try {
            try {
                try {
                    this.preferencias = (Preferencias) new ObjectInputStream(new FileInputStream(new File(String.valueOf(DIR) + "/preferencias.bin"))).readObject();
                    return true;
                } catch (OptionalDataException e) {
                    e.printStackTrace();
                    return false;
                } catch (ClassNotFoundException e2) {
                    e2.printStackTrace();
                    return false;
                } catch (IOException e3) {
                    e3.printStackTrace();
                    return false;
                }
            } catch (StreamCorruptedException e4) {
                e4.printStackTrace();
                return false;
            } catch (IOException e5) {
                e5.printStackTrace();
                return false;
            }
        } catch (FileNotFoundException e6) {
            e6.printStackTrace();
            return false;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 17301591, 0, "Show/Hide Unlock").setIcon(17301591);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        showUnlock();
        return true;
    }

    private void showUnlock() {
        if (!this.unlock.isShown()) {
            this.unlock.setVisibility(0);
        } else {
            this.unlock.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void takePicture() {
        this.camera.takePicture(null, null, this.photoCallback);
    }

    private static void readCalendar(Context context) {
        String contentProvider;
        ContentResolver contentResolver = context.getContentResolver();
        StringBuilder s = new StringBuilder();
        if (Build.VERSION.SDK_INT <= 7) {
            contentProvider = "calendar";
        } else {
            contentProvider = "com.android.calendar";
        }
        Cursor cursor = contentResolver.query(Uri.parse("content://" + contentProvider + "/calendars"), new String[]{"_id", "displayName", "selected"}, null, null, null);
        HashSet<String> calendarIds = new HashSet<>();
        while (cursor.moveToNext()) {
            String _id = cursor.getString(0);
            s.append("Calendar account: " + cursor.getString(1) + "\n\n");
            calendarIds.add(_id);
        }
        Iterator it = calendarIds.iterator();
        while (it.hasNext()) {
            Uri.Builder builder = Uri.parse("content://" + contentProvider + "/instances/when").buildUpon();
            long now = new Date().getTime();
            ContentUris.appendId(builder, now - 604800000);
            ContentUris.appendId(builder, now + 604800000);
            Cursor eventCursor = contentResolver.query(builder.build(), new String[]{"title", "begin", "end", "allDay"}, "Calendars._id=" + ((String) it.next()), null, "startDay ASC, startMinute ASC");
            while (eventCursor.moveToNext()) {
                String title = eventCursor.getString(0);
                Date begin = new Date(eventCursor.getLong(1));
                s.append("Event: " + title + "\n\t\t Begin: " + begin + "\n\t\t End: " + new Date(eventCursor.getLong(2)) + "\n");
            }
        }
        textCalendar.setText(s);
    }

    private void crearFondo() {
        Bitmap bb = BitmapFactory.decodeFile("sdcard/Android/data/org.firezenk.simplylock/bg");
        if (bb != null) {
            setTheme((int) R.style.Theme_Normal);
            getWindow().setBackgroundDrawable(new BitmapDrawable(bb));
            return;
        }
        setTheme((int) R.style.Theme_Transparent);
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.translucent_background));
    }

    class SavePhotoTask extends AsyncTask<byte[], String, String> {
        SavePhotoTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(byte[]... jpeg) {
            try {
                FileOutputStream fos = new FileOutputStream(new File("sdcard/Android/data/org.firezenk.simplylock/bg").getPath());
                fos.write(jpeg[0]);
                fos.close();
                Log.e("Photographer", "Picture taken");
                LockScreen.this.setResult(-1, new Intent());
                LockScreen.this.finish();
                return null;
            } catch (IOException e) {
                Log.e("Photographer", "Exception in photoCallback", e);
                LockScreen.this.setResult(0, new Intent());
                LockScreen.this.finish();
                return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void commonUnlock() {
        if (this.useCode) {
            showDialog(ENTER_CODE);
        } else {
            ((UiModeManager) getSystemService("uimode")).disableCarMode(1);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        Dialog d = new Dialog(this);
        switch (id) {
            case ENTER_CODE /*12894*/:
                return secretCode();
            default:
                return d;
        }
    }

    private Dialog secretCode() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.secret_code);
        dialog.setTitle("Secret code required");
        ((Button) dialog.findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText et = (EditText) dialog.findViewById(R.id.pass);
                if (et.getText().toString().equals(LockScreen.this.SECRET_CODE)) {
                    LockScreen.this.finish();
                } else {
                    et.setText("");
                }
            }
        });
        return dialog;
    }
}
