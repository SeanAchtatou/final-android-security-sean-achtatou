package org.firezenk.simplylock;

import android.widget.LinearLayout;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;

public class PubliListener implements AdListener {
    private LinearLayout layout;

    public PubliListener() {
    }

    public PubliListener(LinearLayout layout2) {
        this.layout = layout2;
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
        if (arg1.equals(AdRequest.ErrorCode.NETWORK_ERROR)) {
            this.layout.findViewById(R.id.publi).setVisibility(0);
        }
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
        this.layout.findViewById(R.id.publi).setVisibility(8);
    }
}
