package org.firezenk.simplylock;

import android.app.KeyguardManager;
import android.app.Service;
import android.app.UiModeManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class SLService extends Service {
    public static boolean IS_RUNNING = false;
    /* access modifiers changed from: private */
    public static volatile int call = 0;
    /* access modifiers changed from: private */
    public static volatile StringBuilder line = new StringBuilder("No new sms's");
    private static volatile int mail = 0;
    protected static boolean musicActive = false;
    /* access modifiers changed from: private */
    public static volatile int sms = 0;
    private String PREFERENCES = "org.firezenk.simplylock";
    /* access modifiers changed from: private */
    public String account = "";
    private final PhoneStateListener callReceiver = new MissedCallPhoneStateListener(this, null);
    private KeyguardManager.KeyguardLock kl;
    /* access modifiers changed from: private */
    public boolean onCall = false;
    BroadcastReceiver screensleep = new BroadcastReceiver() {
        public static final String Screenoff = "android.intent.action.SCREEN_OFF";

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Screenoff)) {
                LockScreen.SCREEN_ON = false;
                if (SLService.this.tm.getCallState() != 2) {
                    try {
                        ((UiModeManager) SLService.this.getSystemService("uimode")).enableCarMode(1);
                    } catch (Exception e) {
                    }
                } else if (SLService.this.onCall) {
                    try {
                        ((UiModeManager) SLService.this.getSystemService("uimode")).enableCarMode(1);
                    } catch (Exception e2) {
                    }
                }
            }
        }
    };
    BroadcastReceiver screenwakeup = new BroadcastReceiver() {
        public static final String Screen = "android.intent.action.SCREEN_ON";

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Screen)) {
                LockScreen.SCREEN_ON = true;
                try {
                    if (SLService.this.account != "") {
                        SLService.this.queryLabels();
                    }
                } catch (Exception e) {
                }
                if (SLService.musicActive) {
                    SLService.this.registerReceiver(SLService.this.songReceiver, new IntentFilter("android.media.AUDIO_BECOMING_NOISY"));
                    SLService.this.sendBroadcast(new Intent("android.media.AUDIO_BECOMING_NOISY"));
                }
                Log.d("MusicActive", "False para que funcione musica: " + SLService.musicActive);
            }
        }
    };
    /* access modifiers changed from: private */
    public final SmsParent smsParent = SmsParent.getInstace();
    private final BroadcastReceiver smsReceiver = new IncommingSMS(this, null);
    /* access modifiers changed from: private */
    public BroadcastReceiver songReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            SLService.this.track = intent.getStringExtra("track");
        }
    };
    /* access modifiers changed from: private */
    public TelephonyManager tm;
    /* access modifiers changed from: private */
    public String track = "";

    public static final class LabelColumns {
        public static final String CANONICAL_NAME = "canonicalName";
        public static final String NAME = "name";
        public static final String NUM_CONVERSATIONS = "numConversations";
        public static final String NUM_UNREAD_CONVERSATIONS = "numUnreadConversations";
    }

    public static void clearAll() {
        sms = 0;
        call = 0;
        mail = 0;
        line = new StringBuilder("No new sms's");
    }

    public IBinder onBind(Intent intent) {
        this.onCall = intent.getBooleanExtra("onCalls", false);
        return null;
    }

    public void onCreate() {
        super.onCreate();
        IS_RUNNING = true;
        registerReceiver(this.songReceiver, new IntentFilter("android.media.AUDIO_BECOMING_NOISY"));
        sendBroadcast(new Intent("android.media.AUDIO_BECOMING_NOISY"));
        IntentFilter onfilter = new IntentFilter(AnonymousClass1.Screen);
        IntentFilter offfilter = new IntentFilter(AnonymousClass2.Screenoff);
        registerReceiver(this.screenwakeup, onfilter);
        registerReceiver(this.screensleep, offfilter);
        registerReceiver(this.smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        this.tm = (TelephonyManager) getSystemService("phone");
        getSystemService("phone");
        this.tm.listen(this.callReceiver, 32);
        this.account = getSharedPreferences(this.PREFERENCES, 0).getString("account", "");
        this.kl = ((KeyguardManager) getSystemService("keyguard")).newKeyguardLock(this.PREFERENCES);
        this.kl.disableKeyguard();
        new SimChangeListener(this, this.tm, this.kl).start();
    }

    public void onDestroy() {
        IS_RUNNING = false;
        unregisterReceiver(this.screenwakeup);
        unregisterReceiver(this.screensleep);
        unregisterReceiver(this.smsReceiver);
        this.kl.reenableKeyguard();
    }

    private class IncommingSMS extends BroadcastReceiver {
        private IncommingSMS() {
        }

        /* synthetic */ IncommingSMS(SLService sLService, IncommingSMS incommingSMS) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            try {
                SLService.this.smsParent.doUpdate(context, intent);
                SLService.sms = SLService.this.smsParent.sms;
                SLService.line = SLService.this.smsParent.line;
                SLService.this.notificador();
            } catch (Exception e) {
                Log.d("SIMPLY:sms", "Error receiving sms");
            }
        }
    }

    private class MissedCallPhoneStateListener extends PhoneStateListener {
        boolean offhook;
        boolean ringing;

        private MissedCallPhoneStateListener() {
            this.ringing = false;
            this.offhook = false;
        }

        /* synthetic */ MissedCallPhoneStateListener(SLService sLService, MissedCallPhoneStateListener missedCallPhoneStateListener) {
            this();
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case 0:
                    if (this.ringing && !this.offhook) {
                        SLService.call = SLService.call + 1;
                        this.ringing = false;
                        this.offhook = false;
                        SLService.this.notificador();
                        return;
                    }
                case 1:
                    break;
                case 2:
                    this.offhook = true;
                    this.ringing = false;
                    return;
                default:
                    return;
            }
            this.ringing = true;
            this.offhook = false;
        }
    }

    /* access modifiers changed from: private */
    public void notificador() {
        Intent i = new Intent();
        i.setAction("org.firezenk.simplylock.NOTIFICADOR");
        i.putExtra("SMScount", sms);
        i.putExtra("SMSline", line.toString());
        i.putExtra("CALLcount", call);
        i.putExtra("MAILcount", mail);
        i.putExtra("MUSIC", !((AudioManager) getSystemService("audio")).isMusicActive());
        i.putExtra("track", this.track);
        sendBroadcast(i);
    }

    public void queryLabels() throws Exception {
        Cursor cursor = getContentResolver().query(Uri.withAppendedPath(Uri.parse("content://gmail-ls/labels/"), this.account), null, null, null, null);
        int temp = 0;
        if (cursor.moveToFirst()) {
            int unreadColumn = cursor.getColumnIndex(LabelColumns.NUM_UNREAD_CONVERSATIONS);
            int nameColumn = cursor.getColumnIndex(LabelColumns.NAME);
            do {
                String name = cursor.getString(nameColumn);
                String unread = cursor.getString(unreadColumn);
                if (name.equals("^i")) {
                    temp += Integer.parseInt(unread);
                }
            } while (cursor.moveToNext());
        }
        if (temp != mail) {
            mail = temp;
        }
        notificador();
    }

    public void onRebind(Intent intent) {
        super.onRebind(intent);
        this.onCall = intent.getBooleanExtra("onCalls", false);
        sms = 0;
        call = 0;
        mail = 0;
        line = new StringBuilder("No new sms's");
        registerReceiver(this.songReceiver, new IntentFilter("android.media.AUDIO_BECOMING_NOISY"));
        sendBroadcast(new Intent("android.media.AUDIO_BECOMING_NOISY"));
    }
}
