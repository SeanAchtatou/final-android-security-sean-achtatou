package org.firezenk.simplylock;

import android.content.Context;
import android.content.Intent;
import android.os.Build;

public abstract class SmsParent {
    private static SmsParent sInstance;
    protected StringBuilder line = new StringBuilder("No new sms's");
    protected int sms = 0;

    public abstract void doUpdate(Context context, Intent intent);

    public static SmsParent getInstace() {
        String className;
        if (sInstance == null) {
            if (Integer.parseInt(Build.VERSION.SDK) <= 4) {
                className = "org.firezenk.simplylock.SmsSdk4";
            } else {
                className = "org.firezenk.simplylock.SmsSdk5";
            }
            try {
                sInstance = (SmsParent) Class.forName(className).asSubclass(SmsParent.class).newInstance();
            } catch (Exception e) {
            }
        }
        return sInstance;
    }

    public void setSms(int sms2) {
        this.sms = sms2;
    }

    public int getSms() {
        return this.sms;
    }

    public void setLine(StringBuilder line2) {
        this.line = line2;
    }

    public StringBuilder getLine() {
        return this.line;
    }
}
