package org.firezenk.simplylock;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class HomeButton extends Activity {
    private String PREFERENCES = "org.firezenk.simplylock";
    private Intent intent;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Intent i = new Intent();
            i.setAction("org.firezenk.simplylock.SLService");
            startService(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SharedPreferences settings = getSharedPreferences(this.PREFERENCES, 0);
        this.intent = new Intent("android.intent.action.MAIN");
        this.intent.addCategory("android.intent.category.HOME");
        String pack = settings.getString("pack", "");
        String clas = settings.getString("clas", "");
        this.intent.setComponent(new ComponentName(pack, clas));
        if (!(pack == "" || clas == "")) {
            this.intent.setFlags(270532608);
            Log.d("LockScreen", new StringBuilder().append(LockScreen.IS_SOWN).toString());
            if (LockScreen.IS_SOWN) {
                finish();
            } else {
                LockScreen.IS_SOWN = false;
                startActivity(this.intent);
            }
        }
        finish();
    }
}
