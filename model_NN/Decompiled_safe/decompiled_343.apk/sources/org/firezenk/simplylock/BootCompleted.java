package org.firezenk.simplylock;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompleted extends BroadcastReceiver {
    public void onReceive(Context context, Intent bootIntent) {
        Intent i = new Intent();
        i.setAction("org.firezenk.simplylock.SLService");
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            context.startService(i);
        }
        context.startService(i);
    }
}
