package org.firezenk.simplylock;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;

public class SimChangeListener extends Thread {
    private Context c;
    private KeyguardManager.KeyguardLock kl;
    private int prevSimState = 0;
    private TelephonyManager tm;

    public SimChangeListener(Context c2, TelephonyManager tm2, KeyguardManager.KeyguardLock kl2) {
        this.c = c2;
        this.tm = tm2;
        this.kl = kl2;
    }

    public void run() {
        super.run();
        while (true) {
            simChangeDetect();
            try {
                sleep(10000);
            } catch (InterruptedException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void simChangeDetect() {
        int simState = this.tm.getSimState();
        switch (simState) {
            case 0:
                System.out.println("*SIM_STATE_UNKNOWN*");
                break;
            case 1:
                System.out.println("*Sim State absent*");
                break;
            case 2:
            case 3:
                System.out.println("*SIM_STATE_PIN/PUK_REQUIRED*");
                this.c.stopService(new Intent(this.c, SLService.class));
                this.c.sendBroadcast(new Intent("org.firezenk.simplylock.kill"));
                this.kl.reenableKeyguard();
                break;
            case 4:
                System.out.println("*SIM_STATE_NETWORK_LOCKED*");
                break;
            case 5:
                if (!SLService.IS_RUNNING && (this.prevSimState == 2 || this.prevSimState == 3)) {
                    this.kl.disableKeyguard();
                    try {
                        this.c.startService(new Intent(this.c, SLService.class));
                        this.c.startActivity(new Intent(this.c, LockScreen.class).addFlags(269025280));
                        break;
                    } catch (Exception e) {
                        break;
                    }
                }
        }
        this.prevSimState = simState;
    }
}
