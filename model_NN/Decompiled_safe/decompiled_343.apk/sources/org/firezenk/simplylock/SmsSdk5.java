package org.firezenk.simplylock;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;

public class SmsSdk5 extends SmsParent {
    public void doUpdate(Context context, Intent intent) {
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdus = (Object[]) bundle.get("pdus");
                SmsMessage[] messages = new SmsMessage[pdus.length];
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                }
                for (SmsMessage smsMessage : messages) {
                    this.line.delete(0, this.line.length());
                    Cursor c = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(smsMessage.getDisplayOriginatingAddress())), null, "display_name", null, null);
                    if (c.getCount() > 0) {
                        c.moveToFirst();
                        if (c.getString(c.getColumnIndex("display_name")) != null) {
                            this.line.append(c.getString(c.getColumnIndex("display_name")));
                        } else {
                            this.line.append(smsMessage.getDisplayOriginatingAddress());
                        }
                    } else {
                        this.line.append(smsMessage.getDisplayOriginatingAddress());
                    }
                    this.line.append(": ");
                    this.line.append(smsMessage.getMessageBody());
                    c.close();
                    this.sms++;
                }
            }
        } catch (Exception e) {
        }
    }
}
