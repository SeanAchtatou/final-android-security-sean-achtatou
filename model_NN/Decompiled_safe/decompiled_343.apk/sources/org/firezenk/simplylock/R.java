package org.firezenk.simplylock;

public final class R {

    public static final class anim {
        public static final int fade = 2130968576;
    }

    public static final class array {
        public static final int clockSize = 2131034115;
        public static final int dateFormat = 2131034114;
        public static final int layouts = 2131034116;
        public static final int resDevices = 2131034113;
        public static final int tempClass = 2131034112;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class color {
        public static final int blanco = 2131099649;
        public static final int blancotrans = 2131099650;
        public static final int bright_text_dark_focused = 2131099653;
        public static final int bubble_dark_background = 2131099652;
        public static final int negro = 2131099648;
        public static final int transparente = 2131099651;
    }

    public static final class drawable {
        public static final int aa = 2130837504;
        public static final int b0 = 2130837505;
        public static final int b1 = 2130837506;
        public static final int b2 = 2130837507;
        public static final int b3 = 2130837508;
        public static final int b4 = 2130837509;
        public static final int b5 = 2130837510;
        public static final int b6 = 2130837511;
        public static final int b7 = 2130837512;
        public static final int b8 = 2130837513;
        public static final int b9 = 2130837514;
        public static final int gray_bg = 2130837533;
        public static final int icon = 2130837515;
        public static final int lockscroll = 2130837516;
        public static final int lockthumb = 2130837517;
        public static final int mail = 2130837518;
        public static final int next = 2130837519;
        public static final int pause = 2130837520;
        public static final int paypal = 2130837521;
        public static final int phone = 2130837522;
        public static final int play = 2130837523;
        public static final int prev = 2130837524;
        public static final int sms = 2130837525;
        public static final int soundscroll = 2130837526;
        public static final int soundthumb = 2130837527;
        public static final int statusbar = 2130837528;
        public static final int translucent_background = 2130837532;
        public static final int transparent_background = 2130837531;
        public static final int volumescroll = 2130837529;
        public static final int volumethumb = 2130837530;
    }

    public static final class id {
        public static final int EnableCalls = 2131296335;
        public static final int FCwarning = 2131296309;
        public static final int Icons = 2131296285;
        public static final int ImageView01 = 2131296291;
        public static final int LinearLayout01 = 2131296256;
        public static final int RelativeLayout01 = 2131296265;
        public static final int ScrollView01 = 2131296301;
        public static final int SlidingDrawer = 2131296293;
        public static final int Spinner01 = 2131296330;
        public static final int Spinner02 = 2131296306;
        public static final int Spinner03 = 2131296312;
        public static final int Spinner04 = 2131296348;
        public static final int Spinner05 = 2131296318;
        public static final int Spinner06 = 2131296346;
        public static final int TextView01 = 2131296354;
        public static final int TextView02 = 2131296331;
        public static final int TextView03 = 2131296307;
        public static final int TextView04 = 2131296311;
        public static final int TextView05 = 2131296314;
        public static final int TextView06 = 2131296328;
        public static final int TextView07 = 2131296317;
        public static final int TextView08 = 2131296315;
        public static final int TextView09 = 2131296342;
        public static final int TextView10 = 2131296323;
        public static final int account = 2131296338;
        public static final int accountLine = 2131296257;
        public static final int ad = 2131296355;
        public static final int alamimage = 2131296283;
        public static final int alarm = 2131296277;
        public static final int alarmvisible = 2131296320;
        public static final int ampm = 2131296275;
        public static final int anuncios = 2131296358;
        public static final int autoClear = 2131296343;
        public static final int barra = 2131296268;
        public static final int bateria = 2131296269;
        public static final int button = 2131296261;
        public static final int call0 = 2131296290;
        public static final int clearN = 2131296310;
        public static final int clockLayout = 2131296297;
        public static final int contentLayout = 2131296295;
        public static final int credits = 2131296353;
        public static final int dateformattext = 2131296327;
        public static final int datevisible = 2131296326;
        public static final int deviceText = 2131296305;
        public static final int dialog_text = 2131296379;
        public static final int disableOriginal = 2131296350;
        public static final int fecha = 2131296276;
        public static final int frameLayout1 = 2131296258;
        public static final int general = 2131296302;
        public static final int granReloj = 2131296272;
        public static final int icon = 2131296266;
        public static final int is24 = 2131296308;
        public static final int isAlarm = 2131296329;
        public static final int isBat = 2131296319;
        public static final int isCalendar = 2131296377;
        public static final int isClock = 2131296332;
        public static final int isDate = 2131296325;
        public static final int isEC = 2131296334;
        public static final int isLockbar = 2131296378;
        public static final int isNotification = 2131296341;
        public static final int isOriginal = 2131296321;
        public static final int isPlayer = 2131296322;
        public static final int isStatus = 2131296347;
        public static final int isVol = 2131296337;
        public static final int isWheather = 2131296339;
        public static final int layoutspin = 2131296364;
        public static final int ll = 2131296359;
        public static final int location = 2131296271;
        public static final int lock = 2131296300;
        public static final int mail0 = 2131296292;
        public static final int more = 2131296362;
        public static final int name = 2131296267;
        public static final int next = 2131296281;
        public static final int notVisible = 2131296333;
        public static final int options = 2131296303;
        public static final int others = 2131296352;
        public static final int pais = 2131296274;
        public static final int pass = 2131296365;
        public static final int percentage = 2131296270;
        public static final int phone = 2131296289;
        public static final int play = 2131296280;
        public static final int player = 2131296278;
        public static final int playerV = 2131296313;
        public static final int power_level = 2131296260;
        public static final int prev = 2131296279;
        public static final int proceed = 2131296380;
        public static final int publi = 2131296357;
        public static final int relativeLayout1 = 2131296361;
        public static final int relativeLayout12 = 2131296356;
        public static final int scroll1 = 2131296360;
        public static final int scroll2 = 2131296366;
        public static final int scroll3 = 2131296376;
        public static final int selectWall = 2131296344;
        public static final int showWht = 2131296349;
        public static final int slideHandleButton = 2131296294;
        public static final int sms = 2131296287;
        public static final int sms1 = 2131296288;
        public static final int song = 2131296282;
        public static final int sound = 2131296299;
        public static final int soundbar = 2131296286;
        public static final int stats = 2131296298;
        public static final int statusbarvisible = 2131296340;
        public static final int stop = 2131296351;
        public static final int temp = 2131296273;
        public static final int tempText = 2131296304;
        public static final int text = 2131296259;
        public static final int textCalendar = 2131296296;
        public static final int textView1 = 2131296262;
        public static final int textView2 = 2131296263;
        public static final int textView3 = 2131296264;
        public static final int textView4 = 2131296367;
        public static final int textView5 = 2131296368;
        public static final int textView6 = 2131296369;
        public static final int textView7 = 2131296370;
        public static final int textView8 = 2131296371;
        public static final int textView9 = 2131296373;
        public static final int theForce = 2131296372;
        public static final int themespin = 2131296316;
        public static final int unblock = 2131296284;
        public static final int useCode = 2131296375;
        public static final int useDef = 2131296363;
        public static final int useDefault = 2131296345;
        public static final int vibrate = 2131296374;
        public static final int volumeKeys = 2131296324;
        public static final int volumeVisible = 2131296336;
    }

    public static final class layout {
        public static final int acc = 2130903040;
        public static final int force_power = 2130903041;
        public static final int help = 2130903042;
        public static final int home = 2130903043;
        public static final int lockscreen = 2130903044;
        public static final int lockscreen_b = 2130903045;
        public static final int main = 2130903046;
        public static final int options = 2130903047;
        public static final int other_options = 2130903048;
        public static final int publi_fail = 2130903049;
        public static final int secret_code = 2130903050;
        public static final int spec_options = 2130903051;
        public static final int view_options = 2130903052;
        public static final int welcome_dialog = 2130903053;
    }

    public static final class string {
        public static final int app_name = 2131165185;
        public static final int b0 = 2131165189;
        public static final int b1 = 2131165190;
        public static final int b2 = 2131165191;
        public static final int b3 = 2131165192;
        public static final int b4 = 2131165193;
        public static final int b5 = 2131165194;
        public static final int b6 = 2131165195;
        public static final int b7 = 2131165196;
        public static final int b8 = 2131165197;
        public static final int b9 = 2131165198;
        public static final int fc = 2131165187;
        public static final int font = 2131165211;
        public static final int hello = 2131165184;
        public static final int help = 2131165188;
        public static final int instructions = 2131165186;
        public static final int lockscroll = 2131165199;
        public static final int lockthumb = 2131165200;
        public static final int mail = 2131165201;
        public static final int next = 2131165202;
        public static final int pause = 2131165203;
        public static final int phone = 2131165204;
        public static final int play = 2131165205;
        public static final int prev = 2131165206;
        public static final int sms = 2131165207;
        public static final int statusbar = 2131165208;
        public static final int volumescroll = 2131165209;
        public static final int volumethumb = 2131165210;
    }

    public static final class style {
        public static final int Theme = 2131230720;
        public static final int Theme_Normal = 2131230722;
        public static final int Theme_Translucent = 2131230723;
        public static final int Theme_Transparent = 2131230721;
        public static final int WorkspaceIcon = 2131230724;
        public static final int WorkspaceIcon_Landscape = 2131230726;
        public static final int WorkspaceIcon_Portrait = 2131230725;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
