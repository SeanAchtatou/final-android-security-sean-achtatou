package org.firezenk.simplylock.options;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import org.firezenk.simplylock.GmailAccounts;
import org.firezenk.simplylock.R;
import org.firezenk.simplylock.SLService;

public class OtherOptions extends Activity {
    public static ArrayList<String> cuentas = new ArrayList<>();
    private String PREFERENCES = "org.firezenk.simplylock";
    private Spinner aSpinner;
    private Button clearNotification;
    private Button disableOriginal;
    private SharedPreferences.Editor editor;
    private Spinner laSpinner;
    private Button more;
    private Spinner rSpinner;
    private Button selectWall;
    private SharedPreferences settings;
    private Spinner thSpinner;
    private Button useDef;
    private Button useDefault;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.other_options);
        this.settings = getSharedPreferences(this.PREFERENCES, 0);
        this.editor = this.settings.edit();
        this.rSpinner = (Spinner) findViewById(R.id.Spinner02);
        if (Options.preferencias.getrState().equals("Nexus")) {
            this.rSpinner.setSelection(2);
        } else if (Options.preferencias.getrState().equals("Droid")) {
            this.rSpinner.setSelection(1);
        } else if (Options.preferencias.getrState().equals("Dream")) {
            this.rSpinner.setSelection(3);
        } else if (Options.preferencias.getrState().equals("Magic")) {
            this.rSpinner.setSelection(4);
        } else {
            this.rSpinner.setSelection(0);
        }
        this.rSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 1:
                        Options.preferencias.setrState("Droid");
                        return;
                    case 2:
                        Options.preferencias.setrState("Nexus");
                        return;
                    case 3:
                        Options.preferencias.setrState("Dream");
                        return;
                    case 4:
                        Options.preferencias.setrState("Magic");
                        return;
                    default:
                        return;
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.disableOriginal = (Button) findViewById(R.id.disableOriginal);
        int sdkVersion = Integer.parseInt(Build.VERSION.SDK);
        if (sdkVersion <= 4) {
            this.disableOriginal.setVisibility(4);
            ((TextView) findViewById(R.id.FCwarning)).setVisibility(4);
        } else {
            this.disableOriginal.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    OtherOptions.this.disableOriginal();
                }
            });
        }
        this.clearNotification = (Button) findViewById(R.id.clearN);
        this.clearNotification.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SLService.clearAll();
                Toast.makeText(OtherOptions.this, "Cleared", 0).show();
            }
        });
        this.thSpinner = (Spinner) findViewById(R.id.themespin);
        this.more = (Button) findViewById(R.id.more);
        this.more.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent("android.intent.action.VIEW");
                i.setData(Uri.parse("market://search?q=Simply Lockscreen theme FireZenk"));
                i.addFlags(268435456);
                OtherOptions.this.startActivity(i);
            }
        });
        final String[] children = new File("sdcard/Android/data/org.firezenk.simplylock/themes").list();
        if (children != null) {
            this.thSpinner.setAdapter((SpinnerAdapter) new ArrayAdapter<>(this, 17367048, children));
            int i = 0;
            while (i < children.length) {
                try {
                    if (children[i].equals(Options.preferencias.getThState())) {
                        this.thSpinner.setSelection(i);
                    }
                    i++;
                } catch (Exception e) {
                }
            }
        }
        this.thSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Options.preferencias.setThState(children[position]);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.selectWall = (Button) findViewById(R.id.selectWall);
        this.selectWall.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OtherOptions.this.getIOWallpaper();
            }
        });
        this.useDefault = (Button) findViewById(R.id.useDefault);
        this.useDefault.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Options.preferencias.setWallpaper("");
                try {
                    new File("sdcard/Android/data/org.firezenk.simplylock/bg").delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Toast.makeText(OtherOptions.this, "Background setted", 0).show();
            }
        });
        this.useDef = (Button) findViewById(R.id.useDef);
        this.useDef.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Options.preferencias.setThState("");
                Toast.makeText(OtherOptions.this, "Original theme restored", 0).show();
            }
        });
        this.laSpinner = (Spinner) findViewById(R.id.layoutspin);
        this.laSpinner.setSelection(Options.preferencias.getLayout());
        this.laSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Options.preferencias.setLayout(position);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.aSpinner = (Spinner) findViewById(R.id.Spinner06);
        if (sdkVersion <= 4) {
            this.aSpinner.setVisibility(4);
            ((TextView) findViewById(R.id.account)).setText("Mail feature is not avaible on Android 1.6");
        } else {
            new GmailAccounts(getApplicationContext());
        }
        if (!cuentas.isEmpty()) {
            this.aSpinner.setAdapter((SpinnerAdapter) new BaseAdapter() {
                public View getView(int position, View convertView, ViewGroup parent) {
                    View convertView2 = ((LayoutInflater) OtherOptions.this.getSystemService("layout_inflater")).inflate((int) R.layout.acc, (ViewGroup) null);
                    ((TextView) convertView2.findViewById(R.id.accountLine)).setText(OtherOptions.cuentas.get(position));
                    return convertView2;
                }

                public long getItemId(int position) {
                    return (long) position;
                }

                public String getItem(int position) {
                    return OtherOptions.cuentas.get(position);
                }

                public int getCount() {
                    return OtherOptions.cuentas.size();
                }
            });
            this.aSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                    Options.preferencias.setaSelected(OtherOptions.cuentas.get(position));
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        } else {
            Options.preferencias.setaSelected("");
        }
        for (int i2 = 0; i2 < cuentas.size(); i2++) {
            if (cuentas.get(i2).equals(Options.preferencias.getaSelected())) {
                this.aSpinner.setSelection(i2);
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void disableOriginal() {
        try {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.ChooseLockGeneric"));
            intent.putExtra("lockscreen.password_type", 0);
            intent.putExtra("lockscreen.password_min", 4);
            intent.putExtra("lockscreen.password_max", 4);
            intent.putExtra("confirm_credentials", false);
            intent.addFlags(33554432);
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, "Function is unavaible on your device", 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            Bitmap bitmap = (Bitmap) data.getParcelableExtra("data");
            FileOutputStream fff = null;
            try {
                fff = new FileOutputStream(new File("sdcard/Android/data/org.firezenk.simplylock/bg"));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fff);
            try {
                fff.flush();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            try {
                fff.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            bitmap.recycle();
            Toast.makeText(this, "Background saved", 0).show();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void getIOWallpaper() {
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        Intent intent = new Intent("android.intent.action.GET_CONTENT", (Uri) null);
        intent.setType("image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", dm.widthPixels / 2);
        intent.putExtra("outputY", dm.heightPixels / 2);
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, 2);
    }
}
