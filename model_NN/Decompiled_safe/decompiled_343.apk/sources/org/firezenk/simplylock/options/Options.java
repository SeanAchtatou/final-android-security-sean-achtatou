package org.firezenk.simplylock.options;

import android.app.Dialog;
import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import org.firezenk.simplylock.PubliListener;
import org.firezenk.simplylock.R;
import org.firezenk.simplylock.SLService;

public class Options extends TabActivity {
    private static String DIR = "sdcard/Android/data/org.firezenk.simplylock";
    protected static Preferencias preferencias = new Preferencias();
    private AdView adView;
    private Intent service;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefs2APP();
        setContentView((int) R.layout.options);
        this.adView = new AdView(this, AdSize.BANNER, "a14cd17219d1204");
        LinearLayout layout = (LinearLayout) findViewById(R.id.ad);
        layout.addView(this.adView);
        this.adView.setAdListener(new PubliListener(layout));
        this.adView.loadAd(new AdRequest());
        this.service = new Intent(this, SLService.class);
        this.service.putExtra("onCalls", preferencias.iseState());
        startService(this.service);
        Resources res = getResources();
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("components").setIndicator("Components", res.getDrawable(17301591)).setContent(new Intent().setClass(this, VisibilityOptions.class)));
        tabHost.addTab(tabHost.newTabSpec("tweaks").setIndicator("Tweaks", res.getDrawable(17301570)).setContent(new Intent().setClass(this, SpecificOptions.class)));
        tabHost.addTab(tabHost.newTabSpec("other").setIndicator("Other", res.getDrawable(17301577)).setContent(new Intent().setClass(this, OtherOptions.class)));
        tabHost.setCurrentTab(0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        prefs2APP();
        this.adView.loadAd(new AdRequest());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 17301568, 0, "Help").setIcon(17301568);
        menu.add(0, 17301569, 1, "About/Donate").setIcon(17301569);
        menu.add(0, 17301560, 2, "Shutdown").setIcon(17301560);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 17301560:
                stopService(this.service);
                finish();
                return true;
            case 17301568:
                openHelp();
                return true;
            case 17301569:
                openInfo();
                return true;
            default:
                return false;
        }
    }

    private void openHelp() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.welcome_dialog);
        dialog.setTitle("Help");
        dialog.setCancelable(true);
        ((TextView) dialog.findViewById(R.id.dialog_text)).setText(getResources().getText(R.string.help));
        ((Button) dialog.findViewById(R.id.proceed)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void openInfo() {
        Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.welcome_dialog);
        dialog.setTitle("About me");
        dialog.setCancelable(true);
        ((TextView) dialog.findViewById(R.id.dialog_text)).setText(getResources().getText(R.string.hello));
        Button button = (Button) dialog.findViewById(R.id.proceed);
        button.setText("");
        button.setBackgroundResource(R.drawable.paypal);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Options.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=5KUHHTKBA44L4&lc=ES&item_name=FireZenk%20Developer&currency_code=EUR&bn=PP%2dDonationsBF%3abtn_donateCC_LG%2egif%3aNonHosted")));
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        prefs2SD();
    }

    /* access modifiers changed from: protected */
    public void createDirectory() {
        File file = new File(DIR);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    /* access modifiers changed from: protected */
    public void prefs2SD() {
        createDirectory();
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(String.valueOf(DIR) + "/preferencias.bin"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(fos);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        try {
            out.writeObject(preferencias);
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 8 */
    /* access modifiers changed from: protected */
    public boolean prefs2APP() {
        try {
            try {
                try {
                    preferencias = (Preferencias) new ObjectInputStream(new FileInputStream(new File(String.valueOf(DIR) + "/preferencias.bin"))).readObject();
                    return true;
                } catch (OptionalDataException e) {
                    e.printStackTrace();
                    return false;
                } catch (ClassNotFoundException e2) {
                    e2.printStackTrace();
                    return false;
                } catch (IOException e3) {
                    e3.printStackTrace();
                    return false;
                }
            } catch (StreamCorruptedException e4) {
                e4.printStackTrace();
                return false;
            } catch (IOException e5) {
                e5.printStackTrace();
                return false;
            }
        } catch (FileNotFoundException e6) {
            e6.printStackTrace();
            return false;
        }
    }
}
