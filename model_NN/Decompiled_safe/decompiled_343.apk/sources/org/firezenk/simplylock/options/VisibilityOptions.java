package org.firezenk.simplylock.options;

import android.app.Activity;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import org.firezenk.simplylock.R;

public class VisibilityOptions extends Activity {
    private CheckBox aBox;
    private CheckBox cBox;
    private CheckBox caBox;
    private CheckBox fBox;
    private CheckBox lBox;
    private CheckBox nBox;
    private CheckBox oBox;
    private CheckBox pBox;
    private CheckBox playerBox;
    private CheckBox sBox;
    private CheckBox vBox;
    private CheckBox wBox;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.view_options);
        this.cBox = (CheckBox) findViewById(R.id.isClock);
        this.cBox.setChecked(Options.preferencias.iscState());
        this.cBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setcState(isChecked);
            }
        });
        this.wBox = (CheckBox) findViewById(R.id.isWheather);
        this.wBox.setChecked(Options.preferencias.iswState());
        this.wBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setwState(isChecked);
            }
        });
        this.vBox = (CheckBox) findViewById(R.id.isVol);
        this.vBox.setChecked(Options.preferencias.isvState());
        this.vBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setvState(isChecked);
            }
        });
        this.sBox = (CheckBox) findViewById(R.id.isStatus);
        this.sBox.setChecked(Options.preferencias.issState());
        this.sBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setsState(isChecked);
            }
        });
        this.oBox = (CheckBox) findViewById(R.id.isOriginal);
        this.oBox.setChecked(Options.preferencias.isoState());
        this.oBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setoState(isChecked);
            }
        });
        this.pBox = (CheckBox) findViewById(R.id.isBat);
        this.pBox.setChecked(Options.preferencias.ispState());
        this.pBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setpState(isChecked);
            }
        });
        this.aBox = (CheckBox) findViewById(R.id.isAlarm);
        this.aBox.setChecked(Options.preferencias.isaState());
        this.aBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setaState(isChecked);
            }
        });
        this.fBox = (CheckBox) findViewById(R.id.isDate);
        this.fBox.setChecked(Options.preferencias.isfState());
        this.fBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setfState(isChecked);
            }
        });
        this.nBox = (CheckBox) findViewById(R.id.isNotification);
        this.nBox.setChecked(Options.preferencias.isnState());
        this.nBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setnState(isChecked);
            }
        });
        this.playerBox = (CheckBox) findViewById(R.id.isPlayer);
        this.playerBox.setChecked(Options.preferencias.isPlayerState());
        this.playerBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setPlayerState(isChecked);
            }
        });
        this.lBox = (CheckBox) findViewById(R.id.isLockbar);
        this.lBox.setChecked(Options.preferencias.islState());
        this.lBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setlState(isChecked);
                if (isChecked) {
                    Options.preferencias.setTheForce(isChecked);
                }
            }
        });
        this.caBox = (CheckBox) findViewById(R.id.isCalendar);
        this.caBox.setChecked(Options.preferencias.isCalendar());
        this.caBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setCalendar(isChecked);
            }
        });
    }
}
