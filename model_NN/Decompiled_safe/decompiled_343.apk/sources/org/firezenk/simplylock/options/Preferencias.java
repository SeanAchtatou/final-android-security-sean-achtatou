package org.firezenk.simplylock.options;

import java.io.Serializable;

public class Preferencias implements Serializable {
    private static final long serialVersionUID = 3483178496851927966L;
    private String THEME = "";
    private String aSelected = "";
    private boolean aState;
    private boolean acState;
    private boolean cState;
    private int clockSizeState;
    private boolean eState;
    private boolean fState;
    private int forceLevel = 5;
    private int formatState;
    private boolean hState;
    private boolean isCalendar;
    private boolean lState = true;
    private int layout = 0;
    private boolean nState;
    private boolean oState;
    private boolean pState;
    private boolean playerState;
    private String rState = "";
    private boolean sState;
    private String secretCode = "";
    private boolean soState;
    private boolean theForce = true;
    private int uState;
    private boolean useCode;
    private boolean vState;
    private boolean viState;
    private boolean vkState;
    private boolean wState;
    private String wallpaper = "";

    public boolean iscState() {
        return this.cState;
    }

    public void setcState(boolean cState2) {
        this.cState = cState2;
    }

    public boolean isvState() {
        return this.vState;
    }

    public void setvState(boolean vState2) {
        this.vState = vState2;
    }

    public boolean issState() {
        return this.sState;
    }

    public void setsState(boolean sState2) {
        this.sState = sState2;
    }

    public boolean isoState() {
        return this.oState;
    }

    public void setoState(boolean oState2) {
        this.oState = oState2;
    }

    public boolean ispState() {
        return this.pState;
    }

    public void setpState(boolean pState2) {
        this.pState = pState2;
    }

    public boolean isfState() {
        return this.fState;
    }

    public void setfState(boolean fState2) {
        this.fState = fState2;
    }

    public boolean isnState() {
        return this.nState;
    }

    public void setnState(boolean nState2) {
        this.nState = nState2;
    }

    public boolean isaState() {
        return this.aState;
    }

    public void setaState(boolean aState2) {
        this.aState = aState2;
    }

    public boolean iswState() {
        return this.wState;
    }

    public void setwState(boolean wState2) {
        this.wState = wState2;
    }

    public boolean isPlayerState() {
        return this.playerState;
    }

    public void setPlayerState(boolean playerState2) {
        this.playerState = playerState2;
    }

    public String getrState() {
        return this.rState;
    }

    public void setrState(String rState2) {
        this.rState = rState2;
    }

    public String getaSelected() {
        return this.aSelected;
    }

    public void setaSelected(String aSelected2) {
        this.aSelected = aSelected2;
    }

    public String getThState() {
        return this.THEME;
    }

    public void setThState(String thState) {
        this.THEME = thState;
    }

    public int getuState() {
        return this.uState;
    }

    public void setuState(int uState2) {
        this.uState = uState2;
    }

    public boolean ishState() {
        return this.hState;
    }

    public void sethState(boolean hState2) {
        this.hState = hState2;
    }

    public boolean iseState() {
        return this.eState;
    }

    public void seteState(boolean eState2) {
        this.eState = eState2;
    }

    public int getFormatState() {
        return this.formatState;
    }

    public void setFormatState(int formatState2) {
        this.formatState = formatState2;
    }

    public int getClockSizeState() {
        return this.clockSizeState;
    }

    public void setClockSizeState(int clockSizeState2) {
        this.clockSizeState = clockSizeState2;
    }

    public boolean isAcState() {
        return this.acState;
    }

    public void setAcState(boolean acState2) {
        this.acState = acState2;
    }

    public String getWallpaper() {
        return this.wallpaper;
    }

    public void setWallpaper(String wallpaper2) {
        this.wallpaper = wallpaper2;
    }

    public boolean isVkState() {
        return this.vkState;
    }

    public void setVkState(boolean vkState2) {
        this.vkState = vkState2;
    }

    public void setTheForce(boolean theForce2) {
        this.theForce = theForce2;
    }

    public boolean isTheForce() {
        return this.theForce;
    }

    public void setLayout(int layout2) {
        this.layout = layout2;
    }

    public int getLayout() {
        return this.layout;
    }

    public void setlState(boolean lState2) {
        this.lState = lState2;
    }

    public boolean islState() {
        return this.lState;
    }

    public void setViState(boolean viState2) {
        this.viState = viState2;
    }

    public boolean isViState() {
        return this.viState;
    }

    public void setSoState(boolean soState2) {
        this.soState = soState2;
    }

    public boolean isSoState() {
        return this.soState;
    }

    public void setCalendar(boolean isCalendar2) {
        this.isCalendar = isCalendar2;
    }

    public boolean isCalendar() {
        return this.isCalendar;
    }

    public void setUseCode(boolean useCode2) {
        this.useCode = useCode2;
    }

    public boolean isUseCode() {
        return this.useCode;
    }

    public void setSecretCode(String secretCode2) {
        this.secretCode = secretCode2;
    }

    public String getSecretCode() {
        return this.secretCode;
    }

    public void setForceLevel(int forceLevel2) {
        this.forceLevel = forceLevel2;
    }

    public int getForceLevel() {
        return this.forceLevel;
    }
}
