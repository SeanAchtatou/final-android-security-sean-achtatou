package org.firezenk.simplylock.options;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import org.firezenk.simplylock.R;
import org.firezenk.simplylock.SLService;

public class SpecificOptions extends Activity {
    private String PREFERENCES = "org.firezenk.simplylock";
    private CheckBox acBox;
    private Spinner cSpinner;
    private CheckBox eBox;
    private SharedPreferences.Editor editor;
    private CheckBox fBox;
    private Spinner fSpinner;
    private CheckBox hBox;
    private CheckBox pBox;
    /* access modifiers changed from: private */
    public Intent service;
    private SharedPreferences settings;
    private CheckBox soBox;
    private Spinner uSpinner;
    String uri = null;
    private CheckBox viBox;
    private CheckBox vkBox;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.spec_options);
        this.settings = getSharedPreferences(this.PREFERENCES, 0);
        this.editor = this.settings.edit();
        this.vkBox = (CheckBox) findViewById(R.id.volumeKeys);
        this.vkBox.setChecked(Options.preferencias.isVkState());
        this.vkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setVkState(isChecked);
            }
        });
        this.acBox = (CheckBox) findViewById(R.id.autoClear);
        this.acBox.setChecked(Options.preferencias.isAcState());
        this.acBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setAcState(isChecked);
            }
        });
        this.hBox = (CheckBox) findViewById(R.id.is24);
        this.hBox.setChecked(Options.preferencias.ishState());
        this.hBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.sethState(isChecked);
            }
        });
        this.eBox = (CheckBox) findViewById(R.id.isEC);
        this.eBox.setChecked(Options.preferencias.iseState());
        this.eBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.seteState(isChecked);
                SpecificOptions.this.service = new Intent(SpecificOptions.this, SLService.class);
                SpecificOptions.this.service.putExtra("onCalls", Options.preferencias.iseState());
                SpecificOptions.this.stopService(SpecificOptions.this.service);
                SpecificOptions.this.service.putExtra("onCalls", Options.preferencias.iseState());
                SpecificOptions.this.startService(SpecificOptions.this.service);
            }
        });
        this.fBox = (CheckBox) findViewById(R.id.theForce);
        this.fBox.setChecked(Options.preferencias.isTheForce());
        this.fBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setTheForce(isChecked);
                if (isChecked) {
                    SpecificOptions.this.showDialog(1);
                }
            }
        });
        this.pBox = (CheckBox) findViewById(R.id.useCode);
        this.pBox.setChecked(Options.preferencias.isUseCode());
        this.pBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    SpecificOptions.this.showDialog(2);
                }
                Options.preferencias.setUseCode(isChecked);
            }
        });
        this.viBox = (CheckBox) findViewById(R.id.vibrate);
        this.viBox.setChecked(Options.preferencias.isViState());
        this.viBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setViState(isChecked);
            }
        });
        this.soBox = (CheckBox) findViewById(R.id.sound);
        this.soBox.setChecked(Options.preferencias.isSoState());
        this.soBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Options.preferencias.setSoState(isChecked);
                if (isChecked) {
                    Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
                    intent.putExtra("android.intent.extra.ringtone.TYPE", 2);
                    intent.putExtra("android.intent.extra.ringtone.TITLE", "Select Tone");
                    if (SpecificOptions.this.uri != null) {
                        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", Uri.parse(SpecificOptions.this.uri));
                    } else {
                        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", (Parcelable) null);
                    }
                    SpecificOptions.this.startActivityForResult(intent, 7);
                }
            }
        });
        this.uSpinner = (Spinner) findViewById(R.id.Spinner01);
        this.uSpinner.setSelection(Options.preferencias.getuState());
        this.uSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Options.preferencias.setuState(position);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.fSpinner = (Spinner) findViewById(R.id.Spinner04);
        this.fSpinner.setSelection(Options.preferencias.getFormatState());
        this.fSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                Options.preferencias.setFormatState(position);
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        this.cSpinner = (Spinner) findViewById(R.id.Spinner05);
        switch (Options.preferencias.getClockSizeState()) {
            case 60:
                this.cSpinner.setSelection(0);
                break;
            case 65:
                this.cSpinner.setSelection(1);
                break;
            case 70:
                this.cSpinner.setSelection(2);
                break;
            case 75:
                this.cSpinner.setSelection(3);
                break;
            case 85:
                this.cSpinner.setSelection(4);
                break;
            case 95:
                this.cSpinner.setSelection(5);
                break;
            case 110:
                this.cSpinner.setSelection(6);
                break;
        }
        this.cSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                switch (position) {
                    case 0:
                        Options.preferencias.setClockSizeState(60);
                        return;
                    case 1:
                        Options.preferencias.setClockSizeState(65);
                        return;
                    case 2:
                        Options.preferencias.setClockSizeState(70);
                        return;
                    case 3:
                        Options.preferencias.setClockSizeState(75);
                        return;
                    case 4:
                        Options.preferencias.setClockSizeState(85);
                        return;
                    case 5:
                        Options.preferencias.setClockSizeState(95);
                        return;
                    case 6:
                        Options.preferencias.setClockSizeState(110);
                        return;
                    default:
                        return;
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Uri uri2;
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && (uri2 = (Uri) data.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI")) != null) {
            this.uri = uri2.toString();
            this.editor.putString("ringtone", uri2.toString());
            this.editor.commit();
            Toast.makeText(this, "Tone setted", 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        final Dialog dialog = new Dialog(this);
        if (id == 1) {
            dialog.setContentView((int) R.layout.force_power);
            dialog.setTitle("Force power");
            final SeekBar mProgress = (SeekBar) dialog.findViewById(R.id.power_level);
            mProgress.setProgress(Options.preferencias.getForceLevel());
            ((Button) dialog.findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Options.preferencias.setForceLevel(mProgress.getProgress());
                    Toast.makeText(SpecificOptions.this, "Saved", 0).show();
                    SpecificOptions.this.dismissDialog(1);
                }
            });
        } else {
            dialog.setContentView((int) R.layout.secret_code);
            dialog.setTitle("Secret code required");
            ((Button) dialog.findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Options.preferencias.setSecretCode(((EditText) dialog.findViewById(R.id.pass)).getText().toString());
                    Toast.makeText(SpecificOptions.this, "Saved", 0).show();
                    SpecificOptions.this.dismissDialog(1);
                }
            });
        }
        return dialog;
    }
}
