package org.firezenk.simplylock;

import java.io.BufferedInputStream;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.util.ByteArrayBuffer;

public class Weather {
    private String city = "";
    private String lat = "";
    private String lng = "";
    private String pattern_condition = "<condition\\sdata=.(((\\w+)(\\s*))+)";
    private String pattern_humidity = "<humidity\\sdata=.(\\w+):\\s(\\d+)";
    private String pattern_tempC = "<temp_c\\sdata=.(\\d+)";
    private String pattern_tempF = "<temp_f\\sdata=.(\\d+)";
    private Pattern regex;
    private Matcher regexMatcher;
    int unitPos = 0;
    private String[] units = {" ¬∫C", " ¬∫F", " K"};
    private String weatherCondition = "";
    private String weatherHumidity = "";
    private String weatherTemp = "";

    public Weather() {
    }

    public Weather(String latitude, String longitude) {
        if (!(latitude == "" || longitude == "")) {
            this.lat = latitude;
            this.lng = longitude;
        }
        loadWeather(false);
    }

    public Weather(String city2) {
        if (city2 != "") {
            this.city = city2;
        }
        loadWeather(true);
    }

    public void loadWeather(boolean isCity) {
        String myString;
        URL myURL;
        if (!isCity) {
            try {
                myURL = new URL("http://www.google.com/ig/api?weather=,,," + this.lat + "," + this.lng);
            } catch (Exception e) {
                myString = e.getMessage();
            }
        } else {
            myURL = new URL("http://www.google.com/ig/api?weather=" + this.city);
        }
        BufferedInputStream bis = new BufferedInputStream(myURL.openConnection().getInputStream());
        ByteArrayBuffer baf = new ByteArrayBuffer(50);
        while (true) {
            int current = bis.read();
            if (current == -1) {
                break;
            }
            baf.append((byte) current);
        }
        myString = new String(baf.toByteArray());
        try {
            this.regex = Pattern.compile(this.pattern_humidity);
            this.regexMatcher = this.regex.matcher(myString);
            if (this.regexMatcher.find()) {
                setWeatherHumidity(String.valueOf(this.regexMatcher.group(2)) + "%");
            } else {
                setWeatherHumidity("");
            }
            this.regex = Pattern.compile(this.pattern_condition);
            this.regexMatcher = this.regex.matcher(myString);
            if (this.regexMatcher.find()) {
                setWeatherCondition(this.regexMatcher.group(1));
            } else {
                setWeatherCondition("");
            }
            updateTempUnit(myString);
        } catch (Exception e2) {
            setWeatherCondition("");
            setWeatherHumidity("");
            setWeatherTemp("");
        }
    }

    private String updateTempUnit(String aux) {
        switch (this.unitPos) {
            case 0:
                this.regex = Pattern.compile(this.pattern_tempC);
                this.regexMatcher = this.regex.matcher(aux);
                if (!this.regexMatcher.find()) {
                    setWeatherTemp("");
                    break;
                } else {
                    setWeatherTemp(String.valueOf(this.regexMatcher.group(1)) + this.units[0]);
                    break;
                }
            case 1:
                this.regex = Pattern.compile(this.pattern_tempF);
                this.regexMatcher = this.regex.matcher(aux);
                if (!this.regexMatcher.find()) {
                    setWeatherTemp("");
                    break;
                } else {
                    setWeatherTemp(String.valueOf(this.regexMatcher.group(1)) + this.units[1]);
                    break;
                }
        }
        return aux;
    }

    public void setWeatherCondition(String weatherCondition2) {
        this.weatherCondition = weatherCondition2;
    }

    public String getWeatherCondition() {
        return this.weatherCondition;
    }

    public void setWeatherTemp(String weatherTemp2) {
        this.weatherTemp = weatherTemp2;
    }

    public String getWeatherTemp() {
        return this.weatherTemp;
    }

    public void setWeatherHumidity(String weatherHumidity2) {
        this.weatherHumidity = weatherHumidity2;
    }

    public String getWeatherHumidity() {
        return this.weatherHumidity;
    }

    public void setUnit(int unit) {
        this.unitPos = unit;
        loadWeather(false);
    }
}
