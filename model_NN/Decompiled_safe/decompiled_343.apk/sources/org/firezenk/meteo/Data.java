package org.firezenk.meteo;

import java.util.ArrayList;
import java.util.List;

public class Data {
    protected List<CurrentCondition> current_condition = new ArrayList();
    protected List<Request> request = new ArrayList();
    protected List<Weather> weather = new ArrayList();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Weather it : this.weather) {
            sb.append(it.toString());
        }
        for (CurrentCondition it2 : this.current_condition) {
            sb.append(it2.toString());
        }
        for (Request it3 : this.request) {
            sb.append(it3.toString());
        }
        return sb.toString();
    }
}
