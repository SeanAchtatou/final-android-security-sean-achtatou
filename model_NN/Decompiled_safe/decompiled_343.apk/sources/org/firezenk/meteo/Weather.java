package org.firezenk.meteo;

import java.util.ArrayList;
import java.util.List;

public class Weather {
    protected String date = "";
    protected float precipMM;
    protected float tempMaxC;
    protected float tempMaxF;
    protected float tempMinC;
    protected float tempMinF;
    protected float weatherCode;
    protected List<WeatherDesc> weatherDesc = new ArrayList();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.date);
        sb.append(this.precipMM);
        sb.append(this.tempMaxC);
        sb.append(this.tempMaxF);
        sb.append(this.tempMinC);
        sb.append(this.tempMinF);
        sb.append(this.weatherCode);
        for (WeatherDesc it : this.weatherDesc) {
            sb.append(it.toString());
        }
        return sb.toString();
    }
}
