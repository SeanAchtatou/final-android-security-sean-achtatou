package org.firezenk.meteo;

import android.util.Log;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import org.json.JSONException;
import org.json.JSONObject;

public class Meteorology {
    private int unitPos;
    private String weatherCondition;
    private String weatherTemp;
    private StringBuilder web;

    public Meteorology() {
        this.weatherCondition = "";
        this.weatherTemp = "";
        this.unitPos = 0;
    }

    public Meteorology(String ip) {
        this.weatherCondition = "";
        this.weatherTemp = "";
        this.unitPos = 0;
        this.web = new StringBuilder("http://free.worldweatheronline.com/feed/weather.ashx?q=");
        this.web.append(ip);
        this.web.append("&format=json&num_of_days=1&key=7818d56c6e162123111701");
    }

    public Meteorology(String lat, String lng) {
        this.weatherCondition = "";
        this.weatherTemp = "";
        this.unitPos = 0;
        this.web = new StringBuilder("http://free.worldweatheronline.com/feed/weather.ashx?q=");
        this.web.append(String.valueOf(lat) + "," + lng);
        this.web.append("&format=json&num_of_days=1&key=7818d56c6e162123111701");
    }

    public String loadWeather() {
        try {
            HttpURLConnection http = conectar(this.web.toString());
            InputStream urlInputStream = null;
            try {
                urlInputStream = http.getInputStream();
            } catch (IOException e) {
                Log.d("SL:STREAM", e.getMessage());
                Log.d("SL:CONTsize", new StringBuilder().append(http.getContentLength()).toString());
                try {
                    Log.d("SL:FICHERO", http.getResponseMessage());
                } catch (IOException e2) {
                }
            }
            String jsontext = null;
            try {
                jsontext = convertStreamToString(urlInputStream);
            } catch (IOException e3) {
                Log.d("SL:toSTRING", e3.getMessage());
            }
            Gson a = new Gson();
            Meteo meteo = new Meteo();
            JSONObject jsondata = new JSONObject();
            try {
                jsondata = new JSONObject(jsontext);
            } catch (JSONException e4) {
                JSONException e5 = e4;
                Log.d("SL:JSONe", e5.getMessage());
                e5.printStackTrace();
            }
            if (jsondata.length() > 0) {
                try {
                    meteo = (Meteo) a.fromJson(jsondata.toString(), Meteo.class);
                } catch (JsonParseException e6) {
                    Log.d("SL:PARSE", jsondata.toString());
                    e6.printStackTrace();
                    return "KOparse";
                } catch (Exception e7) {
                    Exception e8 = e7;
                    Log.d("SL:JEX", e8.getMessage());
                    e8.printStackTrace();
                    return "KOex";
                }
            }
            http.disconnect();
            setWeatherCondition(meteo.data.current_condition.get(0).weatherDesc.get(0).value);
            if (this.unitPos == 0) {
                setWeatherTemp(meteo.data.current_condition.get(0).temp_C + " ºC");
            } else {
                setWeatherTemp(meteo.data.current_condition.get(0).temp_F + " ºF");
            }
            return "OK";
        } catch (Exception e9) {
            Exception ex = e9;
            setWeatherCondition("");
            setWeatherTemp("");
            Log.d("SL", ex.getMessage());
            ex.printStackTrace();
            return "KO";
        }
    }

    public void setWeatherCondition(String weatherCondition2) {
        this.weatherCondition = weatherCondition2;
    }

    public String getWeatherCondition() {
        return this.weatherCondition;
    }

    public void setWeatherTemp(String weatherTemp2) {
        this.weatherTemp = weatherTemp2;
    }

    public String getWeatherTemp() {
        return this.weatherTemp;
    }

    public void setUnit(int unit) {
        this.unitPos = unit;
    }

    /* JADX INFO: finally extract failed */
    public static String convertStreamToString(InputStream is) throws IOException {
        if (is == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    is.close();
                    return sb.toString();
                }
                sb.append(line).append("\n");
            }
        } catch (Throwable th) {
            is.close();
            throw th;
        }
    }

    /* JADX WARN: Type inference failed for: r6v7, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r6v12, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.net.HttpURLConnection conectar(java.lang.String r9) {
        /*
            r8 = this;
            r4 = 0
            java.net.URL r5 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0037 }
            r5.<init>(r9)     // Catch:{ MalformedURLException -> 0x0037 }
            r4 = r5
        L_0x0007:
            r2 = 0
            java.lang.String r6 = r4.getProtocol()
            java.lang.String r6 = r6.toLowerCase()
            java.lang.String r7 = "https"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x004f
            r8.trustEveryone()
            r3 = 0
            java.net.URLConnection r6 = r4.openConnection()     // Catch:{ IOException -> 0x0043 }
            r0 = r6
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ IOException -> 0x0043 }
            r3 = r0
        L_0x0024:
            r2 = r3
            java.lang.String r6 = "RTIME CON"
            java.lang.String r7 = r3.toString()
            android.util.Log.d(r6, r7)
        L_0x002e:
            org.firezenk.meteo.Meteorology$MyAuthenticator r6 = new org.firezenk.meteo.Meteorology$MyAuthenticator
            r6.<init>()
            java.net.Authenticator.setDefault(r6)
            return r2
        L_0x0037:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = "RTIME:URL"
            java.lang.String r7 = r1.getMessage()
            android.util.Log.d(r6, r7)
            goto L_0x0007
        L_0x0043:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = "RTIME:OPENCONs"
            java.lang.String r7 = r1.getMessage()
            android.util.Log.d(r6, r7)
            goto L_0x0024
        L_0x004f:
            java.net.URLConnection r6 = r4.openConnection()     // Catch:{ IOException -> 0x0058 }
            r0 = r6
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x0058 }
            r2 = r0
            goto L_0x002e
        L_0x0058:
            r6 = move-exception
            r1 = r6
            java.lang.String r6 = "RTIME:OPENCONn"
            java.lang.String r7 = r1.getMessage()
            android.util.Log.d(r6, r7)
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.firezenk.meteo.Meteorology.conectar(java.lang.String):java.net.HttpURLConnection");
    }

    private void trustEveryone() {
        try {
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, new X509TrustManager[]{new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) {
            Log.d("RTIME:SSL", e.getMessage());
        }
    }

    public static class MyAuthenticator extends Authenticator {
        /* access modifiers changed from: protected */
        public PasswordAuthentication getPasswordAuthentication() {
            System.out.println("1" + getRequestingPrompt());
            System.out.println("2" + getRequestingHost());
            System.out.println("3" + getRequestingSite());
            return new PasswordAuthentication("", "".toCharArray());
        }
    }
}
