package org.firezenk.meteo;

import java.util.ArrayList;
import java.util.List;

public class CurrentCondition {
    protected int humidity = 0;
    protected int temp_C = 0;
    protected int temp_F = 0;
    protected List<WeatherDesc> weatherDesc = new ArrayList();

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.humidity);
        sb.append(this.temp_C);
        sb.append(this.temp_F);
        for (WeatherDesc it : this.weatherDesc) {
            sb.append(it.toString());
        }
        return sb.toString();
    }
}
