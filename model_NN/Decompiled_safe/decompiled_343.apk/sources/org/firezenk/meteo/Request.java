package org.firezenk.meteo;

public class Request {
    protected String query = "";
    protected String type = "";

    public String toString() {
        return this.type + this.query;
    }
}
