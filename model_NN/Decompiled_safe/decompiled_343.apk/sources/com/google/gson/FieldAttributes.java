package com.google.gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

public final class FieldAttributes {
    private final Field field;

    FieldAttributes(Field f) {
        Preconditions.checkNotNull(f);
        this.field = f;
    }

    public String getName() {
        return this.field.getName();
    }

    public Type getDeclaredType() {
        return this.field.getGenericType();
    }

    public Class<?> getDeclaredClass() {
        return this.field.getType();
    }

    public <T extends Annotation> T getAnnotation(Class<T> annotation) {
        return this.field.getAnnotation(annotation);
    }

    public boolean hasModifier(int modifier) {
        return (this.field.getModifiers() & modifier) != 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isSynthetic() {
        return this.field.isSynthetic();
    }
}
