package com.google.gson;

import java.io.EOFException;
import java.io.Reader;
import java.io.StringReader;

public final class JsonParser {
    public JsonElement parse(String json) throws JsonParseException {
        return parse(new StringReader(json));
    }

    public JsonElement parse(Reader json) throws JsonParseException {
        try {
            return new JsonParserJavacc(json).parse();
        } catch (TokenMgrError e) {
            throw new JsonParseException("Failed parsing JSON source: " + json + " to Json", e);
        } catch (ParseException e2) {
            throw new JsonParseException("Failed parsing JSON source: " + json + " to Json", e2);
        } catch (StackOverflowError e3) {
            throw new JsonParseException("Failed parsing JSON source: " + json + " to Json", e3);
        } catch (OutOfMemoryError e4) {
            throw new JsonParseException("Failed parsing JSON source: " + json + " to Json", e4);
        } catch (JsonParseException e5) {
            JsonParseException e6 = e5;
            if (e6.getCause() instanceof EOFException) {
                return JsonNull.createJsonNull();
            }
            throw e6;
        }
    }
}
