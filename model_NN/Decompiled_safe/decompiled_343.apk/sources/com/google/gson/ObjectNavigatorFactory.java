package com.google.gson;

final class ObjectNavigatorFactory {
    private final FieldNamingStrategy fieldNamingPolicy;
    private final ExclusionStrategy strategy;

    public ObjectNavigatorFactory(ExclusionStrategy strategy2, FieldNamingStrategy fieldNamingPolicy2) {
        ExclusionStrategy exclusionStrategy;
        Preconditions.checkNotNull(fieldNamingPolicy2);
        if (strategy2 == null) {
            exclusionStrategy = new NullExclusionStrategy();
        } else {
            exclusionStrategy = strategy2;
        }
        this.strategy = exclusionStrategy;
        this.fieldNamingPolicy = fieldNamingPolicy2;
    }

    public ObjectNavigator create(ObjectTypePair objTypePair) {
        return new ObjectNavigator(objTypePair, this.strategy);
    }

    /* access modifiers changed from: package-private */
    public FieldNamingStrategy getFieldNamingPolicy() {
        return this.fieldNamingPolicy;
    }
}
