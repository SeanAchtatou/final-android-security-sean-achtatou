package com.google.gson;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

final class ParameterizedTypeHandlerMap<T> {
    private static final Logger logger = Logger.getLogger(ParameterizedTypeHandlerMap.class.getName());
    private final Map<Type, T> map = new HashMap();
    private boolean modifiable = true;

    ParameterizedTypeHandlerMap() {
    }

    public synchronized void register(Type typeOfT, T value) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        if (hasSpecificHandlerFor(typeOfT)) {
            logger.log(Level.WARNING, "Overriding the existing type handler for " + typeOfT);
        }
        this.map.put(typeOfT, value);
    }

    public synchronized void registerIfAbsent(ParameterizedTypeHandlerMap<T> other) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        }
        for (Map.Entry<Type, T> entry : other.entrySet()) {
            if (!this.map.containsKey(entry.getKey())) {
                register(entry.getKey(), entry.getValue());
            }
        }
    }

    public synchronized void registerIfAbsent(Type typeOfT, T value) {
        if (!this.modifiable) {
            throw new IllegalStateException("Attempted to modify an unmodifiable map.");
        } else if (!this.map.containsKey(typeOfT)) {
            register(typeOfT, value);
        }
    }

    public synchronized void makeUnmodifiable() {
        this.modifiable = false;
    }

    public synchronized T getHandlerFor(Type type) {
        T handler;
        handler = this.map.get(type);
        if (handler == null) {
            Class<?> rawClass = TypeUtils.toRawClass(type);
            if (rawClass != type) {
                handler = getHandlerFor(rawClass);
            }
            if (handler == null) {
                if (Map.class.isAssignableFrom(rawClass)) {
                    handler = this.map.get(Map.class);
                } else if (Collection.class.isAssignableFrom(rawClass)) {
                    handler = this.map.get(Collection.class);
                } else if (Enum.class.isAssignableFrom(rawClass)) {
                    handler = this.map.get(Enum.class);
                }
            }
        }
        return handler;
    }

    public synchronized boolean hasSpecificHandlerFor(Type type) {
        return this.map.containsKey(type);
    }

    public synchronized ParameterizedTypeHandlerMap<T> copyOf() {
        ParameterizedTypeHandlerMap<T> copy;
        copy = new ParameterizedTypeHandlerMap<>();
        for (Map.Entry<Type, T> entry : this.map.entrySet()) {
            copy.register(entry.getKey(), entry.getValue());
        }
        return copy;
    }

    public synchronized Set<Map.Entry<Type, T>> entrySet() {
        return this.map.entrySet();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("{");
        boolean first = true;
        for (Map.Entry<Type, T> entry : this.map.entrySet()) {
            if (first) {
                first = false;
            } else {
                sb.append(',');
            }
            sb.append(typeToString(entry.getKey())).append(':');
            sb.append((Object) entry.getValue());
        }
        return sb.toString();
    }

    private String typeToString(Type type) {
        return TypeUtils.toRawClass(type).getSimpleName();
    }
}
