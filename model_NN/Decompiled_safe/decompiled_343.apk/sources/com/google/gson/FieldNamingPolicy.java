package com.google.gson;

import com.google.gson.ModifyFirstLetterNamingPolicy;

public enum FieldNamingPolicy {
    UPPER_CAMEL_CASE(new ModifyFirstLetterNamingPolicy(ModifyFirstLetterNamingPolicy.LetterModifier.UPPER)),
    LOWER_CASE_WITH_UNDERSCORES(new LowerCamelCaseSeparatorNamingPolicy("_")),
    LOWER_CASE_WITH_DASHES(new LowerCamelCaseSeparatorNamingPolicy("-"));
    
    private final FieldNamingStrategy namingPolicy;

    private FieldNamingPolicy(FieldNamingStrategy namingPolicy2) {
        this.namingPolicy = namingPolicy2;
    }

    /* access modifiers changed from: package-private */
    public FieldNamingStrategy getFieldNamingPolicy() {
        return this.namingPolicy;
    }
}
