package com.google.gson;

import java.io.IOException;

final class JsonPrintFormatter implements JsonFormatter {
    public static final int DEFAULT_INDENTATION_SIZE = 2;
    public static final int DEFAULT_PRINT_MARGIN = 80;
    public static final int DEFAULT_RIGHT_MARGIN = 4;
    private final boolean escapeHtmlChars;
    /* access modifiers changed from: private */
    public final int indentationSize;
    /* access modifiers changed from: private */
    public final int printMargin;
    /* access modifiers changed from: private */
    public final int rightMargin;

    JsonPrintFormatter() {
        this(true);
    }

    JsonPrintFormatter(boolean escapeHtmlChars2) {
        this(80, 2, 4, escapeHtmlChars2);
    }

    JsonPrintFormatter(int printMargin2, int indentationSize2, int rightMargin2, boolean escapeHtmlChars2) {
        this.printMargin = printMargin2;
        this.indentationSize = indentationSize2;
        this.rightMargin = rightMargin2;
        this.escapeHtmlChars = escapeHtmlChars2;
    }

    private class JsonWriter {
        private int level = 0;
        private StringBuilder line = new StringBuilder();
        private final Appendable writer;

        JsonWriter(Appendable writer2) {
            this.writer = writer2;
        }

        /* access modifiers changed from: package-private */
        public void key(String key) throws IOException {
            breakLineIfThisToNextExceedsLimit(key.length() + 2);
            getLine().append('\"');
            getLine().append(key);
            getLine().append('\"');
        }

        /* access modifiers changed from: package-private */
        public void value(String value) throws IOException {
            breakLineIfThisToNextExceedsLimit(value.length() + 2);
            getLine().append(value);
        }

        /* access modifiers changed from: package-private */
        public void fieldSeparator() throws IOException {
            getLine().append(':');
            breakLineIfNeeded();
        }

        /* access modifiers changed from: package-private */
        public void elementSeparator() throws IOException {
            getLine().append(',');
            breakLineIfNeeded();
        }

        /* access modifiers changed from: package-private */
        public void beginObject() throws IOException {
            breakLineIfNeeded();
            getLine().append('{');
            this.level++;
        }

        /* access modifiers changed from: package-private */
        public void endObject() {
            getLine().append('}');
            this.level--;
        }

        /* access modifiers changed from: package-private */
        public void beginArray() throws IOException {
            breakLineIfNeeded();
            getLine().append('[');
            this.level++;
        }

        /* access modifiers changed from: package-private */
        public void endArray() {
            getLine().append(']');
            this.level--;
        }

        private void breakLineIfNeeded() throws IOException {
            breakLineIfThisToNextExceedsLimit(0);
        }

        private void breakLineIfThisToNextExceedsLimit(int nextLength) throws IOException {
            if (getLine().length() + nextLength > JsonPrintFormatter.this.printMargin - JsonPrintFormatter.this.rightMargin) {
                finishLine();
            }
        }

        /* access modifiers changed from: private */
        public void finishLine() throws IOException {
            if (this.line != null) {
                this.writer.append(this.line).append("\n");
            }
            this.line = null;
        }

        private StringBuilder getLine() {
            if (this.line == null) {
                createNewLine();
            }
            return this.line;
        }

        private void createNewLine() {
            this.line = new StringBuilder();
            for (int i = 0; i < this.level; i++) {
                for (int j = 0; j < JsonPrintFormatter.this.indentationSize; j++) {
                    this.line.append(' ');
                }
            }
        }
    }

    private class PrintFormattingVisitor implements JsonElementVisitor {
        private final Escaper escaper;
        private final boolean serializeNulls;
        private final JsonWriter writer;

        PrintFormattingVisitor(JsonWriter writer2, Escaper escaper2, boolean serializeNulls2) {
            this.writer = writer2;
            this.escaper = escaper2;
            this.serializeNulls = serializeNulls2;
        }

        private void addCommaCheckingFirst(boolean first) throws IOException {
            if (!first) {
                this.writer.elementSeparator();
            }
        }

        public void startArray(JsonArray array) throws IOException {
            this.writer.beginArray();
        }

        public void visitArrayMember(JsonArray parent, JsonPrimitive member, boolean isFirst) throws IOException {
            addCommaCheckingFirst(isFirst);
            this.writer.value(escapeJsonPrimitive(member));
        }

        public void visitArrayMember(JsonArray parent, JsonArray member, boolean first) throws IOException {
            addCommaCheckingFirst(first);
        }

        public void visitArrayMember(JsonArray parent, JsonObject member, boolean first) throws IOException {
            addCommaCheckingFirst(first);
        }

        public void visitNullArrayMember(JsonArray parent, boolean isFirst) throws IOException {
            addCommaCheckingFirst(isFirst);
        }

        public void endArray(JsonArray array) {
            this.writer.endArray();
        }

        public void startObject(JsonObject object) throws IOException {
            this.writer.beginObject();
        }

        public void visitObjectMember(JsonObject parent, String memberName, JsonPrimitive member, boolean isFirst) throws IOException {
            addCommaCheckingFirst(isFirst);
            this.writer.key(memberName);
            this.writer.fieldSeparator();
            this.writer.value(escapeJsonPrimitive(member));
        }

        public void visitObjectMember(JsonObject parent, String memberName, JsonArray member, boolean isFirst) throws IOException {
            addCommaCheckingFirst(isFirst);
            this.writer.key(memberName);
            this.writer.fieldSeparator();
        }

        public void visitObjectMember(JsonObject parent, String memberName, JsonObject member, boolean isFirst) throws IOException {
            addCommaCheckingFirst(isFirst);
            this.writer.key(memberName);
            this.writer.fieldSeparator();
        }

        public void visitNullObjectMember(JsonObject parent, String memberName, boolean isFirst) throws IOException {
            if (this.serializeNulls) {
                visitObjectMember(parent, memberName, (JsonObject) null, isFirst);
            }
        }

        public void endObject(JsonObject object) {
            this.writer.endObject();
        }

        public void visitPrimitive(JsonPrimitive primitive) throws IOException {
            this.writer.value(escapeJsonPrimitive(primitive));
        }

        public void visitNull() throws IOException {
            this.writer.value("null");
        }

        private String escapeJsonPrimitive(JsonPrimitive member) throws IOException {
            StringBuilder builder = new StringBuilder();
            member.toString(builder, this.escaper);
            return builder.toString();
        }
    }

    public void format(JsonElement root, Appendable writer, boolean serializeNulls) throws IOException {
        if (root != null) {
            JsonWriter jsonWriter = new JsonWriter(writer);
            new JsonTreeNavigator(new PrintFormattingVisitor(jsonWriter, new Escaper(this.escapeHtmlChars), serializeNulls), serializeNulls).navigate(root);
            jsonWriter.finishLine();
        }
    }
}
