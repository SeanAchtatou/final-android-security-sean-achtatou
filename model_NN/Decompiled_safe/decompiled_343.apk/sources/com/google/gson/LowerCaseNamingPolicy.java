package com.google.gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

class LowerCaseNamingPolicy extends RecursiveFieldNamingPolicy {
    LowerCaseNamingPolicy() {
    }

    /* access modifiers changed from: protected */
    public String translateName(String target, Type fieldType, Annotation[] annotations) {
        return target.toLowerCase();
    }
}
