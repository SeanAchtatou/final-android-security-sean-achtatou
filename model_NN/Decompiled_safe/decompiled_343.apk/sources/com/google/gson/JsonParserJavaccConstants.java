package com.google.gson;

interface JsonParserJavaccConstants {
    public static final int BOOLEAN = 10;
    public static final int CHAR = 21;
    public static final int CNTRL_ESC = 22;
    public static final int DEFAULT = 0;
    public static final int DIGITS = 6;
    public static final int DOUBLE_QUOTE_LITERAL = 17;
    public static final int ENDQUOTE = 20;
    public static final int EOF = 0;
    public static final int ESCAPE_CHAR = 15;
    public static final int ESC_STATE = 2;
    public static final int EXPONENT = 5;
    public static final int HEX = 24;
    public static final int HEX_CHAR = 13;
    public static final int HEX_ESC = 25;
    public static final int HEX_STATE = 3;
    public static final int IDENTIFIER_SANS_EXPONENT = 11;
    public static final int IDENTIFIER_STARTS_WITH_EXPONENT = 12;
    public static final int INFINITY = 9;
    public static final int NAN = 8;
    public static final int NULL = 7;
    public static final int QUOTE = 18;
    public static final int SINGLE_QUOTE_LITERAL = 16;
    public static final int STRING_STATE = 1;
    public static final int UNICODE_CHAR = 14;
    public static final String[] tokenImage = {"<EOF>", "\" \"", "\"\\t\"", "\"\\n\"", "\"\\r\"", "<EXPONENT>", "<DIGITS>", "\"null\"", "\"NaN\"", "\"Infinity\"", "<BOOLEAN>", "<IDENTIFIER_SANS_EXPONENT>", "<IDENTIFIER_STARTS_WITH_EXPONENT>", "<HEX_CHAR>", "<UNICODE_CHAR>", "<ESCAPE_CHAR>", "<SINGLE_QUOTE_LITERAL>", "<DOUBLE_QUOTE_LITERAL>", "\"\\\"\"", "\"\\\\\"", "<ENDQUOTE>", "<CHAR>", "<CNTRL_ESC>", "\"u\"", "<HEX>", "<HEX_ESC>", "\")]}\\'\\n\"", "\"{\"", "\"}\"", "\",\"", "\":\"", "\"[\"", "\"]\"", "\"-\"", "\".\""};
}
