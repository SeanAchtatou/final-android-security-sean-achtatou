package com.google.gson;

import java.io.IOException;

interface JsonFormatter {
    void format(JsonElement jsonElement, Appendable appendable, boolean z) throws IOException;
}
