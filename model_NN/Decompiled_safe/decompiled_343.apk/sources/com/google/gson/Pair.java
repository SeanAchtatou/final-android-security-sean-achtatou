package com.google.gson;

final class Pair<FIRST, SECOND> {
    private final FIRST first;
    private final SECOND second;

    Pair(FIRST first2, SECOND second2) {
        this.first = first2;
        this.second = second2;
    }

    public FIRST getFirst() {
        return this.first;
    }

    public SECOND getSecond() {
        return this.second;
    }
}
