package com.google.gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;

final class ParameterizedTypeImpl implements ParameterizedType {
    private final Type[] actualTypeArguments;
    private final Type owner;
    private final Type rawType;

    public ParameterizedTypeImpl(Type rawType2, Type[] actualTypeArguments2, Type owner2) {
        this.rawType = rawType2;
        this.actualTypeArguments = actualTypeArguments2;
        this.owner = owner2;
    }

    public Type getRawType() {
        return this.rawType;
    }

    public Type[] getActualTypeArguments() {
        return this.actualTypeArguments;
    }

    public Type getOwnerType() {
        return this.owner;
    }

    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 1
            r6 = 0
            boolean r4 = r9 instanceof java.lang.reflect.ParameterizedType
            if (r4 != 0) goto L_0x0008
            r4 = r6
        L_0x0007:
            return r4
        L_0x0008:
            r0 = r9
            java.lang.reflect.ParameterizedType r0 = (java.lang.reflect.ParameterizedType) r0
            r1 = r0
            if (r8 != r1) goto L_0x0010
            r4 = r7
            goto L_0x0007
        L_0x0010:
            java.lang.reflect.Type r2 = r1.getOwnerType()
            java.lang.reflect.Type r3 = r1.getRawType()
            java.lang.reflect.Type r4 = r8.owner
            if (r4 != 0) goto L_0x0032
            if (r2 != 0) goto L_0x003a
        L_0x001e:
            java.lang.reflect.Type r4 = r8.rawType
            if (r4 != 0) goto L_0x003c
            if (r3 != 0) goto L_0x003a
        L_0x0024:
            java.lang.reflect.Type[] r4 = r8.actualTypeArguments
            java.lang.reflect.Type[] r5 = r1.getActualTypeArguments()
            boolean r4 = java.util.Arrays.equals(r4, r5)
            if (r4 == 0) goto L_0x003a
            r4 = r7
            goto L_0x0007
        L_0x0032:
            java.lang.reflect.Type r4 = r8.owner
            boolean r4 = r4.equals(r2)
            if (r4 != 0) goto L_0x001e
        L_0x003a:
            r4 = r6
            goto L_0x0007
        L_0x003c:
            java.lang.reflect.Type r4 = r8.rawType
            boolean r4 = r4.equals(r3)
            if (r4 == 0) goto L_0x003a
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.ParameterizedTypeImpl.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        return (Arrays.hashCode(this.actualTypeArguments) ^ (this.owner == null ? 0 : this.owner.hashCode())) ^ (this.rawType == null ? 0 : this.rawType.hashCode());
    }
}
