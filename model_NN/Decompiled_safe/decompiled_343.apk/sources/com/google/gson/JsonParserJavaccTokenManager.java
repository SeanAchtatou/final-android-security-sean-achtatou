package com.google.gson;

import java.io.IOException;
import java.io.PrintStream;

final class JsonParserJavaccTokenManager implements JsonParserJavaccConstants {
    static final long[] jjbitVec0 = {-2, -1, -1, -1};
    static final long[] jjbitVec2 = {0, 0, -1, -1};
    public static final int[] jjnewLexState = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 2, 0, -1, 1, 3, -1, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
    static final int[] jjnextStates = {29, 30, 37, 38, 18, 19, 26, 27};
    public static final String[] jjstrLiteralImages = {"", null, null, null, null, null, null, "null", "NaN", "Infinity", null, null, null, null, null, null, null, null, "\"", null, null, null, null, null, null, null, ")]}'\n", "{", "}", ",", ":", "[", "]", "-", "."};
    static final long[] jjtoMore = {8912896};
    static final long[] jjtoSkip = {30};
    static final long[] jjtoToken = {34334007265L};
    public static final String[] lexStateNames = {"DEFAULT", "STRING_STATE", "ESC_STATE", "HEX_STATE"};
    protected char curChar;
    int curLexState;
    public PrintStream debugStream;
    int defaultLexState;
    protected SimpleCharStream input_stream;
    int jjmatchedKind;
    int jjmatchedPos;
    int jjnewStateCnt;
    int jjround;
    private final int[] jjrounds;
    private final int[] jjstateSet;

    public void setDebugStream(PrintStream ds) {
        this.debugStream = ds;
    }

    private int jjMoveStringLiteralDfa0_3() {
        return jjMoveNfa_3(0, 0);
    }

    private int jjMoveNfa_3(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 4;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr2 = this.jjstateSet;
                                int i4 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i4 + 1;
                                iArr2[i4] = 2;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr3 = this.jjstateSet;
                                int i5 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i5 + 1;
                                iArr3[i5] = 3;
                                continue;
                            } else {
                                continue;
                            }
                        case 3:
                            if ((287948901175001088L & l) != 0 && kind > 25) {
                                kind = 25;
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr4 = this.jjstateSet;
                                int i6 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i6 + 1;
                                iArr4[i6] = 1;
                                continue;
                            } else {
                                continue;
                            }
                        case 1:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr5 = this.jjstateSet;
                                int i7 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i7 + 1;
                                iArr5[i7] = 2;
                                continue;
                            } else {
                                continue;
                            }
                        case 2:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr6 = this.jjstateSet;
                                int i8 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i8 + 1;
                                iArr6[i8] = 3;
                                continue;
                            } else {
                                continue;
                            }
                        case 3:
                            if ((541165879422L & l2) != 0 && kind > 25) {
                                kind = 25;
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int hiByte = this.curChar >> 8;
                int i9 = hiByte >> 6;
                long j = 1 << (hiByte & 63);
                int i10 = (this.curChar & 255) >> 6;
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    int i11 = this.jjstateSet[i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 4 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_0(int pos, long active0) {
        switch (pos) {
            case 0:
                if ((896 & active0) == 0) {
                    return (262144 & active0) != 0 ? 43 : -1;
                }
                this.jjmatchedKind = 11;
                return 10;
            case 1:
                if ((896 & active0) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 11;
                this.jjmatchedPos = 1;
                return 10;
            case 2:
                if ((256 & active0) != 0) {
                    return 10;
                }
                if ((640 & active0) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 11;
                this.jjmatchedPos = 2;
                return 10;
            case 3:
                if ((128 & active0) != 0) {
                    return 10;
                }
                if ((active0 & 512) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 11;
                this.jjmatchedPos = 3;
                return 10;
            case 4:
                if ((active0 & 512) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 11;
                this.jjmatchedPos = 4;
                return 10;
            case 5:
                if ((active0 & 512) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 11;
                this.jjmatchedPos = 5;
                return 10;
            case 6:
                if ((active0 & 512) == 0) {
                    return -1;
                }
                this.jjmatchedKind = 11;
                this.jjmatchedPos = 6;
                return 10;
            default:
                return -1;
        }
    }

    private final int jjStartNfa_0(int pos, long active0) {
        return jjMoveNfa_0(jjStopStringLiteralDfa_0(pos, active0), pos + 1);
    }

    private int jjStopAtPos(int pos, int kind) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        return pos + 1;
    }

    private int jjMoveStringLiteralDfa0_0() {
        switch (this.curChar) {
            case '\"':
                return jjStartNfaWithStates_0(0, 18, 43);
            case ')':
                return jjMoveStringLiteralDfa1_0(67108864);
            case ',':
                return jjStopAtPos(0, 29);
            case '-':
                return jjStopAtPos(0, 33);
            case '.':
                return jjStopAtPos(0, 34);
            case ':':
                return jjStopAtPos(0, 30);
            case 'I':
                return jjMoveStringLiteralDfa1_0(512);
            case 'N':
                return jjMoveStringLiteralDfa1_0(256);
            case '[':
                return jjStopAtPos(0, 31);
            case ']':
                return jjStopAtPos(0, 32);
            case 'n':
                return jjMoveStringLiteralDfa1_0(128);
            case '{':
                return jjStopAtPos(0, 27);
            case '}':
                return jjStopAtPos(0, 28);
            default:
                return jjMoveNfa_0(4, 0);
        }
    }

    private int jjMoveStringLiteralDfa1_0(long active0) {
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case ']':
                    return jjMoveStringLiteralDfa2_0(active0, 67108864);
                case 'a':
                    return jjMoveStringLiteralDfa2_0(active0, 256);
                case 'n':
                    return jjMoveStringLiteralDfa2_0(active0, 512);
                case 'u':
                    return jjMoveStringLiteralDfa2_0(active0, 128);
                default:
                    return jjStartNfa_0(0, active0);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(0, active0);
            return 1;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private int jjMoveStringLiteralDfa2_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            return jjStartNfa_0(0, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case 'N':
                    if ((256 & active02) != 0) {
                        return jjStartNfaWithStates_0(2, 8, 10);
                    }
                    break;
                case 'f':
                    return jjMoveStringLiteralDfa3_0(active02, 512);
                case 'l':
                    return jjMoveStringLiteralDfa3_0(active02, 128);
                case '}':
                    return jjMoveStringLiteralDfa3_0(active02, 67108864);
            }
            return jjStartNfa_0(1, active02);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(1, active02);
            return 2;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private int jjMoveStringLiteralDfa3_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            return jjStartNfa_0(1, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case '\'':
                    return jjMoveStringLiteralDfa4_0(active02, 67108864);
                case 'i':
                    return jjMoveStringLiteralDfa4_0(active02, 512);
                case 'l':
                    if ((128 & active02) != 0) {
                        return jjStartNfaWithStates_0(3, 7, 10);
                    }
                    break;
            }
            return jjStartNfa_0(2, active02);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(2, active02);
            return 3;
        }
    }

    private int jjMoveStringLiteralDfa4_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            return jjStartNfa_0(2, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case 'n':
                    return jjMoveStringLiteralDfa5_0(active02, 512);
                case JsonParserJavaccConstants.BOOLEAN:
                    if ((67108864 & active02) != 0) {
                        return jjStopAtPos(4, 26);
                    }
                    break;
            }
            return jjStartNfa_0(3, active02);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(3, active02);
            return 4;
        }
    }

    private int jjMoveStringLiteralDfa5_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            return jjStartNfa_0(3, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case 'i':
                    return jjMoveStringLiteralDfa6_0(active02, 512);
                default:
                    return jjStartNfa_0(4, active02);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(4, active02);
            return 5;
        }
    }

    private int jjMoveStringLiteralDfa6_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            return jjStartNfa_0(4, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case 't':
                    return jjMoveStringLiteralDfa7_0(active02, 512);
                default:
                    return jjStartNfa_0(5, active02);
            }
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(5, active02);
            return 6;
        }
    }

    private int jjMoveStringLiteralDfa7_0(long old0, long active0) {
        long active02 = active0 & old0;
        if (active02 == 0) {
            return jjStartNfa_0(5, old0);
        }
        try {
            this.curChar = this.input_stream.readChar();
            switch (this.curChar) {
                case 'y':
                    if ((512 & active02) != 0) {
                        return jjStartNfaWithStates_0(7, 9, 10);
                    }
                    break;
            }
            return jjStartNfa_0(6, active02);
        } catch (IOException e) {
            jjStopStringLiteralDfa_0(6, active02);
            return 7;
        }
    }

    private int jjStartNfaWithStates_0(int pos, int kind, int state) {
        this.jjmatchedKind = kind;
        this.jjmatchedPos = pos;
        try {
            this.curChar = this.input_stream.readChar();
            return jjMoveNfa_0(state, pos + 1);
        } catch (IOException e) {
            return pos + 1;
        }
    }

    private int jjMoveNfa_0(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 43;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 6) {
                                    kind = 6;
                                }
                                jjCheckNAdd(0);
                                continue;
                            }
                        case 4:
                            if ((287948901175001088L & l) != 0) {
                                if (kind > 6) {
                                    kind = 6;
                                }
                                jjCheckNAdd(0);
                                continue;
                            } else if (this.curChar == '\"') {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else if (this.curChar == '\'') {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.BOOLEAN:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 11) {
                                    kind = 11;
                                }
                                int[] iArr = this.jjstateSet;
                                int i3 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i3 + 1;
                                iArr[i3] = 10;
                                continue;
                            }
                        case JsonParserJavaccConstants.IDENTIFIER_STARTS_WITH_EXPONENT:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr2 = this.jjstateSet;
                                int i4 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i4 + 1;
                                iArr2[i4] = 13;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.HEX_CHAR:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr3 = this.jjstateSet;
                                int i5 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i5 + 1;
                                iArr3[i5] = 14;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.UNICODE_CHAR:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr4 = this.jjstateSet;
                                int i6 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i6 + 1;
                                iArr4[i6] = 15;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.ESCAPE_CHAR:
                            if ((287948901175001088L & l) != 0 && kind > 14) {
                                kind = 14;
                                continue;
                            }
                        case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                            if (this.curChar == '\'') {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.QUOTE:
                            if ((-549755823105L & l) != 0) {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.ENDQUOTE:
                            if ((141304424038400L & l) != 0) {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.CNTRL_ESC:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr5 = this.jjstateSet;
                                int i7 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i7 + 1;
                                iArr5[i7] = 23;
                                continue;
                            } else {
                                continue;
                            }
                        case 23:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr6 = this.jjstateSet;
                                int i8 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i8 + 1;
                                iArr6[i8] = 24;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.HEX:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr7 = this.jjstateSet;
                                int i9 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i9 + 1;
                                iArr7[i9] = 25;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.HEX_ESC:
                            if ((287948901175001088L & l) != 0) {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case 27:
                            if (this.curChar == '\'' && kind > 16) {
                                kind = 16;
                                continue;
                            }
                        case 28:
                            if (this.curChar == '\"') {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 29:
                            if ((-17179878401L & l) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 31:
                            if ((141304424038400L & l) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 33:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr8 = this.jjstateSet;
                                int i10 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i10 + 1;
                                iArr8[i10] = 34;
                                continue;
                            } else {
                                continue;
                            }
                        case 34:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr9 = this.jjstateSet;
                                int i11 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i11 + 1;
                                iArr9[i11] = 35;
                                continue;
                            } else {
                                continue;
                            }
                        case 35:
                            if ((287948901175001088L & l) != 0) {
                                int[] iArr10 = this.jjstateSet;
                                int i12 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i12 + 1;
                                iArr10[i12] = 36;
                                continue;
                            } else {
                                continue;
                            }
                        case 36:
                            if ((287948901175001088L & l) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 38:
                            if (this.curChar == '\"' && kind > 17) {
                                kind = 17;
                                continue;
                            }
                        case 40:
                            if ((287992881640112128L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 5) {
                                    kind = 5;
                                }
                                jjCheckNAdd(41);
                                continue;
                            }
                        case 41:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 5) {
                                    kind = 5;
                                }
                                jjCheckNAdd(41);
                                continue;
                            }
                        case 42:
                            if ((287948901175001088L & l) == 0) {
                                continue;
                            } else {
                                if (kind > 12) {
                                    kind = 12;
                                }
                                int[] iArr11 = this.jjstateSet;
                                int i13 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i13 + 1;
                                iArr11[i13] = 42;
                                continue;
                            }
                        case 43:
                            if ((-17179878401L & l) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else if (this.curChar == '\"' && kind > 17) {
                                kind = 17;
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 1:
                            if (this.curChar == 'e' && kind > 10) {
                                kind = 10;
                                continue;
                            }
                        case 2:
                            if (this.curChar == 'u') {
                                jjCheckNAdd(1);
                                continue;
                            } else {
                                continue;
                            }
                        case 3:
                            if (this.curChar == 'r') {
                                int[] iArr12 = this.jjstateSet;
                                int i14 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i14 + 1;
                                iArr12[i14] = 2;
                                continue;
                            } else {
                                continue;
                            }
                        case 4:
                            if ((576460608556236766L & l2) != 0) {
                                if (kind > 11) {
                                    kind = 11;
                                }
                                jjCheckNAdd(10);
                            } else if ((137438953504L & l2) != 0) {
                                if (kind > 12) {
                                    kind = 12;
                                }
                                jjCheckNAddTwoStates(40, 42);
                            } else if (this.curChar == '\\') {
                                int[] iArr13 = this.jjstateSet;
                                int i15 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i15 + 1;
                                iArr13[i15] = 11;
                            }
                            if (this.curChar == 'f') {
                                int[] iArr14 = this.jjstateSet;
                                int i16 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i16 + 1;
                                iArr14[i16] = 7;
                                continue;
                            } else if (this.curChar == 't') {
                                int[] iArr15 = this.jjstateSet;
                                int i17 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i17 + 1;
                                iArr15[i17] = 3;
                                continue;
                            } else {
                                continue;
                            }
                        case 5:
                            if (this.curChar == 's') {
                                jjCheckNAdd(1);
                                continue;
                            } else {
                                continue;
                            }
                        case 6:
                            if (this.curChar == 'l') {
                                int[] iArr16 = this.jjstateSet;
                                int i18 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i18 + 1;
                                iArr16[i18] = 5;
                                continue;
                            } else {
                                continue;
                            }
                        case 7:
                            if (this.curChar == 'a') {
                                int[] iArr17 = this.jjstateSet;
                                int i19 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i19 + 1;
                                iArr17[i19] = 6;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.NAN:
                            if (this.curChar == 'f') {
                                int[] iArr18 = this.jjstateSet;
                                int i20 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i20 + 1;
                                iArr18[i20] = 7;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.INFINITY:
                            if ((576460608556236766L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 11) {
                                    kind = 11;
                                }
                                jjCheckNAdd(10);
                                continue;
                            }
                        case JsonParserJavaccConstants.BOOLEAN:
                            if ((576460745995190270L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 11) {
                                    kind = 11;
                                }
                                jjCheckNAdd(10);
                                continue;
                            }
                        case JsonParserJavaccConstants.IDENTIFIER_SANS_EXPONENT:
                            if (this.curChar == 'u') {
                                int[] iArr19 = this.jjstateSet;
                                int i21 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i21 + 1;
                                iArr19[i21] = 12;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.IDENTIFIER_STARTS_WITH_EXPONENT:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr20 = this.jjstateSet;
                                int i22 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i22 + 1;
                                iArr20[i22] = 13;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.HEX_CHAR:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr21 = this.jjstateSet;
                                int i23 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i23 + 1;
                                iArr21[i23] = 14;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.UNICODE_CHAR:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr22 = this.jjstateSet;
                                int i24 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i24 + 1;
                                iArr22[i24] = 15;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.ESCAPE_CHAR:
                            if ((541165879422L & l2) != 0 && kind > 14) {
                                kind = 14;
                                continue;
                            }
                        case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
                            if (this.curChar == '\\') {
                                int[] iArr23 = this.jjstateSet;
                                int i25 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i25 + 1;
                                iArr23[i25] = 11;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.QUOTE:
                            if ((-268435457 & l2) != 0) {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case 19:
                            if (this.curChar == '\\') {
                                int[] iArr24 = this.jjstateSet;
                                int i26 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i26 + 1;
                                iArr24[i26] = 20;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.ENDQUOTE:
                            if ((5700160604602368L & l2) != 0) {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.CHAR:
                            if (this.curChar == 'u') {
                                int[] iArr25 = this.jjstateSet;
                                int i27 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i27 + 1;
                                iArr25[i27] = 22;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.CNTRL_ESC:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr26 = this.jjstateSet;
                                int i28 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i28 + 1;
                                iArr26[i28] = 23;
                                continue;
                            } else {
                                continue;
                            }
                        case 23:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr27 = this.jjstateSet;
                                int i29 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i29 + 1;
                                iArr27[i29] = 24;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.HEX:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr28 = this.jjstateSet;
                                int i30 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i30 + 1;
                                iArr28[i30] = 25;
                                continue;
                            } else {
                                continue;
                            }
                        case JsonParserJavaccConstants.HEX_ESC:
                            if ((541165879422L & l2) != 0) {
                                jjCheckNAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case 26:
                            if (this.curChar == '\\') {
                                int[] iArr29 = this.jjstateSet;
                                int i31 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i31 + 1;
                                iArr29[i31] = 21;
                                continue;
                            } else {
                                continue;
                            }
                        case 29:
                            if ((-268435457 & l2) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 30:
                            if (this.curChar == '\\') {
                                int[] iArr30 = this.jjstateSet;
                                int i32 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i32 + 1;
                                iArr30[i32] = 31;
                                continue;
                            } else {
                                continue;
                            }
                        case 31:
                            if ((5700160604602368L & l2) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 32:
                            if (this.curChar == 'u') {
                                int[] iArr31 = this.jjstateSet;
                                int i33 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i33 + 1;
                                iArr31[i33] = 33;
                                continue;
                            } else {
                                continue;
                            }
                        case 33:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr32 = this.jjstateSet;
                                int i34 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i34 + 1;
                                iArr32[i34] = 34;
                                continue;
                            } else {
                                continue;
                            }
                        case 34:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr33 = this.jjstateSet;
                                int i35 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i35 + 1;
                                iArr33[i35] = 35;
                                continue;
                            } else {
                                continue;
                            }
                        case 35:
                            if ((541165879422L & l2) != 0) {
                                int[] iArr34 = this.jjstateSet;
                                int i36 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i36 + 1;
                                iArr34[i36] = 36;
                                continue;
                            } else {
                                continue;
                            }
                        case 36:
                            if ((541165879422L & l2) != 0) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                        case 37:
                            if (this.curChar == '\\') {
                                int[] iArr35 = this.jjstateSet;
                                int i37 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i37 + 1;
                                iArr35[i37] = 32;
                                continue;
                            } else {
                                continue;
                            }
                        case 39:
                            if ((137438953504L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 12) {
                                    kind = 12;
                                }
                                jjCheckNAddTwoStates(40, 42);
                                continue;
                            }
                        case 42:
                            if ((576460745995190270L & l2) == 0) {
                                continue;
                            } else {
                                if (kind > 12) {
                                    kind = 12;
                                }
                                jjCheckNAdd(42);
                                continue;
                            }
                        case 43:
                            if ((-268435457 & l2) != 0) {
                                jjCheckNAddStates(0, 3);
                            } else if (this.curChar == '\\') {
                                int[] iArr36 = this.jjstateSet;
                                int i38 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i38 + 1;
                                iArr36[i38] = 32;
                            }
                            if (this.curChar == '\\') {
                                int[] iArr37 = this.jjstateSet;
                                int i39 = this.jjnewStateCnt;
                                this.jjnewStateCnt = i39 + 1;
                                iArr37[i39] = 31;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int hiByte = this.curChar >> 8;
                int i1 = hiByte >> 6;
                long l1 = 1 << (hiByte & 63);
                int i210 = (this.curChar & 255) >> 6;
                long l22 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case JsonParserJavaccConstants.QUOTE:
                            if (jjCanMove_0(hiByte, i1, i210, l1, l22)) {
                                jjAddStates(4, 7);
                                continue;
                            } else {
                                continue;
                            }
                        case 29:
                        case 43:
                            if (jjCanMove_0(hiByte, i1, i210, l1, l22)) {
                                jjCheckNAddStates(0, 3);
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 43 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_2(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_2(int pos, long active0) {
        return jjMoveNfa_2(jjStopStringLiteralDfa_2(pos, active0), pos + 1);
    }

    private int jjMoveStringLiteralDfa0_2() {
        switch (this.curChar) {
            case 'u':
                return jjStopAtPos(0, 23);
            default:
                return jjMoveNfa_2(0, 0);
        }
    }

    private int jjMoveNfa_2(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 1;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((140754668224512L & l) != 0) {
                                kind = 22;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((5700160604602368L & l2) != 0) {
                                kind = 22;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int hiByte = this.curChar >> 8;
                int i3 = hiByte >> 6;
                long j = 1 << (hiByte & 63);
                int i4 = (this.curChar & 255) >> 6;
                long j2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    int i5 = this.jjstateSet[i];
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 1 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private final int jjStopStringLiteralDfa_1(int pos, long active0) {
        return -1;
    }

    private final int jjStartNfa_1(int pos, long active0) {
        return jjMoveNfa_1(jjStopStringLiteralDfa_1(pos, active0), pos + 1);
    }

    private int jjMoveStringLiteralDfa0_1() {
        switch (this.curChar) {
            case '\\':
                return jjStopAtPos(0, 19);
            default:
                return jjMoveNfa_1(0, 0);
        }
    }

    private int jjMoveNfa_1(int startState, int curPos) {
        int startsAt = 0;
        this.jjnewStateCnt = 2;
        int i = 1;
        this.jjstateSet[0] = startState;
        int kind = Integer.MAX_VALUE;
        while (true) {
            int i2 = this.jjround + 1;
            this.jjround = i2;
            if (i2 == Integer.MAX_VALUE) {
                ReInitRounds();
            }
            if (this.curChar < '@') {
                long l = 1 << this.curChar;
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((-17179869185L & l) != 0) {
                                if (kind > 21) {
                                    kind = 21;
                                    continue;
                                } else {
                                    continue;
                                }
                            } else if (this.curChar == '\"' && kind > 20) {
                                kind = 20;
                                continue;
                            }
                        case 1:
                            if ((-17179869185L & l) != 0) {
                                kind = 21;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else if (this.curChar < 128) {
                long l2 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if ((-268435457 & l2) != 0) {
                                kind = 21;
                                continue;
                            } else {
                                continue;
                            }
                    }
                } while (i != startsAt);
            } else {
                int hiByte = this.curChar >> 8;
                int i1 = hiByte >> 6;
                long l1 = 1 << (hiByte & 63);
                int i22 = (this.curChar & 255) >> 6;
                long l22 = 1 << (this.curChar & '?');
                do {
                    i--;
                    switch (this.jjstateSet[i]) {
                        case 0:
                            if (jjCanMove_0(hiByte, i1, i22, l1, l22) && kind > 21) {
                                kind = 21;
                                continue;
                            }
                    }
                } while (i != startsAt);
            }
            if (kind != Integer.MAX_VALUE) {
                this.jjmatchedKind = kind;
                this.jjmatchedPos = curPos;
                kind = Integer.MAX_VALUE;
            }
            curPos++;
            i = this.jjnewStateCnt;
            this.jjnewStateCnt = startsAt;
            startsAt = 2 - startsAt;
            if (i != startsAt) {
                try {
                    this.curChar = this.input_stream.readChar();
                } catch (IOException e) {
                }
            }
            return curPos;
        }
    }

    private static final boolean jjCanMove_0(int hiByte, int i1, int i2, long l1, long l2) {
        switch (hiByte) {
            case 0:
                return (jjbitVec2[i2] & l2) != 0;
            default:
                if ((jjbitVec0[i1] & l1) != 0) {
                    return true;
                }
                return false;
        }
    }

    public JsonParserJavaccTokenManager(SimpleCharStream stream) {
        this.debugStream = System.out;
        this.jjrounds = new int[43];
        this.jjstateSet = new int[86];
        this.curLexState = 0;
        this.defaultLexState = 0;
        this.input_stream = stream;
    }

    public JsonParserJavaccTokenManager(SimpleCharStream stream, int lexState) {
        this(stream);
        SwitchTo(lexState);
    }

    public void ReInit(SimpleCharStream stream) {
        this.jjnewStateCnt = 0;
        this.jjmatchedPos = 0;
        this.curLexState = this.defaultLexState;
        this.input_stream = stream;
        ReInitRounds();
    }

    private void ReInitRounds() {
        this.jjround = -2147483647;
        int i = 43;
        while (true) {
            int i2 = i;
            i = i2 - 1;
            if (i2 > 0) {
                this.jjrounds[i] = Integer.MIN_VALUE;
            } else {
                return;
            }
        }
    }

    public void ReInit(SimpleCharStream stream, int lexState) {
        ReInit(stream);
        SwitchTo(lexState);
    }

    public void SwitchTo(int lexState) {
        if (lexState >= 4 || lexState < 0) {
            throw new TokenMgrError("Error: Ignoring invalid lexical state : " + lexState + ". State unchanged.", 2);
        }
        this.curLexState = lexState;
    }

    /* access modifiers changed from: protected */
    public Token jjFillToken() {
        String curTokenImage;
        String im = jjstrLiteralImages[this.jjmatchedKind];
        if (im == null) {
            curTokenImage = this.input_stream.GetImage();
        } else {
            curTokenImage = im;
        }
        int beginLine = this.input_stream.getBeginLine();
        int beginColumn = this.input_stream.getBeginColumn();
        int endLine = this.input_stream.getEndLine();
        int endColumn = this.input_stream.getEndColumn();
        Token t = Token.newToken(this.jjmatchedKind, curTokenImage);
        t.beginLine = beginLine;
        t.endLine = endLine;
        t.beginColumn = beginColumn;
        t.endColumn = endColumn;
        return t;
    }

    public Token getNextToken() {
        int curPos = 0;
        while (true) {
            try {
                this.curChar = this.input_stream.BeginToken();
                while (true) {
                    switch (this.curLexState) {
                        case 0:
                            try {
                                this.input_stream.backup(0);
                                while (this.curChar <= ' ' && (4294977024L & (1 << this.curChar)) != 0) {
                                    this.curChar = this.input_stream.BeginToken();
                                }
                                this.jjmatchedKind = Integer.MAX_VALUE;
                                this.jjmatchedPos = 0;
                                curPos = jjMoveStringLiteralDfa0_0();
                                break;
                            } catch (IOException e) {
                                break;
                            }
                            break;
                        case 1:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_1();
                            break;
                        case 2:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_2();
                            break;
                        case 3:
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            this.jjmatchedPos = 0;
                            curPos = jjMoveStringLiteralDfa0_3();
                            break;
                    }
                    if (this.jjmatchedKind != Integer.MAX_VALUE) {
                        if (this.jjmatchedPos + 1 < curPos) {
                            this.input_stream.backup((curPos - this.jjmatchedPos) - 1);
                        }
                        if ((jjtoToken[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) != 0) {
                            Token matchedToken = jjFillToken();
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            return matchedToken;
                        } else if ((jjtoSkip[this.jjmatchedKind >> 6] & (1 << (this.jjmatchedKind & 63))) == 0) {
                            if (jjnewLexState[this.jjmatchedKind] != -1) {
                                this.curLexState = jjnewLexState[this.jjmatchedKind];
                            }
                            curPos = 0;
                            this.jjmatchedKind = Integer.MAX_VALUE;
                            try {
                                this.curChar = this.input_stream.readChar();
                            } catch (IOException e2) {
                            }
                        } else if (jjnewLexState[this.jjmatchedKind] != -1) {
                            this.curLexState = jjnewLexState[this.jjmatchedKind];
                        }
                    }
                }
            } catch (IOException e3) {
                this.jjmatchedKind = 0;
                return jjFillToken();
            }
        }
        int error_line = this.input_stream.getEndLine();
        int error_column = this.input_stream.getEndColumn();
        String error_after = null;
        boolean EOFSeen = false;
        try {
            this.input_stream.readChar();
            this.input_stream.backup(1);
        } catch (IOException e4) {
            EOFSeen = true;
            error_after = curPos <= 1 ? "" : this.input_stream.GetImage();
            if (this.curChar == 10 || this.curChar == 13) {
                error_line++;
                error_column = 0;
            } else {
                error_column++;
            }
        }
        if (!EOFSeen) {
            this.input_stream.backup(1);
            if (curPos <= 1) {
                error_after = "";
            } else {
                error_after = this.input_stream.GetImage();
            }
        }
        throw new TokenMgrError(EOFSeen, this.curLexState, error_line, error_column, error_after, this.curChar, 0);
    }

    private void jjCheckNAdd(int state) {
        if (this.jjrounds[state] != this.jjround) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = state;
            this.jjrounds[state] = this.jjround;
        }
    }

    private void jjAddStates(int start, int end) {
        while (true) {
            int[] iArr = this.jjstateSet;
            int i = this.jjnewStateCnt;
            this.jjnewStateCnt = i + 1;
            iArr[i] = jjnextStates[start];
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }

    private void jjCheckNAddTwoStates(int state1, int state2) {
        jjCheckNAdd(state1);
        jjCheckNAdd(state2);
    }

    private void jjCheckNAddStates(int start, int end) {
        while (true) {
            jjCheckNAdd(jjnextStates[start]);
            int start2 = start + 1;
            if (start != end) {
                start = start2;
            } else {
                return;
            }
        }
    }
}
