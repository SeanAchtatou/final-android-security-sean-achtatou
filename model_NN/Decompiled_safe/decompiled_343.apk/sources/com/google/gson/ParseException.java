package com.google.gson;

final class ParseException extends Exception {
    private static final long serialVersionUID = 1;
    public Token currentToken;
    protected String eol = System.getProperty("line.separator", "\n");
    public int[][] expectedTokenSequences;
    public String[] tokenImage;

    public ParseException(Token currentTokenVal, int[][] expectedTokenSequencesVal, String[] tokenImageVal) {
        super(initialise(currentTokenVal, expectedTokenSequencesVal, tokenImageVal));
        this.currentToken = currentTokenVal;
        this.expectedTokenSequences = expectedTokenSequencesVal;
        this.tokenImage = tokenImageVal;
    }

    public ParseException() {
    }

    public ParseException(String message) {
        super(message);
    }

    private static String initialise(Token currentToken2, int[][] expectedTokenSequences2, String[] tokenImage2) {
        String retval;
        String eol2 = System.getProperty("line.separator", "\n");
        StringBuffer expected = new StringBuffer();
        int maxSize = 0;
        for (int i = 0; i < expectedTokenSequences2.length; i++) {
            if (maxSize < expectedTokenSequences2[i].length) {
                maxSize = expectedTokenSequences2[i].length;
            }
            for (int i2 : expectedTokenSequences2[i]) {
                expected.append(tokenImage2[i2]).append(' ');
            }
            if (expectedTokenSequences2[i][expectedTokenSequences2[i].length - 1] != 0) {
                expected.append("...");
            }
            expected.append(eol2).append("    ");
        }
        String retval2 = "Encountered \"";
        Token tok = currentToken2.next;
        int i3 = 0;
        while (true) {
            if (i3 >= maxSize) {
                break;
            }
            if (i3 != 0) {
                retval2 = retval2 + " ";
            }
            if (tok.kind == 0) {
                retval2 = retval2 + tokenImage2[0];
                break;
            }
            retval2 = (((retval2 + " " + tokenImage2[tok.kind]) + " \"") + add_escapes(tok.image)) + " \"";
            tok = tok.next;
            i3++;
        }
        String retval3 = (retval2 + "\" at line " + currentToken2.next.beginLine + ", column " + currentToken2.next.beginColumn) + "." + eol2;
        if (expectedTokenSequences2.length == 1) {
            retval = retval3 + "Was expecting:" + eol2 + "    ";
        } else {
            retval = retval3 + "Was expecting one of:" + eol2 + "    ";
        }
        return retval + expected.toString();
    }

    static String add_escapes(String str) {
        StringBuffer retval = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case 0:
                    break;
                case JsonParserJavaccConstants.NAN:
                    retval.append("\\b");
                    break;
                case JsonParserJavaccConstants.INFINITY:
                    retval.append("\\t");
                    break;
                case JsonParserJavaccConstants.BOOLEAN:
                    retval.append("\\n");
                    break;
                case JsonParserJavaccConstants.IDENTIFIER_STARTS_WITH_EXPONENT:
                    retval.append("\\f");
                    break;
                case JsonParserJavaccConstants.HEX_CHAR:
                    retval.append("\\r");
                    break;
                case '\"':
                    retval.append("\\\"");
                    break;
                case '\'':
                    retval.append("\\'");
                    break;
                case '\\':
                    retval.append("\\\\");
                    break;
                default:
                    char ch = str.charAt(i);
                    if (ch >= ' ' && ch <= '~') {
                        retval.append(ch);
                        break;
                    } else {
                        String s = "0000" + Integer.toString(ch, 16);
                        retval.append("\\u" + s.substring(s.length() - 4, s.length()));
                        break;
                    }
                    break;
            }
        }
        return retval.toString();
    }
}
