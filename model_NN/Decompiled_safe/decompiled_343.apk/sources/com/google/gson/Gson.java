package com.google.gson;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public final class Gson {
    static final AnonymousAndLocalClassExclusionStrategy DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY = new AnonymousAndLocalClassExclusionStrategy();
    private static final ExclusionStrategy DEFAULT_EXCLUSION_STRATEGY = createExclusionStrategy(-1.0d);
    static final JsonFormatter DEFAULT_JSON_FORMATTER = new JsonCompactFormatter();
    static final boolean DEFAULT_JSON_NON_EXECUTABLE = false;
    static final ModifierBasedExclusionStrategy DEFAULT_MODIFIER_BASED_EXCLUSION_STRATEGY = new ModifierBasedExclusionStrategy(128, 8);
    static final FieldNamingStrategy DEFAULT_NAMING_POLICY = new SerializedNameAnnotationInterceptingNamingPolicy(new JavaFieldNamingPolicy());
    static final SyntheticFieldExclusionStrategy DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY = new SyntheticFieldExclusionStrategy(true);
    private static final String JSON_NON_EXECUTABLE_PREFIX = ")]}'\n";
    private static final String NULL_STRING = "null";
    private final ExclusionStrategy deserializationStrategy;
    private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers;
    private final FieldNamingStrategy fieldNamingPolicy;
    private final JsonFormatter formatter;
    private final boolean generateNonExecutableJson;
    private final MappedObjectConstructor objectConstructor;
    private final ExclusionStrategy serializationStrategy;
    private final boolean serializeNulls;
    private final ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers;

    public Gson() {
        this(DEFAULT_EXCLUSION_STRATEGY, DEFAULT_EXCLUSION_STRATEGY, DEFAULT_NAMING_POLICY, new MappedObjectConstructor(DefaultTypeAdapters.getDefaultInstanceCreators()), DEFAULT_JSON_FORMATTER, DEFAULT_JSON_NON_EXECUTABLE, DefaultTypeAdapters.getDefaultSerializers(), DefaultTypeAdapters.getDefaultDeserializers(), DEFAULT_JSON_NON_EXECUTABLE);
    }

    Gson(ExclusionStrategy serializationStrategy2, ExclusionStrategy deserializationStrategy2, FieldNamingStrategy fieldNamingPolicy2, MappedObjectConstructor objectConstructor2, JsonFormatter formatter2, boolean serializeNulls2, ParameterizedTypeHandlerMap<JsonSerializer<?>> serializers2, ParameterizedTypeHandlerMap<JsonDeserializer<?>> deserializers2, boolean generateNonExecutableGson) {
        this.serializationStrategy = serializationStrategy2;
        this.deserializationStrategy = deserializationStrategy2;
        this.fieldNamingPolicy = fieldNamingPolicy2;
        this.objectConstructor = objectConstructor2;
        this.formatter = formatter2;
        this.serializeNulls = serializeNulls2;
        this.serializers = serializers2;
        this.deserializers = deserializers2;
        this.generateNonExecutableJson = generateNonExecutableGson;
    }

    private ObjectNavigatorFactory createDefaultObjectNavigatorFactory(ExclusionStrategy strategy) {
        return new ObjectNavigatorFactory(strategy, this.fieldNamingPolicy);
    }

    private static ExclusionStrategy createExclusionStrategy(double version) {
        List<ExclusionStrategy> strategies = new LinkedList<>();
        strategies.add(DEFAULT_ANON_LOCAL_CLASS_EXCLUSION_STRATEGY);
        strategies.add(DEFAULT_SYNTHETIC_FIELD_EXCLUSION_STRATEGY);
        strategies.add(DEFAULT_MODIFIER_BASED_EXCLUSION_STRATEGY);
        if (version != -1.0d) {
            strategies.add(new VersionExclusionStrategy(version));
        }
        return new DisjunctionExclusionStrategy(strategies);
    }

    public JsonElement toJsonTree(Object src) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        return toJsonTree(src, src.getClass());
    }

    public JsonElement toJsonTree(Object src, Type typeOfSrc) {
        if (src == null) {
            return JsonNull.createJsonNull();
        }
        return new JsonSerializationContextDefault(createDefaultObjectNavigatorFactory(this.serializationStrategy), this.serializeNulls, this.serializers).serialize(src, typeOfSrc, true);
    }

    public String toJson(Object src) {
        if (src == null) {
            return this.serializeNulls ? NULL_STRING : "";
        }
        return toJson(src, src.getClass());
    }

    public String toJson(Object src, Type typeOfSrc) {
        StringWriter writer = new StringWriter();
        toJson(src, typeOfSrc, writer);
        return writer.toString();
    }

    public void toJson(Object src, Appendable writer) {
        if (src != null) {
            try {
                toJson(src, src.getClass(), writer);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else if (this.serializeNulls) {
            writeOutNullString(writer);
        }
    }

    public void toJson(Object src, Type typeOfSrc, Appendable writer) {
        toJson(toJsonTree(src, typeOfSrc), writer);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.toJson(com.google.gson.JsonElement, java.lang.Appendable):void
     arg types: [com.google.gson.JsonElement, java.io.StringWriter]
     candidates:
      com.google.gson.Gson.toJson(java.lang.Object, java.lang.reflect.Type):java.lang.String
      com.google.gson.Gson.toJson(java.lang.Object, java.lang.Appendable):void
      com.google.gson.Gson.toJson(com.google.gson.JsonElement, java.lang.Appendable):void */
    public String toJson(JsonElement jsonElement) {
        StringWriter writer = new StringWriter();
        toJson(jsonElement, (Appendable) writer);
        return writer.toString();
    }

    public void toJson(JsonElement jsonElement, Appendable writer) {
        try {
            if (this.generateNonExecutableJson) {
                writer.append(JSON_NON_EXECUTABLE_PREFIX);
            }
            if (jsonElement == null && this.serializeNulls) {
                writeOutNullString(writer);
            }
            this.formatter.format(jsonElement, writer, this.serializeNulls);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
     arg types: [java.lang.String, java.lang.Class<T>]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T */
    public <T> T fromJson(String json, Class<T> classOfT) throws JsonParseException {
        return fromJson(json, (Type) classOfT);
    }

    public <T> T fromJson(String json, Type typeOfT) throws JsonParseException {
        return fromJson(new StringReader(json), typeOfT);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
     arg types: [java.io.Reader, java.lang.Class<T>]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T */
    public <T> T fromJson(Reader json, Class<T> classOfT) throws JsonParseException {
        return classOfT.cast(fromJson(json, (Type) classOfT));
    }

    public <T> T fromJson(Reader json, Type typeOfT) throws JsonParseException {
        return fromJson(new JsonParser().parse(json), typeOfT);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
     arg types: [com.google.gson.JsonElement, java.lang.Class<T>]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T */
    public <T> T fromJson(JsonElement json, Class<T> classOfT) throws JsonParseException {
        return classOfT.cast(fromJson(json, (Type) classOfT));
    }

    public <T> T fromJson(JsonElement json, Type typeOfT) throws JsonParseException {
        if (json == null) {
            return null;
        }
        return new JsonDeserializationContextDefault(createDefaultObjectNavigatorFactory(this.deserializationStrategy), this.deserializers, this.objectConstructor).deserialize(json, typeOfT);
    }

    private void writeOutNullString(Appendable writer) throws IOException {
        writer.append(NULL_STRING);
    }

    public String toString() {
        return "{" + "serializeNulls:" + this.serializeNulls + ",serializers:" + this.serializers + ",deserializers:" + this.deserializers + ",instanceCreators:" + this.objectConstructor + "}";
    }
}
