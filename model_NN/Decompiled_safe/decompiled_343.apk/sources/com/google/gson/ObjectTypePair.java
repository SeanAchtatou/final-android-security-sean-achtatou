package com.google.gson;

import java.lang.reflect.Type;

final class ObjectTypePair {
    private Object obj;
    private final boolean preserveType;
    private final Type type;

    ObjectTypePair(Object obj2, Type type2, boolean preserveType2) {
        this.obj = obj2;
        this.type = type2;
        this.preserveType = preserveType2;
    }

    /* access modifiers changed from: package-private */
    public Object getObject() {
        return this.obj;
    }

    /* access modifiers changed from: package-private */
    public void setObject(Object obj2) {
        this.obj = obj2;
    }

    /* access modifiers changed from: package-private */
    public Type getType() {
        return this.type;
    }

    /* access modifiers changed from: package-private */
    public <HANDLER> Pair<HANDLER, ObjectTypePair> getMatchingHandler(ParameterizedTypeHandlerMap<HANDLER> handlers) {
        if (!this.preserveType && this.obj != null) {
            ObjectTypePair moreSpecificType = toMoreSpecificType();
            HANDLER handler = handlers.getHandlerFor(moreSpecificType.type);
            if (handler != null) {
                return new Pair<>(handler, moreSpecificType);
            }
        }
        HANDLER handler2 = handlers.getHandlerFor(this.type);
        if (handler2 == null) {
            return null;
        }
        return new Pair<>(handler2, this);
    }

    /* access modifiers changed from: package-private */
    public ObjectTypePair toMoreSpecificType() {
        if (this.preserveType || this.obj == null) {
            return this;
        }
        Type actualType = getActualTypeIfMoreSpecific(this.type, this.obj.getClass());
        return actualType == this.type ? this : new ObjectTypePair(this.obj, actualType, this.preserveType);
    }

    static Type getActualTypeIfMoreSpecific(Type type2, Class<?> actualClass) {
        if (!(type2 instanceof Class)) {
            return type2;
        }
        if (((Class) type2).isAssignableFrom(actualClass)) {
            type2 = actualClass;
        }
        if (type2 == Object.class) {
            return actualClass;
        }
        return type2;
    }

    public int hashCode() {
        if (this.obj == null) {
            return 31;
        }
        return this.obj.hashCode();
    }

    public boolean equals(Object obj2) {
        if (this == obj2) {
            return true;
        }
        if (obj2 == null) {
            return false;
        }
        if (getClass() != obj2.getClass()) {
            return false;
        }
        ObjectTypePair other = (ObjectTypePair) obj2;
        if (this.obj == null) {
            if (other.obj != null) {
                return false;
            }
        } else if (this.obj != other.obj) {
            return false;
        }
        if (this.type == null) {
            if (other.type != null) {
                return false;
            }
        } else if (!this.type.equals(other.type)) {
            return false;
        }
        return this.preserveType == other.preserveType;
    }

    public boolean isPreserveType() {
        return this.preserveType;
    }
}
