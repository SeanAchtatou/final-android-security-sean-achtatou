package com.google.gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;

abstract class RecursiveFieldNamingPolicy implements FieldNamingStrategy {
    /* access modifiers changed from: protected */
    public abstract String translateName(String str, Type type, Annotation[] annotationArr);

    RecursiveFieldNamingPolicy() {
    }

    public final String translateName(Field f) {
        Preconditions.checkNotNull(f);
        return translateName(f.getName(), f.getGenericType(), f.getAnnotations());
    }
}
