package com.google.gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

class UpperCaseNamingPolicy extends RecursiveFieldNamingPolicy {
    UpperCaseNamingPolicy() {
    }

    /* access modifiers changed from: protected */
    public String translateName(String target, Type fieldType, Annotation[] annotations) {
        return target.toUpperCase();
    }
}
