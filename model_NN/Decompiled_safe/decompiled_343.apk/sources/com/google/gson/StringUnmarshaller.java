package com.google.gson;

final class StringUnmarshaller {
    StringUnmarshaller() {
    }

    static String unmarshall(String str) {
        String str2 = str.substring(1, str.length() - 1);
        int len = str2.length();
        StringBuilder sb = new StringBuilder(len);
        int i = 0;
        while (i < len) {
            char c = str2.charAt(i);
            i++;
            if (c == '\\') {
                char c1 = str2.charAt(i);
                i++;
                if (c1 == 'u') {
                    sb.appendCodePoint(getCodePoint(str2, i));
                    i += 4;
                } else {
                    sb.append(getEscapedChar(str2, c1));
                }
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static int getCodePoint(String str, int i) {
        return Integer.parseInt(str.substring(i, i + 4), 16);
    }

    private static char getEscapedChar(String str, char c) {
        switch (c) {
            case '\"':
                return '\"';
            case '\'':
                return '\'';
            case '/':
                return '/';
            case '\\':
                return '\\';
            case 'b':
                return 8;
            case 'f':
                return 12;
            case 'n':
                return 10;
            case 'r':
                return 13;
            case 't':
                return 9;
            default:
                throw new IllegalStateException("Unexpected character: " + c + " in " + str);
        }
    }
}
