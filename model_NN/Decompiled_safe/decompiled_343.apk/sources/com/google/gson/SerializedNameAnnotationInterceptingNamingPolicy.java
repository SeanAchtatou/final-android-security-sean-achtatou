package com.google.gson;

import com.google.gson.annotations.SerializedName;
import java.lang.reflect.Field;

class SerializedNameAnnotationInterceptingNamingPolicy implements FieldNamingStrategy {
    private static final JsonFieldNameValidator fieldNameValidator = new JsonFieldNameValidator();
    private final FieldNamingStrategy delegate;

    public SerializedNameAnnotationInterceptingNamingPolicy(FieldNamingStrategy delegate2) {
        this.delegate = delegate2;
    }

    public String translateName(Field f) {
        Preconditions.checkNotNull(f);
        SerializedName serializedName = (SerializedName) f.getAnnotation(SerializedName.class);
        if (serializedName != null) {
            return fieldNameValidator.validate(serializedName.value());
        }
        return this.delegate.translateName(f);
    }
}
