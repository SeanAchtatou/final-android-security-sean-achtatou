package com.google.gson;

import java.io.EOFException;
import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class JsonParserJavacc implements JsonParserJavaccConstants {
    private static int[] jj_la1_0;
    private static int[] jj_la1_1;
    private final JJCalls[] jj_2_rtns;
    private int jj_endpos;
    private List jj_expentries;
    private int[] jj_expentry;
    private int jj_gc;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private int jj_la;
    private final int[] jj_la1;
    private Token jj_lastpos;
    private int[] jj_lasttokens;
    private final LookaheadSuccess jj_ls;
    public Token jj_nt;
    private int jj_ntk;
    private boolean jj_rescan;
    private Token jj_scanpos;
    public Token token;
    public JsonParserJavaccTokenManager token_source;

    public final JsonElement parse() throws ParseException {
        int i;
        int i2;
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 0:
                jj_consume_token(0);
                throw new JsonParseException(new EOFException());
            case 6:
            case 7:
            case JsonParserJavaccConstants.NAN:
            case JsonParserJavaccConstants.INFINITY:
            case JsonParserJavaccConstants.BOOLEAN:
            case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
            case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
            case 26:
            case 27:
            case 31:
            case 33:
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case 26:
                        jj_consume_token(26);
                        break;
                    default:
                        this.jj_la1[0] = this.jj_gen;
                        break;
                }
                if (this.jj_ntk == -1) {
                    i2 = jj_ntk();
                } else {
                    i2 = this.jj_ntk;
                }
                switch (i2) {
                    case 6:
                    case JsonParserJavaccConstants.NAN:
                    case JsonParserJavaccConstants.INFINITY:
                    case JsonParserJavaccConstants.BOOLEAN:
                    case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
                    case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                    case 33:
                        return JsonPrimitive();
                    case 7:
                        return JsonNull();
                    case 27:
                        return JsonObject();
                    case 31:
                        return JsonArray();
                    default:
                        this.jj_la1[1] = this.jj_gen;
                        jj_consume_token(-1);
                        throw new ParseException();
                }
            default:
                this.jj_la1[2] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    private final JsonObject JsonObject() throws ParseException {
        JsonObject o = new JsonObject();
        jj_consume_token(27);
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case JsonParserJavaccConstants.IDENTIFIER_SANS_EXPONENT:
            case JsonParserJavaccConstants.IDENTIFIER_STARTS_WITH_EXPONENT:
            case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
            case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                Members(o);
                break;
            case JsonParserJavaccConstants.HEX_CHAR:
            case JsonParserJavaccConstants.UNICODE_CHAR:
            case JsonParserJavaccConstants.ESCAPE_CHAR:
            default:
                this.jj_la1[3] = this.jj_gen;
                break;
        }
        jj_consume_token(28);
        return o;
    }

    private final JsonNull JsonNull() throws ParseException {
        Token jj_consume_token = jj_consume_token(7);
        return JsonNull.createJsonNull();
    }

    private final void Members(JsonObject o) throws ParseException {
        Pair(o);
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 29:
                jj_consume_token(29);
                Members(o);
                return;
            default:
                this.jj_la1[4] = this.jj_gen;
                return;
        }
    }

    private final void Pair(JsonObject o) throws ParseException {
        JsonPrimitive property = JsonMemberName();
        jj_consume_token(30);
        o.add(property.getAsString(), JsonValue());
    }

    private final JsonPrimitive JsonMemberName() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case JsonParserJavaccConstants.IDENTIFIER_SANS_EXPONENT:
            case JsonParserJavaccConstants.IDENTIFIER_STARTS_WITH_EXPONENT:
                return new JsonPrimitive(Identifier().image);
            case JsonParserJavaccConstants.HEX_CHAR:
            case JsonParserJavaccConstants.UNICODE_CHAR:
            case JsonParserJavaccConstants.ESCAPE_CHAR:
            default:
                this.jj_la1[5] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
            case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
            case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                return JsonString();
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e A[FALL_THROUGH, LOOP:0: B:9:0x0032->B:15:0x004e, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003d A[SYNTHETIC] */
    private final com.google.gson.JsonArray JsonArray() throws com.google.gson.ParseException {
        /*
            r6 = this;
            r5 = -1
            com.google.gson.JsonArray r0 = new com.google.gson.JsonArray
            r0.<init>()
            r2 = 31
            r6.jj_consume_token(r2)
            int r2 = r6.jj_ntk
            if (r2 != r5) goto L_0x0026
            int r2 = r6.jj_ntk()
        L_0x0013:
            switch(r2) {
                case 6: goto L_0x002f;
                case 7: goto L_0x002f;
                case 8: goto L_0x002f;
                case 9: goto L_0x002f;
                case 10: goto L_0x002f;
                case 16: goto L_0x002f;
                case 17: goto L_0x002f;
                case 27: goto L_0x002f;
                case 31: goto L_0x002f;
                case 32: goto L_0x0029;
                case 33: goto L_0x002f;
                default: goto L_0x0016;
            }
        L_0x0016:
            int[] r2 = r6.jj_la1
            r3 = 7
            int r4 = r6.jj_gen
            r2[r3] = r4
            r6.jj_consume_token(r5)
            com.google.gson.ParseException r2 = new com.google.gson.ParseException
            r2.<init>()
            throw r2
        L_0x0026:
            int r2 = r6.jj_ntk
            goto L_0x0013
        L_0x0029:
            com.google.gson.JsonArray r0 = r6.JsonArrayEmpty(r0)
            r1 = r0
        L_0x002e:
            return r1
        L_0x002f:
            r6.JsonArrayElement(r0)
        L_0x0032:
            int r2 = r6.jj_ntk
            if (r2 != r5) goto L_0x004b
            int r2 = r6.jj_ntk()
        L_0x003a:
            switch(r2) {
                case 29: goto L_0x004e;
                default: goto L_0x003d;
            }
        L_0x003d:
            int[] r2 = r6.jj_la1
            r3 = 6
            int r4 = r6.jj_gen
            r2[r3] = r4
            r2 = 32
            r6.jj_consume_token(r2)
            r1 = r0
            goto L_0x002e
        L_0x004b:
            int r2 = r6.jj_ntk
            goto L_0x003a
        L_0x004e:
            r6.JsonArrayNextElement(r0)
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.JsonParserJavacc.JsonArray():com.google.gson.JsonArray");
    }

    private final JsonArray JsonArrayEmpty(JsonArray array) throws ParseException {
        jj_consume_token(32);
        return array;
    }

    private final JsonArray JsonArrayElement(JsonArray array) throws ParseException {
        array.add(JsonValue());
        return array;
    }

    private final JsonArray JsonArrayNextElement(JsonArray array) throws ParseException {
        jj_consume_token(29);
        array.add(JsonValue());
        return array;
    }

    private final JsonElement JsonValue() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 6:
            case JsonParserJavaccConstants.NAN:
            case JsonParserJavaccConstants.INFINITY:
            case 33:
                return JsonNumber();
            case 7:
                return JsonNull();
            case JsonParserJavaccConstants.BOOLEAN:
                return JsonBoolean();
            case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
            case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                return JsonString();
            case 27:
                return JsonObject();
            case 31:
                return JsonArray();
            default:
                this.jj_la1[8] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    private final JsonPrimitive JsonBoolean() throws ParseException {
        return new JsonPrimitive(Boolean.valueOf(Boolean.valueOf(jj_consume_token(10).image).booleanValue()));
    }

    private final JsonPrimitive JsonPrimitive() throws ParseException {
        int i;
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 6:
            case JsonParserJavaccConstants.NAN:
            case JsonParserJavaccConstants.INFINITY:
            case 33:
                return JsonNumber();
            case JsonParserJavaccConstants.BOOLEAN:
                return JsonBoolean();
            case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
            case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                return JsonString();
            default:
                this.jj_la1[9] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    private final JsonPrimitive JsonNumber() throws ParseException {
        int i;
        Number n;
        String fracpart = null;
        String exppart = null;
        if (jj_2_1(2)) {
            return JsonSpecialNumbers();
        }
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 6:
            case 33:
                String intpart = JsonInt();
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case 34:
                        fracpart = JsonFrac();
                        break;
                    default:
                        this.jj_la1[10] = this.jj_gen;
                        break;
                }
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case 5:
                        exppart = JsonExp();
                        break;
                    default:
                        this.jj_la1[11] = this.jj_gen;
                        break;
                }
                if (exppart == null && fracpart == null) {
                    n = new BigInteger(intpart);
                } else {
                    if (fracpart == null) {
                        fracpart = "";
                    }
                    if (exppart == null) {
                        exppart = "";
                    }
                    n = new BigDecimal(intpart + fracpart + exppart);
                }
                return new JsonPrimitive(n);
            default:
                this.jj_la1[12] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    private final JsonPrimitive JsonSpecialNumbers() throws ParseException {
        int i;
        double d;
        boolean negative = false;
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case JsonParserJavaccConstants.NAN:
                jj_consume_token(8);
                return new JsonPrimitive((Number) Double.valueOf(Double.NaN));
            case JsonParserJavaccConstants.INFINITY:
            case 33:
                switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
                    case 33:
                        jj_consume_token(33);
                        negative = true;
                        break;
                    default:
                        this.jj_la1[13] = this.jj_gen;
                        break;
                }
                jj_consume_token(9);
                if (negative) {
                    d = Double.NEGATIVE_INFINITY;
                } else {
                    d = Double.POSITIVE_INFINITY;
                }
                return new JsonPrimitive((Number) Double.valueOf(d));
            default:
                this.jj_la1[14] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    private final String JsonInt() throws ParseException {
        boolean negative = false;
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 33:
                jj_consume_token(33);
                negative = true;
                break;
            default:
                this.jj_la1[15] = this.jj_gen;
                break;
        }
        String digits = Digits();
        if (negative) {
            return "-" + digits;
        }
        return digits;
    }

    private final String JsonFrac() throws ParseException {
        jj_consume_token(34);
        return "." + Digits();
    }

    private final String JsonExp() throws ParseException {
        return jj_consume_token(5).image;
    }

    private final Token Identifier() throws ParseException {
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case JsonParserJavaccConstants.IDENTIFIER_SANS_EXPONENT:
                return jj_consume_token(11);
            case JsonParserJavaccConstants.IDENTIFIER_STARTS_WITH_EXPONENT:
                return jj_consume_token(12);
            default:
                this.jj_la1[16] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
    }

    private final String Digits() throws ParseException {
        return jj_consume_token(6).image;
    }

    private final JsonPrimitive JsonString() throws ParseException {
        Token t;
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case JsonParserJavaccConstants.SINGLE_QUOTE_LITERAL:
                t = jj_consume_token(16);
                break;
            case JsonParserJavaccConstants.DOUBLE_QUOTE_LITERAL:
                t = jj_consume_token(17);
                break;
            default:
                this.jj_la1[17] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        return new JsonPrimitive(StringUnmarshaller.unmarshall(t.image));
    }

    /* JADX INFO: finally extract failed */
    private boolean jj_2_1(int xla) {
        boolean z;
        this.jj_la = xla;
        Token token2 = this.token;
        this.jj_scanpos = token2;
        this.jj_lastpos = token2;
        try {
            if (!jj_3_1()) {
                z = true;
            } else {
                z = false;
            }
            jj_save(0, xla);
            return z;
        } catch (LookaheadSuccess e) {
            jj_save(0, xla);
            return true;
        } catch (Throwable th) {
            jj_save(0, xla);
            throw th;
        }
    }

    private boolean jj_3R_3() {
        if (jj_scan_token(8)) {
            return true;
        }
        return false;
    }

    private boolean jj_3R_2() {
        Token xsp = this.jj_scanpos;
        if (jj_3R_3()) {
            this.jj_scanpos = xsp;
            if (jj_3R_4()) {
                return true;
            }
        }
        return false;
    }

    private boolean jj_3R_5() {
        if (jj_scan_token(33)) {
            return true;
        }
        return false;
    }

    private boolean jj_3R_4() {
        Token xsp = this.jj_scanpos;
        if (jj_3R_5()) {
            this.jj_scanpos = xsp;
        }
        if (jj_scan_token(9)) {
            return true;
        }
        return false;
    }

    private boolean jj_3_1() {
        if (jj_3R_2()) {
            return true;
        }
        return false;
    }

    static {
        jj_la1_init_0();
        jj_la1_init_1();
    }

    private static void jj_la1_init_0() {
        jj_la1_0 = new int[]{67108864, -2013067328, -1945958463, 202752, 536870912, 202752, 536870912, -2013067328, -2013067328, 198464, 0, 32, 64, 0, 768, 0, 6144, 196608};
    }

    private static void jj_la1_init_1() {
        jj_la1_1 = new int[]{0, 2, 2, 0, 0, 0, 0, 3, 2, 2, 4, 0, 2, 2, 2, 2, 0, 0};
    }

    public JsonParserJavacc(InputStream stream) {
        this(stream, null);
    }

    public JsonParserJavacc(InputStream stream, String encoding) {
        this.jj_la1 = new int[18];
        this.jj_2_rtns = new JJCalls[1];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new ArrayList();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new JsonParserJavaccTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 18; i++) {
                this.jj_la1[i] = -1;
            }
            for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
                this.jj_2_rtns[i2] = new JJCalls();
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ReInit(InputStream stream) {
        ReInit(stream, null);
    }

    public void ReInit(InputStream stream, String encoding) {
        try {
            this.jj_input_stream.ReInit(stream, encoding, 1, 1);
            this.token_source.ReInit(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 18; i++) {
                this.jj_la1[i] = -1;
            }
            for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
                this.jj_2_rtns[i2] = new JJCalls();
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public JsonParserJavacc(Reader stream) {
        this.jj_la1 = new int[18];
        this.jj_2_rtns = new JJCalls[1];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new ArrayList();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new JsonParserJavaccTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 18; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void ReInit(Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 18; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public JsonParserJavacc(JsonParserJavaccTokenManager tm) {
        this.jj_la1 = new int[18];
        this.jj_2_rtns = new JJCalls[1];
        this.jj_rescan = false;
        this.jj_gc = 0;
        this.jj_ls = new LookaheadSuccess();
        this.jj_expentries = new ArrayList();
        this.jj_kind = -1;
        this.jj_lasttokens = new int[100];
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 18; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    public void ReInit(JsonParserJavaccTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 18; i++) {
            this.jj_la1[i] = -1;
        }
        for (int i2 = 0; i2 < this.jj_2_rtns.length; i2++) {
            this.jj_2_rtns[i2] = new JJCalls();
        }
    }

    private Token jj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            int i = this.jj_gc + 1;
            this.jj_gc = i;
            if (i > 100) {
                this.jj_gc = 0;
                for (JJCalls c : this.jj_2_rtns) {
                    while (c != null) {
                        if (c.gen < this.jj_gen) {
                            c.first = null;
                        }
                        c = c.next;
                    }
                }
            }
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw generateParseException();
    }

    private static final class LookaheadSuccess extends Error {
        private LookaheadSuccess() {
        }
    }

    private boolean jj_scan_token(int kind) {
        if (this.jj_scanpos == this.jj_lastpos) {
            this.jj_la--;
            if (this.jj_scanpos.next == null) {
                Token token2 = this.jj_scanpos;
                Token nextToken = this.token_source.getNextToken();
                token2.next = nextToken;
                this.jj_scanpos = nextToken;
                this.jj_lastpos = nextToken;
            } else {
                Token token3 = this.jj_scanpos.next;
                this.jj_scanpos = token3;
                this.jj_lastpos = token3;
            }
        } else {
            this.jj_scanpos = this.jj_scanpos.next;
        }
        if (this.jj_rescan) {
            int i = 0;
            Token tok = this.token;
            while (tok != null && tok != this.jj_scanpos) {
                i++;
                tok = tok.next;
            }
            if (tok != null) {
                jj_add_error_token(kind, i);
            }
        }
        if (this.jj_scanpos.kind != kind) {
            return true;
        }
        if (this.jj_la != 0 || this.jj_scanpos != this.jj_lastpos) {
            return false;
        }
        throw this.jj_ls;
    }

    public final Token getNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    public final Token getToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = this.token_source.getNextToken();
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private int jj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            int i = nextToken.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    private void jj_add_error_token(int kind, int pos) {
        if (pos < 100) {
            if (pos == this.jj_endpos + 1) {
                int[] iArr = this.jj_lasttokens;
                int i = this.jj_endpos;
                this.jj_endpos = i + 1;
                iArr[i] = kind;
            } else if (this.jj_endpos != 0) {
                this.jj_expentry = new int[this.jj_endpos];
                for (int i2 = 0; i2 < this.jj_endpos; i2++) {
                    this.jj_expentry[i2] = this.jj_lasttokens[i2];
                }
                Iterator it = this.jj_expentries.iterator();
                loop1:
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    int[] oldentry = (int[]) it.next();
                    if (oldentry.length == this.jj_expentry.length) {
                        int i3 = 0;
                        while (i3 < this.jj_expentry.length) {
                            if (oldentry[i3] == this.jj_expentry[i3]) {
                                i3++;
                            }
                        }
                        this.jj_expentries.add(this.jj_expentry);
                        break loop1;
                    }
                }
                if (pos != 0) {
                    int[] iArr2 = this.jj_lasttokens;
                    this.jj_endpos = pos;
                    iArr2[pos - 1] = kind;
                }
            }
        }
    }

    public ParseException generateParseException() {
        this.jj_expentries.clear();
        boolean[] la1tokens = new boolean[35];
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i = 0; i < 18; i++) {
            if (this.jj_la1[i] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                    if ((jj_la1_1[i] & (1 << j)) != 0) {
                        la1tokens[j + 32] = true;
                    }
                }
            }
        }
        for (int i2 = 0; i2 < 35; i2++) {
            if (la1tokens[i2]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i2;
                this.jj_expentries.add(this.jj_expentry);
            }
        }
        this.jj_endpos = 0;
        jj_rescan_token();
        jj_add_error_token(0, 0);
        int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int i3 = 0; i3 < this.jj_expentries.size(); i3++) {
            exptokseq[i3] = (int[]) this.jj_expentries.get(i3);
        }
        return new ParseException(this.token, exptokseq, tokenImage);
    }

    public final void enable_tracing() {
    }

    public final void disable_tracing() {
    }

    private void jj_rescan_token() {
        this.jj_rescan = true;
        for (int i = 0; i < 1; i++) {
            try {
                JJCalls p = this.jj_2_rtns[i];
                do {
                    if (p.gen > this.jj_gen) {
                        this.jj_la = p.arg;
                        Token token2 = p.first;
                        this.jj_scanpos = token2;
                        this.jj_lastpos = token2;
                        switch (i) {
                            case 0:
                                jj_3_1();
                                break;
                        }
                    }
                    p = p.next;
                } while (p != null);
            } catch (LookaheadSuccess e) {
            }
        }
        this.jj_rescan = false;
    }

    private void jj_save(int index, int xla) {
        JJCalls p = this.jj_2_rtns[index];
        while (true) {
            if (p.gen <= this.jj_gen) {
                break;
            } else if (p.next == null) {
                JJCalls p2 = new JJCalls();
                p.next = p2;
                p = p2;
                break;
            } else {
                p = p.next;
            }
        }
        p.gen = (this.jj_gen + xla) - this.jj_la;
        p.first = this.token;
        p.arg = xla;
    }

    static final class JJCalls {
        int arg;
        Token first;
        int gen;
        JJCalls next;

        JJCalls() {
        }
    }
}
