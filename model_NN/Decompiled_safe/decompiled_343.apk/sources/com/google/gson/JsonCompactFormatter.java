package com.google.gson;

import java.io.IOException;

final class JsonCompactFormatter implements JsonFormatter {
    private final boolean escapeHtmlChars;

    private static class FormattingVisitor implements JsonElementVisitor {
        private final Escaper escaper;
        private final boolean serializeNulls;
        private final Appendable writer;

        FormattingVisitor(Appendable writer2, Escaper escaper2, boolean serializeNulls2) {
            this.writer = writer2;
            this.escaper = escaper2;
            this.serializeNulls = serializeNulls2;
        }

        public void visitPrimitive(JsonPrimitive primitive) throws IOException {
            primitive.toString(this.writer, this.escaper);
        }

        public void visitNull() throws IOException {
            this.writer.append("null");
        }

        public void startArray(JsonArray array) throws IOException {
            this.writer.append('[');
        }

        public void visitArrayMember(JsonArray parent, JsonPrimitive member, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
            member.toString(this.writer, this.escaper);
        }

        public void visitArrayMember(JsonArray parent, JsonArray member, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
        }

        public void visitArrayMember(JsonArray parent, JsonObject member, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
        }

        public void visitNullArrayMember(JsonArray parent, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
        }

        public void endArray(JsonArray array) throws IOException {
            this.writer.append(']');
        }

        public void startObject(JsonObject object) throws IOException {
            this.writer.append('{');
        }

        public void visitObjectMember(JsonObject parent, String memberName, JsonPrimitive member, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
            this.writer.append('\"');
            this.writer.append(memberName);
            this.writer.append("\":");
            member.toString(this.writer, this.escaper);
        }

        public void visitObjectMember(JsonObject parent, String memberName, JsonArray member, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
            this.writer.append('\"');
            this.writer.append(memberName);
            this.writer.append("\":");
        }

        public void visitObjectMember(JsonObject parent, String memberName, JsonObject member, boolean isFirst) throws IOException {
            if (!isFirst) {
                this.writer.append(',');
            }
            this.writer.append('\"');
            this.writer.append(memberName);
            this.writer.append("\":");
        }

        public void visitNullObjectMember(JsonObject parent, String memberName, boolean isFirst) throws IOException {
            if (this.serializeNulls) {
                visitObjectMember(parent, memberName, (JsonObject) null, isFirst);
            }
        }

        public void endObject(JsonObject object) throws IOException {
            this.writer.append('}');
        }
    }

    JsonCompactFormatter() {
        this(true);
    }

    JsonCompactFormatter(boolean escapeHtmlChars2) {
        this.escapeHtmlChars = escapeHtmlChars2;
    }

    public void format(JsonElement root, Appendable writer, boolean serializeNulls) throws IOException {
        if (root != null) {
            new JsonTreeNavigator(new FormattingVisitor(writer, new Escaper(this.escapeHtmlChars), serializeNulls), serializeNulls).navigate(root);
        }
    }
}
