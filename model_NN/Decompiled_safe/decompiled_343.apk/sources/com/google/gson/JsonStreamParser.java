package com.google.gson;

import java.io.EOFException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class JsonStreamParser implements Iterator<JsonElement> {
    private final Object lock;
    private JsonElement nextElement;
    private final JsonParserJavacc parser;

    public JsonStreamParser(String json) {
        this(new StringReader(json));
    }

    public JsonStreamParser(Reader reader) {
        this.parser = new JsonParserJavacc(reader);
        this.lock = new Object();
        this.nextElement = null;
    }

    public JsonElement next() throws JsonParseException {
        synchronized (this.lock) {
            if (this.nextElement != null) {
                JsonElement returnValue = this.nextElement;
                this.nextElement = null;
                return returnValue;
            }
            try {
                return this.parser.parse();
            } catch (TokenMgrError e) {
                throw new JsonParseException("Failed parsing JSON source to Json", e);
            } catch (ParseException e2) {
                throw new JsonParseException("Failed parsing JSON source to Json", e2);
            } catch (StackOverflowError e3) {
                throw new JsonParseException("Failed parsing JSON source to Json", e3);
            } catch (OutOfMemoryError e4) {
                throw new JsonParseException("Failed parsing JSON source to Json", e4);
            } catch (JsonParseException e5) {
                JsonParseException e6 = e5;
                if (e6.getCause() instanceof EOFException) {
                    throw new NoSuchElementException();
                }
                throw e6;
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean hasNext() {
        /*
            r3 = this;
            java.lang.Object r1 = r3.lock
            monitor-enter(r1)
            com.google.gson.JsonElement r2 = r3.next()     // Catch:{ NoSuchElementException -> 0x000d }
            r3.nextElement = r2     // Catch:{ NoSuchElementException -> 0x000d }
            r2 = 1
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            r1 = r2
        L_0x000c:
            return r1
        L_0x000d:
            r2 = move-exception
            r0 = r2
            r2 = 0
            r3.nextElement = r2     // Catch:{ all -> 0x0016 }
            r2 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            r1 = r2
            goto L_0x000c
        L_0x0016:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0016 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.JsonStreamParser.hasNext():boolean");
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
