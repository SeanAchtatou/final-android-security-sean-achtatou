package org.json;

import cn.org.dian.easycommunicate.util.Constants;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class JSONTokener {
    private int index;
    private char lastChar;
    private Reader reader;
    private boolean useLastChar;

    public JSONTokener(Reader reader2) {
        this.reader = reader2.markSupported() ? reader2 : new BufferedReader(reader2);
        this.useLastChar = false;
        this.index = 0;
    }

    public JSONTokener(String str) {
        this(new StringReader(str));
    }

    public void back() throws JSONException {
        if (this.useLastChar || this.index <= 0) {
            throw new JSONException("Stepping back two steps is not supported");
        }
        this.index--;
        this.useLastChar = true;
    }

    public static int dehexchar(char c) {
        if (c >= '0' && c <= '9') {
            return c - '0';
        }
        if (c >= 'A' && c <= 'F') {
            return c - '7';
        }
        if (c < 'a' || c > 'f') {
            return -1;
        }
        return c - 'W';
    }

    public boolean more() throws JSONException {
        if (next() == 0) {
            return false;
        }
        back();
        return true;
    }

    public char next() throws JSONException {
        if (this.useLastChar) {
            this.useLastChar = false;
            if (this.lastChar != 0) {
                this.index++;
            }
            return this.lastChar;
        }
        try {
            int read = this.reader.read();
            if (read <= 0) {
                this.lastChar = 0;
                return 0;
            }
            this.index++;
            this.lastChar = (char) read;
            return this.lastChar;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    public char next(char c) throws JSONException {
        char next = next();
        if (next == c) {
            return next;
        }
        throw syntaxError("Expected '" + c + "' and instead saw '" + next + "'");
    }

    public String next(int i) throws JSONException {
        int i2;
        if (i == 0) {
            return "";
        }
        char[] cArr = new char[i];
        if (this.useLastChar) {
            this.useLastChar = false;
            cArr[0] = this.lastChar;
            i2 = 1;
        } else {
            i2 = 0;
        }
        while (i2 < i) {
            try {
                int read = this.reader.read(cArr, i2, i - i2);
                if (read == -1) {
                    break;
                }
                i2 += read;
            } catch (IOException e) {
                throw new JSONException(e);
            }
        }
        this.index += i2;
        if (i2 < i) {
            throw syntaxError("Substring bounds error");
        }
        this.lastChar = cArr[i - 1];
        return new String(cArr);
    }

    public char nextClean() throws JSONException {
        char next;
        do {
            next = next();
            if (next == 0) {
                break;
            }
        } while (next <= ' ');
        return next;
    }

    public String nextString(char c) throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            char next = next();
            switch (next) {
                case Constants.ENGLISH_INDICATOR:
                case 10:
                case 13:
                    throw syntaxError("Unterminated string");
                case '\\':
                    char next2 = next();
                    switch (next2) {
                        case 'b':
                            stringBuffer.append(8);
                            continue;
                        case 'f':
                            stringBuffer.append(12);
                            continue;
                        case 'n':
                            stringBuffer.append(10);
                            continue;
                        case 'r':
                            stringBuffer.append(13);
                            continue;
                        case 't':
                            stringBuffer.append(9);
                            continue;
                        case 'u':
                            stringBuffer.append((char) Integer.parseInt(next(4), 16));
                            continue;
                        case 'x':
                            stringBuffer.append((char) Integer.parseInt(next(2), 16));
                            continue;
                        default:
                            stringBuffer.append(next2);
                            continue;
                    }
                default:
                    if (next != c) {
                        stringBuffer.append(next);
                        break;
                    } else {
                        return stringBuffer.toString();
                    }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0017  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String nextTo(char r4) throws org.json.JSONException {
        /*
            r3 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
        L_0x0005:
            char r1 = r3.next()
            if (r1 == r4) goto L_0x0015
            if (r1 == 0) goto L_0x0015
            r2 = 10
            if (r1 == r2) goto L_0x0015
            r2 = 13
            if (r1 != r2) goto L_0x0023
        L_0x0015:
            if (r1 == 0) goto L_0x001a
            r3.back()
        L_0x001a:
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = r0.trim()
            return r0
        L_0x0023:
            r0.append(r1)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONTokener.nextTo(char):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String nextTo(java.lang.String r4) throws org.json.JSONException {
        /*
            r3 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
        L_0x0005:
            char r1 = r3.next()
            int r2 = r4.indexOf(r1)
            if (r2 >= 0) goto L_0x0019
            if (r1 == 0) goto L_0x0019
            r2 = 10
            if (r1 == r2) goto L_0x0019
            r2 = 13
            if (r1 != r2) goto L_0x0027
        L_0x0019:
            if (r1 == 0) goto L_0x001e
            r3.back()
        L_0x001e:
            java.lang.String r0 = r0.toString()
            java.lang.String r0 = r0.trim()
            return r0
        L_0x0027:
            r0.append(r1)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONTokener.nextTo(java.lang.String):java.lang.String");
    }

    public Object nextValue() throws JSONException {
        char nextClean = nextClean();
        switch (nextClean) {
            case '\"':
            case '\'':
                return nextString(nextClean);
            case '(':
            case '[':
                back();
                return new JSONArray(this);
            case '{':
                back();
                return new JSONObject(this);
            default:
                StringBuffer stringBuffer = new StringBuffer();
                while (nextClean >= ' ' && ",:]}/\\\"[{;=#".indexOf(nextClean) < 0) {
                    stringBuffer.append(nextClean);
                    nextClean = next();
                }
                back();
                String trim = stringBuffer.toString().trim();
                if (!trim.equals("")) {
                    return JSONObject.stringToValue(trim);
                }
                throw syntaxError("Missing value");
        }
    }

    public char skipTo(char c) throws JSONException {
        char next;
        try {
            int i = this.index;
            this.reader.mark(Integer.MAX_VALUE);
            do {
                next = next();
                if (next == 0) {
                    this.reader.reset();
                    this.index = i;
                    return next;
                }
            } while (next != c);
            back();
            return next;
        } catch (IOException e) {
            throw new JSONException(e);
        }
    }

    public JSONException syntaxError(String str) {
        return new JSONException(str + toString());
    }

    public String toString() {
        return " at character " + this.index;
    }
}
