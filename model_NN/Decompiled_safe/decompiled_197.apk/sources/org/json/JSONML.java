package org.json;

import java.util.Iterator;

public class JSONML {
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0145, code lost:
        throw r9.syntaxError("Reserved attribute.");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Object parse(org.json.XMLTokener r9, boolean r10, org.json.JSONArray r11) throws org.json.JSONException {
        /*
            r8 = 0
            r7 = 91
            r6 = 45
        L_0x0005:
            java.lang.Object r0 = r9.nextContent()
            java.lang.Character r1 = org.json.XML.LT
            if (r0 != r1) goto L_0x01cd
            java.lang.Object r0 = r9.nextToken()
            boolean r1 = r0 instanceof java.lang.Character
            if (r1 == 0) goto L_0x00be
            java.lang.Character r1 = org.json.XML.SLASH
            if (r0 != r1) goto L_0x004f
            java.lang.Object r0 = r9.nextToken()
            boolean r1 = r0 instanceof java.lang.String
            if (r1 != 0) goto L_0x0040
            org.json.JSONException r1 = new org.json.JSONException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Expected a closing name instead of '"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "'."
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x0040:
            java.lang.Object r1 = r9.nextToken()
            java.lang.Character r2 = org.json.XML.GT
            if (r1 == r2) goto L_0x0174
            java.lang.String r0 = "Misshaped close tag"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x004f:
            java.lang.Character r1 = org.json.XML.BANG
            if (r0 != r1) goto L_0x00ac
            char r0 = r9.next()
            if (r0 != r6) goto L_0x0068
            char r0 = r9.next()
            if (r0 != r6) goto L_0x0064
            java.lang.String r0 = "-->"
            r9.skipPast(r0)
        L_0x0064:
            r9.back()
            goto L_0x0005
        L_0x0068:
            if (r0 != r7) goto L_0x008d
            java.lang.Object r0 = r9.nextToken()
            java.lang.String r1 = "CDATA"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0086
            char r0 = r9.next()
            if (r0 != r7) goto L_0x0086
            if (r11 == 0) goto L_0x0005
            java.lang.String r0 = r9.nextCDATA()
            r11.put(r0)
            goto L_0x0005
        L_0x0086:
            java.lang.String r0 = "Expected 'CDATA['"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x008d:
            r0 = 1
        L_0x008e:
            java.lang.Object r1 = r9.nextMeta()
            if (r1 != 0) goto L_0x009b
            java.lang.String r0 = "Missing '>' after '<!'."
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x009b:
            java.lang.Character r2 = org.json.XML.LT
            if (r1 != r2) goto L_0x00a5
            int r0 = r0 + 1
        L_0x00a1:
            if (r0 > 0) goto L_0x008e
            goto L_0x0005
        L_0x00a5:
            java.lang.Character r2 = org.json.XML.GT
            if (r1 != r2) goto L_0x00a1
            int r0 = r0 + -1
            goto L_0x00a1
        L_0x00ac:
            java.lang.Character r1 = org.json.XML.QUEST
            if (r0 != r1) goto L_0x00b7
            java.lang.String r0 = "?>"
            r9.skipPast(r0)
            goto L_0x0005
        L_0x00b7:
            java.lang.String r0 = "Misshaped tag"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x00be:
            boolean r1 = r0 instanceof java.lang.String
            if (r1 != 0) goto L_0x00e0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Bad tagName '"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "'."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x00e0:
            java.lang.String r0 = (java.lang.String) r0
            org.json.JSONArray r3 = new org.json.JSONArray
            r3.<init>()
            org.json.JSONObject r4 = new org.json.JSONObject
            r4.<init>()
            if (r10 == 0) goto L_0x0106
            r3.put(r0)
            if (r11 == 0) goto L_0x00f6
            r11.put(r3)
        L_0x00f6:
            r1 = r8
        L_0x00f7:
            if (r1 != 0) goto L_0x00fd
            java.lang.Object r1 = r9.nextToken()
        L_0x00fd:
            if (r1 != 0) goto L_0x0111
            java.lang.String r0 = "Misshaped tag"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x0106:
            java.lang.String r1 = "tagName"
            r4.put(r1, r0)
            if (r11 == 0) goto L_0x00f6
            r11.put(r4)
            goto L_0x00f6
        L_0x0111:
            boolean r2 = r1 instanceof java.lang.String
            if (r2 != 0) goto L_0x0133
            if (r10 == 0) goto L_0x0120
            int r2 = r4.length()
            if (r2 <= 0) goto L_0x0120
            r3.put(r4)
        L_0x0120:
            java.lang.Character r2 = org.json.XML.SLASH
            if (r1 != r2) goto L_0x0177
            java.lang.Object r0 = r9.nextToken()
            java.lang.Character r1 = org.json.XML.GT
            if (r0 == r1) goto L_0x016f
            java.lang.String r0 = "Misshaped tag"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x0133:
            java.lang.String r1 = (java.lang.String) r1
            if (r10 != 0) goto L_0x0146
            java.lang.String r2 = "tagName"
            if (r1 == r2) goto L_0x013f
            java.lang.String r2 = "childNode"
            if (r1 != r2) goto L_0x0146
        L_0x013f:
            java.lang.String r0 = "Reserved attribute."
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x0146:
            java.lang.Object r2 = r9.nextToken()
            java.lang.Character r5 = org.json.XML.EQ
            if (r2 != r5) goto L_0x0168
            java.lang.Object r2 = r9.nextToken()
            boolean r5 = r2 instanceof java.lang.String
            if (r5 != 0) goto L_0x015d
            java.lang.String r0 = "Missing value"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x015d:
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r2 = org.json.JSONObject.stringToValue(r2)
            r4.accumulate(r1, r2)
            r1 = r8
            goto L_0x00f7
        L_0x0168:
            java.lang.String r5 = ""
            r4.accumulate(r1, r5)
            r1 = r2
            goto L_0x00f7
        L_0x016f:
            if (r11 != 0) goto L_0x0005
            if (r10 == 0) goto L_0x0175
            r0 = r3
        L_0x0174:
            return r0
        L_0x0175:
            r0 = r4
            goto L_0x0174
        L_0x0177:
            java.lang.Character r2 = org.json.XML.GT
            if (r1 == r2) goto L_0x0182
            java.lang.String r0 = "Misshaped tag"
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x0182:
            java.lang.Object r1 = parse(r9, r10, r3)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x0005
            boolean r2 = r1.equals(r0)
            if (r2 != 0) goto L_0x01b8
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Mismatched '"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r2 = "' and '"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "'"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            org.json.JSONException r0 = r9.syntaxError(r0)
            throw r0
        L_0x01b8:
            if (r10 != 0) goto L_0x01c5
            int r0 = r3.length()
            if (r0 <= 0) goto L_0x01c5
            java.lang.String r0 = "childNodes"
            r4.put(r0, r3)
        L_0x01c5:
            if (r11 != 0) goto L_0x0005
            if (r10 == 0) goto L_0x01cb
            r0 = r3
            goto L_0x0174
        L_0x01cb:
            r0 = r4
            goto L_0x0174
        L_0x01cd:
            if (r11 == 0) goto L_0x0005
            boolean r1 = r0 instanceof java.lang.String
            if (r1 == 0) goto L_0x01d9
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = org.json.JSONObject.stringToValue(r0)
        L_0x01d9:
            r11.put(r0)
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.JSONML.parse(org.json.XMLTokener, boolean, org.json.JSONArray):java.lang.Object");
    }

    public static JSONArray toJSONArray(String str) throws JSONException {
        return toJSONArray(new XMLTokener(str));
    }

    public static JSONArray toJSONArray(XMLTokener xMLTokener) throws JSONException {
        return (JSONArray) parse(xMLTokener, true, null);
    }

    public static JSONObject toJSONObject(XMLTokener xMLTokener) throws JSONException {
        return (JSONObject) parse(xMLTokener, false, null);
    }

    public static JSONObject toJSONObject(String str) throws JSONException {
        return toJSONObject(new XMLTokener(str));
    }

    public static String toString(JSONArray jSONArray) throws JSONException {
        int i;
        StringBuffer stringBuffer = new StringBuffer();
        String string = jSONArray.getString(0);
        XML.noSpace(string);
        String escape = XML.escape(string);
        stringBuffer.append('<');
        stringBuffer.append(escape);
        Object opt = jSONArray.opt(1);
        if (opt instanceof JSONObject) {
            JSONObject jSONObject = (JSONObject) opt;
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String obj = keys.next().toString();
                XML.noSpace(obj);
                String optString = jSONObject.optString(obj);
                if (optString != null) {
                    stringBuffer.append(' ');
                    stringBuffer.append(XML.escape(obj));
                    stringBuffer.append('=');
                    stringBuffer.append('\"');
                    stringBuffer.append(XML.escape(optString));
                    stringBuffer.append('\"');
                }
            }
            i = 2;
        } else {
            i = 1;
        }
        int length = jSONArray.length();
        if (i >= length) {
            stringBuffer.append('/');
            stringBuffer.append('>');
        } else {
            stringBuffer.append('>');
            int i2 = i;
            do {
                Object obj2 = jSONArray.get(i2);
                i2++;
                if (obj2 != null) {
                    if (obj2 instanceof String) {
                        stringBuffer.append(XML.escape(obj2.toString()));
                        continue;
                    } else if (obj2 instanceof JSONObject) {
                        stringBuffer.append(toString((JSONObject) obj2));
                        continue;
                    } else if (obj2 instanceof JSONArray) {
                        stringBuffer.append(toString((JSONArray) obj2));
                        continue;
                    } else {
                        continue;
                    }
                }
            } while (i2 < length);
            stringBuffer.append('<');
            stringBuffer.append('/');
            stringBuffer.append(escape);
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }

    public static String toString(JSONObject jSONObject) throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        String optString = jSONObject.optString("tagName");
        if (optString == null) {
            return XML.escape(jSONObject.toString());
        }
        XML.noSpace(optString);
        String escape = XML.escape(optString);
        stringBuffer.append('<');
        stringBuffer.append(escape);
        Iterator keys = jSONObject.keys();
        while (keys.hasNext()) {
            String obj = keys.next().toString();
            if (!obj.equals("tagName") && !obj.equals("childNodes")) {
                XML.noSpace(obj);
                String optString2 = jSONObject.optString(obj);
                if (optString2 != null) {
                    stringBuffer.append(' ');
                    stringBuffer.append(XML.escape(obj));
                    stringBuffer.append('=');
                    stringBuffer.append('\"');
                    stringBuffer.append(XML.escape(optString2));
                    stringBuffer.append('\"');
                }
            }
        }
        JSONArray optJSONArray = jSONObject.optJSONArray("childNodes");
        if (optJSONArray == null) {
            stringBuffer.append('/');
            stringBuffer.append('>');
        } else {
            stringBuffer.append('>');
            int length = optJSONArray.length();
            for (int i = 0; i < length; i++) {
                Object obj2 = optJSONArray.get(i);
                if (obj2 != null) {
                    if (obj2 instanceof String) {
                        stringBuffer.append(XML.escape(obj2.toString()));
                    } else if (obj2 instanceof JSONObject) {
                        stringBuffer.append(toString((JSONObject) obj2));
                    } else if (obj2 instanceof JSONArray) {
                        stringBuffer.append(toString((JSONArray) obj2));
                    }
                }
            }
            stringBuffer.append('<');
            stringBuffer.append('/');
            stringBuffer.append(escape);
            stringBuffer.append('>');
        }
        return stringBuffer.toString();
    }
}
