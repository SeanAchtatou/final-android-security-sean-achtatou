package org.json;

import java.util.HashMap;

public class XMLTokener extends JSONTokener {
    public static final HashMap entity = new HashMap(8);

    static {
        entity.put("amp", XML.AMP);
        entity.put("apos", XML.APOS);
        entity.put("gt", XML.GT);
        entity.put("lt", XML.LT);
        entity.put("quot", XML.QUOT);
    }

    public XMLTokener(String str) {
        super(str);
    }

    public String nextCDATA() throws JSONException {
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            char next = next();
            if (next == 0) {
                throw syntaxError("Unclosed CDATA");
            }
            stringBuffer.append(next);
            int length = stringBuffer.length() - 3;
            if (length >= 0 && stringBuffer.charAt(length) == ']' && stringBuffer.charAt(length + 1) == ']' && stringBuffer.charAt(length + 2) == '>') {
                stringBuffer.setLength(length);
                return stringBuffer.toString();
            }
        }
    }

    public Object nextContent() throws JSONException {
        char next;
        do {
            next = next();
        } while (Character.isWhitespace(next));
        if (next == 0) {
            return null;
        }
        if (next == '<') {
            return XML.LT;
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (next != '<' && next != 0) {
            if (next == '&') {
                stringBuffer.append(nextEntity(next));
            } else {
                stringBuffer.append(next);
            }
            next = next();
        }
        back();
        return stringBuffer.toString().trim();
    }

    public Object nextEntity(char c) throws JSONException {
        char next;
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            next = next();
            if (!Character.isLetterOrDigit(next) && next != '#') {
                break;
            }
            stringBuffer.append(Character.toLowerCase(next));
        }
        if (next == ';') {
            String stringBuffer2 = stringBuffer.toString();
            Object obj = entity.get(stringBuffer2);
            if (obj != null) {
                return obj;
            }
            return c + stringBuffer2 + ";";
        }
        throw syntaxError("Missing ';' in XML entity: &" + ((Object) stringBuffer));
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045 A[LOOP:1: B:3:0x000d->B:20:0x0045, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0017 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object nextMeta() throws org.json.JSONException {
        /*
            r2 = this;
        L_0x0000:
            char r0 = r2.next()
            boolean r1 = java.lang.Character.isWhitespace(r0)
            if (r1 != 0) goto L_0x0000
            switch(r0) {
                case 0: goto L_0x001a;
                case 33: goto L_0x002d;
                case 34: goto L_0x0033;
                case 39: goto L_0x0033;
                case 47: goto L_0x0027;
                case 60: goto L_0x0021;
                case 61: goto L_0x002a;
                case 62: goto L_0x0024;
                case 63: goto L_0x0030;
                default: goto L_0x000d;
            }
        L_0x000d:
            char r0 = r2.next()
            boolean r1 = java.lang.Character.isWhitespace(r0)
            if (r1 == 0) goto L_0x0045
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
        L_0x0019:
            return r0
        L_0x001a:
            java.lang.String r0 = "Misshaped meta tag"
            org.json.JSONException r0 = r2.syntaxError(r0)
            throw r0
        L_0x0021:
            java.lang.Character r0 = org.json.XML.LT
            goto L_0x0019
        L_0x0024:
            java.lang.Character r0 = org.json.XML.GT
            goto L_0x0019
        L_0x0027:
            java.lang.Character r0 = org.json.XML.SLASH
            goto L_0x0019
        L_0x002a:
            java.lang.Character r0 = org.json.XML.EQ
            goto L_0x0019
        L_0x002d:
            java.lang.Character r0 = org.json.XML.BANG
            goto L_0x0019
        L_0x0030:
            java.lang.Character r0 = org.json.XML.QUEST
            goto L_0x0019
        L_0x0033:
            char r1 = r2.next()
            if (r1 != 0) goto L_0x0040
            java.lang.String r0 = "Unterminated string"
            org.json.JSONException r0 = r2.syntaxError(r0)
            throw r0
        L_0x0040:
            if (r1 != r0) goto L_0x0033
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0019
        L_0x0045:
            switch(r0) {
                case 0: goto L_0x0049;
                case 33: goto L_0x0049;
                case 34: goto L_0x0049;
                case 39: goto L_0x0049;
                case 47: goto L_0x0049;
                case 60: goto L_0x0049;
                case 61: goto L_0x0049;
                case 62: goto L_0x0049;
                case 63: goto L_0x0049;
                default: goto L_0x0048;
            }
        L_0x0048:
            goto L_0x000d
        L_0x0049:
            r2.back()
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.XMLTokener.nextMeta():java.lang.Object");
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x006a A[LOOP:1: B:4:0x0012->B:27:0x006a, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x001f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object nextToken() throws org.json.JSONException {
        /*
            r4 = this;
        L_0x0000:
            char r0 = r4.next()
            boolean r1 = java.lang.Character.isWhitespace(r0)
            if (r1 != 0) goto L_0x0000
            switch(r0) {
                case 0: goto L_0x0024;
                case 33: goto L_0x003b;
                case 34: goto L_0x0041;
                case 39: goto L_0x0041;
                case 47: goto L_0x0035;
                case 60: goto L_0x002b;
                case 61: goto L_0x0038;
                case 62: goto L_0x0032;
                case 63: goto L_0x003e;
                default: goto L_0x000d;
            }
        L_0x000d:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0012:
            r1.append(r0)
            char r0 = r4.next()
            boolean r2 = java.lang.Character.isWhitespace(r0)
            if (r2 == 0) goto L_0x006a
            java.lang.String r0 = r1.toString()
        L_0x0023:
            return r0
        L_0x0024:
            java.lang.String r0 = "Misshaped element"
            org.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        L_0x002b:
            java.lang.String r0 = "Misplaced '<'"
            org.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        L_0x0032:
            java.lang.Character r0 = org.json.XML.GT
            goto L_0x0023
        L_0x0035:
            java.lang.Character r0 = org.json.XML.SLASH
            goto L_0x0023
        L_0x0038:
            java.lang.Character r0 = org.json.XML.EQ
            goto L_0x0023
        L_0x003b:
            java.lang.Character r0 = org.json.XML.BANG
            goto L_0x0023
        L_0x003e:
            java.lang.Character r0 = org.json.XML.QUEST
            goto L_0x0023
        L_0x0041:
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
        L_0x0046:
            char r2 = r4.next()
            if (r2 != 0) goto L_0x0053
            java.lang.String r0 = "Unterminated string"
            org.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        L_0x0053:
            if (r2 != r0) goto L_0x005a
            java.lang.String r0 = r1.toString()
            goto L_0x0023
        L_0x005a:
            r3 = 38
            if (r2 != r3) goto L_0x0066
            java.lang.Object r2 = r4.nextEntity(r2)
            r1.append(r2)
            goto L_0x0046
        L_0x0066:
            r1.append(r2)
            goto L_0x0046
        L_0x006a:
            switch(r0) {
                case 0: goto L_0x006e;
                case 33: goto L_0x0073;
                case 34: goto L_0x007b;
                case 39: goto L_0x007b;
                case 47: goto L_0x0073;
                case 60: goto L_0x007b;
                case 61: goto L_0x0073;
                case 62: goto L_0x0073;
                case 63: goto L_0x0073;
                case 91: goto L_0x0073;
                case 93: goto L_0x0073;
                default: goto L_0x006d;
            }
        L_0x006d:
            goto L_0x0012
        L_0x006e:
            java.lang.String r0 = r1.toString()
            goto L_0x0023
        L_0x0073:
            r4.back()
            java.lang.String r0 = r1.toString()
            goto L_0x0023
        L_0x007b:
            java.lang.String r0 = "Bad character in a name"
            org.json.JSONException r0 = r4.syntaxError(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.XMLTokener.nextToken():java.lang.Object");
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 131 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean skipPast(java.lang.String r10) throws org.json.JSONException {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            int r0 = r10.length()
            char[] r1 = new char[r0]
            r2 = r7
        L_0x0009:
            if (r2 >= r0) goto L_0x0042
            char r3 = r9.next()
            if (r3 != 0) goto L_0x0013
            r0 = r7
        L_0x0012:
            return r0
        L_0x0013:
            r1[r2] = r3
            int r2 = r2 + 1
            goto L_0x0009
        L_0x0018:
            r1[r2] = r3
            int r2 = r2 + 1
            if (r2 < r0) goto L_0x001f
            int r2 = r2 - r0
        L_0x001f:
            r3 = r2
            r4 = r7
        L_0x0021:
            if (r4 >= r0) goto L_0x0040
            char r5 = r1[r3]
            char r6 = r10.charAt(r4)
            if (r5 == r6) goto L_0x0030
            r3 = r7
        L_0x002c:
            if (r3 == 0) goto L_0x0038
            r0 = r8
            goto L_0x0012
        L_0x0030:
            int r3 = r3 + 1
            if (r3 < r0) goto L_0x0035
            int r3 = r3 - r0
        L_0x0035:
            int r4 = r4 + 1
            goto L_0x0021
        L_0x0038:
            char r3 = r9.next()
            if (r3 != 0) goto L_0x0018
            r0 = r7
            goto L_0x0012
        L_0x0040:
            r3 = r8
            goto L_0x002c
        L_0x0042:
            r2 = r7
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.json.XMLTokener.skipPast(java.lang.String):boolean");
    }
}
