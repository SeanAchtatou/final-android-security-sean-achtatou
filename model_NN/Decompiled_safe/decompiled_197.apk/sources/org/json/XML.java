package org.json;

import java.util.Iterator;

public class XML {
    public static final Character AMP = new Character('&');
    public static final Character APOS = new Character('\'');
    public static final Character BANG = new Character('!');
    public static final Character EQ = new Character('=');
    public static final Character GT = new Character('>');
    public static final Character LT = new Character('<');
    public static final Character QUEST = new Character('?');
    public static final Character QUOT = new Character('\"');
    public static final Character SLASH = new Character('/');

    public static String escape(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case '\"':
                    stringBuffer.append("&quot;");
                    break;
                case '&':
                    stringBuffer.append("&amp;");
                    break;
                case '<':
                    stringBuffer.append("&lt;");
                    break;
                case '>':
                    stringBuffer.append("&gt;");
                    break;
                default:
                    stringBuffer.append(charAt);
                    break;
            }
        }
        return stringBuffer.toString();
    }

    public static void noSpace(String str) throws JSONException {
        int length = str.length();
        if (length == 0) {
            throw new JSONException("Empty string.");
        }
        for (int i = 0; i < length; i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                throw new JSONException("'" + str + "' contains a space character.");
            }
        }
    }

    private static boolean parse(XMLTokener xMLTokener, JSONObject jSONObject, String str) throws JSONException {
        Object nextToken = xMLTokener.nextToken();
        if (nextToken == BANG) {
            char next = xMLTokener.next();
            if (next == '-') {
                if (xMLTokener.next() == '-') {
                    xMLTokener.skipPast("-->");
                    return false;
                }
                xMLTokener.back();
            } else if (next == '[') {
                if (!xMLTokener.nextToken().equals("CDATA") || xMLTokener.next() != '[') {
                    throw xMLTokener.syntaxError("Expected 'CDATA['");
                }
                String nextCDATA = xMLTokener.nextCDATA();
                if (nextCDATA.length() > 0) {
                    jSONObject.accumulate("content", nextCDATA);
                }
                return false;
            }
            int i = 1;
            do {
                Object nextMeta = xMLTokener.nextMeta();
                if (nextMeta == null) {
                    throw xMLTokener.syntaxError("Missing '>' after '<!'.");
                } else if (nextMeta == LT) {
                    i++;
                    continue;
                } else if (nextMeta == GT) {
                    i--;
                    continue;
                } else {
                    continue;
                }
            } while (i > 0);
            return false;
        } else if (nextToken == QUEST) {
            xMLTokener.skipPast("?>");
            return false;
        } else if (nextToken == SLASH) {
            Object nextToken2 = xMLTokener.nextToken();
            if (str == null) {
                throw xMLTokener.syntaxError("Mismatched close tag" + nextToken2);
            } else if (!nextToken2.equals(str)) {
                throw xMLTokener.syntaxError("Mismatched " + str + " and " + nextToken2);
            } else if (xMLTokener.nextToken() == GT) {
                return true;
            } else {
                throw xMLTokener.syntaxError("Misshaped close tag");
            }
        } else if (nextToken instanceof Character) {
            throw xMLTokener.syntaxError("Misshaped tag");
        } else {
            String str2 = (String) nextToken;
            JSONObject jSONObject2 = new JSONObject();
            Object obj = null;
            while (true) {
                if (obj == null) {
                    obj = xMLTokener.nextToken();
                }
                if (obj instanceof String) {
                    String str3 = (String) obj;
                    Object nextToken3 = xMLTokener.nextToken();
                    if (nextToken3 == EQ) {
                        Object nextToken4 = xMLTokener.nextToken();
                        if (!(nextToken4 instanceof String)) {
                            throw xMLTokener.syntaxError("Missing value");
                        }
                        jSONObject2.accumulate(str3, JSONObject.stringToValue((String) nextToken4));
                        obj = null;
                    } else {
                        jSONObject2.accumulate(str3, "");
                        obj = nextToken3;
                    }
                } else if (obj == SLASH) {
                    if (xMLTokener.nextToken() != GT) {
                        throw xMLTokener.syntaxError("Misshaped tag");
                    }
                    jSONObject.accumulate(str2, jSONObject2);
                    return false;
                } else if (obj == GT) {
                    while (true) {
                        Object nextContent = xMLTokener.nextContent();
                        if (nextContent == null) {
                            if (str2 == null) {
                                return false;
                            }
                            throw xMLTokener.syntaxError("Unclosed tag " + str2);
                        } else if (nextContent instanceof String) {
                            String str4 = (String) nextContent;
                            if (str4.length() > 0) {
                                jSONObject2.accumulate("content", JSONObject.stringToValue(str4));
                            }
                        } else if (nextContent == LT && parse(xMLTokener, jSONObject2, str2)) {
                            if (jSONObject2.length() == 0) {
                                jSONObject.accumulate(str2, "");
                            } else if (jSONObject2.length() != 1 || jSONObject2.opt("content") == null) {
                                jSONObject.accumulate(str2, jSONObject2);
                            } else {
                                jSONObject.accumulate(str2, jSONObject2.opt("content"));
                            }
                            return false;
                        }
                    }
                } else {
                    throw xMLTokener.syntaxError("Misshaped tag");
                }
            }
        }
    }

    public static JSONObject toJSONObject(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        XMLTokener xMLTokener = new XMLTokener(str);
        while (xMLTokener.more() && xMLTokener.skipPast("<")) {
            parse(xMLTokener, jSONObject, null);
        }
        return jSONObject;
    }

    public static String toString(Object obj) throws JSONException {
        return toString(obj, null);
    }

    public static String toString(Object obj, String str) throws JSONException {
        String str2;
        Object obj2;
        StringBuffer stringBuffer = new StringBuffer();
        if (obj instanceof JSONObject) {
            if (str != null) {
                stringBuffer.append('<');
                stringBuffer.append(str);
                stringBuffer.append('>');
            }
            JSONObject jSONObject = (JSONObject) obj;
            Iterator keys = jSONObject.keys();
            while (keys.hasNext()) {
                String obj3 = keys.next().toString();
                Object opt = jSONObject.opt(obj3);
                if (opt == null) {
                    obj2 = "";
                } else {
                    obj2 = opt;
                }
                if (obj2 instanceof String) {
                    String str3 = (String) obj2;
                }
                if (obj3.equals("content")) {
                    if (obj2 instanceof JSONArray) {
                        JSONArray jSONArray = (JSONArray) obj2;
                        int length = jSONArray.length();
                        for (int i = 0; i < length; i++) {
                            if (i > 0) {
                                stringBuffer.append(10);
                            }
                            stringBuffer.append(escape(jSONArray.get(i).toString()));
                        }
                    } else {
                        stringBuffer.append(escape(obj2.toString()));
                    }
                } else if (obj2 instanceof JSONArray) {
                    JSONArray jSONArray2 = (JSONArray) obj2;
                    int length2 = jSONArray2.length();
                    for (int i2 = 0; i2 < length2; i2++) {
                        Object obj4 = jSONArray2.get(i2);
                        if (obj4 instanceof JSONArray) {
                            stringBuffer.append('<');
                            stringBuffer.append(obj3);
                            stringBuffer.append('>');
                            stringBuffer.append(toString(obj4));
                            stringBuffer.append("</");
                            stringBuffer.append(obj3);
                            stringBuffer.append('>');
                        } else {
                            stringBuffer.append(toString(obj4, obj3));
                        }
                    }
                } else if (obj2.equals("")) {
                    stringBuffer.append('<');
                    stringBuffer.append(obj3);
                    stringBuffer.append("/>");
                } else {
                    stringBuffer.append(toString(obj2, obj3));
                }
            }
            if (str != null) {
                stringBuffer.append("</");
                stringBuffer.append(str);
                stringBuffer.append('>');
            }
            return stringBuffer.toString();
        } else if (obj instanceof JSONArray) {
            JSONArray jSONArray3 = (JSONArray) obj;
            int length3 = jSONArray3.length();
            for (int i3 = 0; i3 < length3; i3++) {
                Object opt2 = jSONArray3.opt(i3);
                if (str == null) {
                    str2 = "array";
                } else {
                    str2 = str;
                }
                stringBuffer.append(toString(opt2, str2));
            }
            return stringBuffer.toString();
        } else {
            String escape = obj == null ? "null" : escape(obj.toString());
            if (str == null) {
                return "\"" + escape + "\"";
            }
            return escape.length() == 0 ? "<" + str + "/>" : "<" + str + ">" + escape + "</" + str + ">";
        }
    }
}
