package org.junit.internal.runners.statements;

import org.junit.runners.model.Statement;

public class FailOnTimeout extends Statement {
    /* access modifiers changed from: private */
    public boolean fFinished = false;
    /* access modifiers changed from: private */
    public Statement fNext;
    /* access modifiers changed from: private */
    public Throwable fThrown = null;
    private final long fTimeout;

    public FailOnTimeout(Statement next, long timeout) {
        this.fNext = next;
        this.fTimeout = timeout;
    }

    public void evaluate() throws Throwable {
        Thread thread = new Thread() {
            public void run() {
                try {
                    FailOnTimeout.this.fNext.evaluate();
                    boolean unused = FailOnTimeout.this.fFinished = true;
                } catch (Throwable th) {
                    Throwable unused2 = FailOnTimeout.this.fThrown = th;
                }
            }
        };
        thread.start();
        thread.join(this.fTimeout);
        if (!this.fFinished) {
            if (this.fThrown != null) {
                throw this.fThrown;
            }
            Exception exception = new Exception(String.format("test timed out after %d milliseconds", Long.valueOf(this.fTimeout)));
            exception.setStackTrace(thread.getStackTrace());
            throw exception;
        }
    }
}
