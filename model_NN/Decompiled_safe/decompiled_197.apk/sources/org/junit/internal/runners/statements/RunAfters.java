package org.junit.internal.runners.statements;

import java.util.List;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class RunAfters extends Statement {
    private final List<FrameworkMethod> fAfters;
    private final Statement fNext;
    private final Object fTarget;

    public RunAfters(Statement next, List<FrameworkMethod> afters, Object target) {
        this.fNext = next;
        this.fAfters = afters;
        this.fTarget = target;
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public void evaluate() throws java.lang.Throwable {
        /*
            r7 = this;
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            r2.clear()
            org.junit.runners.model.Statement r4 = r7.fNext     // Catch:{ Throwable -> 0x002e }
            r4.evaluate()     // Catch:{ Throwable -> 0x002e }
            java.util.List<org.junit.runners.model.FrameworkMethod> r4 = r7.fAfters
            java.util.Iterator r3 = r4.iterator()
        L_0x0013:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0077
            java.lang.Object r1 = r3.next()
            org.junit.runners.model.FrameworkMethod r1 = (org.junit.runners.model.FrameworkMethod) r1
            java.lang.Object r4 = r7.fTarget     // Catch:{ Throwable -> 0x0028 }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x0028 }
            r1.invokeExplosively(r4, r5)     // Catch:{ Throwable -> 0x0028 }
            goto L_0x0013
        L_0x0028:
            r4 = move-exception
            r0 = r4
            r2.add(r0)
            goto L_0x0013
        L_0x002e:
            r4 = move-exception
            r0 = r4
            r2.add(r0)     // Catch:{ all -> 0x0054 }
            java.util.List<org.junit.runners.model.FrameworkMethod> r4 = r7.fAfters
            java.util.Iterator r3 = r4.iterator()
        L_0x0039:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0077
            java.lang.Object r1 = r3.next()
            org.junit.runners.model.FrameworkMethod r1 = (org.junit.runners.model.FrameworkMethod) r1
            java.lang.Object r4 = r7.fTarget     // Catch:{ Throwable -> 0x004e }
            r5 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Throwable -> 0x004e }
            r1.invokeExplosively(r4, r5)     // Catch:{ Throwable -> 0x004e }
            goto L_0x0039
        L_0x004e:
            r4 = move-exception
            r0 = r4
            r2.add(r0)
            goto L_0x0039
        L_0x0054:
            r4 = move-exception
            java.util.List<org.junit.runners.model.FrameworkMethod> r5 = r7.fAfters
            java.util.Iterator r3 = r5.iterator()
        L_0x005b:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x0076
            java.lang.Object r1 = r3.next()
            org.junit.runners.model.FrameworkMethod r1 = (org.junit.runners.model.FrameworkMethod) r1
            java.lang.Object r5 = r7.fTarget     // Catch:{ Throwable -> 0x0070 }
            r6 = 0
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ Throwable -> 0x0070 }
            r1.invokeExplosively(r5, r6)     // Catch:{ Throwable -> 0x0070 }
            goto L_0x005b
        L_0x0070:
            r5 = move-exception
            r0 = r5
            r2.add(r0)
            goto L_0x005b
        L_0x0076:
            throw r4
        L_0x0077:
            org.junit.internal.runners.model.MultipleFailureException.assertEmpty(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.junit.internal.runners.statements.RunAfters.evaluate():void");
    }
}
