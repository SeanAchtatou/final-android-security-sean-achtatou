package org.junit.internal.matchers;

import org.hamcrest.Description;

public abstract class SubstringMatcher extends TypeSafeMatcher<String> {
    protected final String substring;

    /* access modifiers changed from: protected */
    public abstract boolean evalSubstringOf(String str);

    /* access modifiers changed from: protected */
    public abstract String relationship();

    protected SubstringMatcher(String substring2) {
        this.substring = substring2;
    }

    public boolean matchesSafely(String item) {
        return evalSubstringOf(item);
    }

    public void describeTo(Description description) {
        description.appendText("a string ").appendText(relationship()).appendText(" ").appendValue(this.substring);
    }
}
