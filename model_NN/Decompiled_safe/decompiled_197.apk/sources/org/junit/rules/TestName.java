package org.junit.rules;

import org.junit.runners.model.FrameworkMethod;

public class TestName extends TestWatchman {
    private String fName;

    public void starting(FrameworkMethod method) {
        this.fName = method.getName();
    }

    public String getMethodName() {
        return this.fName;
    }
}
