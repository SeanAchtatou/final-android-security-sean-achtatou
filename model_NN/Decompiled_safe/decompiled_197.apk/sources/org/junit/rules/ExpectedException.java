package org.junit.rules;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.junit.Assert;
import org.junit.internal.matchers.TypeSafeMatcher;
import org.junit.matchers.JUnitMatchers;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class ExpectedException implements MethodRule {
    /* access modifiers changed from: private */
    public Matcher<Object> fMatcher = null;

    public static ExpectedException none() {
        return new ExpectedException();
    }

    private ExpectedException() {
    }

    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        return new ExpectedExceptionStatement(base);
    }

    public void expect(Matcher<?> matcher) {
        if (this.fMatcher == null) {
            this.fMatcher = matcher;
        } else {
            this.fMatcher = JUnitMatchers.both(this.fMatcher).and(matcher);
        }
    }

    public void expect(Class<? extends Throwable> type) {
        expect(CoreMatchers.instanceOf(type));
    }

    public void expectMessage(String substring) {
        expectMessage(JUnitMatchers.containsString(substring));
    }

    public void expectMessage(Matcher<String> matcher) {
        expect(hasMessage(matcher));
    }

    private class ExpectedExceptionStatement extends Statement {
        private final Statement fNext;

        public ExpectedExceptionStatement(Statement base) {
            this.fNext = base;
        }

        public void evaluate() throws Throwable {
            try {
                this.fNext.evaluate();
                if (ExpectedException.this.fMatcher != null) {
                    throw new AssertionError("Expected test to throw " + StringDescription.toString(ExpectedException.this.fMatcher));
                }
            } catch (Throwable th) {
                Throwable e = th;
                if (ExpectedException.this.fMatcher == null) {
                    throw e;
                }
                Assert.assertThat(e, ExpectedException.this.fMatcher);
            }
        }
    }

    private Matcher<Throwable> hasMessage(final Matcher<String> matcher) {
        return new TypeSafeMatcher<Throwable>() {
            public void describeTo(Description description) {
                description.appendText("exception with message ");
                description.appendDescriptionOf(matcher);
            }

            public boolean matchesSafely(Throwable item) {
                return matcher.matches(item.getMessage());
            }
        };
    }
}
