package org.junit.rules;

import org.junit.internal.runners.statements.FailOnTimeout;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public class Timeout implements MethodRule {
    private final int fMillis;

    public Timeout(int millis) {
        this.fMillis = millis;
    }

    public Statement apply(Statement base, FrameworkMethod method, Object target) {
        return new FailOnTimeout(base, (long) this.fMillis);
    }
}
