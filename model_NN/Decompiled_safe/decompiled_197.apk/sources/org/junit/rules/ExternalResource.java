package org.junit.rules;

import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

public abstract class ExternalResource implements MethodRule {
    public final Statement apply(final Statement base, FrameworkMethod method, Object target) {
        return new Statement() {
            public void evaluate() throws Throwable {
                ExternalResource.this.before();
                try {
                    base.evaluate();
                } finally {
                    ExternalResource.this.after();
                }
            }
        };
    }

    /* access modifiers changed from: protected */
    public void before() throws Throwable {
    }

    /* access modifiers changed from: protected */
    public void after() {
    }
}
