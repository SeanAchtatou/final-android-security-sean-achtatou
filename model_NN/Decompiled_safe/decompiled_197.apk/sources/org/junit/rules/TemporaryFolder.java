package org.junit.rules;

import java.io.File;
import java.io.IOException;

public class TemporaryFolder extends ExternalResource {
    private File folder;

    /* access modifiers changed from: protected */
    public void before() throws Throwable {
        create();
    }

    /* access modifiers changed from: protected */
    public void after() {
        delete();
    }

    public void create() throws IOException {
        this.folder = File.createTempFile("junit", "");
        this.folder.delete();
        this.folder.mkdir();
    }

    public File newFile(String fileName) throws IOException {
        File file = new File(this.folder, fileName);
        file.createNewFile();
        return file;
    }

    public File newFolder(String folderName) {
        File file = new File(this.folder, folderName);
        file.mkdir();
        return file;
    }

    public File getRoot() {
        return this.folder;
    }

    public void delete() {
        recursiveDelete(this.folder);
    }

    private void recursiveDelete(File file) {
        File[] files = file.listFiles();
        if (files != null) {
            for (File each : files) {
                recursiveDelete(each);
            }
        }
        file.delete();
    }
}
