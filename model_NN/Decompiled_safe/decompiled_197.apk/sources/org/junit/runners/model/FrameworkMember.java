package org.junit.runners.model;

import java.lang.annotation.Annotation;
import java.util.Iterator;
import java.util.List;
import org.junit.runners.model.FrameworkMember;

abstract class FrameworkMember<T extends FrameworkMember<T>> {
    /* access modifiers changed from: package-private */
    public abstract Annotation[] getAnnotations();

    /* access modifiers changed from: package-private */
    public abstract boolean isShadowedBy(FrameworkMember frameworkMember);

    FrameworkMember() {
    }

    /* access modifiers changed from: package-private */
    public boolean isShadowedBy(List list) {
        Iterator i$ = list.iterator();
        while (i$.hasNext()) {
            if (isShadowedBy((FrameworkMember) ((FrameworkMember) i$.next()))) {
                return true;
            }
        }
        return false;
    }
}
