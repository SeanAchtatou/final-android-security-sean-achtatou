package org.junit.runners.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class FrameworkField extends FrameworkMember<FrameworkField> {
    private final Field fField;

    FrameworkField(Field field) {
        this.fField = field;
    }

    public Annotation[] getAnnotations() {
        return this.fField.getAnnotations();
    }

    public boolean isShadowedBy(FrameworkField otherMember) {
        return otherMember.getField().getName().equals(getField().getName());
    }

    public Field getField() {
        return this.fField;
    }

    public Object get(Object target) throws IllegalArgumentException, IllegalAccessException {
        return this.fField.get(target);
    }
}
