package org.junit.runners;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.internal.AssumptionViolatedException;
import org.junit.internal.runners.model.EachTestNotifier;
import org.junit.internal.runners.statements.RunAfters;
import org.junit.internal.runners.statements.RunBefores;
import org.junit.runner.Description;
import org.junit.runner.Runner;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.Filterable;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runner.manipulation.Sortable;
import org.junit.runner.manipulation.Sorter;
import org.junit.runner.notification.RunNotifier;
import org.junit.runner.notification.StoppedByUserException;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.RunnerScheduler;
import org.junit.runners.model.Statement;
import org.junit.runners.model.TestClass;

public abstract class ParentRunner<T> extends Runner implements Filterable, Sortable {
    private Filter fFilter = null;
    private RunnerScheduler fScheduler = new RunnerScheduler() {
        public void schedule(Runnable childStatement) {
            childStatement.run();
        }

        public void finished() {
        }
    };
    /* access modifiers changed from: private */
    public Sorter fSorter = Sorter.NULL;
    private final TestClass fTestClass;

    /* access modifiers changed from: protected */
    public abstract Description describeChild(T t);

    /* access modifiers changed from: protected */
    public abstract List<T> getChildren();

    /* access modifiers changed from: protected */
    public abstract void runChild(T t, RunNotifier runNotifier);

    protected ParentRunner(Class<?> testClass) throws InitializationError {
        this.fTestClass = new TestClass(testClass);
        validate();
    }

    /* access modifiers changed from: protected */
    public void collectInitializationErrors(List<Throwable> errors) {
        validatePublicVoidNoArgMethods(BeforeClass.class, true, errors);
        validatePublicVoidNoArgMethods(AfterClass.class, true, errors);
    }

    /* access modifiers changed from: protected */
    public void validatePublicVoidNoArgMethods(Class<? extends Annotation> annotation, boolean isStatic, List<Throwable> errors) {
        for (FrameworkMethod eachTestMethod : getTestClass().getAnnotatedMethods(annotation)) {
            eachTestMethod.validatePublicVoidNoArg(isStatic, errors);
        }
    }

    /* access modifiers changed from: protected */
    public Statement classBlock(RunNotifier notifier) {
        return withAfterClasses(withBeforeClasses(childrenInvoker(notifier)));
    }

    /* access modifiers changed from: protected */
    public Statement withBeforeClasses(Statement statement) {
        List<FrameworkMethod> befores = this.fTestClass.getAnnotatedMethods(BeforeClass.class);
        return befores.isEmpty() ? statement : new RunBefores(statement, befores, null);
    }

    /* access modifiers changed from: protected */
    public Statement withAfterClasses(Statement statement) {
        List<FrameworkMethod> afters = this.fTestClass.getAnnotatedMethods(AfterClass.class);
        return afters.isEmpty() ? statement : new RunAfters(statement, afters, null);
    }

    /* access modifiers changed from: protected */
    public Statement childrenInvoker(final RunNotifier notifier) {
        return new Statement() {
            public void evaluate() {
                ParentRunner.this.runChildren(notifier);
            }
        };
    }

    /* access modifiers changed from: private */
    public void runChildren(final RunNotifier notifier) {
        for (final T each : getFilteredChildren()) {
            this.fScheduler.schedule(new Runnable() {
                public void run() {
                    ParentRunner.this.runChild(each, notifier);
                }
            });
        }
        this.fScheduler.finished();
    }

    /* access modifiers changed from: protected */
    public String getName() {
        return this.fTestClass.getName();
    }

    public final TestClass getTestClass() {
        return this.fTestClass;
    }

    public Description getDescription() {
        Description description = Description.createSuiteDescription(getName(), this.fTestClass.getAnnotations());
        for (T child : getFilteredChildren()) {
            description.addChild(describeChild(child));
        }
        return description;
    }

    public void run(RunNotifier notifier) {
        EachTestNotifier testNotifier = new EachTestNotifier(notifier, getDescription());
        try {
            classBlock(notifier).evaluate();
        } catch (AssumptionViolatedException e) {
            testNotifier.fireTestIgnored();
        } catch (StoppedByUserException e2) {
            throw e2;
        } catch (Throwable th) {
            testNotifier.addFailure(th);
        }
    }

    public void filter(Filter filter) throws NoTestsRemainException {
        this.fFilter = filter;
        for (T each : getChildren()) {
            if (shouldRun(each)) {
                return;
            }
        }
        throw new NoTestsRemainException();
    }

    public void sort(Sorter sorter) {
        this.fSorter = sorter;
    }

    private void validate() throws InitializationError {
        List<Throwable> errors = new ArrayList<>();
        collectInitializationErrors(errors);
        if (!errors.isEmpty()) {
            throw new InitializationError(errors);
        }
    }

    private List<T> getFilteredChildren() {
        ArrayList<T> filtered = new ArrayList<>();
        for (T each : getChildren()) {
            if (shouldRun(each)) {
                try {
                    filterChild(each);
                    sortChild(each);
                    filtered.add(each);
                } catch (NoTestsRemainException e) {
                }
            }
        }
        Collections.sort(filtered, comparator());
        return filtered;
    }

    private void sortChild(T child) {
        this.fSorter.apply(child);
    }

    private void filterChild(T child) throws NoTestsRemainException {
        if (this.fFilter != null) {
            this.fFilter.apply(child);
        }
    }

    private boolean shouldRun(T each) {
        return this.fFilter == null || this.fFilter.shouldRun(describeChild(each));
    }

    private Comparator<? super T> comparator() {
        return new Comparator<T>() {
            public int compare(T o1, T o2) {
                return ParentRunner.this.fSorter.compare(ParentRunner.this.describeChild(o1), ParentRunner.this.describeChild(o2));
            }
        };
    }

    public void setScheduler(RunnerScheduler scheduler) {
        this.fScheduler = scheduler;
    }
}
