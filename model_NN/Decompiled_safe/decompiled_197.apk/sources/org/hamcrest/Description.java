package org.hamcrest;

public interface Description {
    Description appendDescriptionOf(SelfDescribing selfDescribing);

    Description appendList(String str, String str2, String str3, Iterable<? extends SelfDescribing> iterable);

    Description appendText(String str);

    Description appendValue(Object obj);

    <T> Description appendValueList(String str, String str2, String str3, Iterable iterable);

    <T> Description appendValueList(String str, String str2, String str3, Object... objArr);
}
