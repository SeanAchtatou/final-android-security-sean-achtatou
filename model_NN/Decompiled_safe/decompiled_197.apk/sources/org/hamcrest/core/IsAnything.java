package org.hamcrest.core;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;

public class IsAnything<T> extends BaseMatcher<T> {
    private final String description;

    public IsAnything() {
        this("ANYTHING");
    }

    public IsAnything(String description2) {
        this.description = description2;
    }

    public boolean matches(Object o) {
        return true;
    }

    public void describeTo(Description description2) {
        description2.appendText(this.description);
    }

    @Factory
    public static <T> Matcher<T> anything() {
        return new IsAnything();
    }

    @Factory
    public static <T> Matcher<T> anything(String description2) {
        return new IsAnything(description2);
    }

    @Factory
    public static <T> Matcher<T> any(Class<T> cls) {
        return new IsAnything();
    }
}
