package cn.org.dian.easycommunicate;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import cn.org.dian.easycommunicate.model.DataCenter;
import cn.org.dian.easycommunicate.model.Term;
import cn.org.dian.easycommunicate.util.Constants;
import cn.org.dian.easycommunicate.util.Utilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TermActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        getWindow().setFeatureInt(5, R.layout.progress);
        setContentView((int) R.layout.term_activity);
        Term term = DataCenter.getTerm(getIntent().getStringExtra("term_name"));
        setTitle(term.getName());
        prepareData(term);
    }

    private void prepareData(Term term) {
        List<HashMap<String, Object>> contentList = new ArrayList<>();
        HashMap<String, String> allLanguages = term.getAllLanguages();
        for (String key : allLanguages.keySet()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("text_foreign", allLanguages.get(key));
            map.put("language", key);
            contentList.add(map);
        }
        SimpleAdapter adapter = new SimpleAdapter(this, contentList, R.layout.term_activity_list_item, new String[]{"text_foreign", "button_speak", "language"}, new int[]{R.id.text_foreign_2, R.id.button_speak_2, R.id.language_text});
        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object data, String textRepresentation) {
                int viewId = view.getId();
                if (viewId == R.id.text_foreign_2 || viewId == R.id.language_text) {
                    ((TextView) view).setText((String) data);
                    return true;
                } else if (viewId != R.id.button_speak_2) {
                    return false;
                } else {
                    ((ImageButton) view).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Utilities.playLocalFile(Constants.languageIndicatorToEnum(Term.getAllLanguageNames().indexOf((String) ((TextView) ((LinearLayout) ((RelativeLayout) v.getParent()).getChildAt(1)).getChildAt(0)).getText())), (String) ((TextView) ((LinearLayout) ((RelativeLayout) v.getParent()).getChildAt(1)).getChildAt(1)).getText());
                        }
                    });
                    return true;
                }
            }
        });
        ((ListView) findViewById(R.id.term_content_list)).setAdapter((ListAdapter) adapter);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return Utilities.onCreateOptionsMenu(this, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return Utilities.onOptionsItemSelected(this, item);
    }
}
