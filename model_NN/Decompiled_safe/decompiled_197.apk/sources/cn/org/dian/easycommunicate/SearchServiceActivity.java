package cn.org.dian.easycommunicate;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import cn.org.dian.easycommunicate.model.DataCenter;
import java.util.List;

public class SearchServiceActivity extends ListActivity {
    private static final String TAG = "SearchServiceActivity";
    private List<String> currentResultList = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            String query = intent.getStringExtra("query");
            Log.d(TAG, "query: " + query);
            List<String> resultList = DataCenter.searchTerm(query);
            if (resultList.size() == 0) {
                resultList.add(getResources().getString(R.string.no_search_result));
            }
            setListAdapter(new ArrayAdapter<>(this, 17367043, (String[]) resultList.toArray(new String[0])));
            this.currentResultList = resultList;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        String selectedTermName = this.currentResultList.get(position);
        if (DataCenter.getTerm(selectedTermName) != null) {
            Intent intent = new Intent(this, TermActivity.class);
            intent.putExtra("term_name", selectedTermName);
            startActivity(intent);
        }
    }
}
