package cn.org.dian.easycommunicate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import cn.org.dian.easycommunicate.model.DataCenter;

public class LocalContentActivity extends Activity implements AdapterView.OnItemClickListener {
    private static final String TAG = "WelcomeActivity";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.local_content_activity);
        initLanguageSelectList();
    }

    private void initLanguageSelectList() {
        ListView languageSelectList = (ListView) findViewById(R.id.select_language_list);
        languageSelectList.setAdapter(new ArrayAdapter(this, 17367043, DataCenter.getAllSupportLanguages()));
        languageSelectList.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Log.d(TAG, "user click pos: " + position + " id: " + id);
        Intent intent = new Intent(this, ThemeSelectActivity.class);
        intent.putExtra("language_indicator", position);
        startActivity(intent);
    }

    public void onBackPressed() {
        Activity parent = getParent();
        if (parent != null) {
            parent.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
