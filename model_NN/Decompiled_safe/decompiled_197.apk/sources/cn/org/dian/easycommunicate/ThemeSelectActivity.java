package cn.org.dian.easycommunicate;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import cn.org.dian.easycommunicate.model.DataCenter;
import cn.org.dian.easycommunicate.util.Utilities;

public class ThemeSelectActivity extends ListActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new ArrayAdapter(this, 17367043, DataCenter.getAllThemeName()));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(this, ThemeActivity.class);
        intent.putExtra("theme_indicator", position);
        intent.putExtra("language_indicator", getIntent().getIntExtra("language_indicator", 0));
        startActivity(intent);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return Utilities.onCreateOptionsMenu(this, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return Utilities.onOptionsItemSelected(this, item);
    }
}
