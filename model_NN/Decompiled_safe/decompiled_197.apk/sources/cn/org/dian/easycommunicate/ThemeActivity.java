package cn.org.dian.easycommunicate;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import cn.org.dian.easycommunicate.model.DataCenter;
import cn.org.dian.easycommunicate.model.Term;
import cn.org.dian.easycommunicate.model.Theme;
import cn.org.dian.easycommunicate.util.Constants;
import cn.org.dian.easycommunicate.util.Utilities;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ThemeActivity extends Activity {
    private static final String TAG = "ThemeActivity";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        getWindow().setFeatureInt(5, R.layout.progress);
        setContentView((int) R.layout.theme_activity);
        Intent passedIntent = getIntent();
        int themeIndicator = passedIntent.getIntExtra("theme_indicator", 0);
        int languageIndicator = passedIntent.getIntExtra("language_indicator", 0);
        Log.d(TAG, "themeIndicator:" + themeIndicator + " languageIndicator: " + languageIndicator);
        setTitle(Theme.getAllThemeNames().get(themeIndicator));
        prepareDisplayData(languageIndicator, themeIndicator);
    }

    private void prepareDisplayData(final int languageIndicator, int themeIndicator) {
        List<HashMap<String, Object>> contentList = new ArrayList<>();
        HashMap<String, Term> allTerms = DataCenter.getTheme(themeIndicator).getAllTermsUnderTheme();
        String language = Term.getAllLanguageNames().get(languageIndicator);
        for (String termName : allTerms.keySet()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("text_chinese", termName);
            map.put("text_foreign", allTerms.get(termName).getAllLanguages().get(language));
            contentList.add(map);
        }
        SimpleAdapter adapter = new SimpleAdapter(this, contentList, R.layout.theme_activity_list_item, new String[]{"text_chinese", "text_foreign", "button_speak"}, new int[]{R.id.text_chinese, R.id.text_foreign, R.id.button_speak});
        ListView lv = (ListView) findViewById(R.id.theme_content_list);
        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            public boolean setViewValue(View view, Object data, String textRepresentation) {
                int viewId = view.getId();
                if (viewId == R.id.text_chinese || viewId == R.id.text_foreign) {
                    ((TextView) view).setText((String) data);
                    return true;
                } else if (viewId != R.id.button_speak) {
                    return false;
                } else {
                    final int i = languageIndicator;
                    ((ImageButton) view).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Utilities.playLocalFile(Constants.languageIndicatorToEnum(i), (String) ((TextView) ((LinearLayout) ((RelativeLayout) v.getParent()).getChildAt(1)).getChildAt(1)).getText());
                        }
                    });
                    return true;
                }
            }
        });
        lv.setAdapter((ListAdapter) adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Log.d(ThemeActivity.TAG, "click: " + position);
                Intent intent = new Intent(ThemeActivity.this, TermActivity.class);
                intent.putExtra("term_name", (String) ((TextView) ((LinearLayout) ((RelativeLayout) view).getChildAt(1)).getChildAt(0)).getText());
                ThemeActivity.this.startActivity(intent);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return Utilities.onCreateOptionsMenu(this, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return Utilities.onOptionsItemSelected(this, item);
    }
}
