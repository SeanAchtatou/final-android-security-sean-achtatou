package cn.org.dian.easycommunicate.util;

import com.google.api.translate.Language;

public class Constants {
    public static final String DEFAULT_TAG = "EasyComm";
    public static final int ENGLISH_INDICATOR = 0;
    public static final Integer FILE_NAME_LENGTH_MAX = 255;
    public static final int FRENCH_INDICATOR = 1;
    public static final int GERMAN_INDICATOR = 2;
    public static final int ITALIAN_INDICATOR = 3;
    public static final String PREF_KEY_MOBILE_NETWORK_STATS = "mobile_network_stats";
    public static final String PREF_KEY_SHOW_CLEAR_STAT = "show_clear_stats";
    public static final String PREF_KEY_SHOW_MOBILE_NETWORK_STATS = "show_mobile_network_stats";
    public static final String PREF_NAME = "cn.org.dian.easycommunicate_preferences";
    public static final int SPANISH_INDICATOR = 4;

    public static Language languageIndicatorToEnum(int languageIndicator) {
        if (Utilities.isCurrentLanEng()) {
            languageIndicator++;
        }
        switch (languageIndicator) {
            case ENGLISH_INDICATOR /*0*/:
                return Language.ENGLISH;
            case 1:
                return Language.FRENCH;
            case 2:
                return Language.GERMAN;
            case ITALIAN_INDICATOR /*3*/:
                return Language.ITALIAN;
            case SPANISH_INDICATOR /*4*/:
                return Language.SPANISH;
            default:
                return null;
        }
    }

    public static Integer enumToLanguageIndicator(Language language) {
        if (language.equals(Language.ENGLISH)) {
            return 0;
        }
        if (language.equals(Language.FRENCH)) {
            return 1;
        }
        if (language.equals(Language.GERMAN)) {
            return 2;
        }
        if (language.equals(Language.ITALIAN)) {
            return 3;
        }
        if (language.equals(Language.SPANISH)) {
            return 4;
        }
        return null;
    }
}
