package cn.org.dian.easycommunicate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import cn.org.dian.easycommunicate.model.DataCenter;
import cn.org.dian.easycommunicate.util.Utilities;

public class WelcomeActivity extends Activity {
    public static boolean isExiting = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.welcome_activity);
        new AsyncInitTask().execute(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (isExiting) {
            isExiting = false;
            finish();
        }
    }

    class AsyncInitTask extends AsyncTask<Context, Void, Void> {
        AsyncInitTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Context... params) {
            Utilities.init(WelcomeActivity.this);
            DataCenter.init(WelcomeActivity.this.getResources().openRawResource(Utilities.isCurrentLanEng() ? R.raw.contenten : R.raw.content));
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WelcomeActivity.this.startActivity(new Intent(WelcomeActivity.this, MainFrame.class));
            return null;
        }
    }
}
