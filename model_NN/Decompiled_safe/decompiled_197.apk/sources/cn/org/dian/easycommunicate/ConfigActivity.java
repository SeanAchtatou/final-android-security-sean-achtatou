package cn.org.dian.easycommunicate;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import cn.org.dian.easycommunicate.util.Constants;
import cn.org.dian.easycommunicate.util.NetworkTrafficStats;
import cn.org.dian.easycommunicate.util.Utilities;

public class ConfigActivity extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String summaryToSet;
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.config);
        Preference networkStatsPref = findPreference(Constants.PREF_KEY_SHOW_MOBILE_NETWORK_STATS);
        if (NetworkTrafficStats.getAvailbility()) {
            long statsInByte = Utilities.getSharedPreferences().getLong(Constants.PREF_KEY_MOBILE_NETWORK_STATS, 0);
            summaryToSet = getResources().getString(R.string.show_mobile_network_stats_summary, convertByteToMB(statsInByte));
        } else {
            summaryToSet = getResources().getString(R.string.network_stats_not_support);
            findPreference(Constants.PREF_KEY_SHOW_CLEAR_STAT).setEnabled(false);
        }
        networkStatsPref.setSummary(summaryToSet);
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference.getKey().equals(Constants.PREF_KEY_SHOW_CLEAR_STAT)) {
            showClearStatsDialog();
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    private void showClearStatsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.clear_stats_prompt).setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Utilities.getSharedPreferences().edit().putLong(Constants.PREF_KEY_MOBILE_NETWORK_STATS, 0).commit();
                Utilities.showShortToast(ConfigActivity.this, (int) R.string.clear_success);
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    private String convertByteToMB(long bytes) {
        long bytes2 = bytes / 1000;
        String kbPart = String.valueOf(bytes2 % 1000);
        if (kbPart.length() == 1) {
            kbPart = "00" + kbPart;
        } else if (kbPart.length() == 2) {
            kbPart = "0" + kbPart;
        }
        return String.valueOf(String.valueOf(bytes2 / 1000)) + "." + kbPart + "M";
    }
}
