package cn.org.dian.easycommunicate.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class Term {
    private static List<String> allLanguageNames = new ArrayList();
    private HashMap<String, String> allLanguages;
    private String name;

    public Term() {
        this.name = null;
        this.allLanguages = null;
        this.allLanguages = new LinkedHashMap();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void addLanguage(String languageName, String content) {
        this.allLanguages.put(languageName, content);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("---Term: " + this.name + "\n");
        for (String languageName : this.allLanguages.keySet()) {
            sb.append("language: " + languageName + "content: " + this.allLanguages.get(languageName) + "\n");
        }
        sb.append("---Term end\n");
        return new String(sb);
    }

    public HashMap<String, String> getAllLanguages() {
        return this.allLanguages;
    }

    public static List<String> getAllLanguageNames() {
        return allLanguageNames;
    }

    public static void addAllLanguageNames(String languageName) {
        allLanguageNames.add(languageName);
    }
}
