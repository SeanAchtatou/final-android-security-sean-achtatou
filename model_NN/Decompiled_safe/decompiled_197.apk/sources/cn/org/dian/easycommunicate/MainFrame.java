package cn.org.dian.easycommunicate;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import cn.org.dian.easycommunicate.util.Utilities;

public class MainFrame extends TabActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        getWindow().setFeatureInt(5, R.layout.progress);
        setContentView((int) R.layout.main_frame);
        createTabs();
    }

    private void createTabs() {
        TabHost tabHost = getTabHost();
        tabHost.addTab(tabHost.newTabSpec("local").setIndicator(getString(R.string.local_indicator), getResources().getDrawable(R.drawable.local_tag_small)).setContent(new Intent(this, LocalContentActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("online").setIndicator(getString(R.string.online_indicator), getResources().getDrawable(R.drawable.online_tag_small)).setContent(new Intent(this, OnlineTranslateActivity.class)));
        getTabHost().setCurrentTab(0);
    }

    public void onBackPressed() {
        new AlertDialog.Builder(this).setMessage((int) R.string.exit_prompt).setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                WelcomeActivity.isExiting = true;
                MainFrame.this.finish();
            }
        }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        }).create().show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return Utilities.onCreateOptionsMenu(this, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        return Utilities.onOptionsItemSelected(this, item);
    }
}
