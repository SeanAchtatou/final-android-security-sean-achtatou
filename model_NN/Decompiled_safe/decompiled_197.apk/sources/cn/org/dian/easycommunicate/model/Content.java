package cn.org.dian.easycommunicate.model;

import java.util.HashMap;

public class Content {
    private HashMap<String, Theme> allThemes;

    public Content() {
        this.allThemes = null;
        this.allThemes = new HashMap<>();
    }

    public void addTheme(String themeName, Theme theme) {
        this.allThemes.put(themeName, theme);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("-Content:\n");
        for (String themeName : this.allThemes.keySet()) {
            sb.append(this.allThemes.get(themeName).toString());
        }
        sb.append("-Content end\n");
        return new String(sb);
    }

    public HashMap<String, Theme> getAllThemes() {
        return this.allThemes;
    }
}
