package cn.org.dian.easycommunicate.xml;

import cn.org.dian.easycommunicate.model.Content;
import cn.org.dian.easycommunicate.model.Term;
import cn.org.dian.easycommunicate.model.Theme;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/* compiled from: XmlParser */
class XMLContentHandler extends DefaultHandler {
    private static final String ATTR_NAME = "name";
    private static final String TAG_LANGUAGE = "language";
    private static final String TAG_TERM = "term";
    private static final String TAG_THEME = "theme";
    private Content content = null;
    private String currentLanguage = null;
    private Term currentTerm = null;
    private Theme currentTheme = null;
    private boolean hasLanguageNamesAdded = false;
    private boolean isAdding = false;

    XMLContentHandler() {
    }

    public void startDocument() throws SAXException {
        this.content = new Content();
    }

    public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
        String attsString = atts.getValue(ATTR_NAME);
        if (TAG_THEME.equals(localName)) {
            this.currentTheme = new Theme();
            this.currentTheme.setName(attsString);
            Theme.addThemeName(attsString);
        } else if (TAG_TERM.equals(localName)) {
            this.currentTerm = new Term();
            this.currentTerm.setName(attsString);
            if (!this.hasLanguageNamesAdded) {
                this.isAdding = true;
            }
        } else if (TAG_LANGUAGE.equals(localName)) {
            this.currentLanguage = attsString;
            if (this.isAdding) {
                Term.addAllLanguageNames(attsString);
            }
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (TAG_LANGUAGE.equals(localName)) {
            this.currentLanguage = null;
        } else if (TAG_TERM.equals(localName)) {
            this.currentTheme.addTerm(this.currentTerm.getName(), this.currentTerm);
            if (this.isAdding) {
                this.isAdding = false;
                this.hasLanguageNamesAdded = true;
            }
        } else if (TAG_THEME.equals(localName)) {
            this.content.addTheme(this.currentTheme.getName(), this.currentTheme);
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (this.currentLanguage != null) {
            this.currentTerm.addLanguage(this.currentLanguage, new String(ch, start, length));
        }
    }

    public Content getContent() {
        return this.content;
    }
}
