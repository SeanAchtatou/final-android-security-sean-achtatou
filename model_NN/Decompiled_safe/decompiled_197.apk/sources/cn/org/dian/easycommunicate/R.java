package cn.org.dian.easycommunicate;

public final class R {

    public static final class array {
        public static final int online_search_languages = 2131165184;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int earth_lightblue = 2130837504;
        public static final int icon = 2130837505;
        public static final int local_tag_small = 2130837506;
        public static final int media_play = 2130837507;
        public static final int online_tag_small = 2130837508;
        public static final int speak_out = 2130837509;
    }

    public static final class id {
        public static final int button_speak = 2131296274;
        public static final int button_speak_2 = 2131296270;
        public static final int from_lang_speak_button = 2131296261;
        public static final int from_lang_textview = 2131296258;
        public static final int from_language_select_spinner = 2131296259;
        public static final int imageView1 = 2131296278;
        public static final int language_text = 2131296271;
        public static final int menu_help = 2131296285;
        public static final int menu_search = 2131296284;
        public static final int menu_settings = 2131296283;
        public static final int online_search_button = 2131296265;
        public static final int online_search_input = 2131296262;
        public static final int online_search_result = 2131296267;
        public static final int progress = 2131296268;
        public static final int select_language_list = 2131296257;
        public static final int select_language_text = 2131296256;
        public static final int term_content_list = 2131296269;
        public static final int textView1 = 2131296277;
        public static final int textView2 = 2131296279;
        public static final int textView3 = 2131296281;
        public static final int textView4 = 2131296282;
        public static final int textView5 = 2131296280;
        public static final int text_chinese = 2131296275;
        public static final int text_foreign = 2131296276;
        public static final int text_foreign_2 = 2131296272;
        public static final int theme_content_list = 2131296273;
        public static final int to_lang_speak_button = 2131296266;
        public static final int to_lang_textview = 2131296263;
        public static final int to_language_select_spinner = 2131296264;
        public static final int voice_input_button = 2131296260;
    }

    public static final class layout {
        public static final int custom_list_item_single_choice = 2130903040;
        public static final int local_content_activity = 2130903041;
        public static final int main_frame = 2130903042;
        public static final int online_translate_activity = 2130903043;
        public static final int progress = 2130903044;
        public static final int term_activity = 2130903045;
        public static final int term_activity_list_item = 2130903046;
        public static final int theme_activity = 2130903047;
        public static final int theme_activity_list_item = 2130903048;
        public static final int welcome_activity = 2130903049;
    }

    public static final class menu {
        public static final int main_frame_option_menu = 2131230720;
    }

    public static final class raw {
        public static final int content = 2131034112;
        public static final int contenten = 2131034113;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int cancel = 2131099657;
        public static final int clear_stats_prompt = 2131099661;
        public static final int clear_success = 2131099662;
        public static final int confirm = 2131099656;
        public static final int dest_lang = 2131099682;
        public static final int empty_alert_text = 2131099655;
        public static final int exit_prompt = 2131099660;
        public static final int help = 2131099674;
        public static final int help_content = 2131099687;
        public static final int install_prompt = 2131099686;
        public static final int local_indicator = 2131099664;
        public static final int network_category = 2131099676;
        public static final int network_error = 2131099666;
        public static final int network_stats_not_support = 2131099679;
        public static final int no_search_result = 2131099651;
        public static final int online_indicator = 2131099665;
        public static final int online_search_input_hint = 2131099653;
        public static final int online_search_resullt_hint = 2131099654;
        public static final int remote_service_error = 2131099667;
        public static final int search = 2131099675;
        public static final int search_hint = 2131099650;
        public static final int search_result_title = 2131099652;
        public static final int select_language = 2131099649;
        public static final int settings = 2131099673;
        public static final int show_clear_stats_title = 2131099680;
        public static final int show_mobile_network_stats_summary = 2131099678;
        public static final int show_mobile_network_stats_title = 2131099677;
        public static final int source_lang = 2131099681;
        public static final int speak_again = 2131099658;
        public static final int theme_select_activity_label = 2131099663;
        public static final int translate = 2131099684;
        public static final int translate_prepare = 2131099669;
        public static final int tts_fail = 2131099670;
        public static final int tts_prepare = 2131099668;
        public static final int voice_input = 2131099683;
        public static final int voice_input_prompt = 2131099672;
        public static final int voice_reco_fail = 2131099671;
        public static final int voice_reco_result_choose_msg = 2131099659;
        public static final int welcome_title = 2131099685;
    }

    public static final class xml {
        public static final int config = 2130968576;
        public static final int searchable = 2130968577;
    }
}
