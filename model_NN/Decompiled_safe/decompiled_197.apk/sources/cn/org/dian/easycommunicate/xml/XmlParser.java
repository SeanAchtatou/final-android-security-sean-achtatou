package cn.org.dian.easycommunicate.xml;

import cn.org.dian.easycommunicate.model.Content;
import java.io.InputStream;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class XmlParser {
    public static Content readContentfromXML(InputStream inputStream) {
        try {
            SAXParser saxParser = SAXParserFactory.newInstance().newSAXParser();
            XMLContentHandler handler = new XMLContentHandler();
            saxParser.parse(inputStream, handler);
            return handler.getContent();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
