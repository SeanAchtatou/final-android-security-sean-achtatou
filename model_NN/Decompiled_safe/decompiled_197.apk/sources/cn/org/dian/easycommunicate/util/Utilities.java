package cn.org.dian.easycommunicate.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import cn.org.dian.easycommunicate.ConfigActivity;
import cn.org.dian.easycommunicate.R;
import com.google.api.translate.Language;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

public class Utilities {
    private static final String LANGUAGE_ENGLISH = "en";
    private static final String LANGYAGE_CHINESE = "zh";
    private static final String TAG = "Utilities";
    private static AssetManager assetManager = null;
    private static ConnectivityManager connMan = null;
    private static String currentLanguage = null;
    private static SharedPreferences sharedPreferences = null;

    public static void init(Activity activity) {
        connMan = (ConnectivityManager) activity.getSystemService("connectivity");
        sharedPreferences = activity.getSharedPreferences(Constants.PREF_NAME, 0);
        assetManager = activity.getAssets();
        currentLanguage = Locale.getDefault().getLanguage();
        Log.d(TAG, "currentLanguage: " + currentLanguage);
    }

    public static boolean playLocalFile(Language language, String speakText) {
        try {
            AssetFileDescriptor afd = assetManager.openFd("audio_files/" + (String.valueOf(language.toString()) + "_" + BKDRHash(speakText).toString() + ".mp3"));
            MediaPlayer mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                mediaPlayer.prepare();
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
                return true;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return false;
            } catch (IllegalStateException e2) {
                e2.printStackTrace();
                return false;
            } catch (IOException e3) {
                e3.printStackTrace();
                return false;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            Log.d(TAG, "local file not found for: " + language.toString() + " " + speakText);
            return false;
        }
    }

    private static Integer BKDRHash(String str) {
        int hash = 0;
        for (int i = 0; i < str.length(); i++) {
            hash = (hash * 131) + str.charAt(i);
        }
        return Integer.valueOf(Integer.MAX_VALUE & hash);
    }

    public static String utf8Encode(String raw) {
        try {
            return URLEncoder.encode(raw, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String convertToLegalName(String origin) {
        return utf8Encode(origin);
    }

    public static boolean checkNetworkUsability() {
        NetworkInfo networkInfo = connMan.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    public static boolean checkSDCardUsability() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    public static boolean onCreateOptionsMenu(Activity activity, Menu menu) {
        activity.getMenuInflater().inflate(R.menu.main_frame_option_menu, menu);
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public static boolean onOptionsItemSelected(Activity activity, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                activity.startActivity(new Intent(activity, ConfigActivity.class));
                return true;
            case R.id.menu_search:
                activity.onSearchRequested();
                break;
            case R.id.menu_help:
                showHelpDialog(activity);
                return true;
        }
        return true;
    }

    public static void showShortToast(Context context, int resId) {
        Toast.makeText(context, resId, 0).show();
    }

    public static void showLongToast(Context context, int resId) {
        Toast.makeText(context, resId, 1).show();
    }

    public static void showShortToast(Context context, String string) {
        Toast.makeText(context, string, 0).show();
    }

    public static void showLongToast(Context context, String string) {
        Toast.makeText(context, string, 1).show();
    }

    public static ConnectivityManager getConnectivityManager() {
        return connMan;
    }

    public static SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public static boolean isCurrentLanEng() {
        return currentLanguage.equalsIgnoreCase(LANGUAGE_ENGLISH);
    }

    private static void showHelpDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle((int) R.string.help).setMessage(activity.getText(R.string.help_content)).setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }
}
