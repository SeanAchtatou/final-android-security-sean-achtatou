package cn.org.dian.easycommunicate.util;

import android.os.Environment;
import com.google.api.translate.Language;
import java.io.File;

public class OnlineTTS {
    private static final String AUDIO_CACHE_FOLDER_PREFIX = "/Android/data/cn.org.dian.easycommunicate/audio_cache";
    private static final String URL_BASE = "http://translate.google.com/translate_tts";

    public static String getURL(Language language, String content) {
        return "http://translate.google.com/translate_tts?tl=" + language.toString() + "&q=" + Utilities.utf8Encode(content);
    }

    public static File getCachedAudioFile(Language language, String speakText) {
        if (!Utilities.checkSDCardUsability()) {
            return null;
        }
        File audioCacheFolder = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + AUDIO_CACHE_FOLDER_PREFIX + "/" + language.toString());
        if (!audioCacheFolder.exists()) {
            return null;
        }
        String legalFileName = Utilities.convertToLegalName(speakText);
        if (legalFileName.length() > Constants.FILE_NAME_LENGTH_MAX.intValue()) {
            return null;
        }
        File cacheFile = new File(audioCacheFolder, String.valueOf(legalFileName) + ".mp3");
        if (cacheFile.exists()) {
            return cacheFile;
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r8v8 java.io.FileOutputStream: [D('httpURLConn' java.net.HttpURLConnection), D('fos' java.io.FileOutputStream)] */
    /* JADX INFO: Multiple debug info for r7v9 java.lang.Exception: [D('e' java.lang.Exception), D('fos' java.io.FileOutputStream)] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00cd A[SYNTHETIC, Splitter:B:43:0x00cd] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00d3 A[SYNTHETIC, Splitter:B:47:0x00d3] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00d9 A[SYNTHETIC, Splitter:B:51:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00fa A[SYNTHETIC, Splitter:B:64:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0100 A[Catch:{ Exception -> 0x010b }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0106 A[Catch:{ Exception -> 0x010b }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x0145  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0149  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File requestWebAndSaveToFile(com.google.api.translate.Language r7, java.lang.String r8) {
        /*
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r1 = r1.getAbsolutePath()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r2.<init>(r1)
            java.lang.String r1 = "/Android/data/cn.org.dian.easycommunicate/audio_cache"
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r7.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 != 0) goto L_0x0037
            r0.mkdirs()
        L_0x0037:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = cn.org.dian.easycommunicate.util.Utilities.convertToLegalName(r8)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = ".mp3"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.io.File r2 = new java.io.File
            r2.<init>(r0, r1)
            r0 = 0
            java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00b6 }
            java.lang.String r7 = getURL(r7, r8)     // Catch:{ MalformedURLException -> 0x00b6 }
            r4.<init>(r7)     // Catch:{ MalformedURLException -> 0x00b6 }
            r7 = 0
            cn.org.dian.easycommunicate.util.NetworkTrafficStats.beginTrafficStats(r7)
            r8 = 0
            r1 = 0
            r7 = 0
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ Exception -> 0x012e, all -> 0x00f0 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x012e, all -> 0x00f0 }
            java.lang.String r8 = "GET"
            r0.setRequestMethod(r8)     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            java.lang.String r8 = "Content-Type"
            java.lang.String r3 = "text/xml; charset=UTF-8"
            r0.setRequestProperty(r8, r3)     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            java.lang.String r8 = "User-Agent"
            java.lang.String r3 = "Mozilla/5.0"
            r0.setRequestProperty(r8, r3)     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            r0.connect()     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            boolean r8 = r2.exists()     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            if (r8 != 0) goto L_0x008d
            r2.createNewFile()     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
        L_0x008d:
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            r8.<init>(r2)     // Catch:{ Exception -> 0x0134, all -> 0x0115 }
            r7 = 1024(0x400, float:1.435E-42)
            byte[] r7 = new byte[r7]     // Catch:{ Exception -> 0x00c3 }
            r3 = 0
        L_0x0097:
            r5 = -1
            int r3 = r1.read(r7)     // Catch:{ Exception -> 0x00c3 }
            if (r5 != r3) goto L_0x00be
            r7 = 0
            cn.org.dian.easycommunicate.util.NetworkTrafficStats.endTrafficStats(r7)
            if (r8 == 0) goto L_0x014e
            r8.close()     // Catch:{ Exception -> 0x00e0 }
            r7 = 0
        L_0x00a8:
            if (r1 == 0) goto L_0x014b
            r1.close()     // Catch:{ Exception -> 0x0139 }
            r8 = 0
        L_0x00ae:
            if (r0 == 0) goto L_0x00b3
            r0.disconnect()     // Catch:{ Exception -> 0x013f }
        L_0x00b3:
            r7 = r4
            r8 = r2
        L_0x00b5:
            return r8
        L_0x00b6:
            r7 = move-exception
            r7.printStackTrace()
            r7 = 0
            r8 = r7
            r7 = r0
            goto L_0x00b5
        L_0x00be:
            r5 = 0
            r8.write(r7, r5, r3)     // Catch:{ Exception -> 0x00c3 }
            goto L_0x0097
        L_0x00c3:
            r7 = move-exception
        L_0x00c4:
            r7.printStackTrace()     // Catch:{ all -> 0x011b }
            r7 = 0
            cn.org.dian.easycommunicate.util.NetworkTrafficStats.endTrafficStats(r7)
            if (r8 == 0) goto L_0x0149
            r8.close()     // Catch:{ Exception -> 0x00e8 }
            r7 = 0
        L_0x00d1:
            if (r1 == 0) goto L_0x0147
            r1.close()     // Catch:{ Exception -> 0x0122 }
            r8 = 0
        L_0x00d7:
            if (r0 == 0) goto L_0x00dc
            r0.disconnect()     // Catch:{ Exception -> 0x0128 }
        L_0x00dc:
            r7 = 0
            r8 = r7
            r7 = r4
            goto L_0x00b5
        L_0x00e0:
            r7 = move-exception
            r0 = r1
        L_0x00e2:
            r7.printStackTrace()
            r7 = r8
            r8 = r0
            goto L_0x00b3
        L_0x00e8:
            r7 = move-exception
            r0 = r1
        L_0x00ea:
            r7.printStackTrace()
            r7 = r8
            r8 = r0
            goto L_0x00dc
        L_0x00f0:
            r0 = move-exception
            r6 = r0
            r0 = r1
            r1 = r6
        L_0x00f4:
            r2 = 0
            cn.org.dian.easycommunicate.util.NetworkTrafficStats.endTrafficStats(r2)
            if (r7 == 0) goto L_0x00fe
            r7.close()     // Catch:{ Exception -> 0x010b }
            r7 = 0
        L_0x00fe:
            if (r0 == 0) goto L_0x0104
            r0.close()     // Catch:{ Exception -> 0x010b }
            r0 = 0
        L_0x0104:
            if (r8 == 0) goto L_0x0145
            r8.disconnect()     // Catch:{ Exception -> 0x010b }
            r8 = r0
        L_0x010a:
            throw r1
        L_0x010b:
            r8 = move-exception
            r6 = r8
            r8 = r7
            r7 = r6
            r7.printStackTrace()
            r7 = r8
            r8 = r0
            goto L_0x010a
        L_0x0115:
            r8 = move-exception
            r6 = r8
            r8 = r0
            r0 = r1
            r1 = r6
            goto L_0x00f4
        L_0x011b:
            r7 = move-exception
            r6 = r7
            r7 = r8
            r8 = r0
            r0 = r1
            r1 = r6
            goto L_0x00f4
        L_0x0122:
            r8 = move-exception
            r0 = r1
            r6 = r7
            r7 = r8
            r8 = r6
            goto L_0x00ea
        L_0x0128:
            r0 = move-exception
            r6 = r0
            r0 = r8
            r8 = r7
            r7 = r6
            goto L_0x00ea
        L_0x012e:
            r0 = move-exception
            r6 = r0
            r0 = r8
            r8 = r7
            r7 = r6
            goto L_0x00c4
        L_0x0134:
            r8 = move-exception
            r6 = r8
            r8 = r7
            r7 = r6
            goto L_0x00c4
        L_0x0139:
            r8 = move-exception
            r0 = r1
            r6 = r7
            r7 = r8
            r8 = r6
            goto L_0x00e2
        L_0x013f:
            r0 = move-exception
            r6 = r0
            r0 = r8
            r8 = r7
            r7 = r6
            goto L_0x00e2
        L_0x0145:
            r8 = r0
            goto L_0x010a
        L_0x0147:
            r8 = r1
            goto L_0x00d7
        L_0x0149:
            r7 = r8
            goto L_0x00d1
        L_0x014b:
            r8 = r1
            goto L_0x00ae
        L_0x014e:
            r7 = r8
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.org.dian.easycommunicate.util.OnlineTTS.requestWebAndSaveToFile(com.google.api.translate.Language, java.lang.String):java.io.File");
    }

    public static boolean clearCache() {
        if (!Utilities.checkSDCardUsability()) {
            return false;
        }
        File audioCacheFolder = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + AUDIO_CACHE_FOLDER_PREFIX);
        if (!audioCacheFolder.exists()) {
            return false;
        }
        return deleteAll(audioCacheFolder);
    }

    public static long getCacheSize() {
        if (!Utilities.checkSDCardUsability()) {
            return -1;
        }
        File audioCacheFolder = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + AUDIO_CACHE_FOLDER_PREFIX);
        if (!audioCacheFolder.exists()) {
            return 0;
        }
        return getSize(audioCacheFolder);
    }

    private static boolean deleteAll(File path) {
        boolean finalResult;
        if (!path.exists()) {
            return false;
        }
        if (path.isFile()) {
            return path.delete();
        }
        File[] files = path.listFiles();
        boolean finalResult2 = true;
        for (int i = 0; i < files.length; i++) {
            if (!finalResult2 || !deleteAll(files[i])) {
                finalResult2 = false;
            } else {
                finalResult2 = true;
            }
        }
        if (!finalResult2 || !path.delete()) {
            finalResult = false;
        } else {
            finalResult = true;
        }
        return finalResult;
    }

    private static long getSize(File path) {
        if (!path.exists()) {
            return 0;
        }
        if (path.isFile()) {
            return path.length();
        }
        File[] files = path.listFiles();
        long totalSize = 0;
        for (File size : files) {
            totalSize += getSize(size);
        }
        return totalSize;
    }
}
