package cn.org.dian.easycommunicate.util;

import android.util.Log;
import com.google.api.translate.Language;
import com.google.api.translate.Translate;

public class GoogleTranslateService {
    private static final String HTTP_REFERRER = "www.dian.org.cn";
    private static final String TAG = "GoogleTranslateService";

    static {
        Translate.setHttpReferrer(HTTP_REFERRER);
    }

    public static String translate(String target, Language fromLanguage, Language toLanguage) throws Exception {
        String result = Translate.execute(target, fromLanguage, toLanguage);
        Log.d(TAG, "result: " + result);
        return result;
    }
}
