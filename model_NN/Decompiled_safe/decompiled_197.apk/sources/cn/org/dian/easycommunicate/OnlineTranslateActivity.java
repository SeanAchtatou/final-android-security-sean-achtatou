package cn.org.dian.easycommunicate;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import cn.org.dian.easycommunicate.util.GoogleTranslateService;
import cn.org.dian.easycommunicate.util.NetworkTrafficStats;
import cn.org.dian.easycommunicate.util.Utilities;
import com.google.api.translate.Language;
import java.util.ArrayList;

public class OnlineTranslateActivity extends TTSActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    private static final int VOICE_RECOGNITION_REQUEST_CODE = 1;
    private static ArrayList<Language> languageIndexMapper = new ArrayList<>();
    private Button buttonVoiceInput = null;
    private int fromSpinnerPosition;
    /* access modifiers changed from: private */
    public EditText onlineSearchInput = null;
    private EditText onlineSearchResult = null;
    private int toSpinnerPosition;
    private boolean voiceInputAvailability = false;
    /* access modifiers changed from: private */
    public ArrayList<String> voiceRecoResultList = null;
    /* access modifiers changed from: private */
    public int voiceRecoResultPickIndex;

    static {
        languageIndexMapper.add(Language.CHINESE_SIMPLIFIED);
        languageIndexMapper.add(Language.CHINESE_TRADITIONAL);
        languageIndexMapper.add(Language.ENGLISH);
        languageIndexMapper.add(Language.FRENCH);
        languageIndexMapper.add(Language.GERMAN);
        languageIndexMapper.add(Language.ITALIAN);
        languageIndexMapper.add(Language.SPANISH);
        languageIndexMapper.add(Language.JAPANESE);
        languageIndexMapper.add(Language.KOREAN);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int i;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.online_translate_activity);
        ArrayAdapter<String> fromLanguageAdapter = new ArrayAdapter<>(this, 17367048, getResources().getStringArray(R.array.online_search_languages));
        fromLanguageAdapter.setDropDownViewResource(17367049);
        Spinner fromLanguageSpinner = (Spinner) findViewById(R.id.from_language_select_spinner);
        fromLanguageSpinner.setAdapter((SpinnerAdapter) fromLanguageAdapter);
        fromLanguageSpinner.setOnItemSelectedListener(this);
        ArrayAdapter<String> toLanguageAdapter = new ArrayAdapter<>(this, 17367048, getResources().getStringArray(R.array.online_search_languages));
        toLanguageAdapter.setDropDownViewResource(17367049);
        Spinner toLanguageSpinner = (Spinner) findViewById(R.id.to_language_select_spinner);
        toLanguageSpinner.setAdapter((SpinnerAdapter) toLanguageAdapter);
        toLanguageSpinner.setOnItemSelectedListener(this);
        boolean isEng = Utilities.isCurrentLanEng();
        fromLanguageSpinner.setSelection(isEng ? 2 : 0);
        if (isEng) {
            i = 0;
        } else {
            i = 2;
        }
        toLanguageSpinner.setSelection(i);
        this.onlineSearchInput = (EditText) findViewById(R.id.online_search_input);
        this.onlineSearchResult = (EditText) findViewById(R.id.online_search_result);
        findViewById(R.id.online_search_button).setOnClickListener(this);
        findViewById(R.id.from_lang_speak_button).setOnClickListener(this);
        findViewById(R.id.to_lang_speak_button).setOnClickListener(this);
        this.buttonVoiceInput = (Button) findViewById(R.id.voice_input_button);
        if (!checkVoiceInputUsability()) {
            this.buttonVoiceInput.setVisibility(4);
            this.voiceInputAvailability = false;
            return;
        }
        this.buttonVoiceInput.setOnClickListener(this);
        this.voiceInputAvailability = true;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!Utilities.checkNetworkUsability()) {
            Utilities.showLongToast(this, (int) R.string.network_error);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            NetworkTrafficStats.endTrafficStats(true);
            if (resultCode == -1) {
                this.voiceRecoResultList = data.getStringArrayListExtra("android.speech.extra.RESULTS");
                ArrayAdapter<String> matchesAdapter = new ArrayAdapter<>(this, (int) R.layout.custom_list_item_single_choice, this.voiceRecoResultList);
                String alertMessage = getResources().getString(R.string.voice_reco_result_choose_msg);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(alertMessage).setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OnlineTranslateActivity.this.onlineSearchInput.setText((CharSequence) OnlineTranslateActivity.this.voiceRecoResultList.get(OnlineTranslateActivity.this.voiceRecoResultPickIndex));
                        OnlineTranslateActivity.this.voiceRecoResultPickIndex = 0;
                    }
                }).setNegativeButton((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OnlineTranslateActivity.this.voiceRecoResultPickIndex = 0;
                        dialog.cancel();
                    }
                }).setSingleChoiceItems(matchesAdapter, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OnlineTranslateActivity.this.voiceRecoResultPickIndex = which;
                    }
                }).setNeutralButton((int) R.string.speak_again, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        OnlineTranslateActivity.this.voiceRecoResultPickIndex = 0;
                        OnlineTranslateActivity.this.requestVoiceReco();
                    }
                });
                builder.create().show();
            } else {
                Utilities.showLongToast(getContext(), (int) R.string.voice_reco_fail);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.from_language_select_spinner) {
            this.fromSpinnerPosition = position;
            if (getExtraLanguage() == null || !this.voiceInputAvailability) {
                this.buttonVoiceInput.setVisibility(4);
            } else {
                this.buttonVoiceInput.setVisibility(0);
            }
        } else if (parent.getId() == R.id.to_language_select_spinner) {
            this.toSpinnerPosition = position;
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    public void onClick(View v) {
        if (v.getId() == R.id.online_search_button) {
            if (!Utilities.checkNetworkUsability()) {
                Utilities.showLongToast(this, (int) R.string.network_error);
            } else if (verifyEmptyEditText(this.onlineSearchInput)) {
                String query = this.onlineSearchInput.getText().toString().trim();
                new AsyncTranslateTask().execute(query, languageIndexMapper.get(this.fromSpinnerPosition), languageIndexMapper.get(this.toSpinnerPosition));
            }
        } else if (v.getId() == R.id.from_lang_speak_button) {
            if (verifyEmptyEditText(this.onlineSearchInput)) {
                asyncSpeak(languageIndexMapper.get(this.fromSpinnerPosition), this.onlineSearchInput.getText().toString().trim());
            }
        } else if (v.getId() == R.id.to_lang_speak_button) {
            if (verifyEmptyEditText(this.onlineSearchResult)) {
                asyncSpeak(languageIndexMapper.get(this.toSpinnerPosition), this.onlineSearchResult.getText().toString().trim());
            }
        } else if (v.getId() != R.id.voice_input_button) {
        } else {
            if (Utilities.checkNetworkUsability()) {
                requestVoiceReco();
            } else {
                Utilities.showLongToast(this, (int) R.string.voice_reco_fail);
            }
        }
    }

    /* access modifiers changed from: private */
    public void requestVoiceReco() {
        Intent intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
        intent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
        intent.putExtra("android.speech.extra.PROMPT", getResources().getString(R.string.voice_input_prompt));
        intent.putExtra("android.speech.extra.LANGUAGE", getExtraLanguage());
        NetworkTrafficStats.beginTrafficStats(true);
        startActivityForResult(intent, 1);
    }

    private boolean verifyEmptyEditText(EditText editText) {
        if (!"".equals(editText.getText().toString().trim())) {
            return true;
        }
        String alertMessage = getResources().getString(R.string.empty_alert_text);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(alertMessage).setPositiveButton((int) R.string.confirm, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
        return false;
    }

    private boolean checkVoiceInputUsability() {
        if (getPackageManager().queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), 0).size() != 0) {
            return true;
        }
        return false;
    }

    private String getExtraLanguage() {
        Language language = languageIndexMapper.get(this.fromSpinnerPosition);
        if (language.equals(Language.CHINESE_SIMPLIFIED)) {
            return "zh-CN";
        }
        if (language.equals(Language.ENGLISH)) {
            return "en-US";
        }
        return null;
    }

    /* access modifiers changed from: private */
    public Context getContext() {
        return this;
    }

    class AsyncTranslateTask extends AsyncTask<Object, Void, String> {
        AsyncTranslateTask() {
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            try {
                NetworkTrafficStats.beginTrafficStats(false);
                String translate = GoogleTranslateService.translate((String) params[0], (Language) params[1], (Language) params[2]);
                NetworkTrafficStats.endTrafficStats(false);
                return translate;
            } catch (Exception e) {
                e.printStackTrace();
                NetworkTrafficStats.endTrafficStats(false);
                return null;
            } catch (Throwable th) {
                NetworkTrafficStats.endTrafficStats(false);
                throw th;
            }
        }

        /* Debug info: failed to restart local var, previous not found, register: 2 */
        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            super.onPostExecute((Object) result);
            OnlineTranslateActivity.this.getParent().setProgressBarIndeterminateVisibility(false);
            if (result != null) {
                ((TextView) OnlineTranslateActivity.this.findViewById(R.id.online_search_result)).setText(result);
            } else {
                Utilities.showShortToast(OnlineTranslateActivity.this.getContext(), (int) R.string.remote_service_error);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            OnlineTranslateActivity.this.getParent().setProgressBarIndeterminateVisibility(true);
            Utilities.showShortToast(OnlineTranslateActivity.this.getContext(), (int) R.string.translate_prepare);
        }
    }

    public void onBackPressed() {
        Activity parent = getParent();
        if (parent != null) {
            parent.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}
