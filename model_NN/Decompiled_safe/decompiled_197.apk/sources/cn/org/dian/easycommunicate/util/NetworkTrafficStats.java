package cn.org.dian.easycommunicate.util;

import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Process;
import android.util.Log;

public class NetworkTrafficStats {
    private static final String TAG = "NetworkTrafficStats";
    private static boolean availbility;
    private static long beginRxBytes;
    private static long beginTxBytes;
    private static boolean isCounting = false;
    private static int myUid = Process.myUid();

    static {
        availbility = false;
        if (TrafficStats.getMobileRxBytes() == -1) {
            availbility = false;
        } else {
            availbility = true;
        }
    }

    public static void beginTrafficStats(boolean isGlobal) {
        if (availbility) {
            NetworkInfo networkInfo = Utilities.getConnectivityManager().getActiveNetworkInfo();
            if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
                isCounting = false;
            } else if (networkInfo.getType() == 0) {
                isCounting = true;
                if (isGlobal) {
                    beginRxBytes = TrafficStats.getMobileRxBytes();
                    beginTxBytes = TrafficStats.getMobileTxBytes();
                    return;
                }
                beginRxBytes = TrafficStats.getUidRxBytes(myUid);
                beginTxBytes = TrafficStats.getUidTxBytes(myUid);
            } else {
                isCounting = false;
            }
        }
    }

    public static void endTrafficStats(boolean isGlobal) {
        long endRxBytes;
        long endTxBytes;
        if (availbility && isCounting) {
            isCounting = false;
            if (isGlobal) {
                endRxBytes = TrafficStats.getMobileRxBytes();
                endTxBytes = TrafficStats.getMobileTxBytes();
            } else {
                endRxBytes = TrafficStats.getUidRxBytes(myUid);
                endTxBytes = TrafficStats.getUidTxBytes(myUid);
            }
            long totalRxTxBytes = ((endRxBytes - beginRxBytes) + endTxBytes) - beginTxBytes;
            Log.d(TAG, "Traffic stats in bytes: " + totalRxTxBytes);
            SharedPreferences sp = Utilities.getSharedPreferences();
            sp.edit().putLong(Constants.PREF_KEY_MOBILE_NETWORK_STATS, sp.getLong(Constants.PREF_KEY_MOBILE_NETWORK_STATS, 0) + totalRxTxBytes).commit();
        }
    }

    public static boolean getAvailbility() {
        return availbility;
    }
}
