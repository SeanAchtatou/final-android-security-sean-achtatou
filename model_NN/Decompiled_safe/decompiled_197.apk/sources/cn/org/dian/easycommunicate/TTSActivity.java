package cn.org.dian.easycommunicate;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import cn.org.dian.easycommunicate.util.Constants;
import cn.org.dian.easycommunicate.util.NetworkTrafficStats;
import cn.org.dian.easycommunicate.util.OnlineTTS;
import cn.org.dian.easycommunicate.util.Utilities;
import com.google.api.translate.Language;
import java.io.File;
import java.io.IOException;

public class TTSActivity extends Activity {
    private static final String TAG = "TTSActivity";
    private MediaPlayer mediaPlayer = null;

    /* access modifiers changed from: protected */
    public void asyncSpeak(Language language, String speakText) {
        new AsyncSpeakTask().execute(language, speakText);
    }

    /* access modifiers changed from: protected */
    public boolean speak(Language language, String speakText) {
        File audioCacheFile = OnlineTTS.getCachedAudioFile(language, speakText);
        if (audioCacheFile != null) {
            return playFromAudioFile(audioCacheFile);
        }
        if (!Utilities.checkNetworkUsability()) {
            return false;
        }
        String legalFileName = Utilities.convertToLegalName(speakText);
        if (!Utilities.checkSDCardUsability() || legalFileName.length() > Constants.FILE_NAME_LENGTH_MAX.intValue()) {
            return streamSpeakOnline(language, speakText);
        }
        File audioCacheFile2 = OnlineTTS.requestWebAndSaveToFile(language, speakText);
        if (audioCacheFile2 == null || !audioCacheFile2.exists()) {
            return false;
        }
        return playFromAudioFile(audioCacheFile2);
    }

    private boolean streamSpeakOnline(Language language, String speakText) {
        String URL = OnlineTTS.getURL(language, speakText);
        this.mediaPlayer = new MediaPlayer();
        try {
            this.mediaPlayer.setDataSource(URL);
            NetworkTrafficStats.beginTrafficStats(true);
            this.mediaPlayer.prepare();
            this.mediaPlayer.start();
            this.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    NetworkTrafficStats.endTrafficStats(true);
                    mp.release();
                }
            });
            return true;
        } catch (IllegalArgumentException e) {
            NetworkTrafficStats.endTrafficStats(true);
            e.printStackTrace();
            return false;
        } catch (IllegalStateException e2) {
            NetworkTrafficStats.endTrafficStats(true);
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            NetworkTrafficStats.endTrafficStats(true);
            e3.printStackTrace();
            return false;
        }
    }

    private boolean playFromAudioFile(File audioFile) {
        this.mediaPlayer = new MediaPlayer();
        try {
            this.mediaPlayer.setDataSource(audioFile.getAbsolutePath());
            this.mediaPlayer.prepare();
            this.mediaPlayer.start();
            this.mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            return true;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public Activity getContextActivity() {
        return getParent() == null ? this : getParent();
    }

    class AsyncSpeakTask extends AsyncTask<Object, Void, Boolean> {
        AsyncSpeakTask() {
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            TTSActivity.this.getContextActivity().setProgressBarIndeterminateVisibility(true);
            Utilities.showShortToast(TTSActivity.this.getContextActivity(), (int) R.string.tts_prepare);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Object... params) {
            return Boolean.valueOf(TTSActivity.this.speak((Language) params[0], (String) params[1]));
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            super.onPostExecute((Object) result);
            TTSActivity.this.getContextActivity().setProgressBarIndeterminateVisibility(false);
            if (!result.booleanValue()) {
                Utilities.showShortToast(TTSActivity.this.getContextActivity(), (int) R.string.tts_fail);
            }
        }
    }
}
