package cn.org.dian.easycommunicate.model;

import android.util.Log;
import cn.org.dian.easycommunicate.xml.XmlParser;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataCenter {
    private static final String TAG = "DataCenter";
    private static List<String> allTermNames = new ArrayList();
    private static Content content = null;
    public static boolean hasNotifiedTTSNotSupport = false;

    public static boolean init(InputStream is) {
        if (content != null) {
            return true;
        }
        if (is != null) {
            Log.d(TAG, "load xml success");
            content = XmlParser.readContentfromXML(is);
            if (content != null) {
                return true;
            }
            return false;
        }
        Log.d(TAG, "load xml fail");
        return false;
    }

    private DataCenter() {
    }

    public static String[] getAllSupportLanguages() {
        return (String[]) Term.getAllLanguageNames().toArray(new String[0]);
    }

    public static String[] getAllThemeName() {
        return (String[]) Theme.getAllThemeNames().toArray(new String[0]);
    }

    public static Theme getTheme(int themeIndicator) {
        return content.getAllThemes().get(Theme.getAllThemeNames().get(themeIndicator));
    }

    public static List<String> searchTerm(String query) {
        List<String> resultList = new ArrayList<>();
        for (String termName : allTermNames) {
            if (termName.contains(query)) {
                resultList.add(termName);
            }
        }
        return resultList;
    }

    public static void addAllTermName(String termName) {
        allTermNames.add(termName);
    }

    public static Term getTerm(String termName) {
        HashMap<String, Theme> allTheme = content.getAllThemes();
        for (String themeName : content.getAllThemes().keySet()) {
            if (allTheme.get(themeName).getAllTermsUnderTheme().containsKey(termName)) {
                return allTheme.get(themeName).getAllTermsUnderTheme().get(termName);
            }
        }
        return null;
    }
}
