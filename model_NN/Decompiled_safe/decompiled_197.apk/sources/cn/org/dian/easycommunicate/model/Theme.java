package cn.org.dian.easycommunicate.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class Theme {
    private static List<String> allThemeNames = new ArrayList();
    private HashMap<String, Term> allTermsUnderTheme;
    private String name;

    public Theme() {
        this.name = null;
        this.allTermsUnderTheme = null;
        this.allTermsUnderTheme = new LinkedHashMap();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void addTerm(String termName, Term term) {
        this.allTermsUnderTheme.put(termName, term);
        DataCenter.addAllTermName(termName);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("--Theme: " + this.name + "\n");
        for (String termName : this.allTermsUnderTheme.keySet()) {
            sb.append(this.allTermsUnderTheme.get(termName).toString());
        }
        sb.append("--Theme end\n");
        return new String(sb);
    }

    public HashMap<String, Term> getAllTermsUnderTheme() {
        return this.allTermsUnderTheme;
    }

    public static void addThemeName(String themeName) {
        allThemeNames.add(themeName);
    }

    public static List<String> getAllThemeNames() {
        return allThemeNames;
    }
}
