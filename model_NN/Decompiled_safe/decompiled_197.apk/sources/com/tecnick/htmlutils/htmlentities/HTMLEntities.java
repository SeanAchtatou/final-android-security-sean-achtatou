package com.tecnick.htmlutils.htmlentities;

import java.util.Hashtable;

public class HTMLEntities {
    private static final Object[][] html_entities_table = {new Object[]{new String("&Aacute;"), new Integer(193)}, new Object[]{new String("&aacute;"), new Integer(225)}, new Object[]{new String("&Acirc;"), new Integer(194)}, new Object[]{new String("&acirc;"), new Integer(226)}, new Object[]{new String("&acute;"), new Integer(180)}, new Object[]{new String("&AElig;"), new Integer(198)}, new Object[]{new String("&aelig;"), new Integer(230)}, new Object[]{new String("&Agrave;"), new Integer(192)}, new Object[]{new String("&agrave;"), new Integer(224)}, new Object[]{new String("&alefsym;"), new Integer(8501)}, new Object[]{new String("&Alpha;"), new Integer(913)}, new Object[]{new String("&alpha;"), new Integer(945)}, new Object[]{new String("&amp;"), new Integer(38)}, new Object[]{new String("&and;"), new Integer(8743)}, new Object[]{new String("&ang;"), new Integer(8736)}, new Object[]{new String("&Aring;"), new Integer(197)}, new Object[]{new String("&aring;"), new Integer(229)}, new Object[]{new String("&asymp;"), new Integer(8776)}, new Object[]{new String("&Atilde;"), new Integer(195)}, new Object[]{new String("&atilde;"), new Integer(227)}, new Object[]{new String("&Auml;"), new Integer(196)}, new Object[]{new String("&auml;"), new Integer(228)}, new Object[]{new String("&bdquo;"), new Integer(8222)}, new Object[]{new String("&Beta;"), new Integer(914)}, new Object[]{new String("&beta;"), new Integer(946)}, new Object[]{new String("&brvbar;"), new Integer(166)}, new Object[]{new String("&bull;"), new Integer(8226)}, new Object[]{new String("&cap;"), new Integer(8745)}, new Object[]{new String("&Ccedil;"), new Integer(199)}, new Object[]{new String("&ccedil;"), new Integer(231)}, new Object[]{new String("&cedil;"), new Integer(184)}, new Object[]{new String("&cent;"), new Integer(162)}, new Object[]{new String("&Chi;"), new Integer(935)}, new Object[]{new String("&chi;"), new Integer(967)}, new Object[]{new String("&circ;"), new Integer(710)}, new Object[]{new String("&clubs;"), new Integer(9827)}, new Object[]{new String("&cong;"), new Integer(8773)}, new Object[]{new String("&copy;"), new Integer(169)}, new Object[]{new String("&crarr;"), new Integer(8629)}, new Object[]{new String("&cup;"), new Integer(8746)}, new Object[]{new String("&curren;"), new Integer(164)}, new Object[]{new String("&dagger;"), new Integer(8224)}, new Object[]{new String("&Dagger;"), new Integer(8225)}, new Object[]{new String("&darr;"), new Integer(8595)}, new Object[]{new String("&dArr;"), new Integer(8659)}, new Object[]{new String("&deg;"), new Integer(176)}, new Object[]{new String("&Delta;"), new Integer(916)}, new Object[]{new String("&delta;"), new Integer(948)}, new Object[]{new String("&diams;"), new Integer(9830)}, new Object[]{new String("&divide;"), new Integer(247)}, new Object[]{new String("&Eacute;"), new Integer(201)}, new Object[]{new String("&eacute;"), new Integer(233)}, new Object[]{new String("&Ecirc;"), new Integer(202)}, new Object[]{new String("&ecirc;"), new Integer(234)}, new Object[]{new String("&Egrave;"), new Integer(200)}, new Object[]{new String("&egrave;"), new Integer(232)}, new Object[]{new String("&empty;"), new Integer(8709)}, new Object[]{new String("&emsp;"), new Integer(8195)}, new Object[]{new String("&ensp;"), new Integer(8194)}, new Object[]{new String("&Epsilon;"), new Integer(917)}, new Object[]{new String("&epsilon;"), new Integer(949)}, new Object[]{new String("&equiv;"), new Integer(8801)}, new Object[]{new String("&Eta;"), new Integer(919)}, new Object[]{new String("&eta;"), new Integer(951)}, new Object[]{new String("&ETH;"), new Integer(208)}, new Object[]{new String("&eth;"), new Integer(240)}, new Object[]{new String("&Euml;"), new Integer(203)}, new Object[]{new String("&euml;"), new Integer(235)}, new Object[]{new String("&euro;"), new Integer(8364)}, new Object[]{new String("&exist;"), new Integer(8707)}, new Object[]{new String("&fnof;"), new Integer(402)}, new Object[]{new String("&forall;"), new Integer(8704)}, new Object[]{new String("&frac12;"), new Integer(189)}, new Object[]{new String("&frac14;"), new Integer(188)}, new Object[]{new String("&frac34;"), new Integer(190)}, new Object[]{new String("&frasl;"), new Integer(8260)}, new Object[]{new String("&Gamma;"), new Integer(915)}, new Object[]{new String("&gamma;"), new Integer(947)}, new Object[]{new String("&ge;"), new Integer(8805)}, new Object[]{new String("&harr;"), new Integer(8596)}, new Object[]{new String("&hArr;"), new Integer(8660)}, new Object[]{new String("&hearts;"), new Integer(9829)}, new Object[]{new String("&hellip;"), new Integer(8230)}, new Object[]{new String("&Iacute;"), new Integer(205)}, new Object[]{new String("&iacute;"), new Integer(237)}, new Object[]{new String("&Icirc;"), new Integer(206)}, new Object[]{new String("&icirc;"), new Integer(238)}, new Object[]{new String("&iexcl;"), new Integer(161)}, new Object[]{new String("&Igrave;"), new Integer(204)}, new Object[]{new String("&igrave;"), new Integer(236)}, new Object[]{new String("&image;"), new Integer(8465)}, new Object[]{new String("&infin;"), new Integer(8734)}, new Object[]{new String("&int;"), new Integer(8747)}, new Object[]{new String("&Iota;"), new Integer(921)}, new Object[]{new String("&iota;"), new Integer(953)}, new Object[]{new String("&iquest;"), new Integer(191)}, new Object[]{new String("&isin;"), new Integer(8712)}, new Object[]{new String("&Iuml;"), new Integer(207)}, new Object[]{new String("&iuml;"), new Integer(239)}, new Object[]{new String("&Kappa;"), new Integer(922)}, new Object[]{new String("&kappa;"), new Integer(954)}, new Object[]{new String("&Lambda;"), new Integer(923)}, new Object[]{new String("&lambda;"), new Integer(955)}, new Object[]{new String("&lang;"), new Integer(9001)}, new Object[]{new String("&laquo;"), new Integer(171)}, new Object[]{new String("&larr;"), new Integer(8592)}, new Object[]{new String("&lArr;"), new Integer(8656)}, new Object[]{new String("&lceil;"), new Integer(8968)}, new Object[]{new String("&ldquo;"), new Integer(8220)}, new Object[]{new String("&le;"), new Integer(8804)}, new Object[]{new String("&lfloor;"), new Integer(8970)}, new Object[]{new String("&lowast;"), new Integer(8727)}, new Object[]{new String("&loz;"), new Integer(9674)}, new Object[]{new String("&lrm;"), new Integer(8206)}, new Object[]{new String("&lsaquo;"), new Integer(8249)}, new Object[]{new String("&lsquo;"), new Integer(8216)}, new Object[]{new String("&macr;"), new Integer(175)}, new Object[]{new String("&mdash;"), new Integer(8212)}, new Object[]{new String("&micro;"), new Integer(181)}, new Object[]{new String("&middot;"), new Integer(183)}, new Object[]{new String("&minus;"), new Integer(8722)}, new Object[]{new String("&Mu;"), new Integer(924)}, new Object[]{new String("&mu;"), new Integer(956)}, new Object[]{new String("&nabla;"), new Integer(8711)}, new Object[]{new String("&nbsp;"), new Integer(160)}, new Object[]{new String("&ndash;"), new Integer(8211)}, new Object[]{new String("&ne;"), new Integer(8800)}, new Object[]{new String("&ni;"), new Integer(8715)}, new Object[]{new String("&not;"), new Integer(172)}, new Object[]{new String("&notin;"), new Integer(8713)}, new Object[]{new String("&nsub;"), new Integer(8836)}, new Object[]{new String("&Ntilde;"), new Integer(209)}, new Object[]{new String("&ntilde;"), new Integer(241)}, new Object[]{new String("&Nu;"), new Integer(925)}, new Object[]{new String("&nu;"), new Integer(957)}, new Object[]{new String("&Oacute;"), new Integer(211)}, new Object[]{new String("&oacute;"), new Integer(243)}, new Object[]{new String("&Ocirc;"), new Integer(212)}, new Object[]{new String("&ocirc;"), new Integer(244)}, new Object[]{new String("&OElig;"), new Integer(338)}, new Object[]{new String("&oelig;"), new Integer(339)}, new Object[]{new String("&Ograve;"), new Integer(210)}, new Object[]{new String("&ograve;"), new Integer(242)}, new Object[]{new String("&oline;"), new Integer(8254)}, new Object[]{new String("&Omega;"), new Integer(937)}, new Object[]{new String("&omega;"), new Integer(969)}, new Object[]{new String("&Omicron;"), new Integer(927)}, new Object[]{new String("&omicron;"), new Integer(959)}, new Object[]{new String("&oplus;"), new Integer(8853)}, new Object[]{new String("&or;"), new Integer(8744)}, new Object[]{new String("&ordf;"), new Integer(170)}, new Object[]{new String("&ordm;"), new Integer(186)}, new Object[]{new String("&Oslash;"), new Integer(216)}, new Object[]{new String("&oslash;"), new Integer(248)}, new Object[]{new String("&Otilde;"), new Integer(213)}, new Object[]{new String("&otilde;"), new Integer(245)}, new Object[]{new String("&otimes;"), new Integer(8855)}, new Object[]{new String("&Ouml;"), new Integer(214)}, new Object[]{new String("&ouml;"), new Integer(246)}, new Object[]{new String("&para;"), new Integer(182)}, new Object[]{new String("&part;"), new Integer(8706)}, new Object[]{new String("&permil;"), new Integer(8240)}, new Object[]{new String("&perp;"), new Integer(8869)}, new Object[]{new String("&Phi;"), new Integer(934)}, new Object[]{new String("&phi;"), new Integer(966)}, new Object[]{new String("&Pi;"), new Integer(928)}, new Object[]{new String("&pi;"), new Integer(960)}, new Object[]{new String("&piv;"), new Integer(982)}, new Object[]{new String("&plusmn;"), new Integer(177)}, new Object[]{new String("&pound;"), new Integer(163)}, new Object[]{new String("&prime;"), new Integer(8242)}, new Object[]{new String("&Prime;"), new Integer(8243)}, new Object[]{new String("&prod;"), new Integer(8719)}, new Object[]{new String("&prop;"), new Integer(8733)}, new Object[]{new String("&Psi;"), new Integer(936)}, new Object[]{new String("&psi;"), new Integer(968)}, new Object[]{new String("&quot;"), new Integer(34)}, new Object[]{new String("&radic;"), new Integer(8730)}, new Object[]{new String("&rang;"), new Integer(9002)}, new Object[]{new String("&raquo;"), new Integer(187)}, new Object[]{new String("&rarr;"), new Integer(8594)}, new Object[]{new String("&rArr;"), new Integer(8658)}, new Object[]{new String("&rceil;"), new Integer(8969)}, new Object[]{new String("&rdquo;"), new Integer(8221)}, new Object[]{new String("&real;"), new Integer(8476)}, new Object[]{new String("&reg;"), new Integer(174)}, new Object[]{new String("&rfloor;"), new Integer(8971)}, new Object[]{new String("&Rho;"), new Integer(929)}, new Object[]{new String("&rho;"), new Integer(961)}, new Object[]{new String("&rlm;"), new Integer(8207)}, new Object[]{new String("&rsaquo;"), new Integer(8250)}, new Object[]{new String("&rsquo;"), new Integer(8217)}, new Object[]{new String("&sbquo;"), new Integer(8218)}, new Object[]{new String("&Scaron;"), new Integer(352)}, new Object[]{new String("&scaron;"), new Integer(353)}, new Object[]{new String("&sdot;"), new Integer(8901)}, new Object[]{new String("&sect;"), new Integer(167)}, new Object[]{new String("&shy;"), new Integer(173)}, new Object[]{new String("&Sigma;"), new Integer(931)}, new Object[]{new String("&sigma;"), new Integer(963)}, new Object[]{new String("&sigmaf;"), new Integer(962)}, new Object[]{new String("&sim;"), new Integer(8764)}, new Object[]{new String("&spades;"), new Integer(9824)}, new Object[]{new String("&sub;"), new Integer(8834)}, new Object[]{new String("&sube;"), new Integer(8838)}, new Object[]{new String("&sum;"), new Integer(8721)}, new Object[]{new String("&sup1;"), new Integer(185)}, new Object[]{new String("&sup2;"), new Integer(178)}, new Object[]{new String("&sup3;"), new Integer(179)}, new Object[]{new String("&sup;"), new Integer(8835)}, new Object[]{new String("&supe;"), new Integer(8839)}, new Object[]{new String("&szlig;"), new Integer(223)}, new Object[]{new String("&Tau;"), new Integer(932)}, new Object[]{new String("&tau;"), new Integer(964)}, new Object[]{new String("&there4;"), new Integer(8756)}, new Object[]{new String("&Theta;"), new Integer(920)}, new Object[]{new String("&theta;"), new Integer(952)}, new Object[]{new String("&thetasym;"), new Integer(977)}, new Object[]{new String("&thinsp;"), new Integer(8201)}, new Object[]{new String("&THORN;"), new Integer(222)}, new Object[]{new String("&thorn;"), new Integer(254)}, new Object[]{new String("&tilde;"), new Integer(732)}, new Object[]{new String("&times;"), new Integer(215)}, new Object[]{new String("&trade;"), new Integer(8482)}, new Object[]{new String("&Uacute;"), new Integer(218)}, new Object[]{new String("&uacute;"), new Integer(250)}, new Object[]{new String("&uarr;"), new Integer(8593)}, new Object[]{new String("&uArr;"), new Integer(8657)}, new Object[]{new String("&Ucirc;"), new Integer(219)}, new Object[]{new String("&ucirc;"), new Integer(251)}, new Object[]{new String("&Ugrave;"), new Integer(217)}, new Object[]{new String("&ugrave;"), new Integer(249)}, new Object[]{new String("&uml;"), new Integer(168)}, new Object[]{new String("&upsih;"), new Integer(978)}, new Object[]{new String("&Upsilon;"), new Integer(933)}, new Object[]{new String("&upsilon;"), new Integer(965)}, new Object[]{new String("&Uuml;"), new Integer(220)}, new Object[]{new String("&uuml;"), new Integer(252)}, new Object[]{new String("&weierp;"), new Integer(8472)}, new Object[]{new String("&Xi;"), new Integer(926)}, new Object[]{new String("&xi;"), new Integer(958)}, new Object[]{new String("&Yacute;"), new Integer(221)}, new Object[]{new String("&yacute;"), new Integer(253)}, new Object[]{new String("&yen;"), new Integer(165)}, new Object[]{new String("&yuml;"), new Integer(255)}, new Object[]{new String("&Yuml;"), new Integer(376)}, new Object[]{new String("&Zeta;"), new Integer(918)}, new Object[]{new String("&zeta;"), new Integer(950)}, new Object[]{new String("&zwj;"), new Integer(8205)}, new Object[]{new String("&zwnj;"), new Integer(8204)}};
    private static final Hashtable htmlentities_map = new Hashtable();
    private static final Hashtable unhtmlentities_map = new Hashtable();

    public HTMLEntities() {
        initializeEntitiesTables();
    }

    private static void initializeEntitiesTables() {
        for (int i = 0; i < html_entities_table.length; i++) {
            htmlentities_map.put(html_entities_table[i][1], html_entities_table[i][0]);
            unhtmlentities_map.put(html_entities_table[i][0], html_entities_table[i][1]);
        }
    }

    public static Object[][] getEntitiesTable() {
        return html_entities_table;
    }

    public static String htmlentities(String str) {
        if (str == null) {
            return "";
        }
        if (htmlentities_map.isEmpty()) {
            initializeEntitiesTables();
        }
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            String entity = (String) htmlentities_map.get(new Integer(ch));
            if (entity != null) {
                buf.append(entity);
            } else if (ch > 128) {
                buf.append("&#" + ((int) ch) + ";");
            } else {
                buf.append(ch);
            }
        }
        return buf.toString();
    }

    public static String unhtmlentities(String str) {
        Integer iso;
        if (htmlentities_map.isEmpty()) {
            initializeEntitiesTables();
        }
        StringBuffer buf = new StringBuffer();
        int i = 0;
        while (i < str.length()) {
            char ch = str.charAt(i);
            if (ch == '&') {
                int semi = str.indexOf(59, i + 1);
                if (semi == -1 || semi - i > 7) {
                    buf.append(ch);
                } else {
                    String entity = str.substring(i, semi + 1);
                    if (entity.charAt(1) == ' ') {
                        buf.append(ch);
                    } else {
                        if (entity.charAt(1) != '#') {
                            iso = (Integer) unhtmlentities_map.get(entity);
                        } else if (entity.charAt(2) == 'x') {
                            iso = new Integer(Integer.parseInt(entity.substring(3, entity.length() - 1), 16));
                        } else {
                            iso = new Integer(entity.substring(2, entity.length() - 1));
                        }
                        if (iso == null) {
                            buf.append(entity);
                        } else {
                            buf.append((char) iso.intValue());
                        }
                        i = semi;
                    }
                }
            } else {
                buf.append(ch);
            }
            i++;
        }
        return buf.toString();
    }

    public static String htmlSingleQuotes(String str) {
        return str.replaceAll("[']", "&rsquo;").replaceAll("&#039;", "&rsquo;").replaceAll("&#145;", "&rsquo;").replaceAll("&#146;", "&rsquo;");
    }

    public static String unhtmlSingleQuotes(String str) {
        return str.replaceAll("&rsquo;", "'");
    }

    public static String htmlDoubleQuotes(String str) {
        return str.replaceAll("[\"]", "&quot;").replaceAll("&#147;", "&quot;").replaceAll("&#148;", "&quot;");
    }

    public static String unhtmlDoubleQuotes(String str) {
        return str.replaceAll("&quot;", "\"");
    }

    public static String htmlQuotes(String str) {
        return htmlSingleQuotes(htmlDoubleQuotes(str));
    }

    public static String unhtmlQuotes(String str) {
        return unhtmlSingleQuotes(unhtmlDoubleQuotes(str));
    }

    public static String htmlAngleBrackets(String str) {
        return str.replaceAll("<", "&lt;").replaceAll(">", "&gt;");
    }

    public static String unhtmlAngleBrackets(String str) {
        return str.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
    }

    public static String htmlAmpersand(String str) {
        return str.replaceAll("&", "&amp;");
    }

    public static String unhtmlAmpersand(String str) {
        return str.replaceAll("&amp;", "&");
    }
}
