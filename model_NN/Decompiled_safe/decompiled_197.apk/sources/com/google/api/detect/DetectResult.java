package com.google.api.detect;

import com.google.api.translate.Language;

public class DetectResult {
    private double confidence;
    private Language language;
    private boolean reliable;

    public DetectResult(Language language2, boolean reliable2, double confidence2) {
        this.language = language2;
        this.reliable = reliable2;
        this.confidence = confidence2;
    }

    public Language getLanguage() {
        return this.language;
    }

    public void setLanguage(Language language2) {
        this.language = language2;
    }

    public boolean isReliable() {
        return this.reliable;
    }

    public void setReliable(boolean reliable2) {
        this.reliable = reliable2;
    }

    public double getConfidence() {
        return this.confidence;
    }

    public void setConfidence(double confidence2) {
        this.confidence = confidence2;
    }
}
