package com.google.api.detect;

import com.google.api.GoogleAPI;
import com.google.api.translate.Language;
import junit.framework.TestCase;
import org.junit.Test;

public class DetectTest extends TestCase {
    @Test
    public void testDetect() throws Exception {
        System.out.println("testDetect");
        GoogleAPI.setHttpReferrer("http://code.google.com/p/google-api-translate-java/");
        DetectResult detectResult = Detect.execute("Hello world");
        assertEquals(Language.ENGLISH, detectResult.getLanguage());
        assertTrue(detectResult.getConfidence() > 0.0d);
    }
}
