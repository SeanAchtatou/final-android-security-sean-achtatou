package com.google.api.detect;

import com.google.api.GoogleAPI;
import com.google.api.translate.Language;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONObject;

public class Detect extends GoogleAPI {
    private static String URL = "http://ajax.googleapis.com/ajax/services/language/detect?v=1.0&q=";

    public static DetectResult execute(String text) throws Exception {
        validateReferrer();
        JSONObject json = retrieveJSON(new URL(String.valueOf(URL) + URLEncoder.encode(text, "UTF-8")));
        return new DetectResult(Language.fromString(json.getJSONObject("responseData").getString("language")), json.getJSONObject("responseData").getBoolean("isReliable"), json.getJSONObject("responseData").getDouble("confidence"));
    }
}
