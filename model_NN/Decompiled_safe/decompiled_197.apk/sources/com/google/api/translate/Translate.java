package com.google.api.translate;

import com.google.api.GoogleAPI;
import com.tecnick.htmlutils.htmlentities.HTMLEntities;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONObject;

public final class Translate extends GoogleAPI {
    private static final String LANG_PARAM = "&langpair=";
    private static final String PARAMETERS = "v=2.0&langpair=#FROM#%7C#TO#&q=";
    private static final String PIPE_PARAM = "%7C";
    private static final String TEXT_PARAM = "&q=";
    private static final String URL = "http://ajax.googleapis.com/ajax/services/language/translate";

    public static String execute(String text, Language from, Language to) throws Exception {
        validateReferrer();
        return getJSONResponse(retrieveJSON(new URL(URL), String.valueOf(PARAMETERS.replaceAll("#FROM#", from.toString()).replaceAll("#TO#", to.toString())) + URLEncoder.encode(text, "UTF-8") + (key != null ? "&key=" + key : "")));
    }

    public static String[] execute(String[] text, Language from, Language to) throws Exception {
        validateReferrer();
        Language[] fromArgs = new Language[text.length];
        Language[] toArgs = new Language[text.length];
        for (int i = 0; i < text.length; i++) {
            fromArgs[i] = from;
            toArgs[i] = to;
        }
        return execute(text, fromArgs, toArgs);
    }

    public static String[] execute(String text, Language from, Language[] to) throws Exception {
        validateReferrer();
        String[] textArgs = new String[to.length];
        Language[] fromArgs = new Language[to.length];
        for (int i = 0; i < to.length; i++) {
            textArgs[i] = text;
            fromArgs[i] = from;
        }
        return execute(textArgs, fromArgs, to);
    }

    public static String[] execute(String[] text, Language[] from, Language[] to) throws Exception {
        validateReferrer();
        if (text.length != from.length || from.length != to.length) {
            throw new Exception("[google-api-translate-java] The same number of texts, from and to languages must be supplied.");
        } else if (text.length == 1) {
            return new String[]{execute(text[0], from[0], to[0])};
        } else {
            String[] responses = new String[text.length];
            StringBuilder parametersBuilder = new StringBuilder();
            parametersBuilder.append(String.valueOf(PARAMETERS.replaceAll("#FROM#", from[0].toString()).replaceAll("#TO#", to[0].toString())) + (key != null ? "&key=" + key : ""));
            parametersBuilder.append(URLEncoder.encode(text[0], "UTF-8"));
            for (int i = 1; i < text.length; i++) {
                parametersBuilder.append(LANG_PARAM);
                parametersBuilder.append(from[i].toString());
                parametersBuilder.append(PIPE_PARAM);
                parametersBuilder.append(to[i].toString());
                parametersBuilder.append(TEXT_PARAM);
                parametersBuilder.append(URLEncoder.encode(text[i].toString(), "UTF-8"));
            }
            JSONArray json = retrieveJSON(new URL(URL), parametersBuilder.toString()).getJSONArray("responseData");
            for (int i2 = 0; i2 < json.length(); i2++) {
                responses[i2] = getJSONResponse(json.getJSONObject(i2));
            }
            return responses;
        }
    }

    private static String getJSONResponse(JSONObject json) throws Exception {
        if (json.getString("responseStatus").equals("200")) {
            return HTMLEntities.unhtmlentities(json.getJSONObject("responseData").getString("translatedText"));
        }
        throw new Exception("Google returned the following error: [" + json.getString("responseStatus") + "] " + json.getString("responseDetails"));
    }

    @Deprecated
    public static String translate(String text, Language from, Language to) throws Exception {
        return execute(text, from, to);
    }
}
