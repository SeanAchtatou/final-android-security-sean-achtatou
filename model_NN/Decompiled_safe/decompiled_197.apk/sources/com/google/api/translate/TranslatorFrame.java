package com.google.api.translate;

import com.google.api.Files;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class TranslatorFrame extends JFrame {
    private static final String REFERRER_PATH = (String.valueOf(System.getProperty("user.home")) + "/.gtReferrer");
    private static final long serialVersionUID = 7916697355146649532L;
    private ButtonGroup buttonGroup1;
    private ButtonGroup buttonGroup2;
    private JTextArea fromTextArea;
    private JMenu jMenu1;
    private JMenuBar jMenuBar1;
    private JMenu jMenuFrom;
    private JMenuItem jMenuItem1;
    private JMenu jMenuTo;
    private JPanel jPanel2;
    private JPanel jPanel3;
    private JScrollPane jScrollPane1;
    private JScrollPane jScrollPane2;
    /* access modifiers changed from: private */
    public Language languageFrom = Language.FRENCH;
    /* access modifiers changed from: private */
    public Language languageTo = Language.ENGLISH;
    private JTextArea toTextArea;

    public TranslatorFrame() throws IOException {
        String referrer;
        initComponents();
        setLocationRelativeTo(null);
        File ref = new File(REFERRER_PATH);
        if (ref.exists()) {
            referrer = Files.read(ref).trim();
        } else {
            referrer = JOptionPane.showInputDialog(this, "Please enter the address of your website.\n(This is just to help Google identify how their translation tools are used).", "Website address", 0);
            Files.write(ref, referrer);
        }
        if (referrer.length() > 0) {
            Translate.setHttpReferrer(referrer);
        } else {
            System.exit(1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, ?[OBJECT, ARRAY], java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private void translate() {
        try {
            this.toTextArea.setText(Translate.translate(this.fromTextArea.getText().trim(), this.languageFrom, this.languageTo));
        } catch (Exception e) {
            Logger.getLogger(TranslatorFrame.class.getName()).log(Level.SEVERE, (String) null, (Throwable) e);
        }
    }

    private void initComponents() {
        this.buttonGroup1 = new ButtonGroup();
        this.buttonGroup2 = new ButtonGroup();
        this.jPanel2 = new JPanel();
        this.jScrollPane1 = new JScrollPane();
        this.fromTextArea = new JTextArea();
        this.jPanel3 = new JPanel();
        this.jScrollPane2 = new JScrollPane();
        this.toTextArea = new JTextArea();
        this.jMenuBar1 = new JMenuBar();
        this.jMenu1 = new JMenu();
        this.jMenuItem1 = new JMenuItem();
        this.jMenuTo = new JMenu();
        this.jMenuFrom = new JMenu();
        setDefaultCloseOperation(3);
        setTitle("Translator");
        getContentPane().setLayout(new BoxLayout(getContentPane(), 3));
        this.fromTextArea.setColumns(20);
        this.fromTextArea.setLineWrap(true);
        this.fromTextArea.setRows(5);
        this.fromTextArea.setWrapStyleWord(true);
        this.fromTextArea.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                TranslatorFrame.this.fromTextAreaKeyPressed(evt);
            }
        });
        this.jScrollPane1.setViewportView(this.fromTextArea);
        GroupLayout jPanel2Layout = new GroupLayout(this.jPanel2);
        this.jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 309, 32767).addContainerGap()));
        jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel2Layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane1, -1, 93, 32767).addContainerGap()));
        getContentPane().add(this.jPanel2);
        this.toTextArea.setColumns(20);
        this.toTextArea.setEditable(false);
        this.toTextArea.setLineWrap(true);
        this.toTextArea.setRows(5);
        this.toTextArea.setWrapStyleWord(true);
        this.jScrollPane2.setViewportView(this.toTextArea);
        GroupLayout jPanel3Layout = new GroupLayout(this.jPanel3);
        this.jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane2, -1, 309, 32767).addContainerGap()));
        jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING).addGroup(jPanel3Layout.createSequentialGroup().addContainerGap().addComponent(this.jScrollPane2, -1, 93, 32767).addContainerGap()));
        getContentPane().add(this.jPanel3);
        this.jMenu1.setText("File");
        this.jMenuItem1.setText("Exit");
        this.jMenuItem1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                TranslatorFrame.this.jMenuItem1ActionPerformed(evt);
            }
        });
        this.jMenu1.add(this.jMenuItem1);
        this.jMenuBar1.add(this.jMenu1);
        this.jMenuFrom.setText("From");
        this.jMenuTo.setText("To");
        for (final Language language : Language.values()) {
            JRadioButtonMenuItem menuItem = new JRadioButtonMenuItem();
            menuItem.setText(language.name());
            if (language.equals(this.languageFrom)) {
                menuItem.setSelected(true);
            }
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    TranslatorFrame.this.languageFrom = language;
                }
            });
            this.buttonGroup1.add(menuItem);
            this.jMenuFrom.add(menuItem);
            if (language != Language.AUTO_DETECT) {
                JRadioButtonMenuItem menuItem2 = new JRadioButtonMenuItem();
                menuItem2.setText(language.name());
                if (language.equals(this.languageTo)) {
                    menuItem2.setSelected(true);
                }
                menuItem2.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        TranslatorFrame.this.languageTo = language;
                    }
                });
                this.buttonGroup2.add(menuItem2);
                this.jMenuTo.add(menuItem2);
            }
        }
        this.jMenuBar1.add(this.jMenuFrom);
        this.jMenuBar1.add(this.jMenuTo);
        setJMenuBar(this.jMenuBar1);
        pack();
    }

    /* access modifiers changed from: private */
    public void jMenuItem1ActionPerformed(ActionEvent evt) {
        System.exit(0);
    }

    /* access modifiers changed from: private */
    public void fromTextAreaKeyPressed(KeyEvent evt) {
        if (evt.getKeyCode() == 10) {
            translate();
            evt.consume();
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TranslatorFrame().setVisible(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
