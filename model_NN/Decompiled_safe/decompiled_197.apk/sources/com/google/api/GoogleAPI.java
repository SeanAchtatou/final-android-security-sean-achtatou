package com.google.api;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public abstract class GoogleAPI {
    protected static final String ENCODING = "UTF-8";
    protected static String key;
    protected static String referrer;

    public static void setHttpReferrer(String pReferrer) {
        referrer = pReferrer;
    }

    public static void setKey(String pKey) {
        key = pKey;
    }

    public static void validateReferrer() throws Exception {
        if (referrer == null || referrer.length() == 0) {
            throw new Exception("[google-api-translate-java] Referrer is not set. Call setHttpReferrer().");
        }
    }

    protected static JSONObject retrieveJSON(URL url) throws Exception {
        HttpURLConnection uc;
        try {
            uc = (HttpURLConnection) url.openConnection();
            uc.setRequestProperty("referer", referrer);
            uc.setRequestMethod("GET");
            uc.setDoOutput(true);
            JSONObject jSONObject = new JSONObject(inputStreamToString(uc.getInputStream()));
            uc.getInputStream().close();
            if (uc.getErrorStream() != null) {
                uc.getErrorStream().close();
            }
            return jSONObject;
        } catch (Exception e) {
            throw new Exception("[google-api-translate-java] Error retrieving translation.", e);
        } catch (Throwable th) {
            uc.getInputStream().close();
            if (uc.getErrorStream() != null) {
                uc.getErrorStream().close();
            }
            throw th;
        }
    }

    protected static JSONObject retrieveJSON(URL url, String parameters) throws Exception {
        HttpURLConnection uc;
        PrintWriter pw;
        try {
            uc = (HttpURLConnection) url.openConnection();
            uc.setRequestProperty("referer", referrer);
            uc.setRequestMethod("POST");
            uc.setDoOutput(true);
            pw = new PrintWriter(uc.getOutputStream());
            pw.write(parameters);
            pw.close();
            uc.getOutputStream().close();
            JSONObject jSONObject = new JSONObject(inputStreamToString(uc.getInputStream()));
            if (uc.getInputStream() != null) {
                uc.getInputStream().close();
            }
            if (uc.getErrorStream() != null) {
                uc.getErrorStream().close();
            }
            if (pw != null) {
                pw.close();
            }
            return jSONObject;
        } catch (Exception e) {
            throw new Exception("[google-api-translate-java] Error retrieving translation.", e);
        } catch (Throwable th) {
            if (uc.getInputStream() != null) {
                uc.getInputStream().close();
            }
            if (uc.getErrorStream() != null) {
                uc.getErrorStream().close();
            }
            if (pw != null) {
                pw.close();
            }
            throw th;
        }
    }

    private static String inputStreamToString(InputStream inputStream) throws Exception {
        StringBuilder outputBuilder = new StringBuilder();
        if (inputStream != null) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, ENCODING));
                while (true) {
                    String string = reader.readLine();
                    if (string == null) {
                        break;
                    }
                    outputBuilder.append(string).append(10);
                }
            } catch (Exception e) {
                throw new Exception("[google-api-translate-java] Error reading translation stream.", e);
            }
        }
        return outputBuilder.toString();
    }
}
