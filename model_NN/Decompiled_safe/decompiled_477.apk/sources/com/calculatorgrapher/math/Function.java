package com.calculatorgrapher.math;

import java.awt.Point;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

public class Function {
    public static final char Infinity = '∞';
    public static final char cInfinity = '∞';
    public static final char cPi = 'π';
    public static final char cTeta = 'θ';
    public static final char cTheta = 'θ';
    public static char derVar = 'x';
    public static final double e = 2.718281828459045d;
    public static final char infinity = '∞';
    public static boolean jump = false;
    static boolean multivar = false;
    public static final String negInfinity = "-∞";
    public static final double pi = 3.141592653589793d;
    public static final String posInfinity = "+∞";
    static String[] prefixFn = {"ln", "exp", "log2", "log", "abs", "sqrt", "sin", "cos", "tan", "sec", "csc", "cot", "asin", "acos", "atan", "asec", "acsc", "acot", "sinh", "cosh", "tanh", "sech", "csch", "coth", "asinh", "acosh", "atanh", "asech", "acsch", "acoth", "floor", "gint", "round", "ceil", "lint", "end"};
    public static final String sInfinity = "∞";
    public static final String sPi = "π";
    public static final String sTeta = "θ";
    public static final String sTheta = "θ";
    static double stepValue = 0.0d;
    public String caption;
    protected String entered;
    protected String expression;
    String forDiff;
    protected String formula;
    String one;
    String zero;
    Hashtable zeros;

    public Function() {
        this("1");
    }

    public Function(String expression2) {
        this.zero = "0";
        this.one = "1";
        this.zeros = new Hashtable();
        setUp(expression2);
        checkExpression(this.formula);
    }

    public Function(Function fn) {
        this(fn.formula());
    }

    public void setUpAndCheck(String expression2) {
        setUp(expression2);
        checkExpression(this.formula);
    }

    public void setUp(String expression2) {
        StringBuffer bfr = new StringBuffer(replaceByUniChars(deleteWhiteSpace(expression2)));
        this.entered = bfr.toString();
        if (expression2.indexOf("~") > -1) {
            throw new InvalidFunctionExpressionException("Invalid Operator: ", "~", this.entered.substring(0, expression2.indexOf("~")));
        }
        setBuffer(bfr, trim(insertMultSymb(enclose(this.entered))));
        this.forDiff = deleteWhiteSpace(bfr.toString());
        this.formula = replaceNegs(bfr.toString().substring(1, bfr.length() - 1));
        setBuffer(bfr, replace(bfr.toString(), "-", "-1 * "));
        setBuffer(bfr, replace(bfr.toString(), "^ -1 * ", "^ -"));
        setBuffer(bfr, replace(bfr.toString(), "/ -1 * ", "/ -"));
        this.expression = replaceMathConstsByValue(bfr.toString());
        this.caption = deleteWhiteSpace(replaceNegs(reduceMinuses(this.entered)));
    }

    public void setExpression(String expression2) {
        this.expression = expression2;
    }

    public void setExpression(Function function) {
        this.expression = function.getExpression();
    }

    public String getExpression() {
        return this.expression;
    }

    public String expression() {
        return this.expression.substring(1, this.expression.length() - 1);
    }

    public String entered() {
        return this.entered;
    }

    public String forDiff() {
        return this.forDiff;
    }

    public String formula() {
        return this.formula;
    }

    public String toString() {
        return this.formula;
    }

    public String captionW() {
        return this.caption;
    }

    public String caption() {
        return "(x) = " + captionW();
    }

    public void checkExpression(String expression2) {
        if (isEmptyExpression()) {
            throw new InvalidFunctionExpressionException("Empty expression", "");
        } else if (isOnlyNegSings(expression2)) {
            throw new InvalidFunctionExpressionException("Operand Expected", "");
        } else if (isEmptyParantheses()) {
            throw new InvalidFunctionExpressionException("Empty Parentheses", "");
        } else if (!isParenthesesBalanced()) {
            throw new InvalidFunctionExpressionException("Unbalanced Parentheses", "");
        } else if (isLeftParanthesisMissing()) {
            throw new InvalidFunctionExpressionException("Left Parenthesis Required", "");
        } else if (isRightParanthesisMissing()) {
            throw new InvalidFunctionExpressionException("Right Parenthesis Required", "");
        } else if (this.entered.equals("nil")) {
            throw new InvalidFunctionExpressionException("", "");
        } else {
            StringTokenizer tokenizerLP = new StringTokenizer(expression2, "(");
            StringBuffer LP_LP = new StringBuffer();
            StringBuffer tail = new StringBuffer();
            StringBuffer elem = new StringBuffer();
            StringBuffer tailToken = new StringBuffer();
            int indexLP = 0;
            while (tokenizerLP.hasMoreTokens()) {
                setBuffer(LP_LP, tokenizerLP.nextToken());
                StringTokenizer tokenizerElem = new StringTokenizer(new StringBuilder().append((Object) LP_LP).toString());
                while (expression2.charAt(indexLP) == '(') {
                    indexLP++;
                }
                setBuffer(tail, expression2.substring(0, indexLP));
                while (tokenizerElem.hasMoreTokens()) {
                    setBuffer(elem, tokenizerElem.nextToken());
                    if (!isPrefix(new StringBuilder().append((Object) elem).toString()) || !tokenizerElem.hasMoreTokens()) {
                        if (isOperand(new StringBuilder().append((Object) elem).toString()) || isPrefix(new StringBuilder().append((Object) elem).toString())) {
                            tail.append(((Object) elem) + " ");
                            if (tokenizerElem.hasMoreTokens()) {
                                setBuffer(elem, tokenizerElem.nextToken());
                                if (!isOperator(new StringBuilder().append((Object) elem).toString())) {
                                    throw new InvalidFunctionExpressionException("Not an Operator:", new StringBuilder().append((Object) elem).toString(), new StringBuilder().append((Object) tail).toString());
                                }
                                tail.append(((Object) elem) + " ");
                            }
                        } else {
                            throw new InvalidFunctionExpressionException("Invalid Operand: ", new StringBuilder().append((Object) elem).toString(), new StringBuilder().append((Object) tail).toString());
                        }
                    } else if (elem.charAt(0) != '-') {
                        throw new InvalidFunctionExpressionException("Missing '(' after predefined function:", new StringBuilder().append((Object) elem).toString(), new StringBuilder().append((Object) tail).toString());
                    } else {
                        throw new InvalidFunctionExpressionException("Operand Expected:", new StringBuilder().append((Object) elem).toString(), new StringBuilder().append((Object) tail).toString());
                    }
                }
                if (!tokenizerLP.hasMoreTokens() && isOperator(new StringBuilder().append((Object) elem).toString())) {
                    throw new InvalidFunctionExpressionException("Operand Expected:", "___", new StringBuilder().append((Object) tail).toString());
                } else if (tokenizerLP.hasMoreTokens() || !isPrefix(new StringBuilder().append((Object) elem).toString())) {
                    StringTokenizer tknzr1 = new StringTokenizer(new StringBuilder().append((Object) LP_LP).toString());
                    tailToken.setLength(0);
                    if (tknzr1.hasMoreTokens()) {
                        tailToken.append(tknzr1.nextToken());
                    }
                    indexLP = expression2.indexOf("(", expression2.indexOf(new StringBuilder().append((Object) tailToken).toString(), indexLP));
                } else {
                    throw new InvalidFunctionExpressionException("Argument Expected:", "(___)", new StringBuilder().append((Object) tail).toString());
                }
            }
        }
    }

    public static String trim(String expression2) {
        StringTokenizer tokenizer = new StringTokenizer(reduceMinuses(expression2), "^/*+", true);
        StringBuffer bfr = new StringBuffer();
        while (tokenizer.hasMoreTokens()) {
            bfr = bfr.append(tokenizer.nextToken()).append(" ");
        }
        return bfr.toString().trim();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String replaceNegs(String expression2) {
        return expression2.replace('~', '-');
    }

    public String replaceNegs() {
        return replaceNegs(this.forDiff);
    }

    public static String reduceMinuses(String expression2) {
        StringTokenizer tokenizer = new StringTokenizer(expression2, "-", true);
        StringBuffer bfr = new StringBuffer();
        StringBuffer token = new StringBuffer();
        while (tokenizer.hasMoreTokens()) {
            token.setLength(0);
            token.append(tokenizer.nextToken());
            if (token.toString().equals("-")) {
                int j = 0;
                while (token.toString().equals("-")) {
                    j++;
                    token.setLength(0);
                    if (tokenizer.hasMoreTokens()) {
                        token.append(tokenizer.nextToken());
                    }
                }
                boolean b = bfr.toString().endsWith("^") | bfr.toString().endsWith("/") | bfr.toString().endsWith("*") | bfr.toString().endsWith("+") | bfr.toString().endsWith("(") | bfr.toString().equals("");
                switch (j % 2) {
                    case 0:
                        if (!b) {
                            bfr.append(" ~ -").append(token);
                            break;
                        } else {
                            bfr.append(token);
                            continue;
                        }
                    case 1:
                        if (!b) {
                            bfr.append(" ~ ").append(token);
                            break;
                        } else {
                            bfr.append("-").append(token);
                            continue;
                        }
                }
            } else {
                bfr.append(token);
            }
        }
        return bfr.toString();
    }

    public static String enclose(String expression2) {
        if (!isParenthesesBalanced(expression2) || indexOfMatchingRP(expression2) != -2) {
            return "(" + expression2 + ")";
        }
        return expression2;
    }

    public static String encloseDer(String fStr) {
        if (indexOfMatchingRP(fStr) == -2) {
            return fStr;
        }
        String str = makeTokenizable(fStr);
        return (str.indexOf("+") > 0 || str.indexOf("~") > 0) ? "(" + fStr + ")" : fStr;
    }

    public static String encloseNeg(String fStr) {
        if (makeTokenizable(fStr).startsWith("-")) {
            return "(" + fStr + ")";
        }
        return fStr;
    }

    public static String encloseIfNecessary(String fStr) {
        if (indexOfMatchingRP(fStr) == -2) {
            return fStr;
        }
        String str = makeTokenizable(fStr);
        return (str.indexOf("+") > 0 || str.indexOf("-") >= 0 || Character.isDigit(fStr.charAt(0)) || fStr.charAt(0) == '.') ? "(" + fStr + ")" : fStr;
    }

    public static boolean isNumber(String fStr) {
        try {
            Double.valueOf(fStr).doubleValue();
            return true;
        } catch (NumberFormatException e2) {
            return false;
        }
    }

    public boolean isEmptyExpression() {
        if (this.entered.length() == 0) {
            return true;
        }
        return false;
    }

    public boolean isOnlyNegSings(String formula2) {
        if (formula2.equals("")) {
            return true;
        }
        return false;
    }

    public boolean isEmptyParantheses() {
        if (this.entered.toString().trim().indexOf("()") > -1) {
            return true;
        }
        return false;
    }

    public boolean isLeftParanthesisMissing() {
        if (this.entered.toString().trim().indexOf(")") <= -1 || this.entered.toString().trim().lastIndexOf("(") <= this.entered.toString().trim().lastIndexOf(")")) {
            return false;
        }
        return true;
    }

    public boolean isRightParanthesisMissing() {
        if (this.entered.toString().trim().indexOf("(") <= -1 || this.entered.toString().trim().lastIndexOf("(") <= this.entered.toString().trim().lastIndexOf(")")) {
            return false;
        }
        return true;
    }

    public boolean isParenthesesBalanced() {
        StringTokenizer tokenizer = new StringTokenizer(this.entered.toString(), "()", true);
        int noOfL = 0;
        int noOfR = 0;
        int signum = 0;
        StringBuffer token = new StringBuffer();
        while (tokenizer.hasMoreTokens()) {
            setBuffer(token, tokenizer.nextToken());
            if (token.toString().equals("(")) {
                noOfL++;
                signum--;
                continue;
            } else if (token.toString().equals(")")) {
                noOfR++;
                signum++;
                continue;
            } else {
                continue;
            }
            if (signum > 0) {
                return false;
            }
        }
        if (noOfL == noOfR) {
            return true;
        }
        return false;
    }

    public static boolean isParenthesesBalanced(String expression2) {
        StringTokenizer tokenizer = new StringTokenizer(expression2, "()", true);
        int noOfL = 0;
        int noOfR = 0;
        int signum = 0;
        StringBuffer token = new StringBuffer();
        while (tokenizer.hasMoreTokens()) {
            setBuffer(token, tokenizer.nextToken());
            if (token.toString().equals("(")) {
                noOfL++;
                signum--;
                continue;
            } else if (token.toString().equals(")")) {
                noOfR++;
                signum++;
                continue;
            } else {
                continue;
            }
            if (signum > 0) {
                return false;
            }
        }
        if (noOfL == noOfR) {
            return true;
        }
        return false;
    }

    public static boolean isOperand(String str) {
        if (str.indexOf(")") > 0) {
            str = str.substring(0, str.indexOf(")"));
        }
        if (str.equals("x") || str.equals("-x") || str.equals("y") || str.equals("-y") || str.equals("θ") || str.equals("-θ") || str.equals("r") || str.equals("-r") || str.equals("t") || str.equals("-t") || str.equals("e") || str.equals("-e") || str.equals(sPi) || str.equals("-π") || str.equalsIgnoreCase("Infinity") || str.equalsIgnoreCase("-Infinity") || str.equals(sInfinity) || str.equals(posInfinity) || str.equals(negInfinity)) {
            return true;
        }
        try {
            Double.valueOf(str).doubleValue();
            return true;
        } catch (NumberFormatException e2) {
            return false;
        }
    }

    public static boolean isPrefix(String str) {
        if (str.equalsIgnoreCase("-")) {
            return true;
        }
        for (int i = 0; i < prefixFn.length; i++) {
            if (str.equalsIgnoreCase(prefixFn[i]) || str.equalsIgnoreCase("-" + prefixFn[i])) {
                return true;
            }
        }
        return false;
    }

    public static boolean isIsolated(String expression2, String element) {
        return isIsolatedL(expression2, element) && isIsolatedR(expression2, element);
    }

    public static boolean isIsolatedL(String expression2, String element) {
        int index = expression2.indexOf(element);
        return expression2.regionMatches(index - 1, " ", 0, 1) || expression2.regionMatches(index - 1, "(", 0, 1) || expression2.regionMatches(index - 1, "-", 0, 1) || index == 0;
    }

    public static boolean isIsolatedR(String expression2, String element) {
        int index = expression2.indexOf(element);
        return expression2.regionMatches(element.length() + index, " ", 0, 1) || expression2.regionMatches(element.length() + index, ")", 0, 1) || index == expression2.length() - element.length();
    }

    public boolean isIsolated(String expression2, char c) {
        return isIsolated(expression2, new StringBuilder().append(c).toString());
    }

    public static boolean isOperator(char op) {
        return isOperator(new StringBuilder().append(op).toString());
    }

    public static boolean isOperator(String op) {
        return op.equals("^") || op.equals("/") || op.equals("*") || op.equals("-") || op.equals("+");
    }

    public static boolean isVarOrParam(char c) {
        return c == 'x' || c == 'y' || c == 952 || c == 'r' || c == 't' || c == 'e' || c == 960;
    }

    public static boolean isVarOrParam(String expression2, char c, int index) {
        if (c == 'e') {
            if (expression2.regionMatches(index, "exp(", 0, 4) || expression2.regionMatches(index - 1, "sec(", 0, 4) || expression2.regionMatches(index - 1, "sech(", 0, 5) || expression2.regionMatches(index - 2, "asech(", 0, 6) || expression2.regionMatches(index - 2, "asec(", 0, 5) || expression2.regionMatches(index - 1, "ceil(", 0, 5)) {
                return false;
            }
            return true;
        } else if (c == 960) {
            return true;
        } else {
            return isVar(expression2, c, index);
        }
    }

    public static boolean isVar(String expression2, char c, int index) {
        if (c == 'x') {
            if (expression2.regionMatches(index - 1, "exp(", 0, 4)) {
                return false;
            }
            return true;
        } else if (c == 't') {
            if (!expression2.regionMatches(index - 3, "sqrt", 0, 4) && !expression2.regionMatches(index, "tan", 0, 3) && !expression2.regionMatches(index - 1, "atan", 0, 4) && !expression2.regionMatches(index - 0, "tanh", 0, 4) && !expression2.regionMatches(index - 1, "atanh", 0, 5) && !expression2.regionMatches(index - 2, "cot", 0, 3) && !expression2.regionMatches(index - 3, "acot", 0, 4) && !expression2.regionMatches(index - 2, "coth", 0, 4) && !expression2.regionMatches(index - 3, "acoth", 0, 5) && !expression2.regionMatches(index - 3, "gint", 0, 4) && !expression2.regionMatches(index - 3, "lint", 0, 4)) {
                if (!expression2.regionMatches(true, index - 6, "Infinity", 0, 8)) {
                    if (!expression2.regionMatches(true, index - 2, "enter", 0, 5)) {
                        return true;
                    }
                }
            }
            return false;
        } else if (c == 'y') {
            return !expression2.regionMatches(true, index - 7, "Infinity", 0, 8);
        } else if (c == 952) {
            return true;
        } else {
            if (c != 'r') {
                return false;
            }
            if (!expression2.regionMatches(true, index - 4, "floor", 0, 5)) {
                if (!expression2.regionMatches(true, index - 2, "sqrt", 0, 4) && !expression2.regionMatches(true, index, "round", 0, 5)) {
                    if (!expression2.regionMatches(true, index - 4, "enter", 0, 5)) {
                        if (!expression2.regionMatches(true, index - 3, "expression", 0, 5)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }

    public static boolean hasVar(String expression2, char var) {
        int index = expression2.indexOf(var);
        while (index >= 0) {
            if (isVar(expression2, var, index)) {
                return true;
            }
            index = expression2.indexOf(var, index + 1);
        }
        return false;
    }

    public static boolean hasVar(String expression2) {
        return hasVar(expression2, "xyrtθ");
    }

    public static boolean hasVar(String expression2, String vars) {
        char[] variables = vars.toCharArray();
        for (int i = 0; i < variables.length; i++) {
            int index = expression2.indexOf(variables[i]);
            while (index >= 0) {
                if (isVar(expression2, variables[i], index)) {
                    return true;
                }
                index = expression2.indexOf(variables[i], index + 1);
            }
        }
        return false;
    }

    public static boolean hasVar(Function fn) {
        try {
            parseD(new StringBuilder().append(fn.calculate()).toString());
            return false;
        } catch (NumberFormatException e2) {
            return true;
        }
    }

    public boolean hasVar1(String expression2) {
        char[] variables = {'x', 'y', 't', 952};
        for (int i = 0; i < variables.length; i++) {
            int index = expression2.indexOf(variables[i]);
            while (index >= 0) {
                if (isIsolated(expression2.substring(index), variables[i])) {
                    return true;
                }
                index = expression2.indexOf(variables[i], index + 1);
            }
        }
        return false;
    }

    public static boolean isOperatorOrLeftParenthesis(char c) {
        return isOperator(c) || c == '(';
    }

    public static boolean isOperatorOrRightParenthesis(char c) {
        return isOperator(c) || c == ')';
    }

    public static boolean isDigitOrDecimalPoint(char c) {
        return Character.isDigit(c) || c == '.';
    }

    private static double result(double num1, char operator, double num2) {
        switch (operator) {
            case '*':
                return num1 * num2;
            case '+':
                return num1 + num2;
            case '/':
                return num1 / num2;
            case '^':
                return power(num1, num2);
            case '~':
                return num1 - num2;
            default:
                return 0.0d;
        }
    }

    public static double parseD(String str) {
        if (str.equalsIgnoreCase("Infinity") || str.equalsIgnoreCase(sInfinity) || str.equalsIgnoreCase(posInfinity)) {
            return Double.POSITIVE_INFINITY;
        }
        if (str.equalsIgnoreCase("-Infinity") || str.equalsIgnoreCase(negInfinity)) {
            return Double.NEGATIVE_INFINITY;
        }
        if (str.equalsIgnoreCase("NaN")) {
            return Double.NaN;
        }
        return Double.valueOf(str).doubleValue();
    }

    public static double roundD(double x, int n) {
        return ((double) Math.round(Math.pow(10.0d, (double) n) * x)) / Math.pow(10.0d, (double) n);
    }

    public static String format(double d, int n) {
        String dcp = "";
        for (int i = 1; i <= n; i++) {
            dcp = String.valueOf(dcp) + "#";
        }
        String s = new StringBuilder().append(d).toString();
        if (((double) Math.round(d)) - d == 0.0d && d <= 4.6116860184273879E18d && d >= -4.6116860184273879E18d) {
            return new DecimalFormat("#,###").format((long) d);
        } else if (Math.abs(d) > 1.0E15d) {
            return s;
        } else {
            if (Math.abs(d) > 1.0d && Math.abs(d) <= 1.0E15d) {
                return new DecimalFormat("#,###." + dcp).format(d);
            }
            if (Math.abs(d) < 1.0E-4d) {
                return s;
            }
            DecimalFormat formatter = new DecimalFormat("#." + dcp);
            if (formatter.format(d).equals("-0")) {
                return "0";
            }
            return formatter.format(d);
        }
    }

    public static double calculateInner(String simpleExpression) {
        return parseD(doOperations(doOperations(doOperations(doOperations(doOperations(simpleExpression, '^'), '/'), '*'), '~'), '+'));
    }

    private static String doOperations1(String simpleExpression, char operator) {
        StringTokenizer tokenizer = new StringTokenizer(simpleExpression);
        String num1 = tokenizer.nextToken();
        while (tokenizer.hasMoreTokens()) {
            char op = tokenizer.nextToken().charAt(0);
            String num2 = tokenizer.nextToken();
            if (op == operator) {
                double result = result(parseD(num1), op, parseD(num2));
                int index = simpleExpression.indexOf(op);
                simpleExpression = replaceIntv(simpleExpression, (index - num1.length()) - 1, num2.length() + index + 1, new StringBuilder().append(result).toString());
                num1 = new StringBuilder().append(result).toString();
            } else {
                num1 = num2;
            }
        }
        return simpleExpression;
    }

    private static String doOperations(String simpleExpression, char operator) {
        StringBuffer bfr = new StringBuffer(simpleExpression);
        StringTokenizer tokenizer = new StringTokenizer(simpleExpression);
        StringBuffer num1 = new StringBuffer(tokenizer.nextToken());
        StringBuffer num2 = new StringBuffer();
        while (tokenizer.hasMoreTokens()) {
            char op = tokenizer.nextToken().charAt(0);
            setBuffer(num2, tokenizer.nextToken());
            if (op == operator) {
                double result = result(parseD(num1.toString()), op, parseD(num2.toString()));
                int index = bfr.toString().indexOf(op);
                setBuffer(bfr, replaceIntv(bfr.toString(), (index - num1.length()) - 1, num2.length() + index + 1, new StringBuilder().append(result).toString()));
                setBuffer(num1, new StringBuilder().append(result).toString());
            } else {
                setBuffer(num1, num2);
            }
        }
        return bfr.toString();
    }

    private String _doOperations(String simpleExpression, char operator) {
        StringBuffer bfr = new StringBuffer(simpleExpression);
        while (bfr.toString().indexOf(operator) > 0) {
            setBuffer(bfr, doOp(bfr.toString(), operator));
        }
        return bfr.toString();
    }

    private static String doOp(String simpleExpr, char operator) {
        int indexOp = simpleExpr.indexOf(operator);
        return replaceIntv(simpleExpr, startOfOperand1(simpleExpr, indexOp), endOfOperand2(simpleExpr, indexOp), new StringBuilder().append(result(parseD(operand1(simpleExpr, indexOp)), operator, parseD(operand2(simpleExpr, indexOp)))).toString());
    }

    private static String reduce(String numExpression) {
        int right = numExpression.indexOf(41);
        int left = numExpression.lastIndexOf(40, right);
        double innerValue = calculateInner(numExpression.substring(left + 1, right));
        String tail = numExpression.substring(0, left);
        int index = Math.max(tail.lastIndexOf(" "), tail.lastIndexOf("("));
        return replaceIntv(numExpression, index + 1, right, new StringBuilder().append(calculatePrefix(tail.substring(index + 1).trim(), innerValue)).toString());
    }

    private static double calculatePrefix(String prefix, double innerValue) {
        if (prefix.startsWith("-")) {
            return -getValueOfPrefix(prefix.substring(1), innerValue);
        }
        return getValueOfPrefix(prefix, innerValue);
    }

    public static double calculate(String numExpression) {
        StringBuffer bfr = new StringBuffer(numExpression);
        while (bfr.toString().indexOf(41) > 0) {
            setBuffer(bfr, reduce(deleteDoubleNegatives(bfr.toString())));
        }
        return parseD(bfr.toString());
    }

    public double calculate() {
        return calculate(this.expression);
    }

    public double valueAt(double xValue) {
        return calculate(replaceVarByValue(this.expression, 'x', xValue));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public double valueAtCart(double xValue) {
        return calculate(replaceVarBy(this.expression, 'x', xValue));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public double valueAtPolar(double tetaValue) {
        return calculate(replaceVarBy(this.expression, 952, tetaValue));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public double valueAtT(double tValue) {
        return calculate(replaceVarBy(this.expression, 't', tValue));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public double valueAtY(double yValue) {
        return calculate(replaceVarBy(this.expression, 'y', yValue));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public double valueAtR(double rValue) {
        return calculate(replaceVarBy(this.expression, 'r', rValue));
    }

    public double valueAt(char var, double value) {
        if (var == 'x') {
            return valueAtCart(value);
        }
        if (var == 952) {
            return valueAtPolar(value);
        }
        if (var == 't') {
            return valueAtT(value);
        }
        if (var == 'y') {
            return valueAtY(value);
        }
        if (var == 'r') {
            return valueAtR(value);
        }
        return 1.0d;
    }

    public double valueAt(double x, double y) {
        return calculate(replaceTByValue(replaceTetaByValue(replaceRByValue(replaceYByValue(replaceXByValue(this.expression, x), y), y), x), x));
    }

    public static int startsWithPrefix(String expression2) {
        for (int i = 0; i < prefixFn.length; i++) {
            if (expression2.toLowerCase().startsWith(prefixFn[i])) {
                return prefixFn[i].length();
            }
        }
        return 0;
    }

    public static int endsWithPrefix(String expression2) {
        for (int i = 0; i < prefixFn.length; i++) {
            if (expression2.toLowerCase().endsWith(prefixFn[i])) {
                return prefixFn[i].length();
            }
        }
        return 0;
    }

    public static String insertMultSymb(String expression2) {
        return insertMultSymbRight(insertMultSymbLeft(expression2));
    }

    public static String insertMultSymbLeft(String expression2) {
        StringBuffer bfr = new StringBuffer(expression2);
        int index = 1;
        while (index < bfr.length() - 1) {
            char c = bfr.charAt(index);
            char c0 = bfr.charAt(index - 1);
            int indexPrefixStart = startsWithPrefix(new StringBuilder().append((Object) bfr).toString().substring(index));
            int indexPrefixEnd = endsWithPrefix(new StringBuilder().append((Object) bfr).toString().substring(0, index));
            if (indexPrefixStart > 0 && !isOperatorOrLeftParenthesis(c0)) {
                bfr.insert(index, "*");
                index += indexPrefixStart;
            } else if (indexPrefixStart > 0) {
                index += indexPrefixStart;
            } else if (c == '(' && indexPrefixEnd <= 0 && !isOperatorOrLeftParenthesis(c0)) {
                bfr.insert(index, "*");
                index++;
            } else if (isDigitOrDecimalPoint(c) && !isOperatorOrLeftParenthesis(c0) && !isDigitOrDecimalPoint(c0)) {
                bfr.insert(index, "*");
                index++;
            } else if (isOperand(new StringBuilder().append(c).toString()) && !isDigitOrDecimalPoint(c) && !isOperatorOrLeftParenthesis(c0)) {
                bfr.insert(index, "*");
                index++;
            }
            index++;
        }
        return new StringBuilder().append((Object) bfr).toString();
    }

    public static String insertMultSymbRight(String expression2) {
        StringBuffer bfr = new StringBuffer(expression2);
        int index = 1;
        while (index < bfr.length() - 1) {
            char c = bfr.charAt(index);
            char c1 = bfr.charAt(index + 1);
            int indexPrefixStart = startsWithPrefix(new StringBuilder().append((Object) bfr).toString().substring(index));
            if (indexPrefixStart > 0) {
                index += indexPrefixStart;
            } else if ((isOperand(new StringBuilder().append(c).toString()) || c == ')') && !isOperatorOrRightParenthesis(c1) && !isDigitOrDecimalPoint(c1)) {
                index++;
                bfr.insert(index, "*");
            }
            index++;
        }
        return new StringBuilder().append((Object) bfr).toString();
    }

    public static String deleteWhiteSpace(String expression2) {
        StringBuffer bfr = new StringBuffer();
        for (int i = 0; i < expression2.length(); i++) {
            if (expression2.charAt(i) != ' ') {
                bfr.append(expression2.charAt(i));
            }
        }
        return bfr.toString();
    }

    public static StringBuffer deleteWhiteSpace(StringBuffer expression2) {
        StringBuffer bfr = new StringBuffer();
        for (int i = 0; i < expression2.length(); i++) {
            if (expression2.charAt(i) != ' ') {
                bfr.append(expression2.charAt(i));
            }
        }
        return bfr;
    }

    public static String deleteDoubleNegatives(String expression2) {
        StringBuffer bfr = new StringBuffer(expression2);
        for (int index = expression2.indexOf("--"); index >= 0; index = bfr.toString().indexOf("--")) {
            setBuffer(bfr, String.valueOf(bfr.toString().substring(0, index)) + bfr.toString().substring(index + 2));
        }
        return bfr.toString();
    }

    public static StringBuffer deleteDoubleNegatives(StringBuffer expression2) {
        return new StringBuffer(deleteDoubleNegatives(expression2.toString()));
    }

    private static int startOfOperand1(String simpleExpr, char operator) {
        return startOfOperand1(simpleExpr, simpleExpr.indexOf(operator));
    }

    private static int startOfOperand1(String simpleExpr, int indexOp) {
        return simpleExpr.lastIndexOf(32, indexOp - 2) + 1;
    }

    private static String operand1(String simpleExpr, char operator) {
        return operand1(simpleExpr, simpleExpr.indexOf(operator));
    }

    private static String operand1(String simpleExpr, int indexOp) {
        return simpleExpr.substring(startOfOperand1(simpleExpr, indexOp), indexOp).trim();
    }

    private static int endOfOperand2(String simpleExpr, char operator) {
        return endOfOperand2(simpleExpr, simpleExpr.indexOf(operator));
    }

    private static int endOfOperand2(String simpleExpr, int indexOp) {
        return (String.valueOf(simpleExpr) + ' ').indexOf(32, indexOp + 2) - 1;
    }

    private static String operand2(String simpleExpr, char operator) {
        return operand2(simpleExpr, simpleExpr.indexOf(operator));
    }

    private static String operand2(String simpleExpr, int indexOp) {
        return simpleExpr.substring(indexOp + 1, endOfOperand2(simpleExpr, indexOp) + 1).trim();
    }

    public static StringBuffer setBuffer(StringBuffer bfr, String str) {
        bfr.setLength(0);
        return bfr.append(str);
    }

    public static StringBuffer setBuffer(StringBuffer bfr, StringBuffer strBfr) {
        return setBuffer(bfr, strBfr.toString());
    }

    public static String replaceVarByValue(String expression2, char var, double value) {
        return replaceVarBy(expression2, var, value);
    }

    public static String replaceVarBy(String expression2, char var, char newVar) {
        int index = expression2.indexOf(var);
        StringBuffer bfr = new StringBuffer(expression2);
        while (index >= 0) {
            if (isVar(expression2, var, index)) {
                setBuffer(bfr, String.valueOf(bfr.toString().substring(0, index)) + newVar + bfr.toString().substring(index + 1));
            }
            index = bfr.toString().indexOf(var, index + 1);
        }
        return bfr.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, double, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, boolean):java.lang.String */
    public static String replaceVarBy(String expression2, char var, double value) {
        return replace(expression2, new StringBuilder().append(var).toString(), value, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public static String replaceXByValue(String expression2, double value) {
        return replaceVarBy(expression2, 'x', value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public static String replaceYByValue(String expression2, double value) {
        return replaceVarBy(expression2, 'y', value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public static String replaceTetaByValue(String expression2, double value) {
        return replaceVarBy(expression2, 952, value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public static String replaceRByValue(String expression2, double value) {
        return replaceVarBy(expression2, 'r', value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
     arg types: [java.lang.String, int, double]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String */
    public static String replaceTByValue(String expression2, double value) {
        return replaceVarBy(expression2, 't', value);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceTetaByX(String expression2) {
        return replaceVarBy(expression2, 952, 'x');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceXByTeta(String expression2) {
        return replaceVarBy(expression2, 'x', 952);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceTByTeta(String expression2) {
        return replaceVarBy(expression2, 't', 952);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceTByX(String expression2) {
        return replaceVarBy(expression2, 't', 'x');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceTetaByT(String expression2) {
        return replaceVarBy(expression2, 952, 't');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceYByR(String expression2) {
        return replaceVarBy(expression2, 'y', 'r');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, double):java.lang.String
      com.calculatorgrapher.math.Function.replaceVarBy(java.lang.String, char, char):java.lang.String */
    public static String replaceRByY(String expression2) {
        return replaceVarBy(expression2, 'r', 'y');
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, boolean):java.lang.String */
    public static String replaceEByValue(String expression2) {
        return replace(expression2, "e", 2.718281828459045d, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, boolean):java.lang.String */
    public static String replacePiByValue(String expression2) {
        return replace(expression2, sPi, 3.141592653589793d, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char):java.lang.String */
    public static String replaceByUniChars(String expression2) {
        return replacePiByUniChar(replace(replace(replace(expression2, "infinity", 8734), "teta", "θ"), "theta", "θ"));
    }

    public static String replacePiByUniChar(String expression2) {
        return replace(expression2, "pi", sPi);
    }

    public static String replaceTetaByUniChar(String expression2) {
        return replace(replace(expression2, "teta", "θ"), "theta", "θ");
    }

    public static String replaceMathConstsByValue(String str) {
        return replaceEByValue(replacePiByValue(str));
    }

    public static String replace(String expression2, String str1, String str2, int index, boolean isolated) {
        int index2 = expression2.indexOf(str1, index);
        if (index2 < 0) {
            return expression2;
        }
        if (!isolated) {
            expression2 = replaceIntv(expression2, index2, (str1.length() + index2) - 1, str2);
        } else if (isIsolated(expression2.substring(Math.max(0, index2 - 1)), str1)) {
            expression2 = replaceIntv(expression2, index2, (str1.length() + index2) - 1, str2);
        }
        return replace(expression2, str1, str2, index2 + 2, isolated);
    }

    public static String replace(String expression2, String str1, double str2, int index, boolean isolated) {
        return replace(expression2, str1, new StringBuilder().append(str2).toString(), -1, isolated);
    }

    public static String replace(String expression2, String str1, char str2, int index, boolean isolated) {
        return replace(expression2, str1, new StringBuilder().append(str2).toString(), -1, isolated);
    }

    public static String replace(String expression2, String str1, String str2, boolean isolated) {
        return replace(expression2, str1, str2, -1, isolated);
    }

    public static String replace(String expression2, String str1, double value, boolean isolated) {
        return replace(expression2, str1, new StringBuilder().append(value).toString(), isolated);
    }

    public static String replace(String expression2, String str1, char value, boolean isolated) {
        return replace(expression2, str1, new StringBuilder().append(value).toString(), isolated);
    }

    public static String replace(String expression2, char char1, char char2, int index, boolean isolated) {
        return replace(expression2, new StringBuilder().append(char1).toString(), new StringBuilder().append(char2).toString(), index, isolated);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, char, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String */
    public static String replace(String expression2, String str1, String str2, int index) {
        return replace(expression2, str1, str2, index, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, char, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String */
    public static String replace(String expression2, String str1, double str2, int index) {
        return replace(expression2, str1, new StringBuilder().append(str2).toString(), index, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, char, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String */
    public static String replace(String expression2, String str1, char str2, int index) {
        return replace(expression2, str1, new StringBuilder().append(str2).toString(), index, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      com.calculatorgrapher.math.Function.replace(java.lang.String, char, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, char, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, double, int, boolean):java.lang.String
      com.calculatorgrapher.math.Function.replace(java.lang.String, java.lang.String, java.lang.String, int, boolean):java.lang.String */
    public static String replace(String expression2, String str1, String str2) {
        return replace(expression2, str1, str2, -1, false);
    }

    public static String replace(String expression2, String str1, double value) {
        return replace(expression2, str1, new StringBuilder().append(value).toString());
    }

    public static String replace(String expression2, String str1, char value) {
        return replace(expression2, str1, new StringBuilder().append(value).toString());
    }

    public static String replaceIntv(String expression2, int i, int j, String segm) {
        return String.valueOf(expression2.substring(0, i)) + segm + expression2.substring(j + 1);
    }

    public static String replaceIntv(String expression2, int i, int j, double d) {
        return replaceIntv(expression2, i, j, new StringBuilder().append(d).toString());
    }

    public static String replaceIntv(String expression2, int index, String segm) {
        return replaceIntv(expression2, index, index, segm);
    }

    public static String replaceIntv(String expression2, int index, double d) {
        return replaceIntv(expression2, index, index, new StringBuilder().append(d).toString());
    }

    public static String replaceIntv(String expression2, int index, char c) {
        return replaceIntv(expression2, index, new StringBuilder().append(c).toString());
    }

    public static boolean isInt(double x) {
        return x - ((double) ((int) x)) == 0.0d;
    }

    public static boolean isPosInt(double x) {
        return x > 0.0d && isInt(x);
    }

    public static double factorial(int n) {
        if (n == 0) {
            return 1.0d;
        }
        return ((double) n) * factorial(n - 1);
    }

    public int numTerms(double x, double r) {
        return 20;
    }

    public static double getValueOfPrefix(String fPrefix, double result) {
        if (fPrefix.equalsIgnoreCase("")) {
            return result;
        }
        if (fPrefix.equalsIgnoreCase("sqrt")) {
            result = Math.sqrt(result);
        } else if (fPrefix.equalsIgnoreCase("sin")) {
            result = sin(result);
        } else if (fPrefix.equalsIgnoreCase("sinh")) {
            result = sinh(result);
        } else if (fPrefix.equalsIgnoreCase("asin")) {
            result = asin(result);
        } else if (fPrefix.equalsIgnoreCase("asinh")) {
            result = asinh(result);
        } else if (fPrefix.equalsIgnoreCase("cos")) {
            result = cos(result);
        } else if (fPrefix.equalsIgnoreCase("cosh")) {
            result = cosh(result);
        } else if (fPrefix.equalsIgnoreCase("acos")) {
            result = acos(result);
        } else if (fPrefix.equalsIgnoreCase("acosh")) {
            result = acosh(result);
        } else if (fPrefix.equalsIgnoreCase("tan")) {
            result = tan(result);
        } else if (fPrefix.equalsIgnoreCase("tanh")) {
            result = tanh(result);
        } else if (fPrefix.equalsIgnoreCase("atan")) {
            result = atan(result);
        } else if (fPrefix.equalsIgnoreCase("atanh")) {
            result = atanh(result);
        } else if (fPrefix.equalsIgnoreCase("cot")) {
            result = cot(result);
        } else if (fPrefix.equalsIgnoreCase("coth")) {
            result = coth(result);
        } else if (fPrefix.equalsIgnoreCase("acot")) {
            result = acot(result);
        } else if (fPrefix.equalsIgnoreCase("acoth")) {
            result = acoth(result);
        } else if (fPrefix.equalsIgnoreCase("sec")) {
            result = sec(result);
        } else if (fPrefix.equalsIgnoreCase("sech")) {
            result = sech(result);
        } else if (fPrefix.equalsIgnoreCase("asec")) {
            result = asec(result);
        } else if (fPrefix.equalsIgnoreCase("asech")) {
            result = asech(result);
        } else if (fPrefix.equalsIgnoreCase("csc")) {
            result = csc(result);
        } else if (fPrefix.equalsIgnoreCase("csch")) {
            result = csch(result);
        } else if (fPrefix.equalsIgnoreCase("acsc")) {
            result = acsc(result);
        } else if (fPrefix.equalsIgnoreCase("acsch")) {
            result = acsch(result);
        } else if (fPrefix.equalsIgnoreCase("log2")) {
            result = log2(result);
        } else if (fPrefix.equalsIgnoreCase("log")) {
            result = log(result);
        } else if (fPrefix.equalsIgnoreCase("ln")) {
            result = ln(result);
        } else if (fPrefix.equalsIgnoreCase("exp")) {
            result = exp(result);
        } else if (fPrefix.equalsIgnoreCase("abs")) {
            result = abs(result);
        } else if (fPrefix.equalsIgnoreCase("ceil")) {
            result = _ceil(result);
        } else if (fPrefix.equalsIgnoreCase("floor")) {
            result = _floor(result);
        } else if (fPrefix.equalsIgnoreCase("round")) {
            result = _round(result);
        } else if (fPrefix.equalsIgnoreCase("lint")) {
            result = _ceil(result);
        } else if (fPrefix.equalsIgnoreCase("gint")) {
            result = _floor(result);
        }
        return result;
    }

    public static double log2(double x) {
        return ln(x) / ln(2.0d);
    }

    public static double log(double x) {
        return ln(x) / ln(10.0d);
    }

    public static double ln(double x) {
        return Math.log(x);
    }

    public static double exp(double x) {
        return Math.pow(2.718281828459045d, x);
    }

    public static double power(double x, double r) {
        if (x < 0.0d) {
            int i = 3;
            while (i < 100) {
                if (((double) i) * r != ((double) Math.round(((double) i) * r))) {
                    i += 2;
                } else if ((((double) i) * r) % 2.0d == 0.0d) {
                    return Math.pow(-x, r);
                } else {
                    return -Math.pow(-x, r);
                }
            }
        }
        return Math.pow(x, r);
    }

    public double powerTylor(double x, double r) {
        int n = numTerms(x, r);
        double nFac = factorial(n);
        double rTerm = r;
        double xTerm = x - 1.0d;
        double iFac = 1.0d;
        double result = 0.0d;
        for (int i = 1; i <= n; i++) {
            iFac *= (double) i;
            result += (nFac / iFac) * rTerm * xTerm;
            rTerm *= r - ((double) i);
            xTerm *= x - 1.0d;
        }
        return 1.0d + (result / nFac);
    }

    public Hashtable roots(double a, double b) {
        return roots(a, b, (b - a) / 20.0d);
    }

    public Hashtable roots(double a, double b, double intv) {
        Function Df = differentiate(derVar);
        double dx = intv;
        double x1 = a;
        double x2 = x1 + dx;
        double y1 = valueAt(derVar, x1);
        double y2 = valueAt(derVar, x2);
        do {
            if (abs(y1) < 1.0E-12d) {
                this.zeros.put(" " + roundD(x1, 4), "");
            } else if (y1 * y2 < 0.0d) {
                double r = newton(Df, x2);
                if (r >= a && r <= b) {
                    this.zeros.put(" " + roundD(r, 4), "");
                }
            }
            x1 = x2;
            y1 = y2;
            x2 = x1 + dx;
            y2 = valueAt(derVar, x2);
        } while (x2 <= b + dx);
        if (abs(valueAt(derVar, a)) < 1.0E-12d) {
            this.zeros.put(" " + roundD(a, 4), "");
        }
        if (abs(valueAt(derVar, b)) < 1.0E-12d) {
            this.zeros.put(" " + roundD(b, 4), "");
        }
        return this.zeros;
    }

    public Hashtable rootsOther(double a, double b, double intv) {
        Function Df = differentiate(derVar);
        double dx = intv;
        double x1 = a;
        double y1 = valueAt(derVar, x1);
        do {
            if (abs(y1) < 0.01d) {
                double r = newton(Df, x1);
                if (r >= a && r <= b) {
                    this.zeros.put(" " + roundD(r, 4), "");
                }
            }
            x1 += dx;
            y1 = valueAt(derVar, x1);
        } while (x1 <= b + dx);
        return this.zeros;
    }

    public double newton(Function Df, double a) {
        double value = valueAt(derVar, a);
        if (Df.equals(zero())) {
            return Double.NaN;
        }
        int k = 0;
        while (abs(value) >= 1.0E-12d && abs(value) <= 100.0d) {
            int k2 = k + 1;
            if (k > 20) {
                return Double.NaN;
            }
            double dValue = Df.valueAt(derVar, a);
            if (abs(dValue) != 0.0d) {
                a -= value / dValue;
            } else {
                a += 0.1d;
            }
            value = valueAt(derVar, a);
            k = k2;
        }
        return a;
    }

    public double newton(double a) {
        return newton(differentiate(derVar), a);
    }

    public static double sin(double x) {
        if (abs(x % 3.141592653589793d) <= 1.0E-12d) {
            return 0.0d;
        }
        return Math.sin(x);
    }

    public static double asin(double x) {
        return Math.asin(x);
    }

    public static double sinh(double x) {
        return (exp(x) - exp(-x)) / 2.0d;
    }

    public static double asinh(double x) {
        return ln(Math.sqrt((x * x) + 1.0d) + x);
    }

    public static double cos(double x) {
        if (abs((x - 1.5707963267948966d) % 3.141592653589793d) <= 1.0E-12d) {
            return 0.0d;
        }
        return Math.cos(x);
    }

    public static double acos(double x) {
        return Math.acos(x);
    }

    public static double cosh(double x) {
        return (exp(x) + exp(-x)) / 2.0d;
    }

    public static double acosh(double x) {
        return ln(Math.sqrt((x * x) - 1.0d) + x);
    }

    public static double tan(double x) {
        return sin(x) / cos(x);
    }

    public static double atan(double x) {
        return Math.atan(x);
    }

    public static double tanh(double x) {
        return (1.0d - exp(-2.0d * x)) / (exp(-2.0d * x) + 1.0d);
    }

    public static double atanh(double x) {
        return 0.5d * ln((1.0d + x) / (1.0d - x));
    }

    public static double cot(double x) {
        return 1.0d / tan(x);
    }

    public static double acot(double x) {
        double inv = atan(1.0d / x);
        if (x >= 0.0d) {
            return inv;
        }
        return 3.141592653589793d + inv;
    }

    public static double coth(double x) {
        return 1.0d / tanh(x);
    }

    public static double acoth(double x) {
        return atanh(1.0d / x);
    }

    public static double sec(double x) {
        return 1.0d / cos(x);
    }

    public static double asec(double x) {
        return acos(1.0d / x);
    }

    public static double sech(double x) {
        return 1.0d / cosh(x);
    }

    public static double asech(double x) {
        return acosh(1.0d / x);
    }

    public static double csc(double x) {
        return 1.0d / sin(x);
    }

    public static double acsc(double x) {
        return asin(1.0d / x);
    }

    public static double csch(double x) {
        return 1.0d / sinh(x);
    }

    public static double acsch(double x) {
        return asinh(1.0d / x);
    }

    private double floor(double x) {
        return _floor(x);
    }

    private double ceil(double x) {
        return _ceil(x);
    }

    public double round(double x) {
        return _round(x);
    }

    private static double _floor(double x) {
        double gint = Math.floor(x);
        if (isPosInt(gint - stepValue)) {
            jump = true;
        } else {
            jump = false;
        }
        stepValue = gint;
        return gint;
    }

    private static double _ceil(double x) {
        return _floor(1.0d + x);
    }

    private static double _round(double x) {
        return _floor(0.5d + x);
    }

    public static double abs(double x) {
        return Math.abs(x);
    }

    public static int mod(int x, int y) {
        int r = x % y;
        return r >= 0 ? r : r + y;
    }

    public static double mod(double x, double y) {
        double r = x % y;
        return r >= 0.0d ? r : r + y;
    }

    public static double norm(int x, int y) {
        return Math.sqrt((double) ((x * x) + (y * y)));
    }

    public static double norm(Point p) {
        return norm(p.x, p.y);
    }

    public static Vector elementalSubExpr(String fStr) {
        Vector subExpr = new Vector();
        boolean done = false;
        while (!done) {
            int pL = 0;
            int pR = 0;
            int i = 0;
            while (true) {
                if (i >= fStr.length()) {
                    break;
                }
                if (fStr.charAt(i) == '(') {
                    pL++;
                } else if (fStr.charAt(i) == ')') {
                    pR++;
                }
                if (pL == pR) {
                    if (i <= fStr.length() - 1) {
                        subExpr.addElement(fStr.substring(0, i + 2));
                        fStr = fStr.substring(i + 2);
                        break;
                    }
                    subExpr.addElement(fStr);
                    done = true;
                }
                i++;
            }
        }
        return subExpr;
    }

    public static int indexOfMatchingRP(String fStr) {
        return indexOfMatchingRP(fStr, 0);
    }

    public static int indexOfMatchingRP(String fStr, int index) {
        int indexLP = fStr.indexOf(40, index);
        if (indexLP < 0) {
            return -1;
        }
        int numLP = 1;
        int numRP = 0;
        int i = indexLP + 1;
        while (numLP > numRP) {
            if (fStr.charAt(i) == '(') {
                numLP++;
            } else if (fStr.charAt(i) == ')') {
                numRP++;
            }
            i++;
        }
        int indexMRP = i - 1;
        if (indexMRP == fStr.length() - 1 && fStr.indexOf(40) == 0) {
            return -2;
        }
        return indexMRP;
    }

    public static String applyDiffRules(String fStr) {
        int index = indexOfMatchingRP(fStr);
        if (index == -2) {
            return diffSimpleExpr(fStr.substring(1, fStr.length()));
        }
        if (index == -1) {
            return diffSimpleExpr(fStr);
        }
        return diffComplexExpr(fStr);
    }

    public static String diffSimpleExpr(String fStr) {
        return "Simple Expression";
    }

    public static String diffComplexExpr(String fStr) {
        return "Complex Complex";
    }

    public static String zero(String fStr) {
        return "(0)";
    }

    public static String one(String fStr) {
        return "(1)";
    }

    public static String zero() {
        return "0";
    }

    public static String one() {
        return "1";
    }

    public String makeTokenizable() {
        return makeTokenizable(this.forDiff.substring(1, this.forDiff.length() - 1));
    }

    public static String makeTokenizable(String fStr) {
        int indexR;
        StringBuffer fBfr = new StringBuffer();
        StringBuffer subBfr = new StringBuffer(fStr);
        int indexR2 = indexOfMatchingRP(subBfr.toString());
        if (indexR2 == -2) {
            return makeTokenizable(subBfr.toString().substring(1, subBfr.length() - 1));
        }
        if (indexR2 < 0) {
            return fStr;
        }
        do {
            indexR = indexOfMatchingRP(subBfr.toString());
            if (indexR < 0) {
                return fBfr.append(subBfr).toString();
            }
            int indexL = subBfr.toString().indexOf("(");
            fBfr.append(subBfr.toString().substring(0, indexL));
            fBfr.append(replaceOps(subBfr.toString().substring(indexL, indexR + 1)));
            setBuffer(subBfr, subBfr.toString().substring(indexR + 1));
        } while (indexR > 0);
        return fBfr.toString();
    }

    public static String makeTokenizable1(String fStr) {
        StringBuffer fBfr = new StringBuffer();
        StringBuffer subBfr = new StringBuffer(fStr);
        int indexR = indexOfMatchingRP(subBfr.toString());
        if (indexR == -2) {
            return makeTokenizable(subBfr.toString().substring(1, subBfr.length() - 1));
        }
        if (indexR < 0) {
            return fStr;
        }
        while (indexR > 0) {
            if (indexR < 0) {
                return fBfr.append(subBfr).toString();
            }
            int indexL = subBfr.toString().indexOf("(");
            fBfr.append(subBfr.toString().substring(0, indexL));
            fBfr.append(replaceOps(subBfr.toString().substring(indexL, indexR + 1)));
            setBuffer(subBfr, subBfr.toString().substring(indexR + 1));
            indexR = indexOfMatchingRP(subBfr.toString());
        }
        return fBfr.toString();
    }

    public static String makeTokenizableStr(String str) {
        return makeTokenizable(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String replaceOps(String fStr) {
        StringBuffer fBfr = new StringBuffer(fStr.replace('+', 'A'));
        setBuffer(fBfr, fBfr.toString().replace('~', 'B'));
        setBuffer(fBfr, fBfr.toString().replace('*', 'C'));
        setBuffer(fBfr, fBfr.toString().replace('/', 'D'));
        setBuffer(fBfr, fBfr.toString().replace('^', 'E'));
        return fBfr.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static String replaceByOps(String fStr) {
        StringBuffer fBfr = new StringBuffer(fStr.replace('A', '+'));
        setBuffer(fBfr, fBfr.toString().replace('B', '~'));
        setBuffer(fBfr, fBfr.toString().replace('C', '*'));
        setBuffer(fBfr, fBfr.toString().replace('D', '/'));
        setBuffer(fBfr, fBfr.toString().replace('E', '^'));
        return fBfr.toString();
    }

    public Function differentiate() {
        return differentiate(derVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public Function differentiate(char var) {
        derVar = var;
        String d = simplifyDer(diff(this.forDiff).replace('~', '-'));
        Function Df = new Function("1");
        Df.setUp(d);
        return Df;
    }

    public static String differentiate(String fStr) {
        return differentiate(fStr, derVar);
    }

    public static String differentiate(String fStr, char var) {
        return new Function(fStr).differentiate(var).entered();
    }

    public String diff(String fStr) {
        if (!hasVar(fStr, derVar)) {
            return zero();
        }
        if (isConstExpr(fStr)) {
            return zero();
        }
        if (fStr.equals(new Character(derVar).toString())) {
            return one();
        }
        if (fStr.equals("-" + new Character(derVar).toString())) {
            return "-" + one();
        }
        if (indexOfMatchingRP(fStr.substring(1)) == -2) {
            return "-" + encloseDer(diff(fStr.substring(1)));
        }
        if (indexOfMatchingRP(fStr) == -2) {
            return diff(fStr.substring(1, fStr.length() - 1));
        }
        if (makeTokenizable(fStr).indexOf("+") > 0 || makeTokenizable(fStr).indexOf("~") > 0) {
            return applySumRule(fStr);
        }
        if (makeTokenizable(fStr).indexOf("*") > 0) {
            return applyProductRule(fStr);
        }
        if (makeTokenizable(fStr).indexOf("/") > 0) {
            return applyQoutientRule(fStr);
        }
        if (makeTokenizable(fStr).indexOf("^") > 0) {
            return applyPowerRule(fStr);
        }
        if (isComposite(fStr)) {
            return applyChainRule(fStr);
        }
        return String.valueOf(fStr) + " : case not handeled";
    }

    public boolean equals(String fStr) {
        return equal(formula(), enclose(fStr));
    }

    public static boolean equal(String fStr1, String fStr2) {
        if (constValue(String.valueOf(fStr1) + "-" + enclose(fStr2)) == 0.0d) {
            return true;
        }
        return false;
    }

    public boolean isPositive(double a, double b) {
        for (int i = 0; i < 20; i++) {
            if (valueAt(derVar, ((b - a) * Math.random()) + a) < 0.0d) {
                return false;
            }
        }
        return true;
    }

    public boolean isNegative(double a, double b) {
        for (int i = 0; i < 20; i++) {
            if (valueAt(derVar, ((b - a) * Math.random()) + a) > 0.0d) {
                return false;
            }
        }
        return true;
    }

    public boolean isUndefined(double a) {
        if (Double.isNaN(valueAt(a)) || Double.isInfinite(valueAt(a))) {
            return true;
        }
        return false;
    }

    public boolean isUndefined(double a, double b) {
        double seedValue;
        for (int i = 0; i < 20; i++) {
            if (a == Double.NEGATIVE_INFINITY || b == Double.POSITIVE_INFINITY) {
                seedValue = Math.random();
            } else if (a == Double.NEGATIVE_INFINITY) {
                seedValue = b + ln(Math.random());
            } else if (b == Double.POSITIVE_INFINITY) {
                seedValue = (b - ln(Math.random())) + b;
            } else {
                seedValue = valueAt(derVar, ((b - a) * Math.random()) + a);
            }
            if (!Double.isNaN(seedValue) && !Double.isInfinite(seedValue)) {
                return false;
            }
        }
        return true;
    }

    public boolean isUndefined1() {
        for (int i = 0; i < 20; i++) {
            if (Double.isNaN(valueAt(Math.random()))) {
                return true;
            }
        }
        return false;
    }

    public boolean isUndefined() {
        return isUndefined(Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY);
    }

    public double undefinedMax(double a) {
        if (isUndefined()) {
            return Double.POSITIVE_INFINITY;
        }
        if (!isUndefined(a)) {
            return a;
        }
        double c = a;
        double d = a + 0.5d;
        while (isUndefined(c, d)) {
            c += 0.5d;
            d += 0.5d;
        }
        double c2 = d - 0.5d;
        double d2 = c2 + 0.01d;
        while (isUndefined(c2, d2)) {
            c2 += 0.01d;
            d2 += 0.01d;
        }
        return d2 - 0.01d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public String simplifyDer(String fStr) {
        Function fn = new Function("1");
        fn.setUp(fStr.replace('~', '-'));
        if (!isConstExpr(fn.expression)) {
            return fStr;
        }
        double value = fn.valueAt((Math.random() * 1000.0d) - 500.0d, (Math.random() * 1000.0d) - 500.0d);
        while (true) {
            if (!Double.isNaN(value) && !Double.isInfinite(value)) {
                break;
            }
            value = fn.valueAt((Math.random() * 1000.0d) - 500.0d, (Math.random() * 1000.0d) - 500.0d);
        }
        if (isInt(value)) {
            return new StringBuilder().append((int) value).toString();
        }
        return new StringBuilder().append(value).toString();
    }

    public static boolean isConstExpr(String fStr) {
        if (isConstExpr(fStr, 10.0d)) {
            return isConstExpr(fStr, 50.0d);
        }
        return false;
    }

    public static boolean isConstExpr(String fStr, double d) {
        boolean isConst = false;
        Function fn = new Function("1");
        fn.setUp(replaceNegs(fStr));
        double randValue = fn.valueAt(((2.0d * d) * Math.random()) - d, ((2.0d * d) * Math.random()) - d);
        int i = 0;
        while (true) {
            double seedValue = randValue;
            if (i >= 20) {
                return isConst;
            }
            if (Double.isNaN(seedValue) || Double.isInfinite(seedValue)) {
                randValue = fn.valueAt(((2.0d * d) * Math.random()) - d, ((2.0d * d) * Math.random()) - d);
            } else {
                double randValue2 = fn.valueAt(((2.0d * d) * Math.random()) - d, ((2.0d * d) * Math.random()) - d);
                if (Double.isNaN(randValue2)) {
                    randValue = seedValue;
                } else if (Double.isInfinite(seedValue)) {
                    randValue = seedValue;
                } else if (Math.abs(randValue2 - seedValue) >= 1.0E-12d) {
                    return false;
                } else {
                    isConst = true;
                    randValue = seedValue;
                }
            }
            i++;
        }
    }

    public static double constValue(String fStr) {
        if (!isConstExpr(fStr)) {
            return Double.NaN;
        }
        Function fn = new Function("1");
        fn.setUp(replaceNegs(fStr));
        double value = fn.valueAt((Math.random() * 1000.0d) - 500.0d, (Math.random() * 1000.0d) - 500.0d);
        while (true) {
            if (!Double.isNaN(value) && !Double.isInfinite(value)) {
                return value;
            }
            value = fn.valueAt((Math.random() * 1000.0d) - 500.0d, (Math.random() * 1000.0d) - 500.0d);
        }
    }

    private String applySumRule(String fStr) {
        StringTokenizer tokenizer = new StringTokenizer(makeTokenizable(fStr), "+~", true);
        StringBuffer bfr = new StringBuffer();
        StringBuffer dTerm = new StringBuffer();
        while (tokenizer.hasMoreTokens()) {
            dTerm.setLength(0);
            dTerm.append(encloseDer(diff(replaceByOps(tokenizer.nextToken()))));
            if (!dTerm.toString().equals(zero()) && !dTerm.toString().equals("-" + zero())) {
                bfr.append(dTerm.toString());
            } else if (bfr.length() > 0) {
                bfr.setLength(bfr.length() - 1);
            }
            if (tokenizer.hasMoreTokens()) {
                bfr.append(tokenizer.nextToken());
            }
        }
        if (bfr.toString().startsWith("+")) {
            setBuffer(bfr, bfr.toString().substring(1));
        } else if (bfr.toString().equals("")) {
            setBuffer(bfr, "0");
        }
        return bfr.toString();
    }

    private String applyProductRuleSimplify(String fStr) {
        int indexMult = makeTokenizable(fStr).indexOf("*");
        String p1 = fStr.substring(0, indexMult);
        String p2 = fStr.substring(indexMult + 1);
        String dp1 = diff(p1);
        String dp2 = diff(p2);
        String term1 = String.valueOf(encloseDer(dp1)) + "*" + p2;
        String term2 = "+" + encloseDer(p1) + "*" + dp2;
        if (dp1.equals(p2)) {
            term1 = String.valueOf(encloseNeg(p2)) + "^2";
        } else if (dp1.equals("-" + p2) || p2.equals("-" + dp1)) {
            term1 = "-" + encloseNeg(p2) + "^2";
        }
        if (p1.equals(dp2)) {
            term2 = "+" + encloseNeg(p1) + "^2";
        } else if (p1.equals("-" + dp2) || dp2.equals("-" + p1)) {
            term2 = "-" + encloseNeg(p1) + "^2";
        }
        if (dp1.equals(zero())) {
            if (dp2.equals(one())) {
                return p1;
            }
            return String.valueOf(p1) + "*" + encloseDer(dp2);
        } else if (dp2.equals(zero())) {
            return dp1.equals(one()) ? p2 : String.valueOf(encloseDer(dp1)) + "*" + p2;
        } else {
            if (dp1.equals(one()) && dp2.equals(one())) {
                return String.valueOf(p1) + "+" + p2;
            }
            if (dp1.equals(one())) {
                return String.valueOf(p2) + "+" + p1 + "*" + encloseDer(dp2);
            }
            if (dp2.equals(one())) {
                return String.valueOf(encloseDer(dp1)) + p2 + "+" + p1;
            }
            return String.valueOf(term1) + term2;
        }
    }

    private String applyProductRule(String fStr) {
        int indexMult = makeTokenizable(fStr).indexOf("*");
        String p1 = fStr.substring(0, indexMult);
        String p2 = fStr.substring(indexMult + 1);
        String dp1 = diff(p1);
        String dp2 = diff(p2);
        if (dp1.equals(zero())) {
            if (dp2.equals(one())) {
                return p1;
            }
            return String.valueOf(p1) + "*" + encloseDer(dp2);
        } else if (dp2.equals(zero())) {
            return dp1.equals(one()) ? p2 : String.valueOf(encloseDer(dp1)) + "*" + p2;
        } else {
            if (dp1.equals(one()) && dp2.equals(one())) {
                return String.valueOf(p1) + "+" + p2;
            }
            if (dp1.equals(one())) {
                return String.valueOf(p2) + "+" + p1 + "*" + encloseDer(dp2);
            }
            if (dp2.equals(one())) {
                return String.valueOf(encloseDer(dp1)) + p2 + "+" + p1;
            }
            return String.valueOf(encloseDer(dp1)) + "*" + p2 + "+" + p1 + "*" + encloseDer(dp2);
        }
    }

    private String applyQoutientRule(String fStr) {
        int indexQut = makeTokenizable(fStr).lastIndexOf("/");
        String p1 = fStr.substring(0, indexQut);
        String p2 = fStr.substring(indexQut + 1);
        String dp1 = diff(p1);
        String dp2 = diff(p2);
        if (dp1.equals(zero())) {
            if (dp2.equals(one())) {
                return "-" + p1 + "/" + encloseNeg(p2) + "^2";
            }
            return "-" + p1 + "*" + encloseDer(dp2) + "/" + encloseNeg(p2) + "^2";
        } else if (dp2.equals(zero())) {
            return dp1.equals(one()) ? "1/" + p2 : String.valueOf(encloseDer(dp1)) + "/" + p2;
        } else {
            if (dp1.equals(one()) && dp2.equals(one())) {
                return "(" + p2 + "-" + p1 + ")/" + encloseNeg(p2) + "^2";
            }
            if (dp1.equals(one())) {
                return "(" + p2 + "-" + p1 + "*" + encloseDer(dp2) + ")/" + encloseNeg(p2) + "^2";
            }
            if (dp2.equals(one())) {
                return "(" + encloseDer(dp1) + "*" + p2 + "-" + p1 + ")/" + p2 + "^2";
            }
            return "(" + encloseDer(dp1) + "*" + p2 + "-" + p1 + "*" + encloseDer(dp2) + ")/" + encloseNeg(p2) + "^2";
        }
    }

    private String applyPowerRule(String fStr) {
        int indexCarr = makeTokenizable(fStr).lastIndexOf("^");
        String base = fStr.substring(0, indexCarr);
        if (base.startsWith("-")) {
            return "-" + diff(fStr.substring(1));
        }
        String pow = fStr.substring(indexCarr + 1);
        String dBase = encloseDer(diff(base));
        String dPow = encloseDer(diff(pow));
        String mdb = dBase.equals(one()) ? "" : "*" + dBase;
        if (!dBase.equals(one())) {
            String str = String.valueOf(dBase) + "*";
        }
        String mdp = dPow.equals(one()) ? "" : "*" + dPow;
        String dpm = dPow.equals(one()) ? "" : String.valueOf(dPow) + "*";
        if (isConstExpr(pow)) {
            try {
                double powD = parseD(pow);
                if (powD == 0.0d) {
                    return "0";
                }
                if (powD == 1.0d) {
                    return dBase;
                }
                if (powD == 2.0d) {
                    return "2" + base + mdb;
                }
                if (isInt(powD)) {
                    return String.valueOf(pow) + base + "^" + ((int) (powD - 1.0d)) + mdb;
                }
                return String.valueOf(pow) + base + "^" + roundD(powD - 1.0d, 4) + mdb;
            } catch (NumberFormatException e2) {
                return String.valueOf(pow) + base + "^(" + pow + "-1)" + mdb;
            }
        } else if (!isConstExpr(base)) {
            String pbase = enclose(base);
            if (equal(base, pow)) {
                return String.valueOf(fStr) + encloseDer(String.valueOf(dBase) + "+" + "ln" + pbase + mdp);
            }
            return String.valueOf(fStr) + encloseDer(String.valueOf(pow) + mdb + "/" + base + "+ln" + pbase + mdp);
        } else if (new Function(replaceNegs(base)).calculate() == 2.718281828459045d) {
            return String.valueOf(dpm) + fStr;
        } else {
            if (indexOfMatchingRP(base) == -2) {
                return "ln" + base + "*" + dpm + fStr;
            }
            return "ln(" + base + ")" + "*" + dpm + fStr;
        }
    }

    public static boolean isComposite(String fStr) {
        int indexLP = fStr.indexOf(40);
        if (indexLP <= 0 || indexOfMatchingRP(fStr.substring(indexLP)) != -2) {
            return false;
        }
        return true;
    }

    private String applyChainRule(String fStr) {
        if (fStr.startsWith("-")) {
            return "-" + encloseDer(diff(fStr.substring(1)));
        }
        int indexLP = makeTokenizable(fStr).indexOf("(");
        return derPrefix(fStr.substring(0, indexLP), fStr.substring(indexLP));
    }

    public String derPrefix(String prefix, String arg) {
        String dd;
        String pArg;
        String sDerVar = new StringBuilder().append(new Character(derVar)).toString();
        String dArg = encloseDer(diff(arg));
        String dm = dArg.equals(one()) ? "" : String.valueOf(dArg) + "*";
        if (dArg.equals(one())) {
            dd = "1";
        } else {
            dd = dArg;
        }
        String md = dArg.equals(one()) ? "" : "*" + dArg;
        if (arg.equals(enclose(sDerVar))) {
            pArg = sDerVar;
        } else {
            pArg = arg;
        }
        if (prefix.equalsIgnoreCase("sin")) {
            return "cos" + arg + md;
        }
        if (prefix.equalsIgnoreCase("cos")) {
            return "-sin" + arg + md;
        }
        if (prefix.equalsIgnoreCase("tan")) {
            return "sec" + arg + "^2" + md;
        }
        if (prefix.equalsIgnoreCase("sec")) {
            return "sec" + arg + "tan" + arg + md;
        }
        if (prefix.equalsIgnoreCase("csc")) {
            return "-csc" + arg + "cot" + arg + md;
        }
        if (prefix.equalsIgnoreCase("cot")) {
            return "-csc" + arg + "^2" + md;
        }
        if (prefix.equalsIgnoreCase("asin")) {
            return String.valueOf(dd) + "/sqrt(1-" + pArg + "^2)";
        }
        if (prefix.equalsIgnoreCase("acos")) {
            return "-" + dd + "/sqrt(1-" + pArg + "^2)";
        }
        if (prefix.equalsIgnoreCase("atan")) {
            return String.valueOf(dd) + "/(" + pArg + "^2+1)";
        }
        if (prefix.equalsIgnoreCase("asec")) {
            return String.valueOf(dd) + "/(abs(" + pArg + ")sqrt(" + pArg + "^2-1))";
        }
        if (prefix.equalsIgnoreCase("acsc")) {
            return "-" + dd + "/(abs(" + pArg + ")sqrt(" + pArg + "^2-1))";
        }
        if (prefix.equalsIgnoreCase("acot")) {
            return "-" + dd + "/(" + pArg + "^2+1)";
        }
        if (prefix.equalsIgnoreCase("sinh")) {
            return "cosh" + arg + md;
        }
        if (prefix.equalsIgnoreCase("cosh")) {
            return "sinh" + arg + md;
        }
        if (prefix.equalsIgnoreCase("tanh")) {
            return "sech" + arg + "^2" + md;
        }
        if (prefix.equalsIgnoreCase("sech")) {
            return "-sech" + arg + "tanh" + arg + md;
        }
        if (prefix.equalsIgnoreCase("csch")) {
            return "-csc" + arg + "coth" + arg + md;
        }
        if (prefix.equalsIgnoreCase("coth")) {
            return "-csch" + arg + "^2" + md;
        }
        if (prefix.equalsIgnoreCase("asinh")) {
            return String.valueOf(dd) + "/sqrt(" + pArg + "^2+1)";
        }
        if (prefix.equalsIgnoreCase("acosh")) {
            return String.valueOf(dd) + "/sqrt(" + pArg + "^2-1)";
        }
        if (prefix.equalsIgnoreCase("atanh")) {
            return String.valueOf(dd) + "/(1-" + pArg + "^2)";
        }
        if (prefix.equalsIgnoreCase("asech")) {
            return "-" + dd + "/(" + pArg + "sqrt(1-" + pArg + "^2))";
        }
        if (prefix.equalsIgnoreCase("acsch")) {
            return "-" + dd + "/(abs(" + pArg + ")sqrt(" + pArg + "^2+1))";
        }
        if (prefix.equalsIgnoreCase("acoth")) {
            return String.valueOf(dd) + "/(1-" + pArg + "^2)";
        }
        if (prefix.equalsIgnoreCase("ln")) {
            return String.valueOf(dd) + "/" + pArg;
        }
        if (prefix.equalsIgnoreCase("log")) {
            return String.valueOf(dd) + "/" + enclose("ln(10)" + arg);
        }
        if (prefix.equalsIgnoreCase("log2")) {
            return String.valueOf(dd) + "/" + enclose("ln(2)" + arg);
        }
        if (prefix.equalsIgnoreCase("exp")) {
            return String.valueOf(dm) + "exp" + arg;
        }
        if (prefix.equalsIgnoreCase("sqrt")) {
            return "0.5*" + dm + pArg + "^-0.5";
        }
        if (prefix.equalsIgnoreCase("abs")) {
            return String.valueOf(dm) + "abs" + arg + "/" + pArg;
        }
        if (prefix.equalsIgnoreCase("floor")) {
            return zero();
        }
        if (prefix.equalsIgnoreCase("ceil")) {
            return zero();
        }
        if (prefix.equalsIgnoreCase("round")) {
            return zero();
        }
        if (prefix.equalsIgnoreCase("gint")) {
            return zero();
        }
        if (prefix.equalsIgnoreCase("lint")) {
            return zero();
        }
        return "apply chain rule for prefix";
    }

    public static String getCopyRightInfo() {
        return "Copyright © 2003-5 AMIR  Applied Mathematics Internet Resources. All rights reserved";
    }
}
