package com.calculatorgrapher.math;

public class InvalidFunctionExpressionException extends RuntimeException {
    private String after;
    private String before;
    private String err;

    public InvalidFunctionExpressionException() {
    }

    public InvalidFunctionExpressionException(String message) {
        super(message);
    }

    public InvalidFunctionExpressionException(String message, String err2) {
        this(message, err2, "");
    }

    public InvalidFunctionExpressionException(String message, String err2, String before2) {
        this(message, err2, before2, "");
    }

    public InvalidFunctionExpressionException(String message, String err2, String before2, String after2) {
        this(message);
        this.err = err2;
        this.before = before2;
        this.after = after2;
    }

    public String getErr() {
        return this.err;
    }

    public String getBefore() {
        return this.before;
    }

    public String getAfter() {
        return this.after;
    }
}
