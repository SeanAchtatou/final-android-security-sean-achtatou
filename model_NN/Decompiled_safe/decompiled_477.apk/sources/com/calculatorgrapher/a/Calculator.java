package com.calculatorgrapher.a;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.calculatorgrapher.math.Function;
import com.calculatorgrapher.math.InvalidFunctionExpressionException;

public class Calculator extends Activity {
    static final int DELETE_CHAR_TO_LEFT = -1;
    static final int MOVE_CURSOR_TO_LEFT = -1;
    String arrowUp = "↑";
    private String blank = "   ";
    Button bt0;
    Button bt1;
    Button bt2;
    Button bt3;
    Button bt4;
    Button bt5;
    Button bt6;
    Button bt7;
    Button bt8;
    Button bt9;
    Button btBackSpace;
    Button btCloseGraph;
    Button btEq;
    Button btLP;
    Button btMore;
    Button btMul;
    Button btMult;
    Button btPlus;
    Button btPt;
    Button btRP;
    Button btSin;
    CheckBox cbHyp;
    CheckBox cbInv;
    private boolean deletingChar = true;
    private Display display;
    /* access modifiers changed from: private */
    public String errMessage = "";
    EditText etExpr;
    Function f = new Function("1");
    GraphCanvas gCanvas;
    int h;
    private boolean isGraphFullScreen;
    private boolean isScientific = true;
    boolean isSmallScrn;
    /* access modifiers changed from: private */
    public String lastExpr = "";
    int leftArrowFn;
    private int more;
    private View pGraph;
    LinearLayout pGraphCanvas;
    private View pMainKeys;
    LinearLayout pMainKeys_;
    private View pResult;
    private View pTrans;
    Color royalBlue;
    private int scrnHeight;
    private int scrnWidth;
    double t0;
    double t1;
    TextView tvResult;
    String upgradeURL = "http://www.calculator-grapher.com/mobile/index.html";
    Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Sci_Calc   www.calculator-grapher.com");
        this.display = getWindowManager().getDefaultDisplay();
        this.scrnWidth = this.display.getWidth();
        this.scrnHeight = this.display.getHeight();
        this.isSmallScrn = this.scrnHeight < 540;
        this.btCloseGraph = new Button(this);
        if (this.isSmallScrn) {
            setContentView((int) R.layout.main_s);
        } else {
            setContentView((int) R.layout.main);
        }
        this.gCanvas = new GraphCanvas(this);
        this.pGraph = findViewById(R.id.pGraph);
        this.pResult = findViewById(R.id.pResult_Eq);
        this.pMainKeys = findViewById(R.id.pMainKeys);
        this.pGraphCanvas = (LinearLayout) findViewById(R.id.pGraphCanvas);
        this.pGraphCanvas.addView(this.gCanvas);
        this.pGraphCanvas.setVisibility(8);
        this.pTrans = findViewById(R.id.pTrans);
        this.etExpr = (EditText) findViewById(R.id.etExpr);
        this.btBackSpace = (Button) findViewById(R.id.btBackSpace);
        this.btEq = (Button) findViewById(R.id.btEq);
        this.btMore = (Button) findViewById(R.id.btMore);
        this.btLP = (Button) findViewById(R.id.btLP);
        this.btRP = (Button) findViewById(R.id.btRP);
        this.bt1 = (Button) findViewById(R.id.bt1);
        this.bt2 = (Button) findViewById(R.id.bt2);
        this.bt3 = (Button) findViewById(R.id.bt3);
        this.bt4 = (Button) findViewById(R.id.bt4);
        this.bt5 = (Button) findViewById(R.id.bt5);
        this.bt6 = (Button) findViewById(R.id.bt6);
        this.bt7 = (Button) findViewById(R.id.bt7);
        this.bt8 = (Button) findViewById(R.id.bt8);
        this.bt9 = (Button) findViewById(R.id.bt9);
        this.bt0 = (Button) findViewById(R.id.bt0);
        this.btPt = (Button) findViewById(R.id.btPt);
        this.btSin = (Button) findViewById(R.id.btSin);
        this.btMult = (Button) findViewById(R.id.btMul);
        this.tvResult = (TextView) findViewById(R.id.tvResult);
        this.cbInv = (CheckBox) findViewById(R.id.cbInv);
        this.cbHyp = (CheckBox) findViewById(R.id.cbHyp);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.etExpr.setInputType(0);
        this.etExpr.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Calculator.this.tvResult.setText(((Object) s) + "  " + start + "  " + before + "  " + count);
                try {
                    Calculator.this.calculate();
                } catch (IllegalStateException e) {
                    Calculator.this.calculate(Calculator.this.etExpr);
                }
            }

            public void afterTextChanged(Editable s) {
            }
        });
        this.btEq.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Calculator.this.btEq.getText().equals("?")) {
                    Calculator.this.tvResult.setTextColor(-65536);
                    Calculator.this.tvResult.setText(Calculator.this.errMessage);
                    Calculator.this.btEq.setTextColor(-16776961);
                    Calculator.this.btEq.setText((int) R.string.arrowUp);
                } else if (Calculator.this.btEq.getText().equals("↑")) {
                    Calculator.this.tvResult.setText(" . . .");
                    Calculator.this.btEq.setText((int) R.string.qsMark);
                    Calculator.this.btEq.setTextColor(-16776961);
                    Calculator.this.tvResult.setTextColor(-1);
                } else if (Calculator.this.btEq.getText().equals("f'")) {
                    Calculator.this.tvResult.setText(Calculator.this.f.differentiate().formula());
                    Calculator.this.btEq.setText("= f'");
                } else {
                    Calculator.this.calculate(v);
                }
            }
        });
        this.btEq.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                if (Calculator.this.btEq.getText().toString().trim().equals("?") || Calculator.this.btEq.getText().toString().trim().equals(Calculator.this.arrowUp)) {
                    return true;
                }
                if (Function.indexOfMatchingRP(Calculator.this.etExpr.getText().toString()) == -2) {
                    return true;
                }
                String str = new StringBuilder().append((Object) Calculator.this.etExpr.getText()).toString();
                Calculator.this.etExpr.setText("");
                Calculator.this.etExpr.append(Function.enclose(str));
                return true;
            }
        });
        this.btBackSpace.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                return Calculator.this.backSpaceLong(v);
            }
        });
        this.btRP.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (new StringBuilder().append((Object) Calculator.this.etExpr.getText()).toString().toString().trim().equals("")) {
                    Calculator.this.etExpr.append(Calculator.this.lastExpr);
                } else {
                    Calculator.this.insertAC(v);
                }
            }
        });
        this.btRP.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                int end = Math.max(Calculator.this.etExpr.getSelectionStart(), Calculator.this.etExpr.getSelectionEnd());
                if (end < Calculator.this.etExpr.getText().length()) {
                    Calculator.this.etExpr.setSelection(end + 1);
                }
                Calculator.this.vibrator.vibrate(20);
                return true;
            }
        });
        this.btLP.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                int start = Math.min(Calculator.this.etExpr.getSelectionStart(), Calculator.this.etExpr.getSelectionEnd());
                int max = Math.max(Calculator.this.etExpr.getSelectionStart(), Calculator.this.etExpr.getSelectionEnd());
                if (start > 0) {
                    Calculator.this.etExpr.setSelection(start - 1);
                }
                Calculator.this.vibrator.vibrate(20);
                return true;
            }
        });
        this.bt1.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                int start = Calculator.this.etExpr.getSelectionStart();
                int end = Calculator.this.etExpr.getSelectionEnd();
                Calculator.this.etExpr.getText().replace(Math.min(start, end), Math.max(start, end), "i");
                return true;
            }
        });
        this.bt2.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt3.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt4.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt5.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt6.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt7.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt8.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt9.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.bt0.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                Calculator.this.insertAC_Long(v);
                return true;
            }
        });
        this.btPt.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                int start = Calculator.this.etExpr.getSelectionStart();
                int end = Calculator.this.etExpr.getSelectionEnd();
                Calculator.this.etExpr.getText().replace(Math.min(start, end), Math.max(start, end), "");
                return true;
            }
        });
        this.btMult.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                int start = Calculator.this.etExpr.getSelectionStart();
                int end = Calculator.this.etExpr.getSelectionEnd();
                Calculator.this.etExpr.getText().replace(Math.min(start, end), Math.max(start, end), "x");
                return true;
            }
        });
    }

    public void showGraph(View v) {
        showGraph("2.3sin(4x)");
    }

    public void showGraph(String expr) {
        this.pGraphCanvas.setVisibility(0);
        this.pMainKeys.setVisibility(8);
        findViewById(R.id.pResult_Eq).setVisibility(8);
        findViewById(R.id.pGraph).setVisibility(8);
        this.gCanvas.setFunction(expr);
        this.gCanvas.invalidate();
    }

    public void closeGrah() {
        this.pGraphCanvas.setVisibility(8);
        this.pMainKeys.setVisibility(0);
        this.pResult.setVisibility(0);
        this.pGraph.setVisibility(0);
    }

    public void closeGrah(View v) {
        closeGrah();
    }

    public void calculate() {
        calculate(this.etExpr);
    }

    public void calculate(View v) {
        this.vibrator.vibrate(30);
        this.tvResult.setTextColor(-1);
        this.btEq.setTextColor(-16776961);
        this.btEq.setText("=");
        try {
            this.f.setUpAndCheck(new StringBuilder().append((Object) this.etExpr.getText()).toString());
            if (Function.hasVar(this.f.formula(), "x")) {
                throw new InvalidFunctionExpressionException("Do you want to enter a function f(x)? Upgrade to Graphing Calculator.", "", "");
            } else if (Function.hasVar(this.f.formula(), "t")) {
                throw new InvalidFunctionExpressionException("Variables t cannot be used in numeric expressions.", "", "");
            } else if (Function.hasVar(this.f.formula(), "y")) {
                throw new InvalidFunctionExpressionException("Variables y cannot be used in numeric expressions.", "", "");
            } else if (Function.hasVar(this.f.formula(), "r")) {
                throw new InvalidFunctionExpressionException("Variables r cannot be used in numeric expressions.", "", "");
            } else if (Function.hasVar(this.f.formula(), "θ")) {
                throw new InvalidFunctionExpressionException("Variables θcannot be used in numeric expressions.", "", "");
            } else {
                double result = this.f.valueAt(0.0d);
                this.tvResult.setText(Function.format(result, 6));
                if (Double.isNaN(result)) {
                    this.tvResult.setText("Indeterminant/undefined");
                } else if (result == Double.POSITIVE_INFINITY) {
                    this.tvResult.setText(Function.posInfinity);
                } else if (result == Double.NEGATIVE_INFINITY) {
                    this.tvResult.setText(Function.negInfinity);
                }
                if (Function.hasVar(this.f.formula(), "x")) {
                    this.btEq.setText("f'");
                    this.tvResult.setText("f(0) = " + ((Object) this.tvResult.getText()));
                }
            }
        } catch (InvalidFunctionExpressionException e) {
            InvalidFunctionExpressionException ifee = e;
            this.tvResult.setText("  . . .");
            this.btEq.setTextColor(-65536);
            this.btEq.setText((int) R.string.qsMark);
            if (ifee.getErr().trim().equals("")) {
                this.errMessage = ifee.getMessage();
            } else if (ifee.getErr().trim().startsWith("i")) {
                this.errMessage = "Imaginary unit i:  \n" + ifee.getBefore() + ifee.getErr() + "\nPresently no complex numbers!";
            } else {
                this.errMessage = ifee.getMessage() + " " + ifee.getErr().trim().charAt(0) + "\n" + ifee.getBefore() + ifee.getErr();
            }
        }
    }

    public void insertAC(View v) {
        int start = this.etExpr.getSelectionStart();
        int end = this.etExpr.getSelectionEnd();
        int s = Math.min(start, end);
        int f2 = Math.max(start, end);
        String label = (String) ((Button) v).getText();
        Editable editable = this.etExpr.getText();
        if (s <= 0 || editable.toString().charAt(s - 1) != '*' || !label.equals("*")) {
            editable.replace(s, f2, label);
        } else {
            editable.replace(s - 1, f2, "x");
        }
    }

    public void insertAC_LP(View v) {
        int start = this.etExpr.getSelectionStart();
        int end = this.etExpr.getSelectionEnd();
        CharSequence txt = ((Button) v).getText();
        this.etExpr.getText().replace(Math.min(start, end), Math.max(start, end), ((Object) txt) + "(");
        if (this.pGraphCanvas.getVisibility() != 0) {
            showGraph(((Object) txt) + "(x)");
        } else {
            this.gCanvas.setFunction(((Object) txt) + "(x)");
        }
        this.gCanvas.invalidate();
    }

    public void insertAC_Long(View v) {
        int start = this.etExpr.getSelectionStart();
        int end = this.etExpr.getSelectionEnd();
        this.etExpr.getText().replace(Math.min(start, end), Math.max(start, end), "(1/" + ((Object) ((Button) v).getText()) + ")");
    }

    public void moreFunctions(View v) {
        Button bt12 = (Button) findViewById(R.id.btExp);
        Button bt22 = (Button) findViewById(R.id.btLn);
        Button bt32 = (Button) findViewById(R.id.btLog);
        Button btMore2 = (Button) findViewById(R.id.btMore);
        btMore2.setTextColor(-1);
        this.more++;
        switch (this.more % 3) {
            case 0:
                btMore2.setTextColor(-7829368);
                bt12.setText("exp");
                bt22.setText("ln");
                bt32.setText("log");
                break;
            case 1:
                bt12.setText("abs");
                bt22.setText("sqrt");
                bt32.setText("log2");
                break;
            case 2:
                bt12.setText("round");
                bt22.setText("floor");
                bt32.setText("ceil");
                break;
        }
        this.vibrator.vibrate(40);
    }

    public void insertPrefixA(View v) {
        CheckBox cb = this.cbInv;
        if (cb.isChecked()) {
            cb.setTextColor(-1);
            Button btn = (Button) findViewById(R.id.btSin);
            btn.setText("a" + ((Object) btn.getText()));
            Button btn2 = (Button) findViewById(R.id.btCos);
            btn2.setText("a" + ((Object) btn2.getText()));
            Button btn3 = (Button) findViewById(R.id.btTan);
            btn3.setText("a" + ((Object) btn3.getText()));
            Button btn4 = (Button) findViewById(R.id.btCsc);
            btn4.setText("a" + ((Object) btn4.getText()));
            Button btn5 = (Button) findViewById(R.id.btSec);
            btn5.setText("a" + ((Object) btn5.getText()));
            Button btn6 = (Button) findViewById(R.id.btCot);
            btn6.setText("a" + ((Object) btn6.getText()));
        } else {
            cb.setTextColor(-7829368);
            Button btn7 = (Button) findViewById(R.id.btSin);
            btn7.setText(btn7.getText().subSequence(1, btn7.getText().length()));
            Button btn8 = (Button) findViewById(R.id.btCos);
            btn8.setText(btn8.getText().subSequence(1, btn8.getText().length()));
            Button btn9 = (Button) findViewById(R.id.btTan);
            btn9.setText(btn9.getText().subSequence(1, btn9.getText().length()));
            Button btn10 = (Button) findViewById(R.id.btCsc);
            btn10.setText(btn10.getText().subSequence(1, btn10.getText().length()));
            Button btn11 = (Button) findViewById(R.id.btSec);
            btn11.setText(btn11.getText().subSequence(1, btn11.getText().length()));
            Button btn12 = (Button) findViewById(R.id.btCot);
            btn12.setText(btn12.getText().subSequence(1, btn12.getText().length()));
        }
        this.vibrator.vibrate(40);
    }

    public void insertSuffixH(View v) {
        CheckBox cb = this.cbHyp;
        if (cb.isChecked()) {
            cb.setTextColor(-1);
            Button btn = (Button) findViewById(R.id.btSin);
            btn.setText(((Object) btn.getText()) + "h");
            Button btn2 = (Button) findViewById(R.id.btCos);
            btn2.setText(((Object) btn2.getText()) + "h");
            Button btn3 = (Button) findViewById(R.id.btTan);
            btn3.setText(((Object) btn3.getText()) + "h");
            Button btn4 = (Button) findViewById(R.id.btCsc);
            btn4.setText(((Object) btn4.getText()) + "h");
            Button btn5 = (Button) findViewById(R.id.btSec);
            btn5.setText(((Object) btn5.getText()) + "h");
            Button btn6 = (Button) findViewById(R.id.btCot);
            btn6.setText(((Object) btn6.getText()) + "h");
        } else {
            cb.setTextColor(-7829368);
            Button btn7 = (Button) findViewById(R.id.btSin);
            btn7.setText(btn7.getText().subSequence(0, btn7.getText().length() - 1));
            Button btn8 = (Button) findViewById(R.id.btCos);
            btn8.setText(btn8.getText().subSequence(0, btn8.getText().length() - 1));
            Button btn9 = (Button) findViewById(R.id.btTan);
            btn9.setText(btn9.getText().subSequence(0, btn9.getText().length() - 1));
            Button btn10 = (Button) findViewById(R.id.btCsc);
            btn10.setText(btn10.getText().subSequence(0, btn10.getText().length() - 1));
            Button btn11 = (Button) findViewById(R.id.btSec);
            btn11.setText(btn11.getText().subSequence(0, btn11.getText().length() - 1));
            Button btn12 = (Button) findViewById(R.id.btCot);
            btn12.setText(btn12.getText().subSequence(0, btn12.getText().length() - 1));
        }
        this.vibrator.vibrate(40);
    }

    public void backSpace(View v) {
        if (new StringBuilder().append((Object) this.etExpr.getText()).toString().trim().equals("")) {
            this.etExpr.append(this.lastExpr);
            return;
        }
        int start = Math.min(this.etExpr.getSelectionStart(), this.etExpr.getSelectionEnd());
        int end = Math.max(this.etExpr.getSelectionStart(), this.etExpr.getSelectionEnd());
        if (start < end) {
            this.etExpr.getText().replace(start, end, "");
        }
        if (start == end && start > 0) {
            this.etExpr.getText().replace(start - 1, end, "");
        }
    }

    public boolean backSpaceLong(View v) {
        String str = new StringBuilder().append((Object) this.etExpr.getText()).toString();
        if (!str.trim().equals("")) {
            this.lastExpr = str;
            this.etExpr.setText("");
            this.tvResult.setText(" ...");
            return true;
        }
        if (str.trim().equals("")) {
            this.etExpr.append(this.lastExpr);
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calc_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        boolean z;
        switch (item.getItemId()) {
            case R.id.settings /*2131296320*/:
                showSettings();
                return false;
            case R.id.reset /*2131296321*/:
                reset();
                return false;
            case R.id.rbBasic /*2131296322*/:
                this.isScientific = false;
                item.setChecked(!item.isChecked());
                this.pTrans.setVisibility(8);
                this.pGraphCanvas.setVisibility(8);
                this.pMainKeys.setVisibility(0);
                this.pGraph.setVisibility(8);
                this.pResult.setVisibility(0);
                return false;
            case R.id.rbScientific /*2131296323*/:
                this.isScientific = true;
                if (item.isChecked()) {
                    z = false;
                } else {
                    z = true;
                }
                item.setChecked(z);
                this.pGraph.setVisibility(0);
                this.pTrans.setVisibility(0);
                return false;
            case R.id.help /*2131296324*/:
                showHelp();
                return false;
            case R.id.about /*2131296325*/:
                showAbout();
                return false;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void reset() {
        this.cbInv.setChecked(false);
        this.cbHyp.setChecked(false);
        ((Button) findViewById(R.id.btSin)).setText("sin");
        ((Button) findViewById(R.id.btCos)).setText("cos");
        ((Button) findViewById(R.id.btTan)).setText("tan");
        ((Button) findViewById(R.id.btCsc)).setText("csc");
        ((Button) findViewById(R.id.btSec)).setText("sec");
        ((Button) findViewById(R.id.btCot)).setText("cot");
        ((Button) findViewById(R.id.btExp)).setText("exp");
        ((Button) findViewById(R.id.btLn)).setText("ln");
        ((Button) findViewById(R.id.btLog)).setText("log");
        this.cbInv.setTextColor(-7829368);
        this.cbHyp.setTextColor(-7829368);
        this.btMore.setTextColor(-7829368);
    }

    private void showSettings() {
    }

    public void fullScreen(View v) {
        this.isGraphFullScreen = true;
        Intent iGraph = new Intent(this, GraphCanvas_FullScr.class);
        iGraph.putExtra("expression", this.gCanvas.function.formula());
        iGraph.putExtra("cart", new StringBuilder().append(this.gCanvas.cart).toString());
        try {
            startActivity(iGraph);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            this.tvResult.setText(e.getMessage());
        }
    }

    public void upgrade(View v) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.upgradeURL)));
    }

    private void upgrade() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("upgradeURL")));
    }

    private void showAbout() {
        try {
            startActivity(new Intent(this, CalcAbout.class));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            this.tvResult.setText(e.getMessage());
        }
    }

    private void showHelp() {
        try {
            startActivity(new Intent(this, CalcHelp.class));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
            this.tvResult.setText(e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        reset();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 2) {
            this.pGraph.setVisibility(8);
            this.pTrans.setVisibility(8);
            return;
        }
        if (this.isScientific) {
            this.pTrans.setVisibility(0);
            this.pGraph.setVisibility(0);
        }
        if (this.pGraphCanvas.getVisibility() == 0) {
            this.pResult.setVisibility(8);
            this.pGraph.setVisibility(8);
        }
    }
}
