package com.calculatorgrapher.a;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class CalcAbout extends Activity {
    TextView tvAbout;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.app.Activity.onCreate(android.os.Bundle):void in method: com.calculatorgrapher.a.CalcAbout.onCreate(android.os.Bundle):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.app.Activity.onCreate(android.os.Bundle):void
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:538)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public void onCreate(android.os.Bundle r1) {
        /*
            r2 = this;
            super.onCreate(r3)
            r0 = 2130903040(0x7f030000, float:1.7412887E38)
            r2.setContentView(r0)
            r0 = 2131296258(0x7f090002, float:1.8210428E38)
            android.view.View r0 = r2.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            r2.tvAbout = r0
            android.widget.TextView r0 = r2.tvAbout
            r1 = 15
            android.text.util.Linkify.addLinks(r0, r1)
            android.widget.TextView r0 = r2.tvAbout
            java.lang.String r1 = r2.readHtml()
            android.text.Spanned r1 = android.text.Html.fromHtml(r1)
            r0.setText(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.calculatorgrapher.a.CalcAbout.onCreate(android.os.Bundle):void");
    }

    private String readHtml() {
        InputStream inputStream = getResources().openRawResource(R.raw.about);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            for (int i = inputStream.read(); i != -1; i = inputStream.read()) {
                byteArrayOutputStream.write(i);
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toString();
    }

    public void close(View v) {
        finish();
    }
}
