package com.calculatorgrapher.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import com.calculatorgrapher.math.Function;
import java.lang.reflect.Array;
import java.util.Date;

public class GraphCanvas extends CoordinateSystem {
    static final int EQUATION = 2;
    static final int FUNCTION = 1;
    static final int PARAMETRIC = 3;
    static final int POINTS = 4;
    public boolean animate = true;
    public boolean animatePolar = false;
    public boolean clear = false;
    int cness = 12;
    int coarseness = this.icness;
    public int curveColor;
    public int curveColor0;
    public int[] curvesColor;
    public double[] endPoint;
    double[][] endPoints;
    public Function[] fns = new Function[this.numFnMax];
    Function function = new Function("pi");
    public int gSleep = 5;
    public int graphType = FUNCTION;
    int icness = PARAMETRIC;
    public String[] interval;
    private int nanNum;
    public int numFnMax = FUNCTION;
    int pause = 100;

    public GraphCanvas(Context context) {
        super(context);
        double[] dArr = new double[EQUATION];
        // fill-array-data instruction
        dArr[0] = -4591138345127510016;
        dArr[1] = 4632233691727265792;
        this.endPoint = dArr;
        this.curveColor0 = -16776961;
        this.curveColor = -16776961;
        this.curvesColor = new int[this.numFnMax];
        this.interval = new String[this.numFnMax];
        this.endPoints = (double[][]) Array.newInstance(Double.TYPE, this.numFnMax, EQUATION);
        setFunction("pi");
    }

    public GraphCanvas(Context context, String expr) {
        super(context);
        double[] dArr = new double[EQUATION];
        // fill-array-data instruction
        dArr[0] = -4591138345127510016;
        dArr[1] = 4632233691727265792;
        this.endPoint = dArr;
        this.curveColor0 = -16776961;
        this.curveColor = -16776961;
        this.curvesColor = new int[this.numFnMax];
        this.interval = new String[this.numFnMax];
        this.endPoints = (double[][]) Array.newInstance(Double.TYPE, this.numFnMax, EQUATION);
        setFunction(expr);
    }

    public void setFunction(String expr) {
        this.fns[0] = new Function(expr);
        this.curvesColor[0] = -16776961;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas cnvs) {
        super.onDraw(cnvs);
        Paint paint = new Paint();
        Date t1 = new Date();
        for (int i = 0; i < this.numFnMax; i += FUNCTION) {
            this.function = this.fns[i];
            paint.setColor(this.curvesColor[i]);
            this.curveColor = this.curvesColor[i];
            if (this.movingGraph) {
                this.coarseness = this.cness;
            } else {
                this.coarseness = this.icness;
            }
            if (this.cart) {
                drawCart(cnvs);
            } else {
                drawPolar(cnvs);
            }
            paint.setTextSize(12.0f);
            if (this.polar) {
                cnvs.drawText(String.valueOf(this.function.formula().replace("(x)", "(θ)")) + "   intvl = (-π, π)", (float) ((-this.ox) + 10), (float) ((((-this.oy) + this.h) - 5) - (i * 15)), paint);
            } else {
                cnvs.drawText(String.valueOf(this.function.formula()) + "   intvl = (-∞, +∞)", (float) ((-this.ox) + 10), (float) ((((-this.oy) + this.h) - 5) - (i * 15)), paint);
            }
        }
        double t = ((double) (new Date().getTime() - t1.getTime())) / 1000.0d;
        paint.setColor(this.axisColor);
        paint.setTextSize(10.0f);
        cnvs.drawText(String.valueOf(Function.format(t, EQUATION)) + "s", (float) ((this.w - this.ox) - 30), (float) (((-this.oy) + this.h) - 5), paint);
    }

    private void drawCart(Canvas cnvs) {
        switch (this.graphType) {
            case FUNCTION /*1*/:
                drawFunctionCart(cnvs);
                return;
            case EQUATION /*2*/:
            case PARAMETRIC /*3*/:
            default:
                return;
        }
    }

    public void drawFunctionCart(Canvas cnvs) {
        Function.jump = false;
        this.endPoint[0] = Double.NEGATIVE_INFINITY;
        this.endPoint[FUNCTION] = Double.POSITIVE_INFINITY;
        Function fn = this.function;
        double s = (double) graphBoundary().x;
        double f = (double) this.right;
        int start = (int) Math.max(s, this.endPoint[0] * this.xAxis.scale);
        int end = (int) Math.min(f, this.endPoint[FUNCTION] * this.xAxis.scale);
        boolean isPreviousNaN = false;
        int value1 = 0;
        double y2 = 0.0d;
        Paint paint = new Paint();
        paint.setColor(this.curveColor);
        int var = start - (this.coarseness * EQUATION);
        while (var <= end) {
            int value2 = toCompCoor(y2);
            double yNext = fn.valueAt((((double) var) + ((double) (this.coarseness * EQUATION))) / this.xAxis.scale) * this.yAxis.scale;
            if (var < start) {
                value1 = value2;
                y2 = yNext;
            } else {
                if (Double.isNaN(y2)) {
                    if (!isPreviousNaN && !Double.isNaN(yNext)) {
                        cnvs.drawCircle((float) point(var, value1).x, (float) point(var, value1).y, 3.0f, paint);
                    }
                    if (this.nanNum % PARAMETRIC == FUNCTION) {
                        drawLine(cnvs, var + PARAMETRIC, POINTS, var - PARAMETRIC, -4, paint);
                    }
                    isPreviousNaN = true;
                    this.nanNum = this.nanNum + FUNCTION;
                } else if (Double.isInfinite(y2)) {
                    drawAsymptote(cnvs, var + FUNCTION);
                } else if (isPreviousNaN) {
                    drawLine(cnvs, var + FUNCTION, value2, var + FUNCTION, value2, paint);
                    isPreviousNaN = false;
                } else if (value1 * value2 >= 0 || Math.abs(value1 - value2) <= 1200) {
                    drawLineThick(cnvs, var, value1, var + this.coarseness, value2, paint);
                    if (Function.jump) {
                        value1 = toCompCoor(yNext);
                        y2 = fn.valueAt((((double) var) + ((double) (this.coarseness * PARAMETRIC))) / this.xAxis.scale) * this.yAxis.scale;
                        var += this.coarseness;
                    }
                } else {
                    drawAsymptote(cnvs, var + FUNCTION);
                }
                value1 = value2;
                y2 = yNext;
            }
            var += this.coarseness;
        }
    }

    public void drawFunctionPolar(Canvas cnvs) {
        double s;
        int i = (this.gSleep / EQUATION) + FUNCTION;
        Function fn = this.function;
        Function.jump = false;
        double angle = this.pAxis.angle;
        double s2 = -3.141592653589793d;
        this.endPoint[0] = s2;
        this.endPoint[FUNCTION] = 3.141592653589793d;
        double f = 3.141592653589793d;
        if (s2 <= -32768.0d) {
            s2 = 0.0d;
        }
        if (3.141592653589793d >= 32767.0d) {
            f = 2.0d * 3.141592653589793d;
        }
        this.pAxis.angle = this.pAxis.angle + s;
        double incr = (((double) (this.coarseness * EQUATION)) * 3.141592653589793d) / 180.0d;
        double value1 = fn.valueAt(s - incr) * this.pAxis.scale;
        double value2 = fn.valueAt(s) * this.pAxis.scale;
        int y1 = toCompCoor(value1);
        int y2 = toCompCoor(value2);
        int y3 = toCompCoor(fn.valueAt(s + incr) * this.pAxis.scale);
        this.pAxis.angle -= incr;
        Point point = point(y1, 0);
        this.pAxis.angle += incr;
        Point p2 = point(y2, 0);
        Point point2 = point(y3, 0);
        Paint paint = new Paint();
        paint.setColor(this.curveColor);
        while (s < f) {
            double value3 = fn.valueAt(s + incr) * this.pAxis.scale;
            int y32 = toCompCoor(value3);
            this.pAxis.angle += incr;
            Point p3 = point(y32, 0);
            if (Double.isNaN(value2)) {
                if (!Double.isNaN(value1) && !Double.isNaN(value3)) {
                    drawHole(cnvs, point(y1, 0), 7);
                }
            } else if (Double.isNaN(value3)) {
                drawLineThick(cnvs, p2, p2, paint);
            } else if (value2 * value3 >= 0.0d || Math.abs(value2 - value3) <= 1200.0d) {
                if (Function.jump) {
                    value1 = value3;
                    y1 = y32;
                    value2 = fn.valueAt((3.0d * incr) + s) * this.pAxis.scale;
                    y2 = toCompCoor(value2);
                    s += incr;
                    this.pAxis.angle += incr;
                    p2 = point(y2, 0);
                } else {
                    paint.setColor(this.bgColor);
                    drawLineThick(cnvs, p2, p3, paint);
                    if (this.animate) {
                        try {
                            Thread.sleep((long) this.gSleep);
                        } catch (InterruptedException e) {
                        }
                    }
                    paint.setColor(this.curveColor);
                    drawLineThick(cnvs, p2, p3, paint);
                }
            }
            value1 = value2;
            y1 = y2;
            value2 = value3;
            y2 = y32;
            p2 = p3;
            s += incr;
        }
        this.pAxis.angle = angle;
    }

    public long pause(int ms) {
        Date t1 = new Date();
        long t = 0;
        while (t < ((long) ms)) {
            t = new Date().getTime() - t1.getTime();
        }
        return t;
    }

    private void drawFunctionCartBzr(Canvas cnvs) {
        Function fn = this.function;
        double s = (double) graphBoundary().x;
        double f = (double) graphBoundary().y;
        int start = (int) Math.max(s, this.endPoint[0] * this.xAxis.scale);
        int end = (int) Math.min(f, this.endPoint[FUNCTION] * this.xAxis.scale);
        Path gPath = new Path();
        gPath.moveTo((float) start, -((float) (fn.valueAt(((double) ((float) start)) / this.xAxis.scale) * this.yAxis.scale)));
        for (int i = start + this.coarseness; i < end; i += this.coarseness * PARAMETRIC) {
            gPath.cubicTo((float) i, -((float) (fn.valueAt(((double) i) / this.xAxis.scale) * this.yAxis.scale)), (float) (this.coarseness + i), -((float) (fn.valueAt(((double) (this.coarseness + i)) / this.xAxis.scale) * this.yAxis.scale)), (float) ((this.coarseness * EQUATION) + i), -((float) (fn.valueAt(((double) ((this.coarseness * EQUATION) + i)) / this.xAxis.scale) * this.yAxis.scale)));
        }
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2.0f);
        paint.setColor(this.curveColor);
        cnvs.drawPath(gPath, paint);
    }

    private void drawHole(Canvas cnvs, Point p, int thickness) {
        Paint paint = new Paint();
        paint.setColor(this.curveColor);
        cnvs.drawCircle((float) p.x, (float) (-p.y), (float) thickness, paint);
    }

    public void drawAsymptote(Canvas cnvs, int x) {
        Paint paint = new Paint();
        paint.setColor(this.curveColor);
        for (int j = -this.h; j <= this.h; j += 20) {
            drawLine(cnvs, x, j + 5, x, j + 5 + 10, paint);
        }
    }

    private void drawPolar(Canvas cnvs) {
        switch (this.graphType) {
            case FUNCTION /*1*/:
                drawFunctionPolar(cnvs);
                break;
        }
        drawFunctionPolar(cnvs);
    }
}
