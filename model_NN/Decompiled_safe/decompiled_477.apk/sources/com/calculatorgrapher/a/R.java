package com.calculatorgrapher.a;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int azure = 2131099667;
        public static final int btCalculate = 2131099658;
        public static final int btEq = 2131099656;
        public static final int btEqBg = 2131099657;
        public static final int btLP = 2131099653;
        public static final int btOp = 2131099659;
        public static final int btOp1 = 2131099660;
        public static final int btRP = 2131099654;
        public static final int calcBackground = 2131099648;
        public static final int darkBlue = 2131099666;
        public static final int dimGray = 2131099668;
        public static final int expLnBackground = 2131099650;
        public static final int gKey_TextColor = 2131099652;
        public static final int hiColor = 2131099663;
        public static final int hintColor = 2131099664;
        public static final int keyBackground = 2131099655;
        public static final int keyPadBackground = 2131099649;
        public static final int royalBlue = 2131099665;
        public static final int shadowColor_fn = 2131099671;
        public static final int shadowColor_g = 2131099672;
        public static final int shadowColor_main = 2131099669;
        public static final int shadowColor_opr = 2131099670;
        public static final int trigHypBackground = 2131099651;
        public static final int tvResultBg = 2131099662;
        public static final int tvResultTextColor = 2131099661;
    }

    public static final class dimen {
        public static final int fn_keyTextSize = 2131165199;
        public static final int fn_keyTextSize_s = 2131165196;
        public static final int gKeyH = 2131165191;
        public static final int g_keyTextSize = 2131165190;
        public static final int keyTextSize = 2131165194;
        public static final int keyTextSize_s = 2131165195;
        public static final int pad_btm_fn_key = 2131165198;
        public static final int pad_btm_g_key = 2131165189;
        public static final int pad_btm_main_key = 2131165193;
        public static final int pad_top_fn_key = 2131165197;
        public static final int pad_top_g_key = 2131165188;
        public static final int pad_top_main_key = 2131165192;
        public static final int paddingB = 2131165187;
        public static final int paddingLR = 2131165184;
        public static final int paddingLR18 = 2131165185;
        public static final int paddingT = 2131165186;
        public static final int transKeyH = 2131165200;
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int ic_launcher0 = 2130837505;
        public static final int ic_launcher1 = 2130837506;
        public static final int ic_menu_help = 2130837507;
        public static final int ic_menu_info_details = 2130837508;
        public static final int ic_menu_manage = 2130837509;
        public static final int ic_menu_settings = 2130837510;
        public static final int icon = 2130837511;
    }

    public static final class id {
        public static final int about = 2131296325;
        public static final int bt0 = 2131296294;
        public static final int bt1 = 2131296289;
        public static final int bt2 = 2131296290;
        public static final int bt3 = 2131296291;
        public static final int bt4 = 2131296284;
        public static final int bt5 = 2131296285;
        public static final int bt6 = 2131296286;
        public static final int bt7 = 2131296279;
        public static final int bt8 = 2131296280;
        public static final int bt9 = 2131296281;
        public static final int btBackSpace = 2131296301;
        public static final int btCloseGraph = 2131296262;
        public static final int btCos = 2131296308;
        public static final int btCot = 2131296313;
        public static final int btCsc = 2131296311;
        public static final int btD = 2131296293;
        public static final int btE = 2131296298;
        public static final int btEq = 2131296304;
        public static final int btExp = 2131296265;
        public static final int btF = 2131296271;
        public static final int btFullScrn = 2131296260;
        public static final int btGraph = 2131296273;
        public static final int btHelpClose = 2131296257;
        public static final int btLP = 2131296282;
        public static final int btLn = 2131296266;
        public static final int btLog = 2131296267;
        public static final int btMore = 2131296268;
        public static final int btMu = 2131296292;
        public static final int btMul = 2131296288;
        public static final int btPi = 2131296297;
        public static final int btPlus = 2131296287;
        public static final int btPt = 2131296295;
        public static final int btPw = 2131296296;
        public static final int btRP = 2131296283;
        public static final int btSec = 2131296312;
        public static final int btSin = 2131296307;
        public static final int btSolve = 2131296272;
        public static final int btTan = 2131296309;
        public static final int btUpgrade = 2131296261;
        public static final int btX = 2131296270;
        public static final int cbHyp = 2131296277;
        public static final int cbInv = 2131296276;
        public static final int etExpr = 2131296300;
        public static final int help = 2131296324;
        public static final int pAbout = 2131296256;
        public static final int pExpLog = 2131296264;
        public static final int pExpLog_ = 2131296263;
        public static final int pExpr_BS = 2131296299;
        public static final int pGraph = 2131296269;
        public static final int pGraphCanvas = 2131296317;
        public static final int pGraphCanvas_top = 2131296259;
        public static final int pHelp = 2131296274;
        public static final int pMainKeys = 2131296316;
        public static final int pMainKeys_ = 2131296314;
        public static final int pMainKeys__ = 2131296315;
        public static final int pResult_Eq = 2131296302;
        public static final int pRow1 = 2131296278;
        public static final int pTrans = 2131296318;
        public static final int pTrig = 2131296305;
        public static final int pTrig1 = 2131296306;
        public static final int pTrig2 = 2131296310;
        public static final int pTrig_ = 2131296319;
        public static final int rbBasic = 2131296322;
        public static final int rbScientific = 2131296323;
        public static final int reset = 2131296321;
        public static final int settings = 2131296320;
        public static final int tvAbout = 2131296258;
        public static final int tvHelp = 2131296275;
        public static final int tvResult = 2131296303;
    }

    public static final class layout {
        public static final int calc_about = 2130903040;
        public static final int calc_canvas_top = 2130903041;
        public static final int calc_exp_log = 2130903042;
        public static final int calc_exp_log_s = 2130903043;
        public static final int calc_graph = 2130903044;
        public static final int calc_graph_s = 2130903045;
        public static final int calc_help = 2130903046;
        public static final int calc_inv_hyp = 2130903047;
        public static final int calc_inv_hyp_s = 2130903048;
        public static final int calc_row1_num_op = 2130903049;
        public static final int calc_row2_num_op = 2130903050;
        public static final int calc_row3_num_op = 2130903051;
        public static final int calc_row4_num_op = 2130903052;
        public static final int calc_top = 2130903053;
        public static final int calc_top_s = 2130903054;
        public static final int calc_trig = 2130903055;
        public static final int calc_trig_s = 2130903056;
        public static final int main = 2130903057;
        public static final int main1 = 2130903058;
        public static final int main_s = 2130903059;
    }

    public static final class menu {
        public static final int calc_menu = 2131230720;
    }

    public static final class raw {
        public static final int about = 2130968576;
        public static final int help = 2130968577;
    }

    public static final class string {
        public static final int app_name = 2131034112;
        public static final int arrowDel = 2131034113;
        public static final int arrowDel_ = 2131034118;
        public static final int arrowLeft = 2131034114;
        public static final int arrowUp = 2131034115;
        public static final int mult = 2131034116;
        public static final int qsMark = 2131034117;
        public static final int qsMark_ = 2131034119;
        public static final int tvHelp = 2131034120;
        public static final int tvResult = 2131034121;
    }
}
