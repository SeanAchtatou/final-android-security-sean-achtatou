package com.calculatorgrapher.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import com.calculatorgrapher.math.Function;
import java.util.Date;

public class CoordinateSystem extends View implements View.OnTouchListener {
    public static final double epsilon = 1.0E-6d;
    public boolean D3 = false;
    public int axisColor;
    public int axisColor0;
    public int bgColor;
    public int bgColor0;
    int btm;
    public boolean cart = true;
    boolean center;
    Axis depAxis;
    public int gridColor;
    public int gridColor0;
    private double gridIntv;
    public int h;
    public int idColor;
    boolean isAnimatingDrag;
    boolean isDrawingAxes;
    public boolean isGraphingCalculator;
    public boolean isPolarActive = true;
    int left;
    int mId;
    int mLastTouchX;
    int mLastTouchY;
    Point mMoved;
    Point mPressed;
    int margin = 18;
    int marginBtmFx;
    int marginBtmFxy;
    int marginLeftFx;
    int marginLeftFxy;
    int marginRightFx;
    int marginRightFxy;
    int marginTopFx;
    int marginTopFxy;
    public boolean movingGraph;
    Point origin;
    public int ox;
    public int oy;
    public Axis pAxis;
    final int pS = 30;
    int pScale;
    public boolean polar;
    RectF rectCP;
    Rect rectCP_;
    Rect rectCoors;
    RectF rectZoom;
    Rect rectZoomIn;
    Rect rectZoomOut;
    int right;
    String s;
    public boolean sameColorGridAxis;
    Axis selectedAxis;
    public boolean showGrid;
    private Date tActDwn;
    public double theta = 0.0d;
    String tick;
    int top;
    public int w;
    public double xAngle = 0.0d;
    public Axis xAxis;
    int xDCS;
    int xDirContourSpacing;
    public String xMajorTicks;
    int xMoved;
    final int xS = 30;
    int xScale;
    public double yAngle = 1.5707963267948966d;
    public Axis yAxis;
    int yDCS;
    int yDirContourSpacing;
    public String yMajorTicks;
    int yMoved;
    final int yS = 30;
    int yScale;
    public double zAngle = 0.0d;
    final int zS = 30;
    int zScale;

    public CoordinateSystem(Context context) {
        super(context);
        boolean z;
        if (this.cart) {
            z = false;
        } else {
            z = true;
        }
        this.polar = z;
        this.isDrawingAxes = true;
        this.showGrid = true;
        this.center = true;
        this.isAnimatingDrag = false;
        this.xScale = 30;
        this.yScale = 30;
        this.zScale = 30;
        this.pScale = 30;
        this.mId = 20;
        this.marginTopFx = 0;
        this.marginBtmFx = 0;
        this.marginLeftFx = 0;
        this.marginRightFx = 0;
        this.marginTopFxy = 0;
        this.marginBtmFxy = 0;
        this.marginLeftFxy = 0;
        this.marginRightFxy = 0;
        this.xDirContourSpacing = 5;
        this.yDirContourSpacing = 5;
        this.gridIntv = 1.0d;
        this.s = "";
        this.xMajorTicks = "1";
        this.tick = "1";
        this.yMajorTicks = "1";
        this.rectCoors = new Rect();
        this.rectCP = new RectF();
        this.rectCP_ = new Rect();
        this.rectZoom = new RectF();
        this.rectZoomIn = new Rect();
        this.rectZoomOut = new Rect();
        this.bgColor0 = -16777216;
        this.axisColor0 = -1;
        this.gridColor0 = -3355444;
        this.idColor = -65536;
        this.pAxis = new Axis(0.0d, (double) this.xScale, "θ");
        this.xAxis = new Axis(this.xAngle, (double) this.xScale, "x");
        this.yAxis = new Axis(this.yAngle, (double) this.yScale, "y");
        setOnTouchListener(this);
        this.bgColor = -1;
        this.axisColor = -16777216;
        this.gridColor = Color.rgb(208, 208, 208);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas cnvs) {
        this.w = getWidth();
        this.h = getHeight();
        if (this.center) {
            this.ox = this.w / 2;
            this.oy = this.h / 2;
        }
        this.center = false;
        Paint paint = new Paint();
        paint.setColor(this.bgColor);
        cnvs.drawPaint(paint);
        if (this.isPolarActive) {
            int i = this.w - 60;
            int j = this.margin;
            this.rectCP.set((float) i, 0.0f, (float) this.w, (float) j);
            paint.setColor(-3355444);
            paint.setStyle(Paint.Style.STROKE);
            this.rectCP_.set(i - 5, 0, this.w, j + 10);
            int i2 = this.w - 55;
            Rect rectCb = new Rect();
            rectCb.set(i2, 4, i2 + 10, 4 + 10);
            paint.setColor(-16776961);
            if (this.polar) {
                cnvs.drawText("✔", (float) (i2 + 0), (float) (j - 3), paint);
            }
            cnvs.drawText("Polar", (float) (i2 + 10 + 6), (float) (j - 3), paint);
            paint.setStyle(Paint.Style.STROKE);
            cnvs.drawRect(rectCb, paint);
        }
        this.rectCoors.set(0, 0, 80, this.margin + 10);
        cnvs.translate((float) this.ox, (float) this.oy);
        setBoundaries(this.margin);
        if (this.isDrawingAxes) {
            if (this.showGrid) {
                drawGrids(cnvs);
            }
            drawAxes(cnvs);
            cnvs.translate((float) (-this.ox), (float) (-this.oy));
            paint.setColor(-65536);
            paint.setTextSize(12.0f);
            cnvs.drawText(" ( " + Function.format(((double) (-((this.w / 2) - this.ox))) / this.xAxis.scale, 2) + ", " + Function.format(((double) (-(this.oy - (this.h / 2)))) / this.yAxis.scale, 2) + " )", 2.0f, 13.0f, paint);
            cnvs.drawCircle((float) (this.w / 2), (float) (this.h / 2), 1.0f, paint);
            cnvs.translate((float) this.ox, (float) this.oy);
        }
        cnvs.translate((float) (-this.ox), (float) (-this.oy));
        int a1 = this.w - 80;
        int a2 = this.w - 10;
        int b1 = this.h - 30;
        int b2 = this.h - 17;
        int r = ((b2 - b1) / 2) - -4;
        int cxIn = (a2 - r) - 4;
        int cy = (b2 + b1) / 2;
        int cxOut = a1 + r + 4;
        paint.setColor(-3355444);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.rectZoom.set((float) a1, (float) b1, (float) a2, (float) b2);
        cnvs.drawRoundRect(this.rectZoom, 50.0f, 10.0f, paint);
        paint.setColor(-1);
        paint.setStyle(Paint.Style.STROKE);
        cnvs.drawCircle((float) cxIn, (float) cy, (float) r, paint);
        cnvs.drawCircle((float) cxOut, (float) cy, (float) r, paint);
        paint.setColor(-7829368);
        cnvs.drawLine((float) ((a1 + a2) / 2), (float) (b1 + 2), (float) ((a1 + a2) / 2), (float) (b2 - 2), paint);
        paint.setTextSize(16.0f);
        paint.setColor(-12303292);
        cnvs.drawText("++", (float) ((cxIn - r) + 1), (float) (b2 - 1), paint);
        cnvs.drawText("--", (float) ((cxOut - r) + 5), (float) (b2 - 2), paint);
        this.rectZoomIn.set(((a1 + a2) / 2) + 2, b1 - 0, this.w, this.h);
        this.rectZoomOut.set(a1 - 10, b1 - 0, ((a1 + a2) / 2) - 2, this.h);
        paint.setColor(-7829368);
        paint.setTextSize(12.0f);
        cnvs.drawText("The world's most accurate & most capable Graphing Calculator.", 5.0f, (float) (this.h - 40), paint);
        paint.setTextSize(20.0f);
        paint.setTextSkewX(-0.35f);
        paint.setColor(Color.rgb(100, 100, 100));
        cnvs.drawText("Demo", 20.0f, 50.0f, paint);
        cnvs.drawText("Graphing Calculator", 20.0f, 90.0f, paint);
        paint.setColor(Color.rgb(200, 0, 0));
        cnvs.drawText("Graph, Solve, Differentiate", 70.0f, 65.0f, paint);
        cnvs.drawText("Cartesian, Polar", 70.0f, 115.0f, paint);
        paint.setColor(Color.rgb(0, 200, 0));
        cnvs.drawText("Powerful, Easy-to-use", 50.0f, 150.0f, paint);
        cnvs.translate((float) this.ox, (float) this.oy);
    }

    public void drawAxes(Canvas cnvs) {
        if (this.cart) {
            drawRectAxes(cnvs);
        } else if (this.polar) {
            drawPolarAxes(cnvs);
        }
    }

    public void drawPolarAxes(Canvas cnvs) {
        this.pAxis = this.xAxis;
        this.pAxis.id = "θ";
        Paint paint = new Paint();
        paint.setColor(this.axisColor);
        this.tick = this.xMajorTicks;
        drawAxisSegmentThick(cnvs, this.pAxis, paint);
        drawId(cnvs, this.pAxis);
        drawAxisHead(cnvs, this.pAxis);
        hLabel(cnvs, this.xAxis);
        paint.setColor(this.idColor);
        cnvs.drawText("o", 3.0f, 12.0f, paint);
    }

    private void drawRectAxes(Canvas cnvs) {
        Paint paint = new Paint();
        paint.setColor(this.axisColor);
        drawAxisSegmentThick(cnvs, this.xAxis, paint);
        drawAxisHead(cnvs, this.xAxis);
        this.xAxis.id = "x";
        drawId(cnvs, this.xAxis);
        Paint paint2 = new Paint();
        paint2.setColor(this.idColor);
        cnvs.drawText("o", 3.0f, 12.0f, paint2);
        paint2.setColor(this.axisColor);
        drawAxisSegmentThick(cnvs, this.yAxis, paint2);
        drawAxisHead(cnvs, this.yAxis);
        drawId(cnvs, this.yAxis);
        this.tick = this.xMajorTicks;
        hLabel(cnvs, this.xAxis);
        exchangeXYAxes();
        this.tick = this.yMajorTicks;
        hLabel(cnvs, this.xAxis);
        exchangeXYAxes();
    }

    public void drawAxisSegment(Canvas cnvs, Axis axis, Paint paint) {
        Point tip = axis.tip();
        Point tail = axis.tail();
        cnvs.drawLine((float) tip.x, (float) (-tip.y), (float) tail.x, (float) (-tail.y), paint);
    }

    public void drawAxisSegmentThick(Canvas cnvs, Axis axis, Paint paint) {
        Point tip = axis.tip();
        Point tail = axis.tail();
        drawLineThick(cnvs, tip.x, tip.y, tail.x, tail.y, paint);
    }

    /* access modifiers changed from: protected */
    public void drawLineThick(Canvas cnvs, Point p1, Point p2, Paint paint) {
        drawLineThick(cnvs, p1.x, p1.y, p2.x, p2.y, paint);
    }

    /* access modifiers changed from: protected */
    public void drawLineThick(Canvas cnvs, int x1, int y1, int x2, int y2, Paint paint) {
        float sw = paint.getStrokeWidth();
        paint.setStrokeWidth(3.0f);
        Canvas canvas = cnvs;
        canvas.drawLine((float) x1, (float) (-y1), (float) x2, (float) (-y2), paint);
        paint.setStrokeWidth(sw);
    }

    /* access modifiers changed from: protected */
    public void drawLine(Canvas cnvs, int x1, int y1, int x2, int y2, Paint paint) {
        Canvas canvas = cnvs;
        canvas.drawLine((float) x1, (float) (-y1), (float) x2, (float) (-y2), paint);
    }

    /* access modifiers changed from: protected */
    public void drawLine(Canvas cnvs, Point pt1, Point pt2, Paint paint) {
        drawLine(cnvs, pt1.x, pt1.y, pt2.x, pt2.y, paint);
    }

    public void drawLineX(Canvas cnvs, double x, Paint paint) {
        int k = (int) Math.round(this.xAxis.scale * x);
        translateRel(cnvs, (double) pointOrth(k, 0).x, (double) pointOrth(k, 0).y);
        setBoundaries(this.margin);
        drawAxisSegment(cnvs, this.yAxis, paint);
        translateRel(cnvs, (double) (-pointOrth(k, 0).x), (double) (-pointOrth(k, 0).y));
        setBoundaries(this.margin);
    }

    public void drawLineY(Canvas cnvs, double y, Paint paint) {
        int k = (int) Math.round(this.yAxis.scale * y);
        translateRel(cnvs, (double) pointOrth(0, k).x, (double) (-pointOrth(0, k).y));
        setBoundaries(this.margin);
        drawAxisSegment(cnvs, this.xAxis, paint);
        translateRel(cnvs, (double) (-pointOrth(0, k).x), (double) pointOrth(0, k).y);
        setBoundaries(this.margin);
    }

    private void exchangeXYAxes() {
        Axis axis = this.xAxis;
        this.xAxis = this.yAxis;
        this.yAxis = axis;
    }

    public void drawGrids(Canvas cnvs) {
        if (this.cart) {
            drawRectGrids(cnvs);
        } else if (this.polar) {
            drawPolarGrids(cnvs);
        }
    }

    public void drawPolarGrids(Canvas cnvs) {
        if (this.pAxis.scale >= 2.0d) {
            double d1 = (double) ((this.ox * this.ox) + (this.oy * this.oy));
            double d2 = (double) (((this.w - this.ox) * (this.w - this.ox)) + (this.oy * this.oy));
            double d3 = (double) (((this.w - this.ox) * (this.w - this.ox)) + ((this.h - this.oy) * (this.h - this.oy)));
            double d4 = (double) ((this.ox * this.ox) + ((this.h - this.oy) * (this.h - this.oy)));
            double round = (double) Math.round(Math.min(Math.min(d1, d2), Math.min(d3, d4)));
            double endCir = (double) Math.round(Math.max(Math.max(d1, d2), Math.max(d3, d4)));
            double startCir = (double) (-this.ox);
            double endCir2 = Math.sqrt(endCir);
            if (this.ox >= 0 && this.ox <= this.w && this.oy >= 0 && this.oy <= this.h) {
                startCir = 0.0d;
            }
            double endCir3 = endCir2 - (endCir2 % this.pAxis.scale);
            Paint paint = new Paint();
            paint.setColor(adjustGridColor());
            for (double startCir2 = startCir - (startCir % this.pAxis.scale); startCir2 < endCir3; startCir2 += this.pAxis.scale) {
                float s2 = (float) Math.round(startCir2);
                paint.setStyle(Paint.Style.STROKE);
                cnvs.drawCircle(0.0f, 0.0f, s2, paint);
            }
            for (int i = 15; i < 180; i += 15) {
                paint.setColor(this.gridColor);
                drawAxisSegment(cnvs, new Axis(this.pAxis.angle + ((((double) i) * 3.141592653589793d) / 180.0d), 1.0d, new StringBuilder().append(i).toString()), paint);
            }
            paint.setColor(this.axisColor);
            drawAxisSegment(cnvs, new Axis(this.pAxis.angle + 1.5707963267948966d, 1.0d, "90°"), paint);
        }
    }

    public void drawRectGrids(Canvas cnvs) {
        drawGridLinesX(cnvs);
        drawGridLinesY(cnvs);
    }

    public void drawGridLinesX(Canvas cnvs) {
        double a0 = this.xAxis.scale;
        if (a0 >= 4.0d) {
            Paint paint = new Paint();
            paint.setColor(this.gridColor);
            double s2 = (double) graphBoundary().x;
            double f = (double) graphBoundary().y;
            double s3 = (s2 - (s2 % a0)) / a0;
            double f2 = (f - (f % a0)) / a0;
            while (s3 <= f2) {
                drawLineX(cnvs, s3, paint);
                s3 += this.gridIntv;
            }
        }
    }

    public void drawGridLinesY(Canvas cnvs) {
        double a0 = this.yAxis.scale;
        if (a0 >= 4.0d) {
            Paint paint = new Paint();
            paint.setColor(this.gridColor);
            exchangeXYAxes();
            double s2 = (double) graphBoundary().x;
            double f = (double) graphBoundary().y;
            exchangeXYAxes();
            double s3 = (s2 - (s2 % a0)) / a0;
            double f2 = (f - (f % a0)) / a0;
            while (s3 <= f2) {
                drawLineY(cnvs, s3, paint);
                s3 += this.gridIntv;
            }
        }
    }

    public Point graphBoundary() {
        double a1 = this.xAxis.angle;
        double a2 = this.yAxis.angle;
        int start = -13;
        int end = -17;
        if (Function.abs(Function.sin(a1 - a2)) <= 0.05d) {
            return new Point(-this.w, this.w);
        }
        if (this.yAxis.isHorizontal() && (isInQ3(a1) || isInQ4(a1))) {
            start = (int) (-(((double) this.btm) / Function.sin(a1)));
            end = (int) (-(((double) this.top) / Function.sin(a1)));
        } else if (this.yAxis.isHorizontal()) {
            return new Point((int) (((double) this.btm) / Function.sin(a1)), (int) (((double) this.top) / Function.sin(a1)));
        } else {
            if (this.yAxis.hasPositiveSlope()) {
                start = c(((double) this.left) - (((double) this.top) * Function.cot(a2)), a2 - a1, a1);
                end = c(((double) this.right) - (((double) this.btm) * Function.cot(a2)), a2 - a1, a1);
            } else if (!this.yAxis.hasPositiveSlope()) {
                start = c(((double) this.left) - (((double) this.btm) * Function.cot(a2)), a2 - a1, a1);
                end = c(((double) this.right) - (((double) this.top) * Function.cot(a2)), a2 - a1, a1);
            }
        }
        if (isInQ2(a1) || isInQ3(a1)) {
            int temp = -start;
            start = -end;
            end = temp;
        }
        return new Point(start, end);
    }

    public int c(double a, double aAngle, double bAngle) {
        return (int) (Math.sqrt((1.0d + ((Function.sin(bAngle) / Function.sin(aAngle)) * (Function.sin(bAngle) / Function.sin(aAngle)))) - (((2.0d * Function.sin(bAngle)) * Function.cos(3.141592653589793d - (aAngle + bAngle))) / Function.sin(aAngle))) * a);
    }

    public void hLabel(Canvas cnvs, Axis axis) {
        Paint paint = new Paint();
        paint.setColor(this.axisColor);
        double minor = Function.abs(axis.minorTicks * axis.scale);
        double major = Function.abs(axis.majorTicks * axis.scale);
        int axisS = axis.start();
        int axisE = axis.end();
        for (double x = ((double) axisS) - (((double) axisS) % minor); x <= ((double) axisE); x += minor) {
            drawLine(cnvs, point((int) Math.round(x), 0), point((int) Math.round(x), -3), paint);
        }
        for (double x2 = ((double) axisS) - (((double) axisS) % major); x2 <= ((double) axisE); x2 += major) {
            Point p1 = point((int) Math.round(x2), 2);
            Point p2 = point((int) Math.round(x2), -5);
            drawLine(cnvs, p1, p2, paint);
            if (Math.abs(x2) > 0.01d) {
                try {
                    int tk = Integer.parseInt(this.tick);
                    if ((axis.angle < 0.0d || axis.angle >= 1.5707963267948966d) && (axis.angle < -3.141592653589793d || axis.angle >= -1.5707963267948966d)) {
                        cnvs.drawText(new StringBuilder().append((int) Math.round((((double) tk) * x2) / major)).toString(), (float) (p2.x + 10), (float) ((-p2.y) + 5), paint);
                    } else {
                        cnvs.drawText(new StringBuilder().append(Math.round((((double) tk) * x2) / major)).toString(), (float) (p2.x + 2), (float) ((-p2.y) + 10), paint);
                    }
                } catch (NumberFormatException e) {
                    try {
                        double tk2 = Function.parseD(this.tick);
                        if ((axis.angle < 0.0d || axis.angle >= 1.5707963267948966d) && (axis.angle < -3.141592653589793d || axis.angle >= -1.5707963267948966d)) {
                            cnvs.drawText(new StringBuilder().append(Function.roundD((x2 / major) * tk2, 2)).toString(), (float) (p2.x + 10), (float) ((-p2.y) + 5), paint);
                        } else {
                            cnvs.drawText(new StringBuilder().append(Function.roundD((x2 / major) * tk2, 2)).toString(), (float) (p2.x + 2), (float) ((-p2.y) + 10), paint);
                        }
                    } catch (NumberFormatException e2) {
                        int num = (int) Math.round(x2 / major);
                        if ((axis.angle < 0.0d || axis.angle >= 1.5707963267948966d) && (axis.angle < -3.141592653589793d || axis.angle >= -1.5707963267948966d)) {
                            cnvs.drawText(num + Function.encloseIfNecessary(this.tick), (float) (p2.x + 10), (float) ((-p2.y) + 5), paint);
                        } else {
                            cnvs.drawText(num + Function.encloseIfNecessary(this.tick), (float) p2.x, (float) ((-p2.y) + 10), paint);
                        }
                    }
                }
            }
        }
    }

    public void trimTicksZoom() {
        double s2 = Math.pow((double) 2, Math.ceil(Math.log(this.xAxis.scale / (0.65d * ((double) Math.min(this.w / 2, this.h / 2)))) / Math.log((double) 2)));
        this.xAxis.majorTicks = 1.0d / s2;
        this.xMajorTicks = new StringBuilder().append(this.xAxis.majorTicks).toString();
        this.yAxis.majorTicks = 1.0d / s2;
        this.yMajorTicks = new StringBuilder().append(this.yAxis.majorTicks).toString();
        if (s2 <= 1.0d) {
            this.xMajorTicks = new StringBuilder().append((long) this.xAxis.majorTicks).toString();
            this.yMajorTicks = new StringBuilder().append((long) this.yAxis.majorTicks).toString();
        }
    }

    public void drawAxisHead(Canvas cnvs, Axis axis) {
        int x1 = (int) Math.round(((double) axis.tip().x) - (((double) 9) * Function.cos(axis.angle + 0.6632251157578452d)));
        int y1 = (int) Math.round(((double) axis.tip().y) - (((double) 9) * Function.sin(axis.angle + 0.6632251157578452d)));
        new Point(x1, y1);
        Paint paint = new Paint();
        paint.setColor(this.axisColor);
        drawLineThick(cnvs, axis.tip().x, axis.tip().y, x1, y1, paint);
        double arAngle = -0.6632251157578452d;
        int x2 = (int) Math.round(((double) axis.tip().x) - (((double) 9) * Function.cos(axis.angle + arAngle)));
        int y2 = (int) Math.round(((double) axis.tip().y) - (((double) 9) * Function.sin(axis.angle + arAngle)));
        new Point(x2, y2);
        drawLineThick(cnvs, axis.tip().x, axis.tip().y, x2, y2, paint);
    }

    public void drawId(Canvas cnvs, Axis axis) {
        int idX;
        int idY;
        Paint paint = new Paint();
        paint.setColor(this.idColor);
        if (Function.cos(axis.angle) > 0.0d) {
            idX = axis.tip().x - 5;
        } else {
            idX = axis.tip().x - 15;
        }
        if (Function.sin(axis.angle) > 0.0d) {
            idY = axis.tip().y - 5;
        } else {
            idY = axis.tip().y - 15;
        }
        cnvs.drawText(new StringBuilder().append(axis.id.charAt(0)).toString(), (float) idX, (float) (-idY), paint);
        cnvs.drawText(axis.id.substring(1), (float) (idX + 8), (float) idY, paint);
    }

    public Point point(int xr, int yr) {
        double a1 = this.xAxis.angle;
        double a2 = this.yAxis.angle;
        return new Point((int) Math.round((((double) xr) * Function.cos(a1)) + (((double) yr) * Function.cos(a2))), (int) Math.round((((double) xr) * Function.sin(a1)) + (((double) yr) * Function.sin(a2))));
    }

    public Point pointOrth(int xp, int yp) {
        double a1 = this.xAxis.angle;
        double a2 = this.yAxis.angle;
        double det = (Function.cos(a1) * Function.sin(a2)) - (Function.sin(a1) * Function.cos(a2));
        return new Point((int) Math.round(((((double) xp) * Function.sin(a2)) - (((double) yp) * Function.cos(a2))) / det), (int) Math.round(((((double) (-xp)) * Function.sin(a1)) + (((double) yp) * Function.cos(a1))) / det));
    }

    public void plot(Canvas cnvs, int x, int y, Paint paint) {
        drawLineThick(cnvs, x, y, x + 1, y + 1, paint);
    }

    public void plot(Canvas cnvs, Point p, Paint paint) {
        plot(cnvs, p.x, p.y, paint);
    }

    public boolean isInQ1(double angle) {
        return Function.sin(angle) >= 0.0d && Function.cos(angle) >= 0.0d;
    }

    public boolean isInQ2(double angle) {
        return Function.sin(angle) > 0.0d && Function.cos(angle) <= 0.0d;
    }

    public boolean isInQ3(double angle) {
        return Function.sin(angle) < 0.0d && Function.cos(angle) <= 0.0d;
    }

    public boolean isInQ4(double angle) {
        return Function.sin(angle) < 0.0d && Function.cos(angle) > 0.0d;
    }

    public double norm(int x, int y) {
        return Math.sqrt((double) ((x * x) + (y * y)));
    }

    public int normInt(int x, int y) {
        return (int) Math.round(norm(x, y));
    }

    public int normInt(Point p) {
        return normInt(p.x, p.y);
    }

    public int adjustGridColor() {
        return this.gridColor;
    }

    private void translateRel(Canvas cnvs, double x, double y) {
        this.ox = (int) (((double) this.ox) + x);
        this.oy = (int) (((double) this.oy) + y);
        cnvs.translate((float) x, (float) y);
    }

    private void translateAbs(Canvas cnvs) {
        cnvs.translate((float) (-this.ox), (float) (-this.oy));
        this.ox = 0;
        this.oy = 0;
    }

    public void setBoundaries(int margin2) {
        setBoundaries(margin2, margin2, margin2, margin2);
    }

    public void setBoundaries(int mLeft, int mRight, int mTop, int mBtm) {
        this.left = (-this.ox) + mLeft;
        this.right = (this.w - this.ox) - mRight;
        this.top = this.oy - mTop;
        this.btm = (-this.h) + this.oy + mBtm;
    }

    public int toCompCoor(double y) {
        if (y > ((double) (32767 - this.oy))) {
            return 32767 - this.oy;
        }
        if (y < ((double) (this.oy - 32768))) {
            return this.oy - 32767;
        }
        return (int) y;
    }

    public class Axis {
        public double angle;
        String id;
        double majorTicks;
        double minorTicks;
        double scale;

        public Axis(double angle2, double scale2, String id2) {
            this.minorTicks = 1.0d;
            this.majorTicks = 1.0d;
            this.angle = angle2;
            this.scale = scale2;
            this.minorTicks = 1.0d;
            this.majorTicks = 1.0d;
            this.id = id2;
        }

        public Axis(CoordinateSystem coordinateSystem, Axis axis) {
            this(axis.angle, axis.scale, axis.id);
        }

        public void setAngle(double angle2) {
            this.angle = Function.mod(angle2, 2.0d * 3.141592653589793d);
            if (this.angle > 3.141592653589793d) {
                this.angle -= 2.0d * 3.141592653589793d;
            }
            if (Function.abs(this.angle) < 1.0E-7d) {
                this.angle = 0.0d;
            }
            if (Function.abs(Function.abs(this.angle) - 3.141592653589793d) < 1.0E-7d) {
                this.angle = 3.141592653589793d;
            }
            if (Function.abs(this.angle - (3.141592653589793d / 2.0d)) < 1.0E-7d) {
                this.angle = 3.141592653589793d / 2.0d;
            }
            if (Function.abs(this.angle + (3.141592653589793d / 2.0d)) < 1.0E-7d) {
                this.angle = (-3.141592653589793d) / 2.0d;
            }
        }

        public void setAngle(int x, int y) {
            this.angle = Math.atan2((double) y, (double) x);
        }

        public void setAngle(Point p) {
            setAngle(p.x, p.y);
        }

        public boolean hasStrictlyPositiveSlope() {
            return Function.tan(this.angle) > 0.0d;
        }

        public boolean hasPositiveSlope() {
            return Function.tan(this.angle) >= 0.0d;
        }

        public boolean isHorizontal() {
            return Function.abs(Function.cos(this.angle)) == 1.0d;
        }

        public boolean isVertical() {
            return Function.abs(Function.sin(this.angle)) == 1.0d;
        }

        public void setScale(double d) {
            this.scale = d;
        }

        public void setMajorTicks(double units) {
            this.majorTicks = units;
        }

        public void setMinorTicks(double units) {
            this.minorTicks = units;
        }

        public double getAngle() {
            return this.angle;
        }

        public double getScale() {
            return this.scale;
        }

        public double getMajorTicks() {
            return this.majorTicks;
        }

        public double getMinorTicks() {
            return this.minorTicks;
        }

        public int iMajorTicks() {
            return (int) Math.round(this.majorTicks * this.scale * Function.cos(this.angle));
        }

        public int iMinorTicks() {
            return (int) Math.round(this.minorTicks * this.scale * Function.cos(this.angle));
        }

        public int jMajorTicks() {
            return (int) Math.round(this.majorTicks * this.scale * Function.sin(this.angle));
        }

        public int jMinorTicks() {
            return (int) Math.round(this.minorTicks * this.scale * Function.sin(this.angle));
        }

        public double pxlToUnit(int pxl) {
            return ((double) pxl) / this.scale;
        }

        public int unitToPxl(double unit) {
            return (int) (this.scale * unit);
        }

        public int iScale() {
            return (int) Math.round(this.scale * Function.cos(this.angle));
        }

        public int jScale() {
            return (int) Math.round(this.scale * Function.sin(this.angle));
        }

        public int deviationAt(int j) {
            return (int) Math.round(((double) j) * Function.cot(this.angle));
        }

        public int elevationAt(int i) {
            return (int) Math.round(((double) i) * Function.tan(this.angle));
        }

        public void setColor(int color) {
            CoordinateSystem.this.axisColor = color;
        }

        public int getColor() {
            return CoordinateSystem.this.axisColor;
        }

        public Point tip() {
            if (Function.cos(this.angle) == 1.0d) {
                return new Point(CoordinateSystem.this.right, 0);
            }
            if (Function.cos(this.angle) == -1.0d) {
                return new Point(CoordinateSystem.this.left, 0);
            }
            if (Function.sin(this.angle) == 1.0d) {
                return new Point(0, CoordinateSystem.this.top);
            }
            if (Function.sin(this.angle) == -1.0d) {
                return new Point(0, CoordinateSystem.this.btm);
            }
            if (Function.sin(this.angle) > 0.0d) {
                if (deviationAt(CoordinateSystem.this.top) >= CoordinateSystem.this.right) {
                    return new Point(CoordinateSystem.this.right, elevationAt(CoordinateSystem.this.right));
                }
                if (deviationAt(CoordinateSystem.this.top) > CoordinateSystem.this.left) {
                    return new Point(deviationAt(CoordinateSystem.this.top), CoordinateSystem.this.top);
                }
                return new Point(CoordinateSystem.this.left, elevationAt(CoordinateSystem.this.left));
            } else if (deviationAt(CoordinateSystem.this.btm) >= CoordinateSystem.this.right) {
                return new Point(CoordinateSystem.this.right, elevationAt(CoordinateSystem.this.right));
            } else {
                if (deviationAt(CoordinateSystem.this.btm) > CoordinateSystem.this.left) {
                    return new Point(deviationAt(CoordinateSystem.this.btm), CoordinateSystem.this.btm);
                }
                return new Point(CoordinateSystem.this.left, elevationAt(CoordinateSystem.this.left));
            }
        }

        public Point tail() {
            this.angle += 3.141592653589793d;
            Point p = tip();
            this.angle -= 3.141592653589793d;
            return p;
        }

        public int start() {
            if ((CoordinateSystem.this.ox < 0 || CoordinateSystem.this.ox > CoordinateSystem.this.w) && Function.abs(Function.sin(this.angle)) != 1.0d && ((CoordinateSystem.this.ox < 0 && Function.cos(this.angle) >= 0.0d) || (CoordinateSystem.this.ox > CoordinateSystem.this.w && Function.cos(this.angle) <= 0.0d))) {
                return normInt(tail());
            }
            return -normInt(tail());
        }

        public int end() {
            this.angle += 3.141592653589793d;
            int temp = -start();
            this.angle -= 3.141592653589793d;
            return temp;
        }

        public int top() {
            return Math.max(tail().y, tip().y);
        }

        public int btm() {
            return Math.min(tail().y, tip().y);
        }

        public int normInt(Point p) {
            return normInt(p.x, p.y);
        }

        public double norm(int x, int y) {
            return Math.sqrt((double) ((x * x) + (y * y)));
        }

        public double norm(Point p) {
            return norm(p.x, p.y);
        }

        public int normInt(int x, int y) {
            return (int) Math.round(norm(x, y));
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case 0:
                this.tActDwn = new Date();
                int i = (int) ev.getX();
                int j = (int) ev.getY();
                if (this.isPolarActive && this.rectCP_.contains(i, j)) {
                    this.cart = !this.cart;
                    this.polar = !this.cart;
                    invalidate();
                    return false;
                } else if (this.rectZoomIn.contains(i, j)) {
                    this.xAxis.scale *= 1.2d;
                    this.yAxis.scale *= 1.2d;
                    trimTicksZoom();
                    invalidate();
                    return false;
                } else if (this.rectZoomOut.contains(i, j)) {
                    this.xAxis.scale /= 1.2d;
                    this.yAxis.scale /= 1.2d;
                    trimTicksZoom();
                    invalidate();
                    return false;
                } else if (this.rectCoors.contains(i, j)) {
                    this.ox = this.w / 2;
                    this.oy = this.h / 2;
                    invalidate();
                    return false;
                } else {
                    this.mLastTouchX = (int) ev.getX();
                    this.mLastTouchY = (int) ev.getY();
                    return true;
                }
            case 1:
                Date tActUp = new Date();
                if (this.rectZoom.contains((float) this.mLastTouchX, (float) this.mLastTouchY) && tActUp.getTime() - this.tActDwn.getTime() > 100) {
                    this.xAxis.setScale(30.0d);
                    this.yAxis.setScale(30.0d);
                    this.xMajorTicks = "1";
                    this.yMajorTicks = "1";
                }
                this.movingGraph = false;
                invalidate();
                return false;
            case 2:
                float x = ev.getX();
                float y = ev.getY();
                int i2 = (int) (x - ((float) this.mLastTouchX));
                int i3 = (int) (y - ((float) this.mLastTouchY));
                this.ox += (int) (x - ((float) this.mLastTouchX));
                this.oy += (int) (y - ((float) this.mLastTouchY));
                this.mLastTouchX = (int) x;
                this.mLastTouchY = (int) y;
                this.movingGraph = true;
                invalidate();
                return true;
            default:
                return true;
        }
    }

    public boolean onTouch(View v, MotionEvent ev) {
        return false;
    }
}
