package com.calculatorgrapher.a;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;

public class GraphCanvas_FullScr extends Activity {
    String expr = "2sin(4x)";
    GraphCanvas gCanvas;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        this.gCanvas = new GraphCanvas(this);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.expr = extras.getString("expression");
            this.gCanvas.cart = extras.getString("cart").equals("true");
        }
        this.gCanvas.setFunction(this.expr);
        LinearLayout panel = new LinearLayout(this);
        panel.addView(this.gCanvas);
        setContentView(panel);
    }
}
