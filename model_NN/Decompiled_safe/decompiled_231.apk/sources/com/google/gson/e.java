package com.google.gson;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class e extends av {
    private final av[] a;

    public e(av... avVarArr) {
        if (avVarArr == null) {
            throw new NullPointerException("naming policies can not be null.");
        }
        this.a = avVarArr;
    }

    /* access modifiers changed from: protected */
    public final String a(String str, Type type, Collection<Annotation> collection) {
        String str2 = str;
        for (av a2 : this.a) {
            str2 = a2.a(str2, type, collection);
        }
        return str2;
    }
}
