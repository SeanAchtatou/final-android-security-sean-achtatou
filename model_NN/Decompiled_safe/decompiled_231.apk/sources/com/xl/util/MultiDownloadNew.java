package com.xl.util;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Vector;

public class MultiDownloadNew extends Thread {
    private long currentDownloadCnt = 0;
    private Vector<Download> currentDownloadVector;
    private long[] downloadFileSize = null;
    private long downloadtotal;
    private File[] fList;
    private long fileTotalSize = 0;
    private int idownloadcnt = 5;
    private long lastDownloadCnt = 0;
    private Integer percntInt = new Integer(0);
    private boolean stop = false;
    private String strDesFile = "";
    private String strSpeed = "";
    private String strTmpDir = "";
    private String strWebURL = "";

    public MultiDownloadNew(String strWebURL2, String strDesFile2) {
        this.strWebURL = strWebURL2;
        this.strDesFile = strDesFile2;
        this.downloadFileSize = new long[this.idownloadcnt];
        this.strTmpDir = strDesFile2.substring(0, strDesFile2.lastIndexOf("/"));
        initDownload();
    }

    public MultiDownloadNew(String strWebURL2, String strDesFile2, String strTmpDir2) {
        this.strWebURL = strWebURL2;
        this.strDesFile = strDesFile2;
        this.strTmpDir = strTmpDir2;
        this.downloadFileSize = new long[this.idownloadcnt];
        initDownload();
    }

    public MultiDownloadNew(int downloadint, String strWebURL2, String strDesFile2, String strTmpDir2) {
        this.idownloadcnt = downloadint;
        this.strWebURL = strWebURL2;
        this.strDesFile = strDesFile2;
        this.strTmpDir = strTmpDir2;
        this.downloadFileSize = new long[this.idownloadcnt];
        initDownload();
    }

    private boolean initDownload() {
        long start;
        long end;
        String strTmp = this.strDesFile.substring(0, this.strDesFile.lastIndexOf("/"));
        if (!FileUtil.fileExist(strTmp)) {
            try {
                FileUtil.createFolders(strTmp);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        GenUtil.systemPrint("strWebURL = " + this.strWebURL);
        GenUtil.systemPrint("strTmp = " + strTmp);
        try {
            try {
                this.fileTotalSize = (long) new URL(this.strWebURL).openConnection().getContentLength();
                GenUtil.systemPrint("fileTotalSize A= " + this.fileTotalSize);
                System.gc();
                this.currentDownloadCnt = this.fileTotalSize / ((long) this.idownloadcnt);
                this.lastDownloadCnt = this.fileTotalSize - (this.currentDownloadCnt * ((long) (this.idownloadcnt - 1)));
                this.currentDownloadVector = new Vector<>(this.idownloadcnt);
                this.fList = new File[this.idownloadcnt];
                String strRandom = ((int) (Math.random() * 100000.0d)) + "";
                for (int i = 1; i <= this.idownloadcnt; i++) {
                    if (i == this.idownloadcnt) {
                        start = ((long) (i - 1)) * this.currentDownloadCnt;
                        end = this.fileTotalSize - 1;
                    } else {
                        start = ((long) (i - 1)) * this.currentDownloadCnt;
                        end = (((long) i) * this.currentDownloadCnt) - 1;
                    }
                    String strTmpFile = this.strTmpDir + strRandom + i + ".x";
                    this.fList[i - 1] = new File(strTmpFile);
                    this.currentDownloadVector.add(new Download(i, start, end, this.strWebURL, strTmpFile));
                    GenUtil.systemPrintln("start = " + start);
                    GenUtil.systemPrintln("end = " + end);
                    GenUtil.systemPrintln("fileTotalSize = " + this.fileTotalSize);
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        } catch (MalformedURLException e3) {
            e3.printStackTrace();
        }
        return false;
    }

    public void run() {
        InterruptedException e;
        Date date;
        for (int i = 0; i < this.currentDownloadVector.size(); i++) {
            this.currentDownloadVector.get(i).start();
        }
        boolean downloadresult = true;
        Date date2 = new Date();
        long lasttime = date2.getTime();
        long lastSum = 0;
        Date date3 = date2;
        while (true) {
            if (!downloadresult) {
                break;
            }
            try {
                sleep(500);
                if (this.stop) {
                    for (int i2 = 0; i2 < this.currentDownloadVector.size(); i2++) {
                        this.currentDownloadVector.get(i2).runStop(this.stop);
                    }
                } else {
                    this.downloadtotal = 0;
                    for (int i3 = 0; i3 < this.currentDownloadVector.size(); i3++) {
                        this.downloadtotal = this.downloadtotal + this.currentDownloadVector.get(i3).getBytesum();
                    }
                    if (this.fileTotalSize != this.downloadtotal) {
                        this.percntInt = Integer.valueOf((int) ((100 * this.downloadtotal) / this.fileTotalSize));
                        Long valueOf = Long.valueOf(this.downloadtotal - lastSum);
                        date = new Date();
                        try {
                            long thistime = date.getTime();
                            this.strSpeed = formaSpeed(this.downloadtotal - lastSum, thistime - lasttime);
                            lasttime = thistime;
                            lastSum = this.downloadtotal;
                        } catch (InterruptedException e2) {
                            e = e2;
                            e.printStackTrace();
                            date3 = date;
                        }
                    } else {
                        for (int i4 = 0; i4 < this.currentDownloadVector.size(); i4++) {
                            this.currentDownloadVector.get(i4).interrupt();
                            this.currentDownloadVector.remove(i4);
                        }
                        GenUtil.systemPrint("strDesFile = " + this.strDesFile);
                        FileUtil.Filecombine(this.fList, this.strDesFile);
                        this.percntInt = 100;
                        downloadresult = false;
                        date = date3;
                    }
                    date3 = date;
                }
            } catch (InterruptedException e3) {
                e = e3;
                date = date3;
                e.printStackTrace();
                date3 = date;
            }
        }
        super.run();
    }

    public int getPercntInt() {
        return this.percntInt.intValue();
    }

    public void stopRun() {
        this.stop = true;
    }

    public void setPercntInt(int percntInt2) {
        this.percntInt = Integer.valueOf(percntInt2);
    }

    public long getFileTotalSize() {
        return this.fileTotalSize;
    }

    public long getFileDownloadTotal() {
        return this.downloadtotal;
    }

    private String formaSpeed(long bData, long iTimeMS) {
        if (iTimeMS == 0) {
            return "0KB/s";
        }
        return OtherUtil.FormatSpeed((1000 * bData) / iTimeMS);
    }

    public String getSpeed() {
        return this.strSpeed;
    }
}
