package wall.bt.wp.P022;

import android.app.TabActivity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import com.xl.util.AssetUtil;
import com.xl.util.BookMarkUtil;
import com.xl.util.FileMatch;
import com.xl.util.FileUtil;
import com.xl.util.Folder;
import com.xl.util.GenUtil;
import com.xl.util.XMEnDecrypt;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class MainActivity extends TabActivity {
    private static final int Tab_Favorite = 2;
    private static final int Tab_Hot = 1;
    private static final int Tab_More = 3;
    private static final int Tab_Recent = 0;
    public static String strLocalImgRootURLCache = "";
    public static String strLocalImgRootURLMyCollection = "";
    public static String strWebImgRootURL = "";
    public static TabHost tabHost = null;
    private int iNowTab = 0;
    private int iPreTab = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initTab() {
        tabHost = getTabHost();
        TabWidget tabWidget = tabHost.getTabWidget();
        Resources resources = getResources();
        LayoutInflater.from(this).inflate((int) R.layout.main, (ViewGroup) tabHost.getTabContentView(), true);
        String strMyAlbum = getResources().getString(R.string.txt_myalbum);
        String strMoreAlbum = getResources().getString(R.string.txt_morealbum);
        String strMoreApk = getResources().getString(R.string.txt_moreapk);
        tabHost.addTab(tabHost.newTabSpec("1").setIndicator(strMyAlbum, getResources().getDrawable(R.drawable.current)).setContent(new Intent(this, LocalImg.class)));
        tabHost.addTab(tabHost.newTabSpec("2").setIndicator(strMoreAlbum, getResources().getDrawable(R.drawable.morealbum)).setContent(new Intent(this, WebMoreImg.class)));
        tabHost.addTab(tabHost.newTabSpec("3").setIndicator(strMoreApk, getResources().getDrawable(R.drawable.moreapk)).setContent(new Intent(this, MoreApk.class)));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(1024);
        requestWindowFeature(1);
        strWebImgRootURL = getResources().getString(R.string.webimgrooturl);
        strLocalImgRootURLCache = Folder.MYCACHE;
        strLocalImgRootURLMyCollection = Folder.MYCOLLECTION;
        copyAssetWallpaper();
        initTab();
    }

    private void copyAssetWallpaper() {
        String strAppName = getResources().getString(R.string.app_name).replace(" ", "");
        String strDestFolder = (Environment.getExternalStorageState().equals("mounted") ? Folder.MYCOLLECTION : Folder.MYCOLLECTION1) + strAppName + "/";
        List<String> listFile = null;
        try {
            listFile = getSplitsFiles("index.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (listFile == null) {
            GenUtil.systemPrintln("No packaged wallpapers");
            return;
        }
        if (!FileUtil.fileExist(strDestFolder)) {
            try {
                FileUtil.createFolders(strDestFolder);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            AssetUtil aUtil = new AssetUtil(this);
            aUtil.releaseAssetRes("index.xml", strDestFolder + "index.xml", true);
            GenUtil.systemPrintln("packaged wallpapers = " + listFile.size());
            aUtil.releaseAssetRes("index.jpg", strDestFolder + "index.jpg", true);
            for (int i = 0; i < listFile.size(); i++) {
                String strOrigFile = listFile.get(i);
                aUtil.releaseAssetRes(strOrigFile, strDestFolder + strOrigFile, true);
                String strOrigFile2 = strOrigFile.replace("sm_", "");
                Boolean releaseAssetResRet = aUtil.releaseAssetResRet(strOrigFile2, strDestFolder + strOrigFile2, true);
                GenUtil.systemPrintln("");
            }
        }
        new BookMarkUtil(Environment.getExternalStorageState().equals("mounted") ? Folder.MYBOOKMARK : Folder.MYBOOKMARK1, "bookmark").add(strAppName + "|||" + strDestFolder + "index.xml" + "|||" + strDestFolder + "index.jpg");
    }

    public List<String> getSplitsFiles(String strFileName) throws IOException {
        UnsupportedEncodingException e;
        List<String> lstFiles = null;
        InputStream finstream = getAssets().open(strFileName);
        if (finstream != null) {
            int iLen = finstream.available();
            byte[] btBuffer = new byte[iLen];
            if (finstream.read(btBuffer) == iLen) {
                try {
                    String strContent = new String(XMEnDecrypt.XMDecryptString(btBuffer), "utf-8");
                    try {
                        GenUtil.systemPrintln(strContent);
                        lstFiles = FileMatch.matchRootListNoencode(strContent, "Name");
                    } catch (UnsupportedEncodingException e2) {
                        e = e2;
                    }
                } catch (UnsupportedEncodingException e3) {
                    e = e3;
                    e.printStackTrace();
                    finstream.close();
                    return lstFiles;
                }
            }
            try {
                finstream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        }
        return lstFiles;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        getMenuInflater().inflate(R.menu.menu, paramMenu);
        return super.onCreateOptionsMenu(paramMenu);
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case R.id.shortcut /*2131230787*/:
                addShortCut();
                return true;
            case R.id.share /*2131230788*/:
                Intent intent1 = new Intent("android.intent.action.SEND");
                intent1.putExtra("android.intent.extra.EMAIL", new String[0]);
                intent1.putExtra("android.intent.extra.TEXT", getResources().getString(R.string.txt_mailcontent));
                intent1.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.txt_mailsubject));
                intent1.setType("text/plain");
                startActivity(Intent.createChooser(intent1, getResources().getString(R.string.txt_choice)));
                return true;
            default:
                return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void addShortCut() {
        Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcut.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        shortcut.putExtra("duplicate", false);
        shortcut.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(getPackageName(), "." + getLocalClassName())));
        shortcut.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.icon));
        sendBroadcast(shortcut);
    }
}
