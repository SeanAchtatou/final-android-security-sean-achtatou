package com.xl.wp.yeexm;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class color {
        public static final int solid_blue = 2130968577;
        public static final int solid_green = 2130968578;
        public static final int solid_red = 2130968576;
        public static final int solid_yellow = 2130968579;
    }

    public static final class drawable {
        public static final int bk = 2130837513;
        public static final int blue = 2130837572;
        public static final int bottom = 2130837504;
        public static final int btn_refresh = 2130837505;
        public static final int btn_refresh_press = 2130837506;
        public static final int buy = 2130837507;
        public static final int cancel = 2130837508;
        public static final int cancel_pressed = 2130837509;
        public static final int current = 2130837514;
        public static final int download = 2130837510;
        public static final int error_h = 2130837511;
        public static final int error_s = 2130837512;
        public static final int file = 2130837515;
        public static final int file_blank = 2130837516;
        public static final int file_excel = 2130837517;
        public static final int file_image = 2130837518;
        public static final int file_music = 2130837519;
        public static final int file_package = 2130837520;
        public static final int file_pdf = 2130837521;
        public static final int file_powerpoint = 2130837522;
        public static final int file_txt = 2130837523;
        public static final int file_video = 2130837524;
        public static final int file_word = 2130837525;
        public static final int flag = 2130837526;
        public static final int folder = 2130837527;
        public static final int go = 2130837528;
        public static final int go_s = 2130837529;
        public static final int green = 2130837573;
        public static final int ic_menu_about = 2130837532;
        public static final int ic_menu_add = 2130837530;
        public static final int ic_menu_paid = 2130837533;
        public static final int ic_menu_refresh = 2130837534;
        public static final int ic_menu_save_draft = 2130837535;
        public static final int ic_menu_send = 2130837536;
        public static final int ic_menu_set_wallpaper = 2130837537;
        public static final int ic_menu_star = 2130837538;
        public static final int icon = 2130837539;
        public static final int icon_error = 2130837540;
        public static final int info = 2130837541;
        public static final int morealbum = 2130837531;
        public static final int moreapk = 2130837542;
        public static final int next = 2130837544;
        public static final int next1 = 2130837545;
        public static final int next_press = 2130837546;
        public static final int next_press1 = 2130837547;
        public static final int next_s = 2130837548;
        public static final int no = 2130837549;
        public static final int ok = 2130837550;
        public static final int ok_pressed = 2130837551;
        public static final int open = 2130837552;
        public static final int prev = 2130837553;
        public static final int prev1 = 2130837554;
        public static final int prev_press = 2130837555;
        public static final int prev_press1 = 2130837556;
        public static final int prev_s = 2130837557;
        public static final int preva = 2130837543;
        public static final int preva_s = 2130837558;
        public static final int red = 2130837571;
        public static final int screen_background_black = 2130837575;
        public static final int tick = 2130837560;
        public static final int toast_warnning = 2130837561;
        public static final int translucent_background = 2130837576;
        public static final int transparent_background = 2130837559;
        public static final int up = 2130837562;
        public static final int xml_btn_cancel = 2130837563;
        public static final int xml_btn_goto = 2130837564;
        public static final int xml_btn_next = 2130837565;
        public static final int xml_btn_next1 = 2130837566;
        public static final int xml_btn_ok = 2130837567;
        public static final int xml_btn_prev = 2130837568;
        public static final int xml_btn_prev1 = 2130837569;
        public static final int xml_btn_refresh = 2130837570;
        public static final int yellow = 2130837574;
    }

    public static final class id {
        public static final int BANNER = 2131230768;
        public static final int IAB_BANNER = 2131230770;
        public static final int IAB_LEADERBOARD = 2131230771;
        public static final int IAB_MRECT = 2131230769;
        public static final int about = 2131230767;
        public static final int about_layout = 2131230720;
        public static final int about_us = 2131230721;
        public static final int ad = 2131230743;
        public static final int addfav_cancel = 2131230725;
        public static final int addfav_ok = 2131230724;
        public static final int addfav_txt = 2131230723;
        public static final int album = 2131230726;
        public static final int album_list = 2131230760;
        public static final int album_refresh = 2131230727;
        public static final int apps_name = 2131230729;
        public static final int apps_status = 2131230730;
        public static final int apps_thumb = 2131230728;
        public static final int clearcache = 2131230783;
        public static final int deletealbum = 2131230780;
        public static final int directorypicker_btn_cancel = 2131230734;
        public static final int directorypicker_btn_save = 2131230733;
        public static final int directorypicker_iv_icon = 2131230735;
        public static final int directorypicker_lv_directory = 2131230732;
        public static final int directorypicker_tv_name = 2131230736;
        public static final int download_percent = 2131230757;
        public static final int email_us = 2131230766;
        public static final int goto_cancel = 2131230742;
        public static final int goto_end_txt = 2131230740;
        public static final int goto_ok = 2131230741;
        public static final int goto_page = 2131230755;
        public static final int goto_page_edt = 2131230739;
        public static final int goto_txt = 2131230738;
        public static final int grid = 2131230744;
        public static final int grid_img = 2131230745;
        public static final int image = 2131230746;
        public static final int img_progressbar = 2131230749;
        public static final int iv_img_cat = 2131230773;
        public static final int layout = 2131230737;
        public static final int mainback = 2131230775;
        public static final int myfavorite_album_list = 2131230751;
        public static final int myfavorite_prompt = 2131230752;
        public static final int next = 2131230748;
        public static final int our_email = 2131230722;
        public static final int page_info = 2131230756;
        public static final int prev = 2131230747;
        public static final int progress = 2131230758;
        public static final int progress_layout = 2131230754;
        public static final int prompt_txt = 2131230759;
        public static final int refresh = 2131230778;
        public static final int save = 2131230764;
        public static final int savegrid = 2131230779;
        public static final int set_wallpaper = 2131230765;
        public static final int share = 2131230782;
        public static final int shortcut = 2131230781;
        public static final int star = 2131230731;
        public static final int toast_img = 2131230762;
        public static final int toast_title = 2131230761;
        public static final int toast_txt = 2131230763;
        public static final int top_layout = 2131230753;
        public static final int tv_txt_cat = 2131230774;
        public static final int txt_progress = 2131230750;
        public static final int web = 2131230777;
        public static final int webparent = 2131230776;
        public static final int wv_page = 2131230772;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int add_favorite_dialog = 2130903041;
        public static final int album_list = 2130903042;
        public static final int app_list = 2130903043;
        public static final int directorypicker = 2130903044;
        public static final int directorypicker_listitem = 2130903045;
        public static final int file_web_view = 2130903054;
        public static final int goto_dialog = 2130903046;
        public static final int grid = 2130903047;
        public static final int grid_category = 2130903055;
        public static final int grid_img = 2130903048;
        public static final int image = 2130903049;
        public static final int main = 2130903050;
        public static final int moreimg = 2130903052;
        public static final int my_favorite = 2130903051;
        public static final int toast_view = 2130903053;
        public static final int webmoreimg = 2130903056;
    }

    public static final class menu {
        public static final int apk_menu = 2131165186;
        public static final int grid_menu = 2131165187;
        public static final int image_menu = 2131165184;
        public static final int local_menu = 2131165188;
        public static final int menu = 2131165185;
        public static final int more_menu = 2131165189;
    }

    public static final class string {
        public static final int about_us = 2131034138;
        public static final int add2favorite_success = 2131034123;
        public static final int add_fav_confirm = 2131034118;
        public static final int album_exist_favorite = 2131034124;
        public static final int app_name = 2131034113;
        public static final int btn_cancel = 2131034152;
        public static final int btn_ok = 2131034151;
        public static final int can_not_start_apk = 2131034130;
        public static final int directorypicker_btn_cancel = 2131034140;
        public static final int directorypicker_btn_save = 2131034139;
        public static final int directorypicker_dlg_overwrite_cancel = 2131034144;
        public static final int directorypicker_dlg_overwrite_caption = 2131034142;
        public static final int directorypicker_dlg_overwrite_message = 2131034141;
        public static final int directorypicker_dlg_overwrite_ok = 2131034143;
        public static final int directorypicker_listview_up = 2131034145;
        public static final int enter_error_number = 2131034129;
        public static final int enter_numeric_error = 2131034128;
        public static final int exit_confirm = 2131034121;
        public static final int goto_page = 2131034127;
        public static final int hello = 2131034112;
        public static final int long_click_prompt = 2131034126;
        public static final int menu_about_us = 2131034133;
        public static final int menu_add_favor = 2131034136;
        public static final int menu_email_us = 2131034132;
        public static final int menu_paid_version = 2131034131;
        public static final int menu_save = 2131034134;
        public static final int menu_set_wallpaper = 2131034135;
        public static final int myfavorite_prompt = 2131034122;
        public static final int network_error = 2131034114;
        public static final int our_email = 2131034137;
        public static final int page_info_head = 2131034147;
        public static final int refresh = 2131034115;
        public static final int reload_success = 2131034117;
        public static final int remove_fav_confirm = 2131034119;
        public static final int remove_favorite_success = 2131034125;
        public static final int save_image_sucess = 2131034146;
        public static final int tip_loading = 2131034186;
        public static final int txt_addbookmark_body = 2131034154;
        public static final int txt_album_save = 2131034163;
        public static final int txt_album_save_exist = 2131034164;
        public static final int txt_bookmark_title = 2131034153;
        public static final int txt_choice = 2131034185;
        public static final int txt_clearcache = 2131034166;
        public static final int txt_clearcache_wait = 2131034167;
        public static final int txt_delbookmark_body = 2131034155;
        public static final int txt_deletealbum = 2131034168;
        public static final int txt_deletealbum_msg = 2131034169;
        public static final int txt_download_msg = 2131034178;
        public static final int txt_download_msg_cancel = 2131034180;
        public static final int txt_download_msg_error = 2131034179;
        public static final int txt_lastimg_body = 2131034177;
        public static final int txt_lastimg_title = 2131034176;
        public static final int txt_mailcontent = 2131034183;
        public static final int txt_mailsubject = 2131034184;
        public static final int txt_morealbum = 2131034171;
        public static final int txt_moreapk = 2131034172;
        public static final int txt_myalbum = 2131034170;
        public static final int txt_no_wifi_connection = 2131034161;
        public static final int txt_no_wifi_data = 2131034162;
        public static final int txt_opening = 2131034165;
        public static final int txt_quit_body = 2131034174;
        public static final int txt_quit_title = 2131034173;
        public static final int txt_refresh = 2131034175;
        public static final int txt_savecollection = 2131034156;
        public static final int txt_setWallpaper_body = 2131034159;
        public static final int txt_setWallpaper_title = 2131034158;
        public static final int txt_setwallpaper = 2131034157;
        public static final int txt_share = 2131034182;
        public static final int txt_shortcut = 2131034181;
        public static final int txt_wifi_none_title = 2131034160;
        public static final int update_prompt = 2131034116;
        public static final int verify_confirm = 2131034120;
        public static final int webapkurl = 2131034150;
        public static final int webimgrooturl = 2131034148;
        public static final int webimgrooturlHtml = 2131034149;
    }

    public static final class style {
        public static final int Theme_Translucent = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {wall.bt.wp.w184.R.attr.adSize, wall.bt.wp.w184.R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
