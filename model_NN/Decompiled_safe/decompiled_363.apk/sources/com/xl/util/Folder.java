package com.xl.util;

public class Folder {
    public static final String AUDIO = "Audio";
    public static final String BOOK = "Book";
    public static final String IMAGE = "Image";
    public static final String MYBOOKMARK = "/sdcard/PopImg/mycollection/bookmark.xml";
    public static final String MYBOOKMARK1 = "/data/PopImg/mycollection/bookmark.xml";
    public static final String MYCACHE = "/sdcard/PopImg/cache/";
    public static final String MYCACHE1 = "/data/PopImg/cache/";
    public static final String MYCOLLECTION = "/sdcard/PopImg/mycollection/";
    public static final String MYCOLLECTION1 = "/data/PopImg/mycollection/";
    public static final String OTHER = "Other";
    public static final String POPIMG = "/sdcard/PopImg/";
    public static final String POPIMG1 = "/data/PopImg/";
    public static final String SOFT = "Soft";
    public static final String TMP = "Tmp";
    public static final String URL = "Url";
    public static final String USER = "user";
    public static final String VIDEO = "Video";
    public static final String WEB = "Web";
}
