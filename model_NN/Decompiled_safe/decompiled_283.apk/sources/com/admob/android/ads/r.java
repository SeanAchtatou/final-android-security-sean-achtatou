package com.admob.android.ads;

import android.os.Build;
import android.util.Log;
import java.net.URL;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* compiled from: AdMobConnector */
public abstract class r implements Runnable {
    private static Executor m = null;
    private static String n;
    protected String a;
    protected int b;
    protected Exception c = null;
    protected Map<String, String> d;
    protected int e;
    protected int f;
    protected String g;
    protected h h;
    protected URL i;
    protected byte[] j;
    protected boolean k;
    protected String l;
    private String o;

    public abstract boolean a();

    public abstract void b();

    public static String c() {
        if (n == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(" Build/");
                stringBuffer.append(str3);
            }
            n = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (AdMob-ANDROID-%s)", stringBuffer, AdManager.SDK_VERSION_DATE);
            if (Log.isLoggable(AdManager.LOG, 3)) {
                Log.d(AdManager.LOG, "Phone's user-agent is:  " + n);
            }
        }
        return n;
    }

    protected r(String str, String str2, h hVar, int i2, Map<String, String> map, String str3) {
        this.o = str;
        this.g = str2;
        this.h = hVar;
        this.b = i2;
        this.d = map;
        this.k = true;
        this.e = 0;
        this.f = 3;
        if (str3 != null) {
            this.l = str3;
            this.a = "application/x-www-form-urlencoded";
            return;
        }
        this.l = null;
        this.a = null;
    }

    public final byte[] d() {
        return this.j;
    }

    public final String e() {
        return this.o;
    }

    public final URL f() {
        return this.i;
    }

    public final void g() {
        if (m == null) {
            m = Executors.newCachedThreadPool();
        }
        m.execute(this);
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.a = str;
    }

    public final void a(int i2) {
        this.f = i2;
    }
}
