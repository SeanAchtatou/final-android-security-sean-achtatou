package com.feasy.app.memory.HalloweenMemoryP2;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Applause extends Activity {
    private final int MAX_GAME_TIME = 100;
    private final String PREFS_NAME = "com.feasy.app.memory.HalloweenMemoryP2";
    private final int REQUEST_CODE = 2;
    /* access modifiers changed from: private */
    public int[] checkedImageList = new int[20];
    /* access modifiers changed from: private */
    public int[] checkedTwoImg = new int[2];
    /* access modifiers changed from: private */
    public int finishednum = 0;
    Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            if (Applause.this.mGameStatus == GAME_STATUS.GS_PLAYING) {
                switch (msg.what) {
                    case 1:
                        if (!Applause.this.updateProgressBar()) {
                            boolean unused = Applause.this.setGameOver();
                            break;
                        }
                        break;
                }
            }
            super.handleMessage(msg);
        }
    };
    private ImageView img_play;
    private ImageView img_ringtone;
    private boolean isShowAd;
    /* access modifiers changed from: private */
    public int lastcheckedImageId = -1;
    private int level = 1;
    private ProgressBar mBar;
    private boolean mChangeTheme;
    AlertDialog mExitDlg;
    private boolean mGameMusic;
    AlertDialog mGameOverDlg;
    GAME_STATUS mGameStatus;
    private int mGameTime;
    private ImageView mImgMain;
    private ListView mList;
    private boolean mMatchSound;
    private MediaPlayer mMusicPlayer;
    private int mPlayIndex;
    private View mPlayView;
    private int mSkinIndex;
    private MediaPlayer mSoundPlayer;
    TimerTask mTask = new TimerTask() {
        public void run() {
            Message message = new Message();
            message.what = 1;
            Applause.this.handler.sendMessage(message);
        }
    };
    Timer mTimer = new Timer();
    private TextView mTvTime;
    private boolean mVibrate;
    private boolean m_IsSilent = false;
    private int m_ScreenHeight = 480;
    private int m_ScreenWidth = 320;
    /* access modifiers changed from: private */
    public int maxfinishednum = 20;
    public int[] plants = {R.drawable.aa, R.drawable.bb, R.drawable.cc, R.drawable.dd, R.drawable.ee, R.drawable.ff, R.drawable.gg, R.drawable.hh, R.drawable.ii, R.drawable.jj, R.drawable.kk};
    /* access modifiers changed from: private */
    public int[] randImage = new int[20];
    /* access modifiers changed from: private */
    public int score = 0;
    private int[] selectedImage = new int[20];

    private enum GAME_STATUS {
        GS_INIT,
        GS_PLAYING,
        GS_PAUSE,
        GS_OVER
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        this.m_ScreenWidth = display.getWidth();
        this.m_ScreenHeight = display.getHeight();
        setContentView((int) R.layout.main);
        this.mTvTime = (TextView) findViewById(R.id.game_time);
        this.mImgMain = (ImageView) findViewById(R.id.game_icon);
        this.mBar = (ProgressBar) findViewById(R.id.game_timebar);
        this.mGameTime = 100;
        this.mGameStatus = GAME_STATUS.GS_INIT;
        this.mPlayIndex = -1;
        this.mPlayView = null;
        this.mSoundPlayer = null;
        this.m_IsSilent = false;
        loadGameParam();
        initGame();
        this.mTimer.schedule(this.mTask, 1000, 1000);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2 && resultCode == -1) {
            this.mGameMusic = data.getBooleanExtra("GameMusic", false);
            this.mMatchSound = data.getBooleanExtra("MatchSound", true);
            this.mVibrate = data.getBooleanExtra("Vibrate", true);
            this.mChangeTheme = data.getBooleanExtra("ChangeTheme", true);
            if (this.mGameMusic) {
                playMusic();
            } else {
                stopMusic();
            }
        }
    }

    public void initGame() {
        this.finishednum = 0;
        if (-1 == this.mSkinIndex) {
            setRandomSkin();
        } else {
            setNextSkin();
        }
        updateBkg();
        int[] newlist = new int[(this.plants.length * 2)];
        System.arraycopy(this.plants, 0, newlist, 0, this.plants.length);
        System.arraycopy(this.plants, 0, newlist, this.plants.length, this.plants.length);
        int r = new Random().nextInt(this.plants.length);
        System.arraycopy(newlist, r, this.selectedImage, 0, this.maxfinishednum / 2);
        System.arraycopy(newlist, r, this.selectedImage, this.maxfinishednum / 2, this.maxfinishednum / 2);
        randImageType(this.selectedImage);
        for (int i = 0; i < this.maxfinishednum; i++) {
            clickImage((ImageView) findViewById(R.id.img11 + i), i);
            this.checkedImageList[i] = 0;
        }
        if (!(this.mGameStatus == GAME_STATUS.GS_PAUSE || this.mGameStatus == GAME_STATUS.GS_PLAYING)) {
            Log.v("main", "inigame(), mGameStatus = " + this.mGameStatus.toString());
        }
        this.mGameStatus = GAME_STATUS.GS_INIT;
        this.mGameTime = Math.max(30, 100 - ((this.level - 1) * 10));
        this.mBar.setMax(this.mGameTime);
        this.mBar.setProgress(this.mGameTime);
        this.mGameStatus = GAME_STATUS.GS_PLAYING;
        updateScoreAndLevel();
        updateGameIcon();
        updateTimeText();
        playMusic();
    }

    public void newGame() {
        this.level = 1;
        this.score = 0;
        initGame();
    }

    /* access modifiers changed from: private */
    public boolean setGameOver() {
        Log.v("main", "setGameOver(), mTimer.cancel()");
        this.mGameStatus = GAME_STATUS.GS_OVER;
        showGameOverDlg();
        return true;
    }

    private boolean setGamePause() {
        if (this.mGameStatus == GAME_STATUS.GS_PAUSE) {
            this.mGameStatus = GAME_STATUS.GS_PLAYING;
            return true;
        } else if (this.mGameStatus != GAME_STATUS.GS_PLAYING) {
            return true;
        } else {
            this.mGameStatus = GAME_STATUS.GS_PAUSE;
            return true;
        }
    }

    public void clickImage(final ImageView img, final int rid) {
        img.setBackgroundResource(getCurSkinImageId(2));
        img.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Applause.this.mGameStatus == GAME_STATUS.GS_PAUSE) {
                    Toast.makeText(Applause.this, "Game is pause,please resume first!", 0).show();
                } else if (Applause.this.mGameStatus == GAME_STATUS.GS_OVER) {
                    Applause.this.newGame();
                } else {
                    if (!(Applause.this.lastcheckedImageId != -1 || Applause.this.checkedTwoImg[0] == -1 || Applause.this.checkedTwoImg[1] == -1)) {
                        Applause.this.doCountDown(Applause.this.checkedTwoImg[0]);
                        Applause.this.doCountDown(Applause.this.checkedTwoImg[1]);
                    }
                    if (Applause.this.finishednum == Applause.this.maxfinishednum) {
                        Applause.this.setNextLevel();
                        Applause.this.initGame();
                        Applause.this.updateScoreAndLevel();
                    }
                    if (Applause.this.checkedImageList[rid] != 1 && rid != Applause.this.lastcheckedImageId) {
                        img.setBackgroundResource(Applause.this.randImage[rid]);
                        int randType = R.anim.slide_in_left;
                        int n = new Random().nextInt(6);
                        if (n == 0) {
                            randType = R.anim.slide_in_left;
                        } else if (n == 1) {
                            randType = R.anim.slide_out_left;
                        } else if (n == 2) {
                            randType = R.anim.slide_out_right;
                        } else if (n == 3) {
                            randType = R.anim.alpha_scale_rotate;
                        } else if (n == 4) {
                            randType = R.anim.grow_fade_in_center;
                        }
                        Animation anim2 = AnimationUtils.loadAnimation(Applause.this, randType);
                        anim2.setDuration(400);
                        img.startAnimation(anim2);
                        if (Applause.this.lastcheckedImageId == -1) {
                            Applause.this.lastcheckedImageId = rid;
                            Applause.this.checkedTwoImg[0] = rid;
                        } else if (Applause.this.randImage[rid] == Applause.this.randImage[Applause.this.lastcheckedImageId]) {
                            Applause.this.checkedImageList[rid] = 1;
                            Applause.this.checkedImageList[Applause.this.lastcheckedImageId] = 1;
                            Applause.this.lastcheckedImageId = -1;
                            Applause applause = Applause.this;
                            applause.score = applause.score + 10;
                            Applause.this.updateScoreAndLevel();
                            Applause.this.checkedTwoImg[0] = -1;
                            Applause.this.checkedTwoImg[1] = -1;
                            Applause applause2 = Applause.this;
                            applause2.finishednum = applause2.finishednum + 2;
                            Applause.this.playSound();
                            Applause.this.gameAddTime();
                            if (Applause.this.finishednum >= Applause.this.maxfinishednum) {
                                Applause.this.mGameStatus = GAME_STATUS.GS_INIT;
                                Toast toast = Toast.makeText(Applause.this, "WellDone, Tap to the next level", 1);
                                toast.setGravity(49, 0, 280);
                                toast.show();
                            }
                        } else {
                            Applause.this.checkedTwoImg[1] = rid;
                            Applause.this.lastcheckedImageId = -1;
                            Applause.this.score = Math.max(0, Applause.this.score - 1);
                            Applause.this.updateScoreAndLevel();
                            Applause.this.playVibrate();
                        }
                    }
                }
            }
        });
    }

    public boolean updateProgressBar() {
        if (this.mBar == null) {
            return true;
        }
        this.mGameTime--;
        if (this.mGameTime >= 0) {
            this.mBar.setProgress(this.mGameTime);
            updateTimeText();
        }
        if (this.mGameTime > 0) {
            return true;
        }
        this.mGameTime = 0;
        return false;
    }

    public void updateTimeText() {
        this.mTvTime.setText(" " + Integer.toString(this.mGameTime) + "s");
    }

    public void updateGameIcon() {
        int iconIndex = this.level - 1;
        if (this.level >= this.plants.length) {
            iconIndex = 0;
        }
        this.mImgMain.setBackgroundResource(this.plants[iconIndex]);
    }

    public void gameAddTime() {
        if (this.mGameStatus == GAME_STATUS.GS_PLAYING) {
            this.mGameTime += 5;
        }
    }

    /* access modifiers changed from: private */
    public void setNextLevel() {
        this.level++;
    }

    public void doCountDown(int resid) {
        int imgResId = getCurSkinImageId(2);
        ImageView img = (ImageView) findViewById(R.id.img11 + resid);
        img.setBackgroundResource(imgResId);
        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
        anim2.setDuration(300);
        img.startAnimation(anim2);
    }

    public void randImageType(int[] selectedImage2) {
        int len = selectedImage2.length;
        int[] result = new int[len];
        Random random = new Random();
        for (int i = 0; i < len; i++) {
            int r = random.nextInt(len - i);
            result[i] = selectedImage2[r];
            selectedImage2[r] = selectedImage2[(len - 1) - i];
        }
        for (int i2 = 0; i2 < this.maxfinishednum; i2++) {
            this.randImage[i2] = result[i2];
        }
    }

    public void playVibrate() {
        if (this.mVibrate) {
            try {
                ((Vibrator) getSystemService("vibrator")).vibrate(new long[]{10, 30, 20, 10}, -1);
            } catch (Exception e) {
            }
        }
    }

    public void playSound() {
        if (this.mMatchSound) {
            stopSound();
            if (this.mSoundPlayer == null) {
                this.mSoundPlayer = MediaPlayer.create(getBaseContext(), (int) R.raw.matchit);
            }
            this.mSoundPlayer.start();
        }
    }

    public void stopSound() {
        if (this.mSoundPlayer != null) {
            if (this.mSoundPlayer.isPlaying()) {
                this.mSoundPlayer.stop();
            }
            this.mSoundPlayer.release();
            this.mSoundPlayer = null;
        }
    }

    public void playMusic() {
        if (this.mGameMusic) {
            stopMusic();
            if (this.mMusicPlayer == null) {
                this.mMusicPlayer = MediaPlayer.create(getBaseContext(), (int) R.raw.music);
            }
            this.mMusicPlayer.start();
            this.mMusicPlayer.setLooping(true);
        }
    }

    public void stopMusic() {
        if (this.mMusicPlayer != null) {
            if (this.mMusicPlayer.isPlaying()) {
                this.mMusicPlayer.stop();
            }
            this.mMusicPlayer.release();
            this.mMusicPlayer = null;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        showExitDlg();
        return true;
    }

    private void confirmExitDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Exit");
        alertDialogBuilder.setIcon((int) R.drawable.icon);
        alertDialogBuilder.setMessage("Do you really want to exit?\n");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Applause.this.stopSound();
                Applause.this.finish();
            }
        });
        alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }
        });
        alertDialogBuilder.create().show();
    }

    /* access modifiers changed from: package-private */
    public void mySleep(int millSecond) {
        try {
            Thread.sleep((long) millSecond);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: package-private */
    public void showAd() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public boolean saveas(int ressound) {
        InputStream fIn = getBaseContext().getResources().openRawResource(ressound);
        try {
            byte[] buffer = new byte[fIn.available()];
            fIn.read(buffer);
            fIn.close();
            if (!new File("/sdcard/media/audio/ringtones/").exists()) {
                new File("/sdcard/media/audio/ringtones/").mkdirs();
            }
            try {
                FileOutputStream save = new FileOutputStream(String.valueOf("/sdcard/media/audio/ringtones/") + "examplefile.mp3");
                save.write(buffer);
                save.flush();
                save.close();
                sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + "/sdcard/media/audio/ringtones/" + "examplefile.mp3")));
                File k = new File("/sdcard/media/audio/ringtones/", "examplefile.mp3");
                ContentValues values = new ContentValues();
                values.put("_data", k.getAbsolutePath());
                values.put("title", "mario");
                values.put("mime_type", "audio/midi");
                values.put("artist", "jazz jackrabbit");
                values.put("is_ringtone", (Boolean) true);
                values.put("is_notification", (Boolean) true);
                values.put("is_alarm", (Boolean) true);
                values.put("is_music", (Boolean) false);
                RingtoneManager.setActualDefaultRingtoneUri(this, 1, getContentResolver().insert(MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath()), values));
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        } catch (IOException e3) {
            return false;
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 3, 0, "New Game").setIcon((int) R.drawable.ic_menu_refresh);
        if (this.mGameStatus == GAME_STATUS.GS_PAUSE) {
            menu.add(0, 9, 0, "Resume").setIcon((int) R.drawable.play_btn);
        } else if (this.mGameStatus == GAME_STATUS.GS_PLAYING) {
            menu.add(0, 9, 0, "Pause").setIcon((int) R.drawable.pause_btn);
        }
        menu.add(0, 1, 0, "More Apps").setIcon((int) R.drawable.ic_menu_emoticons);
        menu.add(0, 5, 0, "Option").setIcon((int) R.drawable.ic_menu_info_details);
        menu.add(0, 99, 0, "Exit").setIcon((int) R.drawable.ic_menu_close_clear_cancel);
        return super.onPrepareOptionsMenu(menu);
    }

    public void updateScoreAndLevel() {
        ((TextView) findViewById(R.id.game_score)).setText(Integer.toString(this.score));
        ((TextView) findViewById(R.id.game_level)).setText(Integer.toString(this.level));
    }

    /* access modifiers changed from: package-private */
    public void showMoreApp(int selIndex) {
        String url;
        new String();
        if (selIndex == 1) {
            url = "market://search?q=pub:%22funweaver%22";
        } else if (selIndex == 2) {
            url = "market://search?q=pub:%22funweaver%22";
        } else {
            url = "market://details?id=com.feasy.jewels.HalloweenTap";
        }
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                showMoreApp(1);
                break;
            case 3:
                this.level = 1;
                initGame();
                break;
            case 5:
                showOptionDlg();
                break;
            case 9:
                setGamePause();
                break;
            case 99:
                showExitDlg();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showOptionDlg() {
        Intent intent = new Intent();
        intent.setClass(this, OptionActivity.class);
        intent.putExtra("GameMusic", this.mGameMusic);
        intent.putExtra("MatchSound", this.mMatchSound);
        intent.putExtra("Vibrate", this.mVibrate);
        intent.putExtra("ChangeTheme", this.mChangeTheme);
        startActivityForResult(intent, 2);
    }

    public void showExitDlg() {
        View entryDlg = LayoutInflater.from(this).inflate((int) R.layout.popup_lv, (ViewGroup) null);
        this.mExitDlg = new AlertDialog.Builder(this).setView(entryDlg).create();
        ListView lv = (ListView) entryDlg.findViewById(R.id.lv);
        ArrayList<HashMap<String, Object>> users = new ArrayList<>();
        HashMap<String, Object> rec1 = new HashMap<>();
        HashMap<String, Object> rec2 = new HashMap<>();
        HashMap<String, Object> rec3 = new HashMap<>();
        HashMap<String, Object> rec4 = new HashMap<>();
        rec1.put("img", Integer.valueOf((int) R.drawable.app1));
        rec1.put("name", "More Games");
        users.add(rec1);
        rec2.put("img", Integer.valueOf((int) R.drawable.ic_bye));
        rec2.put("name", "Exit");
        users.add(rec2);
        rec3.put("img", Integer.valueOf((int) R.drawable.ic_backandcancel));
        rec3.put("name", "Back");
        users.add(rec3);
        rec4.put("img", Integer.valueOf((int) R.drawable.app2));
        rec4.put("name", "Halloween Tap Tap! :D");
        users.add(rec4);
        lv.setAdapter((ListAdapter) new SimpleAdapter(this, users, R.layout.popup_lv_item, new String[]{"img", "name"}, new int[]{R.id.img_icon, R.id.tv_name}));
        lv.setBackgroundColor(-16777216);
        lv.setItemsCanFocus(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int index = arg2;
                if (index == 0) {
                    Applause.this.showMoreApp(1);
                } else if (1 == index) {
                    Applause.this.mExitDlg.dismiss();
                    Applause.this.exitApp();
                } else if (2 == index) {
                    Applause.this.mExitDlg.dismiss();
                } else if (3 == index) {
                    Applause.this.showMoreApp(0);
                }
            }
        });
        this.mExitDlg.show();
    }

    public void loadGameParam() {
        SharedPreferences settings = getSharedPreferences("com.feasy.app.memory.HalloweenMemoryP2", 0);
        this.mGameMusic = settings.getBoolean("isMusic", false);
        this.mMatchSound = settings.getBoolean("isMatchSound", true);
        this.mVibrate = settings.getBoolean("isVibrate", true);
        this.mChangeTheme = settings.getBoolean("isChangeTheme", true);
        this.mSkinIndex = settings.getInt("skinIndex", -1);
    }

    public void saveGameParam() {
        SharedPreferences.Editor editor = getSharedPreferences("com.feasy.app.memory.HalloweenMemoryP2", 0).edit();
        editor.putBoolean("isMusic", this.mGameMusic);
        editor.putBoolean("isMatchSound", this.mMatchSound);
        editor.putBoolean("isVibrate", this.mVibrate);
        editor.putBoolean("isChangeTheme", this.mChangeTheme);
        editor.putInt("skinIndex", this.mSkinIndex);
        editor.commit();
    }

    public void exitApp() {
        stopMusic();
        saveGameParam();
        Process.killProcess(Process.myPid());
    }

    public void showGameOverDlg() {
        View entryDlg = LayoutInflater.from(this).inflate((int) R.layout.popup_lv, (ViewGroup) null);
        this.mGameOverDlg = new AlertDialog.Builder(this).setView(entryDlg).setTitle("Game Over").create();
        ListView lv = (ListView) entryDlg.findViewById(R.id.lv);
        ArrayList<HashMap<String, Object>> users = new ArrayList<>();
        HashMap<String, Object> rec1 = new HashMap<>();
        HashMap<String, Object> rec2 = new HashMap<>();
        HashMap<String, Object> rec3 = new HashMap<>();
        rec1.put("img", Integer.valueOf((int) R.drawable.ic_menu_emoticons));
        rec1.put("name", "Try Again");
        users.add(rec1);
        rec2.put("img", Integer.valueOf((int) R.drawable.ic_menu_star));
        rec2.put("name", "New Game");
        users.add(rec2);
        rec3.put("img", Integer.valueOf((int) R.drawable.ic_menu_revert));
        rec3.put("name", "Back");
        users.add(rec3);
        lv.setAdapter((ListAdapter) new SimpleAdapter(this, users, R.layout.popup_lv_item, new String[]{"img", "name"}, new int[]{R.id.img_icon, R.id.tv_name}));
        lv.setBackgroundColor(-16777216);
        lv.setItemsCanFocus(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                int index = arg2;
                if (index == 0) {
                    Applause.this.initGame();
                } else if (1 == index) {
                    Applause.this.newGame();
                }
                Applause.this.mGameOverDlg.dismiss();
            }
        });
        this.mGameOverDlg.show();
    }

    private void updateBkg() {
        ((RelativeLayout) findViewById(R.id.main)).setBackgroundResource(getCurSkinImageId(1));
    }

    public int getCurSkinImageId(int imgType) {
        int bkgResId = R.drawable.bkg0;
        int imgResId = R.drawable.image0;
        switch (this.mSkinIndex) {
            case 0:
                bkgResId = R.drawable.bkg0;
                imgResId = R.drawable.image0;
                break;
            case 1:
                bkgResId = R.drawable.bkg1;
                imgResId = R.drawable.image1;
                break;
            case 2:
                bkgResId = R.drawable.bkg2;
                imgResId = R.drawable.image2;
                break;
            case 3:
                bkgResId = R.drawable.bkg3;
                imgResId = R.drawable.image3;
                break;
        }
        if (imgType == 1) {
            return bkgResId;
        }
        return imgResId;
    }

    public void setRandomSkin() {
        this.mSkinIndex = new Random().nextInt(4);
    }

    public void setNextSkin() {
        if (this.mChangeTheme) {
            this.mSkinIndex = this.mSkinIndex >= 4 ? 0 : this.mSkinIndex + 1;
        }
    }
}
