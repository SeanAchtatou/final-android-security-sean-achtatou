package com.adwo.adsdk;

import android.app.Activity;
import android.view.View;

final class I implements View.OnClickListener {
    /* access modifiers changed from: private */
    public /* synthetic */ G a;

    I(G g) {
        this.a = g;
    }

    public final void onClick(View view) {
        ((Activity) G.a).stopService(((Activity) G.a).getIntent());
        this.a.b.setClickable(false);
        this.a.n.setDuration(3000);
        this.a.n.setRepeatMode(1);
        this.a.n.setRepeatCount(4);
        this.a.b.startAnimation(this.a.n);
        this.a.e.setVisibility(4);
        this.a.post(new J(this));
    }
}
