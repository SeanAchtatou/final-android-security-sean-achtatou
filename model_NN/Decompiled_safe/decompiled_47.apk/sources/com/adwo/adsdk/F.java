package com.adwo.adsdk;

import android.view.animation.Animation;

final class F implements Animation.AnimationListener {
    private /* synthetic */ E a;

    F(E e) {
        this.a = e;
    }

    public final void onAnimationStart(Animation animation) {
    }

    public final void onAnimationEnd(Animation animation) {
        if (this.a.b != null) {
            this.a.d.removeView(this.a.b);
            this.a.b.a();
            this.a.b = null;
        }
        this.a.d.c = this.a.a;
    }

    public final void onAnimationRepeat(Animation animation) {
    }
}
