package com.waps.ads.adapters;

import android.app.Activity;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;
import com.waps.AnimationType;
import com.waps.ads.AdGroupLayout;
import com.waps.ads.AdGroupTargeting;
import com.waps.ads.a.a;
import com.waps.ads.b.b;
import com.waps.ads.b.c;
import com.waps.ads.g;
import show.magicicon.R;

public class MadhouseAdapter extends a implements AdListener {
    static AdGroupLayout a;

    public MadhouseAdapter(AdGroupLayout adGroupLayout, c cVar) {
        super(adGroupLayout, cVar);
    }

    public void handle() {
        a = (AdGroupLayout) this.c.get();
        if (a != null) {
            b bVar = a.d;
            Activity activity = (Activity) a.a.get();
            if (activity != null) {
                AdManager.setApplicationId(activity, this.d.a);
                new AdView(activity, (AttributeSet) null, 0, this.d.e, bVar.i, 0, AdGroupTargeting.getTestMode()).setListener(this);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void
     arg types: [com.waps.ads.AdGroupLayout, com.madhouse.android.ads.AdView]
     candidates:
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.View):void
      com.waps.ads.g.<init>(com.waps.ads.AdGroupLayout, android.view.ViewGroup):void */
    public void onAdEvent(AdView adView, int i) {
        switch (i) {
            case 1:
            case R.styleable.com_adwo_adsdk_AdwoAdView_isTesting /*3*/:
                AdGroupLayout adGroupLayout = (AdGroupLayout) this.c.get();
                if (adGroupLayout != null) {
                    adGroupLayout.j.resetRollover();
                    adGroupLayout.b.post(new g(adGroupLayout, (ViewGroup) adView));
                    adGroupLayout.rotateThreadedDelayed();
                    adView.setListener((AdListener) null);
                    return;
                }
                return;
            case 2:
                adView.setListener((AdListener) null);
                return;
            case AnimationType.ROTATE:
                adView.setListener((AdListener) null);
                AdGroupLayout adGroupLayout2 = (AdGroupLayout) this.c.get();
                if (adGroupLayout2 != null) {
                    adGroupLayout2.j.resetRollover();
                    adGroupLayout2.rollover();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onAdStatus(int i) {
        AdGroupLayout adGroupLayout;
        if (i != 200 && (adGroupLayout = (AdGroupLayout) this.c.get()) != null) {
            adGroupLayout.j.resetRollover();
            adGroupLayout.rollover();
        }
    }
}
