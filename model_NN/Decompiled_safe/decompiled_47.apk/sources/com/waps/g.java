package com.waps;

import android.os.AsyncTask;

class g extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private g(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ g(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String a2 = AppConnect.x.a("http://app.wapx.cn/action/" + AppConnect.af, this.a.N + "&" + AppConnect.ag + "=" + this.a.P);
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }
}
