package show.magicicon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class image4 extends Activity {
    ImageView image4;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.image4);
        this.image4 = (ImageView) findViewById(R.id.image4);
        this.image4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(image4.this, AccelerometerActivity4.class);
                image4.this.startActivity(intent);
                image4.this.overridePendingTransition(R.anim.alpha, R.anim.my);
            }
        });
    }
}
