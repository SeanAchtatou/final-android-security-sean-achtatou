package show.magicicon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class image3 extends Activity {
    ImageView image3;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.image3);
        this.image3 = (ImageView) findViewById(R.id.image3);
        this.image3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(image3.this, AccelerometerActivity3.class);
                image3.this.startActivity(intent);
                image3.this.overridePendingTransition(R.anim.alpha, R.anim.my);
            }
        });
    }
}
