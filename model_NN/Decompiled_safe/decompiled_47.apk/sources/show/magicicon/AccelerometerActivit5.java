package show.magicicon;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import cn.itcast.accelerometer.view.BallView;

public class AccelerometerActivit5 extends Activity {
    private static final float MAX_ACCELEROMETER = 9.81f;
    private BallView ball;
    private int ball_height = 0;
    private int ball_width = 0;
    private int container_height = 0;
    private int container_width = 0;
    /* access modifiers changed from: private */
    public boolean init = false;
    private SensorEventListener listener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent event) {
            if (AccelerometerActivit5.this.init) {
                float x = event.values[0];
                float y = event.values[1];
                float f = event.values[2];
                AccelerometerActivit5.this.moveTo(-x, y);
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private SensorManager sensorManager;
    private boolean success = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.ceshi);
        this.sensorManager = (SensorManager) getSystemService("sensor");
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && !this.init) {
            View container = findViewById(R.id.ball_container);
            this.container_width = container.getWidth();
            this.container_height = container.getHeight();
            this.ball = (BallView) findViewById(R.id.ball);
            this.ball_width = this.ball.getWidth();
            this.ball_height = this.ball.getHeight();
            moveTo(0.0f, 0.0f);
            this.init = true;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.success = this.sensorManager.registerListener(this.listener, this.sensorManager.getDefaultSensor(1), 1);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.success) {
            this.sensorManager.unregisterListener(this.listener);
        }
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void moveTo(float x, float y) {
        int max_x = (this.container_width - this.ball_width) / 2;
        int max_y = (this.container_height - this.ball_height) / 2;
        this.ball.moveTo(max_x + ((int) (((float) max_x) * (x / MAX_ACCELEROMETER))), max_y + ((int) (((float) max_y) * (y / MAX_ACCELEROMETER))));
    }
}
