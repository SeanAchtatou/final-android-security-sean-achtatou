package show.magicicon;

public final class R {

    public static final class anim {
        public static final int alpha = 2130968576;
        public static final int my = 2130968577;
    }

    public static final class attr {
        public static final int backgroundColor = 2130771971;
        public static final int backgroundTransparent = 2130771976;
        public static final int isTesting = 2130771974;
        public static final int keywords = 2130771968;
        public static final int primaryTextColor = 2130771972;
        public static final int refreshInterval = 2130771970;
        public static final int secondaryTextColor = 2130771973;
        public static final int selectedColor = 2130771977;
        public static final int spots = 2130771969;
        public static final int textColor = 2130771975;
    }

    public static final class drawable {
        public static final int back = 2130837504;
        public static final int icon = 2130837505;
        public static final int icon1 = 2130837506;
        public static final int icon2 = 2130837507;
        public static final int icon3 = 2130837508;
        public static final int icon3333 = 2130837509;
        public static final int icon4 = 2130837510;
        public static final int main400a = 2130837511;
        public static final int main800a = 2130837512;
        public static final int main854a = 2130837513;
        public static final int zhuomian1 = 2130837514;
        public static final int zhuomian2 = 2130837515;
        public static final int zhuomian3 = 2130837516;
    }

    public static final class id {
        public static final int AdLinearLayout = 2131099650;
        public static final int ball = 16842753;
        public static final int ball_container = 16842752;
        public static final int icon1 = 2131099649;
        public static final int icon2 = 2131099651;
        public static final int icon3 = 2131099652;
        public static final int icon4 = 2131099653;
        public static final int image1 = 2131099654;
        public static final int image2 = 2131099655;
        public static final int image3 = 2131099656;
        public static final int image4 = 2131099657;
        public static final int main = 2131099658;
        public static final int widget0 = 2131099648;
    }

    public static final class layout {
        public static final int ceshi = 2130903040;
        public static final int ceshi2 = 2130903041;
        public static final int ceshi3 = 2130903042;
        public static final int ceshi4 = 2130903043;
        public static final int ceshi5 = 2130903044;
        public static final int icon = 2130903045;
        public static final int image1 = 2130903046;
        public static final int image2 = 2130903047;
        public static final int image3 = 2130903048;
        public static final int image4 = 2130903049;
        public static final int main = 2130903050;
    }

    public static final class string {
        public static final int app_name = 2131034113;
        public static final int hello = 2131034112;
    }

    public static final class styleable {
        public static final int[] cn_domob_android_ads_DomobAdView = {R.attr.keywords, R.attr.spots, R.attr.refreshInterval};
        public static final int cn_domob_android_ads_DomobAdView_keywords = 0;
        public static final int cn_domob_android_ads_DomobAdView_refreshInterval = 2;
        public static final int cn_domob_android_ads_DomobAdView_spots = 1;
        public static final int[] com_adpooh_adscast_banner_AdkBannerView = {R.attr.textColor, R.attr.backgroundTransparent, R.attr.selectedColor};
        public static final int com_adpooh_adscast_banner_AdkBannerView_backgroundTransparent = 1;
        public static final int com_adpooh_adscast_banner_AdkBannerView_selectedColor = 2;
        public static final int com_adpooh_adscast_banner_AdkBannerView_textColor = 0;
        public static final int[] com_adwo_adsdk_AdwoAdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.isTesting};
        public static final int com_adwo_adsdk_AdwoAdView_backgroundColor = 0;
        public static final int com_adwo_adsdk_AdwoAdView_isTesting = 3;
        public static final int com_adwo_adsdk_AdwoAdView_primaryTextColor = 1;
        public static final int com_adwo_adsdk_AdwoAdView_secondaryTextColor = 2;
    }
}
