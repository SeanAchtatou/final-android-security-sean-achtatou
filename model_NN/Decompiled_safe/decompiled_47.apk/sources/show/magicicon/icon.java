package show.magicicon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mt.airad.AirAD;
import com.waps.AdView;
import com.waps.AppConnect;

public class icon extends Activity {
    AirAD ad;
    ImageView icon1;
    ImageView icon2;
    ImageView icon3;
    ImageView icon4;

    static {
        AirAD.setGlobalParameter("3521ffee-f51e-47c6-a69f-49c7d221baf3", 10.0d);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.icon);
        AppConnect.getInstance(this);
        this.ad = new AirAD(this, 11, 1);
        new AdView(this, (LinearLayout) findViewById(R.id.AdLinearLayout)).DisplayAd(30);
        this.icon1 = (ImageView) findViewById(R.id.icon1);
        this.icon2 = (ImageView) findViewById(R.id.icon2);
        this.icon3 = (ImageView) findViewById(R.id.icon3);
        this.icon4 = (ImageView) findViewById(R.id.icon4);
        this.icon4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimationSet animationset = new AnimationSet(true);
                TranslateAnimation translate = new TranslateAnimation(0.0f, 1000.0f, 0.0f, 0.0f);
                translate.setDuration(800);
                translate.setFillAfter(true);
                RotateAnimation ratate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 2, 0.5f);
                ratate.setDuration(800);
                ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.1f, 2, 0.9f);
                scale.setDuration(800);
                animationset.addAnimation(translate);
                animationset.addAnimation(scale);
                animationset.addAnimation(ratate);
                icon.this.icon4.startAnimation(animationset);
                animationset.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent();
                        intent.setClass(icon.this, image4.class);
                        icon.this.startActivity(intent);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
            }
        });
        this.icon1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimationSet animationset = new AnimationSet(true);
                TranslateAnimation translate = new TranslateAnimation(0.0f, 1000.0f, 0.0f, 0.0f);
                translate.setDuration(800);
                translate.setFillAfter(true);
                RotateAnimation ratate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 2, 0.5f);
                ratate.setDuration(800);
                ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.1f, 2, 0.9f);
                scale.setDuration(800);
                animationset.addAnimation(translate);
                animationset.addAnimation(scale);
                animationset.addAnimation(ratate);
                icon.this.icon1.startAnimation(animationset);
                animationset.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent();
                        intent.setClass(icon.this, image1.class);
                        icon.this.startActivity(intent);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
            }
        });
        this.icon2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimationSet animationset = new AnimationSet(true);
                TranslateAnimation translate = new TranslateAnimation(0.0f, 1000.0f, 0.0f, 0.0f);
                translate.setDuration(800);
                translate.setFillAfter(true);
                RotateAnimation ratate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 2, 0.5f);
                ratate.setDuration(800);
                ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.1f, 2, 0.9f);
                scale.setDuration(800);
                animationset.addAnimation(translate);
                animationset.addAnimation(scale);
                animationset.addAnimation(ratate);
                icon.this.icon2.startAnimation(animationset);
                animationset.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent();
                        intent.setClass(icon.this, image2.class);
                        icon.this.startActivity(intent);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
            }
        });
        this.icon3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimationSet animationset = new AnimationSet(true);
                TranslateAnimation translate = new TranslateAnimation(0.0f, 1000.0f, 0.0f, 0.0f);
                translate.setDuration(800);
                translate.setFillAfter(true);
                RotateAnimation ratate = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 2, 0.5f);
                ratate.setDuration(800);
                ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.1f, 2, 0.9f);
                scale.setDuration(800);
                animationset.addAnimation(translate);
                animationset.addAnimation(scale);
                animationset.addAnimation(ratate);
                icon.this.icon3.startAnimation(animationset);
                animationset.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent();
                        intent.setClass(icon.this, image3.class);
                        icon.this.startActivity(intent);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.ad != null) {
            this.ad.destory();
        }
        super.onDestroy();
    }
}
