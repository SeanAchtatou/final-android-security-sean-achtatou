package show.magicicon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class image2 extends Activity {
    ImageView image2;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.image2);
        this.image2 = (ImageView) findViewById(R.id.image2);
        this.image2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(image2.this, AccelerometerActivity2.class);
                image2.this.startActivity(intent);
                image2.this.overridePendingTransition(R.anim.alpha, R.anim.my);
            }
        });
    }
}
