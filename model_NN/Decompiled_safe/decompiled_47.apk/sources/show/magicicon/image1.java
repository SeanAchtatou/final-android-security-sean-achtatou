package show.magicicon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class image1 extends Activity {
    ImageView image1;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.image1);
        this.image1 = (ImageView) findViewById(R.id.image1);
        this.image1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(image1.this, AccelerometerActivity.class);
                image1.this.startActivity(intent);
                image1.this.overridePendingTransition(R.anim.alpha, R.anim.my);
            }
        });
    }
}
