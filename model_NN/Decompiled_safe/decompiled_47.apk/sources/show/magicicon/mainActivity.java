package show.magicicon;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.mt.airad.AirAD;
import com.waps.AdView;
import com.waps.AppConnect;

public class mainActivity extends Activity {
    AirAD ad;
    ImageView main;

    static {
        AirAD.setGlobalParameter("3521ffee-f51e-47c6-a69f-49c7d221baf3", 10.0d);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.ad = new AirAD(this, 11, 1);
        this.main = (ImageView) findViewById(R.id.main);
        AppConnect.getInstance(this);
        new AdView(this, (LinearLayout) findViewById(R.id.AdLinearLayout)).DisplayAd(30);
        this.main.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AnimationSet animationset = new AnimationSet(true);
                ScaleAnimation scale = new ScaleAnimation(1.0f, 0.0f, 1.0f, 1.0f, 2, 1.0f, 2, 0.1f);
                scale.setDuration(1400);
                animationset.addAnimation(scale);
                mainActivity.this.main.startAnimation(animationset);
                animationset.setFillAfter(true);
                animationset.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        Intent intent = new Intent();
                        intent.setClass(mainActivity.this, icon.class);
                        mainActivity.this.startActivity(intent);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });
            }
        });
    }
}
