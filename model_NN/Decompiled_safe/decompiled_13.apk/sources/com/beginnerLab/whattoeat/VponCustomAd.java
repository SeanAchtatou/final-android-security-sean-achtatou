package com.beginnerLab.whattoeat;

import android.app.Activity;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.customevent.CustomEventBanner;
import com.google.ads.mediation.customevent.CustomEventBannerListener;
import com.google.ads.mediation.customevent.CustomEventInterstitial;
import com.google.ads.mediation.customevent.CustomEventInterstitialListener;
import com.vpon.ads.VponAd;
import com.vpon.ads.VponAdListener;
import com.vpon.ads.VponAdRequest;
import com.vpon.ads.VponAdSize;
import com.vpon.ads.VponBanner;
import com.vpon.ads.VponInterstitialAd;
import com.vpon.ads.VponPlatform;

public class VponCustomAd implements CustomEventBanner, CustomEventInterstitial {
    /* access modifiers changed from: private */
    public VponInterstitialAd interstitialAd = null;
    /* access modifiers changed from: private */
    public VponBanner vponBanner = null;

    public void destroy() {
        if (this.vponBanner != null) {
            this.vponBanner.destroy();
            this.vponBanner = null;
        }
        if (this.interstitialAd != null) {
            this.interstitialAd.destroy();
            this.interstitialAd = null;
        }
    }

    private VponAdSize getVponAdSizeByAdSize(AdSize adSize) {
        if (adSize.equals(AdSize.BANNER)) {
            return VponAdSize.BANNER;
        }
        if (adSize.equals(AdSize.IAB_BANNER)) {
            return VponAdSize.IAB_BANNER;
        }
        if (adSize.equals(AdSize.IAB_LEADERBOARD)) {
            return VponAdSize.IAB_LEADERBOARD;
        }
        if (adSize.equals(AdSize.IAB_MRECT)) {
            return VponAdSize.IAB_MRECT;
        }
        if (adSize.equals(AdSize.IAB_WIDE_SKYSCRAPER)) {
            return VponAdSize.IAB_WIDE_SKYSCRAPER;
        }
        if (adSize.equals(AdSize.SMART_BANNER)) {
            return VponAdSize.SMART_BANNER;
        }
        boolean isAutoHeight = false;
        boolean isFullWidth = false;
        if (adSize.isAutoHeight()) {
            isAutoHeight = true;
        }
        if (adSize.isFullWidth()) {
            isFullWidth = true;
        }
        if (isAutoHeight && isFullWidth) {
            return VponAdSize.SMART_BANNER;
        }
        if (isAutoHeight && !isFullWidth) {
            return new VponAdSize(adSize.getWidth(), -1);
        }
        if (!isAutoHeight && isFullWidth) {
            return new VponAdSize(-2, adSize.getHeight());
        }
        if (adSize.isCustomAdSize()) {
            return new VponAdSize(adSize.getWidth(), adSize.getHeight());
        }
        return VponAdSize.SMART_BANNER;
    }

    private VponAdRequest getVponAdRequestByMediationAdRequest(MediationAdRequest request) {
        VponAdRequest adRequest = new VponAdRequest();
        if (request.getBirthday() != null) {
            adRequest.setBirthday(request.getBirthday());
        }
        if (request.getAgeInYears() != null) {
            adRequest.setAge(request.getAgeInYears().intValue());
        }
        if (request.getKeywords() != null) {
            adRequest.setKeywords(request.getKeywords());
        }
        if (request.getGender() != null) {
            if (request.getGender().equals(AdRequest.Gender.FEMALE)) {
                adRequest.setGender(VponAdRequest.Gender.FEMALE);
            } else if (request.getGender().equals(AdRequest.Gender.MALE)) {
                adRequest.setGender(VponAdRequest.Gender.MALE);
            } else {
                adRequest.setGender(VponAdRequest.Gender.UNKNOWN);
            }
        }
        return adRequest;
    }

    public void requestBannerAd(final CustomEventBannerListener listener, Activity activity, String label, String serverParameter, AdSize adSize, MediationAdRequest request, Object customEventExtra) {
        if (this.vponBanner != null) {
            this.vponBanner.destroy();
            this.vponBanner = null;
        }
        VponAdRequest adRequest = getVponAdRequestByMediationAdRequest(request);
        this.vponBanner = new VponBanner(activity, serverParameter, getVponAdSizeByAdSize(adSize), VponPlatform.TW);
        this.vponBanner.setAdListener(new VponAdListener() {
            public void onVponDismissScreen(VponAd arg0) {
                listener.onDismissScreen();
            }

            public void onVponFailedToReceiveAd(VponAd arg0, VponAdRequest.VponErrorCode arg1) {
                listener.onFailedToReceiveAd();
            }

            public void onVponLeaveApplication(VponAd arg0) {
                listener.onLeaveApplication();
            }

            public void onVponPresentScreen(VponAd arg0) {
                listener.onPresentScreen();
            }

            public void onVponReceiveAd(VponAd arg0) {
                listener.onReceivedAd(VponCustomAd.this.vponBanner);
            }
        });
        this.vponBanner.loadAd(adRequest);
    }

    public void requestInterstitialAd(final CustomEventInterstitialListener listener, Activity activity, String label, String serverParameter, MediationAdRequest request, Object customEventExtra) {
        this.interstitialAd = new VponInterstitialAd(activity, serverParameter, VponPlatform.TW);
        this.interstitialAd.setAdListener(new VponAdListener() {
            public void onVponDismissScreen(VponAd arg0) {
                if (VponCustomAd.this.interstitialAd != null) {
                    VponCustomAd.this.interstitialAd.destroy();
                    VponCustomAd.this.interstitialAd = null;
                }
                listener.onDismissScreen();
            }

            public void onVponFailedToReceiveAd(VponAd arg0, VponAdRequest.VponErrorCode arg1) {
                if (VponCustomAd.this.interstitialAd != null) {
                    VponCustomAd.this.interstitialAd.destroy();
                    VponCustomAd.this.interstitialAd = null;
                }
                listener.onFailedToReceiveAd();
            }

            public void onVponLeaveApplication(VponAd arg0) {
                listener.onLeaveApplication();
            }

            public void onVponPresentScreen(VponAd arg0) {
                listener.onPresentScreen();
            }

            public void onVponReceiveAd(VponAd arg0) {
                listener.onReceivedAd();
            }
        });
        this.interstitialAd.loadAd(new VponAdRequest());
    }

    public void showInterstitial() {
        if (this.interstitialAd != null && this.interstitialAd.isReady()) {
            this.interstitialAd.show();
        }
    }
}
