package com.beginnerLab.whattoeat;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.analytics.tracking.android.EasyTracker;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity implements LocationListener, SensorEventListener {
    private String Provider = "network";
    /* access modifiers changed from: private */
    public boolean activeGetData = false;
    private RelativeLayout adBannerLayout;
    private AdView adMobAdView;
    private long curTime = 0;
    private ProgressDialog dialog;
    private long duration = 0;
    private EasyTracker easyTracker = null;
    private long initTime = 0;
    private long lastTime = 0;
    private float last_x = 0.0f;
    private float last_y = 0.0f;
    private float last_z = 0.0f;
    private LocationManager lms;
    private Location location = null;
    private Boolean locationStatus = false;
    private SensorManager mSensorManager;
    private Boolean networkStatus = false;
    private float shake = 0.0f;
    private float totalShake = 0.0f;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        UseTypeFace();
        new ConnectionDetector();
        this.networkStatus = Boolean.valueOf(ConnectionDetector.isConnectingToInternet(this));
        if (!this.networkStatus.booleanValue()) {
            AlertDialog("Hi! Pleasre enabled network services on your device.");
        }
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.lms = (LocationManager) getSystemService("location");
        if (this.lms.isProviderEnabled("gps") || this.lms.isProviderEnabled("network")) {
            this.locationStatus = true;
            locationServiceInitial();
        } else {
            AlertDialog("Hi! Pleasre enabled location services on your device.");
        }
        EasyTracker.getInstance(this).activityStart(this);
        adMobMediationInit();
    }

    private void adMobMediationInit() {
        getWindow().setFlags(16777216, 16777216);
        setContentView((int) R.layout.activity_main);
        this.adBannerLayout = (RelativeLayout) findViewById(R.id.adLayout);
        AdRequest adReq = new AdRequest();
        this.adMobAdView = new AdView(this, AdSize.SMART_BANNER, "e792587cfdbe4149");
        this.adMobAdView.setAdListener(new AdListener() {
            public void onDismissScreen(Ad arg0) {
                Log.d("admob_banner", "onDismissScreen");
            }

            public void onFailedToReceiveAd(Ad arg0, AdRequest.ErrorCode arg1) {
                Log.d("admob_banner", "onFailedToReceiveAd");
            }

            public void onLeaveApplication(Ad arg0) {
                Log.d("admob_banner", "onLeaveApplication");
            }

            public void onPresentScreen(Ad arg0) {
                Log.d("admob_banner", "onPresentScreen");
            }

            public void onReceiveAd(Ad ad) {
                Log.d("admob_banner", "onReceiveAd ad:" + ad.getClass());
            }
        });
        this.adMobAdView.loadAd(adReq);
        this.adBannerLayout.addView(this.adMobAdView);
    }

    private void UseTypeFace() {
        Typeface fonts = Typeface.createFromAsset(getResources().getAssets(), "fonts/Roboto-Bold.ttf");
        ((TextView) findViewById(R.id.textView1)).setTypeface(fonts, 1);
        ((TextView) findViewById(R.id.textView2)).setTypeface(fonts, 1);
    }

    private void AlertDialog(String desc) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(desc);
        alertDialogBuilder.create().show();
        this.locationStatus = false;
    }

    public void onLocationChanged(Location arg0) {
        locationServiceInitial();
    }

    public void onProviderDisabled(String arg0) {
        this.locationStatus = false;
    }

    public void onProviderEnabled(String arg0) {
        this.locationStatus = true;
    }

    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        locationServiceInitial();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        FontUtils.setRobotoFont(this, (ViewGroup) getWindow().getDecorView());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mSensorManager.registerListener(this, this.mSensorManager.getDefaultSensor(1), 3);
        if (this.lms.isProviderEnabled("gps") || this.lms.isProviderEnabled("network")) {
            this.lms.requestLocationUpdates(this.Provider, 300, 0.0f, this);
        } else {
            this.locationStatus = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.mSensorManager.unregisterListener(this);
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mSensorManager.unregisterListener(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];
            this.curTime = System.currentTimeMillis();
            if (this.curTime - this.lastTime > 90) {
                this.duration = this.curTime - this.lastTime;
                if (this.last_x == 0.0f && this.last_y == 0.0f && this.last_z == 0.0f) {
                    this.initTime = System.currentTimeMillis();
                } else {
                    this.shake = Math.abs(x - this.last_x) + Math.abs(y - this.last_y) + Math.abs(z - this.last_z);
                }
                this.totalShake += this.shake;
                if (this.shake > 45.0f) {
                    if (!this.activeGetData) {
                        this.activeGetData = true;
                        new ConnectionDetector();
                        this.networkStatus = Boolean.valueOf(ConnectionDetector.isConnectingToInternet(this));
                        if (!this.networkStatus.booleanValue()) {
                            AlertDialog("Hi! Pleasre enabled network services on your device.");
                        } else {
                            this.dialog = new ProgressDialog(this, R.style.dialog);
                            this.dialog.setIndeterminate(true);
                            this.dialog.setMessage("Searching...");
                            this.dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                public void onDismiss(DialogInterface dialog) {
                                    MainActivity.this.activeGetData = false;
                                }
                            });
                            this.dialog.show();
                            new LoadingDataAsyncTask().execute(new String[0]);
                        }
                    }
                    initShake();
                }
                this.last_x = x;
                this.last_y = y;
                this.last_z = z;
                this.lastTime = this.curTime;
            }
        }
    }

    private void initShake() {
        this.lastTime = 0;
        this.duration = 0;
        this.curTime = 0;
        this.initTime = 0;
        this.last_x = 0.0f;
        this.last_y = 0.0f;
        this.last_z = 0.0f;
        this.shake = 0.0f;
        this.totalShake = 0.0f;
    }

    private void locationServiceInitial() {
        this.lms = (LocationManager) getSystemService("location");
        if (this.lms.isProviderEnabled("gps")) {
            this.Provider = "gps";
            this.location = this.lms.getLastKnownLocation(this.Provider);
        } else if (this.lms.isProviderEnabled("network")) {
            this.Provider = "network";
            this.location = this.lms.getLastKnownLocation(this.Provider);
        } else {
            AlertDialog("Hi! Pleasre enabled location services on your device.");
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject GetData() throws Exception {
        Double longitude = Double.valueOf(this.location.getLongitude());
        return new EatDataHelper().getJsonObjData(Double.valueOf(this.location.getLatitude()).toString(), longitude.toString(), "", "700");
    }

    /* access modifiers changed from: package-private */
    public void ShowData(JSONObject result) throws Exception {
        try {
            Intent intent = new Intent();
            Bundle bundle = new Bundle();
            JSONArray jsonArray = result.getJSONObject("response").getJSONArray("venues");
            int count = jsonArray.length();
            Random random = new Random();
            DataModel DM = new DataModel();
            boolean TrueData = false;
            int counts = 0;
            while (true) {
                if (counts <= 5) {
                    DM = GetOneData(jsonArray.getJSONObject(random.nextInt(count)));
                    if (DM.address != "null") {
                        TrueData = true;
                        continue;
                    } else {
                        counts++;
                        continue;
                    }
                    if (TrueData) {
                        break;
                    }
                } else {
                    this.dialog.dismiss();
                    this.activeGetData = false;
                    Toast.makeText(this, "has not any restaurant", 1).show();
                    break;
                }
            }
            if (TrueData) {
                bundle.putString("name", DM.name);
                bundle.putString("phone", DM.phone);
                bundle.putString("address", DM.address);
                bundle.putString("distance", DM.distance);
                bundle.putString("elat", DM.lat);
                bundle.putString("elng", DM.lng);
                bundle.putDouble("slat", this.location.getLatitude());
                bundle.putDouble("slng", this.location.getLongitude());
                intent.putExtras(bundle);
                intent.setClass(this, ResultActivity.class);
                this.dialog.dismiss();
                startActivity(intent);
            }
        } catch (JSONException e) {
            throw new Exception("隨機選擇資料發生錯誤", e);
        }
    }

    private DataModel GetOneData(JSONObject response) throws JSONException {
        DataModel DM = new DataModel();
        if (!response.getJSONObject("location").isNull("address")) {
            DM.address = response.getJSONObject("location").getString("address");
            DM.name = response.getString("name");
            DM.lat = response.getJSONObject("location").getString("lat");
            DM.lng = response.getJSONObject("location").getString("lng");
            if (!response.getJSONObject("contact").isNull("phone")) {
                DM.phone = response.getJSONObject("contact").getString("phone");
            } else {
                DM.phone = "NO TEL INFOMATION";
            }
            if (!response.getJSONObject("location").isNull("distance")) {
                DM.distance = response.getJSONObject("location").getString("distance");
            } else {
                DM.distance = "null";
            }
        } else {
            DM.address = "null";
        }
        return DM;
    }

    class LoadingDataAsyncTask extends AsyncTask<String, Integer, JSONObject> {
        LoadingDataAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public JSONObject doInBackground(String... urls) {
            try {
                return MainActivity.this.GetData();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(JSONObject result) {
            try {
                MainActivity.this.ShowData(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
            super.onProgressUpdate((Object[]) values);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
        }
    }
}
