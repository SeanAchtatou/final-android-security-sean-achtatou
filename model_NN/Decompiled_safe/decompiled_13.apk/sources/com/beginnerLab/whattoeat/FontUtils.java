package com.beginnerLab.whattoeat;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.HashMap;
import java.util.Map;

public class FontUtils {
    private static Map fontMap = new HashMap();
    private static Map typefaceCache = new HashMap();

    public interface FontTypes {
        public static final String BOLD = "Bold";
        public static final String LIGHT = "Light";
    }

    static {
        fontMap.put(FontTypes.LIGHT, "fonts/Roboto-Light.ttf");
        fontMap.put(FontTypes.BOLD, "fonts/Roboto-Bold.ttf");
    }

    private static Typeface getRobotoTypeface(Context context, String fontType) {
        String fontPath = fontMap.get(fontType).toString();
        if (!typefaceCache.containsKey(fontType)) {
            typefaceCache.put(fontType, Typeface.createFromAsset(context.getAssets(), fontPath));
        }
        return (Typeface) typefaceCache.get(fontType);
    }

    private static Typeface getRobotoTypeface(Context context, Typeface originalTypeface) {
        String robotoFontType = FontTypes.LIGHT;
        if (originalTypeface != null) {
            switch (originalTypeface.getStyle()) {
                case 1:
                    robotoFontType = FontTypes.BOLD;
                    break;
            }
        }
        return getRobotoTypeface(context, robotoFontType);
    }

    public static void setRobotoFont(Context context, View view) {
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                setRobotoFont(context, ((ViewGroup) view).getChildAt(i));
            }
        } else if (view instanceof TextView) {
            ((TextView) view).setTypeface(getRobotoTypeface(context, ((TextView) view).getTypeface()));
        }
    }
}
