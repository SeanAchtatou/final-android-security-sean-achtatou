package com.beginnerLab.whattoeat;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {
    public static boolean isConnectingToInternet(Context context) {
        NetworkInfo[] info;
        if (context == null) {
            throw new IllegalArgumentException("context can not be null.");
        }
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (!(connectivity == null || (info = connectivity.getAllNetworkInfo()) == null)) {
            for (NetworkInfo state : info) {
                if (state.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }
}
