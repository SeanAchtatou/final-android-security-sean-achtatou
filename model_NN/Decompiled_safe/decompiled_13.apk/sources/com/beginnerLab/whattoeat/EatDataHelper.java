package com.beginnerLab.whattoeat;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EatDataHelper {
    private String foursquareClientId = "YLQSULA1MFGG5JNZELF1OTZE3VZO4JGFTIXJIB42YFW50MVS";
    private String foursquareKey = "HO3YYDTO5VH2VKJ3QZNSYFMABXWAX3XWTUMI0MG1IRKCURR0";
    HttpClientHelper hCH = new HttpClientHelper();

    public JSONObject getJsonObjData(String lat, String lng, String category, String radius) {
        try {
            return new JSONObject(this.hCH.getHttpData("https://api.foursquare.com/v2/venues/search?ll=" + lat + "," + lng + "&categoryId=4d4b7105d754a06374d81259&client_secret=" + this.foursquareKey + "&radius=" + radius + "&client_id=" + this.foursquareClientId + "&v=" + new SimpleDateFormat("yyyyMMdd").format(new Date()) + "&limit=50&intent=browse"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public JSONArray getJsonArrayData(String lat, String lng, String category, String radius) {
        try {
            return new JSONArray(this.hCH.getHttpData("https://api.foursquare.com/v2/venues/search?ll=" + lat + "," + lng + "&categoryId=4d4b7105d754a06374d81259&client_secret=" + this.foursquareKey + "&radius=" + radius + "&client_id=" + this.foursquareClientId));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
