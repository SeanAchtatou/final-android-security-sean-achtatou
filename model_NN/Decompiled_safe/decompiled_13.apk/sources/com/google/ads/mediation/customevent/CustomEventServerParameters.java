package com.google.ads.mediation.customevent;

import com.beginnerLab.whattoeat.BuildConfig;
import com.google.ads.mediation.MediationServerParameters;

public class CustomEventServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "class_name", required = BuildConfig.DEBUG)
    public String className;
    @MediationServerParameters.Parameter(name = "label", required = BuildConfig.DEBUG)
    public String label;
    @MediationServerParameters.Parameter(name = "parameter", required = false)
    public String parameter = null;
}
