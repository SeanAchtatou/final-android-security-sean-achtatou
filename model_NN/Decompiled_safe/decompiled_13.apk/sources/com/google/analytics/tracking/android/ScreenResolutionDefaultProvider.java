package com.google.analytics.tracking.android;

import android.content.Context;
import android.util.DisplayMetrics;
import com.google.android.gms.common.util.VisibleForTesting;

class ScreenResolutionDefaultProvider implements DefaultProvider {
    private static ScreenResolutionDefaultProvider sInstance;
    private static Object sInstanceLock = new Object();
    private final Context mContext;

    public static void initializeProvider(Context c2) {
        synchronized (sInstanceLock) {
            if (sInstance == null) {
                sInstance = new ScreenResolutionDefaultProvider(c2);
            }
        }
    }

    public static ScreenResolutionDefaultProvider getProvider() {
        ScreenResolutionDefaultProvider screenResolutionDefaultProvider;
        synchronized (sInstanceLock) {
            screenResolutionDefaultProvider = sInstance;
        }
        return screenResolutionDefaultProvider;
    }

    @VisibleForTesting
    static void dropInstance() {
        synchronized (sInstanceLock) {
            sInstance = null;
        }
    }

    @VisibleForTesting
    protected ScreenResolutionDefaultProvider(Context c2) {
        this.mContext = c2;
    }

    public boolean providesField(String field) {
        return Fields.SCREEN_RESOLUTION.equals(field);
    }

    public String getValue(String field) {
        if (field != null && field.equals(Fields.SCREEN_RESOLUTION)) {
            return getScreenResolutionString();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public String getScreenResolutionString() {
        DisplayMetrics dm = this.mContext.getResources().getDisplayMetrics();
        return dm.widthPixels + "x" + dm.heightPixels;
    }
}
