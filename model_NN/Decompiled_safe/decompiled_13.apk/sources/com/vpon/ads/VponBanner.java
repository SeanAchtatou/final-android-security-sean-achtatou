package com.vpon.ads;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.vpon.ads.VponAdRequest;
import vpadn.C;
import vpadn.C0003a;
import vpadn.I;
import vpadn.Q;
import vpadn.X;
import vpadn.Y;

public class VponBanner extends RelativeLayout implements LocationListener, VponAd, I {
    public static final int DEFAULT_BACKGROUND_COLOR = 17170457;
    /* access modifiers changed from: private */
    public C a = null;
    private VponAdListener b = null;

    /* renamed from: c  reason: collision with root package name */
    private Activity f85c;
    private boolean d = false;
    private int e;
    private int f;
    private boolean g = true;

    public VponBanner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f85c = (Activity) context;
        setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
        X.a(context).a(this);
        this.g = false;
        this.a = new C((Activity) context, this);
        a(attributeSet);
    }

    private void a(AttributeSet attributeSet) {
        boolean z = false;
        VponPlatform vponPlatform = VponPlatform.TW;
        VponAdSize vponAdSize = VponAdSize.SMART_BANNER;
        String str = null;
        int attributeCount = attributeSet.getAttributeCount();
        boolean z2 = false;
        for (int i = 0; i < attributeCount; i++) {
            String attributeName = attributeSet.getAttributeName(i);
            if (attributeName.equals("bannerId")) {
                str = attributeSet.getAttributeValue(i);
            } else if (attributeName.equals("platform")) {
                if (attributeSet.getAttributeValue(i).equals("CN")) {
                    vponPlatform = VponPlatform.CN;
                }
            } else if (attributeName.equals("adSize")) {
                String attributeValue = attributeSet.getAttributeValue(i);
                if (attributeValue.equals("BANNER")) {
                    vponAdSize = VponAdSize.BANNER;
                } else if (attributeValue.equals("IAB_BANNER")) {
                    vponAdSize = VponAdSize.IAB_BANNER;
                } else if (attributeValue.equals("IAB_LEADERBOARD")) {
                    vponAdSize = VponAdSize.IAB_LEADERBOARD;
                } else if (attributeValue.equals("IAB_MRECT")) {
                    vponAdSize = VponAdSize.IAB_MRECT;
                } else if (attributeValue.equals("IAB_WIDE_SKYSCRAPER")) {
                    vponAdSize = VponAdSize.IAB_WIDE_SKYSCRAPER;
                }
            } else if (attributeName.equals("loadAdOnCreate")) {
                if (attributeSet.getAttributeValue(i).toLowerCase().equals("true")) {
                    z2 = true;
                }
            } else if (attributeName.equals("autoFresh") && attributeSet.getAttributeValue(i).toLowerCase().equals("true")) {
                z = true;
            }
        }
        if (str == null) {
            this.d = true;
            return;
        }
        this.a.a(str);
        this.a.a(vponAdSize);
        String a2 = a(vponPlatform);
        if (a2 == null) {
            this.d = true;
            return;
        }
        this.a.b(a2);
        if (z2 && z) {
            VponAdRequest vponAdRequest = new VponAdRequest();
            vponAdRequest.setEnableAutoRefresh(true);
            loadAd(vponAdRequest);
        } else if (z2 && !z) {
            loadAd(new VponAdRequest());
        }
    }

    public VponBanner(Activity activity, String str, VponAdSize vponAdSize, VponPlatform vponPlatform) {
        super(activity);
        this.f85c = activity;
        setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
        X.a(activity).a(this);
        this.g = false;
        this.a = new C(activity, this);
        if (str == null) {
            this.d = true;
            return;
        }
        this.a.a(str);
        if (vponAdSize == null) {
            C0003a.d("VponBanner", "adSize is Null, use SMART_BANNER");
            this.a.a(VponAdSize.SMART_BANNER);
        } else {
            this.a.a(vponAdSize);
        }
        String a2 = a(vponPlatform);
        if (a2 == null) {
            this.d = true;
        } else {
            this.a.b(a2);
        }
    }

    public void request(boolean z) {
        if (!this.d) {
            this.a.b(z);
        }
    }

    public void onLocationChanged(Location location) {
        this.a.a(location);
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }

    public void onVponAdReceived() {
        if (this.b != null) {
            this.b.onVponReceiveAd(this);
        }
    }

    public void onVponAdFailed(VponAdRequest.VponErrorCode vponErrorCode) {
        C0003a.d("VponBanner", "onVponAdFailed VponErrorCode code:" + vponErrorCode.toString());
        if (this.b != null) {
            this.b.onVponFailedToReceiveAd(this, vponErrorCode);
        }
        Q.a().d();
    }

    public void onVponPresent() {
        if (this.b != null) {
            this.b.onVponPresentScreen(this);
        }
    }

    public void onVponDismiss() {
        if (this.b != null) {
            this.b.onVponDismissScreen(this);
        }
    }

    public void onVponLeaveApplication() {
        if (this.b != null) {
            this.b.onVponLeaveApplication(this);
        }
    }

    public void onControllerWebViewReady(int i, int i2) {
        this.e = i;
        this.f = i2;
        removeAllViews();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.e, this.f);
        layoutParams.addRule(13);
        if (this.a == null || this.a.m() == null) {
            C0003a.b("VponBanner", "mController IS NULL or mController.getWebView() IS NULL");
        } else {
            addView(this.a.m(), layoutParams);
        }
        C0003a.a("VponBanner", "this.getChildCount():" + getChildCount());
    }

    public void onPrepareExpandMode() {
        removeAllViews();
        if (this.a.m() != null) {
            TextView textView = new TextView(this.f85c);
            textView.setWidth(this.e);
            textView.setHeight(this.f);
            textView.setTextColor(0);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.e, this.f);
            layoutParams.addRule(13);
            addView(textView, layoutParams);
        }
    }

    public void onLeaveExpandMode() {
        removeAllViews();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.e, this.f);
        layoutParams.addRule(13);
        if (this.a != null && this.a.m() != null) {
            addView(this.a.m(), layoutParams);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            C0003a.a("VponBanner", "enter onDetachedFromWindow in VponBanner");
            super.onDetachedFromWindow();
            if (!this.g) {
                X.a(this.f85c).b(this);
                this.g = true;
            }
            if (this.a != null) {
                this.a.v();
                this.a.b();
                this.a.w();
                this.a = null;
            }
        } catch (Exception e2) {
            C0003a.a("VponBanner", "onDetachedFromWindow throws Exception", e2);
        }
    }

    private static String a(VponPlatform vponPlatform) {
        if (vponPlatform == VponPlatform.TW) {
            return "tw";
        }
        if (vponPlatform == VponPlatform.CN) {
            return "cn";
        }
        return null;
    }

    public void destroy() {
        Q.a().b();
        if (!this.g) {
            X.a(this.f85c).b(this);
            this.g = true;
        }
        new Handler().post(new Runnable() {
            public void run() {
                try {
                    if (VponBanner.this.a != null) {
                        VponBanner.this.a.v();
                        VponBanner.this.a.b();
                        VponBanner.this.a.w();
                        VponBanner.this.a = null;
                    }
                } catch (Exception e) {
                    C0003a.a("VponBanner", "destroy() throws Exception!", e);
                }
            }
        });
    }

    public boolean isReady() {
        if (this.a != null) {
            return this.a.x();
        }
        return false;
    }

    public void loadAd(VponAdRequest vponAdRequest) {
        if (!Y.d(this.f85c)) {
            C0003a.b("VponBanner", "permission-checking  is failde in loadAd!!");
            if (this.b != null) {
                this.b.onVponFailedToReceiveAd(this, VponAdRequest.VponErrorCode.INTERNAL_ERROR);
            }
        } else if (!this.d) {
            this.a.a(vponAdRequest);
        }
    }

    public void setAdListener(VponAdListener vponAdListener) {
        this.b = vponAdListener;
    }

    public void stopLoading() {
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        if (i == 0) {
            C0003a.a("VponBanner", "VponBanner visibility: VISIBLE");
            if (this.a != null) {
                this.a.B();
            }
        } else if (4 == i) {
            C0003a.a("VponBanner", "VponBanner visibility: INVISIBLE");
        } else if (8 == i) {
            C0003a.a("VponBanner", "VponBanner visibility: GONE");
            if (this.a != null) {
                this.a.A();
            }
        }
        super.onWindowVisibilityChanged(i);
    }
}
