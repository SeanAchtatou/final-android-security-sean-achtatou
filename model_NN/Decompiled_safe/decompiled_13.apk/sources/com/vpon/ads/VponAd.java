package com.vpon.ads;

public interface VponAd {
    boolean isReady();

    void loadAd(VponAdRequest vponAdRequest);

    void setAdListener(VponAdListener vponAdListener);

    void stopLoading();
}
