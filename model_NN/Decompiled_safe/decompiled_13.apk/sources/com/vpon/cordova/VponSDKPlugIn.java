package com.vpon.cordova;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Html;
import android.webkit.URLUtil;
import c.Globalization;
import c.NetworkManager;
import com.google.ads.AdActivity;
import com.vpon.webview.VponAdWebView;
import com.vpon.widget.VponActivity;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C;
import vpadn.C0003a;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.D;
import vpadn.O;
import vpadn.Q;

public class VponSDKPlugIn extends C0019q {
    String a;

    /* access modifiers changed from: private */
    public static void a(C0017o oVar, String str) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(AdActivity.INTENT_EXTRAS_PARAM, str);
        oVar.b(jSONObject);
    }

    public boolean execute(String str, final JSONArray jSONArray, final C0017o oVar) {
        final String str2;
        final String str3;
        boolean z;
        Intent intent;
        boolean z2;
        if (str == null || jSONArray == null || oVar == null) {
            try {
                C0003a.b("VponSDKPlugIn", "action == null || args == null || callbackContext ==null");
                return false;
            } catch (Exception e) {
                e.printStackTrace();
                C0003a.a("VponSDKPlugIn", "throws exception at execute", e);
                try {
                    a(oVar, "throw Exception:" + e.getMessage());
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                return false;
            }
        } else {
            C0003a.a("VponSDKPlugIn", "-->>execute action:" + str + " callbackId:" + oVar.a() + " args:" + jSONArray.toString(4));
            if ("load_sdk_constants".equals(str)) {
                boolean z3 = false;
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(0);
                    O a2 = O.a();
                    String str4 = (String) jSONObject.opt("mraid2_banner");
                    C0003a.a("VponSDKPlugIn", "Banner JS:" + str4);
                    if (!C0003a.c(str4)) {
                        a2.a("mraid2_banner", str4);
                    } else {
                        z3 = true;
                    }
                    String str5 = (String) jSONObject.opt("mraid2_expanded");
                    C0003a.a("VponSDKPlugIn", "Banner expanded JS:" + str5);
                    if (!C0003a.c(str5)) {
                        a2.a("mraid2_expanded", str5);
                    } else {
                        z3 = true;
                    }
                    String str6 = (String) jSONObject.opt("mraid2_interstitial");
                    C0003a.a("VponSDKPlugIn", "Interstitial JS:" + str6);
                    if (!C0003a.c(str6)) {
                        a2.a("mraid2_intersitial", str6);
                        z2 = z3;
                    } else {
                        z2 = true;
                    }
                    double d = jSONObject.getDouble("viewable_rate");
                    C0003a.a("VponSDKPlugIn", "cover rate:" + d);
                    a2.a("viewable_rate", Double.valueOf(d));
                    int i = jSONObject.getInt("viewable_duration");
                    C0003a.a("VponSDKPlugIn", "cover duration:" + i);
                    a2.a("viewable_duration", Integer.valueOf(i));
                    if (z2) {
                        C0003a.b("VponSDKPlugIn", "Cannot get all of js files");
                        a(oVar, "Cannot get all of js files");
                    } else {
                        oVar.b();
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doLoadSdkConstants throw exception:" + e3.getMessage(), e3);
                    try {
                        a(oVar, "doLoadSdkConstants throws Exception:" + e3.getMessage());
                    } catch (JSONException e4) {
                        e4.printStackTrace();
                    }
                }
                return true;
            } else if ("close".equals(str)) {
                try {
                    if (this.webView instanceof VponAdWebView) {
                        VponAdWebView vponAdWebView = (VponAdWebView) this.webView;
                        if (vponAdWebView.h().equals("bannerWebView")) {
                            C0003a.b("VponSDKPlugIn", "banner of status of non-expanded cannot call close");
                            a(oVar, "banner of status of non-expanded cannot call close");
                        } else {
                            String h = vponAdWebView.h();
                            if (h.equals("bannerWebViewExpanded") || h.equals("bannerWebViewResized")) {
                                final C c2 = (C) this.cordova;
                                this.cordova.a().runOnUiThread(new Runnable(this) {
                                    public final void run() {
                                        c2.n();
                                        oVar.b();
                                    }
                                });
                            } else if (h.equals("SdkOpenWebApp") || h.equals("InterstitialAdWebView(new Activity)")) {
                                final VponActivity vponActivity = (VponActivity) this.cordova;
                                this.cordova.a().runOnUiThread(new Runnable(this) {
                                    public final void run() {
                                        vponActivity.b();
                                        oVar.b();
                                    }
                                });
                            } else {
                                C0003a.b("VponSDKPlugIn", "dont support to close unknown VponWebView ID");
                                a(oVar, "dont support to close unknown VponWebView ID");
                            }
                        }
                    } else {
                        C0003a.b("VponSDKPlugIn", "something wrong webView instanceof VponAdWebView is falsed");
                        a(oVar, "something wrong webView instanceof VponAdWebView is false");
                    }
                } catch (Exception e5) {
                    e5.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doClose throw exception:" + e5.getMessage(), e5);
                    try {
                        a(oVar, "doClose throw Exception:" + e5.getMessage());
                    } catch (JSONException e6) {
                        e6.printStackTrace();
                    }
                }
                return true;
            } else if ("open_webapp".equals(str)) {
                try {
                    if (this.webView instanceof VponAdWebView) {
                        VponAdWebView vponAdWebView2 = (VponAdWebView) this.webView;
                        if (!vponAdWebView2.h().equals("bannerWebView")) {
                            C0003a.b("VponSDKPlugIn", "only banner webview allow to call open_webapp");
                            a(oVar, "only banner webview allow to call open_webapp");
                        } else {
                            String h2 = vponAdWebView2.h();
                            JSONObject jSONObject2 = jSONArray.getJSONObject(0);
                            C0003a.a("VponSDKPlugIn", "jsonObj:" + jSONObject2.toString());
                            String str7 = null;
                            if (jSONObject2.has(AdActivity.URL_PARAM)) {
                                str7 = jSONObject2.getString(AdActivity.URL_PARAM);
                            }
                            String str8 = null;
                            if (jSONObject2.has(AdActivity.HTML_PARAM)) {
                                str8 = jSONObject2.getString(AdActivity.HTML_PARAM);
                            }
                            boolean z4 = false;
                            if (jSONObject2.has(AdActivity.CUSTOM_CLOSE_PARAM)) {
                                z4 = jSONObject2.getInt(AdActivity.CUSTOM_CLOSE_PARAM) > 0;
                            }
                            boolean z5 = true;
                            if (jSONObject2.has("allow_orientation_change")) {
                                z5 = jSONObject2.getInt("allow_orientation_change") > 0;
                            }
                            String str9 = NetworkManager.TYPE_NONE;
                            if (jSONObject2.has("force_orientation")) {
                                str9 = jSONObject2.getString("force_orientation");
                            }
                            int i2 = 16777215;
                            if (jSONObject2.has("bk_c")) {
                                i2 = jSONObject2.getInt("bk_c");
                            }
                            boolean z6 = false;
                            if (jSONObject2.has("show_prog_bar")) {
                                z6 = jSONObject2.getInt("show_prog_bar") > 0;
                            }
                            boolean z7 = false;
                            if (jSONObject2.has("show_nav_bar")) {
                                z7 = jSONObject2.getInt("show_nav_bar") > 0;
                            }
                            boolean z8 = false;
                            if (jSONObject2.has("use_webview_load_url")) {
                                z8 = jSONObject2.getInt("use_webview_load_url") > 0;
                            }
                            if (str7 == null && str8 == null) {
                                C0003a.b("VponSDKPlugIn", "u is null and html is null");
                                a(oVar, "u is null and html is null");
                            } else {
                                doOpenWebAppStep2(h2, oVar, str7, str8, z4, z5, str9, i2, z6, z7, z8);
                            }
                        }
                    } else {
                        C0003a.b("VponSDKPlugIn", "webView instanceof VponAdWebView is false at doOpenWebAppStep1");
                        a(oVar, "webView instanceof VponAdWebView is false at doOpenWebAppStep1");
                    }
                } catch (Exception e7) {
                    e7.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throws exception in doOpenWebAppStep1", e7);
                    try {
                        a(oVar, "throws exception in doOpenWebAppStep1 exception:" + e7.getMessage());
                    } catch (JSONException e8) {
                        e8.printStackTrace();
                    }
                }
                return true;
            } else if ("open_browser".equals(str)) {
                try {
                    JSONObject jSONObject3 = jSONArray.getJSONObject(0);
                    String str10 = null;
                    if (jSONObject3.has(AdActivity.URL_PARAM)) {
                        str10 = jSONObject3.getString(AdActivity.URL_PARAM);
                    }
                    if (C0003a.c(str10)) {
                        C0003a.b("VponSDKPlugIn", "cannot get url!");
                        a(oVar, "Cannot get ur at doOpenBrowserl!");
                    } else {
                        Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str10));
                        intent2.addFlags(268435456);
                        this.cordova.a().startActivity(intent2);
                        if (this.webView instanceof VponAdWebView) {
                            VponAdWebView vponAdWebView3 = (VponAdWebView) this.webView;
                            if (vponAdWebView3.h().equals("bannerWebView")) {
                                if (this.cordova instanceof C) {
                                    C c3 = (C) this.cordova;
                                    c3.o();
                                    c3.q();
                                }
                            } else if ((vponAdWebView3.h().equals("SdkOpenWebApp") || vponAdWebView3.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                                ((D) this.cordova).y();
                            }
                        } else {
                            a(oVar, "something error [webView instanceof VponAdWebView is false]");
                        }
                        oVar.b();
                    }
                } catch (Exception e9) {
                    C0003a.a("VponSDKPlugIn", "doOpenBrowser throw exception:" + e9.getMessage(), e9);
                    try {
                        a(oVar, "something error e.getLocalizedMessage:" + e9.getMessage());
                    } catch (JSONException e10) {
                        e10.printStackTrace();
                    }
                }
                return true;
            } else if ("expand".equals(str)) {
                try {
                    if (!(this.webView instanceof VponAdWebView)) {
                        C0003a.b("VponSDKPlugIn", "something wrong webView instanceof VponAdWebView is falsed");
                        a(oVar, "only banner webview allow to call expand");
                    } else if (!((VponAdWebView) this.webView).h().equals("bannerWebView")) {
                        C0003a.b("VponSDKPlugIn", "only banner webview allow to call expand");
                        a(oVar, "only banner webview allow to call expand");
                    } else {
                        JSONObject jSONObject4 = jSONArray.getJSONObject(0);
                        final boolean z9 = false;
                        if (jSONObject4.has(AdActivity.CUSTOM_CLOSE_PARAM)) {
                            z9 = jSONObject4.getInt(AdActivity.CUSTOM_CLOSE_PARAM) > 0;
                        }
                        final boolean z10 = true;
                        if (jSONObject4.has("allow_orientation_change")) {
                            z10 = jSONObject4.getInt("allow_orientation_change") > 0;
                        }
                        final String str11 = NetworkManager.TYPE_NONE;
                        if (jSONObject4.has("force_orientation")) {
                            str11 = jSONObject4.getString("force_orientation");
                        }
                        final int i3 = 0;
                        if (jSONObject4.has("bk_c")) {
                            i3 = jSONObject4.getInt("bk_c");
                        }
                        if (this.cordova instanceof C) {
                            final C c4 = (C) this.cordova;
                            final C0017o oVar2 = oVar;
                            this.cordova.a().runOnUiThread(new Runnable(this) {
                                public final void run() {
                                    c4.a(oVar2, z9, z10, str11, i3);
                                }
                            });
                        } else {
                            C0003a.b("VponSDKPlugIn", "only banner webview allow to call expand");
                            a(oVar, "something error [cordova instanceof VponBannerController is false]");
                        }
                    }
                } catch (Exception e11) {
                    e11.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doExpand throw exception:" + e11.getMessage(), e11);
                    try {
                        a(oVar, "something wrong, error exception:" + e11.getMessage());
                    } catch (JSONException e12) {
                        e12.printStackTrace();
                    }
                }
                return true;
            } else if ("resize".equals(str)) {
                try {
                    if (this.webView instanceof VponAdWebView) {
                        VponAdWebView vponAdWebView4 = (VponAdWebView) this.webView;
                        if (vponAdWebView4.h().equals("bannerWebView") || vponAdWebView4.h().equals("bannerWebViewResized")) {
                            JSONObject jSONObject5 = jSONArray.getJSONObject(0);
                            final int i4 = -1;
                            final int i5 = -1;
                            if (jSONObject5.has("w")) {
                                i4 = jSONObject5.getInt("w");
                            } else {
                                a(oVar, "doResize cannot find out width attribute");
                            }
                            if (jSONObject5.has("h")) {
                                i5 = jSONObject5.getInt("h");
                            } else {
                                a(oVar, "doResize cannot find out height attribute");
                            }
                            if (i4 <= 0 || i5 <= 0) {
                                a(oVar, "doResize  height <=0 or width <= 0");
                            }
                            final int i6 = 0;
                            final int i7 = 0;
                            if (jSONObject5.has("off_x")) {
                                i6 = jSONObject5.getInt("off_x");
                            } else {
                                a(oVar, "doResize cannot find out off_x attribute");
                            }
                            if (jSONObject5.has("off_y")) {
                                i7 = jSONObject5.getInt("off_y");
                            } else {
                                a(oVar, "doResize cannot find out off_y attribute");
                            }
                            final String str12 = null;
                            if (jSONObject5.has("cust_close_pos")) {
                                str12 = jSONObject5.getString("cust_close_pos");
                            }
                            final boolean z11 = false;
                            if (jSONObject5.has("allow_off_scr")) {
                                z11 = jSONObject5.getInt("allow_off_scr") > 0;
                            }
                            if (this.cordova instanceof C) {
                                final C c5 = (C) this.cordova;
                                final C0017o oVar3 = oVar;
                                this.cordova.a().runOnUiThread(new Runnable(this) {
                                    public final void run() {
                                        C c2 = c5;
                                        C0017o oVar = oVar3;
                                        int i = i4;
                                        int i2 = i5;
                                        int i3 = i6;
                                        int i4 = i7;
                                        String str = str12;
                                        boolean z = z11;
                                        c2.a(oVar, i, i2, i3, i4, str);
                                    }
                                });
                            } else {
                                C0003a.b("VponSDKPlugIn", "only banner webview allow to call resize");
                                a(oVar, "something error [cordova instanceof VponBannerController is false]");
                            }
                        } else {
                            C0003a.b("VponSDKPlugIn", "only banner or resized webview allow to call resize");
                            a(oVar, "only banner or resized webview allow to call resize");
                        }
                    } else {
                        C0003a.b("VponSDKPlugIn", "something wrong webView instanceof VponAdWebView is falsed");
                        a(oVar, "only banner webview allow to call expand");
                    }
                } catch (Exception e13) {
                    e13.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doResize throw exception:" + e13.getMessage(), e13);
                    try {
                        a(oVar, "something wrong, error exception:" + e13.getMessage());
                    } catch (JSONException e14) {
                        e14.printStackTrace();
                    }
                }
                return true;
            } else if ("click".equals(str)) {
                try {
                    if (this.cordova instanceof D) {
                        final String d2 = ((D) this.cordova).d();
                        if (d2 == null) {
                            a(oVar, "cannot get click-url, maybe banner webview has sent click to server");
                        } else {
                            C0003a.a("VponSDKPlugIn", "====> clickUrl is" + d2);
                            this.cordova.a().runOnUiThread(new Runnable(this) {
                                public final void run() {
                                    Q.a().b(d2);
                                    final String str = d2;
                                    final C0017o oVar = oVar;
                                    new AsyncTask<Object, Integer, Integer>(this) {
                                        private JSONObject a = new JSONObject();
                                        private int b = -1;

                                        /* access modifiers changed from: protected */
                                        public final /* synthetic */ Object doInBackground(Object... objArr) {
                                            return a();
                                        }

                                        private Integer a() {
                                            try {
                                                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                                                C0003a.a(defaultHttpClient);
                                                C0003a.a(str, defaultHttpClient);
                                                Object a2 = O.a().a("user-agent");
                                                if (a2 != null) {
                                                    C0003a.c("VponSDKPlugIn", "userAgent:" + a2);
                                                    defaultHttpClient.getParams().setParameter("http.useragent", a2);
                                                } else {
                                                    C0003a.b("VponSDKPlugIn", "Cannot get user agent from StaticStorage.instance().get(StaticStorage.USER_AGENT)");
                                                }
                                                HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
                                                C0003a.b(str, defaultHttpClient);
                                                this.b = execute.getStatusLine().getStatusCode();
                                                if (this.b < 100 || this.b >= 300) {
                                                    C0003a.b("VponSDKPlugIn", "doClick fail. status code:" + this.b);
                                                    this.a.put("status", this.b);
                                                    this.a.put(AdActivity.INTENT_EXTRAS_PARAM, "http status is not in (1xx~2xx) ");
                                                    oVar.b(this.a);
                                                } else {
                                                    C0003a.a("VponSDKPlugIn", "====> clickUrl is OK, status code:" + this.b);
                                                    this.a.put("status", this.b);
                                                    oVar.a(this.a);
                                                }
                                                return 1;
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                                try {
                                                    C0003a.b("VponSDKPlugIn", "http_get throw Exception");
                                                    this.a.put(AdActivity.INTENT_EXTRAS_PARAM, "do_click throw Exception:" + e.getMessage());
                                                    oVar.b(this.a);
                                                } catch (Exception e2) {
                                                    e2.printStackTrace();
                                                }
                                                return 1;
                                            }
                                        }
                                    }.execute(new Object[0]);
                                }
                            });
                        }
                    } else {
                        a(oVar, "cordova instanceof VponControllerInterface is false [doClick]");
                    }
                } catch (Exception e15) {
                    e15.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doClick Exception:" + e15.getMessage(), e15);
                    try {
                        a(oVar, "throw exception at doClick Exception:" + e15.getMessage());
                    } catch (JSONException e16) {
                        e16.printStackTrace();
                    }
                }
                return true;
            } else if ("place_call".equals(str)) {
                try {
                    JSONObject jSONObject6 = jSONArray.getJSONObject(0);
                    String str13 = null;
                    if (jSONObject6.has("tel")) {
                        str13 = jSONObject6.getString("tel");
                    }
                    if (C0003a.c(str13)) {
                        C0003a.b("VponSDKPlugIn", "TEL number format is wrong");
                        a(oVar, "TEL number format is wrong");
                    } else {
                        Intent intent3 = new Intent("android.intent.action.VIEW", Uri.parse(str13.replaceAll("\\s+", "").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\-", "")));
                        intent3.addFlags(268435456);
                        this.cordova.a().startActivity(intent3);
                        if (this.webView instanceof VponAdWebView) {
                            VponAdWebView vponAdWebView5 = (VponAdWebView) this.webView;
                            if (vponAdWebView5.h().equals("bannerWebView")) {
                                if (this.cordova instanceof C) {
                                    C c6 = (C) this.cordova;
                                    c6.o();
                                    c6.q();
                                }
                            } else if ((vponAdWebView5.h().equals("SdkOpenWebApp") || vponAdWebView5.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                                ((D) this.cordova).y();
                            }
                        } else {
                            a(oVar, "something error [webView instanceof VponAdWebView is false]");
                        }
                        oVar.b();
                    }
                } catch (Exception e17) {
                    e17.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doPlaceCall throw Exception:" + e17.getMessage(), e17);
                    try {
                        a(oVar, "doPlaceCall throw Exception:" + e17.getMessage());
                    } catch (JSONException e18) {
                        e18.printStackTrace();
                    }
                }
                return true;
            } else if ("send_sms".equals(str)) {
                try {
                    JSONObject jSONObject7 = jSONArray.getJSONObject(0);
                    String str14 = null;
                    if (jSONObject7.has("tel")) {
                        str14 = jSONObject7.getString("tel");
                    }
                    if (C0003a.c(str14)) {
                        C0003a.b("VponSDKPlugIn", "TEL number format is wrong");
                        a(oVar, "TEL number format is wrong");
                    } else {
                        String replaceAll = str14.replaceAll("\\s+", "").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\-", "");
                        String str15 = null;
                        if (jSONObject7.has("b")) {
                            str15 = jSONObject7.getString("b");
                        }
                        if (C0003a.c(str15)) {
                            C0003a.b("VponSDKPlugIn", "SMS body is empty");
                            a(oVar, "SMS body is empty");
                        } else {
                            Intent intent4 = new Intent("android.intent.action.VIEW");
                            intent4.putExtra("sms_body", str15);
                            intent4.setData(Uri.parse("sms:" + replaceAll));
                            intent4.putExtra("address", replaceAll);
                            intent4.setType("vnd.android-dir/mms-sms");
                            intent4.addFlags(268435456);
                            this.cordova.a().startActivity(intent4);
                            if (this.webView instanceof VponAdWebView) {
                                VponAdWebView vponAdWebView6 = (VponAdWebView) this.webView;
                                if (vponAdWebView6.h().equals("bannerWebView")) {
                                    if (this.cordova instanceof C) {
                                        C c7 = (C) this.cordova;
                                        c7.o();
                                        c7.q();
                                    }
                                } else if ((vponAdWebView6.h().equals("SdkOpenWebApp") || vponAdWebView6.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                                    ((D) this.cordova).y();
                                }
                            } else {
                                a(oVar, "something error [webView instanceof VponAdWebView is false]");
                            }
                            oVar.b();
                        }
                    }
                } catch (Exception e19) {
                    e19.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doSendSMS throw Exception:" + e19.getMessage(), e19);
                    try {
                        a(oVar, "doSendSMS throw Exception:" + e19.getMessage());
                    } catch (JSONException e20) {
                        e20.printStackTrace();
                    }
                }
                return true;
            } else if ("add_event".equals(str)) {
                try {
                    JSONObject jSONObject8 = jSONArray.getJSONObject(0);
                    ((D) this.cordova).a((String) jSONObject8.opt("et"), ((Integer) jSONObject8.opt("eid")).intValue(), oVar);
                } catch (Exception e21) {
                    e21.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doAddEvent throw exception:" + e21.getMessage(), e21);
                    try {
                        a(oVar, " doAddEvent throw exception:" + e21.getMessage());
                    } catch (Exception e22) {
                        e22.printStackTrace();
                    }
                }
                return true;
            } else if ("remove_event".equals(str)) {
                try {
                    JSONObject jSONObject9 = jSONArray.getJSONObject(0);
                    ((D) this.cordova).b((String) jSONObject9.opt("et"), ((Integer) jSONObject9.opt("eid")).intValue(), oVar);
                } catch (Exception e23) {
                    e23.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doRemoveEvent throw exception:" + e23.getMessage(), e23);
                    try {
                        a(oVar, " doRemoveEvent throw exception:" + e23.getMessage());
                    } catch (Exception e24) {
                        e24.printStackTrace();
                    }
                }
                return true;
            } else if ("open_store".equals(str)) {
                try {
                    JSONObject jSONObject10 = jSONArray.getJSONObject(0);
                    C0003a.a("VponSDKPlugIn", "jsonObj:" + jSONObject10.toString());
                    String str16 = null;
                    if (jSONObject10.has(AdActivity.URL_PARAM)) {
                        str16 = jSONObject10.getString(AdActivity.URL_PARAM);
                    }
                    if (str16.startsWith("market:") || str16.startsWith("http:") || str16.startsWith("https:")) {
                        Intent intent5 = new Intent("android.intent.action.VIEW");
                        intent5.setData(Uri.parse(str16));
                        intent5.addFlags(268435456);
                        this.cordova.a().startActivity(intent5);
                        if (this.webView instanceof VponAdWebView) {
                            VponAdWebView vponAdWebView7 = (VponAdWebView) this.webView;
                            if (vponAdWebView7.h().equals("bannerWebView")) {
                                if (this.cordova instanceof C) {
                                    C c8 = (C) this.cordova;
                                    c8.o();
                                    c8.q();
                                }
                            } else if ((vponAdWebView7.h().equals("SdkOpenWebApp") || vponAdWebView7.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                                ((D) this.cordova).y();
                            }
                        } else {
                            a(oVar, "something error [webView instanceof VponAdWebView is false]");
                        }
                        oVar.b();
                        return true;
                    }
                    C0003a.b("VponSDKPlugIn", "url format error");
                    a(oVar, "url format error");
                    return true;
                } catch (Exception e25) {
                    e25.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doOpenStore Exception:" + e25.getMessage(), e25);
                    try {
                        a(oVar, "throw exception at doOpenStore Exception:" + e25.getMessage());
                    } catch (JSONException e26) {
                        e26.printStackTrace();
                    }
                }
            } else if ("open_video".equals(str)) {
                try {
                    JSONObject jSONObject11 = jSONArray.getJSONObject(0);
                    C0003a.a("VponSDKPlugIn", "jsonObj:" + jSONObject11.toString());
                    String str17 = null;
                    if (jSONObject11.has(AdActivity.URL_PARAM)) {
                        str17 = jSONObject11.getString(AdActivity.URL_PARAM);
                    }
                    if (str17.contains("bit.ly/") || str17.contains("goo.gl/") || str17.contains("tinyurl.com/") || str17.contains("youtu.be/")) {
                        URLConnection openConnection = new URL(str17).openConnection();
                        openConnection.connect();
                        InputStream inputStream = openConnection.getInputStream();
                        str17 = openConnection.getURL().toString();
                        inputStream.close();
                    }
                    Uri parse = Uri.parse(str17);
                    if (str17.contains("youtube.com")) {
                        Uri parse2 = Uri.parse("vnd.youtube:" + parse.getQueryParameter("v"));
                        if (a()) {
                            intent = new Intent("android.intent.action.VIEW", parse2);
                        } else {
                            C0003a.b("VponSDKPlugIn", "URL is youtube format but not install youtube client");
                            a(oVar, "URL is youtube format but not install youtube client");
                            return true;
                        }
                    } else if (str17.contains("file:///android_asset/")) {
                        String replace = str17.replace("file:///android_asset/", "");
                        String substring = replace.substring(replace.lastIndexOf("/") + 1, replace.length());
                        if (!new File(this.cordova.a().getFilesDir() + "/" + substring).exists()) {
                            a(replace, substring);
                        }
                        Uri parse3 = Uri.parse("file://" + this.cordova.a().getFilesDir() + "/" + substring);
                        intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(parse3, "video/*");
                    } else {
                        intent = new Intent("android.intent.action.VIEW");
                        intent.setDataAndType(parse, "video/*");
                    }
                    this.cordova.a().startActivity(intent);
                    if (this.webView instanceof VponAdWebView) {
                        VponAdWebView vponAdWebView8 = (VponAdWebView) this.webView;
                        if (vponAdWebView8.h().equals("bannerWebView")) {
                            if (this.cordova instanceof C) {
                                C c9 = (C) this.cordova;
                                c9.o();
                                c9.q();
                            }
                        } else if ((vponAdWebView8.h().equals("SdkOpenWebApp") || vponAdWebView8.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                            ((D) this.cordova).y();
                        }
                        oVar.b();
                    } else {
                        a(oVar, "something error [webView instanceof VponAdWebView is false]");
                    }
                } catch (Exception e27) {
                    e27.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doOpenVideo throws exception", e27);
                    try {
                        a(oVar, "doOpenVideo throws exception:" + e27.getMessage());
                    } catch (JSONException e28) {
                        e28.printStackTrace();
                    }
                }
                return true;
            } else if ("open_intent".equals(str)) {
                a(jSONArray, oVar);
                return true;
            } else if ("cre_cal_event".equals(str)) {
                try {
                    AnonymousClass8 r5 = new SimpleDateFormat(this, "yyyy-MM-dd'T'HH:mm:ssZ") {
                        public final Date parse(String str, ParsePosition parsePosition) {
                            return super.parse(str.replaceFirst(":(?=[0-9]{2}$)", ""), parsePosition);
                        }
                    };
                    String str18 = null;
                    String str19 = null;
                    String str20 = null;
                    String str21 = null;
                    String str22 = null;
                    JSONObject jSONObject12 = jSONArray.getJSONObject(0).getJSONObject(AdActivity.INTENT_EXTRAS_PARAM);
                    if (jSONObject12.has("description")) {
                        str22 = jSONObject12.getString("description");
                    }
                    if (jSONObject12.has("location")) {
                        str19 = jSONObject12.getString("location");
                    }
                    if (jSONObject12.has("start")) {
                        str20 = jSONObject12.getString("start");
                    }
                    if (jSONObject12.has("end")) {
                        str21 = jSONObject12.getString("end");
                    }
                    if (jSONObject12.has("summary")) {
                        str18 = jSONObject12.getString("summary");
                    }
                    if (str20 == null || str21 == null) {
                        a(oVar, "Cannot get start or end at doCreateCalendarEvent");
                        return true;
                    }
                    if (str22 == null) {
                        a(oVar, "Cannot get title (description) at doCreateCalendarEvent");
                    } else {
                        Date parse4 = r5.parse(str20);
                        Date parse5 = r5.parse(str21);
                        GregorianCalendar gregorianCalendar = new GregorianCalendar();
                        Intent intent6 = new Intent("android.intent.action.EDIT");
                        intent6.setType("vnd.android.cursor.item/event");
                        gregorianCalendar.setTime(parse4);
                        intent6.putExtra("beginTime", gregorianCalendar.getTimeInMillis());
                        gregorianCalendar.setTime(parse5);
                        intent6.putExtra("endTime", gregorianCalendar.getTimeInMillis());
                        if (str18 != null) {
                            intent6.putExtra("description", str18);
                        }
                        if (str19 != null) {
                            intent6.putExtra("eventLocation", str19);
                        }
                        if (str22 != null) {
                            intent6.putExtra("title", str22);
                        }
                        intent6.addFlags(268435456);
                        try {
                            this.cordova.a().startActivity(intent6);
                            if (this.webView instanceof VponAdWebView) {
                                VponAdWebView vponAdWebView9 = (VponAdWebView) this.webView;
                                if (vponAdWebView9.h().equals("bannerWebView")) {
                                    if (this.cordova instanceof C) {
                                        C c10 = (C) this.cordova;
                                        c10.o();
                                        c10.q();
                                    }
                                } else if ((vponAdWebView9.h().equals("SdkOpenWebApp") || vponAdWebView9.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                                    ((D) this.cordova).y();
                                }
                            } else {
                                a(oVar, "something error [webView instanceof VponAdWebView is false]");
                            }
                            oVar.b();
                        } catch (Exception e29) {
                            e29.printStackTrace();
                            C0003a.b("VponSDKPlugIn", "doCreateCalendarEvent throw Exception[startActivity]");
                            a(oVar, "doCreateCalendarEvent throw Exception:" + e29.getMessage());
                        }
                    }
                    return true;
                } catch (Exception e30) {
                    e30.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "doCreateCalendarEvent throw Exception" + e30.getMessage(), e30);
                    try {
                        a(oVar, "doCreateCalendarEvent throw Exception:" + e30.getMessage());
                    } catch (JSONException e31) {
                        e31.printStackTrace();
                    }
                }
            } else if ("store_pic".equals(str)) {
                final Activity a3 = this.cordova.a();
                if (a3 == null) {
                    C0003a.b("VponSDKPlugIn", "context is null in doStroePicture");
                    try {
                        a(oVar, "context is null at doStroePicture");
                    } catch (JSONException e32) {
                        e32.printStackTrace();
                    }
                } else {
                    this.cordova.a().runOnUiThread(new Runnable() {
                        public final void run() {
                            try {
                                JSONObject jSONObject = jSONArray.getJSONObject(0);
                                if (!jSONObject.has(AdActivity.URL_PARAM)) {
                                    C0003a.b("VponSDKPlugIn", "jsonObj.has(JSONParamConstant.URL) is false");
                                    VponSDKPlugIn.a(oVar, "jsonObj.has(JSONParamConstant.URL) is false");
                                } else if (!URLUtil.isNetworkUrl(jSONObject.getString(AdActivity.URL_PARAM))) {
                                    C0003a.b("VponSDKPlugIn", "!URLUtil.isNetworkUrl(url)");
                                    VponSDKPlugIn.a(oVar, "jsonObj.has(JSONParamConstant.URL) is false");
                                } else {
                                    Configuration configuration = a3.getResources().getConfiguration();
                                    String str = "Store Picture";
                                    String str2 = "Are You sure to add picture to photo album?";
                                    String str3 = "Yes";
                                    String str4 = "No";
                                    if (configuration.locale.equals(Locale.TAIWAN) || configuration.locale.equals(Locale.TRADITIONAL_CHINESE)) {
                                        str = "儲存圖片";
                                        str2 = "確定將儲存圖片到手機的相簿？";
                                        str3 = "同意";
                                        str4 = "不同意";
                                    } else if (configuration.locale.equals(Locale.SIMPLIFIED_CHINESE)) {
                                        str = "储存图片";
                                        str2 = "确定将储存图片到手机的相簿？";
                                        str3 = "同意";
                                        str4 = "不同意";
                                    }
                                    AlertDialog.Builder message = new AlertDialog.Builder(a3).setTitle(str).setMessage(str2);
                                    final JSONArray jSONArray = jSONArray;
                                    final C0017o oVar = oVar;
                                    AlertDialog.Builder positiveButton = message.setPositiveButton(str3, new DialogInterface.OnClickListener() {
                                        public final void onClick(DialogInterface dialogInterface, int i) {
                                            ExecutorService e = VponSDKPlugIn.this.cordova.e();
                                            final JSONArray jSONArray = jSONArray;
                                            final C0017o oVar = oVar;
                                            e.execute(new Runnable() {
                                                public final void run() {
                                                    try {
                                                        if (!VponSDKPlugIn.this.a(jSONArray.getJSONObject(0).getString(AdActivity.URL_PARAM))) {
                                                            C0003a.b("VponSDKPlugIn", "storePicture return false");
                                                            VponSDKPlugIn.a(oVar, "storePicture return false");
                                                            return;
                                                        }
                                                        oVar.b();
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                        C0003a.b("VponSDKPlugIn", "throw exception at doStorePicture postion 2 exception:" + e.getMessage());
                                                        try {
                                                            VponSDKPlugIn.a(oVar, "throw exception at doStorePicture postion 2 exception:" + e.getMessage());
                                                        } catch (JSONException e2) {
                                                            e2.printStackTrace();
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    });
                                    final C0017o oVar2 = oVar;
                                    positiveButton.setNegativeButton(str4, new DialogInterface.OnClickListener() {
                                        public final void onClick(DialogInterface dialogInterface, int i) {
                                            C0003a.a("VponSDKPlugIn", "pic cannot save!");
                                            try {
                                                VponSDKPlugIn.a(oVar2, "pic cannot save!");
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                C0003a.a("VponSDKPlugIn", "throw exception at doStorePicture Exception:" + e.getMessage(), e);
                                try {
                                    VponSDKPlugIn.a(oVar, "throw exception at doStorePicture exception:" + e.getMessage());
                                } catch (JSONException e2) {
                                    e2.printStackTrace();
                                }
                            }
                        }
                    });
                }
                return true;
            } else if ("set_orientation".equals(str)) {
                try {
                    if (!(this.webView instanceof VponAdWebView)) {
                        C0003a.b("VponSDKPlugIn", "something wrong [webView instanceof VponAdWebView is falsed]");
                        a(oVar, "something wrong [webView instanceof VponAdWebView is false]");
                    } else if (((VponAdWebView) this.webView).h().equals("bannerWebView")) {
                        C0003a.b("VponSDKPlugIn", "banner webview do not allow to call set_orientation");
                        a(oVar, "banner webview do not allow to call set_orientation");
                    } else {
                        JSONObject jSONObject13 = jSONArray.getJSONObject(0);
                        C0003a.a("VponSDKPlugIn", "jsonObj:" + jSONObject13.toString());
                        if (jSONObject13.has("allow_orientation_change")) {
                            z = jSONObject13.getInt("allow_orientation_change") > 0;
                        } else {
                            z = true;
                        }
                        String str23 = NetworkManager.TYPE_NONE;
                        if (jSONObject13.has("force_orientation")) {
                            str23 = jSONObject13.getString("force_orientation");
                        }
                        int i8 = Resources.getSystem().getConfiguration().orientation;
                        if (!str23.equals(NetworkManager.TYPE_NONE)) {
                            if (str23.equals("portrait")) {
                                this.cordova.a().setRequestedOrientation(1);
                            } else if (str23.equals("landscape")) {
                                this.cordova.a().setRequestedOrientation(0);
                            }
                        } else if (!z) {
                            if (i8 == 2) {
                                this.cordova.a().setRequestedOrientation(0);
                            } else if (i8 == 1) {
                                this.cordova.a().setRequestedOrientation(1);
                            }
                        }
                        oVar.b();
                    }
                } catch (Exception e33) {
                    e33.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doSetOrientation Exception:" + e33.getMessage(), e33);
                    try {
                        a(oVar, "throw exception at doSetOrientation Exception:" + e33.getMessage());
                    } catch (JSONException e34) {
                        e34.printStackTrace();
                    }
                }
                return true;
            } else if ("http_get".equals(str)) {
                try {
                    JSONObject jSONObject14 = jSONArray.getJSONObject(0);
                    if (jSONObject14.has(AdActivity.URL_PARAM)) {
                        str3 = jSONObject14.getString(AdActivity.URL_PARAM);
                    } else {
                        str3 = null;
                    }
                    if (C0003a.c(str3)) {
                        a(oVar, "Cannot get url!");
                    } else if (str3.toLowerCase().startsWith("http://") || str3.toLowerCase().startsWith("https://")) {
                        final int i9 = jSONObject14.has("timeout") ? jSONObject14.getInt("timeout") : -1;
                        this.cordova.a().runOnUiThread(new Runnable(this) {
                            public final void run() {
                                final String str = str3;
                                final int i = i9;
                                final C0017o oVar = oVar;
                                new AsyncTask<Object, Integer, Integer>(this) {
                                    private JSONObject a = new JSONObject();
                                    private int b = -1;

                                    /* access modifiers changed from: protected */
                                    public final /* synthetic */ Object doInBackground(Object... objArr) {
                                        return a();
                                    }

                                    private Integer a() {
                                        try {
                                            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                                            C0003a.a(defaultHttpClient);
                                            C0003a.a(str, defaultHttpClient);
                                            if (i != -1) {
                                                C0003a.c("VponSDKPlugIn", "timeout ms:" + (i * 1000));
                                                defaultHttpClient.getParams().setParameter("http.connection.timeout", Integer.valueOf(i * 1000));
                                            }
                                            Object a2 = O.a().a("user-agent");
                                            if (a2 != null) {
                                                C0003a.c("VponSDKPlugIn", "userAgent:" + a2);
                                                defaultHttpClient.getParams().setParameter("http.useragent", a2);
                                            } else {
                                                C0003a.b("VponSDKPlugIn", "Cannot get user agent from StaticStorage.instance().get(StaticStorage.USER_AGENT)");
                                            }
                                            HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
                                            C0003a.b(str, defaultHttpClient);
                                            this.b = execute.getStatusLine().getStatusCode();
                                            if (this.b < 100 || this.b >= 300) {
                                                C0003a.b("VponSDKPlugIn", "!(statusCode >= 100 && statusCode < 300) at doHttpGet");
                                                this.a.put("status", this.b);
                                                this.a.put("error", "http status is not in (1xx~2xx) ");
                                                oVar.b(this.a);
                                            } else {
                                                this.a.put("status", this.b);
                                                oVar.a(this.a);
                                            }
                                            return 1;
                                        } catch (Exception e2) {
                                            e2.printStackTrace();
                                            try {
                                                C0003a.a("VponSDKPlugIn", "http_get throw Exception:" + e2.getMessage(), e2);
                                                this.a.put("error", "http_get throw Exception:" + e2.getMessage());
                                                this.a.put("status", this.b);
                                                oVar.b(this.a);
                                            } catch (Exception e3) {
                                                e3.printStackTrace();
                                            }
                                            return 1;
                                        }
                                    }
                                }.execute(new Object[0]);
                            }
                        });
                    } else {
                        a(oVar, "url format error!");
                    }
                } catch (Exception e35) {
                    e35.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doHttpGet Exception:" + e35.getMessage(), e35);
                    try {
                        a(oVar, "throw exception at doHttpGet" + e35.getMessage());
                    } catch (JSONException e36) {
                        e36.printStackTrace();
                    }
                }
                return true;
            } else if ("ad_req".equals(str)) {
                try {
                    Q.a().b();
                    if (this.webView instanceof VponAdWebView) {
                        VponAdWebView vponAdWebView10 = (VponAdWebView) this.webView;
                        if (!vponAdWebView10.h().equals("init") && !vponAdWebView10.h().equals("init-finish")) {
                            C0003a.b("VponSDKPlugIn", "CALL doAdReq from VponWebView ID:" + vponAdWebView10.h());
                            a(oVar, "CALL doAdReq from VponWebView ID:" + vponAdWebView10.h());
                            return true;
                        }
                    }
                    JSONObject jSONObject15 = jSONArray.getJSONObject(0);
                    if (jSONObject15.has(AdActivity.URL_PARAM)) {
                        str2 = jSONObject15.getString(AdActivity.URL_PARAM);
                    } else {
                        str2 = null;
                    }
                    if (C0003a.c(str2)) {
                        a(oVar, "Cannot get url!");
                    } else if (str2.toLowerCase().startsWith("http://") || str2.toLowerCase().startsWith("https://")) {
                        C0003a.a("VponSDKPlugIn", "doAdReq http url:" + str2);
                        if (this.cordova instanceof D) {
                            final D d3 = (D) this.cordova;
                            this.cordova.a().runOnUiThread(new Runnable() {
                                public final void run() {
                                    final String str = str2;
                                    final C0017o oVar = oVar;
                                    final D d2 = d3;
                                    new AsyncTask<Object, Integer, Integer>() {
                                        private JSONObject a = new JSONObject();
                                        private int b = -1;

                                        /* access modifiers changed from: protected */
                                        public final /* synthetic */ Object doInBackground(Object... objArr) {
                                            return a();
                                        }

                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: vpadn.Q.b(org.apache.http.HttpResponse, boolean):void
                                         arg types: [org.apache.http.HttpResponse, int]
                                         candidates:
                                          vpadn.Q.b(java.lang.String, boolean):void
                                          vpadn.Q.b(org.apache.http.HttpResponse, boolean):void */
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: vpadn.Q.a(org.apache.http.HttpResponse, boolean):void
                                         arg types: [org.apache.http.HttpResponse, int]
                                         candidates:
                                          vpadn.Q.a(java.lang.String, boolean):void
                                          vpadn.Q.a(org.apache.http.HttpResponse, boolean):void */
                                        private Integer a() {
                                            try {
                                                DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
                                                C0003a.a(defaultHttpClient);
                                                C0003a.a(str, defaultHttpClient);
                                                HttpClientParams.setRedirecting(defaultHttpClient.getParams(), false);
                                                Object a2 = O.a().a("user-agent");
                                                if (a2 != null) {
                                                    C0003a.c("VponSDKPlugIn", "userAgent:" + a2);
                                                    defaultHttpClient.getParams().setParameter("http.useragent", a2);
                                                } else {
                                                    C0003a.b("VponSDKPlugIn", "Cannot get user agent from StaticStorage.instance().get(StaticStorage.USER_AGENT)");
                                                }
                                                HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
                                                C0003a.b(str, defaultHttpClient);
                                                this.b = execute.getStatusLine().getStatusCode();
                                                C0003a.c("VponSDKPlugIn", "HTTP-STATUS-CODE:" + this.b);
                                                if (this.b == 302 || this.b == 301 || this.b == 303) {
                                                    Q.a().a(execute, false);
                                                    if (execute.containsHeader("Vpadn-Refresh-Time")) {
                                                        String value = execute.getFirstHeader("Vpadn-Refresh-Time").getValue();
                                                        C0003a.c("VponSDKPlugIn", "refreshTime:" + value);
                                                        if (value != null) {
                                                            try {
                                                                d2.a((long) Integer.valueOf(value).intValue());
                                                            } catch (NumberFormatException e2) {
                                                                e2.printStackTrace();
                                                            }
                                                        } else {
                                                            C0003a.b("VponSDKPlugIn", "Cannot get banner refreshSecond");
                                                        }
                                                    } else {
                                                        C0003a.b("VponSDKPlugIn", "Cannot get banner refreshSecond (cannot find field in header)");
                                                    }
                                                    if (execute.containsHeader("Location")) {
                                                        String value2 = execute.getFirstHeader("Location").getValue();
                                                        C0003a.c("VponSDKPlugIn", "bannerUrl:" + value2);
                                                        if (value2 != null) {
                                                            d2.c(value2);
                                                        } else {
                                                            C0003a.b("VponSDKPlugIn", "Cannot get banner URL");
                                                        }
                                                    } else {
                                                        C0003a.b("VponSDKPlugIn", "Cannot get banner URL (cannot find field in header)");
                                                    }
                                                    if (execute.containsHeader("Vpadn-Imp")) {
                                                        String value3 = execute.getFirstHeader("Vpadn-Imp").getValue();
                                                        C0003a.c("VponSDKPlugIn", "impressionUrl:" + value3);
                                                        if (value3 != null) {
                                                            d2.e(value3);
                                                        } else {
                                                            C0003a.b("VponSDKPlugIn", "Cannot get impression URL");
                                                        }
                                                    } else {
                                                        C0003a.b("VponSDKPlugIn", "Cannot get impression URL (cannot find field in header)");
                                                    }
                                                    if (execute.containsHeader("Vpadn-Clk")) {
                                                        String value4 = execute.getFirstHeader("Vpadn-Clk").getValue();
                                                        C0003a.c("VponSDKPlugIn", "clickUrl:" + value4);
                                                        if (value4 != null) {
                                                            d2.d(value4);
                                                        } else {
                                                            C0003a.b("VponSDKPlugIn", "Cannot get click URL");
                                                        }
                                                    } else {
                                                        C0003a.b("VponSDKPlugIn", "Cannot get click URL (cannot find field in header)");
                                                    }
                                                    this.a.put("status", this.b);
                                                    oVar.a(this.a);
                                                    VponSDKPlugIn.this.cordova.a().runOnUiThread(new Runnable() {
                                                        public final void run() {
                                                            VponSDKPlugIn.this.webView.a("load_banner", (Object) null);
                                                        }
                                                    });
                                                    return 1;
                                                }
                                                this.a.put("status", this.b);
                                                this.a.put(AdActivity.INTENT_EXTRAS_PARAM, "http status is not 301~303 ");
                                                C0003a.b("VponSDKPlugIn", "do doAdReq return status code:" + this.b);
                                                if (execute.containsHeader("Vpadn-Status-Code")) {
                                                    C0003a.b("VponSDKPlugIn", "doAdReq return error status code:" + execute.getFirstHeader("Vpadn-Status-Code").getValue());
                                                }
                                                if (execute.containsHeader("Vpadn-Status")) {
                                                    VponSDKPlugIn.this.a = execute.getFirstHeader("Vpadn-Status").getValue();
                                                    C0003a.b("VponSDKPlugIn", "doAdReq return error status:" + VponSDKPlugIn.this.a);
                                                } else {
                                                    VponSDKPlugIn.this.a = null;
                                                }
                                                if (execute.containsHeader("Vpadn-Status-Desc")) {
                                                    C0003a.b("VponSDKPlugIn", "doAdReq return error status description:" + execute.getFirstHeader("Vpadn-Status-Desc").getValue());
                                                }
                                                Q.a().b(execute, false);
                                                VponSDKPlugIn.this.cordova.a().runOnUiThread(new Runnable() {
                                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                                     method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
                                                     arg types: [java.lang.String, java.lang.String]
                                                     candidates:
                                                      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
                                                      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
                                                      c.CordovaWebView.a(vpadn.v, java.lang.String):void
                                                      c.CordovaWebView.a(boolean, boolean):void
                                                      c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
                                                    public final void run() {
                                                        if (VponSDKPlugIn.this.webView != null) {
                                                            VponSDKPlugIn.this.webView.a("load_banner_fail", (Object) VponSDKPlugIn.this.a);
                                                        }
                                                    }
                                                });
                                                oVar.b(this.a);
                                                return 1;
                                            } catch (Exception e3) {
                                                e3.printStackTrace();
                                                try {
                                                    C0003a.a("VponSDKPlugIn", "doAdReq throw Exception:" + e3.getMessage(), e3);
                                                    this.a.put(AdActivity.INTENT_EXTRAS_PARAM, "doAdReq throw Exception:" + e3.getMessage());
                                                    oVar.b(this.a);
                                                } catch (Exception e4) {
                                                    e4.printStackTrace();
                                                }
                                                return 1;
                                            }
                                        }
                                    }.execute(new Object[0]);
                                }
                            });
                        } else {
                            a(oVar, "cordova instanceof VponControllerInterface is false [doAdReq]");
                        }
                    } else {
                        a(oVar, "url format error!");
                    }
                } catch (Exception e37) {
                    e37.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doAdReq Exception:" + e37.getMessage(), e37);
                    try {
                        a(oVar, "throw exception at doAdReq Exception:" + e37.getMessage());
                    } catch (JSONException e38) {
                        e38.printStackTrace();
                    }
                }
                return true;
            } else if ("get_sdk_params".equals(str)) {
                try {
                    JSONObject h3 = ((D) this.cordova).h();
                    C0003a.a("VponSDKPlugIn", "doGetSdkParams:" + h3.toString(4));
                    oVar.a(h3);
                } catch (Exception e39) {
                    e39.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doGetSdkParams", e39);
                    try {
                        a(oVar, "doGetSdkParams return Exception:" + e39.getMessage());
                    } catch (JSONException e40) {
                        e40.printStackTrace();
                    }
                }
                return true;
            } else if ("use_custom_close".equals(str)) {
                try {
                    final boolean z12 = jSONArray.getJSONObject(0).getInt(AdActivity.CUSTOM_CLOSE_PARAM) != 0;
                    this.cordova.a().runOnUiThread(new Runnable() {
                        public final void run() {
                            ((D) VponSDKPlugIn.this.cordova).a(z12);
                            oVar.b();
                        }
                    });
                } catch (Exception e41) {
                    e41.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doSetCustomClose", e41);
                    try {
                        a(oVar, "doSetCustomClose return Exception:" + e41.getMessage());
                    } catch (JSONException e42) {
                        e42.printStackTrace();
                    }
                }
                return true;
            } else if ("put_data".equals(str)) {
                try {
                    JSONObject jSONObject16 = jSONArray.getJSONObject(0);
                    String str24 = null;
                    if (jSONObject16.has("k")) {
                        str24 = jSONObject16.getString("k");
                    }
                    if (C0003a.c(str24)) {
                        a(oVar, "Cannot get key at JSON of put_data!");
                    } else {
                        O.a().a(str24, new JSONObject(jSONObject16.toString()));
                        oVar.b();
                    }
                } catch (Exception e43) {
                    e43.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doPutData", e43);
                    try {
                        a(oVar, "doPutData return Exception:" + e43.getMessage());
                    } catch (JSONException e44) {
                        e44.printStackTrace();
                    }
                }
                return true;
            } else if ("get_data".equals(str)) {
                try {
                    JSONObject jSONObject17 = jSONArray.getJSONObject(0);
                    String str25 = null;
                    if (jSONObject17.has("k")) {
                        str25 = jSONObject17.getString("k");
                    }
                    if (C0003a.c(str25)) {
                        a(oVar, "Cannot get key at JSON of get_data!");
                    } else {
                        JSONObject d4 = O.a().d(str25);
                        if (d4 != null) {
                            oVar.a(d4);
                        } else {
                            a(oVar, "Cannot get value by the key:" + str25);
                        }
                    }
                } catch (Exception e45) {
                    e45.printStackTrace();
                    C0003a.a("VponSDKPlugIn", "throw exception at doGetData", e45);
                    try {
                        a(oVar, "doGetData return Exception:" + e45.getMessage());
                    } catch (JSONException e46) {
                        e46.printStackTrace();
                    }
                }
                return true;
            } else if ("remove_data".equals(str)) {
                b(jSONArray, oVar);
                return true;
            } else if ("test".equals(str)) {
                a(oVar, "Cannot support test now!!");
                return true;
            } else {
                C0003a.b("VponSDKPlugIn", "SDK: no action matched!");
                JSONObject jSONObject18 = new JSONObject();
                jSONObject18.put("action", str);
                jSONObject18.put(AdActivity.INTENT_EXTRAS_PARAM, "SDK: no action matched!");
                oVar.b(jSONObject18);
                return true;
            }
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        C0003a.b("VponSDKPlugIn", "Call onActivityResult requestCode:" + i + " resultCode:" + i2);
        if (600001 == i) {
            C0003a.b("VponSDKPlugIn", "return from 'openBrowser'");
        }
    }

    public void doOpenWebAppStep2(String str, C0017o oVar, String str2, String str3, boolean z, boolean z2, String str4, int i, boolean z3, boolean z4, boolean z5) {
        final String str5 = str;
        final C0017o oVar2 = oVar;
        final String str6 = str2;
        final String str7 = str3;
        final boolean z6 = z;
        final boolean z7 = z2;
        final String str8 = str4;
        final int i2 = i;
        final boolean z8 = z3;
        final boolean z9 = z4;
        final boolean z10 = z5;
        this.cordova.a().runOnUiThread(new Runnable() {
            public final void run() {
                if (str5.equals("bannerWebView")) {
                    if (VponSDKPlugIn.this.cordova instanceof C) {
                        ((C) VponSDKPlugIn.this.cordova).a(oVar2, str6, str7, z6, z7, str8, i2, z8, z9, z10);
                        return;
                    }
                    C0003a.b("VponSDKPlugIn", "cannot cast to VponBannerController");
                    try {
                        VponSDKPlugIn.a(oVar2, "cannot cast to VponBannerController");
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                } else if (!str5.equals("InterstitialAdWebView(new Activity)")) {
                    try {
                        C0003a.b("VponSDKPlugIn", "webId is " + str5 + " at doOpenWebAppStep2");
                        VponSDKPlugIn.a(oVar2, "webId is " + str5 + " at doOpenWebAppStep2");
                    } catch (JSONException e3) {
                        e3.printStackTrace();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        C0003a.b("VponSDKPlugIn", "call storePicture(urlStr)");
        try {
            if (!URLUtil.isNetworkUrl(str)) {
                C0003a.b("VponSDKPlugIn", "ERROR URL");
                return false;
            }
            URLConnection openConnection = new URL(str).openConnection();
            openConnection.connect();
            InputStream inputStream = openConnection.getInputStream();
            if (inputStream == null) {
                C0003a.b("VponSDKPlugIn", "inputStream == null");
                return false;
            }
            File randomFile = getRandomFile(openConnection.getContentType());
            if (randomFile == null) {
                return false;
            }
            FileOutputStream fileOutputStream = new FileOutputStream(randomFile);
            byte[] bArr = new byte[256];
            while (true) {
                int read = inputStream.read(bArr);
                if (read > 0) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    try {
                        inputStream.close();
                        fileOutputStream.close();
                        MediaStore.Images.Media.insertImage(this.cordova.a().getContentResolver(), randomFile.getAbsolutePath(), "VPON_AD_PIC", "VPON_AD_PIC_DES");
                        randomFile.delete();
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            C0003a.a("VponSDKPlugIn", "storePicture throws Exception:" + e2.getMessage(), e2);
            return false;
        }
    }

    public File getRandomFile(String str) {
        String str2 = ".jpg";
        if (!str.startsWith("image")) {
            return null;
        }
        if (str.contains("png")) {
            str2 = ".png";
        } else if (str.contains("gif")) {
            str2 = ".gif";
        }
        String uuid = UUID.randomUUID().toString();
        String path = Environment.getExternalStorageDirectory().getPath();
        C0003a.b("VponSDKPlugIn", path);
        File file = new File(path, "/vpon");
        file.mkdirs();
        return new File(file, "VPON-justin" + uuid + str2);
    }

    private void a(JSONArray jSONArray, C0017o oVar) {
        String str;
        Uri uri;
        JSONObject jSONObject;
        JSONArray jSONArray2;
        JSONArray jSONArray3 = null;
        try {
            JSONObject jSONObject2 = jSONArray.getJSONObject(0);
            String string = jSONObject2.has("a") ? jSONObject2.getString("a") : null;
            if (jSONObject2.has(Globalization.TYPE)) {
                str = jSONObject2.getString(Globalization.TYPE);
            } else {
                str = null;
            }
            if (jSONObject2.has(AdActivity.URL_PARAM)) {
                uri = Uri.parse(jSONObject2.getString(AdActivity.URL_PARAM));
            } else {
                uri = null;
            }
            if (jSONObject2.has("extras")) {
                jSONObject = jSONObject2.getJSONObject("extras");
            } else {
                jSONObject = null;
            }
            HashMap hashMap = new HashMap();
            if (jSONObject != null) {
                JSONArray names = jSONObject.names();
                for (int i = 0; i < names.length(); i++) {
                    String string2 = names.getString(i);
                    hashMap.put(string2, jSONObject.getString(string2));
                }
            }
            if (jSONObject2.has("cats")) {
                jSONArray2 = jSONObject2.getJSONArray("cats");
            } else {
                jSONArray2 = null;
            }
            ArrayList<String> arrayList = new ArrayList<>();
            if (jSONArray2 != null) {
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    arrayList.add(jSONArray2.getString(i2));
                }
            }
            if (jSONObject2.has("flags")) {
                jSONArray3 = jSONObject2.getJSONArray("flags");
            }
            ArrayList<Integer> arrayList2 = new ArrayList<>();
            if (jSONArray3 != null) {
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    arrayList2.add(Integer.valueOf(jSONArray3.getInt(i3)));
                }
            }
            Intent intent = uri != null ? new Intent(string, uri) : new Intent(string);
            if (str != null && uri != null) {
                intent.setDataAndType(uri, str);
            } else if (str != null) {
                intent.setType(str);
            }
            for (String str2 : hashMap.keySet()) {
                String str3 = (String) hashMap.get(str2);
                if (str2.equals("android.intent.extra.TEXT") && str.equals("text/html")) {
                    intent.putExtra(str2, Html.fromHtml(str3));
                } else if (str2.equals("android.intent.extra.STREAM")) {
                    intent.putExtra(str2, Uri.parse(str3));
                } else if (str2.equals("android.intent.extra.EMAIL")) {
                    intent.putExtra("android.intent.extra.EMAIL", new String[]{str3});
                } else {
                    intent.putExtra(str2, str3);
                }
            }
            for (String addCategory : arrayList) {
                intent.addCategory(addCategory);
            }
            for (Integer intValue : arrayList2) {
                intent.addFlags(intValue.intValue());
            }
            this.cordova.a().startActivity(intent);
            if (this.webView instanceof VponAdWebView) {
                VponAdWebView vponAdWebView = (VponAdWebView) this.webView;
                if (vponAdWebView.h().equals("bannerWebView")) {
                    if (this.cordova instanceof C) {
                        C c2 = (C) this.cordova;
                        c2.o();
                        c2.q();
                    }
                } else if ((vponAdWebView.h().equals("SdkOpenWebApp") || vponAdWebView.h().equals("InterstitialAdWebView(new Activity)")) && (this.cordova instanceof D)) {
                    ((D) this.cordova).y();
                }
            } else {
                a(oVar, "something error [webView instanceof VponAdWebView is false]");
            }
            oVar.b();
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.a("VponSDKPlugIn", "throw exception at openIntent Exception:" + e.getMessage(), e);
            try {
                a(oVar, "throw exception at openIntent Exception:" + e.getMessage());
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void b(JSONArray jSONArray, C0017o oVar) {
        try {
            JSONObject jSONObject = jSONArray.getJSONObject(0);
            String str = null;
            if (jSONObject.has("k")) {
                str = jSONObject.getString("k");
            }
            if (C0003a.c(str)) {
                a(oVar, "Cannot get key at JSON of remove_data!");
                return;
            }
            O.a().e(str);
            oVar.b();
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.a("VponSDKPlugIn", "throw exception at doRemoveData", e);
            try {
                a(oVar, "doRemoveData return Exception:" + e.getMessage());
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    private void a(String str, String str2) throws IOException {
        InputStream open = this.cordova.a().getAssets().open(str);
        FileOutputStream openFileOutput = this.cordova.a().openFileOutput(str2, 1);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = open.read(bArr);
            if (read <= 0) {
                open.close();
                openFileOutput.close();
                return;
            }
            openFileOutput.write(bArr, 0, read);
        }
    }

    private boolean a() {
        try {
            this.cordova.a().getPackageManager().getPackageInfo("com.google.android.youtube", 1);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
