package com.vpon.widget;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import c.NetworkManager;
import com.google.ads.AdActivity;
import com.vpon.ads.VponAdSize;
import com.vpon.webview.VponAdWebView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0003a;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0024v;
import vpadn.D;
import vpadn.L;
import vpadn.M;
import vpadn.O;

public class VponActivity extends Activity implements View.OnClickListener, D, L, C0018p {
    private static final RelativeLayout.LayoutParams G = new RelativeLayout.LayoutParams(-1, -1);
    private Map<String, Map<Integer, C0017o>> A = Collections.synchronizedMap(new HashMap());
    private Map<String, String> B = Collections.synchronizedMap(new HashMap());
    private boolean C = false;
    private JSONObject D = new JSONObject();
    private long E;
    private long F;
    /* access modifiers changed from: private */
    public VponAdWebView a = null;
    /* access modifiers changed from: private */
    public RelativeLayout b = null;

    /* renamed from: c  reason: collision with root package name */
    private String f100c = null;
    /* access modifiers changed from: private */
    public String d = null;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public ImageView f = null;
    private C0019q g = null;
    private int h;
    private int i;
    private String j;
    private boolean k;
    private int l;
    private String m = null;
    private String n = null;
    private boolean o = true;
    private boolean p = false;
    private boolean q = false;
    private int r = 16777215;
    /* access modifiers changed from: private */
    public String s = null;
    /* access modifiers changed from: private */
    public ProgressBar t = null;
    /* access modifiers changed from: private */
    public LinearLayout u = null;
    /* access modifiers changed from: private */
    public Button v = null;
    /* access modifiers changed from: private */
    public Button w = null;
    /* access modifiers changed from: private */
    public Button x = null;
    /* access modifiers changed from: private */
    public Button y = null;
    private final ExecutorService z = Executors.newCachedThreadPool();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z2;
        C0003a.a("VponActivity", ">>>>>>>>>Enter onCreate");
        super.onCreate(bundle);
        Bundle extras = getIntent().getExtras();
        requestWindowFeature(1);
        if (extras.getBoolean("isFullScreen")) {
            getWindow().addFlags(1024);
        }
        getWindow().setFlags(16777216, 16777216);
        C0003a.b(this);
        this.A.clear();
        this.e = extras.getBoolean("isUseCustomClose");
        this.h = extras.getInt("originalRequestedOrientation");
        this.i = extras.getInt("beforeActivityOrientation");
        this.j = extras.getString("forceOrientation");
        this.k = extras.getBoolean("isAllowOrientationChange");
        if (!this.j.equals(NetworkManager.TYPE_NONE)) {
            if (this.j.equals("portrait")) {
                setRequestedOrientation(1);
            } else if (this.j.equals("landscape")) {
                setRequestedOrientation(0);
            }
        } else if (!this.k) {
            if (this.i == 2) {
                setRequestedOrientation(0);
            } else if (this.i == 1) {
                setRequestedOrientation(1);
            }
        }
        this.l = extras.getInt("distance");
        this.m = extras.getString("getControllerKey");
        this.n = extras.getString("getCallbackContextKey");
        String string = extras.getString("click_url");
        if (string != null) {
            this.B.put("url_type_click", string);
        }
        this.E = extras.getLong("session_id");
        this.F = extras.getLong("sequence_number");
        this.b = new RelativeLayout(this);
        this.f100c = extras.getString("adType");
        if (C0003a.c(this.f100c)) {
            C0003a.b("VponActivity", "mAdType is null at VponActivity onCreate method");
            finish();
            return;
        }
        extras.getInt("statusBarHeight");
        if (this.f100c.equals("interstitial")) {
            this.s = extras.getString("url");
            this.d = extras.getString(AdActivity.HTML_PARAM);
            if (C0003a.c(this.d)) {
                C0003a.b("VponActivity", "show interstitial ad, but mHtml is NULL!!");
                finish();
                return;
            }
            C0003a.a("VponActivity", "showInterstitialAd :HTML:" + this.d);
            this.a = new VponAdWebView("InterstitialAdWebView(new Activity)", this, this);
            this.a.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.b.setBackgroundColor(0);
            this.a.setBackgroundColor(extras.containsKey("backgroundColor") ? extras.getInt("backgroundColor") : 0);
            this.b.addView(this.a, G);
            if (!this.e) {
                i();
            }
            setContentView(this.b);
            b(false);
        } else if (this.f100c.equals("sdkOpenWebApp")) {
            C0003a.a("VponActivity", ">>>>>>>>>Enter onCreate showSdkOpenWebApp!!");
            this.s = extras.getString("url");
            this.d = extras.getString(AdActivity.HTML_PARAM);
            this.r = extras.getInt("backgroundColor");
            this.o = extras.getBoolean("isShowProgressBar");
            this.p = extras.getBoolean("isShowNavigationBar");
            this.q = extras.getBoolean("isUseWebViewLoadUrl");
            if (C0003a.c(this.s)) {
                this.q = false;
                z2 = false;
            } else {
                z2 = true;
            }
            if (!C0003a.c(this.s) && !C0003a.c(this.d)) {
                this.q = false;
                z2 = false;
            }
            this.b.setBackgroundColor(0);
            this.a = new VponAdWebView("SdkOpenWebApp", this, this);
            this.a.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            this.a.setBackgroundColor(this.r);
            this.a.setOnTouchListener(new View.OnTouchListener(this) {
                public final boolean onTouch(View view, MotionEvent motionEvent) {
                    return false;
                }
            });
            this.b.addView(this.a, G);
            if (this.o) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(13);
                this.t = new ProgressBar(this, null, 16842874);
                this.b.addView(this.t, layoutParams);
            }
            if (this.p) {
                c();
            }
            if (!this.e) {
                i();
            }
            setContentView(this.b);
            if (z2 && !this.q) {
                b(true);
            } else if (this.q) {
                this.a.loadUrl(this.s);
            } else if (!C0003a.c(this.d)) {
                this.a.loadDataWithBaseURL("file:///android_asset/www/vpon", this.d, "text/html", "utf-8", null);
            }
        } else {
            C0003a.b("VponActivity", "illeage AdType:" + this.f100c + " at VponActivity onCreate method");
        }
    }

    public void setTheme(int i2) {
        super.setTheme(16973839);
    }

    private void b(final boolean z2) {
        new AsyncTask<Object, Integer, Integer>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object doInBackground(Object... objArr) {
                if (z2) {
                    if (VponActivity.this.a(VponActivity.this.s)) {
                        if (VponActivity.this.a != null) {
                            VponActivity.this.a.loadDataWithBaseURL(VponActivity.this.s, VponActivity.this.d, "text/html", "utf-8", null);
                        }
                    } else if (VponActivity.this.a != null) {
                        VponActivity.this.a.loadUrl(VponActivity.this.s);
                    }
                } else if (C0003a.c(VponActivity.this.d) || VponActivity.this.a == null) {
                    C0003a.b("VponActivity", "SHOW interstitial ad error (StringUtils.isBlank(mHtml) == true || mShowWebView == null) ");
                } else {
                    VponActivity.this.a.loadDataWithBaseURL(VponActivity.this.s, VponActivity.this.d, "text/html", "utf-8", null);
                }
                return 1;
            }
        }.execute(new Object[0]);
    }

    /* access modifiers changed from: private */
    public void c() {
        boolean z2;
        int i2;
        double d2 = 0.0d;
        C0003a.a("VponActivity", "CALL createNaviationBar");
        if (Resources.getSystem().getConfiguration().orientation == 2) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.u = new LinearLayout(this);
        this.v = new Button(this);
        this.w = new Button(this);
        this.x = new Button(this);
        this.y = new Button(this);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int height = defaultDisplay.getHeight();
        int width = defaultDisplay.getWidth();
        if (z2) {
            i2 = (int) (((double) height) * 0.09375d);
        } else {
            i2 = (int) (((double) height) * 0.08333333333333333d);
        }
        this.u.setMinimumHeight(i2);
        this.u.setMinimumWidth(width);
        int i3 = width / 4;
        int i4 = (int) (((double) i3) / 2.030075187969925d);
        if (i4 > i2) {
            C0003a.a("VponActivity", "(buttonHeight > navigationBarLayoutHeight) buttonHeight:" + i4 + " navigationBarLayoutHeight" + i2);
            i3 = (int) (((double) i2) * 2.030075187969925d);
            i4 = i2;
        }
        C0003a.a("VponActivity", "screenWidth:" + width + " screenHeight:" + height);
        C0003a.a("VponActivity", "navigationBarLayoutHeight:" + i2 + " buttonWidth:" + i3 + " buttonHeight:" + i4);
        this.u.setBackgroundColor(0);
        this.u.setBackgroundDrawable(b("/vpon_bg.png"));
        this.v.setId(991);
        this.v.setBackgroundColor(0);
        this.v.setBackgroundDrawable(b("/vpon_close.png"));
        this.w.setId(992);
        this.w.setBackgroundColor(0);
        this.w.setBackgroundDrawable(b("/vpon_prev.png"));
        this.x.setId(993);
        this.x.setBackgroundColor(0);
        this.x.setBackgroundDrawable(b("/vpon_next.png"));
        this.y.setId(994);
        this.y.setBackgroundColor(0);
        this.y.setBackgroundDrawable(b("/vpon_opennew.png"));
        double d3 = ((((double) width) / 4.0d) - ((double) i3)) / 2.0d;
        double d4 = ((double) (i2 - i4)) / 2.0d;
        C0003a.a("VponActivity", "buttonMargins:" + d4);
        if (d4 < 0.0d) {
            d4 = 0.0d;
        }
        if (d3 >= 0.0d) {
            d2 = d3;
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i3, i4);
        layoutParams.setMargins((int) d2, (int) d4, 0, 0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(i3, i4);
        layoutParams2.setMargins((int) (d2 * 2.0d), (int) d4, 0, 0);
        this.v.setOnClickListener(this);
        this.w.setOnClickListener(this);
        this.x.setOnClickListener(this);
        this.y.setOnClickListener(this);
        AnonymousClass6 r1 = new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    if (view.getId() == 991) {
                        VponActivity.this.v.setBackgroundColor(0);
                        VponActivity.this.v.setBackgroundDrawable(VponActivity.this.b("/vpon_close_mix.png"));
                    } else if (view.getId() == 992) {
                        VponActivity.this.w.setBackgroundColor(0);
                        VponActivity.this.w.setBackgroundDrawable(VponActivity.this.b("/vpon_prev_mix.png"));
                    } else if (view.getId() == 993) {
                        VponActivity.this.x.setBackgroundColor(0);
                        VponActivity.this.x.setBackgroundDrawable(VponActivity.this.b("/vpon_next_mix.png"));
                    } else if (view.getId() == 994) {
                        VponActivity.this.y.setBackgroundColor(0);
                        VponActivity.this.y.setBackgroundDrawable(VponActivity.this.b("/vpon_opennew_mix.png"));
                    }
                } else if (motionEvent.getAction() == 1) {
                    if (view.getId() == 991) {
                        VponActivity.this.v.setBackgroundColor(0);
                        VponActivity.this.v.setBackgroundDrawable(VponActivity.this.b("/vpon_close.png"));
                    } else if (view.getId() == 992) {
                        VponActivity.this.w.setBackgroundColor(0);
                        VponActivity.this.w.setBackgroundDrawable(VponActivity.this.b("/vpon_prev.png"));
                    } else if (view.getId() == 993) {
                        VponActivity.this.x.setBackgroundColor(0);
                        VponActivity.this.x.setBackgroundDrawable(VponActivity.this.b("/vpon_next.png"));
                    } else if (view.getId() == 994) {
                        VponActivity.this.y.setBackgroundColor(0);
                        VponActivity.this.y.setBackgroundDrawable(VponActivity.this.b("/vpon_opennew.png"));
                    }
                }
                return false;
            }
        };
        this.v.setOnTouchListener(r1);
        this.w.setOnTouchListener(r1);
        this.x.setOnTouchListener(r1);
        this.y.setOnTouchListener(r1);
        this.u.addView(this.v, layoutParams);
        this.u.addView(this.w, layoutParams2);
        this.u.addView(this.x, layoutParams2);
        this.u.addView(this.y, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(12);
        layoutParams3.addRule(14);
        layoutParams3.addRule(13);
        this.b.addView(this.u, layoutParams3);
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            C0003a.a(defaultHttpClient);
            C0003a.a(str, defaultHttpClient);
            Object a2 = O.a().a("user-agent");
            if (a2 != null) {
                C0003a.c("VponActivity", "userAgent:" + a2);
                defaultHttpClient.getParams().setParameter("http.useragent", a2);
            }
            HttpResponse execute = defaultHttpClient.execute(new HttpGet(str));
            C0003a.b(str, defaultHttpClient);
            if (execute.getStatusLine().getStatusCode() != 200) {
                C0003a.b("VponActivity", ">>>>>getStatusCode() != HttpStatus.SC_OK\n" + execute.getStatusLine().getStatusCode());
                return false;
            }
            this.d = EntityUtils.toString(execute.getEntity(), "UTF-8");
            if (C0003a.c(this.d)) {
                C0003a.b("VponActivity", "StringUtils.isBlank(mHtml)");
                return false;
            }
            C0003a.c("VponActivity", "two part mHtml:" + this.d);
            return true;
        } catch (Exception e2) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Object a2;
        C0003a.a("VponActivity", "------------------> onDestroy");
        super.onDestroy();
        if (this.a != null) {
            this.a.setWebViewJsAlertShow(false);
        }
        setRequestedOrientation(this.h);
        O a3 = O.a();
        if (C0003a.c(this.f100c)) {
            C0003a.b("VponActivity", "onDestroy--> StringUtils.isBlank(mAdType)");
        } else if (!(this.m == null || (a2 = a3.a(this.m)) == null)) {
            D d2 = (D) a2;
            d2.p();
            d2.f();
            a3.b(this.m);
        }
        if (this.a != null) {
            this.a.stopLoading();
            this.a.removeAllViews();
            this.a.e();
            this.a = null;
        }
        this.b.removeAllViews();
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        if (this.b == null) {
            C0003a.b("VponActivity", "redrawCloseIndicatorButton, but mRelativeLayout == null");
        } else if (z2) {
            new Handler().post(new Runnable() {
                public final void run() {
                    if (VponActivity.this.b.getChildCount() >= 2 && VponActivity.this.f != null) {
                        VponActivity.this.b.removeView(VponActivity.this.f);
                        VponActivity.this.f = null;
                    }
                    VponActivity.this.i();
                }
            });
        } else if (this.b != null) {
            new Handler().post(new Runnable() {
                public final void run() {
                    if (!VponActivity.this.e && VponActivity.this.f == null) {
                        VponActivity.this.i();
                    } else if (!VponActivity.this.e && VponActivity.this.b.getChildCount() >= 2 && VponActivity.this.f != null) {
                        VponActivity.this.b.removeView(VponActivity.this.f);
                        VponActivity.this.f = null;
                        VponActivity.this.i();
                    } else if (!VponActivity.this.e || VponActivity.this.b.getChildCount() < 2 || VponActivity.this.f == null) {
                        boolean unused = VponActivity.this.e;
                    } else {
                        VponActivity.this.b.removeView(VponActivity.this.f);
                        VponActivity.this.f = null;
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.b != null) {
            this.f = new ImageView(this);
            this.f.setVisibility(0);
            this.f.setBackgroundDrawable(b("/vpon_button_close50.png"));
            this.f.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    VponActivity.this.j();
                }
            });
            DisplayMetrics f2 = C0003a.f(this);
            int i2 = (int) (f2.density * 50.0f);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i2, i2);
            layoutParams.leftMargin = f2.widthPixels - i2;
            this.b.addView(this.f, layoutParams);
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        C0003a.a("VponActivity", "doClose()");
        if (this.a != null) {
            this.a.b(true);
        }
        finish();
    }

    public final void b() {
        new Handler().post(new Runnable() {
            public final void run() {
                VponActivity.this.j();
            }
        });
    }

    public void onClick(View view) {
        Uri parse;
        C0003a.a("VponActivity", "v.getId():" + view.getId());
        switch (view.getId()) {
            case 991:
                j();
                return;
            case 992:
                if (this.a.canGoBack()) {
                    C0003a.b("VponActivity", "CanGoBack()");
                    this.a.goBack();
                    return;
                }
                return;
            case 993:
                this.a.goForward();
                return;
            case 994:
                if (this.s != null) {
                    parse = Uri.parse(this.s);
                } else {
                    parse = Uri.parse(this.a.getUrl());
                }
                Intent intent = new Intent("android.intent.action.VIEW", parse);
                intent.setFlags(65536);
                intent.setFlags(268435456);
                startActivity(intent);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public Drawable b(String str) {
        return new BitmapDrawable(getClass().getResourceAsStream(str));
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        C0003a.b("VponActivity", "-------->>>requestCode:" + i2 + " resultCode:" + i3);
        C0019q qVar = this.g;
        if (qVar != null) {
            C0003a.b("VponActivity", "--------call callback.onActivityResult");
            qVar.onActivityResult(i2, i3, intent);
            return;
        }
        C0003a.b("VponActivity", "--------callback == null");
    }

    public final void a(C0019q qVar, Intent intent, int i2) {
        C0003a.b("VponActivity", "-------->>>Call startActivityForResult requestCode:" + i2);
        this.g = qVar;
        startActivityForResult(intent, i2);
    }

    public final Activity a() {
        return this;
    }

    public final Object a(String str, Object obj) {
        if (!str.equals("close")) {
            return null;
        }
        C0003a.b("VponActivity", "Call onMessage id is close");
        new Handler().post(new Runnable() {
            public final void run() {
                VponActivity.this.j();
            }
        });
        return null;
    }

    public final ExecutorService e() {
        return this.z;
    }

    public final void r() {
        this.C = true;
        a("onshow", (JSONObject) null);
    }

    public final void s() {
        this.C = false;
        a("onhide", (JSONObject) null);
    }

    public final void t() {
        D d2;
        D d3;
        if (this.f100c != null && this.f100c.equals("sdkOpenWebApp")) {
            new Handler().post(new Runnable() {
                public final void run() {
                    if (VponActivity.this.b != null && VponActivity.this.t != null) {
                        C0003a.a("VponActivity", "REMOVE mProgressBar");
                        VponActivity.this.b.removeView(VponActivity.this.t);
                    }
                }
            });
            if (this.b != null) {
                C0003a.a("VponActivity", "end onWebViewLoadPageFinish");
                if (this.n != null) {
                    O a2 = O.a();
                    C0017o oVar = (C0017o) a2.a(this.n);
                    if (oVar != null) {
                        oVar.b();
                        a2.b(this.n);
                    }
                }
                if (!(this.m == null || (d3 = (D) O.a().a(this.m)) == null)) {
                    d3.g();
                }
            }
        } else if (!(this.f100c == null || !this.f100c.equals("interstitial") || this.m == null || (d2 = (D) O.a().a(this.m)) == null)) {
            d2.g();
        }
        if (this.l > 0) {
            this.a.loadUrl(String.format("javascript:getDistance('%d')", Integer.valueOf(this.l)));
        }
    }

    public final void a(int i2, int i3) {
        C0003a.a("VponActivity", "Call onWebViewSizeChanged w:" + i2 + " h:" + i3);
        c(false);
        if (this.b != null && this.u != null && this.p) {
            new Handler().post(new Runnable() {
                public final void run() {
                    VponActivity.this.b.removeView(VponActivity.this.u);
                    VponActivity.this.c();
                }
            });
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (this.a != null) {
            Rect rect = new Rect();
            this.a.getGlobalVisibleRect(rect);
            int round = Math.round(VponAdSize.convertPixelsToDp((float) rect.left, this));
            int round2 = Math.round(VponAdSize.convertPixelsToDp((float) rect.top, this));
            int round3 = Math.round(VponAdSize.convertPixelsToDp((float) (rect.right - rect.left), this));
            int round4 = Math.round(VponAdSize.convertPixelsToDp((float) (rect.bottom - rect.top), this));
            C0003a.a("VponActivity", "X1:" + round + " Y1:" + round2 + " wDip:" + round3 + " hDip:" + round4);
            try {
                this.D.put("x", round);
                this.D.put("y", round2);
                this.D.put("w", round3);
                this.D.put("h", round4);
                a("ad_pos_change", this.D);
            } catch (Exception e2) {
                e2.printStackTrace();
                C0003a.b("VponActivity", "onWebViewLayoutChanged throw exception");
            }
        }
    }

    public final void a(WebView webView, int i2, String str, String str2) {
        if (this.t != null && this.f100c.equals("sdkOpenWebApp")) {
            new Handler().post(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.vpon.widget.VponActivity.a(com.vpon.widget.VponActivity, boolean):void
                 arg types: [com.vpon.widget.VponActivity, int]
                 candidates:
                  com.vpon.widget.VponActivity.a(com.vpon.widget.VponActivity, android.widget.ImageView):void
                  com.vpon.widget.VponActivity.a(java.lang.String, org.json.JSONObject):void
                  com.vpon.widget.VponActivity.a(com.vpon.widget.VponActivity, java.lang.String):boolean
                  com.vpon.widget.VponActivity.a(java.lang.String, java.lang.Object):java.lang.Object
                  com.vpon.widget.VponActivity.a(int, int):void
                  vpadn.L.a(int, int):void
                  vpadn.p.a(java.lang.String, java.lang.Object):java.lang.Object
                  com.vpon.widget.VponActivity.a(com.vpon.widget.VponActivity, boolean):void */
                public final void run() {
                    if (VponActivity.this.b != null) {
                        if (VponActivity.this.t != null) {
                            VponActivity.this.b.removeView(VponActivity.this.t);
                        }
                        VponActivity.this.b.setBackgroundColor(-1);
                        VponActivity.this.c(true);
                    }
                }
            });
        }
        if (this.n != null) {
            O a2 = O.a();
            C0017o oVar = (C0017o) a2.a(this.n);
            if (oVar != null) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(AdActivity.INTENT_EXTRAS_PARAM, "call onWebViewReceivedError");
                    oVar.b(jSONObject);
                    a2.b(this.n);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public final JSONObject h() {
        Exception e2;
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject = M.a().a(this, jSONObject2);
            try {
                jSONObject.put("sid", this.E);
                jSONObject.put("seq", this.F);
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return jSONObject;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            jSONObject = jSONObject2;
            e2 = exc;
            e2.printStackTrace();
            return jSONObject;
        }
        return jSONObject;
    }

    public final void a(boolean z2) {
        this.e = z2;
        c(false);
    }

    public final void f() {
    }

    public final void g() {
    }

    public final void a(long j2) {
    }

    public final void p() {
    }

    public final void c(String str) {
    }

    public final void d(String str) {
    }

    public final void e(String str) {
    }

    public final String d() {
        return this.B.remove("url_type_click");
    }

    public final void a(String str, int i2, C0017o oVar) {
        try {
            if ("onhide".equals(str) || "onshow".equals(str) || "ad_pos_change".equals(str)) {
                Map map = this.A.get(str);
                if (map == null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(Integer.valueOf(i2), oVar);
                    this.A.put(str, hashMap);
                } else {
                    map.put(Integer.valueOf(i2), oVar);
                }
                if ("ad_pos_change".equals(str)) {
                    C0024v vVar = new C0024v(C0024v.a.OK, this.D);
                    vVar.a(true);
                    oVar.a(vVar);
                } else if ("onshow".equals(str)) {
                    if (this.C) {
                        C0024v vVar2 = new C0024v(C0024v.a.OK);
                        vVar2.a(true);
                        oVar.a(vVar2);
                    }
                } else if ("onhide".equals(str) && this.C) {
                    C0024v vVar3 = new C0024v(C0024v.a.OK);
                    vVar3.a(true);
                    oVar.a(vVar3);
                }
            } else {
                C0003a.c("VponActivity", "EventType not supported! " + str);
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "EventType not supported!"));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "addEventListener throw Exception:" + e2.getMessage()));
            } catch (Exception e3) {
                e2.printStackTrace();
            }
        }
    }

    public final void b(String str, int i2, C0017o oVar) {
        try {
            if (!"onhide".equals(str) && !"onshow".equals(str) && !"ad_pos_change".equals(str)) {
                C0003a.c("VponActivity", "EventType not supported! " + str);
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "EventType not supported!"));
            } else if (this.A.containsKey(str)) {
                Map map = this.A.get(str);
                map.remove(Integer.valueOf(i2));
                if (map.size() == 0) {
                    this.A.remove(str);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "removeEventListener throw Exception:" + e2.getMessage()));
            } catch (Exception e3) {
                e2.printStackTrace();
            }
        }
    }

    private void a(String str, JSONObject jSONObject) {
        C0024v vVar;
        if (jSONObject != null) {
            C0003a.a("VponActivity", "VponActivity TriggerEvent eventType:" + str + " retObj:" + jSONObject.toString());
        } else {
            C0003a.a("VponActivity", "VponActivity TriggerEvent eventType:" + str);
        }
        if (this.A.get(str) != null) {
            if (jSONObject != null) {
                vVar = new C0024v(C0024v.a.OK, jSONObject);
            } else {
                vVar = new C0024v(C0024v.a.OK);
            }
            vVar.a(true);
            for (C0017o a2 : this.A.get(str).values()) {
                a2.a(vVar);
            }
        }
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        View view = null;
        if (this.a != null) {
            view = this.a.getFocusedChild();
        }
        if (this.a == null || ((!this.a.g() && view == null) || i2 != 4)) {
            return super.onKeyUp(i2, keyEvent);
        }
        return this.a.onKeyUp(i2, keyEvent);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        View view = null;
        if (this.a != null) {
            view = this.a.getFocusedChild();
        }
        if (view == null || i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        return this.a.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        C0003a.a("VponActivity", "onPause()");
        super.onPause();
        if (this.a != null) {
            this.a.b(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vpon.webview.VponAdWebView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.vpon.webview.VponAdWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(java.lang.String, java.lang.Object):void
      c.CordovaWebView.a(vpadn.v, java.lang.String):void
      com.vpon.webview.VponAdWebView.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void onResume() {
        C0003a.a("VponActivity", "onResume()");
        super.onResume();
        if (this.a != null) {
            this.a.a(false, false);
        }
    }

    public final void y() {
        Object a2;
        O a3 = O.a();
        if (this.m != null && (a2 = a3.a(this.m)) != null && (a2 instanceof D)) {
            ((D) a2).y();
        }
    }

    public final void z() {
    }
}
