package c;

import android.net.Uri;
import android.os.Build;
import android.util.Log;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0024v;

public class FileTransfer extends C0019q {
    public static int ABORTED_ERR = 4;
    public static int CONNECTION_ERR = 3;
    public static int FILE_NOT_FOUND_ERR = 1;
    public static int INVALID_URL_ERR = 2;
    /* access modifiers changed from: private */
    public static HashMap<String, b> a = new HashMap<>();
    /* access modifiers changed from: private */
    public static final HostnameVerifier b = new HostnameVerifier() {
        public final boolean verify(String str, SSLSession sSLSession) {
            return true;
        }
    };

    /* renamed from: c  reason: collision with root package name */
    private static final TrustManager[] f14c = {new X509TrustManager() {
        public final X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }

        public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        }

        public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        }
    }};

    static final class b {
        String a;
        String b;

        /* renamed from: c  reason: collision with root package name */
        File f17c;
        InputStream d;
        OutputStream e;
        boolean f;
        private C0017o g;

        b(String str, String str2, C0017o oVar) {
            this.a = str;
            this.b = str2;
            this.g = oVar;
        }

        /* access modifiers changed from: package-private */
        public final void a(C0024v vVar) {
            synchronized (this) {
                if (!this.f) {
                    this.g.a(vVar);
                }
            }
        }
    }

    static final class a extends FilterInputStream {
        private boolean a;

        public a(InputStream inputStream) {
            super(inputStream);
        }

        public final int read() throws IOException {
            int read = this.a ? -1 : super.read();
            this.a = read == -1;
            return read;
        }

        public final int read(byte[] bArr) throws IOException {
            int read = this.a ? -1 : super.read(bArr);
            this.a = read == -1;
            return read;
        }

        public final int read(byte[] bArr, int i, int i2) throws IOException {
            int read = this.a ? -1 : super.read(bArr, i, i2);
            this.a = read == -1;
            return read;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.FileTransfer.a(int, java.lang.String, java.lang.String, java.lang.Integer):org.json.JSONObject
     arg types: [int, java.lang.String, java.lang.String, int]
     candidates:
      c.FileTransfer.a(int, java.lang.String, java.lang.String, java.net.URLConnection):org.json.JSONObject
      c.FileTransfer.a(int, java.lang.String, java.lang.String, java.lang.Integer):org.json.JSONObject */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean execute(java.lang.String r18, org.json.JSONArray r19, vpadn.C0017o r20) throws org.json.JSONException {
        /*
            r17 = this;
            java.lang.String r1 = "upload"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x0014
            java.lang.String r1 = "download"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x028a
        L_0x0014:
            r1 = 0
            r0 = r19
            java.lang.String r7 = r0.getString(r1)
            r1 = 1
            r0 = r19
            java.lang.String r8 = r0.getString(r1)
            java.lang.String r1 = "upload"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01cb
            java.lang.String r1 = "UTF-8"
            java.lang.String r14 = java.net.URLDecoder.decode(r7, r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "upload "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r14)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = " to "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r1 = 2
            java.lang.String r2 = "file"
            r0 = r19
            java.lang.String r11 = a(r0, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r1 = 3
            java.lang.String r2 = "image.jpg"
            r0 = r19
            java.lang.String r12 = a(r0, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r1 = 4
            java.lang.String r2 = "image/jpeg"
            r0 = r19
            java.lang.String r13 = a(r0, r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r1 = 5
            r0 = r19
            org.json.JSONObject r1 = r0.optJSONObject(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            if (r1 != 0) goto L_0x0180
            org.json.JSONObject r10 = new org.json.JSONObject     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r10.<init>()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
        L_0x0079:
            r1 = 6
            r0 = r19
            boolean r5 = r0.optBoolean(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r1 = 7
            r0 = r19
            boolean r1 = r0.optBoolean(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            if (r1 != 0) goto L_0x0189
            r1 = 7
            r0 = r19
            boolean r1 = r0.isNull(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            if (r1 != 0) goto L_0x0189
            r15 = 0
        L_0x0093:
            r1 = 8
            r0 = r19
            org.json.JSONObject r1 = r0.optJSONObject(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            if (r1 != 0) goto L_0x018c
            java.lang.String r1 = "headers"
            org.json.JSONObject r9 = r10.optJSONObject(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
        L_0x00a3:
            r1 = 9
            r0 = r19
            java.lang.String r4 = r0.getString(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "fileKey: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "fileName: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r12)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "mimeType: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r13)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "params: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r10)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "trustEveryone: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "chunkedMode: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r15)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "headers: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r9)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "objectId: "
            r2.<init>(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.d(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.net.URL r7 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0196 }
            r7.<init>(r8)     // Catch:{ MalformedURLException -> 0x0196 }
            java.lang.String r1 = r7.getProtocol()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r2 = "https"
            boolean r6 = r1.equals(r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            c.FileTransfer$b r3 = new c.FileTransfer$b     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r0 = r20
            r3.<init>(r14, r8, r0)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.util.HashMap<java.lang.String, c.FileTransfer$b> r2 = c.FileTransfer.a     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            monitor-enter(r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.util.HashMap<java.lang.String, c.FileTransfer$b> r1 = c.FileTransfer.a     // Catch:{ all -> 0x01c8 }
            r1.put(r4, r3)     // Catch:{ all -> 0x01c8 }
            monitor-exit(r2)     // Catch:{ all -> 0x01c8 }
            r0 = r17
            vpadn.p r1 = r0.cordova     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.util.concurrent.ExecutorService r16 = r1.e()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            c.FileTransfer$3 r1 = new c.FileTransfer$3     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r2 = r17
            r1.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r0 = r16
            r0.execute(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
        L_0x017e:
            r1 = 1
        L_0x017f:
            return r1
        L_0x0180:
            r1 = 5
            r0 = r19
            org.json.JSONObject r10 = r0.optJSONObject(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            goto L_0x0079
        L_0x0189:
            r15 = 1
            goto L_0x0093
        L_0x018c:
            r1 = 8
            r0 = r19
            org.json.JSONObject r9 = r0.optJSONObject(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            goto L_0x00a3
        L_0x0196:
            r1 = move-exception
            int r2 = c.FileTransfer.INVALID_URL_ERR     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            org.json.JSONObject r2 = a(r2, r14, r8, r3)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            java.lang.String r3 = "FileTransfer"
            java.lang.String r4 = r2.toString()     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            android.util.Log.e(r3, r4, r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            vpadn.v r1 = new vpadn.v     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            vpadn.v$a r3 = vpadn.C0024v.a.IO_EXCEPTION     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r1.<init>(r3, r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            r0 = r20
            r0.a(r1)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            goto L_0x017e
        L_0x01b8:
            r1 = move-exception
            vpadn.v r1 = new vpadn.v
            vpadn.v$a r2 = vpadn.C0024v.a.MALFORMED_URL_EXCEPTION
            java.lang.String r3 = "UTF-8 error."
            r1.<init>(r2, r3)
            r0 = r20
            r0.a(r1)
            goto L_0x017e
        L_0x01c8:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
            throw r1     // Catch:{ UnsupportedEncodingException -> 0x01b8 }
        L_0x01cb:
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "download "
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = " to "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r8)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            r1 = 2
            r0 = r19
            boolean r5 = r0.optBoolean(r1)
            r1 = 3
            r0 = r19
            java.lang.String r4 = r0.getString(r1)
            java.net.URL r9 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0240 }
            r9.<init>(r7)     // Catch:{ MalformedURLException -> 0x0240 }
            java.lang.String r1 = r9.getProtocol()
            java.lang.String r2 = "https"
            boolean r6 = r1.equals(r2)
            boolean r1 = vpadn.C0004b.a(r7)
            if (r1 != 0) goto L_0x0263
            java.lang.String r1 = "FileTransfer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Source URL is not in white list: '"
            r2.<init>(r3)
            java.lang.StringBuilder r2 = r2.append(r7)
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.w(r1, r2)
            int r1 = c.FileTransfer.CONNECTION_ERR
            r2 = 401(0x191, float:5.62E-43)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            org.json.JSONObject r1 = a(r1, r7, r8, r2)
            vpadn.v r2 = new vpadn.v
            vpadn.v$a r3 = vpadn.C0024v.a.IO_EXCEPTION
            r2.<init>(r3, r1)
            r0 = r20
            r0.a(r2)
            goto L_0x017e
        L_0x0240:
            r1 = move-exception
            int r2 = c.FileTransfer.INVALID_URL_ERR
            r3 = 0
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            org.json.JSONObject r2 = a(r2, r7, r8, r3)
            java.lang.String r3 = "FileTransfer"
            java.lang.String r4 = r2.toString()
            android.util.Log.e(r3, r4, r1)
            vpadn.v r1 = new vpadn.v
            vpadn.v$a r3 = vpadn.C0024v.a.IO_EXCEPTION
            r1.<init>(r3, r2)
            r0 = r20
            r0.a(r1)
            goto L_0x017e
        L_0x0263:
            c.FileTransfer$b r3 = new c.FileTransfer$b
            r0 = r20
            r3.<init>(r7, r8, r0)
            java.util.HashMap<java.lang.String, c.FileTransfer$b> r2 = c.FileTransfer.a
            monitor-enter(r2)
            java.util.HashMap<java.lang.String, c.FileTransfer$b> r1 = c.FileTransfer.a     // Catch:{ all -> 0x0287 }
            r1.put(r4, r3)     // Catch:{ all -> 0x0287 }
            monitor-exit(r2)     // Catch:{ all -> 0x0287 }
            r0 = r17
            vpadn.p r1 = r0.cordova
            java.util.concurrent.ExecutorService r10 = r1.e()
            c.FileTransfer$4 r1 = new c.FileTransfer$4
            r2 = r17
            r1.<init>(r3, r4, r5, r6, r7, r8, r9)
            r10.execute(r1)
            goto L_0x017e
        L_0x0287:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x028a:
            java.lang.String r1 = "abort"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x02ec
            r1 = 0
            r0 = r19
            java.lang.String r1 = r0.getString(r1)
            java.util.HashMap<java.lang.String, c.FileTransfer$b> r2 = c.FileTransfer.a
            monitor-enter(r2)
            java.util.HashMap<java.lang.String, c.FileTransfer$b> r3 = c.FileTransfer.a     // Catch:{ all -> 0x02e6 }
            java.lang.Object r1 = r3.remove(r1)     // Catch:{ all -> 0x02e6 }
            c.FileTransfer$b r1 = (c.FileTransfer.b) r1     // Catch:{ all -> 0x02e6 }
            monitor-exit(r2)     // Catch:{ all -> 0x02e6 }
            if (r1 == 0) goto L_0x02e0
            java.io.File r2 = r1.f17c
            if (r2 == 0) goto L_0x02b0
            r2.delete()
        L_0x02b0:
            int r2 = c.FileTransfer.ABORTED_ERR
            java.lang.String r3 = r1.a
            java.lang.String r4 = r1.b
            r5 = -1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            org.json.JSONObject r2 = a(r2, r3, r4, r5)
            monitor-enter(r1)
            vpadn.v r3 = new vpadn.v     // Catch:{ all -> 0x02e9 }
            vpadn.v$a r4 = vpadn.C0024v.a.ERROR     // Catch:{ all -> 0x02e9 }
            r3.<init>(r4, r2)     // Catch:{ all -> 0x02e9 }
            r1.a(r3)     // Catch:{ all -> 0x02e9 }
            r2 = 1
            r1.f = r2     // Catch:{ all -> 0x02e9 }
            monitor-exit(r1)     // Catch:{ all -> 0x02e9 }
            r0 = r17
            vpadn.p r2 = r0.cordova
            java.util.concurrent.ExecutorService r2 = r2.e()
            c.FileTransfer$5 r3 = new c.FileTransfer$5
            r0 = r17
            r3.<init>(r0, r1)
            r2.execute(r3)
        L_0x02e0:
            r20.b()
            r1 = 1
            goto L_0x017f
        L_0x02e6:
            r1 = move-exception
            monitor-exit(r2)
            throw r1
        L_0x02e9:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x02ec:
            r1 = 0
            goto L_0x017f
        */
        throw new UnsupportedOperationException("Method not decompiled: c.FileTransfer.execute(java.lang.String, org.json.JSONArray, vpadn.o):boolean");
    }

    static /* synthetic */ void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    static /* synthetic */ InputStream a(URLConnection uRLConnection) throws IOException {
        return Build.VERSION.SDK_INT < 11 ? new a(uRLConnection.getInputStream()) : uRLConnection.getInputStream();
    }

    /* access modifiers changed from: private */
    public static SSLSocketFactory b(HttpsURLConnection httpsURLConnection) {
        SSLSocketFactory sSLSocketFactory = httpsURLConnection.getSSLSocketFactory();
        try {
            SSLContext instance = SSLContext.getInstance("TLS");
            instance.init(null, f14c, new SecureRandom());
            httpsURLConnection.setSSLSocketFactory(instance.getSocketFactory());
        } catch (Exception e) {
            Log.e("FileTransfer", e.getMessage(), e);
        }
        return sSLSocketFactory;
    }

    /* access modifiers changed from: private */
    public static JSONObject b(int i, String str, String str2, URLConnection uRLConnection) {
        int i2 = 0;
        if (uRLConnection != null) {
            try {
                if (uRLConnection instanceof HttpURLConnection) {
                    i2 = ((HttpURLConnection) uRLConnection).getResponseCode();
                }
            } catch (IOException e) {
                Log.w("FileTransfer", "Error getting HTTP status code from connection.", e);
            }
        }
        return a(i, str, str2, Integer.valueOf(i2));
    }

    private static JSONObject a(int i, String str, String str2, Integer num) {
        JSONException e;
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject();
            try {
                jSONObject.put("code", i);
                jSONObject.put("source", str);
                jSONObject.put("target", str2);
                if (num != null) {
                    jSONObject.put("http_status", num);
                }
            } catch (JSONException e2) {
                e = e2;
                Log.e("FileTransfer", e.getMessage(), e);
                return jSONObject;
            }
        } catch (JSONException e3) {
            JSONException jSONException = e3;
            jSONObject = null;
            e = jSONException;
            Log.e("FileTransfer", e.getMessage(), e);
            return jSONObject;
        }
        return jSONObject;
    }

    private static String a(JSONArray jSONArray, int i, String str) {
        String optString;
        return (jSONArray.length() < i || (optString = jSONArray.optString(i)) == null || "null".equals(optString)) ? str : optString;
    }

    static /* synthetic */ InputStream a(FileTransfer fileTransfer, String str) throws FileNotFoundException {
        if (str.startsWith("content:")) {
            return fileTransfer.cordova.a().getContentResolver().openInputStream(Uri.parse(str));
        } else if (!str.startsWith("file://")) {
            return new FileInputStream(str);
        } else {
            int indexOf = str.indexOf("?");
            return indexOf == -1 ? new FileInputStream(str.substring(7)) : new FileInputStream(str.substring(7, indexOf));
        }
    }

    static /* synthetic */ File b(FileTransfer fileTransfer, String str) throws FileNotFoundException {
        File file = str.startsWith("file://") ? new File(str.substring("file://".length())) : new File(str);
        if (file.getParent() != null) {
            return file;
        }
        throw new FileNotFoundException();
    }
}
