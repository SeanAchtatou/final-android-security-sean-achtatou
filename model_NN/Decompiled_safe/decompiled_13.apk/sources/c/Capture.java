package c;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0003a;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0020r;
import vpadn.C0024v;

public class Capture extends C0019q {
    private C0017o a;
    private long b;

    /* renamed from: c  reason: collision with root package name */
    private double f2c;
    private JSONArray d;
    private int e;

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        JSONObject jSONObject;
        this.a = oVar;
        this.b = 1;
        this.f2c = 0.0d;
        this.d = new JSONArray();
        JSONObject optJSONObject = jSONArray.optJSONObject(0);
        if (optJSONObject != null) {
            this.b = optJSONObject.optLong("limit", 1);
            this.f2c = optJSONObject.optDouble("duration", 0.0d);
        }
        if (str.equals("getFormatData")) {
            String string = jSONArray.getString(0);
            String string2 = jSONArray.getString(1);
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("height", 0);
            jSONObject2.put("width", 0);
            jSONObject2.put("bitrate", 0);
            jSONObject2.put("duration", 0);
            jSONObject2.put("codecs", "");
            if (string2 == null || string2.equals("") || "null".equals(string2)) {
                string2 = FileUtils.getMimeType(string);
            }
            Log.d("Capture", "Mime type = " + string2);
            if (string2.equals("image/jpeg") || string.endsWith(".jpg")) {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(FileUtils.stripFileProtocol(string), options);
                jSONObject2.put("height", options.outHeight);
                jSONObject2.put("width", options.outWidth);
                jSONObject = jSONObject2;
            } else {
                jSONObject = string2.endsWith("audio/3gpp") ? a(string, jSONObject2, false) : (string2.equals("video/3gpp") || string2.equals("video/mp4")) ? a(string, jSONObject2, true) : jSONObject2;
            }
            oVar.a(jSONObject);
            return true;
        }
        if (str.equals("captureAudio")) {
            a();
        } else if (str.equals("captureImage")) {
            b();
        } else if (!str.equals("captureVideo")) {
            return false;
        } else {
            double d2 = this.f2c;
            c();
        }
        return true;
    }

    private static JSONObject a(String str, JSONObject jSONObject, boolean z) throws JSONException {
        MediaPlayer mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(str);
            mediaPlayer.prepare();
            jSONObject.put("duration", mediaPlayer.getDuration() / 1000);
            if (z) {
                jSONObject.put("height", mediaPlayer.getVideoHeight());
                jSONObject.put("width", mediaPlayer.getVideoWidth());
            }
        } catch (IOException e2) {
            Log.d("Capture", "Error: loading video file");
        }
        return jSONObject;
    }

    private void a() {
        this.cordova.a(this, new Intent("android.provider.MediaStore.RECORD_SOUND"), 0);
    }

    private void b() {
        this.e = b(d()).getCount();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        intent.putExtra("output", Uri.fromFile(new File(C0003a.a(this.cordova.a()), "Capture.jpg")));
        this.cordova.a(this, intent, 1);
    }

    private void c() {
        this.cordova.a(this, new Intent("android.media.action.VIDEO_CAPTURE"), 2);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        Uri insert;
        if (i2 == -1) {
            if (i == 0) {
                this.d.put(a(intent.getData()));
                if (((long) this.d.length()) >= this.b) {
                    this.a.a(new C0024v(C0024v.a.OK, this.d));
                } else {
                    a();
                }
            } else if (i == 1) {
                try {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("mime_type", "image/jpeg");
                    try {
                        insert = this.cordova.a().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                    } catch (UnsupportedOperationException e2) {
                        C0020r.b("Capture", "Can't write to external media storage.");
                        try {
                            insert = this.cordova.a().getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, contentValues);
                        } catch (UnsupportedOperationException e3) {
                            C0020r.b("Capture", "Can't write to internal media storage.");
                            fail(a(0, "Error capturing image - no media storage found."));
                            return;
                        }
                    }
                    FileInputStream fileInputStream = new FileInputStream(String.valueOf(C0003a.a(this.cordova.a())) + "/Capture.jpg");
                    OutputStream openOutputStream = this.cordova.a().getContentResolver().openOutputStream(insert);
                    byte[] bArr = new byte[4096];
                    while (true) {
                        int read = fileInputStream.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        openOutputStream.write(bArr, 0, read);
                    }
                    openOutputStream.flush();
                    openOutputStream.close();
                    fileInputStream.close();
                    this.d.put(a(insert));
                    Uri d2 = d();
                    Cursor b2 = b(d2);
                    if (b2.getCount() - this.e == 2) {
                        b2.moveToLast();
                        this.cordova.a().getContentResolver().delete(Uri.parse(d2 + "/" + (Integer.valueOf(b2.getString(b2.getColumnIndex("_id"))).intValue() - 1)), null, null);
                    }
                    if (((long) this.d.length()) >= this.b) {
                        this.a.a(new C0024v(C0024v.a.OK, this.d));
                    } else {
                        b();
                    }
                } catch (IOException e4) {
                    e4.printStackTrace();
                    fail(a(0, "Error capturing image."));
                }
            } else if (i == 2) {
                this.d.put(a(intent.getData()));
                if (((long) this.d.length()) >= this.b) {
                    this.a.a(new C0024v(C0024v.a.OK, this.d));
                    return;
                }
                double d3 = this.f2c;
                c();
            }
        } else if (i2 == 0) {
            if (this.d.length() > 0) {
                this.a.a(new C0024v(C0024v.a.OK, this.d));
            } else {
                fail(a(3, "Canceled."));
            }
        } else if (this.d.length() > 0) {
            this.a.a(new C0024v(C0024v.a.OK, this.d));
        } else {
            fail(a(3, "Did not complete!"));
        }
    }

    private JSONObject a(Uri uri) {
        File file = new File(FileUtils.getRealPathFromURI(uri, this.cordova));
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", file.getName());
            jSONObject.put("fullPath", "file://" + file.getAbsolutePath());
            if (!file.getAbsoluteFile().toString().endsWith(".3gp") && !file.getAbsoluteFile().toString().endsWith(".3gpp")) {
                jSONObject.put(Globalization.TYPE, FileUtils.getMimeType(file.getAbsolutePath()));
            } else if (uri.toString().contains("/audio/")) {
                jSONObject.put(Globalization.TYPE, "audio/3gpp");
            } else {
                jSONObject.put(Globalization.TYPE, "video/3gpp");
            }
            jSONObject.put("lastModifiedDate", file.lastModified());
            jSONObject.put("size", file.length());
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    private static JSONObject a(int i, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("code", i);
            jSONObject.put("message", str);
        } catch (JSONException e2) {
        }
        return jSONObject;
    }

    public void fail(JSONObject jSONObject) {
        this.a.b(jSONObject);
    }

    private Cursor b(Uri uri) {
        return this.cordova.a().getContentResolver().query(uri, new String[]{"_id"}, null, null, null);
    }

    private static Uri d() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }
        return MediaStore.Images.Media.INTERNAL_CONTENT_URI;
    }
}
