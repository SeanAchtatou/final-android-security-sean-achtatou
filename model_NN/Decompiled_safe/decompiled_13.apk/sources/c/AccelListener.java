package c;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0024v;

public class AccelListener extends C0019q implements SensorEventListener {
    public static int ERROR_FAILED_TO_START = 3;
    public static int RUNNING = 2;
    public static int STARTING = 1;
    public static int STOPPED = 0;
    private float a = 0.0f;
    private float b = 0.0f;

    /* renamed from: c  reason: collision with root package name */
    private float f0c = 0.0f;
    private long d = 0;
    private int e = STOPPED;
    private int f = 0;
    private SensorManager g;
    private Sensor h;
    private C0017o i;

    public void initialize(C0018p pVar, CordovaWebView cordovaWebView) {
        super.initialize(pVar, cordovaWebView);
        this.g = (SensorManager) pVar.a().getSystemService("sensor");
    }

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) {
        if (str.equals("start")) {
            this.i = oVar;
            if (this.e != RUNNING) {
                if (this.e == RUNNING || this.e == STARTING) {
                    int i2 = this.e;
                } else {
                    this.e = STARTING;
                    List<Sensor> sensorList = this.g.getSensorList(1);
                    if (sensorList == null || sensorList.size() <= 0) {
                        this.e = ERROR_FAILED_TO_START;
                        a(ERROR_FAILED_TO_START, "No sensors found to register accelerometer listening to.");
                        int i3 = this.e;
                    } else {
                        this.h = sensorList.get(0);
                        this.g.registerListener(this, this.h, 2);
                        this.e = STARTING;
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            public final void run() {
                                AccelListener.a(AccelListener.this);
                            }
                        }, 2000);
                        int i4 = this.e;
                    }
                }
            }
        } else if (!str.equals("stop")) {
            return false;
        } else {
            if (this.e == RUNNING) {
                a();
            }
        }
        C0024v vVar = new C0024v(C0024v.a.NO_RESULT, "");
        vVar.a(true);
        oVar.a(vVar);
        return true;
    }

    public void onDestroy() {
        a();
    }

    private void a() {
        if (this.e != STOPPED) {
            this.g.unregisterListener(this);
        }
        this.e = STOPPED;
        this.f = 0;
    }

    static /* synthetic */ void a(AccelListener accelListener) {
        if (accelListener.e == STARTING) {
            accelListener.e = ERROR_FAILED_TO_START;
            accelListener.a(ERROR_FAILED_TO_START, "Accelerometer could not be started.");
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i2) {
        if (sensor.getType() == 1 && this.e != STOPPED) {
            this.f = i2;
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 1 && this.e != STOPPED) {
            this.e = RUNNING;
            if (this.f >= 2) {
                this.d = System.currentTimeMillis();
                this.a = sensorEvent.values[0];
                this.b = sensorEvent.values[1];
                this.f0c = sensorEvent.values[2];
                C0024v vVar = new C0024v(C0024v.a.OK, b());
                vVar.a(true);
                this.i.a(vVar);
            }
        }
    }

    public void onReset() {
        if (this.e == RUNNING) {
            a();
        }
    }

    private void a(int i2, String str) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("code", i2);
            jSONObject.put("message", str);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        C0024v vVar = new C0024v(C0024v.a.ERROR, jSONObject);
        vVar.a(true);
        this.i.a(vVar);
    }

    private JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("x", (double) this.a);
            jSONObject.put("y", (double) this.b);
            jSONObject.put("z", (double) this.f0c);
            jSONObject.put("timestamp", this.d);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }
}
