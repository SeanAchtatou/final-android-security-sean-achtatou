package c;

import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.channels.FileChannel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.A;
import vpadn.C0003a;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0024v;
import vpadn.C0025w;
import vpadn.C0026x;
import vpadn.C0027y;
import vpadn.R;
import vpadn.z;

public class FileUtils extends C0019q {
    public static int ABORT_ERR = 3;
    public static int APPLICATION = 3;
    public static int ENCODING_ERR = 5;
    public static int INVALID_MODIFICATION_ERR = 9;
    public static int INVALID_STATE_ERR = 7;
    public static int NOT_FOUND_ERR = 1;
    public static int NOT_READABLE_ERR = 4;
    public static int NO_MODIFICATION_ALLOWED_ERR = 6;
    public static int PATH_EXISTS_ERR = 12;
    public static int PERSISTENT = 1;
    public static int QUOTA_EXCEEDED_ERR = 10;
    public static int RESOURCE = 2;
    public static int SECURITY_ERR = 2;
    public static int SYNTAX_ERR = 8;
    public static int TEMPORARY = 0;
    public static int TYPE_MISMATCH_ERR = 11;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.FileUtils.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):org.json.JSONObject
     arg types: [java.lang.String, java.lang.String, org.json.JSONObject, int]
     candidates:
      c.FileUtils.a(java.lang.String, java.lang.String, java.lang.String, boolean):org.json.JSONObject
      c.FileUtils.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):org.json.JSONObject */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.FileUtils.a(java.lang.String, java.lang.String, java.lang.String, boolean):org.json.JSONObject
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      c.FileUtils.a(java.lang.String, java.lang.String, org.json.JSONObject, boolean):org.json.JSONObject
      c.FileUtils.a(java.lang.String, java.lang.String, java.lang.String, boolean):org.json.JSONObject */
    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        File file;
        int i;
        int i2;
        int i3;
        int i4;
        boolean z = false;
        try {
            if (str.equals("testSaveLocationExists")) {
                oVar.a(new C0024v(C0024v.a.OK, C0003a.a()));
            } else if (str.equals("getFreeDiskSpace")) {
                oVar.a(new C0024v(C0024v.a.OK, (float) C0003a.a(false)));
            } else if (str.equals("testFileExists")) {
                oVar.a(new C0024v(C0024v.a.OK, C0003a.a(jSONArray.getString(0))));
            } else if (str.equals("testDirectoryExists")) {
                oVar.a(new C0024v(C0024v.a.OK, C0003a.a(jSONArray.getString(0))));
            } else if (str.equals("readAsText")) {
                if (jSONArray.length() >= 3) {
                    i3 = jSONArray.getInt(2);
                } else {
                    i3 = 0;
                }
                if (jSONArray.length() >= 4) {
                    i4 = jSONArray.getInt(3);
                } else {
                    i4 = Integer.MAX_VALUE;
                }
                oVar.a(new C0024v(C0024v.a.OK, readAsText(jSONArray.getString(0), jSONArray.getString(1), i3, i4)));
            } else if (str.equals("readAsDataURL")) {
                if (jSONArray.length() >= 2) {
                    i = jSONArray.getInt(1);
                } else {
                    i = 0;
                }
                if (jSONArray.length() >= 3) {
                    i2 = jSONArray.getInt(2);
                } else {
                    i2 = Integer.MAX_VALUE;
                }
                oVar.a(new C0024v(C0024v.a.OK, readAsDataURL(jSONArray.getString(0), i, i2)));
            } else if (str.equals("write")) {
                oVar.a(new C0024v(C0024v.a.OK, (float) write(jSONArray.getString(0), jSONArray.getString(1), jSONArray.getInt(2))));
            } else if (str.equals("truncate")) {
                oVar.a(new C0024v(C0024v.a.OK, (float) a(jSONArray.getString(0), jSONArray.getLong(1))));
            } else if (str.equals("requestFileSystem")) {
                long optLong = jSONArray.optLong(1);
                if (optLong == 0 || optLong <= C0003a.a(true) * 1024) {
                    int i5 = jSONArray.getInt(0);
                    JSONObject jSONObject = new JSONObject();
                    if (i5 == TEMPORARY) {
                        jSONObject.put("name", "temporary");
                        if (Environment.getExternalStorageState().equals("mounted")) {
                            new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/" + this.cordova.a().getPackageName() + "/cache/").mkdirs();
                            jSONObject.put("root", e(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/" + this.cordova.a().getPackageName() + "/cache/"));
                        } else {
                            new File("/data/data/" + this.cordova.a().getPackageName() + "/cache/").mkdirs();
                            jSONObject.put("root", e("/data/data/" + this.cordova.a().getPackageName() + "/cache/"));
                        }
                    } else if (i5 == PERSISTENT) {
                        jSONObject.put("name", "persistent");
                        if (Environment.getExternalStorageState().equals("mounted")) {
                            jSONObject.put("root", getEntry(Environment.getExternalStorageDirectory()));
                        } else {
                            jSONObject.put("root", e("/data/data/" + this.cordova.a().getPackageName()));
                        }
                    } else {
                        throw new IOException("No filesystem of type requested");
                    }
                    oVar.a(jSONObject);
                } else {
                    oVar.a(new C0024v(C0024v.a.ERROR, QUOTA_EXCEEDED_ERR));
                }
            } else if (str.equals("resolveLocalFileSystemURI")) {
                String decode = URLDecoder.decode(jSONArray.getString(0), "UTF-8");
                if (decode.startsWith("content:")) {
                    Cursor managedQuery = this.cordova.a().managedQuery(Uri.parse(decode), new String[]{"_data"}, null, null, null);
                    int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
                    managedQuery.moveToFirst();
                    file = new File(managedQuery.getString(columnIndexOrThrow));
                } else {
                    new URL(decode);
                    if (decode.startsWith("file://")) {
                        int indexOf = decode.indexOf("?");
                        file = indexOf < 0 ? new File(decode.substring(7, decode.length())) : new File(decode.substring(7, indexOf));
                    } else {
                        file = new File(decode);
                    }
                }
                if (!file.exists()) {
                    throw new FileNotFoundException();
                } else if (!file.canRead()) {
                    throw new IOException();
                } else {
                    oVar.a(getEntry(file));
                }
            } else if (str.equals("getMetadata")) {
                C0024v.a aVar = C0024v.a.OK;
                File d = d(jSONArray.getString(0));
                if (!d.exists()) {
                    throw new FileNotFoundException("Failed to find file in getMetadata");
                }
                oVar.a(new C0024v(aVar, (float) d.lastModified()));
            } else if (str.equals("getFileMetadata")) {
                String string = jSONArray.getString(0);
                File d2 = d(string);
                if (!d2.exists()) {
                    throw new FileNotFoundException("File: " + string + " does not exist.");
                }
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("size", d2.length());
                jSONObject2.put(Globalization.TYPE, getMimeType(string));
                jSONObject2.put("name", d2.getName());
                jSONObject2.put("fullPath", string);
                jSONObject2.put("lastModifiedDate", d2.lastModified());
                oVar.a(jSONObject2);
            } else if (str.equals("getParent")) {
                String realPathFromURI = getRealPathFromURI(Uri.parse(jSONArray.getString(0)), this.cordova);
                oVar.a(c(realPathFromURI) ? e(realPathFromURI) : e(new File(realPathFromURI).getParent()));
            } else if (str.equals("getDirectory")) {
                oVar.a(a(jSONArray.getString(0), jSONArray.getString(1), jSONArray.optJSONObject(2), true));
            } else if (str.equals("getFile")) {
                oVar.a(a(jSONArray.getString(0), jSONArray.getString(1), jSONArray.optJSONObject(2), false));
            } else if (str.equals("remove")) {
                String string2 = jSONArray.getString(0);
                File d3 = d(string2);
                if (c(string2)) {
                    throw new z("You can't delete the root directory");
                } else if (d3.isDirectory() && d3.list().length > 0) {
                    throw new C0027y("You can't delete a directory that is not empty.");
                } else if (d3.delete()) {
                    a(jSONArray.getString(0));
                    oVar.b();
                } else {
                    oVar.a(NO_MODIFICATION_ALLOWED_ERR);
                }
            } else if (str.equals("removeRecursively")) {
                String string3 = jSONArray.getString(0);
                File d4 = d(string3);
                if (!c(string3)) {
                    z = a(d4);
                }
                if (z) {
                    oVar.b();
                } else {
                    oVar.a(NO_MODIFICATION_ALLOWED_ERR);
                }
            } else if (str.equals("moveTo")) {
                oVar.a(a(jSONArray.getString(0), jSONArray.getString(1), jSONArray.getString(2), true));
            } else if (str.equals("copyTo")) {
                oVar.a(a(jSONArray.getString(0), jSONArray.getString(1), jSONArray.getString(2), false));
            } else if (!str.equals("readEntries")) {
                return false;
            } else {
                oVar.a(new C0024v(C0024v.a.OK, b(jSONArray.getString(0))));
            }
        } catch (FileNotFoundException e) {
            oVar.a(NOT_FOUND_ERR);
        } catch (C0026x e2) {
            oVar.a(PATH_EXISTS_ERR);
        } catch (z e3) {
            oVar.a(NO_MODIFICATION_ALLOWED_ERR);
        } catch (C0027y e4) {
            oVar.a(INVALID_MODIFICATION_ERR);
        } catch (MalformedURLException e5) {
            oVar.a(ENCODING_ERR);
        } catch (IOException e6) {
            oVar.a(INVALID_MODIFICATION_ERR);
        } catch (C0025w e7) {
            oVar.a(ENCODING_ERR);
        } catch (A e8) {
            oVar.a(TYPE_MISMATCH_ERR);
        }
        return true;
    }

    private void a(String str) {
        String realPathFromURI = getRealPathFromURI(Uri.parse(str), this.cordova);
        try {
            this.cordova.a().getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "_data = ?", new String[]{realPathFromURI});
        } catch (UnsupportedOperationException e) {
        }
    }

    private JSONArray b(String str) throws FileNotFoundException, JSONException {
        File d = d(str);
        if (!d.exists()) {
            throw new FileNotFoundException();
        }
        JSONArray jSONArray = new JSONArray();
        if (d.isDirectory()) {
            File[] listFiles = d.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].canRead()) {
                    jSONArray.put(getEntry(listFiles[i]));
                }
            }
        }
        return jSONArray;
    }

    private JSONObject a(String str, String str2, String str3, boolean z) throws JSONException, z, IOException, C0027y, C0025w, C0026x {
        String realPathFromURI = getRealPathFromURI(Uri.parse(str), this.cordova);
        String realPathFromURI2 = getRealPathFromURI(Uri.parse(str2), this.cordova);
        if (str3 == null || !str3.contains(":")) {
            File file = new File(realPathFromURI);
            if (!file.exists()) {
                throw new FileNotFoundException("The source does not exist");
            }
            File file2 = new File(realPathFromURI2);
            if (!file2.exists()) {
                throw new FileNotFoundException("The source does not exist");
            }
            if ("null".equals(str3) || "".equals(str3)) {
                str3 = null;
            }
            File file3 = str3 != null ? new File(String.valueOf(file2.getAbsolutePath()) + File.separator + str3) : new File(String.valueOf(file2.getAbsolutePath()) + File.separator + file.getName());
            if (file.getAbsolutePath().equals(file3.getAbsolutePath())) {
                throw new C0027y("Can't copy a file onto itself");
            } else if (file.isDirectory()) {
                if (!z) {
                    return c(file, file3);
                }
                if (file3.exists() && file3.isFile()) {
                    throw new C0027y("Can't rename a file to a directory");
                } else if (a(file.getAbsolutePath(), file3.getAbsolutePath())) {
                    throw new C0027y("Can't move itself into itself");
                } else if (!file3.exists() || file3.list().length <= 0) {
                    if (!file.renameTo(file3)) {
                        c(file, file3);
                        if (file3.exists()) {
                            a(file);
                        } else {
                            throw new IOException("moved failed");
                        }
                    }
                    return getEntry(file3);
                } else {
                    throw new C0027y("directory is not empty");
                }
            } else if (!z) {
                return a(file, file3);
            } else {
                if (!file3.exists() || !file3.isDirectory()) {
                    if (!file.renameTo(file3)) {
                        b(file, file3);
                        if (file3.exists()) {
                            file.delete();
                        } else {
                            throw new IOException("moved failed");
                        }
                    }
                    JSONObject entry = getEntry(file3);
                    if (!str.startsWith("content://")) {
                        return entry;
                    }
                    a(str);
                    return entry;
                }
                throw new C0027y("Can't rename a file to a directory");
            }
        } else {
            throw new C0025w("Bad file name");
        }
    }

    private JSONObject a(File file, File file2) throws IOException, C0027y, JSONException {
        if (!file2.exists() || !file2.isDirectory()) {
            b(file, file2);
            return getEntry(file2);
        }
        throw new C0027y("Can't rename a file to a directory");
    }

    private static void b(File file, File file2) throws FileNotFoundException, IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        FileOutputStream fileOutputStream = new FileOutputStream(file2);
        FileChannel channel = fileInputStream.getChannel();
        FileChannel channel2 = fileOutputStream.getChannel();
        try {
            channel.transferTo(0, channel.size(), channel2);
        } finally {
            fileInputStream.close();
            fileOutputStream.close();
            channel.close();
            channel2.close();
        }
    }

    private JSONObject c(File file, File file2) throws JSONException, IOException, z, C0027y {
        if (file2.exists() && file2.isFile()) {
            throw new C0027y("Can't rename a file to a directory");
        } else if (a(file.getAbsolutePath(), file2.getAbsolutePath())) {
            throw new C0027y("Can't copy itself into itself");
        } else if (file2.exists() || file2.mkdir()) {
            for (File file3 : file.listFiles()) {
                if (file3.isDirectory()) {
                    c(file3, file2);
                } else {
                    a(file3, new File(file2.getAbsoluteFile() + File.separator + file3.getName()));
                }
            }
            return getEntry(file2);
        } else {
            throw new z("Couldn't create the destination directory");
        }
    }

    private static boolean a(String str, String str2) {
        if (!str2.startsWith(str) || str2.indexOf(File.separator, str.length() - 1) == -1) {
            return false;
        }
        return true;
    }

    private boolean a(File file) throws C0026x {
        if (file.isDirectory()) {
            for (File a : file.listFiles()) {
                a(a);
            }
        }
        if (file.delete()) {
            return true;
        }
        throw new C0026x("could not delete: " + file.getName());
    }

    private JSONObject a(String str, String str2, JSONObject jSONObject, boolean z) throws C0026x, IOException, A, C0025w, JSONException {
        boolean z2;
        boolean z3 = false;
        if (jSONObject != null) {
            z2 = jSONObject.optBoolean("create");
            if (z2) {
                z3 = jSONObject.optBoolean("exclusive");
            }
        } else {
            z2 = false;
        }
        if (str2.contains(":")) {
            throw new C0025w("This file has a : in it's name");
        }
        File file = str2.startsWith("/") ? new File(str2) : new File(String.valueOf(getRealPathFromURI(Uri.parse(str), this.cordova)) + File.separator + str2);
        if (z2) {
            if (!z3 || !file.exists()) {
                if (z) {
                    file.mkdir();
                } else {
                    file.createNewFile();
                }
                if (!file.exists()) {
                    throw new C0026x("create fails");
                }
            } else {
                throw new C0026x("create/exclusive fails");
            }
        } else if (!file.exists()) {
            throw new FileNotFoundException("path does not exist");
        } else if (z) {
            if (file.isFile()) {
                throw new A("path doesn't exist or is file");
            }
        } else if (file.isDirectory()) {
            throw new A("path doesn't exist or is directory");
        }
        return getEntry(file);
    }

    private boolean c(String str) {
        String realPathFromURI = getRealPathFromURI(Uri.parse(str), this.cordova);
        if (realPathFromURI.equals(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/" + this.cordova.a().getPackageName() + "/cache") || realPathFromURI.equals(Environment.getExternalStorageDirectory().getAbsolutePath()) || realPathFromURI.equals("/data/data/" + this.cordova.a().getPackageName())) {
            return true;
        }
        return false;
    }

    public static String stripFileProtocol(String str) {
        if (str.startsWith("file://")) {
            return str.substring(7);
        }
        return str;
    }

    private File d(String str) {
        return new File(getRealPathFromURI(Uri.parse(str), this.cordova));
    }

    public JSONObject getEntry(File file) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("isFile", file.isFile());
        jSONObject.put("isDirectory", file.isDirectory());
        jSONObject.put("name", file.getName());
        jSONObject.put("fullPath", "file://" + file.getAbsolutePath());
        return jSONObject;
    }

    private JSONObject e(String str) throws JSONException {
        return getEntry(new File(str));
    }

    public boolean isSynch(String str) {
        if (!str.equals("testSaveLocationExists") && !str.equals("getFreeDiskSpace") && !str.equals("testFileExists") && !str.equals("testDirectoryExists")) {
            return false;
        }
        return true;
    }

    public String readAsText(String str, String str2, int i, int i2) throws FileNotFoundException, IOException {
        int i3 = i2 - i;
        byte[] bArr = new byte[1000];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(f(str), 1024);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (i > 0) {
            bufferedInputStream.skip((long) i);
        }
        while (i3 > 0) {
            int read = bufferedInputStream.read(bArr, 0, Math.min(1000, i3));
            if (read < 0) {
                break;
            }
            i3 -= read;
            byteArrayOutputStream.write(bArr, 0, read);
        }
        return new String(byteArrayOutputStream.toByteArray(), str2);
    }

    public String readAsDataURL(String str, int i, int i2) throws FileNotFoundException, IOException {
        String mimeType;
        int i3 = i2 - i;
        byte[] bArr = new byte[1000];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(f(str), 1024);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (i > 0) {
            bufferedInputStream.skip((long) i);
        }
        while (i3 > 0) {
            int read = bufferedInputStream.read(bArr, 0, Math.min(1000, i3));
            if (read < 0) {
                break;
            }
            i3 -= read;
            byteArrayOutputStream.write(bArr, 0, read);
        }
        if (str.startsWith("content:")) {
            mimeType = this.cordova.a().getContentResolver().getType(Uri.parse(str));
        } else {
            mimeType = getMimeType(str);
        }
        return "data:" + mimeType + ";base64," + new String(R.a(byteArrayOutputStream.toByteArray(), true));
    }

    public static String getMimeType(String str) {
        if (str == null) {
            return "";
        }
        String lowerCase = str.replace(" ", "%20").toLowerCase();
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(lowerCase);
        if (fileExtensionFromUrl.toLowerCase().equals("3ga")) {
            return "audio/3gpp";
        }
        return singleton.getMimeTypeFromExtension(fileExtensionFromUrl);
    }

    public long write(String str, String str2, int i) throws FileNotFoundException, IOException, z {
        boolean z;
        if (str.startsWith("content://")) {
            throw new z("Couldn't write to file given its content URI");
        }
        String realPathFromURI = getRealPathFromURI(Uri.parse(str), this.cordova);
        if (i > 0) {
            a(realPathFromURI, (long) i);
            z = true;
        } else {
            z = false;
        }
        byte[] bytes = str2.getBytes();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        FileOutputStream fileOutputStream = new FileOutputStream(realPathFromURI, z);
        byte[] bArr = new byte[bytes.length];
        byteArrayInputStream.read(bArr, 0, bArr.length);
        fileOutputStream.write(bArr, 0, bytes.length);
        fileOutputStream.flush();
        fileOutputStream.close();
        return (long) bytes.length;
    }

    private long a(String str, long j) throws FileNotFoundException, IOException, z {
        if (str.startsWith("content://")) {
            throw new z("Couldn't truncate file given its content URI");
        }
        RandomAccessFile randomAccessFile = new RandomAccessFile(getRealPathFromURI(Uri.parse(str), this.cordova), "rw");
        try {
            if (randomAccessFile.length() >= j) {
                randomAccessFile.getChannel().truncate(j);
            } else {
                j = randomAccessFile.length();
                randomAccessFile.close();
            }
            return j;
        } finally {
            randomAccessFile.close();
        }
    }

    private InputStream f(String str) throws FileNotFoundException {
        if (!str.startsWith("content")) {
            return new FileInputStream(getRealPathFromURI(Uri.parse(str), this.cordova));
        }
        return this.cordova.a().getContentResolver().openInputStream(Uri.parse(str));
    }

    protected static String getRealPathFromURI(Uri uri, C0018p pVar) {
        String scheme = uri.getScheme();
        if (scheme == null) {
            return uri.toString();
        }
        if (scheme.compareTo("content") == 0) {
            Cursor managedQuery = pVar.a().managedQuery(uri, new String[]{"_data"}, null, null, null);
            int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow("_data");
            managedQuery.moveToFirst();
            return managedQuery.getString(columnIndexOrThrow);
        } else if (scheme.compareTo("file") == 0) {
            return uri.getPath();
        } else {
            return uri.toString();
        }
    }
}
