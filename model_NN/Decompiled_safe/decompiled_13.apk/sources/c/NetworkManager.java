package c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import org.json.JSONArray;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0024v;

public class NetworkManager extends C0019q {
    public static final String CDMA = "cdma";
    public static final String EDGE = "edge";
    public static final String EHRPD = "ehrpd";
    public static final String GPRS = "gprs";
    public static final String GSM = "gsm";
    public static final String HSDPA = "hsdpa";
    public static final String HSPA = "hspa";
    public static final String HSPA_PLUS = "hspa+";
    public static final String HSUPA = "hsupa";
    public static final String LTE = "lte";
    public static final String MOBILE = "mobile";
    public static int NOT_REACHABLE = 0;
    public static final String ONEXRTT = "1xrtt";
    public static int REACHABLE_VIA_CARRIER_DATA_NETWORK = 1;
    public static int REACHABLE_VIA_WIFI_NETWORK = 2;
    public static final String TYPE_2G = "2g";
    public static final String TYPE_3G = "3g";
    public static final String TYPE_4G = "4g";
    public static final String TYPE_ETHERNET = "ethernet";
    public static final String TYPE_NONE = "none";
    public static final String TYPE_UNKNOWN = "unknown";
    public static final String TYPE_WIFI = "wifi";
    public static final String UMB = "umb";
    public static final String UMTS = "umts";
    public static final String WIFI = "wifi";
    public static final String WIMAX = "wimax";
    ConnectivityManager a;
    private C0017o b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f22c = false;
    private BroadcastReceiver d = null;
    private String e = "";

    public void initialize(C0018p pVar, CordovaWebView cordovaWebView) {
        super.initialize(pVar, cordovaWebView);
        this.a = (ConnectivityManager) pVar.a().getSystemService("connectivity");
        this.b = null;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        if (this.d == null) {
            this.d = new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    if (NetworkManager.this.webView != null) {
                        NetworkManager.a(NetworkManager.this, NetworkManager.this.a.getActiveNetworkInfo());
                    }
                }
            };
            try {
                pVar.a().registerReceiver(this.d, intentFilter);
                this.f22c = true;
            } catch (Exception e2) {
                Log.e("VPON", "NetworkManager.initialize method throw Exception:" + e2.getMessage(), e2);
                this.f22c = false;
            }
        }
    }

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) {
        if (!str.equals("getConnectionInfo")) {
            return false;
        }
        this.b = oVar;
        C0024v vVar = new C0024v(C0024v.a.OK, a(this.a.getActiveNetworkInfo()));
        vVar.a(true);
        oVar.a(vVar);
        return true;
    }

    public void onDestroy() {
        if (this.d != null && this.f22c) {
            try {
                this.cordova.a().unregisterReceiver(this.d);
                this.f22c = false;
            } catch (Exception e2) {
                Log.e("NetworkManager", "Error unregistering network receiver: " + e2.getMessage(), e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(vpadn.v, java.lang.String):void
      c.CordovaWebView.a(boolean, boolean):void
      c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
    static /* synthetic */ void a(NetworkManager networkManager, NetworkInfo networkInfo) {
        String a2 = networkManager.a(networkInfo);
        if (!a2.equals(networkManager.e)) {
            if (networkManager.b != null) {
                C0024v vVar = new C0024v(C0024v.a.OK, a2);
                vVar.a(true);
                networkManager.b.a(vVar);
            }
            networkManager.webView.a("networkconnection", (Object) a2);
            networkManager.e = a2;
        }
    }

    private String a(NetworkInfo networkInfo) {
        String str = TYPE_NONE;
        if (networkInfo != null) {
            if (!networkInfo.isConnected()) {
                str = TYPE_NONE;
            } else if (networkInfo != null) {
                String typeName = networkInfo.getTypeName();
                if (typeName.toLowerCase().equals("wifi")) {
                    str = "wifi";
                } else {
                    if (typeName.toLowerCase().equals(MOBILE)) {
                        String subtypeName = networkInfo.getSubtypeName();
                        if (subtypeName.toLowerCase().equals(GSM) || subtypeName.toLowerCase().equals(GPRS) || subtypeName.toLowerCase().equals(EDGE)) {
                            str = TYPE_2G;
                        } else if (subtypeName.toLowerCase().startsWith(CDMA) || subtypeName.toLowerCase().equals(UMTS) || subtypeName.toLowerCase().equals(ONEXRTT) || subtypeName.toLowerCase().equals(EHRPD) || subtypeName.toLowerCase().equals(HSUPA) || subtypeName.toLowerCase().equals(HSDPA) || subtypeName.toLowerCase().equals(HSPA)) {
                            str = TYPE_3G;
                        } else if (subtypeName.toLowerCase().equals(LTE) || subtypeName.toLowerCase().equals(UMB) || subtypeName.toLowerCase().equals(HSPA_PLUS)) {
                            str = TYPE_4G;
                        }
                    }
                    str = TYPE_UNKNOWN;
                }
            } else {
                str = TYPE_NONE;
            }
        }
        Log.d("CordovaNetworkManager", "Connection Type: " + str);
        return str;
    }
}
