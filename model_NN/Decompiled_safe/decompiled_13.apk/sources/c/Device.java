package c;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0017o;
import vpadn.C0018p;
import vpadn.C0019q;
import vpadn.C0020r;

public class Device extends C0019q {
    public static final String TAG = "Device";
    public static String cordovaVersion = "2.5.0";
    public static String platform = "Android";
    public static String uuid;
    private BroadcastReceiver a = null;

    public void initialize(C0018p pVar, CordovaWebView cordovaWebView) {
        super.initialize(pVar, cordovaWebView);
        uuid = getUuid();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PHONE_STATE");
        this.a = new BroadcastReceiver() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
             arg types: [java.lang.String, java.lang.String]
             candidates:
              c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
              c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
              c.CordovaWebView.a(vpadn.v, java.lang.String):void
              c.CordovaWebView.a(boolean, boolean):void
              c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
            public final void onReceive(Context context, Intent intent) {
                if (intent != null && intent.getAction().equals("android.intent.action.PHONE_STATE") && intent.hasExtra("state")) {
                    String stringExtra = intent.getStringExtra("state");
                    if (stringExtra.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                        C0020r.c(Device.TAG, "Telephone RINGING");
                        Device.this.webView.a("telephone", (Object) "ringing");
                    } else if (stringExtra.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                        C0020r.c(Device.TAG, "Telephone OFFHOOK");
                        Device.this.webView.a("telephone", (Object) "offhook");
                    } else if (stringExtra.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                        C0020r.c(Device.TAG, "Telephone IDLE");
                        Device.this.webView.a("telephone", (Object) "idle");
                    }
                }
            }
        };
        try {
            this.cordova.a().registerReceiver(this.a, intentFilter);
        } catch (Exception e) {
            C0020r.b("VPON", "initTelephonyReceiver throw Exception:" + e.getMessage(), e);
        }
    }

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        if (!str.equals("getDeviceInfo")) {
            return false;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("uuid", uuid);
        jSONObject.put("version", getOSVersion());
        jSONObject.put("platform", platform);
        jSONObject.put("name", getProductName());
        jSONObject.put("cordova", cordovaVersion);
        jSONObject.put("model", getModel());
        oVar.a(jSONObject);
        return true;
    }

    public void onDestroy() {
        try {
            this.cordova.a().unregisterReceiver(this.a);
        } catch (Exception e) {
            C0020r.a(TAG, "Device onDestroy throw Exception:" + e.getMessage(), e);
        }
    }

    public String getPlatform() {
        return platform;
    }

    public String getUuid() {
        return Settings.Secure.getString(this.cordova.a().getContentResolver(), "android_id");
    }

    public String getCordovaVersion() {
        return cordovaVersion;
    }

    public String getModel() {
        return Build.MODEL;
    }

    public String getProductName() {
        return Build.PRODUCT;
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public String getTimeZoneID() {
        return TimeZone.getDefault().getID();
    }
}
