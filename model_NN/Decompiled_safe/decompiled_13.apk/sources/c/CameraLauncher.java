package c;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import vpadn.C0003a;
import vpadn.C0009g;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0020r;
import vpadn.C0024v;
import vpadn.R;

public class CameraLauncher extends C0019q implements MediaScannerConnection.MediaScannerConnectionClient {
    private int a;
    private int b;

    /* renamed from: c  reason: collision with root package name */
    private int f1c;
    public C0017o callbackContext;
    private Uri d;
    private int e;
    private int f;
    private boolean g;
    private boolean h;
    private int i;
    private MediaScannerConnection j;
    private Uri k;

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        this.callbackContext = oVar;
        if (!str.equals("takePicture")) {
            return false;
        }
        this.g = false;
        this.f1c = 0;
        this.b = 0;
        this.e = 0;
        this.f = 0;
        this.a = 80;
        this.a = jSONArray.getInt(0);
        int i2 = jSONArray.getInt(1);
        int i3 = jSONArray.getInt(2);
        this.b = jSONArray.getInt(3);
        this.f1c = jSONArray.getInt(4);
        this.e = jSONArray.getInt(5);
        this.f = jSONArray.getInt(6);
        this.h = jSONArray.getBoolean(8);
        this.g = jSONArray.getBoolean(9);
        if (this.b <= 0) {
            this.b = -1;
        }
        if (this.f1c <= 0) {
            this.f1c = -1;
        }
        if (i3 == 1) {
            takePicture(i2, this.e);
        } else if (i3 == 0 || i3 == 2) {
            getImage(i3, i2);
        }
        C0024v vVar = new C0024v(C0024v.a.NO_RESULT);
        vVar.a(true);
        oVar.a(vVar);
        return true;
    }

    public void takePicture(int i2, int i3) {
        File file;
        this.i = b(b()).getCount();
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (i3 == 0) {
            file = new File(C0003a.a(this.cordova.a()), ".Pic.jpg");
        } else if (i3 == 1) {
            file = new File(C0003a.a(this.cordova.a()), ".Pic.png");
        } else {
            throw new IllegalArgumentException("Invalid Encoding Type: " + i3);
        }
        intent.putExtra("output", Uri.fromFile(file));
        this.d = Uri.fromFile(file);
        if (this.cordova != null) {
            this.cordova.a(this, intent, i2 + 32 + 1);
        }
    }

    public void getImage(int i2, int i3) {
        Intent intent = new Intent();
        String str = "Get Picture";
        if (this.f == 0) {
            intent.setType("image/*");
        } else if (this.f == 1) {
            intent.setType("video/*");
            str = "Get Video";
        } else if (this.f == 2) {
            intent.setType("*/*");
            str = "Get All";
        }
        intent.setAction("android.intent.action.GET_CONTENT");
        intent.addCategory("android.intent.category.OPENABLE");
        if (this.cordova != null) {
            this.cordova.a(this, Intent.createChooser(intent, new String(str)), ((i2 + 1) * 16) + i3 + 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0264  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0271  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r12, int r13, android.content.Intent r14) {
        /*
            r11 = this;
            int r0 = r12 / 16
            int r0 = r0 + -1
            int r1 = r12 % 16
            int r8 = r1 + -1
            r6 = 0
            r1 = 1
            if (r0 != r1) goto L_0x019d
            r0 = -1
            if (r13 != r0) goto L_0x018d
            vpadn.g r3 = new vpadn.g     // Catch:{ IOException -> 0x0073 }
            r3.<init>()     // Catch:{ IOException -> 0x0073 }
            int r0 = r11.e     // Catch:{ IOException -> 0x006e }
            if (r0 != 0) goto L_0x003f
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x006e }
            vpadn.p r1 = r11.cordova     // Catch:{ IOException -> 0x006e }
            android.app.Activity r1 = r1.a()     // Catch:{ IOException -> 0x006e }
            java.lang.String r1 = vpadn.C0003a.a(r1)     // Catch:{ IOException -> 0x006e }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ IOException -> 0x006e }
            r0.<init>(r1)     // Catch:{ IOException -> 0x006e }
            java.lang.String r1 = "/.Pic.jpg"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ IOException -> 0x006e }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x006e }
            r3.a(r0)     // Catch:{ IOException -> 0x006e }
            r3.a()     // Catch:{ IOException -> 0x006e }
            int r6 = r3.c()     // Catch:{ IOException -> 0x006e }
        L_0x003f:
            r2 = 0
            r1 = 0
            if (r8 != 0) goto L_0x00d7
            android.net.Uri r0 = r11.d     // Catch:{ IOException -> 0x0073 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x0073 }
            java.lang.String r0 = c.FileUtils.stripFileProtocol(r0)     // Catch:{ IOException -> 0x0073 }
            android.graphics.Bitmap r0 = r11.a(r0)     // Catch:{ IOException -> 0x0073 }
            if (r0 != 0) goto L_0x005f
            android.os.Bundle r0 = r14.getExtras()     // Catch:{ IOException -> 0x0073 }
            java.lang.String r2 = "data"
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IOException -> 0x0073 }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ IOException -> 0x0073 }
        L_0x005f:
            if (r0 != 0) goto L_0x007d
            java.lang.String r0 = "CameraLauncher"
            java.lang.String r1 = "I either have a null image path or bitmap"
            android.util.Log.d(r0, r1)     // Catch:{ IOException -> 0x0073 }
            java.lang.String r0 = "Unable to create bitmap!"
            r11.failPicture(r0)     // Catch:{ IOException -> 0x0073 }
        L_0x006d:
            return
        L_0x006e:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ IOException -> 0x0073 }
            goto L_0x003f
        L_0x0073:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = "Error capturing image."
            r11.failPicture(r0)
            goto L_0x006d
        L_0x007d:
            if (r6 == 0) goto L_0x0087
            boolean r2 = r11.h     // Catch:{ IOException -> 0x0073 }
            if (r2 == 0) goto L_0x0087
            android.graphics.Bitmap r0 = a(r6, r0, r3)     // Catch:{ IOException -> 0x0073 }
        L_0x0087:
            r11.processPicture(r0)     // Catch:{ IOException -> 0x0073 }
            r2 = 0
            r11.a(r2)     // Catch:{ IOException -> 0x0073 }
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0091:
            android.net.Uri r2 = r11.d     // Catch:{ IOException -> 0x0073 }
            if (r1 == 0) goto L_0x0098
            r1.recycle()     // Catch:{ IOException -> 0x0073 }
        L_0x0098:
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0073 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0073 }
            java.lang.String r2 = c.FileUtils.stripFileProtocol(r2)     // Catch:{ IOException -> 0x0073 }
            r1.<init>(r2)     // Catch:{ IOException -> 0x0073 }
            r1.delete()     // Catch:{ IOException -> 0x0073 }
            r1 = 1
            r11.a(r1)     // Catch:{ IOException -> 0x0073 }
            boolean r1 = r11.g     // Catch:{ IOException -> 0x0073 }
            if (r1 == 0) goto L_0x00d3
            if (r0 == 0) goto L_0x00d3
            r11.k = r0     // Catch:{ IOException -> 0x0073 }
            android.media.MediaScannerConnection r0 = r11.j     // Catch:{ IOException -> 0x0073 }
            if (r0 == 0) goto L_0x00bd
            android.media.MediaScannerConnection r0 = r11.j     // Catch:{ IOException -> 0x0073 }
            r0.disconnect()     // Catch:{ IOException -> 0x0073 }
        L_0x00bd:
            android.media.MediaScannerConnection r0 = new android.media.MediaScannerConnection     // Catch:{ IOException -> 0x0073 }
            vpadn.p r1 = r11.cordova     // Catch:{ IOException -> 0x0073 }
            android.app.Activity r1 = r1.a()     // Catch:{ IOException -> 0x0073 }
            android.content.Context r1 = r1.getApplicationContext()     // Catch:{ IOException -> 0x0073 }
            r0.<init>(r1, r11)     // Catch:{ IOException -> 0x0073 }
            r11.j = r0     // Catch:{ IOException -> 0x0073 }
            android.media.MediaScannerConnection r0 = r11.j     // Catch:{ IOException -> 0x0073 }
            r0.connect()     // Catch:{ IOException -> 0x0073 }
        L_0x00d3:
            java.lang.System.gc()     // Catch:{ IOException -> 0x0073 }
            goto L_0x006d
        L_0x00d7:
            r0 = 1
            if (r8 == r0) goto L_0x00dd
            r0 = 2
            if (r8 != r0) goto L_0x0322
        L_0x00dd:
            boolean r0 = r11.g     // Catch:{ IOException -> 0x0073 }
            if (r0 != 0) goto L_0x013e
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0073 }
            vpadn.p r1 = r11.cordova     // Catch:{ IOException -> 0x0073 }
            android.app.Activity r1 = r1.a()     // Catch:{ IOException -> 0x0073 }
            java.lang.String r1 = vpadn.C0003a.a(r1)     // Catch:{ IOException -> 0x0073 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0073 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0073 }
            java.lang.String r5 = java.lang.String.valueOf(r7)     // Catch:{ IOException -> 0x0073 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x0073 }
            java.lang.String r5 = ".jpg"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IOException -> 0x0073 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0073 }
            r0.<init>(r1, r4)     // Catch:{ IOException -> 0x0073 }
            android.net.Uri r0 = android.net.Uri.fromFile(r0)     // Catch:{ IOException -> 0x0073 }
        L_0x010b:
            if (r0 != 0) goto L_0x0112
            java.lang.String r1 = "Error capturing image - no media storage found."
            r11.failPicture(r1)     // Catch:{ IOException -> 0x0073 }
        L_0x0112:
            int r1 = r11.f1c     // Catch:{ IOException -> 0x0073 }
            r4 = -1
            if (r1 != r4) goto L_0x0143
            int r1 = r11.b     // Catch:{ IOException -> 0x0073 }
            r4 = -1
            if (r1 != r4) goto L_0x0143
            int r1 = r11.a     // Catch:{ IOException -> 0x0073 }
            r4 = 100
            if (r1 != r4) goto L_0x0143
            boolean r1 = r11.h     // Catch:{ IOException -> 0x0073 }
            if (r1 != 0) goto L_0x0143
            r11.a(r0)     // Catch:{ IOException -> 0x0073 }
            vpadn.o r1 = r11.callbackContext     // Catch:{ IOException -> 0x0073 }
            java.lang.String r3 = r0.toString()     // Catch:{ IOException -> 0x0073 }
            r1.a(r3)     // Catch:{ IOException -> 0x0073 }
            r1 = r2
        L_0x0133:
            vpadn.o r2 = r11.callbackContext     // Catch:{ IOException -> 0x0073 }
            java.lang.String r3 = r0.toString()     // Catch:{ IOException -> 0x0073 }
            r2.a(r3)     // Catch:{ IOException -> 0x0073 }
            goto L_0x0091
        L_0x013e:
            android.net.Uri r0 = r11.a()     // Catch:{ IOException -> 0x0073 }
            goto L_0x010b
        L_0x0143:
            android.net.Uri r1 = r11.d     // Catch:{ IOException -> 0x0073 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0073 }
            java.lang.String r1 = c.FileUtils.stripFileProtocol(r1)     // Catch:{ IOException -> 0x0073 }
            android.graphics.Bitmap r1 = r11.a(r1)     // Catch:{ IOException -> 0x0073 }
            if (r6 == 0) goto L_0x015b
            boolean r2 = r11.h     // Catch:{ IOException -> 0x0073 }
            if (r2 == 0) goto L_0x015b
            android.graphics.Bitmap r1 = a(r6, r1, r3)     // Catch:{ IOException -> 0x0073 }
        L_0x015b:
            vpadn.p r2 = r11.cordova     // Catch:{ IOException -> 0x0073 }
            android.app.Activity r2 = r2.a()     // Catch:{ IOException -> 0x0073 }
            android.content.ContentResolver r2 = r2.getContentResolver()     // Catch:{ IOException -> 0x0073 }
            java.io.OutputStream r2 = r2.openOutputStream(r0)     // Catch:{ IOException -> 0x0073 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ IOException -> 0x0073 }
            int r5 = r11.a     // Catch:{ IOException -> 0x0073 }
            r1.compress(r4, r5, r2)     // Catch:{ IOException -> 0x0073 }
            r2.close()     // Catch:{ IOException -> 0x0073 }
            int r2 = r11.e     // Catch:{ IOException -> 0x0073 }
            if (r2 != 0) goto L_0x0133
            boolean r2 = r11.g     // Catch:{ IOException -> 0x0073 }
            if (r2 == 0) goto L_0x0188
            vpadn.p r2 = r11.cordova     // Catch:{ IOException -> 0x0073 }
            java.lang.String r2 = c.FileUtils.getRealPathFromURI(r0, r2)     // Catch:{ IOException -> 0x0073 }
        L_0x0181:
            r3.b(r2)     // Catch:{ IOException -> 0x0073 }
            r3.b()     // Catch:{ IOException -> 0x0073 }
            goto L_0x0133
        L_0x0188:
            java.lang.String r2 = r0.getPath()     // Catch:{ IOException -> 0x0073 }
            goto L_0x0181
        L_0x018d:
            if (r13 != 0) goto L_0x0196
            java.lang.String r0 = "Camera cancelled."
            r11.failPicture(r0)
            goto L_0x006d
        L_0x0196:
            java.lang.String r0 = "Did not complete!"
            r11.failPicture(r0)
            goto L_0x006d
        L_0x019d:
            if (r0 == 0) goto L_0x01a2
            r1 = 2
            if (r0 != r1) goto L_0x006d
        L_0x01a2:
            r0 = -1
            if (r13 != r0) goto L_0x030c
            android.net.Uri r9 = r14.getData()
            int r0 = r11.f
            if (r0 == 0) goto L_0x01b8
            vpadn.o r0 = r11.callbackContext
            java.lang.String r1 = r9.toString()
            r0.a(r1)
            goto L_0x006d
        L_0x01b8:
            int r0 = r11.f1c
            r1 = -1
            if (r0 != r1) goto L_0x01d7
            int r0 = r11.b
            r1 = -1
            if (r0 != r1) goto L_0x01d7
            r0 = 1
            if (r8 == r0) goto L_0x01c8
            r0 = 2
            if (r8 != r0) goto L_0x01d7
        L_0x01c8:
            boolean r0 = r11.h
            if (r0 != 0) goto L_0x01d7
            vpadn.o r0 = r11.callbackContext
            java.lang.String r1 = r9.toString()
            r0.a(r1)
            goto L_0x006d
        L_0x01d7:
            vpadn.p r0 = r11.cordova
            java.lang.String r0 = c.FileUtils.getRealPathFromURI(r9, r0)
            java.lang.String r1 = c.FileUtils.getMimeType(r0)
            if (r0 == 0) goto L_0x01f5
            if (r1 == 0) goto L_0x01f5
            java.lang.String r2 = "image/jpeg"
            boolean r2 = r1.equalsIgnoreCase(r2)
            if (r2 != 0) goto L_0x0203
            java.lang.String r2 = "image/png"
            boolean r1 = r1.equalsIgnoreCase(r2)
            if (r1 != 0) goto L_0x0203
        L_0x01f5:
            java.lang.String r0 = "CameraLauncher"
            java.lang.String r1 = "I either have a null image path or bitmap"
            android.util.Log.d(r0, r1)
            java.lang.String r0 = "Unable to retrieve path to picture!"
            r11.failPicture(r0)
            goto L_0x006d
        L_0x0203:
            android.graphics.Bitmap r7 = r11.a(r0)
            if (r7 != 0) goto L_0x0217
            java.lang.String r0 = "CameraLauncher"
            java.lang.String r1 = "I either have a null image path or bitmap"
            android.util.Log.d(r0, r1)
            java.lang.String r0 = "Unable to create bitmap!"
            r11.failPicture(r0)
            goto L_0x006d
        L_0x0217:
            boolean r0 = r11.h
            if (r0 == 0) goto L_0x031c
            r0 = 1
            java.lang.String[] r2 = new java.lang.String[r0]
            r0 = 0
            java.lang.String r1 = "orientation"
            r2[r0] = r1
            vpadn.p r0 = r11.cordova
            android.app.Activity r0 = r0.a()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r1 = r14.getData()
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)
            if (r1 == 0) goto L_0x031f
            r0 = 0
            r1.moveToPosition(r0)
            r0 = 0
            int r0 = r1.getInt(r0)
            r1.close()
        L_0x0246:
            if (r0 == 0) goto L_0x031c
            android.graphics.Matrix r5 = new android.graphics.Matrix
            r5.<init>()
            float r0 = (float) r0
            r5.setRotate(r0)
            r1 = 0
            r2 = 0
            int r3 = r7.getWidth()
            int r4 = r7.getHeight()
            r6 = 1
            r0 = r7
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r0, r1, r2, r3, r4, r5, r6)
            r1 = r0
        L_0x0262:
            if (r8 != 0) goto L_0x0271
            r11.processPicture(r1)
        L_0x0267:
            if (r1 == 0) goto L_0x026c
            r1.recycle()
        L_0x026c:
            java.lang.System.gc()
            goto L_0x006d
        L_0x0271:
            r0 = 1
            if (r8 == r0) goto L_0x0277
            r0 = 2
            if (r8 != r0) goto L_0x0267
        L_0x0277:
            int r0 = r11.f1c
            if (r0 <= 0) goto L_0x0301
            int r0 = r11.b
            if (r0 <= 0) goto L_0x0301
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02f1 }
            vpadn.p r2 = r11.cordova     // Catch:{ Exception -> 0x02f1 }
            android.app.Activity r2 = r2.a()     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r2 = vpadn.C0003a.a(r2)     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x02f1 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r2 = "/resize.jpg"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r2 = r0.toString()     // Catch:{ Exception -> 0x02f1 }
            vpadn.g r3 = new vpadn.g     // Catch:{ Exception -> 0x02f1 }
            r3.<init>()     // Catch:{ Exception -> 0x02f1 }
            int r0 = r11.e     // Catch:{ IOException -> 0x02fc }
            if (r0 != 0) goto L_0x02ae
            r3.a(r2)     // Catch:{ IOException -> 0x02fc }
            r3.a()     // Catch:{ IOException -> 0x02fc }
            r3.c()     // Catch:{ IOException -> 0x02fc }
        L_0x02ae:
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x02f1 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x02f1 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x02f1 }
            int r5 = r11.a     // Catch:{ Exception -> 0x02f1 }
            r1.compress(r4, r5, r0)     // Catch:{ Exception -> 0x02f1 }
            r0.close()     // Catch:{ Exception -> 0x02f1 }
            int r0 = r11.e     // Catch:{ Exception -> 0x02f1 }
            if (r0 != 0) goto L_0x02cd
            vpadn.p r0 = r11.cordova     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r0 = c.FileUtils.getRealPathFromURI(r9, r0)     // Catch:{ Exception -> 0x02f1 }
            r3.b(r0)     // Catch:{ Exception -> 0x02f1 }
            r3.b()     // Catch:{ Exception -> 0x02f1 }
        L_0x02cd:
            vpadn.o r0 = r11.callbackContext     // Catch:{ Exception -> 0x02f1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r4 = "file://"
            r3.<init>(r4)     // Catch:{ Exception -> 0x02f1 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r3 = "?"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x02f1 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x02f1 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x02f1 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x02f1 }
            r0.a(r2)     // Catch:{ Exception -> 0x02f1 }
            goto L_0x0267
        L_0x02f1:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r0 = "Error retrieving image."
            r11.failPicture(r0)
            goto L_0x0267
        L_0x02fc:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x02f1 }
            goto L_0x02ae
        L_0x0301:
            vpadn.o r0 = r11.callbackContext
            java.lang.String r2 = r9.toString()
            r0.a(r2)
            goto L_0x0267
        L_0x030c:
            if (r13 != 0) goto L_0x0315
            java.lang.String r0 = "Selection cancelled."
            r11.failPicture(r0)
            goto L_0x006d
        L_0x0315:
            java.lang.String r0 = "Selection did not complete!"
            r11.failPicture(r0)
            goto L_0x006d
        L_0x031c:
            r1 = r7
            goto L_0x0262
        L_0x031f:
            r0 = r6
            goto L_0x0246
        L_0x0322:
            r0 = r1
            r1 = r2
            goto L_0x0091
        */
        throw new UnsupportedOperationException("Method not decompiled: c.CameraLauncher.onActivityResult(int, int, android.content.Intent):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap a(int i2, Bitmap bitmap, C0009g gVar) {
        Matrix matrix = new Matrix();
        if (i2 == 180) {
            matrix.setRotate((float) i2);
        } else {
            matrix.setRotate((float) i2, ((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
        }
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        gVar.a = "1";
        return createBitmap;
    }

    private void a(Uri uri) throws FileNotFoundException, IOException {
        FileInputStream fileInputStream = new FileInputStream(FileUtils.stripFileProtocol(this.d.toString()));
        OutputStream openOutputStream = this.cordova.a().getContentResolver().openOutputStream(uri);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read == -1) {
                openOutputStream.flush();
                openOutputStream.close();
                fileInputStream.close();
                return;
            }
            openOutputStream.write(bArr, 0, read);
        }
    }

    private Uri a() {
        ContentValues contentValues = new ContentValues();
        contentValues.put("mime_type", "image/jpeg");
        try {
            return this.cordova.a().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
        } catch (UnsupportedOperationException e2) {
            C0020r.b("CameraLauncher", "Can't write to external media storage.");
            try {
                return this.cordova.a().getContentResolver().insert(MediaStore.Images.Media.INTERNAL_CONTENT_URI, contentValues);
            } catch (UnsupportedOperationException e3) {
                C0020r.b("CameraLauncher", "Can't write to internal media storage.");
                return null;
            }
        }
    }

    private Bitmap a(String str) {
        if (this.b <= 0 && this.f1c <= 0) {
            return BitmapFactory.decodeFile(str);
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        if (options.outWidth == 0 || options.outHeight == 0) {
            return null;
        }
        int[] calculateAspectRatio = calculateAspectRatio(options.outWidth, options.outHeight);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, this.b, this.f1c);
        Bitmap decodeFile = BitmapFactory.decodeFile(str, options);
        if (decodeFile != null) {
            return Bitmap.createScaledBitmap(decodeFile, calculateAspectRatio[0], calculateAspectRatio[1], true);
        }
        return null;
    }

    public int[] calculateAspectRatio(int i2, int i3) {
        int i4 = this.b;
        int i5 = this.f1c;
        if (i4 > 0 || i5 > 0) {
            if (i4 > 0 && i5 <= 0) {
                i3 = (i4 * i3) / i2;
                i2 = i4;
            } else if (i4 > 0 || i5 <= 0) {
                double d2 = ((double) i4) / ((double) i5);
                double d3 = ((double) i2) / ((double) i3);
                if (d3 > d2) {
                    i3 = (i4 * i3) / i2;
                    i2 = i4;
                } else if (d3 < d2) {
                    i2 = (i5 * i2) / i3;
                    i3 = i5;
                } else {
                    i3 = i5;
                    i2 = i4;
                }
            } else {
                i2 = (i5 * i2) / i3;
                i3 = i5;
            }
        }
        return new int[]{i2, i3};
    }

    public static int calculateSampleSize(int i2, int i3, int i4, int i5) {
        if (((float) i2) / ((float) i3) > ((float) i4) / ((float) i5)) {
            return i2 / i4;
        }
        return i3 / i5;
    }

    private Cursor b(Uri uri) {
        return this.cordova.a().getContentResolver().query(uri, new String[]{"_id"}, null, null, null);
    }

    private void a(int i2) {
        int i3;
        int i4 = 1;
        Uri b2 = b();
        Cursor b3 = b(b2);
        int count = b3.getCount();
        if (i2 == 1 && this.g) {
            i4 = 2;
        }
        if (count - this.i == i4) {
            b3.moveToLast();
            int intValue = Integer.valueOf(b3.getString(b3.getColumnIndex("_id"))).intValue();
            if (i4 == 2) {
                i3 = intValue - 1;
            } else {
                i3 = intValue;
            }
            this.cordova.a().getContentResolver().delete(Uri.parse(b2 + "/" + i3), null, null);
        }
    }

    private static Uri b() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        }
        return MediaStore.Images.Media.INTERNAL_CONTENT_URI;
    }

    public void processPicture(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, this.a, byteArrayOutputStream)) {
                this.callbackContext.a(new String(R.a(byteArrayOutputStream.toByteArray(), true)));
            }
        } catch (Exception e2) {
            failPicture("Error compressing image.");
        }
    }

    public void failPicture(String str) {
        this.callbackContext.b(str);
    }

    public void onMediaScannerConnected() {
        try {
            this.j.scanFile(this.k.toString(), "image/*");
        } catch (IllegalStateException e2) {
            C0020r.e("CameraLauncher", "Can't scan file in MediaScanner after taking picture");
        }
    }

    public void onScanCompleted(String str, Uri uri) {
        this.j.disconnect();
    }
}
