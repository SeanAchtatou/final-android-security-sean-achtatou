package c;

import android.webkit.JavascriptInterface;
import org.json.JSONException;
import vpadn.C0015m;
import vpadn.C0023u;

public class ExposedJsApi {
    private C0023u a;
    private C0015m b;

    public ExposedJsApi(C0023u uVar, C0015m mVar) {
        this.a = uVar;
        this.b = mVar;
    }

    @JavascriptInterface
    public String exec(String str, String str2, String str3, String str4) throws JSONException {
        this.b.a(true);
        try {
            this.a.a(str, str2, str3, str4);
            return this.b.b();
        } finally {
            this.b.a(false);
        }
    }

    @JavascriptInterface
    public void setNativeToJsBridgeMode(int i) {
        this.b.a(i);
    }

    @JavascriptInterface
    public String retrieveJsMessages() {
        return this.b.b();
    }
}
