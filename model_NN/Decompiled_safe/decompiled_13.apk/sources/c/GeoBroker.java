package c;

import android.location.Location;
import android.location.LocationManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import vpadn.C0007e;
import vpadn.C0012j;
import vpadn.C0016n;
import vpadn.C0017o;
import vpadn.C0019q;
import vpadn.C0024v;

public class GeoBroker extends C0019q {
    private C0012j a;
    private C0016n b;

    /* renamed from: c  reason: collision with root package name */
    private LocationManager f18c;

    public boolean execute(String str, JSONArray jSONArray, C0017o oVar) throws JSONException {
        if (this.f18c == null) {
            this.f18c = (LocationManager) this.cordova.a().getSystemService("location");
            this.b = new C0016n(this.f18c, this);
            this.a = new C0012j(this.f18c, this);
        }
        if (!this.f18c.isProviderEnabled("gps") && !this.f18c.isProviderEnabled("network")) {
            oVar.a(new C0024v(C0024v.a.NO_RESULT, "Location API is not available for this device."));
        } else if (str.equals("getLocation")) {
            boolean z = jSONArray.getBoolean(0);
            int i = jSONArray.getInt(1);
            Location lastKnownLocation = this.f18c.getLastKnownLocation(z ? "gps" : "network");
            if (lastKnownLocation != null && System.currentTimeMillis() - lastKnownLocation.getTime() <= ((long) i)) {
                oVar.a(new C0024v(C0024v.a.OK, returnLocationJSON(lastKnownLocation)));
            } else if (z) {
                this.a.a(oVar);
            } else {
                this.b.a(oVar);
            }
        } else if (str.equals("addWatch")) {
            String string = jSONArray.getString(0);
            if (jSONArray.getBoolean(1)) {
                this.a.a(string, oVar);
            } else {
                this.b.a(string, oVar);
            }
        } else if (!str.equals("clearWatch")) {
            return false;
        } else {
            String string2 = jSONArray.getString(0);
            this.a.a(string2);
            this.b.a(string2);
        }
        return true;
    }

    public void onDestroy() {
        if (this.b != null) {
            this.b.a();
            this.b = null;
        }
        if (this.a != null) {
            this.a.a();
            this.a = null;
        }
    }

    public void onReset() {
        onDestroy();
    }

    public JSONObject returnLocationJSON(Location location) {
        Float f = null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("latitude", location.getLatitude());
            jSONObject.put("longitude", location.getLongitude());
            jSONObject.put("altitude", location.hasAltitude() ? Double.valueOf(location.getAltitude()) : null);
            jSONObject.put("accuracy", (double) location.getAccuracy());
            if (location.hasBearing() && location.hasSpeed()) {
                f = Float.valueOf(location.getBearing());
            }
            jSONObject.put("heading", f);
            jSONObject.put("speed", (double) location.getSpeed());
            jSONObject.put("timestamp", location.getTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject;
    }

    public void win(Location location, C0017o oVar) {
        oVar.a(new C0024v(C0024v.a.OK, returnLocationJSON(location)));
    }

    public void fail(int i, String str, C0017o oVar) {
        String str2;
        JSONObject jSONObject;
        C0024v vVar;
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("code", i);
            jSONObject2.put("message", str);
            jSONObject = jSONObject2;
            str2 = null;
        } catch (JSONException e) {
            str2 = "{'code':" + i + ",'message':'" + str.replaceAll("'", "'") + "'}";
            jSONObject = null;
        }
        if (jSONObject != null) {
            vVar = new C0024v(C0024v.a.ERROR, jSONObject);
        } else {
            vVar = new C0024v(C0024v.a.ERROR, str2);
        }
        oVar.a(vVar);
    }

    public boolean isGlobalListener(C0007e eVar) {
        if (this.a == null || this.b == null) {
            return false;
        }
        if (this.a.equals(eVar) || this.b.equals(eVar)) {
            return true;
        }
        return false;
    }
}
