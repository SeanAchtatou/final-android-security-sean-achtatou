package vpadn;

import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import c.CordovaWebView;
import java.io.IOException;

/* renamed from: vpadn.l  reason: case insensitive filesystem */
public final class C0014l extends C0008f {
    public C0014l(C0018p pVar, CordovaWebView cordovaWebView) {
        super(pVar, cordovaWebView);
    }

    public final WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (str.contains("?") || str.contains("#")) {
            return a(str);
        }
        return super.shouldInterceptRequest(webView, str);
    }

    private WebResourceResponse a(String str) {
        String str2;
        if (str.startsWith("file:///android_asset/")) {
            String replaceFirst = str.replaceFirst("file:///android_asset/", "");
            if (replaceFirst.contains("?")) {
                replaceFirst = replaceFirst.split("\\?")[0];
            } else if (replaceFirst.contains("#")) {
                replaceFirst = replaceFirst.split("#")[0];
            }
            if (replaceFirst.endsWith(".html")) {
                str2 = "text/html";
            } else {
                str2 = null;
            }
            try {
                return new WebResourceResponse(str2, "UTF-8", this.a.a().getAssets().open(Uri.parse(replaceFirst).getPath(), 2));
            } catch (IOException e) {
                C0020r.b("generateWebResourceResponse", e.getMessage(), e);
            }
        }
        return null;
    }
}
