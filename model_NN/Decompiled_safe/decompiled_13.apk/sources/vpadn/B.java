package vpadn;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.vpon.ads.VponAdRequest;
import com.vpon.ads.VponAdSize;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONObject;
import vpadn.C0024v;

public abstract class B implements D, C0018p {
    protected Activity a;
    protected String b;

    /* renamed from: c  reason: collision with root package name */
    protected VponAdSize f101c = null;
    protected String d;
    protected String e = null;
    protected boolean f = false;
    protected Map<String, String> g = Collections.synchronizedMap(new HashMap());
    protected boolean h = false;
    protected JSONObject i = new JSONObject();
    protected long j;
    protected long k;
    protected long l = -1;
    protected VponAdRequest m = null;
    protected boolean n = false;
    private final ExecutorService o = Executors.newCachedThreadPool();
    private Map<String, Map<Integer, C0017o>> p = Collections.synchronizedMap(new HashMap());

    /* access modifiers changed from: protected */
    public abstract void a(Object obj);

    /* access modifiers changed from: protected */
    public abstract void c();

    public B(Activity activity) {
        this.a = activity;
        if (O.a().a("user-agent") == null) {
            this.e = String.valueOf(new WebView(this.a).getSettings().getUserAgentString()) + " (Mobile; vpadn-sdk-a-v4.0.0)";
            O.a().a("user-agent", new String(this.e));
        } else {
            this.e = (String) O.a().a("user-agent");
        }
        C0003a.b(this.a);
    }

    public final void b() {
        this.p.clear();
    }

    public void a(String str) {
        this.b = str;
    }

    public final void a(VponAdSize vponAdSize) {
        this.f101c = vponAdSize;
    }

    public final void b(String str) {
        this.d = str;
    }

    public final void a(long j2) {
        this.l = j2;
    }

    public final void c(String str) {
        this.g.put("url_type_banner", str);
    }

    public final void d(String str) {
        this.g.put("url_type_click", str);
    }

    public final void e(String str) {
        this.g.put("url_type_impression", str);
    }

    public final String d() {
        return this.g.remove("url_type_click");
    }

    public final void a(String str, int i2, C0017o oVar) {
        try {
            if ("onhide".equals(str) || "onshow".equals(str) || "ad_pos_change".equals(str)) {
                Map map = this.p.get(str);
                if (map == null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put(Integer.valueOf(i2), oVar);
                    this.p.put(str, hashMap);
                } else {
                    map.put(Integer.valueOf(i2), oVar);
                }
                if ("ad_pos_change".equals(str)) {
                    C0024v vVar = new C0024v(C0024v.a.OK, this.i);
                    vVar.a(true);
                    oVar.a(vVar);
                } else if ("onshow".equals(str)) {
                    if (this.h) {
                        C0024v vVar2 = new C0024v(C0024v.a.OK);
                        vVar2.a(true);
                        oVar.a(vVar2);
                    }
                } else if ("onhide".equals(str) && !this.h) {
                    C0024v vVar3 = new C0024v(C0024v.a.OK);
                    vVar3.a(true);
                    oVar.a(vVar3);
                }
            } else {
                C0003a.c("AbstractVponController", "EventType not supported! " + str);
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "EventType not supported!"));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "addEventListener throw Exception:" + e2.getMessage()));
            } catch (Exception e3) {
                e2.printStackTrace();
            }
        }
    }

    public final void b(String str, int i2, C0017o oVar) {
        try {
            if (!"onhide".equals(str) && !"onshow".equals(str) && !"ad_pos_change".equals(str)) {
                C0003a.c("AbstractVponController", "EventType not supported! " + str);
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "EventType not supported!"));
            } else if (this.p.containsKey(str)) {
                Map map = this.p.get(str);
                map.remove(Integer.valueOf(i2));
                if (map.size() == 0) {
                    this.p.remove(str);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            try {
                oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "removeEventListener throw Exception:" + e2.getMessage()));
            } catch (Exception e3) {
                e2.printStackTrace();
            }
        }
    }

    public void f() {
        this.h = true;
        a("onshow", (JSONObject) null);
    }

    public final void g() {
        this.h = false;
        a("onhide", (JSONObject) null);
    }

    public final JSONObject h() {
        Exception e2;
        JSONObject jSONObject;
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject = M.a().a(this.a, jSONObject2);
            try {
                jSONObject.put("sid", this.j);
                jSONObject.put("seq", this.k);
            } catch (Exception e3) {
                e2 = e3;
                e2.printStackTrace();
                return jSONObject;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            jSONObject = jSONObject2;
            e2 = exc;
            e2.printStackTrace();
            return jSONObject;
        }
        return jSONObject;
    }

    /* access modifiers changed from: protected */
    public final void a(String str, JSONObject jSONObject) {
        C0024v vVar;
        if (jSONObject != null) {
            C0003a.c("AbstractVponController", "Banner TriggerEvent eventType:" + str + " retObj:" + jSONObject.toString());
        } else {
            C0003a.c("AbstractVponController", "Banner TriggerEvent eventType:" + str);
        }
        if (this.p.get(str) != null) {
            if (jSONObject != null) {
                vVar = new C0024v(C0024v.a.OK, jSONObject);
            } else {
                vVar = new C0024v(C0024v.a.OK);
            }
            vVar.a(true);
            for (C0017o a2 : this.p.get(str).values()) {
                a2.a(vVar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void i() {
        N a2 = N.a();
        this.j = a2.b();
        this.k = a2.c();
    }

    /* access modifiers changed from: protected */
    public final long j() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final long k() {
        return this.k;
    }

    public final void a(C0019q qVar, Intent intent, int i2) {
        this.a.startActivity(intent);
    }

    public final Activity a() {
        return this.a;
    }

    public final ExecutorService e() {
        return this.o;
    }

    public final Object a(String str, Object obj) {
        if (str.equals("load_banner")) {
            c();
            return null;
        } else if (!str.equals("load_banner_fail")) {
            return null;
        } else {
            a(obj);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final boolean l() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }
}
