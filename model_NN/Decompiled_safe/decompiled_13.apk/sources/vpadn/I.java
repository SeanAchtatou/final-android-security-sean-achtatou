package vpadn;

import com.vpon.ads.VponAdRequest;

public interface I {
    void onControllerWebViewReady(int i, int i2);

    void onLeaveExpandMode();

    void onPrepareExpandMode();

    void onVponAdFailed(VponAdRequest.VponErrorCode vponErrorCode);

    void onVponAdReceived();

    void onVponDismiss();

    void onVponLeaveApplication();

    void onVponPresent();
}
