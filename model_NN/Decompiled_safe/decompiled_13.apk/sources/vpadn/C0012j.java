package vpadn;

import android.location.LocationManager;
import c.GeoBroker;

/* renamed from: vpadn.j  reason: case insensitive filesystem */
public final class C0012j extends C0007e {
    public C0012j(LocationManager locationManager, GeoBroker geoBroker) {
        super(locationManager, geoBroker, "[Cordova GPSListener]");
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.f114c) {
            return;
        }
        if (this.b.getProvider("gps") != null) {
            this.f114c = true;
            this.b.requestLocationUpdates("gps", 60000, 0.0f, this);
            return;
        }
        a(C0007e.a, "GPS provider is not available.");
    }
}
