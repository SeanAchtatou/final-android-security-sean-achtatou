package vpadn;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;
import com.google.ads.AdActivity;
import com.vpon.ads.VponAdSize;
import java.util.List;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public final class M {
    private static M a = new M();

    private M() {
    }

    public static M a() {
        return a;
    }

    public final JSONObject a(Context context, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        return n(j(o(d(p(q(s(a(r(jSONObject, context), context), context), context), context)), context), context), context);
    }

    public final JSONObject b(Context context, JSONObject jSONObject) {
        return a(j(b(k(l(c(m(n(o(d(p(q(s(a(r(new JSONObject(), context), context), context), context), context)), context), context), context)), context), context)), context));
    }

    public final JSONObject c(Context context, JSONObject jSONObject) {
        return b(c(d(e(f(g(h(i(new JSONObject(), context), context), context), context), context), context), context), context);
    }

    private static JSONObject b(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addLac throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("lac", C0003a.l(context));
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addLac throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject a(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addAcceptAdFormat throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("output", AdActivity.HTML_PARAM);
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addAcceptAdFormat throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject c(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addCellId throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("cell_id", C0003a.k(context));
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addCellId throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject d(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addLatLonAccuracy throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            X.a(context);
            Location a2 = X.a();
            if (a2 != null) {
                jSONObject2.put("lat", a2.getLatitude());
                jSONObject2.put("lon", a2.getLongitude());
                jSONObject2.put("loc_acc", (double) a2.getAccuracy());
            } else {
                C0003a.d("DeviceDataCollector", "VponLocation.instance(context).getLocation() return null");
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addLatLonAccuracy throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject e(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addBSSId throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            String e2 = C0003a.e(context);
            if (e2 != null) {
                jSONObject2.put("bssid", e2.replaceAll(":", "").toUpperCase());
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addBSSId throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject f(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addAndroidId throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("and_id", C0003a.g(context));
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addAndroidId throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject g(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addOpenUdid throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("open_udid", C0003a.h(context));
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addOpenUdid throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject h(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addMacAddress throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            String d = C0003a.d(context);
            if (d != null) {
                jSONObject2.put("mac", d.replaceAll(":", "").toUpperCase());
            } else {
                C0003a.d("DeviceDataCollector", " VponDevice.getMac(context) is null");
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addMacAddress throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject i(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addIMEI throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("imei", C0003a.c(context));
            C0003a.a("DeviceDataCollector", "IMEI:" + jSONObject2.toString(4));
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addIMEI throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static JSONObject j(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        Exception exc;
        JSONObject jSONObject3;
        boolean z;
        boolean z2;
        boolean z3;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                exc = e;
                jSONObject3 = jSONObject;
                exc.printStackTrace();
                C0003a.a("DeviceDataCollector", "addCapabilities throw Exception:" + exc.getMessage(), exc);
                return jSONObject3;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            JSONArray jSONArray = new JSONArray();
            jSONArray.put("m2");
            jSONArray.put("a");
            if (Build.VERSION.SDK_INT > 11) {
                jSONArray.put("inv");
            }
            if (Y.c(context)) {
                jSONArray.put("locC");
            }
            if (Y.b(context)) {
                jSONArray.put("locF");
            }
            if (context.getPackageManager().hasSystemFeature("android.hardware.camera")) {
                jSONArray.put("cam");
            }
            switch (((TelephonyManager) context.getSystemService("phone")).getSimState()) {
                case 0:
                    C0003a.d("VponDevice", "TelephonyManager.SIM_STATE_UNKNOWN");
                    z = false;
                    break;
                case 1:
                    C0003a.d("VponDevice", "TelephonyManager.SIM_STATE_ABSENT");
                    z = false;
                    break;
                case 2:
                    C0003a.d("VponDevice", "TelephonyManager.SIM_STATE_PIN_REQUIRED");
                    z = false;
                    break;
                case 3:
                    C0003a.d("VponDevice", "TelephonyManager.SIM_STATE_PUK_REQUIRED");
                    z = false;
                    break;
                case 4:
                    C0003a.d("VponDevice", "TelephonyManager.SIM_STATE_NETWORK_LOCKED");
                    z = false;
                    break;
                case 5:
                    C0003a.a("VponDevice", "TelephonyManager.SIM_STATE_READY");
                    z = true;
                    break;
                default:
                    z = false;
                    break;
            }
            if (z) {
                jSONArray.put("ph");
                jSONArray.put("sms");
            }
            List<Sensor> sensorList = ((SensorManager) context.getSystemService("sensor")).getSensorList(3);
            if (sensorList == null || sensorList.size() <= 0) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (z2) {
                jSONArray.put("comp");
            }
            if (Y.a(context)) {
                jSONArray.put("fw");
            }
            jSONArray.put("fr");
            if (context instanceof Activity) {
                Activity activity = (Activity) context;
                if (Build.VERSION.SDK_INT >= 11) {
                    z3 = activity.getWindow().getDecorView().isHardwareAccelerated();
                    if (!z3) {
                        C0003a.d("VponDevice", "Hardware Accelerated is disabled, Suggest to enable Hardware Accelerated Using Activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED) on onCreate of Activity method");
                    }
                }
                z3 = false;
            } else {
                C0003a.b("VponDevice", "context cannot cast to Activity");
                z3 = false;
            }
            if (z3) {
                jSONArray.put("banInv");
            }
            jSONObject2.put("cap", jSONArray);
            return jSONObject2;
        } catch (Exception e2) {
            Exception exc2 = e2;
            jSONObject3 = jSONObject2;
            exc = exc2;
            exc.printStackTrace();
            C0003a.a("DeviceDataCollector", "addCapabilities throw Exception:" + exc.getMessage(), exc);
            return jSONObject3;
        }
    }

    private static JSONObject b(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addIsRunInSimulator throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            C0003a.c("DeviceDataCollector", "android.os.Build.PRODUCT:" + Build.PRODUCT);
            if (Build.PRODUCT.equals("google_sdk") || Build.PRODUCT.equals("sdk")) {
                jSONObject2.put("simulator", 1);
            } else {
                jSONObject2.put("simulator", 0);
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addIsRunInSimulator throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject k(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addAppName throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            String b = C0003a.b();
            jSONObject2.put("app_name", String.valueOf(b) + ".android." + ((Activity) context).getPackageName());
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addAppName throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject l(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addPackageName throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("msid", ((Activity) context).getPackageName());
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addPackageName throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject c(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addOutputFomat throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("format", AdActivity.HTML_PARAM);
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addOutputFomat throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject m(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addMncMCC throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            String j = C0003a.j(context);
            String i = C0003a.i(context);
            jSONObject2.put("mnc", j);
            jSONObject2.put("mcc", i);
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addMncMCC throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject n(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addOsVersion throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("os_v", C0003a.b());
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addOsVersion throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject o(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addDeviceOrientation throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            int i = context.getResources().getConfiguration().orientation;
            if (i == 3) {
                i = 0;
            }
            if (2 == i) {
                jSONObject2.put("u_o", 2);
            } else {
                jSONObject2.put("u_o", 1);
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addDeviceOrientation throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject d(JSONObject jSONObject) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addSDKVersion throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("sdk", "vpadn-sdk-a-v4.0.0");
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addSDKVersion throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject p(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addNetworkType throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            jSONObject2.put("ni", C0003a.m(context));
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addNetworkType throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    private static JSONObject q(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addDeviceLanguage throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            Configuration configuration = context.getResources().getConfiguration();
            if (configuration.locale.equals(Locale.TAIWAN) || configuration.locale.equals(Locale.TRADITIONAL_CHINESE)) {
                jSONObject2.put("lang", "zh_TW");
                return jSONObject2;
            }
            if (configuration.locale.equals(Locale.SIMPLIFIED_CHINESE)) {
                jSONObject2.put("lang", "zh_CN");
            } else {
                jSONObject2.put("lang", "en_US");
            }
            return jSONObject2;
        } catch (Exception e2) {
            e = e2;
        }
    }

    @SuppressLint({"NewApi"})
    private static JSONObject r(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        Exception exc;
        JSONObject jSONObject3;
        int i;
        int i2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                exc = e;
                jSONObject3 = jSONObject;
                exc.printStackTrace();
                C0003a.a("DeviceDataCollector", "addScreenWidthAndHeight throw Exception:" + exc.getMessage(), exc);
                return jSONObject3;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            int i3 = Build.VERSION.SDK_INT;
            C0003a.c("DeviceDataCollector", "currentApiVersion:" + i3);
            if (i3 >= 17) {
                Point point = new Point();
                ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRealSize(point);
                i = point.x;
                i2 = point.y;
            } else {
                i = C0003a.f(context).widthPixels;
                i2 = C0003a.f(context).heightPixels;
            }
            int round = Math.round(VponAdSize.convertPixelsToDp((float) i, context));
            int round2 = Math.round(VponAdSize.convertPixelsToDp((float) i2, context));
            jSONObject2.put("s_w", round);
            jSONObject2.put("s_h", round2);
            return jSONObject2;
        } catch (Exception e2) {
            Exception exc2 = e2;
            jSONObject3 = jSONObject2;
            exc = exc2;
            exc.printStackTrace();
            C0003a.a("DeviceDataCollector", "addScreenWidthAndHeight throw Exception:" + exc.getMessage(), exc);
            return jSONObject3;
        }
    }

    private JSONObject s(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                e = e;
                jSONObject2 = jSONObject;
                e.printStackTrace();
                C0003a.a("DeviceDataCollector", "addDensity throw Exception:" + e.getMessage(), e);
                return jSONObject2;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            StringBuilder sb = new StringBuilder("Device:");
            String str = Build.MANUFACTURER;
            String str2 = Build.MODEL;
            C0003a.c("DeviceDataCollector", sb.append((str2 == null || str == null || !str2.startsWith(str)) ? String.valueOf(a(str)) + " " + str2 : a(str2)).append(" metrics.densityDpi:").append(displayMetrics.densityDpi).append(" metrics.density:").append(displayMetrics.density).toString());
            jSONObject2.put("u_sd", (double) displayMetrics.density);
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            C0003a.a("DeviceDataCollector", "addDensity throw Exception:" + e.getMessage(), e);
            return jSONObject2;
        }
        return jSONObject2;
    }

    public final JSONObject a(JSONObject jSONObject, Context context) {
        JSONObject jSONObject2;
        Exception exc;
        JSONObject jSONObject3;
        if (jSONObject == null) {
            try {
                jSONObject2 = new JSONObject();
            } catch (Exception e) {
                exc = e;
                jSONObject3 = jSONObject;
                exc.printStackTrace();
                C0003a.a("DeviceDataCollector", "addUnitWidthAndHeight throw Exception:" + exc.getMessage(), exc);
                return jSONObject3;
            }
        } else {
            jSONObject2 = jSONObject;
        }
        try {
            int i = C0003a.f(context).widthPixels;
            int i2 = C0003a.f(context).heightPixels;
            int a2 = a(context);
            int round = Math.round(VponAdSize.convertPixelsToDp((float) i, context));
            int round2 = Math.round(VponAdSize.convertPixelsToDp((float) ((i2 - a2) - (((Activity) context).getWindow().findViewById(16908290).getTop() - a(context))), context));
            jSONObject2.put("u_w", round);
            jSONObject2.put("u_h", round2);
            return jSONObject2;
        } catch (Exception e2) {
            Exception exc2 = e2;
            jSONObject3 = jSONObject2;
            exc = exc2;
            exc.printStackTrace();
            C0003a.a("DeviceDataCollector", "addUnitWidthAndHeight throw Exception:" + exc.getMessage(), exc);
            return jSONObject3;
        }
    }

    private static int a(Context context) {
        Rect rect = new Rect();
        Window window = ((Activity) context).getWindow();
        if (window == null || window.getDecorView() == null) {
            C0003a.b("DeviceDataCollector", "getStatusBarHeight ERROR (w != null && w.getDecorView() != null) is false");
            return 0;
        }
        window.getDecorView().getWindowVisibleDisplayFrame(rect);
        return rect.top;
    }

    private static String a(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        char charAt = str.charAt(0);
        return !Character.isUpperCase(charAt) ? String.valueOf(Character.toUpperCase(charAt)) + str.substring(1) : str;
    }
}
