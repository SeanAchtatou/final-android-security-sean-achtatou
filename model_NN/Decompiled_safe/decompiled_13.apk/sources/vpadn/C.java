package vpadn;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import c.NetworkManager;
import com.google.ads.AdActivity;
import com.vpon.ads.VponAdRequest;
import com.vpon.ads.VponAdSize;
import com.vpon.webview.VponAdWebView;
import com.vpon.widget.VponActivity;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class C extends B implements J, K, L {
    private Timer A;
    private Timer B = null;
    private int C = -1;
    private int D;
    private boolean E = true;
    private String F = NetworkManager.TYPE_NONE;
    /* access modifiers changed from: private */
    public boolean G = false;
    /* access modifiers changed from: private */
    public int H = 0;
    private int I = 0;
    private int J = 0;
    private int K = 0;
    /* access modifiers changed from: private */
    public int L = 0;
    /* access modifiers changed from: private */
    public int M = 0;
    /* access modifiers changed from: private */
    public int N = 0;
    /* access modifiers changed from: private */
    public boolean O = false;
    /* access modifiers changed from: private */
    public boolean P = false;
    private Bitmap Q;
    private Canvas R;
    private Rect S;
    private VponAdWebView o;
    private VponAdWebView p;
    private VponAdWebView q;
    private P r;
    /* access modifiers changed from: private */
    public P s;
    /* access modifiers changed from: private */
    public RelativeLayout t = null;
    /* access modifiers changed from: private */
    public ImageView u = null;
    private boolean v = false;
    /* access modifiers changed from: private */
    public I w = null;
    /* access modifiers changed from: private */
    public boolean x = false;
    /* access modifiers changed from: private */
    public Timer y = null;
    private int z = 0;

    public C(Activity activity, I i) {
        super(activity);
        this.w = i;
        this.C = Resources.getSystem().getConfiguration().orientation;
    }

    public final VponAdWebView m() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        new Handler().post(new Runnable() {
            public final void run() {
                C0003a.a("VponBannerController", "doLoadBanner");
                if (C.this.w != null) {
                    C0003a.a("VponBannerController", "Call mNotificationListener.onVponAdReceived();");
                    C.this.w.onVponAdReceived();
                }
                C.this.b(C.this.x);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(final Object obj) {
        new Handler().post(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: vpadn.C.a(vpadn.C, boolean):void
             arg types: [vpadn.C, int]
             candidates:
              vpadn.C.a(android.view.View, android.view.ViewGroup):int
              vpadn.C.a(vpadn.C, int):void
              vpadn.C.a(vpadn.C, long):void
              vpadn.C.a(vpadn.C, vpadn.P):void
              vpadn.C.a(int, int):void
              vpadn.B.a(java.lang.String, java.lang.Object):java.lang.Object
              vpadn.B.a(java.lang.String, org.json.JSONObject):void
              vpadn.p.a(java.lang.String, java.lang.Object):java.lang.Object
              vpadn.L.a(int, int):void
              vpadn.C.a(vpadn.C, boolean):void */
            public final void run() {
                C0003a.b("VponBannerController", "doLoadBannerFail");
                if (C.this.w != null) {
                    C.this.w();
                    C.this.P = false;
                    if (obj == null || !(obj instanceof String) || !obj.toString().equals("NO_FILL")) {
                        C.this.w.onVponAdFailed(VponAdRequest.VponErrorCode.INTERNAL_ERROR);
                    } else {
                        C.this.w.onVponAdFailed(VponAdRequest.VponErrorCode.NO_FILL);
                    }
                }
            }
        });
    }

    public final void n() {
        new Handler().post(new Runnable() {
            public final void run() {
                C.f(C.this);
            }
        });
    }

    public final void a(boolean z2) {
        this.v = z2;
        if (this.G) {
            final boolean z3 = this.v;
            if (this.G && this.t != null) {
                new Handler().post(new Runnable() {
                    public final void run() {
                        if (C.this.t.getChildCount() >= 2 && z3) {
                            C.this.t.removeView(C.this.u);
                        }
                        if (C.this.t.getChildCount() == 1 && !z3) {
                            C.this.C();
                        }
                    }
                });
            }
        }
    }

    public final void a(C0017o oVar, String str, String str2, boolean z2, boolean z3, String str3, int i, boolean z4, boolean z5, boolean z6) {
        try {
            C0003a.c("VponBannerController", "===========>>Enter doOpenWebAppForSDKPlugIn");
            this.D = this.a.getRequestedOrientation();
            Bundle bundle = new Bundle();
            bundle.putString("adType", "sdkOpenWebApp");
            bundle.putString("url", str);
            bundle.putBoolean("isUseCustomClose", z2);
            String uuid = UUID.randomUUID().toString();
            bundle.putString("getControllerKey", uuid);
            O a2 = O.a();
            a2.a(uuid, this);
            String uuid2 = UUID.randomUUID().toString();
            bundle.putString("getCallbackContextKey", uuid2);
            a2.a(uuid2, oVar);
            this.D = this.a.getRequestedOrientation();
            this.C = Resources.getSystem().getConfiguration().orientation;
            bundle.putInt("originalRequestedOrientation", this.D);
            bundle.putInt("beforeActivityOrientation", this.C);
            bundle.putString("forceOrientation", str3);
            bundle.putBoolean("isAllowOrientationChange", z3);
            Rect rect = new Rect();
            this.a.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            bundle.putInt("statusBarHeight", rect.top);
            if (this.s != null) {
                X.a(this.a);
                Location a3 = X.a();
                if (a3 != null) {
                    P p2 = this.s;
                    P p3 = this.s;
                    bundle.putInt("distance", Y.a(null, null, a3.getLatitude(), a3.getLongitude()));
                }
            }
            if (str2 != null) {
                bundle.putString(AdActivity.HTML_PARAM, str2);
            }
            bundle.putInt("backgroundColor", i);
            bundle.putBoolean("isShowProgressBar", z4);
            bundle.putBoolean("isShowNavigationBar", z5);
            bundle.putBoolean("isUseWebViewLoadUrl", z6);
            bundle.putString("click_url", (String) this.g.get("url_type_click"));
            bundle.putLong("session_id", j());
            bundle.putLong("sequence_number", k());
            Intent intent = new Intent(this.a, VponActivity.class);
            intent.setFlags(65536);
            intent.setFlags(268435456);
            bundle.putBoolean("isFullScreen", (this.a.getWindow().getAttributes().flags & 1024) != 0);
            intent.putExtras(bundle);
            this.a.startActivity(intent);
            if (this.w != null) {
                this.w.onVponPresent();
            }
            if (this.x) {
                G();
                this.O = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.b("VponBannerController", "doOpenWebAppForSDKPlugIn throw Exception:" + e.getMessage());
        }
    }

    public final void f() {
        super.f();
        if (this.x && this.s != null && this.y == null && this.O) {
            this.a.runOnUiThread(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: vpadn.C.b(vpadn.C, boolean):void
                 arg types: [vpadn.C, int]
                 candidates:
                  vpadn.C.b(vpadn.C, int):void
                  vpadn.C.b(vpadn.C, long):void
                  vpadn.C.b(vpadn.C, boolean):void */
                public final void run() {
                    if (!C.this.P) {
                        C.this.b(C.this.s.b);
                    }
                    C.this.O = false;
                }
            });
        }
    }

    public final void a(C0017o oVar, boolean z2, boolean z3, String str, int i) {
        C0003a.a("VponBannerController", "Call doExpandForSDKPlugin");
        if (this.q != null) {
            try {
                this.D = this.a.getRequestedOrientation();
                this.C = Resources.getSystem().getConfiguration().orientation;
                this.w.onPrepareExpandMode();
                this.F = str;
                this.E = z3;
                this.v = z2;
                if (!this.F.equals(NetworkManager.TYPE_NONE)) {
                    if (this.F.equals("portrait")) {
                        this.a.setRequestedOrientation(1);
                    } else if (this.F.equals("landscape")) {
                        this.a.setRequestedOrientation(0);
                    }
                } else if (!this.E) {
                    if (this.C == 2) {
                        this.a.setRequestedOrientation(0);
                    } else if (this.C == 1) {
                        this.a.setRequestedOrientation(1);
                    }
                }
                this.t = new RelativeLayout(this.a);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                this.t.setBackgroundColor(0);
                this.q.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                this.q.setVponWebViewId("bannerWebViewExpanded");
                this.q.setBackgroundColor(i);
                this.q.requestFocus();
                this.t.addView(this.q, layoutParams);
                ViewGroup viewGroup = (ViewGroup) this.a.findViewById(16908290);
                if (viewGroup.getRootView() != null) {
                    Rect rect = new Rect();
                    this.a.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                    this.t.setPadding(0, rect.top, 0, 0);
                    ((ViewGroup) viewGroup.getRootView()).addView(this.t);
                } else {
                    viewGroup.addView(this.t);
                }
                if (!this.v) {
                    C();
                }
                this.G = true;
                oVar.b();
                if (this.w != null) {
                    this.w.onVponPresent();
                }
                if (this.x) {
                    G();
                    this.O = true;
                }
            } catch (Exception e) {
                C0003a.a("VponBannerController", "doExpandForSDKPlugin throw Exception:" + e.getMessage(), e);
                try {
                    oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "doExpandForSDKPlugin throw execption:" + e.getMessage()));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                this.q.setVponWebViewId("bannerWebView");
                this.G = false;
                p();
            }
        }
    }

    public final void a(C0017o oVar, int i, int i2, int i3, int i4, String str) {
        TextView textView;
        RelativeLayout.LayoutParams layoutParams;
        int i5;
        int i6;
        C0003a.a("VponBannerController", "w:" + i + " h:" + i2 + " offX:" + i3 + " offY:" + i4);
        if (this.q != null) {
            try {
                this.D = this.a.getRequestedOrientation();
                this.C = Resources.getSystem().getConfiguration().orientation;
                Rect rect = new Rect();
                this.q.getGlobalVisibleRect(rect);
                Rect rect2 = new Rect();
                this.a.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect2);
                int i7 = rect2.top;
                int top = i7 + (this.a.getWindow().findViewById(16908290).getTop() - i7);
                int convertDpToPixel = (int) VponAdSize.convertDpToPixel((float) i, this.a);
                int convertDpToPixel2 = (int) VponAdSize.convertDpToPixel((float) i2, this.a);
                int convertDpToPixel3 = rect.left + ((int) VponAdSize.convertDpToPixel((float) i3, this.a));
                int convertDpToPixel4 = (rect.top + ((int) VponAdSize.convertDpToPixel((float) i4, this.a))) - top;
                if (convertDpToPixel3 < 0 || convertDpToPixel4 < 0) {
                    C0003a.b("VponBannerController", "leftMarginPix < 0 || topMarginPix < 0 at doResizeForSDKPlugin. leftMarginPix:" + convertDpToPixel3 + " topMarginPix:" + convertDpToPixel4);
                    oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "leftMarginPix < 0 || topMarginPix < 0 at doResizeForSDKPlugin"));
                    return;
                }
                int i8 = -1;
                int i9 = -1;
                int convertDpToPixel5 = (int) VponAdSize.convertDpToPixel(50.0f, this.a);
                if (!C0003a.c(str)) {
                    if (str.equals("top-right")) {
                        i8 = convertDpToPixel3 + (convertDpToPixel - convertDpToPixel5);
                        i9 = convertDpToPixel4;
                    } else if (str.equals("top-left")) {
                        i9 = convertDpToPixel4;
                        i8 = convertDpToPixel3;
                    } else if (str.equals("bottom-left")) {
                        i9 = (convertDpToPixel2 - convertDpToPixel5) + convertDpToPixel4;
                        i8 = convertDpToPixel3;
                    } else if (str.equals("bottom-right")) {
                        i8 = convertDpToPixel3 + (convertDpToPixel - convertDpToPixel5);
                        i9 = (convertDpToPixel2 - convertDpToPixel5) + convertDpToPixel4;
                    } else if (str.equals("top-center")) {
                        i8 = convertDpToPixel3 + ((convertDpToPixel / 2) - (convertDpToPixel5 / 2));
                        i9 = convertDpToPixel4;
                    } else if (str.equals("bottom-center")) {
                        i8 = convertDpToPixel3 + ((convertDpToPixel / 2) - (convertDpToPixel5 / 2));
                        i9 = (convertDpToPixel2 - convertDpToPixel5) + convertDpToPixel4;
                    } else if (str.equals("center")) {
                        i8 = convertDpToPixel3 + ((convertDpToPixel / 2) - (convertDpToPixel5 / 2));
                        i9 = ((convertDpToPixel2 / 2) - (convertDpToPixel5 / 2)) + convertDpToPixel4;
                    }
                    TextView textView2 = new TextView(this.a);
                    textView2.setWidth(convertDpToPixel5);
                    textView2.setHeight(convertDpToPixel5);
                    textView2.setBackgroundColor(0);
                    textView2.setOnClickListener(new View.OnClickListener() {
                        public final void onClick(View view) {
                            C.f(C.this);
                        }
                    });
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(convertDpToPixel5, convertDpToPixel5);
                    layoutParams2.leftMargin = i8;
                    layoutParams2.topMargin = i9;
                    textView = textView2;
                    layoutParams = layoutParams2;
                    i5 = i8;
                    i6 = i9;
                } else {
                    textView = null;
                    layoutParams = null;
                    i5 = -1;
                    i6 = -1;
                }
                C0003a.a("VponBannerController", " doResizeForSDKPlugin wPix:" + convertDpToPixel + " hPix:" + convertDpToPixel2 + " leftMarginPix:" + convertDpToPixel3 + " topMarginPix:" + convertDpToPixel4);
                this.w.onPrepareExpandMode();
                if (this.t != null) {
                    ((ViewGroup) this.a.findViewById(16908290)).removeView(this.t);
                    this.t.removeAllViews();
                    this.t = null;
                }
                this.t = new RelativeLayout(this.a);
                this.t.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(convertDpToPixel, convertDpToPixel2);
                layoutParams3.leftMargin = convertDpToPixel3;
                layoutParams3.topMargin = convertDpToPixel4;
                this.t.setBackgroundColor(0);
                this.q.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                this.q.setVponWebViewId("bannerWebViewResized");
                this.q.setBackgroundColor(0);
                this.q.bringToFront();
                this.q.requestFocus();
                this.t.addView(this.q, layoutParams3);
                if (textView != null && layoutParams != null && i5 >= 0 && i6 >= 0) {
                    this.t.addView(textView, layoutParams);
                }
                ((ViewGroup) this.a.findViewById(16908290)).addView(this.t);
                this.G = true;
                oVar.b();
                if (this.w != null) {
                    this.w.onVponPresent();
                }
                if (this.x) {
                    G();
                    this.O = true;
                }
            } catch (Exception e) {
                C0003a.a("VponBannerController", "doResizeForSDKPlugin throw Exception:" + e.getMessage(), e);
                try {
                    oVar.b(new JSONObject().put(AdActivity.INTENT_EXTRAS_PARAM, "doResizeForSDKPlugin throw execption:" + e.getMessage()));
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                this.q.setVponWebViewId("bannerWebView");
                this.G = false;
                p();
            }
        }
    }

    /* access modifiers changed from: private */
    public void C() {
        if (this.t != null) {
            this.u = new ImageView(this.a);
            this.u.setVisibility(0);
            this.u.setBackgroundDrawable(new BitmapDrawable(getClass().getResourceAsStream("/vpon_button_close50.png")));
            this.u.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    C.f(C.this);
                }
            });
            DisplayMetrics f = C0003a.f(this.a);
            int i = (int) (f.density * 50.0f);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i);
            layoutParams.leftMargin = f.widthPixels - i;
            this.t.addView(this.u, layoutParams);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.vpon.webview.VponAdWebView.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.vpon.webview.VponAdWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(java.lang.String, java.lang.Object):void
      c.CordovaWebView.a(vpadn.v, java.lang.String):void
      com.vpon.webview.VponAdWebView.a(boolean, boolean):void */
    static /* synthetic */ void f(C c2) {
        C0003a.a("VponBannerController", "ENTER doCloseExpand()");
        if (c2.q != null) {
            c2.q.b(true);
            c2.q.a(false, false);
        } else {
            C0003a.b("VponBannerController", "mShowWebView is NULL in doCloseExpand");
        }
        if (c2.t != null) {
            if (c2.G) {
                c2.G = false;
                if (c2.q != null) {
                    c2.q.setVponWebViewId("bannerWebView");
                    c2.q.setBackgroundColor(0);
                }
            }
            if (c2.t != null) {
                ((ViewGroup) c2.a.findViewById(16908290)).removeView(c2.t);
                c2.t.removeAllViews();
                c2.t = null;
            }
            c2.a.setRequestedOrientation(c2.D);
            c2.w.onLeaveExpandMode();
            if (c2.x && c2.s != null && c2.y == null && c2.O) {
                if (!c2.P) {
                    c2.b(c2.s.b);
                }
                c2.O = false;
            }
            c2.p();
            return;
        }
        C0003a.b("VponBannerController", "mExpandRelativeLayout is null at doCloseExpand()");
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void b(boolean z2) {
        this.x = z2;
        if (C0003a.c(this.b)) {
            C0003a.b("VponBannerController", "Invalid Banner ID!!");
        }
        if (this.a == null) {
            return;
        }
        if (Y.d(this.a)) {
            String str = (String) this.g.get("url_type_banner");
            if (str != null) {
                StringBuilder sb = new StringBuilder("endRequestToServer mRequestCount:");
                int i = this.K + 1;
                this.K = i;
                C0003a.a("VponBannerController", sb.append(i).append(" mLoadInitHtmlTimeOutCount:").append(this.M).toString());
                Y.a();
                new H(str, this, this.e).execute(new Object[0]);
                return;
            }
            C0003a.b("VponBannerController", "mUrlMap.get(VponControllerInterface.URL_TYPE_BANNER) return null");
            return;
        }
        C0003a.b("VponBannerController", "permission-checking is failed!!");
    }

    public final void a(VponAdRequest vponAdRequest) {
        try {
            C0003a.a("VponBannerController", "---->enter loadInitHtmlTemplate");
            final int i = this.L;
            this.f = false;
            this.P = true;
            this.m = vponAdRequest;
            this.x = vponAdRequest.isAutoRefresh();
            H();
            AnonymousClass14 r1 = new TimerTask() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: vpadn.C.b(vpadn.C, long):void
                 arg types: [vpadn.C, int]
                 candidates:
                  vpadn.C.b(vpadn.C, int):void
                  vpadn.C.b(vpadn.C, boolean):void
                  vpadn.C.b(vpadn.C, long):void */
                public final void run() {
                    if (i == C.this.L) {
                        C0003a.b("VponBannerController", "TIMEOUT FOR LOAD INIT HTML TO BANNER");
                        C c2 = C.this;
                        c2.M = c2.M + 1;
                        C c3 = C.this;
                        c3.N = c3.N + 1;
                        C.this.a.runOnUiThread(new Runnable() {
                            public final void run() {
                                C.this.L();
                            }
                        });
                        C.this.b(1L);
                        return;
                    }
                    C.this.N = 0;
                }
            };
            I();
            this.z = 0;
            this.H = 0;
            if (!l()) {
                C0003a.b("VponBannerController", "Device is not on-line");
                if (this.w != null) {
                    this.w.onVponAdFailed(VponAdRequest.VponErrorCode.NETWORK_ERROR);
                }
                if (this.x) {
                    this.B = new Timer();
                    this.B.schedule(r1, 60000);
                    return;
                }
                return;
            }
            if (this.x) {
                this.B = new Timer();
                this.B.schedule(r1, 60000);
            }
            if (!((PowerManager) this.a.getSystemService("power")).isScreenOn()) {
                C0003a.d("VponBannerController", "ScreenOff");
                if (this.x) {
                    b(10);
                }
                if (this.w != null) {
                    this.w.onVponAdFailed(VponAdRequest.VponErrorCode.INTERNAL_ERROR);
                    return;
                }
                return;
            }
            JSONObject jSONObject = new JSONObject();
            JSONObject b2 = M.a().b(this.a, (JSONObject) null);
            if (this.d.equals("tw")) {
                jSONObject.put("pf", "TW");
                b2.put("pf", "TW");
            } else {
                jSONObject.put("pf", "CN");
                b2.put("pf", "CN");
            }
            i();
            JSONObject a2 = a(b2, vponAdRequest, this.j, this.k);
            a2.put("build", "20803102");
            String replaceAll = "<!doctype html> <html> <head> <meta charset='utf-8'/>\n<script type='text/javascript' charset='utf-8' src='http://m.vpon.com/sdk/vpadn-sdk-core-v1.js'></script>\n<script type='text/javascript' charset='utf-8'>\nVPSDK_LoadSdkConstants( JSON_REPLACE1 );\nVPSDK_BuildAdReqUrl( JSON_REPLACE2 );\n</script><body></body></html>".replaceAll("JSON_REPLACE1", jSONObject.toString(4)).replaceAll("JSON_REPLACE2", a2.toString(4));
            C0003a.a("VponBannerController", replaceAll);
            J();
            this.p = new VponAdWebView("init", this.a, this, this);
            this.p.loadDataWithBaseURL("file:///android_asset/www/vpon", replaceAll, "text/html", "utf-8", null);
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.b("VponBannerController", "loadInitHtmlTemplate throw Exception: " + e.getMessage());
        }
    }

    private String D() {
        try {
            JSONObject a2 = M.a().a((JSONObject) null, this.a);
            int intValue = ((Integer) a2.get("u_w")).intValue();
            double intValue2 = ((double) ((Integer) a2.get("u_h")).intValue()) * 0.125d;
            boolean z2 = false;
            if (this.f101c.getMixModeCustomStr().equals(VponAdSize.MIX_CUSTOME_HEIGHT)) {
                z2 = true;
            }
            if (z2) {
                this.J = this.f101c.getHeightInPixels(this.a);
                if (intValue >= 728 && intValue2 >= 90.0d) {
                    this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                    return "728x90_mb";
                } else if (intValue < 468 || intValue2 < 60.0d) {
                    this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                    return "320x50_mb";
                } else {
                    this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                    return "468x60_mb";
                }
            } else {
                this.I = this.f101c.getWidthInPixels(this.a);
                if (intValue >= 728 && intValue2 >= 90.0d) {
                    this.J = (int) VponAdSize.convertDpToPixel(90.0f, this.a);
                    return "728x90_mb";
                } else if (intValue < 468 || intValue2 < 60.0d) {
                    this.J = (int) VponAdSize.convertDpToPixel(50.0f, this.a);
                    return "320x50_mb";
                } else {
                    this.J = (int) VponAdSize.convertDpToPixel(60.0f, this.a);
                    return "468x60_mb";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private String E() {
        try {
            JSONObject a2 = M.a().a((JSONObject) null, this.a);
            int intValue = ((Integer) a2.get("u_w")).intValue();
            int intValue2 = ((Integer) a2.get("u_h")).intValue();
            C0003a.c("VponBannerController", "unitWidthDip:" + intValue + " unitHeightDip:" + intValue2);
            double d = ((double) intValue2) * 0.125d;
            if (intValue >= 728 && d >= 90.0d) {
                this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                this.J = (int) VponAdSize.convertDpToPixel(90.0f, this.a);
                return "728x90_mb";
            } else if (intValue >= 468 && d >= 60.0d) {
                this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                this.J = (int) VponAdSize.convertDpToPixel(60.0f, this.a);
                return "468x60_mb";
            } else if (intValue < 320 || d < 50.0d) {
                this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                this.J = (int) VponAdSize.convertDpToPixel(32.0f, this.a);
                return "480x32_mb";
            } else {
                this.I = (int) VponAdSize.convertDpToPixel((float) intValue, this.a);
                this.J = (int) VponAdSize.convertDpToPixel(50.0f, this.a);
                return "320x50_mb";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject a(JSONObject jSONObject, VponAdRequest vponAdRequest, long j, long j2) {
        try {
            jSONObject.put("sid", j);
            jSONObject.put("seq", j2);
            if (this.f101c.equals(VponAdSize.SMART_BANNER)) {
                jSONObject.put("format", E());
            } else if (this.f101c.isMix()) {
                jSONObject.put("format", D());
            } else if (this.f101c.isCustomAdSize()) {
                this.I = this.f101c.getWidthInPixels(this.a);
                this.J = this.f101c.getHeightInPixels(this.a);
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("w", this.f101c.getWidth());
                jSONObject2.put("h", this.f101c.getHeight());
                jSONObject.put("ad_frame", jSONObject2);
            } else {
                this.I = this.f101c.getWidthInPixels(this.a);
                this.J = this.f101c.getHeightInPixels(this.a);
                jSONObject.put("format", this.f101c.getWidth() + "x" + this.f101c.getHeight() + "_mb");
            }
            jSONObject.put("bid", this.b);
            if (vponAdRequest.isTestDevice(this.a)) {
                jSONObject.put("adtest", 1);
            } else {
                jSONObject.put("adtest", 0);
            }
            if (this.i.length() >= 4) {
                if (this.i.has("x")) {
                    jSONObject.put("ad_x", this.i.getInt("x"));
                }
                if (this.i.has("y")) {
                    jSONObject.put("ad_y", this.i.getInt("y"));
                }
                if (this.i.has("w")) {
                    jSONObject.put("ad_w", this.i.getInt("w"));
                }
                if (this.i.has("h")) {
                    jSONObject.put("ad_h", this.i.getInt("h"));
                }
            } else {
                jSONObject.put("ad_x", 0);
                jSONObject.put("ad_y", 0);
                jSONObject.put("ad_w", 0);
                jSONObject.put("ad_h", 0);
            }
            if (this.h) {
                jSONObject.put("ad_v", 1);
            } else {
                jSONObject.put("ad_v", 0);
            }
            Set<String> keywords = vponAdRequest.getKeywords();
            JSONArray jSONArray = new JSONArray();
            for (String put : keywords) {
                jSONArray.put(put);
            }
            jSONObject.put("kw", jSONArray);
            JSONObject c2 = M.a().c(this.a, (JSONObject) null);
            int age = vponAdRequest.getAge();
            if (age > 0 && age < 150) {
                c2.put("age", age);
            }
            if (!vponAdRequest.getGender().equals(VponAdRequest.Gender.UNKNOWN)) {
                if (vponAdRequest.getGender().equals(VponAdRequest.Gender.MALE)) {
                    c2.put("gender", 0);
                } else {
                    c2.put("gender", 1);
                }
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-DD");
            Date birthday = vponAdRequest.getBirthday();
            if (birthday != null) {
                c2.put("bday", simpleDateFormat.format(birthday));
            }
            String jSONObject3 = c2.toString();
            C0003a.a("VponBannerController", "secretJsonObjStr:" + jSONObject3);
            jSONObject.put("ms", C0003a.e("NH/mLeyCBfokzYKUPNGEEg==", jSONObject3));
        } catch (Exception e) {
            e.printStackTrace();
            C0003a.a("VponBannerController", "collectPushlierParams throw Exception", e);
        }
        return jSONObject;
    }

    public final void a(Location location) {
        if (location != null && this.q != null && !this.f && this.s != null) {
            P p2 = this.s;
        }
    }

    public final void o() {
        this.n = true;
        if (this.w != null) {
            this.w.onVponPresent();
        }
    }

    public final void p() {
        if (this.n) {
            this.n = false;
        }
        if (this.w != null) {
            this.w.onVponDismiss();
        }
    }

    public final void q() {
        if (this.w != null) {
            this.w.onVponLeaveApplication();
        }
    }

    public final void r() {
        this.h = true;
        a("onshow", (JSONObject) null);
        if (this.n) {
            this.n = false;
            p();
        }
    }

    public final void s() {
        this.h = false;
        a("onhide", (JSONObject) null);
    }

    public final void t() {
        if (this.p == null || !this.p.h().equals("init")) {
            C0003a.a("VponBannerController", "onWebViewLoadPageFinish -> showBanner");
            J();
            C0003a.a("VponBannerController", "enter showBanner");
            this.P = false;
            this.L++;
            if (!this.G) {
                K();
            }
            if (this.o != null && this.r != null) {
                if (this.x) {
                    b(this.r.b);
                }
                if (!this.G) {
                    this.q = this.o;
                    this.q.setBackgroundColor(0);
                    this.s = this.r;
                    this.w.onControllerWebViewReady(this.I, this.J);
                    X.a(this.a);
                    a(X.a());
                    Q.a().c();
                    F();
                }
            } else if (this.o == null) {
                C0003a.b("VponBannerController", " mPrepareWebView == null at showBanner");
            } else {
                C0003a.b("VponBannerController", " mPrepareAd == null at showBanner");
            }
        } else {
            this.p.setVponWebViewId("init-finish");
            C0003a.c("VponBannerController", "Load init html template finish");
        }
    }

    public final void a(int i, int i2) {
        C0003a.c("VponBannerController", "Call onWebViewSizeChanged");
        if (this.q != null && this.a != null && !this.v && this.G && this.t != null && this.u != null && !this.v) {
            new Handler().post(new Runnable() {
                public final void run() {
                    if (C.this.t.getChildCount() >= 2) {
                        C.this.t.removeView(C.this.u);
                        C.this.C();
                    }
                }
            });
        }
    }

    public final void a(int i, int i2, int i3, int i4) {
        C0003a.a("VponBannerController", "onWebViewLayoutChanged Left:" + i + " top:" + i2);
        if (this.q != null) {
            C0003a.a("VponBannerController", "globalvisible Left:" + i + " top:" + i2);
            int round = Math.round(VponAdSize.convertPixelsToDp((float) i, this.a));
            int round2 = Math.round(VponAdSize.convertPixelsToDp((float) i2, this.a));
            int round3 = Math.round(VponAdSize.convertPixelsToDp((float) (i3 - i), this.a));
            int round4 = Math.round(VponAdSize.convertPixelsToDp((float) (i4 - i2), this.a));
            C0003a.c("VponBannerController", "onWebViewLayoutChanged: X1:" + round + " Y1:" + round2 + " wDip:" + round3 + " hDip:" + round4);
            try {
                this.i.put("x", round);
                this.i.put("y", round2);
                this.i.put("w", round3);
                this.i.put("h", round4);
                a("ad_pos_change", this.i);
            } catch (Exception e) {
                e.printStackTrace();
                C0003a.b("VponBannerController", "onWebViewLayoutChanged throw exception");
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: vpadn.Q.b(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      vpadn.Q.b(org.apache.http.HttpResponse, boolean):void
      vpadn.Q.b(java.lang.String, boolean):void */
    public final void a(WebView webView, int i, String str, String str2) {
        VponAdWebView vponAdWebView;
        String str3 = "onWebViewReceivedError errorCode:" + i + " des:" + str + " failingUrl:" + str2;
        C0003a.b("VponBannerController", str3);
        Q.a().b("[ERROR] " + str3, true);
        if (webView == null) {
            C0003a.b("VponBannerController", "webView is null in onWebViewReceivedError");
            vponAdWebView = null;
        } else {
            vponAdWebView = (VponAdWebView) webView;
            C0003a.b("VponBannerController", "vponWebView ID:" + vponAdWebView.h());
            Q.a().b("[ERROR] vponWebView ID:" + vponAdWebView.h(), true);
        }
        if (!vponAdWebView.h().equals("init")) {
            this.a.runOnUiThread(new Runnable(this) {
                public final void run() {
                    Q.a().d();
                }
            });
        } else if (this.w != null) {
            this.a.runOnUiThread(new Runnable() {
                public final void run() {
                    C.this.w.onVponAdFailed(VponAdRequest.VponErrorCode.INVALID_REQUEST);
                }
            });
        }
    }

    public final void b(final Object obj) {
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                P p = (P) obj;
                if (C.this.l != -1) {
                    p.b = C.this.l;
                } else {
                    C0003a.b("VponBannerController", "Cannot get refresh time, set isFresh field to false");
                    C.this.x = false;
                }
                C.a(C.this, p);
                C c2 = C.this;
            }
        });
    }

    public final void u() {
        C0003a.a("VponBannerController", "call onVponBannerImpression");
        if (this.s != null) {
            this.s.f107c = true;
        }
    }

    public final void a(F f) {
        C0003a.b("VponBannerController", "call onVponBannerImpressionFailed VponReturnCode:" + f.a());
        if (this.s != null) {
            this.s.f107c = true;
        }
    }

    static /* synthetic */ void a(C c2, P p2) {
        C0003a.a("VponBannerController", "Enter perpareBanner");
        if (c2.a == null || c2.f) {
            if (c2.a == null) {
                C0003a.b("VponBannerController", "prepareBanner mContext == null");
            }
            if (c2.f) {
                C0003a.b("VponBannerController", "prepareBanner mIsDestroy == true");
                return;
            }
            return;
        }
        c2.o = new VponAdWebView("bannerWebView", c2.a, c2, c2);
        c2.r = p2;
        String str = p2.a;
        C0003a.a("VponBannerController", "real get BannerHtml:" + str);
        C0003a.c("VponBannerController", "/////////////Banner OK!!//////////////////");
        c2.b();
        String str2 = (String) c2.g.get("url_type_banner");
        int lastIndexOf = str2.lastIndexOf(47);
        C0003a.a("VponBannerController", "baseUrl:" + (lastIndexOf > 0 ? str2.substring(0, lastIndexOf + 1) : String.valueOf(str2) + "/"));
        c2.o.loadDataWithBaseURL(str2, str, "text/html", "utf-8", null);
    }

    static /* synthetic */ void o(C c2) {
        if (c2.a != null && c2.s != null && !c2.s.f107c) {
            final String str = (String) c2.g.remove("url_type_impression");
            if (str != null) {
                C0003a.a("VponBannerController", "Send impression to server impressionUrl:" + str);
                new G(str, c2, c2.e).execute(new Object[0]);
                c2.a.runOnUiThread(new Runnable(c2) {
                    public final void run() {
                        Q.a().a(str);
                    }
                });
                return;
            }
            C0003a.b("VponBannerController", "Cannot get impression URL");
        }
    }

    /* access modifiers changed from: private */
    public void F() {
        int i = 1;
        if (O.a().c("viewable_duration")) {
            i = (((Integer) O.a().a("viewable_duration")).intValue() / 500) + 1;
        }
        if (!a((View) this.q)) {
            this.H++;
            if (this.H == i) {
                this.a.runOnUiThread(new Runnable() {
                    public final void run() {
                        C.o(C.this);
                    }
                });
                this.z = 0;
                this.H = 0;
                I();
            } else if (this.f) {
                C0003a.d("VponBannerController", "mIsDestroy == true NO need to create mCoveredCheckTimer");
                this.z = 0;
                this.H = 0;
                I();
            } else {
                if (this.A == null) {
                    this.A = new Timer();
                }
                this.A.schedule(new a(this, (byte) 0), 500);
            }
        } else {
            C0003a.a("VponBannerController", "Cover test fail mTimesOfBlockViewCheck:" + this.z);
            if (this.z < 60) {
                this.z++;
                long j = 1000;
                if (this.z > 30) {
                    j = 5000;
                }
                if (this.f) {
                    C0003a.d("VponBannerController", "mIsDestroy == true NO need to create mCoveredCheckTimer");
                    this.z = 0;
                    this.H = 0;
                    I();
                    return;
                }
                if (this.A == null) {
                    this.A = new Timer();
                }
                this.H = 0;
                this.A.schedule(new a(this, (byte) 0), j);
                return;
            }
            this.z = 0;
            this.H = 0;
        }
    }

    class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(C c2, byte b) {
            this();
        }

        public final void run() {
            if (C.this.s == null || !C.this.s.f107c) {
                int i = 1000;
                if (C.this.H > 0) {
                    i = 500;
                }
                C.this.F();
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(long j) {
        G();
        C0003a.a("VponBannerController", "refreshRequestTimerStart second:" + j);
        this.y = new Timer();
        this.y.schedule(new b(this, (byte) 0), 1000 * j);
    }

    class b extends TimerTask {
        private b() {
        }

        /* synthetic */ b(C c2, byte b) {
            this();
        }

        public final void run() {
            if (!C.this.G) {
                C.this.a.runOnUiThread(new Runnable() {
                    public final void run() {
                        C.this.a(C.this.m);
                    }
                });
            }
        }
    }

    public final void v() {
        try {
            H();
            G();
            I();
        } catch (Exception e) {
            C0003a.a("VponBannerController", "cancelTimer() throws Exception!", e);
        }
    }

    private synchronized void G() {
        if (this.y != null) {
            C0003a.a("VponBannerController", "cancelAutoRefreshTimer()");
            this.y.cancel();
            this.y.purge();
            this.y = null;
        }
    }

    private synchronized void H() {
        if (this.B != null) {
            C0003a.a("VponBannerController", "mCheckLoadInitHtmlToShowBannerTimer()");
            this.B.cancel();
            this.B.purge();
            this.B = null;
        }
    }

    private synchronized void I() {
        if (this.A != null) {
            C0003a.a("VponBannerController", "cancelCoveredCheckTimer()");
            this.A.cancel();
            this.A.purge();
            this.A = null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005d, code lost:
        r4 = r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(android.view.View r12) {
        /*
            r11 = this;
            r7 = 0
            r3 = 0
            r2 = 1
            if (r12 != 0) goto L_0x0007
        L_0x0006:
            return r2
        L_0x0007:
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            boolean r4 = r12.getGlobalVisibleRect(r1)
            int r0 = r1.bottom
            int r5 = r1.top
            int r0 = r0 - r5
            int r5 = r12.getMeasuredHeight()
            if (r0 < r5) goto L_0x003d
            r0 = r2
        L_0x001c:
            int r5 = r1.right
            int r1 = r1.left
            int r1 = r5 - r1
            int r5 = r12.getMeasuredWidth()
            if (r1 < r5) goto L_0x003f
            r1 = r2
        L_0x0029:
            if (r4 == 0) goto L_0x0041
            if (r0 == 0) goto L_0x0041
            if (r1 == 0) goto L_0x0041
            r0 = r2
        L_0x0030:
            if (r0 == 0) goto L_0x0006
            r4 = r12
        L_0x0033:
            android.view.ViewParent r0 = r4.getParent()
            boolean r0 = r0 instanceof android.view.ViewGroup
            if (r0 != 0) goto L_0x0043
            r2 = r3
            goto L_0x0006
        L_0x003d:
            r0 = r3
            goto L_0x001c
        L_0x003f:
            r1 = r3
            goto L_0x0029
        L_0x0041:
            r0 = r3
            goto L_0x0030
        L_0x0043:
            android.view.ViewParent r0 = r4.getParent()
            r1 = r0
            android.view.ViewGroup r1 = (android.view.ViewGroup) r1
            int r0 = r1.getVisibility()
            if (r0 != 0) goto L_0x0006
            int r0 = a(r4, r1)
            int r0 = r0 + 1
            r4 = r0
        L_0x0057:
            int r0 = r1.getChildCount()
            if (r4 < r0) goto L_0x005f
            r4 = r1
            goto L_0x0033
        L_0x005f:
            android.graphics.Rect r6 = new android.graphics.Rect
            r6.<init>()
            r12.getGlobalVisibleRect(r6)
            android.view.View r9 = r1.getChildAt(r4)
            int r0 = r9.getVisibility()
            r5 = 8
            if (r0 == r5) goto L_0x010e
            int r0 = r9.getVisibility()
            r5 = 4
            if (r0 == r5) goto L_0x010e
            android.graphics.drawable.Drawable r0 = r9.getBackground()
            boolean r0 = r0 instanceof android.graphics.drawable.ColorDrawable
            if (r0 == 0) goto L_0x0126
            int r0 = android.os.Build.VERSION.SDK_INT
            r5 = 11
            if (r0 >= r5) goto L_0x0113
            android.graphics.Bitmap r0 = r11.Q
            if (r0 != 0) goto L_0x00a4
            android.graphics.Bitmap$Config r0 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r0 = android.graphics.Bitmap.createBitmap(r2, r2, r0)
            r11.Q = r0
            android.graphics.Canvas r0 = new android.graphics.Canvas
            android.graphics.Bitmap r5 = r11.Q
            r0.<init>(r5)
            r11.R = r0
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r11.S = r0
        L_0x00a4:
            android.graphics.drawable.Drawable r0 = r9.getBackground()
            android.graphics.drawable.ColorDrawable r0 = (android.graphics.drawable.ColorDrawable) r0
            android.graphics.Rect r5 = r11.S
            android.graphics.Rect r10 = r0.getBounds()
            r5.set(r10)
            r0.setBounds(r3, r3, r2, r2)
            android.graphics.Canvas r5 = r11.R
            r0.draw(r5)
            android.graphics.Bitmap r5 = r11.Q
            int r5 = r5.getPixel(r3, r3)
            android.graphics.Rect r10 = r11.S
            r0.setBounds(r10)
            r0 = r5
        L_0x00c7:
            int r0 = r0 >>> 24
            if (r0 != 0) goto L_0x011e
            r0 = r2
        L_0x00cc:
            if (r0 != 0) goto L_0x010e
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r9.getGlobalVisibleRect(r0)
            boolean r5 = android.graphics.Rect.intersects(r6, r0)
            if (r5 == 0) goto L_0x010e
            r0.setIntersect(r6, r0)
            int r5 = r0.width()
            int r0 = r0.height()
            int r0 = r0 * r5
            double r9 = (double) r0
            int r0 = r6.width()
            int r5 = r6.height()
            int r0 = r0 * r5
            double r5 = (double) r0
            double r9 = r9 / r5
            vpadn.O r0 = vpadn.O.a()     // Catch:{ Exception -> 0x0120 }
            java.lang.String r5 = "viewable_rate"
            java.lang.Object r0 = r0.a(r5)     // Catch:{ Exception -> 0x0120 }
            java.lang.Double r0 = (java.lang.Double) r0     // Catch:{ Exception -> 0x0120 }
            double r5 = r0.doubleValue()     // Catch:{ Exception -> 0x0120 }
        L_0x0104:
            int r0 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r0 > 0) goto L_0x010a
            r5 = 4602678819172646912(0x3fe0000000000000, double:0.5)
        L_0x010a:
            int r0 = (r9 > r5 ? 1 : (r9 == r5 ? 0 : -1))
            if (r0 > 0) goto L_0x0006
        L_0x010e:
            int r0 = r4 + 1
            r4 = r0
            goto L_0x0057
        L_0x0113:
            android.graphics.drawable.Drawable r0 = r9.getBackground()
            android.graphics.drawable.ColorDrawable r0 = (android.graphics.drawable.ColorDrawable) r0
            int r0 = r0.getColor()
            goto L_0x00c7
        L_0x011e:
            r0 = r3
            goto L_0x00cc
        L_0x0120:
            r0 = move-exception
            r0.printStackTrace()
            r5 = r7
            goto L_0x0104
        L_0x0126:
            r0 = r3
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: vpadn.C.a(android.view.View):boolean");
    }

    private static int a(View view, ViewGroup viewGroup) {
        int i = 0;
        while (i < viewGroup.getChildCount() && viewGroup.getChildAt(i) != view) {
            i++;
        }
        return i;
    }

    public final void w() {
        C0003a.c("VponBannerController", "call webViewHandleDestroy");
        this.f = true;
        if (this.q != this.o || this.q == null) {
            L();
            K();
        } else {
            K();
        }
        J();
    }

    private synchronized void J() {
        if (this.p != null) {
            C0003a.a("VponBannerController", "destroy mInitWebView");
            this.p.stopLoading();
            this.p.removeAllViews();
            this.p.e();
            this.p = null;
        }
    }

    private synchronized void K() {
        if (this.q != null) {
            C0003a.a("VponBannerController", "destroy mShowWebView");
            this.q.stopLoading();
            this.q.removeAllViews();
            this.q.e();
            this.q = null;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void L() {
        if (this.o != null) {
            C0003a.a("VponBannerController", "destroy mPrepareWebView");
            this.o.stopLoading();
            this.o.removeAllViews();
            this.o.e();
            this.o = null;
        }
    }

    public final boolean x() {
        if (this.q != null) {
            return true;
        }
        return false;
    }

    public final void y() {
        q();
    }

    public final void z() {
        new Handler().post(new Runnable() {
            public final void run() {
                C.f(C.this);
            }
        });
    }

    public final void A() {
        if (this.x) {
            G();
        }
    }

    public final void B() {
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                if (C.this.x && !C.this.P && C.this.s != null && C.this.y == null) {
                    C.this.b(C.this.s.b);
                }
            }
        });
    }
}
