package vpadn;

import android.util.Log;
import c.CordovaWebView;
import org.json.JSONObject;
import vpadn.C0024v;

/* renamed from: vpadn.o  reason: case insensitive filesystem */
public final class C0017o {
    public String a;
    boolean b;

    /* renamed from: c  reason: collision with root package name */
    private CordovaWebView f122c;

    public C0017o(String str, CordovaWebView cordovaWebView) {
        this.a = str;
        this.f122c = cordovaWebView;
    }

    public final String a() {
        return this.a;
    }

    public final void a(C0024v vVar) {
        boolean z;
        synchronized (this) {
            if (this.b) {
                Log.w("CordovaPlugin", "Attempted to send a second callback for ID: " + this.a + "\nResult was: " + vVar.c());
                return;
            }
            if (vVar.e()) {
                z = false;
            } else {
                z = true;
            }
            this.b = z;
            this.f122c.a(vVar, this.a);
        }
    }

    public final void a(JSONObject jSONObject) {
        a(new C0024v(C0024v.a.OK, jSONObject));
    }

    public final void a(String str) {
        a(new C0024v(C0024v.a.OK, str));
    }

    public final void b() {
        a(new C0024v(C0024v.a.OK));
    }

    public final void b(JSONObject jSONObject) {
        a(new C0024v(C0024v.a.ERROR, jSONObject));
    }

    public final void b(String str) {
        a(new C0024v(C0024v.a.ERROR, str));
    }

    public final void a(int i) {
        a(new C0024v(C0024v.a.ERROR, i));
    }
}
