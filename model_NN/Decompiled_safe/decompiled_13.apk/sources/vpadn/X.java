package vpadn;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import java.util.LinkedList;
import java.util.List;

public class X implements LocationListener {
    private static X a;
    private static Location d = null;
    private LocationManager b = null;

    /* renamed from: c  reason: collision with root package name */
    private List<LocationListener> f111c = new LinkedList();
    private boolean e = false;
    private boolean f = false;

    private X(Context context) {
        try {
            this.b = (LocationManager) context.getSystemService("location");
            if (Y.b(context)) {
                this.e = this.b.isProviderEnabled("gps");
            }
            this.f = this.b.isProviderEnabled("network");
            C0003a.a("VponLocation", "isGpsProviderEnable:" + this.e + " isNetworkProvideEnable:" + this.f);
            if (this.e || this.f) {
                String bestProvider = this.b.getBestProvider(new Criteria(), true);
                C0003a.a("VponLocation", "Best Location Provide:" + bestProvider);
                d = this.b.getLastKnownLocation(bestProvider);
                return;
            }
            C0003a.b("VponLocation", "new VponLocation failed");
        } catch (Exception e2) {
            e2.printStackTrace();
            C0003a.a("VponLocation", "requestLocationUpdates throws Exception:" + e2.getMessage(), e2);
        }
    }

    public static X a(Context context) {
        if (a == null) {
            synchronized (X.class) {
                if (a == null) {
                    a = new X(context);
                }
            }
        }
        return a;
    }

    public static Location a() {
        return d;
    }

    public final synchronized void a(LocationListener locationListener) {
        if (this.f111c.size() == 0) {
            if (this.e) {
                this.b.requestLocationUpdates("gps", 2000, 10.0f, this);
            }
            if (this.f) {
                this.b.requestLocationUpdates("network", 2000, 10.0f, this);
            }
        }
        this.f111c.remove(locationListener);
        this.f111c.add(locationListener);
    }

    public final synchronized void b(LocationListener locationListener) {
        this.f111c.remove(locationListener);
        if (this.f111c.size() == 0) {
            this.b.removeUpdates(this);
        }
    }

    public void onLocationChanged(Location location) {
        C0003a.a("VponLocation", "onLocationChanged");
        d = location;
        for (LocationListener onLocationChanged : this.f111c) {
            onLocationChanged.onLocationChanged(location);
        }
    }

    public void onProviderDisabled(String str) {
        for (LocationListener onProviderDisabled : this.f111c) {
            onProviderDisabled.onProviderDisabled(str);
        }
    }

    public void onProviderEnabled(String str) {
        for (LocationListener onProviderEnabled : this.f111c) {
            onProviderEnabled.onProviderEnabled(str);
        }
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        for (LocationListener onStatusChanged : this.f111c) {
            onStatusChanged.onStatusChanged(str, i, bundle);
        }
    }
}
