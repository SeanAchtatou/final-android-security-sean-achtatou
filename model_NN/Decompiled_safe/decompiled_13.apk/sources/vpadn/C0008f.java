package vpadn;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import c.CordovaWebView;
import java.util.Hashtable;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: vpadn.f  reason: case insensitive filesystem */
public class C0008f extends WebViewClient {
    C0018p a;
    CordovaWebView b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f115c = false;
    private Hashtable<String, C0003a> d = new Hashtable<>();

    public C0008f(C0018p pVar, CordovaWebView cordovaWebView) {
        this.a = pVar;
        this.b = cordovaWebView;
    }

    public final void a(CordovaWebView cordovaWebView) {
        this.b = cordovaWebView;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String substring;
        if (this.b.a == null || !this.b.a.b(str)) {
            if (str.startsWith("tel:")) {
                try {
                    Intent intent = new Intent("android.intent.action.DIAL");
                    intent.setData(Uri.parse(str));
                    this.a.a().startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    C0020r.e("Cordova", "Error dialing " + str + ": " + e.toString());
                }
            } else if (str.startsWith("geo:")) {
                try {
                    Intent intent2 = new Intent("android.intent.action.VIEW");
                    intent2.setData(Uri.parse(str));
                    this.a.a().startActivity(intent2);
                } catch (ActivityNotFoundException e2) {
                    C0020r.e("Cordova", "Error showing map " + str + ": " + e2.toString());
                }
            } else if (str.startsWith("mailto:")) {
                try {
                    Intent intent3 = new Intent("android.intent.action.VIEW");
                    intent3.setData(Uri.parse(str));
                    this.a.a().startActivity(intent3);
                } catch (ActivityNotFoundException e3) {
                    C0020r.e("Cordova", "Error sending email " + str + ": " + e3.toString());
                }
            } else if (str.startsWith("sms:")) {
                try {
                    Intent intent4 = new Intent("android.intent.action.VIEW");
                    int indexOf = str.indexOf(63);
                    if (indexOf == -1) {
                        substring = str.substring(4);
                    } else {
                        substring = str.substring(4, indexOf);
                        String query = Uri.parse(str).getQuery();
                        if (query != null && query.startsWith("body=")) {
                            intent4.putExtra("sms_body", query.substring(5));
                        }
                    }
                    intent4.setData(Uri.parse("sms:" + substring));
                    intent4.putExtra("address", substring);
                    intent4.setType("vnd.android-dir/mms-sms");
                    this.a.a().startActivity(intent4);
                } catch (ActivityNotFoundException e4) {
                    C0020r.e("Cordova", "Error sending sms " + str + ":" + e4.toString());
                }
            } else if (!str.startsWith("file://") && !str.startsWith("data:") && str.indexOf(this.b.f4c) != 0 && !C0004b.a(str)) {
                try {
                    Intent intent5 = new Intent("android.intent.action.VIEW");
                    intent5.setData(Uri.parse(str));
                    this.a.a().startActivity(intent5);
                } catch (ActivityNotFoundException e5) {
                    C0020r.b("Cordova", "Error loading url " + str, e5);
                }
            } else if (this.b.d || str.startsWith("data:")) {
                return false;
            } else {
                this.b.loadUrl(str);
            }
        }
        return true;
    }

    public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
        C0003a aVar = this.d.get(str.concat(str2));
        if (aVar == null) {
            aVar = this.d.get(str);
            if (aVar == null) {
                aVar = this.d.get(str2);
            }
            if (aVar == null) {
                aVar = this.d.get("");
            }
        }
        if (aVar != null) {
            httpAuthHandler.proceed(null, null);
        } else {
            super.onReceivedHttpAuthRequest(webView, httpAuthHandler, str, str2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(vpadn.v, java.lang.String):void
      c.CordovaWebView.a(boolean, boolean):void
      c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (!this.b.d) {
            webView.clearHistory();
            this.f115c = true;
        }
        this.b.f.a();
        this.b.a("onPageStarted", (Object) str);
        if (this.b.a != null) {
            this.b.a.c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
      c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
      c.CordovaWebView.a(vpadn.v, java.lang.String):void
      c.CordovaWebView.a(boolean, boolean):void
      c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        C0020r.b("Cordova", "onPageFinished(" + str + ")");
        if (this.f115c) {
            webView.clearHistory();
            this.f115c = false;
        }
        this.b.e++;
        if (!str.equals("about:blank")) {
            C0020r.b("Cordova", "Trying to fire onNativeReady");
            this.b.loadUrl("javascript:try{ cordova.require('cordova/channel').onNativeReady.fire();}catch(e){_nativeReady = true;}");
            this.b.a("onNativeReady", (Object) null);
        }
        this.b.a("onPageFinished", (Object) str);
        if (this.b.getVisibility() == 4) {
            new Thread(new Runnable() {
                public final void run() {
                    try {
                        Thread.sleep(2000);
                        C0008f.this.a.a().runOnUiThread(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: c.CordovaWebView.a(java.lang.String, java.lang.Object):void
                             arg types: [java.lang.String, java.lang.String]
                             candidates:
                              c.CordovaWebView.a(java.lang.String, java.lang.String):java.lang.String
                              c.CordovaWebView.a(android.view.View, android.webkit.WebChromeClient$CustomViewCallback):void
                              c.CordovaWebView.a(vpadn.v, java.lang.String):void
                              c.CordovaWebView.a(boolean, boolean):void
                              c.CordovaWebView.a(java.lang.String, java.lang.Object):void */
                            public final void run() {
                                C0008f.this.b.a("spinner", (Object) "stop");
                            }
                        });
                    } catch (InterruptedException e) {
                    }
                }
            }).start();
        }
        if (str.equals("about:blank")) {
            this.b.a("exit", (Object) null);
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        C0020r.b("Cordova", "CordovaWebViewClient.onReceivedError: Error code=%s Description=%s URL=%s", Integer.valueOf(i), str, str2);
        this.b.e++;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("errorCode", i);
            jSONObject.put("description", str);
            jSONObject.put("url", str2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.b.a("onReceivedError", jSONObject);
    }

    @TargetApi(8)
    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        try {
            if ((this.a.a().getPackageManager().getApplicationInfo(this.a.a().getPackageName(), 128).flags & 2) != 0) {
                sslErrorHandler.proceed();
            } else {
                super.onReceivedSslError(webView, sslErrorHandler, sslError);
            }
        } catch (PackageManager.NameNotFoundException e) {
            super.onReceivedSslError(webView, sslErrorHandler, sslError);
        }
    }

    public void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
        if (!this.b.b().equals(str)) {
            this.b.d(str);
        }
    }
}
