package vpadn;

import android.os.Message;
import android.util.Log;
import android.webkit.WebView;
import c.CordovaWebView;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedList;
import vpadn.C0024v;

/* renamed from: vpadn.m  reason: case insensitive filesystem */
public final class C0015m {
    private static int a = -1;
    private int b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f119c;
    /* access modifiers changed from: private */
    public final LinkedList<b> d = new LinkedList<>();
    private final a[] e;
    /* access modifiers changed from: private */
    public final C0018p f;
    /* access modifiers changed from: private */
    public final CordovaWebView g;

    /* renamed from: vpadn.m$a */
    interface a {
        void a();
    }

    public C0015m(CordovaWebView cordovaWebView, C0018p pVar) {
        this.f = pVar;
        this.g = cordovaWebView;
        this.e = new a[4];
        this.e[0] = null;
        this.e[1] = new c(this, (byte) 0);
        this.e[2] = new d();
        this.e[3] = new e(this, (byte) 0);
        a();
    }

    public final void a(int i) {
        if (i < 0 || i >= this.e.length) {
            Log.d("JsMessageQueue", "Invalid NativeToJsBridgeMode: " + i);
        } else if (i != this.b) {
            Log.d("JsMessageQueue", "Set native->JS mode to " + i);
            synchronized (this) {
                this.b = i;
                a aVar = this.e[i];
                if (!this.f119c && !this.d.isEmpty() && aVar != null) {
                    aVar.a();
                }
            }
        }
    }

    public final void a() {
        synchronized (this) {
            this.d.clear();
            a(2);
        }
    }

    public final String b() {
        synchronized (this) {
            if (this.d.isEmpty()) {
                return null;
            }
            Iterator<b> it = this.d.iterator();
            int i = 0;
            int i2 = 0;
            while (it.hasNext()) {
                int a2 = it.next().a();
                int length = a2 + String.valueOf(a2).length() + 1;
                if (i > 0 && i2 + length > a && a > 0) {
                    break;
                }
                i2 += length;
                i++;
            }
            StringBuilder sb = new StringBuilder(i2);
            for (int i3 = 0; i3 < i; i3++) {
                b removeFirst = this.d.removeFirst();
                sb.append(removeFirst.a()).append(' ');
                if (removeFirst.b == null) {
                    sb.append('J').append(removeFirst.a);
                } else {
                    int a3 = removeFirst.b.a();
                    sb.append(((a3 == C0024v.a.NO_RESULT.ordinal()) || (a3 == C0024v.a.OK.ordinal())) ? 'S' : 'F').append(removeFirst.b.e() ? '1' : '0').append(a3).append(' ').append(removeFirst.a).append(' ');
                    switch (removeFirst.b.b()) {
                        case 1:
                            sb.append('s');
                            sb.append(removeFirst.b.d());
                            continue;
                        case 2:
                        default:
                            sb.append(removeFirst.b.c());
                            continue;
                        case 3:
                            sb.append('n').append(removeFirst.b.c());
                            continue;
                        case 4:
                            sb.append(removeFirst.b.c().charAt(0));
                            continue;
                        case 5:
                            sb.append('N');
                            continue;
                        case 6:
                            sb.append('A');
                            sb.append(removeFirst.b.c());
                            continue;
                    }
                }
            }
            if (!this.d.isEmpty()) {
                sb.append('*');
            }
            String sb2 = sb.toString();
            return sb2;
        }
    }

    /* access modifiers changed from: private */
    public String c() {
        boolean z;
        int i;
        synchronized (this) {
            if (this.d.size() == 0) {
                return null;
            }
            Iterator<b> it = this.d.iterator();
            int i2 = 0;
            int i3 = 0;
            while (it.hasNext()) {
                int a2 = it.next().a() + 50;
                if (i2 > 0 && i3 + a2 > a && a > 0) {
                    break;
                }
                i3 += a2;
                i2++;
            }
            if (i2 == this.d.size()) {
                z = true;
            } else {
                z = false;
            }
            StringBuilder sb = new StringBuilder((z ? 0 : 100) + i3);
            for (int i4 = 0; i4 < i2; i4++) {
                b removeFirst = this.d.removeFirst();
                if (!z || i4 + 1 != i2) {
                    sb.append("try{");
                    removeFirst.a(sb);
                    sb.append("}finally{");
                } else {
                    removeFirst.a(sb);
                }
            }
            if (!z) {
                sb.append("window.setTimeout(function(){cordova.require('cordova/plugin/android/polling').pollOnce();},0);");
            }
            if (z) {
                i = 1;
            } else {
                i = 0;
            }
            while (i < i2) {
                sb.append('}');
                i++;
            }
            String sb2 = sb.toString();
            return sb2;
        }
    }

    public final void a(String str) {
        a(new b(str));
    }

    public final void a(C0024v vVar, String str) {
        if (str == null) {
            Log.e("JsMessageQueue", "Got plugin result with no callbackId", new Throwable());
            return;
        }
        boolean z = vVar.a() == C0024v.a.NO_RESULT.ordinal();
        boolean e2 = vVar.e();
        if (!z || !e2) {
            a(new b(vVar, str));
        }
    }

    private void a(b bVar) {
        synchronized (this) {
            this.d.add(bVar);
            if (!this.f119c && this.e[this.b] != null) {
                this.e[this.b].a();
            }
        }
    }

    public final void a(boolean z) {
        if (this.f119c && z) {
            Log.e("JsMessageQueue", "nested call to setPaused detected.", new Throwable());
        }
        this.f119c = z;
        if (!z) {
            synchronized (this) {
                if (!this.d.isEmpty() && this.e[this.b] != null) {
                    this.e[this.b].a();
                }
            }
        }
    }

    /* renamed from: vpadn.m$c */
    class c implements a {
        private Runnable b;

        private c() {
            this.b = new Runnable() {
                public final void run() {
                    String a2 = C0015m.this.c();
                    if (a2 != null) {
                        C0015m.this.g.b("javascript:" + a2);
                    }
                }
            };
        }

        /* synthetic */ c(C0015m mVar, byte b2) {
            this();
        }

        public final void a() {
            C0015m.this.f.a().runOnUiThread(this.b);
        }
    }

    /* renamed from: vpadn.m$d */
    class d implements a {
        boolean a = true;

        /* renamed from: c  reason: collision with root package name */
        private Runnable f120c = new Runnable() {
            public final void run() {
                boolean z = true;
                if (!C0015m.this.d.isEmpty()) {
                    d dVar = d.this;
                    if (d.this.a) {
                        z = false;
                    }
                    dVar.a = z;
                    try {
                        C0015m.this.g.setNetworkAvailable(d.this.a);
                        if (!d.this.a) {
                            d.this.a = true;
                            C0015m.this.g.setNetworkAvailable(d.this.a);
                        }
                    } catch (Exception e) {
                        Log.e("JsMessageQueue", "OnlineEventsBridgeMode class throw Exception:" + e.getMessage(), e);
                    }
                }
            }
        };

        d() {
            try {
                C0015m.this.g.setNetworkAvailable(true);
            } catch (Exception e) {
                Log.e("JsMessageQueue", "OnlineEventsBridgeMode class throw Exception:" + e.getMessage(), e);
            }
        }

        public final void a() {
            C0015m.this.f.a().runOnUiThread(this.f120c);
        }
    }

    /* renamed from: vpadn.m$e */
    class e implements a {
        private Method a;
        private Object b;

        /* renamed from: c  reason: collision with root package name */
        private boolean f121c;

        private e() {
        }

        /* synthetic */ e(C0015m mVar, byte b2) {
            this();
        }

        public final void a() {
            Object obj;
            Class cls;
            if (this.a == null && !this.f121c) {
                Object b2 = C0015m.this.g;
                Class cls2 = WebView.class;
                try {
                    Field declaredField = cls2.getDeclaredField("mProvider");
                    declaredField.setAccessible(true);
                    b2 = declaredField.get(C0015m.this.g);
                    obj = b2;
                    cls = b2.getClass();
                } catch (Throwable th) {
                    Class cls3 = cls2;
                    obj = b2;
                    cls = cls3;
                }
                try {
                    Field declaredField2 = cls.getDeclaredField("mWebViewCore");
                    declaredField2.setAccessible(true);
                    this.b = declaredField2.get(obj);
                    if (this.b != null) {
                        this.a = this.b.getClass().getDeclaredMethod("sendMessage", Message.class);
                        this.a.setAccessible(true);
                    }
                } catch (Throwable th2) {
                    this.f121c = true;
                    Log.e("JsMessageQueue", "PrivateApiBridgeMode failed to find the expected APIs.", th2);
                }
            }
            if (this.a != null) {
                Message obtain = Message.obtain(null, 194, C0015m.this.c());
                try {
                    this.a.invoke(this.b, obtain);
                } catch (Throwable th3) {
                    Log.e("JsMessageQueue", "Reflection message bridge failed.", th3);
                }
            }
        }
    }

    /* renamed from: vpadn.m$b */
    static class b {
        final String a;
        final C0024v b;

        b(String str) {
            if (str == null) {
                throw new NullPointerException();
            }
            this.a = str;
            this.b = null;
        }

        b(C0024v vVar, String str) {
            if (str == null || vVar == null) {
                throw new NullPointerException();
            }
            this.a = str;
            this.b = vVar;
        }

        /* access modifiers changed from: package-private */
        public final int a() {
            if (this.b == null) {
                return this.a.length() + 1;
            }
            int length = String.valueOf(this.b.a()).length() + 2 + 1 + this.a.length() + 1;
            switch (this.b.b()) {
                case 1:
                    return length + this.b.d().length() + 1;
                case 2:
                default:
                    return length + this.b.c().length();
                case 3:
                    return length + this.b.c().length() + 1;
                case 4:
                case 5:
                    return length + 1;
                case 6:
                    return length + this.b.c().length() + 1;
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(StringBuilder sb) {
            if (this.b == null) {
                sb.append(this.a);
                return;
            }
            int a2 = this.b.a();
            sb.append("cordova.callbackFromNative('").append(this.a).append("',").append(a2 == C0024v.a.OK.ordinal() || a2 == C0024v.a.NO_RESULT.ordinal()).append(",").append(a2).append(",").append(this.b.c()).append(",").append(this.b.e()).append(");");
        }
    }
}
