package vpadn;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import java.util.concurrent.ExecutorService;

@Deprecated
/* renamed from: vpadn.s  reason: case insensitive filesystem */
public final class C0021s implements C0018p {
    private C0018p a;

    public C0021s(C0018p pVar) {
        this.a = pVar;
    }

    @Deprecated
    public final Activity a() {
        Log.i("Deprecation Notice", "Replace ctx.getActivity() with cordova.getActivity()");
        return this.a.a();
    }

    @Deprecated
    public final Object a(String str, Object obj) {
        Log.i("Deprecation Notice", "Replace ctx.onMessage() with cordova.onMessage()");
        return this.a.a(str, obj);
    }

    @Deprecated
    public final void a(C0019q qVar, Intent intent, int i) {
        Log.i("Deprecation Notice", "Replace ctx.startActivityForResult() with cordova.startActivityForResult()");
        this.a.a(qVar, intent, i);
    }

    public final ExecutorService e() {
        Log.i("Deprecation Notice", "Replace ctx.getThreadPool() with cordova.getThreadPool()");
        return this.a.e();
    }
}
