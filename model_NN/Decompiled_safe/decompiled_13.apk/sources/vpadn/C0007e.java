package vpadn;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import c.GeoBroker;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* renamed from: vpadn.e  reason: case insensitive filesystem */
public class C0007e implements LocationListener {
    public static int a = 2;
    protected LocationManager b;

    /* renamed from: c  reason: collision with root package name */
    protected boolean f114c = false;
    private GeoBroker d;
    private HashMap<String, C0017o> e = new HashMap<>();
    private List<C0017o> f = new ArrayList();
    private String g = "[Cordova Location Listener]";

    public C0007e(LocationManager locationManager, GeoBroker geoBroker, String str) {
        this.b = locationManager;
        this.d = geoBroker;
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public final void a(int i, String str) {
        for (C0017o fail : this.f) {
            this.d.fail(i, str, fail);
        }
        if (this.d.isGlobalListener(this)) {
            Log.d(this.g, "Stopping global listener");
            d();
        }
        this.f.clear();
        for (C0017o fail2 : this.e.values()) {
            this.d.fail(i, str, fail2);
        }
    }

    public void onProviderDisabled(String str) {
        Log.d(this.g, "Location provider '" + str + "' disabled.");
        a(a, "GPS provider disabled.");
    }

    public void onProviderEnabled(String str) {
        Log.d(this.g, "Location provider " + str + " has been enabled");
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
        Log.d(this.g, "The status of the provider " + str + " has changed");
        if (i == 0) {
            Log.d(this.g, String.valueOf(str) + " is OUT OF SERVICE");
            a(a, "Provider " + str + " is out of service.");
        } else if (i == 1) {
            Log.d(this.g, String.valueOf(str) + " is TEMPORARILY_UNAVAILABLE");
        } else {
            Log.d(this.g, String.valueOf(str) + " is AVAILABLE");
        }
    }

    public void onLocationChanged(Location location) {
        Log.d(this.g, "The location has been updated!");
        for (C0017o win : this.f) {
            this.d.win(location, win);
        }
        if (this.d.isGlobalListener(this)) {
            Log.d(this.g, "Stopping global listener");
            d();
        }
        this.f.clear();
        for (C0017o win2 : this.e.values()) {
            this.d.win(location, win2);
        }
    }

    private int c() {
        return this.e.size() + this.f.size();
    }

    public final void a(String str, C0017o oVar) {
        this.e.put(str, oVar);
        if (c() == 1) {
            b();
        }
    }

    public final void a(C0017o oVar) {
        this.f.add(oVar);
        if (c() == 1) {
            b();
        }
    }

    public final void a(String str) {
        if (this.e.containsKey(str)) {
            this.e.remove(str);
        }
        if (c() == 0) {
            d();
        }
    }

    public final void a() {
        d();
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f114c) {
            return;
        }
        if (this.b.getProvider("network") != null) {
            this.f114c = true;
            this.b.requestLocationUpdates("network", 60000, 10.0f, this);
            return;
        }
        a(a, "Network provider is not available.");
    }

    private void d() {
        if (this.f114c) {
            this.b.removeUpdates(this);
            this.f114c = false;
        }
    }
}
