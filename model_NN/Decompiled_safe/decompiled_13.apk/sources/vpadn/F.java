package vpadn;

import com.vpon.ads.VponAdSize;

public enum F {
    API_ERR_UNKNOWN_ERROR(-1),
    API_ERR_NO_AD_FOUND_IN_ONLINE_QUEUE(-3),
    API_ERR_SDK_DEPRECATED(-5),
    API_ERR_DEVICE_NOT_SUPPORTED(-6),
    API_ERR_UNABLE_TO_DETEMINE_QUADKEY(-7),
    API_ERR_UNKNOWN_LICENSE_KEY(-8),
    API_ERR_BLOCKED_LICENSE_KEY(-9),
    API_ERR_INVALID_LICENSE_KEY(-10),
    API_ERR_WRONG_APP_STATUS(-13),
    API_ERR_TARGET_OS_NOT_SUPPORTED(-15),
    API_ERR_APP_IS_APPROVING(-17),
    API_ERR_NO_MATCHED_SCREEN_SIZE(-21),
    API_ERR_NO_MATCHED_AD_IMAGE(-22),
    API_ERR_DATABASE_DATA_ACCESS(-23),
    API_ERR_INVALID_IMPRESSION_AD_ID(-24),
    API_ERR_INVALID_REQ_PARAMETERS(-2),
    API_ERR_WRONG_AD_TYPE(-4),
    API_ERR_WRONG_CLICK_TYPE(-11),
    API_ERR_GEO_DECODING_FAILED(-12),
    API_ERR_ONLINE_QUEUED_AD_NOT_FOUND(-14),
    API_ERR_TEST_AD_NOT_FOUND(-16),
    API_ERR_INVALID_TS(-101),
    API_ERR_PERMISSION(-1001),
    API_ERR_IMP_CLICK_LIVE_AD_WEIGHT_ZERO(-104),
    API_SUCCESS(0);
    
    private int z = 0;

    private F(int i) {
        this.z = i;
    }

    public static F a(int i) {
        switch (i) {
            case -1001:
                return API_ERR_PERMISSION;
            case -104:
                return API_ERR_IMP_CLICK_LIVE_AD_WEIGHT_ZERO;
            case -101:
                return API_ERR_INVALID_TS;
            case -24:
                return API_ERR_INVALID_IMPRESSION_AD_ID;
            case -23:
                return API_ERR_DATABASE_DATA_ACCESS;
            case -22:
                return API_ERR_NO_MATCHED_AD_IMAGE;
            case -21:
                return API_ERR_NO_MATCHED_SCREEN_SIZE;
            case -17:
                return API_ERR_APP_IS_APPROVING;
            case -16:
                return API_ERR_TEST_AD_NOT_FOUND;
            case -15:
                return API_ERR_TARGET_OS_NOT_SUPPORTED;
            case -14:
                return API_ERR_ONLINE_QUEUED_AD_NOT_FOUND;
            case -13:
                return API_ERR_WRONG_APP_STATUS;
            case -12:
                return API_ERR_GEO_DECODING_FAILED;
            case -11:
                return API_ERR_WRONG_CLICK_TYPE;
            case -10:
                return API_ERR_INVALID_LICENSE_KEY;
            case -9:
                return API_ERR_BLOCKED_LICENSE_KEY;
            case -8:
                return API_ERR_UNKNOWN_LICENSE_KEY;
            case -7:
                return API_ERR_UNABLE_TO_DETEMINE_QUADKEY;
            case -6:
                return API_ERR_DEVICE_NOT_SUPPORTED;
            case VponAdSize.PORTRAIT_AD_HEIGHT:
                return API_ERR_SDK_DEPRECATED;
            case VponAdSize.LARGE_AD_HEIGHT:
                return API_ERR_WRONG_AD_TYPE;
            case VponAdSize.LANDSCAPE_AD_HEIGHT:
                return API_ERR_NO_AD_FOUND_IN_ONLINE_QUEUE;
            case -2:
                return API_ERR_INVALID_REQ_PARAMETERS;
            case -1:
                return API_ERR_UNKNOWN_ERROR;
            case 0:
                return API_SUCCESS;
            default:
                return API_ERR_UNKNOWN_ERROR;
        }
    }

    public final int a() {
        return this.z;
    }
}
