package vpadn;

import java.io.UnsupportedEncodingException;

public class S {
    private static /* synthetic */ boolean a;

    static {
        boolean z;
        if (!S.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        a = z;
    }

    static abstract class a {
        public byte[] a;
        public int b;

        a() {
        }
    }

    public static byte[] a(String str, int i) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        b bVar = new b(0, new byte[((length * 3) / 4)]);
        if (!bVar.a(bytes, 0, length, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (bVar.b == bVar.a.length) {
            return bVar.a;
        } else {
            byte[] bArr = new byte[bVar.b];
            System.arraycopy(bVar.a, 0, bArr, 0, bVar.b);
            return bArr;
        }
    }

    static class b extends a {

        /* renamed from: c  reason: collision with root package name */
        private static final int[] f108c;
        private static final int[] d;
        private int e;
        private int f;
        private final int[] g;

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: int[]} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v3, resolved type: int[]} */
        /* JADX WARNING: Multi-variable type inference failed */
        static {
            /*
                r7 = 4
                r6 = 3
                r5 = 2
                r4 = 1
                r3 = -1
                r0 = 256(0x100, float:3.59E-43)
                int[] r0 = new int[r0]
                r1 = 0
                r0[r1] = r3
                r0[r4] = r3
                r0[r5] = r3
                r0[r6] = r3
                r0[r7] = r3
                r1 = 5
                r0[r1] = r3
                r1 = 6
                r0[r1] = r3
                r1 = 7
                r0[r1] = r3
                r1 = 8
                r0[r1] = r3
                r1 = 9
                r0[r1] = r3
                r1 = 10
                r0[r1] = r3
                r1 = 11
                r0[r1] = r3
                r1 = 12
                r0[r1] = r3
                r1 = 13
                r0[r1] = r3
                r1 = 14
                r0[r1] = r3
                r1 = 15
                r0[r1] = r3
                r1 = 16
                r0[r1] = r3
                r1 = 17
                r0[r1] = r3
                r1 = 18
                r0[r1] = r3
                r1 = 19
                r0[r1] = r3
                r1 = 20
                r0[r1] = r3
                r1 = 21
                r0[r1] = r3
                r1 = 22
                r0[r1] = r3
                r1 = 23
                r0[r1] = r3
                r1 = 24
                r0[r1] = r3
                r1 = 25
                r0[r1] = r3
                r1 = 26
                r0[r1] = r3
                r1 = 27
                r0[r1] = r3
                r1 = 28
                r0[r1] = r3
                r1 = 29
                r0[r1] = r3
                r1 = 30
                r0[r1] = r3
                r1 = 31
                r0[r1] = r3
                r1 = 32
                r0[r1] = r3
                r1 = 33
                r0[r1] = r3
                r1 = 34
                r0[r1] = r3
                r1 = 35
                r0[r1] = r3
                r1 = 36
                r0[r1] = r3
                r1 = 37
                r0[r1] = r3
                r1 = 38
                r0[r1] = r3
                r1 = 39
                r0[r1] = r3
                r1 = 40
                r0[r1] = r3
                r1 = 41
                r0[r1] = r3
                r1 = 42
                r0[r1] = r3
                r1 = 43
                r2 = 62
                r0[r1] = r2
                r1 = 44
                r0[r1] = r3
                r1 = 45
                r0[r1] = r3
                r1 = 46
                r0[r1] = r3
                r1 = 47
                r2 = 63
                r0[r1] = r2
                r1 = 48
                r2 = 52
                r0[r1] = r2
                r1 = 49
                r2 = 53
                r0[r1] = r2
                r1 = 50
                r2 = 54
                r0[r1] = r2
                r1 = 51
                r2 = 55
                r0[r1] = r2
                r1 = 52
                r2 = 56
                r0[r1] = r2
                r1 = 53
                r2 = 57
                r0[r1] = r2
                r1 = 54
                r2 = 58
                r0[r1] = r2
                r1 = 55
                r2 = 59
                r0[r1] = r2
                r1 = 56
                r2 = 60
                r0[r1] = r2
                r1 = 57
                r2 = 61
                r0[r1] = r2
                r1 = 58
                r0[r1] = r3
                r1 = 59
                r0[r1] = r3
                r1 = 60
                r0[r1] = r3
                r1 = 61
                r2 = -2
                r0[r1] = r2
                r1 = 62
                r0[r1] = r3
                r1 = 63
                r0[r1] = r3
                r1 = 64
                r0[r1] = r3
                r1 = 66
                r0[r1] = r4
                r1 = 67
                r0[r1] = r5
                r1 = 68
                r0[r1] = r6
                r1 = 69
                r0[r1] = r7
                r1 = 70
                r2 = 5
                r0[r1] = r2
                r1 = 71
                r2 = 6
                r0[r1] = r2
                r1 = 72
                r2 = 7
                r0[r1] = r2
                r1 = 73
                r2 = 8
                r0[r1] = r2
                r1 = 74
                r2 = 9
                r0[r1] = r2
                r1 = 75
                r2 = 10
                r0[r1] = r2
                r1 = 76
                r2 = 11
                r0[r1] = r2
                r1 = 77
                r2 = 12
                r0[r1] = r2
                r1 = 78
                r2 = 13
                r0[r1] = r2
                r1 = 79
                r2 = 14
                r0[r1] = r2
                r1 = 80
                r2 = 15
                r0[r1] = r2
                r1 = 81
                r2 = 16
                r0[r1] = r2
                r1 = 82
                r2 = 17
                r0[r1] = r2
                r1 = 83
                r2 = 18
                r0[r1] = r2
                r1 = 84
                r2 = 19
                r0[r1] = r2
                r1 = 85
                r2 = 20
                r0[r1] = r2
                r1 = 86
                r2 = 21
                r0[r1] = r2
                r1 = 87
                r2 = 22
                r0[r1] = r2
                r1 = 88
                r2 = 23
                r0[r1] = r2
                r1 = 89
                r2 = 24
                r0[r1] = r2
                r1 = 90
                r2 = 25
                r0[r1] = r2
                r1 = 91
                r0[r1] = r3
                r1 = 92
                r0[r1] = r3
                r1 = 93
                r0[r1] = r3
                r1 = 94
                r0[r1] = r3
                r1 = 95
                r0[r1] = r3
                r1 = 96
                r0[r1] = r3
                r1 = 97
                r2 = 26
                r0[r1] = r2
                r1 = 98
                r2 = 27
                r0[r1] = r2
                r1 = 99
                r2 = 28
                r0[r1] = r2
                r1 = 100
                r2 = 29
                r0[r1] = r2
                r1 = 101(0x65, float:1.42E-43)
                r2 = 30
                r0[r1] = r2
                r1 = 102(0x66, float:1.43E-43)
                r2 = 31
                r0[r1] = r2
                r1 = 103(0x67, float:1.44E-43)
                r2 = 32
                r0[r1] = r2
                r1 = 104(0x68, float:1.46E-43)
                r2 = 33
                r0[r1] = r2
                r1 = 105(0x69, float:1.47E-43)
                r2 = 34
                r0[r1] = r2
                r1 = 106(0x6a, float:1.49E-43)
                r2 = 35
                r0[r1] = r2
                r1 = 107(0x6b, float:1.5E-43)
                r2 = 36
                r0[r1] = r2
                r1 = 108(0x6c, float:1.51E-43)
                r2 = 37
                r0[r1] = r2
                r1 = 109(0x6d, float:1.53E-43)
                r2 = 38
                r0[r1] = r2
                r1 = 110(0x6e, float:1.54E-43)
                r2 = 39
                r0[r1] = r2
                r1 = 111(0x6f, float:1.56E-43)
                r2 = 40
                r0[r1] = r2
                r1 = 112(0x70, float:1.57E-43)
                r2 = 41
                r0[r1] = r2
                r1 = 113(0x71, float:1.58E-43)
                r2 = 42
                r0[r1] = r2
                r1 = 114(0x72, float:1.6E-43)
                r2 = 43
                r0[r1] = r2
                r1 = 115(0x73, float:1.61E-43)
                r2 = 44
                r0[r1] = r2
                r1 = 116(0x74, float:1.63E-43)
                r2 = 45
                r0[r1] = r2
                r1 = 117(0x75, float:1.64E-43)
                r2 = 46
                r0[r1] = r2
                r1 = 118(0x76, float:1.65E-43)
                r2 = 47
                r0[r1] = r2
                r1 = 119(0x77, float:1.67E-43)
                r2 = 48
                r0[r1] = r2
                r1 = 120(0x78, float:1.68E-43)
                r2 = 49
                r0[r1] = r2
                r1 = 121(0x79, float:1.7E-43)
                r2 = 50
                r0[r1] = r2
                r1 = 122(0x7a, float:1.71E-43)
                r2 = 51
                r0[r1] = r2
                r1 = 123(0x7b, float:1.72E-43)
                r0[r1] = r3
                r1 = 124(0x7c, float:1.74E-43)
                r0[r1] = r3
                r1 = 125(0x7d, float:1.75E-43)
                r0[r1] = r3
                r1 = 126(0x7e, float:1.77E-43)
                r0[r1] = r3
                r1 = 127(0x7f, float:1.78E-43)
                r0[r1] = r3
                r1 = 128(0x80, float:1.794E-43)
                r0[r1] = r3
                r1 = 129(0x81, float:1.81E-43)
                r0[r1] = r3
                r1 = 130(0x82, float:1.82E-43)
                r0[r1] = r3
                r1 = 131(0x83, float:1.84E-43)
                r0[r1] = r3
                r1 = 132(0x84, float:1.85E-43)
                r0[r1] = r3
                r1 = 133(0x85, float:1.86E-43)
                r0[r1] = r3
                r1 = 134(0x86, float:1.88E-43)
                r0[r1] = r3
                r1 = 135(0x87, float:1.89E-43)
                r0[r1] = r3
                r1 = 136(0x88, float:1.9E-43)
                r0[r1] = r3
                r1 = 137(0x89, float:1.92E-43)
                r0[r1] = r3
                r1 = 138(0x8a, float:1.93E-43)
                r0[r1] = r3
                r1 = 139(0x8b, float:1.95E-43)
                r0[r1] = r3
                r1 = 140(0x8c, float:1.96E-43)
                r0[r1] = r3
                r1 = 141(0x8d, float:1.98E-43)
                r0[r1] = r3
                r1 = 142(0x8e, float:1.99E-43)
                r0[r1] = r3
                r1 = 143(0x8f, float:2.0E-43)
                r0[r1] = r3
                r1 = 144(0x90, float:2.02E-43)
                r0[r1] = r3
                r1 = 145(0x91, float:2.03E-43)
                r0[r1] = r3
                r1 = 146(0x92, float:2.05E-43)
                r0[r1] = r3
                r1 = 147(0x93, float:2.06E-43)
                r0[r1] = r3
                r1 = 148(0x94, float:2.07E-43)
                r0[r1] = r3
                r1 = 149(0x95, float:2.09E-43)
                r0[r1] = r3
                r1 = 150(0x96, float:2.1E-43)
                r0[r1] = r3
                r1 = 151(0x97, float:2.12E-43)
                r0[r1] = r3
                r1 = 152(0x98, float:2.13E-43)
                r0[r1] = r3
                r1 = 153(0x99, float:2.14E-43)
                r0[r1] = r3
                r1 = 154(0x9a, float:2.16E-43)
                r0[r1] = r3
                r1 = 155(0x9b, float:2.17E-43)
                r0[r1] = r3
                r1 = 156(0x9c, float:2.19E-43)
                r0[r1] = r3
                r1 = 157(0x9d, float:2.2E-43)
                r0[r1] = r3
                r1 = 158(0x9e, float:2.21E-43)
                r0[r1] = r3
                r1 = 159(0x9f, float:2.23E-43)
                r0[r1] = r3
                r1 = 160(0xa0, float:2.24E-43)
                r0[r1] = r3
                r1 = 161(0xa1, float:2.26E-43)
                r0[r1] = r3
                r1 = 162(0xa2, float:2.27E-43)
                r0[r1] = r3
                r1 = 163(0xa3, float:2.28E-43)
                r0[r1] = r3
                r1 = 164(0xa4, float:2.3E-43)
                r0[r1] = r3
                r1 = 165(0xa5, float:2.31E-43)
                r0[r1] = r3
                r1 = 166(0xa6, float:2.33E-43)
                r0[r1] = r3
                r1 = 167(0xa7, float:2.34E-43)
                r0[r1] = r3
                r1 = 168(0xa8, float:2.35E-43)
                r0[r1] = r3
                r1 = 169(0xa9, float:2.37E-43)
                r0[r1] = r3
                r1 = 170(0xaa, float:2.38E-43)
                r0[r1] = r3
                r1 = 171(0xab, float:2.4E-43)
                r0[r1] = r3
                r1 = 172(0xac, float:2.41E-43)
                r0[r1] = r3
                r1 = 173(0xad, float:2.42E-43)
                r0[r1] = r3
                r1 = 174(0xae, float:2.44E-43)
                r0[r1] = r3
                r1 = 175(0xaf, float:2.45E-43)
                r0[r1] = r3
                r1 = 176(0xb0, float:2.47E-43)
                r0[r1] = r3
                r1 = 177(0xb1, float:2.48E-43)
                r0[r1] = r3
                r1 = 178(0xb2, float:2.5E-43)
                r0[r1] = r3
                r1 = 179(0xb3, float:2.51E-43)
                r0[r1] = r3
                r1 = 180(0xb4, float:2.52E-43)
                r0[r1] = r3
                r1 = 181(0xb5, float:2.54E-43)
                r0[r1] = r3
                r1 = 182(0xb6, float:2.55E-43)
                r0[r1] = r3
                r1 = 183(0xb7, float:2.56E-43)
                r0[r1] = r3
                r1 = 184(0xb8, float:2.58E-43)
                r0[r1] = r3
                r1 = 185(0xb9, float:2.59E-43)
                r0[r1] = r3
                r1 = 186(0xba, float:2.6E-43)
                r0[r1] = r3
                r1 = 187(0xbb, float:2.62E-43)
                r0[r1] = r3
                r1 = 188(0xbc, float:2.63E-43)
                r0[r1] = r3
                r1 = 189(0xbd, float:2.65E-43)
                r0[r1] = r3
                r1 = 190(0xbe, float:2.66E-43)
                r0[r1] = r3
                r1 = 191(0xbf, float:2.68E-43)
                r0[r1] = r3
                r1 = 192(0xc0, float:2.69E-43)
                r0[r1] = r3
                r1 = 193(0xc1, float:2.7E-43)
                r0[r1] = r3
                r1 = 194(0xc2, float:2.72E-43)
                r0[r1] = r3
                r1 = 195(0xc3, float:2.73E-43)
                r0[r1] = r3
                r1 = 196(0xc4, float:2.75E-43)
                r0[r1] = r3
                r1 = 197(0xc5, float:2.76E-43)
                r0[r1] = r3
                r1 = 198(0xc6, float:2.77E-43)
                r0[r1] = r3
                r1 = 199(0xc7, float:2.79E-43)
                r0[r1] = r3
                r1 = 200(0xc8, float:2.8E-43)
                r0[r1] = r3
                r1 = 201(0xc9, float:2.82E-43)
                r0[r1] = r3
                r1 = 202(0xca, float:2.83E-43)
                r0[r1] = r3
                r1 = 203(0xcb, float:2.84E-43)
                r0[r1] = r3
                r1 = 204(0xcc, float:2.86E-43)
                r0[r1] = r3
                r1 = 205(0xcd, float:2.87E-43)
                r0[r1] = r3
                r1 = 206(0xce, float:2.89E-43)
                r0[r1] = r3
                r1 = 207(0xcf, float:2.9E-43)
                r0[r1] = r3
                r1 = 208(0xd0, float:2.91E-43)
                r0[r1] = r3
                r1 = 209(0xd1, float:2.93E-43)
                r0[r1] = r3
                r1 = 210(0xd2, float:2.94E-43)
                r0[r1] = r3
                r1 = 211(0xd3, float:2.96E-43)
                r0[r1] = r3
                r1 = 212(0xd4, float:2.97E-43)
                r0[r1] = r3
                r1 = 213(0xd5, float:2.98E-43)
                r0[r1] = r3
                r1 = 214(0xd6, float:3.0E-43)
                r0[r1] = r3
                r1 = 215(0xd7, float:3.01E-43)
                r0[r1] = r3
                r1 = 216(0xd8, float:3.03E-43)
                r0[r1] = r3
                r1 = 217(0xd9, float:3.04E-43)
                r0[r1] = r3
                r1 = 218(0xda, float:3.05E-43)
                r0[r1] = r3
                r1 = 219(0xdb, float:3.07E-43)
                r0[r1] = r3
                r1 = 220(0xdc, float:3.08E-43)
                r0[r1] = r3
                r1 = 221(0xdd, float:3.1E-43)
                r0[r1] = r3
                r1 = 222(0xde, float:3.11E-43)
                r0[r1] = r3
                r1 = 223(0xdf, float:3.12E-43)
                r0[r1] = r3
                r1 = 224(0xe0, float:3.14E-43)
                r0[r1] = r3
                r1 = 225(0xe1, float:3.15E-43)
                r0[r1] = r3
                r1 = 226(0xe2, float:3.17E-43)
                r0[r1] = r3
                r1 = 227(0xe3, float:3.18E-43)
                r0[r1] = r3
                r1 = 228(0xe4, float:3.2E-43)
                r0[r1] = r3
                r1 = 229(0xe5, float:3.21E-43)
                r0[r1] = r3
                r1 = 230(0xe6, float:3.22E-43)
                r0[r1] = r3
                r1 = 231(0xe7, float:3.24E-43)
                r0[r1] = r3
                r1 = 232(0xe8, float:3.25E-43)
                r0[r1] = r3
                r1 = 233(0xe9, float:3.27E-43)
                r0[r1] = r3
                r1 = 234(0xea, float:3.28E-43)
                r0[r1] = r3
                r1 = 235(0xeb, float:3.3E-43)
                r0[r1] = r3
                r1 = 236(0xec, float:3.31E-43)
                r0[r1] = r3
                r1 = 237(0xed, float:3.32E-43)
                r0[r1] = r3
                r1 = 238(0xee, float:3.34E-43)
                r0[r1] = r3
                r1 = 239(0xef, float:3.35E-43)
                r0[r1] = r3
                r1 = 240(0xf0, float:3.36E-43)
                r0[r1] = r3
                r1 = 241(0xf1, float:3.38E-43)
                r0[r1] = r3
                r1 = 242(0xf2, float:3.39E-43)
                r0[r1] = r3
                r1 = 243(0xf3, float:3.4E-43)
                r0[r1] = r3
                r1 = 244(0xf4, float:3.42E-43)
                r0[r1] = r3
                r1 = 245(0xf5, float:3.43E-43)
                r0[r1] = r3
                r1 = 246(0xf6, float:3.45E-43)
                r0[r1] = r3
                r1 = 247(0xf7, float:3.46E-43)
                r0[r1] = r3
                r1 = 248(0xf8, float:3.48E-43)
                r0[r1] = r3
                r1 = 249(0xf9, float:3.49E-43)
                r0[r1] = r3
                r1 = 250(0xfa, float:3.5E-43)
                r0[r1] = r3
                r1 = 251(0xfb, float:3.52E-43)
                r0[r1] = r3
                r1 = 252(0xfc, float:3.53E-43)
                r0[r1] = r3
                r1 = 253(0xfd, float:3.55E-43)
                r0[r1] = r3
                r1 = 254(0xfe, float:3.56E-43)
                r0[r1] = r3
                r1 = 255(0xff, float:3.57E-43)
                r0[r1] = r3
                vpadn.S.b.f108c = r0
                r0 = 256(0x100, float:3.59E-43)
                int[] r0 = new int[r0]
                r1 = 0
                r0[r1] = r3
                r0[r4] = r3
                r0[r5] = r3
                r0[r6] = r3
                r0[r7] = r3
                r1 = 5
                r0[r1] = r3
                r1 = 6
                r0[r1] = r3
                r1 = 7
                r0[r1] = r3
                r1 = 8
                r0[r1] = r3
                r1 = 9
                r0[r1] = r3
                r1 = 10
                r0[r1] = r3
                r1 = 11
                r0[r1] = r3
                r1 = 12
                r0[r1] = r3
                r1 = 13
                r0[r1] = r3
                r1 = 14
                r0[r1] = r3
                r1 = 15
                r0[r1] = r3
                r1 = 16
                r0[r1] = r3
                r1 = 17
                r0[r1] = r3
                r1 = 18
                r0[r1] = r3
                r1 = 19
                r0[r1] = r3
                r1 = 20
                r0[r1] = r3
                r1 = 21
                r0[r1] = r3
                r1 = 22
                r0[r1] = r3
                r1 = 23
                r0[r1] = r3
                r1 = 24
                r0[r1] = r3
                r1 = 25
                r0[r1] = r3
                r1 = 26
                r0[r1] = r3
                r1 = 27
                r0[r1] = r3
                r1 = 28
                r0[r1] = r3
                r1 = 29
                r0[r1] = r3
                r1 = 30
                r0[r1] = r3
                r1 = 31
                r0[r1] = r3
                r1 = 32
                r0[r1] = r3
                r1 = 33
                r0[r1] = r3
                r1 = 34
                r0[r1] = r3
                r1 = 35
                r0[r1] = r3
                r1 = 36
                r0[r1] = r3
                r1 = 37
                r0[r1] = r3
                r1 = 38
                r0[r1] = r3
                r1 = 39
                r0[r1] = r3
                r1 = 40
                r0[r1] = r3
                r1 = 41
                r0[r1] = r3
                r1 = 42
                r0[r1] = r3
                r1 = 43
                r0[r1] = r3
                r1 = 44
                r0[r1] = r3
                r1 = 45
                r2 = 62
                r0[r1] = r2
                r1 = 46
                r0[r1] = r3
                r1 = 47
                r0[r1] = r3
                r1 = 48
                r2 = 52
                r0[r1] = r2
                r1 = 49
                r2 = 53
                r0[r1] = r2
                r1 = 50
                r2 = 54
                r0[r1] = r2
                r1 = 51
                r2 = 55
                r0[r1] = r2
                r1 = 52
                r2 = 56
                r0[r1] = r2
                r1 = 53
                r2 = 57
                r0[r1] = r2
                r1 = 54
                r2 = 58
                r0[r1] = r2
                r1 = 55
                r2 = 59
                r0[r1] = r2
                r1 = 56
                r2 = 60
                r0[r1] = r2
                r1 = 57
                r2 = 61
                r0[r1] = r2
                r1 = 58
                r0[r1] = r3
                r1 = 59
                r0[r1] = r3
                r1 = 60
                r0[r1] = r3
                r1 = 61
                r2 = -2
                r0[r1] = r2
                r1 = 62
                r0[r1] = r3
                r1 = 63
                r0[r1] = r3
                r1 = 64
                r0[r1] = r3
                r1 = 66
                r0[r1] = r4
                r1 = 67
                r0[r1] = r5
                r1 = 68
                r0[r1] = r6
                r1 = 69
                r0[r1] = r7
                r1 = 70
                r2 = 5
                r0[r1] = r2
                r1 = 71
                r2 = 6
                r0[r1] = r2
                r1 = 72
                r2 = 7
                r0[r1] = r2
                r1 = 73
                r2 = 8
                r0[r1] = r2
                r1 = 74
                r2 = 9
                r0[r1] = r2
                r1 = 75
                r2 = 10
                r0[r1] = r2
                r1 = 76
                r2 = 11
                r0[r1] = r2
                r1 = 77
                r2 = 12
                r0[r1] = r2
                r1 = 78
                r2 = 13
                r0[r1] = r2
                r1 = 79
                r2 = 14
                r0[r1] = r2
                r1 = 80
                r2 = 15
                r0[r1] = r2
                r1 = 81
                r2 = 16
                r0[r1] = r2
                r1 = 82
                r2 = 17
                r0[r1] = r2
                r1 = 83
                r2 = 18
                r0[r1] = r2
                r1 = 84
                r2 = 19
                r0[r1] = r2
                r1 = 85
                r2 = 20
                r0[r1] = r2
                r1 = 86
                r2 = 21
                r0[r1] = r2
                r1 = 87
                r2 = 22
                r0[r1] = r2
                r1 = 88
                r2 = 23
                r0[r1] = r2
                r1 = 89
                r2 = 24
                r0[r1] = r2
                r1 = 90
                r2 = 25
                r0[r1] = r2
                r1 = 91
                r0[r1] = r3
                r1 = 92
                r0[r1] = r3
                r1 = 93
                r0[r1] = r3
                r1 = 94
                r0[r1] = r3
                r1 = 95
                r2 = 63
                r0[r1] = r2
                r1 = 96
                r0[r1] = r3
                r1 = 97
                r2 = 26
                r0[r1] = r2
                r1 = 98
                r2 = 27
                r0[r1] = r2
                r1 = 99
                r2 = 28
                r0[r1] = r2
                r1 = 100
                r2 = 29
                r0[r1] = r2
                r1 = 101(0x65, float:1.42E-43)
                r2 = 30
                r0[r1] = r2
                r1 = 102(0x66, float:1.43E-43)
                r2 = 31
                r0[r1] = r2
                r1 = 103(0x67, float:1.44E-43)
                r2 = 32
                r0[r1] = r2
                r1 = 104(0x68, float:1.46E-43)
                r2 = 33
                r0[r1] = r2
                r1 = 105(0x69, float:1.47E-43)
                r2 = 34
                r0[r1] = r2
                r1 = 106(0x6a, float:1.49E-43)
                r2 = 35
                r0[r1] = r2
                r1 = 107(0x6b, float:1.5E-43)
                r2 = 36
                r0[r1] = r2
                r1 = 108(0x6c, float:1.51E-43)
                r2 = 37
                r0[r1] = r2
                r1 = 109(0x6d, float:1.53E-43)
                r2 = 38
                r0[r1] = r2
                r1 = 110(0x6e, float:1.54E-43)
                r2 = 39
                r0[r1] = r2
                r1 = 111(0x6f, float:1.56E-43)
                r2 = 40
                r0[r1] = r2
                r1 = 112(0x70, float:1.57E-43)
                r2 = 41
                r0[r1] = r2
                r1 = 113(0x71, float:1.58E-43)
                r2 = 42
                r0[r1] = r2
                r1 = 114(0x72, float:1.6E-43)
                r2 = 43
                r0[r1] = r2
                r1 = 115(0x73, float:1.61E-43)
                r2 = 44
                r0[r1] = r2
                r1 = 116(0x74, float:1.63E-43)
                r2 = 45
                r0[r1] = r2
                r1 = 117(0x75, float:1.64E-43)
                r2 = 46
                r0[r1] = r2
                r1 = 118(0x76, float:1.65E-43)
                r2 = 47
                r0[r1] = r2
                r1 = 119(0x77, float:1.67E-43)
                r2 = 48
                r0[r1] = r2
                r1 = 120(0x78, float:1.68E-43)
                r2 = 49
                r0[r1] = r2
                r1 = 121(0x79, float:1.7E-43)
                r2 = 50
                r0[r1] = r2
                r1 = 122(0x7a, float:1.71E-43)
                r2 = 51
                r0[r1] = r2
                r1 = 123(0x7b, float:1.72E-43)
                r0[r1] = r3
                r1 = 124(0x7c, float:1.74E-43)
                r0[r1] = r3
                r1 = 125(0x7d, float:1.75E-43)
                r0[r1] = r3
                r1 = 126(0x7e, float:1.77E-43)
                r0[r1] = r3
                r1 = 127(0x7f, float:1.78E-43)
                r0[r1] = r3
                r1 = 128(0x80, float:1.794E-43)
                r0[r1] = r3
                r1 = 129(0x81, float:1.81E-43)
                r0[r1] = r3
                r1 = 130(0x82, float:1.82E-43)
                r0[r1] = r3
                r1 = 131(0x83, float:1.84E-43)
                r0[r1] = r3
                r1 = 132(0x84, float:1.85E-43)
                r0[r1] = r3
                r1 = 133(0x85, float:1.86E-43)
                r0[r1] = r3
                r1 = 134(0x86, float:1.88E-43)
                r0[r1] = r3
                r1 = 135(0x87, float:1.89E-43)
                r0[r1] = r3
                r1 = 136(0x88, float:1.9E-43)
                r0[r1] = r3
                r1 = 137(0x89, float:1.92E-43)
                r0[r1] = r3
                r1 = 138(0x8a, float:1.93E-43)
                r0[r1] = r3
                r1 = 139(0x8b, float:1.95E-43)
                r0[r1] = r3
                r1 = 140(0x8c, float:1.96E-43)
                r0[r1] = r3
                r1 = 141(0x8d, float:1.98E-43)
                r0[r1] = r3
                r1 = 142(0x8e, float:1.99E-43)
                r0[r1] = r3
                r1 = 143(0x8f, float:2.0E-43)
                r0[r1] = r3
                r1 = 144(0x90, float:2.02E-43)
                r0[r1] = r3
                r1 = 145(0x91, float:2.03E-43)
                r0[r1] = r3
                r1 = 146(0x92, float:2.05E-43)
                r0[r1] = r3
                r1 = 147(0x93, float:2.06E-43)
                r0[r1] = r3
                r1 = 148(0x94, float:2.07E-43)
                r0[r1] = r3
                r1 = 149(0x95, float:2.09E-43)
                r0[r1] = r3
                r1 = 150(0x96, float:2.1E-43)
                r0[r1] = r3
                r1 = 151(0x97, float:2.12E-43)
                r0[r1] = r3
                r1 = 152(0x98, float:2.13E-43)
                r0[r1] = r3
                r1 = 153(0x99, float:2.14E-43)
                r0[r1] = r3
                r1 = 154(0x9a, float:2.16E-43)
                r0[r1] = r3
                r1 = 155(0x9b, float:2.17E-43)
                r0[r1] = r3
                r1 = 156(0x9c, float:2.19E-43)
                r0[r1] = r3
                r1 = 157(0x9d, float:2.2E-43)
                r0[r1] = r3
                r1 = 158(0x9e, float:2.21E-43)
                r0[r1] = r3
                r1 = 159(0x9f, float:2.23E-43)
                r0[r1] = r3
                r1 = 160(0xa0, float:2.24E-43)
                r0[r1] = r3
                r1 = 161(0xa1, float:2.26E-43)
                r0[r1] = r3
                r1 = 162(0xa2, float:2.27E-43)
                r0[r1] = r3
                r1 = 163(0xa3, float:2.28E-43)
                r0[r1] = r3
                r1 = 164(0xa4, float:2.3E-43)
                r0[r1] = r3
                r1 = 165(0xa5, float:2.31E-43)
                r0[r1] = r3
                r1 = 166(0xa6, float:2.33E-43)
                r0[r1] = r3
                r1 = 167(0xa7, float:2.34E-43)
                r0[r1] = r3
                r1 = 168(0xa8, float:2.35E-43)
                r0[r1] = r3
                r1 = 169(0xa9, float:2.37E-43)
                r0[r1] = r3
                r1 = 170(0xaa, float:2.38E-43)
                r0[r1] = r3
                r1 = 171(0xab, float:2.4E-43)
                r0[r1] = r3
                r1 = 172(0xac, float:2.41E-43)
                r0[r1] = r3
                r1 = 173(0xad, float:2.42E-43)
                r0[r1] = r3
                r1 = 174(0xae, float:2.44E-43)
                r0[r1] = r3
                r1 = 175(0xaf, float:2.45E-43)
                r0[r1] = r3
                r1 = 176(0xb0, float:2.47E-43)
                r0[r1] = r3
                r1 = 177(0xb1, float:2.48E-43)
                r0[r1] = r3
                r1 = 178(0xb2, float:2.5E-43)
                r0[r1] = r3
                r1 = 179(0xb3, float:2.51E-43)
                r0[r1] = r3
                r1 = 180(0xb4, float:2.52E-43)
                r0[r1] = r3
                r1 = 181(0xb5, float:2.54E-43)
                r0[r1] = r3
                r1 = 182(0xb6, float:2.55E-43)
                r0[r1] = r3
                r1 = 183(0xb7, float:2.56E-43)
                r0[r1] = r3
                r1 = 184(0xb8, float:2.58E-43)
                r0[r1] = r3
                r1 = 185(0xb9, float:2.59E-43)
                r0[r1] = r3
                r1 = 186(0xba, float:2.6E-43)
                r0[r1] = r3
                r1 = 187(0xbb, float:2.62E-43)
                r0[r1] = r3
                r1 = 188(0xbc, float:2.63E-43)
                r0[r1] = r3
                r1 = 189(0xbd, float:2.65E-43)
                r0[r1] = r3
                r1 = 190(0xbe, float:2.66E-43)
                r0[r1] = r3
                r1 = 191(0xbf, float:2.68E-43)
                r0[r1] = r3
                r1 = 192(0xc0, float:2.69E-43)
                r0[r1] = r3
                r1 = 193(0xc1, float:2.7E-43)
                r0[r1] = r3
                r1 = 194(0xc2, float:2.72E-43)
                r0[r1] = r3
                r1 = 195(0xc3, float:2.73E-43)
                r0[r1] = r3
                r1 = 196(0xc4, float:2.75E-43)
                r0[r1] = r3
                r1 = 197(0xc5, float:2.76E-43)
                r0[r1] = r3
                r1 = 198(0xc6, float:2.77E-43)
                r0[r1] = r3
                r1 = 199(0xc7, float:2.79E-43)
                r0[r1] = r3
                r1 = 200(0xc8, float:2.8E-43)
                r0[r1] = r3
                r1 = 201(0xc9, float:2.82E-43)
                r0[r1] = r3
                r1 = 202(0xca, float:2.83E-43)
                r0[r1] = r3
                r1 = 203(0xcb, float:2.84E-43)
                r0[r1] = r3
                r1 = 204(0xcc, float:2.86E-43)
                r0[r1] = r3
                r1 = 205(0xcd, float:2.87E-43)
                r0[r1] = r3
                r1 = 206(0xce, float:2.89E-43)
                r0[r1] = r3
                r1 = 207(0xcf, float:2.9E-43)
                r0[r1] = r3
                r1 = 208(0xd0, float:2.91E-43)
                r0[r1] = r3
                r1 = 209(0xd1, float:2.93E-43)
                r0[r1] = r3
                r1 = 210(0xd2, float:2.94E-43)
                r0[r1] = r3
                r1 = 211(0xd3, float:2.96E-43)
                r0[r1] = r3
                r1 = 212(0xd4, float:2.97E-43)
                r0[r1] = r3
                r1 = 213(0xd5, float:2.98E-43)
                r0[r1] = r3
                r1 = 214(0xd6, float:3.0E-43)
                r0[r1] = r3
                r1 = 215(0xd7, float:3.01E-43)
                r0[r1] = r3
                r1 = 216(0xd8, float:3.03E-43)
                r0[r1] = r3
                r1 = 217(0xd9, float:3.04E-43)
                r0[r1] = r3
                r1 = 218(0xda, float:3.05E-43)
                r0[r1] = r3
                r1 = 219(0xdb, float:3.07E-43)
                r0[r1] = r3
                r1 = 220(0xdc, float:3.08E-43)
                r0[r1] = r3
                r1 = 221(0xdd, float:3.1E-43)
                r0[r1] = r3
                r1 = 222(0xde, float:3.11E-43)
                r0[r1] = r3
                r1 = 223(0xdf, float:3.12E-43)
                r0[r1] = r3
                r1 = 224(0xe0, float:3.14E-43)
                r0[r1] = r3
                r1 = 225(0xe1, float:3.15E-43)
                r0[r1] = r3
                r1 = 226(0xe2, float:3.17E-43)
                r0[r1] = r3
                r1 = 227(0xe3, float:3.18E-43)
                r0[r1] = r3
                r1 = 228(0xe4, float:3.2E-43)
                r0[r1] = r3
                r1 = 229(0xe5, float:3.21E-43)
                r0[r1] = r3
                r1 = 230(0xe6, float:3.22E-43)
                r0[r1] = r3
                r1 = 231(0xe7, float:3.24E-43)
                r0[r1] = r3
                r1 = 232(0xe8, float:3.25E-43)
                r0[r1] = r3
                r1 = 233(0xe9, float:3.27E-43)
                r0[r1] = r3
                r1 = 234(0xea, float:3.28E-43)
                r0[r1] = r3
                r1 = 235(0xeb, float:3.3E-43)
                r0[r1] = r3
                r1 = 236(0xec, float:3.31E-43)
                r0[r1] = r3
                r1 = 237(0xed, float:3.32E-43)
                r0[r1] = r3
                r1 = 238(0xee, float:3.34E-43)
                r0[r1] = r3
                r1 = 239(0xef, float:3.35E-43)
                r0[r1] = r3
                r1 = 240(0xf0, float:3.36E-43)
                r0[r1] = r3
                r1 = 241(0xf1, float:3.38E-43)
                r0[r1] = r3
                r1 = 242(0xf2, float:3.39E-43)
                r0[r1] = r3
                r1 = 243(0xf3, float:3.4E-43)
                r0[r1] = r3
                r1 = 244(0xf4, float:3.42E-43)
                r0[r1] = r3
                r1 = 245(0xf5, float:3.43E-43)
                r0[r1] = r3
                r1 = 246(0xf6, float:3.45E-43)
                r0[r1] = r3
                r1 = 247(0xf7, float:3.46E-43)
                r0[r1] = r3
                r1 = 248(0xf8, float:3.48E-43)
                r0[r1] = r3
                r1 = 249(0xf9, float:3.49E-43)
                r0[r1] = r3
                r1 = 250(0xfa, float:3.5E-43)
                r0[r1] = r3
                r1 = 251(0xfb, float:3.52E-43)
                r0[r1] = r3
                r1 = 252(0xfc, float:3.53E-43)
                r0[r1] = r3
                r1 = 253(0xfd, float:3.55E-43)
                r0[r1] = r3
                r1 = 254(0xfe, float:3.56E-43)
                r0[r1] = r3
                r1 = 255(0xff, float:3.57E-43)
                r0[r1] = r3
                vpadn.S.b.d = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: vpadn.S.b.<clinit>():void");
        }

        public b(int i, byte[] bArr) {
            this.a = bArr;
            this.g = (i & 8) == 0 ? f108c : d;
            this.e = 0;
            this.f = 0;
        }

        /* JADX WARNING: Removed duplicated region for block: B:53:0x0112  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x0118  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0122  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x0132  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(byte[] r10, int r11, int r12, boolean r13) {
            /*
                r9 = this;
                int r0 = r9.e
                r1 = 6
                if (r0 != r1) goto L_0x0007
                r0 = 0
            L_0x0006:
                return r0
            L_0x0007:
                int r4 = r12 + r11
                int r2 = r9.e
                int r1 = r9.f
                r0 = 0
                byte[] r5 = r9.a
                int[] r6 = r9.g
                r3 = r2
                r2 = r11
            L_0x0014:
                if (r2 < r4) goto L_0x0020
            L_0x0016:
                r2 = r1
                switch(r3) {
                    case 0: goto L_0x001a;
                    case 1: goto L_0x0112;
                    case 2: goto L_0x0118;
                    case 3: goto L_0x0122;
                    case 4: goto L_0x0132;
                    default: goto L_0x001a;
                }
            L_0x001a:
                r9.e = r3
                r9.b = r0
                r0 = 1
                goto L_0x0006
            L_0x0020:
                if (r3 != 0) goto L_0x0051
            L_0x0022:
                int r7 = r2 + 4
                if (r7 > r4) goto L_0x004f
                byte r1 = r10[r2]
                r1 = r1 & 255(0xff, float:3.57E-43)
                r1 = r6[r1]
                int r1 = r1 << 18
                int r7 = r2 + 1
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 12
                r1 = r1 | r7
                int r7 = r2 + 2
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                int r7 = r7 << 6
                r1 = r1 | r7
                int r7 = r2 + 3
                byte r7 = r10[r7]
                r7 = r7 & 255(0xff, float:3.57E-43)
                r7 = r6[r7]
                r1 = r1 | r7
                if (r1 >= 0) goto L_0x005e
            L_0x004f:
                if (r2 >= r4) goto L_0x0016
            L_0x0051:
                int r11 = r2 + 1
                byte r2 = r10[r2]
                r2 = r2 & 255(0xff, float:3.57E-43)
                r2 = r6[r2]
                switch(r3) {
                    case 0: goto L_0x0074;
                    case 1: goto L_0x0084;
                    case 2: goto L_0x0097;
                    case 3: goto L_0x00bb;
                    case 4: goto L_0x00f7;
                    case 5: goto L_0x0109;
                    default: goto L_0x005c;
                }
            L_0x005c:
                r2 = r11
                goto L_0x0014
            L_0x005e:
                int r7 = r0 + 2
                byte r8 = (byte) r1
                r5[r7] = r8
                int r7 = r0 + 1
                int r8 = r1 >> 8
                byte r8 = (byte) r8
                r5[r7] = r8
                int r7 = r1 >> 16
                byte r7 = (byte) r7
                r5[r0] = r7
                int r0 = r0 + 3
                int r2 = r2 + 4
                goto L_0x0022
            L_0x0074:
                if (r2 < 0) goto L_0x007c
                int r1 = r3 + 1
                r3 = r1
                r1 = r2
                r2 = r11
                goto L_0x0014
            L_0x007c:
                r7 = -1
                if (r2 == r7) goto L_0x005c
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0084:
                if (r2 < 0) goto L_0x008e
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x008e:
                r7 = -1
                if (r2 == r7) goto L_0x005c
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0097:
                if (r2 < 0) goto L_0x00a2
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00a2:
                r7 = -2
                if (r2 != r7) goto L_0x00b2
                int r2 = r0 + 1
                int r3 = r1 >> 4
                byte r3 = (byte) r3
                r5[r0] = r3
                r0 = 4
                r3 = r0
                r0 = r2
                r2 = r11
                goto L_0x0014
            L_0x00b2:
                r7 = -1
                if (r2 == r7) goto L_0x005c
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00bb:
                if (r2 < 0) goto L_0x00d8
                int r1 = r1 << 6
                r1 = r1 | r2
                int r2 = r0 + 2
                byte r3 = (byte) r1
                r5[r2] = r3
                int r2 = r0 + 1
                int r3 = r1 >> 8
                byte r3 = (byte) r3
                r5[r2] = r3
                int r2 = r1 >> 16
                byte r2 = (byte) r2
                r5[r0] = r2
                int r0 = r0 + 3
                r2 = 0
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00d8:
                r7 = -2
                if (r2 != r7) goto L_0x00ee
                int r2 = r0 + 1
                int r3 = r1 >> 2
                byte r3 = (byte) r3
                r5[r2] = r3
                int r2 = r1 >> 10
                byte r2 = (byte) r2
                r5[r0] = r2
                int r0 = r0 + 2
                r2 = 5
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x00ee:
                r7 = -1
                if (r2 == r7) goto L_0x005c
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x00f7:
                r7 = -2
                if (r2 != r7) goto L_0x0100
                int r2 = r3 + 1
                r3 = r2
                r2 = r11
                goto L_0x0014
            L_0x0100:
                r7 = -1
                if (r2 == r7) goto L_0x005c
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0109:
                r7 = -1
                if (r2 == r7) goto L_0x005c
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0112:
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            L_0x0118:
                int r1 = r0 + 1
                int r2 = r2 >> 4
                byte r2 = (byte) r2
                r5[r0] = r2
                r0 = r1
                goto L_0x001a
            L_0x0122:
                int r1 = r0 + 1
                int r4 = r2 >> 10
                byte r4 = (byte) r4
                r5[r0] = r4
                int r0 = r1 + 1
                int r2 = r2 >> 2
                byte r2 = (byte) r2
                r5[r1] = r2
                goto L_0x001a
            L_0x0132:
                r0 = 6
                r9.e = r0
                r0 = 0
                goto L_0x0006
            */
            throw new UnsupportedOperationException("Method not decompiled: vpadn.S.b.a(byte[], int, int, boolean):boolean");
        }
    }

    public static String a(byte[] bArr, int i) {
        int i2;
        int i3 = 2;
        try {
            int length = bArr.length;
            c cVar = new c(2, null);
            int i4 = (length / 3) * 4;
            if (!cVar.f109c) {
                switch (length % 3) {
                    case 1:
                        i4 += 2;
                        break;
                    case 2:
                        i4 += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i4 += 4;
            }
            if (!cVar.d || length <= 0) {
                i2 = i4;
            } else {
                int i5 = ((length - 1) / 57) + 1;
                if (!cVar.e) {
                    i3 = 1;
                }
                i2 = (i3 * i5) + i4;
            }
            cVar.a = new byte[i2];
            cVar.a(bArr, 0, length, true);
            if (a || cVar.b == i2) {
                return new String(cVar.a, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    static class c extends a {
        private static final byte[] f = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] g = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        private static /* synthetic */ boolean l;

        /* renamed from: c  reason: collision with root package name */
        public final boolean f109c;
        public final boolean d;
        public final boolean e;
        private final byte[] h;
        private int i;
        private int j;
        private final byte[] k;

        static {
            boolean z;
            if (!S.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            l = z;
        }

        public c(int i2, byte[] bArr) {
            boolean z;
            boolean z2 = true;
            this.a = null;
            this.f109c = (i2 & 1) == 0;
            if ((i2 & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.d = z;
            this.e = (i2 & 4) == 0 ? false : z2;
            this.k = (i2 & 8) == 0 ? f : g;
            this.h = new byte[2];
            this.i = 0;
            this.j = this.d ? 19 : -1;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final boolean a(byte[] bArr, int i2, int i3, boolean z) {
            int i4;
            byte b;
            int i5;
            int i6;
            int i7;
            int i8;
            int i9;
            byte b2;
            int i10;
            byte b3;
            int i11;
            byte b4;
            int i12;
            int i13;
            int i14;
            byte[] bArr2 = this.k;
            byte[] bArr3 = this.a;
            int i15 = 0;
            int i16 = this.j;
            int i17 = i3 + i2;
            switch (this.i) {
                case 0:
                    b = -1;
                    i4 = i2;
                    break;
                case 1:
                    if (i2 + 2 <= i17) {
                        int i18 = i2 + 1;
                        this.i = 0;
                        b = ((this.h[0] & 255) << 16) | ((bArr[i2] & 255) << 8) | (bArr[i18] & 255);
                        i4 = i18 + 1;
                        break;
                    }
                    b = -1;
                    i4 = i2;
                    break;
                case 2:
                    if (i2 + 1 <= i17) {
                        byte b5 = ((this.h[0] & 255) << 16) | ((this.h[1] & 255) << 8);
                        i4 = i2 + 1;
                        this.i = 0;
                        b = b5 | (bArr[i2] & 255);
                        break;
                    }
                    b = -1;
                    i4 = i2;
                    break;
                default:
                    b = -1;
                    i4 = i2;
                    break;
            }
            if (b != -1) {
                bArr3[0] = bArr2[(b >> 18) & 63];
                bArr3[1] = bArr2[(b >> 12) & 63];
                bArr3[2] = bArr2[(b >> 6) & 63];
                int i19 = 4;
                bArr3[3] = bArr2[b & 63];
                int i20 = i16 - 1;
                if (i20 == 0) {
                    if (this.e) {
                        i19 = 5;
                        bArr3[4] = 13;
                    }
                    i15 = i19 + 1;
                    bArr3[i19] = 10;
                    i6 = 19;
                } else {
                    i6 = i20;
                    i15 = 4;
                }
            } else {
                i6 = i16;
            }
            while (i5 + 3 <= i17) {
                byte b6 = ((bArr[i5] & 255) << 16) | ((bArr[i5 + 1] & 255) << 8) | (bArr[i5 + 2] & 255);
                bArr3[i7] = bArr2[(b6 >> 18) & 63];
                bArr3[i7 + 1] = bArr2[(b6 >> 12) & 63];
                bArr3[i7 + 2] = bArr2[(b6 >> 6) & 63];
                bArr3[i7 + 3] = bArr2[b6 & 63];
                int i21 = i5 + 3;
                int i22 = i7 + 4;
                int i23 = i6 - 1;
                if (i23 == 0) {
                    if (this.e) {
                        i14 = i22 + 1;
                        bArr3[i22] = 13;
                    } else {
                        i14 = i22;
                    }
                    i7 = i14 + 1;
                    bArr3[i14] = 10;
                    i5 = i21;
                    i13 = 19;
                } else {
                    i13 = i23;
                    i7 = i22;
                    i5 = i21;
                }
            }
            if (i5 - this.i == i17 - 1) {
                if (this.i > 0) {
                    i12 = 1;
                    b4 = this.h[0];
                } else {
                    b4 = bArr[i5];
                    i5++;
                    i12 = 0;
                }
                int i24 = (b4 & 255) << 4;
                this.i -= i12;
                int i25 = i7 + 1;
                bArr3[i7] = bArr2[(i24 >> 6) & 63];
                i8 = i25 + 1;
                bArr3[i25] = bArr2[i24 & 63];
                if (this.f109c) {
                    int i26 = i8 + 1;
                    bArr3[i8] = 61;
                    i8 = i26 + 1;
                    bArr3[i26] = 61;
                }
                if (this.d) {
                    if (this.e) {
                        bArr3[i8] = 13;
                        i8++;
                    }
                    i7 = i8 + 1;
                    bArr3[i8] = 10;
                }
                i7 = i8;
            } else if (i5 - this.i == i17 - 2) {
                if (this.i > 1) {
                    i10 = 1;
                    b2 = this.h[0];
                } else {
                    b2 = bArr[i5];
                    i5++;
                    i10 = 0;
                }
                int i27 = (b2 & 255) << 10;
                if (this.i > 0) {
                    b3 = this.h[i10];
                    i10++;
                } else {
                    b3 = bArr[i5];
                    i5++;
                }
                int i28 = ((b3 & 255) << 2) | i27;
                this.i -= i10;
                int i29 = i7 + 1;
                bArr3[i7] = bArr2[(i28 >> 12) & 63];
                int i30 = i29 + 1;
                bArr3[i29] = bArr2[(i28 >> 6) & 63];
                int i31 = i30 + 1;
                bArr3[i30] = bArr2[i28 & 63];
                if (this.f109c) {
                    i11 = i31 + 1;
                    bArr3[i31] = 61;
                } else {
                    i11 = i31;
                }
                if (this.d) {
                    if (this.e) {
                        bArr3[i8] = 13;
                        i8++;
                    }
                    i7 = i8 + 1;
                    bArr3[i8] = 10;
                }
                i7 = i8;
            } else if (this.d && i7 > 0 && i6 != 19) {
                if (this.e) {
                    i9 = i7 + 1;
                    bArr3[i7] = 13;
                } else {
                    i9 = i7;
                }
                i7 = i9 + 1;
                bArr3[i9] = 10;
            }
            if (!l && this.i != 0) {
                throw new AssertionError();
            } else if (l || i5 == i17) {
                this.b = i7;
                this.j = i6;
                return true;
            } else {
                throw new AssertionError();
            }
        }
    }

    private S() {
    }
}
