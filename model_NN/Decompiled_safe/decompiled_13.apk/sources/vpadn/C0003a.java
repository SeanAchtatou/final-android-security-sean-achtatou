package vpadn;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.RFC2109Spec;
import org.apache.http.message.BasicHeader;

@TargetApi(16)
/* renamed from: vpadn.a  reason: case insensitive filesystem */
public class C0003a {
    public static String i(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator().substring(0, 3);
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

    public static int a(String str, String str2) {
        return Log.d("VPON", "[::" + str + "::]  " + str2);
    }

    public static File c() {
        File file = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/", "/vpadn006");
        file.mkdirs();
        return new File(file, "vpadnguid006");
    }

    public static int b(String str, String str2) {
        return Log.e("VPON", "[::" + str + "::]  " + str2);
    }

    public static boolean c(String str) {
        int length;
        if (str == null || (length = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < length; i++) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static int a(String str, String str2, Throwable th) {
        return Log.e("VPON", "[::" + str + "::]  " + str2, th);
    }

    public static String j(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperator().substring(3);
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

    public static void b(Context context) {
        CookieSyncManager.createInstance(context);
        CookieManager.getInstance().setAcceptCookie(true);
    }

    public static int c(String str, String str2) {
        return Log.i("VPON", "[::" + str + "::]  " + str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0045 A[SYNTHETIC, Splitter:B:19:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0051 A[SYNTHETIC, Splitter:B:25:0x0051] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void d(java.lang.String r4) {
        /*
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            java.lang.String r2 = "/vpadn006"
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            boolean r0 = r0.exists()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            if (r0 == 0) goto L_0x001f
            java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            java.lang.String r2 = "/vpadn006"
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            a(r0)     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
        L_0x001f:
            java.io.File r0 = c()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            boolean r1 = r0.exists()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            if (r1 != 0) goto L_0x002c
            r0.createNewFile()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
        L_0x002c:
            r2 = 0
            java.io.PrintStream r1 = new java.io.PrintStream     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x004e }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x004e }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x004e }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x003e, all -> 0x004e }
            r1.print(r4)     // Catch:{ FileNotFoundException -> 0x0062 }
            r1.close()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
        L_0x003d:
            return
        L_0x003e:
            r0 = move-exception
            r1 = r2
        L_0x0040:
            r0.printStackTrace()     // Catch:{ all -> 0x005f }
            if (r1 == 0) goto L_0x003d
            r1.close()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
            goto L_0x003d
        L_0x0049:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003d
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            if (r2 == 0) goto L_0x0054
            r2.close()     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
        L_0x0054:
            throw r0     // Catch:{ IOException -> 0x0049, NullPointerException -> 0x0055, Exception -> 0x005a }
        L_0x0055:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003d
        L_0x005a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x003d
        L_0x005f:
            r0 = move-exception
            r2 = r1
            goto L_0x004f
        L_0x0062:
            r0 = move-exception
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: vpadn.C0003a.d(java.lang.String):void");
    }

    public static String e(String str, String str2) throws Exception {
        byte[] a = S.a(str, 0);
        byte[] bytes = str2.getBytes();
        if (a.length != 16) {
            throw new IllegalArgumentException("Key length should be 16.");
        }
        SecretKeySpec secretKeySpec = new SecretKeySpec(a, "AES");
        byte[] bArr = new byte[16];
        Arrays.fill(bArr, (byte) 0);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS7Padding");
        instance.init(1, secretKeySpec, ivParameterSpec);
        return S.a(instance.doFinal(bytes), 2);
    }

    public static int d(String str, String str2) {
        return Log.w("VPON", "[::" + str + "::]  " + str2);
    }

    public static DefaultHttpClient a(DefaultHttpClient defaultHttpClient) {
        defaultHttpClient.addRequestInterceptor(new T());
        defaultHttpClient.addResponseInterceptor(new U());
        return defaultHttpClient;
    }

    public static void a(String str, DefaultHttpClient defaultHttpClient) {
        BasicCookieStore basicCookieStore = new BasicCookieStore();
        defaultHttpClient.setCookieStore(basicCookieStore);
        CookieManager instance = CookieManager.getInstance();
        if (instance != null) {
            RFC2109Spec rFC2109Spec = new RFC2109Spec();
            try {
                URL url = new URL(str);
                String cookie = instance.getCookie(url.getHost());
                if (cookie != null) {
                    List<Cookie> parse = rFC2109Spec.parse(new BasicHeader("set-cookie", cookie), new CookieOrigin(url.getHost(), url.getPort() == -1 ? url.getDefaultPort() : url.getPort(), "/", false));
                    boolean z = false;
                    for (Cookie cookie2 : parse) {
                        if (!cookie2.getName().equals("Vpadn-Guid") || cookie2.getDomain() == null || cookie2.getDomain().indexOf("vpon.com") < 0) {
                            basicCookieStore.addCookie(cookie2);
                        } else {
                            a("CookieUtil", "Vpadn-Guid Cookie Found!! " + cookie2.toString());
                            z = true;
                            BasicClientCookie basicClientCookie = new BasicClientCookie("Vpadn-Guid", cookie2.getValue());
                            basicClientCookie.setDomain("vpon.com");
                            basicClientCookie.setPath("/");
                            basicCookieStore.addCookie(basicClientCookie);
                        }
                    }
                    if (!z) {
                        String d = d();
                        a("CookieUtil", "1.savedGuid:" + d);
                        if (d != null) {
                            BasicClientCookie basicClientCookie2 = new BasicClientCookie("Vpadn-Guid", d);
                            basicClientCookie2.setDomain("vpon.com");
                            basicClientCookie2.setPath("/");
                            parse.add(basicClientCookie2);
                            return;
                        }
                        return;
                    }
                    a("CookieUtil", "1.save cookie:" + ((String) null));
                    d((String) null);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (MalformedCookieException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static String k(Context context) {
        try {
            return String.valueOf(((GsmCellLocation) ((TelephonyManager) context.getSystemService("phone")).getCellLocation()).getCid());
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

    public static boolean a(String str) {
        if (!a() || str.equals("")) {
            return false;
        }
        String file = Environment.getExternalStorageDirectory().toString();
        return (str.startsWith(file) ? new File(str) : new File(String.valueOf(file) + "/" + str)).exists();
    }

    public static String l(Context context) {
        try {
            return String.valueOf(((GsmCellLocation) ((TelephonyManager) context.getSystemService("phone")).getCellLocation()).getLac());
        } catch (Exception e) {
            e.getStackTrace();
            return null;
        }
    }

    public static long a(boolean z) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return b(Environment.getExternalStorageDirectory().getPath());
        }
        if (z) {
            return b("/");
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0047 A[SYNTHETIC, Splitter:B:26:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c A[SYNTHETIC, Splitter:B:29:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0062 A[SYNTHETIC, Splitter:B:40:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0067 A[SYNTHETIC, Splitter:B:43:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x007d A[SYNTHETIC, Splitter:B:54:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0082 A[SYNTHETIC, Splitter:B:57:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0096 A[SYNTHETIC, Splitter:B:66:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x009b A[SYNTHETIC, Splitter:B:69:0x009b] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x0042=Splitter:B:23:0x0042, B:51:0x0078=Splitter:B:51:0x0078, B:37:0x005d=Splitter:B:37:0x005d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String d() {
        /*
            r0 = 0
            java.io.File r1 = c()
            if (r1 == 0) goto L_0x000d
            boolean r2 = r1.exists()
            if (r2 == 0) goto L_0x003c
        L_0x000d:
            boolean r2 = r1.canRead()
            if (r2 == 0) goto L_0x003c
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ UnsupportedEncodingException -> 0x00c3, FileNotFoundException -> 0x005a, IOException -> 0x0075, all -> 0x0090 }
            r3.<init>(r1)     // Catch:{ UnsupportedEncodingException -> 0x00c3, FileNotFoundException -> 0x005a, IOException -> 0x0075, all -> 0x0090 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ UnsupportedEncodingException -> 0x00c8, FileNotFoundException -> 0x00be, IOException -> 0x00b9, all -> 0x00b3 }
            java.lang.String r1 = "UTF-8"
            r2.<init>(r3, r1)     // Catch:{ UnsupportedEncodingException -> 0x00c8, FileNotFoundException -> 0x00be, IOException -> 0x00b9, all -> 0x00b3 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            r1.<init>(r2)     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            r4.<init>()     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
        L_0x0029:
            java.lang.String r5 = r1.readLine()     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            if (r5 != 0) goto L_0x003d
            java.lang.String r0 = r4.toString()     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            r1.close()     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            r3.close()     // Catch:{ IOException -> 0x00a9 }
        L_0x0039:
            r2.close()     // Catch:{ IOException -> 0x00ae }
        L_0x003c:
            return r0
        L_0x003d:
            r4.append(r5)     // Catch:{ UnsupportedEncodingException -> 0x0041, FileNotFoundException -> 0x00c1, IOException -> 0x00bc }
            goto L_0x0029
        L_0x0041:
            r1 = move-exception
        L_0x0042:
            r1.printStackTrace()     // Catch:{ all -> 0x00b7 }
            if (r3 == 0) goto L_0x004a
            r3.close()     // Catch:{ IOException -> 0x0055 }
        L_0x004a:
            if (r2 == 0) goto L_0x003c
            r2.close()     // Catch:{ IOException -> 0x0050 }
            goto L_0x003c
        L_0x0050:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003c
        L_0x0055:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x004a
        L_0x005a:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x005d:
            r1.printStackTrace()     // Catch:{ all -> 0x00b7 }
            if (r3 == 0) goto L_0x0065
            r3.close()     // Catch:{ IOException -> 0x0070 }
        L_0x0065:
            if (r2 == 0) goto L_0x003c
            r2.close()     // Catch:{ IOException -> 0x006b }
            goto L_0x003c
        L_0x006b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003c
        L_0x0070:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0065
        L_0x0075:
            r1 = move-exception
            r2 = r0
            r3 = r0
        L_0x0078:
            r1.printStackTrace()     // Catch:{ all -> 0x00b7 }
            if (r3 == 0) goto L_0x0080
            r3.close()     // Catch:{ IOException -> 0x008b }
        L_0x0080:
            if (r2 == 0) goto L_0x003c
            r2.close()     // Catch:{ IOException -> 0x0086 }
            goto L_0x003c
        L_0x0086:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003c
        L_0x008b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0080
        L_0x0090:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r0 = r1
        L_0x0094:
            if (r3 == 0) goto L_0x0099
            r3.close()     // Catch:{ IOException -> 0x009f }
        L_0x0099:
            if (r2 == 0) goto L_0x009e
            r2.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x009e:
            throw r0
        L_0x009f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0099
        L_0x00a4:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x009e
        L_0x00a9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0039
        L_0x00ae:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003c
        L_0x00b3:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x0094
        L_0x00b7:
            r0 = move-exception
            goto L_0x0094
        L_0x00b9:
            r1 = move-exception
            r2 = r0
            goto L_0x0078
        L_0x00bc:
            r1 = move-exception
            goto L_0x0078
        L_0x00be:
            r1 = move-exception
            r2 = r0
            goto L_0x005d
        L_0x00c1:
            r1 = move-exception
            goto L_0x005d
        L_0x00c3:
            r1 = move-exception
            r2 = r0
            r3 = r0
            goto L_0x0042
        L_0x00c8:
            r1 = move-exception
            r2 = r0
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: vpadn.C0003a.d():java.lang.String");
    }

    static long b(String str) {
        StatFs statFs = new StatFs(str);
        return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1024;
    }

    public static String c(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean a() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    public static String d(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            c("macAddress", "not use wifi for internet");
            return null;
        }
    }

    public static void b(String str, DefaultHttpClient defaultHttpClient) {
        String str2;
        String str3;
        String str4;
        boolean z = false;
        String str5 = null;
        List<Cookie> cookies = defaultHttpClient.getCookieStore().getCookies();
        if (cookies.size() <= 0) {
            a("CookieUtil", "cookies.size() < 1");
            return;
        }
        a("CookieUtil", "cookie size:" + cookies.size());
        CookieSyncManager instance = CookieSyncManager.getInstance();
        CookieManager instance2 = CookieManager.getInstance();
        if (instance2 == null) {
            a("CookieUtil", "appCookieManager is null");
            return;
        }
        for (Cookie cookie : cookies) {
            if (!cookie.getName().equals("$Version")) {
                String name = cookie.getName();
                String domain = cookie.getDomain();
                String path = cookie.getPath();
                if (!name.equals("Vpadn-Guid") || domain == null || domain.indexOf("vpon.com") < 0) {
                    str2 = str;
                } else {
                    z = true;
                    path = "/";
                    domain = "vpon.com";
                    str5 = cookie.getValue();
                    str2 = "vpon.com";
                }
                String str6 = String.valueOf(name) + "=" + str5;
                if (domain != null) {
                    str3 = String.valueOf(str6) + "; Domain=" + domain;
                } else {
                    str3 = str6;
                }
                if (path != null) {
                    str4 = String.valueOf(str3) + "; Path=" + path;
                } else {
                    str4 = str3;
                }
                a("CookieUtil", "appCookieManager.setCookie:" + str4);
                instance2.setCookie(str2, str4);
            }
        }
        if (!z) {
            String d = d();
            a("CookieUtil", "2.savedGuid:" + d);
            if (d != null) {
                instance2.setCookie("vpon.com", "Vpadn-Guid=" + d + "; Domain=vpon.com" + "; Path=/");
            }
        } else {
            a("CookieUtil", "2.save cookie:" + str5);
            d(str5);
        }
        instance.sync();
    }

    public static int m(Context context) {
        try {
            int type = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo().getType();
            switch (type) {
                case 0:
                    return 1;
                case 1:
                    return 0;
                default:
                    return type;
            }
        } catch (Exception e) {
            e.getStackTrace();
            return 0;
        }
    }

    public static boolean a(File file) {
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return true;
            }
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    a(listFiles[i]);
                } else {
                    listFiles[i].delete();
                }
            }
        }
        return file.delete();
    }

    public static String e(Context context) {
        try {
            return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getBSSID();
        } catch (Exception e) {
            c("macAddress", "not use wifi for internet");
            return null;
        }
    }

    public static String a(Context context) {
        File cacheDir;
        if (Environment.getExternalStorageState().equals("mounted")) {
            cacheDir = new File(String.valueOf(Environment.getExternalStorageDirectory().getAbsolutePath()) + "/Android/data/" + context.getPackageName() + "/cache/");
        } else {
            cacheDir = context.getCacheDir();
        }
        if (!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        return cacheDir.getAbsolutePath();
    }

    public static String b() {
        return String.valueOf(Build.VERSION.SDK_INT);
    }

    public static DisplayMetrics f(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String g(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String h(Context context) {
        try {
            W.a(context);
            if (W.b()) {
                return W.a();
            }
            return g(context);
        } catch (Exception e) {
            e.printStackTrace();
            return g(context);
        }
    }
}
