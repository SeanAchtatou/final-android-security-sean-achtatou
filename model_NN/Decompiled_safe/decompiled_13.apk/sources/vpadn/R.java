package vpadn;

import java.util.Arrays;

public final class R {
    private static final char[] a = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();
    private static final int[] b;

    static {
        int[] iArr = new int[256];
        b = iArr;
        Arrays.fill(iArr, -1);
        int length = a.length;
        for (int i = 0; i < length; i++) {
            b[a[i]] = i;
        }
        b[61] = 0;
    }

    public static final byte[] a(byte[] bArr, boolean z) {
        int i = 0;
        int length = bArr != null ? bArr.length : 0;
        if (length == 0) {
            return new byte[0];
        }
        int i2 = (length / 3) * 3;
        int i3 = (((length - 1) / 3) + 1) << 2;
        int i4 = i3 + (((i3 - 1) / 76) << 1);
        byte[] bArr2 = new byte[i4];
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i7 < i2) {
            int i8 = i7 + 1;
            int i9 = i8 + 1;
            byte b2 = ((bArr[i8] & 255) << 8) | ((bArr[i7] & 255) << 16);
            i7 = i9 + 1;
            byte b3 = b2 | (bArr[i9] & 255);
            int i10 = i6 + 1;
            bArr2[i6] = (byte) a[(b3 >>> 18) & 63];
            int i11 = i10 + 1;
            bArr2[i10] = (byte) a[(b3 >>> 12) & 63];
            int i12 = i11 + 1;
            bArr2[i11] = (byte) a[(b3 >>> 6) & 63];
            i6 = i12 + 1;
            bArr2[i12] = (byte) a[b3 & 63];
            i5++;
            if (i5 == 19 && i6 < i4 - 2) {
                int i13 = i6 + 1;
                bArr2[i6] = 13;
                bArr2[i13] = 10;
                i6 = i13 + 1;
                i5 = 0;
            }
        }
        int i14 = length - i2;
        if (i14 > 0) {
            int i15 = (bArr[i2] & 255) << 10;
            if (i14 == 2) {
                i = (bArr[length - 1] & 255) << 2;
            }
            int i16 = i | i15;
            bArr2[i4 - 4] = (byte) a[i16 >> 12];
            bArr2[i4 - 3] = (byte) a[(i16 >>> 6) & 63];
            bArr2[i4 - 2] = i14 == 2 ? (byte) a[i16 & 63] : 61;
            bArr2[i4 - 1] = 61;
        }
        return bArr2;
    }
}
