package com.heyzap.sdk;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class HeyzapLib {
    private static final String HEYZAP_ANALYTICS_ID_PREF = "heyzap_button_analytics_id";
    private static final String HEYZAP_INTENT_CLASS = ".CheckinForm";
    private static final String HEYZAP_PACKAGE = "com.heyzap.android";
    private static final String HEYZAP_URL = "http://www.heyzap.com";

    public static void init(Context context) {
        broadcastEnableSDK(context);
    }

    public static void checkin(Context context) {
        checkin(context, null);
    }

    public static void checkin(Context context, String prefillMessage) {
        if (packageIsInstalled(HEYZAP_PACKAGE, context)) {
            Intent popup = new Intent("android.intent.action.MAIN");
            popup.putExtra("message", prefillMessage);
            popup.setAction(HEYZAP_PACKAGE);
            popup.addCategory("android.intent.category.LAUNCHER");
            popup.putExtra("packageName", context.getPackageName());
            popup.setComponent(new ComponentName(HEYZAP_PACKAGE, "com.heyzap.android.CheckinForm"));
            context.startActivity(popup);
            return;
        }
        try {
            registerSDKClick(context);
            String uri = "market://details?id=com.heyzap.android&referrer=" + getAnalyticsReferrer(context);
            Log.d("Heyzap", "Sending to market, uri: " + uri);
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
        } catch (ActivityNotFoundException e) {
        }
    }

    private static void registerSDKClick(Context context) {
        final String url = "http://www.heyzap.com/mobile/register_sdk_click?game_package=" + context.getPackageName() + "&device_id=" + getDeviceId(context) + "&track_hash=" + getTrackHash(context);
        Log.d("Heyzap", "Sending analytics click request to " + url);
        new Thread(new Runnable() {
            public void run() {
                try {
                    HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setDoOutput(true);
                    conn.getInputStream();
                    Log.d("Heyzap", "Analytics click");
                } catch (MalformedURLException e) {
                    Log.d("Heyzap", "Malformed url: " + e.getMessage());
                } catch (IOException e2) {
                    Log.d("Heyzap", "IO Exception: " + e2.getMessage());
                }
            }
        }).start();
    }

    public static void broadcastEnableSDK(Context context) {
        Intent broadcast = new Intent("com.heyzap.android.enableSDK");
        broadcast.putExtra("packageName", context.getPackageName());
        context.sendBroadcast(broadcast);
    }

    private static boolean packageIsInstalled(String packageName, Context context) {
        PackageManager pm = context.getPackageManager();
        Intent pi = pm.getLaunchIntentForPackage(packageName);
        if (pi == null) {
            return false;
        }
        return pm.queryIntentActivities(pi, 65536).size() > 0;
    }

    public static void buttonSeenAnalytics(final Context context) {
        Log.d("Heyzap", "Heyzap button seen - registered? " + isAnalyticsRegistered(context));
        if (!isAnalyticsRegistered(context)) {
            final String url = "http://www.heyzap.com/mobile/register_sdk_view?game_package=" + context.getPackageName() + "&device_id=" + getDeviceId(context);
            Log.d("Heyzap", "Sending analytics request to " + url);
            new Thread(new Runnable() {
                public void run() {
                    try {
                        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
                        conn.setDoOutput(true);
                        String response = HeyzapLib.convertStreamToString(conn.getInputStream());
                        Log.d("Heyzap", "Analytics response: " + response);
                        SharedPreferences.Editor editor = context.getSharedPreferences(HeyzapLib.HEYZAP_ANALYTICS_ID_PREF, 0).edit();
                        editor.putString(HeyzapLib.HEYZAP_ANALYTICS_ID_PREF, response);
                        editor.commit();
                    } catch (MalformedURLException e) {
                        Log.d("Heyzap", "Malformed url: " + e.getMessage());
                    } catch (IOException e2) {
                        Log.d("Heyzap", "IO Exception: " + e2.getMessage());
                    } catch (Exception e3) {
                        Log.d("Heyzap", "Exception: " + e3.getMessage());
                    }
                }
            }).start();
        }
    }

    /* access modifiers changed from: private */
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(String.valueOf(line) + "\n");
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    private static String getDeviceId(Context mContext) {
        String product = Build.PRODUCT;
        String androidId = Settings.Secure.getString(mContext.getContentResolver(), "android_id");
        if (product == null || androidId == null) {
            return null;
        }
        return String.valueOf(product) + "_" + androidId;
    }

    private static String getTrackHash(Context context) {
        return context.getSharedPreferences(HEYZAP_ANALYTICS_ID_PREF, 0).getString(HEYZAP_ANALYTICS_ID_PREF, "");
    }

    private static String getAnalyticsReferrer(Context context) {
        String dm_registration = getTrackHash(context);
        Log.d("Heyzap", "Analytics registration: " + dm_registration);
        if (!dm_registration.equals("")) {
            return "heyzap_" + dm_registration;
        }
        return URLEncoder.encode("src=sdk&game_package=" + context.getPackageName());
    }

    private static boolean isAnalyticsRegistered(Context context) {
        return !context.getSharedPreferences(HEYZAP_ANALYTICS_ID_PREF, 0).getString(HEYZAP_ANALYTICS_ID_PREF, "undefined").equals("undefined");
    }
}
