package com.anoshenko.android.theme;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ResourceFile {
    private static final String MANIFEST_FILE = "manifest.xml";
    private static final String TAG_APPEARANCE = "Appearance";
    private static final String TAG_CARD_BACK = "CardBack";
    private static final String TAG_COLORS = "Colors";
    private static final String TAG_ICONS = "Icons";
    private static final String TAG_PACK = "Pack";
    private final Vector<CardBack> mCardBacks = new Vector<>();
    private final String mZipFilename;

    public ResourceFile(String zip_filename) throws IOException, IllegalStateException, SAXException, ParserConfigurationException {
        this.mZipFilename = zip_filename;
        ZipFile zipFile = new ZipFile(zip_filename);
        try {
            ZipEntry manifest = zipFile.getEntry(MANIFEST_FILE);
            if (manifest == null) {
                throw new FileNotFoundException("manifest.xml not found");
            }
            NodeList root_nodes = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(zipFile.getInputStream(manifest)).getChildNodes();
            int root_node_count = root_nodes.getLength();
            int j = 0;
            while (true) {
                if (j >= root_node_count) {
                    break;
                }
                Node root_node = root_nodes.item(j);
                if (root_node.getNodeName().equals(TAG_APPEARANCE)) {
                    NodeList nodes = root_node.getChildNodes();
                    int node_count = nodes.getLength();
                    for (int i = 0; i < node_count; i++) {
                        Node node = nodes.item(i);
                        String node_name = node.getNodeName();
                        if (node_name.equals("CardBack")) {
                            parseCardBack(zipFile, node);
                        } else if (node_name.equals(TAG_PACK)) {
                            parsePack(zipFile, node);
                        } else if (node_name.equals(TAG_ICONS)) {
                            parseIcons(zipFile, node);
                        } else if (node_name.equals(TAG_COLORS)) {
                            parseColors(zipFile, node);
                        }
                    }
                } else {
                    j++;
                }
            }
        } finally {
            zipFile.close();
        }
    }

    private void parseCardBack(ZipFile zip_file, Node node) {
    }

    private void parsePack(ZipFile zip_file, Node node) {
    }

    private void parseIcons(ZipFile zip_file, Node node) {
    }

    private void parseColors(ZipFile zip_file, Node node) {
    }

    public Vector<CardBack> getCardBacks() {
        return this.mCardBacks;
    }

    public String getFilename() {
        return this.mZipFilename;
    }
}
