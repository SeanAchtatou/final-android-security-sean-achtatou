package com.anoshenko.android.theme;

public interface GalleryResultListener {
    void onGalleryResult(String str);
}
