package com.anoshenko.android.theme;

import android.graphics.Bitmap;
import com.anoshenko.android.solitaires.CardSize;

public interface CardResources {
    int getAngle(CardSize cardSize);

    int getBorder(CardSize cardSize);

    Bitmap getJokerImage(CardSize cardSize);

    int[] getPictureData(CardSize cardSize);

    int getPictureImageHeight(CardSize cardSize);

    Bitmap getPictureImages(CardSize cardSize);

    Bitmap getSuitImages(CardSize cardSize);
}
