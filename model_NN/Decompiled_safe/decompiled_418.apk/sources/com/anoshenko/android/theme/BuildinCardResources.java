package com.anoshenko.android.theme;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import com.anoshenko.android.solitaires.CardSize;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;

public class BuildinCardResources implements CardResources {
    private static final int[] ANGLE = {1, 2, 2, 2, 3, 3, 3, 3, 3, 4};
    private static final int HVGA = 1;
    private static final int[] JOKER_ID = {R.drawable.joker_24, R.drawable.joker_32, R.drawable.joker_32, R.drawable.joker_40, R.drawable.joker_48, R.drawable.joker_60, R.drawable.joker_60, R.drawable.joker_75, R.drawable.joker_75, R.drawable.joker_96};
    private static final int[][] PICTURES_DATA = {PICTURES_DATA_24, PICTURES_DATA_30, PICTURES_DATA_32, PICTURES_DATA_40, PICTURES_DATA_48, PICTURES_DATA_60, PICTURES_DATA_60, PICTURES_DATA_75, PICTURES_DATA_75, PICTURES_DATA_96};
    private static final int[] PICTURES_DATA_24;
    private static final int[] PICTURES_DATA_30;
    private static final int[] PICTURES_DATA_32;
    private static final int[] PICTURES_DATA_40;
    private static final int[] PICTURES_DATA_48;
    private static final int[] PICTURES_DATA_60;
    private static final int[] PICTURES_DATA_75;
    private static final int[] PICTURES_DATA_96;
    private static final int[] PICTURES_ID = {R.drawable.pictures_24, R.drawable.pictures_32, R.drawable.pictures_32, R.drawable.pictures_40, R.drawable.pictures_48, R.drawable.pictures_60, R.drawable.pictures_60, R.drawable.pictures_75, R.drawable.pictures_75, R.drawable.pictures_96};
    private static final int QVGA = 0;
    private static final int[] SUIT_ID = {R.drawable.suit_8, R.drawable.suit_12, R.drawable.suit_12, R.drawable.suit_14, R.drawable.suit_17, R.drawable.suit_21, R.drawable.suit_21, R.drawable.suit_25, R.drawable.suit_25, R.drawable.suit_28};
    private static final int SVGA = 3;
    private static final int VGA = 2;
    private static final int XVGA = 4;
    private final Context mContext;
    private final int mSizeIndex;

    static {
        int[] iArr = new int[26];
        iArr[1] = 9;
        iArr[2] = 9;
        iArr[3] = 5;
        iArr[4] = 34;
        iArr[5] = 5;
        iArr[6] = 9;
        iArr[7] = 15;
        iArr[8] = 19;
        iArr[9] = 15;
        iArr[10] = 34;
        iArr[11] = 15;
        iArr[12] = 44;
        iArr[13] = 15;
        iArr[14] = 54;
        iArr[15] = 15;
        iArr[16] = 69;
        iArr[17] = 12;
        iArr[18] = 77;
        iArr[19] = 12;
        iArr[20] = 89;
        iArr[21] = 20;
        iArr[22] = 109;
        iArr[23] = 20;
        iArr[24] = 129;
        iArr[25] = 20;
        PICTURES_DATA_24 = iArr;
        int[] iArr2 = new int[26];
        iArr2[0] = 199;
        iArr2[1] = 14;
        iArr2[3] = 7;
        iArr2[4] = 35;
        iArr2[5] = 7;
        iArr2[7] = 21;
        iArr2[8] = 14;
        iArr2[9] = 21;
        iArr2[10] = 35;
        iArr2[11] = 21;
        iArr2[12] = 49;
        iArr2[13] = 21;
        iArr2[14] = 63;
        iArr2[15] = 21;
        iArr2[16] = 84;
        iArr2[17] = 15;
        iArr2[18] = 94;
        iArr2[19] = 15;
        iArr2[20] = 110;
        iArr2[21] = 28;
        iArr2[22] = 170;
        iArr2[23] = 28;
        iArr2[24] = 140;
        iArr2[25] = 28;
        PICTURES_DATA_30 = iArr2;
        int[] iArr3 = new int[26];
        iArr3[0] = 199;
        iArr3[1] = 14;
        iArr3[3] = 7;
        iArr3[4] = 35;
        iArr3[5] = 7;
        iArr3[7] = 21;
        iArr3[8] = 14;
        iArr3[9] = 21;
        iArr3[10] = 35;
        iArr3[11] = 21;
        iArr3[12] = 49;
        iArr3[13] = 21;
        iArr3[14] = 63;
        iArr3[15] = 21;
        iArr3[16] = 84;
        iArr3[17] = 15;
        iArr3[18] = 94;
        iArr3[19] = 15;
        iArr3[20] = 109;
        iArr3[21] = 30;
        iArr3[22] = 169;
        iArr3[23] = 30;
        iArr3[24] = 139;
        iArr3[25] = 30;
        PICTURES_DATA_32 = iArr3;
        int[] iArr4 = new int[26];
        iArr4[0] = 261;
        iArr4[1] = 17;
        iArr4[3] = 9;
        iArr4[4] = 45;
        iArr4[5] = 9;
        iArr4[7] = 27;
        iArr4[8] = 18;
        iArr4[9] = 27;
        iArr4[10] = 45;
        iArr4[11] = 27;
        iArr4[12] = 63;
        iArr4[13] = 27;
        iArr4[14] = 81;
        iArr4[15] = 27;
        iArr4[16] = 108;
        iArr4[17] = 23;
        iArr4[18] = 124;
        iArr4[19] = 23;
        iArr4[20] = 147;
        iArr4[21] = 38;
        iArr4[22] = 185;
        iArr4[23] = 38;
        iArr4[24] = 223;
        iArr4[25] = 38;
        PICTURES_DATA_40 = iArr4;
        int[] iArr5 = new int[26];
        iArr5[1] = 20;
        iArr5[2] = 20;
        iArr5[3] = 11;
        iArr5[4] = 79;
        iArr5[5] = 11;
        iArr5[6] = 20;
        iArr5[7] = 35;
        iArr5[8] = 44;
        iArr5[9] = 35;
        iArr5[10] = 79;
        iArr5[11] = 35;
        iArr5[12] = 103;
        iArr5[13] = 35;
        iArr5[14] = 127;
        iArr5[15] = 35;
        iArr5[16] = 162;
        iArr5[17] = 33;
        iArr5[18] = 186;
        iArr5[19] = 33;
        iArr5[20] = 219;
        iArr5[21] = 44;
        iArr5[22] = 263;
        iArr5[23] = 44;
        iArr5[24] = 307;
        iArr5[25] = 44;
        PICTURES_DATA_48 = iArr5;
        int[] iArr6 = new int[26];
        iArr6[1] = 25;
        iArr6[2] = 25;
        iArr6[3] = 13;
        iArr6[4] = 98;
        iArr6[5] = 13;
        iArr6[6] = 25;
        iArr6[7] = 43;
        iArr6[8] = 55;
        iArr6[9] = 43;
        iArr6[10] = 98;
        iArr6[11] = 43;
        iArr6[12] = 128;
        iArr6[13] = 43;
        iArr6[14] = 158;
        iArr6[15] = 43;
        iArr6[16] = 201;
        iArr6[17] = 43;
        iArr6[18] = 233;
        iArr6[19] = 43;
        iArr6[20] = 276;
        iArr6[21] = 56;
        iArr6[22] = 332;
        iArr6[23] = 56;
        iArr6[24] = 388;
        iArr6[25] = 56;
        PICTURES_DATA_60 = iArr6;
        int[] iArr7 = new int[26];
        iArr7[1] = 25;
        iArr7[2] = 25;
        iArr7[3] = 15;
        iArr7[4] = 100;
        iArr7[5] = 15;
        iArr7[6] = 25;
        iArr7[7] = 45;
        iArr7[8] = 55;
        iArr7[9] = 45;
        iArr7[10] = 100;
        iArr7[11] = 45;
        iArr7[12] = 130;
        iArr7[13] = 45;
        iArr7[14] = 160;
        iArr7[15] = 45;
        iArr7[16] = 205;
        iArr7[17] = 43;
        iArr7[18] = 235;
        iArr7[19] = 43;
        iArr7[20] = 278;
        iArr7[21] = 71;
        iArr7[22] = 349;
        iArr7[23] = 71;
        iArr7[24] = 420;
        iArr7[25] = 71;
        PICTURES_DATA_75 = iArr7;
        int[] iArr8 = new int[26];
        iArr8[1] = 31;
        iArr8[2] = 31;
        iArr8[3] = 21;
        iArr8[4] = 140;
        iArr8[5] = 21;
        iArr8[6] = 31;
        iArr8[7] = 65;
        iArr8[8] = 75;
        iArr8[9] = 65;
        iArr8[10] = 140;
        iArr8[11] = 65;
        iArr8[12] = 184;
        iArr8[13] = 65;
        iArr8[14] = 228;
        iArr8[15] = 65;
        iArr8[16] = 293;
        iArr8[17] = 61;
        iArr8[18] = 335;
        iArr8[19] = 61;
        iArr8[20] = 396;
        iArr8[21] = 96;
        iArr8[22] = 492;
        iArr8[23] = 96;
        iArr8[24] = 588;
        iArr8[25] = 96;
        PICTURES_DATA_96 = iArr8;
    }

    public BuildinCardResources(Context context) {
        this.mContext = context;
        DisplayMetrics dm = Utils.getDisplayMetrics(context);
        int min_dimension = Math.min(dm.widthPixels, dm.heightPixels);
        if (min_dimension == 240) {
            this.mSizeIndex = 0;
        } else if (min_dimension >= 768) {
            this.mSizeIndex = 4;
        } else if (min_dimension >= 600) {
            this.mSizeIndex = 3;
        } else if (min_dimension >= 480) {
            this.mSizeIndex = 2;
        } else {
            this.mSizeIndex = 1;
        }
    }

    public Bitmap getJokerImage(CardSize size) {
        return Utils.loadBitmap(this.mContext.getResources(), JOKER_ID[(this.mSizeIndex * 2) + size.mId]);
    }

    public Bitmap getSuitImages(CardSize size) {
        return Utils.loadBitmap(this.mContext.getResources(), SUIT_ID[(this.mSizeIndex * 2) + size.mId]);
    }

    public Bitmap getPictureImages(CardSize size) {
        return Utils.loadBitmap(this.mContext.getResources(), PICTURES_ID[(this.mSizeIndex * 2) + size.mId]);
    }

    public int getPictureImageHeight(CardSize size) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(this.mContext.getResources(), PICTURES_ID[(this.mSizeIndex * 2) + size.mId]);
        return opts.outHeight;
    }

    public int[] getPictureData(CardSize size) {
        return PICTURES_DATA[(this.mSizeIndex * 2) + size.mId];
    }

    public int getBorder(CardSize size) {
        return this.mSizeIndex >= 2 ? 2 : 1;
    }

    public int getAngle(CardSize size) {
        return ANGLE[(this.mSizeIndex * 2) + size.mId];
    }
}
