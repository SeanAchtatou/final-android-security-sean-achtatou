package com.anoshenko.android.theme;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import com.anoshenko.android.solitaires.Card;
import com.anoshenko.android.solitaires.CardData;
import com.anoshenko.android.solitaires.CardSize;
import com.anoshenko.android.solitaires.ValueFont;

public class CardsPreview extends View {
    private CardData mCardData;
    private final Context mContext;

    public CardsPreview(Context context) {
        super(context);
        this.mContext = context;
    }

    public CardsPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    public CardsPreview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
    }

    /* access modifiers changed from: package-private */
    public void setCards(CardSize size, ValueFont font) {
        if (this.mCardData != null) {
            this.mCardData.release();
            this.mCardData = null;
        }
        try {
            this.mCardData = new CardData(size, this.mContext, font, true);
            invalidate();
        } catch (CardData.InvalidSizeException e) {
            e.printStackTrace();
        }
    }

    public void onDraw(Canvas canvas) {
        if (this.mCardData != null) {
            int y = 0;
            int count = Math.min(getWidth() / this.mCardData.Width, 13);
            int[] values = new int[count];
            if (count >= 1) {
                values[0] = 1;
            }
            if (count >= 2) {
                values[count - 1] = 13;
            }
            if (count >= 3) {
                values[count - 2] = 12;
            }
            if (count >= 4) {
                values[count - 3] = 11;
            }
            if (count >= 5) {
                values[count - 4] = 10;
            }
            if (count >= 6) {
                for (int i = 1; i <= count - 5; i++) {
                    values[i] = i + 1;
                }
            }
            for (int suit = 0; suit < 4; suit++) {
                int x = 0;
                for (int value : values) {
                    Card card = new Card(suit, value, this.mCardData);
                    card.xPos = x;
                    card.yPos = y;
                    card.mNextOffset = 0;
                    card.mOpen = true;
                    card.draw(canvas, null);
                    x += card.getWidth();
                }
                y += this.mCardData.Height;
            }
        }
    }
}
