package com.anoshenko.android.theme;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.preference.PreferenceManager;
import com.anoshenko.android.solitaires.CardData;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;
import com.anoshenko.android.ui.ToolbarTheme;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class CardBack {
    private static final String ATTR_ADDITIONAL_COLOR = "additional_color";
    private static final String ATTR_BORDER_COLOR = "border";
    private static final String ATTR_FILENAME = "filename";
    private static final String ATTR_TYPE = "type";
    private static final String ATTR_VALUE_STRETCH = "STRETCH";
    private static final String ATTR_VALUE_STRETCH_AND_CUT = "STRETCH AND CUT";
    private static final String ATTR_VALUE_TILE = "TILE";
    private static final String BORDER_COLOR_KEY = "CARDBACK_BORDER";
    private static final int CUSTOM_BITMAP = -2;
    private static final String FRAME_COLOR_KEY = "CARDBACK_FRAME_COLOR";
    private static final String FRAME_KEY = "CARDBACK_FRAME";
    private static final String IMAGE_FILE_KEY = "CARDBACK_IMAGE_FILE";
    public static final int STRETCH = 1;
    public static final int STRETCH_AND_CUT = 2;
    public static final int TILE = 0;
    private static final String TYPE_KEY = "CARDBACK_IMAGE_TYPE";
    private static final int ZIP_FILE_BITMAP = -1;
    private int mBorderColor;
    private boolean mFrame;
    private int mFrameColor;
    private String mImageFile;
    public final int mImageId;
    private int mType;
    private ZipFile mZipFile;

    public CardBack() {
        this.mImageId = -1;
    }

    public CardBack(Context context) {
        this.mImageId = CUSTOM_BITMAP;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.mImageFile = prefs.getString(IMAGE_FILE_KEY, null);
        this.mType = prefs.getInt(TYPE_KEY, 1);
        this.mBorderColor = prefs.getInt(BORDER_COLOR_KEY, -16777216);
        this.mFrame = prefs.getBoolean(FRAME_KEY, false);
        this.mFrameColor = prefs.getInt(FRAME_COLOR_KEY, -1);
    }

    public CardBack(int buildin_number) {
        switch (buildin_number) {
            case 0:
                this.mImageId = R.drawable.back0;
                this.mBorderColor = -16777216;
                this.mFrame = true;
                this.mFrameColor = -1;
                this.mType = 0;
                return;
            case 1:
                this.mImageId = R.drawable.back1;
                this.mBorderColor = -16777216;
                this.mFrame = true;
                this.mFrameColor = -1;
                this.mType = 0;
                return;
            case 2:
                this.mImageId = R.drawable.back2;
                this.mBorderColor = -4416256;
                this.mType = 2;
                return;
            case 3:
                this.mImageId = R.drawable.back3;
                this.mBorderColor = -3890432;
                this.mType = 2;
                return;
            case 4:
                this.mImageId = R.drawable.back4;
                this.mBorderColor = -8947754;
                this.mType = 1;
                return;
            case 5:
                this.mImageId = R.drawable.back5;
                this.mBorderColor = -10240;
                this.mType = 1;
                return;
            case 6:
                this.mImageId = R.drawable.back6;
                this.mBorderColor = -16767233;
                this.mType = 1;
                return;
            case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                this.mImageId = R.drawable.back7;
                this.mBorderColor = -8947754;
                this.mType = 1;
                return;
            default:
                this.mImageId = -1;
                return;
        }
    }

    public CardBack(ZipFile zip_file, Node node_root) throws ManifestParseException {
        this.mImageId = -1;
        if (!parse(zip_file, node_root)) {
            throw new ManifestParseException();
        }
        this.mZipFile = zip_file;
    }

    private boolean parse(ZipFile zip_file, Node node_root) {
        NamedNodeMap attrs = node_root.getAttributes();
        if (attrs == null) {
            return false;
        }
        Node node = attrs.getNamedItem(ATTR_FILENAME);
        if (node == null) {
            return false;
        }
        this.mImageFile = node.getNodeValue();
        if (zip_file.getEntry(this.mImageFile) == null) {
            return false;
        }
        Node node2 = attrs.getNamedItem(ATTR_TYPE);
        if (node2 != null) {
            String value = node2.getNodeValue().toUpperCase();
            if (value.equals(ATTR_VALUE_TILE) || value.equals(Integer.toString(0))) {
                this.mType = 0;
            } else if (value.equals(ATTR_VALUE_STRETCH) || value.equals(Integer.toString(1))) {
                this.mType = 1;
            } else if (value.equals(ATTR_VALUE_STRETCH_AND_CUT) || value.equals(Integer.toString(2))) {
                this.mType = 2;
            }
        }
        Node node3 = attrs.getNamedItem(ATTR_BORDER_COLOR);
        if (node3 != null) {
            try {
                this.mBorderColor = ParseUtil.parseColor(node3.getNodeValue());
            } catch (ManifestParseException | DOMException e) {
            }
        }
        Node node4 = attrs.getNamedItem(ATTR_ADDITIONAL_COLOR);
        if (node4 != null) {
            try {
                this.mFrameColor = ParseUtil.parseColor(node4.getNodeValue());
                this.mFrame = true;
            } catch (ManifestParseException | DOMException e2) {
            }
        } else {
            this.mFrame = false;
        }
        return true;
    }

    public boolean setType(int type) {
        switch (type) {
            case 0:
            case 1:
            case 2:
                this.mType = type;
                return true;
            default:
                return false;
        }
    }

    public int getType() {
        return this.mType;
    }

    public void setImageFile(String image_file) {
        this.mImageFile = image_file;
    }

    public String getImageFile() {
        return this.mImageFile;
    }

    public void setFrameBorder(boolean frame, int color) {
        this.mFrame = frame;
        this.mFrameColor = color;
    }

    public boolean isFrame() {
        return this.mFrame;
    }

    public int getFrameColor() {
        return this.mFrameColor;
    }

    public void setBorderColor(int color) {
        this.mBorderColor = color;
    }

    public int getBorderColor() {
        return this.mBorderColor;
    }

    public Bitmap createBitmap(Context context, CardData data) {
        Bitmap image;
        RectF back_rect;
        ZipEntry entry;
        Bitmap back_image = null;
        if (this.mImageId == -1) {
            if (!(this.mZipFile == null || this.mImageFile == null || (entry = this.mZipFile.getEntry(this.mImageFile)) != null)) {
                try {
                    InputStream in_stream = this.mZipFile.getInputStream(entry);
                    back_image = BitmapFactory.decodeStream(in_stream);
                    in_stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (back_image == null) {
                return new CardBack(0).createBitmap(context, data);
            }
        }
        if (this.mImageId == CUSTOM_BITMAP) {
            if (this.mImageFile != null) {
                back_image = Utils.loadBitmap(this.mImageFile, data.Width - 2, data.Height - 2);
            }
            if (back_image == null) {
                return new CardBack(0).createBitmap(context, data);
            }
        } else {
            back_image = BitmapFactory.decodeResource(context.getResources(), this.mImageId);
        }
        try {
            image = Bitmap.createBitmap(data.Width, data.Height, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e2) {
            System.gc();
            try {
                image = Bitmap.createBitmap(data.Width, data.Height, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError e3) {
                e3.printStackTrace();
                return null;
            }
        }
        image.eraseColor(0);
        Canvas canvas = new Canvas(image);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setStyle(Paint.Style.FILL);
        float r = data.getBorderRadius();
        RectF rectF = new RectF(0.5f, 0.5f, ((float) data.Width) - 0.5f, ((float) data.Height) - 0.5f);
        if (this.mFrame) {
            paint.setColor(this.mFrameColor);
            canvas.drawRoundRect(rectF, r, r, paint);
            float border2 = 1.0f + ((float) data.getBorderWidth());
            back_rect = new RectF(border2, border2, ((float) data.Width) - border2, ((float) data.Height) - border2);
            r -= (float) data.getBorderWidth();
            if (r < 1.0f) {
                r = 1.0f;
            }
        } else {
            back_rect = this.mType != 0 ? new RectF(1.0f, 1.0f, ((float) data.Width) - 1.0f, ((float) data.Height) - 1.0f) : rectF;
        }
        if (back_image != null) {
            switch (this.mType) {
                case 0:
                    Shader old = paint.setShader(new BitmapShader(back_image, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
                    canvas.drawRoundRect(back_rect, r, r, paint);
                    paint.setShader(old);
                    break;
                case 1:
                    canvas.drawBitmap(back_image, new Rect(0, 0, back_image.getWidth(), back_image.getHeight()), back_rect, paint);
                    break;
                case 2:
                    int back_width = back_image.getWidth();
                    int back_height = back_image.getHeight();
                    int src_width = back_width;
                    int src_height = (((int) back_rect.height()) * src_width) / ((int) back_rect.width());
                    if (src_height > back_height) {
                        src_height = back_height;
                        src_width = (((int) back_rect.width()) * src_height) / ((int) back_rect.height());
                    }
                    int x = (back_width - src_width) / 2;
                    int y = (back_height - src_height) / 2;
                    canvas.drawBitmap(back_image, new Rect(x, y, x + src_width, y + src_height), back_rect, paint);
                    break;
            }
        }
        paint.reset();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        if (this.mFrame) {
            float off = 1.0f + (((float) data.getBorderWidth()) / 2.0f);
            back_rect.set(off, off, ((float) data.Width) - off, ((float) data.Height) - off);
            paint.setStrokeWidth((float) data.getBorderWidth());
            paint.setColor(this.mFrameColor);
            float r2 = data.getBorderRadius() - off;
            canvas.drawRoundRect(back_rect, r2, r2, paint);
        }
        paint.setStrokeWidth(1.0f);
        paint.setColor(this.mBorderColor);
        float r3 = data.getBorderRadius();
        canvas.drawRoundRect(rectF, r3, r3, paint);
        return image;
    }

    public void save(Context context) {
        if (this.mImageId == CUSTOM_BITMAP) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
            editor.putString(IMAGE_FILE_KEY, this.mImageFile);
            editor.putInt(TYPE_KEY, this.mType);
            editor.putInt(BORDER_COLOR_KEY, this.mBorderColor);
            editor.putBoolean(FRAME_KEY, this.mFrame);
            editor.putInt(FRAME_COLOR_KEY, this.mFrameColor);
            editor.commit();
        }
    }
}
