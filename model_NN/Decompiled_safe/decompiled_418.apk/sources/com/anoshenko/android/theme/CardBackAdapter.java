package com.anoshenko.android.theme;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.anoshenko.android.solitaires.CardData;
import com.anoshenko.android.solitaires.CardSize;
import com.anoshenko.android.solitaires.R;
import java.util.ArrayList;

public class CardBackAdapter implements ListAdapter, AdapterView.OnItemClickListener {
    private final CardData mCardData;
    private final Context mContext;
    private ArrayList<DataSetObserver> mDataSetObservers = new ArrayList<>();
    private final GridView mGrid;
    private Bitmap[] mSamples;

    public CardBackAdapter(Context context, GridView grid) {
        CardData.InvalidSizeException e;
        this.mContext = context;
        this.mGrid = grid;
        this.mSamples = new Bitmap[8];
        CardData data = null;
        try {
            CardData data2 = new CardData(CardSize.NORMAL, context, null, false);
            int i = 0;
            while (i < this.mSamples.length) {
                try {
                    this.mSamples[i] = new CardBack(i).createBitmap(context, data2);
                    i++;
                } catch (CardData.InvalidSizeException e2) {
                    e = e2;
                    data = data2;
                    e.printStackTrace();
                    this.mCardData = data;
                    this.mGrid.setAdapter((ListAdapter) this);
                    this.mGrid.setOnItemClickListener(this);
                }
            }
            data = data2;
        } catch (CardData.InvalidSizeException e3) {
            e = e3;
            e.printStackTrace();
            this.mCardData = data;
            this.mGrid.setAdapter((ListAdapter) this);
            this.mGrid.setOnItemClickListener(this);
        }
        this.mCardData = data;
        this.mGrid.setAdapter((ListAdapter) this);
        this.mGrid.setOnItemClickListener(this);
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return this.mSamples.length == 0;
    }

    public int getCount() {
        return this.mSamples.length;
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.cardback_view, (ViewGroup) null);
        }
        if (this.mSamples[position] != null) {
            ((ImageView) convertView.findViewById(R.id.CardBackSample)).setImageBitmap(this.mSamples[position]);
        }
        convertView.setBackgroundResource(position == this.mCardData.mBackNumber ? R.drawable.current_card_back : R.drawable.passive_card_back);
        return convertView;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.mDataSetObservers.indexOf(observer);
        if (index >= 0 && index < this.mDataSetObservers.size()) {
            this.mDataSetObservers.remove(index);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        if (this.mCardData != null) {
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
            editor.putInt(CardData.CARD_BACK_KEY, position);
            editor.commit();
            this.mCardData.mBackNumber = position;
        }
        this.mGrid.invalidateViews();
    }
}
