package com.anoshenko.android.theme;

public class ManifestParseException extends Exception {
    private static final long serialVersionUID = 1;

    public ManifestParseException() {
    }

    public ManifestParseException(String detailMessage) {
        super(detailMessage);
    }

    public ManifestParseException(Throwable throwable) {
        super(throwable);
    }

    public ManifestParseException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
