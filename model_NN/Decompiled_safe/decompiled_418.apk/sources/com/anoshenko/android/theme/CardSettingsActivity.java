package com.anoshenko.android.theme;

import android.app.TabActivity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TabHost;
import com.anoshenko.android.solitaires.CardData;
import com.anoshenko.android.solitaires.CardSize;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;
import com.anoshenko.android.solitaires.ValueFont;
import com.anoshenko.android.ui.ColorChooser;
import com.anoshenko.android.ui.ColorSelectListener;

public class CardSettingsActivity extends TabActivity implements CompoundButton.OnCheckedChangeListener {
    protected static final int ABOUT_COMMAND = 100;
    static final int ABOUT_DIALOG = 4097;
    private static final String BACK_TAG = "backs";
    private static final String CARDS_TAG = "cards";
    static final int MENU_DIALOG = 4098;
    /* access modifiers changed from: private */
    public int mCardBackType = 0;
    /* access modifiers changed from: private */
    public CardSize mCardSize;
    /* access modifiers changed from: private */
    public CardsPreview mCardsPreview;
    /* access modifiers changed from: private */
    public CardBack mCustomCardBack;
    /* access modifiers changed from: private */
    public ValueFont mValueFont;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TabHost tabHost = getTabHost();
        LayoutInflater.from(this).inflate((int) R.layout.card_settings, (ViewGroup) tabHost.getTabContentView(), true);
        Resources res = getResources();
        tabHost.addTab(tabHost.newTabSpec(CARDS_TAG).setIndicator(getString(R.string.cards), new BitmapDrawable(Utils.loadBitmap(res, R.drawable.icon_game))).setContent((int) R.id.CardSettingsPage));
        tabHost.addTab(tabHost.newTabSpec(BACK_TAG).setIndicator(getString(R.string.card_back), new BitmapDrawable(Utils.loadBitmap(res, R.drawable.icon_cardback))).setContent((int) R.id.CardBackPage));
        this.mCustomCardBack = new CardBack(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int font_id = prefs.getInt(CardData.VALUE_FONT_KEY, 0);
        this.mValueFont = ValueFont.getById(font_id);
        this.mCardSize = CardSize.NORMAL;
        Spinner spinner = (Spinner) findViewById(R.id.CardValueFont);
        spinner.setOnItemSelectedListener(new FontChangeListener(this, null));
        spinner.setSelection(font_id);
        Spinner spinner2 = (Spinner) findViewById(R.id.PreviewCardSize);
        spinner2.setOnItemSelectedListener(new SizeChangeListener(this, null));
        spinner2.setSelection(this.mCardSize.mId);
        this.mCardBackType = prefs.getInt(CardData.CARD_BACK_TYPE_KEY, 0);
        Spinner spinner3 = (Spinner) findViewById(R.id.CardBackType);
        spinner3.setOnItemSelectedListener(new BackTypeListener(this, null));
        spinner3.setSelection(this.mCardBackType);
        GridView card_back_grid = (GridView) findViewById(R.id.CardBackGrid);
        View custom_page = findViewById(R.id.CustomBackPage);
        if (this.mCardBackType == 0) {
            card_back_grid.setVisibility(0);
            custom_page.setVisibility(4);
        } else {
            card_back_grid.setVisibility(4);
            custom_page.setVisibility(0);
        }
        this.mCardsPreview = (CardsPreview) findViewById(R.id.CardsPreview);
        this.mCardsPreview.setCards(this.mCardSize, ValueFont.getById(font_id));
        new CardBackAdapter(this, card_back_grid);
        Spinner spinner4 = (Spinner) findViewById(R.id.CustomBackImageFill);
        spinner4.setOnItemSelectedListener(new BackImageTypeListener(this, null));
        spinner4.setSelection(this.mCustomCardBack.getType());
        CheckBox checkbox = (CheckBox) findViewById(R.id.CustomBackFrame);
        checkbox.setChecked(this.mCustomCardBack.isFrame());
        checkbox.setOnCheckedChangeListener(new BackFrameListener(this, null));
        View button = findViewById(R.id.CustomBackFrameColor);
        button.setEnabled(this.mCustomCardBack.isFrame());
        button.setOnClickListener(new BackFrameColorSelectListener(this, null));
        findViewById(R.id.CustomBackImage).setOnClickListener(new BackImageSelectListener(this, null));
        findViewById(R.id.CustomBackBorder).setOnClickListener(new BackBorderColorSelectListener(this, null));
        Spinner spinner5 = (Spinner) findViewById(R.id.SmallCardValueSize);
        String font_size_str = prefs.getString(CardData.SMALL_VALUE_FONT_KEY, Integer.toString(CardData.getDefaultValueFontSize(this, CardSize.SMALL)));
        int count = spinner5.getCount();
        int i = 0;
        while (true) {
            if (i >= count) {
                break;
            } else if (((String) spinner5.getItemAtPosition(i)).equals(font_size_str)) {
                spinner5.setSelection(i);
                break;
            } else {
                i++;
            }
        }
        spinner5.setOnItemSelectedListener(new ValueFontSizeListener(CardSize.SMALL));
        Spinner spinner6 = (Spinner) findViewById(R.id.NormalCardValueSize);
        String font_size_str2 = prefs.getString(CardData.NORMAL_VALUE_FONT_KEY, Integer.toString(CardData.getDefaultValueFontSize(this, CardSize.NORMAL)));
        int count2 = spinner6.getCount();
        int i2 = 0;
        while (true) {
            if (i2 >= count2) {
                break;
            } else if (((String) spinner6.getItemAtPosition(i2)).equals(font_size_str2)) {
                spinner6.setSelection(i2);
                break;
            } else {
                i2++;
            }
        }
        spinner6.setOnItemSelectedListener(new ValueFontSizeListener(CardSize.NORMAL));
        CheckBox checkbox2 = (CheckBox) findViewById(R.id.UseAJQK);
        checkbox2.setOnCheckedChangeListener(this);
        if (!getString(R.string.Joker).equalsIgnoreCase("JOKER") || !getString(R.string.A).equalsIgnoreCase("A") || !getString(R.string.J).equalsIgnoreCase("J") || !getString(R.string.Q).equalsIgnoreCase("Q") || !getString(R.string.K).equalsIgnoreCase("K")) {
            checkbox2.setChecked(prefs.getBoolean(CardData.USE_AJQK_KEY, false));
        } else {
            checkbox2.setVisibility(8);
        }
        updateCustomBackPreview();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putInt(CardData.CARD_BACK_TYPE_KEY, this.mCardBackType);
        edit.commit();
        this.mCustomCardBack.save(this);
        super.onDestroy();
    }

    private class FontChangeListener implements AdapterView.OnItemSelectedListener {
        private FontChangeListener() {
        }

        /* synthetic */ FontChangeListener(CardSettingsActivity cardSettingsActivity, FontChangeListener fontChangeListener) {
            this();
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            CardSettingsActivity.this.mValueFont = ValueFont.getById(position);
            CardSettingsActivity.this.mCardsPreview.setCards(CardSettingsActivity.this.mCardSize, CardSettingsActivity.this.mValueFont);
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(CardSettingsActivity.this).edit();
            edit.putInt(CardData.VALUE_FONT_KEY, position);
            edit.commit();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class SizeChangeListener implements AdapterView.OnItemSelectedListener {
        private SizeChangeListener() {
        }

        /* synthetic */ SizeChangeListener(CardSettingsActivity cardSettingsActivity, SizeChangeListener sizeChangeListener) {
            this();
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            CardSettingsActivity.this.mCardSize = CardSize.get(position);
            CardSettingsActivity.this.mCardsPreview.setCards(CardSettingsActivity.this.mCardSize, CardSettingsActivity.this.mValueFont);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class BackTypeListener implements AdapterView.OnItemSelectedListener {
        private BackTypeListener() {
        }

        /* synthetic */ BackTypeListener(CardSettingsActivity cardSettingsActivity, BackTypeListener backTypeListener) {
            this();
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (CardSettingsActivity.this.mCardBackType != position) {
                CardSettingsActivity.this.mCardBackType = position;
                View card_back_grid = CardSettingsActivity.this.findViewById(R.id.CardBackGrid);
                View custom_page = CardSettingsActivity.this.findViewById(R.id.CustomBackPage);
                if (CardSettingsActivity.this.mCardBackType == 0) {
                    card_back_grid.setVisibility(0);
                    custom_page.setVisibility(4);
                    return;
                }
                card_back_grid.setVisibility(4);
                custom_page.setVisibility(0);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class BackImageTypeListener implements AdapterView.OnItemSelectedListener {
        private BackImageTypeListener() {
        }

        /* synthetic */ BackImageTypeListener(CardSettingsActivity cardSettingsActivity, BackImageTypeListener backImageTypeListener) {
            this();
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            CardSettingsActivity.this.mCustomCardBack.setType(position);
            CardSettingsActivity.this.updateCustomBackPreview();
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    private class BackFrameListener implements CompoundButton.OnCheckedChangeListener {
        private BackFrameListener() {
        }

        /* synthetic */ BackFrameListener(CardSettingsActivity cardSettingsActivity, BackFrameListener backFrameListener) {
            this();
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            CardSettingsActivity.this.mCustomCardBack.setFrameBorder(isChecked, CardSettingsActivity.this.mCustomCardBack.getFrameColor());
            CardSettingsActivity.this.findViewById(R.id.CustomBackFrameColor).setEnabled(isChecked);
            CardSettingsActivity.this.updateCustomBackPreview();
        }
    }

    private class BackImageSelectListener implements View.OnClickListener, GalleryResultListener {
        private BackImageSelectListener() {
        }

        /* synthetic */ BackImageSelectListener(CardSettingsActivity cardSettingsActivity, BackImageSelectListener backImageSelectListener) {
            this();
        }

        public void onClick(View v) {
            GalleryDialog.show(CardSettingsActivity.this, this);
        }

        public void onGalleryResult(String path) {
            CardSettingsActivity.this.mCustomCardBack.setImageFile(path);
            CardSettingsActivity.this.updateCustomBackPreview();
        }
    }

    private class BackBorderColorSelectListener implements View.OnClickListener, ColorSelectListener {
        private BackBorderColorSelectListener() {
        }

        /* synthetic */ BackBorderColorSelectListener(CardSettingsActivity cardSettingsActivity, BackBorderColorSelectListener backBorderColorSelectListener) {
            this();
        }

        public void onClick(View v) {
            ColorChooser.show(CardSettingsActivity.this, 0, this);
        }

        public void onColorSelected(int id, int color) {
            CardSettingsActivity.this.mCustomCardBack.setBorderColor(color);
            CardSettingsActivity.this.updateCustomBackPreview();
        }
    }

    private class BackFrameColorSelectListener implements View.OnClickListener, ColorSelectListener {
        private BackFrameColorSelectListener() {
        }

        /* synthetic */ BackFrameColorSelectListener(CardSettingsActivity cardSettingsActivity, BackFrameColorSelectListener backFrameColorSelectListener) {
            this();
        }

        public void onClick(View v) {
            ColorChooser.show(CardSettingsActivity.this, 0, this);
        }

        public void onColorSelected(int id, int color) {
            CardSettingsActivity.this.mCustomCardBack.setFrameBorder(CardSettingsActivity.this.mCustomCardBack.isFrame(), color);
            CardSettingsActivity.this.updateCustomBackPreview();
        }
    }

    /* access modifiers changed from: private */
    public void updateCustomBackPreview() {
        try {
            ((ImageView) findViewById(R.id.CustomBackPreview)).setImageBitmap(this.mCustomCardBack.createBitmap(this, new CardData(CardSize.NORMAL, this, ValueFont.DEFAULT, false)));
        } catch (CardData.InvalidSizeException e) {
            e.printStackTrace();
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putBoolean(CardData.USE_AJQK_KEY, isChecked);
        edit.commit();
        this.mCardsPreview.setCards(this.mCardSize, this.mValueFont);
    }

    private class ValueFontSizeListener implements AdapterView.OnItemSelectedListener {
        private final CardSize mCardSize;

        ValueFontSizeListener(CardSize size) {
            this.mCardSize = size;
        }

        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(CardSettingsActivity.this).edit();
            edit.putString(this.mCardSize == CardSize.SMALL ? CardData.SMALL_VALUE_FONT_KEY : CardData.NORMAL_VALUE_FONT_KEY, (String) parent.getSelectedItem());
            edit.commit();
            if (this.mCardSize == CardSettingsActivity.this.mCardSize) {
                CardSettingsActivity.this.mCardsPreview.setCards(this.mCardSize, CardSettingsActivity.this.mValueFont);
            }
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }
}
