package com.anoshenko.android.theme;

import java.util.Scanner;

public final class ParseUtil {
    private static final int parseFloatColorComponent(String value_text) throws ManifestParseException {
        if (value_text.indexOf(46) < 0) {
            throw new ManifestParseException();
        }
        try {
            float value = Float.parseFloat(value_text);
            if (((double) value) >= 0.0d && ((double) value) <= 1.0d) {
                return (int) (255.0f * value);
            }
            throw new ManifestParseException();
        } catch (NumberFormatException e) {
            throw new ManifestParseException();
        }
    }

    private static final int parseIntColorComponent(String value_text) throws ManifestParseException {
        try {
            int value = Integer.parseInt(value_text);
            if (value >= 0 && value <= 255) {
                return value;
            }
            throw new ManifestParseException();
        } catch (NumberFormatException e) {
            throw new ManifestParseException();
        }
    }

    public static final int parseColor(String color_str) throws ManifestParseException {
        String color_str2 = color_str.trim().toUpperCase();
        int len = color_str2.length();
        if (len == 6 || len == 8) {
            int i = 0;
            while (true) {
                if (i >= len) {
                    break;
                }
                char ch = color_str2.charAt(i);
                if (ch >= '0' && ((ch <= '9' || ch >= 'A') && ch <= 'F')) {
                    if (1 != 0) {
                        int color = Integer.parseInt(color_str2, 16);
                        return len == 6 ? color | -16777216 : color;
                    }
                    i++;
                }
            }
        }
        Scanner scanner = new Scanner(color_str2);
        scanner.useDelimiter(",");
        if (scanner.hasNext()) {
            String value_text = scanner.next().trim();
            int[] value = new int[4];
            int n = 1;
            if (value_text.indexOf(46) >= 0) {
                value[0] = parseFloatColorComponent(value_text);
                while (n < 4 && scanner.hasNext()) {
                    value[n] = parseFloatColorComponent(scanner.next().trim());
                    n++;
                }
            } else {
                value[0] = parseIntColorComponent(value_text);
                while (n < 4 && scanner.hasNext()) {
                    value[n] = parseIntColorComponent(scanner.next().trim());
                    n++;
                }
            }
            if (scanner.hasNext() || n < 3) {
                throw new ManifestParseException();
            } else if (n == 3) {
                return (value[0] << 16) | -16777216 | (value[1] << 8) | value[2];
            } else {
                return (value[0] << 24) | (value[1] << 16) | (value[2] << 8) | value[3];
            }
        } else {
            throw new ManifestParseException();
        }
    }
}
