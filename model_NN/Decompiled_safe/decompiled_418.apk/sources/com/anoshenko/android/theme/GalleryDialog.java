package com.anoshenko.android.theme;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.anoshenko.android.solitaires.R;
import java.util.ArrayList;
import java.util.Vector;

public class GalleryDialog implements ListAdapter, AdapterView.OnItemClickListener {
    private ArrayList<DataSetObserver> DataSetObserverList = new ArrayList<>();
    private final Context mContext;
    private AlertDialog mDialog;
    private Vector<GalleryElement> mElements = new Vector<>();
    private GalleryResultListener mResultListener;

    class GalleryElement {
        final int ThumbId;
        final int id;

        GalleryElement(int id2, int thumb_id) {
            this.id = id2;
            this.ThumbId = thumb_id;
        }
    }

    private GalleryDialog(Context context, GalleryResultListener listener, GridView grid_view) {
        this.mContext = context;
        this.mResultListener = listener;
        Cursor cursor = MediaStore.Images.Thumbnails.query(context.getContentResolver(), MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_id", "image_id"});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int id_index = cursor.getColumnIndexOrThrow("image_id");
                int thumb_index = cursor.getColumnIndexOrThrow("_id");
                do {
                    this.mElements.add(new GalleryElement(cursor.getInt(id_index), cursor.getInt(thumb_index)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        grid_view.setOnItemClickListener(this);
        grid_view.setAdapter((ListAdapter) this);
    }

    public static void show(Context context, GalleryResultListener listener) {
        View view = LayoutInflater.from(context).inflate((int) R.layout.gallery_grid_view, (ViewGroup) null);
        GalleryDialog dialog = new GalleryDialog(context, listener, (GridView) view.findViewById(R.id.GalleryGrid));
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        builder.setCancelable(true);
        builder.setTitle((int) R.string.select_image);
        dialog.mDialog = builder.show();
    }

    public int getCount() {
        return this.mElements.size();
    }

    public Object getItem(int position) {
        return this.mElements.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.gallery_element, (ViewGroup) null);
        } else {
            view = convertView;
        }
        try {
            Uri thumbUri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new StringBuilder().append(this.mElements.get(position).ThumbId).toString());
            ImageView image_view = (ImageView) view.findViewById(R.id.GalleryImage);
            image_view.setImageURI(thumbUri);
            image_view.setBackgroundColor(-3355444);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.DataSetObserverList.indexOf(observer);
        if (index >= 0 && index < this.DataSetObserverList.size()) {
            this.DataSetObserverList.remove(index);
        }
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mDialog.dismiss();
        if (this.mResultListener != null) {
            String filename = null;
            Cursor cursor = MediaStore.Images.Media.query(this.mContext.getContentResolver(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_data"}, "_id=" + this.mElements.get(position).id, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        filename = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }
                cursor.close();
            }
            if (filename != null) {
                this.mResultListener.onGalleryResult(filename);
            }
        }
    }
}
