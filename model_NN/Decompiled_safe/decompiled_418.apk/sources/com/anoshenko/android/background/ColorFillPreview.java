package com.anoshenko.android.background;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import com.anoshenko.android.solitaires.R;

public class ColorFillPreview extends View {
    public static final int GRADIENT_FILL = 1;
    public static final int HORIZONTAL_GRADIENT_FILL = 3;
    public static final int HORIZONTAL_MIRROR_GRADIENT_FILL = 4;
    public static final int MIRROR_GRADIENT_FILL = 2;
    public static final int SOLID_FILL = 0;
    private Bitmap mAlphaBackground;
    private int mColor1;
    private int mColor2;
    private int mFillStyle;

    public ColorFillPreview(Context context) {
        super(context);
    }

    public ColorFillPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorFillPreview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setColor1(int color) {
        this.mColor1 = color;
    }

    public int getColor1() {
        return this.mColor1;
    }

    public void setColor2(int color) {
        this.mColor2 = color;
    }

    public int getColor2() {
        return this.mColor2;
    }

    public void setFillStyle(int style) {
        this.mFillStyle = style;
    }

    public void setFillStyle(int style, int color1, int color2) {
        this.mFillStyle = style;
        this.mColor1 = color1;
        this.mColor2 = color2;
    }

    public void setSolidFill(int color) {
        this.mFillStyle = 0;
        this.mColor1 = color;
    }

    public int getFillStyle() {
        return this.mFillStyle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, int, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        Rect rect = new Rect(0, 0, width, height);
        if (this.mAlphaBackground == null) {
            this.mAlphaBackground = BitmapFactory.decodeResource(getResources(), R.drawable.alpha_bk);
        }
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setShader(new BitmapShader(this.mAlphaBackground, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
        canvas.drawRect(rect, paint);
        paint.reset();
        paint.setStyle(Paint.Style.FILL);
        switch (this.mFillStyle) {
            case 1:
                paint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) height, this.mColor1, this.mColor2, Shader.TileMode.MIRROR));
                break;
            case 2:
                paint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) (height / 2), this.mColor1, this.mColor2, Shader.TileMode.MIRROR));
                break;
            case 3:
                paint.setShader(new LinearGradient(0.0f, 0.0f, (float) width, 0.0f, this.mColor1, this.mColor2, Shader.TileMode.MIRROR));
                break;
            case 4:
                paint.setShader(new LinearGradient(0.0f, 0.0f, (float) (width / 2), 0.0f, this.mColor1, this.mColor2, Shader.TileMode.MIRROR));
                break;
            default:
                paint.setColor(this.mColor1);
                break;
        }
        canvas.drawRect(rect, paint);
        paint.reset();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1.0f);
        paint.setColor(-1);
        rect.right--;
        rect.bottom--;
        canvas.drawRect(rect, paint);
    }
}
