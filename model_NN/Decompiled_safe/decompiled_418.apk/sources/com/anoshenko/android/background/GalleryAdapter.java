package com.anoshenko.android.background;

import android.content.Context;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.anoshenko.android.solitaires.ArrowDrawer;
import com.anoshenko.android.solitaires.R;
import java.util.ArrayList;
import java.util.Vector;

public class GalleryAdapter implements ListAdapter, AdapterView.OnItemClickListener {
    private ArrayList<DataSetObserver> DataSetObserverList = new ArrayList<>();
    private final Background mBackground;
    private final Context mContext;
    private Vector<GalleryElement> mElements = new Vector<>();
    private final GridView mGridView;

    class GalleryElement {
        final int ThumbId;
        final int id;

        GalleryElement(int id2, int thumb_id) {
            this.id = id2;
            this.ThumbId = thumb_id;
        }
    }

    public GalleryAdapter(Context context, GridView grid_view, Background background) {
        this.mContext = context;
        this.mBackground = background;
        this.mGridView = grid_view;
        Cursor cursor = MediaStore.Images.Thumbnails.query(context.getContentResolver(), MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new String[]{"_id", "image_id"});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int id_index = cursor.getColumnIndexOrThrow("image_id");
                int thumb_index = cursor.getColumnIndexOrThrow("_id");
                do {
                    this.mElements.add(new GalleryElement(cursor.getInt(id_index), cursor.getInt(thumb_index)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        grid_view.setOnItemClickListener(this);
    }

    public int getCount() {
        return this.mElements.size();
    }

    public Object getItem(int position) {
        return this.mElements.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.gallery_element, (ViewGroup) null);
        } else {
            view = convertView;
        }
        try {
            GalleryElement e = this.mElements.get(position);
            Uri thumbUri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new StringBuilder().append(e.ThumbId).toString());
            ImageView image_view = (ImageView) view.findViewById(R.id.GalleryImage);
            image_view.setImageURI(thumbUri);
            image_view.setBackgroundColor(this.mBackground.getImageId() == e.id ? ArrowDrawer.COLOR : -3355444);
        } catch (Exception e2) {
            e2.printStackTrace();
        } catch (OutOfMemoryError e3) {
            e3.printStackTrace();
        }
        return view;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.DataSetObserverList.indexOf(observer);
        if (index >= 0 && index < this.DataSetObserverList.size()) {
            this.DataSetObserverList.remove(index);
        }
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        this.mBackground.setGalleryImage(this.mElements.get(position).id);
        this.mGridView.invalidateViews();
    }
}
