package com.anoshenko.android.background;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import com.anoshenko.android.solitaires.Utils;
import java.util.Scanner;

public class Background {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType;
    public static int[] BuildinIds;
    private final String PREFS_KEY = "Background";
    private int mColor1 = -16744704;
    private int mColor2 = -16777216;
    private final Context mContext;
    private GradientType mGradient = GradientType.VERTICAL;
    private Bitmap mImage;
    private int mImageId;
    private int mImageNumber;
    private BackgroundType mType;

    static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType() {
        int[] iArr = $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType;
        if (iArr == null) {
            iArr = new int[BackgroundType.values().length];
            try {
                iArr[BackgroundType.BUILDIN.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[BackgroundType.EXTERN.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[BackgroundType.GRADIENT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType = iArr;
        }
        return iArr;
    }

    public Background(Context context, int[] buildin_ids) {
        this.mContext = context;
        BuildinIds = buildin_ids;
        try {
            String value = PreferenceManager.getDefaultSharedPreferences(context).getString("Background", null);
            if (value != null) {
                Scanner scanner = new Scanner(value);
                scanner.useDelimiter(";");
                if (scanner.hasNext()) {
                    int type = Integer.parseInt(scanner.next());
                    if (type == BackgroundType.GRADIENT.Id) {
                        if (scanner.hasNext()) {
                            int type2 = Integer.parseInt(scanner.next());
                            GradientType[] values = GradientType.values();
                            int length = values.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    break;
                                }
                                GradientType gradient = values[i];
                                if (gradient.Id != type2) {
                                    i++;
                                } else if (scanner.hasNext()) {
                                    int color1 = Integer.parseInt(scanner.next());
                                    if (scanner.hasNext()) {
                                        setGradient(gradient, color1, Integer.parseInt(scanner.next()));
                                        return;
                                    }
                                }
                            }
                        }
                    } else if (type == BackgroundType.BUILDIN.Id) {
                        if (scanner.hasNext() && setBuildin(Integer.parseInt(scanner.next()))) {
                            return;
                        }
                    } else if (type == BackgroundType.EXTERN.Id && scanner.hasNext() && setGalleryImage(Integer.parseInt(scanner.next()))) {
                        return;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setGradient(GradientType.VERTICAL, -16744704, -16777216);
    }

    public void recycle() {
        if (this.mImage != null) {
            this.mImage.recycle();
            this.mImage = null;
            setGradient(GradientType.VERTICAL, -16744704, -16777216);
        }
    }

    public void Store() {
        String value;
        switch ($SWITCH_TABLE$com$anoshenko$android$background$BackgroundType()[this.mType.ordinal()]) {
            case 1:
                value = String.format("%d;%d;%d;%d", Integer.valueOf(this.mType.Id), Integer.valueOf(this.mGradient.Id), Integer.valueOf(this.mColor1), Integer.valueOf(this.mColor2));
                break;
            case 2:
                value = String.format("%d;%d", Integer.valueOf(this.mType.Id), Integer.valueOf(this.mImageNumber));
                break;
            case 3:
                value = String.format("%d;%d", Integer.valueOf(this.mType.Id), Integer.valueOf(this.mImageId));
                break;
            default:
                return;
        }
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
        editor.putString("Background", value);
        editor.commit();
    }

    public BackgroundType getType() {
        return this.mType;
    }

    public GradientType getGradient() {
        return this.mGradient;
    }

    public int getColor1() {
        return this.mColor1;
    }

    public int getColor2() {
        return this.mColor2;
    }

    public int getImageNumber() {
        return this.mImageNumber;
    }

    public int getImageId() {
        return this.mImageId;
    }

    public void setGradient(GradientType type, int color1, int color2) {
        this.mType = BackgroundType.GRADIENT;
        this.mGradient = type;
        this.mColor1 = color1;
        this.mColor2 = color2;
        this.mImage = null;
    }

    public boolean setBuildin(int number) {
        if (number < 0 || number >= BuildinIds.length) {
            return false;
        }
        this.mType = BackgroundType.BUILDIN;
        this.mImageNumber = number;
        return true;
    }

    public boolean setGalleryImage(int id) {
        this.mType = BackgroundType.EXTERN;
        this.mImageId = id;
        return true;
    }

    public void setColor1(int color) {
        this.mColor1 = color;
    }

    public void setColor2(int color) {
        this.mColor2 = color;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [float, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public void draw(Canvas g, Rect rect, int width, int height) {
        Paint paint = new Paint();
        switch ($SWITCH_TABLE$com$anoshenko$android$background$BackgroundType()[this.mType.ordinal()]) {
            case 1:
                if (this.mGradient == GradientType.CENTER) {
                    int cx = width / 2;
                    int cy = height / 2;
                    Rect fill_rect = new Rect(0, 0, cx, cy);
                    paint.setShader(new LinearGradient(0.0f, 0.0f, (float) cx, (float) cy, this.mColor2, this.mColor1, Shader.TileMode.CLAMP));
                    g.drawRect(fill_rect, paint);
                    fill_rect.set(cx, 0, width, cy);
                    paint.setShader(new LinearGradient((float) width, 0.0f, (float) cx, (float) cy, this.mColor2, this.mColor1, Shader.TileMode.CLAMP));
                    g.drawRect(fill_rect, paint);
                    fill_rect.set(0, cy, cx, height);
                    paint.setShader(new LinearGradient(0.0f, (float) height, (float) cx, (float) cy, this.mColor2, this.mColor1, Shader.TileMode.CLAMP));
                    g.drawRect(fill_rect, paint);
                    fill_rect.set(cx, cy, width, height);
                    paint.setShader(new LinearGradient((float) width, (float) height, (float) cx, (float) cy, this.mColor2, this.mColor1, Shader.TileMode.CLAMP));
                    g.drawRect(fill_rect, paint);
                    return;
                }
                paint.setShader(this.mGradient.getShader(width, height, this.mColor1, this.mColor2));
                g.drawRect(rect, paint);
                return;
            case 2:
                if (this.mImage == null) {
                    updateImage(width, height);
                }
                if (this.mImage == null) {
                    paint.setShader(this.mGradient.getShader(width, height, this.mColor1, this.mColor2));
                } else {
                    paint.setShader(new BitmapShader(this.mImage, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
                }
                g.drawRect(rect, paint);
                return;
            case 3:
                if (this.mImage == null) {
                    updateImage(width, height);
                }
                if (this.mImage == null) {
                    paint.setShader(this.mGradient.getShader(width, height, this.mColor1, this.mColor2));
                    g.drawRect(rect, paint);
                    return;
                } else if (rect != null) {
                    g.drawBitmap(this.mImage, rect, rect, paint);
                    return;
                } else {
                    g.drawBitmap(this.mImage, 0.0f, 0.0f, paint);
                    return;
                }
            default:
                return;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void updateImage(int width, int height) {
        if (this.mImage != null) {
            this.mImage.recycle();
            this.mImage = null;
        }
        try {
            switch ($SWITCH_TABLE$com$anoshenko$android$background$BackgroundType()[this.mType.ordinal()]) {
                case 1:
                default:
                    return;
                case 2:
                    this.mImage = Utils.loadBitmap(this.mContext.getResources(), BuildinIds[this.mImageNumber]);
                    if (this.mImage == null) {
                        this.mType = BackgroundType.GRADIENT;
                        return;
                    }
                    return;
                case 3:
                    loadGalleryImage(width, height);
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        e.printStackTrace();
    }

    private void loadGalleryImage(int width, int height) {
        int src_height;
        int src_x;
        int src_y;
        try {
            this.mImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            OutOfMemoryError ex = e;
            System.gc();
            try {
                this.mImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError e2) {
                ex.printStackTrace();
                setGradient(this.mGradient, this.mColor1, this.mColor2);
                return;
            }
        }
        Bitmap bitmap = null;
        String filename = null;
        Cursor cursor = MediaStore.Images.Media.query(this.mContext.getContentResolver(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{"_data"}, "_id=" + this.mImageId, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                try {
                    filename = cursor.getString(cursor.getColumnIndexOrThrow("_data"));
                } catch (IllegalArgumentException e3) {
                    e3.printStackTrace();
                }
            }
            cursor.close();
        }
        if (filename != null) {
            bitmap = Utils.loadBitmap(filename, width, height);
        }
        if (bitmap == null) {
            this.mImage.recycle();
            this.mImage = null;
            setGradient(this.mGradient, this.mColor1, this.mColor2);
            return;
        }
        int bmp_width = bitmap.getWidth();
        int bmp_height = bitmap.getHeight();
        if (bmp_width == width && bmp_height == height) {
            this.mImage = bitmap;
            return;
        }
        Canvas canvas = new Canvas(this.mImage);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        int src_width = (bmp_height * width) / height;
        if (src_width <= bmp_width) {
            src_height = bmp_height;
            src_x = (bmp_width - src_width) / 2;
            src_y = 0;
        } else {
            src_width = bmp_width;
            src_height = (bmp_width * height) / width;
            src_x = 0;
            src_y = (bmp_height - src_height) / 2;
        }
        canvas.drawBitmap(bitmap, new Rect(src_x, src_y, src_x + src_width, src_y + src_height), new Rect(0, 0, width, height), paint);
        bitmap.recycle();
    }
}
