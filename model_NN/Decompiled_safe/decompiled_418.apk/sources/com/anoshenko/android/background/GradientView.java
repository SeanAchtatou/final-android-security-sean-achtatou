package com.anoshenko.android.background;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import com.anoshenko.android.solitaires.ArrowDrawer;

public class GradientView extends View {
    private int mColor0;
    private int mColor1;
    private boolean mCurrent;
    private GradientType mType;

    public GradientView(Context context) {
        super(context);
        setMinimumWidth(80);
        setMinimumHeight(80);
    }

    public GradientView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setMinimumWidth(80);
        setMinimumHeight(80);
    }

    public GradientView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setMinimumWidth(80);
        setMinimumHeight(80);
    }

    public final void setGradient(int color0, int color1, GradientType type, boolean current) {
        this.mColor0 = color0;
        this.mColor1 = color1;
        this.mType = type;
        this.mCurrent = current;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Rect rect = new Rect(0, 0, getWidth(), getHeight());
        Paint paint = new Paint();
        paint.setShader(this.mType.getShader(getWidth(), getHeight(), this.mColor0, this.mColor1));
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect, paint);
        if (this.mCurrent) {
            paint.reset();
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor((int) ArrowDrawer.COLOR);
            paint.setStrokeWidth(10.0f);
            canvas.drawRect(rect, paint);
        }
    }
}
