package com.anoshenko.android.background;

import android.app.TabActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TabHost;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;
import com.anoshenko.android.ui.ColorChooser;
import com.anoshenko.android.ui.ColorSelectListener;
import com.anoshenko.android.ui.ToolbarColorsAdapter;

public class BackgroundActivity extends TabActivity implements ColorSelectListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType = null;
    private static final String BACKGROUND_TAB = "BACKGROUND";
    public static int[] BuildinIds = {R.drawable.background00, R.drawable.background01, R.drawable.background02, R.drawable.background03, R.drawable.background04, R.drawable.background05, R.drawable.background06, R.drawable.background07, R.drawable.background08, R.drawable.background09, R.drawable.background10, R.drawable.background11};
    private static final String TOOLBAR_TAB = "TOOLBAR";
    private AdapterView.OnItemSelectedListener BACKGROUND_TYPE_CHANGE = new AdapterView.OnItemSelectedListener() {
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (position) {
                case 0:
                    BackgroundActivity.this.mBackground.setGradient(BackgroundActivity.this.mBackground.getGradient(), BackgroundActivity.this.mBackground.getColor1(), BackgroundActivity.this.mBackground.getColor2());
                    break;
                case 1:
                    BackgroundActivity.this.mBackground.setBuildin(BackgroundActivity.this.mBackground.getImageNumber());
                    break;
            }
            BackgroundActivity.this.setCurrentPage(position);
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    };
    private View.OnClickListener COLOR1_BUTTON = new View.OnClickListener() {
        public void onClick(View arg0) {
            ColorChooser.show(BackgroundActivity.this, 0, BackgroundActivity.this);
        }
    };
    private View.OnClickListener COLOR2_BUTTON = new View.OnClickListener() {
        public void onClick(View arg0) {
            ColorChooser.show(BackgroundActivity.this, 1, BackgroundActivity.this);
        }
    };
    /* access modifiers changed from: private */
    public Background mBackground;
    private GalleryAdapter mGalleryAdapter;
    private GradientListAdapter mGradientAdapter;
    private ToolbarColorsAdapter mToolbarColorsAdapter;

    static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType() {
        int[] iArr = $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType;
        if (iArr == null) {
            iArr = new int[BackgroundType.values().length];
            try {
                iArr[BackgroundType.BUILDIN.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[BackgroundType.EXTERN.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[BackgroundType.GRADIENT.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$anoshenko$android$background$BackgroundType = iArr;
        }
        return iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(this);
        super.onCreate(savedInstanceState);
        this.mBackground = new Background(this, BuildinIds);
        TabHost tabHost = getTabHost();
        LayoutInflater.from(this).inflate((int) R.layout.background_view, (ViewGroup) tabHost.getTabContentView(), true);
        tabHost.addTab(tabHost.newTabSpec(BACKGROUND_TAB).setIndicator(getString(R.string.background), null).setContent((int) R.id.BackgroundPage));
        tabHost.addTab(tabHost.newTabSpec(TOOLBAR_TAB).setIndicator(getString(R.string.toolbar_colors), null).setContent((int) R.id.ToolbarColorsPage));
        Spinner spinner = (Spinner) findViewById(R.id.BackgroundType);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.background_type_item_values, 17367048);
        adapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter) adapter);
        spinner.setOnItemSelectedListener(this.BACKGROUND_TYPE_CHANGE);
        switch ($SWITCH_TABLE$com$anoshenko$android$background$BackgroundType()[this.mBackground.getType().ordinal()]) {
            case 1:
                spinner.setSelection(0);
                setCurrentPage(0);
                break;
            case 2:
                spinner.setSelection(1);
                setCurrentPage(1);
                break;
            case 3:
                spinner.setSelection(2);
                setCurrentPage(2);
                break;
        }
        ((Button) findViewById(R.id.GradientColor0)).setOnClickListener(this.COLOR1_BUTTON);
        ((Button) findViewById(R.id.GradientColor1)).setOnClickListener(this.COLOR2_BUTTON);
        this.mGradientAdapter = new GradientListAdapter(this);
        this.mGradientAdapter.setBackground(this.mBackground);
        GridView gradient_grid = (GridView) findViewById(R.id.GradientChooser);
        gradient_grid.setAdapter((ListAdapter) this.mGradientAdapter);
        if (this.mBackground.getGradient() != null) {
            gradient_grid.setSelection(this.mBackground.getGradient().Id);
        }
        gradient_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BackgroundActivity.this.mBackground.setGradient(GradientType.values()[position], BackgroundActivity.this.mBackground.getColor1(), BackgroundActivity.this.mBackground.getColor2());
                ((GridView) BackgroundActivity.this.findViewById(R.id.GradientChooser)).invalidateViews();
            }
        });
        GridView buildin_grid = (GridView) findViewById(R.id.BuildinChooser);
        buildin_grid.setAdapter((ListAdapter) new BuildinImageAdapter(this, BuildinIds, this.mBackground));
        buildin_grid.setSelection(this.mBackground.getImageNumber());
        buildin_grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                BackgroundActivity.this.mBackground.setBuildin(position);
                ((GridView) BackgroundActivity.this.findViewById(R.id.BuildinChooser)).invalidateViews();
            }
        });
        GridView gallery = (GridView) findViewById(R.id.GalleryChooser);
        this.mGalleryAdapter = new GalleryAdapter(this, gallery, this.mBackground);
        gallery.setAdapter((ListAdapter) this.mGalleryAdapter);
        this.mToolbarColorsAdapter = new ToolbarColorsAdapter(this, (ListView) findViewById(R.id.ToolbarColorsList));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.mBackground.Store();
        this.mToolbarColorsAdapter.save();
        super.onPause();
    }

    /* access modifiers changed from: private */
    public void setCurrentPage(int number) {
        RelativeLayout gradient_page = (RelativeLayout) findViewById(R.id.GradientPage);
        GridView buildin_page = (GridView) findViewById(R.id.BuildinChooser);
        GridView gallery_page = (GridView) findViewById(R.id.GalleryChooser);
        switch (number) {
            case 0:
                gradient_page.setVisibility(0);
                buildin_page.setVisibility(4);
                gallery_page.setVisibility(4);
                return;
            case 1:
                gradient_page.setVisibility(4);
                buildin_page.setVisibility(0);
                gallery_page.setVisibility(4);
                return;
            case 2:
                gradient_page.setVisibility(4);
                buildin_page.setVisibility(4);
                gallery_page.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void onColorSelected(int id, int color) {
        if (id == 0) {
            this.mBackground.setColor1(color);
        } else {
            this.mBackground.setColor2(color);
        }
        ((GridView) findViewById(R.id.GradientChooser)).invalidateViews();
    }
}
