package com.anoshenko.android.background;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.anoshenko.android.solitaires.Utils;
import java.util.ArrayList;

public class BuildinImageAdapter implements ListAdapter {
    private static final int SAMPLE_HEIGHT = 80;
    private static final int SAMPLE_WIDTH = 90;
    private final int[] BuildinImageId;
    private ArrayList<DataSetObserver> DataSetObserverList = new ArrayList<>();
    private final Background mBackground;
    private final Context mContext;
    private Bitmap[] samples;

    public BuildinImageAdapter(Context context, int[] image_ids, Background background) {
        this.mContext = context;
        this.BuildinImageId = image_ids;
        this.mBackground = background;
        this.samples = new Bitmap[Background.BuildinIds.length];
        for (int j = 0; j < this.samples.length; j++) {
            this.samples[j] = Bitmap.createBitmap(SAMPLE_WIDTH, SAMPLE_HEIGHT, Bitmap.Config.ARGB_8888);
            Canvas g = new Canvas(this.samples[j]);
            Bitmap image = Utils.loadBitmap(context.getResources(), this.BuildinImageId[j]);
            int ih = image.getHeight();
            int iw = image.getWidth();
            Rect src = new Rect();
            Rect dst = new Rect();
            Paint paint = new Paint();
            int i = 0;
            while (i < SAMPLE_HEIGHT) {
                ih = i + ih > SAMPLE_HEIGHT ? SAMPLE_HEIGHT - i : ih;
                for (int k = 0; k < SAMPLE_WIDTH; k += iw) {
                    src.set(0, 0, k + iw <= SAMPLE_WIDTH ? iw : SAMPLE_WIDTH - k, ih);
                    dst.set(k, i, src.width() + k, i + ih);
                    g.drawBitmap(image, src, dst, paint);
                }
                i += ih;
            }
        }
    }

    public int getCount() {
        return this.samples.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        SelectedImageView view;
        if (convertView == null) {
            view = new SelectedImageView(this.mContext);
        } else {
            view = (SelectedImageView) convertView;
        }
        view.setImageBitmap(this.samples[position]);
        view.setSelected(this.mBackground.getImageNumber() == position);
        return view;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.DataSetObserverList.indexOf(observer);
        if (index >= 0 && index < this.DataSetObserverList.size()) {
            this.DataSetObserverList.remove(index);
        }
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }
}
