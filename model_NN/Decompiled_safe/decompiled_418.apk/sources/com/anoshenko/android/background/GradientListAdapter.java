package com.anoshenko.android.background;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import java.util.ArrayList;

public class GradientListAdapter implements ListAdapter {
    private ArrayList<DataSetObserver> DataSetObserverList = null;
    private Background mBackground;
    private final Context mContext;

    public GradientListAdapter(Context context) {
        this.mContext = context;
    }

    public void setBackground(Background background) {
        this.mBackground = background;
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getCount() {
        return GradientType.values().length;
    }

    public Object getItem(int position) {
        return GradientType.values()[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        GradientView view;
        if (convertView == null) {
            view = new GradientView(this.mContext);
        } else {
            view = (GradientView) convertView;
        }
        view.setGradient(this.mBackground.getColor1(), this.mBackground.getColor2(), GradientType.values()[position], position == this.mBackground.getGradient().Id);
        return view;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        if (this.DataSetObserverList == null) {
            this.DataSetObserverList = new ArrayList<>();
        }
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index;
        if (this.DataSetObserverList != null && (index = this.DataSetObserverList.indexOf(observer)) >= 0 && index < this.DataSetObserverList.size()) {
            this.DataSetObserverList.remove(index);
        }
    }
}
