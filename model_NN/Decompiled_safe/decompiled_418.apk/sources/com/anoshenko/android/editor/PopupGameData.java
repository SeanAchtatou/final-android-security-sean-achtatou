package com.anoshenko.android.editor;

import android.content.Context;
import android.widget.Spinner;
import android.widget.TextView;
import com.anoshenko.android.data.GameRules;
import com.anoshenko.android.solitaires.GameBaseActivity;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.ui.GamePopupListener;
import com.anoshenko.android.ui.GamePopupPage;

public class PopupGameData implements GamePopupListener {
    private final GamePopupPage[] mPage = new GamePopupPage[2];
    /* access modifiers changed from: private */
    public final GameRules mRules;

    private PopupGameData(Context context, GameRules rules) {
        this.mRules = rules;
        this.mPage[0] = new InfoPage(context);
        this.mPage[1] = new DataPage(context);
    }

    public static void show(GameBaseActivity activity, GameRules rules) {
        PopupGameData data = new PopupGameData(activity, rules);
        activity.showPopupPages(data.mPage, data);
    }

    private class InfoPage extends GamePopupPage {
        public InfoPage(Context context) {
            super(context, R.layout.editor_game_info, R.string.editor_game_title);
        }

        /* access modifiers changed from: protected */
        public void setViewData() {
            TextView view = (TextView) this.mView.findViewById(R.id.EditorGameName);
            if (view != null) {
                view.setText(PopupGameData.this.mRules.mName == null ? "" : PopupGameData.this.mRules.mName);
            }
            TextView view2 = (TextView) this.mView.findViewById(R.id.EditorAuthor);
            if (view2 != null) {
                view2.setText(PopupGameData.this.mRules.mAuthor == null ? "" : PopupGameData.this.mRules.mAuthor);
            }
            TextView view3 = (TextView) this.mView.findViewById(R.id.EditorEmail);
            if (view3 != null) {
                view3.setText(PopupGameData.this.mRules.mEmail == null ? "" : PopupGameData.this.mRules.mEmail);
            }
            TextView view4 = (TextView) this.mView.findViewById(R.id.EditorHomePage);
            if (view4 != null) {
                view4.setText(PopupGameData.this.mRules.mHomePage == null ? "" : PopupGameData.this.mRules.mHomePage);
            }
            TextView view5 = (TextView) this.mView.findViewById(R.id.EditorComment);
            if (view5 != null) {
                view5.setText(PopupGameData.this.mRules.mComment == null ? "" : PopupGameData.this.mRules.mComment);
            }
        }

        public void saveViewData() {
            TextView view = (TextView) this.mView.findViewById(R.id.EditorGameName);
            if (view != null) {
                PopupGameData.this.mRules.mName = view.getText().toString();
            }
            TextView view2 = (TextView) this.mView.findViewById(R.id.EditorAuthor);
            if (view2 != null) {
                PopupGameData.this.mRules.mAuthor = view2.getText().toString();
            }
            TextView view3 = (TextView) this.mView.findViewById(R.id.EditorEmail);
            if (view3 != null) {
                PopupGameData.this.mRules.mEmail = view3.getText().toString();
            }
            TextView view4 = (TextView) this.mView.findViewById(R.id.EditorHomePage);
            if (view4 != null) {
                PopupGameData.this.mRules.mHomePage = view4.getText().toString();
            }
            TextView view5 = (TextView) this.mView.findViewById(R.id.EditorComment);
            if (view5 != null) {
                PopupGameData.this.mRules.mComment = view5.getText().toString();
            }
        }
    }

    private class DataPage extends GamePopupPage {
        public DataPage(Context context) {
            super(context, R.layout.editor_game_data, R.string.editor_game_title);
        }

        /* access modifiers changed from: protected */
        public void setViewData() {
            Spinner spinner = (Spinner) this.mView.findViewById(R.id.EditorBaseCard);
            if (spinner != null) {
                spinner.setSelection(PopupGameData.this.mRules.mBaseCardType);
            }
            Spinner spinner2 = (Spinner) this.mView.findViewById(R.id.EditorAutoplay);
            if (spinner2 != null) {
                spinner2.setSelection(PopupGameData.this.mRules.mAutoplay);
            }
            Spinner spinner3 = (Spinner) this.mView.findViewById(R.id.EditorRedealCount);
            if (spinner3 != null) {
                spinner3.setSelection(PopupGameData.this.mRules.mRedealCount);
            }
        }

        public void saveViewData() {
            Spinner spinner = (Spinner) this.mView.findViewById(R.id.EditorBaseCard);
            if (spinner != null) {
                PopupGameData.this.mRules.mBaseCardType = spinner.getSelectedItemPosition();
            }
            Spinner spinner2 = (Spinner) this.mView.findViewById(R.id.EditorAutoplay);
            if (spinner2 != null) {
                PopupGameData.this.mRules.mAutoplay = spinner2.getSelectedItemPosition();
            }
            Spinner spinner3 = (Spinner) this.mView.findViewById(R.id.EditorRedealCount);
            if (spinner3 != null) {
                PopupGameData.this.mRules.mRedealCount = spinner3.getSelectedItemPosition();
            }
        }
    }

    public void onGamePopupOkClosed() {
    }
}
