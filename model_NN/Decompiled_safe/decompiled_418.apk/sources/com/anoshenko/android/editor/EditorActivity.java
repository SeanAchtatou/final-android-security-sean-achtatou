package com.anoshenko.android.editor;

import android.os.Bundle;
import com.anoshenko.android.data.GameRules;
import com.anoshenko.android.solitaires.GameBaseActivity;

public class EditorActivity extends GameBaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mView.mGame = new GameEdit(this, this.mView, new GameRules());
        this.mView.mGame.initToolbar();
        this.mToolbar.setCommandListener(this.mView.mGame);
    }

    public boolean isEnableMovementSound() {
        return true;
    }

    public boolean isAnimation() {
        return false;
    }

    public boolean isDealAnimation() {
        return false;
    }

    public boolean isMirror() {
        return false;
    }

    public boolean isHidePackSize() {
        return false;
    }

    public boolean isHidePackRedeal() {
        return false;
    }

    public int getAutoplay() {
        return 0;
    }
}
