package com.anoshenko.android.editor;

import android.view.View;
import com.anoshenko.android.data.GameRules;
import com.anoshenko.android.solitaires.Command;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.solitaires.GameBaseActivity;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.ui.ToolbarButton;

public class GameEdit extends Game {
    private static final int[] TOOLBAR_BUTTONS = {R.drawable.icon_cardback, R.drawable.icon_cardback, R.string.properties_button, Command.GAME_RULES, R.drawable.icon_add, R.drawable.icon_add, R.string.add_button, Command.ADD_PILES, R.drawable.icon_delete, R.drawable.icon_delete_disable, R.string.delete_button, Command.DELETE_PILES, R.drawable.icon_save, R.drawable.icon_save, R.string.save_button, Command.SAVE, R.drawable.icon_resume, R.drawable.icon_resume, R.string.test_button, Command.TEST_RUN};
    private GameRules mGameRules = new GameRules();

    public GameEdit(GameBaseActivity activity, View view, GameRules rules) {
        super(activity, view, rules);
    }

    public void doCommand(int command) {
        switch (command) {
            case Command.GAME_RULES /*132*/:
                PopupGameData.show(this.mActivity, this.mGameRules);
                return;
            case Command.ADD_PILES /*133*/:
            case Command.DELETE_PILES /*134*/:
            case Command.SAVE /*135*/:
            default:
                return;
        }
    }

    public void initToolbar() {
        ToolbarButton[] buttons = new ToolbarButton[(TOOLBAR_BUTTONS.length / 4)];
        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new ToolbarButton(this.mActivity, TOOLBAR_BUTTONS[i * 4], TOOLBAR_BUTTONS[(i * 4) + 1], TOOLBAR_BUTTONS[(i * 4) + 2], TOOLBAR_BUTTONS[(i * 4) + 3]);
        }
        setToolbarButtons(buttons);
        updateToolbar();
    }
}
