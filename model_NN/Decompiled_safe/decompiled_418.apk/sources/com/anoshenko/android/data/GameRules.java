package com.anoshenko.android.data;

import java.util.Vector;

public class GameRules {
    public static final int ALIGN_BOTTOM = 1;
    public static final int ALIGN_CENTER = 2;
    public static final int ALIGN_CENTER_HALF = 3;
    public static final int ALIGN_LEFT = 0;
    public static final int ALIGN_RIGHT = 1;
    public static final int ALIGN_TOP = 0;
    public static final int BASE_CARD_FIXED_NO_WRAP = 1;
    public static final int BASE_CARD_FIXED_WRAP = 0;
    public static final int BASE_CARD_RANDOM_WRAP = 2;
    public String mAuthor;
    public int mAutoplay;
    public int mBaseCardType = 0;
    public String mComment;
    public String mEmail;
    public String mHomePage;
    public int mJokerCount;
    public String mName;
    public int mOpenCount = 1;
    public int mPackAlignX = 1;
    public int mPackAlignY = 0;
    public int mPackCount = 1;
    public int mPackX = 0;
    public int mPackY = 0;
    public final Vector<PileGroupRules> mPileGroup = new Vector<>();
    public int mRedealCount = 0;
    public int mRoundCount = 0;
}
