package com.anoshenko.android.data;

public class PileGroupRules {
    public static final int SUIT_ORDER_ALTERNATION = 2;
    public static final int SUIT_ORDER_COLOR = 4;
    public static final int SUIT_ORDER_DIFFERENT = 8;
    public static final int SUIT_ORDER_EQUAL = 1;
    public static final int SUIT_ORDER_NONE = 0;
    public static final int VALUE_ORDER_ASCENT = 256;
    public static final int VALUE_ORDER_DESCENT = 512;
    public static final int VALUE_ORDER_NONE = 0;
    public boolean mAdd;
    public int mAddOrder;
    public boolean mAddSeries;
    public int mAddSeriesOrder;
    public boolean mFoundation;
    public String mName;
    public boolean mPackPile;
    public int mPileCount = 1;
    public boolean mRemove;
    public boolean mRemoveSeries;
    public int mRemoveSeriesOrder;

    public PileGroupRules(String name) {
        this.mName = name;
    }

    public boolean equal(PileGroupRules rules) {
        return rules.mName.equals(this.mName) && rules.mFoundation == this.mFoundation && rules.mPackPile == this.mPackPile && rules.mAdd == this.mAdd && rules.mAddSeries == this.mAddSeries && rules.mRemove == this.mRemove && rules.mRemoveSeries == this.mRemoveSeries && rules.mAddOrder == this.mAddOrder && rules.mRemoveSeriesOrder == this.mRemoveSeriesOrder && rules.mAddSeriesOrder == this.mAddSeriesOrder && rules.mPileCount == this.mPileCount;
    }
}
