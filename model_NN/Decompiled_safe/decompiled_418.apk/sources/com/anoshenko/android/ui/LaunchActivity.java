package com.anoshenko.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import com.anoshenko.android.select.SelectActivity;
import com.anoshenko.android.select.TabletSelectActivity;
import com.anoshenko.android.solitaires.Backup;
import com.anoshenko.android.solitaires.Command;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Settings;
import com.anoshenko.android.solitaires.Utils;
import com.heyzap.sdk.HeyzapLib;

public class LaunchActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Intent intent;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.about_view);
        if (new Settings(this).getLastBackupTime() != 0 || !Backup.isBackupExists(this)) {
            Backup.backupBySchedule(this);
        } else {
            Backup.restore(this);
        }
        HeyzapLib.buttonSeenAnalytics(this);
        HeyzapLib.broadcastEnableSDK(this);
        DisplayMetrics dm = Utils.getDisplayMetrics(this);
        int min_size = Math.min(dm.heightPixels, dm.widthPixels);
        if (min_size >= 600 || (min_size >= 480 && dm.densityDpi != 240)) {
            intent = new Intent(this, TabletSelectActivity.class);
        } else {
            intent = new Intent(this, SelectActivity.class);
        }
        startActivityForResult(intent, Command.SELECT_ACTIVITY);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        finish();
    }
}
