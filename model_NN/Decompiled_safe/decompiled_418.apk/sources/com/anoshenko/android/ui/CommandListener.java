package com.anoshenko.android.ui;

public interface CommandListener {
    void doCommand(int i);
}
