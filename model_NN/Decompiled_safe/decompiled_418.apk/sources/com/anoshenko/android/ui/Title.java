package com.anoshenko.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;

public class Title extends View {
    public static final int NO_TIMER = -1;
    private ToolbarTheme mTheme;
    private String mTimer = null;
    private String mTitle = "";

    public Title(Context context) {
        super(context);
        init(context);
    }

    public Title(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Title(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.mTheme = new ToolbarTheme(context);
    }

    public void updateTheme() {
        this.mTheme = new ToolbarTheme(getContext());
        invalidate();
    }

    public void setTitle(String title) {
        String str;
        if (title == null) {
            str = "";
        } else {
            str = title;
        }
        this.mTitle = str;
    }

    public void setTitle(int title_id) {
        setTitle(getContext().getString(title_id));
    }

    public void setTimer(int timer) {
        if (timer < 0) {
            this.mTimer = null;
            return;
        }
        StringBuilder builder = new StringBuilder();
        int hour = timer / 3600;
        int timer2 = timer % 3600;
        if (hour > 0) {
            builder.append(hour);
            builder.append(':');
        }
        int minute = timer2 / 60;
        int seconds = timer2 % 60;
        if (hour > 0 && minute < 10) {
            builder.append('0');
        }
        builder.append(minute);
        builder.append(':');
        if (seconds < 10) {
            builder.append('0');
        }
        builder.append(seconds);
        this.mTimer = builder.toString();
        postInvalidate();
    }

    private Paint getPaint(Paint paint) {
        if (paint == null) {
            paint = new Paint();
        } else {
            paint.reset();
        }
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(16.0f * getResources().getDisplayMetrics().density);
        return paint;
    }

    public int getMinHeight() {
        Paint.FontMetricsInt fm = getPaint(null).getFontMetricsInt();
        return fm.bottom - fm.top;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public void draw(Canvas canvas) {
        int y2;
        int tm_y;
        int width = getWidth();
        int height = getHeight();
        Rect rect = new Rect(0, 0, width, height);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        int color1 = this.mTheme.get(0);
        int color2 = this.mTheme.get(1);
        if (this.mTheme.get(2) == 1) {
            y2 = height / 2;
        } else {
            y2 = height;
        }
        paint.setShader(new LinearGradient(0.0f, 0.0f, 0.0f, (float) y2, color1, color2, Shader.TileMode.MIRROR));
        canvas.drawRect(rect, paint);
        getPaint(paint);
        paint.setColor(this.mTheme.get(6));
        Paint.FontMetricsInt fm = paint.getFontMetricsInt();
        int title_max_width = width;
        paint.getTextBounds(this.mTitle, 0, this.mTitle.length(), rect);
        if (rect.top > fm.ascent) {
            rect.top = fm.ascent;
        }
        int y = ((height - rect.height()) / 2) - rect.top;
        if (this.mTimer != null) {
            int tm_w = (int) paint.measureText(this.mTimer);
            if ((rect.height() - fm.ascent) + 2 <= getHeight()) {
                tm_y = (((getHeight() * 2) + rect.height()) - fm.ascent) / 3;
                y = (((getHeight() - rect.height()) + fm.ascent) / 3) - fm.ascent;
            } else {
                tm_y = y;
                title_max_width -= tm_w - fm.ascent;
            }
            canvas.drawText(this.mTimer, (float) ((getWidth() - tm_w) - 1), (float) tm_y, paint);
        }
        String title = this.mTitle;
        if (rect.width() > title_max_width) {
            int title_max_width2 = title_max_width - ((int) paint.measureText("..."));
            int len = title.length() - 1;
            while (len > 0 && ((int) paint.measureText(title, 0, len)) >= title_max_width2) {
                len--;
            }
            if (len > 0) {
                title = String.valueOf(title.substring(0, len)) + "...";
            } else {
                title = "...";
            }
        }
        canvas.drawText(title, 1.0f, (float) y, paint);
    }
}
