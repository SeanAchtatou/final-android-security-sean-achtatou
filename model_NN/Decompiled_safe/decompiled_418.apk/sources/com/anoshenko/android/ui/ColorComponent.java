package com.anoshenko.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.anoshenko.android.solitaires.ArrowDrawer;
import com.anoshenko.android.solitaires.R;

public class ColorComponent extends View {
    public static final int ALPHA = 3;
    public static final int BLUE = 0;
    public static final int GREEN = 1;
    public static final int RED = 2;
    private Bitmap mAlphaBackground;
    private int mBorder;
    private int mComponent;
    private int mCursorSize;
    private float mDensity;
    private int mEndColor;
    private Listener mListener;
    private int mMask;
    private int mStartColor;
    private int mTextWidth;
    private int mValue;

    public interface Listener {
        void onColorComponentChanged(int i, int i2);
    }

    public ColorComponent(Context context) {
        super(context);
        init(context);
    }

    public ColorComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ColorComponent(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.mDensity = getResources().getDisplayMetrics().density;
        this.mBorder = (int) (4.0f * this.mDensity);
        this.mCursorSize = this.mBorder * 2;
        this.mTextWidth = (int) (getTextPaint(null).measureText("255") + (2.0f * this.mDensity));
    }

    public void setValue(int component, int color) {
        this.mComponent = component;
        switch (this.mComponent) {
            case 0:
                this.mMask = 255;
                this.mStartColor = -16777216;
                this.mEndColor = -16776961;
                this.mValue = color & 255;
                this.mAlphaBackground = null;
                return;
            case 1:
                this.mMask = 65280;
                this.mStartColor = -16777216;
                this.mEndColor = -16711936;
                this.mValue = (color >> 8) & 255;
                this.mAlphaBackground = null;
                return;
            case 2:
                this.mMask = 16711680;
                this.mStartColor = -16777216;
                this.mEndColor = ArrowDrawer.COLOR;
                this.mValue = (color >> 16) & 255;
                this.mAlphaBackground = null;
                return;
            case 3:
                this.mMask = -16777216;
                this.mStartColor = 16777215 & color;
                this.mEndColor = -16777216 | color;
                this.mValue = (color >> 24) & 255;
                this.mAlphaBackground = BitmapFactory.decodeResource(getResources(), R.drawable.alpha_bk);
                return;
            default:
                return;
        }
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    private Paint getTextPaint(Paint paint) {
        if (paint == null) {
            paint = new Paint();
        } else {
            paint.reset();
        }
        paint.setAntiAlias(true);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(16.0f * this.mDensity);
        paint.setColor(-1);
        return paint;
    }

    public void onDraw(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();
        Paint paint = new Paint();
        Rect rect = new Rect(this.mBorder, this.mBorder, (width - this.mBorder) - this.mTextWidth, height - this.mBorder);
        paint.setStyle(Paint.Style.FILL);
        if (this.mAlphaBackground != null) {
            paint.setShader(new BitmapShader(this.mAlphaBackground, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
            canvas.drawRect(rect, paint);
        }
        paint.setShader(new LinearGradient((float) rect.left, (float) rect.top, (float) rect.right, (float) rect.top, this.mStartColor, this.mEndColor, Shader.TileMode.MIRROR));
        canvas.drawRect(rect, paint);
        int x = this.mBorder + ((this.mValue * ((getWidth() - this.mTextWidth) - (this.mBorder * 2))) / 255);
        int y = (getHeight() - (this.mCursorSize * 2)) - 1;
        Path path = new Path();
        path.moveTo((float) x, (float) y);
        path.lineTo((float) ((this.mCursorSize / 2) + x), (float) ((this.mCursorSize * 2) + y));
        path.lineTo((float) (x - (this.mCursorSize / 2)), (float) ((this.mCursorSize * 2) + y));
        path.lineTo((float) x, (float) y);
        paint.reset();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16777216);
        canvas.drawPath(path, paint);
        paint.reset();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-1);
        canvas.drawPath(path, paint);
        path.reset();
        int y2 = this.mCursorSize * 2;
        path.moveTo((float) x, (float) y2);
        path.lineTo((float) ((this.mCursorSize / 2) + x), (float) (y2 - (this.mCursorSize * 2)));
        path.lineTo((float) (x - (this.mCursorSize / 2)), (float) (y2 - (this.mCursorSize * 2)));
        path.lineTo((float) x, (float) y2);
        paint.reset();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-16777216);
        canvas.drawPath(path, paint);
        paint.reset();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(-1);
        canvas.drawPath(path, paint);
        getTextPaint(paint);
        String value_text = Integer.toString(this.mValue);
        paint.getTextBounds(value_text, 0, value_text.length(), rect);
        int x2 = getWidth() - this.mTextWidth;
        canvas.drawText(value_text, (float) x2, (float) (((getHeight() - rect.height()) / 2) - rect.top), paint);
    }

    public boolean onTouchEvent(MotionEvent event) {
        int old_value = this.mValue;
        switch (event.getAction()) {
            case 0:
            case 2:
                int x = (int) event.getX();
                if (x <= this.mBorder) {
                    this.mValue = 0;
                } else {
                    this.mValue = Math.min(255, ((x - this.mBorder) * 255) / (((getWidth() - this.mTextWidth) - this.mBorder) - this.mBorder));
                }
                if (old_value == this.mValue) {
                    return true;
                }
                invalidate();
                if (this.mListener == null) {
                    return true;
                }
                this.mListener.onColorComponentChanged(this.mMask, this.mComponent > 0 ? this.mValue << (this.mComponent * 8) : this.mValue);
                return true;
            case 1:
            default:
                return true;
        }
    }
}
