package com.anoshenko.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.View;
import com.anoshenko.android.solitaires.R;

public class ColorPreview extends View {
    private Bitmap mAlphaBackground;
    private int mColor;

    public ColorPreview(Context context) {
        super(context);
    }

    public ColorPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorPreview(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setColor(int color) {
        this.mColor = color;
    }

    public void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, getWidth(), getHeight());
        if (Color.alpha(this.mColor) < 255) {
            if (this.mAlphaBackground == null) {
                this.mAlphaBackground = BitmapFactory.decodeResource(getResources(), R.drawable.alpha_bk);
            }
            paint.setStyle(Paint.Style.FILL);
            paint.setShader(new BitmapShader(this.mAlphaBackground, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
            canvas.drawRect(rect, paint);
            paint.reset();
        }
        paint.setColor(this.mColor);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(rect, paint);
    }
}
