package com.anoshenko.android.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.anoshenko.android.solitaires.Card;
import com.anoshenko.android.solitaires.CardList;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.solitaires.GameAction;
import com.anoshenko.android.solitaires.GameBaseActivity;
import com.anoshenko.android.solitaires.Pile;
import java.util.Iterator;

public class CardMoveView extends View implements Animation.AnimationListener {
    private GameBaseActivity mActivity;
    public Animation mAnimation;
    private final CardList mCards = new CardList();
    private final Rect mEndBounds = new Rect();
    private int[] mEndCardPos;
    private boolean mFastRun;
    private GameAction mFinishAction;
    private final Rect mStartBounds = new Rect();
    private int[] mStartCardPos;
    private Pile mToPile;

    public CardMoveView(Context context) {
        super(context);
    }

    public CardMoveView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CardMoveView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public boolean isAnimation() {
        return this.mAnimation != null;
    }

    public void setGameActivity(GameBaseActivity activity) {
        this.mActivity = activity;
    }

    public void setSourceCard(Card card, GameAction action) {
        this.mFinishAction = action;
        this.mFastRun = false;
        Card add_card = new Card(card);
        add_card.mNextOffset = 0;
        this.mCards.clear();
        this.mCards.add(add_card);
        card.getRect(this.mStartBounds);
        this.mStartCardPos = new int[2];
    }

    public void setSourceCards(CardList cards, GameAction action) {
        this.mFinishAction = action;
        this.mFastRun = false;
        cards.getBounds(this.mStartBounds);
        this.mStartCardPos = new int[(cards.size() * 2)];
        this.mCards.clear();
        int n = 0;
        Iterator<Card> it = cards.iterator();
        while (it.hasNext()) {
            Card card = it.next();
            Card add_card = new Card(card);
            add_card.xPos = card.xPos - this.mStartBounds.left;
            add_card.yPos = card.yPos - this.mStartBounds.top;
            this.mStartCardPos[n] = add_card.xPos;
            int n2 = n + 1;
            this.mStartCardPos[n2] = add_card.yPos;
            n = n2 + 1;
            this.mCards.add(add_card);
        }
    }

    public void start(Card end_card, Pile to_pile) {
        end_card.getRect(this.mEndBounds);
        this.mEndCardPos = new int[2];
        start(to_pile);
    }

    public boolean start(CardList end_cards, Pile to_pile) {
        if (this.mCards.size() != end_cards.size()) {
            return false;
        }
        end_cards.getBounds(this.mEndBounds);
        this.mEndCardPos = new int[(end_cards.size() * 2)];
        int n = 0;
        Iterator<Card> it = end_cards.iterator();
        while (it.hasNext()) {
            Card card = it.next();
            this.mEndCardPos[n] = card.xPos - this.mEndBounds.left;
            int n2 = n + 1;
            this.mEndCardPos[n2] = card.yPos - this.mEndBounds.top;
            n = n2 + 1;
        }
        start(to_pile);
        return true;
    }

    private void start(Pile to_pile) {
        this.mToPile = to_pile;
        int x = this.mStartBounds.left + this.mActivity.mView.getLeft();
        int y = this.mStartBounds.top + this.mActivity.mView.getTop();
        setVisibility(0);
        layout(x, y, this.mStartBounds.width() + x, this.mStartBounds.height() + y);
        int max_size = Math.max(this.mActivity.mView.getWidth(), this.mActivity.mView.getHeight());
        int start_x = (this.mStartBounds.left + this.mStartBounds.right) / 2;
        int start_y = (this.mStartBounds.top + this.mStartBounds.bottom) / 2;
        int end_x = (this.mEndBounds.left + this.mEndBounds.right) / 2;
        int end_y = (this.mEndBounds.top + this.mEndBounds.bottom) / 2;
        int distance = Math.max(Math.abs(start_x - end_x), Math.abs(start_y - end_y));
        this.mAnimation = new TranslateAnimation(1, 0.0f, 1, ((float) (end_x - start_x)) / ((float) getWidth()), 1, 0.0f, 1, ((float) (end_y - start_y)) / ((float) getHeight()));
        if (!(this.mStartBounds.width() == this.mEndBounds.width() && this.mStartBounds.height() == this.mEndBounds.height())) {
            AnimationSet animation_set = new AnimationSet(true);
            animation_set.addAnimation(this.mAnimation);
            animation_set.addAnimation(new ScaleAnimation(1.0f, ((float) this.mEndBounds.width()) / ((float) this.mStartBounds.width()), 1.0f, ((float) this.mEndBounds.height()) / ((float) this.mStartBounds.height())));
            this.mAnimation = animation_set;
        }
        this.mAnimation.setAnimationListener(this);
        if (max_size > 0) {
            this.mAnimation.setDuration((long) ((distance * 250) / max_size));
        }
        setAnimation(this.mAnimation);
        this.mAnimation.start();
    }

    public void setFastRun() {
        this.mFastRun = true;
    }

    public int getViewWidth() {
        if (this.mCards.size() == 0) {
            return 10;
        }
        Rect rect = new Rect();
        this.mCards.getBounds(rect);
        return rect.width();
    }

    public int getViewHeight() {
        if (this.mCards.size() == 0) {
            return 10;
        }
        Rect rect = new Rect();
        this.mCards.getBounds(rect);
        return rect.height();
    }

    public void onDraw(Canvas canvas) {
        boolean update_x;
        boolean update_y;
        int start_width = this.mStartBounds.width();
        int start_height = this.mStartBounds.height();
        int end_width = this.mEndBounds.width();
        int end_height = this.mEndBounds.height();
        int width = getWidth();
        int height = getHeight();
        if (start_width != end_width) {
            update_x = true;
        } else {
            update_x = false;
        }
        if (start_height != end_height) {
            update_y = true;
        } else {
            update_y = false;
        }
        int n = 0;
        Iterator<Card> it = this.mCards.iterator();
        while (it.hasNext()) {
            Card card = it.next();
            if (update_x) {
                card.xPos = this.mStartCardPos[n] + (((this.mEndCardPos[n] - this.mStartCardPos[n]) * (end_width - width)) / (end_width - start_width));
            }
            int n2 = n + 1;
            if (update_y) {
                card.yPos = this.mStartCardPos[n2] + (((this.mEndCardPos[n2] - this.mStartCardPos[n2]) * (end_height - height)) / (end_height - start_height));
            }
            n = n2 + 1;
            card.draw(canvas, null);
        }
    }

    private void finishMoving() {
        if (this.mAnimation != null) {
            this.mAnimation = null;
            this.mCards.clear();
            setVisibility(4);
            Game game = this.mActivity.mView.mGame;
            if (this.mFastRun) {
                game.mNeedCorrect = true;
            } else if (this.mToPile == null) {
                game.mPack.Correct();
                this.mActivity.mView.invalidate(game.mPack.mBounds);
            } else {
                this.mToPile.Correct();
                this.mActivity.mView.invalidate(this.mToPile.mCardBounds);
            }
            if (this.mFinishAction != null) {
                GameAction action = this.mFinishAction;
                this.mFinishAction = null;
                if (this.mFastRun) {
                    action.fastRun();
                } else {
                    action.run();
                }
            }
            this.mActivity.mView.mGame.CorrectAndRedrawIfNeed();
        }
    }

    public void cancel() {
        finishMoving();
    }

    public void onAnimationEnd(Animation animation) {
        finishMoving();
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
    }

    public Rect getRect() {
        return this.mStartBounds;
    }
}
