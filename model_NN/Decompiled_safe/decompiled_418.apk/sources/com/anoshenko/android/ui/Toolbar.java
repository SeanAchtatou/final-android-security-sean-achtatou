package com.anoshenko.android.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import java.util.Vector;

public class Toolbar extends View {
    private static final int MIN_ICON_SPACE = 2;
    private ToolbarButton[] mButton;
    private CommandListener mCommandListener;
    private boolean mPushed;
    private int mPushedButton = -1;
    private ToolbarTheme mTheme;
    private boolean mVertical;

    public Toolbar(Context context) {
        super(context);
        init(context);
    }

    public Toolbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Toolbar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        this.mTheme = new ToolbarTheme(context);
    }

    public void updateTheme() {
        this.mTheme = new ToolbarTheme(getContext());
        invalidate();
    }

    public void setToolbarButton(ToolbarButton[] buttons) {
        this.mButton = buttons;
        this.mPushed = false;
        this.mPushedButton = -1;
        invalidate();
    }

    public void setToolbarButton(Vector<ToolbarButton> buttons) {
        if (buttons == null) {
            this.mButton = null;
        } else {
            this.mButton = new ToolbarButton[buttons.size()];
            buttons.toArray(this.mButton);
        }
        this.mPushed = false;
        this.mPushedButton = -1;
        invalidate();
    }

    private Paint getPaint(Paint paint) {
        if (paint == null) {
            paint = new Paint();
        } else {
            paint.reset();
        }
        paint.setTextSize(10.0f * getResources().getDisplayMetrics().density);
        paint.setAntiAlias(true);
        return paint;
    }

    public int getVerticalWidth() {
        int width = 0;
        if (this.mButton != null) {
            Paint paint = getPaint(null);
            Paint.FontMetricsInt fm = paint.getFontMetricsInt();
            Context context = getContext();
            for (ToolbarButton button : this.mButton) {
                width = Math.max(Math.max(Math.max(width, ((int) paint.measureText(context.getString(button.mTextId))) + fm.ascent), (button.mIcon.getWidth() * 7) / 4), (button.mDisabledIcon.getWidth() * 7) / 4);
            }
        }
        return width;
    }

    public int getHorizontalHeight() {
        Paint paint = getPaint(null);
        Paint.FontMetricsInt fm = paint.getFontMetricsInt();
        int height = fm.descent - fm.ascent;
        if (this.mButton != null) {
            Rect bounds = new Rect();
            int icon_space = Math.max(fm.leading, 2);
            Context context = getContext();
            for (ToolbarButton button : this.mButton) {
                String text = context.getString(button.mTextId);
                int h = ((icon_space * 2) - fm.ascent) + Math.max(button.mIcon.getHeight(), button.mDisabledIcon.getHeight());
                paint.getTextBounds(text, 0, text.length(), bounds);
                height = Math.max(height, h + (bounds.bottom >= fm.descent ? bounds.bottom + 1 : fm.descent));
            }
        }
        return height;
    }

    public void setVertical(boolean vertical) {
        this.mVertical = vertical;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public void onDraw(Canvas canvas) {
        int color1;
        int color2;
        int y2;
        int x2;
        int width = getWidth();
        int height = getHeight();
        Rect rect = new Rect(0, 0, width, height);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        if (this.mVertical) {
            color1 = this.mTheme.get(3);
            color2 = this.mTheme.get(4);
            if (this.mTheme.get(5) == 1) {
                x2 = width / 2;
            } else {
                x2 = width;
            }
            y2 = 0;
        } else {
            color1 = this.mTheme.get(0);
            color2 = this.mTheme.get(1);
            if (this.mTheme.get(2) == 1) {
                y2 = height / 2;
            } else {
                y2 = height;
            }
            x2 = 0;
        }
        paint.setShader(new LinearGradient(0.0f, 0.0f, (float) x2, (float) y2, color1, color2, Shader.TileMode.MIRROR));
        canvas.drawRect(rect, paint);
        if (this.mButton != null && this.mButton.length > 0) {
            int x_step = 0;
            int y_step = 0;
            Paint paint2 = getPaint(paint);
            if (this.mVertical) {
                height /= this.mButton.length;
                y_step = height;
            } else {
                width /= this.mButton.length;
                x_step = width;
            }
            int x = 0;
            int y = 0;
            int n = 0;
            for (ToolbarButton button : this.mButton) {
                rect.set(x, y, x + width, y + height);
                drawButton(canvas, paint2, button, rect, this.mPushedButton == n);
                n++;
                x += x_step;
                y += y_step;
            }
        }
    }

    private void drawButton(Canvas canvas, Paint paint, ToolbarButton button, Rect rect, boolean pressed) {
        Bitmap icon;
        int y;
        int i;
        int y2;
        int x2;
        if (pressed) {
            Paint bk_paint = new Paint();
            if (this.mVertical) {
                x2 = this.mTheme.get(11) == 1 ? (rect.left + rect.right) / 2 : rect.right;
                y2 = rect.top;
            } else {
                if (this.mTheme.get(10) == 1) {
                    y2 = (rect.top + rect.bottom) / 2;
                } else {
                    y2 = rect.bottom;
                }
                x2 = rect.left;
            }
            bk_paint.setShader(new LinearGradient((float) rect.left, (float) rect.right, (float) x2, (float) y2, this.mTheme.get(8), this.mTheme.get(9), Shader.TileMode.MIRROR));
            canvas.drawRect(rect, bk_paint);
        }
        if (button.mEnabled) {
            icon = button.mIcon;
        } else {
            icon = button.mDisabledIcon;
        }
        String text = getContext().getString(button.mTextId);
        Paint.FontMetricsInt fm = paint.getFontMetricsInt();
        int icon_space = Math.max(fm.leading, 2);
        int y3 = rect.top;
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);
        if (this.mVertical) {
            y = y3 + ((rect.height() - ((icon.getHeight() + icon_space) + fm.ascent)) / 2);
        } else {
            y = y3 + icon_space;
        }
        canvas.drawBitmap(icon, (float) (rect.left + ((rect.width() - icon.getWidth()) / 2)), (float) y, paint);
        int y4 = y + ((icon.getHeight() + icon_space) - fm.ascent);
        if (button.mEnabled) {
            i = this.mTheme.get(6);
        } else {
            i = this.mTheme.get(7);
        }
        paint.setColor(i);
        canvas.drawText(text, (float) (rect.left + ((rect.width() - bounds.width()) / 2)), (float) y4, paint);
    }

    private int getButtonNumber(int x, int y) {
        int result;
        int width = getWidth();
        int height = getHeight();
        if (x < 0 || x >= width || y < 0 || y >= height || this.mButton == null || this.mButton.length <= 0) {
            return -1;
        }
        if (this.mVertical) {
            result = y / (height / this.mButton.length);
        } else {
            result = x / (width / this.mButton.length);
        }
        if (result >= this.mButton.length) {
            return -1;
        }
        return result;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.mButton == null || this.mButton.length == 0) {
            return true;
        }
        int number = getButtonNumber((int) event.getX(), (int) event.getY());
        switch (event.getAction()) {
            case 0:
                if (number >= 0 && this.mButton[number].mEnabled) {
                    this.mPushedButton = number;
                    this.mPushed = true;
                    invalidate();
                    break;
                }
            case 1:
                int number2 = getButtonNumber((int) event.getX(), (int) event.getY());
                if (number2 == this.mPushedButton && number2 >= 0 && this.mCommandListener != null) {
                    this.mCommandListener.doCommand(this.mButton[number2].mCommand);
                }
                this.mPushedButton = -1;
                this.mPushed = false;
                invalidate();
                break;
            case 2:
                if (this.mPushedButton >= 0) {
                    int number3 = getButtonNumber((int) event.getX(), (int) event.getY());
                    if (!this.mPushed) {
                        if (number3 == this.mPushedButton) {
                            this.mPushed = true;
                            invalidate();
                            break;
                        }
                    } else if (number3 != this.mPushedButton) {
                        this.mPushed = false;
                        invalidate();
                        break;
                    }
                }
                break;
            case 3:
                this.mPushedButton = -1;
                if (this.mPushed) {
                    this.mPushed = false;
                    invalidate();
                    break;
                }
                break;
        }
        return true;
    }

    public void setCommandListener(CommandListener listener) {
        this.mCommandListener = listener;
    }

    public CommandListener getCommandListener() {
        return this.mCommandListener;
    }
}
