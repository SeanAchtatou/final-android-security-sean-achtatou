package com.anoshenko.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.anoshenko.android.background.ColorFillPreview;
import com.anoshenko.android.solitaires.R;
import java.util.Vector;

public class FillStyleDialog implements ListAdapter, DialogInterface.OnClickListener {
    private final int mColor1;
    private final int mColor2;
    private final Context mContext;
    private AlertDialog mDialog;
    private final int mId;
    private final Vector<DataSetObserver> mObserver = new Vector<>();
    private final ColorSelectListener mResult;
    private final boolean mVertical;

    private FillStyleDialog(Context context, int id, int color1, int color2, boolean vertical, ColorSelectListener result) {
        this.mContext = context;
        this.mColor1 = color1;
        this.mColor2 = color2;
        this.mVertical = vertical;
        this.mResult = result;
        this.mId = id;
    }

    public static void show(Context context, int id, int current, int color1, int color2, boolean vertical, ColorSelectListener result) {
        FillStyleDialog adapter = new FillStyleDialog(context, id, color1, color2, vertical, result);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setSingleChoiceItems(adapter, current, adapter);
        builder.setCancelable(true);
        adapter.mDialog = builder.show();
        adapter.mDialog.setCanceledOnTouchOutside(true);
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getCount() {
        return 2;
    }

    public boolean isEmpty() {
        return false;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int style;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.fill_style_item, (ViewGroup) null);
        }
        ColorFillPreview preview = (ColorFillPreview) convertView.findViewById(R.id.FillStyleItemPreview);
        if (preview != null) {
            if (this.mVertical) {
                if (position == 0) {
                    style = 1;
                } else {
                    style = 2;
                }
            } else if (position == 0) {
                style = 3;
            } else {
                style = 4;
            }
            preview.setFillStyle(style, this.mColor1, this.mColor2);
        }
        return convertView;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mObserver.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mObserver.remove(observer);
    }

    public void onClick(DialogInterface dialog, int which) {
        this.mResult.onColorSelected(this.mId, which);
        this.mDialog.dismiss();
    }
}
