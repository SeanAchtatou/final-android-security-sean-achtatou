package com.anoshenko.android.ui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import com.anoshenko.android.solitaires.Utils;

public class ToolbarButton {
    public final int mCommand;
    public final Bitmap mDisabledIcon;
    public boolean mEnabled = true;
    public final Bitmap mIcon;
    public final int mTextId;

    public ToolbarButton(Bitmap icon, Bitmap disabled_icon, int text_id, int command) {
        Bitmap bitmap;
        this.mIcon = icon;
        if (disabled_icon == null) {
            bitmap = icon;
        } else {
            bitmap = disabled_icon;
        }
        this.mDisabledIcon = bitmap;
        this.mTextId = text_id;
        this.mCommand = command;
    }

    public ToolbarButton(Context context, int icon_id, int disabled_icon_id, int text_id, int command) {
        Resources resources = context.getResources();
        this.mIcon = Utils.loadBitmap(resources, icon_id);
        this.mDisabledIcon = (icon_id == disabled_icon_id || disabled_icon_id == -1) ? this.mIcon : Utils.loadBitmap(resources, disabled_icon_id);
        this.mTextId = text_id;
        this.mCommand = command;
    }
}
