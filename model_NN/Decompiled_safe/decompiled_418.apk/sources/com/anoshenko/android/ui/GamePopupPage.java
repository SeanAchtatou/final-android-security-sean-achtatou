package com.anoshenko.android.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class GamePopupPage {
    private final Context mContext;
    private final int mTitleId;
    protected View mView = null;
    private final int mViewId;

    public abstract void saveViewData();

    /* access modifiers changed from: protected */
    public abstract void setViewData();

    public GamePopupPage(Context context, int view_id, int title_id) {
        this.mContext = context;
        this.mTitleId = title_id;
        this.mViewId = view_id;
    }

    public Context getContext() {
        return this.mContext;
    }

    public String getTitle() {
        return this.mContext.getString(this.mTitleId);
    }

    public View getView() {
        if (this.mView == null) {
            this.mView = LayoutInflater.from(this.mContext).inflate(this.mViewId, (ViewGroup) null);
            if (this.mView != null) {
                setViewData();
            }
        }
        return this.mView;
    }
}
