package com.anoshenko.android.ui;

import android.content.Context;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.anoshenko.android.solitaires.R;

public class GameViewGroup extends ViewGroup {
    public static final int AD_VIEW_ID = 12306;
    private final Context mContext;

    public GameViewGroup(Context context) {
        super(context);
        this.mContext = context;
        init(context);
    }

    public GameViewGroup(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init(context);
    }

    public GameViewGroup(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mContext = context;
        init(context);
    }

    private void init(Context context) {
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int x;
        int width = right - left;
        int height = bottom - top;
        boolean portrait = width < height;
        int top2 = 0;
        int left2 = 0;
        int right2 = width;
        int bottom2 = height;
        Toolbar toolbar = (Toolbar) findViewById(R.id.GameToolbar);
        if (toolbar != null) {
            toolbar.setVertical(!portrait);
            if (portrait) {
                int h = toolbar.getHorizontalHeight();
                toolbar.layout(0, bottom2 - h, right2, bottom2);
                bottom2 -= h;
                int height2 = bottom2 - 0;
            } else {
                String value = PreferenceManager.getDefaultSharedPreferences(this.mContext).getString(this.mContext.getString(R.string.pref_landscape_toolbar_key), "0");
                int w = toolbar.getVerticalWidth();
                if (value.equals("0")) {
                    toolbar.layout(0, 0, 0 + w, bottom2);
                    left2 = 0 + w;
                } else {
                    toolbar.layout(right2 - w, 0, right2, bottom2);
                    right2 -= w;
                }
                width = right2 - left2;
            }
        }
        boolean mirror = false;
        if (!portrait) {
            try {
                mirror = PreferenceManager.getDefaultSharedPreferences(this.mContext).getBoolean(getResources().getString(R.string.pref_mirror), false);
            } catch (Exception e) {
            }
        }
        View child = findViewById(AD_VIEW_ID);
        int ad_width = 0;
        int ad_height = 0;
        if (child != null && child.getVisibility() == 0) {
            child.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
            ad_height = child.getMeasuredHeight();
            ad_width = child.getMeasuredWidth();
            if (portrait) {
                x = left2 + ((width - ad_width) / 2);
            } else if (mirror) {
                x = right2 - ad_width;
            } else {
                x = left2;
            }
            child.layout(x, 0, x + ad_width, 0 + ad_height);
        }
        Title title = (Title) findViewById(R.id.GameTitle);
        if (title != null) {
            int h2 = title.getMinHeight();
            if (portrait) {
                top2 = 0 + ad_height;
                title.layout(left2, top2, right2, top2 + h2);
            } else {
                h2 = Math.max(h2, ad_height);
                if (mirror) {
                    title.layout(left2, 0, right2 - ad_width, 0 + h2);
                } else {
                    title.layout(left2 + ad_width, 0, right2, 0 + h2);
                }
            }
            top2 += h2;
        }
        View child2 = findViewById(R.id.GameArrea);
        if (child2 != null) {
            child2.layout(left2, top2, right2, bottom2);
        }
        CardMoveView move_view = (CardMoveView) findViewById(R.id.GameCardMove);
        Rect rect = move_view.getRect();
        move_view.layout(rect.left, rect.top, rect.right, rect.bottom);
    }
}
