package com.anoshenko.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.media.MediaPlayer;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.anoshenko.android.solitaires.R;
import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

public class VictorySoundDialog implements DialogInterface.OnClickListener, DialogInterface.OnDismissListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener, SpinnerAdapter, AdapterView.OnItemSelectedListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    private Vector<DataSetObserver> DataSetObserverList = new Vector<>();
    private final CheckBox mCheckbox;
    private final Activity mContext;
    private final String mKey;
    private MediaPlayer mPlayer;
    private final Vector<SoundElement> mSounds = new Vector<>();
    private final Spinner mSpinner;
    private final Button mTestButton;

    private class SoundElement implements Comparable<SoundElement> {
        final String Filename;
        final int Id;
        final String Title;

        SoundElement(int id, String title, String filename) {
            this.Id = id;
            this.Title = title;
            this.Filename = filename;
        }

        public int compareTo(SoundElement another) {
            int result = (this.Title == null ? "" : this.Title).compareTo(another.Title == null ? "" : another.Title);
            if (result == 0) {
                return this.Filename.compareTo(another.Filename);
            }
            return result;
        }
    }

    public static void show(Activity context) {
        new VictorySoundDialog(context);
    }

    private VictorySoundDialog(Activity context) {
        this.mContext = context;
        this.mKey = context.getString(R.string.pref_select_victory_sound);
        scanAudio(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        scanAudio(MediaStore.Audio.Media.INTERNAL_CONTENT_URI);
        Collections.sort(this.mSounds);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate((int) R.layout.victory_sound, (ViewGroup) null);
        String sound_file = PreferenceManager.getDefaultSharedPreferences(context).getString(this.mKey, "").trim();
        this.mSpinner = (Spinner) view.findViewById(R.id.VictorySoundList);
        this.mSpinner.setAdapter((SpinnerAdapter) this);
        this.mSpinner.setOnItemSelectedListener(this);
        this.mCheckbox = (CheckBox) view.findViewById(R.id.VictorySoundBuildIn);
        if (sound_file.length() == 0) {
            this.mCheckbox.setChecked(true);
            this.mSpinner.setEnabled(false);
        } else {
            this.mCheckbox.setChecked(false);
            this.mSpinner.setEnabled(true);
            int index = 0;
            Iterator<SoundElement> it = this.mSounds.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                } else if (it.next().Filename.equals(sound_file)) {
                    this.mSpinner.setSelection(index);
                    break;
                } else {
                    index++;
                }
            }
        }
        this.mCheckbox.setOnCheckedChangeListener(this);
        this.mTestButton = (Button) view.findViewById(R.id.VictorySoundTest);
        this.mTestButton.setOnClickListener(this);
        builder.setView(view);
        builder.setTitle((int) R.string.pref_text_sound_level);
        builder.setCancelable(true);
        builder.setNegativeButton((int) R.string.cancel_button, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((int) R.string.ok_button, this);
        builder.show().setOnDismissListener(this);
    }

    private void scanAudio(Uri uri) {
        Cursor cursor = this.mContext.managedQuery(uri, new String[]{"_id", "title", "_data"}, null, null, "title ASC");
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int id_index = cursor.getColumnIndexOrThrow("_id");
                int data_index = cursor.getColumnIndexOrThrow("_data");
                int title_index = cursor.getColumnIndexOrThrow("title");
                do {
                    this.mSounds.add(new SoundElement(cursor.getInt(id_index), cursor.getString(title_index), cursor.getString(data_index)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    public void onDismiss(DialogInterface dialog) {
        stopPlayer();
    }

    private void stopPlayer() {
        if (this.mPlayer != null) {
            try {
                this.mPlayer.stop();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            this.mPlayer.reset();
            this.mPlayer.release();
            this.mPlayer = null;
        }
        this.mTestButton.setText((int) R.string.test);
    }

    /* Debug info: failed to restart local var, previous not found, register: 4 */
    public void onClick(DialogInterface dialog, int which) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
        if (this.mCheckbox.isChecked()) {
            editor.putString(this.mKey, "");
        } else {
            editor.putString(this.mKey, (String) this.mSpinner.getSelectedItem());
        }
        editor.commit();
    }

    public void onClick(View v) {
        MediaPlayer player;
        if (this.mPlayer != null) {
            stopPlayer();
            return;
        }
        try {
            if (this.mCheckbox.isChecked()) {
                player = MediaPlayer.create(this.mContext, (int) R.raw.applause);
                player.setOnPreparedListener(this);
                player.setOnCompletionListener(this);
                player.setOnErrorListener(this);
            } else {
                player = new MediaPlayer();
                player.setOnPreparedListener(this);
                player.setOnCompletionListener(this);
                player.setOnErrorListener(this);
                player.setDataSource((String) this.mSpinner.getSelectedItem());
                player.prepareAsync();
            }
            this.mPlayer = player;
            this.mTestButton.setText((int) R.string.stop);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        stopPlayer();
        this.mSpinner.setEnabled(!isChecked);
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public int getCount() {
        return this.mSounds.size();
    }

    public Object getItem(int position) {
        return this.mSounds.get(position).Filename;
    }

    public long getItemId(int position) {
        return (long) this.mSounds.get(position).Id;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view;
        if (convertView == null) {
            view = (TextView) LayoutInflater.from(this.mContext).inflate((int) R.layout.sound_spinner_text, (ViewGroup) null);
        } else {
            view = (TextView) convertView;
        }
        view.setText(this.mSounds.get(position).Title);
        return view;
    }

    /* Debug info: failed to restart local var, previous not found, register: 5 */
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView view;
        if (convertView == null) {
            view = (TextView) LayoutInflater.from(this.mContext).inflate((int) R.layout.sound_dropdown_text, (ViewGroup) null);
        } else {
            view = (TextView) convertView;
        }
        view.setText(this.mSounds.get(position).Title);
        return view;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.remove(observer);
    }

    public boolean onError(MediaPlayer player, int what, int extra) {
        stopPlayer();
        return true;
    }

    public void onCompletion(MediaPlayer player) {
        stopPlayer();
    }

    public void onPrepared(MediaPlayer player) {
        player.start();
    }

    public void onItemSelected(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
        stopPlayer();
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
        stopPlayer();
    }
}
