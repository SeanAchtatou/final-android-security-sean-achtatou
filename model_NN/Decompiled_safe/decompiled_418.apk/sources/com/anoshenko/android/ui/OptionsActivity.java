package com.anoshenko.android.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.anoshenko.android.solitaires.Backup;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Settings;
import java.text.DateFormat;
import java.util.Calendar;

public class OptionsActivity extends PreferenceActivity {
    private final Preference.OnPreferenceClickListener mBackupNow = new Preference.OnPreferenceClickListener() {
        public boolean onPreferenceClick(Preference preference) {
            if (!Backup.create(OptionsActivity.this)) {
                return false;
            }
            Preference pref = OptionsActivity.this.findPreference(OptionsActivity.this.getString(R.string.pref_backup_statistics_now));
            if (pref != null) {
                OptionsActivity.this.setBackupTime(pref);
                OptionsActivity.this.getListView().invalidateViews();
            }
            Preference pref2 = OptionsActivity.this.findPreference(OptionsActivity.this.getString(R.string.pref_restore_statistics));
            if (pref2 == null) {
                return false;
            }
            pref2.setOnPreferenceClickListener(OptionsActivity.this.mRestore);
            pref2.setEnabled(true);
            return false;
        }
    };
    /* access modifiers changed from: private */
    public final Preference.OnPreferenceClickListener mRestore = new Preference.OnPreferenceClickListener() {
        public boolean onPreferenceClick(Preference preference) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(OptionsActivity.this);
            dialog.setMessage((int) R.string.restore_question);
            dialog.setPositiveButton((int) R.string.yes, new RestoreYes(OptionsActivity.this, null));
            dialog.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
            dialog.setCancelable(true);
            dialog.show();
            return false;
        }
    };
    private final Preference.OnPreferenceClickListener mSelectSound = new Preference.OnPreferenceClickListener() {
        public boolean onPreferenceClick(Preference preference) {
            VictorySoundDialog.show(OptionsActivity.this);
            return false;
        }
    };
    private final Preference.OnPreferenceClickListener mSoundLevel = new Preference.OnPreferenceClickListener() {
        public boolean onPreferenceClick(Preference preference) {
            SoundLevelDialog.show(OptionsActivity.this);
            return true;
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        Preference pref = findPreference(getString(R.string.pref_sound_level));
        if (pref != null) {
            pref.setOnPreferenceClickListener(this.mSoundLevel);
        }
        Preference pref2 = findPreference(getString(R.string.pref_select_victory_sound));
        if (pref2 != null) {
            pref2.setOnPreferenceClickListener(this.mSelectSound);
        }
        Preference pref3 = findPreference(getString(R.string.pref_backup_statistics_now));
        if (pref3 != null) {
            pref3.setOnPreferenceClickListener(this.mBackupNow);
            setBackupTime(pref3);
        }
        Preference pref4 = findPreference(getString(R.string.pref_restore_statistics));
        if (pref4 != null) {
            pref4.setOnPreferenceClickListener(this.mRestore);
            pref4.setEnabled(Backup.isBackupExists(this));
        }
    }

    private class RestoreYes implements DialogInterface.OnClickListener {
        private RestoreYes() {
        }

        /* synthetic */ RestoreYes(OptionsActivity optionsActivity, RestoreYes restoreYes) {
            this();
        }

        public void onClick(DialogInterface dialog, int which) {
            Backup.restore(OptionsActivity.this);
        }
    }

    /* access modifiers changed from: private */
    public void setBackupTime(Preference pref) {
        long time = new Settings(this).getLastBackupTime();
        if (time > 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(time);
            pref.setSummary(String.valueOf(getString(R.string.last_backup)) + ' ' + DateFormat.getDateTimeInstance().format(calendar.getTime()));
        }
    }
}
