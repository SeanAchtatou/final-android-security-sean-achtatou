package com.anoshenko.android.ui;

import android.app.Dialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListAdapter;
import com.anoshenko.android.solitaires.ArrowDrawer;
import com.anoshenko.android.solitaires.R;
import java.util.ArrayList;

public class ColorChooser implements ListAdapter, AdapterView.OnItemClickListener, ColorSelectListener {
    private static final int[] COLORS = {-16777216, -12303292, -7829368, -3355444, -1, -8454144, ArrowDrawer.COLOR, -16711936, -16744704, -16776961, -16777089, -32897, -38400, -5761, -10240, -8427008, -4784384, -16711792, -16711681, -16744577, -16739073, -6193153, -5111553, -8454034, -65316, -65426, -8454089};
    private int[] mColors;
    private final Context mContext;
    private Dialog mDialog;
    private final GridView mGridView;
    private final int mId;
    private final ColorSelectListener mListener;
    private ArrayList<DataSetObserver> mObserverList = new ArrayList<>();

    private ColorChooser(Context context, int id, ColorSelectListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.mId = id;
        this.mColors = new int[COLORS.length];
        System.arraycopy(COLORS, 0, this.mColors, 0, COLORS.length);
        this.mDialog = new Dialog(context);
        this.mDialog.setContentView((int) R.layout.color_select);
        this.mGridView = (GridView) this.mDialog.findViewById(R.id.ColorSelectGrid);
        this.mGridView.setAdapter((ListAdapter) this);
        this.mGridView.setOnItemClickListener(this);
    }

    public static void show(Context context, int id, ColorSelectListener listener) {
        new ColorChooser(context, id, listener).mDialog.show();
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public int getCount() {
        return this.mColors.length + 1;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getItemViewType(int position) {
        return position < this.mColors.length ? 0 : 1;
    }

    public int getViewTypeCount() {
        return 2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ColorPreview view;
        if (position < this.mColors.length) {
            if (convertView == null) {
                view = new ColorPreview(this.mContext);
                convertView = view;
                view.setMinimumWidth(50);
            } else {
                view = (ColorPreview) convertView;
            }
            view.setColor(this.mColors[position]);
        } else if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.color_add_item, (ViewGroup) null);
        }
        convertView.setMinimumHeight(50);
        return convertView;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.mObserverList.indexOf(observer);
        if (index >= 0 && index < this.mObserverList.size()) {
            this.mObserverList.remove(index);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        if (position >= this.mColors.length) {
            ColorEditor.show(this.mContext, this, this.mId, -16777216, true);
        } else if (this.mListener != null) {
            this.mListener.onColorSelected(this.mId, this.mColors[position]);
            this.mDialog.dismiss();
        }
    }

    public void onColorSelected(int id, int color) {
        int[] old_colors = this.mColors;
        int old_count = old_colors.length;
        this.mColors = new int[(old_count + 1)];
        System.arraycopy(old_colors, 0, this.mColors, 0, old_count);
        this.mColors[old_count] = color;
        this.mGridView.invalidateViews();
    }
}
