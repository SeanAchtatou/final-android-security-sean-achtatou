package com.anoshenko.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.ui.ColorComponent;

class ColorEditor implements ColorComponent.Listener, DialogInterface.OnClickListener {
    private final ColorComponent mAlphaComponent = ((ColorComponent) this.mView.findViewById(R.id.AlphaComponent));
    private int mColor;
    private final int mId;
    private final ColorSelectListener mListener;
    private final ColorPreview mPreview;
    private final View mView;

    private ColorEditor(Context context, ColorSelectListener listener, int id, int color, boolean gone_alpha) {
        this.mListener = listener;
        this.mId = id;
        this.mColor = color;
        this.mView = LayoutInflater.from(context).inflate((int) R.layout.color_editor, (ViewGroup) null);
        this.mAlphaComponent.setValue(3, color);
        this.mAlphaComponent.setListener(this);
        if (gone_alpha) {
            this.mAlphaComponent.setVisibility(8);
        }
        ColorComponent component = (ColorComponent) this.mView.findViewById(R.id.RedComponent);
        component.setValue(2, color);
        component.setListener(this);
        ColorComponent component2 = (ColorComponent) this.mView.findViewById(R.id.GreenComponent);
        component2.setValue(1, color);
        component2.setListener(this);
        ColorComponent component3 = (ColorComponent) this.mView.findViewById(R.id.BlueComponent);
        component3.setValue(0, color);
        component3.setListener(this);
        this.mPreview = (ColorPreview) this.mView.findViewById(R.id.ColorEditorPreview);
        this.mPreview.setColor(color);
    }

    public static void show(Context context, ColorSelectListener listener, int id, int color, boolean gone_alpha) {
        ColorEditor editor = new ColorEditor(context, listener, id, color, gone_alpha);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(editor.mView);
        builder.setCancelable(true);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton(17039370, editor);
        builder.show();
    }

    public void onColorComponentChanged(int mask, int color) {
        this.mColor = (this.mColor & (mask ^ -1)) | color;
        this.mAlphaComponent.setValue(3, this.mColor);
        this.mAlphaComponent.invalidate();
        this.mPreview.setColor(this.mColor);
        this.mPreview.invalidate();
    }

    public void onClick(DialogInterface dialog, int which) {
        if (this.mListener != null) {
            this.mListener.onColorSelected(this.mId, this.mColor);
        }
    }
}
