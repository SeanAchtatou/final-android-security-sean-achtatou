package com.anoshenko.android.ui;

public interface ColorSelectListener {
    void onColorSelected(int i, int i2);
}
