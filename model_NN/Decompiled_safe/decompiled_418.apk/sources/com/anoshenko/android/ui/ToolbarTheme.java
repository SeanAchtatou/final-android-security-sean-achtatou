package com.anoshenko.android.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.anoshenko.android.background.ColorFillPreview;
import com.anoshenko.android.solitaires.R;

public class ToolbarTheme {
    public static final int COUNT = 12;
    private static final int[] DEFAULT_VALUES;
    public static final int DISABLED_TEXT_COLOR = 7;
    public static final int GRADIENT_FILL = 0;
    public static final int GRADIENT_MIRROR = 1;
    private static final String[] KEYS = {"TOOLBAR_PORTRAIT_COLOR1", "TOOLBAR_PORTRAIT_COLOR2", "TOOLBAR_PORTRAIT_STYLE", "TOOLBAR_LANDSCAPE_COLOR1", "TOOLBAR_LANDSCAPE_COLOR2", "TOOLBAR_LANDSCAPE_STYLE", "TOOLBAR_TEXT_COLOR", "TOOLBAR_DISABLED_TEXT_COLOR", "TOOLBAR_PUSHED_FIRST_COLOR", "TOOLBAR_PUSHED_SECOND_COLOR", "TOOLBAR_PUSHED_PORTRAIT_STYLE", "TOOLBAR_PUSHED_LANDSCAPE_STYLE"};
    public static final int LANDSCAPE_FIRST_COLOR = 3;
    public static final int LANDSCAPE_SECOND_COLOR = 4;
    public static final int LANDSCAPE_STYLE = 5;
    public static final int PORTRAIT_FIRST_COLOR = 0;
    public static final int PORTRAIT_SECOND_COLOR = 1;
    public static final int PORTRAIT_STYLE = 2;
    public static final int PUSHED_FIRST_COLOR = 8;
    public static final int PUSHED_LANDSCAPE_STYLE = 11;
    public static final int PUSHED_PORTRAIT_STYLE = 10;
    public static final int PUSHED_SECOND_COLOR = 9;
    public static final int TEXT_COLOR = 6;
    private static final int[] TEXT_IDS = {R.string.toolbar_portrait_color1, R.string.toolbar_portrait_color2, R.string.toolbar_portrait_style, R.string.toolbar_landscape_color1, R.string.toolbar_landscape_color2, R.string.toolbar_landscape_style, R.string.toolbar_text_color, R.string.toolbar_disabled_text_color, R.string.toolbar_pressed_color1, R.string.toolbar_pressed_color2, R.string.toolbar_pressed_portrait_style, R.string.toolbar_pressed_landscape_style};
    private final int[] mValues = new int[12];

    static {
        int[] iArr = new int[12];
        iArr[0] = -8355712;
        iArr[1] = -16777216;
        iArr[3] = -16777216;
        iArr[4] = -8355712;
        iArr[5] = 1;
        iArr[6] = -1;
        iArr[7] = -6250336;
        iArr[8] = -35584;
        iArr[9] = -28160;
        iArr[11] = 1;
        DEFAULT_VALUES = iArr;
    }

    public ToolbarTheme(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        for (int i = 0; i < 12; i++) {
            this.mValues[i] = prefs.getInt(KEYS[i], DEFAULT_VALUES[i]);
        }
    }

    public void save(Context context) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        for (int i = 0; i < 12; i++) {
            editor.putInt(KEYS[i], this.mValues[i]);
        }
        editor.commit();
    }

    public int get(int index) {
        if (index < 0 || index >= this.mValues.length) {
            return 0;
        }
        return this.mValues[index];
    }

    public void set(int index, int value) {
        this.mValues[index] = value;
    }

    public String getText(Context context, int index) {
        return context.getString(TEXT_IDS[index]);
    }

    public void setPreviewData(ColorFillPreview preview, int index) {
        switch (index) {
            case 0:
            case 1:
            case 3:
            case 4:
            case 6:
            case DISABLED_TEXT_COLOR /*7*/:
            case 8:
            case PUSHED_SECOND_COLOR /*9*/:
                preview.setSolidFill(this.mValues[index]);
                return;
            case 2:
                preview.setFillStyle(this.mValues[2] == 0 ? 1 : 2, this.mValues[0], this.mValues[1]);
                return;
            case 5:
                preview.setFillStyle(this.mValues[5] == 0 ? 3 : 4, this.mValues[3], this.mValues[4]);
                return;
            case 10:
                preview.setFillStyle(this.mValues[10] == 0 ? 1 : 2, this.mValues[8], this.mValues[9]);
                return;
            case PUSHED_LANDSCAPE_STYLE /*11*/:
                preview.setFillStyle(this.mValues[11] == 0 ? 3 : 4, this.mValues[8], this.mValues[9]);
                return;
            default:
                return;
        }
    }
}
