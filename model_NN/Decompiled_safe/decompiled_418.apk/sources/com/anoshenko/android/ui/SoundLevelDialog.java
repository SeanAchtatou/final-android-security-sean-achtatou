package com.anoshenko.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.SoundPool;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import com.anoshenko.android.solitaires.R;

public class SoundLevelDialog implements DialogInterface.OnClickListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener, DialogInterface.OnDismissListener {
    private final Context mContext;
    private final String mKey;
    private final SeekBar mLevelBar;
    private final TextView mLevelText;
    private int mSoundId;
    private SoundPool mSoundPool = null;

    public static void show(Context context) {
        new SoundLevelDialog(context);
    }

    private SoundLevelDialog(Context context) {
        this.mContext = context;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate((int) R.layout.sound_level, (ViewGroup) null);
        this.mKey = context.getString(R.string.pref_sound_level);
        int level = PreferenceManager.getDefaultSharedPreferences(context).getInt(this.mKey, 5);
        this.mLevelText = (TextView) view.findViewById(R.id.SoundLevelText);
        this.mLevelText.setText(Integer.toString(level));
        this.mLevelBar = (SeekBar) view.findViewById(R.id.SoundLevelSpinner);
        this.mLevelBar.setMax(10);
        this.mLevelBar.setProgress(level);
        this.mLevelBar.setOnSeekBarChangeListener(this);
        view.findViewById(R.id.SoundLevelTest).setOnClickListener(this);
        builder.setView(view);
        builder.setTitle((int) R.string.pref_text_sound_level);
        builder.setCancelable(true);
        builder.setNegativeButton((int) R.string.cancel_button, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((int) R.string.ok_button, this);
        builder.show().setOnDismissListener(this);
        this.mSoundPool = new SoundPool(1, 3, 0);
        this.mSoundId = this.mSoundPool.load(context, R.raw.move, 1);
    }

    public void onClick(DialogInterface dialog, int which) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
        editor.putInt(this.mKey, this.mLevelBar.getProgress());
        editor.commit();
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        this.mLevelText.setText(Integer.toString(progress));
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    public void onClick(View v) {
        if (this.mSoundPool != null) {
            float volume = ((float) this.mLevelBar.getProgress()) / 10.0f;
            this.mSoundPool.play(this.mSoundId, volume, volume, 0, 0, 1.0f);
        }
    }

    public void onDismiss(DialogInterface arg0) {
        if (this.mSoundPool == null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
    }
}
