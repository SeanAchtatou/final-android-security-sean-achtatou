package com.anoshenko.android.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.anoshenko.android.background.ColorFillPreview;
import com.anoshenko.android.solitaires.R;
import java.util.Vector;

public class ToolbarColorsAdapter implements ListAdapter, AdapterView.OnItemClickListener, ColorSelectListener {
    private final Context mContext;
    private final ListView mListView;
    private final Vector<DataSetObserver> mObserver = new Vector<>();
    private final ToolbarTheme mTheme;

    public ToolbarColorsAdapter(Context context, ListView list_view) {
        this.mContext = context;
        this.mListView = list_view;
        this.mTheme = new ToolbarTheme(context);
        list_view.setAdapter((ListAdapter) this);
        list_view.setOnItemClickListener(this);
    }

    public void save() {
        this.mTheme.save(this.mContext);
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getCount() {
        return 12;
    }

    public boolean isEmpty() {
        return false;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.color_list_item, (ViewGroup) null);
        }
        ColorFillPreview preview = (ColorFillPreview) convertView.findViewById(R.id.ColorListItemPreview);
        if (preview != null) {
            this.mTheme.setPreviewData(preview, position);
        }
        TextView text_view = (TextView) convertView.findViewById(R.id.ColorListItemText);
        if (text_view != null) {
            text_view.setText(this.mTheme.getText(this.mContext, position));
        }
        return convertView;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mObserver.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mObserver.remove(observer);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        switch (position) {
            case 0:
            case 1:
            case 3:
            case 4:
            case 6:
            case ToolbarTheme.DISABLED_TEXT_COLOR:
            case 8:
            case ToolbarTheme.PUSHED_SECOND_COLOR:
                ColorChooser.show(this.mContext, position, this);
                return;
            case 2:
                FillStyleDialog.show(this.mContext, position, this.mTheme.get(2), this.mTheme.get(0), this.mTheme.get(1), true, this);
                return;
            case 5:
                FillStyleDialog.show(this.mContext, position, this.mTheme.get(5), this.mTheme.get(3), this.mTheme.get(4), false, this);
                return;
            case 10:
                FillStyleDialog.show(this.mContext, position, this.mTheme.get(10), this.mTheme.get(8), this.mTheme.get(9), true, this);
                return;
            case ToolbarTheme.PUSHED_LANDSCAPE_STYLE:
                FillStyleDialog.show(this.mContext, position, this.mTheme.get(11), this.mTheme.get(8), this.mTheme.get(9), false, this);
                return;
            default:
                return;
        }
    }

    public void onColorSelected(int id, int color) {
        this.mTheme.set(id, color);
        this.mListView.invalidateViews();
    }
}
