package com.anoshenko.android.select;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import com.anoshenko.android.solitaires.Command;
import com.anoshenko.android.solitaires.DemoActivity;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.solitaires.PlayActivity;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.RulesDialog;
import com.anoshenko.android.solitaires.Utils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class GameResources implements GameList {
    final Activity mActivity;
    final Vector<GamesGroupElement> mAllGames;
    final Favorites mFavorites = new Favorites();
    final GamesGroup[] mGroups;

    GameResources(Activity activity) throws IOException {
        this.mActivity = activity;
        Resources resources = activity.getResources();
        InputStream stream = resources.openRawResource(R.raw.games_list);
        int count = stream.read();
        this.mGroups = new GamesGroup[count];
        for (int i = 0; i < count; i++) {
            this.mGroups[i] = new GamesGroup(resources, stream, i);
        }
        Arrays.sort(this.mGroups);
        this.mAllGames = new Vector<>();
        for (GamesGroup group : this.mGroups) {
            for (GamesGroupElement game : group.Game) {
                this.mAllGames.add(game);
            }
        }
        Collections.sort(this.mAllGames);
        this.mFavorites.init(activity, this);
    }

    public GamesGroupElement getGameById(int id) {
        int group_id = id & 65280;
        GamesGroup[] gamesGroupArr = this.mGroups;
        int length = gamesGroupArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            GamesGroup group = gamesGroupArr[i];
            if (group.Id == group_id) {
                for (GamesGroupElement game : group.Game) {
                    if (game.Id == id) {
                        return game;
                    }
                }
            } else {
                i++;
            }
        }
        return null;
    }

    public void doCommand(int command, GamesGroupElement element, FavoritesChangeListener listener) {
        switch (command) {
            case Command.START /*102*/:
                Play(element);
                return;
            case Command.DEMO /*110*/:
                Demo(element);
                return;
            case Command.RULES /*111*/:
                Rules(element);
                return;
            case Command.REMOVE_FAVOTITE /*116*/:
                removeFromFavorites(element, listener);
                return;
            case Command.ADD_TO_FAVOTITES /*117*/:
                if (this.mFavorites.addAndStore(element) && listener != null) {
                    listener.onFavoritesChanged();
                    return;
                }
                return;
            default:
                return;
        }
    }

    private class RemoveFromFavorites implements DialogInterface.OnClickListener {
        private final FavoritesChangeListener mListener;
        private final GamesGroupElement removeElement;

        public RemoveFromFavorites(GamesGroupElement e, FavoritesChangeListener listener) {
            this.removeElement = e;
            this.mListener = listener;
        }

        public void onClick(DialogInterface dialog, int arg1) {
            GameResources.this.mFavorites.remove(this.removeElement);
            GameResources.this.mFavorites.store();
            dialog.dismiss();
            if (this.mListener != null) {
                this.mListener.onFavoritesChanged();
            }
        }
    }

    private void removeFromFavorites(GamesGroupElement e, FavoritesChangeListener listener) {
        Utils.Question(this.mActivity, R.string.remove_from_favorites, e.Name, -1, new RemoveFromFavorites(e, listener));
    }

    private void Play(GamesGroupElement element) {
        Intent intent = new Intent(this.mActivity, PlayActivity.class);
        Game.putExtra(intent, element.Id, element.Type, element.Name, element.Position, element.RuleSize, element.DemoSize, element.HelpSize);
        this.mActivity.startActivityForResult(intent, Command.PLAY_ACTIVITY);
    }

    private void Rules(GamesGroupElement element) {
        RulesDialog.show(this.mActivity, element.Name, element.Position + element.RuleSize + element.DemoSize, element.HelpSize);
    }

    private void Demo(GamesGroupElement element) {
        Intent intent = new Intent(this.mActivity, DemoActivity.class);
        Game.putExtra(intent, element.Id, element.Type, element.Name, element.Position, element.RuleSize, element.DemoSize, element.HelpSize);
        this.mActivity.startActivity(intent);
    }
}
