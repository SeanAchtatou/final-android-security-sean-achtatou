package com.anoshenko.android.select;

import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;
import com.anoshenko.android.solitaires.R;
import java.util.ArrayList;

class GamesTreeAdapter implements ExpandableListAdapter {
    private ArrayList<DataSetObserver> DataSetObserverList = new ArrayList<>();
    private boolean[] fExpand;
    private final SelectActivity mSelectActivity;

    GamesTreeAdapter(SelectActivity activity) {
        this.mSelectActivity = activity;
        this.fExpand = new boolean[this.mSelectActivity.mGames.mGroups.length];
        for (int i = 0; i < this.fExpand.length; i++) {
            this.fExpand[i] = false;
        }
    }

    public Object getChild(int groupPosition, int childPosition) {
        return this.mSelectActivity.mGames.mGroups[groupPosition].Game[childPosition];
    }

    public long getChildId(int groupPosition, int childPosition) {
        return (long) this.mSelectActivity.mGames.mGroups[groupPosition].Game[childPosition].Id;
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mSelectActivity).inflate((int) R.layout.list_game_item_view, (ViewGroup) null);
        }
        ((TextView) convertView.findViewById(R.id.SelectGameItemText)).setText(this.mSelectActivity.mGames.mGroups[groupPosition].Game[childPosition].Name);
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return this.mSelectActivity.mGames.mGroups[groupPosition].Game.length;
    }

    public long getCombinedChildId(long groupId, long childId) {
        return childId;
    }

    public long getCombinedGroupId(long groupId) {
        return groupId;
    }

    public Object getGroup(int groupPosition) {
        return this.mSelectActivity.mGames.mGroups[groupPosition];
    }

    public int getGroupCount() {
        return this.mSelectActivity.mGames.mGroups.length;
    }

    public long getGroupId(int groupPosition) {
        return (long) ((groupPosition << 8) | 255);
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mSelectActivity).inflate((int) R.layout.tree_game_item_view, (ViewGroup) null);
        }
        ((TextView) convertView).setText(this.mSelectActivity.mGames.mGroups[groupPosition].Name);
        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public void onGroupCollapsed(int groupPosition) {
        this.fExpand[groupPosition] = false;
    }

    public void onGroupExpanded(int groupPosition) {
        this.fExpand[groupPosition] = true;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.DataSetObserverList.indexOf(observer);
        if (index >= 0 && index < this.DataSetObserverList.size()) {
            this.DataSetObserverList.remove(index);
        }
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }
}
