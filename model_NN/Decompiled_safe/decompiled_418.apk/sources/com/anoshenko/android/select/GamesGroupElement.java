package com.anoshenko.android.select;

import android.content.res.Resources;
import com.anoshenko.android.solitaires.R;
import java.io.IOException;
import java.io.InputStream;

final class GamesGroupElement implements Comparable<GamesGroupElement> {
    final int Complexity;
    final int DemoSize;
    final int HelpSize;
    final int Id;
    int Losses;
    final String Name;
    final int Position;
    final int Probability;
    final int RuleSize;
    final int Type;
    int Wins;

    GamesGroupElement(Resources resources, InputStream stream, int id) throws IOException {
        this.Id = id;
        this.Name = resources.getString(R.string.rules000 + readWord(stream));
        int n = stream.read();
        this.Type = n & 3;
        this.Probability = (n >> 2) & 3;
        this.Complexity = (n >> 4) & 15;
        this.Position = readDword(stream);
        this.RuleSize = readWord(stream);
        this.DemoSize = readWord(stream);
        this.HelpSize = readWord(stream);
    }

    public int compareTo(GamesGroupElement another) {
        char ch;
        char ch2;
        int index = this.Name.indexOf("10x");
        if (index <= 0) {
            int index2 = another.Name.indexOf("10x");
            if (index2 > 0 && this.Name.startsWith(another.Name.substring(0, index2 - 1)) && (ch = this.Name.charAt(index2)) > '1' && ch <= '9') {
                return '1' - ch;
            }
        } else if (another.Name.startsWith(this.Name.substring(0, index - 1)) && (ch2 = another.Name.charAt(index)) > '1' && ch2 <= '9') {
            return ch2 - '1';
        }
        return this.Name.compareToIgnoreCase(another.Name);
    }

    private final int readDword(InputStream stream) throws IOException {
        return (stream.read() & 255) | ((stream.read() & 255) << 8) | ((stream.read() & 255) << 16) | (stream.read() << 24);
    }

    private final int readWord(InputStream stream) throws IOException {
        return (stream.read() & 255) | ((stream.read() & 255) << 8);
    }
}
