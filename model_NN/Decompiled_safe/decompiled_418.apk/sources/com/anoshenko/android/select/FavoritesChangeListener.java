package com.anoshenko.android.select;

public interface FavoritesChangeListener {
    void onFavoritesChanged();
}
