package com.anoshenko.android.select;

public interface GameList {
    GamesGroupElement getGameById(int i);
}
