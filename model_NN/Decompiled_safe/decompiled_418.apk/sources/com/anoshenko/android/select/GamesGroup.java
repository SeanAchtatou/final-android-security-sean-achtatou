package com.anoshenko.android.select;

import android.content.res.Resources;
import com.anoshenko.android.solitaires.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

final class GamesGroup implements Comparable<GamesGroup> {
    final GamesGroupElement[] Game;
    final int Id;
    final String Name;

    GamesGroup(Resources resources, InputStream stream, int number) throws IOException {
        this.Id = number << 8;
        this.Name = resources.getString(R.string.rules000 + (stream.read() & 255) + ((stream.read() & 255) << 8));
        int count = stream.read();
        this.Game = new GamesGroupElement[count];
        for (int i = 0; i < count; i++) {
            this.Game[i] = new GamesGroupElement(resources, stream, this.Id | i);
        }
        Arrays.sort(this.Game);
    }

    public int compareTo(GamesGroup another) {
        return this.Name.compareToIgnoreCase(another.Name);
    }
}
