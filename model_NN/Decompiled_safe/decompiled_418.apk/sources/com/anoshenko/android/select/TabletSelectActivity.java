package com.anoshenko.android.select;

import android.app.Activity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.anoshenko.android.solitaires.Command;
import com.anoshenko.android.solitaires.PopupMenu;
import com.anoshenko.android.solitaires.ProgramStartMessage;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;
import com.anoshenko.android.ui.CommandListener;
import java.io.IOException;
import java.util.ArrayList;

public class TabletSelectActivity extends Activity implements CommandListener, FavoritesChangeListener {
    /* access modifiers changed from: private */
    public int mCurrentGroup = 0;
    private GamesGroupElement mCurrentItem = null;
    private ListView mGameListView;
    /* access modifiers changed from: private */
    public GameResources mGames;
    private GamesListAdapter mGamesListAdapter;
    private ListView mGroupListView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(this);
        super.onCreate(savedInstanceState);
        try {
            this.mGames = new GameResources(this);
            setContentView((int) R.layout.select_tablet_view);
            GameGroupListAdapter adapter = new GameGroupListAdapter(this, null);
            this.mGroupListView = (ListView) findViewById(R.id.SelectGroupList);
            this.mGroupListView.setAdapter((ListAdapter) adapter);
            this.mGroupListView.setOnItemClickListener(adapter);
            this.mCurrentGroup = 0;
            this.mGamesListAdapter = new GamesListAdapter(this, this.mGames.mFavorites, true);
            this.mGameListView = (ListView) findViewById(R.id.SelectGameList);
            this.mGameListView.setAdapter((ListAdapter) this.mGamesListAdapter);
            GameListOnItemClick listener = new GameListOnItemClick(this, null);
            this.mGameListView.setOnItemClickListener(listener);
            this.mGameListView.setOnItemLongClickListener(listener);
            new Handler().post(new Runnable() {
                public void run() {
                    ProgramStartMessage.show(TabletSelectActivity.this);
                }
            });
        } catch (IOException e) {
            Toast.makeText(this, e.toString(), 1);
        }
    }

    /* access modifiers changed from: private */
    public void setCurrentGroup(int number) {
        this.mCurrentGroup = number;
        switch (number) {
            case 0:
                this.mGamesListAdapter.setGameList(this.mGames.mFavorites);
                break;
            case 1:
                this.mGamesListAdapter.setGameList(this.mGames.mAllGames);
                break;
            default:
                this.mGamesListAdapter.setGameList(this.mGames.mGroups[number - 2].Game);
                break;
        }
        this.mGroupListView.invalidateViews();
        this.mGameListView.invalidateViews();
    }

    private class GameListOnItemClick implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
        private GameListOnItemClick() {
        }

        /* synthetic */ GameListOnItemClick(TabletSelectActivity tabletSelectActivity, GameListOnItemClick gameListOnItemClick) {
            this();
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            TabletSelectActivity.this.gameItemClick(TabletSelectActivity.this.mCurrentGroup == 0, (int) id);
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            TabletSelectActivity.this.showMenu(TabletSelectActivity.this.mCurrentGroup == 0, (int) id);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void gameItemClick(boolean favorite, int id) {
        int value = 0;
        try {
            value = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.pref_game_item_click), "0"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (value == 0) {
            showMenu(favorite, id);
            return;
        }
        this.mCurrentItem = this.mGames.getGameById(id);
        if (this.mCurrentItem != null) {
            this.mGames.doCommand(Command.START, this.mCurrentItem, this);
            this.mCurrentItem = null;
        }
    }

    /* access modifiers changed from: private */
    public void showMenu(boolean favorite, int id) {
        this.mCurrentItem = this.mGames.getGameById(id);
        if (this.mCurrentItem != null) {
            PopupMenu menu = new PopupMenu(this, this);
            menu.addItem(Command.START, R.string.play_menu_item, R.drawable.icon_start);
            menu.addItem(Command.DEMO, R.string.demo_menu_item, R.drawable.icon_demo);
            menu.addItem(Command.RULES, R.string.rules, R.drawable.icon_rules);
            if (favorite) {
                menu.addItem(Command.REMOVE_FAVOTITE, R.string.remove_from_favorites_menu_item, R.drawable.icon_favorite_remove);
            } else {
                menu.addItem(Command.ADD_TO_FAVOTITES, R.string.add_to_favorites_menu_item, R.drawable.icon_favorite_add);
            }
            menu.setTitle(this.mCurrentItem.Name);
            menu.show();
        }
    }

    public void doCommand(int command) {
        if (this.mCurrentItem != null) {
            this.mGames.doCommand(command, this.mCurrentItem, this);
            this.mCurrentItem = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            Utils.setOrientation(this);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Utils.createMenu(this, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        return Utils.onMenuItemSelected(this, item);
    }

    private class GameGroupListAdapter implements ListAdapter, AdapterView.OnItemClickListener {
        private ArrayList<DataSetObserver> mDataSetObserverList;

        private GameGroupListAdapter() {
            this.mDataSetObserverList = new ArrayList<>();
        }

        /* synthetic */ GameGroupListAdapter(TabletSelectActivity tabletSelectActivity, GameGroupListAdapter gameGroupListAdapter) {
            this();
        }

        public boolean hasStableIds() {
            return true;
        }

        public boolean isEmpty() {
            return false;
        }

        public boolean areAllItemsEnabled() {
            return true;
        }

        public boolean isEnabled(int position) {
            return true;
        }

        public int getCount() {
            return TabletSelectActivity.this.mGames.mGroups.length + 2;
        }

        public Object getItem(int position) {
            switch (position) {
                case 0:
                    return TabletSelectActivity.this.mGames.mFavorites;
                case 1:
                    return TabletSelectActivity.this.mGames.mAllGames;
                default:
                    return TabletSelectActivity.this.mGames.mGroups[position - 2];
            }
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public int getViewTypeCount() {
            return 2;
        }

        public int getItemViewType(int position) {
            return position == TabletSelectActivity.this.mCurrentGroup ? 1 : 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            TextView text_view;
            ImageView image_view;
            int i;
            if (convertView == null) {
                LayoutInflater from = LayoutInflater.from(TabletSelectActivity.this);
                if (position == TabletSelectActivity.this.mCurrentGroup) {
                    i = R.layout.list_current_group_item;
                } else {
                    i = R.layout.list_game_item_view;
                }
                convertView = from.inflate(i, (ViewGroup) null);
            }
            if (position == TabletSelectActivity.this.mCurrentGroup) {
                text_view = (TextView) convertView.findViewById(R.id.SelectGroupItemText);
                image_view = (ImageView) convertView.findViewById(R.id.SelectGroupItemIcon);
            } else {
                text_view = (TextView) convertView.findViewById(R.id.SelectGameItemText);
                image_view = (ImageView) convertView.findViewById(R.id.SelectGameItemIcon);
            }
            switch (position) {
                case 0:
                    if (text_view != null) {
                        text_view.setText((int) R.string.favorites_tab);
                    }
                    if (image_view != null) {
                        image_view.setImageResource(R.drawable.icon_favorites);
                        break;
                    }
                    break;
                case 1:
                    if (text_view != null) {
                        text_view.setText((int) R.string.game_list_tab);
                    }
                    if (image_view != null) {
                        image_view.setImageResource(R.drawable.icon_all_games);
                        break;
                    }
                    break;
                default:
                    if (text_view != null) {
                        text_view.setText(TabletSelectActivity.this.mGames.mGroups[position - 2].Name);
                    }
                    if (image_view != null) {
                        image_view.setImageResource(R.drawable.icon_tree);
                        break;
                    }
                    break;
            }
            return convertView;
        }

        public void registerDataSetObserver(DataSetObserver observer) {
            this.mDataSetObserverList.add(observer);
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            this.mDataSetObserverList.remove(observer);
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            if (position != TabletSelectActivity.this.mCurrentGroup) {
                TabletSelectActivity.this.setCurrentGroup(position);
            }
        }
    }

    public void onFavoritesChanged() {
        if (this.mCurrentGroup == 0) {
            setCurrentGroup(0);
        }
    }
}
