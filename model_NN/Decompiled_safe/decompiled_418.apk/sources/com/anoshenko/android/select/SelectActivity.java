package com.anoshenko.android.select;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;
import com.anoshenko.android.solitaires.Command;
import com.anoshenko.android.solitaires.PopupMenu;
import com.anoshenko.android.solitaires.ProgramStartMessage;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;
import com.anoshenko.android.ui.CommandListener;
import java.io.IOException;

public class SelectActivity extends TabActivity implements CommandListener, FavoritesChangeListener, ExpandableListView.OnChildClickListener, AdapterView.OnItemLongClickListener {
    private static final String FAVORITES_TAG = "favorites";
    private static final String GAME_LIST_TAG = "game_list";
    private static final String GAME_TREE_TAG = "game_tree";
    protected GamesGroupElement mCurrentItem = null;
    private GamesListAdapter mFavoriteAdapter;
    private ListView mFavoritesList;
    GameResources mGames;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Utils.setOrientation(this);
        super.onCreate(savedInstanceState);
        try {
            this.mGames = new GameResources(this);
            TabHost tabHost = getTabHost();
            LayoutInflater.from(this).inflate((int) R.layout.select_view, (ViewGroup) tabHost.getTabContentView(), true);
            Resources res = getResources();
            tabHost.addTab(tabHost.newTabSpec(FAVORITES_TAG).setIndicator(getString(R.string.favorites_tab), new BitmapDrawable(Utils.loadBitmap(res, R.drawable.icon_favorites))).setContent((int) R.id.favorites_list));
            tabHost.addTab(tabHost.newTabSpec(GAME_TREE_TAG).setIndicator(getString(R.string.game_tree_tab), new BitmapDrawable(Utils.loadBitmap(res, R.drawable.icon_tree))).setContent((int) R.id.game_tree_list));
            tabHost.addTab(tabHost.newTabSpec(GAME_LIST_TAG).setIndicator(getString(R.string.game_list_tab), new BitmapDrawable(Utils.loadBitmap(res, R.drawable.icon_all_games))).setContent((int) R.id.all_game_list));
            this.mFavoriteAdapter = new GamesListAdapter(this, this.mGames.mFavorites, false);
            this.mFavoritesList = (ListView) findViewById(R.id.favorites_list);
            this.mFavoritesList.setAdapter((ListAdapter) this.mFavoriteAdapter);
            GameListOnItemClick itemClick = new GameListOnItemClick(true);
            this.mFavoritesList.setOnItemClickListener(itemClick);
            this.mFavoritesList.setOnItemLongClickListener(itemClick);
            ExpandableListView tree_view = (ExpandableListView) findViewById(R.id.game_tree_list);
            tree_view.setAdapter(new GamesTreeAdapter(this));
            tree_view.setOnChildClickListener(this);
            tree_view.setOnItemLongClickListener(this);
            ListView list_view = (ListView) findViewById(R.id.all_game_list);
            list_view.setAdapter((ListAdapter) new GamesListAdapter(this, this.mGames.mAllGames, true));
            GameListOnItemClick itemClick2 = new GameListOnItemClick(false);
            list_view.setOnItemClickListener(itemClick2);
            list_view.setOnItemLongClickListener(itemClick2);
            new Handler().post(new Runnable() {
                public void run() {
                    ProgramStartMessage.show(SelectActivity.this);
                }
            });
        } catch (IOException e) {
            Toast.makeText(this, e.toString(), 1);
        }
    }

    private class GameListOnItemClick implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {
        private final boolean mFavoriteList;

        GameListOnItemClick(boolean favorite_list) {
            this.mFavoriteList = favorite_list;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            SelectActivity.this.gameItemClick(this.mFavoriteList, (int) id);
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            SelectActivity.this.showMenu(this.mFavoriteList, (int) id);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void gameItemClick(boolean favorite, int id) {
        int value = 0;
        try {
            value = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(this).getString(getString(R.string.pref_game_item_click), "0"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        if (value == 0) {
            showMenu(favorite, id);
            return;
        }
        this.mCurrentItem = this.mGames.getGameById(id);
        if (this.mCurrentItem != null) {
            this.mGames.doCommand(Command.START, this.mCurrentItem, this);
            this.mCurrentItem = null;
        }
    }

    /* access modifiers changed from: private */
    public void showMenu(boolean favorite, int id) {
        this.mCurrentItem = this.mGames.getGameById(id);
        if (this.mCurrentItem != null) {
            PopupMenu menu = new PopupMenu(this, this);
            menu.addItem(Command.START, R.string.play_menu_item, R.drawable.icon_start);
            menu.addItem(Command.DEMO, R.string.demo_menu_item, R.drawable.icon_demo);
            menu.addItem(Command.RULES, R.string.rules, R.drawable.icon_rules);
            if (favorite) {
                menu.addItem(Command.REMOVE_FAVOTITE, R.string.remove_from_favorites_menu_item, R.drawable.icon_favorite_remove);
            } else {
                menu.addItem(Command.ADD_TO_FAVOTITES, R.string.add_to_favorites_menu_item, R.drawable.icon_favorite_add);
            }
            menu.setTitle(this.mCurrentItem.Name);
            menu.show();
        }
    }

    public void doCommand(int command) {
        if (this.mCurrentItem != null) {
            this.mGames.doCommand(command, this.mCurrentItem, this);
            this.mCurrentItem = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1001) {
            Utils.setOrientation(this);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Utils.createMenu(this, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        return Utils.onMenuItemSelected(this, item);
    }

    public void onFavoritesChanged() {
        this.mFavoriteAdapter.setGameList(this.mGames.mFavorites);
        this.mFavoritesList.invalidateViews();
    }

    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        gameItemClick(false, (int) id);
        return true;
    }

    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        if ((id & 255) >= 255) {
            return false;
        }
        showMenu(false, (int) id);
        return true;
    }
}
