package com.anoshenko.android.select;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.anoshenko.android.solitaires.R;
import com.anoshenko.android.solitaires.Utils;
import java.util.Collections;
import java.util.Iterator;
import java.util.Vector;

public class Favorites extends Vector<GamesGroupElement> {
    public static final String FAVORITES_KEY = "Favorites";
    private static final int[] mDefaultIds = {3330, 513, 1294, 3337, 1810, 2320, 3082, 1032};
    private static final long serialVersionUID = 3642954620429686679L;
    private Context mContext;
    private GameList mGameList;

    public void init(Context context, GameList game_list) {
        this.mContext = context;
        this.mGameList = game_list;
        String favorites = PreferenceManager.getDefaultSharedPreferences(context).getString(FAVORITES_KEY, null);
        if (favorites != null) {
            int count = favorites.length() / 4;
            clear();
            if (count > 0) {
                for (int i = 0; i < count; i++) {
                    int group_id = ((favorites.charAt(i * 4) - '0') * 10) + (favorites.charAt((i * 4) + 1) - '0');
                    GamesGroupElement game = this.mGameList.getGameById((group_id << 8) + ((favorites.charAt((i * 4) + 2) - '0') * 10) + (favorites.charAt((i * 4) + 3) - '0'));
                    if (game != null) {
                        add(game);
                    }
                }
                Collections.sort(this);
                return;
            }
            return;
        }
        setDefault();
    }

    public void init(Context context, GameList game_list, int[] defaultIds) {
        this.mContext = context;
        this.mGameList = game_list;
        setDefault(defaultIds);
    }

    public void setDefault() {
        setDefault(mDefaultIds);
    }

    public void setDefault(int[] defaultIds) {
        clear();
        for (int gameById : defaultIds) {
            GamesGroupElement game = this.mGameList.getGameById(gameById);
            if (game != null) {
                add(game);
            }
        }
        Collections.sort(this);
    }

    public void store() {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.mContext).edit();
        StringBuilder builder = new StringBuilder();
        Iterator it = iterator();
        while (it.hasNext()) {
            GamesGroupElement favorite = (GamesGroupElement) it.next();
            int n = (favorite.Id >> 8) & 255;
            builder.append((char) ((n / 10) + 48));
            builder.append((char) ((n % 10) + 48));
            int n2 = favorite.Id & 255;
            builder.append((char) ((n2 / 10) + 48));
            builder.append((char) ((n2 % 10) + 48));
        }
        editor.putString(FAVORITES_KEY, builder.toString());
        editor.commit();
    }

    public boolean addAndStore(GamesGroupElement e) {
        Iterator it = iterator();
        while (it.hasNext()) {
            if (((GamesGroupElement) it.next()).Id == e.Id) {
                Utils.Toast(this.mContext, R.string.already_in_favorites, e.Name);
                return false;
            }
        }
        add(e);
        Collections.sort(this);
        store();
        Utils.Toast(this.mContext, R.string.added_to_favorites, e.Name);
        return true;
    }
}
