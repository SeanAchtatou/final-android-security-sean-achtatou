package com.anoshenko.android.select;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.anoshenko.android.solitaires.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

class GamesListAdapter implements ListAdapter {
    private ArrayList<DataSetObserver> DataSetObserverList = new ArrayList<>();
    private final Context mContext;
    private final Vector<GamesGroupElement> mGameList = new Vector<>();
    private final boolean mLetters;

    public GamesListAdapter(Context context, Vector<GamesGroupElement> list, boolean letters) {
        this.mContext = context;
        this.mLetters = letters;
        setGameList(list);
    }

    public void setGameList(Vector<GamesGroupElement> list) {
        GamesGroupElement prev = null;
        this.mGameList.clear();
        Iterator<GamesGroupElement> it = list.iterator();
        while (it.hasNext()) {
            GamesGroupElement element = it.next();
            if (this.mLetters && (prev == null || prev.Name.charAt(0) != element.Name.charAt(0))) {
                this.mGameList.add(null);
            }
            this.mGameList.add(element);
            prev = element;
        }
    }

    public void setGameList(GamesGroupElement[] list) {
        GamesGroupElement prev = null;
        this.mGameList.clear();
        for (GamesGroupElement element : list) {
            if (this.mLetters && (prev == null || prev.Name.charAt(0) != element.Name.charAt(0))) {
                this.mGameList.add(null);
            }
            this.mGameList.add(element);
            prev = element;
        }
    }

    public boolean hasStableIds() {
        return false;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int position) {
        return this.mGameList.get(position) != null;
    }

    public int getCount() {
        return this.mGameList.size();
    }

    public boolean isEmpty() {
        return this.mGameList.size() == 0;
    }

    public Object getItem(int position) {
        return this.mGameList.get(position);
    }

    public long getItemId(int position) {
        GamesGroupElement element = this.mGameList.get(position);
        return (long) (element == null ? -1 : element.Id);
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int position) {
        return this.mGameList.get(position) != null ? 0 : 1;
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    public View getView(int position, View convertView, ViewGroup parent) {
        if (this.mGameList.get(position) == null) {
            if (convertView == null) {
                convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.list_letter, (ViewGroup) null);
            }
            ((TextView) convertView).setText(this.mGameList.get(position + 1).Name.substring(0, 1));
        } else {
            if (convertView == null) {
                convertView = LayoutInflater.from(this.mContext).inflate((int) R.layout.list_game_item_view, (ViewGroup) null);
            }
            ((TextView) convertView.findViewById(R.id.SelectGameItemText)).setText(this.mGameList.get(position).Name);
        }
        return convertView;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.DataSetObserverList.remove(observer);
    }
}
