package com.anoshenko.android.solitaires;

import android.graphics.Typeface;

public enum ValueFont {
    DEFAULT(0, Typeface.DEFAULT),
    BOLD(1, Typeface.DEFAULT_BOLD),
    MONOSPACE(2, Typeface.MONOSPACE),
    SANS_SERIF(3, Typeface.SANS_SERIF),
    SERIF(4, Typeface.SERIF);
    
    public final int mId;
    public final Typeface mTypeface;

    private ValueFont(int id, Typeface typeface) {
        this.mId = id;
        this.mTypeface = typeface;
    }

    public static final ValueFont getById(int id) {
        for (ValueFont font : values()) {
            if (font.mId == id) {
                return font;
            }
        }
        return null;
    }
}
