package com.anoshenko.android.solitaires;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

public final class ArrowDrawer {
    public static final int COLOR = -65536;
    private final int ARROW_SIZE;
    private final int ARROW_SWING;
    private final int WIDTH;
    private final Paint mPaint = new Paint();

    public ArrowDrawer(Context context) {
        int min_size = Utils.getMinDislpaySide(context);
        if (min_size > 360) {
            this.ARROW_SWING = 10;
            this.ARROW_SIZE = 32;
            this.WIDTH = 4;
        } else if (min_size < 320) {
            this.ARROW_SWING = 6;
            this.ARROW_SIZE = 16;
            this.WIDTH = 2;
        } else {
            this.ARROW_SWING = 8;
            this.ARROW_SIZE = 24;
            this.WIDTH = 3;
        }
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setColor((int) COLOR);
        this.mPaint.setStrokeWidth((float) this.WIDTH);
        this.mPaint.setAntiAlias(true);
    }

    public Paint getPaint() {
        return this.mPaint;
    }

    public void draw(Canvas g, int start_x, int start_y, int end_x, int end_y) {
        int x3;
        int y3;
        int y5;
        int y4;
        int size_x = end_x - start_x;
        int size_y = end_y - start_y;
        int size = (int) Math.sqrt((double) ((size_x * size_x) + (size_y * size_y)));
        if (size != 0) {
            x3 = end_x - ((this.ARROW_SIZE * size_x) / size);
        } else {
            x3 = end_x;
        }
        if (size != 0) {
            y3 = end_y - ((this.ARROW_SIZE * size_y) / size);
        } else {
            y3 = end_y;
        }
        int dx = size > 0 ? Math.abs((this.ARROW_SWING * size_y) / size) : 1;
        int dy = size > 0 ? Math.abs((this.ARROW_SWING * size_x) / size) : 1;
        int x4 = x3 - dx;
        int x5 = x3 + dx;
        if ((end_x >= start_x || end_y >= start_y) && (end_x < start_x || end_y < start_y)) {
            y4 = y3 - dy;
            y5 = y3 + dy;
        } else {
            y4 = y3 + dy;
            y5 = y3 - dy;
        }
        g.drawRect((float) (start_x - (this.WIDTH / 2)), (float) (start_y - (this.WIDTH / 2)), (float) ((this.WIDTH / 2) + start_x), (float) ((this.WIDTH / 2) + start_y), this.mPaint);
        g.drawLine((float) start_x, (float) start_y, (float) end_x, (float) end_y, this.mPaint);
        g.drawLine((float) x4, (float) y4, (float) end_x, (float) end_y, this.mPaint);
        g.drawLine((float) x5, (float) y5, (float) end_x, (float) end_y, this.mPaint);
    }
}
