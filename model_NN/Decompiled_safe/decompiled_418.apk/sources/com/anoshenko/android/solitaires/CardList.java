package com.anoshenko.android.solitaires;

import android.graphics.Canvas;
import android.graphics.Rect;
import java.util.Iterator;

public class CardList implements Iterable<Card> {
    /* access modifiers changed from: private */
    public Card mFirst = null;
    private Card mLast = null;
    /* access modifiers changed from: private */
    public int mSize = 0;

    public CardList() {
    }

    public CardList(Card[] cards) {
        for (Card add : cards) {
            add(add);
        }
    }

    public final int size() {
        return this.mSize;
    }

    public Card firstElement() {
        return this.mFirst;
    }

    public Card lastElement() {
        return this.mLast;
    }

    public final void clear() {
        this.mLast = null;
        this.mFirst = null;
        this.mSize = 0;
    }

    public final void add(Card card) {
        if (card != null) {
            if (this.mSize == 0) {
                this.mFirst = card;
            } else {
                this.mLast.next = card;
            }
            card.prev = this.mLast;
            card.next = null;
            this.mLast = card;
            this.mSize++;
        }
    }

    public final void add(CardList list) {
        if (list.mSize > 0) {
            if (this.mSize == 0) {
                this.mFirst = list.mFirst;
                this.mFirst.prev = null;
            } else {
                this.mLast.next = list.mFirst;
            }
            list.mFirst.prev = this.mLast;
            list.mLast.next = null;
            this.mLast = list.mLast;
            this.mSize += list.mSize;
        }
    }

    public final void insert(Card card, int number) {
        if (card == null) {
            return;
        }
        if (number >= this.mSize) {
            add(card);
        } else if (number <= 0) {
            card.next = this.mFirst;
            if (this.mFirst != null) {
                this.mFirst.prev = card;
            }
            this.mFirst = card;
            this.mSize++;
        } else {
            Card card2 = this.mFirst;
            for (int i = 1; i < number; i++) {
                card2 = card2.next;
            }
            card.prev = card2;
            card.next = card2.next;
            card.next.prev = card;
            card2.next = card;
            this.mSize++;
        }
    }

    public final Card remove(int number) {
        Card card;
        if (number < 0 || number >= this.mSize) {
            return null;
        }
        if (number == 0) {
            card = this.mFirst;
            if (this.mSize > 1) {
                this.mFirst = this.mFirst.next;
                this.mFirst.prev = null;
            } else {
                this.mLast = null;
                this.mFirst = null;
            }
        } else if (number == this.mSize - 1) {
            card = this.mLast;
            this.mLast = this.mLast.prev;
            this.mLast.next = null;
        } else {
            card = this.mFirst;
            for (int i = 0; i < number; i++) {
                card = card.next;
            }
            card.prev.next = card.next;
            card.next.prev = card.prev;
        }
        this.mSize--;
        return card;
    }

    public final Card removeLast() {
        Card card = this.mLast;
        switch (this.mSize) {
            case 0:
                break;
            case 1:
                this.mLast = null;
                this.mFirst = null;
                this.mSize = 0;
                break;
            default:
                this.mLast = this.mLast.prev;
                this.mLast.next = null;
                this.mSize--;
                break;
        }
        return card;
    }

    public final CardList removeLast(int count) {
        CardList list = new CardList();
        if (count >= this.mSize) {
            list.mSize = this.mSize;
            list.mFirst = this.mFirst;
            list.mLast = this.mLast;
            this.mLast = null;
            this.mFirst = null;
            this.mSize = 0;
        } else {
            list.mSize = count;
            Card card = this.mLast;
            list.mLast = card;
            list.mFirst = card;
            for (int i = 1; i < count; i++) {
                list.mFirst = list.mFirst.prev;
            }
            this.mLast = list.mFirst.prev;
            this.mLast.next = null;
            this.mSize -= count;
        }
        return list;
    }

    public final Card getCard(int number) {
        if (number < 0 || number >= this.mSize) {
            return null;
        }
        Card card = this.mFirst;
        for (int i = 0; i < number; i++) {
            if (card == null) {
                return null;
            }
            card = card.next;
        }
        return card;
    }

    /* access modifiers changed from: package-private */
    public final void StoreState(BitStack stack) {
        stack.add(this.mSize, 5, 10);
        Card card = this.mFirst;
        for (int i = 0; i < this.mSize; i++) {
            stack.add(card.mOpen);
            stack.add(card.mLock);
            stack.add(card.mSuit, 2);
            stack.add(card.mValue, 4);
            card = card.next;
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean LoadState(BitStack stack, Pack pack) {
        int size = stack.getInt(5, 10);
        for (int i = 0; i < size; i++) {
            boolean f_open = stack.getFlag();
            boolean f_lock = stack.getFlag();
            int suit = stack.getInt(2);
            int value = stack.getInt(4);
            Card card = pack.mWorkPack.mLast;
            int k = pack.mWorkPack.mSize - 1;
            while (true) {
                if (k >= 0) {
                    if (card.mValue == value && card.mSuit == suit) {
                        card.mOpen = f_open;
                        card.mLock = f_lock;
                        add(pack.mWorkPack.remove(k));
                        break;
                    }
                    card = card.prev;
                    k--;
                } else {
                    break;
                }
            }
            if (k < 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void draw(Canvas canvas, Rect clip_rect) {
        Card card = this.mFirst;
        for (int i = 0; i < this.mSize; i++) {
            if (card.mNextOffset != -1) {
                card.draw(canvas, clip_rect);
            }
            card = card.next;
        }
    }

    public final void getBounds(Rect rect) {
        if (this.mSize > 0) {
            int width = this.mFirst.getWidth();
            int height = this.mFirst.getHeight();
            rect.set(this.mFirst.xPos, this.mFirst.yPos, this.mFirst.xPos + width, this.mFirst.yPos + height);
            Card card = this.mFirst.next;
            for (int i = 1; i < this.mSize; i++) {
                rect.union(card.xPos, card.yPos, card.xPos + width, card.yPos + height);
                card = card.next;
            }
        }
    }

    public Iterator<Card> iterator() {
        return new Iterator<Card>() {
            Card card;
            int number = 0;

            {
                this.card = CardList.this.mFirst;
            }

            public boolean hasNext() {
                return this.number < CardList.this.mSize;
            }

            public Card next() {
                Card ret = this.card;
                this.card = this.card.next;
                this.number++;
                return ret;
            }

            public void remove() {
            }
        };
    }

    public void unmarkAll() {
        Iterator<Card> it = iterator();
        while (it.hasNext()) {
            it.next().mMark = false;
        }
    }
}
