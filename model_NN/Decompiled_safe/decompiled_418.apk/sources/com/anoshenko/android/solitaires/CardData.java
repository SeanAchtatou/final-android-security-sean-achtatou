package com.anoshenko.android.solitaires;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import com.anoshenko.android.theme.BuildinCardResources;
import com.anoshenko.android.theme.CardBack;

public final class CardData {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize = null;
    public static final String CARD_BACK_KEY = "CardBack";
    public static final String CARD_BACK_TYPE_KEY = "CARD_BACK_TYPE";
    public static final int CARD_TYPE_DRAG = 2;
    public static final int CARD_TYPE_MARKED = 1;
    public static final int CARD_TYPE_NORMAL = 0;
    public static final String NORMAL_VALUE_FONT_KEY = "NORMAL_VALUE_FONT";
    public static final String SMALL_VALUE_FONT_KEY = "SMALL_VALUE_FONT";
    public static final String USE_AJQK_KEY = "USE_AJQK";
    public static final String VALUE_FONT_KEY = "VALUE_FONT";
    public final int Angle;
    public final int Height;
    final int[] PictureData;
    public final int Width;
    public Bitmap mBackImage = null;
    public int mBackNumber = 0;
    public int mBackType = 0;
    public Bitmap mBaseImage;
    private final int mBorder;
    public Bitmap mBorderImage;
    private final Context mContext;
    public Bitmap mDragImage;
    public Bitmap mJokerImage;
    public Bitmap mMarkImage;
    private final int mMaxValueDescent;
    public Bitmap mPictureImage;
    private final int mSingleWidth;
    public final CardSize mSizeType;
    public Bitmap mSuitImage;
    private final int mValueAscent;
    private final int[] mValueDescent = new int[14];
    private final ValueFont mValueFont;
    private final int mValueFontSize;
    private final Paint mValuePaint;
    private final int[] mValueWidth = new int[14];
    private final String[] mValues = new String[14];
    public final int xOffset;
    public final int yOffset;
    private int yTextOffset;

    static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize() {
        int[] iArr = $SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize;
        if (iArr == null) {
            iArr = new int[CardSize.values().length];
            try {
                iArr[CardSize.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[CardSize.SMALL.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            $SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize = iArr;
        }
        return iArr;
    }

    public class InvalidSizeException extends Exception {
        public InvalidSizeException() {
        }
    }

    public CardData(CardSize size, Context context, ValueFont font, boolean load_images) throws InvalidSizeException {
        int pictureImageHeight;
        int height;
        int w;
        this.mContext = context;
        this.mSizeType = size;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        font = font == null ? ValueFont.getById(prefs.getInt(VALUE_FONT_KEY, 0)) : font;
        for (int i = 2; i <= 10; i++) {
            this.mValues[i] = Integer.toString(i);
        }
        if (prefs.getBoolean(USE_AJQK_KEY, false)) {
            this.mValues[0] = "Joker";
            this.mValues[1] = "A";
            this.mValues[11] = "J";
            this.mValues[12] = "Q";
            this.mValues[13] = "K";
        } else {
            this.mValues[0] = context.getString(R.string.Joker);
            this.mValues[1] = context.getString(R.string.A);
            this.mValues[11] = context.getString(R.string.J);
            this.mValues[12] = context.getString(R.string.Q);
            this.mValues[13] = context.getString(R.string.K);
        }
        this.mValueFont = font != null ? font : ValueFont.DEFAULT;
        BuildinCardResources buildinCardResources = new BuildinCardResources(context);
        Resources resources = context.getResources();
        DisplayMetrics dm = Utils.getDisplayMetrics(context);
        int min_dimension = Math.min(dm.widthPixels, dm.heightPixels);
        switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize()[size.ordinal()]) {
            case 1:
                this.Width = min_dimension / 10;
                break;
            default:
                this.Width = min_dimension / 8;
                break;
        }
        this.Angle = buildinCardResources.getAngle(size);
        this.mBorder = buildinCardResources.getBorder(size);
        this.PictureData = buildinCardResources.getPictureData(size);
        this.yTextOffset = this.mBorder;
        if (load_images) {
            this.mBorderImage = Utils.loadBitmap(resources, R.drawable.border);
            this.mJokerImage = buildinCardResources.getJokerImage(size);
            this.mSuitImage = buildinCardResources.getSuitImages(size);
            this.mPictureImage = buildinCardResources.getPictureImages(size);
        }
        if (min_dimension == 320 && this.yTextOffset == 1) {
            this.yTextOffset = 2;
        }
        int font_size = getDefaultValueFontSize(context, size);
        String font_size_str = prefs.getString(size == CardSize.SMALL ? SMALL_VALUE_FONT_KEY : NORMAL_VALUE_FONT_KEY, null);
        if (font_size_str != null) {
            try {
                font_size = Integer.parseInt(font_size_str);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        this.mValueFontSize = font_size;
        int single_width = 0;
        int ascent = 0;
        int descent = 0;
        Paint paint = new Paint();
        Rect bounds = new Rect();
        paint.setAntiAlias(true);
        paint.setTextSize((float) this.mValueFontSize);
        paint.setTypeface(this.mValueFont.mTypeface);
        for (int value_index = 1; value_index <= 13; value_index++) {
            String value = this.mValues[value_index];
            int len = value.length();
            if (len > 1) {
                if (this.mValueFont == ValueFont.BOLD || this.mValueFont == ValueFont.SANS_SERIF) {
                    paint.setTypeface(Typeface.DEFAULT);
                }
                w = 0;
                for (int i2 = 0; i2 < len; i2++) {
                    paint.getTextBounds(value, i2, i2 + 1, bounds);
                    w += bounds.width();
                    this.mValueDescent[value_index] = Math.max(this.mValueDescent[value_index], bounds.bottom);
                    ascent = Math.max(ascent, -bounds.top);
                }
                descent = Math.max(descent, bounds.bottom);
                paint.setTypeface(this.mValueFont.mTypeface);
            } else {
                paint.getTextBounds(value, 0, 1, bounds);
                w = bounds.width();
                single_width = Math.max(single_width, w);
                ascent = Math.max(ascent, -bounds.top);
                descent = Math.max(descent, bounds.bottom);
                this.mValueDescent[value_index] = bounds.bottom;
            }
            this.mValueWidth[value_index] = w;
        }
        this.mSingleWidth = single_width;
        int width = Math.max(0, (this.mBorder * 2) + single_width);
        this.mValueAscent = ascent;
        this.mMaxValueDescent = descent;
        this.xOffset = this.mBorder + width;
        if (descent + 1 > this.yTextOffset * 2) {
            this.yOffset = this.mBorder + ascent + descent + 1;
        } else {
            this.yOffset = this.mBorder + ascent + (this.yTextOffset * 2);
        }
        if (this.mPictureImage != null) {
            pictureImageHeight = this.mPictureImage.getHeight();
        } else {
            pictureImageHeight = buildinCardResources.getPictureImageHeight(size);
        }
        int pic_height = pictureImageHeight / 4;
        if (this.mMaxValueDescent > this.yTextOffset) {
            height = (this.mBorder * 2) + this.mValueAscent + this.mMaxValueDescent + pic_height;
        } else {
            height = this.yOffset + this.mBorder + pic_height;
        }
        this.Height = Math.max((this.Width * 4) / 3, height);
        this.mValuePaint = new Paint();
        this.mValuePaint.setAntiAlias(true);
        this.mValuePaint.setTextSize((float) this.mValueFontSize);
        this.mValuePaint.setTypeface(this.mValueFont.mTypeface);
        if (load_images) {
            updateCardBack();
        } else {
            this.mBackType = prefs.getInt(CARD_BACK_TYPE_KEY, 0);
            this.mBackNumber = prefs.getInt(CARD_BACK_KEY, 0);
        }
        this.mBaseImage = createBaseImage(-1);
        this.mMarkImage = createBaseImage(-5761);
        this.mDragImage = createBaseImage(-1056964609);
    }

    public static int getDefaultValueFontSize(Context context, CardSize size) {
        DisplayMetrics dm = Utils.getDisplayMetrics(context);
        int min_dimension = Math.min(dm.widthPixels, dm.heightPixels);
        if (min_dimension == 240) {
            switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize()[size.ordinal()]) {
                case 1:
                    return 12;
                case 2:
                    return 14;
                default:
                    return 0;
            }
        } else if (min_dimension >= 768) {
            switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize()[size.ordinal()]) {
                case 1:
                    return 44;
                case 2:
                    return 48;
                default:
                    return 0;
            }
        } else if (min_dimension >= 600) {
            switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize()[size.ordinal()]) {
                case 1:
                    return 40;
                case 2:
                    return 40;
                default:
                    return 0;
            }
        } else if (min_dimension >= 480) {
            switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize()[size.ordinal()]) {
                case 1:
                    return 32;
                case 2:
                    return 38;
                default:
                    return 0;
            }
        } else {
            switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$CardSize()[size.ordinal()]) {
                case 1:
                    return 22;
                case 2:
                    return 26;
                default:
                    return 0;
            }
        }
    }

    private Bitmap createBaseImage(int bk_color) {
        Bitmap result;
        try {
            result = Bitmap.createBitmap(this.Width, this.Height, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            System.gc();
            result = Bitmap.createBitmap(this.Width, this.Height, Bitmap.Config.ARGB_8888);
        }
        result.eraseColor(0);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(bk_color);
        float off = ((float) this.mBorder) / 2.0f;
        float r = getBorderRadius();
        RectF rect = new RectF(off, off, ((float) this.Width) - off, ((float) this.Height) - off);
        canvas.drawRoundRect(rect, r, r, paint);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(1.0f);
        rect.set(0.5f, 0.5f, ((float) this.Width) - 0.5f, ((float) this.Height) - 0.5f);
        if (this.mBorder > 1) {
            paint.setColor(-6250336);
            canvas.drawRoundRect(rect, r, r, paint);
            float off2 = (((float) (this.mBorder - 1)) / 2.0f) + 1.0f;
            rect.set(off2, off2, ((float) this.Width) - off2, ((float) this.Height) - off2);
            paint.setStrokeWidth((float) (this.mBorder - 1));
            paint.setColor(-16777216);
            canvas.drawRoundRect(rect, r, r, paint);
        } else {
            paint.setColor(-16777216);
            canvas.drawRoundRect(rect, r, r, paint);
        }
        return result;
    }

    public void release() {
        if (this.mJokerImage != null) {
            this.mJokerImage.recycle();
            this.mJokerImage = null;
        }
        if (this.mSuitImage != null) {
            this.mSuitImage.recycle();
            this.mSuitImage = null;
        }
        if (this.mPictureImage != null) {
            this.mPictureImage.recycle();
            this.mPictureImage = null;
        }
        if (this.mBorderImage != null) {
            this.mBorderImage.recycle();
            this.mBorderImage = null;
        }
        if (this.mBaseImage != null) {
            this.mBaseImage.recycle();
            this.mBaseImage = null;
        }
        if (this.mMarkImage != null) {
            this.mMarkImage.recycle();
            this.mMarkImage = null;
        }
        if (this.mDragImage != null) {
            this.mDragImage.recycle();
            this.mDragImage = null;
        }
    }

    public boolean isNeedReload() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        if (ValueFont.getById(prefs.getInt(VALUE_FONT_KEY, 0)) != this.mValueFont) {
            return true;
        }
        int back_type = prefs.getInt(CARD_BACK_TYPE_KEY, 0);
        int back_number = prefs.getInt(CARD_BACK_KEY, 0);
        if (!(back_type == this.mBackType && back_type != 1 && back_number == this.mBackNumber)) {
            updateCardBack();
        }
        return false;
    }

    public static final int getNormalWidth(Context context) {
        DisplayMetrics dm = Utils.getDisplayMetrics(context);
        int min_dimension = Math.min(dm.widthPixels, dm.heightPixels);
        if (min_dimension == 240) {
            return 30;
        }
        if (min_dimension >= 600) {
            return 75;
        }
        if (min_dimension >= 480) {
            return 60;
        }
        return 40;
    }

    public final void updateCardBack() {
        CardBack card_back;
        if (this.mBackImage != null) {
            this.mBackImage.recycle();
            this.mBackImage = null;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this.mContext);
        this.mBackType = prefs.getInt(CARD_BACK_TYPE_KEY, 0);
        if (this.mBackType == 1) {
            card_back = new CardBack(this.mContext);
        } else {
            this.mBackNumber = prefs.getInt(CARD_BACK_KEY, 0);
            card_back = new CardBack(this.mBackNumber);
        }
        this.mBackImage = card_back.createBitmap(this.mContext, this);
    }

    public void drawTopLeftValue(Canvas canvas, int card_x, int card_y, int value, int color) {
        Paint paint = this.mValuePaint;
        String value_text = this.mValues[value];
        int len = value_text.length();
        paint.setAntiAlias(true);
        paint.setTextSize((float) this.mValueFontSize);
        paint.setColor(color);
        paint.setTypeface((len <= 1 || !(this.mValueFont == ValueFont.BOLD || this.mValueFont == ValueFont.SERIF)) ? this.mValueFont.mTypeface : Typeface.DEFAULT);
        float y = (float) (this.mBorder + card_y + (this.mValueDescent[value] > this.yTextOffset ? 1 : this.yTextOffset) + this.mValueAscent);
        if (len > 1) {
            paint.setTextAlign(Paint.Align.LEFT);
            Rect bounds = new Rect();
            float x = (float) (this.mBorder + card_x);
            for (int i = 0; i < len; i++) {
                paint.getTextBounds(value_text, i, i + 1, bounds);
                canvas.drawText(value_text, i, i + 1, x - ((float) bounds.left), y, paint);
                x += (float) bounds.width();
            }
            return;
        }
        paint.setTextAlign(Paint.Align.CENTER);
        canvas.drawText(value_text, (float) ((this.mBorder * 2) + card_x + (this.mSingleWidth / 2)), y, paint);
    }

    public void drawBottomRightValue(Canvas canvas, int card_x, int card_y, int value, int color) {
        float y;
        Paint paint = this.mValuePaint;
        String value_text = this.mValues[value];
        int len = value_text.length();
        paint.setAntiAlias(true);
        paint.setTextSize((float) this.mValueFontSize);
        paint.setColor(color);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTypeface((len <= 1 || !(this.mValueFont == ValueFont.BOLD || this.mValueFont == ValueFont.SERIF)) ? this.mValueFont.mTypeface : Typeface.DEFAULT);
        float y2 = (float) ((this.Height + card_y) - this.mBorder);
        if (this.mValueDescent[value] > 0) {
            y = y2 - ((float) (this.mValueDescent[value] + 1));
        } else {
            y = y2 - ((float) this.yTextOffset);
        }
        if (len > 1) {
            float x = (float) (((this.Width + card_x) - this.mBorder) - this.mValueWidth[value]);
            Rect bounds = new Rect();
            for (int i = 0; i < len; i++) {
                paint.getTextBounds(value_text, i, i + 1, bounds);
                canvas.drawText(value_text, i, i + 1, x - ((float) bounds.left), y, paint);
                x += (float) bounds.width();
            }
            return;
        }
        canvas.drawText(value_text, (float) ((((this.Width + card_x) - (this.mBorder * 2)) - this.mSingleWidth) + (((this.mSingleWidth - this.mValueWidth[value]) + 1) / 2)), y, paint);
    }

    public void DrawEmpty(Canvas canvas, int x, int y, int symbol) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setShader(new BitmapShader(this.mBorderImage, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT));
        paint.setStrokeWidth((float) this.mBorder);
        float r = getBorderRadius();
        float off = ((float) this.mBorder) / 2.0f;
        canvas.drawRoundRect(new RectF(((float) x) + off, ((float) y) + off, ((float) (this.Width + x)) - off, ((float) (this.Height + y)) - off), r, r, paint);
        if (symbol > 0 && symbol <= 13) {
            drawTopLeftValue(canvas, x, y, symbol, -1);
        }
    }

    public final int getBorderWidth() {
        return this.mBorder;
    }

    public final float getBorderRadius() {
        return (float) (this.Angle * 2);
    }
}
