package com.anoshenko.android.solitaires;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.anoshenko.android.background.BackgroundActivity;
import com.anoshenko.android.theme.CardSettingsActivity;
import com.anoshenko.android.ui.OptionsActivity;
import com.heyzap.sdk.HeyzapLib;
import java.io.File;
import java.io.IOException;

public final class Utils {
    public static final String HOME_FOLDER = "250Solitaires";

    public static void Message(Context context, int text_id, int title_id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        if (title_id > 0) {
            dialog.setTitle(title_id);
        }
        dialog.setMessage(text_id);
        dialog.setCancelable(true);
        dialog.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        dialog.show();
    }

    public static void Message(Context context, int text_id, String arg, int title_id) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        if (title_id > 0) {
            dialog.setTitle(title_id);
        }
        dialog.setMessage(context.getString(text_id, arg));
        dialog.setCancelable(true);
        dialog.setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        dialog.show();
    }

    public static void Question(Context context, int text_id, int title_id, DialogInterface.OnClickListener yes_listener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        if (title_id > 0) {
            dialog.setTitle(title_id);
        }
        dialog.setMessage(text_id);
        dialog.setCancelable(true);
        dialog.setPositiveButton((int) R.string.yes, yes_listener);
        dialog.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        dialog.show();
    }

    public static void Question(Context context, int text_id, String arg, int title_id, DialogInterface.OnClickListener yes_listener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        if (title_id > 0) {
            dialog.setTitle(title_id);
        }
        dialog.setMessage(context.getString(text_id, arg));
        dialog.setCancelable(true);
        dialog.setPositiveButton((int) R.string.yes, yes_listener);
        dialog.setNegativeButton((int) R.string.no, (DialogInterface.OnClickListener) null);
        dialog.show();
    }

    public static void Toast(Context context, int text_id, String arg) {
        Toast.makeText(context, context.getString(text_id, arg), 1).show();
    }

    public static Dialog About(Context context) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate((int) R.layout.about_view, (ViewGroup) null);
        try {
            ((TextView) view.findViewById(R.id.VersionNumber)).setText(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        dialog.setTitle((int) R.string.about);
        dialog.setView(view);
        dialog.setNeutralButton(17039370, (DialogInterface.OnClickListener) null);
        dialog.setCancelable(true);
        return dialog.show();
    }

    public static final void createMenu(Context context, Menu menu) {
        menu.add(0, (int) Command.OPTIONS, 1, (int) R.string.options_menu_item).setIcon((int) R.drawable.icon_settings);
        menu.add(0, (int) Command.BACKGROUND, 1, (int) R.string.appearance).setIcon((int) R.drawable.icon_background);
        menu.add(0, (int) Command.CARD_BACK, 1, (int) R.string.cards).setIcon((int) R.drawable.icon_cardback);
        menu.add(0, (int) Command.HEYZAP, 1, "Heyzap check in").setIcon((int) R.drawable.icon_heyzap);
        menu.add(0, (int) Command.TRANSLATE, 1, (int) R.string.translate).setIcon((int) R.drawable.icon_translate);
        menu.add(0, (int) Command.ABOUT, 1, (int) R.string.about).setIcon((int) R.drawable.icon_about);
    }

    public static final boolean onMenuItemSelected(Activity activity, MenuItem item) {
        switch (item.getItemId()) {
            case Command.ABOUT:
                About(activity);
                return true;
            case Command.OPTIONS:
                activity.startActivityForResult(new Intent(activity, OptionsActivity.class), Command.OPTIONS_ACTIVITY);
                return true;
            case Command.CARD_BACK:
                activity.startActivityForResult(new Intent(activity, CardSettingsActivity.class), Command.CARDS_ACTIVITY);
                return true;
            case Command.BACKGROUND:
                activity.startActivityForResult(new Intent(activity, BackgroundActivity.class), Command.BACKGROUND_ACTIVITY);
                return true;
            case Command.TRANSLATE:
                Translation.startTranslationPage(activity);
                return true;
            case Command.HEYZAP:
                HeyzapLib.checkin(activity);
                return true;
            default:
                return false;
        }
    }

    public static final DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        return dm;
    }

    public static final int getMinDislpaySide(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(dm);
        return Math.min(dm.heightPixels, dm.widthPixels);
    }

    public static final Bitmap loadBitmap(Resources resources, int id) {
        try {
            return BitmapFactory.decodeResource(resources, id);
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
            System.gc();
            return BitmapFactory.decodeResource(resources, id);
        }
    }

    public static Bitmap loadBitmap(String filename, int pref_width, int pref_height) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, opts);
        opts.inSampleSize = Math.min(Math.max(1, opts.outHeight / pref_height), Math.max(1, opts.outWidth / pref_width));
        opts.inJustDecodeBounds = false;
        try {
            return BitmapFactory.decodeFile(filename, opts);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.gc();
            try {
                return BitmapFactory.decodeFile(filename, opts);
            } catch (OutOfMemoryError e2) {
                return null;
            }
        }
    }

    public static void setOrientation(Activity activity) {
        int new_orientation;
        try {
            switch (Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(activity).getString(activity.getResources().getString(R.string.pref_orientation), "0"))) {
                case 1:
                    new_orientation = 1;
                    break;
                case 2:
                    new_orientation = 0;
                    break;
                case 3:
                    new_orientation = 4;
                    break;
                default:
                    new_orientation = -1;
                    break;
            }
            if (activity.getRequestedOrientation() != new_orientation) {
                activity.setRequestedOrientation(new_orientation);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File getHomeFolder() throws IOException {
        File storage = Environment.getExternalStorageDirectory();
        if (storage == null) {
            storage = new File("/emmc");
            if (!storage.exists() || !storage.isDirectory() || !storage.canWrite()) {
                return null;
            }
        }
        File home_folder = new File(storage, HOME_FOLDER);
        if (!home_folder.exists()) {
            home_folder.mkdir();
        }
        return home_folder;
    }
}
