package com.anoshenko.android.solitaires;

import android.app.Dialog;
import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

class MoveVariantChooser implements ListAdapter, AdapterView.OnItemClickListener {
    private final Pile fromPile;
    private final PlayActivity mActivity;
    /* access modifiers changed from: private */
    public final int mControlHeight;
    private ArrayList<DataSetObserver> mDataSetObserverList = new ArrayList<>();
    private Dialog mDialog = null;
    /* access modifiers changed from: private */
    public final Game mGame;
    private final Vector<MoveVariant> mVariants;
    private final Pile toPile;

    private class MoveVariantPreview extends View {
        private Card[] mCards;
        private MoveVariant mVariant;

        public MoveVariantPreview(Context context, MoveVariant variant) {
            super(context);
            int count = variant.Count;
            Card card = variant.card;
            int i = 0;
            while (true) {
                if (i >= count) {
                    break;
                } else if (card == null) {
                    count = i;
                    break;
                } else {
                    card = card.next;
                    i++;
                }
            }
            this.mVariant = variant;
            this.mCards = new Card[count];
            Card card2 = this.mVariant.card;
            if (variant.Type == MoveVariantType.SEQUENTIALLY) {
                for (int i2 = count - 1; i2 >= 0; i2--) {
                    this.mCards[i2] = card2;
                    card2 = card2.next;
                }
            } else {
                for (int i3 = 0; i3 < count; i3++) {
                    this.mCards[i3] = card2;
                    card2 = card2.next;
                }
            }
            setLayoutParams(new ViewGroup.LayoutParams(MoveVariantChooser.this.mGame.mCardData.Width, MoveVariantChooser.this.mControlHeight));
        }

        public void onDraw(Canvas g) {
            if (this.mCards.length != 0) {
                CardData data = MoveVariantChooser.this.mGame.mCardData;
                int y_pos = 0;
                int n = 0;
                if (this.mCards.length > 1) {
                    drawCard(g, this.mCards[0], 0, 1);
                    y_pos = 0 + data.yOffset;
                    n = 0 + 1;
                    if (this.mCards.length > 2) {
                        int step = Math.min(data.yOffset, ((MoveVariantChooser.this.mControlHeight - data.Height) - data.yOffset) / (this.mVariant.Count - 2));
                        for (int i = 2; i < this.mCards.length; i++) {
                            drawCard(g, this.mCards[n], y_pos, 1);
                            y_pos += step;
                            n++;
                        }
                    }
                }
                if (this.mCards[n] != null) {
                    drawCard(g, this.mCards[n], y_pos, 0);
                }
            }
        }

        private void drawCard(Canvas g, Card card, int y_pos, int next_off) {
            int old_x = card.xPos;
            int old_y = card.yPos;
            int old_next = card.mNextOffset;
            card.xPos = 0;
            card.yPos = y_pos;
            card.mNextOffset = next_off;
            card.draw(g, null);
            card.xPos = old_x;
            card.yPos = old_y;
            card.mNextOffset = old_next;
        }
    }

    private MoveVariantChooser(PlayActivity activity, Pile from_pile, Pile to_pile, Vector<MoveVariant> variants) {
        this.mActivity = activity;
        this.mVariants = variants;
        this.fromPile = from_pile;
        this.toPile = to_pile;
        this.mGame = from_pile.mOwner.mGame;
        int max_count = 0;
        Iterator<MoveVariant> it = variants.iterator();
        while (it.hasNext()) {
            MoveVariant variant = it.next();
            if (variant.Count > max_count) {
                max_count = variant.Count;
            }
        }
        CardData data = this.mGame.mCardData;
        int height = data.Height + ((max_count - 1) * data.yOffset);
        if (variants.size() > 5) {
            if (height > data.Height * 2) {
                height = data.Height * 2;
            }
        } else if (height > data.Height * 3) {
            height = data.Height * 3;
        }
        this.mControlHeight = height;
    }

    /* access modifiers changed from: package-private */
    public void setDialog(Dialog dialog) {
        this.mDialog = dialog;
    }

    static void show(PlayActivity activity2, Pile from_pile, Pile to_pile, Vector<MoveVariant> variants) {
        MoveVariantChooser chooser = new MoveVariantChooser(activity2, from_pile, to_pile, variants);
        Dialog dialog = new Dialog(activity2);
        dialog.setTitle((int) R.string.move_variants_title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView((int) R.layout.move_variant_view);
        GridView view = (GridView) dialog.findViewById(R.id.CardBackGrid);
        view.setAdapter((ListAdapter) chooser);
        view.setOnItemClickListener(chooser);
        chooser.setDialog(dialog);
        dialog.show();
    }

    public int getCount() {
        return this.mVariants.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout layout;
        if (convertView == null) {
            layout = new LinearLayout(this.mActivity);
        } else {
            layout = (LinearLayout) convertView;
            layout.removeAllViews();
        }
        layout.addView(new MoveVariantPreview(this.mActivity, this.mVariants.get(position)));
        return layout;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDataSetObserverList.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        int index = this.mDataSetObserverList.indexOf(observer);
        if (index >= 0 && index < this.mDataSetObserverList.size()) {
            this.mDataSetObserverList.remove(index);
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
        if (this.mDialog != null) {
            this.mDialog.dismiss();
            ((GamePlay_Type0) this.mActivity.mView.mGame).VariantAction(this.fromPile, this.toPile, this.mVariants.get(position));
        }
    }

    public boolean areAllItemsEnabled() {
        return true;
    }

    public boolean isEnabled(int position) {
        return true;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }
}
