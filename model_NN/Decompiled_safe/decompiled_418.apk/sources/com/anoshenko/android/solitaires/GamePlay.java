package com.anoshenko.android.solitaires;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.View;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.ui.Title;
import com.anoshenko.android.ui.ToolbarButton;
import java.util.Vector;

public abstract class GamePlay extends Game {
    /* access modifiers changed from: private */
    public boolean fTimePause = true;
    /* access modifiers changed from: private */
    public Runnable gameTimeRunnable = new Runnable() {
        public void run() {
            if (!GamePlay.this.fTimePause) {
                Title title = (Title) GamePlay.this.mActivity.findViewById(R.id.GameTitle);
                if (title != null) {
                    title.setTimer((int) ((System.currentTimeMillis() - GamePlay.this.mStartTime) / 1000));
                }
                GamePlay.this.mView.postDelayed(GamePlay.this.gameTimeRunnable, 1000 - ((System.currentTimeMillis() - GamePlay.this.mStartTime) % 1000));
            }
        }
    };
    private final ToolbarButton[] mAvailableButton = new ToolbarButton[2];
    protected boolean mAvailableMovesMode = false;
    public long mCurrentTime;
    private ToolbarButton[] mGameToolbarButton;
    final MoveMemory mMoves = new MoveMemory(this);
    protected long mPenDownTime;
    public long mStartTime = System.currentTimeMillis();
    public final Statistics mStatistics = new Statistics();
    private DialogInterface.OnClickListener redeal_yes_listener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
            GamePlay.this.ResetSelection();
            GamePlay.this.mMoves.Reset();
            GamePlay.this.PauseTime();
            GamePlay.this.RedealStart(GamePlay.this.isDealAnimation(), null, new DealFinishAction(GamePlay.this, null));
        }
    };
    private DialogInterface.OnClickListener start_yes_listener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
            GamePlay.this.mStatistics.lose(GamePlay.this);
            GamePlay.this.Start();
        }
    };

    /* access modifiers changed from: package-private */
    public abstract void NextAvailableMoves();

    /* access modifiers changed from: package-private */
    public abstract void PenDownAction(int i, int i2, Vector<Pile> vector);

    /* access modifiers changed from: package-private */
    public abstract boolean isNextAvailableMovesExist();

    /* access modifiers changed from: package-private */
    public abstract boolean isPenDownPackOpen();

    /* access modifiers changed from: package-private */
    public abstract boolean isPenDownPile(Pile pile);

    GamePlay(GameActivity activity, View view, DataSource source) {
        super(activity, view, source);
        String data = Storage.load(this.mActivity, getGameId(), this);
        if (data != null) {
            LoadState(new BitStack(data));
            this.mStartTime = System.currentTimeMillis() - this.mCurrentTime;
        }
    }

    public void initToolbar() {
        Vector<ToolbarButton> buttons = new Vector<>();
        buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_undo, R.drawable.icon_undo_disable, R.string.undo_menu_item, Command.UNDO));
        buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_redo, R.drawable.icon_redo_disable, R.string.redo_menu_item, Command.REDO));
        buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_game, R.drawable.icon_game, R.string.game_menu_item, Command.GAME_MENU));
        Bitmap icon = BitmapFactory.decodeResource(this.mActivity.getResources(), R.drawable.icon_undo);
        DisplayMetrics dm = Utils.getDisplayMetrics(this.mActivity);
        if (Math.min(dm.widthPixels, dm.heightPixels) / icon.getWidth() >= 15) {
            buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_bookmark, R.drawable.icon_bookmark, R.string.set_bookmark_menu_item, Command.SET_BOOKMARK));
            buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_bookmark_back, R.drawable.icon_bookmark_back, R.string.back_button, Command.BACK_BOOKMARK));
            buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_available_moves, R.drawable.icon_available_moves, R.string.available_moves_menu_item, Command.AVAILABLE));
            if (this.mGameType == 0) {
                buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_collect, R.drawable.icon_collect, R.string.collect_menu_item, Command.COLLECT));
            }
        } else {
            buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_collect, R.drawable.icon_collect, R.string.move_menu_item, Command.MOVE_MENU));
        }
        buttons.add(new ToolbarButton(this.mActivity, R.drawable.icon_help, R.drawable.icon_help, R.string.help_menu_item, Command.HELP_MENU));
        this.mGameToolbarButton = new ToolbarButton[buttons.size()];
        buttons.toArray(this.mGameToolbarButton);
        this.mAvailableButton[0] = new ToolbarButton(this.mActivity, R.drawable.icon_ok, R.drawable.icon_ok, R.string.back_button, Command.AVAILABLE_BACK);
        this.mAvailableButton[1] = new ToolbarButton(this.mActivity, R.drawable.icon_next, R.drawable.icon_next_disable, R.string.next_button, Command.AVAILABLE_NEXT);
        setToolbarButtons(this.mGameToolbarButton);
        updateToolbar();
    }

    /* access modifiers changed from: package-private */
    public void Store() {
        PauseTime();
        String data = null;
        if (!isWinFinish()) {
            BitStack stack = new BitStack();
            StoreState(stack);
            data = stack.toString();
        }
        Storage.store(this.mActivity, getGameId(), this.mStatistics, data);
    }

    /* access modifiers changed from: package-private */
    public boolean isEnableEvent() {
        if (this.mAvailableMovesMode || !getCardMoveView().isAnimation()) {
            return true;
        }
        getCardMoveView().setFastRun();
        return false;
    }

    /* access modifiers changed from: package-private */
    public void ResumeTime() {
        if (this.fTimePause) {
            this.mStartTime = System.currentTimeMillis() - this.mCurrentTime;
            this.fTimePause = false;
            this.gameTimeRunnable.run();
        }
    }

    /* access modifiers changed from: package-private */
    public void PauseTime() {
        if (!this.fTimePause) {
            this.fTimePause = true;
            this.mCurrentTime = System.currentTimeMillis() - this.mStartTime;
        }
    }

    /* access modifiers changed from: package-private */
    public void MoveCardAndSave(Pile from_pile, int number, Pile to_pile, boolean f_new_move, GameAction next_action) {
        if (MoveCard(from_pile, number, to_pile, to_pile.size(), true, next_action)) {
            if (f_new_move) {
                ResetSelection();
                this.mMoves.IncreaseMoveNumber();
            }
            this.mMoves.addOneCardMove(from_pile, number, to_pile);
        }
    }

    /* access modifiers changed from: package-private */
    public void MoveCardsAndSave(Pile from_pile, Pile to_pile, int count, boolean f_new_move, GameAction next_action) {
        if (count == 1) {
            MoveCardAndSave(from_pile, from_pile.size() - 1, to_pile, f_new_move, next_action);
            return;
        }
        if (f_new_move) {
            ResetSelection();
            this.mMoves.IncreaseMoveNumber();
        }
        this.mMoves.addSeriesCardMove(from_pile, to_pile, count);
        MoveCards(from_pile, to_pile, count, next_action);
    }

    /* access modifiers changed from: package-private */
    public void Start() {
        ResetSelection();
        this.mMoves.Reset();
        PauseTime();
        this.mCurrentTime = 0;
        this.mStartTime = System.currentTimeMillis();
        DealStart(isDealAnimation(), null, new DealFinishAction(this, null));
    }

    /* access modifiers changed from: package-private */
    public void StartCommand() {
        if (!this.mStarted || isWinFinish()) {
            Start();
        } else {
            Utils.Question(this.mActivity, R.string.start_question, -1, this.start_yes_listener);
        }
    }

    /* access modifiers changed from: package-private */
    public void RestartCommand() {
        Utils.Question(this.mActivity, R.string.restart_question, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                GamePlay.this.ResetSelection();
                GamePlay.this.mMoves.Reset();
                GamePlay.this.PauseTime();
                GamePlay.this.DealStart(GamePlay.this.isDealAnimation(), GamePlay.this.mPack.getStartPack(), new DealFinishAction(GamePlay.this, null));
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void RedealCommand() {
        if (!this.mEnableRedeal) {
            Utils.Message(this.mActivity, R.string.disable_redeal, -1);
        } else if (this.RedealCurrent < this.mRedealCount) {
            Utils.Question(this.mActivity, R.string.redeal_question, -1, this.redeal_yes_listener);
        } else {
            Utils.Message(this.mActivity, R.string.no_more_redeal, -1);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean PenDown(int x, int y) {
        int d;
        if (this.mAvailableMovesMode) {
            return true;
        }
        if (!isEnableEvent()) {
            return false;
        }
        if (!this.mPack.isAvailable() || !isPenDownPackOpen() || !this.mPack.isBelong(x, y)) {
            this.mPenDownTime = System.currentTimeMillis();
            Vector<Pile> pile_list = new Vector<>();
            int order_size = 0;
            Pile[] pileArr = this.mPiles;
            int length = pileArr.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    Pile pile = pileArr[i];
                    if (pile.isBelong(x, y) && isPenDownPile(pile)) {
                        pile_list.add(pile);
                        order_size = 0 + 1;
                        break;
                    }
                    i++;
                } else {
                    break;
                }
            }
            if (pile_list.size() != 0 || !isPenDownPackOpen() || !this.mPack.isAround(x, y)) {
                int[] order = new int[this.mPiles.length];
                for (Pile pile2 : this.mPiles) {
                    if (isPenDownPile(pile2)) {
                        if (x < pile2.mCardBounds.left) {
                            d = pile2.mCardBounds.left - x;
                        } else if (x >= pile2.mCardBounds.right) {
                            d = x - pile2.mCardBounds.right;
                        } else {
                            d = 0;
                        }
                        if (y < pile2.mCardBounds.top) {
                            d = Math.max(d, pile2.mCardBounds.top - y);
                        } else if (y >= pile2.mCardBounds.bottom) {
                            d = Math.max(d, y - pile2.mCardBounds.bottom);
                        }
                        if (d < this.mTouchArrea) {
                            int number = order_size;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= order_size) {
                                    break;
                                } else if (order[i2] > d) {
                                    number = i2;
                                    break;
                                } else {
                                    i2++;
                                }
                            }
                            if (number < order_size) {
                                pile_list.insertElementAt(pile2, number + 1);
                                for (int i3 = order_size; i3 > number; i3--) {
                                    order[i3] = order[i3 - 1];
                                }
                            } else {
                                pile_list.add(pile2);
                            }
                            order[number] = d;
                            order_size++;
                        }
                    }
                }
                if (pile_list.size() <= 0 && (isPenDownPackOpen() || !this.mPack.isAround(x, y))) {
                    return false;
                }
                PenDownAction(x, y, pile_list);
                return true;
            }
            ResetSelection();
            new Game.PackOpenCardsAction(new ResumeMoveAction(true), this.mMoves).run();
            return true;
        }
        ResetSelection();
        new Game.PackOpenCardsAction(new ResumeMoveAction(true), this.mMoves).run();
        return true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0011 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isVerticalAccept(int r3, int r4, int r5, int r6) {
        /*
            r2 = this;
            r1 = 1
            r0 = 2
            if (r5 <= r0) goto L_0x000d
            if (r3 <= r0) goto L_0x000c
            if (r6 > r4) goto L_0x000c
            if (r6 != r4) goto L_0x0011
            if (r5 <= r3) goto L_0x0011
        L_0x000c:
            return r1
        L_0x000d:
            if (r3 > r0) goto L_0x0011
            if (r6 > r4) goto L_0x000c
        L_0x0011:
            r1 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.GamePlay.isVerticalAccept(int, int, int, int):boolean");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0011 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean isHorizontalAccept(int r3, int r4, int r5, int r6) {
        /*
            r2 = this;
            r1 = 1
            r0 = 2
            if (r6 <= r0) goto L_0x000d
            if (r4 <= r0) goto L_0x000c
            if (r5 > r3) goto L_0x000c
            if (r5 != r3) goto L_0x0011
            if (r6 <= r4) goto L_0x0011
        L_0x000c:
            return r1
        L_0x000d:
            if (r4 > r0) goto L_0x0011
            if (r5 > r3) goto L_0x000c
        L_0x0011:
            r1 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.GamePlay.isHorizontalAccept(int, int, int, int):boolean");
    }

    /* access modifiers changed from: package-private */
    public final boolean isWinOrDeadend() {
        if (isWinFinish()) {
            CorrectAndRedrawIfNeed();
            WinMessage();
            return true;
        } else if (isAvailablePileMove() || this.mPack.isAvailableMove()) {
            return false;
        } else {
            CorrectAndRedrawIfNeed();
            if (!this.mEnableRedeal || this.RedealCurrent >= this.mRedealCount) {
                Utils.Question(this.mActivity, R.string.no_moves_start_question, -1, this.start_yes_listener);
            } else {
                Utils.Question(this.mActivity, R.string.no_moves_redeal_question, -1, this.redeal_yes_listener);
            }
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public boolean fastResumeMove(boolean collect) {
        boolean f_new_move = true;
        while (true) {
            boolean f_repeat = false;
            for (Pile pile : this.mPiles) {
                if (pile.OpenLastCard(this.mMoves)) {
                    addRedrawRect(pile.mCardBounds);
                }
            }
            if (this.mGameType != 1) {
                PileGroup[] pileGroupArr = this.mGroup;
                int length = pileGroupArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    PileGroup dst_group = pileGroupArr[i];
                    if (dst_group.mAddEmptyAuto) {
                        Pile[] pileArr = dst_group.mPile;
                        int length2 = pileArr.length;
                        for (int i2 = 0; i2 < length2; i2++) {
                            Pile dst_pile = pileArr[i2];
                            if (dst_pile.size() == 0) {
                                PileGroup[] pileGroupArr2 = this.mGroup;
                                int length3 = pileGroupArr2.length;
                                for (int i3 = 0; i3 < length3; i3++) {
                                    PileGroup src_group = pileGroupArr2[i3];
                                    if (src_group.mEmptySource) {
                                        Pile[] pileArr2 = src_group.mPile;
                                        int length4 = pileArr2.length;
                                        for (int i4 = 0; i4 < length4; i4++) {
                                            Pile src_pile = pileArr2[i4];
                                            if (src_pile != dst_pile && src_pile.size() > 0 && src_pile.isEnableRemove(src_pile.size() - 1, 1) && dst_pile.isEnableAdd(src_pile, 1, src_pile.lastElement())) {
                                                this.mMoves.addOneCardMove(src_pile, src_pile.size() - 1, dst_pile);
                                                dst_pile.add(src_pile.removeLast());
                                                this.mNeedCorrect = true;
                                                f_repeat = true;
                                                break;
                                            }
                                        }
                                        continue;
                                    }
                                }
                                continue;
                            }
                        }
                        continue;
                    }
                    i++;
                }
            }
            if (!f_repeat) {
                if (isWinOrDeadend()) {
                    return true;
                }
                boolean f_repeat2 = false;
                if (collect) {
                    PileGroup[] pileGroupArr3 = this.mGroup;
                    int length5 = pileGroupArr3.length;
                    int i5 = 0;
                    while (true) {
                        if (i5 >= length5) {
                            break;
                        }
                        PileGroup group = pileGroupArr3[i5];
                        if (group.mFoundation) {
                            if (group.Autoplay(this.mMoves, null, f_new_move)) {
                                f_new_move = false;
                                f_repeat2 = true;
                                break;
                            }
                        }
                        i5++;
                    }
                }
                if (!f_repeat2) {
                    return false;
                }
            }
        }
    }

    protected class ResumeMoveAction implements GameAction {
        private final boolean mCollect;
        private boolean mNewMove;
        private final GameAction mNextAction;

        ResumeMoveAction(boolean collect) {
            this.mNewMove = true;
            this.mNextAction = null;
            this.mCollect = (GamePlay.this.getAutoplayType() > 0) & collect;
        }

        ResumeMoveAction(GameAction next_action, boolean collect) {
            this.mNewMove = true;
            this.mNextAction = next_action;
            this.mCollect = (GamePlay.this.getAutoplayType() > 0) & collect;
        }

        public void fastRun() {
            if (!GamePlay.this.fastResumeMove(this.mCollect) && this.mNextAction != null) {
                this.mNextAction.fastRun();
            }
        }

        public void run() {
            for (Pile pile : GamePlay.this.mPiles) {
                if (pile.OpenLastCard(GamePlay.this.mMoves)) {
                    GamePlay.this.addRedrawRect(pile.mCardBounds);
                }
            }
            if (GamePlay.this.mGameType != 1) {
                for (PileGroup dst_group : GamePlay.this.mGroup) {
                    if (dst_group.mAddEmptyAuto) {
                        for (Pile dst_pile : dst_group.mPile) {
                            if (dst_pile.size() == 0) {
                                for (PileGroup src_group : GamePlay.this.mGroup) {
                                    if (src_group.mEmptySource) {
                                        Pile[] pileArr = src_group.mPile;
                                        int length = pileArr.length;
                                        int i = 0;
                                        while (true) {
                                            int i2 = i;
                                            if (i2 >= length) {
                                                continue;
                                                break;
                                            }
                                            Pile src_pile = pileArr[i2];
                                            if (src_pile == dst_pile || src_pile.size() <= 0 || !src_pile.isEnableRemove(src_pile.size() - 1, 1) || !dst_pile.isEnableAdd(src_pile, 1, src_pile.lastElement())) {
                                                i = i2 + 1;
                                            } else {
                                                GamePlay.this.mMoves.addOneCardMove(src_pile, src_pile.size() - 1, dst_pile);
                                                GamePlay.this.MoveCard(src_pile, src_pile.size() - 1, dst_pile, dst_pile.size(), true, this);
                                                return;
                                            }
                                        }
                                    }
                                }
                                continue;
                            }
                        }
                        continue;
                    }
                }
            }
            if (!GamePlay.this.isWinOrDeadend()) {
                if (this.mCollect) {
                    for (PileGroup group : GamePlay.this.mGroup) {
                        if (group.mFoundation) {
                            if (group.Autoplay(GamePlay.this.mMoves, this, this.mNewMove)) {
                                this.mNewMove = false;
                                return;
                            }
                        }
                    }
                }
                if (this.mNextAction != null) {
                    this.mNextAction.run();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isAvailablePileMove() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void CollectAll() {
    }

    private final void StoreState(BitStack stack) {
        stack.add((int) this.mCurrentTime, 10, 30);
        if (this.mEnableRedeal) {
            stack.add(this.RedealCurrent, 4);
        }
        this.mPack.StoreState(stack);
        for (Pile pile : this.mPiles) {
            pile.StoreState(stack);
        }
        if (this.mGameType == 1) {
            this.mTrash.StoreState(stack);
        }
        this.mMoves.Store(stack);
        stack.add(false);
        stack.add(0, 7);
        stack.add(0, 1);
    }

    private final void LoadState(BitStack stack) {
        this.mCurrentTime = (long) stack.getInt(10, 30);
        if (this.mEnableRedeal) {
            this.RedealCurrent = stack.getInt(4);
        }
        this.mPack.LoadState(stack);
        for (Pile pile : this.mPiles) {
            pile.LoadState(stack, this.mPack);
        }
        if (this.mGameType == 1) {
            this.mTrash.LoadState(stack, this.mPack);
        }
        this.mMoves.Load(stack);
        for (Pile pile2 : this.mPiles) {
            pile2.CorrectEmptyCard();
        }
        this.mStarted = true;
    }

    private class DealFinishAction implements GameAction {
        private DealFinishAction() {
        }

        /* synthetic */ DealFinishAction(GamePlay gamePlay, DealFinishAction dealFinishAction) {
            this();
        }

        public void fastRun() {
            GamePlay.this.mStarted = true;
            GamePlay.this.fTimePause = true;
            GamePlay.this.ResumeTime();
            new ResumeMoveAction(true).fastRun();
        }

        public void run() {
            GamePlay.this.mStarted = true;
            GamePlay.this.fTimePause = true;
            GamePlay.this.ResumeTime();
            new ResumeMoveAction(true).run();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean AvailableMoves() {
        this.mAvailableMovesMode = true;
        this.mAvailableButton[1].mEnabled = isNextAvailableMovesExist();
        setToolbarButtons(this.mAvailableButton);
        this.mView.invalidate();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void AvailableMovesFinish() {
        this.mAvailableMovesMode = false;
        setToolbarButtons(this.mGameToolbarButton);
        this.mView.invalidate();
    }

    /* access modifiers changed from: package-private */
    public void NoAvailableMovesMessage() {
        if (!this.mEnableRedeal || this.RedealCurrent >= this.mRedealCount) {
            if (this.mPack.isAvailableMove()) {
                Utils.Message(this.mActivity, R.string.only_stock_available, -1);
            } else {
                Utils.Question(this.mActivity, R.string.no_moves_start_question, -1, this.start_yes_listener);
            }
        } else if (this.mPack.isAvailableMove()) {
            Utils.Message(this.mActivity, R.string.only_stock_and_redeal_available, -1);
        } else {
            Utils.Question(this.mActivity, R.string.only_redeal_available, -1, this.redeal_yes_listener);
        }
    }

    private void ShowStatictics() {
        PauseTime();
        StatisticsDialog.showStatistics(this);
    }

    private void ShowHelp() {
    }

    private void WinMessage() {
        PauseTime();
        this.mStatistics.win(this, (int) (this.mCurrentTime / 1000));
        StatisticsDialog.showWimMessage(this);
    }

    public void doCommand(int command) {
        if (isEnableEvent()) {
            switch (command) {
                case Command.REDEAL:
                    RedealCommand();
                    break;
                case Command.START:
                    StartCommand();
                    break;
                case Command.RESTART:
                    RestartCommand();
                    break;
                case Command.STATISTICS:
                    ShowStatictics();
                    break;
                case Command.COLLECT:
                    CollectAll();
                    break;
                case Command.SET_BOOKMARK:
                    this.mMoves.setBookmark();
                    break;
                case Command.BACK_BOOKMARK:
                    this.mMoves.backToBookmark();
                    break;
                case Command.AVAILABLE:
                    AvailableMoves();
                    break;
                case Command.HELP:
                    ShowHelp();
                    break;
                case Command.DEMO:
                    Intent intent = new Intent(this.mActivity, DemoActivity.class);
                    putExtra(intent, getGameId(), getGameType(), getGameName(), this.mSource.mOffset, this.mSource.mRuleSize, this.mSource.mDemoSize, this.mSource.mHelpSize);
                    this.mActivity.startActivity(intent);
                    break;
                case Command.RULES:
                    RulesDialog.show(this.mActivity, getGameName(), this.mSource.mOffset + this.mSource.mRuleSize + this.mSource.mDemoSize, this.mSource.mHelpSize);
                    break;
                case Command.ABOUT:
                    Utils.About(this.mActivity);
                    break;
                case Command.AVAILABLE_BACK:
                    AvailableMovesFinish();
                    break;
                case Command.AVAILABLE_NEXT:
                    NextAvailableMoves();
                    this.mAvailableButton[1].mEnabled = isNextAvailableMovesExist();
                    if (!this.mAvailableButton[1].mEnabled) {
                        updateToolbar();
                        break;
                    }
                    break;
                case Command.UNDO:
                    this.mMoves.Undo();
                    break;
                case Command.REDO:
                    this.mMoves.Redo();
                    break;
                case Command.GAME_MENU:
                    PopupMenu menu = new PopupMenu(this.mActivity, this);
                    if (this.mEnableRedeal) {
                        menu.addItem(Command.REDEAL, R.string.redeal_menu_item, R.drawable.icon_redeal);
                    }
                    menu.addItem(Command.START, R.string.start_menu_item, R.drawable.icon_start);
                    menu.addItem(Command.RESTART, R.string.restart_menu_item, R.drawable.icon_restart);
                    menu.addItem(Command.STATISTICS, R.string.statistics, R.drawable.icon_statistics);
                    menu.setTitle((int) R.string.game_menu_item);
                    menu.show();
                    break;
                case Command.MOVE_MENU:
                    PopupMenu menu2 = new PopupMenu(this.mActivity, this);
                    if (this.mGameType == 0) {
                        menu2.addItem(Command.COLLECT, R.string.collect_menu_item, R.drawable.icon_collect);
                    }
                    menu2.addItem(Command.SET_BOOKMARK, R.string.set_bookmark_menu_item, R.drawable.icon_bookmark);
                    menu2.addItem(Command.BACK_BOOKMARK, R.string.back_bookmark_menu_item, R.drawable.icon_bookmark_back, this.mMoves.isBookmarkAvailable());
                    menu2.addItem(Command.AVAILABLE, R.string.available_moves_menu_item, R.drawable.icon_available_moves);
                    menu2.setTitle((int) R.string.move_menu_item);
                    menu2.show();
                    break;
                case Command.HELP_MENU:
                    PopupMenu menu3 = new PopupMenu(this.mActivity, this);
                    menu3.addItem(Command.DEMO, R.string.demo_menu_item, R.drawable.icon_demo);
                    menu3.addItem(Command.RULES, R.string.rules, R.drawable.icon_rules);
                    menu3.addItem(Command.ABOUT, R.string.about, R.drawable.icon_about);
                    menu3.setTitle((int) R.string.help_menu_item);
                    menu3.show();
                    break;
            }
            CorrectAndRedrawIfNeed();
        }
    }

    public void updateToolbar() {
        boolean invalidate = false;
        if (this.mMoves.isUndoAvailable()) {
            if (!this.mGameToolbarButton[0].mEnabled) {
                this.mGameToolbarButton[0].mEnabled = true;
                invalidate = true;
            }
        } else if (this.mGameToolbarButton[0].mEnabled) {
            this.mGameToolbarButton[0].mEnabled = false;
            invalidate = true;
        }
        if (this.mMoves.isRedoAvailable()) {
            if (!this.mGameToolbarButton[1].mEnabled) {
                this.mGameToolbarButton[1].mEnabled = true;
                invalidate = true;
            }
        } else if (this.mGameToolbarButton[1].mEnabled) {
            this.mGameToolbarButton[1].mEnabled = false;
            invalidate = true;
        }
        if (invalidate) {
            updateToolbar();
        }
    }
}
