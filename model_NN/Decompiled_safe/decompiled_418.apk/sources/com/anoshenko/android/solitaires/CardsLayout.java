package com.anoshenko.android.solitaires;

class CardsLayout {
    final int Align;
    final CoordinateY Bottom;
    final int Layout;
    final CoordinateX Left;
    final int LineSize;
    final CoordinateX Right;
    final CoordinateY Top;
    final boolean fCollumnLayout;
    final boolean fOptimalLineSize;

    CardsLayout(DataSource source) {
        this.Layout = source.mRule.getInt(2);
        this.Align = source.mRule.getInt(1, 2);
        this.fCollumnLayout = source.mRule.getFlag();
        this.LineSize = source.mRule.getInt(4);
        this.Top = new CoordinateY(source);
        this.Left = new CoordinateX(source);
        this.Bottom = new CoordinateY(source);
        this.Right = new CoordinateX(source);
        this.fOptimalLineSize = source.mRule.getFlag();
    }
}
