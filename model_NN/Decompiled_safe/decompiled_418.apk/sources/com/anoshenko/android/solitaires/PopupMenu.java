package com.anoshenko.android.solitaires;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.anoshenko.android.ui.CommandListener;
import java.util.Vector;

public class PopupMenu implements ListAdapter, AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public final Context mContext;
    private Vector<DataSetObserver> mDataSetObservers = new Vector<>();
    private AlertDialog mDialog;
    private boolean mHasDisabled = false;
    private Vector<PopupMenuItem> mItems = new Vector<>();
    private final CommandListener mListener;
    private String mTitle;

    private class PopupMenuItem {
        final int Command;
        final boolean Enabled;
        final Bitmap Icon;
        final String Text;

        PopupMenuItem(int command, int text_id, int icon_id, boolean enabled) {
            Resources res = PopupMenu.this.mContext.getResources();
            this.Command = command;
            this.Text = res.getString(text_id);
            this.Icon = Utils.loadBitmap(res, icon_id);
            this.Enabled = enabled;
        }
    }

    public PopupMenu(Context context, CommandListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    public void addItem(int command, int text_id, int icon_id) {
        this.mItems.add(new PopupMenuItem(command, text_id, icon_id, true));
    }

    public void addItem(int command, int text_id, int icon_id, boolean enabled) {
        this.mItems.add(new PopupMenuItem(command, text_id, icon_id, enabled));
        if (!enabled) {
            this.mHasDisabled = true;
        }
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public void setTitle(int title_id) {
        this.mTitle = this.mContext.getResources().getString(title_id);
    }

    public void show() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this.mContext);
        dialog.setCancelable(true);
        ListView list = new ListView(this.mContext);
        list.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        list.setAdapter((ListAdapter) this);
        list.setOnItemClickListener(this);
        dialog.setView(list);
        if (this.mTitle != null) {
            dialog.setTitle(this.mTitle);
        }
        this.mDialog = dialog.show();
        this.mDialog.setCanceledOnTouchOutside(true);
    }

    public boolean areAllItemsEnabled() {
        return !this.mHasDisabled;
    }

    public boolean isEnabled(int position) {
        return this.mItems.get(position).Enabled;
    }

    public int getCount() {
        return this.mItems.size();
    }

    public boolean isEmpty() {
        return getCount() == 0;
    }

    public Object getItem(int position) {
        return this.mItems.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public int getViewTypeCount() {
        return 1;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public View getView(int position, View view, ViewGroup group) {
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate((int) R.layout.menu_item, (ViewGroup) null);
        }
        PopupMenuItem item = this.mItems.get(position);
        ((ImageView) view.findViewById(R.id.MenuItemIcon)).setImageBitmap(item.Icon);
        TextView text = (TextView) view.findViewById(R.id.MenuItemText);
        text.setText(item.Text);
        text.setTextColor(item.Enabled ? -1 : -8355712);
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public void registerDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.add(observer);
    }

    public void unregisterDataSetObserver(DataSetObserver observer) {
        this.mDataSetObservers.remove(observer);
    }

    /* Debug info: failed to restart local var, previous not found, register: 3 */
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        try {
            this.mDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.mListener != null) {
            this.mListener.doCommand(this.mItems.get(position).Command);
        }
    }
}
