package com.anoshenko.android.solitaires;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.View;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.ui.ToolbarButton;

public class Demo extends Game {
    private static final int DELAY_STEP = 500;
    private static final int MAX_SPEED = 10;
    private static final String SPEED_KEY = "DemoSpeed";
    private static final String TAG = "Demo";
    /* access modifiers changed from: private */
    public ArrowDrawer mArrowDrawer;
    private final boolean mCardSeries;
    /* access modifiers changed from: private */
    public Pile mFromPile;
    /* access modifiers changed from: private */
    public int mMoveCount;
    /* access modifiers changed from: private */
    public int mMoveNumber;
    /* access modifiers changed from: private */
    public boolean mPause = false;
    private final ToolbarButton[] mPauseButton = new ToolbarButton[4];
    private PowerManager mPowerManager;
    private final ToolbarButton[] mResumeButton = new ToolbarButton[4];
    private AdditionalDraw mResumeDraw = null;
    /* access modifiers changed from: private */
    public Runnable mResumeRun = null;
    /* access modifiers changed from: private */
    public int mSpeed = 4;
    /* access modifiers changed from: private */
    public StepAction mStepAction = new StepAction(this, null);
    private final boolean mStoreNumber;
    /* access modifiers changed from: private */
    public Pile mToPile;
    /* access modifiers changed from: private */
    public Pile[] mTrashPiles;
    /* access modifiers changed from: private */
    public PowerManager.WakeLock mWakeLock;

    private enum StepType {
        STOP,
        MOVE,
        TRASH,
        PACK_OPEN,
        REDEAL
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    Demo(GameActivity activity, View view, DataSource source) {
        super(activity, view, source);
        this.mArrowDrawer = new ArrowDrawer(activity);
        this.mPowerManager = (PowerManager) activity.getSystemService("power");
        this.mSpeed = PreferenceManager.getDefaultSharedPreferences(this.mActivity).getInt(SPEED_KEY, 4);
        source.mDemo.ResetPos();
        boolean f_card_series = false;
        boolean f_store_number = false;
        for (PileGroup group : this.mGroup) {
            f_card_series = (group.mAddType > 1 || group.mRemoveType > 2) ? true : f_card_series;
            if (group.mRemoveType == 2) {
                f_store_number = true;
            }
        }
        this.mCardSeries = f_card_series;
        this.mStoreNumber = f_store_number;
        byte[] start_pack = this.mPack.getStartPack();
        for (int i = 0; i < this.mPack.getMaxSize(); i++) {
            start_pack[i] = (byte) (this.mSource.mDemo.getInt(2) << 4);
            start_pack[i] = (byte) (start_pack[i] | ((byte) this.mSource.mDemo.getInt(4)));
        }
    }

    public void initToolbar() {
        boolean z;
        boolean z2;
        this.mPauseButton[0] = new ToolbarButton(this.mActivity, R.drawable.icon_stop, R.drawable.icon_stop, R.string.back_button, Command.DEMO_BACK);
        this.mPauseButton[1] = new ToolbarButton(this.mActivity, R.drawable.icon_pause, R.drawable.icon_pause, R.string.pause_button, Command.DEMO_PAUSE);
        this.mPauseButton[2] = new ToolbarButton(this.mActivity, R.drawable.icon_slow, R.drawable.icon_slow_disable, R.string.slowly_button, Command.DEMO_SLOWLY);
        this.mPauseButton[3] = new ToolbarButton(this.mActivity, R.drawable.icon_fast, R.drawable.icon_fast_disable, R.string.fast_button, Command.DEMO_FAST);
        this.mResumeButton[0] = this.mPauseButton[0];
        this.mResumeButton[1] = new ToolbarButton(this.mActivity, R.drawable.icon_resume, R.drawable.icon_resume, R.string.resume_button, Command.DEMO_RESUME);
        this.mResumeButton[2] = this.mPauseButton[2];
        this.mResumeButton[3] = this.mPauseButton[3];
        ToolbarButton toolbarButton = this.mResumeButton[2];
        if (this.mSpeed < 10) {
            z = true;
        } else {
            z = false;
        }
        toolbarButton.mEnabled = z;
        ToolbarButton toolbarButton2 = this.mResumeButton[3];
        if (this.mSpeed > 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        toolbarButton2.mEnabled = z2;
        setToolbarButtons(this.mPauseButton);
    }

    private class StepAction implements GameAction {
        private static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$solitaires$Demo$StepType;

        static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$solitaires$Demo$StepType() {
            int[] iArr = $SWITCH_TABLE$com$anoshenko$android$solitaires$Demo$StepType;
            if (iArr == null) {
                iArr = new int[StepType.values().length];
                try {
                    iArr[StepType.MOVE.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[StepType.PACK_OPEN.ordinal()] = 4;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[StepType.REDEAL.ordinal()] = 5;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[StepType.STOP.ordinal()] = 1;
                } catch (NoSuchFieldError e4) {
                }
                try {
                    iArr[StepType.TRASH.ordinal()] = 3;
                } catch (NoSuchFieldError e5) {
                }
                $SWITCH_TABLE$com$anoshenko$android$solitaires$Demo$StepType = iArr;
            }
            return iArr;
        }

        private StepAction() {
        }

        /* synthetic */ StepAction(Demo demo, StepAction stepAction) {
            this();
        }

        public void fastRun() {
            run();
        }

        public void run() {
            if (Demo.this.mPause) {
                Demo.this.mResumeRun = this;
            } else if (Demo.this.mStepAction == this) {
                try {
                    StepType stepType = Demo.this.loadStepType();
                    while (stepType != StepType.STOP) {
                        switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$Demo$StepType()[stepType.ordinal()]) {
                            case 2:
                                Demo.this.cardMoveStep();
                                return;
                            case 3:
                                Demo.this.trashStep();
                                return;
                            case 4:
                                Demo.this.packOpenStep();
                                return;
                            case 5:
                                Demo.this.redealStep();
                                return;
                            default:
                                stepType = Demo.this.loadStepType();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (Demo.this.mWakeLock != null) {
                    Demo.this.mWakeLock.release();
                    Demo.this.mWakeLock = null;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void start() {
        setScreenSize(this.mView.getWidth(), this.mView.getHeight());
        DealStart(false, this.mPack.getStartPack(), null);
        for (Pile pile : this.mPiles) {
            pile.OpenLastCard(null);
        }
        this.mView.invalidate();
        if (this.mWakeLock == null) {
            this.mWakeLock = this.mPowerManager.newWakeLock(10, TAG);
            this.mWakeLock.acquire();
        }
        if (this.mStepAction != null) {
            this.mStepAction.run();
        }
    }

    private final class DelayedRun implements Runnable {
        private final Runnable mRun;
        private int n = 0;

        public DelayedRun(Runnable run) {
            this.mRun = run;
        }

        public void run() {
            if (Demo.this.mPause) {
                Demo.this.mResumeRun = this;
            } else if (this.n <= Demo.this.mSpeed) {
                this.n++;
                if (Demo.this.mStepAction != null) {
                    Demo.this.mView.postDelayed(this, 500);
                }
            } else {
                Demo.this.mView.post(this.mRun);
            }
        }
    }

    /* access modifiers changed from: private */
    public void cardMoveStep() {
        Card card = this.mFromPile.getCard(this.mMoveNumber);
        for (int i = 0; i < this.mMoveCount; i++) {
            card.mMark = true;
            card = card.next;
        }
        this.mDraw = new ArrowDraw(this, null);
        this.mView.invalidate();
        this.mView.post(new DelayedRun(new Runnable() {
            public void run() {
                if (Demo.this.mPause) {
                    Demo.this.mResumeRun = this;
                    return;
                }
                Demo.this.mDraw = null;
                Demo.this.addRedrawRect(Demo.this.mScreen);
                if (Demo.this.mMoveCount == 1) {
                    Demo.this.MoveCard(Demo.this.mFromPile, Demo.this.mMoveNumber, Demo.this.mToPile, Demo.this.mToPile.size(), true, Demo.this.mStepAction);
                } else {
                    Demo.this.MoveCards(Demo.this.mFromPile, Demo.this.mToPile, Demo.this.mMoveCount, Demo.this.mStepAction);
                }
            }
        }));
    }

    /* access modifiers changed from: private */
    public void trashStep() {
        for (Pile lastElement : this.mTrashPiles) {
            lastElement.lastElement().mMark = true;
        }
        this.mView.invalidate();
        this.mView.post(new DelayedRun(new Runnable() {
            public void run() {
                if (Demo.this.mPause) {
                    Demo.this.mResumeRun = this;
                    return;
                }
                for (int i = 0; i < Demo.this.mTrashPiles.length; i++) {
                    Demo.this.mTrash.add(Demo.this.mTrashPiles[i].removeLast());
                    Demo.this.mTrashPiles[i].Correct();
                }
                Demo.this.mTrashPiles = null;
                Demo.this.mView.invalidate();
                if (Demo.this.mStepAction != null) {
                    Demo.this.mStepAction.run();
                }
            }
        }));
    }

    /* access modifiers changed from: private */
    public void packOpenStep() {
        this.mDraw = new PackMarkDraw(this, null);
        this.mView.invalidate();
        this.mView.post(new DelayedRun(new Runnable() {
            public void run() {
                if (Demo.this.mPause) {
                    Demo.this.mResumeRun = this;
                    return;
                }
                Demo.this.mDraw = null;
                Demo.this.addRedrawRect(Demo.this.mScreen);
                new Game.PackOpenCardsAction(Demo.this.mStepAction, null).run();
            }
        }));
    }

    /* access modifiers changed from: private */
    public void redealStep() {
        this.mDraw = new MessageDraw(R.string.redeal_menu_item);
        this.mView.invalidate();
        this.mView.post(new DelayedRun(new Runnable() {
            public void run() {
                if (Demo.this.mPause) {
                    Demo.this.mResumeRun = this;
                    return;
                }
                Demo.this.mDraw = null;
                int count = Demo.this.mSource.mDemo.getInt(8);
                byte[] start_pack = Demo.this.mPack.getStartPack();
                for (int i = 0; i < count; i++) {
                    start_pack[i] = (byte) (Demo.this.mSource.mDemo.getInt(2) << 4);
                    start_pack[i] = (byte) (start_pack[i] | ((byte) Demo.this.mSource.mDemo.getInt(4)));
                }
                if (count > 0) {
                    Demo.this.RedealStart(false, start_pack, Demo.this.mStepAction);
                }
                Demo.this.mView.invalidate();
            }
        }));
    }

    private void stopDemo() {
        this.mDraw = new MessageDraw(R.string.demo_completed);
        this.mView.invalidate();
    }

    /* access modifiers changed from: package-private */
    public StepType loadStepType() {
        boolean f_repeat;
        StepType result = StepType.STOP;
        try {
            this.mRedrawRect = null;
            for (Pile pile : this.mPiles) {
                if (pile.size() > 0 && !pile.lastElement().mOpen && pile.OpenLastCard(null)) {
                    addRedrawRect(pile.mCardBounds);
                }
            }
            do {
                f_repeat = false;
                if (this.mSource.mDemo.getFlag()) {
                    switch (this.mSource.mDemo.getInt(2)) {
                        case 0:
                            Pile pile2 = loadDemoPile();
                            if (pile2 == null || pile2.size() == 0) {
                                stopDemo();
                                return StepType.STOP;
                            }
                            pile2.lastElement().mOpen = true;
                            addRedrawRect(pile2.lastElement().getRect());
                            f_repeat = true;
                            continue;
                        case 1:
                            if (!this.mPack.isAvailable()) {
                                stopDemo();
                                return StepType.STOP;
                            }
                            result = StepType.PACK_OPEN;
                            continue;
                        case 2:
                            if (!this.mEnableRedeal || this.RedealCurrent > this.mRedealCount) {
                                stopDemo();
                                return StepType.STOP;
                            }
                            result = StepType.REDEAL;
                            continue;
                        case 3:
                            stopDemo();
                            return StepType.STOP;
                    }
                } else if (this.mGameType == 1) {
                    this.mTrashPiles = new Pile[(this.mSource.mDemo.getInt(4, 8) + 1)];
                    for (int i = 0; i < this.mTrashPiles.length; i++) {
                        this.mTrashPiles[i] = loadDemoPile();
                        if (this.mTrashPiles[i] == null) {
                            stopDemo();
                            return StepType.STOP;
                        }
                    }
                    if (!this.mTrashRule.TestTrashPiles(this.mTrashPiles, this.mTrashPiles.length)) {
                        stopDemo();
                        return StepType.STOP;
                    }
                    result = StepType.TRASH;
                    continue;
                } else {
                    this.mFromPile = loadDemoPile();
                    this.mToPile = loadDemoPile();
                    if (this.mFromPile == null || this.mToPile == null) {
                        stopDemo();
                        return StepType.STOP;
                    }
                    if (this.mCardSeries) {
                        if (this.mSource.mDemo.getFlag()) {
                            this.mMoveCount = this.mSource.mDemo.getInt(9);
                        } else {
                            this.mMoveCount = this.mSource.mDemo.getInt(4);
                        }
                        this.mMoveCount++;
                    } else {
                        this.mMoveCount = 1;
                    }
                    if (!this.mStoreNumber || this.mMoveCount != 1) {
                        this.mMoveNumber = this.mFromPile.size() - this.mMoveCount;
                    } else if (this.mSource.mDemo.getFlag()) {
                        this.mMoveNumber = this.mSource.mDemo.getInt(9);
                    } else {
                        this.mMoveNumber = this.mSource.mDemo.getInt(4);
                    }
                    if (this.mMoveNumber < 0 || this.mMoveNumber + this.mMoveCount > this.mFromPile.size() || !this.mFromPile.isEnableRemove(this.mMoveNumber, this.mMoveCount) || !this.mToPile.isEnableAdd(this.mFromPile, this.mMoveCount, this.mFromPile.getCard(this.mMoveNumber))) {
                        stopDemo();
                        return StepType.STOP;
                    }
                    result = StepType.MOVE;
                    continue;
                }
            } while (f_repeat);
            if (this.mRedrawRect != null) {
                CorrectEmpty();
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            stopDemo();
            return StepType.STOP;
        }
    }

    private Pile loadDemoPile() {
        int index;
        int number;
        if (this.mGroup.length > 1) {
            index = this.mSource.mDemo.getInt(this.mIndexSize);
        } else {
            index = 0;
        }
        if (index >= this.mGroup.length) {
            return null;
        }
        if (this.mGroup[index].mNumberSize > 0) {
            number = this.mSource.mDemo.getInt(this.mGroup[index].mNumberSize);
        } else {
            number = 0;
        }
        if (number < this.mGroup[index].mPile.length) {
            return this.mGroup[index].mPile[number];
        }
        return null;
    }

    private void CorrectEmpty() {
        for (Pile pile : this.mPiles) {
            if (pile.size() == 0 && pile.CorrectEmptyCard()) {
                addRedrawRect(pile.mCardBounds);
            }
        }
        CorrectAndRedrawIfNeed();
    }

    private void setSpeed(int speed) {
        this.mSpeed = speed;
        boolean invalidate_toolbar = false;
        if (this.mSpeed >= 10) {
            if (this.mPauseButton[2] != null) {
                this.mPauseButton[2].mEnabled = false;
                invalidate_toolbar = true;
            }
        } else if (this.mSpeed == 1 && this.mPauseButton[3] != null) {
            this.mPauseButton[3].mEnabled = false;
            invalidate_toolbar = true;
        }
        if (this.mSpeed < 10 && this.mPauseButton[2] != null && !this.mPauseButton[2].mEnabled) {
            this.mPauseButton[2].mEnabled = true;
            invalidate_toolbar = true;
        }
        if (this.mSpeed > 1 && this.mPauseButton[3] != null && !this.mPauseButton[3].mEnabled) {
            this.mPauseButton[3].mEnabled = true;
            invalidate_toolbar = true;
        }
        if (invalidate_toolbar) {
            updateToolbar();
        }
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this.mActivity).edit();
        editor.putInt(SPEED_KEY, speed);
        editor.commit();
    }

    public void Terminate() {
        if (this.mWakeLock != null) {
            this.mWakeLock.release();
            this.mWakeLock = null;
        }
        this.mStepAction = null;
        this.mActivity.finish();
    }

    public boolean isAnimation() {
        return false;
    }

    public boolean isDealAnimation() {
        return false;
    }

    public void doCommand(int command) {
        switch (command) {
            case Command.DEMO_BACK:
                Terminate();
                return;
            case Command.DEMO_PAUSE:
                this.mResumeDraw = this.mDraw;
                this.mDraw = new MessageDraw(R.string.pause_button);
                this.mPause = true;
                setToolbarButtons(this.mResumeButton);
                this.mView.invalidate();
                return;
            case Command.DEMO_RESUME:
                this.mDraw = this.mResumeDraw;
                this.mPause = false;
                if (this.mResumeRun != null) {
                    this.mView.post(this.mResumeRun);
                }
                setToolbarButtons(this.mPauseButton);
                this.mView.invalidate();
                return;
            case Command.DEMO_SLOWLY:
                if (this.mSpeed < 10) {
                    setSpeed(this.mSpeed + 1);
                    return;
                }
                return;
            case Command.DEMO_FAST:
                if (this.mSpeed > 1) {
                    setSpeed(this.mSpeed - 1);
                    return;
                }
                return;
            default:
                return;
        }
    }

    private class ArrowDraw implements AdditionalDraw {
        private ArrowDraw() {
        }

        /* synthetic */ ArrowDraw(Demo demo, ArrowDraw arrowDraw) {
            this();
        }

        public void draw(Canvas g) {
            int end_x;
            int end_y;
            Card card = Demo.this.mFromPile.getCard(Demo.this.mMoveNumber);
            if (card != null) {
                int start_x = card.xPos + (Demo.this.mCardData.Width / 2);
                int start_y = card.yPos + (Demo.this.mCardData.Height / 2);
                switch (card.mNextOffset) {
                    case 1:
                        start_y = card.yPos + (Demo.this.mCardData.yOffset / 2);
                        break;
                    case 2:
                        start_y = (card.yPos + Demo.this.mCardData.Height) - (Demo.this.mCardData.yOffset / 2);
                        break;
                    case 3:
                        start_x = card.xPos + (Demo.this.mCardData.xOffset / 2);
                        break;
                    case 4:
                        start_x = (card.xPos + Demo.this.mCardData.Width) - (Demo.this.mCardData.xOffset / 2);
                        break;
                }
                if (Demo.this.mToPile.size() > 0) {
                    end_x = Demo.this.mToPile.lastElement().xPos;
                    end_y = Demo.this.mToPile.lastElement().yPos;
                } else {
                    end_x = Demo.this.mToPile.xPos;
                    end_y = Demo.this.mToPile.yPos;
                }
                Demo.this.mArrowDrawer.draw(g, start_x, start_y, end_x + (Demo.this.mCardData.Width / 2), end_y + (Demo.this.mCardData.Height / 2));
            }
        }
    }

    private class MessageDraw implements AdditionalDraw {
        private final String mMessage;

        MessageDraw(int message_id) {
            this.mMessage = Demo.this.mActivity.getString(message_id);
        }

        public void draw(Canvas g) {
            Paint paint = new Paint();
            int min_size = Utils.getMinDislpaySide(Demo.this.mActivity);
            if (min_size >= 480) {
                paint.setTextSize(26.0f);
            } else if (min_size < 320) {
                paint.setTextSize(14.0f);
            } else {
                paint.setTextSize(20.0f);
            }
            paint.setAntiAlias(true);
            Rect text_rect = new Rect();
            paint.getTextBounds(this.mMessage, 0, this.mMessage.length(), text_rect);
            int text_height = text_rect.height();
            int width = Math.min(Demo.this.mScreen.width(), text_rect.width() + (text_height * 2));
            int x = (Demo.this.mScreen.width() - width) / 2;
            int y = (Demo.this.mScreen.height() - (text_height * 3)) / 2;
            RectF rect = new RectF((float) x, (float) y, (float) (x + width), (float) ((text_height * 3) + y));
            paint.setStyle(Paint.Style.FILL);
            paint.setColor(-1073741824);
            g.drawRoundRect(rect, 4.0f, 4.0f, paint);
            paint.setColor(-1);
            g.drawText(this.mMessage, (float) (((width - text_rect.width()) / 2) + x), (float) ((text_height * 2) + y), paint);
            paint.setStrokeWidth(3.0f);
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(-16777216);
            g.drawRoundRect(rect, 4.0f, 4.0f, paint);
        }
    }

    private class PackMarkDraw implements AdditionalDraw {
        private PackMarkDraw() {
        }

        /* synthetic */ PackMarkDraw(Demo demo, PackMarkDraw packMarkDraw) {
            this();
        }

        public void draw(Canvas g) {
            g.drawOval(new RectF((float) (Demo.this.mPack.mBounds.left - 4), (float) (Demo.this.mPack.mBounds.top - 4), (float) (Demo.this.mPack.mBounds.right + 4), (float) (Demo.this.mPack.mBounds.bottom + 4)), Demo.this.mArrowDrawer.getPaint());
        }
    }
}
