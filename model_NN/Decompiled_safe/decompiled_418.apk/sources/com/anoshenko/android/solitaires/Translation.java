package com.anoshenko.android.solitaires;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

public class Translation {

    private static class TranslateButtonListener implements DialogInterface.OnClickListener {
        private final Context mContext;

        TranslateButtonListener(Context context) {
            this.mContext = context;
        }

        public void onClick(DialogInterface dialog, int which) {
            Translation.startTranslationPage(this.mContext);
        }
    }

    public static void show(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage((int) R.string.translate_message);
        builder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        builder.setPositiveButton((int) R.string.translate, new TranslateButtonListener(context));
        builder.show();
    }

    public static void startTranslationPage(Context context) {
        context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://alx-soft.com/translation.html")));
    }
}
