package com.anoshenko.android.solitaires;

abstract class Condition {
    /* access modifiers changed from: package-private */
    public abstract boolean Examine(int i, int i2, PileGroup pileGroup);

    Condition() {
    }

    /* access modifiers changed from: package-private */
    public int getMaxSize(Pile pile) {
        return 65535;
    }

    /* access modifiers changed from: package-private */
    public boolean isMaxSizeCondition(Pile pile) {
        return false;
    }

    static final Condition Load(Game game) {
        int count = game.mSource.mRule.getInt(1, 3) + 1;
        switch (count) {
            case 0:
                return new ConditionEmpty();
            case 1:
                int count2 = game.mSource.mRule.getInt(3, 5) + 1;
                switch (count2) {
                    case 0:
                        return new ConditionEmpty();
                    case 1:
                        return new ConditionElement(game);
                    default:
                        return new ConditionGroup(game, count2, false);
                }
            default:
                return new ConditionGroup(game, count, true);
        }
    }
}
