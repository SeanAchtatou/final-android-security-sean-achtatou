package com.anoshenko.android.solitaires;

import android.graphics.Canvas;
import android.graphics.Rect;
import java.util.Iterator;
import java.util.Vector;

public final class Pile extends CardList {
    static final int ADD_DISABLE = 0;
    static final int ADD_ONE_ONLY = 1;
    static final int ADD_ONE_OR_SERIES = 2;
    static final int ADD_SERIES_ONLY = 3;
    static final int BOTTOM_UP = 1;
    static final int LEFT_TO_RIGHT = 2;
    static final int REMOVE_ANY_ONE = 2;
    static final int REMOVE_DISABLE = 0;
    static final int REMOVE_LAST_ONLY = 1;
    static final int REMOVE_ONE_OR_SERIES = 3;
    static final int REMOVE_SERIES_ONLY = 4;
    static final int RIGHT_TO_LEFT = 3;
    static final int TOP_DOWN = 0;
    final Rect mBounds = new Rect();
    public final Rect mCardBounds = new Rect();
    final CloseRegion mCloseRegion;
    private boolean mDealComplete;
    int mEmptyCard;
    boolean mExpandable;
    final int mFixedSuit;
    final int mFixedValue;
    int mLetter = -1;
    final int mNumber;
    public final PileGroup mOwner;
    final int mStartSize;
    int xEmptyCenter;
    int xPos;
    int yEmptyCenter;
    int yPos;

    Pile(int number, PileGroup group) {
        int i;
        CloseRegion closeRegion;
        this.mOwner = group;
        this.mNumber = number;
        BitStack stack = group.mGame.mSource.mRule;
        if (this.mOwner.mFixedCardFlag) {
            this.mFixedSuit = stack.getInt(2);
            this.mFixedValue = stack.getInt(4);
        } else {
            this.mFixedSuit = 0;
            this.mFixedValue = 0;
        }
        if (this.mOwner.mStartType == 1) {
            i = stack.getIntF(4, 13);
        } else {
            i = this.mOwner.mStartSize;
        }
        this.mStartSize = i;
        if (this.mOwner.mStartOpen == 4) {
            closeRegion = new CloseRegion(stack);
        } else {
            closeRegion = this.mOwner.mCloseRegion;
        }
        this.mCloseRegion = closeRegion;
    }

    public void Correct() {
        if (this.mBounds.width() != 0 && this.mBounds.height() != 0 && this.mOwner.mGame.mCardData != null) {
            this.mExpandable = false;
            if (this.mOwner.getCardLayout() < 2) {
                VerticalCorrect();
            } else {
                HorizontalCorrect();
            }
            getBounds(this.mCardBounds);
        }
    }

    private void CorrectVisibleType1() {
        int x = this.xPos;
        int y = this.yPos;
        int size = size();
        Card card = firstElement();
        for (int i = 1; i < size && card != null; i++) {
            card.xPos = x;
            card.yPos = y;
            card.mNextOffset = -1;
            this.mExpandable = true;
            card = card.next;
        }
        if (card != null) {
            card.xPos = x;
            card.yPos = y;
            card.mNextOffset = 0;
        }
    }

    private void CorrectVisibleType2(int off_x, int off_y, int next_offset, int visible_count) {
        int x = this.xPos;
        int y = this.yPos;
        int size = size();
        Card card = firstElement();
        int i = 0;
        while (i < size - visible_count && card != null) {
            card.xPos = x;
            card.yPos = y;
            card.mNextOffset = -1;
            this.mExpandable = true;
            card = card.next;
            i++;
        }
        while (i < size - 1 && card != null) {
            card.xPos = x;
            card.yPos = y;
            card.mNextOffset = next_offset;
            x += off_x;
            y += off_y;
            card = card.next;
            i++;
        }
        if (card != null) {
            card.xPos = x;
            card.yPos = y;
            card.mNextOffset = 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x01bf  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void HorizontalCorrect() {
        /*
            r29 = this;
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            com.anoshenko.android.solitaires.Game r0 = r0.mGame
            r24 = r0
            r0 = r24
            com.anoshenko.android.solitaires.CardData r0 = r0.mCardData
            r12 = r0
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            com.anoshenko.android.solitaires.Game r0 = r0.mGame
            r24 = r0
            r0 = r24
            int r0 = r0.mScreenType
            r24 = r0
            r25 = 1
            r0 = r24
            r1 = r25
            if (r0 != r1) goto L_0x0059
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            com.anoshenko.android.solitaires.CardsLayout r0 = r0.mLandscapeLayout
            r24 = r0
            r16 = r24
        L_0x0039:
            int r7 = r12.Width
            int r6 = r12.Height
            int r8 = r12.xOffset
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            com.anoshenko.android.solitaires.Game r0 = r0.mGame
            r24 = r0
            boolean r14 = r24.isMirrorLayout()
            r0 = r16
            int r0 = r0.Layout
            r24 = r0
            switch(r24) {
                case 2: goto L_0x0068;
                case 3: goto L_0x01d8;
                default: goto L_0x0058;
            }
        L_0x0058:
            return
        L_0x0059:
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            com.anoshenko.android.solitaires.CardsLayout r0 = r0.mLayout
            r24 = r0
            r16 = r24
            goto L_0x0039
        L_0x0068:
            if (r14 == 0) goto L_0x01d2
            r24 = 3
            r17 = r24
        L_0x006e:
            switch(r17) {
                case 2: goto L_0x0072;
                case 3: goto L_0x01e5;
                default: goto L_0x0071;
            }
        L_0x0071:
            goto L_0x0058
        L_0x0072:
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            r0 = r24
            int r0 = r0.left
            r24 = r0
            r0 = r24
            r1 = r29
            r1.xPos = r0
            r19 = r8
            r18 = 3
        L_0x0088:
            r0 = r16
            int r0 = r0.Align
            r24 = r0
            switch(r24) {
                case 1: goto L_0x0201;
                case 2: goto L_0x0217;
                default: goto L_0x0091;
            }
        L_0x0091:
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            r0 = r24
            int r0 = r0.top
            r24 = r0
            r0 = r24
            r1 = r29
            r1.yPos = r0
        L_0x00a3:
            r0 = r29
            android.graphics.Rect r0 = r0.mCardBounds
            r24 = r0
            r0 = r29
            int r0 = r0.xPos
            r25 = r0
            r0 = r29
            int r0 = r0.yPos
            r26 = r0
            r0 = r29
            int r0 = r0.xPos
            r27 = r0
            int r27 = r27 + r7
            r0 = r29
            int r0 = r0.yPos
            r28 = r0
            int r28 = r28 + r6
            r24.set(r25, r26, r27, r28)
            r0 = r29
            int r0 = r0.xPos
            r24 = r0
            int r25 = r7 / 2
            int r24 = r24 + r25
            r0 = r24
            r1 = r29
            r1.xEmptyCenter = r0
            r0 = r29
            int r0 = r0.yPos
            r24 = r0
            int r25 = r6 / 2
            int r24 = r24 + r25
            r0 = r24
            r1 = r29
            r1.yEmptyCenter = r0
            int r20 = r29.size()
            switch(r20) {
                case 0: goto L_0x0058;
                case 1: goto L_0x023b;
                default: goto L_0x00ef;
            }
        L_0x00ef:
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            int r0 = r0.mVisible
            r24 = r0
            switch(r24) {
                case 1: goto L_0x0269;
                case 2: goto L_0x026e;
                default: goto L_0x00fe;
            }
        L_0x00fe:
            r0 = r29
            int r0 = r0.xPos
            r22 = r0
            r0 = r29
            int r0 = r0.yPos
            r23 = r0
            r9 = 0
            com.anoshenko.android.solitaires.Card r5 = r29.firstElement()
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            int r0 = r0.mStartOpen
            r24 = r0
            r25 = 3
            r0 = r24
            r1 = r25
            if (r0 >= r1) goto L_0x028f
            r0 = r29
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r24 = r0
            r0 = r24
            int r0 = r0.mVisible
            r24 = r0
            if (r24 != 0) goto L_0x028f
            r24 = 1
            r13 = r24
        L_0x0135:
            if (r13 == 0) goto L_0x013f
            r15 = 1
        L_0x0138:
            r0 = r15
            r1 = r20
            if (r0 >= r1) goto L_0x013f
            if (r5 != 0) goto L_0x0295
        L_0x013f:
            int r10 = r19 / 3
            int r24 = r9 * r8
            int r11 = r24 / 3
            int r24 = r20 - r9
            r25 = 1
            int r24 = r24 - r25
            int r24 = r24 * r8
            int r24 = r24 + r11
            int r21 = r24 + r7
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            int r24 = r24.width()
            r0 = r21
            r1 = r24
            if (r0 <= r1) goto L_0x01ab
            int r10 = r8 / 3
            if (r9 <= 0) goto L_0x02a4
            r24 = 1
            int r24 = r20 - r24
            r0 = r9
            r1 = r24
            if (r0 >= r1) goto L_0x02a4
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            int r24 = r24.width()
            int r24 = r24 - r7
            int r24 = r24 - r11
            int r25 = r20 - r9
            r26 = 1
            int r25 = r25 - r26
            int r19 = r24 / r25
            r0 = r19
            r1 = r10
            if (r0 >= r1) goto L_0x019d
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            int r24 = r24.width()
            int r24 = r24 - r7
            r25 = 1
            int r25 = r20 - r25
            int r19 = r24 / r25
            r10 = r19
        L_0x019d:
            r24 = 3
            r0 = r17
            r1 = r24
            if (r0 != r1) goto L_0x01ab
            int r10 = -r10
            r0 = r19
            int r0 = -r0
            r19 = r0
        L_0x01ab:
            com.anoshenko.android.solitaires.Card r5 = r29.firstElement()
            r15 = 0
        L_0x01b0:
            if (r15 >= r9) goto L_0x01b4
            if (r5 != 0) goto L_0x02c0
        L_0x01b4:
            int r15 = r15 + 1
        L_0x01b6:
            r0 = r15
            r1 = r20
            if (r0 >= r1) goto L_0x01bd
            if (r5 != 0) goto L_0x02d7
        L_0x01bd:
            if (r5 == 0) goto L_0x0058
            r0 = r22
            r1 = r5
            r1.xPos = r0
            r0 = r23
            r1 = r5
            r1.yPos = r0
            r24 = 0
            r0 = r24
            r1 = r5
            r1.mNextOffset = r0
            goto L_0x0058
        L_0x01d2:
            r24 = 2
            r17 = r24
            goto L_0x006e
        L_0x01d8:
            if (r14 == 0) goto L_0x01e0
            r24 = 2
            r17 = r24
        L_0x01de:
            goto L_0x006e
        L_0x01e0:
            r24 = 3
            r17 = r24
            goto L_0x01de
        L_0x01e5:
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            r0 = r24
            int r0 = r0.right
            r24 = r0
            int r24 = r24 - r7
            r0 = r24
            r1 = r29
            r1.xPos = r0
            r0 = r8
            int r0 = -r0
            r19 = r0
            r18 = 4
            goto L_0x0088
        L_0x0201:
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            r0 = r24
            int r0 = r0.bottom
            r24 = r0
            int r24 = r24 - r6
            r0 = r24
            r1 = r29
            r1.yPos = r0
            goto L_0x00a3
        L_0x0217:
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            r0 = r24
            int r0 = r0.top
            r24 = r0
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r25 = r0
            int r25 = r25.height()
            int r25 = r25 - r6
            int r25 = r25 / 2
            int r24 = r24 + r25
            r0 = r24
            r1 = r29
            r1.yPos = r0
            goto L_0x00a3
        L_0x023b:
            com.anoshenko.android.solitaires.Card r24 = r29.firstElement()
            r0 = r29
            int r0 = r0.xPos
            r25 = r0
            r0 = r25
            r1 = r24
            r1.xPos = r0
            com.anoshenko.android.solitaires.Card r24 = r29.firstElement()
            r0 = r29
            int r0 = r0.yPos
            r25 = r0
            r0 = r25
            r1 = r24
            r1.yPos = r0
            com.anoshenko.android.solitaires.Card r24 = r29.firstElement()
            r25 = 0
            r0 = r25
            r1 = r24
            r1.mNextOffset = r0
            goto L_0x0058
        L_0x0269:
            r29.CorrectVisibleType1()
            goto L_0x0058
        L_0x026e:
            r24 = 0
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r25 = r0
            int r25 = r25.width()
            int r25 = r25 - r7
            int r25 = r25 / r8
            int r25 = r25 + 1
            r0 = r29
            r1 = r19
            r2 = r24
            r3 = r18
            r4 = r25
            r0.CorrectVisibleType2(r1, r2, r3, r4)
            goto L_0x0058
        L_0x028f:
            r24 = 0
            r13 = r24
            goto L_0x0135
        L_0x0295:
            r0 = r5
            boolean r0 = r0.mOpen
            r24 = r0
            if (r24 != 0) goto L_0x013f
            int r9 = r9 + 1
            com.anoshenko.android.solitaires.Card r5 = r5.next
            int r15 = r15 + 1
            goto L_0x0138
        L_0x02a4:
            r0 = r29
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            int r24 = r24.width()
            int r24 = r24 - r7
            r25 = 1
            int r25 = r20 - r25
            int r19 = r24 / r25
            r24 = 1
            r0 = r24
            r1 = r29
            r1.mExpandable = r0
            goto L_0x019d
        L_0x02c0:
            r0 = r22
            r1 = r5
            r1.xPos = r0
            r0 = r23
            r1 = r5
            r1.yPos = r0
            r0 = r18
            r1 = r5
            r1.mNextOffset = r0
            int r22 = r22 + r10
            com.anoshenko.android.solitaires.Card r5 = r5.next
            int r15 = r15 + 1
            goto L_0x01b0
        L_0x02d7:
            r0 = r22
            r1 = r5
            r1.xPos = r0
            r0 = r23
            r1 = r5
            r1.yPos = r0
            r0 = r18
            r1 = r5
            r1.mNextOffset = r0
            int r22 = r22 + r19
            com.anoshenko.android.solitaires.Card r5 = r5.next
            int r15 = r15 + 1
            goto L_0x01b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.Pile.HorizontalCorrect():void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x01c8  */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void VerticalCorrect() {
        /*
            r28 = this;
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            com.anoshenko.android.solitaires.Game r0 = r0.mGame
            r23 = r0
            r0 = r23
            com.anoshenko.android.solitaires.CardData r0 = r0.mCardData
            r12 = r0
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            com.anoshenko.android.solitaires.Game r0 = r0.mGame
            r23 = r0
            r0 = r23
            int r0 = r0.mScreenType
            r23 = r0
            r24 = 1
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x0059
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            com.anoshenko.android.solitaires.CardsLayout r0 = r0.mLandscapeLayout
            r23 = r0
            r17 = r23
        L_0x0039:
            int r7 = r12.Width
            int r6 = r12.Height
            int r8 = r12.yOffset
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            com.anoshenko.android.solitaires.Game r0 = r0.mGame
            r23 = r0
            boolean r14 = r23.isMirrorLayout()
            r0 = r17
            int r0 = r0.Layout
            r23 = r0
            switch(r23) {
                case 0: goto L_0x0068;
                case 1: goto L_0x01db;
                default: goto L_0x0058;
            }
        L_0x0058:
            return
        L_0x0059:
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            com.anoshenko.android.solitaires.CardsLayout r0 = r0.mLayout
            r23 = r0
            r17 = r23
            goto L_0x0039
        L_0x0068:
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.top
            r23 = r0
            r0 = r23
            r1 = r28
            r1.yPos = r0
            r19 = r8
            r18 = 1
        L_0x007e:
            r0 = r17
            int r0 = r0.Align
            r23 = r0
            switch(r23) {
                case 1: goto L_0x01f7;
                case 2: goto L_0x021c;
                default: goto L_0x0087;
            }
        L_0x0087:
            if (r14 != 0) goto L_0x0240
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.left
            r23 = r0
        L_0x0095:
            r0 = r23
            r1 = r28
            r1.xPos = r0
        L_0x009b:
            r0 = r28
            android.graphics.Rect r0 = r0.mCardBounds
            r23 = r0
            r0 = r28
            int r0 = r0.xPos
            r24 = r0
            r0 = r28
            int r0 = r0.yPos
            r25 = r0
            r0 = r28
            int r0 = r0.xPos
            r26 = r0
            int r26 = r26 + r7
            r0 = r28
            int r0 = r0.yPos
            r27 = r0
            int r27 = r27 + r6
            r23.set(r24, r25, r26, r27)
            r0 = r28
            int r0 = r0.xPos
            r23 = r0
            int r24 = r7 / 2
            int r23 = r23 + r24
            r0 = r23
            r1 = r28
            r1.xEmptyCenter = r0
            r0 = r28
            int r0 = r0.yPos
            r23 = r0
            int r24 = r6 / 2
            int r23 = r23 + r24
            r0 = r23
            r1 = r28
            r1.yEmptyCenter = r0
            int r20 = r28.size()
            switch(r20) {
                case 0: goto L_0x0058;
                case 1: goto L_0x0250;
                default: goto L_0x00e7;
            }
        L_0x00e7:
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            int r0 = r0.mVisible
            r23 = r0
            switch(r23) {
                case 1: goto L_0x027e;
                case 2: goto L_0x0283;
                default: goto L_0x00f6;
            }
        L_0x00f6:
            r0 = r28
            int r0 = r0.xPos
            r21 = r0
            r0 = r28
            int r0 = r0.yPos
            r22 = r0
            r9 = 0
            com.anoshenko.android.solitaires.Card r5 = r28.firstElement()
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            int r0 = r0.mStartOpen
            r23 = r0
            r24 = 3
            r0 = r23
            r1 = r24
            if (r0 >= r1) goto L_0x02a4
            r0 = r28
            com.anoshenko.android.solitaires.PileGroup r0 = r0.mOwner
            r23 = r0
            r0 = r23
            int r0 = r0.mVisible
            r23 = r0
            if (r23 != 0) goto L_0x02a4
            r23 = 1
            r13 = r23
        L_0x012d:
            if (r13 == 0) goto L_0x0139
            r16 = 1
        L_0x0131:
            r0 = r16
            r1 = r20
            if (r0 >= r1) goto L_0x0139
            if (r5 != 0) goto L_0x02aa
        L_0x0139:
            int r11 = r19 / 3
            int r23 = r9 * r8
            int r10 = r23 / 3
            int r23 = r20 - r9
            r24 = 1
            int r23 = r23 - r24
            int r23 = r23 * r8
            int r23 = r23 + r10
            int r15 = r23 + r6
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            int r23 = r23.height()
            r0 = r15
            r1 = r23
            if (r0 <= r1) goto L_0x01af
            int r11 = r8 / 3
            if (r9 <= 0) goto L_0x02b9
            r23 = 1
            int r23 = r20 - r23
            r0 = r9
            r1 = r23
            if (r0 >= r1) goto L_0x02b9
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            int r23 = r23.height()
            r0 = r12
            int r0 = r0.Height
            r24 = r0
            int r23 = r23 - r24
            int r23 = r23 - r10
            int r24 = r20 - r9
            r25 = 1
            int r24 = r24 - r25
            int r19 = r23 / r24
            r0 = r19
            r1 = r11
            if (r0 >= r1) goto L_0x019b
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            int r23 = r23.height()
            int r23 = r23 - r6
            r24 = 1
            int r24 = r20 - r24
            int r19 = r23 / r24
            r11 = r19
        L_0x019b:
            r0 = r17
            int r0 = r0.Layout
            r23 = r0
            r24 = 1
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x01af
            int r11 = -r11
            r0 = r19
            int r0 = -r0
            r19 = r0
        L_0x01af:
            com.anoshenko.android.solitaires.Card r5 = r28.firstElement()
            r16 = 0
        L_0x01b5:
            r0 = r16
            r1 = r9
            if (r0 >= r1) goto L_0x01bc
            if (r5 != 0) goto L_0x02d5
        L_0x01bc:
            int r16 = r16 + 1
        L_0x01be:
            r0 = r16
            r1 = r20
            if (r0 >= r1) goto L_0x01c6
            if (r5 != 0) goto L_0x02ec
        L_0x01c6:
            if (r5 == 0) goto L_0x0058
            r0 = r21
            r1 = r5
            r1.xPos = r0
            r0 = r22
            r1 = r5
            r1.yPos = r0
            r23 = 0
            r0 = r23
            r1 = r5
            r1.mNextOffset = r0
            goto L_0x0058
        L_0x01db:
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.bottom
            r23 = r0
            int r23 = r23 - r6
            r0 = r23
            r1 = r28
            r1.yPos = r0
            r0 = r8
            int r0 = -r0
            r19 = r0
            r18 = 2
            goto L_0x007e
        L_0x01f7:
            if (r14 == 0) goto L_0x020d
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.left
            r23 = r0
        L_0x0205:
            r0 = r23
            r1 = r28
            r1.xPos = r0
            goto L_0x009b
        L_0x020d:
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.right
            r23 = r0
            int r23 = r23 - r7
            goto L_0x0205
        L_0x021c:
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.left
            r23 = r0
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            int r24 = r24.width()
            int r24 = r24 - r7
            int r24 = r24 / 2
            int r23 = r23 + r24
            r0 = r23
            r1 = r28
            r1.xPos = r0
            goto L_0x009b
        L_0x0240:
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            r0 = r23
            int r0 = r0.right
            r23 = r0
            int r23 = r23 - r7
            goto L_0x0095
        L_0x0250:
            com.anoshenko.android.solitaires.Card r23 = r28.firstElement()
            r0 = r28
            int r0 = r0.xPos
            r24 = r0
            r0 = r24
            r1 = r23
            r1.xPos = r0
            com.anoshenko.android.solitaires.Card r23 = r28.firstElement()
            r0 = r28
            int r0 = r0.yPos
            r24 = r0
            r0 = r24
            r1 = r23
            r1.yPos = r0
            com.anoshenko.android.solitaires.Card r23 = r28.firstElement()
            r24 = 0
            r0 = r24
            r1 = r23
            r1.mNextOffset = r0
            goto L_0x0058
        L_0x027e:
            r28.CorrectVisibleType1()
            goto L_0x0058
        L_0x0283:
            r23 = 0
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r24 = r0
            int r24 = r24.height()
            int r24 = r24 - r6
            int r24 = r24 / r8
            int r24 = r24 + 1
            r0 = r28
            r1 = r23
            r2 = r19
            r3 = r18
            r4 = r24
            r0.CorrectVisibleType2(r1, r2, r3, r4)
            goto L_0x0058
        L_0x02a4:
            r23 = 0
            r13 = r23
            goto L_0x012d
        L_0x02aa:
            r0 = r5
            boolean r0 = r0.mOpen
            r23 = r0
            if (r23 != 0) goto L_0x0139
            int r9 = r9 + 1
            com.anoshenko.android.solitaires.Card r5 = r5.next
            int r16 = r16 + 1
            goto L_0x0131
        L_0x02b9:
            r0 = r28
            android.graphics.Rect r0 = r0.mBounds
            r23 = r0
            int r23 = r23.height()
            int r23 = r23 - r6
            r24 = 1
            int r24 = r20 - r24
            int r19 = r23 / r24
            r23 = 1
            r0 = r23
            r1 = r28
            r1.mExpandable = r0
            goto L_0x019b
        L_0x02d5:
            r0 = r21
            r1 = r5
            r1.xPos = r0
            r0 = r22
            r1 = r5
            r1.yPos = r0
            r0 = r18
            r1 = r5
            r1.mNextOffset = r0
            int r22 = r22 + r11
            com.anoshenko.android.solitaires.Card r5 = r5.next
            int r16 = r16 + 1
            goto L_0x01b5
        L_0x02ec:
            r0 = r21
            r1 = r5
            r1.xPos = r0
            r0 = r22
            r1 = r5
            r1.yPos = r0
            r0 = r18
            r1 = r5
            r1.mNextOffset = r0
            int r22 = r22 + r19
            com.anoshenko.android.solitaires.Card r5 = r5.next
            int r16 = r16 + 1
            goto L_0x01be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.Pile.VerticalCorrect():void");
    }

    /* access modifiers changed from: package-private */
    public void Reset() {
        clear();
        this.mEmptyCard = this.mOwner.mAddEmptyCard == 0 ? 0 : 999423;
        this.mDealComplete = false;
    }

    /* access modifiers changed from: package-private */
    public void SetFixedCard() {
        CardList pack = this.mOwner.mGame.mPack.mWorkPack;
        int n = 0;
        Iterator<Card> it = pack.iterator();
        while (true) {
            if (it.hasNext()) {
                Card card = it.next();
                if (card.mValue == this.mFixedValue && card.mSuit == this.mFixedSuit) {
                    card.mOpen = true;
                    add(pack.remove(n));
                    break;
                }
                n++;
            } else {
                break;
            }
        }
        if (this.mOwner.mStartType < 2 && size() >= this.mStartSize) {
            this.mDealComplete = true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isDealCardOpen(int number) {
        switch (this.mOwner.mStartOpen) {
            case 1:
                return number == this.mStartSize - 1;
            case 2:
                return false;
            case 3:
            case 4:
                for (int i = 0; i < this.mCloseRegion.mCount; i++) {
                    if (number >= this.mCloseRegion.mFirst[i] && number < this.mCloseRegion.mFirst[i] + this.mCloseRegion.mLength[i]) {
                        return false;
                    }
                }
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void DealFinish() {
        if (size() > 0 && !lastElement().mOpen && this.mOwner.mStartOpen == 1) {
            lastElement().mOpen = true;
            this.mOwner.mGame.addRedrawRect(lastElement().getRect());
        }
        this.mDealComplete = true;
    }

    /* access modifiers changed from: package-private */
    public boolean isDealComplete() {
        if (!this.mDealComplete) {
            if (!this.mOwner.isEnableUse()) {
                this.mDealComplete = true;
            } else if (this.mOwner.mStartType < 2 && size() >= this.mStartSize) {
                this.mDealComplete = true;
            }
        }
        return this.mDealComplete;
    }

    /* access modifiers changed from: package-private */
    public boolean DealStep(GameAction deal_action) {
        if (this.mDealComplete) {
            return false;
        }
        if (!this.mOwner.isEnableUse()) {
            this.mDealComplete = true;
            return false;
        }
        CardList pack = this.mOwner.mGame.mPack.mWorkPack;
        int size = size();
        boolean f_open = isDealCardOpen(size);
        switch (this.mOwner.mStartType) {
            case 2:
                int card_mask = this.mOwner.mStartCondition.getNextCard(0);
                int new_card_mask = pack.lastElement().mCardMask;
                if ((card_mask & new_card_mask) == new_card_mask) {
                    switch (this.mOwner.mStartLastCard) {
                        case 0:
                            this.mDealComplete = true;
                            break;
                        case 1:
                            this.mDealComplete = true;
                            if (deal_action == null) {
                                insert(pack.removeLast(), 0);
                                lastElement().mOpen = f_open;
                            } else {
                                this.mOwner.mGame.MoveCard(null, 1, this, 0, f_open, deal_action);
                            }
                            return true;
                        case 2:
                            if (size > 0) {
                                DealFinish();
                                return false;
                            }
                            break;
                    }
                }
                if (deal_action == null) {
                    add(pack.removeLast());
                    lastElement().mOpen = f_open;
                } else {
                    this.mOwner.mGame.MoveCard(null, 1, this, size, f_open, deal_action);
                }
                return true;
            case 0:
            case 1:
                if (size >= this.mStartSize || pack.size() == 0) {
                    DealFinish();
                    break;
                } else if (deal_action == null) {
                    add(pack.removeLast());
                    lastElement().mOpen = f_open;
                    if (size >= this.mStartSize) {
                        DealFinish();
                        break;
                    }
                } else {
                    this.mOwner.mGame.MoveCard(null, 1, this, size, f_open, deal_action);
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean CorrectEmptyCard() {
        int empty_old = this.mEmptyCard;
        this.mEmptyCard = getEmptyAddCard();
        return empty_old != this.mEmptyCard;
    }

    /* access modifiers changed from: package-private */
    public boolean OpenLastCard(MoveMemory memory) {
        if (size() <= 0) {
            return CorrectEmptyCard();
        }
        Card card = lastElement();
        if (card.mOpen || !this.mOwner.mOpenCondition.Examine(card.mCardMask, this.mNumber, null)) {
            return false;
        }
        card.mOpen = true;
        if (memory != null) {
            memory.addOpenCard(this);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void draw(Canvas g, Rect clip_rect) {
        CardData data = this.mOwner.mGame.mCardData;
        if (data != null) {
            boolean invisible_cards = true;
            Iterator<Card> it = iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().mNextOffset != -1) {
                        invisible_cards = false;
                        break;
                    }
                } else {
                    break;
                }
            }
            if (!invisible_cards) {
                super.draw(g, clip_rect);
            } else if ((this.mEmptyCard & 16383) != 0) {
                int symbol = -1;
                int mask = 1;
                int available_cards = this.mOwner.mGame.mPack.getAvailableCards();
                int n = 0;
                while (true) {
                    if (n <= 13) {
                        if ((this.mEmptyCard & 16383) == mask && (available_cards & mask) != 0) {
                            symbol = n;
                            break;
                        } else {
                            n++;
                            mask <<= 1;
                        }
                    } else {
                        break;
                    }
                }
                data.DrawEmpty(g, this.xPos, this.yPos, symbol);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isEnableRemove(int r10, int r11) {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            int r2 = r9.size()
            if (r2 == 0) goto L_0x000a
            if (r10 < r2) goto L_0x000c
        L_0x000a:
            r3 = r7
        L_0x000b:
            return r3
        L_0x000c:
            com.anoshenko.android.solitaires.Card r0 = r9.getCard(r10)
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            boolean r3 = r3.isEnableUse()
            if (r3 == 0) goto L_0x003a
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            int r3 = r3.mRemoveType
            if (r3 == 0) goto L_0x003a
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            com.anoshenko.android.solitaires.Condition r3 = r3.mRemoveCondition
            int r4 = r0.mCardMask
            int r5 = r9.mNumber
            com.anoshenko.android.solitaires.PileGroup r6 = r9.mOwner
            boolean r3 = r3.Examine(r4, r5, r6)
            if (r3 == 0) goto L_0x003a
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            boolean r3 = r3.mDisableEmplyFirstPile
            if (r3 == 0) goto L_0x003c
            int r3 = r9.mNumber
            if (r3 != 0) goto L_0x003c
            if (r2 != r11) goto L_0x003c
        L_0x003a:
            r3 = r7
            goto L_0x000b
        L_0x003c:
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            int r3 = r3.mRemoveType
            switch(r3) {
                case 1: goto L_0x0048;
                case 2: goto L_0x0050;
                case 3: goto L_0x005c;
                case 4: goto L_0x0054;
                default: goto L_0x0043;
            }
        L_0x0043:
            r1 = 0
        L_0x0044:
            if (r1 < r11) goto L_0x006c
            r3 = r8
            goto L_0x000b
        L_0x0048:
            if (r11 > r8) goto L_0x004e
            int r3 = r2 - r8
            if (r10 == r3) goto L_0x0043
        L_0x004e:
            r3 = r7
            goto L_0x000b
        L_0x0050:
            if (r11 <= r8) goto L_0x0043
            r3 = r7
            goto L_0x000b
        L_0x0054:
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            int r3 = r3.mRemoveSeriesSize
            if (r3 == r11) goto L_0x005c
            r3 = r7
            goto L_0x000b
        L_0x005c:
            int r3 = r10 + r11
            if (r3 != r2) goto L_0x006a
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            com.anoshenko.android.solitaires.CardOrder r3 = r3.mRemoveSeries
            boolean r3 = r3.TestOrder(r0, r11, r8)
            if (r3 != 0) goto L_0x0043
        L_0x006a:
            r3 = r7
            goto L_0x000b
        L_0x006c:
            boolean r3 = r0.mOpen
            if (r3 != 0) goto L_0x0072
            r3 = r7
            goto L_0x000b
        L_0x0072:
            com.anoshenko.android.solitaires.Card r0 = r0.next
            int r1 = r1 + 1
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.Pile.isEnableRemove(int, int):boolean");
    }

    /* access modifiers changed from: package-private */
    public final boolean isEnableType1Remove() {
        return size() > 0 && this.mOwner.mRemoveCondition.Examine(0, this.mNumber, null);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean isEnableAdd(com.anoshenko.android.solitaires.Pile r10, int r11, com.anoshenko.android.solitaires.Card r12) {
        /*
            r9 = this;
            r8 = 1
            r7 = 0
            if (r10 == r9) goto L_0x0038
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            boolean r3 = r3.isEnableUse()
            if (r3 == 0) goto L_0x0038
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            int r3 = r3.mAddType
            if (r3 == 0) goto L_0x0038
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            com.anoshenko.android.solitaires.Condition r3 = r3.mAddCondition
            int r4 = r12.mCardMask
            int r5 = r9.mNumber
            com.anoshenko.android.solitaires.PileGroup r6 = r10.mOwner
            boolean r3 = r3.Examine(r4, r5, r6)
            if (r3 == 0) goto L_0x0038
            int r3 = r9.size()
            if (r3 != 0) goto L_0x003a
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            com.anoshenko.android.solitaires.Condition r3 = r3.mAddEmptyCondition
            int r4 = r12.mCardMask
            int r5 = r9.mNumber
            com.anoshenko.android.solitaires.PileGroup r6 = r10.mOwner
            boolean r3 = r3.Examine(r4, r5, r6)
            if (r3 != 0) goto L_0x003a
        L_0x0038:
            r3 = r7
        L_0x0039:
            return r3
        L_0x003a:
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            int r3 = r3.mAddType
            switch(r3) {
                case 1: goto L_0x004d;
                case 2: goto L_0x0059;
                case 3: goto L_0x0051;
                default: goto L_0x0041;
            }
        L_0x0041:
            int r3 = r12.mValue
            if (r3 != 0) goto L_0x009e
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            boolean r3 = r3.mFoundation
            if (r3 == 0) goto L_0x006e
            r3 = r7
            goto L_0x0039
        L_0x004d:
            if (r11 <= r8) goto L_0x0041
            r3 = r7
            goto L_0x0039
        L_0x0051:
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            int r3 = r3.mAddSeriesSize
            if (r11 == r3) goto L_0x0059
            r3 = r7
            goto L_0x0039
        L_0x0059:
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            com.anoshenko.android.solitaires.CardOrder r3 = r3.mAddSeries
            com.anoshenko.android.solitaires.PileGroup r4 = r9.mOwner
            boolean r4 = r4.mFoundation
            if (r4 == 0) goto L_0x006c
            r4 = r7
        L_0x0064:
            boolean r3 = r3.TestOrder(r12, r11, r4)
            if (r3 != 0) goto L_0x0041
            r3 = r7
            goto L_0x0039
        L_0x006c:
            r4 = r8
            goto L_0x0064
        L_0x006e:
            r2 = 999423(0xf3fff, float:1.40049E-39)
            com.anoshenko.android.solitaires.Card r0 = r12.next
            r1 = 1
        L_0x0074:
            if (r1 < r11) goto L_0x0086
        L_0x0076:
            int r3 = r9.getAddCard()
            r2 = r2 & r3
            r3 = 983040(0xf0000, float:1.377532E-39)
            r3 = r3 & r2
            if (r3 == 0) goto L_0x00a1
            r3 = r2 & 16383(0x3fff, float:2.2957E-41)
            if (r3 == 0) goto L_0x00a1
            r3 = r8
            goto L_0x0039
        L_0x0086:
            int r3 = r0.mValue
            if (r3 == 0) goto L_0x0099
            int r2 = r0.mCardMask
        L_0x008c:
            if (r1 <= 0) goto L_0x0076
            com.anoshenko.android.solitaires.PileGroup r3 = r9.mOwner
            com.anoshenko.android.solitaires.CardOrder r3 = r3.mAddSeries
            int r2 = r3.getPrevCard(r2)
            int r1 = r1 + -1
            goto L_0x008c
        L_0x0099:
            com.anoshenko.android.solitaires.Card r0 = r0.next
            int r1 = r1 + 1
            goto L_0x0074
        L_0x009e:
            int r2 = r12.mCardMask
            goto L_0x0076
        L_0x00a1:
            r3 = r7
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.Pile.isEnableAdd(com.anoshenko.android.solitaires.Pile, int, com.anoshenko.android.solitaires.Card):boolean");
    }

    /* access modifiers changed from: package-private */
    public int getAddCard() {
        if (this.mOwner.mAddType <= 0) {
            return 0;
        }
        int size = size();
        if (size <= 0) {
            return getEmptyAddCard();
        }
        if (lastElement().mValue != 0) {
            return this.mOwner.mAdd.getNextCard(lastElement().mCardMask);
        }
        Card card = lastElement().prev;
        int result = getEmptyAddCard();
        int k = 1;
        while (true) {
            if (k >= size) {
                break;
            } else if (card.mValue != 0) {
                result = this.mOwner.mAdd.getNextCard(card.mCardMask);
                break;
            } else {
                card = card.prev;
                k++;
            }
        }
        while (k > 0) {
            result = this.mOwner.mAdd.getNextCard(result);
            k--;
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public int getEmptyAddCard() {
        int result = 0;
        if (this.mOwner.mAddType > 0) {
            switch (this.mOwner.mAddEmptyType) {
                case 1:
                    result = 999423;
                    break;
                case 2:
                    result = this.mOwner.mAddEmptyCard;
                    break;
            }
            if (!(this.mOwner.mAddEmptyCondition instanceof ConditionEmpty)) {
                for (int mask = 1; mask < 16384; mask <<= 1) {
                    if ((mask & result) != 0 && !this.mOwner.mAddEmptyCondition.Examine((983040 & result) | mask, this.mNumber, null)) {
                        result &= mask ^ -1;
                    }
                }
            }
        }
        return this.mOwner.mGame.mPack.getAvailableCards() & result;
    }

    /* access modifiers changed from: package-private */
    public boolean Autoplay(MoveMemory moves, boolean f_ignore_lock, GameAction autoplay_action, boolean new_move) {
        Game game = this.mOwner.mGame;
        int mSize = size();
        for (PileGroup group : game.mGroup) {
            if (!group.mFoundation) {
                Pile[] pileArr = group.mPile;
                int length = pileArr.length;
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= length) {
                        continue;
                    } else {
                        Pile pile = pileArr[i2];
                        Card card = pile.lastElement();
                        int num = pile.size() - 1;
                        if (pile.size() <= 0 || ((card.mLock && !f_ignore_lock) || !pile.isEnableRemove(num, 1) || !isEnableAdd(pile, 1, card))) {
                            switch (group.mRemoveType) {
                                case 2:
                                    int j = 2;
                                    while (j <= pile.size()) {
                                        card = card.prev;
                                        num--;
                                        if (card != null && num >= 0) {
                                            if ((!card.mLock || f_ignore_lock) && pile.isEnableRemove(num, 1) && isEnableAdd(pile, 1, card)) {
                                                if (new_move) {
                                                    moves.IncreaseMoveNumber();
                                                }
                                                moves.addOneCardMove(pile, num, this);
                                                if (autoplay_action == null) {
                                                    add(pile.remove(num));
                                                    game.mNeedCorrect = true;
                                                } else {
                                                    game.MoveCard(pile, num, this, mSize, true, autoplay_action);
                                                }
                                                return true;
                                            }
                                            j++;
                                        }
                                    }
                                    continue;
                                case 3:
                                case 4:
                                    if (this.mOwner.mAddType > 1) {
                                        int j2 = 2;
                                        while (j2 <= pile.size()) {
                                            card = card.prev;
                                            num--;
                                            if (!card.mOpen) {
                                                continue;
                                            } else if ((!card.mLock || f_ignore_lock) && pile.isEnableRemove(num, j2) && isEnableAdd(pile, j2, card)) {
                                                if (new_move) {
                                                    moves.IncreaseMoveNumber();
                                                }
                                                moves.addSeriesCardMove(pile, this, j2);
                                                if (autoplay_action == null) {
                                                    add(pile.removeLast(j2));
                                                    game.mNeedCorrect = true;
                                                } else {
                                                    game.MoveCards(pile, this, j2, autoplay_action);
                                                }
                                                return true;
                                            } else {
                                                j2++;
                                            }
                                        }
                                        continue;
                                    } else {
                                        continue;
                                    }
                            }
                            i = i2 + 1;
                        } else {
                            if (new_move) {
                                moves.IncreaseMoveNumber();
                            }
                            moves.addOneCardMove(pile, num, this);
                            if (autoplay_action == null) {
                                add(pile.remove(num));
                                game.mNeedCorrect = true;
                            } else {
                                game.MoveCard(pile, num, this, mSize, true, autoplay_action);
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getAddPriority(Pile pile, int number, int count, Card first_card) {
        if (!this.mOwner.mFoundation) {
            return size() == 0 ? (count == pile.size() && this.mOwner == pile.mOwner) ? 1 : 3 : first_card.mSuit == lastElement().mSuit ? 5 : 4;
        }
        if (!pile.mOwner.mFoundation || this.mOwner != pile.mOwner) {
            return 6;
        }
        return 2;
    }

    /* access modifiers changed from: package-private */
    public int getMaxSize() {
        return this.mOwner.mAddCondition.getMaxSize(this);
    }

    /* access modifiers changed from: package-private */
    public int getSequentiallyAddCount(Pile from_pile) {
        int count = 0;
        while (from_pile.size() > 0 && from_pile.isEnableRemove(from_pile.size() - 1, 1) && isEnableAdd(from_pile, 1, from_pile.lastElement())) {
            add(from_pile.removeLast());
            count++;
        }
        int joker_count = 0;
        Card card = lastElement();
        for (int i = 0; i < count && card.mValue == 0; i++) {
            card = card.prev;
            joker_count++;
        }
        for (int i2 = 0; i2 < count; i2++) {
            from_pile.add(removeLast());
        }
        if (joker_count < count) {
            count -= joker_count;
        }
        if (count > 1) {
            return count;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getComplexAddCount(Pile from_pile) {
        int work_card;
        if (this.mOwner.mAddType == 0) {
            return 0;
        }
        int count = 0;
        int max_size = getMaxSize();
        Card card = from_pile.lastElement();
        if (max_size <= size() || !card.mOpen) {
            return 0;
        }
        int find_card = getAddCard();
        if (card.mValue != 0) {
            work_card = card.mCardMask;
        } else if (this.mOwner.mFoundation) {
            return 0;
        } else {
            work_card = 999423;
        }
        for (int n = 2; n <= from_pile.size(); n++) {
            card = card.prev;
            if (max_size <= size() + n || !card.mOpen) {
                break;
            }
            int work_card2 = this.mOwner.mAdd.getPrevCard(work_card2);
            if (card.mValue != 0) {
                if ((card.mValueMask & work_card2) == 0 || (card.mSuitMask & work_card2) == 0) {
                    break;
                }
                work_card2 = card.mCardMask;
            } else if (this.mOwner.mFoundation) {
                break;
            }
            if (!((work_card2 & find_card & 16383) == 0 || (work_card2 & find_card & 983040) == 0)) {
                count = n;
            }
        }
        int n_count1 = 0;
        int n_count2 = 0;
        Pile[] pileArr = this.mOwner.mGame.mPiles;
        int length = pileArr.length;
        for (int i = 0; i < length; i++) {
            Pile pile = pileArr[i];
            if (!(pile == from_pile || pile == this)) {
                switch (pile.getComplexMovePileType(from_pile, count)) {
                    case 1:
                        n_count1++;
                        continue;
                    case 2:
                        n_count2++;
                        continue;
                }
            }
        }
        if (n_count2 == 0) {
            n_count1--;
            n_count2++;
        }
        int n_count = n_count1 + n_count2;
        int n2 = 0;
        if (n_count >= 2 && n_count1 > 0) {
            n2 = 0 + n_count2;
            for (int i2 = 1; i2 <= n_count1; i2++) {
                n2 += (n_count2 + 1) * i2;
            }
        }
        Card card2 = from_pile.lastElement();
        for (int i3 = 1; i3 < count; i3++) {
            card2 = card2.prev;
        }
        while (count > 1) {
            if ((n_count >= count - 1 || n2 >= count - 1) && isEnableAdd(from_pile, 1, card2)) {
                return count;
            }
            card2 = card2.next;
            count--;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isEnableComplexAdd(Pile from_pile, int count) {
        Card card = from_pile.lastElement();
        if (this.mOwner.mAddType == 0 || getMaxSize() < size() + count || !card.mOpen) {
            return false;
        }
        for (int i = 1; i < count; i++) {
            card = card.prev;
            if (!card.mOpen) {
                return false;
            }
        }
        if (card.mValue == 0) {
            int mask = 999423;
            Card card2 = card.next;
            int k = 1;
            while (true) {
                if (k >= count) {
                    break;
                } else if (card2.mValue != 0) {
                    mask = card2.mCardMask;
                    while (k > 0) {
                        mask = this.mOwner.mAdd.getPrevCard(mask);
                        k--;
                    }
                } else {
                    card2 = card2.next;
                    k++;
                }
            }
            int mask2 = mask & getAddCard();
            if ((983040 & mask2) == 0 || (mask2 & 16383) == 0) {
                return false;
            }
        }
        if (isEnableAdd(from_pile, 1, card)) {
            if (this.mOwner.mAdd.TestOrder(card, count, !this.mOwner.mFoundation)) {
                int n_count1 = 0;
                int n_count2 = 0;
                Pile[] pileArr = this.mOwner.mGame.mPiles;
                int length = pileArr.length;
                for (int i2 = 0; i2 < length; i2++) {
                    Pile pile = pileArr[i2];
                    if (!(pile == from_pile || pile == this)) {
                        switch (pile.getComplexMovePileType(from_pile, count)) {
                            case 1:
                                n_count1++;
                                continue;
                            case 2:
                                n_count2++;
                                continue;
                        }
                    }
                }
                if (n_count2 == 0) {
                    n_count1--;
                    n_count2++;
                }
                int n_count = n_count1 + n_count2;
                if (n_count >= count - 1) {
                    return true;
                }
                int n = 0;
                if (n_count >= 2 && n_count1 > 0) {
                    n = 0 + n_count2;
                    for (int i3 = 1; i3 <= n_count1; i3++) {
                        n += (n_count2 + 1) * i3;
                    }
                }
                return n >= count - 1;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public int getComplexMovePileType(Pile from_pile, int count) {
        int max_size = getMaxSize();
        if (size() > 0) {
            return 0;
        }
        if (this.mOwner.mRemoveType == 0 || this.mOwner.mRemoveType == 4 || this.mOwner.mAddType == 0 || this.mOwner.mAddType == 3 || this.mOwner.mAddEmptyType == 0) {
            return 0;
        }
        int mask = this.mOwner.mGame.mPack.getAvailableCards() & 999422;
        if (this.mOwner.mAddEmptyType != 2 || (this.mOwner.mAddEmptyCard & mask) == mask) {
            return max_size >= this.mOwner.mGame.mPack.getMaxSize() ? 1 : 2;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getRemoveVariantCount() {
        int result = 0;
        int mSize = size();
        if (mSize <= 0) {
            return 0;
        }
        switch (this.mOwner.mRemoveType) {
            case 0:
            default:
                return 0;
            case 1:
                if (isEnableRemove(mSize - 1, 1)) {
                    return 0 + 1;
                }
                return 0;
            case 2:
                for (int n = 0; n < mSize; n++) {
                    if (isEnableRemove(n, 1)) {
                        result++;
                    }
                }
                return result;
            case 3:
                int n2 = 1;
                while (n2 <= mSize && isEnableRemove(mSize - n2, n2)) {
                    result++;
                    n2++;
                }
                return result;
            case 4:
                if (mSize < this.mOwner.mRemoveSeriesSize || !isEnableRemove(mSize - this.mOwner.mRemoveSeriesSize, this.mOwner.mRemoveSeriesSize)) {
                    return 0;
                }
                return 0 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public int getAddVariant(Pile from_pile) {
        int count;
        int number;
        int from_size = from_pile.size();
        if (from_size == 0) {
            return -1;
        }
        switch (this.mOwner.mAddType) {
            case 1:
            case 2:
                int number2 = from_size - 1;
                if (from_pile.isEnableRemove(number2, 1) && isEnableAdd(from_pile, 1, from_pile.lastElement())) {
                    return number2;
                }
                switch (from_pile.mOwner.mRemoveType) {
                    case 2:
                        Card card = from_pile.firstElement();
                        int count2 = from_size - 1;
                        for (int number3 = 0; number3 < count2; number3++) {
                            if (from_pile.isEnableRemove(number3, 1) && isEnableAdd(from_pile, 1, card)) {
                                return number3;
                            }
                            card = card.next;
                        }
                        break;
                    case 3:
                        if (this.mOwner.mAddType == 2) {
                            Card card2 = from_pile.lastElement().prev;
                            int count3 = 2;
                            while (count3 <= from_size) {
                                int number4 = from_size - count3;
                                if (!from_pile.isEnableRemove(number4, count3)) {
                                    break;
                                } else if (isEnableAdd(from_pile, count3, card2)) {
                                    return number4;
                                } else {
                                    card2 = card2.prev;
                                    count3++;
                                }
                            }
                            break;
                        }
                        break;
                    case 4:
                        if (this.mOwner.mAddType == 2 && (number = from_size - (count = from_pile.mOwner.mRemoveSeriesSize)) >= 0 && from_pile.isEnableRemove(number, count) && isEnableAdd(from_pile, count, from_pile.getCard(number))) {
                            return number;
                        }
                }
                break;
            case 3:
                int count4 = this.mOwner.mAddSeriesSize;
                int number5 = from_size - count4;
                if (number5 >= 0 && from_pile.isEnableRemove(number5, count4) && isEnableAdd(from_pile, count4, from_pile.getCard(number5))) {
                    return number5;
                }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public Vector<MoveVariant> getAllAddVariants(Pile from_pile) {
        int from_size = from_pile.size();
        if (from_size == 0) {
            return null;
        }
        Vector<MoveVariant> result = new Vector<>();
        switch (this.mOwner.mAddType) {
            case 2:
                switch (from_pile.mOwner.mRemoveType) {
                    case 3:
                        Card card = from_pile.firstElement();
                        for (int count = from_size; count > 1; count--) {
                            int number = from_size - count;
                            if (from_pile.isEnableRemove(number, count) && isEnableAdd(from_pile, count, card)) {
                                result.add(new MoveVariant(MoveVariantType.ONE, number, count, card));
                            }
                            card = card.next;
                        }
                    case 4:
                        int count2 = from_pile.mOwner.mRemoveSeriesSize;
                        int number2 = from_size - count2;
                        if (number2 >= 0 && from_pile.isEnableRemove(number2, count2)) {
                            Card card2 = from_pile.getCard(number2);
                            if (isEnableAdd(from_pile, count2, card2)) {
                                result.add(new MoveVariant(MoveVariantType.ONE, number2, count2, card2));
                            }
                        }
                }
            case 1:
                if (from_pile.mOwner.mRemoveType != 2) {
                    int number3 = from_size - 1;
                    if (from_pile.isEnableRemove(number3, 1) && isEnableAdd(from_pile, 1, from_pile.lastElement())) {
                        result.add(new MoveVariant(MoveVariantType.ONE, number3, 1, from_pile.lastElement()));
                    }
                    int count3 = getSequentiallyAddCount(from_pile);
                    if (count3 > 1) {
                        int number4 = from_size - count3;
                        Card card3 = from_pile.getCard(number4);
                        while (count3 > 1) {
                            result.add(new MoveVariant(MoveVariantType.SEQUENTIALLY, number4, count3, card3));
                            number4++;
                            count3--;
                            card3 = card3.next;
                        }
                    }
                    Card card4 = from_pile.firstElement();
                    for (int count4 = from_size; count4 > 1; count4--) {
                        int number5 = from_size - count4;
                        if (isEnableAdd(from_pile, 1, card4) && isEnableComplexAdd(from_pile, count4)) {
                            boolean f_exist = false;
                            Iterator it = result.iterator();
                            while (true) {
                                if (it.hasNext()) {
                                    MoveVariant variant = (MoveVariant) it.next();
                                    if ((variant.Type == MoveVariantType.ONE || variant.Type == MoveVariantType.COMPLEX) && variant.Number == number5 && variant.Count == count4 && variant.card == card4) {
                                        f_exist = true;
                                    }
                                }
                            }
                            if (!f_exist) {
                                result.add(new MoveVariant(MoveVariantType.COMPLEX, number5, count4, card4));
                            }
                        }
                        card4 = card4.next;
                    }
                    break;
                } else {
                    Card card5 = from_pile.firstElement();
                    for (int number6 = 0; number6 < from_size; number6++) {
                        if (from_pile.isEnableRemove(number6, 1) && isEnableAdd(from_pile, 1, card5)) {
                            result.add(new MoveVariant(MoveVariantType.ONE, number6, 1, card5));
                        }
                        card5 = card5.next;
                    }
                    break;
                }
                break;
            case 3:
                int count5 = this.mOwner.mAddSeriesSize;
                int number7 = from_size - count5;
                if (number7 >= 0 && from_pile.isEnableRemove(number7, count5)) {
                    Card card6 = from_pile.getCard(number7);
                    if (isEnableAdd(from_pile, count5, card6)) {
                        result.add(new MoveVariant(MoveVariantType.ONE, number7, count5, card6));
                        break;
                    }
                }
                break;
        }
        if (result.size() > 0) {
            return result;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public int getOverlapping(Card card) {
        CardData data = this.mOwner.mGame.mCardData;
        int overlap = 0;
        if (data == null) {
            return 0;
        }
        if (size() != 0) {
            Iterator<Card> it = iterator();
            while (it.hasNext()) {
                overlap += it.next().getOverlapping(card);
            }
            return overlap;
        } else if (this.mOwner.mAddType == 0 || card.xPos >= this.xPos + data.Width || this.xPos >= card.xPos + data.Width || card.yPos >= this.yPos + data.Height || this.yPos >= card.yPos + data.Height) {
            return 0;
        } else {
            return (data.Width - Math.abs(this.xPos - card.xPos)) * (data.Height - Math.abs(this.yPos - card.yPos));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isBelong(int x, int y) {
        CardData data = this.mOwner.mGame.mCardData;
        if (data != null) {
            if (size() != 0) {
                Iterator<Card> it = iterator();
                while (it.hasNext()) {
                    if (it.next().isBelong(x, y)) {
                        return true;
                    }
                }
            } else if (this.mOwner.mAddType == 0 || this.xPos > x || x >= this.xPos + data.Width || this.yPos > y || y >= this.yPos + data.Height) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }
}
