package com.anoshenko.android.solitaires;

import android.graphics.Rect;

public class PileGroup {
    final CardOrder mAdd;
    final Condition mAddCondition;
    final boolean mAddEmptyAuto;
    final int mAddEmptyCard;
    final Condition mAddEmptyCondition;
    final int mAddEmptyType;
    final CardOrder mAddSeries;
    final int mAddSeriesSize;
    final int mAddType;
    Rect mBounds = new Rect();
    final CloseRegion mCloseRegion;
    final boolean mDisableEmplyFirstPile;
    final boolean mEmptySource;
    final Condition mFinishCondition;
    final boolean mFixedCardFlag;
    public final boolean mFoundation;
    final Game mGame;
    final int mIndex;
    final CardsLayout mLandscapeLayout;
    final CardsLayout mLayout;
    final int mNumberSize;
    final Condition mOpenCondition;
    final boolean mPackPile;
    Pile[] mPile;
    final Condition mRedealCondition;
    final boolean mRedealPile;
    final int mRedealUsed;
    final boolean mRedealUsedFlag;
    final Condition mRemoveCondition;
    final CardOrder mRemoveSeries;
    final int mRemoveSeriesSize;
    final int mRemoveType;
    final CardOrder mStartCondition;
    final int mStartLastCard;
    final int mStartOpen;
    final int mStartSize;
    final int mStartType;
    final int mVisible;

    PileGroup(int index, Game game) {
        this.mGame = game;
        this.mIndex = index;
        BitStack stack = game.mSource.mRule;
        int count = stack.getIntF(4, 10) + 1;
        Condition empty_condition = new ConditionEmpty();
        if (count > 1) {
            int number_size = 1;
            for (int n = 2; count > n; n <<= 1) {
                number_size++;
            }
            this.mNumberSize = number_size;
        } else {
            this.mNumberSize = 0;
        }
        this.mFoundation = stack.getFlag();
        this.mPackPile = game.mPack.isUse() ? stack.getFlag() : false;
        if (game.mEnableRedeal) {
            this.mRedealPile = stack.getFlag();
            if (!this.mRedealPile) {
                this.mRedealCondition = empty_condition;
            } else if (stack.getFlag()) {
                this.mRedealCondition = Condition.Load(game);
            } else {
                this.mRedealCondition = empty_condition;
            }
            this.mRedealUsedFlag = stack.getFlag();
            this.mRedealUsed = this.mRedealUsedFlag ? stack.getInt(game.mRedealCount + 1) : 0;
        } else {
            this.mRedealPile = false;
            this.mRedealCondition = empty_condition;
            this.mRedealUsedFlag = false;
            this.mRedealUsed = 0;
        }
        int remove_series_size = 0;
        int add_series_size = 0;
        int add_empty_card = 0;
        boolean f_disable_emply_first_pile = false;
        boolean f_add_empty_auto = false;
        boolean f_empty_source = false;
        Condition add_empty_condition = empty_condition;
        CardOrder remove_series = null;
        CardOrder add_series = null;
        if (game.mGameType != 1) {
            this.mRemoveType = stack.getInt(1, 3);
            switch (this.mRemoveType) {
                case 4:
                    remove_series_size = stack.getIntF(4, 9) + 1;
                case 3:
                    remove_series = new CardOrder(game);
                case 1:
                case 2:
                    if (stack.getFlag()) {
                        this.mRemoveCondition = Condition.Load(game);
                    } else {
                        this.mRemoveCondition = empty_condition;
                    }
                    f_disable_emply_first_pile = stack.getFlag();
                    f_empty_source = stack.getFlag();
                    break;
                default:
                    this.mRemoveCondition = empty_condition;
                    break;
            }
            this.mAddType = stack.getInt(2);
            switch (this.mAddType) {
                case 3:
                    add_series_size = stack.getIntF(4, 9) + 1;
                case 2:
                    add_series = new CardOrder(game);
                case 1:
                    this.mAdd = new CardOrder(game);
                    if (stack.getFlag()) {
                        this.mAddCondition = Condition.Load(game);
                    } else {
                        this.mAddCondition = empty_condition;
                    }
                    this.mAddEmptyType = stack.getInt(1, 2);
                    switch (this.mAddEmptyType) {
                        case 2:
                            add_empty_card = (stack.getInt(4) << 16) | stack.getInt(14);
                        case 1:
                            add_empty_condition = stack.getFlag() ? Condition.Load(game) : add_empty_condition;
                            f_add_empty_auto = stack.getFlag();
                            break;
                    }
                    break;
                default:
                    this.mAdd = null;
                    this.mAddEmptyType = 0;
                    this.mAddCondition = empty_condition;
                    break;
            }
        } else {
            this.mRemoveType = 1;
            this.mAdd = null;
            this.mAddEmptyType = 0;
            this.mAddType = 0;
            this.mAddCondition = empty_condition;
            if (stack.getFlag()) {
                this.mRemoveCondition = Condition.Load(game);
            } else {
                this.mRemoveCondition = empty_condition;
            }
        }
        this.mRemoveSeriesSize = remove_series_size;
        this.mAddSeriesSize = add_series_size;
        this.mAddEmptyCard = add_empty_card;
        this.mDisableEmplyFirstPile = f_disable_emply_first_pile;
        this.mAddEmptyCondition = add_empty_condition;
        this.mRemoveSeries = remove_series;
        this.mAddSeries = add_series;
        this.mAddEmptyAuto = f_add_empty_auto;
        this.mEmptySource = f_empty_source;
        this.mFixedCardFlag = stack.getFlag();
        this.mStartType = stack.getInt(1, 2);
        switch (this.mStartType) {
            case 0:
                this.mStartSize = stack.getIntF(4, 13);
                this.mStartLastCard = 0;
                this.mStartCondition = null;
                break;
            case 1:
            default:
                this.mStartSize = 0;
                this.mStartLastCard = 0;
                this.mStartCondition = null;
                break;
            case 2:
                this.mStartSize = 0;
                this.mStartCondition = new CardOrder(game);
                this.mStartLastCard = stack.getInt(1, 2);
                break;
        }
        CloseRegion close_region = null;
        if (this.mStartType > 0 || this.mStartSize > 0) {
            this.mStartOpen = stack.getInt(1, 3);
            if (this.mStartOpen > 0) {
                if (stack.getFlag()) {
                    this.mOpenCondition = Condition.Load(game);
                } else {
                    this.mOpenCondition = empty_condition;
                }
                if (this.mStartOpen == 3) {
                    close_region = new CloseRegion(stack);
                }
            } else {
                this.mOpenCondition = empty_condition;
            }
        } else {
            this.mStartOpen = 0;
            this.mOpenCondition = empty_condition;
        }
        this.mCloseRegion = close_region;
        if (stack.getFlag()) {
            this.mFinishCondition = Condition.Load(game);
        } else {
            this.mFinishCondition = empty_condition;
        }
        this.mLayout = new CardsLayout(game.mSource);
        if (stack.getFlag()) {
            this.mLandscapeLayout = new CardsLayout(game.mSource);
        } else {
            this.mLandscapeLayout = this.mLayout;
        }
        this.mVisible = stack.getInt(1, 2);
        this.mPile = new Pile[count];
        for (int i = 0; i < count; i++) {
            this.mPile[i] = new Pile(i, this);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isEnableUse() {
        return !this.mGame.mEnableRedeal || !this.mRedealUsedFlag || (this.mRedealUsed & (1 << this.mGame.RedealCurrent)) != 0;
    }

    /* access modifiers changed from: package-private */
    public void SetFixedCard() {
        if (this.mFixedCardFlag) {
            for (Pile pile : this.mPile) {
                pile.SetFixedCard();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void Correct() {
        CardsLayout layout;
        int start_x;
        int step_x;
        int x;
        int step_x2;
        if (this.mGame.mCardData != null) {
            if (this.mGame.mScreenType == 1) {
                layout = this.mLandscapeLayout;
            } else {
                layout = this.mLayout;
            }
            Rect screen = this.mGame.mScreen;
            int screen_width = screen.width();
            int screen_height = screen.height();
            CardData card_data = this.mGame.mCardData;
            this.mBounds.top = layout.Top.getTop(screen.top, screen_height, card_data);
            this.mBounds.left = layout.Left.getLeft(screen.left, screen_width, card_data);
            this.mBounds.bottom = layout.Bottom.getBottom(screen.top, screen_height, card_data);
            this.mBounds.right = layout.Right.getRight(screen.left, screen_width, card_data);
            if (this.mGame.isMirrorLayout()) {
                int left = this.mGame.mScreen.left + (screen_width - this.mBounds.right);
                int right = this.mGame.mScreen.left + (screen_width - this.mBounds.left);
                this.mBounds.left = left;
                this.mBounds.right = right;
            }
            int bounds_width = this.mBounds.width();
            int bounds_height = this.mBounds.height();
            int line_size = layout.LineSize;
            int line_count = ((this.mPile.length + line_size) - 1) / line_size;
            int last_line = this.mPile.length % line_size;
            int n = 0;
            if (layout.fCollumnLayout) {
                int arrea_width = bounds_width / line_count;
                int arrea_height = bounds_height / line_size;
                int off = (bounds_height % line_size) / 2;
                if (this.mGame.isMirrorLayout()) {
                    x = this.mBounds.left + ((line_count - 1) * arrea_width);
                    step_x2 = -arrea_width;
                } else {
                    x = this.mBounds.left;
                    step_x2 = arrea_width;
                }
                for (int i = 1; i < line_count; i++) {
                    int y = this.mBounds.top + off;
                    for (int k = 0; k < line_size && n < this.mPile.length; k++) {
                        this.mPile[n].mBounds.set(x, y, x + arrea_width, y + arrea_height);
                        this.mPile[n].Correct();
                        y += arrea_height;
                        n++;
                    }
                    x += step_x2;
                }
                if (last_line <= 0) {
                    last_line = line_size;
                } else if (layout.fOptimalLineSize) {
                    arrea_width = bounds_width - ((line_count - 1) * arrea_width);
                    arrea_height = (bounds_height - off) / last_line;
                }
                int y2 = this.mBounds.top + off;
                for (int k2 = 0; k2 < last_line && n < this.mPile.length; k2++) {
                    this.mPile[n].mBounds.set(x, y2, x + arrea_width, y2 + arrea_height);
                    this.mPile[n].Correct();
                    y2 += arrea_height;
                    n++;
                }
                return;
            }
            int arrea_width2 = bounds_width / line_size;
            int arrea_height2 = bounds_height / line_count;
            int off2 = (bounds_width % line_size) / 2;
            if (this.mGame.isMirrorLayout()) {
                start_x = this.mBounds.left + off2 + ((line_size - 1) * arrea_width2);
                step_x = -arrea_width2;
            } else {
                start_x = this.mBounds.left + off2;
                step_x = arrea_width2;
            }
            int y3 = this.mBounds.top;
            for (int i2 = 1; i2 < line_count; i2++) {
                int x2 = start_x;
                for (int k3 = 0; k3 < line_size && n < this.mPile.length; k3++) {
                    this.mPile[n].mBounds.set(x2, y3, x2 + arrea_width2, y3 + arrea_height2);
                    this.mPile[n].Correct();
                    x2 += step_x;
                    n++;
                }
                y3 += arrea_height2;
            }
            if (last_line > 0) {
                if (layout.fOptimalLineSize) {
                    arrea_width2 = (bounds_width - off2) / last_line;
                    arrea_height2 = bounds_height - ((line_count - 1) * arrea_height2);
                    start_x = this.mBounds.left + off2;
                    step_x = arrea_width2;
                }
            }
            int x3 = start_x;
            for (int k4 = 0; k4 < line_size && n < this.mPile.length; k4++) {
                this.mPile[n].mBounds.set(x3, y3, x3 + arrea_width2, y3 + arrea_height2);
                this.mPile[n].Correct();
                x3 += step_x;
                n++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getPileMaxSize() {
        return this.mAddCondition.getMaxSize(this.mPile[0]);
    }

    /* access modifiers changed from: package-private */
    public int getCardLayout() {
        return (this.mGame.mScreenType == 1 ? this.mLandscapeLayout : this.mLayout).Layout;
    }

    /* access modifiers changed from: package-private */
    public boolean Autoplay(MoveMemory moves, GameAction autoplay_action, boolean new_move) {
        boolean z;
        int size = Integer.MAX_VALUE;
        if (this.mGame.getAutoplayType() < 3) {
            for (Pile pile : this.mPile) {
                if (pile.size() < size) {
                    size = pile.size();
                }
            }
            if (this.mGame.getAutoplayType() == 2) {
                size++;
            }
        }
        for (Pile pile2 : this.mPile) {
            if (pile2.size() <= size) {
                if (this.mGame.mUseCardLock) {
                    z = false;
                } else {
                    z = true;
                }
                if (pile2.Autoplay(moves, z, autoplay_action, new_move)) {
                    return true;
                }
            }
        }
        return false;
    }
}
