package com.anoshenko.android.solitaires;

import android.content.Intent;
import android.os.Bundle;
import com.anoshenko.android.background.Background;
import com.anoshenko.android.background.BackgroundActivity;
import com.anoshenko.android.ui.Title;
import java.io.IOException;

public abstract class GameActivity extends GameBaseActivity {
    public static final int AUTOPLAY_COLLECT_ALL = 2;
    public static final int AUTOPLAY_OFF = 0;
    public static final int AUTOPLAY_ON = 1;
    public static final String GAME_ID = "ID";
    public static final String GAME_NAME = "Name";
    public static final String GAME_TYPE = "Type";
    private boolean mAnimation = true;
    private int mAutoplay = 1;
    private boolean mDealAnimation = true;
    private boolean mEnableMovementSound = false;
    int mGameId;
    String mGameName;
    int mGameType;
    private boolean mHidePackRedeal = false;
    private boolean mHidePackSize = false;
    private boolean mMirror = false;
    DataSource mSource;

    /* access modifiers changed from: protected */
    public abstract void createGame(GameView gameView);

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        loadOptions();
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        this.mGameId = intent.getIntExtra(GAME_ID, -1);
        this.mGameType = intent.getIntExtra(GAME_TYPE, 0);
        this.mGameName = intent.getStringExtra(GAME_NAME);
        try {
            this.mSource = new DataSource(this, intent);
            createGame(this.mView);
            this.mView.mGame.initToolbar();
            this.mToolbar.setCommandListener(this.mView.mGame);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void loadOptions() {
        int new_orientation;
        Settings settings = new Settings(this);
        try {
            this.mDealAnimation = settings.isDealAnimation();
            this.mAnimation = settings.isAnimation();
            this.mMirror = settings.isMirrorLayout();
            this.mHidePackSize = settings.isHidePackSize();
            this.mHidePackRedeal = settings.isHidePackRedeal();
            this.mAutoplay = settings.getAutoplay();
            this.mEnableMovementSound = settings.isEnableMovementSound();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            switch (settings.getOrientation()) {
                case 1:
                    new_orientation = 1;
                    break;
                case 2:
                    new_orientation = 0;
                    break;
                case 3:
                    new_orientation = 4;
                    break;
                default:
                    new_orientation = -1;
                    break;
            }
            if (getRequestedOrientation() != new_orientation) {
                setRequestedOrientation(new_orientation);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Command.OPTIONS_ACTIVITY:
                loadOptions();
                return;
            case Command.BACKGROUND_ACTIVITY:
                updateToolbarTheme();
                ((Title) findViewById(R.id.GameTitle)).updateTheme();
                if (this.mBackground != null) {
                    this.mBackground.recycle();
                }
                this.mBackground = new Background(this, BackgroundActivity.BuildinIds);
                this.mView.invalidate();
                return;
            case Command.CARDS_ACTIVITY:
                this.mView.mGame.mCardData.release();
                this.mView.mGame.mCardData = null;
                this.mView.mGame.setScreenSize(this.mView.getWidth(), this.mView.getHeight());
                this.mView.invalidate();
                return;
            default:
                return;
        }
    }

    public boolean isEnableMovementSound() {
        return this.mEnableMovementSound;
    }

    public boolean isAnimation() {
        return this.mAnimation;
    }

    public boolean isDealAnimation() {
        return this.mDealAnimation;
    }

    public boolean isMirror() {
        return this.mMirror;
    }

    public boolean isHidePackSize() {
        return this.mHidePackSize;
    }

    public boolean isHidePackRedeal() {
        return this.mHidePackRedeal;
    }

    public int getAutoplay() {
        return this.mAutoplay;
    }
}
