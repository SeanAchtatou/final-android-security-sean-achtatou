package com.anoshenko.android.solitaires;

import android.content.Context;
import java.io.File;
import java.io.IOException;

public class Backup implements Runnable {
    private static final String BACKUP_FILE = "statistics.backup";
    private static final byte[] BACKUP_HEADER = {115, 116, 97, 116, 115, VERSION};
    private static final byte VERSION = 1;
    private final Context mContext;

    private Backup(Context context) {
        this.mContext = context;
    }

    public void run() {
        create(this.mContext);
    }

    public static void start(Context context) {
        new Thread(new Backup(context)).start();
    }

    public static final File getBackupFile() {
        try {
            File home_folder = Utils.getHomeFolder();
            if (home_folder == null) {
                return null;
            }
            return new File(home_folder, BACKUP_FILE);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static final boolean isBackupExists(Context context) {
        File file = getBackupFile();
        return file != null && file.exists();
    }

    public static final void backupBySchedule(Context context) {
        Settings settings = new Settings(context);
        if (settings.isBackupEnabled()) {
            if (System.currentTimeMillis() >= settings.getLastBackupTime() + 86400000) {
                start(context);
            }
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean create(android.content.Context r6) {
        /*
            java.io.File r1 = getBackupFile()     // Catch:{ IOException -> 0x0033 }
            boolean r3 = r1.exists()     // Catch:{ IOException -> 0x0033 }
            if (r3 == 0) goto L_0x000d
            r1.delete()     // Catch:{ IOException -> 0x0033 }
        L_0x000d:
            r1.createNewFile()     // Catch:{ IOException -> 0x0033 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0033 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x0033 }
            byte[] r3 = com.anoshenko.android.solitaires.Backup.BACKUP_HEADER     // Catch:{ all -> 0x002e }
            r2.write(r3)     // Catch:{ all -> 0x002e }
            com.anoshenko.android.solitaires.Storage.backupSave(r6, r2)     // Catch:{ all -> 0x002e }
            r2.close()     // Catch:{ IOException -> 0x0033 }
            com.anoshenko.android.solitaires.Settings r3 = new com.anoshenko.android.solitaires.Settings     // Catch:{ IOException -> 0x0033 }
            r3.<init>(r6)     // Catch:{ IOException -> 0x0033 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0033 }
            r3.setLastBackupTime(r4)     // Catch:{ IOException -> 0x0033 }
            r3 = 1
        L_0x002d:
            return r3
        L_0x002e:
            r3 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0033 }
            throw r3     // Catch:{ IOException -> 0x0033 }
        L_0x0033:
            r3 = move-exception
            r0 = r3
            r0.printStackTrace()
            r3 = 0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.Backup.create(android.content.Context):boolean");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean restore(android.content.Context r8) {
        /*
            r7 = 0
            java.io.File r2 = getBackupFile()     // Catch:{ IOException -> 0x003d }
            boolean r5 = r2.exists()     // Catch:{ IOException -> 0x003d }
            if (r5 != 0) goto L_0x000d
            r5 = r7
        L_0x000c:
            return r5
        L_0x000d:
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ IOException -> 0x003d }
            r4.<init>(r2)     // Catch:{ IOException -> 0x003d }
            byte[] r5 = com.anoshenko.android.solitaires.Backup.BACKUP_HEADER     // Catch:{ all -> 0x0038 }
            int r5 = r5.length     // Catch:{ all -> 0x0038 }
            byte[] r0 = new byte[r5]     // Catch:{ all -> 0x0038 }
            r4.read(r0)     // Catch:{ all -> 0x0038 }
            r3 = 0
        L_0x001b:
            byte[] r5 = com.anoshenko.android.solitaires.Backup.BACKUP_HEADER     // Catch:{ all -> 0x0038 }
            int r5 = r5.length     // Catch:{ all -> 0x0038 }
            if (r3 < r5) goto L_0x0028
            com.anoshenko.android.solitaires.Storage.backupRestore(r8, r4)     // Catch:{ all -> 0x0038 }
            r4.close()     // Catch:{ IOException -> 0x003d }
            r5 = 1
            goto L_0x000c
        L_0x0028:
            byte r5 = r0[r3]     // Catch:{ all -> 0x0038 }
            byte[] r6 = com.anoshenko.android.solitaires.Backup.BACKUP_HEADER     // Catch:{ all -> 0x0038 }
            byte r6 = r6[r3]     // Catch:{ all -> 0x0038 }
            if (r5 == r6) goto L_0x0035
            r4.close()     // Catch:{ IOException -> 0x003d }
            r5 = r7
            goto L_0x000c
        L_0x0035:
            int r3 = r3 + 1
            goto L_0x001b
        L_0x0038:
            r5 = move-exception
            r4.close()     // Catch:{ IOException -> 0x003d }
            throw r5     // Catch:{ IOException -> 0x003d }
        L_0x003d:
            r5 = move-exception
            r1 = r5
            r1.printStackTrace()
            r5 = r7
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.Backup.restore(android.content.Context):boolean");
    }
}
