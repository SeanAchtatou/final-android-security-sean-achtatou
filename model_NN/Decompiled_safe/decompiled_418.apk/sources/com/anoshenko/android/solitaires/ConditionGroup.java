package com.anoshenko.android.solitaires;

/* compiled from: Condition */
final class ConditionGroup extends Condition {
    private Condition[] mCondition;
    private boolean mTrueAll;

    ConditionGroup(Game game, int count, boolean f_subgroup) {
        this.mTrueAll = game.mSource.mRule.getFlag();
        this.mCondition = new Condition[count];
        if (f_subgroup) {
            for (int i = 0; i < count; i++) {
                int sub_count = game.mSource.mRule.getInt(3, 5) + 1;
                if (sub_count == 1) {
                    this.mCondition[i] = new ConditionElement(game);
                } else {
                    this.mCondition[i] = new ConditionGroup(game, sub_count, false);
                }
            }
            return;
        }
        for (int i2 = 0; i2 < count; i2++) {
            this.mCondition[i2] = new ConditionElement(game);
        }
    }

    /* access modifiers changed from: package-private */
    public boolean Examine(int card_mask, int current_pile_number, PileGroup src_group) {
        int i = 0;
        if (this.mTrueAll) {
            Condition[] conditionArr = this.mCondition;
            int length = conditionArr.length;
            while (i < length) {
                if (!conditionArr[i].Examine(card_mask, current_pile_number, src_group)) {
                    return false;
                }
                i++;
            }
            return true;
        }
        Condition[] conditionArr2 = this.mCondition;
        int length2 = conditionArr2.length;
        while (i < length2) {
            if (conditionArr2[i].Examine(card_mask, current_pile_number, src_group)) {
                return true;
            }
            i++;
        }
        return false;
    }
}
