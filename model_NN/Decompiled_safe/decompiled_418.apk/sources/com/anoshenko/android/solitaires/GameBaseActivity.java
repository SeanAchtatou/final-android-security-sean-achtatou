package com.anoshenko.android.solitaires;

import android.app.Activity;
import android.content.res.Configuration;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import com.anoshenko.android.background.Background;
import com.anoshenko.android.background.BackgroundActivity;
import com.anoshenko.android.ui.CardMoveView;
import com.anoshenko.android.ui.GamePopupListener;
import com.anoshenko.android.ui.GamePopupPage;
import com.anoshenko.android.ui.GameViewGroup;
import com.anoshenko.android.ui.Toolbar;
import com.anoshenko.android.ui.ToolbarButton;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public abstract class GameBaseActivity extends Activity implements AdListener {
    private static final String AD_UNIT_ID = "a14be28f0c4b130";
    public static final int AUDIO_STREAM = 3;
    public static final int DEFAULT_SOUND_LEVEL = 5;
    public static final int DROP_SOUND = 1;
    public static final int MAX_SOUND_LEVEL = 10;
    public static final int MOVE_SOUND = 2;
    private static final int[] SOUND_RES_ID = {R.raw.take, R.raw.drop, R.raw.move};
    public static final int TAKE_SOUND = 0;
    public AdView mAdView;
    Background mBackground;
    public CardMoveView mCardMoveView;
    /* access modifiers changed from: private */
    public int mCurrentPopupPage;
    /* access modifiers changed from: private */
    public GamePopupListener mPopupListener = null;
    /* access modifiers changed from: private */
    public GamePopupPage[] mPopupPages = null;
    private final int[] mSoundId = new int[SOUND_RES_ID.length];
    private SoundPool mSoundPool;
    protected Toolbar mToolbar;
    public GameView mView;
    public GameViewGroup mViewGroup;

    public abstract int getAutoplay();

    public abstract boolean isAnimation();

    public abstract boolean isDealAnimation();

    public abstract boolean isEnableMovementSound();

    public abstract boolean isHidePackRedeal();

    public abstract boolean isHidePackSize();

    public abstract boolean isMirror();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        AdSize ad_size;
        super.onCreate(savedInstanceState);
        DisplayMetrics dm = Utils.getDisplayMetrics(this);
        if (dm.heightPixels < dm.widthPixels) {
            getWindow().setFlags(1024, 1024);
        }
        this.mSoundPool = new SoundPool(10, 3, 0);
        for (int i = 0; i < SOUND_RES_ID.length; i++) {
            this.mSoundId[i] = this.mSoundPool.load(this, SOUND_RES_ID[i], 1);
        }
        setContentView((int) R.layout.game_view);
        this.mViewGroup = (GameViewGroup) findViewById(R.id.GameViewGroup);
        this.mView = (GameView) findViewById(R.id.GameArrea);
        this.mToolbar = (Toolbar) findViewById(R.id.GameToolbar);
        this.mCardMoveView = (CardMoveView) findViewById(R.id.GameCardMove);
        this.mCardMoveView.setGameActivity(this);
        if (Math.min(dm.heightPixels, dm.widthPixels) >= 728) {
            ad_size = AdSize.IAB_LEADERBOARD;
        } else {
            ad_size = AdSize.BANNER;
        }
        this.mAdView = new AdView(this, ad_size, AD_UNIT_ID);
        this.mAdView.setId(GameViewGroup.AD_VIEW_ID);
        this.mAdView.setAdListener(this);
        this.mViewGroup.addView(this.mAdView, 1);
        this.mBackground = new Background(this, BackgroundActivity.BuildinIds);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mSoundPool != null) {
            this.mSoundPool.release();
            this.mSoundPool = null;
        }
        if (this.mBackground != null) {
            this.mBackground.recycle();
            this.mBackground = null;
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mAdView.loadAd(new AdRequest());
    }

    public void onConfigurationChanged(Configuration newConfig) {
        Window window = getWindow();
        if (newConfig.orientation == 2) {
            window.addFlags(1024);
        } else {
            window.clearFlags(1024);
        }
        super.onConfigurationChanged(newConfig);
    }

    public final void playSound(int n) {
        try {
            if (isEnableMovementSound() && this.mSoundPool != null) {
                float volume = ((float) new Settings(this).getSoundLevel()) / 10.0f;
                this.mSoundPool.play(this.mSoundId[n], volume, volume, 0, 0, 1.0f);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.mSoundPool.unload(this.mSoundId[n]);
            this.mSoundId[n] = this.mSoundPool.load(this, SOUND_RES_ID[n], 1);
        }
    }

    public void invalidateToolbar() {
        this.mToolbar.postInvalidate();
    }

    public void setToolbarButton(ToolbarButton[] buttons) {
        this.mToolbar.setToolbarButton(buttons);
    }

    /* access modifiers changed from: protected */
    public void updateToolbarTheme() {
        this.mToolbar.updateTheme();
    }

    public void showPopupPages(GamePopupPage[] pages, GamePopupListener listener) {
        if (pages != null && pages.length != 0) {
            this.mPopupPages = pages;
            this.mPopupListener = listener;
            setCurrentPopupPage(0);
            ((Button) findViewById(R.id.GamePopupButton1)).setOnClickListener(new GamePopupButton1Listener(this, null));
            ((Button) findViewById(R.id.GamePopupButton2)).setOnClickListener(new GamePopupButton2Listener(this, null));
            View view = findViewById(R.id.GamePopupBack);
            if (view != null) {
                view.setVisibility(0);
            }
            View view2 = findViewById(R.id.GamePopup);
            if (view2 != null) {
                view2.setVisibility(0);
            }
        }
    }

    private class GamePopupButton1Listener implements View.OnClickListener {
        private GamePopupButton1Listener() {
        }

        /* synthetic */ GamePopupButton1Listener(GameBaseActivity gameBaseActivity, GamePopupButton1Listener gamePopupButton1Listener) {
            this();
        }

        public void onClick(View v) {
            if (GameBaseActivity.this.mCurrentPopupPage > 0) {
                GameBaseActivity.this.setCurrentPopupPage(GameBaseActivity.this.mCurrentPopupPage - 1);
            } else {
                GameBaseActivity.this.closePopupPages();
            }
        }
    }

    private class GamePopupButton2Listener implements View.OnClickListener {
        private GamePopupButton2Listener() {
        }

        /* synthetic */ GamePopupButton2Listener(GameBaseActivity gameBaseActivity, GamePopupButton2Listener gamePopupButton2Listener) {
            this();
        }

        public void onClick(View v) {
            if (GameBaseActivity.this.mCurrentPopupPage < GameBaseActivity.this.mPopupPages.length - 1) {
                GameBaseActivity.this.setCurrentPopupPage(GameBaseActivity.this.mCurrentPopupPage + 1);
                return;
            }
            for (GamePopupPage page : GameBaseActivity.this.mPopupPages) {
                page.saveViewData();
            }
            if (GameBaseActivity.this.mPopupListener != null) {
                GameBaseActivity.this.mPopupListener.onGamePopupOkClosed();
            }
            GameBaseActivity.this.closePopupPages();
        }
    }

    /* access modifiers changed from: private */
    public void setCurrentPopupPage(int page) {
        String str;
        ScrollView scroll_view = (ScrollView) findViewById(R.id.GamePopupScrollView);
        if (scroll_view != null) {
            scroll_view.removeAllViews();
            scroll_view.addView(this.mPopupPages[page].getView());
            this.mCurrentPopupPage = page;
            ((Button) findViewById(R.id.GamePopupButton1)).setText(page == 0 ? R.string.cancel_button : R.string.back_button);
            ((Button) findViewById(R.id.GamePopupButton2)).setText(page == this.mPopupPages.length - 1 ? R.string.ok_button : R.string.next_button);
            TextView title_view = (TextView) findViewById(R.id.GamePopupTitle);
            if (title_view != null) {
                String title = this.mPopupPages[page].getTitle();
                if (title == null) {
                    str = "";
                } else {
                    str = title;
                }
                title_view.setText(str);
            }
        }
    }

    /* access modifiers changed from: private */
    public void closePopupPages() {
        View view = findViewById(R.id.GamePopup);
        if (view != null) {
            view.setVisibility(4);
        }
        View view2 = findViewById(R.id.GamePopupBack);
        if (view2 != null) {
            view2.setVisibility(4);
        }
        ScrollView scroll_view = (ScrollView) findViewById(R.id.GamePopupScrollView);
        if (scroll_view != null) {
            scroll_view.removeAllViews();
        }
        this.mPopupPages = null;
        this.mPopupListener = null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || this.mPopupPages == null) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mCurrentPopupPage > 0) {
            setCurrentPopupPage(this.mCurrentPopupPage - 1);
        } else {
            closePopupPages();
        }
        return true;
    }

    private class AdRequestRunnable implements Runnable {
        private AdRequestRunnable() {
        }

        /* synthetic */ AdRequestRunnable(GameBaseActivity gameBaseActivity, AdRequestRunnable adRequestRunnable) {
            this();
        }

        public void run() {
            GameBaseActivity.this.mAdView.loadAd(new AdRequest());
        }
    }

    public void onDismissScreen(Ad ad) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode error) {
        this.mViewGroup.postDelayed(new AdRequestRunnable(this, null), 15000);
    }

    public void onLeaveApplication(Ad ad) {
    }

    public void onPresentScreen(Ad ad) {
    }

    public void onReceiveAd(Ad ad) {
    }
}
