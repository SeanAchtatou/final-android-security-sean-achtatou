package com.anoshenko.android.solitaires;

import com.anoshenko.android.ui.ToolbarTheme;

final class MoveMemory {
    private static final byte MT_MOVE_FROM_PACK = 3;
    private static final byte MT_MOVE_NUMBER_INCREMENT = 0;
    private static final byte MT_MOVE_ONE = 1;
    private static final byte MT_MOVE_SERIES = 2;
    private static final byte MT_MOVE_TO_PACK = 4;
    private static final byte MT_OPEN = 6;
    private static final byte MT_ROUND = 7;
    private static final byte MT_TRASH = 5;
    /* access modifiers changed from: private */
    public int[] mBookmark = new int[15];
    /* access modifiers changed from: private */
    public int mBookmarkCount;
    /* access modifiers changed from: private */
    public byte[] mData = new byte[8192];
    /* access modifiers changed from: private */
    public int mDataPos;
    /* access modifiers changed from: private */
    public final GamePlay mGame;
    /* access modifiers changed from: private */
    public int mMoveNumber;
    /* access modifiers changed from: private */
    public int mRedoCount;
    /* access modifiers changed from: private */
    public int mUndoCount;

    MoveMemory(GamePlay game) {
        this.mGame = game;
        this.mBookmarkCount = 0;
        this.mMoveNumber = 0;
        this.mRedoCount = 0;
        this.mUndoCount = 0;
        this.mDataPos = 0;
    }

    /* access modifiers changed from: package-private */
    public final void Reset() {
        this.mDataPos = 0;
        this.mBookmarkCount = 0;
        this.mMoveNumber = 0;
        this.mRedoCount = 0;
        this.mUndoCount = 0;
        this.mGame.updateToolbar();
    }

    /* access modifiers changed from: package-private */
    public final boolean isUndoAvailable() {
        return this.mUndoCount > 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean isRedoAvailable() {
        return this.mRedoCount > 0;
    }

    private final boolean IncreaseDataBuffer() {
        try {
            byte[] new_data = new byte[(this.mData.length + 4096)];
            System.arraycopy(this.mData, 0, new_data, 0, this.mData.length);
            this.mData = new_data;
            return true;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return false;
        }
    }

    private final void addUndoMove(byte type, int add_data_size) {
        int current_add_data;
        boolean f_redraw = this.mUndoCount == 0 || this.mRedoCount > 0;
        if (this.mUndoCount > 0) {
            current_add_data = this.mData[this.mDataPos + 1] & 15;
        } else {
            current_add_data = 0;
        }
        if (this.mDataPos + 4 + current_add_data + add_data_size >= this.mData.length && !IncreaseDataBuffer()) {
            this.mRedoCount = 0;
            this.mUndoCount = 0;
            this.mBookmarkCount = 0;
            f_redraw = true;
        }
        if (this.mUndoCount == 0) {
            this.mDataPos = 0;
        } else {
            this.mDataPos += current_add_data + 2;
        }
        this.mData[this.mDataPos] = type;
        this.mData[this.mDataPos + 1] = (byte) ((current_add_data << 4) | add_data_size);
        this.mUndoCount++;
        this.mRedoCount = 0;
        this.mGame.CorrectAndRedrawIfNeed();
        if (f_redraw) {
            this.mGame.updateToolbar();
        }
    }

    private final byte getPileNumber(Pile pile) {
        Pile[] piles = this.mGame.mPiles;
        for (int i = 0; i < piles.length; i++) {
            if (piles[i] == pile) {
                return (byte) i;
            }
        }
        return MT_MOVE_NUMBER_INCREMENT;
    }

    /* access modifiers changed from: package-private */
    public final void IncreaseMoveNumber() {
        this.mMoveNumber++;
        addUndoMove(MT_MOVE_NUMBER_INCREMENT, 0);
    }

    /* access modifiers changed from: package-private */
    public final void addOneCardMove(Pile from_pile, int number, Pile to_pile) {
        if (from_pile == null) {
            addUndoMove(MT_MOVE_FROM_PACK, 1);
            this.mData[this.mDataPos + 2] = getPileNumber(to_pile);
        } else if (to_pile == null) {
            addCardsToPack(from_pile, 1);
        } else {
            addUndoMove(MT_MOVE_ONE, 3);
            this.mData[this.mDataPos + 2] = (byte) number;
            this.mData[this.mDataPos + 3] = getPileNumber(from_pile);
            this.mData[this.mDataPos + 4] = getPileNumber(to_pile);
        }
    }

    /* access modifiers changed from: package-private */
    public final void addSeriesCardMove(Pile from_pile, Pile to_pile, int count) {
        if (to_pile == null) {
            addCardsToPack(from_pile, count);
            return;
        }
        addUndoMove(MT_MOVE_SERIES, 3);
        this.mData[this.mDataPos + 2] = (byte) count;
        this.mData[this.mDataPos + 3] = getPileNumber(from_pile);
        this.mData[this.mDataPos + 4] = getPileNumber(to_pile);
    }

    /* access modifiers changed from: package-private */
    public final void addCardsToPack(Pile from_pile, int count) {
        addUndoMove(MT_MOVE_TO_PACK, 2);
        this.mData[this.mDataPos + 2] = (byte) count;
        this.mData[this.mDataPos + 3] = getPileNumber(from_pile);
    }

    /* access modifiers changed from: package-private */
    public final void addTrashMove(Pile[] piles, int count) {
        addUndoMove(MT_TRASH, count + 1);
        this.mData[this.mDataPos + 2] = (byte) count;
        for (int i = 0; i < count; i++) {
            this.mData[this.mDataPos + 3 + i] = getPileNumber(piles[i]);
        }
    }

    /* access modifiers changed from: package-private */
    public final void addOpenCard(Pile pile) {
        addUndoMove(MT_OPEN, 1);
        this.mData[this.mDataPos + 2] = getPileNumber(pile);
    }

    /* access modifiers changed from: package-private */
    public final void addRoundIncrease() {
        addUndoMove(MT_ROUND, 0);
    }

    /* access modifiers changed from: package-private */
    public final void Store(BitStack stack) {
        int size = 0;
        stack.add(this.mMoveNumber, 16);
        stack.add(this.mUndoCount, 16);
        stack.add(this.mRedoCount, 16);
        stack.add(this.mDataPos, 16);
        if (this.mUndoCount > 0) {
            size = this.mDataPos + (this.mData[this.mDataPos + 1] & 15) + 2;
        }
        for (int i = 0; i < this.mRedoCount; i++) {
            size += (this.mData[size + 1] & 15) + MT_MOVE_SERIES;
        }
        stack.add(size, 16);
        for (int i2 = 0; i2 < size; i2++) {
            stack.add(this.mData[i2], 8);
        }
        stack.add(this.mBookmarkCount, 4);
        for (int i3 = 0; i3 < this.mBookmarkCount; i3++) {
            stack.add(this.mBookmark[i3], 16);
        }
    }

    /* access modifiers changed from: package-private */
    public final void Load(BitStack stack) {
        this.mMoveNumber = stack.getInt(16);
        this.mUndoCount = stack.getInt(16);
        this.mRedoCount = stack.getInt(16);
        this.mDataPos = stack.getInt(16);
        int size = stack.getInt(16);
        if (size > this.mData.length) {
            this.mData = new byte[(((size + 4095) / 4096) * 4096)];
        }
        for (int i = 0; i < size; i++) {
            this.mData[i] = (byte) stack.getInt(8);
        }
        this.mBookmarkCount = stack.getInt(4);
        for (int i2 = 0; i2 < this.mBookmarkCount; i2++) {
            this.mBookmark[i2] = stack.getInt(16);
        }
    }

    private class UndoAction implements GameAction {
        private final GameAction mNextAction;

        UndoAction(GameAction nextAction) {
            MoveMemory.this.mGame.ResetSelection();
            this.mNextAction = nextAction;
        }

        private void undoFinish(boolean fast) {
            if (MoveMemory.this.mUndoCount > 0) {
                MoveMemory moveMemory = MoveMemory.this;
                moveMemory.mDataPos = moveMemory.mDataPos - (((MoveMemory.this.mData[MoveMemory.this.mDataPos + 1] >> MoveMemory.MT_MOVE_TO_PACK) & 15) + 2);
                MoveMemory moveMemory2 = MoveMemory.this;
                moveMemory2.mUndoCount = moveMemory2.mUndoCount - 1;
                MoveMemory moveMemory3 = MoveMemory.this;
                moveMemory3.mRedoCount = moveMemory3.mRedoCount + 1;
            }
            MoveMemory moveMemory4 = MoveMemory.this;
            moveMemory4.mMoveNumber = moveMemory4.mMoveNumber - 1;
            if (MoveMemory.this.mBookmarkCount > 0) {
                while (MoveMemory.this.mBookmarkCount > 0 && MoveMemory.this.mBookmark[MoveMemory.this.mBookmarkCount - 1] > MoveMemory.this.mMoveNumber) {
                    MoveMemory moveMemory5 = MoveMemory.this;
                    moveMemory5.mBookmarkCount = moveMemory5.mBookmarkCount - 1;
                }
            }
            MoveMemory.this.mGame.updateToolbar();
            if (this.mNextAction == null) {
                return;
            }
            if (!fast) {
                this.mNextAction.run();
            } else {
                this.mNextAction.fastRun();
            }
        }

        public void fastRun() {
            Pile[] piles = MoveMemory.this.mGame.mPiles;
            byte[] data = MoveMemory.this.mData;
            while (MoveMemory.this.mUndoCount > 0 && data[MoveMemory.this.mDataPos] != 0) {
                int pos = MoveMemory.this.mDataPos;
                MoveMemory moveMemory = MoveMemory.this;
                moveMemory.mDataPos = moveMemory.mDataPos - (((data[pos + 1] >> MoveMemory.MT_MOVE_TO_PACK) & 15) + 2);
                MoveMemory moveMemory2 = MoveMemory.this;
                moveMemory2.mUndoCount = moveMemory2.mUndoCount - 1;
                MoveMemory moveMemory3 = MoveMemory.this;
                moveMemory3.mRedoCount = moveMemory3.mRedoCount + 1;
                switch (data[pos]) {
                    case 1:
                        piles[data[pos + 3]].insert(piles[data[pos + 4]].removeLast(), data[pos + 2]);
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 2:
                        piles[data[pos + 3]].add(piles[data[pos + 4]].removeLast(MoveMemory.this.mData[pos + 2]));
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 3:
                        Card card = piles[data[pos + 2]].removeLast();
                        card.mOpen = false;
                        MoveMemory.this.mGame.mPack.mWorkPack.add(card);
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 4:
                        byte b = data[pos + 2];
                        Pile fromPile = piles[data[pos + 3]];
                        for (int i = 0; i < b; i++) {
                            Card card2 = MoveMemory.this.mGame.mPack.mWorkPack.removeLast();
                            card2.mOpen = true;
                            fromPile.add(card2);
                        }
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 5:
                        for (int i2 = data[pos + 2] - MoveMemory.MT_MOVE_ONE; i2 >= 0; i2--) {
                            piles[data[pos + 3 + i2]].add(MoveMemory.this.mGame.mTrash.removeLast());
                            MoveMemory.this.mGame.mNeedCorrect = true;
                        }
                        break;
                    case 6:
                        piles[data[pos + 2]].lastElement().mOpen = false;
                        break;
                    case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                        MoveMemory.this.mGame.mPack.mRoundCurrent++;
                        break;
                }
            }
            MoveMemory.this.mGame.addRedrawRect(MoveMemory.this.mGame.mScreen);
            undoFinish(true);
        }

        public void run() {
            Pile[] piles = MoveMemory.this.mGame.mPiles;
            byte[] data = MoveMemory.this.mData;
            while (MoveMemory.this.mUndoCount > 0 && data[MoveMemory.this.mDataPos] != 0) {
                int pos = MoveMemory.this.mDataPos;
                MoveMemory moveMemory = MoveMemory.this;
                moveMemory.mDataPos = moveMemory.mDataPos - (((data[pos + 1] >> MoveMemory.MT_MOVE_TO_PACK) & 15) + 2);
                MoveMemory moveMemory2 = MoveMemory.this;
                moveMemory2.mUndoCount = moveMemory2.mUndoCount - 1;
                MoveMemory moveMemory3 = MoveMemory.this;
                moveMemory3.mRedoCount = moveMemory3.mRedoCount + 1;
                switch (data[pos]) {
                    case 1:
                        byte b = data[pos + 2];
                        Pile fromPile = piles[data[pos + 3]];
                        Pile toPile = piles[data[pos + 4]];
                        MoveMemory.this.mGame.MoveCard(toPile, toPile != null ? toPile.size() - 1 : 1, fromPile, b, true, this);
                        return;
                    case 2:
                        MoveMemory.this.mGame.MoveCards(piles[data[pos + 4]], piles[data[pos + 3]], data[pos + 2], this);
                        return;
                    case 3:
                        Pile toPile2 = piles[data[pos + 2]];
                        MoveMemory.this.mGame.MoveCard(toPile2, toPile2.size() - 1, null, 1, false, this);
                        return;
                    case 4:
                        MoveMemory.this.mGame.MoveCards(null, piles[data[pos + 3]], data[pos + 2], this);
                        return;
                    case 5:
                        for (int i = MoveMemory.this.mData[pos + 2] - MoveMemory.MT_MOVE_ONE; i >= 0; i--) {
                            Pile pile = piles[data[pos + 3 + i]];
                            pile.add(MoveMemory.this.mGame.mTrash.removeLast());
                            pile.Correct();
                            MoveMemory.this.mGame.addRedrawRect(pile.mCardBounds);
                        }
                        break;
                    case 6:
                        Pile pile2 = piles[data[pos + 2]];
                        pile2.lastElement().mOpen = false;
                        MoveMemory.this.mGame.addRedrawRect(pile2.lastElement().getRect());
                        break;
                    case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                        MoveMemory.this.mGame.mPack.mRoundCurrent++;
                        break;
                }
            }
            undoFinish(false);
        }
    }

    /* access modifiers changed from: package-private */
    public final void Undo() {
        if (this.mUndoCount > 0) {
            new UndoAction(null).run();
        }
    }

    private class RedoAction implements GameAction {
        private RedoAction() {
        }

        /* synthetic */ RedoAction(MoveMemory moveMemory, RedoAction redoAction) {
            this();
        }

        /* access modifiers changed from: package-private */
        public void start() {
            MoveMemory.this.mGame.ResetSelection();
            int pos = MoveMemory.this.mUndoCount > 0 ? MoveMemory.this.mDataPos + (MoveMemory.this.mData[MoveMemory.this.mDataPos + 1] & 15) + 2 : 0;
            if (MoveMemory.this.mData[pos] == 0) {
                MoveMemory.this.mDataPos = pos;
                MoveMemory moveMemory = MoveMemory.this;
                moveMemory.mMoveNumber = moveMemory.mMoveNumber + 1;
                MoveMemory moveMemory2 = MoveMemory.this;
                moveMemory2.mUndoCount = moveMemory2.mUndoCount + 1;
                MoveMemory moveMemory3 = MoveMemory.this;
                moveMemory3.mRedoCount = moveMemory3.mRedoCount - 1;
            }
            run();
        }

        private void redoFinish(boolean fast) {
            MoveMemory.this.mGame.updateToolbar();
        }

        public void fastRun() {
            int pos;
            Pile[] piles = MoveMemory.this.mGame.mPiles;
            if (MoveMemory.this.mUndoCount > 0) {
                pos = MoveMemory.this.mDataPos + (MoveMemory.this.mData[MoveMemory.this.mDataPos + 1] & 15) + 2;
            } else {
                pos = 0;
            }
            while (MoveMemory.this.mRedoCount > 0 && MoveMemory.this.mData[pos] != 0) {
                MoveMemory.this.mDataPos = pos;
                MoveMemory moveMemory = MoveMemory.this;
                moveMemory.mUndoCount = moveMemory.mUndoCount + 1;
                MoveMemory moveMemory2 = MoveMemory.this;
                moveMemory2.mRedoCount = moveMemory2.mRedoCount - 1;
                switch (MoveMemory.this.mData[pos]) {
                    case 1:
                        piles[MoveMemory.this.mData[pos + 4]].add(piles[MoveMemory.this.mData[pos + 3]].remove(MoveMemory.this.mData[pos + 2]));
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 2:
                        piles[MoveMemory.this.mData[pos + 4]].add(piles[MoveMemory.this.mData[pos + 3]].removeLast(MoveMemory.this.mData[pos + 2]));
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 3:
                        Pile toPile = piles[MoveMemory.this.mData[pos + 2]];
                        Card card = MoveMemory.this.mGame.mPack.mWorkPack.removeLast();
                        card.mOpen = true;
                        toPile.add(card);
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 4:
                        byte b = MoveMemory.this.mData[pos + 2];
                        Pile fromPile = piles[MoveMemory.this.mData[pos + 3]];
                        for (int i = 0; i < b; i++) {
                            Card card2 = fromPile.removeLast();
                            card2.mOpen = false;
                            MoveMemory.this.mGame.mPack.mWorkPack.add(card2);
                        }
                        MoveMemory.this.mGame.mNeedCorrect = true;
                        break;
                    case 5:
                        byte b2 = MoveMemory.this.mData[pos + 2];
                        for (int i2 = 0; i2 < b2; i2++) {
                            MoveMemory.this.mGame.mTrash.add(piles[MoveMemory.this.mData[pos + 3 + i2]].removeLast());
                            MoveMemory.this.mGame.mNeedCorrect = true;
                        }
                        break;
                    case 6:
                        piles[MoveMemory.this.mData[pos + 2]].lastElement().mOpen = true;
                        break;
                    case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                        MoveMemory.this.mGame.mPack.mRoundCurrent--;
                        break;
                }
                pos = MoveMemory.this.mDataPos + (MoveMemory.this.mData[MoveMemory.this.mDataPos + 1] & 15) + 2;
            }
            MoveMemory.this.mGame.addRedrawRect(MoveMemory.this.mGame.mScreen);
            redoFinish(true);
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v0, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v1, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v56, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r17v3, resolved type: byte} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r21 = this;
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                r0 = r5
                com.anoshenko.android.solitaires.Pile[] r0 = r0.mPiles
                r19 = r0
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                int r5 = r5.mUndoCount
                if (r5 <= 0) goto L_0x005f
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                int r5 = r5.mDataPos
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r6 = r0
                byte[] r6 = r6.mData
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r7 = r0
                int r7 = r7.mDataPos
                int r7 = r7 + 1
                byte r6 = r6[r7]
                r6 = r6 & 15
                int r5 = r5 + r6
                int r5 = r5 + 2
                r20 = r5
            L_0x003f:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                int r5 = r5.mRedoCount
                if (r5 <= 0) goto L_0x0057
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                byte r5 = r5[r20]
                if (r5 != 0) goto L_0x0063
            L_0x0057:
                r5 = 0
                r0 = r21
                r1 = r5
                r0.redoFinish(r1)
            L_0x005e:
                return
            L_0x005f:
                r5 = 0
                r20 = r5
                goto L_0x003f
            L_0x0063:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                r0 = r5
                r1 = r20
                r0.mDataPos = r1
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                int r6 = r5.mUndoCount
                int r6 = r6 + 1
                r5.mUndoCount = r6
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                int r6 = r5.mRedoCount
                r7 = 1
                int r6 = r6 - r7
                r5.mRedoCount = r6
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                byte r5 = r5[r20]
                switch(r5) {
                    case 1: goto L_0x00bd;
                    case 2: goto L_0x00fd;
                    case 3: goto L_0x013d;
                    case 4: goto L_0x0164;
                    case 5: goto L_0x0196;
                    case 6: goto L_0x01e5;
                    case 7: goto L_0x0211;
                    default: goto L_0x0098;
                }
            L_0x0098:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                int r5 = r5.mDataPos
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r6 = r0
                byte[] r6 = r6.mData
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r7 = r0
                int r7 = r7.mDataPos
                int r7 = r7 + 1
                byte r6 = r6[r7]
                r6 = r6 & 15
                int r5 = r5 + r6
                int r20 = r5 + 2
                goto L_0x003f
            L_0x00bd:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 2
                byte r7 = r5[r6]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 3
                byte r5 = r5[r6]
                r6 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r8 = r20 + 4
                byte r5 = r5[r8]
                r8 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                int r9 = r8.size()
                r10 = 1
                r11 = r21
                r5.MoveCard(r6, r7, r8, r9, r10, r11)
                goto L_0x005e
            L_0x00fd:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 2
                byte r16 = r5[r6]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 3
                byte r5 = r5[r6]
                r6 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r7 = r20 + 4
                byte r5 = r5[r7]
                r8 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                r0 = r5
                r1 = r6
                r2 = r8
                r3 = r16
                r4 = r21
                r0.MoveCards(r1, r2, r3, r4)
                goto L_0x005e
            L_0x013d:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 2
                byte r5 = r5[r6]
                r8 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r9 = r5.mGame
                r10 = 0
                r11 = 1
                int r13 = r8.size()
                r14 = 1
                r12 = r8
                r15 = r21
                r9.MoveCard(r10, r11, r12, r13, r14, r15)
                goto L_0x005e
            L_0x0164:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 2
                byte r16 = r5[r6]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 3
                byte r5 = r5[r6]
                r6 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                r7 = 0
                r0 = r5
                r1 = r6
                r2 = r7
                r3 = r16
                r4 = r21
                r0.MoveCards(r1, r2, r3, r4)
                goto L_0x005e
            L_0x0196:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 2
                byte r16 = r5[r6]
                r17 = 0
            L_0x01a5:
                r0 = r17
                r1 = r16
                if (r0 >= r1) goto L_0x0098
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 3
                int r6 = r6 + r17
                byte r5 = r5[r6]
                r18 = r19[r5]
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                com.anoshenko.android.solitaires.CardList r5 = r5.mTrash
                com.anoshenko.android.solitaires.Card r6 = r18.removeLast()
                r5.add(r6)
                r18.Correct()
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                r0 = r18
                android.graphics.Rect r0 = r0.mCardBounds
                r6 = r0
                r5.addRedrawRect(r6)
                int r17 = r17 + 1
                goto L_0x01a5
            L_0x01e5:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                byte[] r5 = r5.mData
                int r6 = r20 + 2
                byte r5 = r5[r6]
                r18 = r19[r5]
                com.anoshenko.android.solitaires.Card r5 = r18.lastElement()
                r6 = 1
                r5.mOpen = r6
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                com.anoshenko.android.solitaires.Card r6 = r18.lastElement()
                android.graphics.Rect r6 = r6.getRect()
                r5.addRedrawRect(r6)
                goto L_0x0098
            L_0x0211:
                r0 = r21
                com.anoshenko.android.solitaires.MoveMemory r0 = com.anoshenko.android.solitaires.MoveMemory.this
                r5 = r0
                com.anoshenko.android.solitaires.GamePlay r5 = r5.mGame
                com.anoshenko.android.solitaires.Pack r5 = r5.mPack
                int r6 = r5.mRoundCurrent
                r7 = 1
                int r6 = r6 - r7
                r5.mRoundCurrent = r6
                goto L_0x0098
            */
            throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.MoveMemory.RedoAction.run():void");
        }
    }

    /* access modifiers changed from: package-private */
    public void Redo() {
        if (this.mRedoCount > 0) {
            new RedoAction(this, null).start();
        }
    }

    /* access modifiers changed from: package-private */
    public final void setBookmark() {
        if (this.mBookmarkCount <= 0 || this.mBookmark[this.mBookmarkCount - 1] != this.mMoveNumber) {
            if (this.mBookmarkCount == this.mBookmark.length) {
                for (int i = 1; i < this.mBookmarkCount; i++) {
                    this.mBookmark[i - 1] = this.mBookmark[i];
                }
                this.mBookmarkCount--;
            }
            this.mBookmark[this.mBookmarkCount] = this.mMoveNumber;
            this.mBookmarkCount++;
            if (this.mBookmarkCount == 1) {
                this.mGame.updateToolbar();
            }
        }
    }

    private class BackToBookmarkAction implements GameAction {
        private BackToBookmarkAction() {
        }

        /* synthetic */ BackToBookmarkAction(MoveMemory moveMemory, BackToBookmarkAction backToBookmarkAction) {
            this();
        }

        public void fastRun() {
            UndoAction action = new UndoAction(null);
            while (MoveMemory.this.mBookmark[MoveMemory.this.mBookmarkCount - 1] < MoveMemory.this.mMoveNumber) {
                action.fastRun();
            }
        }

        public void run() {
            if (MoveMemory.this.mBookmark[MoveMemory.this.mBookmarkCount - 1] < MoveMemory.this.mMoveNumber) {
                new UndoAction(this).run();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void backToBookmark() {
        this.mGame.ResetSelection();
        if (this.mBookmarkCount > 0 && this.mBookmark[this.mBookmarkCount - 1] == this.mMoveNumber) {
            this.mBookmarkCount--;
        }
        if (this.mBookmarkCount > 0) {
            new BackToBookmarkAction(this, null).run();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean isBookmarkAvailable() {
        return this.mBookmarkCount > 0;
    }
}
