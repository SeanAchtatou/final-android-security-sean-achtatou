package com.anoshenko.android.solitaires;

/* compiled from: Condition */
final class ConditionElement extends Condition {
    private boolean mCardFromEnd;
    private int mCardNumber;
    private CardOrder mCardRule;
    private Game mGame;
    private int mGroupNumber;
    private int mPileNumber;
    private int mPileNumberRule;
    private int mSize;
    private int mSizeType;
    private int mType;

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    ConditionElement(Game game) {
        this.mGame = game;
        BitStack stack = game.mSource.mRule;
        this.mType = stack.getInt(3);
        if (this.mType != 2) {
            this.mGroupNumber = stack.getInt(game.mIndexSize);
            if (this.mType != 6) {
                this.mPileNumberRule = stack.getInt(2);
                this.mPileNumber = this.mPileNumberRule != 1 ? stack.getIntF(4, 10) : 0;
            } else {
                this.mPileNumberRule = 0;
                this.mPileNumber = 0;
            }
        } else {
            this.mPileNumberRule = 0;
            this.mPileNumber = 0;
        }
        switch (this.mType) {
            case 0:
                this.mCardFromEnd = stack.getFlag();
                this.mCardNumber = stack.getInt(1, 6);
                break;
            case 1:
            case 2:
                this.mSizeType = stack.getInt(1, 2);
                if (stack.getFlag()) {
                    this.mSize = stack.getIntF(4, 13) + 1;
                    return;
                } else {
                    this.mSize = 0;
                    return;
                }
            case 3:
            case 4:
                break;
            default:
                return;
        }
        this.mCardRule = new CardOrder(game);
    }

    /* access modifiers changed from: package-private */
    public final Pile getTestPile(int current_pile_number) {
        int n;
        PileGroup group = this.mGame.mGroup[this.mGroupNumber];
        if (group == null) {
            return null;
        }
        switch (this.mPileNumberRule) {
            case 1:
                n = current_pile_number;
                break;
            case 2:
                n = current_pile_number + this.mPileNumber;
                break;
            case 3:
                n = current_pile_number - this.mPileNumber;
                break;
            default:
                n = this.mPileNumber;
                break;
        }
        if (n < 0 || n >= group.mPile.length) {
            return null;
        }
        return group.mPile[n];
    }

    /* access modifiers changed from: package-private */
    public final boolean Examine(int card_mask, int current_pile_number, PileGroup src_group) {
        Card test_card;
        PileGroup group = this.mGame.mGroup[this.mGroupNumber];
        switch (this.mType) {
            case 0:
                Pile pile = getTestPile(current_pile_number);
                if (pile == null) {
                    return true;
                }
                if (this.mCardNumber >= pile.size()) {
                    return false;
                }
                if (this.mCardFromEnd) {
                    test_card = pile.lastElement();
                    for (int i = 0; i < this.mCardNumber; i++) {
                        test_card = test_card.prev;
                    }
                } else {
                    Card test_card2 = pile.firstElement();
                    for (int i2 = 0; i2 < this.mCardNumber; i2++) {
                        test_card2 = test_card.next;
                    }
                }
                int mask = test_card.mCardMask & this.mCardRule.getPrevCard(card_mask);
                if ((983040 & mask) == 0 || (mask & 16383) == 0) {
                    return false;
                }
                return true;
            case 1:
                Pile pile2 = getTestPile(current_pile_number);
                if (pile2 == null) {
                    return true;
                }
                switch (this.mSizeType) {
                    case 0:
                        return pile2.size() == this.mSize;
                    case 1:
                        return pile2.size() < this.mSize;
                    case 2:
                        return pile2.size() > this.mSize;
                    default:
                        return false;
                }
            case 2:
                switch (this.mSizeType) {
                    case 0:
                        return this.mGame.mPack.mWorkPack.size() == this.mSize;
                    case 1:
                        return this.mGame.mPack.mWorkPack.size() < this.mSize;
                    case 2:
                        return this.mGame.mPack.mWorkPack.size() > this.mSize;
                    default:
                        return false;
                }
            case 3:
                Pile pile3 = getTestPile(current_pile_number);
                if (pile3 == null) {
                    return true;
                }
                Card work_card = pile3.firstElement();
                for (int i3 = 0; i3 < pile3.size(); i3++) {
                    int mask2 = this.mCardRule.getNextCard(work_card.mCardMask) & card_mask;
                    if ((983040 & mask2) != 0 && (mask2 & 16383) != 0) {
                        return true;
                    }
                    work_card = work_card.next;
                }
                return false;
            case 4:
                Pile pile4 = getTestPile(current_pile_number);
                if (pile4 == null || pile4.size() <= 1) {
                    return true;
                }
                Card work_card2 = pile4.firstElement();
                int k = this.mCardRule.getNextCard(work_card2.mCardMask);
                for (int i4 = 1; i4 < pile4.size(); i4++) {
                    work_card2 = work_card2.next;
                    int n = work_card2.mCardMask;
                    if ((k & n) != n) {
                        return false;
                    }
                    k = this.mCardRule.getNextCard(n);
                }
                return true;
            case 5:
                Pile pile5 = getTestPile(current_pile_number);
                if (pile5 != null) {
                    return pile5.size() == 0 || pile5.lastElement().mOpen;
                }
                return true;
            case 6:
                if (src_group == group || group == null || src_group == null) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getMaxSize(Pile pile) {
        int result = this.mGame.mPack.getMaxSize();
        if (this.mType == 1 && pile == getTestPile(pile.mNumber) && this.mSizeType == 1) {
            return this.mSize;
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public final boolean isMaxSizeCondition(Pile pile) {
        if (this.mType == 1 && pile == getTestPile(pile.mNumber) && this.mSizeType == 1) {
            return true;
        }
        return false;
    }
}
