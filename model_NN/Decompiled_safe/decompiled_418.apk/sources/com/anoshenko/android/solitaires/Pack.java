package com.anoshenko.android.solitaires;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.anoshenko.android.data.GameRules;
import java.lang.reflect.Array;
import java.util.Iterator;

public final class Pack {
    private int mAvailableCards;
    public Rect mBounds = new Rect();
    private boolean mFinishNotEmpty;
    public final Game mGame;
    private Card[] mGamePack;
    private int mOpenCount;
    private int mRoundCount;
    public int mRoundCurrent;
    private byte[] mStartPack;
    private boolean mUse;
    public CardList mWorkPack = new CardList();
    private final CoordinateX xLandscapePos;
    private final CoordinateX xPos;
    private final CoordinateY yLandscapePos;
    private final CoordinateY yPos;

    Pack(Game game) {
        int joker_count;
        this.mGame = game;
        BitStack stack = game.mSource.mRule;
        this.mUse = stack.getFlag();
        if (this.mUse) {
            this.mOpenCount = stack.getInt(3, 5) + 1;
            this.mRoundCount = stack.getInt(3, 4);
            this.mFinishNotEmpty = stack.getFlag();
        } else {
            this.mOpenCount = 0;
            this.mRoundCount = 0;
            this.mFinishNotEmpty = false;
        }
        int deck_count = stack.getInt(2, 4) + 1;
        if (stack.getFlag()) {
            joker_count = stack.getInt(3, 5) + 1;
        } else {
            joker_count = 0;
        }
        boolean f_equal_deck = true;
        int n = 1;
        if (deck_count > 1 && !(f_equal_deck = stack.getFlag())) {
            n = deck_count;
        }
        boolean[] f_full_deck = new boolean[deck_count];
        boolean[] f_equal_suit = new boolean[deck_count];
        int[][] deck = (int[][]) Array.newInstance(Integer.TYPE, deck_count, 4);
        for (int i = 0; i < n; i++) {
            f_full_deck[i] = stack.getFlag();
            if (!f_full_deck[i]) {
                f_equal_suit[i] = stack.getFlag();
                deck[i][0] = stack.getInt(13);
                if (!f_equal_suit[i]) {
                    for (int k = 1; k < 4; k++) {
                        deck[i][k] = stack.getInt(13);
                    }
                } else {
                    for (int k2 = 1; k2 < 4; k2++) {
                        deck[i][k2] = deck[i][0];
                    }
                }
            }
        }
        this.xPos = new CoordinateX(game.mSource);
        this.yPos = new CoordinateY(game.mSource);
        if (stack.getFlag()) {
            this.xLandscapePos = new CoordinateX(game.mSource);
            this.yLandscapePos = new CoordinateY(game.mSource);
        } else {
            this.xLandscapePos = this.xPos;
            this.yLandscapePos = this.yPos;
        }
        if (f_equal_deck) {
            if (f_full_deck[0]) {
                for (int i2 = 0; i2 < 4; i2++) {
                    deck[0][i2] = 8191;
                }
            } else if (f_equal_suit[0]) {
                for (int i3 = 1; i3 < 4; i3++) {
                    deck[0][i3] = deck[0][0];
                }
            }
            for (int k3 = 1; k3 < deck_count; k3++) {
                for (int i4 = 0; i4 < 4; i4++) {
                    deck[k3][i4] = deck[0][i4];
                }
            }
        } else {
            for (int k4 = 0; k4 < deck_count; k4++) {
                if (f_full_deck[k4]) {
                    for (int i5 = 0; i5 < 4; i5++) {
                        deck[k4][i5] = 8191;
                    }
                } else if (f_equal_suit[k4]) {
                    for (int i6 = 1; i6 < 4; i6++) {
                        deck[k4][i6] = deck[k4][0];
                    }
                }
            }
        }
        init(deck, joker_count);
    }

    Pack(Game game, GameRules rules) {
        this.mGame = game;
        this.mUse = true;
        this.mOpenCount = rules.mOpenCount;
        this.mRoundCount = rules.mRoundCount;
        this.mFinishNotEmpty = false;
        this.xPos = new CoordinateX();
        this.yPos = new CoordinateY();
        this.xLandscapePos = this.xPos;
        this.yLandscapePos = this.yPos;
        int[][] deck = (int[][]) Array.newInstance(Integer.TYPE, rules.mPackCount, 4);
        for (int i = 0; i < 4; i++) {
            deck[0][i] = 8191;
        }
        for (int k = 1; k < deck.length; k++) {
            for (int i2 = 0; i2 < 4; i2++) {
                deck[k][i2] = deck[0][i2];
            }
        }
        init(deck, rules.mJokerCount);
    }

    private void init(int[][] deck, int joker_count) {
        int available_cards;
        for (int i = 0; i < joker_count; i++) {
            this.mWorkPack.add(new Card(0, 0, this.mGame.mCardData));
        }
        if (joker_count > 0) {
            available_cards = 1;
        } else {
            available_cards = 0;
        }
        int mask = 1;
        for (int j = 1; j <= 13; j++) {
            for (int k = 0; k < deck.length; k++) {
                for (int i2 = 0; i2 < 4; i2++) {
                    if ((deck[k][i2] & mask) != 0) {
                        CardList cardList = this.mWorkPack;
                        Card card = new Card(i2, j, this.mGame.mCardData);
                        cardList.add(card);
                        available_cards |= card.mCardMask;
                    }
                }
            }
            mask <<= 1;
        }
        this.mAvailableCards = available_cards;
        if (this.mWorkPack.size() > 0) {
            int cnt = this.mWorkPack.size();
            this.mGamePack = new Card[cnt];
            this.mStartPack = new byte[cnt];
            Card card2 = this.mWorkPack.firstElement();
            for (int i3 = 0; i3 < cnt; i3++) {
                this.mGamePack[i3] = card2;
                card2 = card2.next;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setCardData(CardData data) {
        for (Card card : this.mGamePack) {
            card.setCardData(data);
        }
    }

    /* access modifiers changed from: package-private */
    public final void StoreState(BitStack stack) {
        for (int i = 0; i < this.mGamePack.length; i++) {
            stack.add(this.mStartPack[i], 6);
        }
        if (this.mUse) {
            if (this.mRoundCount > 0 && this.mRoundCount < 11) {
                stack.add(this.mRoundCurrent, 4);
            }
            stack.add(this.mWorkPack.size(), 9);
            Iterator<Card> it = this.mWorkPack.iterator();
            while (it.hasNext()) {
                Card card = it.next();
                stack.add(card.mSuit, 2);
                stack.add(card.mValue, 4);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void LoadState(BitStack stack) {
        this.mWorkPack.clear();
        for (int i = 0; i < this.mGamePack.length; i++) {
            this.mStartPack[i] = (byte) stack.getInt(6);
            this.mWorkPack.add(this.mGamePack[i]);
        }
        if (this.mUse) {
            this.mRoundCurrent = (this.mRoundCount <= 0 || this.mRoundCount >= 11) ? this.mRoundCount : stack.getInt(4);
            int size = stack.getInt(9);
            for (int i2 = 0; i2 < size; i2++) {
                int suit = stack.getInt(2);
                int value = stack.getInt(4);
                Card card = this.mWorkPack.lastElement();
                int k = this.mWorkPack.size() - 1;
                while (true) {
                    if (k > i2) {
                        if (card.mValue == value && card.mSuit == suit) {
                            this.mWorkPack.insert(this.mWorkPack.remove(k), i2);
                            break;
                        } else {
                            card = card.prev;
                            k--;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
    }

    public final void Correct() {
        int x;
        int y;
        CardData data = this.mGame.mCardData;
        if (data != null) {
            if (this.mGame.mScreenType == 1) {
                x = this.xLandscapePos.getLeft(this.mGame.mScreen.left, this.mGame.mScreen.width(), data);
                y = this.yLandscapePos.getTop(this.mGame.mScreen.top, this.mGame.mScreen.height(), data);
            } else {
                x = this.xPos.getLeft(this.mGame.mScreen.left, this.mGame.mScreen.width(), data);
                y = this.yPos.getTop(this.mGame.mScreen.top, this.mGame.mScreen.height(), data);
            }
            if (this.mGame.isMirrorLayout()) {
                x = this.mGame.mScreen.left + (((this.mGame.mScreen.width() - x) - data.Width) - 2);
            }
            this.mBounds.left = x;
            this.mBounds.top = y;
            this.mBounds.right = data.Width + x;
            this.mBounds.bottom = data.Height + y;
            Card card = this.mWorkPack.firstElement();
            switch (this.mWorkPack.size()) {
                case 0:
                    return;
                case 1:
                    card.mOpen = false;
                    card.mLock = false;
                    card.mMark = false;
                    card.xPos = x;
                    card.yPos = y;
                    card.mNextOffset = 0;
                    return;
                default:
                    this.mBounds.right += 2;
                    this.mBounds.bottom += 2;
                    card.mOpen = false;
                    card.mLock = false;
                    card.mMark = false;
                    card.xPos = x;
                    card.yPos = y;
                    card.mNextOffset = 0;
                    int x2 = x + 2;
                    int y2 = y + 2;
                    for (int i = 1; i < this.mWorkPack.size(); i++) {
                        card = card.next;
                        card.mOpen = false;
                        card.mLock = false;
                        card.mMark = false;
                        card.xPos = x2;
                        card.yPos = y2;
                        card.mNextOffset = 0;
                    }
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void StartDeal(byte[] start_pack) {
        this.mWorkPack.clear();
        CardList list = new CardList(this.mGamePack);
        if (start_pack != null) {
            for (int i = 0; i < this.mGamePack.length; i++) {
                int suit = start_pack[i] >> 4;
                int value = start_pack[i] & 15;
                int n = 0;
                Iterator<Card> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        Card card = it.next();
                        if (card.mSuit == suit && card.mValue == value) {
                            this.mWorkPack.add(list.remove(n));
                            break;
                        }
                        n++;
                    } else {
                        break;
                    }
                }
            }
            if (list.size() > 0) {
                this.mWorkPack.add(list);
            }
        } else {
            Deal(list, this.mWorkPack);
        }
        Correct();
        int i2 = 0;
        Iterator<Card> it2 = this.mWorkPack.iterator();
        while (it2.hasNext()) {
            Card card2 = it2.next();
            this.mStartPack[i2] = (byte) ((card2.mSuit << 4) | card2.mValue);
            i2++;
        }
        this.mRoundCurrent = this.mRoundCount;
    }

    /* access modifiers changed from: package-private */
    public final void Deal(CardList src_pack, CardList result_pack) {
        int rand = (int) (System.currentTimeMillis() & 2147483647L);
        result_pack.clear();
        while (src_pack.size() > 1) {
            Card card = src_pack.remove(rand % src_pack.size());
            if (card != null) {
                card.mLock = false;
                result_pack.add(card);
            }
            rand = (1220703125 * rand) & Integer.MAX_VALUE;
        }
        Card card2 = src_pack.removeLast();
        if (card2 != null) {
            card2.mLock = false;
            result_pack.add(card2);
        }
    }

    /* access modifiers changed from: package-private */
    public final void draw(Canvas g) {
        Card card;
        CardData data = this.mGame.mCardData;
        if (data != null) {
            switch (this.mWorkPack.size()) {
                case 0:
                    if (isAvailable()) {
                        data.DrawEmpty(g, this.mBounds.left, this.mBounds.top, -1);
                        drawSizeText(g, data, this.mBounds.left, this.mBounds.top);
                        return;
                    }
                    return;
                case 1:
                    card = this.mWorkPack.firstElement();
                    card.draw(g, null);
                    break;
                default:
                    this.mWorkPack.firstElement().draw(g, null);
                    card = this.mWorkPack.lastElement();
                    card.draw(g, null);
                    break;
            }
            drawSizeText(g, data, card.xPos, card.yPos);
        }
    }

    private void drawSizeText(Canvas g, CardData data, int card_x, int card_y) {
        if (!this.mGame.isHidePackSize()) {
            String text = Integer.toString(this.mWorkPack.size());
            if (this.mRoundCurrent < 11 && !this.mGame.isHidePackRedeal()) {
                text = String.valueOf(text) + "/" + this.mRoundCurrent;
            }
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            int min_size = Utils.getMinDislpaySide(this.mGame.mActivity);
            if (min_size >= 480) {
                paint.setTextSize((float) (data.mSizeType == CardSize.SMALL ? 18 : 24));
            } else if (min_size < 320) {
                paint.setTextSize((float) (data.mSizeType == CardSize.SMALL ? 9 : 12));
            } else {
                paint.setTextSize((float) (data.mSizeType == CardSize.SMALL ? 14 : 18));
            }
            int text_width = (int) paint.measureText(text);
            Paint.FontMetricsInt metrics = paint.getFontMetricsInt();
            int height = (metrics.bottom - metrics.top) + 2;
            int width = text_width + 6 <= height ? height : text_width + 6;
            int x = card_x + ((data.Width - width) / 2);
            int y = card_y + ((data.Height - height) / 2);
            int angle = height / 6;
            RectF rectF = new RectF((float) x, (float) y, (float) (x + width), (float) (y + height));
            Paint rect_paint = new Paint();
            rect_paint.setAntiAlias(true);
            rect_paint.setStyle(Paint.Style.FILL);
            rect_paint.setColor(-1056964609);
            g.drawRoundRect(rectF, (float) angle, (float) angle, rect_paint);
            rect_paint.setStyle(Paint.Style.STROKE);
            rect_paint.setColor(-16776961);
            rect_paint.setStrokeWidth(2.0f);
            g.drawRoundRect(rectF, (float) angle, (float) angle, rect_paint);
            int y2 = y - (metrics.top - 1);
            paint.setColor(-16777216);
            g.drawText(text, (float) (x + ((width - ((int) paint.measureText(text))) / 2)), (float) y2, paint);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean isAvailable() {
        if (this.mUse) {
            if (this.mWorkPack.size() > 0) {
                return true;
            }
            if (this.mRoundCurrent > 0) {
                for (PileGroup group : this.mGame.mGroup) {
                    if (group.mPackPile && group.isEnableUse()) {
                        for (Pile pile : group.mPile) {
                            if (pile.size() > 0) {
                                return true;
                            }
                        }
                        continue;
                    }
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean isAvailableMove() {
        if (this.mUse) {
            if (this.mWorkPack.size() != 0) {
                int stock_count = 0;
                Pile stock = null;
                for (PileGroup group : this.mGame.mGroup) {
                    if (group.mPackPile && group.isEnableUse()) {
                        for (Pile pile : group.mPile) {
                            if (pile.size() > 0) {
                                return true;
                            }
                        }
                        stock_count += group.mPile.length;
                        stock = group.mPile[0];
                    }
                }
                if (stock_count > 1) {
                    return true;
                }
                if (this.mGame.mGameType != 1) {
                    Card card = this.mWorkPack.lastElement();
                    int n = this.mWorkPack.size() - 1;
                    do {
                        int i = 1;
                        while (i < this.mOpenCount && n > 0) {
                            card = card.prev;
                            i++;
                            n--;
                        }
                        card.mOpen = true;
                        for (PileGroup group2 : this.mGame.mGroup) {
                            if (!group2.mPackPile && group2.isEnableUse()) {
                                for (Pile pile2 : group2.mPile) {
                                    if (pile2.isEnableAdd(stock, 1, card)) {
                                        card.mOpen = false;
                                        return true;
                                    }
                                }
                                continue;
                            }
                        }
                        card.mOpen = false;
                        if (n > 0) {
                            card = card.prev;
                        }
                        n--;
                    } while (n > 0);
                } else if (this.mGame.mTrashRule.getMaxTrashCards() > 2) {
                    return true;
                } else {
                    Pile[] mark_pile = new Pile[2];
                    mark_pile[0] = stock;
                    int n2 = this.mWorkPack.size() - this.mOpenCount;
                    while (n2 >= 0) {
                        if (TestType1StockCard(mark_pile, n2)) {
                            return true;
                        }
                        n2 -= this.mOpenCount;
                    }
                    if (this.mWorkPack.size() % this.mOpenCount <= 0 || !TestType1StockCard(mark_pile, 0)) {
                        return false;
                    }
                    return true;
                }
            } else if (this.mRoundCurrent > 0) {
                for (PileGroup group3 : this.mGame.mGroup) {
                    if (group3.mPackPile) {
                        for (Pile pile3 : group3.mPile) {
                            if (pile3.size() > 0) {
                                return true;
                            }
                        }
                        continue;
                    }
                }
                return false;
            }
        }
        return false;
    }

    private final boolean TestType1StockCard(Pile[] mark_pile, int number) {
        Card card = this.mWorkPack.remove(number);
        card.mOpen = true;
        mark_pile[0].add(card);
        boolean f_result = this.mGame.mTrashRule.TestTrashPiles(mark_pile, 1);
        if (!f_result) {
            PileGroup[] pileGroupArr = this.mGame.mGroup;
            int length = pileGroupArr.length;
            int i = 0;
            loop0:
            while (true) {
                if (i >= length) {
                    break;
                }
                PileGroup group = pileGroupArr[i];
                if (!group.mPackPile && group.isEnableUse()) {
                    for (Pile pile : group.mPile) {
                        if (pile.isEnableType1Remove()) {
                            mark_pile[1] = pile;
                            if (this.mGame.mTrashRule.TestTrashPiles(mark_pile, 2)) {
                                f_result = true;
                                break loop0;
                            }
                        }
                    }
                    continue;
                }
                i++;
            }
        }
        mark_pile[0].removeLast();
        card.mOpen = false;
        this.mWorkPack.insert(card, number);
        return f_result;
    }

    /* access modifiers changed from: package-private */
    public boolean isBelong(int x, int y) {
        return this.mBounds.contains(x, y);
    }

    /* access modifiers changed from: package-private */
    public boolean isAround(int x, int y) {
        if (!isAvailable()) {
            return false;
        }
        if (this.mBounds.contains(x, y)) {
            return true;
        }
        int distance = Integer.MAX_VALUE;
        if (x < this.mBounds.left) {
            distance = this.mBounds.left - x;
        } else if (x >= this.mBounds.right) {
            distance = x - this.mBounds.right;
        }
        if (y < this.mBounds.top) {
            distance = Math.max(distance, this.mBounds.top - y);
        } else if (y >= this.mBounds.bottom) {
            distance = Math.max(distance, y - this.mBounds.bottom);
        }
        if (distance <= this.mGame.mTouchArrea) {
            return true;
        }
        return false;
    }

    public boolean isUse() {
        return this.mUse;
    }

    public boolean isFinish() {
        return !this.mUse || this.mFinishNotEmpty || this.mWorkPack.size() == 0;
    }

    public int getAvailableCards() {
        return this.mAvailableCards;
    }

    public int getOpenCount() {
        return this.mOpenCount;
    }

    public byte[] getStartPack() {
        return this.mStartPack;
    }

    public int getMaxSize() {
        return this.mGamePack.length;
    }
}
