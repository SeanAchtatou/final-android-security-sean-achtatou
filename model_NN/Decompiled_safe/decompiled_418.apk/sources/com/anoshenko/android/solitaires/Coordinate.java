package com.anoshenko.android.solitaires;

abstract class Coordinate {
    protected final boolean mOffset;
    protected final int mPos;
    protected final int mType;

    Coordinate(DataSource source) {
        this.mType = source.mRule.getInt(2);
        int pos = source.mRule.getInt(1, 5);
        if (this.mType > 1 && pos != 0) {
            pos = pos < 9 ? pos - 9 : pos - 8;
        }
        this.mPos = pos;
        this.mOffset = source.mRule.getFlag();
    }

    Coordinate() {
        this.mPos = 0;
        this.mType = 0;
        this.mOffset = false;
    }
}
