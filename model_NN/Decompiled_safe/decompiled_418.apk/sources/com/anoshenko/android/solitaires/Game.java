package com.anoshenko.android.solitaires;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.util.Log;
import android.view.View;
import com.anoshenko.android.data.GameRules;
import com.anoshenko.android.solitaires.CardData;
import com.anoshenko.android.ui.CardMoveView;
import com.anoshenko.android.ui.CommandListener;
import com.anoshenko.android.ui.ToolbarButton;
import java.util.Iterator;

public abstract class Game implements CommandListener {
    static final int ADDITION_OFFSET = 2;
    static final int AUTOPLAY_ALL = 3;
    static final int AUTOPLAY_DISABLED = 0;
    static final int AUTOPLAY_ONE = 1;
    static final int AUTOPLAY_TWO = 2;
    static final int SCREEN_LANDSCAPE = 1;
    static final int SCREEN_NORMAL = 0;
    static final int UNLIMITED_PACK_ROUND = 11;
    int RedealCurrent;
    public final GameBaseActivity mActivity;
    private final int mAutoplayType;
    CardData mCardData;
    boolean mDeal = false;
    final boolean mDealByOne;
    public AdditionalDraw mDraw = null;
    final boolean mEnableMirror;
    final boolean mEnableRedeal;
    protected int mGameId;
    protected String mGameName;
    final int mGameType;
    final PileGroup[] mGroup;
    final int mIndexSize;
    final int mLanscapeCardSize;
    public boolean mNeedCorrect;
    final boolean mNoWrap;
    final int mNormalCardSize;
    public final Pack mPack;
    private Paint mPaint = new Paint();
    final Pile[] mPiles;
    final int mRedealCount;
    final boolean mRedealNotDeal;
    public Rect mRedrawRect;
    Rect mScreen = new Rect();
    int mScreenType;
    final DataSource mSource;
    public boolean mStarted;
    public final int mTouchArrea;
    final CardList mTrash = new CardList();
    final CardOrder mTrashRule;
    final boolean mUseCardLock;
    final View mView;

    public abstract void initToolbar();

    public Game(GameActivity activity, View view, DataSource source) {
        this.mView = view;
        this.mActivity = activity;
        this.mTouchArrea = (int) (16.0f * activity.getResources().getDisplayMetrics().density);
        this.mSource = source;
        this.mDeal = false;
        BitStack stack = source.mRule;
        stack.ResetPos();
        this.mGameType = activity.mGameType;
        this.mGameName = activity.mGameName;
        this.mGameId = activity.mGameId;
        this.mEnableMirror = stack.getFlag();
        if (this.mEnableMirror) {
            stack.getFlag();
        }
        this.mNormalCardSize = stack.getInt(1);
        if (stack.getFlag()) {
            this.mLanscapeCardSize = stack.getInt(1);
        } else {
            this.mLanscapeCardSize = this.mNormalCardSize;
        }
        this.mDealByOne = stack.getFlag();
        this.mEnableRedeal = stack.getFlag();
        if (this.mEnableRedeal) {
            this.mRedealCount = stack.getIntF(3, 4) + 1;
            this.mRedealNotDeal = stack.getFlag();
        } else {
            this.mRedealCount = 0;
            this.mRedealNotDeal = false;
        }
        if (this.mGameType == 1) {
            this.mAutoplayType = 0;
            this.mNoWrap = false;
            this.mUseCardLock = false;
            this.mTrashRule = new CardOrder(this);
        } else {
            this.mAutoplayType = stack.getInt(2);
            this.mNoWrap = stack.getFlag();
            this.mUseCardLock = this.mAutoplayType > 1;
            this.mTrashRule = null;
        }
        this.mPack = new Pack(this);
        this.mIndexSize = stack.getIntF(2, 4) + 1;
        int group_count = stack.getInt(this.mIndexSize) + 1;
        this.mGroup = new PileGroup[group_count];
        int pile_count = 0;
        for (int i = 0; i < group_count; i++) {
            this.mGroup[i] = new PileGroup(i, this);
            pile_count += this.mGroup[i].mPile.length;
        }
        this.mPiles = new Pile[pile_count];
        int pile_count2 = 0;
        for (PileGroup group : this.mGroup) {
            for (Pile pile : group.mPile) {
                this.mPiles[pile_count2] = pile;
                pile_count2++;
            }
        }
    }

    public Game(GameBaseActivity activity, View view, GameRules rules) {
        boolean z;
        boolean z2;
        this.mView = view;
        this.mActivity = activity;
        this.mTouchArrea = (int) (16.0f * activity.getResources().getDisplayMetrics().density);
        this.mSource = null;
        this.mDeal = false;
        this.mGameType = 0;
        this.mGameName = rules.mName;
        this.mGameId = 65536;
        this.mEnableMirror = true;
        this.mLanscapeCardSize = 1;
        this.mNormalCardSize = 1;
        this.mDealByOne = false;
        this.mEnableRedeal = rules.mRedealCount > 0;
        this.mRedealCount = rules.mRedealCount;
        this.mRedealNotDeal = false;
        this.mAutoplayType = 1;
        if (rules.mBaseCardType == 1) {
            z = true;
        } else {
            z = false;
        }
        this.mNoWrap = z;
        if (this.mAutoplayType > 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        this.mUseCardLock = z2;
        this.mTrashRule = null;
        this.mPack = new Pack(this, rules);
        this.mIndexSize = 1;
        this.mGroup = new PileGroup[0];
        this.mPiles = new Pile[0];
    }

    public int getAutoplayType() {
        if (this.mGameType != 0) {
            return 0;
        }
        switch (this.mActivity.getAutoplay()) {
            case 0:
                return 0;
            case 1:
            default:
                return this.mAutoplayType;
            case 2:
                return 3;
        }
    }

    public static void putExtra(Intent intent, int id, int type, String name, int off, int rule_size, int demo_size, int help_size) {
        intent.putExtra(GameActivity.GAME_ID, id);
        intent.putExtra(GameActivity.GAME_TYPE, type);
        intent.putExtra(GameActivity.GAME_NAME, name);
        intent.putExtra(DataSource.OFFSET, off);
        intent.putExtra(DataSource.RULE_SIZE, rule_size);
        intent.putExtra(DataSource.DEMO_SIZE, demo_size);
        intent.putExtra(DataSource.HELP_SIZE, help_size);
    }

    /* access modifiers changed from: package-private */
    public void setScreenSize(int width, int height) {
        int state;
        int old_width = this.mScreen.width();
        int old_height = this.mScreen.height();
        if (width < CardData.getNormalWidth(this.mActivity) * 10) {
            state = 0;
        } else {
            state = 1;
        }
        this.mScreen.set(0, 0, width, height);
        this.mScreenType = state;
        CardSize card_size = CardSize.get(state == 1 ? this.mLanscapeCardSize : this.mNormalCardSize);
        if (this.mCardData == null || this.mCardData.mSizeType != card_size) {
            if (this.mCardData != null) {
                this.mCardData.release();
            }
            try {
                this.mCardData = new CardData(card_size, this.mActivity, null, true);
            } catch (CardData.InvalidSizeException e) {
                e.printStackTrace();
            }
        }
        this.mPack.setCardData(this.mCardData);
        this.mPack.Correct();
        for (PileGroup group : this.mGroup) {
            group.Correct();
        }
        if (old_width != width || old_height != this.mScreen.height()) {
            this.mActivity.mBackground.updateImage(width, height);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [float, int, float, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: protected */
    public void drawBackground(Canvas g, Rect rect) {
        int w = this.mView.getWidth();
        int h = this.mView.getHeight();
        if (this.mActivity.mBackground == null) {
            this.mPaint.setShader(new LinearGradient((float) (w / 2), 0.0f, (float) (w / 2), (float) h, -16777216, -16744704, Shader.TileMode.MIRROR));
            g.drawRect(rect, this.mPaint);
            this.mPaint.setShader(null);
            return;
        }
        this.mActivity.mBackground.draw(g, rect, w, h);
    }

    /* access modifiers changed from: package-private */
    public void draw(Canvas g, Rect clip_rect) {
        drawBackground(g, new Rect(0, 0, this.mView.getWidth(), this.mView.getHeight()));
        if (!this.mDeal && Rect.intersects(this.mPack.mBounds, clip_rect)) {
            this.mPack.draw(g);
        }
        for (Pile pile : this.mPiles) {
            if (Rect.intersects(pile.mCardBounds, clip_rect)) {
                pile.draw(g, clip_rect);
            } else {
                Log.i("_", "_");
            }
        }
        if (this.mDeal && Rect.intersects(this.mPack.mBounds, clip_rect)) {
            this.mPack.draw(g);
        }
        if (this.mDraw != null) {
            this.mDraw.draw(g);
        }
        this.mRedrawRect = null;
    }

    public void CorrectAndRedrawIfNeed() {
        if (this.mNeedCorrect && this.mCardData != null) {
            this.mPack.Correct();
            for (PileGroup group : this.mGroup) {
                group.Correct();
            }
            this.mNeedCorrect = false;
            this.mView.invalidate();
            this.mRedrawRect = null;
        }
        if (this.mRedrawRect != null) {
            this.mView.invalidate(this.mRedrawRect);
            this.mRedrawRect = null;
        }
    }

    public void addRedrawRect(Rect rect) {
        if (this.mRedrawRect != null) {
            this.mRedrawRect.union(rect);
        } else {
            this.mRedrawRect = new Rect(rect);
        }
    }

    protected class RunnableAndRedraw implements Runnable {
        private final Runnable mRunnable;

        RunnableAndRedraw(Runnable runnable) {
            this.mRunnable = runnable;
        }

        public void run() {
            this.mRunnable.run();
            Game.this.CorrectAndRedrawIfNeed();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean MoveCard(Pile from_pile, int from_number, Pile to_pile, int to_number, boolean f_open, GameAction next_action) {
        Card prev_card;
        int prev_y;
        int prev_x;
        Card card;
        if (from_pile == null) {
            if (!isAnimation()) {
                addRedrawRect(this.mPack.mBounds);
            }
            card = this.mPack.mWorkPack.removeLast();
            card.mOpen = f_open;
            this.mPack.Correct();
        } else {
            if (!isAnimation()) {
                addRedrawRect(from_pile.mCardBounds);
            }
            boolean not_last_card = from_number < from_pile.size() - 1;
            if (from_number > 0) {
                prev_card = from_pile.getCard(from_number - 1);
                prev_x = prev_card.xPos;
                prev_y = prev_card.yPos;
            } else {
                prev_card = null;
                prev_y = 0;
                prev_x = 0;
            }
            card = from_pile.remove(from_number);
            from_pile.Correct();
            if (not_last_card || !(prev_card == null || (prev_x == prev_card.xPos && prev_y == prev_card.yPos))) {
                addRedrawRect(from_pile.mCardBounds);
            }
        }
        if (card == null) {
            StringBuilder builder = new StringBuilder("Null card. ");
            if (from_pile != null) {
                builder.append("Card number = ");
                builder.append(from_number);
                builder.append("; pile size = ");
                builder.append(from_pile.size());
            } else {
                builder.append("Move from Pack. Pack size = ");
                builder.append(this.mPack.mWorkPack.size());
            }
            Log.e("Game.MoveCard", builder.toString());
            return false;
        }
        if (isAnimation()) {
            playMoveSound();
            getCardMoveView().setSourceCard(card, next_action);
        } else {
            playDropSound();
        }
        if (f_open) {
            card.mOpen = true;
        }
        card.mLock = from_pile != null && from_pile.mOwner.mFoundation;
        card.mMark = false;
        if (to_pile == null) {
            this.mPack.mWorkPack.add(card);
            this.mPack.Correct();
            if (!isAnimation()) {
                addRedrawRect(this.mPack.mBounds);
            }
        } else {
            Iterator<Card> it = to_pile.iterator();
            while (it.hasNext()) {
                it.next().mLock = false;
            }
            to_pile.insert(card, to_number);
            to_pile.Correct();
            if (!isAnimation()) {
                addRedrawRect(to_pile.mCardBounds);
            } else if (to_number > 0) {
                to_pile.getCard(to_number - 1).mNextOffset = 0;
            }
        }
        CorrectAndRedrawIfNeed();
        if (isAnimation()) {
            playMoveSound();
            card.mNextOffset = -1;
            getCardMoveView().start(card, to_pile);
        } else if (next_action != null) {
            this.mView.post(new RunnableAndRedraw(next_action));
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public final void MoveCards(Pile from_pile, Pile to_pile, int count, GameAction next_action) {
        Card prev_card;
        int prev_y;
        int prev_x;
        CardList cards;
        if (count == 1) {
            MoveCard(from_pile, from_pile == null ? 0 : from_pile.size() - 1, to_pile, to_pile == null ? 0 : to_pile.size(), to_pile != null, next_action);
            return;
        }
        if (from_pile == null) {
            if (!isAnimation()) {
                addRedrawRect(this.mPack.mBounds);
            }
            cards = new CardList();
            for (int i = 0; i < count; i++) {
                cards.add(this.mPack.mWorkPack.removeLast());
                cards.lastElement().mOpen = true;
            }
            this.mPack.Correct();
        } else {
            if (!isAnimation()) {
                addRedrawRect(from_pile.mCardBounds);
            }
            if (count > from_pile.size()) {
                count = from_pile.size();
            }
            if (count >= from_pile.size()) {
                prev_card = null;
            } else {
                prev_card = from_pile.getCard((from_pile.size() - count) - 1);
            }
            if (prev_card != null) {
                prev_x = prev_card.xPos;
                prev_y = prev_card.yPos;
            } else {
                prev_y = 0;
                prev_x = 0;
            }
            if (to_pile == null) {
                cards = new CardList();
                for (int i2 = 0; i2 < count; i2++) {
                    cards.add(from_pile.removeLast());
                    cards.lastElement().mOpen = false;
                }
            } else {
                cards = from_pile.removeLast(count);
            }
            from_pile.Correct();
            if (!(prev_card == null || (prev_x == prev_card.xPos && prev_y == prev_card.yPos))) {
                addRedrawRect(from_pile.mCardBounds);
            }
        }
        Iterator<Card> it = cards.iterator();
        while (it.hasNext()) {
            it.next().mMark = false;
        }
        if (isAnimation()) {
            playMoveSound();
            getCardMoveView().setSourceCards(cards, next_action);
        } else {
            playDropSound();
        }
        if (to_pile == null) {
            this.mPack.mWorkPack.add(cards);
            this.mPack.Correct();
            if (!isAnimation()) {
                addRedrawRect(this.mPack.mBounds);
            }
        } else {
            Iterator<Card> it2 = to_pile.iterator();
            while (it2.hasNext()) {
                it2.next().mLock = false;
            }
            to_pile.add(cards);
            to_pile.Correct();
            if (!isAnimation()) {
                addRedrawRect(to_pile.mCardBounds);
            }
        }
        CorrectAndRedrawIfNeed();
        if (isAnimation()) {
            Iterator<Card> it3 = cards.iterator();
            while (it3.hasNext()) {
                it3.next().mNextOffset = -1;
            }
            if (to_pile != null && count < to_pile.size()) {
                to_pile.getCard((to_pile.size() - count) - 1).mNextOffset = 0;
            }
            getCardMoveView().start(cards, to_pile);
        } else if (next_action != null) {
            this.mView.post(new RunnableAndRedraw(next_action));
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean isWinFinish() {
        int i;
        if (!this.mPack.isFinish()) {
            return false;
        }
        for (Pile pile : this.mPiles) {
            Condition condition = pile.mOwner.mFinishCondition;
            if (pile.size() > 0) {
                i = pile.lastElement().mCardMask;
            } else {
                i = 0;
            }
            if (!condition.Examine(i, pile.mNumber, null)) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void DealStart(boolean f_deal_animation, byte[] start_pack, GameAction finish_action) {
        this.mTrash.clear();
        this.RedealCurrent = 0;
        for (Pile pile : this.mPiles) {
            pile.Reset();
        }
        this.mPack.StartDeal(start_pack);
        for (PileGroup group : this.mGroup) {
            group.SetFixedCard();
        }
        this.mStarted = true;
        new DealAction(finish_action).start(f_deal_animation);
    }

    /* access modifiers changed from: package-private */
    public void RedealStart(boolean f_deal_animation, byte[] start_pack, GameAction finish_action) {
        CardList list;
        if (this.mPack.mWorkPack.size() > 0) {
            list = this.mPack.mWorkPack.removeLast(this.mPack.mWorkPack.size());
        } else {
            list = new CardList();
        }
        for (PileGroup group : this.mGroup) {
            if (group.mRedealPile) {
                Pile[] pileArr = group.mPile;
                int length = pileArr.length;
                for (int i = 0; i < length; i++) {
                    Pile pile = pileArr[i];
                    if (group.mRedealCondition.Examine(pile.size() > 0 ? pile.lastElement().mCardMask : 0, pile.mNumber, null)) {
                        if (pile.size() > 0) {
                            list.add(pile.removeLast(pile.size()));
                        }
                        pile.Reset();
                    }
                }
            }
        }
        if (start_pack != null) {
            for (byte b : start_pack) {
                int suit = b >> 4;
                int value = b & 15;
                int k = 0;
                Iterator<Card> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        Card card = it.next();
                        if (card.mSuit == suit && card.mValue == value) {
                            this.mPack.mWorkPack.add(list.remove(k));
                            break;
                        }
                        k++;
                    } else {
                        break;
                    }
                }
            }
            if (list.size() > 0) {
                this.mPack.mWorkPack.add(list);
            }
        } else if (this.mRedealNotDeal) {
            this.mPack.mWorkPack.add(list);
        } else {
            this.mPack.Deal(list, this.mPack.mWorkPack);
        }
        this.RedealCurrent = this.RedealCurrent + 1;
        new DealAction(finish_action).start(f_deal_animation);
    }

    protected final class DealAction implements GameAction {
        private final GameAction mFinishAction;
        private int mPileNumber;

        DealAction(GameAction finish_action) {
            this.mFinishAction = finish_action;
        }

        /* access modifiers changed from: package-private */
        public void start(boolean f_deal_animation) {
            Game.this.mPack.Correct();
            for (PileGroup group : Game.this.mGroup) {
                group.Correct();
                for (Pile pile : group.mPile) {
                    pile.CorrectEmptyCard();
                }
            }
            Game.this.mDeal = true;
            this.mPileNumber = 0;
            if (Game.this.isDealAnimation() && f_deal_animation) {
                Game.this.addRedrawRect(Game.this.mScreen);
                run();
            } else {
                fastRun();
                Game.this.mDeal = false;
                Game.this.mNeedCorrect = true;
            }
            Game.this.CorrectAndRedrawIfNeed();
        }

        private boolean nextPile() {
            if (Game.this.mPack.mWorkPack.size() == 0) {
                Game.this.addRedrawRect(Game.this.mPack.mBounds);
                Game.this.mDeal = false;
                return true;
            }
            int start_pile = this.mPileNumber;
            do {
                this.mPileNumber = (this.mPileNumber + 1) % Game.this.mPiles.length;
                if (start_pile == this.mPileNumber) {
                    if (!Game.this.mPiles[this.mPileNumber].isDealComplete()) {
                        return false;
                    }
                    Game.this.addRedrawRect(Game.this.mPack.mBounds);
                    Game.this.mDeal = false;
                    return true;
                }
            } while (Game.this.mPiles[this.mPileNumber].isDealComplete());
            return false;
        }

        public void fastRun() {
            Pile pile = Game.this.mPiles[this.mPileNumber];
            while (Game.this.mPack.mWorkPack.size() > 0) {
                pile.DealStep(null);
                if (Game.this.mDealByOne || pile.isDealComplete()) {
                    if (nextPile()) {
                        break;
                    }
                    pile = Game.this.mPiles[this.mPileNumber];
                }
            }
            Game.this.mDeal = false;
            if (this.mFinishAction != null) {
                this.mFinishAction.fastRun();
            }
        }

        public void run() {
            while (Game.this.mPack.mWorkPack.size() > 0) {
                if (!Game.this.mPiles[this.mPileNumber].isDealComplete() && Game.this.mPiles[this.mPileNumber].DealStep(this)) {
                    return;
                }
                if ((Game.this.mPiles[this.mPileNumber].isDealComplete() || Game.this.mDealByOne) && nextPile()) {
                    break;
                }
            }
            Game.this.mDeal = false;
            Game.this.addRedrawRect(Game.this.mScreen);
            if (this.mFinishAction != null) {
                this.mFinishAction.run();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void ResetSelection() {
    }

    /* access modifiers changed from: package-private */
    public boolean PenDown(int x, int y) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean PenUp(int x, int y) {
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean PenDrag(int x, int y) {
        return false;
    }

    protected final class PackOpenCardsAction implements GameAction {
        private final boolean mFromPack;
        private int mGroupNumber = 0;
        private final MoveMemory mMoves;
        private boolean mNewMove = true;
        private final GameAction mNextAction;
        private int mOpen = 0;
        private int mPileNumber = 0;

        PackOpenCardsAction(GameAction next_action, MoveMemory moves) {
            boolean z;
            this.mNextAction = next_action;
            this.mMoves = moves;
            if (Game.this.mPack.mWorkPack.size() > 0) {
                z = true;
            } else {
                z = false;
            }
            this.mFromPack = z;
        }

        public void fastRun() {
            if (this.mFromPack) {
                while (Game.this.mPack.mWorkPack.size() > 0 && this.mGroupNumber < Game.this.mGroup.length) {
                    PileGroup group = Game.this.mGroup[this.mGroupNumber];
                    if (group.mPackPile && group.isEnableUse()) {
                        while (Game.this.mPack.mWorkPack.size() > 0 && this.mPileNumber < group.mPile.length) {
                            Pile pile = group.mPile[this.mPileNumber];
                            while (Game.this.mPack.mWorkPack.size() > 0 && this.mOpen < Game.this.mPack.getOpenCount()) {
                                this.mOpen++;
                                if (this.mNewMove) {
                                    if (this.mMoves != null) {
                                        this.mMoves.IncreaseMoveNumber();
                                    }
                                    this.mNewMove = false;
                                }
                                if (this.mMoves != null) {
                                    this.mMoves.addOneCardMove(null, 0, pile);
                                }
                                Card card = Game.this.mPack.mWorkPack.removeLast();
                                card.mOpen = true;
                                pile.add(card);
                            }
                            this.mPileNumber++;
                            this.mOpen = 0;
                        }
                    }
                    this.mGroupNumber++;
                    this.mOpen = 0;
                    this.mPileNumber = 0;
                }
            } else {
                while (this.mGroupNumber < Game.this.mGroup.length) {
                    PileGroup group2 = Game.this.mGroup[this.mGroupNumber];
                    if (group2.mPackPile && group2.isEnableUse()) {
                        while (this.mPileNumber < group2.mPile.length) {
                            Pile pile2 = group2.mPile[this.mPileNumber];
                            this.mPileNumber++;
                            if (pile2.size() > 0) {
                                if (this.mNewMove) {
                                    if (this.mMoves != null) {
                                        this.mMoves.IncreaseMoveNumber();
                                        this.mMoves.addRoundIncrease();
                                    }
                                    this.mNewMove = false;
                                }
                                if (this.mMoves != null) {
                                    this.mMoves.addCardsToPack(pile2, pile2.size());
                                }
                                while (pile2.size() > 0) {
                                    Card card2 = pile2.removeLast();
                                    card2.mOpen = false;
                                    Game.this.mPack.mWorkPack.add(card2);
                                }
                            }
                        }
                    }
                    this.mGroupNumber++;
                    this.mPileNumber = 0;
                }
                if (Game.this.mPack.mRoundCurrent < 11) {
                    Game.this.mPack.mRoundCurrent--;
                }
            }
            Game.this.mNeedCorrect = true;
            if (this.mNextAction != null) {
                this.mNextAction.fastRun();
            } else {
                Game.this.CorrectAndRedrawIfNeed();
            }
        }

        public void run() {
            if (!this.mFromPack) {
                while (this.mGroupNumber < Game.this.mGroup.length) {
                    PileGroup group = Game.this.mGroup[this.mGroupNumber];
                    if (group.mPackPile && group.isEnableUse()) {
                        while (this.mPileNumber < group.mPile.length) {
                            Pile pile = group.mPile[this.mPileNumber];
                            this.mPileNumber++;
                            if (pile.size() > 0) {
                                if (this.mNewMove) {
                                    if (this.mMoves != null) {
                                        this.mMoves.IncreaseMoveNumber();
                                        this.mMoves.addRoundIncrease();
                                    }
                                    this.mNewMove = false;
                                }
                                if (this.mMoves != null) {
                                    this.mMoves.addCardsToPack(pile, pile.size());
                                }
                                Game.this.MoveCards(pile, null, pile.size(), this);
                                return;
                            }
                        }
                        continue;
                    }
                    this.mGroupNumber++;
                    this.mPileNumber = 0;
                }
                if (Game.this.mPack.mRoundCurrent < 11) {
                    Game.this.mPack.mRoundCurrent--;
                }
            } else if (Game.this.mPack.mWorkPack.size() != 0) {
                while (this.mGroupNumber < Game.this.mGroup.length) {
                    PileGroup group2 = Game.this.mGroup[this.mGroupNumber];
                    if (group2.mPackPile && group2.isEnableUse()) {
                        while (this.mPileNumber < group2.mPile.length) {
                            Pile pile2 = group2.mPile[this.mPileNumber];
                            if (this.mOpen < Game.this.mPack.getOpenCount()) {
                                this.mOpen++;
                                if (this.mNewMove) {
                                    if (this.mMoves != null) {
                                        this.mMoves.IncreaseMoveNumber();
                                    }
                                    this.mNewMove = false;
                                }
                                if (this.mMoves != null) {
                                    this.mMoves.addOneCardMove(null, 0, pile2);
                                }
                                Game.this.MoveCard(null, 0, pile2, pile2.size(), true, this);
                                return;
                            }
                            this.mPileNumber++;
                            this.mOpen = 0;
                        }
                        continue;
                    }
                    this.mGroupNumber++;
                    this.mOpen = 0;
                    this.mPileNumber = 0;
                }
            } else if (this.mNextAction != null) {
                this.mNextAction.run();
                return;
            } else {
                return;
            }
            if (this.mNextAction != null) {
                this.mNextAction.run();
            }
        }
    }

    public void setToolbarButtons(ToolbarButton[] buttons) {
        this.mActivity.setToolbarButton(buttons);
    }

    public void updateToolbar() {
        this.mActivity.invalidateToolbar();
    }

    public boolean isAnimation() {
        return this.mActivity.isAnimation();
    }

    public boolean isDealAnimation() {
        return this.mActivity.isDealAnimation();
    }

    public boolean isHidePackSize() {
        return this.mActivity.isHidePackSize();
    }

    public boolean isHidePackRedeal() {
        return this.mActivity.isHidePackRedeal();
    }

    /* access modifiers changed from: package-private */
    public boolean isMirrorLayout() {
        return this.mActivity.isMirror();
    }

    public String getGameName() {
        return this.mGameName;
    }

    public int getGameType() {
        return this.mGameType;
    }

    public int getGameId() {
        return this.mGameId;
    }

    public CardMoveView getCardMoveView() {
        return this.mActivity.mCardMoveView;
    }

    public final void playTakeSound() {
        this.mActivity.playSound(0);
    }

    public final void playDropSound() {
        this.mActivity.playSound(1);
    }

    public final void playMoveSound() {
        this.mActivity.playSound(2);
    }
}
