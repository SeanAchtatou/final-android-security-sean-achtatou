package com.anoshenko.android.solitaires;

import com.anoshenko.android.ui.ToolbarTheme;

/* compiled from: CardOrder */
final class CardOrderElement {
    final Game mGame;
    final int mSuit;
    final int mSuitRule;
    final int mValueData;
    final int mValueRule;

    CardOrderElement(Game game) {
        this.mGame = game;
        BitStack stack = game.mSource.mRule;
        this.mSuitRule = stack.getInt(3);
        this.mSuit = this.mSuitRule == 6 ? stack.getInt(4) << 16 : 0;
        this.mValueRule = stack.getInt(3);
        switch (this.mValueRule) {
            case 2:
            case 3:
                if (stack.getFlag()) {
                    this.mValueData = stack.getInt(3) + 2;
                    return;
                } else {
                    this.mValueData = 1;
                    return;
                }
            case 4:
            case 5:
                this.mValueData = stack.getInt(4);
                return;
            case 6:
                this.mValueData = stack.getInt(4, 8) + 2;
                return;
            case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                this.mValueData = stack.getInt(14);
                return;
            default:
                this.mValueData = 0;
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getSuit(int card_mask) {
        int out_card_mask = 0;
        switch (this.mSuitRule) {
            case 0:
                return 983040;
            case 1:
                return card_mask & 983040;
            case 2:
                if ((card_mask & 196608) != 0) {
                    out_card_mask = 0 | 786432;
                }
                if ((card_mask & 786432) != 0) {
                    return out_card_mask | 196608;
                }
                return out_card_mask;
            case 3:
                if ((card_mask & 196608) != 0) {
                    out_card_mask = 0 | 196608;
                }
                if ((card_mask & 786432) != 0) {
                    return out_card_mask | 786432;
                }
                return out_card_mask;
            case 4:
                if ((card_mask & 65536) != 0) {
                    out_card_mask = 0 | 917504;
                }
                if ((card_mask & 131072) != 0) {
                    out_card_mask |= 851968;
                }
                if ((262144 & card_mask) != 0) {
                    out_card_mask |= 720896;
                }
                if ((card_mask & 524288) != 0) {
                    return out_card_mask | 458752;
                }
                return out_card_mask;
            case 5:
                if ((card_mask & 65536) != 0) {
                    out_card_mask = 0 | 131072;
                }
                if ((card_mask & 131072) != 0) {
                    out_card_mask |= 65536;
                }
                if ((262144 & card_mask) != 0) {
                    out_card_mask |= 524288;
                }
                if ((card_mask & 524288) != 0) {
                    return out_card_mask | 65536;
                }
                return out_card_mask;
            case 6:
                return this.mSuit;
            default:
                return 0;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getNextCard(int card_mask) {
        int out_card_mask = getSuit(card_mask);
        switch (this.mValueRule) {
            case 0:
                return out_card_mask | 16383;
            case 1:
                return out_card_mask | (card_mask & 16383);
            case 2:
                int out_card_mask2 = out_card_mask | (((card_mask & 16382) << this.mValueData) & 16382);
                if (!this.mGame.mNoWrap) {
                    return out_card_mask2 | (((card_mask & 16382) >> (13 - this.mValueData)) & 16382);
                }
                return out_card_mask2;
            case 3:
                int out_card_mask3 = out_card_mask | (((card_mask & 16382) >> this.mValueData) & 16382);
                if (!this.mGame.mNoWrap) {
                    return out_card_mask3 | (((card_mask & 16382) << (13 - this.mValueData)) & 16382);
                }
                return out_card_mask3;
            case 4:
                int mask = 2;
                for (int i = 1; i <= 13; i++) {
                    if ((card_mask & mask) != 0) {
                        int k = ((this.mValueData + this.mValueData) + 2) - i;
                        if (k > 13) {
                            k -= 13;
                        } else if (k < 1) {
                            k += 13;
                        }
                        out_card_mask |= 1 << k;
                    }
                    mask <<= 1;
                }
                return out_card_mask;
            case 5:
                int mask2 = 2;
                for (int i2 = 1; i2 <= 13; i2++) {
                    if ((card_mask & mask2) != 0) {
                        int k2 = ((this.mValueData + this.mValueData) + 3) - i2;
                        if (k2 > 13) {
                            k2 -= 13;
                        } else if (k2 < 1) {
                            k2 += 13;
                        }
                        out_card_mask |= 1 << k2;
                    }
                    mask2 <<= 1;
                }
                return out_card_mask;
            case 6:
                int mask3 = 2;
                for (int i3 = 1; i3 <= 13; i3++) {
                    if ((card_mask & mask3) != 0) {
                        int k3 = this.mValueData - i3;
                        while (k3 > 13) {
                            k3 -= 13;
                        }
                        while (k3 < 1) {
                            k3 += 13;
                        }
                        out_card_mask |= 1 << k3;
                    }
                    mask3 <<= 1;
                }
                return out_card_mask;
            case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                return out_card_mask | this.mValueData;
            default:
                return out_card_mask;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getPrevCard(int card_mask) {
        int out_card_mask = getSuit(card_mask);
        switch (this.mValueRule) {
            case 0:
                return out_card_mask | 16383;
            case 1:
                return out_card_mask | (card_mask & 16383);
            case 2:
                int out_card_mask2 = out_card_mask | (((card_mask & 16382) >> this.mValueData) & 16382);
                if (!this.mGame.mNoWrap) {
                    return out_card_mask2 | (((card_mask & 16382) << (13 - this.mValueData)) & 16382);
                }
                return out_card_mask2;
            case 3:
                int out_card_mask3 = out_card_mask | (((card_mask & 16382) << this.mValueData) & 16382);
                if (!this.mGame.mNoWrap) {
                    return out_card_mask3 | (((card_mask & 16382) >> (13 - this.mValueData)) & 16382);
                }
                return out_card_mask3;
            case 4:
                int mask = 2;
                for (int i = 1; i <= 13; i++) {
                    if ((card_mask & mask) != 0) {
                        int k = ((this.mValueData + this.mValueData) + 2) - i;
                        if (k > 13) {
                            k -= 13;
                        } else if (k < 1) {
                            k += 13;
                        }
                        out_card_mask |= 1 << k;
                    }
                    mask <<= 1;
                }
                return out_card_mask;
            case 5:
                int mask2 = 2;
                for (int i2 = 1; i2 <= 13; i2++) {
                    if ((card_mask & mask2) != 0) {
                        int k2 = ((this.mValueData + this.mValueData) + 3) - i2;
                        if (k2 > 13) {
                            k2 -= 13;
                        } else if (k2 < 1) {
                            k2 += 13;
                        }
                        out_card_mask |= 1 << k2;
                    }
                    mask2 <<= 1;
                }
                return out_card_mask;
            case 6:
                int mask3 = 2;
                for (int i3 = 1; i3 <= 13; i3++) {
                    if ((card_mask & mask3) != 0) {
                        int k3 = this.mValueData - i3;
                        while (k3 > 13) {
                            k3 -= 13;
                        }
                        while (k3 < 1) {
                            k3 += 13;
                        }
                        out_card_mask |= 1 << k3;
                    }
                    mask3 <<= 1;
                }
                return out_card_mask;
            case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                return out_card_mask | this.mValueData;
            default:
                return out_card_mask;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getMaxTrashCards() {
        int result = 2;
        switch (this.mValueRule) {
            case 0:
            case ToolbarTheme.DISABLED_TEXT_COLOR /*7*/:
                result = 1;
                break;
            case 6:
                result = this.mValueData;
                break;
        }
        switch (this.mSuitRule) {
            case 1:
            case 3:
                if (result == 1) {
                    return 2;
                }
                return result;
            case 2:
            case 5:
                return 2;
            case 4:
                if (result > 4) {
                    return 4;
                }
                if (result < 2) {
                    return 2;
                }
                return result;
            default:
                return result;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041 A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean TestTrashPiles(com.anoshenko.android.solitaires.Pile[] r11, int r12) {
        /*
            r10 = this;
            r8 = 1
            if (r12 < r8) goto L_0x0011
            r8 = 0
            r8 = r11[r8]
            if (r8 == 0) goto L_0x0011
            r8 = 0
            r8 = r11[r8]
            com.anoshenko.android.solitaires.Card r5 = r8.lastElement()
            if (r5 != 0) goto L_0x0013
        L_0x0011:
            r8 = 0
        L_0x0012:
            return r8
        L_0x0013:
            r8 = 1
            if (r12 == r8) goto L_0x001b
            r8 = 1
            r8 = r11[r8]
            if (r8 != 0) goto L_0x002e
        L_0x001b:
            r8 = 0
            r6 = r8
        L_0x001d:
            int r8 = r10.mValueRule
            r9 = 1
            if (r8 < r9) goto L_0x0037
            int r8 = r10.mValueRule
            r9 = 5
            if (r8 > r9) goto L_0x0037
            r8 = 2
            if (r12 != r8) goto L_0x002c
            if (r6 != 0) goto L_0x0037
        L_0x002c:
            r8 = 0
            goto L_0x0012
        L_0x002e:
            r8 = 1
            r8 = r11[r8]
            com.anoshenko.android.solitaires.Card r8 = r8.lastElement()
            r6 = r8
            goto L_0x001d
        L_0x0037:
            int r8 = r10.mValueRule
            switch(r8) {
                case 1: goto L_0x0043;
                case 2: goto L_0x0053;
                case 3: goto L_0x0053;
                case 4: goto L_0x0077;
                case 5: goto L_0x009c;
                case 6: goto L_0x00c1;
                case 7: goto L_0x00fb;
                default: goto L_0x003c;
            }
        L_0x003c:
            int r8 = r10.mSuitRule
            switch(r8) {
                case 1: goto L_0x011e;
                case 2: goto L_0x0145;
                case 3: goto L_0x0162;
                case 4: goto L_0x018d;
                case 5: goto L_0x01c8;
                case 6: goto L_0x01eb;
                default: goto L_0x0041;
            }
        L_0x0041:
            r8 = 1
            goto L_0x0012
        L_0x0043:
            int r8 = r5.mValue
            int r9 = r6.mValue
            if (r8 == r9) goto L_0x003c
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x003c
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x003c
            r8 = 0
            goto L_0x0012
        L_0x0053:
            int r8 = r5.mValue
            int r9 = r6.mValue
            int r7 = r8 - r9
            if (r7 >= 0) goto L_0x005c
            int r7 = -r7
        L_0x005c:
            int r8 = r10.mValueData
            if (r8 == r7) goto L_0x003c
            com.anoshenko.android.solitaires.Game r8 = r10.mGame
            boolean r8 = r8.mNoWrap
            if (r8 != 0) goto L_0x006d
            r8 = 13
            int r8 = r8 - r7
            int r9 = r10.mValueData
            if (r8 == r9) goto L_0x003c
        L_0x006d:
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x003c
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x003c
            r8 = 0
            goto L_0x0012
        L_0x0077:
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x003c
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x003c
            int r8 = r10.mValueData
            int r8 = r8 + 1
            int r8 = r8 * 2
            int r9 = r5.mValue
            int r7 = r8 - r9
            r8 = 1
            if (r7 >= r8) goto L_0x0095
            int r7 = r7 + 13
        L_0x008e:
            int r8 = r6.mValue
            if (r7 == r8) goto L_0x003c
            r8 = 0
            goto L_0x0012
        L_0x0095:
            r8 = 13
            if (r7 <= r8) goto L_0x008e
            int r7 = r7 + -13
            goto L_0x008e
        L_0x009c:
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x003c
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x003c
            int r8 = r10.mValueData
            int r8 = r8 * 2
            int r8 = r8 + 3
            int r9 = r5.mValue
            int r7 = r8 - r9
            r8 = 1
            if (r7 >= r8) goto L_0x00ba
            int r7 = r7 + 13
        L_0x00b3:
            int r8 = r6.mValue
            if (r7 == r8) goto L_0x003c
            r8 = 0
            goto L_0x0012
        L_0x00ba:
            r8 = 13
            if (r7 <= r8) goto L_0x00b3
            int r7 = r7 + -13
            goto L_0x00b3
        L_0x00c1:
            r4 = 0
            r7 = 0
            r3 = 0
        L_0x00c4:
            if (r3 < r12) goto L_0x00cf
            if (r4 != 0) goto L_0x00ed
            int r8 = r10.mValueData
            if (r7 == r8) goto L_0x003c
            r8 = 0
            goto L_0x0012
        L_0x00cf:
            r8 = r11[r3]
            if (r8 != 0) goto L_0x00d6
            r8 = 0
            goto L_0x0012
        L_0x00d6:
            r8 = r11[r3]
            com.anoshenko.android.solitaires.Card r0 = r8.lastElement()
            if (r0 != 0) goto L_0x00e1
            r8 = 0
            goto L_0x0012
        L_0x00e1:
            int r8 = r0.mValue
            int r7 = r7 + r8
            int r8 = r0.mValue
            if (r8 != 0) goto L_0x00ea
            int r4 = r4 + 1
        L_0x00ea:
            int r3 = r3 + 1
            goto L_0x00c4
        L_0x00ed:
            int r8 = r10.mValueData
            if (r7 >= r8) goto L_0x00f8
            int r8 = r10.mValueData
            int r8 = r8 - r7
            int r9 = r4 * 13
            if (r8 <= r9) goto L_0x003c
        L_0x00f8:
            r8 = 0
            goto L_0x0012
        L_0x00fb:
            r3 = 0
        L_0x00fc:
            if (r3 >= r12) goto L_0x003c
            r8 = r11[r3]
            if (r8 != 0) goto L_0x0105
            r8 = 0
            goto L_0x0012
        L_0x0105:
            r8 = r11[r3]
            com.anoshenko.android.solitaires.Card r0 = r8.lastElement()
            if (r0 == 0) goto L_0x0118
            int r8 = r0.mValue
            if (r8 == 0) goto L_0x011b
            int r8 = r0.mValueMask
            int r9 = r10.mValueData
            r8 = r8 & r9
            if (r8 != 0) goto L_0x011b
        L_0x0118:
            r8 = 0
            goto L_0x0012
        L_0x011b:
            int r3 = r3 + 1
            goto L_0x00fc
        L_0x011e:
            r3 = 1
        L_0x011f:
            if (r3 >= r12) goto L_0x0041
            r8 = r11[r3]
            if (r8 == 0) goto L_0x012d
            r8 = r11[r3]
            com.anoshenko.android.solitaires.Card r6 = r8.lastElement()
            if (r6 != 0) goto L_0x0130
        L_0x012d:
            r8 = 0
            goto L_0x0012
        L_0x0130:
            int r8 = r6.mSuit
            int r9 = r5.mSuit
            if (r8 == r9) goto L_0x0141
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x0141
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x0141
            r8 = 0
            goto L_0x0012
        L_0x0141:
            r5 = r6
            int r3 = r3 + 1
            goto L_0x011f
        L_0x0145:
            r8 = 2
            if (r12 != r8) goto L_0x014a
            if (r6 != 0) goto L_0x014d
        L_0x014a:
            r8 = 0
            goto L_0x0012
        L_0x014d:
            int r8 = r5.mSuit
            int r8 = r8 / 2
            int r9 = r6.mSuit
            int r9 = r9 / 2
            if (r8 != r9) goto L_0x0041
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x0041
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x0041
            r8 = 0
            goto L_0x0012
        L_0x0162:
            r3 = 1
        L_0x0163:
            if (r3 >= r12) goto L_0x0041
            r8 = r11[r3]
            if (r8 == 0) goto L_0x0171
            r8 = r11[r3]
            com.anoshenko.android.solitaires.Card r6 = r8.lastElement()
            if (r6 != 0) goto L_0x0174
        L_0x0171:
            r8 = 0
            goto L_0x0012
        L_0x0174:
            int r8 = r6.mSuit
            int r8 = r8 / 2
            int r9 = r5.mSuit
            int r9 = r9 / 2
            if (r8 == r9) goto L_0x0189
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x0189
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x0189
            r8 = 0
            goto L_0x0012
        L_0x0189:
            r5 = r6
            int r3 = r3 + 1
            goto L_0x0163
        L_0x018d:
            r8 = 4
            if (r12 <= r8) goto L_0x0193
            r8 = 0
            goto L_0x0012
        L_0x0193:
            r3 = 1
        L_0x0194:
            if (r3 >= r12) goto L_0x0041
            r8 = r11[r3]
            if (r8 != 0) goto L_0x019d
            r8 = 0
            goto L_0x0012
        L_0x019d:
            r8 = r11[r3]
            com.anoshenko.android.solitaires.Card r1 = r8.lastElement()
            if (r1 != 0) goto L_0x01a8
            r8 = 0
            goto L_0x0012
        L_0x01a8:
            int r8 = r1.mValue
            if (r8 == 0) goto L_0x01af
            r4 = 0
        L_0x01ad:
            if (r4 < r3) goto L_0x01b2
        L_0x01af:
            int r3 = r3 + 1
            goto L_0x0194
        L_0x01b2:
            r8 = r11[r4]
            com.anoshenko.android.solitaires.Card r2 = r8.lastElement()
            int r8 = r1.mSuit
            int r9 = r2.mSuit
            if (r8 != r9) goto L_0x01c5
            int r8 = r2.mValue
            if (r8 == 0) goto L_0x01c5
            r8 = 0
            goto L_0x0012
        L_0x01c5:
            int r4 = r4 + 1
            goto L_0x01ad
        L_0x01c8:
            r8 = 2
            if (r12 != r8) goto L_0x01cd
            if (r6 != 0) goto L_0x01d0
        L_0x01cd:
            r8 = 0
            goto L_0x0012
        L_0x01d0:
            int r8 = r5.mValue
            if (r8 == 0) goto L_0x0041
            int r8 = r6.mValue
            if (r8 == 0) goto L_0x0041
            int r8 = r5.mSuit
            int r8 = r8 / 2
            int r9 = r6.mSuit
            int r9 = r9 / 2
            if (r8 != r9) goto L_0x01e8
            int r8 = r5.mSuit
            int r9 = r6.mSuit
            if (r8 != r9) goto L_0x0041
        L_0x01e8:
            r8 = 0
            goto L_0x0012
        L_0x01eb:
            r3 = 0
        L_0x01ec:
            if (r3 >= r12) goto L_0x0041
            r8 = r11[r3]
            if (r8 != 0) goto L_0x01f5
            r8 = 0
            goto L_0x0012
        L_0x01f5:
            r8 = r11[r3]
            com.anoshenko.android.solitaires.Card r0 = r8.lastElement()
            if (r0 == 0) goto L_0x0208
            int r8 = r0.mValue
            if (r8 == 0) goto L_0x020b
            int r8 = r0.mSuitMask
            int r9 = r10.mSuit
            r8 = r8 & r9
            if (r8 != 0) goto L_0x020b
        L_0x0208:
            r8 = 0
            goto L_0x0012
        L_0x020b:
            int r3 = r3 + 1
            goto L_0x01ec
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.CardOrderElement.TestTrashPiles(com.anoshenko.android.solitaires.Pile[], int):boolean");
    }

    /* access modifiers changed from: package-private */
    public final boolean TestOrder(Card card, int count, boolean f_enable_jokers) {
        int mask2;
        if (!card.mOpen) {
            return false;
        }
        int mask = getNextCard(card.mValue == 0 ? 999422 : card.mCardMask);
        for (int i = 1; i < count; i++) {
            card = card.next;
            if (card == null || !card.mOpen) {
                return false;
            }
            if (card.mValue > 0) {
                mask2 = card.mCardMask;
                if ((mask & mask2) != mask2) {
                    return false;
                }
            } else {
                mask2 = mask;
            }
            mask = getNextCard(mask2);
        }
        return true;
    }
}
