package com.anoshenko.android.solitaires;

import android.content.SharedPreferences;

final class BitStack {
    private static final byte[] BYTE_MASK = {Byte.MIN_VALUE, 64, 32, 16, 8, 4, 2, 1};
    private static final int DATA_QUANTUM = 4096;
    private byte[] mData;
    private int mMask;
    private int mPos;

    BitStack() {
        this.mMask = 0;
        this.mPos = 0;
        this.mData = new byte[DATA_QUANTUM];
    }

    BitStack(byte[] data) {
        this.mMask = 0;
        this.mPos = 0;
        this.mData = new byte[data.length];
        System.arraycopy(data, 0, this.mData, 0, data.length);
    }

    BitStack(String data) {
        this.mMask = 0;
        this.mPos = 0;
        this.mData = new byte[(data.length() / 2)];
        for (int i = 0; i < this.mData.length; i++) {
            this.mData[i] = (byte) (((data.charAt(i * 2) - 'A') << 4) | (data.charAt((i * 2) + 1) - 'A'));
        }
    }

    public String toString() {
        char[] buffer = new char[(getDataSize() * 2)];
        for (int i = 0; i < buffer.length; i += 2) {
            byte b = this.mData[i >> 1];
            buffer[i] = (char) (((b >> 4) & 15) + 65);
            buffer[i + 1] = (char) ((b & 15) + 65);
        }
        return new String(buffer);
    }

    /* access modifiers changed from: package-private */
    public final void Store(SharedPreferences.Editor editor, String key) {
        char[] buffer = new char[(getDataSize() * 2)];
        for (int i = 0; i < buffer.length; i += 2) {
            byte b = this.mData[i >> 1];
            buffer[i] = (char) (((b >> 4) & 15) + 65);
            buffer[i + 1] = (char) ((b & 15) + 65);
        }
        editor.putString(key, toString());
    }

    /* access modifiers changed from: package-private */
    public final byte[] getData() {
        return this.mData;
    }

    /* access modifiers changed from: package-private */
    public final int getDataSize() {
        return this.mMask == 0 ? this.mPos : this.mPos + 1;
    }

    /* access modifiers changed from: package-private */
    public final void ResetPos() {
        this.mMask = 0;
        this.mPos = 0;
    }

    /* access modifiers changed from: package-private */
    public final boolean getFlag() {
        boolean f_result = false;
        if (this.mPos < this.mData.length) {
            f_result = (this.mData[this.mPos] & BYTE_MASK[this.mMask]) != 0;
            this.mMask++;
            this.mPos += this.mMask / 8;
            this.mMask %= 8;
        }
        return f_result;
    }

    /* access modifiers changed from: package-private */
    public final void skip(int bit_count) {
        for (int i = 0; i < bit_count; i++) {
            this.mMask++;
            this.mPos += this.mMask / 8;
            this.mMask %= 8;
        }
    }

    /* access modifiers changed from: package-private */
    public final int getInt(int bit_count) {
        int result = 0;
        for (int i = 0; i < bit_count; i++) {
            result <<= 1;
            if (this.mPos < this.mData.length) {
                if ((this.mData[this.mPos] & BYTE_MASK[this.mMask]) != 0) {
                    result |= 1;
                }
                this.mMask++;
                this.mPos += this.mMask / 8;
                this.mMask %= 8;
            }
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public final int getInt(int bit_count0, int bit_count1) {
        int n;
        int result = 0;
        int k = 0;
        if (this.mPos >= this.mData.length) {
            return 0;
        }
        if ((this.mData[this.mPos] & BYTE_MASK[this.mMask]) != 0) {
            n = bit_count1 - 1;
            k = 1;
            if (bit_count0 > 1) {
                k = 1 << (bit_count0 - 1);
            }
        } else {
            n = bit_count0 - 1;
        }
        this.mMask++;
        this.mPos += this.mMask / 8;
        this.mMask %= 8;
        for (int i = 0; i < n; i++) {
            result <<= 1;
            if (this.mPos < this.mData.length) {
                if ((this.mData[this.mPos] & BYTE_MASK[this.mMask]) != 0) {
                    result |= 1;
                }
                this.mMask++;
                this.mPos += this.mMask / 8;
                this.mMask %= 8;
            }
        }
        return result + k;
    }

    /* access modifiers changed from: package-private */
    public final int getIntF(int bit_count, int add_bit_count) {
        int result = getInt(bit_count);
        if (result == (1 << bit_count) - 1) {
            return result + getInt(add_bit_count - bit_count);
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public final void add(boolean flag) {
        if (this.mPos == this.mData.length) {
            byte[] old_data = this.mData;
            this.mData = new byte[(old_data.length + DATA_QUANTUM)];
            System.arraycopy(old_data, 0, this.mData, 0, old_data.length);
        }
        if (flag) {
            byte[] bArr = this.mData;
            int i = this.mPos;
            bArr[i] = (byte) (bArr[i] | BYTE_MASK[this.mMask]);
        } else {
            byte[] bArr2 = this.mData;
            int i2 = this.mPos;
            bArr2[i2] = (byte) (bArr2[i2] & (BYTE_MASK[this.mMask] ^ -1));
        }
        this.mMask++;
        this.mPos += this.mMask / 8;
        this.mMask %= 8;
    }

    /* access modifiers changed from: package-private */
    public final void add(int d, int bit_count) {
        int mask = 1;
        if (d < 0) {
            d = 0;
        }
        if (bit_count > 1) {
            mask = 1 << (bit_count - 1);
        }
        while (mask != 0) {
            if (this.mPos == this.mData.length) {
                byte[] old_data = this.mData;
                this.mData = new byte[(old_data.length + DATA_QUANTUM)];
                System.arraycopy(old_data, 0, this.mData, 0, old_data.length);
            }
            if ((d & mask) != 0) {
                byte[] bArr = this.mData;
                int i = this.mPos;
                bArr[i] = (byte) (bArr[i] | BYTE_MASK[this.mMask]);
            } else {
                byte[] bArr2 = this.mData;
                int i2 = this.mPos;
                bArr2[i2] = (byte) (bArr2[i2] & (BYTE_MASK[this.mMask] ^ -1));
            }
            mask >>= 1;
            this.mMask++;
            this.mPos += this.mMask / 8;
            this.mMask %= 8;
        }
    }

    /* access modifiers changed from: package-private */
    public final void add(int d, int bit_count1, int bit_count2) {
        int threshold = 1;
        if (d < 0) {
            d = 0;
        }
        if (bit_count1 > 1) {
            threshold = 1 << (bit_count1 - 1);
        }
        if (d < threshold) {
            add(d, bit_count1);
            return;
        }
        add(1, 1);
        add(d - threshold, bit_count2 - 1);
    }
}
