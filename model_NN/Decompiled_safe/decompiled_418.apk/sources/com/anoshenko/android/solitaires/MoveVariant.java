package com.anoshenko.android.solitaires;

final class MoveVariant {
    final int Count;
    final int Number;
    final MoveVariantType Type;
    final Card card;

    MoveVariant(MoveVariantType type, int number, int count, Card card2) {
        this.Number = number;
        this.Count = count;
        this.card = card2;
        this.Type = type;
    }
}
