package com.anoshenko.android.solitaires;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public final class Card {
    public static final int INVISIBLE_CARD = -1;
    public static final int JOKER = 0;
    public static final int NEXT_DOWN = 1;
    public static final int NEXT_DRAG = 5;
    public static final int NEXT_LEFT = 4;
    public static final int NEXT_NO = 0;
    public static final int NEXT_RIGHT = 3;
    public static final int NEXT_UP = 2;
    private CardData mCardData;
    public final int mCardMask;
    public final int mColor;
    public boolean mLock;
    public boolean mMark;
    public int mNextOffset;
    public boolean mOpen;
    public final int mSuit;
    public final int mSuitMask;
    public final int mValue;
    public final int mValueMask;
    public Card next;
    public Card prev;
    public int xPos;
    public int yPos;

    public Card(int suit, int value, CardData data) {
        this.mCardData = data;
        this.mSuit = suit;
        this.mValue = value;
        this.mColor = suit / 2;
        this.mValueMask = 1 << value;
        this.mSuitMask = 65536 << suit;
        this.mCardMask = this.mValueMask | this.mSuitMask;
    }

    public Card(Card card) {
        this(card.mSuit, card.mValue, card.mCardData);
        this.mOpen = card.mOpen;
        this.mNextOffset = card.mNextOffset;
    }

    public void getRect(Rect rect) {
        rect.set(this.xPos, this.yPos, this.xPos + this.mCardData.Width, this.yPos + this.mCardData.Height);
    }

    public Rect getRect() {
        return new Rect(this.xPos, this.yPos, this.xPos + this.mCardData.Width, this.yPos + this.mCardData.Height);
    }

    public void setCardData(CardData data) {
        this.mCardData = data;
    }

    public CardData getCardData() {
        return this.mCardData;
    }

    public int getWidth() {
        if (this.mCardData == null) {
            return 0;
        }
        return this.mCardData.Width;
    }

    public int getHeight() {
        if (this.mCardData == null) {
            return 0;
        }
        return this.mCardData.Height;
    }

    public final void draw(Canvas canvas, Rect clip_rect) {
        Bitmap image;
        try {
            CardData data = this.mCardData;
            if (!this.mOpen) {
                image = data.mBackImage;
            } else if (this.mMark) {
                image = data.mMarkImage;
            } else {
                image = data.mBaseImage;
            }
            Rect dst_rect = new Rect(this.xPos, this.yPos, this.xPos + data.Width, this.yPos + data.Height);
            Rect rect = new Rect(0, 0, data.Width, data.Height);
            switch (this.mNextOffset) {
                case 1:
                    rect.bottom = data.yOffset + data.Angle;
                    dst_rect.bottom = this.yPos + rect.bottom;
                    break;
                case 2:
                    rect.top = (data.Height - data.yOffset) - data.Angle;
                    dst_rect.top += rect.top;
                    break;
                case 3:
                    rect.right = data.xOffset + data.Angle;
                    dst_rect.right = this.xPos + rect.right;
                    break;
                case 4:
                    rect.left = (data.Width - data.xOffset) - data.Angle;
                    dst_rect.left += rect.left;
                    break;
                case 5:
                    image = data.mDragImage;
                    break;
            }
            if (clip_rect == null || Rect.intersects(dst_rect, clip_rect)) {
                Paint paint = new Paint();
                canvas.drawBitmap(image, rect, dst_rect, paint);
                if (this.mOpen && this.mValue >= 0) {
                    if (this.mValue > 0) {
                        int value_color = this.mSuit / 2 == 0 ? -16777216 : -1638400;
                        switch (this.mNextOffset) {
                            case 0:
                            case 1:
                            case 3:
                            case 5:
                                data.drawTopLeftValue(canvas, this.xPos, this.yPos, this.mValue, value_color);
                                break;
                            case 2:
                            case 4:
                            default:
                                data.drawBottomRightValue(canvas, this.xPos, this.yPos, this.mValue, value_color);
                                break;
                        }
                        int w = data.mSuitImage.getWidth() / 4;
                        int h = data.mSuitImage.getHeight();
                        rect.left = this.mSuit * w;
                        rect.right = rect.left + w;
                        rect.top = 0;
                        rect.bottom = h;
                        switch (this.mNextOffset) {
                            case 0:
                            case 1:
                            case 4:
                            case 5:
                                dst_rect.left = ((this.xPos + data.Width) - (data.getBorderWidth() * 2)) - w;
                                dst_rect.top = this.yPos + (data.getBorderWidth() * 2);
                                break;
                            case 2:
                            case 3:
                            default:
                                dst_rect.left = this.xPos + (data.getBorderWidth() * 2);
                                dst_rect.top = ((this.yPos + data.Height) - (data.getBorderWidth() * 2)) - h;
                                break;
                        }
                        dst_rect.right = dst_rect.left + w;
                        dst_rect.bottom = dst_rect.top + h;
                        canvas.drawBitmap(data.mSuitImage, rect, dst_rect, paint);
                        if (this.mNextOffset == 0 || this.mNextOffset == 5) {
                            int n = (this.mValue - 1) * 2;
                            int w2 = data.PictureData[n + 1];
                            int h2 = data.mPictureImage.getHeight() / 4;
                            rect.left = data.PictureData[n];
                            rect.right = rect.left + w2;
                            rect.top = this.mSuit * h2;
                            rect.bottom = rect.top + h2;
                            dst_rect.left = this.xPos + ((data.Width - w2) / 2);
                            dst_rect.right = dst_rect.left + w2;
                            dst_rect.top = ((this.yPos + data.Height) - data.getBorderWidth()) - h2;
                            dst_rect.bottom = dst_rect.top + h2;
                            canvas.drawBitmap(data.mPictureImage, rect, dst_rect, paint);
                            return;
                        }
                        return;
                    }
                    int w3 = data.mJokerImage.getWidth();
                    int h3 = data.mJokerImage.getHeight();
                    rect.top = 0;
                    rect.left = 0;
                    rect.right = w3;
                    rect.bottom = h3;
                    dst_rect.left = this.xPos + ((data.Width - w3) / 2);
                    dst_rect.right = dst_rect.left + w3;
                    dst_rect.top = this.yPos + ((data.Height - h3) / 2);
                    dst_rect.bottom = dst_rect.top + h3;
                    canvas.drawBitmap(data.mJokerImage, rect, dst_rect, paint);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isBelong(int x, int y) {
        if (this.mCardData == null) {
            return false;
        }
        return this.mNextOffset != -1 && this.xPos <= x && x < this.xPos + this.mCardData.Width && this.yPos <= y && y < this.yPos + this.mCardData.Height;
    }

    public int getOverlapping(Card card) {
        if (this.mCardData == null) {
            return 0;
        }
        CardData data = this.mCardData;
        int left = this.xPos;
        int top = this.yPos;
        int width = data.Width;
        int height = data.Height;
        switch (this.mNextOffset) {
            case -1:
                return 0;
            case 2:
                top += data.Height - data.yOffset;
            case 1:
                height = data.yOffset;
                break;
            case 3:
                left += data.Width - data.xOffset;
            case 4:
                width = data.xOffset;
                break;
        }
        int right = left + width;
        int bottom = top + height;
        int card_right = card.xPos + data.Width;
        int card_bottom = card.yPos + data.Height;
        if (card.xPos >= right || left >= card_right || card.yPos >= bottom || top >= card_bottom) {
            return 0;
        }
        if (left < card.xPos) {
            left = card.xPos;
        }
        if (card_right < right) {
            right = card_right;
        }
        if (top < card.yPos) {
            top = card.yPos;
        }
        if (card_bottom < bottom) {
            bottom = card_bottom;
        }
        return (right - left) * (bottom - top);
    }
}
