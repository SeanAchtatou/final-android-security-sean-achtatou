package com.anoshenko.android.solitaires;

/* compiled from: Coordinate */
class CoordinateY extends Coordinate {
    CoordinateY(DataSource source) {
        super(source);
    }

    CoordinateY() {
    }

    /* access modifiers changed from: package-private */
    public int getTop(int top_border, int screen_height, CardData data) {
        int i;
        int pos = top_border;
        switch (this.mType) {
            case 0:
                int pos2 = pos + (this.mPos * data.Height);
                if (this.mOffset) {
                    return pos2 + 2;
                }
                return pos2;
            case 1:
                int pos3 = pos + (screen_height - ((this.mPos + 1) * data.Height));
                if (this.mOffset) {
                    return pos3 - 2;
                }
                return pos3;
            case 2:
                int pos4 = pos + (screen_height / 2) + (this.mPos * data.Height);
                if (!this.mOffset) {
                    return pos4;
                }
                if (this.mPos < 0) {
                    i = -2;
                } else {
                    i = 2;
                }
                return pos4 + i;
            case 3:
                int pos5 = pos + ((screen_height / 2) - (data.Height / 2)) + (this.mPos * data.Height);
                if (!this.mOffset) {
                    return pos5;
                }
                return pos5 + (this.mPos < 0 ? -2 : 2);
            default:
                return pos;
        }
    }

    /* access modifiers changed from: package-private */
    public int getBottom(int top_border, int screen_height, CardData data) {
        int i;
        int pos = top_border;
        switch (this.mType) {
            case 0:
                int pos2 = pos + ((this.mPos + 1) * data.Height);
                if (this.mOffset) {
                    return pos2 + 2;
                }
                return pos2;
            case 1:
                int pos3 = pos + (screen_height - (this.mPos * data.Height));
                if (this.mOffset) {
                    return pos3 - 2;
                }
                return pos3;
            case 2:
                int pos4 = pos + (screen_height / 2) + (this.mPos * data.Height);
                if (!this.mOffset) {
                    return pos4;
                }
                if (this.mPos < 0) {
                    i = -2;
                } else {
                    i = 2;
                }
                return pos4 + i;
            case 3:
                int pos5 = pos + (screen_height / 2) + (data.Height / 2) + (this.mPos * data.Height);
                if (!this.mOffset) {
                    return pos5;
                }
                return pos5 + (this.mPos < 0 ? -2 : 2);
            default:
                return pos;
        }
    }
}
