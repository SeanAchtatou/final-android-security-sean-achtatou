package com.anoshenko.android.solitaires;

import android.os.Bundle;
import android.os.Handler;
import com.anoshenko.android.ui.Title;

public class DemoActivity extends GameActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Title) findViewById(R.id.GameTitle)).setTitle("Demo: " + this.mGameName);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        new Handler().post(new Runnable() {
            public void run() {
                ((Demo) DemoActivity.this.mView.mGame).start();
            }
        });
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        ((Demo) this.mView.mGame).Terminate();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void createGame(GameView view) {
        view.setGame(this, new Demo(this, view, this.mSource));
    }

    /* access modifiers changed from: package-private */
    public void Message_YouHaveWon() {
    }
}
