package com.anoshenko.android.solitaires;

import android.graphics.Canvas;

public interface AdditionalDraw {
    void draw(Canvas canvas);
}
