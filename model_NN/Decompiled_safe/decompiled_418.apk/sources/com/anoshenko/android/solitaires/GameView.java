package com.anoshenko.android.solitaires;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

public class GameView extends View {
    GameActivity mActivity;
    public Game mGame = null;

    public GameView(Context context) {
        super(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GameView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: package-private */
    public void setGame(GameActivity activity, Game game) {
        this.mActivity = activity;
        this.mGame = game;
        int w = getWidth();
        int h = getHeight();
        if (w == 0) {
            DisplayMetrics dm = Utils.getDisplayMetrics(activity);
            w = dm.widthPixels;
            h = dm.heightPixels;
        }
        this.mGame.setScreenSize(w, h - 25);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        if (this.mGame != null) {
            this.mGame.setScreenSize(w, h);
            invalidate();
        }
    }

    public void onDraw(Canvas canvas) {
        Rect clip_rect = canvas.getClipBounds();
        if (clip_rect.left == clip_rect.right || clip_rect.top == clip_rect.bottom) {
            clip_rect.top = 0;
            clip_rect.left = 0;
            clip_rect.right = getWidth();
            clip_rect.bottom = getHeight();
        }
        this.mGame.draw(canvas, clip_rect);
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean result = true;
        switch (event.getAction()) {
            case 0:
                result = this.mGame.PenDown((int) event.getX(), (int) event.getY());
                break;
            case 1:
                this.mGame.PenUp((int) event.getX(), (int) event.getY());
                break;
            case 2:
                this.mGame.PenDrag((int) event.getX(), (int) event.getY());
                break;
        }
        this.mGame.CorrectAndRedrawIfNeed();
        return result;
    }
}
