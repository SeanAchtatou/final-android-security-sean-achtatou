package com.anoshenko.android.solitaires;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.util.Iterator;

public class ShowPileDialog extends View {
    private final CardList mCards = new CardList();

    public ShowPileDialog(Context context, Pile pile) {
        super(context);
        int line_count;
        CardData data = pile.mOwner.mGame.mCardData;
        int pile_size = pile.size();
        if (pile_size != 0 && data != null) {
            int line_size = (((Utils.getDisplayMetrics(context).widthPixels - 60) - data.Width) / data.xOffset) + 1;
            if (line_size > pile_size) {
                line_size = pile_size;
                line_count = 1;
            } else {
                line_count = ((pile_size + line_size) - 1) / line_size;
            }
            int x = 0;
            int y = 0;
            int n = 0;
            int last_n = pile_size - 1;
            int line_end = line_size - 1;
            Iterator<Card> it = pile.iterator();
            while (it.hasNext()) {
                Card src_card = it.next();
                Card card = new Card(src_card.mSuit, src_card.mValue, pile.mOwner.mGame.mCardData);
                card.xPos = x;
                card.yPos = y;
                card.mOpen = src_card.mOpen;
                if (n == last_n || n % line_size == line_end) {
                    card.mNextOffset = 0;
                    x = 0;
                    y += data.Height;
                } else {
                    card.mNextOffset = 3;
                    x += data.xOffset;
                }
                this.mCards.add(card);
                n++;
            }
            setLayoutParams(new ViewGroup.LayoutParams(data.Width + (data.xOffset * (line_size - 1)), data.Height * line_count));
        }
    }

    public void onDraw(Canvas g) {
        this.mCards.draw(g, null);
    }

    public static void show(Context context, Pile pile) {
        View view = LayoutInflater.from(context).inflate((int) R.layout.show_pile_view, (ViewGroup) null);
        ((LinearLayout) view.findViewById(R.id.ShowPileLayout)).addView(new ShowPileDialog(context, pile));
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setView(view);
        dialog.setNeutralButton((int) R.string.ok_button, (DialogInterface.OnClickListener) null);
        dialog.setCancelable(true);
        dialog.show().setCanceledOnTouchOutside(true);
    }
}
