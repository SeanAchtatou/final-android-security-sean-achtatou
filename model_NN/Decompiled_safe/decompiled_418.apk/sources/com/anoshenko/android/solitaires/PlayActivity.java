package com.anoshenko.android.solitaires;

import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import com.anoshenko.android.ui.Title;

public class PlayActivity extends GameActivity {
    private boolean mGameSaved = false;
    private final Runnable mGameStart = new Runnable() {
        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public void run() {
            if (PlayActivity.this.mView.getWidth() <= 0 || PlayActivity.this.mView.getHeight() <= 0) {
                new Handler().postDelayed(this, 100);
            } else {
                ((GamePlay) PlayActivity.this.mView.mGame).Start();
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Title) findViewById(R.id.GameTitle)).setTitle(this.mGameName);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mGameSaved = false;
        GamePlay game_play = (GamePlay) this.mView.mGame;
        if (game_play.mStarted) {
            game_play.ResumeTime();
        } else {
            new Handler().post(this.mGameStart);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle save) {
        this.mCardMoveView.cancel();
        ((GamePlay) this.mView.mGame).Store();
        this.mGameSaved = true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (!this.mGameSaved) {
            this.mCardMoveView.cancel();
            GamePlay game_play = (GamePlay) this.mView.mGame;
            game_play.PauseTime();
            new Thread(new GamePlaySaving(game_play), "GamePlaySaving").start();
        }
        super.onPause();
    }

    private class GamePlaySaving implements Runnable {
        private final GamePlay mGamePlay;

        GamePlaySaving(GamePlay game_play) {
            this.mGamePlay = game_play;
        }

        public void run() {
            this.mGamePlay.Store();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void createGame(GameView view) {
        GamePlay game;
        switch (this.mGameType) {
            case 0:
                game = new GamePlay_Type0(this, view, this.mSource);
                break;
            case 1:
                game = new GamePlay_Type1(this, view, this.mSource);
                break;
            case 2:
                game = new GamePlay_Type2(this, view, this.mSource);
                break;
            default:
                return;
        }
        view.setGame(this, game);
    }

    /* access modifiers changed from: protected */
    public void loadOptions() {
        boolean f_old_mirror = isMirror();
        boolean f_old_hide_pack_size = isHidePackSize();
        super.loadOptions();
        if ((isMirror() != f_old_mirror || f_old_hide_pack_size != isHidePackSize()) && this.mView != null) {
            this.mView.invalidate();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Utils.createMenu(this, menu);
        return true;
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        ((GamePlay) this.mView.mGame).isEnableEvent();
        return Utils.onMenuItemSelected(this, item);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            ((GamePlay) this.mView.mGame).isEnableEvent();
        }
        return super.onKeyDown(keyCode, event);
    }
}
