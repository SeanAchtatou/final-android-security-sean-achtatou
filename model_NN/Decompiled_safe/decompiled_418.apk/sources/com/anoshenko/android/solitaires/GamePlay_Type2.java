package com.anoshenko.android.solitaires;

import android.view.View;
import com.anoshenko.android.solitaires.GamePlay;
import java.util.Iterator;
import java.util.Vector;

final class GamePlay_Type2 extends GamePlay {
    GamePlay_Type2(GameActivity activity, View view, DataSource source) {
        super(activity, view, source);
    }

    /* access modifiers changed from: package-private */
    public boolean isPenDownPile(Pile from_pile) {
        if (from_pile.mOwner.mFoundation || !from_pile.isEnableRemove(from_pile.size() - 1, 1)) {
            return false;
        }
        for (PileGroup group : this.mGroup) {
            if (group.mFoundation) {
                for (Pile pile : group.mPile) {
                    if (pile.isEnableAdd(from_pile, 1, from_pile.lastElement())) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isPenDownPackOpen() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void PenDownAction(int x, int y, Vector<Pile> pile_list) {
        if (pile_list.size() > 0) {
            Pile from_pile = pile_list.firstElement();
            int from_number = from_pile.size() - 1;
            for (PileGroup group : this.mGroup) {
                if (group.mFoundation) {
                    for (Pile pile : group.mPile) {
                        if (pile.isEnableAdd(from_pile, 1, from_pile.lastElement())) {
                            MoveCardAndSave(from_pile, from_number, pile, true, new GamePlay.ResumeMoveAction(false));
                            return;
                        }
                    }
                    continue;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isAvailablePileMove() {
        return findAvailableMoves(false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:118:0x0038, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean findAvailableMoves(boolean r27) {
        /*
            r26 = this;
            r7 = 0
            if (r27 == 0) goto L_0x0006
            r26.ResetSelection()
        L_0x0006:
            r0 = r26
            com.anoshenko.android.solitaires.PileGroup[] r0 = r0.mGroup
            r12 = r0
            int r13 = r12.length
            r14 = 0
        L_0x000d:
            if (r14 < r13) goto L_0x0011
            r12 = r7
        L_0x0010:
            return r12
        L_0x0011:
            r10 = r12[r14]
            int r15 = r10.mRemoveType
            if (r15 <= 0) goto L_0x0025
            com.anoshenko.android.solitaires.Pile[] r15 = r10.mPile
            r0 = r15
            int r0 = r0.length
            r16 = r0
            r17 = 0
        L_0x001f:
            r0 = r17
            r1 = r16
            if (r0 < r1) goto L_0x0028
        L_0x0025:
            int r14 = r14 + 1
            goto L_0x000d
        L_0x0028:
            r11 = r15[r17]
            int r18 = r11.size()
            if (r18 <= 0) goto L_0x0038
            r0 = r10
            int r0 = r0.mRemoveType
            r18 = r0
            switch(r18) {
                case 1: goto L_0x003b;
                case 2: goto L_0x00b4;
                case 3: goto L_0x012d;
                case 4: goto L_0x01a3;
                default: goto L_0x0038;
            }
        L_0x0038:
            int r17 = r17 + 1
            goto L_0x001f
        L_0x003b:
            int r18 = r11.size()
            r19 = 1
            int r18 = r18 - r19
            r19 = 1
            r0 = r11
            r1 = r18
            r2 = r19
            boolean r18 = r0.isEnableRemove(r1, r2)
            if (r18 == 0) goto L_0x0038
            r0 = r26
            com.anoshenko.android.solitaires.PileGroup[] r0 = r0.mGroup
            r18 = r0
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r20 = 0
        L_0x005d:
            r0 = r20
            r1 = r19
            if (r0 >= r1) goto L_0x0038
            r5 = r18[r20]
            r0 = r5
            boolean r0 = r0.mFoundation
            r21 = r0
            if (r21 == 0) goto L_0x0085
            r0 = r5
            int r0 = r0.mAddType
            r21 = r0
            if (r21 <= 0) goto L_0x0085
            r0 = r5
            com.anoshenko.android.solitaires.Pile[] r0 = r0.mPile
            r21 = r0
            r0 = r21
            int r0 = r0.length
            r22 = r0
            r23 = 0
        L_0x007f:
            r0 = r23
            r1 = r22
            if (r0 < r1) goto L_0x0088
        L_0x0085:
            int r20 = r20 + 1
            goto L_0x005d
        L_0x0088:
            r6 = r21[r23]
            if (r11 == r6) goto L_0x00b1
            r24 = 1
            com.anoshenko.android.solitaires.Card r25 = r11.lastElement()
            r0 = r6
            r1 = r11
            r2 = r24
            r3 = r25
            boolean r24 = r0.isEnableAdd(r1, r2, r3)
            if (r24 == 0) goto L_0x00b1
            if (r27 == 0) goto L_0x00ae
            com.anoshenko.android.solitaires.Card r18 = r11.lastElement()
            r19 = 1
            r0 = r19
            r1 = r18
            r1.mMark = r0
            r7 = 1
            goto L_0x0038
        L_0x00ae:
            r12 = 1
            goto L_0x0010
        L_0x00b1:
            int r23 = r23 + 1
            goto L_0x007f
        L_0x00b4:
            com.anoshenko.android.solitaires.Card r4 = r11.lastElement()
            int r18 = r11.size()
            r19 = 1
            int r8 = r18 - r19
        L_0x00c0:
            if (r8 < 0) goto L_0x0038
            r18 = 1
            r0 = r11
            r1 = r8
            r2 = r18
            boolean r18 = r0.isEnableRemove(r1, r2)
            if (r18 == 0) goto L_0x00e1
            r0 = r26
            com.anoshenko.android.solitaires.PileGroup[] r0 = r0.mGroup
            r18 = r0
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r20 = 0
        L_0x00db:
            r0 = r20
            r1 = r19
            if (r0 < r1) goto L_0x00e6
        L_0x00e1:
            com.anoshenko.android.solitaires.Card r4 = r4.prev
            int r8 = r8 + -1
            goto L_0x00c0
        L_0x00e6:
            r5 = r18[r20]
            r0 = r5
            boolean r0 = r0.mFoundation
            r21 = r0
            if (r21 == 0) goto L_0x0108
            r0 = r5
            int r0 = r0.mAddType
            r21 = r0
            if (r21 <= 0) goto L_0x0108
            r0 = r5
            com.anoshenko.android.solitaires.Pile[] r0 = r0.mPile
            r21 = r0
            r0 = r21
            int r0 = r0.length
            r22 = r0
            r23 = 0
        L_0x0102:
            r0 = r23
            r1 = r22
            if (r0 < r1) goto L_0x010b
        L_0x0108:
            int r20 = r20 + 1
            goto L_0x00db
        L_0x010b:
            r6 = r21[r23]
            if (r11 == r6) goto L_0x012a
            r24 = 1
            r0 = r6
            r1 = r11
            r2 = r24
            r3 = r4
            boolean r24 = r0.isEnableAdd(r1, r2, r3)
            if (r24 == 0) goto L_0x012a
            if (r27 == 0) goto L_0x0127
            r18 = 1
            r0 = r18
            r1 = r4
            r1.mMark = r0
            r7 = 1
            goto L_0x00e1
        L_0x0127:
            r12 = 1
            goto L_0x0010
        L_0x012a:
            int r23 = r23 + 1
            goto L_0x0102
        L_0x012d:
            com.anoshenko.android.solitaires.Card r4 = r11.lastElement()
            r8 = 1
        L_0x0132:
            int r18 = r11.size()
            r0 = r8
            r1 = r18
            if (r0 > r1) goto L_0x0038
            int r18 = r11.size()
            int r18 = r18 - r8
            r0 = r11
            r1 = r18
            r2 = r8
            boolean r18 = r0.isEnableRemove(r1, r2)
            if (r18 == 0) goto L_0x0038
            r0 = r26
            com.anoshenko.android.solitaires.PileGroup[] r0 = r0.mGroup
            r18 = r0
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r20 = 0
        L_0x0158:
            r0 = r20
            r1 = r19
            if (r0 < r1) goto L_0x0163
        L_0x015e:
            com.anoshenko.android.solitaires.Card r4 = r4.prev
            int r8 = r8 + 1
            goto L_0x0132
        L_0x0163:
            r5 = r18[r20]
            r0 = r5
            boolean r0 = r0.mFoundation
            r21 = r0
            if (r21 == 0) goto L_0x0185
            r0 = r5
            int r0 = r0.mAddType
            r21 = r0
            if (r21 <= 0) goto L_0x0185
            r0 = r5
            com.anoshenko.android.solitaires.Pile[] r0 = r0.mPile
            r21 = r0
            r0 = r21
            int r0 = r0.length
            r22 = r0
            r23 = 0
        L_0x017f:
            r0 = r23
            r1 = r22
            if (r0 < r1) goto L_0x0188
        L_0x0185:
            int r20 = r20 + 1
            goto L_0x0158
        L_0x0188:
            r6 = r21[r23]
            if (r11 == r6) goto L_0x01a0
            boolean r24 = r6.isEnableAdd(r11, r8, r4)
            if (r24 == 0) goto L_0x01a0
            if (r27 == 0) goto L_0x019d
            r18 = 1
            r0 = r18
            r1 = r4
            r1.mMark = r0
            r7 = 1
            goto L_0x015e
        L_0x019d:
            r12 = 1
            goto L_0x0010
        L_0x01a0:
            int r23 = r23 + 1
            goto L_0x017f
        L_0x01a3:
            int r18 = r11.size()
            r0 = r10
            int r0 = r0.mRemoveSeriesSize
            r19 = r0
            r0 = r18
            r1 = r19
            if (r0 < r1) goto L_0x0038
            int r18 = r11.size()
            r0 = r10
            int r0 = r0.mRemoveSeriesSize
            r19 = r0
            int r9 = r18 - r19
            r0 = r10
            int r0 = r0.mRemoveSeriesSize
            r18 = r0
            r0 = r11
            r1 = r9
            r2 = r18
            boolean r18 = r0.isEnableRemove(r1, r2)
            if (r18 == 0) goto L_0x0038
            com.anoshenko.android.solitaires.Card r4 = r11.getCard(r9)
            r0 = r26
            com.anoshenko.android.solitaires.PileGroup[] r0 = r0.mGroup
            r18 = r0
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r20 = 0
        L_0x01dd:
            r0 = r20
            r1 = r19
            if (r0 >= r1) goto L_0x0038
            r5 = r18[r20]
            r0 = r5
            boolean r0 = r0.mFoundation
            r21 = r0
            if (r21 == 0) goto L_0x0205
            r0 = r5
            int r0 = r0.mAddType
            r21 = r0
            if (r21 <= 0) goto L_0x0205
            r0 = r5
            com.anoshenko.android.solitaires.Pile[] r0 = r0.mPile
            r21 = r0
            r0 = r21
            int r0 = r0.length
            r22 = r0
            r23 = 0
        L_0x01ff:
            r0 = r23
            r1 = r22
            if (r0 < r1) goto L_0x0208
        L_0x0205:
            int r20 = r20 + 1
            goto L_0x01dd
        L_0x0208:
            r6 = r21[r23]
            if (r11 == r6) goto L_0x022b
            r0 = r10
            int r0 = r0.mRemoveSeriesSize
            r24 = r0
            r0 = r6
            r1 = r11
            r2 = r24
            r3 = r4
            boolean r24 = r0.isEnableAdd(r1, r2, r3)
            if (r24 == 0) goto L_0x022b
            if (r27 == 0) goto L_0x0228
            r18 = 1
            r0 = r18
            r1 = r4
            r1.mMark = r0
            r7 = 1
            goto L_0x0038
        L_0x0228:
            r12 = 1
            goto L_0x0010
        L_0x022b:
            int r23 = r23 + 1
            goto L_0x01ff
        */
        throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.GamePlay_Type2.findAvailableMoves(boolean):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean AvailableMoves() {
        if (findAvailableMoves(true)) {
            super.AvailableMoves();
            return false;
        }
        NoAvailableMovesMessage();
        return false;
    }

    /* access modifiers changed from: package-private */
    public void AvailableMovesFinish() {
        for (Pile pile : this.mPiles) {
            if (pile.size() > 0) {
                Iterator<Card> it = pile.iterator();
                while (it.hasNext()) {
                    it.next().mMark = false;
                }
            }
        }
        super.AvailableMovesFinish();
    }

    /* access modifiers changed from: package-private */
    public boolean isNextAvailableMovesExist() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void NextAvailableMoves() {
    }
}
