package com.anoshenko.android.solitaires;

final class CloseRegion {
    final int mCount;
    final int[] mFirst;
    final int[] mLength;

    CloseRegion(BitStack stack) {
        this.mCount = stack.getIntF(3, 8);
        if (this.mCount > 0) {
            this.mFirst = new int[this.mCount];
            this.mLength = new int[this.mCount];
            for (int i = 0; i < this.mCount; i++) {
                this.mFirst[i] = stack.getInt(4, 8);
                this.mLength[i] = stack.getInt(4, 8) + 1;
            }
            return;
        }
        this.mFirst = null;
        this.mLength = null;
    }
}
