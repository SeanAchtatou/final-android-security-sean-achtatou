package com.anoshenko.android.solitaires;

import android.view.View;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.solitaires.GamePlay;
import java.util.Vector;

final class GamePlay_Type1 extends GamePlay {
    private int mAvailablePile1;
    private int mAvailablePile2;
    int mMarkedCount = 0;
    Pile[] mMarkedPiles = new Pile[this.mTrashRule.getMaxTrashCards()];
    private Runnable resetSelection = new Runnable() {
        public void run() {
            GamePlay_Type1.this.ResetSelection();
            GamePlay_Type1.this.mView.invalidate();
        }
    };

    GamePlay_Type1(GameActivity activity, View view, DataSource source) {
        super(activity, view, source);
    }

    /* access modifiers changed from: package-private */
    public void ResetSelection() {
        Card card;
        if (this.mMarkedCount > 0) {
            for (int i = 0; i < this.mMarkedCount; i++) {
                if (!(this.mMarkedPiles[i] == null || (card = this.mMarkedPiles[i].lastElement()) == null)) {
                    card.mMark = false;
                    addRedrawRect(card.getRect());
                }
            }
            this.mMarkedCount = 0;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isPenDownPile(Pile pile) {
        if (!pile.isEnableType1Remove() || this.mMarkedCount == this.mMarkedPiles.length) {
            return false;
        }
        if (this.mMarkedCount == 0) {
            return true;
        }
        for (int i = 0; i < this.mMarkedCount; i++) {
            if (this.mMarkedPiles[i] == pile) {
                return true;
            }
        }
        this.mMarkedPiles[this.mMarkedCount] = pile;
        return this.mTrashRule.TestTrashPiles(this.mMarkedPiles, this.mMarkedCount + 1);
    }

    /* access modifiers changed from: package-private */
    public boolean isPenDownPackOpen() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void PenDownAction(int x, int y, Vector<Pile> pile_list) {
        if (this.mMarkedCount <= 0 || pile_list.size() <= 0) {
            if (this.mPack.isBelong(x, y)) {
                ResetSelection();
                new Game.PackOpenCardsAction(new GamePlay.ResumeMoveAction(true), this.mMoves).run();
            }
            if (pile_list.size() > 0) {
                MarkLastCard(pile_list.firstElement());
                return;
            }
            return;
        }
        MarkLastCard(pile_list.firstElement());
    }

    private void MarkLastCard(Pile card_pile) {
        playTakeSound();
        addRedrawRect(this.mScreen);
        int i = 0;
        while (i < this.mMarkedCount) {
            if (this.mMarkedPiles[i] == card_pile) {
                card_pile.lastElement().mMark = false;
                while (true) {
                    i++;
                    if (i >= this.mMarkedCount) {
                        this.mMarkedCount--;
                        return;
                    }
                    this.mMarkedPiles[i - 1] = this.mMarkedPiles[i];
                }
            } else {
                i++;
            }
        }
        if (card_pile != null && card_pile.lastElement() != null) {
            card_pile.lastElement().mMark = true;
            this.mMarkedPiles[this.mMarkedCount] = card_pile;
            this.mMarkedCount++;
            if (this.mTrashRule.TestTrashPiles(this.mMarkedPiles, this.mMarkedCount)) {
                for (int i2 = 0; i2 < this.mMarkedCount; i2++) {
                    Card card = this.mMarkedPiles[i2].removeLast();
                    card.mLock = false;
                    card.mMark = false;
                    this.mTrash.add(card);
                    this.mMarkedPiles[i2].Correct();
                }
                this.mMoves.IncreaseMoveNumber();
                this.mMoves.addTrashMove(this.mMarkedPiles, this.mMarkedCount);
                this.mMarkedCount = 0;
                new GamePlay.ResumeMoveAction(true).run();
            } else if (this.mMarkedCount == this.mMarkedPiles.length) {
                this.mView.postDelayed(this.resetSelection, 1000);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean isAvailablePileMove() {
        Pile[] piles = new Pile[2];
        for (int i = 0; i < this.mPiles.length; i++) {
            if (this.mPiles[i].isEnableType1Remove()) {
                piles[0] = this.mPiles[i];
                if (this.mTrashRule.TestTrashPiles(piles, 1)) {
                    return true;
                }
                for (int k = i + 1; k < this.mPiles.length; k++) {
                    if (this.mPiles[k].isEnableType1Remove()) {
                        piles[1] = this.mPiles[k];
                        if (this.mTrashRule.TestTrashPiles(piles, 2)) {
                            return true;
                        }
                    }
                }
                continue;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean AvailableMoves() {
        ResetSelection();
        this.mAvailablePile1 = 0;
        this.mAvailablePile2 = -1;
        if (findNextAvailableMoves(true)) {
            return super.AvailableMoves();
        }
        NoAvailableMovesMessage();
        return false;
    }

    private boolean findNextAvailableMoves(boolean f_set) {
        int n = this.mAvailablePile2 + 1;
        Pile[] piles = new Pile[2];
        for (int i = this.mAvailablePile1; i < this.mPiles.length; i++) {
            if (this.mPiles[i].isEnableType1Remove()) {
                piles[0] = this.mPiles[i];
                if (i != n || !this.mTrashRule.TestTrashPiles(piles, 1)) {
                    while (true) {
                        n++;
                        if (n >= this.mPiles.length) {
                            n = i + 1;
                            break;
                        } else if (n != i && this.mPiles[n].isEnableType1Remove()) {
                            piles[1] = this.mPiles[n];
                            if (this.mTrashRule.TestTrashPiles(piles, 2)) {
                                if (f_set) {
                                    this.mPiles[i].lastElement().mMark = true;
                                    this.mPiles[n].lastElement().mMark = true;
                                    this.mAvailablePile1 = i;
                                    this.mAvailablePile2 = n;
                                }
                                return true;
                            }
                        }
                    }
                } else {
                    if (f_set) {
                        this.mPiles[i].lastElement().mMark = true;
                        this.mAvailablePile2 = i;
                        this.mAvailablePile1 = i;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean isNextAvailableMovesExist() {
        return findNextAvailableMoves(false);
    }

    /* access modifiers changed from: package-private */
    public void AvailableMovesFinish() {
        this.mPiles[this.mAvailablePile1].lastElement().mMark = false;
        this.mPiles[this.mAvailablePile2].lastElement().mMark = false;
        super.AvailableMovesFinish();
    }

    /* access modifiers changed from: package-private */
    public void NextAvailableMoves() {
        this.mPiles[this.mAvailablePile1].lastElement().mMark = false;
        this.mPiles[this.mAvailablePile2].lastElement().mMark = false;
        findNextAvailableMoves(true);
        this.mView.invalidate();
    }
}
