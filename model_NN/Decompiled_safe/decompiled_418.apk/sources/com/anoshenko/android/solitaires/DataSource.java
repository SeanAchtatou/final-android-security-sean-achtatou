package com.anoshenko.android.solitaires;

import android.content.Context;
import android.content.Intent;
import java.io.IOException;
import java.io.InputStream;

final class DataSource {
    public static final String DEMO_SIZE = "Demo size";
    public static final String HELP_SIZE = "Help size";
    public static final String OFFSET = "Offset";
    public static final String RULE_SIZE = "Rules size";
    BitStack mDemo;
    final int mDemoSize;
    BitStack mHelp;
    final int mHelpSize;
    final int mOffset;
    BitStack mRule;
    final int mRuleSize;

    DataSource(Context context, Intent intent) throws IOException {
        this.mOffset = intent.getIntExtra(OFFSET, -1);
        this.mRuleSize = intent.getIntExtra(RULE_SIZE, 0);
        this.mDemoSize = intent.getIntExtra(DEMO_SIZE, 0);
        this.mHelpSize = intent.getIntExtra(HELP_SIZE, 0);
        if (this.mOffset >= 0) {
            InputStream stream = context.getResources().openRawResource(R.raw.games_data);
            stream.skip((long) this.mOffset);
            byte[] data = new byte[this.mRuleSize];
            stream.read(data);
            this.mRule = new BitStack(data);
            byte[] data2 = new byte[this.mDemoSize];
            stream.read(data2);
            this.mDemo = new BitStack(data2);
            byte[] data3 = new byte[this.mHelpSize];
            stream.read(data3);
            this.mHelp = new BitStack(data3);
            this.mRule.ResetPos();
            stream.close();
        }
    }
}
