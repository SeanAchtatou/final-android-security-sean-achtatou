package com.anoshenko.android.solitaires;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.widget.TextView;
import java.io.IOException;
import java.io.InputStream;

public class RulesDialog extends Dialog {
    private RulesDialog(Context context, String name, int off, int size) {
        super(context);
        Resources resources = context.getResources();
        try {
            InputStream stream = resources.openRawResource(R.raw.games_data);
            stream.skip((long) off);
            byte[] data = new byte[size];
            stream.read(data);
            BitStack stack = new BitStack(data);
            setTitle(name);
            setContentView(R.layout.rules_view);
            ((TextView) findViewById(R.id.RulesPurpose)).setText(R.string.rules000 + stack.getInt(8, 12));
            StringBuilder rules = new StringBuilder();
            int count1 = stack.getIntF(3, 8);
            for (int i = 0; i < count1; i++) {
                rules.append("<p><b>");
                boolean location = stack.getFlag();
                rules.append(resources.getString(R.string.rules000 + stack.getInt(8, 12)));
                if (location) {
                    rules.append(" (");
                    rules.append(resources.getString(R.string.rules000 + stack.getInt(8, 12)));
                    rules.append(')');
                }
                rules.append("</b></p>");
                rules.append("<p>");
                int count2 = stack.getIntF(3, 8);
                for (int k = 0; k < count2; k++) {
                    rules.append("• ");
                    rules.append(resources.getString(R.string.rules000 + stack.getInt(8, 12)));
                    rules.append("<br />");
                }
                rules.append("</p>");
            }
            ((TextView) findViewById(R.id.RulesText)).setText(Html.fromHtml(rules.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void show(Context context, String name, int off, int size) {
        new RulesDialog(context, name, off, size).show();
    }
}
