package com.anoshenko.android.solitaires;

import android.content.ContentValues;
import android.database.Cursor;

public class Statistics {
    private int mBestSeries;
    private int mBestTime;
    private int mCurrentSeries;
    private int mLosses;
    private int mWins;

    public void clear() {
        this.mBestSeries = 0;
        this.mCurrentSeries = 0;
        this.mBestTime = 0;
        this.mLosses = 0;
        this.mWins = 0;
    }

    public String getWins() {
        if (this.mWins == 0) {
            return this.mLosses == 0 ? "0" : "0 (0.00%)";
        }
        if (this.mLosses == 0) {
            return String.format("%d (100.00%%)", Integer.valueOf(this.mWins));
        }
        int percent = (this.mWins * 10000) / (this.mWins + this.mLosses);
        return String.format("%d (%d.%02d%%)", Integer.valueOf(this.mWins), Integer.valueOf(percent / 100), Integer.valueOf(percent % 100));
    }

    public String getLosses() {
        if (this.mLosses == 0) {
            return this.mWins == 0 ? "0" : "0 (0.00%)";
        }
        if (this.mWins == 0) {
            return String.format("%d (100.00%%)", Integer.valueOf(this.mLosses));
        }
        int percent = 10000 - ((this.mWins * 10000) / (this.mWins + this.mLosses));
        return String.format("%d (%d.%02d%%)", Integer.valueOf(this.mLosses), Integer.valueOf(percent / 100), Integer.valueOf(percent % 100));
    }

    public String getTotal() {
        return Integer.toString(this.mWins + this.mLosses);
    }

    public String getBestTime() {
        if (this.mBestTime == 0) {
            return "-";
        }
        return String.format("%d:%02d", Integer.valueOf(this.mBestTime / 60), Integer.valueOf(this.mBestTime % 60));
    }

    public String getCurrentSeries() {
        return Integer.toString(this.mCurrentSeries);
    }

    public String getBestSeries() {
        return Integer.toString(this.mBestSeries);
    }

    public void win(GamePlay game, int time) {
        this.mWins++;
        this.mCurrentSeries++;
        if (this.mCurrentSeries > this.mBestSeries) {
            this.mBestSeries = this.mCurrentSeries;
        }
        if (this.mBestTime == 0 || time < this.mBestTime) {
            this.mBestTime = time;
        }
        Backup.backupBySchedule(game.mActivity);
    }

    public void lose(GamePlay game) {
        this.mLosses++;
        this.mCurrentSeries = 0;
    }

    public void getContentValues(ContentValues values) {
        values.put(Storage.WINS, Integer.valueOf(this.mWins));
        values.put(Storage.LOSSES, Integer.valueOf(this.mLosses));
        values.put(Storage.BEST_TIME, Integer.valueOf(this.mBestTime));
        values.put(Storage.CURRENT_SERIES, Integer.valueOf(this.mCurrentSeries));
        values.put(Storage.BEST_SERIES, Integer.valueOf(this.mBestSeries));
    }

    public void load(Cursor cursor) {
        this.mWins = cursor.getInt(cursor.getColumnIndexOrThrow(Storage.WINS));
        this.mLosses = cursor.getInt(cursor.getColumnIndexOrThrow(Storage.LOSSES));
        this.mBestTime = cursor.getInt(cursor.getColumnIndexOrThrow(Storage.BEST_TIME));
        this.mCurrentSeries = cursor.getInt(cursor.getColumnIndexOrThrow(Storage.CURRENT_SERIES));
        this.mBestSeries = cursor.getInt(cursor.getColumnIndexOrThrow(Storage.BEST_SERIES));
    }
}
