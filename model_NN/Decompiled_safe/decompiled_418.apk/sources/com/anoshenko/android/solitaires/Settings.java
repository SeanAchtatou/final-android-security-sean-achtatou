package com.anoshenko.android.solitaires;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class Settings {
    private static final byte ANIMATION_FLAG = 64;
    private static final byte DEAL_ANIMATION_FLAG = Byte.MIN_VALUE;
    private static final byte HIDE_PACK_SIZE_FLAG = 16;
    private static final String LAST_BACKUP_DATE = "LAST_BACKUP_DATE";
    private static final byte MIRROR_LAYOUT_FLAG = 32;
    private static final byte MOVEMENT_SOUND_FLAG = 8;
    private static final byte VICTORY_SOUND_FLAG = 4;
    private final Context mContext;
    private final SharedPreferences mPreferences;

    public Settings(Context context) {
        this.mContext = context;
        this.mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public boolean isDealAnimation() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_deal_animation), true);
    }

    public boolean isAnimation() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_animation), true);
    }

    public boolean isMirrorLayout() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_mirror), false);
    }

    public boolean isHidePackSize() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_hide_pack_size), false);
    }

    public boolean isHidePackRedeal() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_hide_pack_redeal), false);
    }

    public boolean isEnableMovementSound() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_movement_sound), false);
    }

    public int getSoundLevel() {
        return this.mPreferences.getInt(this.mContext.getString(R.string.pref_sound_level), 5);
    }

    public int getAutoplay() {
        try {
            return Integer.parseInt(this.mPreferences.getString(this.mContext.getString(R.string.pref_autoplay), "1"));
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    public int getOrientation() {
        try {
            return Integer.parseInt(this.mPreferences.getString(this.mContext.getString(R.string.pref_orientation), "0"));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public String getVictorySound() {
        if (this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_victory_sound), false)) {
            return this.mPreferences.getString(this.mContext.getString(R.string.pref_select_victory_sound), "").trim();
        }
        return null;
    }

    public boolean isBackupEnabled() {
        return this.mPreferences.getBoolean(this.mContext.getString(R.string.pref_backup_statistics), true);
    }

    public long getLastBackupTime() {
        return this.mPreferences.getLong(LAST_BACKUP_DATE, 0);
    }

    public void setLastBackupTime(long time) {
        SharedPreferences.Editor editor = this.mPreferences.edit();
        editor.putLong(LAST_BACKUP_DATE, time);
        editor.commit();
    }

    public void store(OutputStream stream) throws IOException {
        byte[] buffer = new byte[5];
        if (isDealAnimation()) {
            buffer[0] = (byte) (buffer[0] | DEAL_ANIMATION_FLAG);
        }
        if (isAnimation()) {
            buffer[0] = (byte) (buffer[0] | ANIMATION_FLAG);
        }
        if (isMirrorLayout()) {
            buffer[0] = (byte) (buffer[0] | MIRROR_LAYOUT_FLAG);
        }
        if (isHidePackSize()) {
            buffer[0] = (byte) (buffer[0] | HIDE_PACK_SIZE_FLAG);
        }
        if (isEnableMovementSound()) {
            buffer[0] = (byte) (buffer[0] | MOVEMENT_SOUND_FLAG);
        }
        String victory_sound = getVictorySound();
        if (victory_sound != null) {
            buffer[0] = (byte) (buffer[0] | VICTORY_SOUND_FLAG);
        }
        stream.write(buffer);
        buffer[2] = (byte) getAutoplay();
        buffer[3] = (byte) getOrientation();
        buffer[4] = (byte) getSoundLevel();
        stream.write(buffer);
        if (victory_sound != null) {
            byte[] data = victory_sound.getBytes();
            buffer[0] = (byte) ((data.length >> 8) & 255);
            buffer[1] = (byte) (data.length & 255);
            stream.write(buffer, 0, 2);
            if (data.length > 0) {
                stream.write(data);
            }
        }
    }

    public void restore(InputStream stream, byte version) throws IOException {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        String path;
        byte[] buffer = new byte[5];
        SharedPreferences.Editor editor = this.mPreferences.edit();
        stream.read(buffer);
        editor.putBoolean(this.mContext.getString(R.string.pref_deal_animation), (buffer[0] & DEAL_ANIMATION_FLAG) != 0);
        String string = this.mContext.getString(R.string.pref_animation);
        if ((buffer[0] & ANIMATION_FLAG) != 0) {
            z = true;
        } else {
            z = false;
        }
        editor.putBoolean(string, z);
        String string2 = this.mContext.getString(R.string.pref_mirror);
        if ((buffer[0] & MIRROR_LAYOUT_FLAG) != 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        editor.putBoolean(string2, z2);
        String string3 = this.mContext.getString(R.string.pref_hide_pack_size);
        if ((buffer[0] & HIDE_PACK_SIZE_FLAG) != 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        editor.putBoolean(string3, z3);
        String string4 = this.mContext.getString(R.string.pref_movement_sound);
        if ((buffer[0] & MOVEMENT_SOUND_FLAG) != 0) {
            z4 = true;
        } else {
            z4 = false;
        }
        editor.putBoolean(string4, z4);
        String string5 = this.mContext.getString(R.string.pref_victory_sound);
        if ((buffer[0] & VICTORY_SOUND_FLAG) != 0) {
            z5 = true;
        } else {
            z5 = false;
        }
        editor.putBoolean(string5, z5);
        editor.putString(this.mContext.getString(R.string.pref_autoplay), Integer.toString(buffer[2]));
        editor.putString(this.mContext.getString(R.string.pref_orientation), Integer.toString(buffer[3]));
        editor.putInt(this.mContext.getString(R.string.pref_sound_level), buffer[4]);
        if ((buffer[0] & VICTORY_SOUND_FLAG) != 0) {
            stream.read(buffer, 0, 2);
            int len = ((buffer[0] & 255) << MOVEMENT_SOUND_FLAG) | (buffer[1] & 255);
            if (len > 0) {
                byte[] data = new byte[len];
                stream.read(data);
                path = new String(data);
            } else {
                path = "";
            }
            editor.putString(this.mContext.getString(R.string.pref_select_victory_sound), path);
        }
        editor.commit();
    }
}
