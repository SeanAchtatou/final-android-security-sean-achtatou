package com.anoshenko.android.solitaires;

/* compiled from: MoveVariant */
enum MoveVariantType {
    ONE,
    COMPLEX,
    SEQUENTIALLY
}
