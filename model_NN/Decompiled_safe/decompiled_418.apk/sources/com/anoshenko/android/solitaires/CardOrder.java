package com.anoshenko.android.solitaires;

final class CardOrder {
    private final CardOrderElement[] mOrder;

    CardOrder(Game game) {
        int count = game.mSource.mRule.getInt(1, 4) + 1;
        this.mOrder = new CardOrderElement[count];
        for (int i = 0; i < count; i++) {
            this.mOrder[i] = new CardOrderElement(game);
        }
    }

    /* access modifiers changed from: package-private */
    public final int getNextCard(int card_mask) {
        int out_card_mask = 0;
        for (CardOrderElement nextCard : this.mOrder) {
            out_card_mask |= nextCard.getNextCard(card_mask);
        }
        return out_card_mask;
    }

    /* access modifiers changed from: package-private */
    public final int getPrevCard(int card_mask) {
        int out_card_mask = 0;
        for (CardOrderElement prevCard : this.mOrder) {
            out_card_mask |= prevCard.getPrevCard(card_mask);
        }
        return out_card_mask;
    }

    /* access modifiers changed from: package-private */
    public final int getMaxTrashCards() {
        int result = 0;
        for (CardOrderElement maxTrashCards : this.mOrder) {
            int n = maxTrashCards.getMaxTrashCards();
            if (result < n) {
                result = n;
            }
        }
        return result;
    }

    /* access modifiers changed from: package-private */
    public final boolean TestTrashPiles(Pile[] piles, int count) {
        for (CardOrderElement TestTrashPiles : this.mOrder) {
            if (TestTrashPiles.TestTrashPiles(piles, count)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final boolean TestOrder(Card first_card, int count, boolean f_enable_jokers) {
        if (!f_enable_jokers) {
            Card card = first_card;
            for (int i = 0; i < count; i++) {
                if (card.mValue == 0) {
                    return false;
                }
                card = card.next;
            }
        }
        for (CardOrderElement TestOrder : this.mOrder) {
            if (TestOrder.TestOrder(first_card, count, f_enable_jokers)) {
                return true;
            }
        }
        return false;
    }
}
