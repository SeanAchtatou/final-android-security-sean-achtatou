package com.anoshenko.android.solitaires;

public enum CardSize {
    SMALL(0),
    NORMAL(1);
    
    public final int mId;

    private CardSize(int id) {
        this.mId = id;
    }

    public static final CardSize get(int id) {
        return id == 0 ? SMALL : NORMAL;
    }
}
