package com.anoshenko.android.solitaires;

/* compiled from: Coordinate */
class CoordinateX extends Coordinate {
    CoordinateX(DataSource source) {
        super(source);
    }

    CoordinateX() {
    }

    /* access modifiers changed from: package-private */
    public int getLeft(int left_border, int screen_width, CardData data) {
        int i;
        int pos = left_border;
        switch (this.mType) {
            case 0:
                int pos2 = pos + (this.mPos * data.Width);
                if (this.mOffset) {
                    return pos2 + 2;
                }
                return pos2;
            case 1:
                int pos3 = pos + (screen_width - ((this.mPos + 1) * data.Width));
                if (this.mOffset) {
                    return pos3 - 2;
                }
                return pos3;
            case 2:
                int pos4 = pos + (screen_width / 2) + (this.mPos * data.Width);
                if (!this.mOffset) {
                    return pos4;
                }
                if (this.mPos < 0) {
                    i = -2;
                } else {
                    i = 2;
                }
                return pos4 + i;
            case 3:
                int pos5 = pos + ((screen_width / 2) - (data.Width / 2)) + (this.mPos * data.Width);
                if (!this.mOffset) {
                    return pos5;
                }
                return pos5 + (this.mPos < 0 ? -2 : 2);
            default:
                return pos;
        }
    }

    /* access modifiers changed from: package-private */
    public int getRight(int left_border, int screen_width, CardData data) {
        int i;
        int pos = left_border;
        switch (this.mType) {
            case 0:
                int pos2 = pos + ((this.mPos + 1) * data.Width);
                if (this.mOffset) {
                    return pos2 + 2;
                }
                return pos2;
            case 1:
                int pos3 = pos + (screen_width - (this.mPos * data.Width));
                if (this.mOffset) {
                    return pos3 - 2;
                }
                return pos3;
            case 2:
                int pos4 = pos + (screen_width / 2) + (this.mPos * data.Width);
                if (!this.mOffset) {
                    return pos4;
                }
                if (this.mPos < 0) {
                    i = -2;
                } else {
                    i = 2;
                }
                return pos4 + i;
            case 3:
                int pos5 = pos + (screen_width / 2) + (data.Width / 2) + (this.mPos * data.Width);
                if (!this.mOffset) {
                    return pos5;
                }
                return pos5 + (this.mPos < 0 ? -2 : 2);
            default:
                return pos;
        }
    }
}
