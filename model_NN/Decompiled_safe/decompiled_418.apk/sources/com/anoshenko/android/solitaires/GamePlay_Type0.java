package com.anoshenko.android.solitaires;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import android.widget.Toast;
import com.anoshenko.android.solitaires.Game;
import com.anoshenko.android.solitaires.GamePlay;
import java.util.Iterator;
import java.util.Vector;

final class GamePlay_Type0 extends GamePlay {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$solitaires$MoveVariantType;
    private DragData mDrag;

    static /* synthetic */ int[] $SWITCH_TABLE$com$anoshenko$android$solitaires$MoveVariantType() {
        int[] iArr = $SWITCH_TABLE$com$anoshenko$android$solitaires$MoveVariantType;
        if (iArr == null) {
            iArr = new int[MoveVariantType.values().length];
            try {
                iArr[MoveVariantType.COMPLEX.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MoveVariantType.ONE.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MoveVariantType.SEQUENTIALLY.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$anoshenko$android$solitaires$MoveVariantType = iArr;
        }
        return iArr;
    }

    private final class CurrentAvailableMoves implements AdditionalDraw {
        int cardCount;
        int cardNumber;
        int pileNumber = 0;
        Vector<Pile> toPiles = new Vector<>();

        CurrentAvailableMoves() {
            this.cardNumber = GamePlay_Type0.this.mPiles[0].size();
        }

        public void draw(Canvas g) {
            int stopX;
            int i;
            int i2;
            ArrowDrawer drawer = new ArrowDrawer(GamePlay_Type0.this.mActivity);
            Card card = GamePlay_Type0.this.mPiles[this.pileNumber].getCard(this.cardNumber);
            int startX = card.xPos + (GamePlay_Type0.this.mCardData.Width / 2);
            int startY = card.yPos + (GamePlay_Type0.this.mCardData.Height / 2);
            switch (card.mNextOffset) {
                case 1:
                    startY = card.yPos + (GamePlay_Type0.this.mCardData.yOffset / 2);
                    break;
                case 2:
                    startY = (card.yPos + GamePlay_Type0.this.mCardData.Height) - (GamePlay_Type0.this.mCardData.yOffset / 2);
                    break;
                case 3:
                    startX = card.xPos + (GamePlay_Type0.this.mCardData.xOffset / 2);
                    break;
                case 4:
                    startX = (card.xPos + GamePlay_Type0.this.mCardData.Width) - (GamePlay_Type0.this.mCardData.xOffset / 2);
                    break;
            }
            Iterator<Pile> it = this.toPiles.iterator();
            while (it.hasNext()) {
                Pile pile = it.next();
                if (pile.size() == 0) {
                    stopX = pile.xPos + (GamePlay_Type0.this.mCardData.Width / 2);
                    i = pile.yPos;
                    i2 = GamePlay_Type0.this.mCardData.Height;
                } else {
                    stopX = pile.lastElement().xPos + (GamePlay_Type0.this.mCardData.Width / 2);
                    i = pile.lastElement().yPos;
                    i2 = GamePlay_Type0.this.mCardData.Height;
                }
                drawer.draw(g, startX, startY, stopX, i + (i2 / 2));
            }
        }
    }

    private final class DragData implements AdditionalDraw {
        Card card;
        int count;
        int number;
        final Vector<Pile> piles;
        final Rect rect = new Rect();
        Pile sourcePile;
        Pile targetPile;
        int x;
        int xStart;
        int y;
        int yStart;

        DragData(Vector<Pile> piles2, int x2, int y2) {
            this.piles = piles2;
            this.card = new Card(-1, -1, GamePlay_Type0.this.mCardData);
            this.card.mNextOffset = 5;
            this.card.mOpen = true;
            setPos(x2, y2);
            this.xStart = x2;
            this.yStart = y2;
            GamePlay_Type0.this.mDraw = this;
        }

        /* access modifiers changed from: package-private */
        public final void setPos(int x2, int y2) {
            int w_2 = GamePlay_Type0.this.mCardData.Width / 2;
            if (x2 < w_2) {
                this.x = w_2;
            } else if (x2 + w_2 > GamePlay_Type0.this.mScreen.right) {
                this.x = GamePlay_Type0.this.mScreen.right - w_2;
            } else {
                this.x = x2;
            }
            if (y2 < GamePlay_Type0.this.mCardData.Height) {
                this.y = GamePlay_Type0.this.mCardData.Height;
            } else if (y2 > GamePlay_Type0.this.mScreen.bottom) {
                this.y = GamePlay_Type0.this.mScreen.bottom;
            } else {
                this.y = y2;
            }
            this.card.xPos = this.x - w_2;
            this.card.yPos = this.y - GamePlay_Type0.this.mCardData.Height;
            this.rect.set(this.card.xPos, this.card.yPos, this.card.xPos + GamePlay_Type0.this.mCardData.Width, this.card.yPos + GamePlay_Type0.this.mCardData.Height);
        }

        public void draw(Canvas g) {
            this.card.draw(g, null);
        }
    }

    GamePlay_Type0(GameActivity activity, View view, DataSource source) {
        super(activity, view, source);
    }

    /* access modifiers changed from: package-private */
    public boolean isPenDownPile(Pile pile) {
        return pile.getRemoveVariantCount() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isPenDownPackOpen() {
        return false;
    }

    /* access modifiers changed from: package-private */
    public void PenDownAction(int x, int y, Vector<Pile> pile_list) {
        playTakeSound();
        this.mDrag = new DragData(pile_list, x, y);
        try {
            setDragPos(x, y);
        } catch (OutOfMemoryError ex) {
            this.mDrag = null;
            Toast.makeText(this.mActivity, ex.toString(), 1).show();
        }
    }

    private void setDragPos(int x, int y) {
        if (this.mDrag.piles.size() != 0) {
            addRedrawRect(this.mDrag.rect);
            this.mDrag.setPos(x, y);
            addRedrawRect(this.mDrag.rect);
            Pile sourcePile = null;
            Pile targetPile = null;
            int number = 0;
            int overlapping = 0;
            Pile[] pileArr = this.mPiles;
            int length = pileArr.length;
            for (int i = 0; i < length; i++) {
                Pile dst_pile = pileArr[i];
                int ov = dst_pile.getOverlapping(this.mDrag.card);
                if (ov > 0) {
                    Iterator<Pile> it = this.mDrag.piles.iterator();
                    while (it.hasNext()) {
                        Pile src_pile = it.next();
                        if (dst_pile != src_pile) {
                            int n = dst_pile.getAddVariant(src_pile);
                            int remove_type = src_pile.mOwner.mRemoveType;
                            if (n < 0 && (remove_type == 1 || remove_type == 3)) {
                                int i2 = 2;
                                while (true) {
                                    if (i2 > src_pile.size()) {
                                        break;
                                    } else if (dst_pile.isEnableComplexAdd(src_pile, i2)) {
                                        n = src_pile.size() - i2;
                                        break;
                                    } else {
                                        i2++;
                                    }
                                }
                            }
                            if (n >= 0 && ov > overlapping) {
                                sourcePile = src_pile;
                                targetPile = dst_pile;
                                number = n;
                                overlapping = ov;
                            }
                        }
                    }
                }
            }
            if (targetPile == null || sourcePile == null) {
                if (this.mDrag.sourcePile != null) {
                    this.mDrag.sourcePile.unmarkAll();
                    addRedrawRect(this.mDrag.sourcePile.mCardBounds);
                    this.mDrag.sourcePile = null;
                    DragData dragData = this.mDrag;
                    this.mDrag.count = 0;
                    dragData.number = 0;
                }
                if (this.mDrag.targetPile != null) {
                    addRedrawRect(this.mDrag.targetPile.mCardBounds);
                    this.mDrag.targetPile = null;
                    DragData dragData2 = this.mDrag;
                    this.mDrag.count = 0;
                    dragData2.number = 0;
                }
                if (!(this.mDrag.card.mValue == -1 && this.mDrag.card.mSuit == -1)) {
                    this.mDrag.card = new Card(-1, -1, this.mCardData);
                    this.mDrag.card.mNextOffset = 5;
                    this.mDrag.card.mOpen = true;
                    this.mDrag.setPos(this.mDrag.x, this.mDrag.y);
                }
            } else if (!(this.mDrag.sourcePile == sourcePile && this.mDrag.targetPile == targetPile)) {
                boolean f_redraw_source = true;
                int count = sourcePile.mOwner.mRemoveType == 2 ? 1 : sourcePile.size() - number;
                if (this.mDrag.sourcePile != null) {
                    if (this.mDrag.sourcePile != sourcePile) {
                        this.mDrag.sourcePile.unmarkAll();
                        addRedrawRect(this.mDrag.sourcePile.mCardBounds);
                    } else if (this.mDrag.number == number && this.mDrag.count == count) {
                        f_redraw_source = false;
                    } else {
                        this.mDrag.sourcePile.unmarkAll();
                    }
                }
                if (f_redraw_source) {
                    this.mDrag.sourcePile = sourcePile;
                    this.mDrag.number = number;
                    this.mDrag.count = count;
                    Card card = sourcePile.getCard(number);
                    if (!(card.mValue == this.mDrag.card.mValue && card.mSuit == this.mDrag.card.mSuit)) {
                        this.mDrag.card = new Card(card);
                        this.mDrag.card.mNextOffset = 5;
                        this.mDrag.card.mOpen = true;
                        this.mDrag.setPos(this.mDrag.x, this.mDrag.y);
                    }
                    for (int i3 = 0; i3 < count; i3++) {
                        if (card != null) {
                            card.mMark = true;
                            card = card.next;
                        }
                    }
                    addRedrawRect(this.mDrag.sourcePile.mCardBounds);
                }
                if (this.mDrag.targetPile != targetPile) {
                    if (this.mDrag.targetPile != null) {
                        addRedrawRect(this.mDrag.targetPile.mCardBounds);
                    }
                    this.mDrag.targetPile = targetPile;
                    addRedrawRect(this.mDrag.targetPile.mCardBounds);
                }
            }
            CorrectAndRedrawIfNeed();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean PenDrag(int x, int y) {
        if (this.mDrag == null) {
            return false;
        }
        setDragPos(x, y);
        return true;
    }

    /* Debug info: failed to restart local var, previous not found, register: 13 */
    /* access modifiers changed from: package-private */
    public boolean PenUp(int x, int y) {
        int d;
        if (this.mDrag == null) {
            return false;
        }
        DragData drag = this.mDrag;
        this.mDrag = null;
        this.mDraw = null;
        addRedrawRect(drag.rect);
        if (drag.sourcePile != null) {
            addRedrawRect(drag.sourcePile.mCardBounds);
        }
        if (drag.targetPile != null) {
            addRedrawRect(drag.targetPile.mCardBounds);
        }
        if (System.currentTimeMillis() < this.mPenDownTime + 700 && Math.abs(drag.xStart - x) <= this.mTouchArrea / 2 && Math.abs(drag.yStart - y) <= this.mTouchArrea / 2) {
            if (!this.mPack.isAvailable() || !this.mPack.isAround(x, y)) {
                Pile expand_pile = null;
                int distance = Integer.MAX_VALUE;
                Iterator<Pile> it = drag.piles.iterator();
                while (it.hasNext()) {
                    Pile pile = it.next();
                    if (pile.mExpandable) {
                        if (x < pile.mCardBounds.left) {
                            d = pile.mCardBounds.left - x;
                        } else if (x > pile.mCardBounds.right) {
                            d = x - pile.mCardBounds.right;
                        } else {
                            d = 0;
                        }
                        if (y < pile.mCardBounds.top) {
                            d = Math.max(d, pile.mCardBounds.top - y);
                        } else if (y > pile.mCardBounds.bottom) {
                            d = Math.max(d, y - pile.mCardBounds.bottom);
                        }
                        if (expand_pile == null || d < distance) {
                            expand_pile = pile;
                            distance = d;
                        }
                    }
                }
                if (expand_pile != null) {
                    CorrectAndRedrawIfNeed();
                    ShowPileDialog.show(this.mActivity, expand_pile);
                    return true;
                }
            } else {
                ResetSelection();
                new Game.PackOpenCardsAction(new GamePlay.ResumeMoveAction(true), this.mMoves).run();
                return true;
            }
        }
        if (!(drag.sourcePile == null || drag.targetPile == null)) {
            Iterator<Card> it2 = drag.sourcePile.iterator();
            while (it2.hasNext()) {
                it2.next().mMark = false;
            }
            Vector<MoveVariant> variants = drag.targetPile.getAllAddVariants(drag.sourcePile);
            if (variants != null) {
                if (variants.size() > 1) {
                    CorrectAndRedrawIfNeed();
                    MoveVariantChooser.show((PlayActivity) this.mActivity, drag.sourcePile, drag.targetPile, variants);
                } else {
                    VariantAction(drag.sourcePile, drag.targetPile, variants.get(0));
                }
                return true;
            }
        }
        if (!this.mPack.isAvailable() || !this.mPack.isAround(x, y) || !this.mPack.isAround(drag.xStart, drag.yStart)) {
            CorrectAndRedrawIfNeed();
            playDropSound();
            return true;
        }
        ResetSelection();
        new Game.PackOpenCardsAction(new GamePlay.ResumeMoveAction(true), this.mMoves).run();
        return true;
    }

    private class SequentiallyMovementAction implements GameAction {
        private final int mCount;
        private final Pile mFromPile;
        private int mNumber = 0;
        private final Pile mToPile;

        SequentiallyMovementAction(Pile fromPile, Pile toPile, int count) {
            this.mFromPile = fromPile;
            this.mToPile = toPile;
            this.mCount = count;
        }

        public void fastRun() {
            boolean z;
            if (this.mNumber == 0) {
                GamePlay_Type0.this.ResetSelection();
                GamePlay_Type0.this.mMoves.IncreaseMoveNumber();
            }
            while (this.mNumber < this.mCount && this.mFromPile.size() > 0) {
                GamePlay_Type0.this.mMoves.addOneCardMove(this.mFromPile, this.mFromPile.size() - 1, this.mToPile);
                this.mToPile.add(this.mFromPile.removeLast());
                this.mNumber++;
            }
            GamePlay_Type0.this.mNeedCorrect = true;
            GamePlay_Type0 gamePlay_Type0 = GamePlay_Type0.this;
            if (this.mFromPile.mOwner.mFoundation) {
                z = false;
            } else {
                z = true;
            }
            gamePlay_Type0.fastResumeMove(z);
        }

        public void run() {
            boolean z;
            boolean z2;
            boolean z3 = false;
            if (this.mFromPile.size() == 0) {
                GamePlay_Type0 gamePlay_Type0 = GamePlay_Type0.this;
                if (this.mFromPile.mOwner.mFoundation) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                new GamePlay.ResumeMoveAction(z2).run();
                return;
            }
            this.mNumber++;
            if (this.mNumber == this.mCount) {
                GamePlay_Type0 gamePlay_Type02 = GamePlay_Type0.this;
                Pile pile = this.mFromPile;
                int size = this.mFromPile.size() - 1;
                Pile pile2 = this.mToPile;
                GamePlay_Type0 gamePlay_Type03 = GamePlay_Type0.this;
                if (this.mFromPile.mOwner.mFoundation) {
                    z = false;
                } else {
                    z = true;
                }
                gamePlay_Type02.MoveCardAndSave(pile, size, pile2, false, new GamePlay.ResumeMoveAction(z));
                return;
            }
            GamePlay_Type0 gamePlay_Type04 = GamePlay_Type0.this;
            Pile pile3 = this.mFromPile;
            int size2 = this.mFromPile.size() - 1;
            Pile pile4 = this.mToPile;
            if (this.mNumber == 1) {
                z3 = true;
            }
            gamePlay_Type04.MoveCardAndSave(pile3, size2, pile4, z3, this);
        }
    }

    /* access modifiers changed from: package-private */
    public void VariantAction(Pile fromPile, Pile toPile, MoveVariant variant) {
        boolean z;
        switch ($SWITCH_TABLE$com$anoshenko$android$solitaires$MoveVariantType()[variant.Type.ordinal()]) {
            case 1:
                if (fromPile.mOwner.mFoundation) {
                    z = false;
                } else {
                    z = true;
                }
                GamePlay.ResumeMoveAction resume_action = new GamePlay.ResumeMoveAction(z);
                if (variant.Count == 1) {
                    MoveCardAndSave(fromPile, variant.Number, toPile, true, resume_action);
                    return;
                }
                MoveCardsAndSave(fromPile, toPile, variant.Count, true, resume_action);
                return;
            case 2:
                ComplexMovement(fromPile, toPile, variant.Count);
                return;
            case 3:
                new SequentiallyMovementAction(fromPile, toPile, variant.Count).run();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean ComplexMovement(Pile from_pile, Pile to_pile, int count) {
        Pile[] pile1 = new Pile[this.mPiles.length];
        Pile[] pile2 = new Pile[this.mPiles.length];
        int n_count1 = 0;
        int n_count2 = 0;
        Pile[] pileArr = this.mPiles;
        int length = pileArr.length;
        for (int i = 0; i < length; i++) {
            Pile pile = pileArr[i];
            if (!(pile == from_pile || pile == to_pile)) {
                switch (pile.getComplexMovePileType(from_pile, count)) {
                    case 1:
                        pile1[n_count1] = pile;
                        n_count1++;
                        continue;
                    case 2:
                        pile2[n_count2] = pile;
                        n_count2++;
                        continue;
                }
            }
        }
        int n_count = n_count1 + n_count2;
        if (n_count == 0) {
            return false;
        }
        Vector<Pile> scenario = new Vector<>();
        if (n_count >= count - 1) {
            for (int i2 = 0; i2 < n_count1; i2++) {
                pile2[n_count2 + i2] = pile1[i2];
            }
            for (int i3 = 0; i3 < count - 1; i3++) {
                scenario.add(from_pile);
                scenario.add(pile2[i3]);
            }
            scenario.add(from_pile);
            scenario.add(to_pile);
            for (int i4 = count - 2; i4 >= 0; i4--) {
                scenario.add(pile2[i4]);
                scenario.add(to_pile);
            }
            new ComplexMovementAction(scenario, !from_pile.mOwner.mFoundation).start();
            return true;
        }
        if (n_count2 == 0) {
            n_count1--;
            n_count2++;
            pile2[0] = pile1[n_count1];
        }
        boolean f_result = false;
        int n = 0;
        if (n_count >= 2 && n_count1 > 0) {
            n = 0 + n_count2;
            for (int i5 = 1; i5 <= n_count1; i5++) {
                n += (n_count2 + 1) * i5;
            }
        }
        if (n >= count - 1) {
            int[] size1 = new int[n_count1];
            int[] size2 = new int[n_count2];
            int q = n_count1;
            for (int n2 = 0; n2 < count - 1; n2++) {
                boolean flag = true;
                int i6 = 0;
                while (true) {
                    if (i6 < n_count2) {
                        if (size2[i6] == 0) {
                            scenario.add(from_pile);
                            scenario.add(pile2[i6]);
                            size2[i6] = 1;
                            flag = false;
                        } else {
                            i6++;
                        }
                    }
                }
                if (flag) {
                    int i7 = 0;
                    while (true) {
                        if (i7 < n_count1) {
                            if (size1[i7] == 0) {
                                scenario.add(from_pile);
                                scenario.add(pile1[i7]);
                                size1[i7] = 1;
                                if (n2 < count - 1) {
                                    for (int k = n_count2 - 1; k >= 0; k--) {
                                        scenario.add(pile2[k]);
                                        scenario.add(pile1[i7]);
                                        size2[k] = 0;
                                        size1[i7] = size1[i7] + 1;
                                    }
                                    if (i7 == q - 1 && i7 > 0) {
                                        q--;
                                        for (int k2 = q - 1; k2 >= 0; k2--) {
                                            for (int j = 0; j < n_count2; j++) {
                                                scenario.add(pile1[k2]);
                                                scenario.add(pile2[j]);
                                            }
                                            scenario.add(pile1[k2]);
                                            scenario.add(pile1[i7]);
                                            for (int j2 = n_count2 - 1; j2 >= 0; j2--) {
                                                scenario.add(pile2[j2]);
                                                scenario.add(pile1[i7]);
                                            }
                                            size1[i7] = size1[i7] + size1[k2];
                                            size1[k2] = 0;
                                        }
                                    }
                                }
                            } else {
                                i7++;
                            }
                        }
                    }
                }
            }
            scenario.add(from_pile);
            scenario.add(to_pile);
            for (int n3 = 0; n3 < count - 1; n3++) {
                boolean flag2 = true;
                int i8 = n_count2 - 1;
                while (true) {
                    if (i8 >= 0) {
                        if (size2[i8] != 0) {
                            scenario.add(pile2[i8]);
                            scenario.add(to_pile);
                            size2[i8] = 0;
                            flag2 = false;
                        } else {
                            i8--;
                        }
                    }
                }
                if (flag2) {
                    int i9 = q - 1;
                    while (true) {
                        if (i9 >= 0) {
                            if (size1[i9] > 0) {
                                for (int k3 = 0; k3 < size1[i9] - 1; k3++) {
                                    scenario.add(pile1[i9]);
                                    scenario.add(pile2[k3]);
                                    size2[k3] = 1;
                                }
                                scenario.add(pile1[i9]);
                                scenario.add(to_pile);
                                size1[i9] = 0;
                                flag2 = false;
                            } else {
                                i9--;
                            }
                        }
                    }
                    if (flag2) {
                        for (int k4 = 0; k4 < q; k4++) {
                            for (int j3 = 0; j3 < n_count2; j3++) {
                                scenario.add(pile1[q]);
                                scenario.add(pile2[j3]);
                            }
                            scenario.add(pile1[q]);
                            scenario.add(pile1[k4]);
                            for (int j4 = n_count2 - 1; j4 >= 0; j4--) {
                                scenario.add(pile2[j4]);
                                scenario.add(pile1[k4]);
                            }
                            size1[k4] = n_count2 + 1;
                        }
                        for (int j5 = 0; j5 < n_count2; j5++) {
                            scenario.add(pile1[q]);
                            scenario.add(pile2[j5]);
                            size2[j5] = 1;
                        }
                        scenario.add(pile1[q]);
                        scenario.add(to_pile);
                        size1[q] = 0;
                    }
                }
            }
            f_result = true;
            new ComplexMovementAction(scenario, !from_pile.mOwner.mFoundation).start();
        }
        return f_result;
    }

    private class ComplexMovementAction implements GameAction {
        private final boolean mCollect;
        private int mNumber = 0;
        private final Vector<Pile> mScenario;

        ComplexMovementAction(Vector<Pile> scenario, boolean collect) {
            this.mScenario = scenario;
            this.mCollect = collect;
        }

        /* access modifiers changed from: package-private */
        public void start() {
            GamePlay_Type0.this.mMoves.IncreaseMoveNumber();
            run();
        }

        public void fastRun() {
            int count = this.mScenario.size();
            while (this.mNumber < count) {
                Pile from_pile = this.mScenario.get(this.mNumber);
                this.mNumber++;
                Pile to_pile = this.mScenario.get(this.mNumber);
                this.mNumber++;
                GamePlay_Type0.this.mMoves.addOneCardMove(from_pile, from_pile.size() - 1, to_pile);
                to_pile.add(from_pile.removeLast());
            }
            GamePlay_Type0.this.mNeedCorrect = true;
            GamePlay_Type0.this.fastResumeMove(this.mCollect);
        }

        public void run() {
            if (this.mNumber < this.mScenario.size()) {
                Pile from_pile = this.mScenario.get(this.mNumber);
                this.mNumber++;
                Pile to_pile = this.mScenario.get(this.mNumber);
                this.mNumber++;
                GamePlay_Type0.this.mMoves.addOneCardMove(from_pile, from_pile.size() - 1, to_pile);
                GamePlay_Type0.this.MoveCard(from_pile, from_pile.size() - 1, to_pile, to_pile.size(), true, this.mNumber < this.mScenario.size() ? this : new GamePlay.ResumeMoveAction(this.mCollect));
            }
        }
    }

    private class CollectAllAction implements GameAction {
        private boolean mNewMove;

        private CollectAllAction() {
            this.mNewMove = true;
        }

        /* synthetic */ CollectAllAction(GamePlay_Type0 gamePlay_Type0, CollectAllAction collectAllAction) {
            this();
        }

        private boolean fastFindFoundationAndAdd(Pile from_pile, Card first_card, int number, int count) {
            for (PileGroup group : GamePlay_Type0.this.mGroup) {
                if (group.mFoundation && group.mAddType > 0) {
                    for (Pile pile : group.mPile) {
                        if (pile.isEnableAdd(from_pile, count, first_card)) {
                            GamePlay_Type0.this.ResetSelection();
                            if (this.mNewMove) {
                                this.mNewMove = false;
                                GamePlay_Type0.this.mMoves.IncreaseMoveNumber();
                            }
                            if (count == 1) {
                                GamePlay_Type0.this.mMoves.addOneCardMove(from_pile, number, pile);
                                pile.add(from_pile.remove(number));
                            } else {
                                GamePlay_Type0.this.mMoves.addSeriesCardMove(from_pile, pile, count);
                                pile.add(from_pile.removeLast(count));
                            }
                            GamePlay_Type0.this.mNeedCorrect = true;
                            return true;
                        }
                    }
                    continue;
                }
            }
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:68:0x0051, code lost:
            continue;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void fastRun() {
            /*
                r15 = this;
                r14 = 1
                r13 = 0
            L_0x0002:
                r1 = 0
                com.anoshenko.android.solitaires.GamePlay_Type0 r5 = com.anoshenko.android.solitaires.GamePlay_Type0.this
                com.anoshenko.android.solitaires.PileGroup[] r5 = r5.mGroup
                int r6 = r5.length
                r7 = r13
            L_0x0009:
                if (r7 < r6) goto L_0x000e
            L_0x000b:
                if (r1 != 0) goto L_0x0002
            L_0x000d:
                return
            L_0x000e:
                r2 = r5[r7]
                boolean r8 = r2.mFoundation
                if (r8 != 0) goto L_0x001e
                int r8 = r2.mRemoveType
                if (r8 <= 0) goto L_0x001e
                com.anoshenko.android.solitaires.Pile[] r8 = r2.mPile
                int r9 = r8.length
                r10 = r13
            L_0x001c:
                if (r10 < r9) goto L_0x0021
            L_0x001e:
                int r7 = r7 + 1
                goto L_0x0009
            L_0x0021:
                r4 = r8[r10]
                int r11 = r4.size()
                if (r11 <= 0) goto L_0x0051
                com.anoshenko.android.solitaires.Card r0 = r4.lastElement()
                int r11 = r4.size()
                int r11 = r11 - r14
                boolean r11 = r4.isEnableRemove(r11, r14)
                if (r11 == 0) goto L_0x004c
                int r11 = r4.size()
                int r11 = r11 - r14
                boolean r1 = r15.fastFindFoundationAndAdd(r4, r0, r11, r14)
                if (r1 == 0) goto L_0x004c
                com.anoshenko.android.solitaires.GamePlay_Type0 r5 = com.anoshenko.android.solitaires.GamePlay_Type0.this
                boolean r5 = r5.fastResumeMove(r13)
                if (r5 == 0) goto L_0x000b
                goto L_0x000d
            L_0x004c:
                int r11 = r2.mRemoveType
                switch(r11) {
                    case 2: goto L_0x0054;
                    case 3: goto L_0x007f;
                    case 4: goto L_0x00ab;
                    default: goto L_0x0051;
                }
            L_0x0051:
                int r10 = r10 + 1
                goto L_0x001c
            L_0x0054:
                r3 = 2
            L_0x0055:
                int r11 = r4.size()
                if (r3 > r11) goto L_0x0051
                com.anoshenko.android.solitaires.Card r0 = r0.prev
                int r11 = r4.size()
                int r11 = r11 - r3
                boolean r11 = r4.isEnableRemove(r11, r14)
                if (r11 == 0) goto L_0x007c
                int r11 = r4.size()
                int r11 = r11 - r3
                boolean r1 = r15.fastFindFoundationAndAdd(r4, r0, r11, r14)
                if (r1 == 0) goto L_0x007c
                com.anoshenko.android.solitaires.GamePlay_Type0 r5 = com.anoshenko.android.solitaires.GamePlay_Type0.this
                boolean r5 = r5.fastResumeMove(r13)
                if (r5 == 0) goto L_0x000b
                goto L_0x000d
            L_0x007c:
                int r3 = r3 + 1
                goto L_0x0055
            L_0x007f:
                r3 = 2
            L_0x0080:
                int r11 = r4.size()
                if (r3 > r11) goto L_0x0051
                com.anoshenko.android.solitaires.Card r0 = r0.prev
                int r11 = r4.size()
                int r11 = r11 - r3
                boolean r11 = r4.isEnableRemove(r11, r3)
                if (r11 == 0) goto L_0x00a8
                int r11 = r4.size()
                int r11 = r11 - r3
                boolean r1 = r15.fastFindFoundationAndAdd(r4, r0, r11, r3)
                if (r1 == 0) goto L_0x00a8
                com.anoshenko.android.solitaires.GamePlay_Type0 r5 = com.anoshenko.android.solitaires.GamePlay_Type0.this
                boolean r5 = r5.fastResumeMove(r13)
                if (r5 == 0) goto L_0x000b
                goto L_0x000d
            L_0x00a8:
                int r3 = r3 + 1
                goto L_0x0080
            L_0x00ab:
                int r11 = r4.size()
                int r12 = r2.mRemoveSeriesSize
                if (r11 < r12) goto L_0x0051
                int r11 = r4.size()
                int r12 = r2.mRemoveSeriesSize
                int r11 = r11 - r12
                int r12 = r2.mRemoveSeriesSize
                boolean r11 = r4.isEnableRemove(r11, r12)
                if (r11 == 0) goto L_0x0051
                int r11 = r4.size()
                int r12 = r2.mRemoveSeriesSize
                int r11 = r11 - r12
                int r12 = r2.mRemoveSeriesSize
                boolean r1 = r15.fastFindFoundationAndAdd(r4, r0, r11, r12)
                if (r1 == 0) goto L_0x0051
                com.anoshenko.android.solitaires.GamePlay_Type0 r5 = com.anoshenko.android.solitaires.GamePlay_Type0.this
                boolean r5 = r5.fastResumeMove(r13)
                if (r5 == 0) goto L_0x000b
                goto L_0x000d
            */
            throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.GamePlay_Type0.CollectAllAction.fastRun():void");
        }

        private boolean findFoundationAndAdd(Pile from_pile, Card first_card, int number, int count) {
            GamePlay.ResumeMoveAction resume_action = new GamePlay.ResumeMoveAction(this, false);
            for (PileGroup group : GamePlay_Type0.this.mGroup) {
                if (group.mFoundation && group.mAddType > 0) {
                    for (Pile pile : group.mPile) {
                        if (pile.isEnableAdd(from_pile, count, first_card)) {
                            GamePlay_Type0.this.ResetSelection();
                            if (this.mNewMove) {
                                this.mNewMove = false;
                                GamePlay_Type0.this.mMoves.IncreaseMoveNumber();
                            }
                            if (count == 1) {
                                GamePlay_Type0.this.mMoves.addOneCardMove(from_pile, number, pile);
                                GamePlay_Type0.this.MoveCard(from_pile, number, pile, pile.size(), true, resume_action);
                            } else {
                                GamePlay_Type0.this.mMoves.addSeriesCardMove(from_pile, pile, count);
                                GamePlay_Type0.this.MoveCards(from_pile, pile, count, resume_action);
                            }
                            return true;
                        }
                    }
                    continue;
                }
            }
            return false;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0045, code lost:
            continue;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r15 = this;
                r14 = 0
                r13 = 1
                com.anoshenko.android.solitaires.GamePlay_Type0 r5 = com.anoshenko.android.solitaires.GamePlay_Type0.this
                com.anoshenko.android.solitaires.PileGroup[] r5 = r5.mGroup
                int r6 = r5.length
                r7 = r14
            L_0x0008:
                if (r7 < r6) goto L_0x000b
            L_0x000a:
                return
            L_0x000b:
                r1 = r5[r7]
                boolean r8 = r1.mFoundation
                if (r8 != 0) goto L_0x001b
                int r8 = r1.mRemoveType
                if (r8 <= 0) goto L_0x001b
                com.anoshenko.android.solitaires.Pile[] r8 = r1.mPile
                int r9 = r8.length
                r10 = r14
            L_0x0019:
                if (r10 < r9) goto L_0x001e
            L_0x001b:
                int r7 = r7 + 1
                goto L_0x0008
            L_0x001e:
                r4 = r8[r10]
                int r11 = r4.size()
                if (r11 <= 0) goto L_0x0045
                com.anoshenko.android.solitaires.Card r0 = r4.lastElement()
                int r11 = r4.size()
                int r11 = r11 - r13
                boolean r11 = r4.isEnableRemove(r11, r13)
                if (r11 == 0) goto L_0x0040
                int r11 = r4.size()
                int r11 = r11 - r13
                boolean r11 = r15.findFoundationAndAdd(r4, r0, r11, r13)
                if (r11 != 0) goto L_0x000a
            L_0x0040:
                int r11 = r1.mRemoveType
                switch(r11) {
                    case 2: goto L_0x0048;
                    case 3: goto L_0x006a;
                    case 4: goto L_0x008c;
                    default: goto L_0x0045;
                }
            L_0x0045:
                int r10 = r10 + 1
                goto L_0x0019
            L_0x0048:
                r2 = 2
            L_0x0049:
                int r11 = r4.size()
                if (r2 > r11) goto L_0x0045
                com.anoshenko.android.solitaires.Card r0 = r0.prev
                int r11 = r4.size()
                int r11 = r11 - r2
                boolean r11 = r4.isEnableRemove(r11, r13)
                if (r11 == 0) goto L_0x0067
                int r11 = r4.size()
                int r11 = r11 - r2
                boolean r11 = r15.findFoundationAndAdd(r4, r0, r11, r13)
                if (r11 != 0) goto L_0x000a
            L_0x0067:
                int r2 = r2 + 1
                goto L_0x0049
            L_0x006a:
                r2 = 2
            L_0x006b:
                int r11 = r4.size()
                if (r2 > r11) goto L_0x0045
                com.anoshenko.android.solitaires.Card r0 = r0.prev
                int r11 = r4.size()
                int r11 = r11 - r2
                boolean r11 = r4.isEnableRemove(r11, r2)
                if (r11 == 0) goto L_0x0089
                int r11 = r4.size()
                int r11 = r11 - r2
                boolean r11 = r15.findFoundationAndAdd(r4, r0, r11, r2)
                if (r11 != 0) goto L_0x000a
            L_0x0089:
                int r2 = r2 + 1
                goto L_0x006b
            L_0x008c:
                int r11 = r4.size()
                int r12 = r1.mRemoveSeriesSize
                int r3 = r11 - r12
                int r11 = r4.size()
                int r12 = r1.mRemoveSeriesSize
                if (r11 < r12) goto L_0x0045
                int r11 = r1.mRemoveSeriesSize
                boolean r11 = r4.isEnableRemove(r3, r11)
                if (r11 == 0) goto L_0x0045
                int r11 = r1.mRemoveSeriesSize
                boolean r11 = r15.findFoundationAndAdd(r4, r0, r3, r11)
                if (r11 == 0) goto L_0x0045
                goto L_0x000a
            */
            throw new UnsupportedOperationException("Method not decompiled: com.anoshenko.android.solitaires.GamePlay_Type0.CollectAllAction.run():void");
        }
    }

    /* access modifiers changed from: package-private */
    public void CollectAll() {
        new CollectAllAction(this, null).run();
    }

    /* access modifiers changed from: package-private */
    public boolean isAvailablePileMove() {
        return findNextAvailable(new CurrentAvailableMoves(), false);
    }

    /* access modifiers changed from: package-private */
    public boolean AvailableMoves() {
        CurrentAvailableMoves move = new CurrentAvailableMoves();
        if (findNextAvailable(move, true)) {
            this.mDraw = move;
            return super.AvailableMoves();
        }
        NoAvailableMovesMessage();
        return false;
    }

    private boolean isValidMove(Pile from_pile, int count, Card first_card, Pile to_pile) {
        if (to_pile == from_pile) {
            return false;
        }
        if (from_pile.mOwner == to_pile.mOwner && (getGameId() & -256) != 768 && from_pile.size() == count && to_pile.size() == 0) {
            return false;
        }
        return to_pile.isEnableAdd(from_pile, count, first_card);
    }

    private boolean findNextAvailable(CurrentAvailableMoves move, boolean f_set) {
        if (move.pileNumber >= this.mPiles.length) {
            return false;
        }
        Pile pile = this.mPiles[move.pileNumber];
        boolean f_exist = false;
        if (f_set) {
            if (move.cardNumber < pile.size()) {
                Iterator<Card> it = pile.iterator();
                while (it.hasNext()) {
                    it.next().mMark = false;
                }
            }
            move.toPiles.clear();
        }
        while (move.pileNumber < this.mPiles.length) {
            move.cardNumber--;
            while (move.cardNumber < 0) {
                move.pileNumber++;
                if (move.pileNumber >= this.mPiles.length) {
                    return false;
                }
                pile = this.mPiles[move.pileNumber];
                move.cardNumber = pile.size() - 1;
            }
            Card card = pile.getCard(move.cardNumber);
            if (move.cardNumber == pile.size() - 1 || pile.mOwner.mRemoveType == 2) {
                if (pile.isEnableRemove(move.cardNumber, 1)) {
                    for (Pile to_pile : this.mPiles) {
                        if (isValidMove(pile, 1, card, to_pile)) {
                            if (!f_set) {
                                return true;
                            }
                            move.toPiles.add(to_pile);
                            move.cardCount = 1;
                            f_exist = true;
                        }
                    }
                    continue;
                } else {
                    continue;
                }
            } else if (pile.mOwner.mRemoveType == 3) {
                int count = pile.size() - move.cardNumber;
                if (pile.isEnableRemove(move.cardNumber, count)) {
                    for (Pile to_pile2 : this.mPiles) {
                        if (isValidMove(pile, count, card, to_pile2)) {
                            if (!f_set) {
                                return true;
                            }
                            move.toPiles.add(to_pile2);
                            move.cardCount = count;
                            f_exist = true;
                        }
                    }
                    continue;
                } else {
                    move.cardNumber = 0;
                    continue;
                }
            } else if (pile.mOwner.mRemoveType == 4) {
                int count2 = pile.mOwner.mRemoveSeriesSize;
                move.cardNumber = pile.size() - count2;
                if (move.cardNumber < 0 || !pile.isEnableRemove(move.cardNumber, count2)) {
                    move.cardNumber = 0;
                    continue;
                } else {
                    card = pile.getCard(move.cardNumber);
                    for (Pile to_pile3 : this.mPiles) {
                        if (isValidMove(pile, count2, card, to_pile3)) {
                            if (!f_set) {
                                return true;
                            }
                            move.toPiles.add(to_pile3);
                            move.cardCount = count2;
                            f_exist = true;
                        }
                    }
                    if (!f_exist) {
                        move.cardNumber = 0;
                        continue;
                    } else {
                        continue;
                    }
                }
            } else {
                continue;
            }
            if (f_exist) {
                for (int i = 0; i < move.cardCount; i++) {
                    card.mMark = true;
                    card = card.next;
                }
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void AvailableMovesFinish() {
        CurrentAvailableMoves current = (CurrentAvailableMoves) this.mDraw;
        if (current != null) {
            Pile pile = this.mPiles[current.pileNumber];
            if (current.cardNumber < pile.size()) {
                Iterator<Card> it = pile.iterator();
                while (it.hasNext()) {
                    it.next().mMark = false;
                }
            }
        }
        this.mDraw = null;
        super.AvailableMovesFinish();
    }

    /* access modifiers changed from: package-private */
    public boolean isNextAvailableMovesExist() {
        CurrentAvailableMoves current = (CurrentAvailableMoves) this.mDraw;
        CurrentAvailableMoves move = new CurrentAvailableMoves();
        move.pileNumber = current.pileNumber;
        move.cardNumber = current.cardNumber;
        move.cardCount = current.cardCount;
        move.toPiles = current.toPiles;
        return findNextAvailable(move, false);
    }

    /* access modifiers changed from: package-private */
    public void NextAvailableMoves() {
        CurrentAvailableMoves current = (CurrentAvailableMoves) this.mDraw;
        Pile pile = this.mPiles[current.pileNumber];
        if (current.cardNumber < pile.size()) {
            Iterator<Card> it = pile.iterator();
            while (it.hasNext()) {
                it.next().mMark = false;
            }
        }
        findNextAvailable(current, true);
        this.mView.invalidate();
        this.mRedrawRect = null;
    }
}
