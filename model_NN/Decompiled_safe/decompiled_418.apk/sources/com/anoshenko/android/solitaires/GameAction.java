package com.anoshenko.android.solitaires;

public interface GameAction extends Runnable {
    void fastRun();
}
