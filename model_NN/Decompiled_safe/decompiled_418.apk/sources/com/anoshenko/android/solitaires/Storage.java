package com.anoshenko.android.solitaires;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.io.InputStream;
import java.io.OutputStream;

public final class Storage {
    public static final String BEST_SERIES = "best_series";
    public static final String BEST_TIME = "best_time";
    public static final String CURRENT_SERIES = "series";
    private static final String DATABASE_NAME = "games.db";
    private static final int DATABASE_VERSION = 2;
    private static final String GAME_ID = "game_id";
    private static final String IS_STORED = "is_stored";
    public static final String LOSSES = "losses";
    private static final String STORED = "stored";
    private static final String TABLE_NAME_V1 = "games";
    private static final String TABLE_NAME_V2 = "games_v2";
    public static final String WINS = "wins";
    private static final String _ID = "id";

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, Storage.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 2);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE games_v2 (id INTEGER PRIMARY KEY, game_id INTEGER, wins INTEGER, losses INTEGER, best_time INTEGER, series INTEGER, best_series INTEGER, is_stored INTEGER, stored TEXT);");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
            try {
                Cursor cursor = db.query(Storage.TABLE_NAME_V1, null, null, null, null, null, null);
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        String[] int_column = {Storage.GAME_ID, Storage.WINS, Storage.LOSSES, Storage.BEST_TIME, Storage.IS_STORED};
                        int[] int_index = new int[int_column.length];
                        int[] int_value = new int[int_column.length];
                        for (int i = 0; i < int_column.length; i++) {
                            int_index[i] = cursor.getColumnIndexOrThrow(int_column[i]);
                        }
                        int stored_index = cursor.getColumnIndexOrThrow(Storage.STORED);
                        ContentValues values = new ContentValues();
                        do {
                            values.clear();
                            for (int i2 = 0; i2 < int_column.length; i2++) {
                                int_value[i2] = cursor.getInt(int_index[i2]);
                                values.put(int_column[i2], Integer.valueOf(int_value[i2]));
                            }
                            if (int_value[2] == 0) {
                                values.put(Storage.CURRENT_SERIES, Integer.valueOf(int_value[1]));
                                values.put(Storage.BEST_SERIES, Integer.valueOf(int_value[1]));
                            } else {
                                values.put(Storage.CURRENT_SERIES, (Integer) null);
                                values.put(Storage.BEST_SERIES, (Integer) null);
                            }
                            values.put(Storage.STORED, int_value[4] == 0 ? "" : cursor.getString(stored_index));
                            db.insert(Storage.TABLE_NAME_V2, null, values);
                        } while (cursor.moveToNext());
                    }
                    cursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            db.execSQL("DROP TABLE IF EXISTS games");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static final void store(Context context, int game_id, Statistics statistics, String game_state) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(GAME_ID, Integer.valueOf(game_id));
            statistics.getContentValues(values);
            if (game_state != null) {
                values.put(IS_STORED, (Integer) 1);
                values.put(STORED, game_state);
            } else {
                values.put(IS_STORED, (Integer) 0);
                values.put(STORED, "");
            }
            String rule = "game_id=" + game_id;
            Cursor cursor = database.query(TABLE_NAME_V2, null, rule, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    cursor.close();
                    database.update(TABLE_NAME_V2, values, rule, null);
                    return;
                }
                cursor.close();
            }
            database.insert(TABLE_NAME_V2, null, values);
            dbHelper.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.close();
        }
    }

    public static final String load(Context context, int game_id, GamePlay game) {
        String result = null;
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        try {
            Cursor cursor = dbHelper.getReadableDatabase().query(TABLE_NAME_V2, null, "game_id=" + game_id, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    try {
                        game.mStatistics.load(cursor);
                        if (cursor.getInt(cursor.getColumnIndexOrThrow(IS_STORED)) != 0) {
                            result = cursor.getString(cursor.getColumnIndexOrThrow(STORED));
                        }
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    }
                }
                cursor.close();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        } finally {
            dbHelper.close();
        }
        return result;
    }

    /* JADX INFO: Multiple debug info for r11v3 android.database.Cursor: [D('cursor' android.database.Cursor), D('context' android.content.Context)] */
    public static final void backupSave(Context context, OutputStream stream) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        try {
            Cursor cursor = dbHelper.getReadableDatabase().query(TABLE_NAME_V2, null, null, null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    String[] int_column = {GAME_ID, WINS, LOSSES, BEST_TIME, CURRENT_SERIES, BEST_SERIES};
                    int[] int_index = new int[int_column.length];
                    byte[] int_data = new byte[4];
                    for (int i = 0; i < int_column.length; i++) {
                        int_index[i] = cursor.getColumnIndexOrThrow(int_column[i]);
                    }
                    int is_stored_index = cursor.getColumnIndexOrThrow(IS_STORED);
                    int stored_index = cursor.getColumnIndexOrThrow(STORED);
                    do {
                        for (int i2 = 0; i2 < int_column.length; i2++) {
                            int value = cursor.getInt(int_index[i2]);
                            int_data[0] = (byte) ((value >> 24) & 255);
                            int_data[1] = (byte) ((value >> 16) & 255);
                            int_data[2] = (byte) ((value >> 8) & 255);
                            int_data[3] = (byte) (value & 255);
                            stream.write(int_data);
                        }
                        if (cursor.getInt(is_stored_index) == 0) {
                            int_data[3] = 0;
                            int_data[2] = 0;
                            int_data[1] = 0;
                            int_data[0] = 0;
                            stream.write(int_data);
                        } else {
                            byte[] data = cursor.getString(stored_index).getBytes();
                            int_data[0] = (byte) ((data.length >> 24) & 255);
                            int_data[1] = (byte) ((data.length >> 16) & 255);
                            int_data[2] = (byte) ((data.length >> 8) & 255);
                            int_data[3] = (byte) (data.length & 255);
                            stream.write(int_data);
                            stream.write(data);
                        }
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            dbHelper.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static final void backupRestore(Context context, InputStream stream) {
        DatabaseHelper dbHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            database.delete(TABLE_NAME_V2, null, null);
            String[] int_column = {GAME_ID, WINS, LOSSES, BEST_TIME, CURRENT_SERIES, BEST_SERIES};
            ContentValues values = new ContentValues();
            byte[] int_data = new byte[4];
            while (stream.available() > 0) {
                values.clear();
                for (String put : int_column) {
                    stream.read(int_data);
                    int value = 0;
                    for (int k = 0; k < 4; k++) {
                        value = (value << 8) | (int_data[k] & 255);
                    }
                    values.put(put, Integer.valueOf(value));
                }
                stream.read(int_data);
                int value2 = 0;
                for (int k2 = 0; k2 < 4; k2++) {
                    value2 = (value2 << 8) | (int_data[k2] & 255);
                }
                if (value2 == 0) {
                    values.put(IS_STORED, (Integer) 0);
                    values.put(STORED, "");
                } else {
                    byte[] data = new byte[value2];
                    stream.read(data);
                    values.put(IS_STORED, (Integer) 1);
                    values.put(STORED, new String(data));
                }
                database.insert(TABLE_NAME_V2, null, values);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.close();
        }
    }
}
