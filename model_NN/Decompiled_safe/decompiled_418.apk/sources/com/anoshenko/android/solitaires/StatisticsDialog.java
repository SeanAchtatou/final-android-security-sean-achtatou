package com.anoshenko.android.solitaires;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

class StatisticsDialog implements DialogInterface.OnDismissListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, Runnable {
    /* access modifiers changed from: private */
    public static StatisticsDialog Instance = null;
    /* access modifiers changed from: private */
    public final GamePlay mGame;
    private MediaPlayer mPlayer;
    private final View mView;

    private StatisticsDialog(GamePlay game, boolean f_win_message) {
        this.mGame = game;
        AlertDialog.Builder builder = new AlertDialog.Builder(game.mActivity);
        this.mView = LayoutInflater.from(game.mActivity).inflate((int) R.layout.statistics_view, (ViewGroup) null);
        ((TextView) this.mView.findViewById(R.id.StatisticsGameName)).setText(f_win_message ? R.string.win_message : R.string.statistics);
        setStatisticsText();
        builder.setView(this.mView);
        builder.setCancelable(true);
        builder.setTitle(game.getGameName());
        if (f_win_message) {
            builder.setPositiveButton((int) R.string.start_button, new StartClickListener(this, null));
            ExitClickListener exit_listener = new ExitClickListener(this, null);
            builder.setNegativeButton((int) R.string.exit_button, exit_listener);
            builder.setOnCancelListener(exit_listener);
            new Handler().postDelayed(this, 750);
        } else {
            builder.setPositiveButton((int) R.string.clear_button, new ClearClickListener(this, null));
            CloseClickListener close_listener = new CloseClickListener(this, null);
            builder.setNegativeButton((int) R.string.close_button, close_listener);
            builder.setOnCancelListener(close_listener);
        }
        builder.show().setOnDismissListener(this);
    }

    private void setStatisticsText() {
        Statistics statistics = this.mGame.mStatistics;
        ((TextView) this.mView.findViewById(R.id.StatisticsTotal)).setText(statistics.getTotal());
        ((TextView) this.mView.findViewById(R.id.StatisticsWins)).setText(statistics.getWins());
        ((TextView) this.mView.findViewById(R.id.StatisticsLosses)).setText(statistics.getLosses());
        ((TextView) this.mView.findViewById(R.id.StatisticsCurrentSeries)).setText(statistics.getCurrentSeries());
        ((TextView) this.mView.findViewById(R.id.StatisticsBestSeries)).setText(statistics.getBestSeries());
        ((TextView) this.mView.findViewById(R.id.StatisticsBestTime)).setText(statistics.getBestTime());
        int time = (int) (this.mGame.mCurrentTime / 1000);
        ((TextView) this.mView.findViewById(R.id.StatisticsCurrentTime)).setText(String.format("%d:%02d", Integer.valueOf(time / 60), Integer.valueOf(time % 60)));
    }

    public static void showStatistics(GamePlay game) {
        if (Instance == null) {
            Instance = new StatisticsDialog(game, false);
        }
    }

    public static void showWimMessage(GamePlay game) {
        if (Instance == null) {
            Instance = new StatisticsDialog(game, true);
        }
    }

    private class ClearClickListener implements DialogInterface.OnClickListener {
        private ClearClickListener() {
        }

        /* synthetic */ ClearClickListener(StatisticsDialog statisticsDialog, ClearClickListener clearClickListener) {
            this();
        }

        public void onClick(DialogInterface dialog, int arg1) {
            StatisticsDialog.Instance = null;
            AlertDialog.Builder builder = new AlertDialog.Builder(StatisticsDialog.this.mGame.mActivity);
            builder.setMessage((int) R.string.clear_question);
            ClearNoListener no_listener = new ClearNoListener(StatisticsDialog.this, null);
            builder.setCancelable(true);
            builder.setOnCancelListener(no_listener);
            builder.setNegativeButton((int) R.string.no, no_listener);
            builder.setPositiveButton((int) R.string.yes, new ClearYesListener(StatisticsDialog.this, null));
            builder.show();
        }
    }

    private class ClearNoListener implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
        private ClearNoListener() {
        }

        /* synthetic */ ClearNoListener(StatisticsDialog statisticsDialog, ClearNoListener clearNoListener) {
            this();
        }

        public void onClick(DialogInterface dialog, int which) {
            StatisticsDialog.showStatistics(StatisticsDialog.this.mGame);
        }

        public void onCancel(DialogInterface dialog) {
            StatisticsDialog.showStatistics(StatisticsDialog.this.mGame);
        }
    }

    private class ClearYesListener implements DialogInterface.OnClickListener {
        private ClearYesListener() {
        }

        /* synthetic */ ClearYesListener(StatisticsDialog statisticsDialog, ClearYesListener clearYesListener) {
            this();
        }

        public void onClick(DialogInterface dialog, int which) {
            StatisticsDialog.this.mGame.mStatistics.clear();
            StatisticsDialog.showStatistics(StatisticsDialog.this.mGame);
        }
    }

    private class CloseClickListener implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
        private CloseClickListener() {
        }

        /* synthetic */ CloseClickListener(StatisticsDialog statisticsDialog, CloseClickListener closeClickListener) {
            this();
        }

        public void onClick(DialogInterface dialog, int arg1) {
            back(dialog);
        }

        public void onCancel(DialogInterface dialog) {
            back(dialog);
        }

        private void back(DialogInterface dialog) {
            StatisticsDialog.Instance = null;
            dialog.dismiss();
            StatisticsDialog.this.mGame.ResumeTime();
        }
    }

    private class StartClickListener implements DialogInterface.OnClickListener {
        private StartClickListener() {
        }

        /* synthetic */ StartClickListener(StatisticsDialog statisticsDialog, StartClickListener startClickListener) {
            this();
        }

        public void onClick(DialogInterface dialog, int arg1) {
            StatisticsDialog.Instance = null;
            dialog.dismiss();
            StatisticsDialog.this.mGame.Start();
        }
    }

    private class ExitClickListener implements DialogInterface.OnClickListener, DialogInterface.OnCancelListener {
        private ExitClickListener() {
        }

        /* synthetic */ ExitClickListener(StatisticsDialog statisticsDialog, ExitClickListener exitClickListener) {
            this();
        }

        public void onClick(DialogInterface dialog, int arg1) {
            exit(dialog);
        }

        public void onCancel(DialogInterface dialog) {
            exit(dialog);
        }

        private void exit(DialogInterface dialog) {
            StatisticsDialog.Instance = null;
            dialog.dismiss();
            StatisticsDialog.this.mGame.mActivity.finish();
        }
    }

    public boolean onError(MediaPlayer player, int what, int extra) {
        stopPlayer();
        return false;
    }

    public void onCompletion(MediaPlayer player) {
        stopPlayer();
    }

    public void onPrepared(MediaPlayer player) {
        player.start();
    }

    public void onDismiss(DialogInterface dialog) {
        stopPlayer();
        Instance = null;
    }

    private void stopPlayer() {
        if (this.mPlayer != null) {
            try {
                this.mPlayer.stop();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            this.mPlayer.reset();
            this.mPlayer.release();
            this.mPlayer = null;
        }
    }

    public void run() {
        MediaPlayer player;
        String sound_file = new Settings(this.mGame.mActivity).getVictorySound();
        if (sound_file != null) {
            try {
                if (sound_file.length() == 0) {
                    player = MediaPlayer.create(this.mGame.mActivity, (int) R.raw.applause);
                    player.setOnPreparedListener(this);
                    player.setOnCompletionListener(this);
                    player.setOnErrorListener(this);
                } else {
                    player = new MediaPlayer();
                    player.setOnPreparedListener(this);
                    player.setOnCompletionListener(this);
                    player.setOnErrorListener(this);
                    player.setDataSource(sound_file);
                    player.prepareAsync();
                }
                this.mPlayer = player;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
