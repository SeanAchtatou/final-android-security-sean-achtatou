package com.google.android.apps.analytics;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import com.google.android.apps.analytics.Dispatcher;
import com.google.android.apps.analytics.PersistentEventStore;

public class GoogleAnalyticsTracker {
    static final boolean DEBUG = false;
    private static final GoogleAnalyticsTracker INSTANCE = new GoogleAnalyticsTracker();
    public static final String PRODUCT = "GoogleAnalytics";
    public static final String TRACKER_TAG = "googleanalytics";
    public static final String VERSION = "1.1";
    public static final String WIRE_VERSION = "4.5ma";
    private String accountId;
    private ConnectivityManager connetivityManager;
    private CustomVariableBuffer customVariableBuffer;
    private int dispatchPeriod;
    private Runnable dispatchRunner = new Runnable() {
        public void run() {
            GoogleAnalyticsTracker.this.dispatch();
        }
    };
    private Dispatcher dispatcher;
    private boolean dispatcherIsBusy;
    /* access modifiers changed from: private */
    public EventStore eventStore;
    /* access modifiers changed from: private */
    public Handler handler;
    private Context parent;
    private boolean powerSaveMode;
    private String userAgentProduct = PRODUCT;
    private String userAgentVersion = VERSION;

    final class DispatcherCallbacks implements Dispatcher.Callbacks {
        DispatcherCallbacks() {
        }

        public void dispatchFinished() {
            GoogleAnalyticsTracker.this.handler.post(new Runnable() {
                public void run() {
                    GoogleAnalyticsTracker.this.dispatchFinished();
                }
            });
        }

        public void eventDispatched(long j) {
            GoogleAnalyticsTracker.this.eventStore.deleteEvent(j);
        }
    }

    private GoogleAnalyticsTracker() {
    }

    private void cancelPendingDispatches() {
        this.handler.removeCallbacks(this.dispatchRunner);
    }

    private void createEvent(String str, String str2, String str3, String str4, int i) {
        Event event = new Event(this.eventStore.getStoreId(), str, str2, str3, str4, i, this.parent.getResources().getDisplayMetrics().widthPixels, this.parent.getResources().getDisplayMetrics().heightPixels);
        event.setCustomVariableBuffer(this.customVariableBuffer);
        this.customVariableBuffer = new CustomVariableBuffer();
        this.eventStore.putEvent(event);
        resetPowerSaveMode();
    }

    public static GoogleAnalyticsTracker getInstance() {
        return INSTANCE;
    }

    private void maybeScheduleNextDispatch() {
        if (this.dispatchPeriod >= 0 && this.handler.postDelayed(this.dispatchRunner, (long) (this.dispatchPeriod * 1000))) {
        }
    }

    private void resetPowerSaveMode() {
        if (this.powerSaveMode) {
            this.powerSaveMode = DEBUG;
            maybeScheduleNextDispatch();
        }
    }

    public boolean dispatch() {
        if (this.dispatcherIsBusy) {
            maybeScheduleNextDispatch();
            return DEBUG;
        }
        NetworkInfo activeNetworkInfo = this.connetivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            maybeScheduleNextDispatch();
            return DEBUG;
        } else if (this.eventStore.getNumStoredEvents() != 0) {
            this.dispatcher.dispatchEvents(this.eventStore.peekEvents());
            this.dispatcherIsBusy = true;
            maybeScheduleNextDispatch();
            return true;
        } else {
            this.powerSaveMode = true;
            return DEBUG;
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchFinished() {
        this.dispatcherIsBusy = DEBUG;
    }

    /* access modifiers changed from: package-private */
    public Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    /* access modifiers changed from: package-private */
    public EventStore getEventStore() {
        return this.eventStore;
    }

    public String getVisitorCustomVar(int i) {
        if (i >= 1 && i <= 5) {
            return this.eventStore.getVisitorCustomVar(i);
        }
        throw new IllegalArgumentException(CustomVariable.INDEX_ERROR_MSG);
    }

    public boolean setCustomVar(int i, String str, String str2) {
        return setCustomVar(i, str, str2, 3);
    }

    public boolean setCustomVar(int i, String str, String str2, int i2) {
        try {
            CustomVariable customVariable = new CustomVariable(i, str, str2, i2);
            if (this.customVariableBuffer == null) {
                this.customVariableBuffer = new CustomVariableBuffer();
            }
            this.customVariableBuffer.setCustomVariable(customVariable);
            return true;
        } catch (IllegalArgumentException e) {
            return DEBUG;
        }
    }

    public void setDispatchPeriod(int i) {
        int i2 = this.dispatchPeriod;
        this.dispatchPeriod = i;
        if (i2 <= 0) {
            maybeScheduleNextDispatch();
        } else if (i2 > 0) {
            cancelPendingDispatches();
            maybeScheduleNextDispatch();
        }
    }

    public void setProductVersion(String str, String str2) {
        this.userAgentProduct = str;
        this.userAgentVersion = str2;
    }

    public void start(String str, int i, Context context) {
        start(str, i, context, this.eventStore == null ? new PersistentEventStore(new PersistentEventStore.DataBaseHelper(context)) : this.eventStore, this.dispatcher == null ? new NetworkDispatcher(this.userAgentProduct, this.userAgentVersion) : this.dispatcher);
    }

    /* access modifiers changed from: package-private */
    public void start(String str, int i, Context context, EventStore eventStore2, Dispatcher dispatcher2) {
        start(str, i, context, eventStore2, dispatcher2, new DispatcherCallbacks());
    }

    /* access modifiers changed from: package-private */
    public void start(String str, int i, Context context, EventStore eventStore2, Dispatcher dispatcher2, Dispatcher.Callbacks callbacks) {
        this.accountId = str;
        this.parent = context;
        this.eventStore = eventStore2;
        this.eventStore.startNewVisit();
        this.dispatcher = dispatcher2;
        this.dispatcher.init(callbacks, this.eventStore.getReferrer());
        this.dispatcherIsBusy = DEBUG;
        if (this.connetivityManager == null) {
            this.connetivityManager = (ConnectivityManager) this.parent.getSystemService("connectivity");
        }
        if (this.handler == null) {
            this.handler = new Handler(context.getMainLooper());
        } else {
            cancelPendingDispatches();
        }
        setDispatchPeriod(i);
    }

    public void start(String str, Context context) {
        start(str, -1, context);
    }

    public void stop() {
        this.dispatcher.stop();
        cancelPendingDispatches();
    }

    public void trackEvent(String str, String str2, String str3, int i) {
        createEvent(this.accountId, str, str2, str3, i);
    }

    public void trackPageView(String str) {
        createEvent(this.accountId, "__##GOOGLEPAGEVIEW##__", str, null, -1);
    }
}
