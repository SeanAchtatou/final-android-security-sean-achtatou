package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.PublishedFor__1_1_0;
import com.scoreloop.client.android.core.PublishedFor__2_0_0;
import com.scoreloop.client.android.core.model.Message;
import com.scoreloop.client.android.core.model.MessageReceiver;
import com.scoreloop.client.android.core.model.MessageReceiverInterface;
import com.scoreloop.client.android.core.model.MessageTargetInterface;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.model.SocialProvider;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.core.server.Request;
import com.scoreloop.client.android.core.server.RequestCompletionCallback;
import com.scoreloop.client.android.core.server.RequestMethod;
import com.scoreloop.client.android.core.server.Response;
import com.scoreloop.client.android.ui.component.base.Constant;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class MessageController extends RequestController {
    @PublishedFor__1_0_0
    public static final Object INVITATION_TARGET = new b();
    @PublishedFor__1_1_0
    public static final Object RECEIVER_EMAIL = new a();
    @PublishedFor__2_0_0
    public static final Object RECEIVER_SYSTEM = new d();
    @PublishedFor__2_0_0
    public static final Object RECEIVER_USER = new e();
    @PublishedFor__2_0_0
    public static final int TYPE_ABUSE_REPORT = Message.Type.ABUSE_REPORT.ordinal();
    @PublishedFor__2_0_0
    public static final int TYPE_RECOMMENDATION = Message.Type.RECOMMENDATION.ordinal();
    @PublishedFor__2_0_0
    public static final int TYPE_TARGET_INFERRED = Message.Type.TARGET_INFERRED.ordinal();
    private final Message c;

    private static class a implements MessageReceiverInterface {
        private a() {
        }

        public String a() {
            return "email";
        }

        public boolean a(Session session) {
            return true;
        }

        public String[] a(Object... objArr) {
            if (objArr.length < 1) {
                return null;
            }
            String[] strArr = new String[objArr.length];
            for (int i = 0; i < objArr.length; i++) {
                Object obj = objArr[i];
                if (!(obj instanceof String)) {
                    return null;
                }
                strArr[i] = (String) obj;
            }
            return strArr;
        }
    }

    private static class b implements MessageTargetInterface {
        private b() {
        }

        public String a() {
            return "invitation";
        }

        public boolean b() {
            return true;
        }

        public String getIdentifier() {
            return null;
        }
    }

    private class c extends Request {
        private final Message b;

        public c(Message message, RequestCompletionCallback requestCompletionCallback) {
            super(requestCompletionCallback);
            this.b = message.a();
        }

        public String a() {
            String identifier = MessageController.this.getGame() != null ? MessageController.this.getGame().getIdentifier() : null;
            if (identifier != null) {
                return String.format("/service/games/%s/users/%s/message", identifier, MessageController.this.i().getIdentifier());
            }
            return String.format("/service/users/%s/message", MessageController.this.i().getIdentifier());
        }

        public JSONObject b() {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("message", this.b.f());
                return jSONObject;
            } catch (JSONException e) {
                throw new IllegalStateException(e);
            }
        }

        public RequestMethod c() {
            return RequestMethod.POST;
        }
    }

    private static class d implements MessageReceiverInterface {
        private final String[] a;

        private d() {
            this.a = new String[0];
        }

        public String a() {
            return "system";
        }

        public boolean a(Session session) {
            return true;
        }

        public String[] a(Object... objArr) {
            if (objArr.length > 0) {
                return null;
            }
            return this.a;
        }
    }

    private static class e implements MessageReceiverInterface {
        private e() {
        }

        public String a() {
            return Constant.USER;
        }

        public boolean a(Session session) {
            return true;
        }

        public String[] a(Object... objArr) {
            if (objArr.length == 0) {
                return null;
            }
            String[] strArr = new String[objArr.length];
            for (int i = 0; i < objArr.length; i++) {
                Object obj = objArr[i];
                if (!(obj instanceof User)) {
                    return null;
                }
                strArr[i] = ((User) obj).getIdentifier();
            }
            return strArr;
        }
    }

    @PublishedFor__1_0_0
    public MessageController(RequestControllerObserver requestControllerObserver) {
        this(null, requestControllerObserver);
    }

    @PublishedFor__1_0_0
    public MessageController(Session session, RequestControllerObserver requestControllerObserver) {
        super(session, requestControllerObserver);
        this.c = new Message();
    }

    /* access modifiers changed from: package-private */
    public boolean a(Request request, Response response) throws Exception {
        if (response.f() == 201) {
            return true;
        }
        RequestControllerException a2 = RequestControllerException.a(response);
        if (a2.getErrorCode() == 110) {
            for (String socialProviderForIdentifier : (List) a2.getUserInfo().get(RequestControllerException.INFO_KEY_DISCONNECTED_PROVIDER_IDENTIFIERS)) {
                SocialProvider.getSocialProviderForIdentifier(socialProviderForIdentifier).a(i());
            }
        }
        throw a2;
    }

    @PublishedFor__1_0_0
    public void addReceiverWithUsers(Object obj, Object... objArr) {
        if (obj == null || !(obj instanceof MessageReceiverInterface)) {
            throw new IllegalArgumentException("invalid receiver");
        }
        MessageReceiverInterface messageReceiverInterface = (MessageReceiverInterface) obj;
        String[] a2 = messageReceiverInterface.a(objArr);
        if (a2 == null) {
            throw new IllegalArgumentException("invalid users");
        }
        this.c.a(new MessageReceiver(messageReceiverInterface, a2));
    }

    @PublishedFor__2_0_0
    public int getMessageType() {
        return this.c.e().ordinal();
    }

    @PublishedFor__1_0_0
    public Object getTarget() {
        return this.c.c();
    }

    @PublishedFor__1_0_0
    public String getText() {
        return this.c.d();
    }

    @PublishedFor__1_0_0
    public boolean isSubmitAllowed() {
        if (getTarget() == null) {
            return false;
        }
        if (this.c.b().isEmpty()) {
            return false;
        }
        for (MessageReceiver a2 : this.c.b()) {
            if (!a2.a().a(h())) {
                return false;
            }
        }
        return true;
    }

    @PublishedFor__2_0_0
    public void setMessageType(int i) {
        this.c.a(Message.Type.values()[i]);
    }

    @PublishedFor__1_0_0
    public void setTarget(Object obj) {
        if (obj == null || !(obj instanceof MessageTargetInterface)) {
            throw new IllegalArgumentException("invalid target object");
        }
        MessageTargetInterface messageTargetInterface = (MessageTargetInterface) obj;
        if (!messageTargetInterface.b()) {
            throw new IllegalArgumentException("invalid target state");
        }
        this.c.a(messageTargetInterface);
    }

    @PublishedFor__1_0_0
    public void setText(String str) {
        this.c.a(str);
    }

    @PublishedFor__1_0_0
    public void submitMessage() {
        if (!isSubmitAllowed()) {
            throw new IllegalStateException("submitting is not allowed");
        }
        a_();
        b(new c(this.c, g()));
    }
}
