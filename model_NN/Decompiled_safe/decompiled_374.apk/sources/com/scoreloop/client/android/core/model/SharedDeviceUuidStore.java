package com.scoreloop.client.android.core.model;

import android.content.Context;
import com.scoreloop.client.android.core.util.FileStore;
import org.json.JSONException;
import org.json.JSONObject;

public class SharedDeviceUuidStore extends FileStore<SharedDeviceUuid> {
    public SharedDeviceUuidStore() {
    }

    public SharedDeviceUuidStore(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SharedDeviceUuid b(JSONObject jSONObject) throws JSONException {
        return new SharedDeviceUuid(jSONObject);
    }

    /* access modifiers changed from: protected */
    public String a() {
        return "edce0eda-3cc1-4144-b11f-c4bbe62da4a6";
    }

    /* access modifiers changed from: protected */
    public JSONObject a(SharedDeviceUuid sharedDeviceUuid) throws JSONException {
        return sharedDeviceUuid.b();
    }
}
