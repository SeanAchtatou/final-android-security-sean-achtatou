package com.scoreloop.client.android.ui.component.achievement;

import com.scoreloop.client.android.core.controller.AchievementController;
import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.model.Achievement;
import java.util.ArrayList;
import java.util.List;

public class AchievementsEngine implements RequestControllerObserver {
    private AchievementController _achievementController;
    private AchievementsController _achievementsController;
    private boolean _isLoading;
    private boolean _isSubmitting;
    private List<Runnable> _loadContinuations = new ArrayList();
    /* access modifiers changed from: private */
    public List<Runnable> _submitContinuations = new ArrayList();

    private AchievementController getAchievementController() {
        if (this._achievementController == null) {
            this._achievementController = new AchievementController(this);
        }
        return this._achievementController;
    }

    public AchievementsController getAchievementsController() {
        if (this._achievementsController == null) {
            this._achievementsController = new AchievementsController(this);
        }
        return this._achievementsController;
    }

    public boolean hadInitialSync() {
        return getAchievementsController().hadInitialSync();
    }

    public boolean hasLoadedAchievements() {
        return getAchievementsController().getAchievements().size() > 0;
    }

    private void invokeLoadContinuations() {
        List<Runnable> continuations = this._loadContinuations;
        this._loadContinuations = new ArrayList();
        for (Runnable continuation : continuations) {
            continuation.run();
        }
    }

    /* access modifiers changed from: private */
    public void invokeSubmitContinuations() {
        List<Runnable> continuations = this._submitContinuations;
        this._submitContinuations = new ArrayList();
        for (Runnable continuation : continuations) {
            continuation.run();
        }
    }

    public void loadAchievements(boolean forceInitialSync, Runnable continuation) {
        if (!hasLoadedAchievements() || (forceInitialSync && !hadInitialSync())) {
            if (continuation != null) {
                this._loadContinuations.add(continuation);
            }
            if (!this._isLoading) {
                this._isLoading = true;
                getAchievementsController().setForceInitialSync(forceInitialSync);
                getAchievementsController().loadAchievements();
            }
        } else if (continuation != null) {
            continuation.run();
        }
    }

    public void requestControllerDidFail(RequestController aRequestController, Exception anException) {
        if (aRequestController == this._achievementsController) {
            this._isLoading = false;
            invokeLoadContinuations();
        } else if (aRequestController == this._achievementController) {
            this._isSubmitting = false;
            invokeSubmitContinuations();
        }
    }

    public void requestControllerDidReceiveResponse(RequestController aRequestController) {
        if (aRequestController == this._achievementsController) {
            this._isLoading = false;
            invokeLoadContinuations();
        } else if (aRequestController == this._achievementController) {
            this._isSubmitting = false;
            submitNextAchievement();
        }
    }

    public void submitAchievements(final boolean forceInitialSync, final Runnable continuation) {
        if (!hasLoadedAchievements() || (!hadInitialSync() && forceInitialSync)) {
            loadAchievements(forceInitialSync, new Runnable() {
                public void run() {
                    if (AchievementsEngine.this.hasLoadedAchievements()) {
                        AchievementsEngine.this.submitAchievements(forceInitialSync, continuation);
                        return;
                    }
                    if (continuation != null) {
                        AchievementsEngine.this._submitContinuations.add(continuation);
                    }
                    AchievementsEngine.this.invokeSubmitContinuations();
                }
            });
            return;
        }
        if (continuation != null) {
            this._submitContinuations.add(continuation);
        }
        if (!this._isSubmitting) {
            submitNextAchievement();
        }
    }

    private void submitNextAchievement() {
        for (Achievement achievement : getAchievementsController().getAchievements()) {
            if (achievement.needsSubmit()) {
                getAchievementController().setAchievement(achievement);
                this._isSubmitting = true;
                this._achievementController.submitAchievement();
                return;
            }
        }
        invokeSubmitContinuations();
    }
}
