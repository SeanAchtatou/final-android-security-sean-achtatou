package com.scoreloop.client.android.ui.component.user;

import android.graphics.drawable.Drawable;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import eu.royalapps.boxingmachine.R;

public class UserAddBuddyListItem extends StandardListItem<Object> {
    public UserAddBuddyListItem(ComponentActivity context, Drawable drawable, String title, Object target) {
        super(context, drawable, title, null, target);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_user;
    }

    public int getType() {
        return 26;
    }
}
