package com.scoreloop.client.android.core.spi.twitter;

import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthRequest;
import com.scoreloop.client.android.core.spi.AuthRequestDelegate;
import com.scoreloop.client.android.core.spi.AuthViewController;

public class TwitterSocialProviderController extends SocialProviderController implements AuthRequestDelegate, AuthViewController.Observer {
    private b b;
    private c c;
    private b d;

    public TwitterSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b = new b(this);
        this.b.b();
    }

    public void a(AuthRequest authRequest) {
        if (authRequest == this.b) {
            this.c = new c(d(), this);
            this.c.a(this.b.c());
            this.c.a(c_());
        } else if (authRequest == this.d) {
            getSocialProvider().a(e(), this.d.c(), this.d.d(), this.d.e());
            a(SocialProviderController.UpdateMode.SUBMIT);
        } else {
            throw new IllegalStateException("unexpected request");
        }
    }

    public void a(AuthRequest authRequest, Throwable th) {
        d_().socialProviderControllerDidFail(this, th);
    }

    public void a(Throwable th) {
        d_().socialProviderControllerDidFail(this, th);
    }

    public void b() {
        d_().socialProviderControllerDidEnterInvalidCredentials(this);
    }

    public void b_() {
        d_().socialProviderControllerDidCancel(this);
    }

    public void c() {
        this.d = new b(this);
        this.d.a(this.b.c(), this.b.d());
    }
}
