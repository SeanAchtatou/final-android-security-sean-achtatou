package com.scoreloop.client.android.ui.component.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.scoreloop.client.android.ui.framework.BaseDialog;
import eu.royalapps.boxingmachine.R;

public class FirstTimeDialog extends BaseDialog {
    public static final int BUTTON_CANCEL = 1;
    public static final int BUTTON_OK = 0;
    private String _currentUsername;
    private EditText _email;
    private TextView _hint;
    private EditText _username;

    public FirstTimeDialog(Context context, String currentUsername) {
        super(context);
        setCancelable(true);
        this._currentUsername = currentUsername;
    }

    /* access modifiers changed from: protected */
    public int getContentViewLayoutId() {
        return R.layout.sl_dialog_profile_edit_initial;
    }

    public void onClick(View v) {
        if (this._listener != null) {
            switch (v.getId()) {
                case R.id.sl_button_ok /*2131427350*/:
                    this._listener.onAction(this, 0);
                    return;
                case R.id.sl_button_cancel /*2131427354*/:
                    this._listener.onAction(this, 1);
                    return;
                default:
                    return;
            }
        }
    }

    public String getUsernameText() {
        return this._username.getText().toString();
    }

    public String getEmailText() {
        return this._email.getText().toString();
    }

    public void setHint(String text) {
        this._hint.setText(text);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((Button) findViewById(R.id.sl_button_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.sl_button_cancel)).setOnClickListener(this);
        ((TextView) findViewById(R.id.sl_user_profile_edit_initial_current_text)).setText(this._currentUsername);
        this._username = (EditText) findViewById(R.id.sl_user_profile_edit_initial_username_text);
        this._email = (EditText) findViewById(R.id.sl_user_profile_edit_initial_email_text);
        this._hint = (TextView) findViewById(R.id.sl_dialog_hint);
    }
}
