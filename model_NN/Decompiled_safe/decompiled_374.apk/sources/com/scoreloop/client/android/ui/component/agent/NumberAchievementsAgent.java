package com.scoreloop.client.android.ui.component.agent;

import com.scoreloop.client.android.core.controller.AchievementsController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.model.User;
import com.scoreloop.client.android.ui.component.base.Constant;
import com.scoreloop.client.android.ui.framework.ValueStore;

public class NumberAchievementsAgent extends BaseAgent {
    public static final String[] SUPPORTED_KEYS = {Constant.NUMBER_ACHIEVEMENTS, Constant.NUMBER_AWARDS};
    private AchievementsController _achievementsController;

    public NumberAchievementsAgent() {
        super(SUPPORTED_KEYS);
    }

    /* access modifiers changed from: protected */
    public void onFinishRetrieve(RequestController aRequestController, ValueStore valueStore) {
        putValue(Constant.NUMBER_AWARDS, Integer.valueOf(this._achievementsController.getAwardList().getAwards().size()));
        putValue(Constant.NUMBER_ACHIEVEMENTS, Integer.valueOf(this._achievementsController.countAchievedAwards()));
    }

    /* access modifiers changed from: protected */
    public void onStartRetrieve(ValueStore valueStore) {
        if (this._achievementsController == null) {
            this._achievementsController = new AchievementsController(this);
        }
        this._achievementsController.setUser((User) valueStore.getValue(Constant.USER));
        this._achievementsController.loadAchievements();
    }
}
