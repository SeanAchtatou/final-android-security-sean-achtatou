package com.scoreloop.client.android.ui;

public interface OnCanStartGamePlayObserver {
    boolean onCanStartGamePlay();
}
