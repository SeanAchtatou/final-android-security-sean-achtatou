package com.scoreloop.client.android.core.controller;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;

public interface RequestControllerObserver {
    @PublishedFor__1_0_0
    void requestControllerDidFail(RequestController requestController, Exception exc);

    @PublishedFor__1_0_0
    void requestControllerDidReceiveResponse(RequestController requestController);
}
