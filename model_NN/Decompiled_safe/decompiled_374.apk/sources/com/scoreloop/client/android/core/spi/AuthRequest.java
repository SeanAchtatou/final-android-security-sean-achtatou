package com.scoreloop.client.android.core.spi;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

public abstract class AuthRequest {
    private AuthRequestDelegate a;

    protected AuthRequest(AuthRequestDelegate authRequestDelegate) {
        this.a = authRequestDelegate;
    }

    /* access modifiers changed from: protected */
    public AuthRequestDelegate a() {
        return this.a;
    }

    /* access modifiers changed from: protected */
    public abstract void a(HttpResponse httpResponse);

    /* access modifiers changed from: protected */
    public abstract void a(HttpResponse httpResponse, Throwable th);

    /* access modifiers changed from: protected */
    public void a(HttpUriRequest httpUriRequest) {
        HttpResponse httpResponse;
        IOException iOException;
        HttpResponse httpResponse2;
        Throwable th;
        try {
            HttpResponse execute = new DefaultHttpClient().execute(httpUriRequest);
            try {
                a(execute);
                return;
            } catch (ClientProtocolException e) {
                Throwable th2 = e;
                httpResponse2 = execute;
                th = th2;
            } catch (IOException e2) {
                IOException iOException2 = e2;
                httpResponse = execute;
                iOException = iOException2;
                a(httpResponse, iOException);
                return;
            }
        } catch (ClientProtocolException e3) {
            Throwable th3 = e3;
            httpResponse2 = null;
            th = th3;
        } catch (IOException e4) {
            IOException iOException3 = e4;
            httpResponse = null;
            iOException = iOException3;
            a(httpResponse, iOException);
            return;
        }
        a(httpResponse2, th);
    }
}
