package com.scoreloop.client.android.ui.framework;

import android.content.Intent;
import java.util.ArrayList;
import java.util.List;

public class ScreenDescription {
    private final List<ActivityDescription> _bodyDescriptions = new ArrayList();
    private ActivityDescription _headerDescription;
    private final ValueStore _screenValues = new ValueStore();
    private int _selectedBodyIndex = 0;
    private final List<ShortcutDescription> _shortcutDescriptions = new ArrayList();
    private ShortcutObserver _shortcutObserver;
    private int _shortcutSelectionId = 0;

    public interface ShortcutObserver {
        void onShortcut(int i);
    }

    public ActivityDescription addBodyDescription(int tabId, Intent intent) {
        ActivityDescription description = new ActivityDescription(tabId, intent);
        this._bodyDescriptions.add(description);
        return description;
    }

    public void addShortcutDescription(int textId, int imageId, int activeImageId) {
        this._shortcutDescriptions.add(new ShortcutDescription(textId, imageId, activeImageId));
    }

    public void addShortcutObserver(ShortcutObserver observer) {
        this._shortcutObserver = observer;
    }

    /* access modifiers changed from: package-private */
    public ActivityDescription getActivityDescription(String id) {
        if (this._headerDescription != null && this._headerDescription.hasIdentfier(id)) {
            return this._headerDescription;
        }
        for (ActivityDescription bodyDescription : this._bodyDescriptions) {
            if (bodyDescription.hasIdentfier(id)) {
                return bodyDescription;
            }
        }
        return null;
    }

    public List<ActivityDescription> getBodyDescriptions() {
        return this._bodyDescriptions;
    }

    public ActivityDescription getHeaderDescription() {
        return this._headerDescription;
    }

    public ValueStore getScreenValues() {
        return this._screenValues;
    }

    public int getSelectedBodyIndex() {
        return this._selectedBodyIndex;
    }

    public List<ShortcutDescription> getShortcutDescriptions() {
        return this._shortcutDescriptions;
    }

    public ShortcutObserver getShortcutObserver() {
        return this._shortcutObserver;
    }

    public int getShortcutSelectionId() {
        return this._shortcutSelectionId;
    }

    public int getShortcutSelectionIndex() {
        if (this._shortcutSelectionId != 0) {
            int count = this._shortcutDescriptions.size();
            for (int i = 0; i < count; i++) {
                if (this._shortcutSelectionId == this._shortcutDescriptions.get(i).getTextId()) {
                    return i;
                }
            }
        }
        return -1;
    }

    public ActivityDescription setBodyDescription(Intent intent) {
        return addBodyDescription(0, intent);
    }

    public ActivityDescription setHeaderDescription(Intent intent) {
        this._headerDescription = new ActivityDescription(0, intent);
        return this._headerDescription;
    }

    public void setSelectedBodyIndex(int index) {
        this._selectedBodyIndex = index;
    }

    public void setShortcutSelectionId(int selectionId) {
        this._shortcutSelectionId = selectionId;
    }
}
