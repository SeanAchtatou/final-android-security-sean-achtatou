package com.scoreloop.client.android.core.server;

import com.scoreloop.client.android.core.util.Logger;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.json.JSONObject;

public class Request {
    private static int a = 0;
    static final /* synthetic */ boolean g = (!Request.class.desiredAssertionStatus());
    private final RequestCompletionCallback b;
    private String c;
    private JSONObject d;
    private Exception e;
    private JSONObject f;
    private final int h = e();
    private RequestMethod i = RequestMethod.GET;
    private Response j;
    private State k = State.IDLE;
    private long l = 0;
    private Object m;

    public enum State {
        CANCELLED,
        COMPLETED,
        ENQUEUED,
        EXECUTING,
        FAILED,
        IDLE
    }

    public Request(RequestCompletionCallback requestCompletionCallback) {
        this.b = requestCompletionCallback;
    }

    public static int e() {
        int i2 = a;
        a = i2 + 1;
        return i2;
    }

    public String a() {
        return this.c;
    }

    public void a(long j2) {
        this.l = j2;
    }

    public void a(Response response) {
        if (g || this.k == State.EXECUTING) {
            this.k = State.COMPLETED;
            this.j = response;
            this.e = null;
            return;
        }
        throw new AssertionError();
    }

    public void a(Exception exc) {
        if (g || this.k == State.EXECUTING) {
            this.k = State.FAILED;
            this.j = null;
            this.e = exc;
            return;
        }
        throw new AssertionError();
    }

    public void a(Object obj) {
        this.m = obj;
    }

    public void a(JSONObject jSONObject) {
        this.f = jSONObject;
    }

    public JSONObject b() {
        return this.d;
    }

    public RequestMethod c() {
        return this.i;
    }

    public RequestCompletionCallback f() {
        if (this.b != null) {
            return this.b;
        }
        throw new IllegalStateException();
    }

    public Exception g() {
        return this.e;
    }

    public JSONObject h() {
        return this.f;
    }

    public int i() {
        return this.h;
    }

    public Response j() {
        return this.j;
    }

    public synchronized State k() {
        return this.k;
    }

    public Object l() {
        return this.m;
    }

    public boolean m() {
        State k2 = k();
        return k2 == State.COMPLETED || k2 == State.CANCELLED || k2 == State.FAILED;
    }

    public void n() {
        if (g || this.k == State.EXECUTING || this.k == State.ENQUEUED) {
            this.k = State.CANCELLED;
            this.j = null;
            this.e = null;
            return;
        }
        throw new AssertionError();
    }

    public void o() {
        if (g || this.k == State.IDLE) {
            this.k = State.ENQUEUED;
            return;
        }
        throw new AssertionError();
    }

    public void p() {
        if (g || this.k == State.IDLE || this.k == State.ENQUEUED) {
            this.k = State.EXECUTING;
            return;
        }
        throw new AssertionError();
    }

    public String q() {
        if (this.i != RequestMethod.GET) {
            return null;
        }
        try {
            String str = a() + b();
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                instance.reset();
                Logger.a("Request", " MD5(" + str + ")");
                instance.update(str.getBytes());
                return new BigInteger(1, instance.digest(str.getBytes())).toString(16);
            } catch (NoSuchAlgorithmException e2) {
                return null;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    public long r() {
        return this.l;
    }
}
