package com.scoreloop.client.android.ui.framework;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import eu.royalapps.boxingmachine.R;
import java.util.List;

public class TabsActivity extends ActivityGroup implements TabsActivityProtocol, View.OnClickListener, OptionsMenuForActivityGroup {
    private ScreenDescription _screenDescription;

    public boolean onCreateOptionsMenuForActivityGroup(Menu menu) {
        Activity bodyActivity = getLocalActivityManager().getActivity(getTabActivityIdentifier(this._screenDescription.getSelectedBodyIndex()));
        if (bodyActivity == null || !(bodyActivity instanceof OptionsMenuForActivityGroup)) {
            return true;
        }
        return ((OptionsMenuForActivityGroup) bodyActivity).onCreateOptionsMenuForActivityGroup(menu);
    }

    public boolean onPrepareOptionsMenuForActivityGroup(Menu menu) {
        Activity bodyActivity = getLocalActivityManager().getActivity(getTabActivityIdentifier(this._screenDescription.getSelectedBodyIndex()));
        if (bodyActivity == null || !(bodyActivity instanceof OptionsMenuForActivityGroup)) {
            return true;
        }
        return ((OptionsMenuForActivityGroup) bodyActivity).onPrepareOptionsMenuForActivityGroup(menu);
    }

    public boolean onOptionsItemSelectedForActivityGroup(MenuItem item) {
        Activity bodyActivity = getLocalActivityManager().getActivity(getTabActivityIdentifier(this._screenDescription.getSelectedBodyIndex()));
        if (bodyActivity == null || !(bodyActivity instanceof OptionsMenuForActivityGroup)) {
            return false;
        }
        return ((OptionsMenuForActivityGroup) bodyActivity).onOptionsItemSelectedForActivityGroup(item);
    }

    public boolean isNavigationAllowed(NavigationIntent navigationIntent) {
        Activity activity = getLocalActivityManager().getCurrentActivity();
        if (activity instanceof BaseActivity) {
            return ((BaseActivity) activity).isNavigationAllowed(navigationIntent);
        }
        return true;
    }

    public void onClick(View view) {
        startTab(((TabView) view).getSelectedSegment());
        ScreenManagerSingleton.get().onShowedTab(this._screenDescription);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sl_tabs);
        ScreenManagerSingleton.get().displayStoredDescriptionInTabs(this);
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        ((TabView) findViewById(R.id.sl_tabs_segments)).removeAllViews();
        ScreenManagerSingleton.get().displayStoredDescriptionInTabs(this);
    }

    public void startDescription(ScreenDescription description) {
        this._screenDescription = description;
        for (ActivityDescription bodyDescription : description.getBodyDescriptions()) {
            bodyDescription.setEnabledWantsClearTop(true);
        }
        TabView tabView = (TabView) findViewById(R.id.sl_tabs_segments);
        tabView.setOnSegmentClickListener(this);
        List<ActivityDescription> descriptions = description.getBodyDescriptions();
        for (int i = 0; i < descriptions.size(); i++) {
            TextView tv = (TextView) getLayoutInflater().inflate((int) R.layout.sl_tab_caption, (ViewGroup) null);
            tv.setLayoutParams(new LinearLayout.LayoutParams(0, (int) getResources().getDimension(R.dimen.sl_clickable_height), 1.0f));
            tv.setText(descriptions.get(i).getTabId());
            tabView.addView(tv);
        }
        tabView.prepareUsage();
        startTab(description.getSelectedBodyIndex());
    }

    private void startTab(int index) {
        ((TabView) findViewById(R.id.sl_tabs_segments)).switchToSegment(index);
        this._screenDescription.setSelectedBodyIndex(index);
        ActivityDescription bodyDescription = this._screenDescription.getBodyDescriptions().get(index);
        ActivityHelper.startLocalActivity(this, bodyDescription.getIntent(), getTabActivityIdentifier(index), R.id.sl_tabs_body, 0);
        bodyDescription.setEnabledWantsClearTop(false);
    }

    private String getTabActivityIdentifier(int index) {
        return "tab-" + index;
    }
}
