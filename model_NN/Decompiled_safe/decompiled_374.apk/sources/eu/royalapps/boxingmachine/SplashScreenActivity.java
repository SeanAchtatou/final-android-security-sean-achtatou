package eu.royalapps.boxingmachine;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreenActivity extends Activity {
    Handler _handler = new Handler();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.splash);
        final ImageView scoreloop = (ImageView) findViewById(R.id.scoreloop);
        final ImageView android = (ImageView) findViewById(R.id.android);
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.splash_anim);
        final Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.splash_anim);
        final Animation animation3 = AnimationUtils.loadAnimation(this, R.anim.splash_anim);
        ((ImageView) findViewById(R.id.logo)).startAnimation(animation);
        this._handler.postDelayed(new Runnable() {
            public void run() {
                android.setVisibility(0);
                android.startAnimation(animation2);
            }
        }, 1000);
        this._handler.postDelayed(new Runnable() {
            public void run() {
                scoreloop.setVisibility(0);
                scoreloop.startAnimation(animation3);
            }
        }, 2000);
        new Thread() {
            int wait = 0;

            public void run() {
                try {
                    super.run();
                    while (this.wait < 4000) {
                        sleep(100);
                        this.wait += 100;
                    }
                } catch (Exception e) {
                    System.out.println("EXc=" + e);
                } finally {
                    SplashScreenActivity.this.startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
                    SplashScreenActivity.this.finish();
                }
            }
        }.start();
    }
}
