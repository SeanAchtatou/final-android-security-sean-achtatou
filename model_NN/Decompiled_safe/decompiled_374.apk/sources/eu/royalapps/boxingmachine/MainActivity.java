package eu.royalapps.boxingmachine;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.LeaderboardsScreenActivity;
import com.scoreloop.client.android.ui.OnScoreSubmitObserver;
import com.scoreloop.client.android.ui.PostScoreOverlayActivity;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.scoreloop.client.android.ui.ShowResultOverlayActivity;

public class MainActivity extends Activity implements AccelerometerListener, View.OnClickListener, Runnable, AdListener, OnScoreSubmitObserver {
    private static final int DIALOG_PROGRESS = 10;
    private static final int SL_POST_SCORE = 1;
    private static final int SL_SHOW_RESULT = 0;
    public static String TAG = "BoxingMachine";
    private static final int WELCOME_BACK_TOAST_DELAY = 2000;
    long AD_DELAY = 15000;
    private Double _scoreResult;
    private int _submitStatus;
    private String _versionName;
    /* access modifiers changed from: private */
    public AdView adView;
    private float appliedAcceleration;
    private ImageView arrow;
    private float averageAccel = 0.0f;
    private MediaPlayer beep;
    private ImageView boxingBag;
    double calibration = 9.806650161743164d;
    private TextView displaySterngthText;
    private float displayedText;
    private long firstUpdate;
    Integer[] forcePics = {Integer.valueOf((int) R.drawable.hit_like), Integer.valueOf((int) R.drawable.force01), Integer.valueOf((int) R.drawable.force02), Integer.valueOf((int) R.drawable.force03), Integer.valueOf((int) R.drawable.force04), Integer.valueOf((int) R.drawable.force05), Integer.valueOf((int) R.drawable.force06), Integer.valueOf((int) R.drawable.force07), Integer.valueOf((int) R.drawable.force08), Integer.valueOf((int) R.drawable.force09)};
    private float[] gravity = new float[3];
    private ImageView hand;
    private long lastAdRefresh = 0;
    private long lastUpdate;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;
    private Handler mHandler = new Handler();
    private SensorManager mSensorManager;
    private ImageSwitcher mSwitcher;
    /* access modifiers changed from: private */
    public boolean mater;
    private float maxAccel = 0.0f;
    float maxRange;
    private MediaPlayer[] punches;
    private boolean screenLocked = false;
    private GameView sensorsView;
    private Runnable showHand;
    private boolean sound = false;
    private ImageView start;
    private ImageView submitBtn;
    private GoogleAnalyticsTracker tracker;
    private float velocity;
    private Vibrator vibrator;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        ((ImageView) findViewById(R.id.boxingMachine)).setOnClickListener(this);
        ((ImageView) findViewById(R.id.scoreloop)).setOnClickListener(this);
        this.submitBtn = (ImageView) findViewById(R.id.submitBtn);
        this.submitBtn.setOnClickListener(this);
        this.mSwitcher = (ImageSwitcher) findViewById(R.id.forceMeterSwitcher);
        this.mSwitcher.setInAnimation(AnimationUtils.loadAnimation(this, 17432578));
        this.mSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this, 17432579));
        this.boxingBag = (ImageView) findViewById(R.id.boxingBag);
        this.displaySterngthText = (TextView) findViewById(R.id.displaySterngthText);
        this.hand = (ImageView) findViewById(R.id.hand);
        this.arrow = (ImageView) findViewById(R.id.arrow);
        this.start = (ImageView) findViewById(R.id.startButton);
        this.beep = MediaPlayer.create(this, (int) R.raw.beep);
        this.punches = new MediaPlayer[3];
        this.punches[0] = MediaPlayer.create(this, (int) R.raw.bang1);
        this.punches[1] = MediaPlayer.create(this, (int) R.raw.bang2);
        this.punches[2] = MediaPlayer.create(this, (int) R.raw.bang3);
        this.displaySterngthText.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/DS-DIGI.TTF"));
        this.adView = (AdView) findViewById(R.id.adView);
        this.adView.setAdListener(this);
        refreshAd();
        this.tracker = GoogleAnalyticsTracker.getInstance();
        this.tracker.start("UA-607644-9", 10, this);
        try {
            this._versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            this._versionName = "NoVersion";
        }
        try {
            ScoreloopManagerSingleton.init(this, "LO9m0txjI0pQi2HoMrWrSmE1qqf1sUk+DKGYSdFrKxd+chqFto6t6g==");
        } catch (Exception e2) {
            Log.w("Debug", "Scoreloop Probleams", e2);
        }
        this.vibrator = (Vibrator) getSystemService("vibrator");
        resetMeter();
        this.showHand = new Runnable() {
            public void run() {
                MainActivity.this.showHand();
            }
        };
        setBagVisibility(true);
        if (savedInstanceState == null) {
            ScoreloopManagerSingleton.get().showWelcomeBackToast(2000);
        }
        Object mSensor = ((SensorManager) getSystemService("sensor")).getSensorList(1).get(0);
        if (mSensor != null) {
            this.maxRange = ((Sensor) mSensor).getMaximumRange();
        }
        trackEvent("accelerometer", "MaxRange:" + this.maxRange);
        trackPageView("start");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AccelerometerManager.setContext(this);
        if (AccelerometerManager.isSupported()) {
            AccelerometerManager.startListening(this);
        }
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (AccelerometerManager.isListening()) {
            AccelerometerManager.stopListening();
        }
        if (this.tracker != null) {
            this.tracker.dispatch();
        }
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        ScoreloopManagerSingleton.get().setOnStartGamePlayRequestObserver(null);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.tracker != null) {
            this.tracker.dispatch();
        }
        ScoreloopManagerSingleton.get().setOnScoreSubmitObserver(null);
        super.onPause();
    }

    public void onAccelerationChanged(float x, float y, float z, float force, float timeElapsed) {
        if (this.mater) {
            this.mAccel = (float) Math.sqrt((double) ((x * x) + (y * y) + (z * z)));
            this.mAccel = (float) (((double) this.mAccel) - this.calibration);
            long timeNow = System.currentTimeMillis();
            if (((double) this.mAccel) > this.calibration * 1.5d) {
                if (this.firstUpdate == 0) {
                    this.firstUpdate = System.currentTimeMillis();
                    setBagVisibility(false);
                    playSound();
                    this.mHandler.postDelayed(new Runnable() {
                        public void run() {
                            MainActivity.this.mater = false;
                        }
                    }, 1000);
                } else if (timeNow - this.firstUpdate > 1000) {
                    this.mater = false;
                }
                if (this.maxAccel < this.mAccel) {
                    this.maxAccel = this.mAccel;
                }
                this.averageAccel = this.averageAccel == 0.0f ? this.mAccel : (this.averageAccel + this.mAccel) / 2.0f;
                long timeDelta = 0;
                if (this.lastUpdate != 0) {
                    timeDelta = timeNow - this.lastUpdate;
                }
                this.lastUpdate = timeNow;
                float deltaVelocity = this.averageAccel * (((float) timeDelta) / 1000.0f);
                this.appliedAcceleration = this.mAccel;
                this.velocity += deltaVelocity;
                this.mHandler.removeCallbacks(this);
                this.mHandler.postDelayed(this, 1000);
                float f = ((float) (this.lastUpdate - this.firstUpdate)) / 1000.0f;
                this.screenLocked = true;
                hideHand();
            }
        }
    }

    public void onShake(float force) {
        Toast.makeText(this, "Phone shaked : " + force, 1000).show();
    }

    public void onClick(View v) {
        if (!this.screenLocked) {
            refreshAd();
        }
        if (v.getId() == R.id.scoreloop) {
            trackPageView("scoreloop");
            startActivity(new Intent(this, LeaderboardsScreenActivity.class));
        } else if (v.getId() == R.id.submitBtn) {
            submitScore();
        } else if (!isBagVisible() && !this.screenLocked) {
            resetMeter();
            setBagVisibility(true);
            hideSubmit();
        }
    }

    public void resetMeter() {
        this.mHandler.removeCallbacks(this);
        this.averageAccel = 0.0f;
        this.maxAccel = 0.0f;
        this.displayedText = 0.0f;
        setText("");
        this.lastUpdate = 0;
        this.firstUpdate = 0;
        this.velocity = 0.0f;
        this.appliedAcceleration = 0.0f;
        this.mSwitcher.setImageResource(this.forcePics[0].intValue());
        this.sound = false;
        this.displaySterngthText.setText("000");
        this.mater = true;
    }

    public void setText(String text) {
        TextView textView = (TextView) findViewById(R.id.sterngthText);
        textView.setText(text);
        textView.invalidate();
    }

    public static void d(String txt) {
        Log.d(TAG, txt);
    }

    public void setBagVisibility(boolean visible) {
        this.boxingBag.setVisibility(visible ? 0 : 4);
        if (visible) {
            this.mHandler.removeCallbacks(this.showHand);
            this.mHandler.postDelayed(this.showHand, 5000);
            this.vibrator.vibrate(100);
            this.start.clearAnimation();
            hideAd();
            return;
        }
        this.mHandler.removeCallbacks(this.showHand);
    }

    public boolean isBagVisible() {
        return this.boxingBag.getVisibility() == 0;
    }

    public void refreshAd() {
        long now = System.currentTimeMillis();
        showAd();
        if (this.adView != null && now - this.lastAdRefresh > this.AD_DELAY) {
            AdRequest adRequest = new AdRequest();
            adRequest.setTesting(false);
            this.adView.loadAd(adRequest);
            this.lastAdRefresh = now;
        }
    }

    public void showAd() {
        if (this.adView.getVisibility() != 0) {
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up_in);
            animation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                    MainActivity.this.adView.setVisibility(0);
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                }
            });
            this.adView.setAnimation(animation);
        }
    }

    public void hideAd() {
        if (Math.random() >= 0.5d) {
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_down_out);
            animation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation animation) {
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationEnd(Animation animation) {
                    MainActivity.this.adView.setVisibility(4);
                }
            });
            this.adView.setAnimation(animation);
        }
    }

    public void bigAd() {
        InterstitialAd interstitialAd = new InterstitialAd(this, "a14dd5759cb894c");
        AdRequest adRequest = new AdRequest();
        adRequest.setTesting(false);
        interstitialAd.loadAd(adRequest);
        interstitialAd.show();
    }

    public void playSound() {
        if (!this.sound) {
            MediaPlayer mp = this.punches[(int) Math.min(Math.floor(Math.random() * ((double) this.punches.length)), (double) (this.punches.length - 1))];
            this.sound = true;
            mp.start();
            this.vibrator.vibrate(300);
            trackPageView("hit");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void run() {
        Double scoreResult;
        refreshAd();
        this.mater = false;
        float realValue = ((this.averageAccel + this.maxAccel) / 2.0f) * (this.velocity + 5.0f);
        float needToDisplay = Math.min(realValue, 999.0f);
        this.displayedText = this.displayedText + Math.max(0.15f * (needToDisplay - this.displayedText), 5.0f);
        if (this.displayedText > needToDisplay) {
            this.displayedText = needToDisplay;
        }
        this.displaySterngthText.setText(String.format("%03.0f", Float.valueOf(this.displayedText)));
        this.displaySterngthText.invalidate();
        this.mHandler.removeCallbacks(this);
        if (this.displayedText < needToDisplay) {
            this.beep.start();
            this.mHandler.postDelayed(this, 16);
            return;
        }
        this.mSwitcher.setImageResource(this.forcePics[(int) Math.min(Math.max(this.displayedText / 100.0f, 1.0f), (float) (this.forcePics.length - 1))].intValue());
        this.screenLocked = false;
        Animation animation = new AlphaAnimation(1.5f, 0.5f);
        animation.setDuration(1000);
        animation.setRepeatCount(-1);
        animation.setRepeatMode(2);
        long time = this.lastUpdate - this.firstUpdate;
        this.start.startAnimation(animation);
        trackEvent("Show", "Result", "Point:" + ((int) (Math.floor((double) (this.displayedText / 100.0f)) * 100.0d)) + "-" + ((int) ((Math.ceil((double) (this.displayedText / 100.0f)) * 100.0d) - 1.0d)), Float.floatToIntBits(this.displayedText));
        trackEvent("Sensor", "MaxRange: " + this.maxRange, "RealPoint:" + ((int) (Math.floor((double) (realValue / 100.0f)) * 100.0d)) + "-" + ((int) ((Math.ceil((double) (realValue / 100.0f)) * 100.0d) - 1.0d)), Float.floatToIntBits(realValue));
        trackEvent("Sensor", "Values: " + this.maxRange, "Average:" + this.averageAccel + " Max:" + this.maxAccel + " Velocity:" + this.velocity + " Time:" + time, Float.floatToIntBits(realValue));
        trackPageView("show");
        try {
            scoreResult = Double.valueOf((double) Math.round(this.displayedText));
        } catch (NumberFormatException e) {
            scoreResult = Double.valueOf(0.0d);
        }
        this._scoreResult = scoreResult;
        showSubmit();
    }

    public void hideHand() {
        this.arrow.setVisibility(4);
        this.hand.clearAnimation();
        this.hand.setVisibility(4);
    }

    public void showHand() {
        trackPageView("hand");
        Toast.makeText(getApplicationContext(), getText(R.string.help), 1).show();
        this.arrow.setVisibility(0);
        this.hand.clearAnimation();
        this.hand.setVisibility(0);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.push);
        anim.setRepeatMode(2);
        anim.setRepeatCount(-1);
        this.hand.startAnimation(anim);
    }

    public void trackPageView(String pPage) {
        this.tracker.trackPageView("/" + this._versionName + "/" + pPage);
    }

    public void trackEvent(String pCategory, String pAction) {
        trackEvent(pCategory, pAction, "", 0);
    }

    public void trackEvent(String pCategory, String pAction, String pLabel, int pValue) {
        refreshAd();
        this.tracker.trackEvent(pCategory, pAction, pLabel, pValue);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.dialog_back_question)).setCancelable(false).setPositiveButton(getString(R.string.dialog_back_yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                MainActivity.this.finish();
            }
        }).setNegativeButton(getString(R.string.dialog_back_no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
        return true;
    }

    public void onDismissScreen(Ad arg0) {
    }

    public void onFailedToReceiveAd(Ad ad, AdRequest.ErrorCode errorCode) {
    }

    public void onLeaveApplication(Ad arg0) {
    }

    public void onPresentScreen(Ad arg0) {
    }

    public void onReceiveAd(Ad arg0) {
    }

    public void onScoreSubmit(int status, Exception error) {
        this._submitStatus = status;
        dismissDialog(10);
        startActivityForResult(new Intent(this, ShowResultOverlayActivity.class), 0);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:
                if (this._submitStatus != 4) {
                    startActivityForResult(new Intent(this, PostScoreOverlayActivity.class), 1);
                    return;
                }
                return;
            case 1:
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(com.scoreloop.client.android.core.model.Score, java.lang.Boolean):void
     arg types: [com.scoreloop.client.android.core.model.Score, int]
     candidates:
      com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(java.lang.Double, java.lang.Integer):void
      com.scoreloop.client.android.ui.ScoreloopManager.onGamePlayEnded(com.scoreloop.client.android.core.model.Score, java.lang.Boolean):void */
    /* access modifiers changed from: protected */
    public void submitScore() {
        showDialog(10);
        ScoreloopManagerSingleton.get().onGamePlayEnded(new Score(this._scoreResult, null), (Boolean) false);
        hideSubmit();
    }

    private Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("progress");
        return dialog;
    }

    private void showSubmit() {
        fadeInAnim(this.submitBtn);
    }

    private void hideSubmit() {
        fadeOutAnim(this.submitBtn);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 10:
                return createProgressDialog();
            default:
                return null;
        }
    }

    public void fadeInAnim(View view) {
        view.setVisibility(0);
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade));
        view.setClickable(true);
    }

    public void fadeOutAnim(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fade_out));
        view.setClickable(false);
    }
}
