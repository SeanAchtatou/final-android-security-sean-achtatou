package eu.royalapps.boxingmachine;

public interface AccelerometerListener {
    void onAccelerationChanged(float f, float f2, float f3, float f4, float f5);

    void onShake(float f);
}
