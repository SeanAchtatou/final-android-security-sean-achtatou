package eu.royalapps.boxingmachine;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.view.View;

public class GameView extends View implements SensorEventListener {
    private Bitmap mBitmap;
    private Canvas mCanvas = new Canvas();
    private int[] mColors = new int[6];
    private float mHeight;
    private float[] mLastValues = new float[6];
    private float mLastX;
    private float mMaxX;
    private float[] mOrientationValues = new float[3];
    private Paint mPaint = new Paint();
    private Path mPath = new Path();
    private RectF mRect = new RectF();
    private float[] mScale = new float[2];
    private float mSpeed = 1.0f;
    private float mWidth;
    private float mYOffset;

    public GameView(Context context) {
        super(context);
        this.mColors[0] = Color.argb(192, 255, 64, 64);
        this.mColors[1] = Color.argb(192, 64, 128, 64);
        this.mColors[2] = Color.argb(192, 64, 64, 255);
        this.mColors[3] = Color.argb(192, 64, 255, 255);
        this.mColors[4] = Color.argb(192, 128, 64, 128);
        this.mColors[5] = Color.argb(192, 255, 255, 64);
        this.mPaint.setFlags(1);
        this.mRect.set(-0.5f, -0.5f, 0.5f, 0.5f);
        this.mPath.arcTo(this.mRect, 0.0f, 180.0f);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        this.mCanvas.setBitmap(this.mBitmap);
        this.mCanvas.drawColor(-1711276033);
        this.mYOffset = ((float) h) * 0.5f;
        this.mScale[0] = -(((float) h) * 0.5f * 0.05098581f);
        this.mScale[1] = -(((float) h) * 0.5f * 0.016666668f);
        this.mWidth = (float) w;
        this.mHeight = (float) h;
        if (this.mWidth < this.mHeight) {
            this.mMaxX = (float) w;
        } else {
            this.mMaxX = (float) (w - 50);
        }
        this.mLastX = this.mMaxX;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        synchronized (this) {
            if (this.mBitmap != null) {
                Paint paint = this.mPaint;
                Path path = this.mPath;
                if (this.mLastX >= this.mMaxX) {
                    this.mLastX = 0.0f;
                    Canvas cavas = this.mCanvas;
                    float yoffset = this.mYOffset;
                    float maxx = this.mMaxX;
                    float oneG = 9.80665f * this.mScale[0];
                    paint.setColor(-1716868438);
                    cavas.drawColor(-1711276033);
                    cavas.drawLine(0.0f, yoffset, maxx, yoffset, paint);
                    cavas.drawLine(0.0f, yoffset + oneG, maxx, yoffset + oneG, paint);
                    cavas.drawLine(0.0f, yoffset - oneG, maxx, yoffset - oneG, paint);
                }
                canvas.drawBitmap(this.mBitmap, 0.0f, 0.0f, (Paint) null);
                float[] values = this.mOrientationValues;
                if (this.mWidth < this.mHeight) {
                    float w0 = this.mWidth * 0.333333f;
                    float w = w0 - 32.0f;
                    float x = w0 * 0.5f;
                    for (int i = 0; i < 3; i++) {
                        canvas.save(1);
                        canvas.translate(x, (0.5f * w) + 4.0f);
                        canvas.save(1);
                        paint.setColor(-1715420992);
                        canvas.scale(w, w);
                        canvas.drawOval(this.mRect, paint);
                        canvas.restore();
                        canvas.scale(w - 5.0f, w - 5.0f);
                        paint.setColor(-1711312880);
                        canvas.rotate(-values[i]);
                        canvas.drawPath(path, paint);
                        canvas.restore();
                        x += w0;
                    }
                } else {
                    float h0 = this.mHeight * 0.333333f;
                    float h = h0 - 32.0f;
                    float y = h0 * 0.5f;
                    for (int i2 = 0; i2 < 3; i2++) {
                        canvas.save(1);
                        canvas.translate(this.mWidth - ((0.5f * h) + 4.0f), y);
                        canvas.save(1);
                        paint.setColor(-1715420992);
                        canvas.scale(h, h);
                        canvas.drawOval(this.mRect, paint);
                        canvas.restore();
                        canvas.scale(h - 5.0f, h - 5.0f);
                        paint.setColor(-1711312880);
                        canvas.rotate(-values[i2]);
                        canvas.drawPath(path, paint);
                        canvas.restore();
                        y += h0;
                    }
                }
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public void onSensorChanged(SensorEvent event) {
        synchronized (this) {
            if (this.mBitmap != null) {
                Canvas canvas = this.mCanvas;
                Paint paint = this.mPaint;
                if (event.sensor.getType() == 3) {
                    for (int i = 0; i < 3; i++) {
                        this.mOrientationValues[i] = event.values[i];
                    }
                } else {
                    float newX = this.mLastX + this.mSpeed;
                    int j = event.sensor.getType() == 2 ? 1 : 0;
                    for (int i2 = 0; i2 < 3; i2++) {
                        int k = i2 + (j * 3);
                        float v = this.mYOffset + (event.values[i2] * this.mScale[j]);
                        paint.setColor(this.mColors[k]);
                        canvas.drawLine(this.mLastX, this.mLastValues[k], newX, v, paint);
                        this.mLastValues[k] = v;
                    }
                    if (event.sensor.getType() == 2) {
                        this.mLastX += this.mSpeed;
                    }
                }
                invalidate();
            }
        }
    }
}
