package eu.royalapps.boxingmachine;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import java.util.List;

public class AccelerometerManager {
    private static Activity context = null;
    /* access modifiers changed from: private */
    public static int interval = 1000;
    /* access modifiers changed from: private */
    public static AccelerometerListener listener;
    private static boolean running = false;
    private static Sensor sensor;
    private static SensorEventListener sensorEventListener = new SensorEventListener() {
        private float force = 0.0f;
        private long lastShake = 0;
        private long lastUpdate = 0;
        private float lastX = 0.0f;
        private float lastY = 0.0f;
        private float lastZ = 0.0f;
        private long now = 0;
        private long timeDiff = 0;
        private float x = 0.0f;
        private float y = 0.0f;
        private float z = 0.0f;

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        public void onSensorChanged(SensorEvent event) {
            this.now = event.timestamp;
            this.x = event.values[0];
            this.y = event.values[1];
            this.z = event.values[2];
            if (this.lastUpdate == 0) {
                this.lastUpdate = this.now;
                this.lastShake = this.now;
                this.lastX = this.x;
                this.lastY = this.y;
                this.lastZ = this.z;
            } else {
                this.timeDiff = this.now - this.lastUpdate;
                if (((float) this.timeDiff) > 0.5f) {
                    this.force = (Math.abs(((((this.x + this.y) + this.z) - this.lastX) - this.lastY) - this.lastZ) / ((float) this.timeDiff)) * 10000.0f;
                    if (this.force > AccelerometerManager.threshold) {
                        if (this.now - this.lastShake >= ((long) AccelerometerManager.interval)) {
                            AccelerometerManager.listener.onShake(this.force);
                        }
                        this.lastShake = this.now;
                    }
                    this.lastX = this.x;
                    this.lastY = this.y;
                    this.lastZ = this.z;
                    this.lastUpdate = this.now;
                }
            }
            AccelerometerManager.listener.onAccelerationChanged(this.x, this.y, this.z, this.force, (float) this.timeDiff);
        }
    };
    private static SensorManager sensorManager;
    private static Boolean supported;
    /* access modifiers changed from: private */
    public static float threshold = 200.0f;

    public static boolean isListening() {
        return running;
    }

    public static void stopListening() {
        running = false;
        try {
            if (sensorManager != null && sensorEventListener != null) {
                sensorManager.unregisterListener(sensorEventListener);
            }
        } catch (Exception e) {
        }
    }

    public static boolean isSupported() {
        if (supported == null) {
            if (getContext() != null) {
                sensorManager = (SensorManager) getContext().getSystemService("sensor");
                supported = new Boolean(sensorManager.getSensorList(1).size() > 0);
            } else {
                supported = Boolean.FALSE;
            }
        }
        return supported.booleanValue();
    }

    public static void configure(int threshold2, int interval2) {
        threshold = (float) threshold2;
        interval = interval2;
    }

    public static void startListening(AccelerometerListener accelerometerListener) {
        sensorManager = (SensorManager) getContext().getSystemService("sensor");
        List<Sensor> sensors = sensorManager.getSensorList(1);
        if (sensors.size() > 0) {
            sensor = sensors.get(0);
            running = sensorManager.registerListener(sensorEventListener, sensor, 0);
            listener = accelerometerListener;
        }
    }

    public static void startListening(AccelerometerListener accelerometerListener, int threshold2, int interval2) {
        configure(threshold2, interval2);
        startListening(accelerometerListener);
    }

    public static void setContext(Activity pActivity) {
        context = pActivity;
    }

    public static Activity getContext() {
        return context;
    }
}
