package matrix.bubbles;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;

public class Compressor {
    private BmpWrap compressor;
    private BmpWrap compressorHead;
    int steps = 0;

    public Compressor(BmpWrap compressorHead2, BmpWrap compressor2) {
        this.compressorHead = compressorHead2;
        this.compressor = compressor2;
    }

    public void saveState(Bundle map) {
        map.putInt("compressor-steps", this.steps);
    }

    public void restoreState(Bundle map) {
        this.steps = map.getInt("compressor-steps");
    }

    public void moveDown() {
        this.steps++;
    }

    public void paint(Canvas c, double scale, int dx, int dy) {
        for (int i = 0; i < this.steps; i++) {
            c.drawBitmap(this.compressor.bmp, (float) ((235.0d * scale) + ((double) dx)), (float) ((((double) ((i * 28) - 4)) * scale) + ((double) dy)), (Paint) null);
            c.drawBitmap(this.compressor.bmp, (float) ((391.0d * scale) + ((double) dx)), (float) ((((double) ((i * 28) - 4)) * scale) + ((double) dy)), (Paint) null);
        }
        c.drawBitmap(this.compressorHead.bmp, (float) ((192.0d * scale) + ((double) dx)), (float) ((((double) ((this.steps * 28) + 26)) * scale) + ((double) dy)), (Paint) null);
    }
}
