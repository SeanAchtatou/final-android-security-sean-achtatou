package matrix.bubbles;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int bbb = 2130837505;
        public static final int bubble_1 = 2130837506;
        public static final int bubble_2 = 2130837507;
        public static final int bubble_3 = 2130837508;
        public static final int bubble_4 = 2130837509;
        public static final int bubble_5 = 2130837510;
        public static final int bubble_6 = 2130837511;
        public static final int bubble_7 = 2130837512;
        public static final int bubble_8 = 2130837513;
        public static final int bubble_blink = 2130837514;
        public static final int bubble_colourblind_1 = 2130837515;
        public static final int bubble_colourblind_2 = 2130837516;
        public static final int bubble_colourblind_3 = 2130837517;
        public static final int bubble_colourblind_4 = 2130837518;
        public static final int bubble_colourblind_5 = 2130837519;
        public static final int bubble_colourblind_6 = 2130837520;
        public static final int bubble_colourblind_7 = 2130837521;
        public static final int bubble_colourblind_8 = 2130837522;
        public static final int bubble_font = 2130837523;
        public static final int cim_aim_small = 2130837524;
        public static final int close_eyes = 2130837525;
        public static final int compressor = 2130837526;
        public static final int compressor_body = 2130837527;
        public static final int fixed_1 = 2130837528;
        public static final int fixed_2 = 2130837529;
        public static final int fixed_3 = 2130837530;
        public static final int fixed_4 = 2130837531;
        public static final int fixed_5 = 2130837532;
        public static final int fixed_6 = 2130837533;
        public static final int frozen_1 = 2130837534;
        public static final int frozen_2 = 2130837535;
        public static final int frozen_3 = 2130837536;
        public static final int frozen_4 = 2130837537;
        public static final int frozen_5 = 2130837538;
        public static final int frozen_6 = 2130837539;
        public static final int frozen_7 = 2130837540;
        public static final int frozen_8 = 2130837541;
        public static final int hurry = 2130837542;
        public static final int icon = 2130837543;
        public static final int launcher = 2130837544;
        public static final int life = 2130837545;
        public static final int lose_panel = 2130837546;
        public static final int penguins = 2130837547;
        public static final int splash = 2130837548;
        public static final int void_panel = 2130837549;
        public static final int win_panel = 2130837550;
    }

    public static final class id {
        public static final int BANNER = 2131034112;
        public static final int IAB_BANNER = 2131034114;
        public static final int IAB_LEADERBOARD = 2131034115;
        public static final int IAB_MRECT = 2131034113;
        public static final int adView = 2131034117;
        public static final int game = 2131034116;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int applause = 2130968576;
        public static final int destroy_group = 2130968577;
        public static final int hurry = 2130968578;
        public static final int launch = 2130968579;
        public static final int lose = 2130968580;
        public static final int newroot_solo = 2130968581;
        public static final int noh = 2130968582;
        public static final int rebound = 2130968583;
        public static final int stick = 2130968584;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int install_editor = 2131099660;
        public static final int market_missing = 2131099661;
        public static final int menu_about = 2131099656;
        public static final int menu_colorblind_mode_off = 2131099651;
        public static final int menu_colorblind_mode_on = 2131099650;
        public static final int menu_dont_rush_me = 2131099657;
        public static final int menu_editor = 2131099659;
        public static final int menu_fullscreen_off = 2131099653;
        public static final int menu_fullscreen_on = 2131099652;
        public static final int menu_new_game = 2131099649;
        public static final int menu_rush_me = 2131099658;
        public static final int menu_sound_off = 2131099655;
        public static final int menu_sound_on = 2131099654;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
