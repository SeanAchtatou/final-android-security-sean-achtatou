package matrix.bubbles;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.Vector;

public class FrozenGame extends GameScreen {
    public static final int FIRE = 1;
    public static final int HORIZONTAL_MOVE = 0;
    public static final int KEY_LEFT = 37;
    public static final int KEY_M = 77;
    public static final int KEY_RIGHT = 39;
    public static final int KEY_S = 83;
    public static final int KEY_SHIFT = 16;
    public static final int KEY_UP = 38;
    public static String PARAMETER_OFFLINE = "offline";
    public static String PARAMETER_PLAYER = "player";
    BmpWrap background;
    int blinkDelay;
    BmpWrap bubbleBlink;
    BubbleManager bubbleManager;
    BubbleSprite[][] bubblePlay;
    BmpWrap[] bubbles;
    BmpWrap[] bubblesBlind;
    Compressor compressor;
    int currentColor;
    boolean endOfGame;
    Vector falling;
    int fixedBubbles;
    BmpWrap[] frozenBubbles;
    boolean frozenify;
    int frozenifyX;
    int frozenifyY;
    BmpWrap gameLost;
    BmpWrap gameWon;
    ImageSprite hurrySprite;
    int hurryTime;
    Vector jumping;
    LaunchBubbleSprite launchBubble;
    double launchBubblePosition;
    Drawable launcher;
    boolean levelCompleted = false;
    LevelManager levelManager;
    boolean modeKeyPressed;
    double moveDown;
    BubbleSprite movingBubble;
    int nbBubbles;
    ImageSprite nextBubble;
    int nextColor;
    PenguinSprite penguin;
    BmpWrap penguins;
    Random random = new Random(System.currentTimeMillis());
    boolean readyToFire;
    boolean soundKeyPressed;
    SoundManager soundManager;
    BmpWrap[] targetedBubbles;

    public FrozenGame(BmpWrap background_arg, BmpWrap[] bubbles_arg, BmpWrap[] bubblesBlind_arg, BmpWrap[] frozenBubbles_arg, BmpWrap[] targetedBubbles_arg, BmpWrap bubbleBlink_arg, BmpWrap gameWon_arg, BmpWrap gameLost_arg, BmpWrap hurry_arg, BmpWrap penguins_arg, BmpWrap compressorHead_arg, BmpWrap compressor_arg, Drawable launcher_arg, SoundManager soundManager_arg, LevelManager levelManager_arg) {
        this.launcher = launcher_arg;
        this.penguins = penguins_arg;
        this.background = background_arg;
        this.bubbles = bubbles_arg;
        this.bubblesBlind = bubblesBlind_arg;
        this.frozenBubbles = frozenBubbles_arg;
        this.targetedBubbles = targetedBubbles_arg;
        this.bubbleBlink = bubbleBlink_arg;
        this.gameWon = gameWon_arg;
        this.gameLost = gameLost_arg;
        this.soundManager = soundManager_arg;
        this.levelManager = levelManager_arg;
        this.launchBubblePosition = 20.0d;
        this.penguin = new PenguinSprite(penguins_arg, this.random);
        addSprite(this.penguin);
        this.compressor = new Compressor(compressorHead_arg, compressor_arg);
        this.hurrySprite = new ImageSprite(new Rect(302, 390, 334, 422), hurry_arg);
        this.jumping = new Vector();
        this.falling = new Vector();
        this.bubblePlay = (BubbleSprite[][]) Array.newInstance(BubbleSprite.class, 8, 13);
        this.bubbleManager = new BubbleManager(this.bubbles);
        byte[][] currentLevel = this.levelManager.getCurrentLevel();
        if (currentLevel != null) {
            for (int j = 0; j < 12; j++) {
                for (int i = j % 2; i < 8; i++) {
                    if (currentLevel[i][j] != -1) {
                        BubbleSprite newOne = new BubbleSprite(new Rect(((i * 32) + 190) - ((j % 2) * 16), (j * 28) + 44, 32, 32), currentLevel[i][j], this.bubbles[currentLevel[i][j]], this.bubblesBlind[currentLevel[i][j]], this.frozenBubbles[currentLevel[i][j]], this.bubbleBlink, this.bubbleManager, this.soundManager, this);
                        this.bubblePlay[i][j] = newOne;
                        addSprite(newOne);
                    }
                }
            }
            this.currentColor = this.bubbleManager.nextBubbleIndex(this.random);
            this.nextColor = this.bubbleManager.nextBubbleIndex(this.random);
            if (FrozenBubble.getMode() == 0) {
                this.nextBubble = new ImageSprite(new Rect(402, 390, 434, 422), this.bubbles[this.nextColor]);
            } else {
                this.nextBubble = new ImageSprite(new Rect(402, 390, 434, 422), this.bubblesBlind[this.nextColor]);
            }
            addSprite(this.nextBubble);
            this.launchBubble = new LaunchBubbleSprite(this.currentColor, (int) this.launchBubblePosition, this.launcher, this.bubbles, this.bubblesBlind);
            spriteToBack(this.launchBubble);
            this.nbBubbles = 0;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 11 */
    public void saveState(Bundle map) {
        Vector savedSprites = new Vector();
        saveSprites(map, savedSprites);
        for (int i = 0; i < this.jumping.size(); i++) {
            ((Sprite) this.jumping.elementAt(i)).saveState(map, savedSprites);
            map.putInt(String.format("jumping-%d", Integer.valueOf(i)), ((Sprite) this.jumping.elementAt(i)).getSavedId());
        }
        map.putInt("numJumpingSprites", this.jumping.size());
        for (int i2 = 0; i2 < this.falling.size(); i2++) {
            ((Sprite) this.falling.elementAt(i2)).saveState(map, savedSprites);
            map.putInt(String.format("falling-%d", Integer.valueOf(i2)), ((Sprite) this.falling.elementAt(i2)).getSavedId());
        }
        map.putInt("numFallingSprites", this.falling.size());
        for (int i3 = 0; i3 < 8; i3++) {
            for (int j = 0; j < 13; j++) {
                if (this.bubblePlay[i3][j] != null) {
                    this.bubblePlay[i3][j].saveState(map, savedSprites);
                    map.putInt(String.format("play-%d-%d", Integer.valueOf(i3), Integer.valueOf(j)), this.bubblePlay[i3][j].getSavedId());
                } else {
                    map.putInt(String.format("play-%d-%d", Integer.valueOf(i3), Integer.valueOf(j)), -1);
                }
            }
        }
        this.launchBubble.saveState(map, savedSprites);
        map.putInt("launchBubbleId", this.launchBubble.getSavedId());
        map.putDouble("launchBubblePosition", this.launchBubblePosition);
        this.penguin.saveState(map, savedSprites);
        this.compressor.saveState(map);
        map.putInt("penguinId", this.penguin.getSavedId());
        this.nextBubble.saveState(map, savedSprites);
        map.putInt("nextBubbleId", this.nextBubble.getSavedId());
        map.putInt("currentColor", this.currentColor);
        map.putInt("nextColor", this.nextColor);
        if (this.movingBubble != null) {
            this.movingBubble.saveState(map, savedSprites);
            map.putInt("movingBubbleId", this.movingBubble.getSavedId());
        } else {
            map.putInt("movingBubbleId", -1);
        }
        this.bubbleManager.saveState(map);
        map.putInt("fixedBubbles", this.fixedBubbles);
        map.putDouble("moveDown", this.moveDown);
        map.putInt("nbBubbles", this.nbBubbles);
        map.putInt("blinkDelay", this.blinkDelay);
        this.hurrySprite.saveState(map, savedSprites);
        map.putInt("hurryId", this.hurrySprite.getSavedId());
        map.putInt("hurryTime", this.hurryTime);
        map.putBoolean("readyToFire", this.readyToFire);
        map.putBoolean("endOfGame", this.endOfGame);
        map.putBoolean("frozenify", this.frozenify);
        map.putInt("frozenifyX", this.frozenifyX);
        map.putInt("frozenifyY", this.frozenifyY);
        map.putInt("numSavedSprites", savedSprites.size());
        for (int i4 = 0; i4 < savedSprites.size(); i4++) {
            ((Sprite) savedSprites.elementAt(i4)).clearSavedId();
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 39 */
    private Sprite restoreSprite(Bundle map, Vector imageList, int i) {
        int left = map.getInt(String.format("%d-left", Integer.valueOf(i)));
        int right = map.getInt(String.format("%d-right", Integer.valueOf(i)));
        int top = map.getInt(String.format("%d-top", Integer.valueOf(i)));
        int bottom = map.getInt(String.format("%d-bottom", Integer.valueOf(i)));
        int type = map.getInt(String.format("%d-type", Integer.valueOf(i)));
        if (type == Sprite.TYPE_BUBBLE) {
            int color = map.getInt(String.format("%d-color", Integer.valueOf(i)));
            return new BubbleSprite(new Rect(left, top, right, bottom), color, map.getDouble(String.format("%d-moveX", Integer.valueOf(i))), map.getDouble(String.format("%d-moveY", Integer.valueOf(i))), map.getDouble(String.format("%d-realX", Integer.valueOf(i))), map.getDouble(String.format("%d-realY", Integer.valueOf(i))), map.getBoolean(String.format("%d-fixed", Integer.valueOf(i))), map.getBoolean(String.format("%d-blink", Integer.valueOf(i))), map.getBoolean(String.format("%d-released", Integer.valueOf(i))), map.getBoolean(String.format("%d-checkJump", Integer.valueOf(i))), map.getBoolean(String.format("%d-checkFall", Integer.valueOf(i))), map.getInt(String.format("%d-fixedAnim", Integer.valueOf(i))), map.getBoolean(String.format("%d-frozen", new Object[]{Integer.valueOf(i)})) ? this.frozenBubbles[color] : this.bubbles[color], this.bubblesBlind[color], this.frozenBubbles[color], this.targetedBubbles, this.bubbleBlink, this.bubbleManager, this.soundManager, this);
        } else if (type == Sprite.TYPE_IMAGE) {
            return new ImageSprite(new Rect(left, top, right, bottom), (BmpWrap) imageList.elementAt(map.getInt(String.format("%d-imageId", Integer.valueOf(i)))));
        } else if (type == Sprite.TYPE_LAUNCH_BUBBLE) {
            return new LaunchBubbleSprite(map.getInt(String.format("%d-currentColor", Integer.valueOf(i))), map.getInt(String.format("%d-currentDirection", Integer.valueOf(i))), this.launcher, this.bubbles, this.bubblesBlind);
        } else if (type != Sprite.TYPE_PENGUIN) {
            return null;
        } else {
            return new PenguinSprite(this.penguins, this.random, map.getInt(String.format("%d-currentPenguin", Integer.valueOf(i))), map.getInt(String.format("%d-count", Integer.valueOf(i))), map.getInt(String.format("%d-finalState", Integer.valueOf(i))), map.getInt(String.format("%d-nextPosition", Integer.valueOf(i))));
        }
    }

    public void restoreState(Bundle map, Vector imageList) {
        Vector savedSprites = new Vector();
        int numSavedSprites = map.getInt("numSavedSprites");
        for (int i = 0; i < numSavedSprites; i++) {
            savedSprites.addElement(restoreSprite(map, imageList, i));
        }
        restoreSprites(map, savedSprites);
        this.jumping = new Vector();
        int numJumpingSprites = map.getInt("numJumpingSprites");
        for (int i2 = 0; i2 < numJumpingSprites; i2++) {
            this.jumping.addElement(savedSprites.elementAt(map.getInt(String.format("jumping-%d", Integer.valueOf(i2)))));
        }
        this.falling = new Vector();
        int numFallingSprites = map.getInt("numFallingSprites");
        for (int i3 = 0; i3 < numFallingSprites; i3++) {
            this.falling.addElement(savedSprites.elementAt(map.getInt(String.format("falling-%d", Integer.valueOf(i3)))));
        }
        this.bubblePlay = (BubbleSprite[][]) Array.newInstance(BubbleSprite.class, 8, 13);
        for (int i4 = 0; i4 < 8; i4++) {
            for (int j = 0; j < 13; j++) {
                int spriteIdx = map.getInt(String.format("play-%d-%d", Integer.valueOf(i4), Integer.valueOf(j)));
                if (spriteIdx != -1) {
                    this.bubblePlay[i4][j] = (BubbleSprite) savedSprites.elementAt(spriteIdx);
                } else {
                    this.bubblePlay[i4][j] = null;
                }
            }
        }
        this.launchBubble = (LaunchBubbleSprite) savedSprites.elementAt(map.getInt("launchBubbleId"));
        this.launchBubblePosition = map.getDouble("launchBubblePosition");
        this.penguin = (PenguinSprite) savedSprites.elementAt(map.getInt("penguinId"));
        this.compressor.restoreState(map);
        this.nextBubble = (ImageSprite) savedSprites.elementAt(map.getInt("nextBubbleId"));
        this.currentColor = map.getInt("currentColor");
        this.nextColor = map.getInt("nextColor");
        int movingBubbleId = map.getInt("movingBubbleId");
        if (movingBubbleId == -1) {
            this.movingBubble = null;
        } else {
            this.movingBubble = (BubbleSprite) savedSprites.elementAt(movingBubbleId);
        }
        this.bubbleManager.restoreState(map);
        this.fixedBubbles = map.getInt("fixedBubbles");
        this.moveDown = map.getDouble("moveDown");
        this.nbBubbles = map.getInt("nbBubbles");
        this.blinkDelay = map.getInt("blinkDelay");
        this.hurrySprite = (ImageSprite) savedSprites.elementAt(map.getInt("hurryId"));
        this.hurryTime = map.getInt("hurryTime");
        this.readyToFire = map.getBoolean("readyToFire");
        this.endOfGame = map.getBoolean("endOfGame");
        this.frozenify = map.getBoolean("frozenify");
        this.frozenifyX = map.getInt("frozenifyX");
        this.frozenifyY = map.getInt("frozenifyY");
    }

    private void initFrozenify() {
        ImageSprite freezeLaunchBubble = new ImageSprite(new Rect(302, 390, 32, 32), this.frozenBubbles[this.currentColor]);
        ImageSprite freezeNextBubble = new ImageSprite(new Rect(402, 390, 32, 32), this.frozenBubbles[this.nextColor]);
        addSprite(freezeLaunchBubble);
        addSprite(freezeNextBubble);
        this.frozenifyX = 7;
        this.frozenifyY = 12;
        this.frozenify = true;
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 120 */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004c, code lost:
        r7.frozenify = false;
        addSprite(new matrix.bubbles.ImageSprite(new android.graphics.Rect(170, 150, 470, 353), r7.gameLost));
        r7.soundManager.playSound(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0041, code lost:
        r7.frozenifyX = 7;
        r7.frozenifyY--;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x004a, code lost:
        if (r7.frozenifyY >= 0) goto L_0x0069;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void frozenify() {
        /*
            r7 = this;
            r6 = 150(0x96, float:2.1E-43)
            r5 = 8
            r4 = 7
            r3 = 0
            r2 = 1
            int r0 = r7.frozenifyX
            int r0 = r0 - r2
            r7.frozenifyX = r0
            int r0 = r7.frozenifyX
            if (r0 >= 0) goto L_0x0069
            r7.frozenifyX = r4
            int r0 = r7.frozenifyY
            int r0 = r0 - r2
            r7.frozenifyY = r0
            int r0 = r7.frozenifyY
            if (r0 >= 0) goto L_0x0069
            r7.frozenify = r3
            matrix.bubbles.ImageSprite r0 = new matrix.bubbles.ImageSprite
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 170(0xaa, float:2.38E-43)
            r3 = 470(0x1d6, float:6.59E-43)
            r4 = 353(0x161, float:4.95E-43)
            r1.<init>(r2, r6, r3, r4)
            matrix.bubbles.BmpWrap r2 = r7.gameLost
            r0.<init>(r1, r2)
            r7.addSprite(r0)
            matrix.bubbles.SoundManager r0 = r7.soundManager
            r0.playSound(r5)
        L_0x0037:
            return
        L_0x0038:
            int r0 = r7.frozenifyX
            int r0 = r0 - r2
            r7.frozenifyX = r0
            int r0 = r7.frozenifyX
            if (r0 >= 0) goto L_0x0069
            r7.frozenifyX = r4
            int r0 = r7.frozenifyY
            int r0 = r0 - r2
            r7.frozenifyY = r0
            int r0 = r7.frozenifyY
            if (r0 >= 0) goto L_0x0069
            r7.frozenify = r3
            matrix.bubbles.ImageSprite r0 = new matrix.bubbles.ImageSprite
            android.graphics.Rect r1 = new android.graphics.Rect
            r2 = 170(0xaa, float:2.38E-43)
            r3 = 470(0x1d6, float:6.59E-43)
            r4 = 353(0x161, float:4.95E-43)
            r1.<init>(r2, r6, r3, r4)
            matrix.bubbles.BmpWrap r2 = r7.gameLost
            r0.<init>(r1, r2)
            r7.addSprite(r0)
            matrix.bubbles.SoundManager r0 = r7.soundManager
            r0.playSound(r5)
            goto L_0x0037
        L_0x0069:
            matrix.bubbles.BubbleSprite[][] r0 = r7.bubblePlay
            int r1 = r7.frozenifyX
            r0 = r0[r1]
            int r1 = r7.frozenifyY
            r0 = r0[r1]
            if (r0 != 0) goto L_0x0079
            int r0 = r7.frozenifyY
            if (r0 >= 0) goto L_0x0038
        L_0x0079:
            matrix.bubbles.BubbleSprite[][] r0 = r7.bubblePlay
            int r1 = r7.frozenifyX
            r0 = r0[r1]
            int r1 = r7.frozenifyY
            r0 = r0[r1]
            r7.spriteToBack(r0)
            matrix.bubbles.BubbleSprite[][] r0 = r7.bubblePlay
            int r1 = r7.frozenifyX
            r0 = r0[r1]
            int r1 = r7.frozenifyY
            r0 = r0[r1]
            r0.frozenify()
            matrix.bubbles.LaunchBubbleSprite r0 = r7.launchBubble
            r7.spriteToBack(r0)
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: matrix.bubbles.FrozenGame.frozenify():void");
    }

    public BubbleSprite[][] getGrid() {
        return this.bubblePlay;
    }

    public void addFallingBubble(BubbleSprite sprite) {
        spriteToFront(sprite);
        this.falling.addElement(sprite);
    }

    public void deleteFallingBubble(BubbleSprite sprite) {
        removeSprite(sprite);
        this.falling.removeElement(sprite);
    }

    public void addJumpingBubble(BubbleSprite sprite) {
        spriteToFront(sprite);
        this.jumping.addElement(sprite);
    }

    public void deleteJumpingBubble(BubbleSprite sprite) {
        removeSprite(sprite);
        this.jumping.removeElement(sprite);
    }

    public Random getRandom() {
        return this.random;
    }

    public double getMoveDown() {
        return this.moveDown;
    }

    private int nextColor() {
        int nextColor2 = this.random.nextInt() % 8;
        if (nextColor2 < 0) {
            return -nextColor2;
        }
        return nextColor2;
    }

    private void sendBubblesDown() {
        this.soundManager.playSound(7);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 12; j++) {
                if (this.bubblePlay[i][j] != null) {
                    this.bubblePlay[i][j].moveDown();
                    if (this.bubblePlay[i][j].getSpritePosition().y >= 380) {
                        this.penguin.updateState(5);
                        this.endOfGame = true;
                        initFrozenify();
                        this.soundManager.playSound(1);
                    }
                }
            }
        }
        this.moveDown += 28.0d;
        this.compressor.moveDown();
    }

    private void blinkLine(int number) {
        int column = (number + 1) >> 1;
        for (int i = number % 2; i < 13; i++) {
            if (this.bubblePlay[column][i] != null) {
                this.bubblePlay[column][i].blink();
            }
        }
    }

    public boolean play(boolean key_left, boolean key_right, boolean key_fire, double trackball_dx, double touch_dx, double touchx, double touchy, boolean touch_fire) {
        int[] move = new int[2];
        if (key_left && !key_right) {
            move[0] = 37;
        } else if (!key_right || key_left) {
            move[0] = 0;
        } else {
            move[0] = 39;
        }
        if (key_fire) {
            move[1] = 38;
        } else {
            move[1] = 0;
        }
        if (move[1] == 0) {
            this.readyToFire = true;
        }
        if (FrozenBubble.getDontRushMe()) {
            this.hurryTime = 1;
        }
        if (this.endOfGame) {
            if (move[1] != 38 || !this.readyToFire) {
                this.penguin.updateState(3);
                if (this.frozenify) {
                    frozenify();
                }
            } else {
                if (this.levelCompleted) {
                    this.levelManager.goToNextLevel();
                }
                return true;
            }
        } else if (move[1] != 38 && this.hurryTime <= 480) {
            double dx = 0.0d;
            if (move[0] == 37) {
                dx = 0.0d - 1.0d;
            }
            if (move[0] == 39) {
                dx += 1.0d;
            }
            this.launchBubblePosition = this.launchBubblePosition + dx + trackball_dx + touch_dx;
            if (this.launchBubblePosition < 1.0d) {
                this.launchBubblePosition = 1.0d;
            }
            if (this.launchBubblePosition > 39.0d) {
                this.launchBubblePosition = 39.0d;
            }
            this.launchBubble.changeDirection((int) this.launchBubblePosition);
        } else if (this.movingBubble != null || !this.readyToFire) {
            this.penguin.updateState(3);
        } else {
            this.nbBubbles = this.nbBubbles + 1;
            if (touch_fire) {
                this.launchBubblePosition = (Math.atan((touchx - 310.0d) / (400.0d - touchy)) * 180.0d) / 3.141592653589793d;
                this.launchBubblePosition = 20.0d + ((this.launchBubblePosition * 20.0d) / 90.0d);
                if (this.launchBubblePosition < 1.0d) {
                    this.launchBubblePosition = 1.0d;
                }
                if (this.launchBubblePosition > 39.0d) {
                    this.launchBubblePosition = 39.0d;
                }
                this.launchBubble.changeDirection((int) this.launchBubblePosition);
            }
            this.movingBubble = new BubbleSprite(new Rect(302, 390, 32, 32), (int) this.launchBubblePosition, this.currentColor, this.bubbles[this.currentColor], this.bubblesBlind[this.currentColor], this.frozenBubbles[this.currentColor], this.targetedBubbles, this.bubbleBlink, this.bubbleManager, this.soundManager, this);
            addSprite(this.movingBubble);
            this.currentColor = this.nextColor;
            this.nextColor = this.bubbleManager.nextBubbleIndex(this.random);
            if (FrozenBubble.getMode() == 0) {
                this.nextBubble.changeImage(this.bubbles[this.nextColor]);
            } else {
                this.nextBubble.changeImage(this.bubblesBlind[this.nextColor]);
            }
            this.launchBubble.changeColor(this.currentColor);
            this.penguin.updateState(2);
            this.soundManager.playSound(2);
            this.readyToFire = false;
            this.hurryTime = 0;
            removeSprite(this.hurrySprite);
        }
        if (this.movingBubble != null) {
            this.movingBubble.move();
            if (this.movingBubble.fixed()) {
                if (this.movingBubble.getSpritePosition().y >= 380 && !this.movingBubble.released()) {
                    this.penguin.updateState(5);
                    this.endOfGame = true;
                    initFrozenify();
                    this.soundManager.playSound(1);
                } else if (this.bubbleManager.countBubbles() == 0) {
                    this.penguin.updateState(4);
                    addSprite(new ImageSprite(new Rect(170, 100, 470, 400), this.gameWon));
                    this.levelCompleted = true;
                    this.endOfGame = true;
                    this.soundManager.playSound(0);
                } else {
                    this.fixedBubbles = this.fixedBubbles + 1;
                    this.blinkDelay = 0;
                    if (this.fixedBubbles == 8) {
                        this.fixedBubbles = 0;
                        sendBubblesDown();
                    }
                }
                this.movingBubble = null;
            }
            if (this.movingBubble != null) {
                this.movingBubble.move();
                if (this.movingBubble.fixed()) {
                    if (this.movingBubble.getSpritePosition().y >= 380 && !this.movingBubble.released()) {
                        this.penguin.updateState(5);
                        this.endOfGame = true;
                        initFrozenify();
                        this.soundManager.playSound(1);
                    } else if (this.bubbleManager.countBubbles() == 0) {
                        this.penguin.updateState(4);
                        addSprite(new ImageSprite(new Rect(170, 100, 470, 400), this.gameWon));
                        this.endOfGame = true;
                        this.levelCompleted = true;
                        this.soundManager.playSound(0);
                    } else {
                        this.fixedBubbles = this.fixedBubbles + 1;
                        this.blinkDelay = 0;
                        if (this.fixedBubbles == 8) {
                            this.fixedBubbles = 0;
                            sendBubblesDown();
                        }
                    }
                    this.movingBubble = null;
                }
            }
        }
        if (this.movingBubble == null && !this.endOfGame) {
            this.hurryTime = this.hurryTime + 1;
            if (this.hurryTime == 2) {
                removeSprite(this.hurrySprite);
            }
            if (this.hurryTime >= 240) {
                if (this.hurryTime % 40 == 10) {
                    addSprite(this.hurrySprite);
                    this.soundManager.playSound(6);
                } else if (this.hurryTime % 40 == 35) {
                    removeSprite(this.hurrySprite);
                }
            }
        }
        if (this.fixedBubbles == 6) {
            if (this.blinkDelay < 15) {
                blinkLine(this.blinkDelay);
            }
            this.blinkDelay = this.blinkDelay + 1;
            if (this.blinkDelay == 40) {
                this.blinkDelay = 0;
            }
        } else if (this.fixedBubbles == 7) {
            if (this.blinkDelay < 15) {
                blinkLine(this.blinkDelay);
            }
            this.blinkDelay = this.blinkDelay + 1;
            if (this.blinkDelay == 25) {
                this.blinkDelay = 0;
            }
        }
        for (int i = 0; i < this.falling.size(); i++) {
            ((BubbleSprite) this.falling.elementAt(i)).fall();
        }
        for (int i2 = 0; i2 < this.jumping.size(); i2++) {
            ((BubbleSprite) this.jumping.elementAt(i2)).jump();
        }
        return false;
    }

    public void paint(Canvas c, double scale, int dx, int dy) {
        this.compressor.paint(c, scale, dx, dy);
        if (FrozenBubble.getMode() == 0) {
            this.nextBubble.changeImage(this.bubbles[this.nextColor]);
        } else {
            this.nextBubble.changeImage(this.bubblesBlind[this.nextColor]);
        }
        super.paint(c, scale, dx, dy);
    }
}
