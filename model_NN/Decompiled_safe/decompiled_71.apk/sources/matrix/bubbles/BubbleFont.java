package matrix.bubbles;

import android.graphics.Canvas;
import android.graphics.Rect;

public class BubbleFont {
    public int SEPARATOR_WIDTH;
    public int SPACE_CHAR_WIDTH;
    private char[] characters = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    private Rect clipRect;
    private BmpWrap fontMap;
    private int[] position;

    public BubbleFont(BmpWrap fontMap2) {
        int[] iArr = new int[11];
        iArr[1] = 17;
        iArr[2] = 30;
        iArr[3] = 48;
        iArr[4] = 66;
        iArr[5] = 82;
        iArr[6] = 101;
        iArr[7] = 121;
        iArr[8] = 139;
        iArr[9] = 157;
        iArr[10] = 172;
        this.position = iArr;
        this.SEPARATOR_WIDTH = 1;
        this.SPACE_CHAR_WIDTH = 6;
        this.fontMap = fontMap2;
        this.clipRect = new Rect();
    }

    public final void print(String s, int x, int y, Canvas canvas, double scale, int dx, int dy) {
        int len = s.length();
        for (int i = 0; i < len; i++) {
            x += paintChar(s.charAt(i), x, y, canvas, scale, dx, dy);
        }
    }

    public final int paintChar(char c, int x, int y, Canvas canvas, double scale, int dx, int dy) {
        if (c == ' ') {
            return this.SPACE_CHAR_WIDTH + this.SEPARATOR_WIDTH;
        }
        int index = getCharIndex(c);
        if (index == -1) {
            return 0;
        }
        int imageWidth = this.position[index + 1] - this.position[index];
        this.clipRect.left = x;
        this.clipRect.right = x + imageWidth;
        this.clipRect.top = y;
        this.clipRect.bottom = y + 26;
        Sprite.drawImageClipped(this.fontMap, x - this.position[index], y, this.clipRect, canvas, scale, dx, dy);
        return this.SEPARATOR_WIDTH + imageWidth;
    }

    private final int getCharIndex(char c) {
        for (int i = 0; i < this.characters.length; i++) {
            if (this.characters[i] == c) {
                return i;
            }
        }
        return -1;
    }
}
