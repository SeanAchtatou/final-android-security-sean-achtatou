package matrix.bubbles;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import java.util.Vector;

public class LaunchBubbleSprite extends Sprite {
    private BmpWrap[] bubbles;
    private BmpWrap[] colorblindBubbles;
    private int currentColor;
    private int currentDirection;
    private Drawable launcher;

    public LaunchBubbleSprite(int initialColor, int initialDirection, Drawable launcher2, BmpWrap[] bubbles2, BmpWrap[] colorblindBubbles2) {
        super(new Rect(276, 362, 362, 438));
        this.currentColor = initialColor;
        this.currentDirection = initialDirection;
        this.launcher = launcher2;
        this.bubbles = bubbles2;
        this.colorblindBubbles = colorblindBubbles2;
    }

    public void saveState(Bundle map, Vector saved_sprites) {
        if (getSavedId() == -1) {
            super.saveState(map, saved_sprites);
            map.putInt(String.format("%d-currentColor", Integer.valueOf(getSavedId())), this.currentColor);
            map.putInt(String.format("%d-currentDirection", Integer.valueOf(getSavedId())), this.currentDirection);
        }
    }

    public int getTypeId() {
        return Sprite.TYPE_LAUNCH_BUBBLE;
    }

    public void changeColor(int newColor) {
        this.currentColor = newColor;
    }

    public void changeDirection(int newDirection) {
        this.currentDirection = newDirection;
    }

    public final void paint(Canvas c, double scale, int dx, int dy) {
        c.save();
        c.rotate((float) (4.5d * ((double) (this.currentDirection - 20))), (float) ((((double) 318) * scale) + ((double) dx)), (float) ((((double) 406) * scale) + ((double) dy)));
        this.launcher.setBounds((int) ((((double) (318 - 50)) * scale) + ((double) dx)), (int) ((((double) (406 - 80)) * scale) + ((double) dy)), (int) ((((double) (318 + 50)) * scale) + ((double) dx)), (int) ((((double) (406 + 20)) * scale) + ((double) dy)));
        this.launcher.draw(c);
        c.restore();
        if (FrozenBubble.getMode() == 0) {
            drawImage(this.bubbles[this.currentColor], 302, 390, c, scale, dx, dy);
        } else {
            drawImage(this.colorblindBubbles[this.currentColor], 302, 390, c, scale, dx, dy);
        }
    }
}
