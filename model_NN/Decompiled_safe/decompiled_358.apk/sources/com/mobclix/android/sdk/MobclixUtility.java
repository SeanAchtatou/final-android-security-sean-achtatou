package com.mobclix.android.sdk;

import android.app.Activity;
import android.os.Environment;
import com.mobclix.android.sdk.MobclixFeedback;

class MobclixUtility {
    private static String TAG = "MobclixUtility";

    MobclixUtility() {
    }

    public static boolean isSdPresent() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    static String JSONescape(String s) {
        return s.replace("'", "\\'").replace("\\r", " ").replace("\\n", " ");
    }

    static class POSTThread implements Runnable {
        Activity activity;
        Mobclix controller = Mobclix.getInstance();
        MobclixFeedback.Listener listener;
        String params;
        String url;

        POSTThread(String u, String p, Activity a, MobclixFeedback.Listener l) {
            this.url = u;
            this.params = p;
            this.activity = a;
            this.listener = l;
        }

        /* JADX WARNING: Removed duplicated region for block: B:25:0x005d A[SYNTHETIC, Splitter:B:25:0x005d] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r8 = this;
                r3 = 0
                r1 = 0
                java.net.URL r6 = new java.net.URL     // Catch:{ Exception -> 0x006e }
                java.lang.String r7 = r8.url     // Catch:{ Exception -> 0x006e }
                r6.<init>(r7)     // Catch:{ Exception -> 0x006e }
                java.net.URLConnection r7 = r6.openConnection()     // Catch:{ Exception -> 0x006e }
                r0 = r7
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x006e }
                r1 = r0
                java.lang.String r7 = "POST"
                r1.setRequestMethod(r7)     // Catch:{ Exception -> 0x006e }
                r7 = 1
                r1.setDoInput(r7)     // Catch:{ Exception -> 0x006e }
                r7 = 1
                r1.setDoOutput(r7)     // Catch:{ Exception -> 0x006e }
                java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x006e }
                java.io.OutputStream r7 = r1.getOutputStream()     // Catch:{ Exception -> 0x006e }
                r4.<init>(r7)     // Catch:{ Exception -> 0x006e }
                java.lang.String r7 = r8.params     // Catch:{ Exception -> 0x0048, all -> 0x006b }
                r4.writeBytes(r7)     // Catch:{ Exception -> 0x0048, all -> 0x006b }
                int r5 = r1.getResponseCode()     // Catch:{ Exception -> 0x0048, all -> 0x006b }
                r7 = 200(0xc8, float:2.8E-43)
                if (r5 != r7) goto L_0x0044
                r8.onSuccess()     // Catch:{ Exception -> 0x0048, all -> 0x006b }
            L_0x0037:
                r1.disconnect()     // Catch:{ Exception -> 0x0048, all -> 0x006b }
                if (r4 == 0) goto L_0x0071
                r4.flush()     // Catch:{ Exception -> 0x0067 }
                r4.close()     // Catch:{ Exception -> 0x0067 }
                r3 = r4
            L_0x0043:
                return
            L_0x0044:
                r8.onFailure()     // Catch:{ Exception -> 0x0048, all -> 0x006b }
                goto L_0x0037
            L_0x0048:
                r7 = move-exception
                r2 = r7
                r3 = r4
            L_0x004b:
                r8.onFailure()     // Catch:{ all -> 0x005a }
                if (r3 == 0) goto L_0x0043
                r3.flush()     // Catch:{ Exception -> 0x0057 }
                r3.close()     // Catch:{ Exception -> 0x0057 }
                goto L_0x0043
            L_0x0057:
                r7 = move-exception
                r2 = r7
                goto L_0x0043
            L_0x005a:
                r7 = move-exception
            L_0x005b:
                if (r3 == 0) goto L_0x0063
                r3.flush()     // Catch:{ Exception -> 0x0064 }
                r3.close()     // Catch:{ Exception -> 0x0064 }
            L_0x0063:
                throw r7
            L_0x0064:
                r7 = move-exception
                r2 = r7
                goto L_0x0043
            L_0x0067:
                r7 = move-exception
                r2 = r7
                r3 = r4
                goto L_0x0043
            L_0x006b:
                r7 = move-exception
                r3 = r4
                goto L_0x005b
            L_0x006e:
                r7 = move-exception
                r2 = r7
                goto L_0x004b
            L_0x0071:
                r3 = r4
                goto L_0x0043
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobclix.android.sdk.MobclixUtility.POSTThread.run():void");
        }

        public void onSuccess() {
            if (this.listener != null && this.activity != null) {
                try {
                    this.activity.runOnUiThread(new Runnable() {
                        public void run() {
                            POSTThread.this.listener.onSuccess();
                        }
                    });
                } catch (Exception e) {
                }
            }
        }

        public void onFailure() {
            if (this.listener != null && this.activity != null) {
                try {
                    this.activity.runOnUiThread(new Runnable() {
                        public void run() {
                            POSTThread.this.listener.onFailure();
                        }
                    });
                } catch (Exception e) {
                }
            }
        }
    }
}
