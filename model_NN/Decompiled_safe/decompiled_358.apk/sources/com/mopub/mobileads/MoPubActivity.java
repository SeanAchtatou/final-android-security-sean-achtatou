package com.mopub.mobileads;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.RelativeLayout;

public class MoPubActivity extends Activity {
    public static final int MOPUB_ACTIVITY_NO_AD = 1234;
    private RelativeLayout mLayout;
    private MoPubView mMoPubView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        String adUnitId = getIntent().getStringExtra("com.mopub.mobileads.AdUnitId");
        String keywords = getIntent().getStringExtra("com.mopub.mobileads.Keywords");
        String source = getIntent().getStringExtra("com.mopub.mobileads.Source");
        int timeout = getIntent().getIntExtra("com.mopub.mobileads.Timeout", 0);
        if (adUnitId == null) {
            throw new RuntimeException("AdUnitId isn't set in com.mopub.mobileads.MoPubActivity");
        }
        this.mMoPubView = new MoPubView(this);
        this.mMoPubView.setAdUnitId(adUnitId);
        if (keywords != null) {
            this.mMoPubView.setKeywords(keywords);
        }
        if (timeout > 0) {
            this.mMoPubView.setTimeout(timeout);
        }
        if (source != null) {
            this.mMoPubView.loadHtmlString(sourceWithImpressionTrackingDisabled(source));
        }
        getWindow().addFlags(1024);
        this.mLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams adViewLayout = new RelativeLayout.LayoutParams(-1, -2);
        adViewLayout.addRule(13);
        this.mLayout.addView(this.mMoPubView, adViewLayout);
        setContentView(this.mLayout);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mMoPubView.destroy();
        super.onDestroy();
    }

    private String sourceWithImpressionTrackingDisabled(String source) {
        return source.replaceAll("http://ads.mopub.com/m/imp", "mopub://null");
    }
}
