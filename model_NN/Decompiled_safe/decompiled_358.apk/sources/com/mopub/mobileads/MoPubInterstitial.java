package com.mopub.mobileads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.mopub.mobileads.MoPubView;
import java.util.HashMap;

public class MoPubInterstitial {
    /* access modifiers changed from: private */
    public Activity mActivity;
    /* access modifiers changed from: private */
    public String mAdUnitId;
    /* access modifiers changed from: private */
    public MoPubInterstitialView mInterstitialView = new MoPubInterstitialView(this.mActivity);
    /* access modifiers changed from: private */
    public MoPubInterstitialListener mListener;

    public interface MoPubInterstitialListener {
        void OnInterstitialFailed();

        void OnInterstitialLoaded();
    }

    public class MoPubInterstitialView extends MoPubView {
        public MoPubInterstitialView(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void loadNativeSDK(HashMap<String, String> paramsHash) {
            MoPubInterstitial parent = MoPubInterstitial.this;
            String type = paramsHash.get("X-Adtype");
            if (type != null && type.equals("interstitial")) {
                Log.i("MoPub", "Loading native adapter for type: " + type);
                BaseInterstitialAdapter adapter = BaseInterstitialAdapter.getAdapterForType(parent, type, paramsHash);
                if (adapter != null) {
                    adapter.loadInterstitial();
                    return;
                }
            }
            Log.i("MoPub", "Couldn't load native adapter. Trying next ad...");
            parent.interstitialFailed();
        }

        /* access modifiers changed from: protected */
        public void trackImpression() {
            Log.d("MoPub", "Tracking impression for interstitial.");
            if (this.mAdView != null) {
                this.mAdView.trackImpression();
            }
        }
    }

    public MoPubInterstitial(Activity activity, String id) {
        this.mActivity = activity;
        this.mAdUnitId = id;
        this.mInterstitialView.setAdUnitId(this.mAdUnitId);
        this.mInterstitialView.setOnAdLoadedListener(new MoPubView.OnAdLoadedListener() {
            public void OnAdLoaded(MoPubView m) {
                if (MoPubInterstitial.this.mListener != null) {
                    MoPubInterstitial.this.mListener.OnInterstitialLoaded();
                }
                if (MoPubInterstitial.this.mActivity != null) {
                    String responseString = MoPubInterstitial.this.mInterstitialView.getResponseString();
                    Intent i = new Intent(MoPubInterstitial.this.mActivity, MoPubActivity.class);
                    i.putExtra("com.mopub.mobileads.AdUnitId", MoPubInterstitial.this.mAdUnitId);
                    i.putExtra("com.mopub.mobileads.Source", responseString);
                    MoPubInterstitial.this.mActivity.startActivity(i);
                }
            }
        });
        this.mInterstitialView.setOnAdFailedListener(new MoPubView.OnAdFailedListener() {
            public void OnAdFailed(MoPubView m) {
                if (MoPubInterstitial.this.mListener != null) {
                    MoPubInterstitial.this.mListener.OnInterstitialFailed();
                }
            }
        });
    }

    public Activity getActivity() {
        return this.mActivity;
    }

    public void showAd() {
        this.mInterstitialView.loadAd();
    }

    public void setListener(MoPubInterstitialListener listener) {
        this.mListener = listener;
    }

    public MoPubInterstitialListener getListener() {
        return this.mListener;
    }

    /* access modifiers changed from: protected */
    public void interstitialLoaded() {
        this.mInterstitialView.trackImpression();
        if (this.mListener != null) {
            this.mListener.OnInterstitialLoaded();
        }
    }

    /* access modifiers changed from: protected */
    public void interstitialFailed() {
        this.mInterstitialView.loadFailUrl();
    }

    /* access modifiers changed from: protected */
    public void interstitialClicked() {
        this.mInterstitialView.registerClick();
    }

    public void destroy() {
        this.mInterstitialView.destroy();
    }
}
