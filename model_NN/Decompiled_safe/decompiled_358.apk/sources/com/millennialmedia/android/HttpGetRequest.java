package com.millennialmedia.android;

import android.util.Log;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

class HttpGetRequest {
    private HttpClient client = new DefaultHttpClient();
    private HttpGet getRequest = new HttpGet();

    HttpGetRequest() {
    }

    /* access modifiers changed from: package-private */
    public HttpResponse get(String url) throws Exception {
        try {
            this.getRequest.setURI(new URI(url));
            return this.client.execute(this.getRequest);
        } catch (OutOfMemoryError e) {
            Log.e(MMAdViewSDK.SDKLOG, "Out of memory!");
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void trackConversion(String goalId, String auid, boolean isFirstLaunch) throws Exception {
        try {
            String url = "http://cvt.mydas.mobi/handleConversion?goalId=" + goalId + "&auid=" + auid + "&firstlaunch=" + (isFirstLaunch ? 1 : 0);
            MMAdViewSDK.Log.d("Sending conversion tracker report: " + url);
            this.getRequest.setURI(new URI(url));
            HttpResponse response = this.client.execute(this.getRequest);
            if (response.getStatusLine().getStatusCode() == 200) {
                MMAdViewSDK.Log.v("Conversion tracker reponse code: " + response.getStatusLine().getStatusCode());
            } else {
                Log.e(MMAdViewSDK.SDKLOG, "Conversion tracker unable to complete report: " + response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line != null) {
                    sb.append(line + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }
}
