package com.jtpub.catsanddogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;
import com.mopub.mobileads.MoPubView;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Activity2 extends Activity implements View.OnClickListener {
    private Button b1;
    private RadioButton rb1;
    private RadioButton rb10;
    private RadioButton rb11;
    private RadioButton rb12;
    private RadioButton rb13;
    private RadioButton rb14;
    private RadioButton rb15;
    private RadioButton rb16;
    private RadioButton rb17;
    private RadioButton rb18;
    private RadioButton rb19;
    private RadioButton rb2;
    private RadioButton rb20;
    private RadioButton rb21;
    private RadioButton rb22;
    private RadioButton rb23;
    private RadioButton rb24;
    private RadioButton rb25;
    private RadioButton rb26;
    private RadioButton rb27;
    private RadioButton rb28;
    private RadioButton rb29;
    private RadioButton rb3;
    private RadioButton rb30;
    private RadioButton rb31;
    private RadioButton rb32;
    private RadioButton rb33;
    private RadioButton rb34;
    private RadioButton rb35;
    private RadioButton rb36;
    private RadioButton rb37;
    private RadioButton rb38;
    private RadioButton rb39;
    private RadioButton rb4;
    private RadioButton rb40;
    private RadioButton rb41;
    private RadioButton rb42;
    private RadioButton rb43;
    private RadioButton rb44;
    private RadioButton rb45;
    private RadioButton rb46;
    private RadioButton rb47;
    private RadioButton rb48;
    private RadioButton rb5;
    private RadioButton rb6;
    private RadioButton rb7;
    private RadioButton rb8;
    private RadioButton rb9;
    private String titleOf;

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.instructions /*2131230895*/:
                showInstructions();
                return true;
            case R.id.ourapps /*2131230896*/:
                showOurApps();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showOurApps() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"JT%20Publishing\"")));
    }

    private void showInstructions() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Instructions");
        alertDialog.setMessage("1. Select ringtone to set. \n2. Click Set Ringtone to set as default ringtone. \nNote: Tone will also be available as alarm.");
        alertDialog.setButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setIcon((int) R.drawable.icon);
        alertDialog.show();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main2);
        MoPubView mpv = (MoPubView) findViewById(R.id.adview);
        mpv.setAdUnitId("agltb3B1Yi1pbmNyDQsSBFNpdGUY7qz6Cgw");
        mpv.loadAd();
        this.rb1 = (RadioButton) findViewById(R.id.option1);
        this.rb2 = (RadioButton) findViewById(R.id.option2);
        this.rb3 = (RadioButton) findViewById(R.id.option3);
        this.rb4 = (RadioButton) findViewById(R.id.option4);
        this.rb5 = (RadioButton) findViewById(R.id.option5);
        this.rb6 = (RadioButton) findViewById(R.id.option6);
        this.rb7 = (RadioButton) findViewById(R.id.option7);
        this.rb8 = (RadioButton) findViewById(R.id.option8);
        this.rb9 = (RadioButton) findViewById(R.id.option9);
        this.rb10 = (RadioButton) findViewById(R.id.option10);
        this.rb11 = (RadioButton) findViewById(R.id.option11);
        this.rb12 = (RadioButton) findViewById(R.id.option12);
        this.rb13 = (RadioButton) findViewById(R.id.option13);
        this.rb14 = (RadioButton) findViewById(R.id.option14);
        this.rb15 = (RadioButton) findViewById(R.id.option15);
        this.rb16 = (RadioButton) findViewById(R.id.option16);
        this.rb17 = (RadioButton) findViewById(R.id.option17);
        this.rb18 = (RadioButton) findViewById(R.id.option18);
        this.rb19 = (RadioButton) findViewById(R.id.option19);
        this.rb20 = (RadioButton) findViewById(R.id.option20);
        this.rb21 = (RadioButton) findViewById(R.id.option21);
        this.rb22 = (RadioButton) findViewById(R.id.option22);
        this.rb23 = (RadioButton) findViewById(R.id.option23);
        this.rb24 = (RadioButton) findViewById(R.id.option24);
        this.rb25 = (RadioButton) findViewById(R.id.option25);
        this.rb26 = (RadioButton) findViewById(R.id.option26);
        this.rb27 = (RadioButton) findViewById(R.id.option27);
        this.rb28 = (RadioButton) findViewById(R.id.option28);
        this.rb29 = (RadioButton) findViewById(R.id.option29);
        this.rb30 = (RadioButton) findViewById(R.id.option30);
        this.rb31 = (RadioButton) findViewById(R.id.option31);
        this.rb32 = (RadioButton) findViewById(R.id.option32);
        this.rb33 = (RadioButton) findViewById(R.id.option33);
        this.rb34 = (RadioButton) findViewById(R.id.option34);
        this.rb35 = (RadioButton) findViewById(R.id.option35);
        this.rb36 = (RadioButton) findViewById(R.id.option36);
        this.rb37 = (RadioButton) findViewById(R.id.option37);
        this.rb38 = (RadioButton) findViewById(R.id.option38);
        this.rb39 = (RadioButton) findViewById(R.id.option39);
        this.rb40 = (RadioButton) findViewById(R.id.option40);
        this.rb41 = (RadioButton) findViewById(R.id.option41);
        this.rb42 = (RadioButton) findViewById(R.id.option42);
        this.rb43 = (RadioButton) findViewById(R.id.option43);
        this.rb44 = (RadioButton) findViewById(R.id.option44);
        this.rb45 = (RadioButton) findViewById(R.id.option45);
        this.rb46 = (RadioButton) findViewById(R.id.option46);
        this.rb47 = (RadioButton) findViewById(R.id.option47);
        this.rb48 = (RadioButton) findViewById(R.id.option48);
        this.b1 = (Button) findViewById(R.id.selected);
        this.b1.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == this.b1) {
            if (this.rb1.isChecked()) {
                this.titleOf = getString(R.string.sound1);
                saveasring(R.raw.sound1);
            }
            if (this.rb2.isChecked()) {
                this.titleOf = getString(R.string.sound2);
                saveasring(R.raw.sound2);
            }
            if (this.rb3.isChecked()) {
                this.titleOf = getString(R.string.sound3);
                saveasring(R.raw.sound3);
            }
            if (this.rb4.isChecked()) {
                this.titleOf = getString(R.string.sound4);
                saveasring(R.raw.sound4);
            }
            if (this.rb5.isChecked()) {
                this.titleOf = getString(R.string.sound5);
                saveasring(R.raw.sound5);
            }
            if (this.rb6.isChecked()) {
                this.titleOf = getString(R.string.sound6);
                saveasring(R.raw.sound6);
            }
            if (this.rb7.isChecked()) {
                this.titleOf = getString(R.string.sound7);
                saveasring(R.raw.sound7);
            }
            if (this.rb8.isChecked()) {
                this.titleOf = getString(R.string.sound8);
                saveasring(R.raw.sound8);
            }
            if (this.rb9.isChecked()) {
                this.titleOf = getString(R.string.sound9);
                saveasring(R.raw.sound9);
            }
            if (this.rb10.isChecked()) {
                this.titleOf = getString(R.string.sound10);
                saveasring(R.raw.sound10);
            }
            if (this.rb11.isChecked()) {
                this.titleOf = getString(R.string.sound11);
                saveasring(R.raw.sound11);
            }
            if (this.rb12.isChecked()) {
                this.titleOf = getString(R.string.sound12);
                saveasring(R.raw.sound12);
            }
            if (this.rb13.isChecked()) {
                this.titleOf = getString(R.string.sound13);
                saveasring(R.raw.sound13);
            }
            if (this.rb14.isChecked()) {
                this.titleOf = getString(R.string.sound14);
                saveasring(R.raw.sound14);
            }
            if (this.rb15.isChecked()) {
                this.titleOf = getString(R.string.sound15);
                saveasring(R.raw.sound15);
            }
            if (this.rb16.isChecked()) {
                this.titleOf = getString(R.string.sound16);
                saveasring(R.raw.sound16);
            }
            if (this.rb17.isChecked()) {
                this.titleOf = getString(R.string.sound17);
                saveasring(R.raw.sound17);
            }
            if (this.rb18.isChecked()) {
                this.titleOf = getString(R.string.sound18);
                saveasring(R.raw.sound18);
            }
            if (this.rb19.isChecked()) {
                this.titleOf = getString(R.string.sound19);
                saveasring(R.raw.sound19);
            }
            if (this.rb20.isChecked()) {
                this.titleOf = getString(R.string.sound20);
                saveasring(R.raw.sound20);
            }
            if (this.rb21.isChecked()) {
                this.titleOf = getString(R.string.sound21);
                saveasring(R.raw.sound21);
            }
            if (this.rb22.isChecked()) {
                this.titleOf = getString(R.string.sound22);
                saveasring(R.raw.sound22);
            }
            if (this.rb23.isChecked()) {
                this.titleOf = getString(R.string.sound23);
                saveasring(R.raw.sound23);
            }
            if (this.rb24.isChecked()) {
                this.titleOf = getString(R.string.sound24);
                saveasring(R.raw.sound24);
            }
            if (this.rb25.isChecked()) {
                this.titleOf = getString(R.string.sound25);
                saveasring(R.raw.sound25);
            }
            if (this.rb26.isChecked()) {
                this.titleOf = getString(R.string.sound26);
                saveasring(R.raw.sound26);
            }
            if (this.rb27.isChecked()) {
                this.titleOf = getString(R.string.sound27);
                saveasring(R.raw.sound27);
            }
            if (this.rb28.isChecked()) {
                this.titleOf = getString(R.string.sound28);
                saveasring(R.raw.sound28);
            }
            if (this.rb29.isChecked()) {
                this.titleOf = getString(R.string.sound29);
                saveasring(R.raw.sound29);
            }
            if (this.rb30.isChecked()) {
                this.titleOf = getString(R.string.sound30);
                saveasring(R.raw.sound30);
            }
            if (this.rb31.isChecked()) {
                this.titleOf = getString(R.string.sound31);
                saveasring(R.raw.sound31);
            }
            if (this.rb32.isChecked()) {
                this.titleOf = getString(R.string.sound32);
                saveasring(R.raw.sound32);
            }
            if (this.rb33.isChecked()) {
                this.titleOf = getString(R.string.sound33);
                saveasring(R.raw.sound33);
            }
            if (this.rb34.isChecked()) {
                this.titleOf = getString(R.string.sound34);
                saveasring(R.raw.sound34);
            }
            if (this.rb35.isChecked()) {
                this.titleOf = getString(R.string.sound35);
                saveasring(R.raw.sound35);
            }
            if (this.rb36.isChecked()) {
                this.titleOf = getString(R.string.sound36);
                saveasring(R.raw.sound36);
            }
            if (this.rb37.isChecked()) {
                this.titleOf = getString(R.string.sound37);
                saveasring(R.raw.sound37);
            }
            if (this.rb38.isChecked()) {
                this.titleOf = getString(R.string.sound38);
                saveasring(R.raw.sound38);
            }
            if (this.rb39.isChecked()) {
                this.titleOf = getString(R.string.sound39);
                saveasring(R.raw.sound39);
            }
            if (this.rb40.isChecked()) {
                this.titleOf = getString(R.string.sound40);
                saveasring(R.raw.sound40);
            }
            if (this.rb41.isChecked()) {
                this.titleOf = getString(R.string.sound41);
                saveasring(R.raw.sound41);
            }
            if (this.rb42.isChecked()) {
                this.titleOf = getString(R.string.sound42);
                saveasring(R.raw.sound42);
            }
            if (this.rb43.isChecked()) {
                this.titleOf = getString(R.string.sound43);
                saveasring(R.raw.sound43);
            }
            if (this.rb44.isChecked()) {
                this.titleOf = getString(R.string.sound44);
                saveasring(R.raw.sound44);
            }
            if (this.rb45.isChecked()) {
                this.titleOf = getString(R.string.sound45);
                saveasring(R.raw.sound45);
            }
            if (this.rb46.isChecked()) {
                this.titleOf = getString(R.string.sound46);
                saveasring(R.raw.sound46);
            }
            if (this.rb47.isChecked()) {
                this.titleOf = getString(R.string.sound47);
                saveasring(R.raw.sound47);
            }
            if (this.rb48.isChecked()) {
                this.titleOf = getString(R.string.sound48);
                saveasring(R.raw.sound48);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public boolean saveasring(int ressound) {
        InputStream fIn = getBaseContext().getResources().openRawResource(ressound);
        try {
            byte[] buffer = new byte[fIn.available()];
            fIn.read(buffer);
            fIn.close();
            String filename = String.valueOf(this.titleOf) + ".mp3";
            if (!new File("/sdcard/media/audio/ringtones/").exists()) {
                new File("/sdcard/media/audio/ringtones/").mkdirs();
            }
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(String.valueOf("/sdcard/media/audio/ringtones/") + filename);
                fileOutputStream.write(buffer);
                fileOutputStream.flush();
                fileOutputStream.close();
                sendBroadcast(new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE", Uri.parse("file://" + "/sdcard/media/audio/ringtones/" + filename)));
                File k = new File("/sdcard/media/audio/ringtones/", filename);
                ContentValues values = new ContentValues();
                values.put("_data", k.getAbsolutePath());
                values.put("title", this.titleOf);
                values.put("mime_type", "audio/mp3");
                values.put("artist", Integer.valueOf((int) R.string.app_name));
                values.put("is_ringtone", (Boolean) true);
                values.put("is_notification", (Boolean) false);
                values.put("is_alarm", (Boolean) true);
                values.put("is_music", (Boolean) false);
                Uri uri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
                getContentResolver().delete(uri, "_data=\"" + k.getAbsolutePath() + "\"", null);
                RingtoneManager.setActualDefaultRingtoneUri(this, 1, getContentResolver().insert(uri, values));
                Toast.makeText(this, String.valueOf(this.titleOf) + " set as ringtone", 0).show();
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        } catch (IOException e3) {
            return false;
        }
    }
}
