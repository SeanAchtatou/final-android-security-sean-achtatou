package com.jtpub.catsanddogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.millennialmedia.android.MMAdView;
import com.mopub.mobileads.MoPubView;
import java.util.Hashtable;

public class Mainmenu extends Activity implements View.OnClickListener {
    public static final String MYAPID = "50716";
    private static final String TAG = "Soundboard";
    private static Button bStop;
    private static Button bbutton1;
    private static Button bbutton10;
    private static Button bbutton11;
    private static Button bbutton12;
    private static Button bbutton13;
    private static Button bbutton14;
    private static Button bbutton15;
    private static Button bbutton16;
    private static Button bbutton17;
    private static Button bbutton18;
    private static Button bbutton19;
    private static Button bbutton2;
    private static Button bbutton20;
    private static Button bbutton21;
    private static Button bbutton22;
    private static Button bbutton23;
    private static Button bbutton24;
    private static Button bbutton25;
    private static Button bbutton26;
    private static Button bbutton27;
    private static Button bbutton28;
    private static Button bbutton29;
    private static Button bbutton3;
    private static Button bbutton30;
    private static Button bbutton31;
    private static Button bbutton32;
    private static Button bbutton33;
    private static Button bbutton34;
    private static Button bbutton35;
    private static Button bbutton36;
    private static Button bbutton37;
    private static Button bbutton38;
    private static Button bbutton39;
    private static Button bbutton4;
    private static Button bbutton40;
    private static Button bbutton41;
    private static Button bbutton42;
    private static Button bbutton43;
    private static Button bbutton44;
    private static Button bbutton45;
    private static Button bbutton46;
    private static Button bbutton47;
    private static Button bbutton48;
    private static Button bbutton5;
    private static Button bbutton6;
    private static Button bbutton7;
    private static Button bbutton8;
    private static Button bbutton9;
    private static MMAdView interAdView;
    /* access modifiers changed from: private */
    public MediaPlayer player;

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.instructions:
                showInstructions();
                return true;
            case R.id.ourapps:
                showOurApps();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showOurApps() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:\"JT%20Publishing\"")));
    }

    private void showInstructions() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Instructions");
        alertDialog.setMessage("1. Use volume key on device to set desired volume. \n2. Touch any button to play sound. \n3. To set default sound, click notification or ringtone icon. Then follow the on screen instructions to set default sound. \n");
        alertDialog.setButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.setIcon((int) R.drawable.icon);
        alertDialog.show();
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        setVolumeControlStream(3);
        Hashtable<String, String> map = new Hashtable<>();
        map.put(MMAdView.KEY_AGE, "27");
        map.put(MMAdView.KEY_ZIP_CODE, "10312");
        map.put(MMAdView.KEY_INCOME, "100000");
        showInterstitial(this);
        MoPubView mpv = (MoPubView) findViewById(R.id.adview);
        mpv.setAdUnitId("agltb3B1Yi1pbmNyDQsSBFNpdGUY7qz6Cgw");
        mpv.loadAd();
        this.player = new MediaPlayer();
        bbutton1 = (Button) findViewById(R.id.button1);
        bbutton2 = (Button) findViewById(R.id.button2);
        bbutton3 = (Button) findViewById(R.id.button3);
        bbutton4 = (Button) findViewById(R.id.button4);
        bbutton5 = (Button) findViewById(R.id.button5);
        bbutton6 = (Button) findViewById(R.id.button6);
        bbutton7 = (Button) findViewById(R.id.button7);
        bbutton8 = (Button) findViewById(R.id.button8);
        bbutton9 = (Button) findViewById(R.id.button9);
        bbutton10 = (Button) findViewById(R.id.button10);
        bbutton11 = (Button) findViewById(R.id.button11);
        bbutton12 = (Button) findViewById(R.id.button12);
        bbutton13 = (Button) findViewById(R.id.button13);
        bbutton14 = (Button) findViewById(R.id.button14);
        bbutton15 = (Button) findViewById(R.id.button15);
        bbutton16 = (Button) findViewById(R.id.button16);
        bbutton17 = (Button) findViewById(R.id.button17);
        bbutton18 = (Button) findViewById(R.id.button18);
        bbutton19 = (Button) findViewById(R.id.button19);
        bbutton20 = (Button) findViewById(R.id.button20);
        bbutton21 = (Button) findViewById(R.id.button21);
        bbutton22 = (Button) findViewById(R.id.button22);
        bbutton23 = (Button) findViewById(R.id.button23);
        bbutton24 = (Button) findViewById(R.id.button24);
        bbutton25 = (Button) findViewById(R.id.button25);
        bbutton26 = (Button) findViewById(R.id.button26);
        bbutton27 = (Button) findViewById(R.id.button27);
        bbutton28 = (Button) findViewById(R.id.button28);
        bbutton29 = (Button) findViewById(R.id.button29);
        bbutton30 = (Button) findViewById(R.id.button30);
        bbutton31 = (Button) findViewById(R.id.button31);
        bbutton32 = (Button) findViewById(R.id.button32);
        bbutton33 = (Button) findViewById(R.id.button33);
        bbutton34 = (Button) findViewById(R.id.button34);
        bbutton35 = (Button) findViewById(R.id.button35);
        bbutton36 = (Button) findViewById(R.id.button36);
        bbutton37 = (Button) findViewById(R.id.button37);
        bbutton38 = (Button) findViewById(R.id.button38);
        bbutton39 = (Button) findViewById(R.id.button39);
        bbutton40 = (Button) findViewById(R.id.button40);
        bbutton41 = (Button) findViewById(R.id.button41);
        bbutton42 = (Button) findViewById(R.id.button42);
        bbutton43 = (Button) findViewById(R.id.button43);
        bbutton44 = (Button) findViewById(R.id.button44);
        bbutton45 = (Button) findViewById(R.id.button45);
        bbutton46 = (Button) findViewById(R.id.button46);
        bbutton47 = (Button) findViewById(R.id.button47);
        bbutton48 = (Button) findViewById(R.id.button48);
        bStop = (Button) findViewById(R.id.stop);
        bbutton1.setOnClickListener(this);
        bbutton2.setOnClickListener(this);
        bbutton3.setOnClickListener(this);
        bbutton4.setOnClickListener(this);
        bbutton5.setOnClickListener(this);
        bbutton6.setOnClickListener(this);
        bbutton7.setOnClickListener(this);
        bbutton8.setOnClickListener(this);
        bbutton9.setOnClickListener(this);
        bbutton10.setOnClickListener(this);
        bbutton11.setOnClickListener(this);
        bbutton12.setOnClickListener(this);
        bbutton13.setOnClickListener(this);
        bbutton14.setOnClickListener(this);
        bbutton15.setOnClickListener(this);
        bbutton16.setOnClickListener(this);
        bbutton17.setOnClickListener(this);
        bbutton18.setOnClickListener(this);
        bbutton19.setOnClickListener(this);
        bbutton20.setOnClickListener(this);
        bbutton21.setOnClickListener(this);
        bbutton22.setOnClickListener(this);
        bbutton23.setOnClickListener(this);
        bbutton24.setOnClickListener(this);
        bbutton25.setOnClickListener(this);
        bbutton26.setOnClickListener(this);
        bbutton27.setOnClickListener(this);
        bbutton28.setOnClickListener(this);
        bbutton29.setOnClickListener(this);
        bbutton30.setOnClickListener(this);
        bbutton31.setOnClickListener(this);
        bbutton32.setOnClickListener(this);
        bbutton33.setOnClickListener(this);
        bbutton34.setOnClickListener(this);
        bbutton35.setOnClickListener(this);
        bbutton36.setOnClickListener(this);
        bbutton37.setOnClickListener(this);
        bbutton38.setOnClickListener(this);
        bbutton39.setOnClickListener(this);
        bbutton40.setOnClickListener(this);
        bbutton41.setOnClickListener(this);
        bbutton42.setOnClickListener(this);
        bbutton43.setOnClickListener(this);
        bbutton44.setOnClickListener(this);
        bbutton45.setOnClickListener(this);
        bbutton46.setOnClickListener(this);
        bbutton47.setOnClickListener(this);
        bbutton48.setOnClickListener(this);
        bStop.setOnClickListener(this);
        ((Button) findViewById(R.id.ringtones)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Mainmenu.this.startActivityForResult(new Intent(view.getContext(), Activity2.class), 0);
            }
        });
        ((Button) findViewById(R.id.notifications)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Mainmenu.this.startActivityForResult(new Intent(view.getContext(), Activity3.class), 0);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, boolean, java.util.Hashtable<java.lang.String, java.lang.String>):void
     arg types: [com.jtpub.catsanddogs.Mainmenu, java.lang.String, java.lang.String, int, java.util.Hashtable<java.lang.String, java.lang.String>]
     candidates:
      com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, int, java.util.Hashtable<java.lang.String, java.lang.String>):void
      com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, int, boolean):void
      com.millennialmedia.android.MMAdView.<init>(android.app.Activity, java.lang.String, java.lang.String, boolean, java.util.Hashtable<java.lang.String, java.lang.String>):void */
    public static void showInterstitial(Mainmenu activity) {
        if (activity != null) {
            if (interAdView == null) {
                interAdView = new MMAdView((Activity) activity, MYAPID, MMAdView.FULLSCREEN_AD_LAUNCH, true, createMetaData());
                interAdView.setId(1897808290);
            }
            interAdView.callForAd();
        }
    }

    private static Hashtable<String, String> createMetaData() {
        Hashtable<String, String> map = new Hashtable<>();
        map.put(MMAdView.KEY_AGE, "27");
        map.put(MMAdView.KEY_GENDER, "male");
        map.put(MMAdView.KEY_ZIP_CODE, "10312");
        map.put(MMAdView.KEY_MARITAL_STATUS, "single");
        map.put(MMAdView.KEY_ORIENTATION, "straight");
        map.put(MMAdView.KEY_ETHNICITY, "white");
        map.put(MMAdView.KEY_INCOME, "76000");
        map.put(MMAdView.KEY_KEYWORDS, "games, video, movies, action");
        return map;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        resetPlayer();
        System.gc();
    }

    public void resetPlayer() {
        if (this.player != null) {
            if (this.player.isPlaying()) {
                this.player.stop();
            }
            this.player.release();
            this.player = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        resetPlayer();
        unbindDrawables(findViewById(R.id.RootView));
        System.gc();
    }

    private void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }

    public void onClick(View v) {
        int resid;
        resetPlayer();
        if (v != bStop) {
            if (v == bbutton1) {
                resid = R.raw.sound1;
            } else if (v == bbutton2) {
                resid = R.raw.sound2;
            } else if (v == bbutton3) {
                resid = R.raw.sound3;
            } else if (v == bbutton4) {
                resid = R.raw.sound4;
            } else if (v == bbutton5) {
                resid = R.raw.sound5;
            } else if (v == bbutton6) {
                resid = R.raw.sound6;
            } else if (v == bbutton7) {
                resid = R.raw.sound7;
            } else if (v == bbutton8) {
                resid = R.raw.sound8;
            } else if (v == bbutton9) {
                resid = R.raw.sound9;
            } else if (v == bbutton10) {
                resid = R.raw.sound10;
            } else if (v == bbutton11) {
                resid = R.raw.sound11;
            } else if (v == bbutton12) {
                resid = R.raw.sound12;
            } else if (v == bbutton13) {
                resid = R.raw.sound13;
            } else if (v == bbutton14) {
                resid = R.raw.sound14;
            } else if (v == bbutton15) {
                resid = R.raw.sound15;
            } else if (v == bbutton16) {
                resid = R.raw.sound16;
            } else if (v == bbutton17) {
                resid = R.raw.sound17;
            } else if (v == bbutton18) {
                resid = R.raw.sound18;
            } else if (v == bbutton19) {
                resid = R.raw.sound19;
            } else if (v == bbutton20) {
                resid = R.raw.sound20;
            } else if (v == bbutton21) {
                resid = R.raw.sound21;
            } else if (v == bbutton22) {
                resid = R.raw.sound22;
            } else if (v == bbutton23) {
                resid = R.raw.sound23;
            } else if (v == bbutton24) {
                resid = R.raw.sound24;
            } else if (v == bbutton25) {
                resid = R.raw.sound25;
            } else if (v == bbutton26) {
                resid = R.raw.sound26;
            } else if (v == bbutton27) {
                resid = R.raw.sound27;
            } else if (v == bbutton28) {
                resid = R.raw.sound28;
            } else if (v == bbutton29) {
                resid = R.raw.sound29;
            } else if (v == bbutton30) {
                resid = R.raw.sound30;
            } else if (v == bbutton31) {
                resid = R.raw.sound31;
            } else if (v == bbutton32) {
                resid = R.raw.sound32;
            } else if (v == bbutton33) {
                resid = R.raw.sound33;
            } else if (v == bbutton34) {
                resid = R.raw.sound34;
            } else if (v == bbutton35) {
                resid = R.raw.sound35;
            } else if (v == bbutton36) {
                resid = R.raw.sound36;
            } else if (v == bbutton37) {
                resid = R.raw.sound37;
            } else if (v == bbutton38) {
                resid = R.raw.sound38;
            } else if (v == bbutton39) {
                resid = R.raw.sound39;
            } else if (v == bbutton40) {
                resid = R.raw.sound40;
            } else if (v == bbutton41) {
                resid = R.raw.sound41;
            } else if (v == bbutton42) {
                resid = R.raw.sound42;
            } else if (v == bbutton43) {
                resid = R.raw.sound43;
            } else if (v == bbutton44) {
                resid = R.raw.sound44;
            } else if (v == bbutton45) {
                resid = R.raw.sound45;
            } else if (v == bbutton46) {
                resid = R.raw.sound46;
            } else if (v == bbutton47) {
                resid = R.raw.sound47;
            } else if (v == bbutton48) {
                resid = R.raw.sound48;
            } else {
                Log.d(TAG, "No resorce found for " + ((Button) v).getText().toString());
                resid = R.raw.sound1;
            }
            this.player = MediaPlayer.create(this, resid);
            this.player.setLooping(false);
            this.player.start();
            this.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer p) {
                    Mainmenu.this.player.release();
                    Mainmenu.this.player = null;
                }
            });
        }
    }
}
