package twitter4j.json;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import twitter4j.AccountTotals;
import twitter4j.Category;
import twitter4j.DirectMessage;
import twitter4j.IDs;
import twitter4j.Location;
import twitter4j.OEmbed;
import twitter4j.Place;
import twitter4j.RateLimitStatus;
import twitter4j.Relationship;
import twitter4j.SavedSearch;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.Trend;
import twitter4j.Trends;
import twitter4j.TwitterException;
import twitter4j.User;
import twitter4j.UserList;
import twitter4j.internal.org.json.JSONException;
import twitter4j.internal.org.json.JSONObject;

public final class DataObjectFactory {
    private static final Constructor<IDs> IDsConstructor;
    private static final Constructor<AccountTotals> accountTotalsConstructor;
    private static final Constructor<Category> categoryConstructor;
    private static final Constructor<DirectMessage> directMessageConstructor;
    private static final Constructor<Location> locationConstructor;
    private static final Constructor<OEmbed> oembedConstructor;
    private static final Constructor<Place> placeConstructor;
    private static final Method rateLimitStatusConstructor;
    private static final ThreadLocal<Map> rawJsonMap = new ThreadLocal<Map>() {
        /* access modifiers changed from: protected */
        public Map initialValue() {
            return new HashMap();
        }
    };
    private static final Constructor<Relationship> relationshipConstructor;
    private static final Constructor<SavedSearch> savedSearchConstructor;
    private static final Constructor<Status> statusConstructor;
    private static final Constructor<StatusDeletionNotice> statusDeletionNoticeConstructor;
    private static final Constructor<Trend> trendConstructor;
    private static final Constructor<Trends> trendsConstructor;
    private static final Constructor<User> userConstructor;
    private static final Constructor<UserList> userListConstructor;

    private DataObjectFactory() {
        throw new AssertionError("not intended to be instantiated.");
    }

    static {
        try {
            statusConstructor = Class.forName("twitter4j.internal.json.StatusJSONImpl").getDeclaredConstructor(JSONObject.class);
            statusConstructor.setAccessible(true);
            userConstructor = Class.forName("twitter4j.internal.json.UserJSONImpl").getDeclaredConstructor(JSONObject.class);
            userConstructor.setAccessible(true);
            relationshipConstructor = Class.forName("twitter4j.internal.json.RelationshipJSONImpl").getDeclaredConstructor(JSONObject.class);
            relationshipConstructor.setAccessible(true);
            placeConstructor = Class.forName("twitter4j.internal.json.PlaceJSONImpl").getDeclaredConstructor(JSONObject.class);
            placeConstructor.setAccessible(true);
            savedSearchConstructor = Class.forName("twitter4j.internal.json.SavedSearchJSONImpl").getDeclaredConstructor(JSONObject.class);
            savedSearchConstructor.setAccessible(true);
            trendConstructor = Class.forName("twitter4j.internal.json.TrendJSONImpl").getDeclaredConstructor(JSONObject.class);
            trendConstructor.setAccessible(true);
            trendsConstructor = Class.forName("twitter4j.internal.json.TrendsJSONImpl").getDeclaredConstructor(String.class);
            trendsConstructor.setAccessible(true);
            IDsConstructor = Class.forName("twitter4j.internal.json.IDsJSONImpl").getDeclaredConstructor(String.class);
            IDsConstructor.setAccessible(true);
            rateLimitStatusConstructor = Class.forName("twitter4j.internal.json.RateLimitStatusJSONImpl").getDeclaredMethod("createRateLimitStatuses", JSONObject.class);
            rateLimitStatusConstructor.setAccessible(true);
            categoryConstructor = Class.forName("twitter4j.internal.json.CategoryJSONImpl").getDeclaredConstructor(JSONObject.class);
            categoryConstructor.setAccessible(true);
            directMessageConstructor = Class.forName("twitter4j.internal.json.DirectMessageJSONImpl").getDeclaredConstructor(JSONObject.class);
            directMessageConstructor.setAccessible(true);
            locationConstructor = Class.forName("twitter4j.internal.json.LocationJSONImpl").getDeclaredConstructor(JSONObject.class);
            locationConstructor.setAccessible(true);
            userListConstructor = Class.forName("twitter4j.internal.json.UserListJSONImpl").getDeclaredConstructor(JSONObject.class);
            userListConstructor.setAccessible(true);
            statusDeletionNoticeConstructor = Class.forName("twitter4j.StatusDeletionNoticeImpl").getDeclaredConstructor(JSONObject.class);
            statusDeletionNoticeConstructor.setAccessible(true);
            accountTotalsConstructor = Class.forName("twitter4j.internal.json.AccountTotalsJSONImpl").getDeclaredConstructor(JSONObject.class);
            accountTotalsConstructor.setAccessible(true);
            oembedConstructor = Class.forName("twitter4j.internal.json.OEmbedJSONImpl").getDeclaredConstructor(JSONObject.class);
            oembedConstructor.setAccessible(true);
        } catch (NoSuchMethodException e) {
            throw new ExceptionInInitializerError(e);
        } catch (ClassNotFoundException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static String getRawJSON(Object obj) {
        Object json = rawJsonMap.get().get(obj);
        if (json instanceof String) {
            return (String) json;
        }
        if (json != null) {
            return json.toString();
        }
        return null;
    }

    public static Status createStatus(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return statusConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static User createUser(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return userConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static AccountTotals createAccountTotals(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return accountTotalsConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Relationship createRelationship(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return relationshipConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Place createPlace(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return placeConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static SavedSearch createSavedSearch(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return savedSearchConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Trend createTrend(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return trendConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Trends createTrends(String rawJSON) throws TwitterException {
        try {
            return trendsConstructor.newInstance(rawJSON);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new TwitterException(e2);
        } catch (InvocationTargetException e3) {
            throw new AssertionError(e3);
        }
    }

    public static IDs createIDs(String rawJSON) throws TwitterException {
        try {
            return IDsConstructor.newInstance(rawJSON);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        }
    }

    public static Map<String, RateLimitStatus> createRateLimitStatus(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return (Map) rateLimitStatusConstructor.invoke(Class.forName("twitter4j.internal.json.RateLimitStatusJSONImpl"), json);
        } catch (ClassNotFoundException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Category createCategory(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return categoryConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static DirectMessage createDirectMessage(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return directMessageConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Location createLocation(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return locationConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static UserList createUserList(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return userListConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static OEmbed createOEmbed(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            return oembedConstructor.newInstance(json);
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    public static Object createObject(String rawJSON) throws TwitterException {
        try {
            JSONObject json = new JSONObject(rawJSON);
            switch (JSONObjectType.determine(json)) {
                case SENDER:
                    return registerJSONObject(directMessageConstructor.newInstance(json.getJSONObject("direct_message")), json);
                case STATUS:
                    return registerJSONObject(statusConstructor.newInstance(json), json);
                case DIRECT_MESSAGE:
                    return registerJSONObject(directMessageConstructor.newInstance(json.getJSONObject("direct_message")), json);
                case DELETE:
                    return registerJSONObject(statusDeletionNoticeConstructor.newInstance(json.getJSONObject("delete").getJSONObject("status")), json);
                case LIMIT:
                case SCRUB_GEO:
                default:
                    return json;
            }
        } catch (InstantiationException e) {
            throw new TwitterException(e);
        } catch (IllegalAccessException e2) {
            throw new AssertionError(e2);
        } catch (InvocationTargetException e3) {
            throw new TwitterException(e3);
        } catch (JSONException e4) {
            throw new TwitterException(e4);
        }
    }

    static void clearThreadLocalMap() {
        rawJsonMap.get().clear();
    }

    static <T> T registerJSONObject(T key, Object json) {
        rawJsonMap.get().put(key, json);
        return key;
    }
}
