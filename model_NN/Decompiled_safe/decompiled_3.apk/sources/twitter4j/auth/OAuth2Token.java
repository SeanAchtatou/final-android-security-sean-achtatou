package twitter4j.auth;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import twitter4j.TwitterException;
import twitter4j.internal.http.HttpResponse;
import twitter4j.internal.json.z_T4JInternalParseUtil;
import twitter4j.internal.org.json.JSONObject;

public class OAuth2Token implements Serializable {
    private static final long serialVersionUID = 358222644448390610L;
    private String accessToken;
    private String tokenType;

    OAuth2Token(HttpResponse res) throws TwitterException {
        JSONObject json = res.asJSONObject();
        this.tokenType = z_T4JInternalParseUtil.getRawString("token_type", json);
        try {
            this.accessToken = URLDecoder.decode(z_T4JInternalParseUtil.getRawString("access_token", json), "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
    }

    public OAuth2Token(String tokenType2, String accessToken2) {
        this.tokenType = tokenType2;
        this.accessToken = accessToken2;
    }

    public String getTokenType() {
        return this.tokenType;
    }

    public String getAccessToken() {
        return this.accessToken;
    }

    /* access modifiers changed from: package-private */
    public String generateAuthorizationHeader() {
        String encoded = "";
        try {
            encoded = URLEncoder.encode(this.accessToken, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        return "Bearer " + encoded;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof OAuth2Token)) {
            return false;
        }
        OAuth2Token that = (OAuth2Token) obj;
        if (this.tokenType != null) {
            if (!this.tokenType.equals(that.tokenType)) {
                return false;
            }
        } else if (that.tokenType != null) {
            return false;
        }
        if (this.accessToken != null) {
            if (this.accessToken.equals(that.accessToken)) {
                return true;
            }
            return false;
        } else if (that.accessToken != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.tokenType != null) {
            result = this.tokenType.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.accessToken != null) {
            i = this.accessToken.hashCode();
        }
        return i2 + i;
    }

    public String toString() {
        return "OAuth2Token{tokenType='" + this.tokenType + '\'' + ", accessToken='" + this.accessToken + '\'' + '}';
    }
}
