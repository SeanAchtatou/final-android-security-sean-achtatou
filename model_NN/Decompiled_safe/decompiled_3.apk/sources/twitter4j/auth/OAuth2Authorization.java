package twitter4j.auth;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import twitter4j.TwitterException;
import twitter4j.conf.Configuration;
import twitter4j.internal.http.BASE64Encoder;
import twitter4j.internal.http.HttpClientWrapper;
import twitter4j.internal.http.HttpParameter;
import twitter4j.internal.http.HttpRequest;
import twitter4j.internal.http.HttpResponse;

public class OAuth2Authorization implements Authorization, Serializable, OAuth2Support {
    private static final long serialVersionUID = 4274784415515174129L;
    private final Configuration conf;
    private String consumerKey;
    private String consumerSecret;
    private HttpClientWrapper http;
    private OAuth2Token token;

    public OAuth2Authorization(Configuration conf2) {
        this.conf = conf2;
        setOAuthConsumer(conf2.getOAuthConsumerKey(), conf2.getOAuthConsumerSecret());
        this.http = new HttpClientWrapper(conf2);
    }

    public void setOAuthConsumer(String consumerKey2, String consumerSecret2) {
        if (consumerKey2 == null) {
            consumerKey2 = "";
        }
        this.consumerKey = consumerKey2;
        if (consumerSecret2 == null) {
            consumerSecret2 = "";
        }
        this.consumerSecret = consumerSecret2;
    }

    public OAuth2Token getOAuth2Token() throws TwitterException {
        if (this.token != null) {
            throw new IllegalStateException("OAuth 2 Bearer Token is already available.");
        }
        HttpResponse res = this.http.post(this.conf.getOAuth2TokenURL(), new HttpParameter[]{new HttpParameter("grant_type", "client_credentials")}, this);
        if (res.getStatusCode() != 200) {
            throw new TwitterException("Obtaining OAuth 2 Bearer Token failed.", res);
        }
        this.token = new OAuth2Token(res);
        return this.token;
    }

    public void setOAuth2Token(OAuth2Token oauth2Token) {
        this.token = oauth2Token;
    }

    public void invalidateOAuth2Token() throws TwitterException {
        if (this.token == null) {
            throw new IllegalStateException("OAuth 2 Bearer Token is not available.");
        }
        HttpParameter[] params = {new HttpParameter("access_token", this.token.getAccessToken())};
        OAuth2Token _token = this.token;
        boolean succeed = false;
        try {
            this.token = null;
            HttpResponse res = this.http.post(this.conf.getOAuth2InvalidateTokenURL(), params, this);
            if (res.getStatusCode() != 200) {
                throw new TwitterException("Invalidating OAuth 2 Bearer Token failed.", res);
            }
            succeed = true;
        } finally {
            if (!succeed) {
                this.token = _token;
            }
        }
    }

    public String getAuthorizationHeader(HttpRequest req) {
        if (this.token != null) {
            return this.token.generateAuthorizationHeader();
        }
        String credentials = "";
        try {
            credentials = URLEncoder.encode(this.consumerKey, "UTF-8") + ":" + URLEncoder.encode(this.consumerSecret, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        return "Basic " + BASE64Encoder.encode(credentials.getBytes());
    }

    public boolean isEnabled() {
        return this.token != null;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof OAuth2Authorization)) {
            return false;
        }
        OAuth2Authorization that = (OAuth2Authorization) obj;
        if (this.consumerKey != null) {
            if (!this.consumerKey.equals(that.consumerKey)) {
                return false;
            }
        } else if (that.consumerKey != null) {
            return false;
        }
        if (this.consumerSecret != null) {
            if (!this.consumerSecret.equals(that.consumerSecret)) {
                return false;
            }
        } else if (that.consumerSecret != null) {
            return false;
        }
        if (this.token != null) {
            if (this.token.equals(that.token)) {
                return true;
            }
            return false;
        } else if (that.token != null) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result;
        int i;
        int i2 = 0;
        if (this.consumerKey != null) {
            result = this.consumerKey.hashCode();
        } else {
            result = 0;
        }
        int i3 = result * 31;
        if (this.consumerSecret != null) {
            i = this.consumerSecret.hashCode();
        } else {
            i = 0;
        }
        int i4 = (i3 + i) * 31;
        if (this.token != null) {
            i2 = this.token.hashCode();
        }
        return i4 + i2;
    }

    public String toString() {
        return "OAuth2Authorization{consumerKey='" + this.consumerKey + '\'' + ", consumerSecret='******************************************'" + ", token=" + this.token.toString() + '}';
    }
}
