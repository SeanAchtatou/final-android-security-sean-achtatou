package com.google.ads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.google.ads.internal.AdVideoView;
import com.google.ads.internal.AdWebView;
import com.google.ads.internal.a;
import com.google.ads.internal.d;
import com.google.ads.internal.e;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import com.google.ads.util.g;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdActivity extends Activity implements View.OnClickListener {
    public static final String BASE_URL_PARAM = "baseurl";
    public static final String COMPONENT_NAME_PARAM = "c";
    public static final String CUSTOM_CLOSE_PARAM = "custom_close";
    public static final String HTML_PARAM = "html";
    public static final String INTENT_ACTION_PARAM = "i";
    public static final String INTENT_EXTRAS_PARAM = "e";
    public static final String INTENT_FLAGS_PARAM = "f";
    public static final String ORIENTATION_PARAM = "o";
    public static final String PACKAGE_NAME_PARAM = "p";
    public static final String TYPE_PARAM = "m";
    public static final String URL_PARAM = "u";
    private static final a a = a.a.b();
    /* access modifiers changed from: private */
    public static final Object b = new Object();
    /* access modifiers changed from: private */
    public static AdActivity c = null;
    /* access modifiers changed from: private */
    public static d d = null;
    /* access modifiers changed from: private */
    public static AdActivity e = null;
    private static AdActivity f = null;
    private static final StaticMethodWrapper g = new StaticMethodWrapper();
    private AdWebView h;
    private FrameLayout i;
    private int j;
    private ViewGroup k = null;
    private boolean l;
    private long m;
    private RelativeLayout n;
    private AdActivity o = null;
    private boolean p;
    private boolean q;
    private boolean r;
    private boolean s;
    private AdVideoView t;

    public static class StaticMethodWrapper {
        public boolean isShowing() {
            boolean z;
            synchronized (AdActivity.b) {
                z = AdActivity.e != null;
            }
            return z;
        }

        public boolean leftApplication() {
            boolean z;
            synchronized (AdActivity.b) {
                z = AdActivity.c != null;
            }
            return z;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
            r1 = new android.content.Intent(r0.getApplicationContext(), com.google.ads.AdActivity.class);
            r1.putExtra("com.google.ads.AdOpener", r6.a());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            com.google.ads.util.b.a("Launching AdActivity.");
            r0.startActivity(r1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0051, code lost:
            com.google.ads.util.b.b("Activity not found.", r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
            r0 = r5.i().c.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
            if (r0 != null) goto L_0x0033;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
            com.google.ads.util.b.e("activity was null while launching an AdActivity.");
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void launchAdActivity(com.google.ads.internal.d r5, com.google.ads.internal.e r6) {
            /*
                r4 = this;
                java.lang.Object r1 = com.google.ads.AdActivity.b
                monitor-enter(r1)
                com.google.ads.internal.d r0 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x0030 }
                if (r0 != 0) goto L_0x0023
                com.google.ads.internal.d unused = com.google.ads.AdActivity.d = r5     // Catch:{ all -> 0x0030 }
            L_0x000e:
                monitor-exit(r1)     // Catch:{ all -> 0x0030 }
                com.google.ads.n r0 = r5.i()
                com.google.ads.util.i$d<android.app.Activity> r0 = r0.c
                java.lang.Object r0 = r0.a()
                android.app.Activity r0 = (android.app.Activity) r0
                if (r0 != 0) goto L_0x0033
                java.lang.String r0 = "activity was null while launching an AdActivity."
                com.google.ads.util.b.e(r0)
            L_0x0022:
                return
            L_0x0023:
                com.google.ads.internal.d r0 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x0030 }
                if (r0 == r5) goto L_0x000e
                java.lang.String r0 = "Tried to launch a new AdActivity with a different AdManager."
                com.google.ads.util.b.b(r0)     // Catch:{ all -> 0x0030 }
                monitor-exit(r1)     // Catch:{ all -> 0x0030 }
                goto L_0x0022
            L_0x0030:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0030 }
                throw r0
            L_0x0033:
                android.content.Intent r1 = new android.content.Intent
                android.content.Context r2 = r0.getApplicationContext()
                java.lang.Class<com.google.ads.AdActivity> r3 = com.google.ads.AdActivity.class
                r1.<init>(r2, r3)
                java.lang.String r2 = "com.google.ads.AdOpener"
                android.os.Bundle r3 = r6.a()
                r1.putExtra(r2, r3)
                java.lang.String r2 = "Launching AdActivity."
                com.google.ads.util.b.a(r2)     // Catch:{ ActivityNotFoundException -> 0x0050 }
                r0.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0050 }
                goto L_0x0022
            L_0x0050:
                r0 = move-exception
                java.lang.String r1 = "Activity not found."
                com.google.ads.util.b.b(r1, r0)
                goto L_0x0022
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.StaticMethodWrapper.launchAdActivity(com.google.ads.internal.d, com.google.ads.internal.e):void");
        }
    }

    /* access modifiers changed from: protected */
    public View a(int i2, boolean z) {
        this.j = (int) TypedValue.applyDimension(1, (float) i2, getResources().getDisplayMetrics());
        this.i = new FrameLayout(getApplicationContext());
        this.i.setMinimumWidth(this.j);
        this.i.setMinimumHeight(this.j);
        this.i.setOnClickListener(this);
        setCustomClose(z);
        return this.i;
    }

    private void a(String str) {
        b.b(str);
        finish();
    }

    private void a(String str, Throwable th) {
        b.b(str, th);
        finish();
    }

    public AdVideoView getAdVideoView() {
        return this.t;
    }

    public AdWebView getOpeningAdWebView() {
        if (this.o != null) {
            return this.o.h;
        }
        synchronized (b) {
            if (d == null) {
                b.e("currentAdManager was null while trying to get the opening AdWebView.");
                return null;
            }
            AdWebView l2 = d.l();
            if (l2 != this.h) {
                return l2;
            }
            return null;
        }
    }

    public static boolean isShowing() {
        return g.isShowing();
    }

    public static boolean leftApplication() {
        return g.leftApplication();
    }

    public static void launchAdActivity(d adManager, e adOpener) {
        g.launchAdActivity(adManager, adOpener);
    }

    /* access modifiers changed from: protected */
    public void a(HashMap<String, String> hashMap, d dVar) {
        boolean z;
        int i2;
        if (hashMap == null) {
            a("Could not get the paramMap in launchIntent()");
            return;
        }
        Intent intent = new Intent();
        String str = hashMap.get("u");
        String str2 = hashMap.get(TYPE_PARAM);
        String str3 = hashMap.get("i");
        String str4 = hashMap.get("p");
        String str5 = hashMap.get("c");
        String str6 = hashMap.get("f");
        String str7 = hashMap.get(INTENT_EXTRAS_PARAM);
        boolean z2 = !TextUtils.isEmpty(str);
        if (!TextUtils.isEmpty(str2)) {
            z = true;
        } else {
            z = false;
        }
        if (z2 && z) {
            intent.setDataAndType(Uri.parse(str), str2);
        } else if (z2) {
            intent.setData(Uri.parse(str));
        } else if (z) {
            intent.setType(str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            intent.setAction(str3);
        } else if (z2) {
            intent.setAction("android.intent.action.VIEW");
        }
        if (!TextUtils.isEmpty(str4) && AdUtil.a >= 4) {
            com.google.ads.util.e.a(intent, str4);
        }
        if (!TextUtils.isEmpty(str5)) {
            String[] split = str5.split("/");
            if (split.length < 2) {
                b.e("Warning: Could not parse component name from open GMSG: " + str5);
            }
            intent.setClassName(split[0], split[1]);
        }
        if (!TextUtils.isEmpty(str6)) {
            try {
                i2 = Integer.parseInt(str6);
            } catch (NumberFormatException e2) {
                b.e("Warning: Could not parse flags from open GMSG: " + str6);
                i2 = 0;
            }
            intent.addFlags(i2);
        }
        if (!TextUtils.isEmpty(str7)) {
            try {
                JSONObject jSONObject = new JSONObject(str7);
                JSONArray names = jSONObject.names();
                for (int i3 = 0; i3 < names.length(); i3++) {
                    String string = names.getString(i3);
                    JSONObject jSONObject2 = jSONObject.getJSONObject(string);
                    int i4 = jSONObject2.getInt("t");
                    switch (i4) {
                        case 1:
                            intent.putExtra(string, jSONObject2.getBoolean("v"));
                            break;
                        case 2:
                            intent.putExtra(string, jSONObject2.getDouble("v"));
                            break;
                        case 3:
                            intent.putExtra(string, jSONObject2.getInt("v"));
                            break;
                        case 4:
                            intent.putExtra(string, jSONObject2.getLong("v"));
                            break;
                        case 5:
                            intent.putExtra(string, jSONObject2.getString("v"));
                            break;
                        default:
                            b.e("Warning: Unknown type in extras from open GMSG: " + string + " (type: " + i4 + ")");
                            break;
                    }
                }
            } catch (JSONException e3) {
                b.e("Warning: Could not parse extras from open GMSG: " + str7);
            }
        }
        if (intent.filterEquals(new Intent())) {
            a("Tried to launch empty intent.");
            return;
        }
        try {
            b.a("Launching an intent from AdActivity: " + intent);
            startActivity(intent);
            a(dVar);
        } catch (ActivityNotFoundException e4) {
            a(e4.getMessage(), e4);
        }
    }

    /* access modifiers changed from: protected */
    public void a(d dVar) {
        this.h = null;
        this.m = SystemClock.elapsedRealtime();
        this.p = true;
        synchronized (b) {
            if (c == null) {
                c = this;
                dVar.w();
            }
        }
    }

    /* access modifiers changed from: protected */
    public AdVideoView a(Activity activity) {
        return new AdVideoView(activity, this.h);
    }

    public void moveAdVideoView(int x, int y, int width, int height) {
        if (this.t != null) {
            this.t.setLayoutParams(a(x, y, width, height));
            this.t.requestLayout();
        }
    }

    public void newAdVideoView(int x, int y, int width, int height) {
        if (this.t == null) {
            this.t = a(this);
            this.n.addView(this.t, 0, a(x, y, width, height));
            synchronized (b) {
                if (d == null) {
                    b.e("currentAdManager was null while trying to get the opening AdWebView.");
                } else {
                    d.m().b(false);
                }
            }
        }
    }

    private RelativeLayout.LayoutParams a(int i2, int i3, int i4, int i5) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i4, i5);
        layoutParams.setMargins(i2, i3, 0, 0);
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        return layoutParams;
    }

    public void onClick(View v) {
        finish();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0088, code lost:
        r11.n = null;
        r11.p = false;
        r11.q = true;
        r11.t = null;
        r0 = getIntent().getBundleExtra("com.google.ads.AdOpener");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x009a, code lost:
        if (r0 != null) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009c, code lost:
        a("Could not get the Bundle used to create AdActivity.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00b0, code lost:
        r1 = new com.google.ads.internal.e(r0);
        r0 = r1.b();
        r10 = r1.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c3, code lost:
        if (r0.equals("intent") == false) goto L_0x00c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c5, code lost:
        a(r10, r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c9, code lost:
        r11.n = new android.widget.RelativeLayout(getApplicationContext());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00da, code lost:
        if (r0.equals("webapp") == false) goto L_0x017a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00dc, code lost:
        r11.h = new com.google.ads.internal.AdWebView(r8.i(), null);
        r1 = com.google.ads.internal.a.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e9, code lost:
        if (r9 != false) goto L_0x014b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00eb, code lost:
        r0 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ec, code lost:
        r0 = com.google.ads.internal.i.a(r8, r1, true, r0);
        r0.d(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00f3, code lost:
        if (r9 == false) goto L_0x00f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00f5, code lost:
        r0.a(true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00f8, code lost:
        r11.h.setWebViewClient(r0);
        r0 = r10.get("u");
        r1 = r10.get(com.google.ads.AdActivity.BASE_URL_PARAM);
        r2 = r10.get(com.google.ads.AdActivity.HTML_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0115, code lost:
        if (r0 == null) goto L_0x014d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0117, code lost:
        r11.h.loadUrl(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x011c, code lost:
        r0 = r10.get(com.google.ads.AdActivity.ORIENTATION_PARAM);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x012a, code lost:
        if ("p".equals(r0) == false) goto L_0x0160;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x012c, code lost:
        r3 = com.google.ads.util.AdUtil.b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0130, code lost:
        r1 = r11.h;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0132, code lost:
        if (r10 == null) goto L_0x0178;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0140, code lost:
        if ("1".equals(r10.get(com.google.ads.AdActivity.CUSTOM_CLOSE_PARAM)) == false) goto L_0x0178;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0142, code lost:
        r5 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0143, code lost:
        a(r1, false, r3, r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x014b, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x014d, code lost:
        if (r2 == null) goto L_0x0159;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x014f, code lost:
        r11.h.loadDataWithBaseURL(r1, r2, com.biznessapps.constants.AppConstants.TEXT_HTML, com.biznessapps.constants.AppConstants.UTF_8_CHARSET, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0159, code lost:
        a("Could not get the URL or HTML parameter to show a web app.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0166, code lost:
        if (com.biznessapps.constants.ReservationSystemConstants.USER_LAST_NAME.equals(r0) == false) goto L_0x016d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0168, code lost:
        r3 = com.google.ads.util.AdUtil.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x016f, code lost:
        if (r11 != com.google.ads.AdActivity.e) goto L_0x0176;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0171, code lost:
        r3 = r8.o();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0176, code lost:
        r3 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0178, code lost:
        r5 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0180, code lost:
        if (r0.equals("interstitial") != false) goto L_0x018a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0188, code lost:
        if (r0.equals("expand") == false) goto L_0x01d9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x018a, code lost:
        r11.h = r8.l();
        r3 = r8.o();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x019a, code lost:
        if (r0.equals("expand") == false) goto L_0x01d1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x019c, code lost:
        r11.h.setIsExpandedMraid(true);
        r11.q = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x01a3, code lost:
        if (r10 == null) goto L_0x01b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01b1, code lost:
        if ("1".equals(r10.get(com.google.ads.AdActivity.CUSTOM_CLOSE_PARAM)) == false) goto L_0x01b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01b3, code lost:
        r7 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01b6, code lost:
        if (r11.r == false) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01ba, code lost:
        if (r11.s != false) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x01bc, code lost:
        com.google.ads.util.b.a("Re-enabling hardware acceleration on expanding MRAID WebView.");
        r11.h.h();
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x01c7, code lost:
        a(r11.h, true, r3, r9, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x01d1, code lost:
        r5 = r11.h.j();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x01d9, code lost:
        a("Unknown AdOpener, <action: " + r0 + ">");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x01f7, code lost:
        r5 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r12) {
        /*
            r11 = this;
            r5 = 0
            r7 = 0
            r6 = 1
            super.onCreate(r12)
            r11.l = r7
            java.lang.Object r2 = com.google.ads.AdActivity.b
            monitor-enter(r2)
            com.google.ads.internal.d r0 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x00a2
            com.google.ads.internal.d r8 = com.google.ads.AdActivity.d     // Catch:{ all -> 0x00a9 }
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x001a
            com.google.ads.AdActivity.e = r11     // Catch:{ all -> 0x00a9 }
            r8.v()     // Catch:{ all -> 0x00a9 }
        L_0x001a:
            com.google.ads.AdActivity r0 = r11.o     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x0026
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.f     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x0026
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.f     // Catch:{ all -> 0x00a9 }
            r11.o = r0     // Catch:{ all -> 0x00a9 }
        L_0x0026:
            com.google.ads.AdActivity.f = r11     // Catch:{ all -> 0x00a9 }
            com.google.ads.n r0 = r8.i()     // Catch:{ all -> 0x00a9 }
            boolean r0 = r0.a()     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x0036
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x00a9 }
            if (r0 == r11) goto L_0x0046
        L_0x0036:
            com.google.ads.n r0 = r8.i()     // Catch:{ all -> 0x00a9 }
            boolean r0 = r0.b()     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x0049
            com.google.ads.AdActivity r0 = r11.o     // Catch:{ all -> 0x00a9 }
            com.google.ads.AdActivity r1 = com.google.ads.AdActivity.e     // Catch:{ all -> 0x00a9 }
            if (r0 != r1) goto L_0x0049
        L_0x0046:
            r8.x()     // Catch:{ all -> 0x00a9 }
        L_0x0049:
            boolean r9 = r8.r()     // Catch:{ all -> 0x00a9 }
            com.google.ads.n r0 = r8.i()     // Catch:{ all -> 0x00a9 }
            com.google.ads.util.i$b<com.google.ads.m> r0 = r0.d     // Catch:{ all -> 0x00a9 }
            java.lang.Object r0 = r0.a()     // Catch:{ all -> 0x00a9 }
            com.google.ads.m r0 = (com.google.ads.m) r0     // Catch:{ all -> 0x00a9 }
            com.google.ads.util.i$b<com.google.ads.m$a> r0 = r0.b     // Catch:{ all -> 0x00a9 }
            java.lang.Object r0 = r0.a()     // Catch:{ all -> 0x00a9 }
            com.google.ads.m$a r0 = (com.google.ads.m.a) r0     // Catch:{ all -> 0x00a9 }
            int r3 = com.google.ads.util.AdUtil.a     // Catch:{ all -> 0x00a9 }
            com.google.ads.util.i$c<java.lang.Integer> r1 = r0.b     // Catch:{ all -> 0x00a9 }
            java.lang.Object r1 = r1.a()     // Catch:{ all -> 0x00a9 }
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ all -> 0x00a9 }
            int r1 = r1.intValue()     // Catch:{ all -> 0x00a9 }
            if (r3 < r1) goto L_0x00ac
            r1 = r6
        L_0x0072:
            r11.s = r1     // Catch:{ all -> 0x00a9 }
            int r1 = com.google.ads.util.AdUtil.a     // Catch:{ all -> 0x00a9 }
            com.google.ads.util.i$c<java.lang.Integer> r0 = r0.d     // Catch:{ all -> 0x00a9 }
            java.lang.Object r0 = r0.a()     // Catch:{ all -> 0x00a9 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x00a9 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x00a9 }
            if (r1 < r0) goto L_0x00ae
            r0 = r6
        L_0x0085:
            r11.r = r0     // Catch:{ all -> 0x00a9 }
            monitor-exit(r2)     // Catch:{ all -> 0x00a9 }
            r11.n = r5
            r11.p = r7
            r11.q = r6
            r11.t = r5
            android.content.Intent r0 = r11.getIntent()
            java.lang.String r1 = "com.google.ads.AdOpener"
            android.os.Bundle r0 = r0.getBundleExtra(r1)
            if (r0 != 0) goto L_0x00b0
            java.lang.String r0 = "Could not get the Bundle used to create AdActivity."
            r11.a(r0)
        L_0x00a1:
            return
        L_0x00a2:
            java.lang.String r0 = "Could not get currentAdManager."
            r11.a(r0)     // Catch:{ all -> 0x00a9 }
            monitor-exit(r2)     // Catch:{ all -> 0x00a9 }
            goto L_0x00a1
        L_0x00a9:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x00a9 }
            throw r0
        L_0x00ac:
            r1 = r7
            goto L_0x0072
        L_0x00ae:
            r0 = r7
            goto L_0x0085
        L_0x00b0:
            com.google.ads.internal.e r1 = new com.google.ads.internal.e
            r1.<init>(r0)
            java.lang.String r0 = r1.b()
            java.util.HashMap r10 = r1.c()
            java.lang.String r1 = "intent"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00c9
            r11.a(r10, r8)
            goto L_0x00a1
        L_0x00c9:
            android.widget.RelativeLayout r1 = new android.widget.RelativeLayout
            android.content.Context r2 = r11.getApplicationContext()
            r1.<init>(r2)
            r11.n = r1
            java.lang.String r1 = "webapp"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x017a
            com.google.ads.internal.AdWebView r0 = new com.google.ads.internal.AdWebView
            com.google.ads.n r1 = r8.i()
            r0.<init>(r1, r5)
            r11.h = r0
            java.util.Map<java.lang.String, com.google.ads.o> r1 = com.google.ads.internal.a.d
            if (r9 != 0) goto L_0x014b
            r0 = r6
        L_0x00ec:
            com.google.ads.internal.i r0 = com.google.ads.internal.i.a(r8, r1, r6, r0)
            r0.d(r6)
            if (r9 == 0) goto L_0x00f8
            r0.a(r6)
        L_0x00f8:
            com.google.ads.internal.AdWebView r1 = r11.h
            r1.setWebViewClient(r0)
            java.lang.String r0 = "u"
            java.lang.Object r0 = r10.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "baseurl"
            java.lang.Object r1 = r10.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "html"
            java.lang.Object r2 = r10.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            if (r0 == 0) goto L_0x014d
            com.google.ads.internal.AdWebView r1 = r11.h
            r1.loadUrl(r0)
        L_0x011c:
            java.lang.String r0 = "o"
            java.lang.Object r0 = r10.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.String r1 = "p"
            boolean r1 = r1.equals(r0)
            if (r1 == 0) goto L_0x0160
            int r3 = com.google.ads.util.AdUtil.b()
        L_0x0130:
            com.google.ads.internal.AdWebView r1 = r11.h
            if (r10 == 0) goto L_0x0178
            java.lang.String r0 = "1"
            java.lang.String r2 = "custom_close"
            java.lang.Object r2 = r10.get(r2)
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0178
            r5 = r6
        L_0x0143:
            r0 = r11
            r2 = r7
            r4 = r9
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x00a1
        L_0x014b:
            r0 = r7
            goto L_0x00ec
        L_0x014d:
            if (r2 == 0) goto L_0x0159
            com.google.ads.internal.AdWebView r0 = r11.h
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)
            goto L_0x011c
        L_0x0159:
            java.lang.String r0 = "Could not get the URL or HTML parameter to show a web app."
            r11.a(r0)
            goto L_0x00a1
        L_0x0160:
            java.lang.String r1 = "l"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x016d
            int r3 = com.google.ads.util.AdUtil.a()
            goto L_0x0130
        L_0x016d:
            com.google.ads.AdActivity r0 = com.google.ads.AdActivity.e
            if (r11 != r0) goto L_0x0176
            int r3 = r8.o()
            goto L_0x0130
        L_0x0176:
            r3 = -1
            goto L_0x0130
        L_0x0178:
            r5 = r7
            goto L_0x0143
        L_0x017a:
            java.lang.String r1 = "interstitial"
            boolean r1 = r0.equals(r1)
            if (r1 != 0) goto L_0x018a
            java.lang.String r1 = "expand"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x01d9
        L_0x018a:
            com.google.ads.internal.AdWebView r1 = r8.l()
            r11.h = r1
            int r3 = r8.o()
            java.lang.String r1 = "expand"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01d1
            com.google.ads.internal.AdWebView r0 = r11.h
            r0.setIsExpandedMraid(r6)
            r11.q = r7
            if (r10 == 0) goto L_0x01b4
            java.lang.String r0 = "1"
            java.lang.String r1 = "custom_close"
            java.lang.Object r1 = r10.get(r1)
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x01b4
            r7 = r6
        L_0x01b4:
            boolean r0 = r11.r
            if (r0 == 0) goto L_0x01f7
            boolean r0 = r11.s
            if (r0 != 0) goto L_0x01f7
            java.lang.String r0 = "Re-enabling hardware acceleration on expanding MRAID WebView."
            com.google.ads.util.b.a(r0)
            com.google.ads.internal.AdWebView r0 = r11.h
            r0.h()
            r5 = r7
        L_0x01c7:
            com.google.ads.internal.AdWebView r1 = r11.h
            r0 = r11
            r2 = r6
            r4 = r9
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x00a1
        L_0x01d1:
            com.google.ads.internal.AdWebView r0 = r11.h
            boolean r7 = r0.j()
            r5 = r7
            goto L_0x01c7
        L_0x01d9:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown AdOpener, <action: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = ">"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r11.a(r0)
            goto L_0x00a1
        L_0x01f7:
            r5 = r7
            goto L_0x01c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ads.AdActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void a(AdWebView adWebView, boolean z, int i2, boolean z2, boolean z3) {
        requestWindowFeature(1);
        Window window = getWindow();
        window.setFlags(1024, 1024);
        if (AdUtil.a >= 11) {
            if (this.r) {
                b.a("Enabling hardware acceleration on the AdActivity window.");
                g.a(window);
            } else {
                b.a("Disabling hardware acceleration on the AdActivity WebView.");
                adWebView.g();
            }
        }
        ViewParent parent = adWebView.getParent();
        if (parent != null) {
            if (!z2) {
                a("Interstitial created with an AdWebView that has a parent.");
                return;
            } else if (parent instanceof ViewGroup) {
                this.k = (ViewGroup) parent;
                this.k.removeView(adWebView);
            } else {
                a("MRAID banner was not a child of a ViewGroup.");
                return;
            }
        }
        if (adWebView.i() != null) {
            a("Interstitial created with an AdWebView that is already in use by another AdActivity.");
            return;
        }
        setRequestedOrientation(i2);
        adWebView.setAdActivity(this);
        View a2 = a(z2 ? 50 : 32, z3);
        this.n.addView(adWebView, -1, -1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        if (z2) {
            layoutParams.addRule(10);
            layoutParams.addRule(11);
        } else {
            layoutParams.addRule(10);
            layoutParams.addRule(9);
        }
        this.n.addView(a2, layoutParams);
        this.n.setKeepScreenOn(true);
        setContentView(this.n);
        this.n.getRootView().setBackgroundColor(-16777216);
        if (z) {
            a.a(adWebView);
        }
    }

    public void onDestroy() {
        if (this.n != null) {
            this.n.removeAllViews();
        }
        if (isFinishing()) {
            e();
            if (this.q && this.h != null) {
                this.h.stopLoading();
                this.h.destroy();
                this.h = null;
            }
        }
        super.onDestroy();
    }

    public void onPause() {
        if (isFinishing()) {
            e();
        }
        super.onPause();
    }

    private void e() {
        if (!this.l) {
            if (this.h != null) {
                a.b(this.h);
                this.h.setAdActivity(null);
                this.h.setIsExpandedMraid(false);
                if (!(this.q || this.n == null || this.k == null)) {
                    if (this.r && !this.s) {
                        b.a("Disabling hardware acceleration on collapsing MRAID WebView.");
                        this.h.g();
                    } else if (!this.r && this.s) {
                        b.a("Re-enabling hardware acceleration on collapsing MRAID WebView.");
                        this.h.h();
                    }
                    this.n.removeView(this.h);
                    this.k.addView(this.h);
                }
            }
            if (this.t != null) {
                this.t.e();
                this.t = null;
            }
            if (this == c) {
                c = null;
            }
            f = this.o;
            synchronized (b) {
                if (!(d == null || !this.q || this.h == null)) {
                    if (this.h == d.l()) {
                        d.a();
                    }
                    this.h.stopLoading();
                }
                if (this == e) {
                    e = null;
                    if (d != null) {
                        d.u();
                        d = null;
                    } else {
                        b.e("currentAdManager is null while trying to destroy AdActivity.");
                    }
                }
            }
            this.l = true;
            b.a("AdActivity is closing.");
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (this.p && hasFocus && SystemClock.elapsedRealtime() - this.m > 250) {
            b.d("Launcher AdActivity got focus and is closing.");
            finish();
        }
        super.onWindowFocusChanged(hasFocus);
    }

    public void setCustomClose(boolean useCustomClose) {
        if (this.i != null) {
            this.i.removeAllViews();
            if (!useCustomClose) {
                ImageButton imageButton = new ImageButton(this);
                imageButton.setImageResource(17301527);
                imageButton.setBackgroundColor(0);
                imageButton.setOnClickListener(this);
                imageButton.setPadding(0, 0, 0, 0);
                this.i.addView(imageButton, new FrameLayout.LayoutParams(this.j, this.j, 17));
            }
        }
    }
}
