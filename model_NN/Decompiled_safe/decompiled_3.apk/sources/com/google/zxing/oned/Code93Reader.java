package com.google.zxing.oned;

import android.support.v4.view.MotionEventCompat;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.BitArray;
import java.util.Hashtable;
import twitter4j.internal.http.HttpResponseCode;

public final class Code93Reader extends OneDReader {
    private static final char[] ALPHABET = ALPHABET_STRING.toCharArray();
    private static final String ALPHABET_STRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*";
    private static final int ASTERISK_ENCODING = CHARACTER_ENCODINGS[47];
    private static final int[] CHARACTER_ENCODINGS = {276, 328, 324, 322, 296, 292, 290, 336, 274, 266, 424, HttpResponseCode.ENHANCE_YOUR_CLAIM, 418, HttpResponseCode.NOT_FOUND, 402, 394, 360, 356, 354, 308, 282, 344, 332, 326, HttpResponseCode.MULTIPLE_CHOICES, 278, 436, 434, 428, HttpResponseCode.UNPROCESSABLE_ENTITY, HttpResponseCode.NOT_ACCEPTABLE, 410, 364, 358, 310, 314, HttpResponseCode.FOUND, 468, 466, 458, 366, 374, 430, 294, 474, 470, 306, 350};

    private static void checkChecksums(StringBuffer stringBuffer) throws ChecksumException {
        int length = stringBuffer.length();
        checkOneChecksum(stringBuffer, length - 2, 20);
        checkOneChecksum(stringBuffer, length - 1, 15);
    }

    private static void checkOneChecksum(StringBuffer stringBuffer, int i, int i2) throws ChecksumException {
        int i3 = 1;
        int i4 = i - 1;
        int i5 = 0;
        while (i4 >= 0) {
            int indexOf = (ALPHABET_STRING.indexOf(stringBuffer.charAt(i4)) * i3) + i5;
            int i6 = i3 + 1;
            if (i6 > i2) {
                i6 = 1;
            }
            i4--;
            i3 = i6;
            i5 = indexOf;
        }
        if (stringBuffer.charAt(i) != ALPHABET[i5 % 47]) {
            throw ChecksumException.getChecksumInstance();
        }
    }

    private static String decodeExtended(StringBuffer stringBuffer) throws FormatException {
        int i;
        char c;
        int length = stringBuffer.length();
        StringBuffer stringBuffer2 = new StringBuffer(length);
        int i2 = 0;
        while (i2 < length) {
            char charAt = stringBuffer.charAt(i2);
            if (charAt < 'a' || charAt > 'd') {
                stringBuffer2.append(charAt);
                i = i2;
            } else {
                char charAt2 = stringBuffer.charAt(i2 + 1);
                switch (charAt) {
                    case 'a':
                        if (charAt2 >= 'A' && charAt2 <= 'Z') {
                            c = (char) (charAt2 - '@');
                            break;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                    case 'b':
                        if (charAt2 < 'A' || charAt2 > 'E') {
                            if (charAt2 >= 'F' && charAt2 <= 'W') {
                                c = (char) (charAt2 - 11);
                                break;
                            } else {
                                throw FormatException.getFormatInstance();
                            }
                        } else {
                            c = (char) (charAt2 - '&');
                            break;
                        }
                        break;
                    case 'c':
                        if (charAt2 >= 'A' && charAt2 <= 'O') {
                            c = (char) (charAt2 - ' ');
                            break;
                        } else if (charAt2 == 'Z') {
                            c = ':';
                            break;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                        break;
                    case 'd':
                        if (charAt2 >= 'A' && charAt2 <= 'Z') {
                            c = (char) (charAt2 + ' ');
                            break;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                    default:
                        c = 0;
                        break;
                }
                stringBuffer2.append(c);
                i = i2 + 1;
            }
            i2 = i + 1;
        }
        return stringBuffer2.toString();
    }

    private static int[] findAsteriskPattern(BitArray bitArray) throws NotFoundException {
        int size = bitArray.getSize();
        int i = 0;
        while (i < size && !bitArray.get(i)) {
            i++;
        }
        int[] iArr = new int[6];
        int length = iArr.length;
        boolean z = false;
        int i2 = 0;
        for (int i3 = i; i3 < size; i3++) {
            if (bitArray.get(i3) ^ z) {
                iArr[i2] = iArr[i2] + 1;
            } else {
                if (i2 != length - 1) {
                    i2++;
                } else if (toPattern(iArr) == ASTERISK_ENCODING) {
                    return new int[]{i, i3};
                } else {
                    i += iArr[0] + iArr[1];
                    for (int i4 = 2; i4 < length; i4++) {
                        iArr[i4 - 2] = iArr[i4];
                    }
                    iArr[length - 2] = 0;
                    iArr[length - 1] = 0;
                    i2--;
                }
                iArr[i2] = 1;
                z = !z;
            }
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static char patternToChar(int i) throws NotFoundException {
        for (int i2 = 0; i2 < CHARACTER_ENCODINGS.length; i2++) {
            if (CHARACTER_ENCODINGS[i2] == i) {
                return ALPHABET[i2];
            }
        }
        throw NotFoundException.getNotFoundInstance();
    }

    private static int toPattern(int[] iArr) {
        int length = iArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            i++;
            i2 = iArr[i] + i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4++) {
            int i5 = ((iArr[i4] << 8) * 9) / i2;
            int i6 = i5 >> 8;
            int i7 = (i5 & MotionEventCompat.ACTION_MASK) > 127 ? i6 + 1 : i6;
            if (i7 < 1 || i7 > 4) {
                return -1;
            }
            if ((i4 & 1) == 0) {
                int i8 = 0;
                while (i8 < i7) {
                    i8++;
                    i3 = (i3 << 1) | 1;
                }
            } else {
                i3 <<= i7;
            }
        }
        return i3;
    }

    public Result decodeRow(int i, BitArray bitArray, Hashtable hashtable) throws NotFoundException, ChecksumException, FormatException {
        int[] findAsteriskPattern = findAsteriskPattern(bitArray);
        int i2 = findAsteriskPattern[1];
        int size = bitArray.getSize();
        while (i2 < size && !bitArray.get(i2)) {
            i2++;
        }
        StringBuffer stringBuffer = new StringBuffer(20);
        int[] iArr = new int[6];
        while (true) {
            recordPattern(bitArray, i2, iArr);
            int pattern = toPattern(iArr);
            if (pattern < 0) {
                throw NotFoundException.getNotFoundInstance();
            }
            char patternToChar = patternToChar(pattern);
            stringBuffer.append(patternToChar);
            int i3 = i2;
            for (int i4 : iArr) {
                i3 += i4;
            }
            int i5 = i3;
            while (i5 < size && !bitArray.get(i5)) {
                i5++;
            }
            if (patternToChar == '*') {
                stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                if (i5 == size || !bitArray.get(i5)) {
                    throw NotFoundException.getNotFoundInstance();
                } else if (stringBuffer.length() < 2) {
                    throw NotFoundException.getNotFoundInstance();
                } else {
                    checkChecksums(stringBuffer);
                    stringBuffer.setLength(stringBuffer.length() - 2);
                    return new Result(decodeExtended(stringBuffer), null, new ResultPoint[]{new ResultPoint(((float) (findAsteriskPattern[0] + findAsteriskPattern[1])) / 2.0f, (float) i), new ResultPoint(((float) (i2 + i5)) / 2.0f, (float) i)}, BarcodeFormat.CODE_93);
                }
            } else {
                i2 = i5;
            }
        }
    }
}
