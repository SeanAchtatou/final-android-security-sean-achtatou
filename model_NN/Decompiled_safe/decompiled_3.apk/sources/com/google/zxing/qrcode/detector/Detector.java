package com.google.zxing.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.DetectorResult;
import com.google.zxing.common.GridSampler;
import com.google.zxing.common.PerspectiveTransform;
import com.google.zxing.qrcode.decoder.Version;
import java.util.Hashtable;

public class Detector {
    private final BitMatrix image;
    private ResultPointCallback resultPointCallback;

    public Detector(BitMatrix bitMatrix) {
        this.image = bitMatrix;
    }

    private float calculateModuleSizeOneWay(ResultPoint resultPoint, ResultPoint resultPoint2) {
        float sizeOfBlackWhiteBlackRunBothWays = sizeOfBlackWhiteBlackRunBothWays((int) resultPoint.getX(), (int) resultPoint.getY(), (int) resultPoint2.getX(), (int) resultPoint2.getY());
        float sizeOfBlackWhiteBlackRunBothWays2 = sizeOfBlackWhiteBlackRunBothWays((int) resultPoint2.getX(), (int) resultPoint2.getY(), (int) resultPoint.getX(), (int) resultPoint.getY());
        return Float.isNaN(sizeOfBlackWhiteBlackRunBothWays) ? sizeOfBlackWhiteBlackRunBothWays2 / 7.0f : Float.isNaN(sizeOfBlackWhiteBlackRunBothWays2) ? sizeOfBlackWhiteBlackRunBothWays / 7.0f : (sizeOfBlackWhiteBlackRunBothWays + sizeOfBlackWhiteBlackRunBothWays2) / 14.0f;
    }

    protected static int computeDimension(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3, float f) throws NotFoundException {
        int round = ((round(ResultPoint.distance(resultPoint, resultPoint2) / f) + round(ResultPoint.distance(resultPoint, resultPoint3) / f)) >> 1) + 7;
        switch (round & 3) {
            case 0:
                return round + 1;
            case 1:
            default:
                return round;
            case 2:
                return round - 1;
            case 3:
                throw NotFoundException.getNotFoundInstance();
        }
    }

    private static int round(float f) {
        return (int) (0.5f + f);
    }

    private static BitMatrix sampleGrid(BitMatrix bitMatrix, PerspectiveTransform perspectiveTransform, int i) throws NotFoundException {
        return GridSampler.getInstance().sampleGrid(bitMatrix, i, perspectiveTransform);
    }

    private float sizeOfBlackWhiteBlackRun(int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        boolean z = Math.abs(i4 - i2) > Math.abs(i3 - i);
        if (!z) {
            int i8 = i4;
            i4 = i3;
            i3 = i8;
            int i9 = i2;
            i2 = i;
            i = i9;
        }
        int abs = Math.abs(i4 - i2);
        int abs2 = Math.abs(i3 - i);
        int i10 = (-abs) >> 1;
        int i11 = i < i3 ? 1 : -1;
        int i12 = i2 < i4 ? 1 : -1;
        int i13 = 0;
        int i14 = i2;
        int i15 = i10;
        int i16 = i;
        while (i14 != i4) {
            int i17 = z ? i16 : i14;
            int i18 = z ? i14 : i16;
            if (i13 == 1) {
                if (this.image.get(i17, i18)) {
                    i5 = i13 + 1;
                }
                i5 = i13;
            } else {
                if (!this.image.get(i17, i18)) {
                    i5 = i13 + 1;
                }
                i5 = i13;
            }
            if (i5 == 3) {
                int i19 = i14 - i2;
                int i20 = i16 - i;
                int i21 = i12 < 0 ? i19 + 1 : i19;
                return (float) Math.sqrt((double) ((i21 * i21) + (i20 * i20)));
            }
            int i22 = i15 + abs2;
            if (i22 <= 0) {
                i6 = i16;
                i7 = i22;
            } else if (i16 == i3) {
                break;
            } else {
                i6 = i16 + i11;
                i7 = i22 - abs;
            }
            i14 += i12;
            i15 = i7;
            i16 = i6;
            i13 = i5;
        }
        int i23 = i4 - i2;
        int i24 = i3 - i;
        return (float) Math.sqrt((double) ((i23 * i23) + (i24 * i24)));
    }

    private float sizeOfBlackWhiteBlackRunBothWays(int i, int i2, int i3, int i4) {
        int i5;
        float f;
        float f2;
        int i6 = 0;
        float sizeOfBlackWhiteBlackRun = sizeOfBlackWhiteBlackRun(i, i2, i3, i4);
        int i7 = i - (i3 - i);
        if (i7 < 0) {
            f = ((float) i) / ((float) (i - i7));
            i5 = 0;
        } else if (i7 > this.image.getWidth()) {
            f = ((float) (this.image.getWidth() - i)) / ((float) (i7 - i));
            i5 = this.image.getWidth();
        } else {
            i5 = i7;
            f = 1.0f;
        }
        int i8 = (int) (((float) i2) - (f * ((float) (i4 - i2))));
        if (i8 < 0) {
            f2 = ((float) i2) / ((float) (i2 - i8));
        } else if (i8 > this.image.getHeight()) {
            f2 = ((float) (this.image.getHeight() - i2)) / ((float) (i8 - i2));
            i6 = this.image.getHeight();
        } else {
            i6 = i8;
            f2 = 1.0f;
        }
        return sizeOfBlackWhiteBlackRun(i, i2, (int) ((f2 * ((float) (i5 - i))) + ((float) i)), i6) + sizeOfBlackWhiteBlackRun;
    }

    /* access modifiers changed from: protected */
    public float calculateModuleSize(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3) {
        return (calculateModuleSizeOneWay(resultPoint, resultPoint2) + calculateModuleSizeOneWay(resultPoint, resultPoint3)) / 2.0f;
    }

    public PerspectiveTransform createTransform(ResultPoint resultPoint, ResultPoint resultPoint2, ResultPoint resultPoint3, ResultPoint resultPoint4, int i) {
        float x;
        float y;
        float f;
        float f2;
        float f3 = ((float) i) - 3.5f;
        if (resultPoint4 != null) {
            x = resultPoint4.getX();
            y = resultPoint4.getY();
            f = f3 - 3.0f;
            f2 = f;
        } else {
            x = (resultPoint2.getX() - resultPoint.getX()) + resultPoint3.getX();
            y = (resultPoint2.getY() - resultPoint.getY()) + resultPoint3.getY();
            f = f3;
            f2 = f3;
        }
        return PerspectiveTransform.quadrilateralToQuadrilateral(3.5f, 3.5f, f3, 3.5f, f2, f, 3.5f, f3, resultPoint.getX(), resultPoint.getY(), resultPoint2.getX(), resultPoint2.getY(), x, y, resultPoint3.getX(), resultPoint3.getY());
    }

    public DetectorResult detect() throws NotFoundException, FormatException {
        return detect(null);
    }

    public DetectorResult detect(Hashtable hashtable) throws NotFoundException, FormatException {
        this.resultPointCallback = hashtable == null ? null : (ResultPointCallback) hashtable.get(DecodeHintType.NEED_RESULT_POINT_CALLBACK);
        return processFinderPatternInfo(new FinderPatternFinder(this.image, this.resultPointCallback).find(hashtable));
    }

    /* access modifiers changed from: protected */
    public AlignmentPattern findAlignmentInRegion(float f, int i, int i2, float f2) throws NotFoundException {
        int i3 = (int) (f2 * f);
        int max = Math.max(0, i - i3);
        int min = Math.min(this.image.getWidth() - 1, i + i3);
        if (((float) (min - max)) < f * 3.0f) {
            throw NotFoundException.getNotFoundInstance();
        }
        int max2 = Math.max(0, i2 - i3);
        int min2 = Math.min(this.image.getHeight() - 1, i3 + i2);
        if (((float) (min2 - max2)) < f * 3.0f) {
            throw NotFoundException.getNotFoundInstance();
        }
        return new AlignmentPatternFinder(this.image, max, max2, min - max, min2 - max2, f, this.resultPointCallback).find();
    }

    /* access modifiers changed from: protected */
    public BitMatrix getImage() {
        return this.image;
    }

    /* access modifiers changed from: protected */
    public ResultPointCallback getResultPointCallback() {
        return this.resultPointCallback;
    }

    /* access modifiers changed from: protected */
    public DetectorResult processFinderPatternInfo(FinderPatternInfo finderPatternInfo) throws NotFoundException, FormatException {
        FinderPattern topLeft = finderPatternInfo.getTopLeft();
        FinderPattern topRight = finderPatternInfo.getTopRight();
        FinderPattern bottomLeft = finderPatternInfo.getBottomLeft();
        float calculateModuleSize = calculateModuleSize(topLeft, topRight, bottomLeft);
        if (calculateModuleSize < 1.0f) {
            throw NotFoundException.getNotFoundInstance();
        }
        int computeDimension = computeDimension(topLeft, topRight, bottomLeft, calculateModuleSize);
        Version provisionalVersionForDimension = Version.getProvisionalVersionForDimension(computeDimension);
        int dimensionForVersion = provisionalVersionForDimension.getDimensionForVersion() - 7;
        AlignmentPattern alignmentPattern = null;
        if (provisionalVersionForDimension.getAlignmentPatternCenters().length > 0) {
            float x = (topRight.getX() - topLeft.getX()) + bottomLeft.getX();
            float y = (topRight.getY() - topLeft.getY()) + bottomLeft.getY();
            float f = 1.0f - (3.0f / ((float) dimensionForVersion));
            int x2 = (int) (((x - topLeft.getX()) * f) + topLeft.getX());
            int y2 = (int) (topLeft.getY() + (f * (y - topLeft.getY())));
            int i = 4;
            while (i <= 16) {
                try {
                    alignmentPattern = findAlignmentInRegion(calculateModuleSize, x2, y2, (float) i);
                    break;
                } catch (NotFoundException e) {
                    i <<= 1;
                }
            }
        }
        return new DetectorResult(sampleGrid(this.image, createTransform(topLeft, topRight, bottomLeft, alignmentPattern, computeDimension), computeDimension), alignmentPattern == null ? new ResultPoint[]{bottomLeft, topLeft, topRight} : new ResultPoint[]{bottomLeft, topLeft, topRight, alignmentPattern});
    }
}
