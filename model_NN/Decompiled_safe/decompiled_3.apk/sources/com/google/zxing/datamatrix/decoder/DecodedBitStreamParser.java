package com.google.zxing.datamatrix.decoder;

import android.support.v4.view.MotionEventCompat;
import com.google.zxing.FormatException;
import com.google.zxing.common.BitSource;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

final class DecodedBitStreamParser {
    private static final int ANSIX12_ENCODE = 4;
    private static final int ASCII_ENCODE = 1;
    private static final int BASE256_ENCODE = 6;
    private static final char[] C40_BASIC_SET_CHARS = {'*', '*', '*', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    private static final int C40_ENCODE = 2;
    private static final char[] C40_SHIFT2_SET_CHARS = {'!', '\"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', ':', ';', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '_'};
    private static final int EDIFACT_ENCODE = 5;
    private static final int PAD_ENCODE = 0;
    private static final char[] TEXT_BASIC_SET_CHARS = {'*', '*', '*', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    private static final int TEXT_ENCODE = 3;
    private static final char[] TEXT_SHIFT3_SET_CHARS = {'\'', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '{', '|', '}', '~', 127};

    private DecodedBitStreamParser() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static com.google.zxing.common.DecoderResult decode(byte[] r8) throws com.google.zxing.FormatException {
        /*
            r1 = 0
            r3 = 1
            com.google.zxing.common.BitSource r4 = new com.google.zxing.common.BitSource
            r4.<init>(r8)
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r0 = 100
            r5.<init>(r0)
            java.lang.StringBuffer r6 = new java.lang.StringBuffer
            r0 = 0
            r6.<init>(r0)
            java.util.Vector r0 = new java.util.Vector
            r0.<init>(r3)
            r2 = r3
        L_0x001a:
            if (r2 != r3) goto L_0x0046
            int r2 = decodeAsciiSegment(r4, r5, r6)
        L_0x0020:
            if (r2 == 0) goto L_0x0028
            int r7 = r4.available()
            if (r7 > 0) goto L_0x001a
        L_0x0028:
            int r2 = r6.length()
            if (r2 <= 0) goto L_0x0035
            java.lang.String r2 = r6.toString()
            r5.append(r2)
        L_0x0035:
            com.google.zxing.common.DecoderResult r2 = new com.google.zxing.common.DecoderResult
            java.lang.String r3 = r5.toString()
            boolean r4 = r0.isEmpty()
            if (r4 == 0) goto L_0x0042
            r0 = r1
        L_0x0042:
            r2.<init>(r8, r3, r0, r1)
            return r2
        L_0x0046:
            switch(r2) {
                case 2: goto L_0x004e;
                case 3: goto L_0x0053;
                case 4: goto L_0x0057;
                case 5: goto L_0x005b;
                case 6: goto L_0x005f;
                default: goto L_0x0049;
            }
        L_0x0049:
            com.google.zxing.FormatException r0 = com.google.zxing.FormatException.getFormatInstance()
            throw r0
        L_0x004e:
            decodeC40Segment(r4, r5)
        L_0x0051:
            r2 = r3
            goto L_0x0020
        L_0x0053:
            decodeTextSegment(r4, r5)
            goto L_0x0051
        L_0x0057:
            decodeAnsiX12Segment(r4, r5)
            goto L_0x0051
        L_0x005b:
            decodeEdifactSegment(r4, r5)
            goto L_0x0051
        L_0x005f:
            decodeBase256Segment(r4, r5, r0)
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.datamatrix.decoder.DecodedBitStreamParser.decode(byte[]):com.google.zxing.common.DecoderResult");
    }

    private static void decodeAnsiX12Segment(BitSource bitSource, StringBuffer stringBuffer) throws FormatException {
        int readBits;
        int[] iArr = new int[3];
        while (bitSource.available() != 8 && (readBits = bitSource.readBits(8)) != 254) {
            parseTwoBytes(readBits, bitSource.readBits(8), iArr);
            for (int i = 0; i < 3; i++) {
                int i2 = iArr[i];
                if (i2 == 0) {
                    stringBuffer.append(13);
                } else if (i2 == 1) {
                    stringBuffer.append('*');
                } else if (i2 == 2) {
                    stringBuffer.append('>');
                } else if (i2 == 3) {
                    stringBuffer.append(' ');
                } else if (i2 < 14) {
                    stringBuffer.append((char) (i2 + 44));
                } else if (i2 < 40) {
                    stringBuffer.append((char) (i2 + 51));
                } else {
                    throw FormatException.getFormatInstance();
                }
            }
            if (bitSource.available() <= 0) {
                return;
            }
        }
    }

    private static int decodeAsciiSegment(BitSource bitSource, StringBuffer stringBuffer, StringBuffer stringBuffer2) throws FormatException {
        boolean z = false;
        do {
            int readBits = bitSource.readBits(8);
            if (readBits == 0) {
                throw FormatException.getFormatInstance();
            } else if (readBits <= 128) {
                stringBuffer.append((char) ((z ? readBits + 128 : readBits) - 1));
                return 1;
            } else if (readBits == 129) {
                return 0;
            } else {
                if (readBits <= 229) {
                    int i = readBits - 130;
                    if (i < 10) {
                        stringBuffer.append('0');
                    }
                    stringBuffer.append(i);
                } else if (readBits == 230) {
                    return 2;
                } else {
                    if (readBits == 231) {
                        return 6;
                    }
                    if (!(readBits == 232 || readBits == 233 || readBits == 234)) {
                        if (readBits == 235) {
                            z = true;
                        } else if (readBits == 236) {
                            stringBuffer.append("[)>\u001e05\u001d");
                            stringBuffer2.insert(0, "\u001e\u0004");
                        } else if (readBits == 237) {
                            stringBuffer.append("[)>\u001e06\u001d");
                            stringBuffer2.insert(0, "\u001e\u0004");
                        } else if (readBits == 238) {
                            return 4;
                        } else {
                            if (readBits == 239) {
                                return 3;
                            }
                            if (readBits == 240) {
                                return 5;
                            }
                            if (readBits != 241 && readBits >= 242) {
                                throw FormatException.getFormatInstance();
                            }
                        }
                    }
                }
            }
        } while (bitSource.available() > 0);
        return 1;
    }

    private static void decodeBase256Segment(BitSource bitSource, StringBuffer stringBuffer, Vector vector) throws FormatException {
        int readBits = bitSource.readBits(8);
        if (readBits == 0) {
            readBits = bitSource.available() / 8;
        } else if (readBits >= 250) {
            readBits = ((readBits - 249) * 250) + bitSource.readBits(8);
        }
        byte[] bArr = new byte[readBits];
        for (int i = 0; i < readBits; i++) {
            if (bitSource.available() < 8) {
                throw FormatException.getFormatInstance();
            }
            bArr[i] = unrandomize255State(bitSource.readBits(8), i);
        }
        vector.addElement(bArr);
        try {
            stringBuffer.append(new String(bArr, "ISO8859_1"));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(new StringBuffer().append("Platform does not support required encoding: ").append(e).toString());
        }
    }

    private static void decodeC40Segment(BitSource bitSource, StringBuffer stringBuffer) throws FormatException {
        int readBits;
        boolean z;
        int i;
        int[] iArr = new int[3];
        boolean z2 = false;
        while (bitSource.available() != 8 && (readBits = bitSource.readBits(8)) != 254) {
            parseTwoBytes(readBits, bitSource.readBits(8), iArr);
            int i2 = 0;
            int i3 = 0;
            while (i2 < 3) {
                int i4 = iArr[i2];
                switch (i3) {
                    case 0:
                        if (i4 >= 3) {
                            if (!z2) {
                                stringBuffer.append(C40_BASIC_SET_CHARS[i4]);
                                int i5 = i3;
                                z = z2;
                                i = i5;
                                break;
                            } else {
                                stringBuffer.append((char) (C40_BASIC_SET_CHARS[i4] + 128));
                                i = i3;
                                z = false;
                                break;
                            }
                        } else {
                            z = z2;
                            i = i4 + 1;
                            break;
                        }
                    case 1:
                        if (z2) {
                            stringBuffer.append((char) (i4 + 128));
                            z2 = false;
                        } else {
                            stringBuffer.append(i4);
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 2:
                        if (i4 < 27) {
                            if (z2) {
                                stringBuffer.append((char) (C40_SHIFT2_SET_CHARS[i4] + 128));
                                z2 = false;
                            } else {
                                stringBuffer.append(C40_SHIFT2_SET_CHARS[i4]);
                            }
                        } else if (i4 == 27) {
                            throw FormatException.getFormatInstance();
                        } else if (i4 == 30) {
                            z2 = true;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 3:
                        if (z2) {
                            stringBuffer.append((char) (i4 + 224));
                            z2 = false;
                        } else {
                            stringBuffer.append((char) (i4 + 96));
                        }
                        z = z2;
                        i = 0;
                        break;
                    default:
                        throw FormatException.getFormatInstance();
                }
                i2++;
                int i6 = i;
                z2 = z;
                i3 = i6;
            }
            if (bitSource.available() <= 0) {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void decodeEdifactSegment(com.google.zxing.common.BitSource r5, java.lang.StringBuffer r6) {
        /*
            r1 = 0
            r0 = r1
        L_0x0002:
            int r2 = r5.available()
            r3 = 16
            if (r2 > r3) goto L_0x000b
        L_0x000a:
            return
        L_0x000b:
            r4 = r1
        L_0x000c:
            r2 = 4
            if (r4 >= r2) goto L_0x002a
            r2 = 6
            int r2 = r5.readBits(r2)
            r3 = 11111(0x2b67, float:1.557E-41)
            if (r2 != r3) goto L_0x0035
            r0 = 1
            r3 = r0
        L_0x001a:
            if (r3 != 0) goto L_0x0025
            r0 = r2 & 32
            if (r0 != 0) goto L_0x0033
            r0 = r2 | 64
        L_0x0022:
            r6.append(r0)
        L_0x0025:
            int r0 = r4 + 1
            r4 = r0
            r0 = r3
            goto L_0x000c
        L_0x002a:
            if (r0 != 0) goto L_0x000a
            int r2 = r5.available()
            if (r2 > 0) goto L_0x0002
            goto L_0x000a
        L_0x0033:
            r0 = r2
            goto L_0x0022
        L_0x0035:
            r3 = r0
            goto L_0x001a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.datamatrix.decoder.DecodedBitStreamParser.decodeEdifactSegment(com.google.zxing.common.BitSource, java.lang.StringBuffer):void");
    }

    private static void decodeTextSegment(BitSource bitSource, StringBuffer stringBuffer) throws FormatException {
        int readBits;
        boolean z;
        int i;
        int[] iArr = new int[3];
        boolean z2 = false;
        while (bitSource.available() != 8 && (readBits = bitSource.readBits(8)) != 254) {
            parseTwoBytes(readBits, bitSource.readBits(8), iArr);
            int i2 = 0;
            int i3 = 0;
            while (i2 < 3) {
                int i4 = iArr[i2];
                switch (i3) {
                    case 0:
                        if (i4 >= 3) {
                            if (!z2) {
                                stringBuffer.append(TEXT_BASIC_SET_CHARS[i4]);
                                int i5 = i3;
                                z = z2;
                                i = i5;
                                break;
                            } else {
                                stringBuffer.append((char) (TEXT_BASIC_SET_CHARS[i4] + 128));
                                i = i3;
                                z = false;
                                break;
                            }
                        } else {
                            z = z2;
                            i = i4 + 1;
                            break;
                        }
                    case 1:
                        if (z2) {
                            stringBuffer.append((char) (i4 + 128));
                            z2 = false;
                        } else {
                            stringBuffer.append(i4);
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 2:
                        if (i4 < 27) {
                            if (z2) {
                                stringBuffer.append((char) (C40_SHIFT2_SET_CHARS[i4] + 128));
                                z2 = false;
                            } else {
                                stringBuffer.append(C40_SHIFT2_SET_CHARS[i4]);
                            }
                        } else if (i4 == 27) {
                            throw FormatException.getFormatInstance();
                        } else if (i4 == 30) {
                            z2 = true;
                        } else {
                            throw FormatException.getFormatInstance();
                        }
                        z = z2;
                        i = 0;
                        break;
                    case 3:
                        if (z2) {
                            stringBuffer.append((char) (TEXT_SHIFT3_SET_CHARS[i4] + 128));
                            z2 = false;
                        } else {
                            stringBuffer.append(TEXT_SHIFT3_SET_CHARS[i4]);
                        }
                        z = z2;
                        i = 0;
                        break;
                    default:
                        throw FormatException.getFormatInstance();
                }
                i2++;
                int i6 = i;
                z2 = z;
                i3 = i6;
            }
            if (bitSource.available() <= 0) {
                return;
            }
        }
    }

    private static void parseTwoBytes(int i, int i2, int[] iArr) {
        int i3 = ((i << 8) + i2) - 1;
        int i4 = i3 / 1600;
        iArr[0] = i4;
        int i5 = i3 - (i4 * 1600);
        int i6 = i5 / 40;
        iArr[1] = i6;
        iArr[2] = i5 - (i6 * 40);
    }

    private static byte unrandomize255State(int i, int i2) {
        int i3 = i - (((i2 * 149) % MotionEventCompat.ACTION_MASK) + 1);
        if (i3 < 0) {
            i3 += 256;
        }
        return (byte) i3;
    }
}
