package com.google.zxing.client.result;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import java.util.Hashtable;

final class ExpandedProductResultParser extends ResultParser {
    private ExpandedProductResultParser() {
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String findAIvalue(int r4, java.lang.String r5) {
        /*
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            r1.<init>()
            char r0 = r5.charAt(r4)
            r2 = 40
            if (r0 == r2) goto L_0x0010
            java.lang.String r0 = "ERROR"
        L_0x000f:
            return r0
        L_0x0010:
            int r0 = r4 + 1
            java.lang.String r2 = r5.substring(r0)
            r0 = 0
        L_0x0017:
            int r3 = r2.length()
            if (r0 >= r3) goto L_0x0032
            char r3 = r2.charAt(r0)
            switch(r3) {
                case 41: goto L_0x002d;
                case 42: goto L_0x0024;
                case 43: goto L_0x0024;
                case 44: goto L_0x0024;
                case 45: goto L_0x0024;
                case 46: goto L_0x0024;
                case 47: goto L_0x0024;
                case 48: goto L_0x0027;
                case 49: goto L_0x0027;
                case 50: goto L_0x0027;
                case 51: goto L_0x0027;
                case 52: goto L_0x0027;
                case 53: goto L_0x0027;
                case 54: goto L_0x0027;
                case 55: goto L_0x0027;
                case 56: goto L_0x0027;
                case 57: goto L_0x0027;
                default: goto L_0x0024;
            }
        L_0x0024:
            java.lang.String r0 = "ERROR"
            goto L_0x000f
        L_0x0027:
            r1.append(r3)
            int r0 = r0 + 1
            goto L_0x0017
        L_0x002d:
            java.lang.String r0 = r1.toString()
            goto L_0x000f
        L_0x0032:
            java.lang.String r0 = r1.toString()
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.client.result.ExpandedProductResultParser.findAIvalue(int, java.lang.String):java.lang.String");
    }

    private static String findValue(int i, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        String substring = str.substring(i);
        for (int i2 = 0; i2 < substring.length(); i2++) {
            char charAt = substring.charAt(i2);
            if (charAt == '(') {
                if (!"ERROR".equals(findAIvalue(i2, substring))) {
                    break;
                }
                stringBuffer.append('(');
            } else {
                stringBuffer.append(charAt);
            }
        }
        return stringBuffer.toString();
    }

    public static ExpandedProductParsedResult parse(Result result) {
        String text;
        if (!BarcodeFormat.RSS_EXPANDED.equals(result.getBarcodeFormat()) || (text = result.getText()) == null) {
            return null;
        }
        String str = "-";
        String str2 = "-";
        String str3 = "-";
        String str4 = "-";
        String str5 = "-";
        String str6 = "-";
        String str7 = "-";
        String str8 = "-";
        String str9 = "-";
        String str10 = "-";
        String str11 = "-";
        String str12 = "-";
        String str13 = "-";
        Hashtable hashtable = new Hashtable();
        int i = 0;
        while (i < text.length()) {
            String findAIvalue = findAIvalue(i, text);
            if ("ERROR".equals(findAIvalue)) {
                return null;
            }
            int length = findAIvalue.length() + 2 + i;
            String findValue = findValue(length, text);
            int length2 = length + findValue.length();
            if ("00".equals(findAIvalue)) {
                str2 = findValue;
            } else if ("01".equals(findAIvalue)) {
                str = findValue;
            } else if ("10".equals(findAIvalue)) {
                str3 = findValue;
            } else if ("11".equals(findAIvalue)) {
                str4 = findValue;
            } else if ("13".equals(findAIvalue)) {
                str5 = findValue;
            } else if ("15".equals(findAIvalue)) {
                str6 = findValue;
            } else if ("17".equals(findAIvalue)) {
                str7 = findValue;
            } else if ("3100".equals(findAIvalue) || "3101".equals(findAIvalue) || "3102".equals(findAIvalue) || "3103".equals(findAIvalue) || "3104".equals(findAIvalue) || "3105".equals(findAIvalue) || "3106".equals(findAIvalue) || "3107".equals(findAIvalue) || "3108".equals(findAIvalue) || "3109".equals(findAIvalue)) {
                str9 = ExpandedProductParsedResult.KILOGRAM;
                str10 = findAIvalue.substring(3);
                str8 = findValue;
            } else if ("3200".equals(findAIvalue) || "3201".equals(findAIvalue) || "3202".equals(findAIvalue) || "3203".equals(findAIvalue) || "3204".equals(findAIvalue) || "3205".equals(findAIvalue) || "3206".equals(findAIvalue) || "3207".equals(findAIvalue) || "3208".equals(findAIvalue) || "3209".equals(findAIvalue)) {
                str9 = ExpandedProductParsedResult.POUND;
                str10 = findAIvalue.substring(3);
                str8 = findValue;
            } else if ("3920".equals(findAIvalue) || "3921".equals(findAIvalue) || "3922".equals(findAIvalue) || "3923".equals(findAIvalue)) {
                str12 = findAIvalue.substring(3);
                str11 = findValue;
            } else if (!"3930".equals(findAIvalue) && !"3931".equals(findAIvalue) && !"3932".equals(findAIvalue) && !"3933".equals(findAIvalue)) {
                hashtable.put(findAIvalue, findValue);
            } else if (findValue.length() < 4) {
                return null;
            } else {
                str11 = findValue.substring(3);
                str13 = findValue.substring(0, 3);
                str12 = findAIvalue.substring(3);
            }
            i = length2;
        }
        return new ExpandedProductParsedResult(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, hashtable);
    }
}
