package com.google.zxing.client.result;

import com.biznessapps.constants.AppConstants;
import com.google.zxing.Result;
import java.util.Hashtable;

final class EmailAddressResultParser extends ResultParser {
    EmailAddressResultParser() {
    }

    public static EmailAddressParsedResult parse(Result result) {
        String str;
        String str2;
        String str3 = null;
        String text = result.getText();
        if (text == null) {
            return null;
        }
        if (text.startsWith(AppConstants.MAILTO_TYPE) || text.startsWith("MAILTO:")) {
            String substring = text.substring(7);
            int indexOf = substring.indexOf(63);
            if (indexOf >= 0) {
                substring = substring.substring(0, indexOf);
            }
            Hashtable parseNameValuePairs = parseNameValuePairs(text);
            if (parseNameValuePairs != null) {
                str = substring.length() == 0 ? (String) parseNameValuePairs.get("to") : substring;
                str2 = (String) parseNameValuePairs.get("subject");
                str3 = (String) parseNameValuePairs.get("body");
            } else {
                str = substring;
                str2 = null;
            }
            return new EmailAddressParsedResult(str, str2, str3, text);
        } else if (EmailDoCoMoResultParser.isBasicallyValidEmailAddress(text)) {
            return new EmailAddressParsedResult(text, null, null, new StringBuffer().append(AppConstants.MAILTO_TYPE).append(text).toString());
        } else {
            return null;
        }
    }
}
