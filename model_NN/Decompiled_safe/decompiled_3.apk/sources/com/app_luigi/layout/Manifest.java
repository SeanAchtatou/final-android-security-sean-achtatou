package com.app_luigi.layout;

public final class Manifest {

    public static final class permission {
        public static final String C2D_MESSAGE = "com.app_luigi.layout.permission.C2D_MESSAGE";
    }
}
