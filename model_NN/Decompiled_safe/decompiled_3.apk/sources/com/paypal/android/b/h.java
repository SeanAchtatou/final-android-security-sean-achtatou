package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.o;

public final class h extends LinearLayout {
    public h(String str, Context context) {
        super(context);
        setOrientation(1);
        setPadding(0, 0, 0, 0);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setGravity(80);
        LinearLayout a = d.a(context, -1, -2);
        a.setOrientation(0);
        a.setPadding(0, 0, 0, 0);
        a.setGravity(80);
        TextView a2 = o.a(o.a.HELVETICA_14_BOLD, context);
        a2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        a2.setPadding(5, 5, 0, 0);
        a2.setGravity(83);
        a2.setBackgroundColor(0);
        a2.setTextColor(-3637184);
        a2.setText(str);
        a.addView(a2);
        a.addView(e.a(context, "paypal_logo_22.png"));
        LinearLayout a3 = d.a(context, -2, -2);
        a3.setPadding(5, 5, 5, 5);
        a3.addView(e.a(context, "lock-icon.png"));
        a.addView(a3);
        addView(a);
        LinearLayout a4 = d.a(context, -1, -2);
        a4.setOrientation(0);
        a4.setPadding(5, 5, 5, 10);
        a4.setGravity(48);
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, 2));
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{-6042131, -14993820, -6042131});
        gradientDrawable.setCornerRadius(0.0f);
        gradientDrawable.setStroke(0, 0);
        gradientDrawable.setAlpha(128);
        imageView.setBackgroundDrawable(gradientDrawable);
        a4.addView(imageView);
        addView(a4);
    }
}
