package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.b.d;

public class c extends g implements View.OnClickListener, d.a {
    protected LinearLayout a;
    protected LinearLayout b;
    protected d c;

    public c(Context context) {
        super(context);
        setOnClickListener(this);
        setOnFocusChangeListener(this);
        this.a = new LinearLayout(context);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.a.setOrientation(0);
        this.a.setGravity(5);
        this.a.setPadding(5, 5, 5, 5);
        this.b = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 1;
        this.b.setLayoutParams(layoutParams);
        this.b.setPadding(5, 0, 5, 5);
        addView(this.a);
        addView(this.b);
        this.b.setVisibility(8);
        this.c = new d(context);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.c.a(this);
        this.c.setGravity(16);
        this.c.setVisibility(8);
        this.c.setClickable(false);
        this.c.setFocusable(false);
    }

    public final void a() {
        int i = 1;
        if (this.d == 1) {
            i = 0;
        }
        a(i);
    }

    public void a(int i) {
        super.a(i);
        if (i == 1) {
            this.b.setVisibility(0);
            this.c.a(1);
            return;
        }
        this.b.setVisibility(8);
        this.c.a(0);
    }

    public final void a(Drawable drawable) {
        if (this.c != null) {
            float pow = (float) Math.pow((double) PayPal.getInstance().getDensity(), 2.0d);
            this.c.a(1, drawable);
            this.c.setLayoutParams(new LinearLayout.LayoutParams((int) (((float) drawable.getMinimumWidth()) * pow), (int) (pow * ((float) drawable.getMinimumHeight()))));
        }
    }

    public final void a(boolean z) {
        if (z) {
            setClickable(true);
            setFocusable(true);
            this.c.setVisibility(0);
            return;
        }
        setClickable(false);
        setFocusable(false);
        this.c.setVisibility(8);
        this.b.setVisibility(8);
    }

    public final void b(Drawable drawable) {
        if (this.c != null) {
            float pow = (float) Math.pow((double) PayPal.getInstance().getDensity(), 2.0d);
            this.c.a(0, drawable);
            this.c.setLayoutParams(new LinearLayout.LayoutParams((int) (((float) drawable.getMinimumWidth()) * pow), (int) (pow * ((float) drawable.getMinimumHeight()))));
        }
    }

    public void onClick(View view) {
        this.c.onClick(this.c);
    }
}
