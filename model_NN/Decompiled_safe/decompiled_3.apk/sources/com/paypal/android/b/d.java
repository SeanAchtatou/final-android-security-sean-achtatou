package com.paypal.android.b;

import android.content.Context;
import android.view.View;

public final class d extends b implements View.OnClickListener {
    private a a;

    public interface a {
        void a();
    }

    public d(Context context) {
        super(context);
        setOnClickListener(this);
    }

    public final void a(a aVar) {
        this.a = aVar;
    }

    /* access modifiers changed from: protected */
    public final void drawableStateChanged() {
    }

    public final void onClick(View view) {
        if (view == this) {
            a(a() == 0 ? 1 : 0);
            if (this.a != null) {
                this.a.a();
            }
        }
    }
}
