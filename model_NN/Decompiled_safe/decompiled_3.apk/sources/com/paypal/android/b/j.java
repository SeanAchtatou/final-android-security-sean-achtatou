package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.a.e;

public abstract class j extends RelativeLayout {
    private ScrollView a;
    private LinearLayout b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(Context context) {
        super(context);
        int i = 640;
        int i2 = 800;
        int width = PayPalActivity.getInstance().getWindowManager().getDefaultDisplay().getWidth();
        int height = PayPalActivity.getInstance().getWindowManager().getDefaultDisplay().getHeight();
        if ((width <= height || height < 800) && (height <= width || width < 800)) {
            i2 = 640;
            i = 480;
        }
        if (width > height && height > i) {
            setPadding((width - i2) / 2, (height - i) / 2, (width - i2) / 2, (height - i) / 2);
        } else if (height <= width || width <= i) {
            setPadding(10, 10, 10, 10);
        } else {
            setPadding((width - i) / 2, (height - i2) / 2, (width - i) / 2, (height - i2) / 2);
        }
        setBackgroundColor(2130706432);
        a(context);
    }

    public abstract void a();

    /* access modifiers changed from: protected */
    public void a(Context context) {
        this.a = new ScrollView(context);
        this.a.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -987685, -987685, -987685});
        gradientDrawable.setCornerRadius(10.0f);
        gradientDrawable.setGradientRadius(10.0f);
        this.a.setBackgroundDrawable(gradientDrawable);
        this.a.setFillViewport(true);
        super.addView(this.a);
        this.b = new LinearLayout(context);
        this.b.setOrientation(1);
        this.b.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.b.setBackgroundColor(0);
        this.a.addView(this.b);
        if (PayPal.getInstance().getServer() == 0) {
            LinearLayout linearLayout = new LinearLayout(PayPalActivity.getInstance());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(12);
            layoutParams.addRule(11);
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.addView(e.a(context, "banner-sandbox.png"));
            super.addView(linearLayout);
        } else if (PayPal.getInstance().getServer() == 2) {
            LinearLayout linearLayout2 = new LinearLayout(PayPalActivity.getInstance());
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(12);
            layoutParams2.addRule(11);
            linearLayout2.setLayoutParams(layoutParams2);
            linearLayout2.addView(e.a(context, "banner-demo.png"));
            super.addView(linearLayout2);
        }
    }

    public void addView(View view) {
        this.b.addView(view);
    }

    public abstract void b();

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = 640;
        int i4 = 800;
        int i5 = 10;
        int size = View.MeasureSpec.getSize(i);
        int height = PayPalActivity.getInstance().getWindowManager().getDefaultDisplay().getHeight();
        boolean z = height - View.MeasureSpec.getSize(i2) > 100;
        if ((size <= height || height < 800) && (height <= size || size < 800)) {
            i4 = 640;
            i3 = 480;
        }
        if (size > height && height > i3) {
            int i6 = (size - i4) / 2;
            int i7 = (height - i3) / 2;
            int i8 = (size - i4) / 2;
            if (!z) {
                i5 = (height - i3) / 2;
            }
            setPadding(i6, i7, i8, i5);
        } else if (height <= size || size <= i3) {
            setPadding(10, 10, 10, 10);
        } else {
            int i9 = (size - i3) / 2;
            int i10 = (height - i4) / 2;
            int i11 = (size - i3) / 2;
            if (!z) {
                i5 = (height - i4) / 2;
            }
            setPadding(i9, i10, i11, i5);
        }
        super.onMeasure(i, i2);
    }
}
