package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.a.e;
import com.paypal.android.a.o;

public final class i extends LinearLayout {
    private Context a;
    private GradientDrawable b;
    private ImageView c;
    private TextView d;

    public enum a {
        RED_ALERT,
        YELLOW_ALERT,
        GREEN_ALERT,
        BLUE_ALERT
    }

    public i(Context context, a aVar) {
        super(context);
        this.a = context;
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setGravity(16);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        linearLayout.setPadding(5, 10, 5, 10);
        switch (aVar) {
            case RED_ALERT:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3172, -52});
                this.b.setStroke(1, -12529);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = e.a(this.a, "system-icon-error.png");
                this.d = o.a(o.a.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-65536);
                break;
            case YELLOW_ALERT:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3172, -52});
                this.b.setStroke(1, -12529);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = e.a(this.a, "system-icon-alert.png");
                this.d = o.a(o.a.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-65536);
                break;
            case GREEN_ALERT:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3219987, -1510918});
                this.b.setStroke(1, -8280890);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = e.a(this.a, "system-icon-confirmation.png");
                this.d = o.a(o.a.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-13408768);
                break;
            case BLUE_ALERT:
                this.b = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3219987, -1510918});
                this.b.setStroke(1, -8280890);
                this.b.setCornerRadius(3.0f);
                setBackgroundDrawable(this.b);
                this.c = e.a(this.a, "system-icon-notification.png");
                this.d = o.a(o.a.HELVETICA_12_NORMAL, context);
                this.d.setTextColor(-13408615);
                break;
        }
        linearLayout.addView(this.c);
        addView(linearLayout);
        addView(this.d);
    }

    public final void a(String str) {
        this.d.setText(str);
    }
}
