package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.StateSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class b extends Button implements View.OnFocusChangeListener {
    private int a = 0;
    private Drawable[] b;

    public b(Context context) {
        super(context);
        setOnFocusChangeListener(this);
        setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
    }

    public final int a() {
        return this.a;
    }

    public final void a(int i) {
        Drawable drawable;
        if (this.a != 2) {
            if (i < 0 || i >= 3) {
                throw new IllegalArgumentException("State " + i + " is outside the acceptable range 0-" + 2);
            }
            this.a = i;
            if (this.b != null && (drawable = this.b[this.a]) != null) {
                setBackgroundColor(0);
                setBackgroundDrawable(drawable);
            }
        }
    }

    public final void a(int i, Drawable drawable) {
        if (i < 0 || i >= 3) {
            throw new IllegalArgumentException("State " + i + " is outside the acceptable range 0-" + 2);
        }
        if (this.b == null) {
            this.b = new Drawable[3];
        }
        this.b[i] = drawable;
        if (i == this.a && drawable != null) {
            setBackgroundDrawable(drawable);
        }
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        if (!StateSet.stateSetMatches(new int[]{16842919}, drawableState)) {
            if (!StateSet.stateSetMatches(new int[]{16842908}, drawableState)) {
                a(0);
                return;
            }
        }
        a(1);
    }

    public void onFocusChange(View view, boolean z) {
    }
}
