package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.LinearLayout;

public class k extends c {
    private LinearLayout e;

    public k(Context context) {
        super(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 1;
        this.b.setLayoutParams(layoutParams);
        this.b.setOrientation(1);
        this.b.setGravity(17);
        this.b.setPadding(10, 0, 10, 0);
        this.e = new LinearLayout(context);
        this.e.setLayoutParams(layoutParams);
        this.e.setOrientation(1);
        this.e.setGravity(17);
        this.e.setPadding(5, 0, 5, 5);
        this.b.addView(this.e);
        this.c.setVisibility(0);
    }

    public final void a(View view) {
        this.e.addView(view);
    }

    public void c() {
    }

    public final void c(Drawable drawable) {
        this.e.setBackgroundDrawable(drawable);
    }

    public final void d() {
        this.e.removeAllViews();
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.a.setBackgroundDrawable(drawable);
    }
}
