package com.paypal.android.b;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;

public final class e extends LinearLayout {
    private AnimationDrawable a;

    public e(Context context) {
        super(context);
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        setPadding(10, 10, 10, 10);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(52, 29));
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        AnimationDrawable animationDrawable = new AnimationDrawable();
        animationDrawable.addFrame(com.paypal.android.a.e.a(41031, 5362), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(176309, 5447), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(111874, 5446), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(72182, 5475), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(20096, 5400), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(154674, 5270), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(98231, 5423), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(57867, 5485), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(7623, 5470), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(133008, 5428), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(124672, 5415), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(83060, 5367), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(33070, 5362), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(169015, 5399), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(105998, 5412), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(66279, 5470), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(14681, 5415), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(139270, 5357), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(92086, 5303), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(52342, 5525), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(46393, 5524), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(0, 5487), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(119216, 5456), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(77657, 5403), 50);
        animationDrawable.addFrame(com.paypal.android.a.e.a(27818, 5252), 50);
        animationDrawable.setOneShot(false);
        animationDrawable.setVisible(true, true);
        this.a = animationDrawable;
        imageView.setBackgroundDrawable(this.a);
        linearLayout.addView(imageView);
        addView(linearLayout);
    }

    public final void a() {
        this.a.stop();
        this.a.start();
    }

    public final void b() {
        this.a.stop();
    }
}
