package com.paypal.android.MEP.a;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a;
import com.paypal.android.MEP.a.d;
import com.paypal.android.MEP.b.b;
import com.paypal.android.a.d;
import com.paypal.android.a.h;
import com.paypal.android.a.o;
import com.paypal.android.b.g;
import com.paypal.android.b.i;
import com.paypal.android.b.j;
import java.util.Hashtable;

public final class e extends j implements TextWatcher, View.OnClickListener, a.b, g.a {
    public static String a = null;
    /* access modifiers changed from: private */
    public static com.paypal.android.b.e l = null;
    private a b;
    private String c;
    private LinearLayout d;
    private RelativeLayout e;
    private b f;
    private EditText g;
    private EditText h;
    private EditText i;
    private Button j;
    private Button k;
    private TextView m;
    private Hashtable<String, Object> n;
    private i o;
    private LinearLayout p;
    private Context q;

    public enum a {
        STATE_NORMAL,
        STATE_ERROR,
        STATE_PIN_SUCCESS
    }

    public e(Context context) {
        super(context);
    }

    private void a(String str, boolean z) {
        LinearLayout a2 = d.a(this.q, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.getLayoutParams());
        layoutParams.addRule(13);
        a2.setLayoutParams(layoutParams);
        a2.setOrientation(1);
        a2.setGravity(1);
        if (l == null) {
            l = new com.paypal.android.b.e(this.q);
        } else {
            ((LinearLayout) l.getParent()).removeAllViews();
        }
        this.m = o.a(o.a.HELVETICA_16_NORMAL, this.q);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(str);
        if (z) {
            LinearLayout a3 = d.a(this.q, -2, -2);
            a3.setOrientation(1);
            a3.setPadding(5, 10, 5, 10);
            i iVar = new i(this.q, i.a.GREEN_ALERT);
            iVar.a(h.a("ANDROID_pin_success"));
            iVar.setPadding(5, 5, 5, 5);
            a3.addView(iVar);
            a2.addView(a3);
        }
        a2.addView(l);
        a2.addView(this.m);
        this.e.removeAllViews();
        this.e.addView(a2);
    }

    private boolean d() {
        if (this.g.getText().toString().length() < 9) {
            return false;
        }
        return h.e(this.g.getText().toString());
    }

    private boolean e() {
        String obj = this.h.getText().toString();
        String obj2 = this.i.getText().toString();
        if (obj.length() < 4 || obj.length() > 8 || obj2.length() < 4 || obj2.length() > 8 || !obj.equals(obj2)) {
            return false;
        }
        return h.f(obj);
    }

    private void f() {
        try {
            ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(this.g.getWindowToken(), 0);
        } catch (Exception e2) {
        }
        try {
            ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(this.h.getWindowToken(), 0);
        } catch (Exception e3) {
        }
        try {
            ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(this.i.getWindowToken(), 0);
        } catch (Exception e4) {
        }
    }

    public final void a() {
    }

    public final void a(int i2, Object obj) {
        this.b = a.STATE_PIN_SUCCESS;
        d.AnonymousClass1.b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.a.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.paypal.android.MEP.a.e.a(int, java.lang.Object):void
      com.paypal.android.MEP.a.e.a(com.paypal.android.b.g, int):void
      com.paypal.android.MEP.a.e.a(java.lang.String, java.lang.Object):void
      com.paypal.android.MEP.a.b.a(int, java.lang.Object):void
      com.paypal.android.MEP.a.b.a(java.lang.String, java.lang.Object):void
      com.paypal.android.b.g.a.a(com.paypal.android.b.g, int):void
      com.paypal.android.MEP.a.e.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.b.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.g.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.b.a(boolean, boolean):void */
    public final void a(Context context) {
        super.a(context);
        this.q = context;
        this.b = a.STATE_NORMAL;
        LinearLayout a2 = com.paypal.android.a.d.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(o.b(o.a.HELVETICA_16_BOLD, context));
        this.f = new b(context, this);
        this.f.a(this);
        a2.addView(this.f);
        addView(a2);
        this.d = com.paypal.android.a.d.a(context, -1, -1);
        this.d.setOrientation(1);
        this.d.setPadding(10, 5, 10, 5);
        this.d.setBackgroundDrawable(com.paypal.android.a.d.a());
        this.d.addView(new com.paypal.android.b.h(h.a("ANDROID_payment_made"), context));
        this.p = com.paypal.android.a.d.a(context, -1, -2);
        this.p.setOrientation(1);
        this.p.setPadding(5, 10, 5, 10);
        this.o = new i(context, i.a.YELLOW_ALERT);
        this.o.a("This page is currently being used to test components.");
        this.o.setPadding(0, 5, 0, 5);
        this.p.setVisibility(8);
        this.p.addView(this.o);
        this.d.addView(this.p);
        LinearLayout a3 = com.paypal.android.a.d.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 0, 5, 5);
        TextView a4 = o.a(o.a.HELVETICA_16_BOLD, context);
        a4.setTextColor(-14993820);
        a4.setText(h.a("ANDROID_create_a_pin"));
        a4.setGravity(3);
        a3.addView(a4);
        this.d.addView(a3);
        LinearLayout a5 = com.paypal.android.a.d.a(context, -1, -2);
        a5.setOrientation(1);
        a5.setPadding(10, 10, 10, 5);
        a5.setBackgroundDrawable(com.paypal.android.a.d.a());
        this.g = new EditText(context);
        this.g.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.g.setInputType(3);
        this.g.setHint(h.a("ANDROID_enter_mobile"));
        this.g.setSingleLine(true);
        this.g.addTextChangedListener(this);
        a5.addView(this.g);
        this.h = new EditText(context);
        this.h.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.h.setInputType(3);
        this.h.setHint(h.a("ANDROID_enter_pin"));
        this.h.setSingleLine(true);
        this.h.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.h.addTextChangedListener(this);
        a5.addView(this.h);
        this.i = new EditText(context);
        this.i.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.i.setInputType(3);
        this.i.setHint(h.a("ANDROID_reenter_pin"));
        this.i.setSingleLine(true);
        this.i.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.i.addTextChangedListener(this);
        a5.addView(this.i);
        this.d.addView(a5);
        LinearLayout a6 = com.paypal.android.a.d.a(context, -1, -2);
        a6.setGravity(1);
        a6.setOrientation(1);
        a6.setPadding(5, 10, 5, 5);
        this.j = new Button(context);
        this.j.setLayoutParams(new RelativeLayout.LayoutParams(-1, com.paypal.android.a.d.b()));
        this.j.setText(h.a("ANDROID_create_pin"));
        this.j.setTextColor(-16777216);
        this.j.setBackgroundDrawable(com.paypal.android.a.e.a());
        this.j.setOnClickListener(this);
        this.j.setEnabled(false);
        a6.addView(this.j);
        this.d.addView(a6);
        LinearLayout a7 = com.paypal.android.a.d.a(context, -1, -2);
        a7.setGravity(1);
        a7.setOrientation(1);
        a7.setPadding(5, 5, 5, 10);
        this.k = new Button(context);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, com.paypal.android.a.d.b()));
        this.k.setText(h.a("ANDROID_skip"));
        this.k.setTextColor(-16777216);
        this.k.setBackgroundDrawable(com.paypal.android.a.e.b());
        this.k.setOnClickListener(this);
        a7.addView(this.k);
        this.d.addView(a7);
        addView(this.d);
        this.e = new RelativeLayout(context);
        this.e.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.e.setBackgroundDrawable(com.paypal.android.a.d.a());
        a(h.a("ANDROID_creating_pin"), false);
        this.e.setVisibility(8);
        addView(this.e);
        this.n = new Hashtable<>();
        if (!PayPal.getInstance().canShowCart()) {
            this.f.a(false, false);
        }
    }

    public final void a(g gVar, int i2) {
    }

    public final void a(String str, Object obj) {
        this.n.put(str, obj);
    }

    public final void afterTextChanged(Editable editable) {
        if (!d() || !e()) {
            this.j.setEnabled(false);
        } else {
            this.j.setEnabled(true);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.a.e.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.paypal.android.MEP.a.e.a(int, java.lang.Object):void
      com.paypal.android.MEP.a.e.a(com.paypal.android.b.g, int):void
      com.paypal.android.MEP.a.e.a(java.lang.String, java.lang.Object):void
      com.paypal.android.MEP.a.b.a(int, java.lang.Object):void
      com.paypal.android.MEP.a.b.a(java.lang.String, java.lang.Object):void
      com.paypal.android.b.g.a.a(com.paypal.android.b.g, int):void
      com.paypal.android.MEP.a.e.a(java.lang.String, boolean):void */
    public final void b() {
        if (this.b == a.STATE_ERROR) {
            this.d.setVisibility(0);
            this.e.setVisibility(8);
            this.o.a(this.c);
            this.p.setVisibility(0);
        } else if (this.b == a.STATE_PIN_SUCCESS) {
            l.b();
            a(h.a("ANDROID_returning_to_merchant"), true);
            l.a();
            new Thread(new Runnable(this) {
                public final void run() {
                    long currentTimeMillis = System.currentTimeMillis();
                    float f = 0.0f;
                    while (f < 3.0f) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                        }
                        long currentTimeMillis2 = System.currentTimeMillis();
                        f = (((float) (currentTimeMillis2 - currentTimeMillis)) / 1000.0f) + f;
                        currentTimeMillis = currentTimeMillis2;
                    }
                    e.l.b();
                    PayPalActivity.getInstance().paymentSucceeded((String) com.paypal.android.a.b.e().c("PayKey"), (String) com.paypal.android.a.b.e().c("PaymentExecStatus"), true);
                }
            }).start();
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final void d(String str) {
        l.b();
        this.c = str;
        this.b = a.STATE_ERROR;
        this.h.setText("");
        this.i.setText("");
        d.AnonymousClass1.b();
    }

    public final void l() {
        com.paypal.android.a.b.e().a("NewPhone", this.n.get("mobileNumber"));
        com.paypal.android.a.b.e().a("NewPin", this.n.get("newPIN"));
        com.paypal.android.a.b.e().a("delegate", this);
        com.paypal.android.a.b.e().a(11);
    }

    public final void onClick(View view) {
        if (view == this.j) {
            if (d() && e()) {
                f();
                if (PayPal.getInstance().getServer() != 2) {
                    com.paypal.android.MEP.a.a().b(this, this.g.getText().toString(), this.h.getText().toString());
                }
                this.d.setVisibility(8);
                this.e.setVisibility(0);
                l.a();
                if (PayPal.getInstance().getServer() == 2) {
                    this.b = a.STATE_PIN_SUCCESS;
                    d.AnonymousClass1.b();
                }
            }
        } else if (view == this.k) {
            f();
            if (a == null || a.length() == 0) {
                a = "11111111";
            }
            PayPalActivity.getInstance().paymentSucceeded((String) com.paypal.android.a.b.e().c("PayKey"), (String) com.paypal.android.a.b.e().c("PaymentExecStatus"));
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }
}
