package com.paypal.android.MEP;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.a.d;
import com.paypal.android.a.b;
import com.paypal.android.a.e;
import com.paypal.android.a.h;

public final class CheckoutButton extends LinearLayout implements View.OnClickListener {
    public static final int TEXT_DONATE = 1;
    public static final int TEXT_PAY = 0;
    private static boolean p = false;
    private int a;
    private int b;
    private int c;
    private int d;
    private boolean e;
    private LinearLayout f;
    private GradientDrawable g;
    private TextView h;
    private TextView i;
    private LinearLayout j;
    private Context k;
    private StateListDrawable l;
    private TextView m;
    private TextView n;
    private ImageView o;

    public CheckoutButton(Context context) {
        super(context);
        this.k = context;
    }

    /* access modifiers changed from: protected */
    public final void a(int i2, int i3) {
        int i4;
        float f2;
        int i5;
        float f3;
        this.a = i2;
        switch (this.a) {
            case 0:
                this.b = 152;
                this.c = 33;
                this.d = 18;
                i4 = 22;
                f2 = 6.0f;
                break;
            case 1:
            default:
                this.b = 194;
                this.c = 37;
                this.d = 20;
                i4 = 22;
                f2 = 6.0f;
                break;
            case 2:
                this.b = 278;
                this.c = 43;
                this.d = 22;
                i4 = 30;
                f2 = 10.0f;
                break;
            case 3:
                this.b = 294;
                this.c = 45;
                this.d = 28;
                i4 = 40;
                f2 = 10.0f;
                break;
        }
        Typeface create = Typeface.create("Helvetica", 0);
        float f4 = (2.5f * ((float) this.a)) + 10.0f;
        float f5 = ((float) this.d) - 8.0f;
        float density = PayPal.getInstance().getDensity();
        this.b = (int) (((float) this.b) * density);
        this.c = (int) (((float) this.c) * density);
        this.d = (int) (density * ((float) this.d));
        setOrientation(1);
        setPadding(0, 0, 0, 0);
        this.g = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -1});
        this.g.setSize(this.b, this.c + this.d);
        this.g.setCornerRadius(f2);
        this.g.setStroke(1, -5789785);
        this.f = new LinearLayout(this.k);
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-1, this.d));
        this.f.setOrientation(0);
        this.f.setPadding(0, 2, 0, 0);
        this.h = new TextView(this.k);
        this.h.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.h.setGravity(3);
        this.h.setPadding(5, 0, 0, 0);
        this.h.setTypeface(create, 1);
        this.h.setTextSize(f5);
        this.h.setTextColor(-15066598);
        this.h.setSingleLine(true);
        this.h.setText("");
        this.h.setOnClickListener(this);
        this.i = new TextView(this.k);
        this.i.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        this.i.setGravity(5);
        this.i.setPadding(0, 0, 5, 0);
        this.i.setTypeface(create);
        this.i.setTextSize(f5);
        this.i.setTextColor(-14993820);
        this.i.setFocusable(true);
        SpannableString spannableString = new SpannableString(h.a("ANDROID_not_you"));
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        this.i.setText(spannableString);
        this.i.setOnClickListener(this);
        this.f.addView(this.h);
        this.f.addView(this.i);
        addView(this.f);
        this.j = new LinearLayout(this.k);
        this.j.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.j.setGravity(17);
        this.j.setOrientation(0);
        this.j.setPadding(2, 2, 2, 2);
        this.j.setOnClickListener(this);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-4922, -22016});
        gradientDrawable.setSize(this.b, this.c);
        gradientDrawable.setCornerRadius(f2);
        gradientDrawable.setStroke(1, -3637191);
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-12951, -1937101});
        gradientDrawable2.setSize(this.b, this.c);
        gradientDrawable2.setCornerRadius(f2);
        gradientDrawable2.setStroke(1, -3637191);
        this.l = new StateListDrawable();
        this.l.addState(new int[]{-16842919}, gradientDrawable);
        this.l.addState(new int[]{16842919}, gradientDrawable2);
        this.j.setBackgroundDrawable(this.l);
        String str = i3 == 1 ? "donate" : "pay";
        String lowerCase = PayPal.getInstance().getLanguage().toLowerCase();
        String substring = lowerCase.substring(0, 2);
        String[] split = h.a("ANDROID_" + str + "_button").split("%PP", -1);
        for (int i6 = 0; i6 < split.length; i6++) {
            int indexOf = split[0].indexOf("\\n");
            if (indexOf != -1) {
                split[i6] = split[i6].substring(0, indexOf) + 10 + split[i6].substring(indexOf + 2);
            }
        }
        if (substring.equals("pl") || (substring.equals("fr") && str.equals("donate"))) {
            f3 = f4 - (2.0f + (0.5f * ((float) this.a)));
            i5 = 3;
        } else if (substring.equals("zh") || substring.equals("jp")) {
            f3 = 2.0f + (0.5f * ((float) this.a)) + f4;
            i5 = 1;
        } else {
            f3 = f4;
            i5 = 3;
        }
        this.m = new TextView(this.k);
        if (!split[0].equals("")) {
            this.m.setText(split[0]);
            this.m.setTypeface(create, i5);
            this.m.setTextSize(f3);
            this.m.setTextColor(-14993820);
            this.m.setGravity(17);
            this.m.setVisibility(0);
        } else {
            this.m.setVisibility(8);
        }
        this.n = new TextView(this.k);
        if (split.length <= 1 || split[1].equals("")) {
            this.n.setVisibility(8);
        } else {
            this.n.setText(split[1]);
            this.n.setTypeface(create, i5);
            this.n.setTextSize(f3);
            this.n.setTextColor(-14993820);
            if (split[0].equals("")) {
                this.n.setGravity(17);
            }
            this.n.setVisibility(0);
        }
        String str2 = "paypal_";
        if (lowerCase.equals("zh_hk")) {
            str2 = str2 + "cn_";
        }
        this.o = e.a(this.k, str2 + "logo_" + i4 + ".png");
        this.o.setVisibility(0);
        this.j.addView(this.m);
        this.j.addView(this.o);
        this.j.addView(this.n);
        addView(this.j);
        updateButton();
        setSelected(false);
    }

    public final void onClick(View view) {
        if (!p && view != this.h) {
            if (view == this.i) {
                PayPal.logd("CheckoutButton", "reset the account");
                PayPal.getInstance().resetAccount();
                b.e().a(12);
                d.a = true;
            } else if (view == this.j) {
                d.a = false;
            }
            p = true;
            performClick();
            setActive(false);
        }
    }

    public final void setActive(boolean z) {
        setClickable(z);
        setFocusable(z);
    }

    public final void updateButton() {
        boolean z = true;
        PayPal instance = PayPal.getInstance();
        p = false;
        updateName();
        if (!instance.getIsRememberMe() || instance.getAuthSetting() != 1) {
            z = false;
        }
        this.e = z;
        if (!this.e || this.h.getText().toString().length() <= 0) {
            this.f.setVisibility(8);
            setMinimumWidth(this.b);
            setMinimumHeight(this.c);
            setBackgroundColor(0);
        } else {
            this.f.setVisibility(0);
            setMinimumWidth(this.b);
            setMinimumHeight(this.c + this.d);
            setBackgroundDrawable(this.g);
        }
        invalidate();
    }

    public final void updateName() {
        String accountName = PayPal.getInstance().getAccountName();
        if (accountName.length() > 21) {
            accountName = accountName.substring(0, 21) + "...";
        }
        this.h.setText(accountName);
    }
}
