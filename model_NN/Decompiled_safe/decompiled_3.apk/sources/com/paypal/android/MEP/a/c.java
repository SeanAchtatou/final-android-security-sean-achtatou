package com.paypal.android.MEP.a;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.b.b;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.h;
import com.paypal.android.a.o;
import com.paypal.android.b.g;
import com.paypal.android.b.i;
import com.paypal.android.b.j;

public final class c extends j implements View.OnClickListener, g.a {
    String a = "";
    String b = "";
    private Intent c = null;
    private i d;
    private b e;

    public c(Context context) {
        super(context);
    }

    public c(Context context, Intent intent) {
        super(context);
        this.c = intent;
        try {
            this.a = this.c.getStringExtra("FATAL_ERROR_ID");
            this.b = this.c.getStringExtra("FATAL_ERROR_MESSAGE");
            this.d.a(this.b);
        } catch (Exception e2) {
            this.a = "10001";
            this.b = h.a("ANDROID_10001");
        }
    }

    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Context context) {
        super.a(context);
        LinearLayout a2 = d.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(o.b(o.a.HELVETICA_16_BOLD, context));
        this.e = new b(context, this);
        this.e.a(this);
        a2.addView(this.e);
        addView(a2);
        LinearLayout a3 = d.a(context, -1, -1);
        a3.setBackgroundDrawable(d.a());
        a3.setPadding(10, 5, 10, 5);
        a3.setOrientation(1);
        a3.addView(new com.paypal.android.b.h(h.a("ANDROID_error_heading"), context));
        LinearLayout a4 = d.a(context, -1, -2);
        a4.setOrientation(1);
        a4.setPadding(5, 10, 5, 10);
        this.d = new i(context, i.a.RED_ALERT);
        this.d.a(h.a("ANDROID_10001"));
        this.d.setPadding(0, 5, 0, 5);
        this.d.setVisibility(0);
        a4.addView(this.d);
        a3.addView(a4);
        Button button = new Button(context);
        button.setText(h.a("ANDROID_ok"));
        button.setLayoutParams(new RelativeLayout.LayoutParams(-1, d.b()));
        button.setGravity(1);
        button.setBackgroundDrawable(e.a());
        button.setTextColor(-16777216);
        button.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                PayPalActivity.getInstance().paymentFailed((String) com.paypal.android.a.b.e().c("CorrelationId"), (String) com.paypal.android.a.b.e().c("PayKey"), c.this.a, c.this.b, true, false);
            }
        });
        a3.addView(button);
        addView(a3);
    }

    public final void a(g gVar, int i) {
    }

    public final void b() {
    }

    public final void onClick(View view) {
    }
}
