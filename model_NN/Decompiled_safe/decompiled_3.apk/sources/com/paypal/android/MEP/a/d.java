package com.paypal.android.MEP.a;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.biznessapps.constants.AppConstants;
import com.facebook.AppEventsConstants;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalPreapproval;
import com.paypal.android.MEP.a;
import com.paypal.android.MEP.b.f;
import com.paypal.android.a.a.c;
import com.paypal.android.a.a.k;
import com.paypal.android.a.h;
import com.paypal.android.a.o;
import com.paypal.android.b.e;
import com.paypal.android.b.g;
import com.paypal.android.b.i;
import com.paypal.android.b.j;
import java.util.Hashtable;
import java.util.Vector;

public final class d extends j implements TextWatcher, View.OnClickListener, a.b, g.a {
    public static boolean a = false;
    private static e l = null;
    private a b;
    private Button c;
    private Button d;
    private Button e;
    private TextView f;
    private com.paypal.android.MEP.b.b g;
    private i h;
    private i i;
    private LinearLayout j;
    private RelativeLayout k;
    private TextView m;
    private com.paypal.android.MEP.b.e n;
    private com.paypal.android.b.a o;
    /* access modifiers changed from: private */
    public WebView p;
    private String q;
    private Hashtable<String, Object> r = new Hashtable<>();

    public enum a {
        STATE_NORMAL,
        STATE_LOGGING_IN,
        STATE_ERROR,
        STATE_LOGGING_OUT
    }

    private class b extends WebViewClient {
        /* synthetic */ b(d dVar) {
            this((byte) 0);
        }

        private b(byte b) {
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!str.equals("About.Quick.Pay")) {
                return true;
            }
            d.this.onClick(d.this.p);
            return true;
        }
    }

    public d(Context context) {
        super(context);
    }

    private void d() {
        boolean z = true;
        String a2 = this.n.a();
        String b2 = this.n.b();
        boolean z2 = h.d(a2) || h.e(a2);
        if (b2 == null || b2.length() <= 0) {
            z = false;
        }
        if (z2 && z) {
            this.n.d().setText("");
            a(a.STATE_LOGGING_IN);
            com.paypal.android.MEP.a.a().a(this, a2, b2);
        }
    }

    private boolean e() {
        String a2 = this.n.a();
        String b2 = this.n.b();
        return (h.d(a2) || h.e(a2)) && (b2 != null && b2.length() > 0);
    }

    public final void a() {
        b();
    }

    public final void a(int i2, Object obj) {
        com.paypal.android.a.b e2 = com.paypal.android.a.b.e();
        if (this.b == a.STATE_LOGGING_IN) {
            switch (i2) {
                case 0:
                case 10:
                    e2.a("currentUser", this.n.a());
                    if (PayPal.getInstance().getPayType() == 3) {
                        e2.j();
                        return;
                    } else {
                        e2.i();
                        return;
                    }
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.b.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.g.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.b.a(boolean, boolean):void */
    /* access modifiers changed from: protected */
    public final void a(Context context) {
        PayPal instance = PayPal.getInstance();
        super.a(context);
        this.b = a.STATE_NORMAL;
        LinearLayout a2 = com.paypal.android.a.d.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(o.b(o.a.HELVETICA_16_BOLD, context));
        this.g = new com.paypal.android.MEP.b.b(context, this);
        this.g.a(this);
        a2.addView(this.g);
        if (instance.getPayType() == 3) {
            this.g.a(false, true);
        } else if (!instance.canShowCart()) {
            this.g.a(false, false);
        }
        addView(a2);
        this.j = new LinearLayout(context);
        this.j.setOrientation(1);
        this.j.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.j.setBackgroundDrawable(com.paypal.android.a.d.a());
        this.j.setPadding(10, 5, 10, 5);
        this.j.addView(new com.paypal.android.b.h(h.a("ANDROID_login"), context));
        this.h = new i(context, i.a.RED_ALERT);
        this.h.a("Placeholder");
        this.h.setVisibility(8);
        this.h.setPadding(0, 5, 0, 5);
        this.j.addView(this.h);
        this.i = new i(context, i.a.BLUE_ALERT);
        this.i.a(h.a("ANDROID_not_you_message"));
        this.i.setVisibility(8);
        this.i.setPadding(0, 5, 0, 5);
        this.j.addView(this.i);
        this.n = new com.paypal.android.MEP.b.e(context);
        this.n.c().addTextChangedListener(this);
        this.n.d().addTextChangedListener(this);
        this.j.addView(this.n);
        LinearLayout a3 = com.paypal.android.a.d.a(context, -1, -2);
        a3.setOrientation(0);
        a3.setGravity(16);
        a3.setPadding(5, 5, 5, 0);
        this.o = new com.paypal.android.b.a(context);
        this.o.setChecked(instance.getIsRememberMe());
        this.o.setOnClickListener(this);
        if (instance.getAuthSetting() == 1) {
            a3.addView(this.o);
        }
        this.p = new WebView(context);
        this.p.setWebViewClient(new b(this));
        this.p.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.p.setBackgroundColor(0);
        this.p.loadData("<html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><head><style type=\"text/css\">b {color:#1B3664; font-family:Helvetica; font-size:12;}a {color:#686868; font-family:Helvetica; font-size:12;}</style></head><body><b>" + h.a("ANDROID_checkbox_opt_in") + "</b>" + "  " + "<a href=\"About.Quick.Pay\">" + h.a("ANDROID_checkbox_whats_this") + AppConstants.LINK_END_TAG + "</body>" + "</html>", AppConstants.TEXT_HTML, AppConstants.UTF_8_CHARSET);
        if (instance.getAuthSetting() == 1) {
            a3.addView(this.p);
        }
        this.j.addView(a3);
        LinearLayout a4 = com.paypal.android.a.d.a(context, -1, -2);
        a4.setPadding(5, 5, 5, 5);
        a4.setOrientation(0);
        a4.setGravity(1);
        if (instance.getPayType() == 3 || instance.getShippingEnabled() || instance.isPersonalPayment()) {
            this.d = null;
            this.e = new Button(context);
            this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b()));
            this.e.setId(184424834);
            this.e.setText(h.a("ANDROID_login"));
            this.e.setTextColor(-16777216);
            this.e.setBackgroundDrawable(com.paypal.android.a.e.a());
            this.e.setOnClickListener(this);
            this.e.setEnabled(false);
            a4.addView(this.e);
        } else {
            LinearLayout a5 = com.paypal.android.a.d.a(context, -1, -2);
            a5.setOrientation(1);
            a5.setPadding(5, 0, 5, 5);
            TextView a6 = o.a(o.a.HELVETICA_10_NORMAL, context);
            a6.setTextColor(-16777216);
            a6.setText(h.a("ANDROID_review_text"));
            a6.setGravity(3);
            a5.addView(a6);
            this.j.addView(a5);
            this.d = new Button(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f);
            layoutParams.setMargins(0, 0, 5, 0);
            this.d.setLayoutParams(layoutParams);
            if (instance.getTextType() == 1) {
                this.d.setText(h.a("ANDROID_donate"));
            } else {
                this.d.setText(h.a("ANDROID_pay"));
            }
            this.d.setTextColor(-16777216);
            this.d.setBackgroundDrawable(com.paypal.android.a.e.a());
            this.d.setOnClickListener(this);
            this.d.setEnabled(false);
            a4.addView(this.d);
            this.e = new Button(context);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f);
            layoutParams2.setMargins(5, 0, 0, 0);
            this.e.setLayoutParams(layoutParams2);
            this.e.setId(184424834);
            this.e.setText(h.a("ANDROID_review"));
            this.e.setTextColor(-16777216);
            this.e.setBackgroundDrawable(com.paypal.android.a.e.b());
            this.e.setOnClickListener(this);
            this.e.setEnabled(false);
            a4.addView(this.e);
        }
        this.j.addView(a4);
        LinearLayout a7 = com.paypal.android.a.d.a(context, -1, -2);
        a7.setPadding(5, 5, 5, 5);
        a7.setOrientation(0);
        a7.setGravity(1);
        this.c = new Button(context);
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b()));
        this.c.setText(h.a("ANDROID_cancel"));
        this.c.setTextColor(-16777216);
        this.c.setBackgroundDrawable(com.paypal.android.a.e.b());
        this.c.setOnClickListener(this);
        a7.addView(this.c);
        this.j.addView(a7);
        this.f = o.a(o.a.HELVETICA_12_NORMAL, context);
        this.f.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.f.setPadding(10, 10, 10, 10);
        this.f.setTextColor(-16776961);
        this.f.setGravity(17);
        this.f.setOnClickListener(this);
        SpannableString spannableString = new SpannableString(h.a("ANDROID_help"));
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        this.f.setText(spannableString);
        this.f.setFocusable(true);
        this.j.addView(this.f);
        this.j.invalidate();
        addView(this.j);
        this.k = new RelativeLayout(context);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.k.setBackgroundDrawable(com.paypal.android.a.d.a());
        LinearLayout a8 = com.paypal.android.a.d.a(context, -1, -2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(a8.getLayoutParams());
        layoutParams3.addRule(13);
        a8.setLayoutParams(layoutParams3);
        a8.setOrientation(1);
        a8.setGravity(1);
        if (l == null) {
            l = new e(context);
        } else {
            ((LinearLayout) l.getParent()).removeAllViews();
        }
        this.m = o.a(o.a.HELVETICA_16_NORMAL, context);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(h.a("ANDROID_logging_in_message"));
        a8.addView(l);
        a8.addView(this.m);
        this.k.addView(a8);
        this.k.setVisibility(8);
        addView(this.k);
        if (instance.getIsRememberMe()) {
            a(a.STATE_LOGGING_IN);
            this.n.d().setText("");
            com.paypal.android.a.b.e().a("delegate", this);
            com.paypal.android.a.b.e().a("quickPay", "false");
            com.paypal.android.a.b.e().a(10);
        }
        if (a) {
            a(a.STATE_ERROR);
        }
    }

    public final void a(a aVar) {
        this.b = aVar;
        AnonymousClass1.b();
    }

    public final void a(g gVar, int i2) {
    }

    public final void a(String str, Object obj) {
        this.r.put(str, obj);
    }

    public final void afterTextChanged(Editable editable) {
        if (this.n.c().getText().length() <= 0 || this.n.d().getText().length() <= 0) {
            if (this.e != null) {
                this.e.setEnabled(false);
            }
            if (this.d != null) {
                this.d.setEnabled(false);
            }
        } else {
            if (this.e != null) {
                this.e.setEnabled(true);
            }
            if (this.d != null) {
                this.d.setEnabled(true);
            }
        }
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.b.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.g.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.b.a(boolean, boolean):void */
    public final void b() {
        if (this.b == a.STATE_LOGGING_IN || this.b == a.STATE_LOGGING_OUT) {
            this.g.a(false, true);
            this.j.setVisibility(8);
            this.k.setVisibility(0);
            l.a();
        } else if (this.b == a.STATE_NORMAL || this.b == a.STATE_ERROR) {
            if (PayPal.getInstance().canShowCart()) {
                this.g.a(true, false);
            }
            l.b();
            this.k.setVisibility(8);
            this.j.setVisibility(0);
            if (this.b != a.STATE_ERROR) {
                return;
            }
            if (a) {
                this.i.setVisibility(0);
                this.h.setVisibility(8);
                return;
            }
            this.i.setVisibility(8);
            this.h.setVisibility(0);
            this.h.a(this.q);
        }
    }

    public final void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    public final a c() {
        return this.b;
    }

    public final void d(String str) {
        a = false;
        this.q = str;
        if (this.b == a.STATE_LOGGING_IN) {
            a(a.STATE_ERROR);
        } else if (this.b == a.STATE_LOGGING_OUT) {
            a(a.STATE_ERROR);
        }
    }

    public final void l() {
        com.paypal.android.a.b.e().a("usernameOrPhone", this.r.get("usernameOrPhone"));
        com.paypal.android.a.b.e().a("passwordOrPin", this.r.get("passwordOrPin"));
        com.paypal.android.a.b.e().a("delegate", this);
        com.paypal.android.a.b.e().a(0);
    }

    public final void onClick(View view) {
        com.paypal.android.a.b e2 = com.paypal.android.a.b.e();
        PayPal instance = PayPal.getInstance();
        PayPalActivity instance2 = PayPalActivity.getInstance();
        if (view == this.o) {
            instance.setIsRememberMe(this.o.isChecked());
        } else if (view == this.p) {
            AnonymousClass1.a(1);
        } else if (view == this.d) {
            if (e()) {
                a(a.STATE_NORMAL);
                this.n.e();
                if (instance.getServer() == 2) {
                    instance2.paymentSucceeded("27892", (String) e2.c("PaymentExecStatus"), true);
                    return;
                }
                e2.a("quickPay", "true");
                d();
            }
        } else if (view == this.e) {
            if (e()) {
                a(a.STATE_NORMAL);
                this.n.e();
                if (instance.getServer() == 2) {
                    if (instance.getPayType() == 3) {
                        PayPalPreapproval preapproval = instance.getPreapproval();
                        preapproval.setStartDate("2011-07-06T23:59:49.000-07:00");
                        preapproval.setEndDate("2011-08-07T23:59:49.000-07:00");
                        preapproval.setPinRequired(true);
                    }
                    a.a = new Hashtable<>();
                    Vector vector = new Vector();
                    Vector vector2 = new Vector();
                    c cVar = new c();
                    cVar.a(AppEventsConstants.EVENT_PARAM_VALUE_NO);
                    cVar.a = new com.paypal.android.a.a.a();
                    cVar.a.b(AppConstants.DEFAULT_CURRENCY);
                    cVar.a.a("2.00");
                    cVar.d = new Vector<>();
                    k kVar = new k();
                    kVar.a = new com.paypal.android.a.a.a();
                    kVar.a.b(AppConstants.DEFAULT_CURRENCY);
                    kVar.a.a("2.00");
                    kVar.b = new com.paypal.android.a.a.g();
                    kVar.b.a("2093");
                    kVar.b.b("BANK_INSTANT");
                    cVar.d.add(kVar);
                    vector.add(cVar);
                    c cVar2 = new c();
                    cVar2.a("1");
                    cVar2.a = new com.paypal.android.a.a.a();
                    cVar2.a.b(AppConstants.DEFAULT_CURRENCY);
                    cVar2.a.a("2.00");
                    cVar2.d = new Vector<>();
                    k kVar2 = new k();
                    kVar2.a = new com.paypal.android.a.a.a();
                    kVar2.a.b(AppConstants.DEFAULT_CURRENCY);
                    kVar2.a.a("2.00");
                    kVar2.b = new com.paypal.android.a.a.g();
                    kVar2.b.a("9853");
                    kVar2.b.b("CREDITCARD");
                    cVar2.d.add(kVar2);
                    vector.add(cVar2);
                    c cVar3 = new c();
                    cVar3.a(AppConstants.TWITTER_USER_TYPE);
                    cVar3.a = new com.paypal.android.a.a.a();
                    cVar3.a.b(AppConstants.DEFAULT_CURRENCY);
                    cVar3.a.a("2.00");
                    cVar3.d = new Vector<>();
                    k kVar3 = new k();
                    kVar3.a = new com.paypal.android.a.a.a();
                    kVar3.a.b(AppConstants.DEFAULT_CURRENCY);
                    kVar3.a.a("2.00");
                    kVar3.b = new com.paypal.android.a.a.g();
                    kVar3.b.a("9691");
                    kVar3.b.b("CREDITCARD");
                    cVar3.d.add(kVar3);
                    vector.add(cVar3);
                    a.a.put("FundingPlanId", AppEventsConstants.EVENT_PARAM_VALUE_NO);
                    a.a.put("FundingPlans", vector);
                    com.paypal.android.a.a.h hVar = new com.paypal.android.a.a.h();
                    hVar.a("Trenton");
                    hVar.c("123 Home St");
                    hVar.d("Apt B");
                    hVar.e("08601");
                    hVar.f("NJ");
                    hVar.g("1");
                    hVar.b("US");
                    vector2.add(hVar);
                    com.paypal.android.a.a.h hVar2 = new com.paypal.android.a.a.h();
                    hVar2.a("Hamlin");
                    hVar2.c("3012 Church Rd");
                    hVar2.d("");
                    hVar2.e("14464");
                    hVar2.f("NY");
                    hVar2.g(AppConstants.TWITTER_USER_TYPE);
                    hVar2.b("US");
                    vector2.add(hVar2);
                    a.a.put("ShippingAddressId", "1");
                    a.a.put("AvailableAddresses", vector2);
                    PayPalActivity.getInstance().sendBroadcast(new Intent(PayPalActivity.LOGIN_SUCCESS));
                    return;
                }
                e2.a("quickPay", "false");
                d();
            }
        } else if (view == this.c) {
            new f(instance2).show();
        } else if (view == this.f) {
            AnonymousClass1.a(2);
        }
    }

    public final void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }
}
