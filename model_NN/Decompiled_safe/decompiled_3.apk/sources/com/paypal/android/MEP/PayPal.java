package com.paypal.android.MEP;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.biznessapps.constants.ReservationSystemConstants;
import com.paypal.android.a.g;
import com.paypal.android.a.h;
import com.paypal.android.a.m;
import java.io.Serializable;
import java.util.Locale;
import junit.framework.Assert;

public final class PayPal {
    public static final int AUTH_SETTING_DISABLED = 0;
    public static final int AUTH_SETTING_ENABLED = 1;
    public static final int BUTTON_152x33 = 0;
    public static final int BUTTON_194x37 = 1;
    public static final int BUTTON_278x43 = 2;
    public static final int BUTTON_294x45 = 3;
    public static final int ENV_LIVE = 1;
    public static final int ENV_NONE = 2;
    public static final int ENV_SANDBOX = 0;
    public static final int FEEPAYER_EACHRECEIVER = 0;
    public static final int FEEPAYER_PRIMARYRECEIVER = 2;
    public static final int FEEPAYER_SECONDARYONLY = 3;
    public static final int FEEPAYER_SENDER = 1;
    public static final int LOGIN_VIA_DRT = 2;
    public static final int LOGIN_VIA_EMAIL = 0;
    public static final int LOGIN_VIA_EMAIL_EBAY_USER = 3;
    public static final int LOGIN_VIA_PHONE = 1;
    public static final int NUM_STYLES = 4;
    public static final int PAYMENT_SUBTYPE_AFFILIATE = 0;
    public static final int PAYMENT_SUBTYPE_B2B = 1;
    public static final int PAYMENT_SUBTYPE_CHILDCARE = 15;
    public static final int PAYMENT_SUBTYPE_CONTRACTORS = 17;
    public static final int PAYMENT_SUBTYPE_DONATIONS = 6;
    public static final int PAYMENT_SUBTYPE_ENTERTAINMENT = 18;
    public static final int PAYMENT_SUBTYPE_EVENTS = 16;
    public static final int PAYMENT_SUBTYPE_GOVERNMENT = 9;
    public static final int PAYMENT_SUBTYPE_INSURANCE = 10;
    public static final int PAYMENT_SUBTYPE_INVOICE = 20;
    public static final int PAYMENT_SUBTYPE_MEDICAL = 14;
    public static final int PAYMENT_SUBTYPE_MORTGAGE = 13;
    public static final int PAYMENT_SUBTYPE_NONE = 22;
    public static final int PAYMENT_SUBTYPE_PAYROLL = 2;
    public static final int PAYMENT_SUBTYPE_REBATES = 3;
    public static final int PAYMENT_SUBTYPE_REFUNDS = 4;
    public static final int PAYMENT_SUBTYPE_REIMBURSEMENTS = 5;
    public static final int PAYMENT_SUBTYPE_REIMBUSEMENTS = 5;
    public static final int PAYMENT_SUBTYPE_REMITTANCES = 11;
    public static final int PAYMENT_SUBTYPE_RENT = 12;
    public static final int PAYMENT_SUBTYPE_TOURISM = 19;
    public static final int PAYMENT_SUBTYPE_TRANSFER = 21;
    public static final int PAYMENT_SUBTYPE_TUITION = 8;
    public static final int PAYMENT_SUBTYPE_UTILITIES = 7;
    public static final int PAYMENT_TYPE_GOODS = 0;
    public static final int PAYMENT_TYPE_NONE = 3;
    public static final int PAYMENT_TYPE_PERSONAL = 2;
    public static final int PAYMENT_TYPE_SERVICE = 1;
    public static final int PAY_TYPE_CHAINED = 2;
    public static final int PAY_TYPE_PARALLEL = 1;
    public static final int PAY_TYPE_PREAPPROVAL = 3;
    public static final int PAY_TYPE_SIMPLE = 0;
    private static final String[] a = {"GOODS", "SERVICE", "PERSONAL", "NONE"};
    private static final String[] b = {"AFFILIATE_PAYMENTS", "B2B", "PAYROLL", "REBATES", "REFUNDS", "REIMBURSEMENTS", "DONATIONS", "UTILITIES", "TUITION", "GOVERNMENT", "INSURANCE", "REMITTANCES", "RENT", "MORTGAGE", "MEDICAL", "CHILD_CARE", "EVENT_PLANNING", "GENERAL_CONTRACTORS", "ENTERTAINMENT", "TOURISM", "INVOICE", "TRANSFER", "NONE"};
    private static com.paypal.android.c.a f = null;
    private a c = new a(this);
    private final b d = new b();
    private Boolean e = false;
    private boolean g = false;
    private String h = null;
    private String i = "";

    private class a {
        protected PayPalAdvancedPayment a = null;
        protected PayPalPreapproval b = null;
        protected CheckoutButton c = null;
        protected Context d;
        protected String e;
        protected String f;
        protected String g;
        protected String h = "https://www.paypal.com";
        protected String i = "https://www.paypal.com";
        protected int j;
        protected int k;
        protected int l;
        protected boolean m;
        protected boolean n;

        public a(PayPal payPal) {
        }
    }

    private class b {
        protected String a;
        protected String b;
        protected String c;
        protected int d;
        protected int e;
        protected boolean f;

        public b() {
        }
    }

    private PayPal() {
        resetAccount();
    }

    private static String a(String str) {
        return str == null ? "" : str;
    }

    public static String getBuild() {
        return "1.5.5.44".substring("1.5.5.44".lastIndexOf(46) + 1);
    }

    public static PayPal getInstance() {
        return PayPalActivity._paypal;
    }

    public static String getPaySubtype(int i2) {
        return b[i2];
    }

    public static String getPayType(int i2) {
        return a[i2];
    }

    public static String getVersion() {
        return "1.5.5.44";
    }

    public static String getVersionWithoutBuild() {
        return "1.5.5.44".substring(0, "1.5.5.44".lastIndexOf(46));
    }

    public static PayPal initWithAppID(Context context, String str, int i2) throws IllegalStateException {
        if (PayPalActivity._paypal != null) {
            PayPalActivity._paypal.deinitialize();
        }
        PayPal payPal = new PayPal();
        PayPalActivity._paypal = payPal;
        payPal.c.d = context;
        PayPalActivity._paypal.c.e = str;
        PayPalActivity._paypal.c.j = i2;
        PayPalActivity._paypal.c.n = false;
        PayPalActivity._paypal.c.m = true;
        com.paypal.android.a.b.c();
        PayPal payPal2 = PayPalActivity._paypal;
        g.a();
        Locale locale = Locale.getDefault();
        String str2 = locale.getLanguage() + '_' + locale.getCountry();
        payPal2.setLanguage(str2);
        h.c(str2);
        if (!PayPalActivity._paypal.e.booleanValue()) {
            synchronized (PayPalActivity._paypal) {
                try {
                    PayPalActivity._paypal.wait();
                } catch (InterruptedException e2) {
                }
            }
        }
        return PayPalActivity._paypal;
    }

    public static int logd(String str, String str2) {
        return 0;
    }

    public static int loge(String str, String str2) {
        return Log.e(str, str2);
    }

    public final boolean canShowCart() {
        if (getPayType() == 3) {
            return false;
        }
        if (getPayment().getReceivers().size() == 1) {
            PayPalReceiverDetails payPalReceiverDetails = getPayment().getReceivers().get(0);
            if (payPalReceiverDetails.getInvoiceData() == null) {
                return false;
            }
            PayPalInvoiceData invoiceData = payPalReceiverDetails.getInvoiceData();
            if (invoiceData.getTax() == null && invoiceData.getShipping() == null && invoiceData.getInvoiceItems().size() == 0) {
                return false;
            }
        }
        return true;
    }

    public final Intent checkout(PayPalAdvancedPayment payPalAdvancedPayment, Context context) {
        return checkout(payPalAdvancedPayment, context, (PaymentAdjuster) null, (PayPalResultDelegate) null);
    }

    public final Intent checkout(PayPalAdvancedPayment payPalAdvancedPayment, Context context, PayPalResultDelegate payPalResultDelegate) {
        return checkout(payPalAdvancedPayment, context, (PaymentAdjuster) null, payPalResultDelegate);
    }

    public final Intent checkout(PayPalAdvancedPayment payPalAdvancedPayment, Context context, PaymentAdjuster paymentAdjuster) {
        return checkout(payPalAdvancedPayment, context, paymentAdjuster, (PayPalResultDelegate) null);
    }

    public final Intent checkout(PayPalAdvancedPayment payPalAdvancedPayment, Context context, PaymentAdjuster paymentAdjuster, PayPalResultDelegate payPalResultDelegate) {
        this.c.a = payPalAdvancedPayment;
        this.c.b = null;
        Intent intent = new Intent(context, PayPalActivity.class);
        intent.putExtra(PayPalActivity.EXTRA_PAYMENT_INFO, payPalAdvancedPayment);
        if (paymentAdjuster != null) {
            intent.putExtra(PayPalActivity.EXTRA_PAYMENT_ADJUSTER, (Serializable) paymentAdjuster);
        }
        if (payPalResultDelegate != null) {
            intent.putExtra(PayPalActivity.EXTRA_RESULT_DELEGATE, (Serializable) payPalResultDelegate);
        }
        return intent;
    }

    public final Intent checkout(PayPalPayment payPalPayment, Context context) {
        return checkout(payPalPayment, context, (PaymentAdjuster) null, (PayPalResultDelegate) null);
    }

    public final Intent checkout(PayPalPayment payPalPayment, Context context, PayPalResultDelegate payPalResultDelegate) {
        return checkout(payPalPayment, context, (PaymentAdjuster) null, payPalResultDelegate);
    }

    public final Intent checkout(PayPalPayment payPalPayment, Context context, PaymentAdjuster paymentAdjuster) {
        return checkout(payPalPayment, context, paymentAdjuster, (PayPalResultDelegate) null);
    }

    public final Intent checkout(PayPalPayment payPalPayment, Context context, PaymentAdjuster paymentAdjuster, PayPalResultDelegate payPalResultDelegate) {
        PayPalAdvancedPayment payPalAdvancedPayment = new PayPalAdvancedPayment();
        payPalAdvancedPayment.setCurrencyType(payPalPayment.getCurrencyType());
        payPalAdvancedPayment.setIpnUrl(payPalPayment.getIpnUrl());
        payPalAdvancedPayment.setMemo(payPalPayment.getMemo());
        PayPalReceiverDetails payPalReceiverDetails = new PayPalReceiverDetails();
        payPalReceiverDetails.setRecipient(payPalPayment.getRecipient());
        payPalReceiverDetails.setSubtotal(payPalPayment.getSubtotal());
        payPalReceiverDetails.setInvoiceData(payPalPayment.getInvoiceData());
        payPalReceiverDetails.setPaymentType(payPalPayment.getPaymentType());
        payPalReceiverDetails.setPaymentSubtype(payPalPayment.getPaymentSubtype());
        payPalReceiverDetails.setDescription(payPalPayment.getMemo());
        payPalReceiverDetails.setCustomID(payPalPayment.getCustomID());
        payPalReceiverDetails.setMerchantName(payPalPayment.getMerchantName());
        payPalReceiverDetails.setIsPrimary(false);
        payPalAdvancedPayment.getReceivers().add(payPalReceiverDetails);
        return checkout(payPalAdvancedPayment, context, paymentAdjuster, payPalResultDelegate);
    }

    public final void deinitialize() {
        com.paypal.android.a.b.d();
        PayPalActivity._paypal = null;
        f = null;
    }

    public final String getAccountCountryDialingCode() {
        return this.i.length() > 0 ? this.i : com.paypal.android.a.b.m();
    }

    public final String getAccountEmail() {
        return a(this.d.b);
    }

    public final String getAccountName() {
        return a(this.d.a);
    }

    public final String getAccountPhone() {
        return this.d.c;
    }

    public final String getAdjustPaymentError() {
        return this.h == null ? h.a("ANDROID_calc_error") : this.h;
    }

    public final String getAppID() {
        return this.c.e;
    }

    public final int getAuthMethod() {
        return this.d.d;
    }

    public final int getAuthSetting() {
        return this.d.e;
    }

    public final String getCancelUrl() {
        return this.c.h;
    }

    public final CheckoutButton getCheckoutButton(Context context, int i2, int i3) {
        this.c.c = new CheckoutButton(context);
        this.c.c.a(i2, i3);
        this.c.k = i3;
        this.c.c.setActive(true);
        return this.c.c;
    }

    public final float getDensity() {
        return getParentContext().getResources().getDisplayMetrics().density;
    }

    public final String getDeviceReferenceToken() {
        return m.b();
    }

    public final boolean getDynamicAmountCalculationEnabled() {
        return this.c.n;
    }

    public final int getFeesPayer() {
        return this.c.l;
    }

    public final String getFlowContext() {
        return null;
    }

    public final boolean getIsRememberMe() {
        return this.g;
    }

    public final String getLanguage() {
        return this.c.g;
    }

    public final Context getParentContext() {
        return this.c.d;
    }

    public final int getPayType() {
        if (this.c.a == null && this.c.b != null) {
            return 3;
        }
        if (this.c.a.getReceivers().size() == 1) {
            return 0;
        }
        return this.c.a.hasPrimaryReceiever() ? 2 : 1;
    }

    public final PayPalAdvancedPayment getPayment() {
        return this.c.a;
    }

    public final PayPalPreapproval getPreapproval() {
        return this.c.b;
    }

    public final String getPreapprovalKey() {
        return this.c.f;
    }

    public final String getReturnUrl() {
        return this.c.i;
    }

    public final int getServer() {
        return this.c.j;
    }

    public final String getSessionToken() {
        return null;
    }

    public final boolean getShippingEnabled() {
        return this.c.m;
    }

    public final int getTextType() {
        return this.c.k;
    }

    public final boolean hasCreatedPIN() {
        return this.d.f;
    }

    public final boolean isHeavyCountry() {
        String iSO3Country = Locale.getDefault().getISO3Country();
        return iSO3Country.equals("USA") || iSO3Country.equals("GBR") || iSO3Country.equals("CAN") || iSO3Country.equals("AUS") || iSO3Country.equals("ESP") || iSO3Country.equals("ITA") || iSO3Country.equals("FRA") || iSO3Country.equals("SGP") || iSO3Country.equals("MYS");
    }

    public final boolean isLibraryInitialized() {
        if (PayPalActivity._paypal == null) {
            return false;
        }
        return this.e.booleanValue();
    }

    public final boolean isLightCountry() {
        return !isHeavyCountry();
    }

    public final boolean isPersonalPayment() {
        if (this.c.a == null) {
            return false;
        }
        for (int i2 = 0; i2 < this.c.a.getReceivers().size(); i2++) {
            if (this.c.a.getReceivers().get(i2).getPaymentType() != 2) {
                return false;
            }
        }
        return true;
    }

    public final void onInitializeCompletedError(int i2, Object obj) {
    }

    public final void onInitializeCompletedOK(int i2, Object obj) {
    }

    public final Intent preapprove(PayPalPreapproval payPalPreapproval, Context context) {
        return preapprove(payPalPreapproval, context, null);
    }

    public final Intent preapprove(PayPalPreapproval payPalPreapproval, Context context, PayPalResultDelegate payPalResultDelegate) {
        this.c.b = payPalPreapproval;
        this.c.a = null;
        Intent intent = new Intent(context, PayPalActivity.class);
        intent.putExtra(PayPalActivity.EXTRA_PREAPPROVAL_INFO, payPalPreapproval);
        if (payPalResultDelegate != null) {
            intent.putExtra(PayPalActivity.EXTRA_RESULT_DELEGATE, (Serializable) payPalResultDelegate);
        }
        return intent;
    }

    public final void resetAccount() {
        this.g = false;
        b bVar = this.d;
        PayPal.this.setAccountName(null);
        PayPal.this.setAccountEmail(null);
        PayPal.this.setAccountPhone(null);
        PayPal.this.setAuthMethod(0);
        PayPal.this.setAuthSetting(0);
        PayPal.this.setPINCreated(false);
    }

    public final void setAccountCountryDialingCode(String str) {
        Assert.assertNotNull("", str);
        this.i = str;
    }

    public final void setAccountEmail(String str) {
        this.d.b = str;
    }

    public final void setAccountName(String str) {
        this.d.a = str;
    }

    public final void setAccountPhone(String str) {
        this.d.c = str;
    }

    public final void setAdjustPaymentError(String str) {
        this.h = str;
    }

    public final void setAuthMethod(int i2) {
        this.d.d = i2;
    }

    public final void setAuthSetting(int i2) {
        this.d.e = i2;
    }

    public final void setCancelUrl(String str) {
        this.c.h = str;
    }

    public final void setDeviceReferenceToken(String str) {
    }

    public final void setDynamicAmountCalculationEnabled(boolean z) {
        this.c.n = z;
    }

    public final void setFeesPayer(int i2) {
        this.c.l = i2;
    }

    public final void setIsRememberMe(boolean z) {
        this.g = z;
    }

    public final void setLanguage(String str) {
        if (!h.b(str)) {
            str = ReservationSystemConstants.DEFAULT_LANGUAGE;
        }
        this.c.g = str;
        h.c(this.c.g);
    }

    public final void setLibraryInitialized(boolean z) {
        synchronized (PayPalActivity._paypal) {
            PayPalActivity._paypal.e = Boolean.valueOf(z);
            PayPalActivity._paypal.notifyAll();
        }
    }

    public final void setPINCreated(boolean z) {
        this.d.f = z;
    }

    public final void setPreapprovalKey(String str) {
        this.c.f = str;
    }

    public final void setReturnUrl(String str) {
        this.c.i = str;
    }

    public final void setSessionToken(String str) {
    }

    public final void setShippingEnabled(boolean z) {
        this.c.m = z;
    }

    public final boolean shouldShowFees() {
        String iSO3Country = Locale.getDefault().getISO3Country();
        return !(Locale.GERMANY.getISO3Country().compareTo(iSO3Country) == 0 || Locale.ITALY.getISO3Country().compareTo(iSO3Country) == 0) && isPersonalPayment();
    }

    public final void shutdown() {
        h.a();
        g.b();
        this.c = null;
    }
}
