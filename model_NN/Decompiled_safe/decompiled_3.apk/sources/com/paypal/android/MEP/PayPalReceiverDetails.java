package com.paypal.android.MEP;

import com.paypal.android.a.h;
import java.io.Serializable;
import java.math.BigDecimal;

public class PayPalReceiverDetails implements Serializable {
    private static final long serialVersionUID = 3;
    private String a = null;
    private BigDecimal b = null;
    private PayPalInvoiceData c = null;
    private int d = 3;
    private int e = 22;
    private String f = null;
    private String g = null;
    private String h = null;
    private boolean i = false;

    public String getCustomID() {
        return this.g;
    }

    public String getDescription() {
        return this.f;
    }

    public PayPalInvoiceData getInvoiceData() {
        return this.c;
    }

    public boolean getIsPrimary() {
        return this.i;
    }

    public String getMerchantName() {
        return this.h;
    }

    public int getPaymentSubtype() {
        return this.e;
    }

    public int getPaymentType() {
        return this.d;
    }

    public String getRecipient() {
        return this.a;
    }

    public BigDecimal getSubtotal() {
        return this.b;
    }

    public BigDecimal getTotal() {
        BigDecimal add = BigDecimal.ZERO.add(this.b);
        if (this.c == null) {
            return add;
        }
        if (this.c.getTax() != null) {
            add = add.add(this.c.getTax());
        }
        return this.c.getShipping() != null ? add.add(this.c.getShipping()) : add;
    }

    public boolean isEmailRecipient() {
        return h.d(this.a);
    }

    public boolean isPhoneRecipient() {
        return h.e(this.a);
    }

    public void setCustomID(String str) {
        this.g = str;
    }

    public void setDescription(String str) {
        this.f = str;
    }

    public void setInvoiceData(PayPalInvoiceData payPalInvoiceData) {
        this.c = payPalInvoiceData;
    }

    public void setIsPrimary(boolean z) {
        this.i = z;
    }

    public void setMerchantName(String str) {
        this.h = str;
    }

    public void setPaymentSubtype(int i2) {
        this.e = i2;
    }

    public void setPaymentType(int i2) {
        this.d = i2;
    }

    public void setRecipient(String str) {
        this.a = str.replace(" ", "");
    }

    public void setSubtotal(BigDecimal bigDecimal) {
        this.b = bigDecimal.setScale(2, 4);
    }
}
