package com.paypal.android.MEP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class b extends BroadcastReceiver {
    b(PayPalActivity payPalActivity) {
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.indexOf(PayPalActivity._pushIntent) != -1) {
            boolean unused = PayPalActivity.c.a(Integer.parseInt(action.substring(PayPalActivity._pushIntent.length())));
        } else if (action.indexOf(PayPalActivity._popIntent) != -1) {
            PayPalActivity.a(PayPalActivity.c);
        } else if (action.indexOf(PayPalActivity._replaceIntent) != -1) {
            PayPalActivity.b(PayPalActivity.c, Integer.parseInt(action.substring(PayPalActivity._replaceIntent.length())));
        } else if (action.indexOf(PayPalActivity._updateIntent) != -1) {
            PayPalActivity.c.d.lastElement().b();
        }
    }
}
