package com.paypal.android.MEP;

import com.biznessapps.constants.AppConstants;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;

public class PayPalAdvancedPayment implements Serializable {
    private static final long serialVersionUID = 2;
    private String a = null;
    private ArrayList<PayPalReceiverDetails> b = new ArrayList<>();
    private String c = null;
    private String d = null;
    private String e = null;

    public String getCurrencyType() {
        return this.a;
    }

    public String getIpnUrl() {
        return this.c;
    }

    public String getMemo() {
        return this.d;
    }

    public String getMerchantName() {
        return this.e;
    }

    public PayPalReceiverDetails getPrimaryReceiver() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return null;
            }
            if (this.b.get(i2).getIsPrimary()) {
                return this.b.get(i2);
            }
            i = i2 + 1;
        }
    }

    public ArrayList<PayPalReceiverDetails> getReceivers() {
        return this.b;
    }

    public BigDecimal getTotal() {
        int i = 0;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return bigDecimal;
            }
            bigDecimal = bigDecimal.add(this.b.get(i2).getTotal());
            i = i2 + 1;
        }
    }

    public BigDecimal getTotalShipping() {
        int i = 0;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return bigDecimal;
            }
            if (!(this.b.get(i2).getInvoiceData() == null || this.b.get(i2).getInvoiceData().getShipping() == null)) {
                bigDecimal = bigDecimal.add(this.b.get(i2).getInvoiceData().getShipping());
            }
            i = i2 + 1;
        }
    }

    public BigDecimal getTotalSubtotal() {
        int i = 0;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return bigDecimal;
            }
            if (this.b.get(i2).getSubtotal() != null) {
                bigDecimal = bigDecimal.add(this.b.get(i2).getSubtotal());
            }
            i = i2 + 1;
        }
    }

    public BigDecimal getTotalTax() {
        int i = 0;
        BigDecimal bigDecimal = BigDecimal.ZERO;
        while (true) {
            int i2 = i;
            if (i2 >= this.b.size()) {
                return bigDecimal;
            }
            if (!(this.b.get(i2).getInvoiceData() == null || this.b.get(i2).getInvoiceData().getTax() == null)) {
                bigDecimal = bigDecimal.add(this.b.get(i2).getInvoiceData().getTax());
            }
            i = i2 + 1;
        }
    }

    public boolean hasPrimaryReceiever() {
        for (int i = 0; i < this.b.size(); i++) {
            if (this.b.get(i).getIsPrimary()) {
                return true;
            }
        }
        return false;
    }

    public boolean isValid() {
        if (this.b.size() <= 0) {
            return false;
        }
        for (int i = 0; i < this.b.size(); i++) {
            if (this.b.get(i).getRecipient() == null || this.b.get(i).getRecipient().length() <= 0 || this.b.get(i).getSubtotal().compareTo(BigDecimal.ZERO) <= 0) {
                return false;
            }
        }
        return (!this.a.equals("AUD") || getTotal().compareTo(new BigDecimal("11000")) < 0) ? (!this.a.equals("CAD") || getTotal().compareTo(new BigDecimal("11000")) < 0) ? (!this.a.equals("CHF") || getTotal().compareTo(new BigDecimal("11000")) < 0) ? (!this.a.equals("CZK") || getTotal().compareTo(new BigDecimal("200000")) < 0) ? (!this.a.equals("DKK") || getTotal().compareTo(new BigDecimal("55000")) < 0) ? (!this.a.equals("EUR") || getTotal().compareTo(new BigDecimal("7500")) < 0) ? (!this.a.equals("GBP") || getTotal().compareTo(new BigDecimal("7500")) < 0) ? (!this.a.equals("HKD") || getTotal().compareTo(new BigDecimal("80000")) < 0) ? (!this.a.equals("HUF") || getTotal().compareTo(new BigDecimal("2000000")) < 0) ? (!this.a.equals("ILS") || getTotal().compareTo(new BigDecimal("40000")) < 0) ? (!this.a.equals("JPY") || getTotal().compareTo(new BigDecimal("1000000")) < 0) ? (!this.a.equals("MXN") || getTotal().compareTo(new BigDecimal("130000")) < 0) ? (!this.a.equals("MYR") || getTotal().compareTo(new BigDecimal("35000")) < 0) ? (!this.a.equals("NOK") || getTotal().compareTo(new BigDecimal("60000")) < 0) ? (!this.a.equals("NZD") || getTotal().compareTo(new BigDecimal("15000")) < 0) ? (!this.a.equals("PHP") || getTotal().compareTo(new BigDecimal("500000")) < 0) ? (!this.a.equals("PLN") || getTotal().compareTo(new BigDecimal("30000")) < 0) ? (!this.a.equals("SEK") || getTotal().compareTo(new BigDecimal("75000")) < 0) ? (!this.a.equals("SGD") || getTotal().compareTo(new BigDecimal("15000")) < 0) ? (!this.a.equals("THB") || getTotal().compareTo(new BigDecimal("350000")) < 0) ? (!this.a.equals("TWD") || getTotal().compareTo(new BigDecimal("350000")) < 0) ? !this.a.equals(AppConstants.DEFAULT_CURRENCY) || getTotal().compareTo(new BigDecimal("10000")) < 0 : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false : false;
    }

    public void setCurrencyType(String str) {
        this.a = str.toUpperCase();
    }

    public void setCurrencyType(Currency currency) {
        this.a = currency.getCurrencyCode();
    }

    public void setIpnUrl(String str) {
        this.c = str;
    }

    public void setMemo(String str) {
        this.d = str;
    }

    public void setMerchantName(String str) {
        this.e = str;
    }

    public void setReceivers(ArrayList<PayPalReceiverDetails> arrayList) {
        this.b = arrayList;
    }
}
