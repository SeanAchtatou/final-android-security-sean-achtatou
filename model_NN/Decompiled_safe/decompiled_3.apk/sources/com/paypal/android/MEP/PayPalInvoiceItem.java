package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayPalInvoiceItem implements Serializable {
    private static final long serialVersionUID = 5;
    BigDecimal a;
    private String b;
    private String c;
    private BigDecimal d;
    private int e;

    public PayPalInvoiceItem() {
        this.b = null;
        this.c = null;
        this.a = null;
        this.d = null;
        this.e = 0;
    }

    public PayPalInvoiceItem(String str, String str2, BigDecimal bigDecimal, int i) {
        this.b = str;
        this.c = str2;
        this.d = bigDecimal;
        this.e = i;
        this.a = bigDecimal.multiply(new BigDecimal(i).setScale(2, 4));
    }

    public String getID() {
        return this.c;
    }

    public String getName() {
        return this.b;
    }

    public int getQuantity() {
        return this.e;
    }

    public BigDecimal getTotalPrice() {
        return this.a;
    }

    public BigDecimal getUnitPrice() {
        return this.d;
    }

    public boolean isValid() {
        if (this.b != null && this.b.length() > 0) {
            return true;
        }
        if (this.c != null && this.c.length() > 0) {
            return true;
        }
        if (this.a == null || this.a.compareTo(BigDecimal.ZERO) <= 0) {
            return (this.d != null && this.d.compareTo(BigDecimal.ZERO) > 0) || this.e > 0;
        }
        return true;
    }

    public void setID(String str) {
        this.c = str;
    }

    public void setName(String str) {
        this.b = str;
    }

    public void setQuantity(int i) {
        this.e = i;
    }

    public void setTotalPrice(BigDecimal bigDecimal) {
        this.a = bigDecimal.setScale(2, 4);
    }

    public void setUnitPrice(BigDecimal bigDecimal) {
        this.d = bigDecimal.setScale(2, 4);
    }
}
