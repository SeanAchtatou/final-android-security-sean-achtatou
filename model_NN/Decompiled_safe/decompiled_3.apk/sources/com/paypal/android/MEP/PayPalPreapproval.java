package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;

public class PayPalPreapproval implements Serializable {
    public static final int DAY_FRIDAY = 5;
    public static final int DAY_MONDAY = 1;
    public static final int DAY_NONE = 7;
    public static final int DAY_SATURDAY = 6;
    public static final int DAY_SUNDAY = 0;
    public static final int DAY_THURSDAY = 4;
    public static final int DAY_TUESDAY = 2;
    public static final int DAY_WEDNESDAY = 3;
    public static final int PERIOD_ANNUALLY = 5;
    public static final int PERIOD_BIWEEKLY = 2;
    public static final int PERIOD_DAILY = 0;
    public static final int PERIOD_MONTHLY = 4;
    public static final int PERIOD_NONE = 6;
    public static final int PERIOD_SEMIMONTHLY = 3;
    public static final int PERIOD_WEEKLY = 1;
    public static final int TYPE_AGREE = 0;
    public static final int TYPE_AGREE_AND_PAY = 1;
    private static final long serialVersionUID = 7;
    private String a = null;
    private String b = null;
    private int c = 0;
    private BigDecimal d = null;
    private BigDecimal e = null;
    private String f = null;
    private String g = null;
    private boolean h = false;
    private boolean i = false;
    private int j = 6;
    private int k = 7;
    private int l = 0;
    private int m = 0;
    private String n = null;
    private String o = null;
    private int p = 1;

    public String getCurrencyType() {
        return this.a;
    }

    public int getDayOfMonth() {
        return this.l;
    }

    public int getDayOfWeek() {
        return this.k;
    }

    public int getDayOfWeekInt(String str) {
        if (str.equals("SUNDAY")) {
            return 0;
        }
        if (str.equals("MONDAY")) {
            return 1;
        }
        if (str.equals("TUESDAY")) {
            return 2;
        }
        if (str.equals("WEDNESDAY")) {
            return 3;
        }
        if (str.equals("THURSDAY")) {
            return 4;
        }
        if (str.equals("FRIDAY")) {
            return 5;
        }
        return str.equals("SATURDAY") ? 6 : 7;
    }

    public String getEndDate() {
        return this.g;
    }

    public String getIpnUrl() {
        return this.n;
    }

    public boolean getIsApproved() {
        return this.h;
    }

    public BigDecimal getMaxAmountPerPayment() {
        return this.d;
    }

    public int getMaxNumberOfPayments() {
        return this.c;
    }

    public int getMaxNumberOfPaymentsPerPeriod() {
        return this.m;
    }

    public BigDecimal getMaxTotalAmountOfAllPayments() {
        return this.e;
    }

    public String getMemo() {
        return this.o;
    }

    public String getMerchantName() {
        return this.b;
    }

    public int getPaymentPeriod() {
        return this.j;
    }

    public int getPaymentPeriodInt(String str) {
        if (str.equals("DAILY")) {
            return 0;
        }
        if (str.equals("WEEKLY")) {
            return 1;
        }
        if (str.equals("BIWEEKLY")) {
            return 2;
        }
        if (str.equals("SEMIMONTHLY")) {
            return 3;
        }
        if (str.equals("MONTHLY")) {
            return 4;
        }
        return str.equals("ANNUALLY") ? 5 : 6;
    }

    public boolean getPinRequired() {
        return this.i;
    }

    public String getStartDate() {
        return this.f;
    }

    public int getType() {
        return this.p;
    }

    public boolean isValid() {
        return this.b != null && this.b.length() > 0;
    }

    public void setCurrencyType(String str) {
        this.a = str;
    }

    public void setDayOfMonth(int i2) {
        this.l = i2;
    }

    public void setDayOfWeek(int i2) {
        this.k = i2;
    }

    public void setEndDate(String str) {
        this.g = str;
    }

    public void setIpnUrl(String str) {
        this.n = str;
    }

    public void setIsApproved(boolean z) {
        this.h = z;
    }

    public void setMaxAmountPerPayment(BigDecimal bigDecimal) {
        this.d = bigDecimal;
    }

    public void setMaxNumberOfPayments(int i2) {
        this.c = i2;
    }

    public void setMaxNumberOfPaymentsPerPeriod(int i2) {
        this.m = i2;
    }

    public void setMaxTotalAmountOfAllPayments(BigDecimal bigDecimal) {
        this.e = bigDecimal;
    }

    public void setMemo(String str) {
        this.o = str;
    }

    public void setMerchantName(String str) {
        this.b = str;
    }

    public void setPaymentPeriod(int i2) {
        this.j = i2;
    }

    public void setPinRequired(boolean z) {
        this.i = z;
    }

    public void setStartDate(String str) {
        this.f = str;
    }

    public void setType(int i2) {
        if (i2 <= 1 && i2 >= 0) {
            this.p = i2;
        }
    }
}
