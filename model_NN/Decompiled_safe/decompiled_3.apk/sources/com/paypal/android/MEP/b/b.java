package com.paypal.android.MEP.b;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalAdvancedPayment;
import com.paypal.android.MEP.PayPalPreapproval;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.paypal.android.MEP.a.d;
import com.paypal.android.MEP.a.e;
import com.paypal.android.MEP.a.h;
import com.paypal.android.a.l;
import com.paypal.android.a.o;
import com.paypal.android.b.c;
import com.paypal.android.b.f;
import com.paypal.android.b.j;
import java.math.BigDecimal;
import java.util.ArrayList;

public final class b extends c implements View.OnTouchListener {
    private f e;
    private boolean f;
    private GradientDrawable g;
    private GradientDrawable h;

    public b(Context context, j jVar) {
        super(context);
        PayPal instance = PayPal.getInstance();
        PayPalAdvancedPayment payment = instance.getPayment();
        PayPalPreapproval preapproval = instance.getPreapproval();
        boolean z = jVar instanceof d;
        boolean z2 = (jVar instanceof h) || (jVar instanceof e);
        a(new LinearLayout.LayoutParams(-1, -2), 0);
        a(new LinearLayout.LayoutParams(-1, -2), 1);
        this.g = com.paypal.android.a.d.a(-1, -1510918, -7829368);
        this.h = com.paypal.android.a.d.a(-1, 14209956, -7829368);
        setBackgroundDrawable(this.g);
        setPadding(5, 5, 5, 0);
        a(com.paypal.android.a.e.a(14218, 463));
        b(com.paypal.android.a.e.a(38432, 430));
        setOnTouchListener(this);
        if (instance.getPayType() != 3) {
            this.f = payment.getTotalShipping().compareTo(BigDecimal.ZERO) > 0 || payment.getTotalTax().compareTo(BigDecimal.ZERO) > 0 || instance.getDynamicAmountCalculationEnabled() || payment.hasPrimaryReceiever() || payment.getReceivers().size() > 1;
            if (payment.getReceivers().get(0).getInvoiceData() != null && payment.getReceivers().get(0).getInvoiceData().getInvoiceItems() != null) {
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 >= payment.getReceivers().get(0).getInvoiceData().getInvoiceItems().size()) {
                        break;
                    }
                    if (payment.getReceivers().get(0).getInvoiceData().getInvoiceItems().get(i2).isValid()) {
                        this.f = true;
                    }
                    i = i2 + 1;
                }
            }
            this.a.setGravity(16);
            this.a.addView(z2 ? com.paypal.android.a.e.a(context, "system-icon-confirmation.png") : (instance.isPersonalPayment() || instance.getTextType() == 1) ? com.paypal.android.a.e.a(context, "shopping-list-enabled.png") : com.paypal.android.a.e.a(context, "shopping-cart-enabled.png"));
            this.e = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_BOLD, 0.5f, 0.5f);
            if (z2) {
                if (instance.isPersonalPayment() || instance.getTextType() == 1) {
                    this.e.a(com.paypal.android.a.h.a("ANDROID_total_paid") + ":");
                } else {
                    this.e.a(com.paypal.android.a.h.a("ANDROID_receipt") + ":");
                }
            } else if (this.f) {
                this.e.a(com.paypal.android.a.h.a("ANDROID_my_total") + ":");
            } else {
                String description = payment.getReceivers().get(0).getDescription();
                if (description == null || description.length() <= 0) {
                    this.e.a(com.paypal.android.a.h.a("ANDROID_my_total") + ":");
                } else {
                    this.e.a(description + ":");
                }
            }
            this.e.a(-16777216);
            if (payment.hasPrimaryReceiever()) {
                this.e.b(l.a(payment.getPrimaryReceiver().getTotal(), payment.getCurrencyType()));
            } else {
                this.e.b(l.a(payment.getTotal(), payment.getCurrencyType()));
            }
            this.e.b(-13408768);
            this.e.setPadding(5, 0, 5, 0);
            this.a.addView(this.e);
            this.a.addView(this.c);
            this.c.setVisibility(0);
            if (this.f) {
                this.b.setOrientation(1);
                View view = new View(context);
                view.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
                view.setBackgroundColor(-7829368);
                this.b.addView(view);
                if (!payment.hasPrimaryReceiever()) {
                    ArrayList<PayPalReceiverDetails> receivers = payment.getReceivers();
                    if (receivers.size() != 1) {
                        int i3 = 0;
                        while (true) {
                            int i4 = i3;
                            if (i4 >= receivers.size()) {
                                break;
                            }
                            this.b.addView(new c(context, receivers.get(i4), payment.getCurrencyType()));
                            i3 = i4 + 1;
                        }
                    } else {
                        this.b.addView(c.a(context, receivers.get(0), payment.getCurrencyType()));
                    }
                } else {
                    this.b.addView(c.a(context, payment.getPrimaryReceiver(), payment.getCurrencyType()));
                }
                View view2 = new View(context);
                view2.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
                view2.setBackgroundColor(-7829368);
                this.b.addView(view2);
                TextView a = o.a(o.a.HELVETICA_14_BOLD, context);
                a.setGravity(5);
                String a2 = payment.hasPrimaryReceiever() ? l.a(payment.getPrimaryReceiver().getTotal(), payment.getCurrencyType()) : l.a(payment.getTotal().toString(), payment.getCurrencyType());
                if (a2.contains(payment.getCurrencyType())) {
                    a.setText(com.paypal.android.a.h.a("ANDROID_total") + ": " + a2);
                } else {
                    a.setText(com.paypal.android.a.h.a("ANDROID_total") + " (" + payment.getCurrencyType() + "): " + a2);
                }
                this.b.addView(a);
                TextView a3 = o.a(o.a.HELVETICA_12_NORMAL, context);
                a3.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                a3.setGravity(5);
                a3.setText(com.paypal.android.a.h.a("ANDROID_shipping_tax_estimated_note"));
                if (instance.getDynamicAmountCalculationEnabled() && z) {
                    this.b.addView(a3);
                    return;
                }
                return;
            }
            setClickable(false);
            return;
        }
        this.f = true;
        this.a.setGravity(16);
        this.a.addView(z2 ? com.paypal.android.a.e.a(context, "system-icon-confirmation.png") : com.paypal.android.a.e.a(context, "shopping-list-enabled.png"));
        this.e = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_BOLD, 0.5f, 0.5f);
        if (z2) {
            this.e.a(com.paypal.android.a.h.a("ANDROID_receipt") + ":");
            this.e.b("");
        } else {
            this.e.a(com.paypal.android.a.h.a("ANDROID_billing_summary"));
            this.e.b("");
        }
        this.e.a(-16777216);
        this.e.b(-13408768);
        this.a.addView(this.e);
        this.a.addView(this.c);
        this.c.setVisibility(0);
        PayPalPreapproval preapproval2 = instance.getPreapproval();
        if (!z) {
            this.b.setOrientation(1);
            View view3 = new View(context);
            view3.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
            view3.setBackgroundColor(-7829368);
            this.b.addView(view3);
            f fVar = new f(context, o.a.HELVETICA_12_BOLD, o.a.HELVETICA_12_NORMAL);
            fVar.a(com.paypal.android.a.h.a("ANDROID_name") + ":");
            fVar.b(preapproval.getMerchantName());
            this.b.addView(fVar);
            if (preapproval2.getStartDate() != null && preapproval2.getStartDate().length() > 0) {
                f fVar2 = new f(context, o.a.HELVETICA_12_BOLD, o.a.HELVETICA_12_NORMAL);
                fVar2.a(com.paypal.android.a.h.a("ANDROID_start_date") + ":");
                fVar2.b(com.paypal.android.a.h.a(preapproval2.getStartDate(), 2));
                this.b.addView(fVar2);
            }
            if (preapproval2.getEndDate() != null && preapproval2.getEndDate().length() > 0) {
                f fVar3 = new f(context, o.a.HELVETICA_12_BOLD, o.a.HELVETICA_12_NORMAL);
                fVar3.a(com.paypal.android.a.h.a("ANDROID_end_date") + ":");
                fVar3.b(com.paypal.android.a.h.a(preapproval2.getEndDate(), 2));
                this.b.addView(fVar3);
            }
        }
    }

    public final void a(boolean z, boolean z2) {
        if (!z || !this.f) {
            setFocusable(false);
            setClickable(false);
            this.b.setVisibility(8);
            this.c.setVisibility(8);
            if (z2) {
                this.e.a(-7829368);
                this.e.b(-7829368);
                return;
            }
            return;
        }
        setFocusable(true);
        setClickable(true);
        this.c.setVisibility(0);
        this.e.a(-16777216);
        this.e.b(-13408768);
    }

    public final void b(boolean z) {
        if (z) {
            setBackgroundDrawable(this.h);
        } else {
            setBackgroundDrawable(this.g);
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (isFocusable()) {
            switch (motionEvent.getAction()) {
                case 0:
                    setBackgroundDrawable(this.h);
                    break;
                default:
                    setBackgroundDrawable(this.g);
                    break;
            }
        }
        return false;
    }
}
