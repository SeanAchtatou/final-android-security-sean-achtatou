package com.paypal.android.MEP.a;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.biznessapps.constants.AppConstants;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalPreapproval;
import com.paypal.android.MEP.a;
import com.paypal.android.MEP.a.d;
import com.paypal.android.MEP.b.a;
import com.paypal.android.MEP.b.b;
import com.paypal.android.MEP.b.f;
import com.paypal.android.a.o;
import com.paypal.android.b.e;
import com.paypal.android.b.g;
import com.paypal.android.b.h;
import com.paypal.android.b.i;
import com.paypal.android.b.j;
import java.util.regex.Pattern;

public final class g extends j implements View.OnClickListener, a.b, a.b, g.a {
    private static e n = null;
    private a a;
    private b b;
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private i g;
    private i h;
    private String i;
    private LinearLayout j;
    private LinearLayout k;
    private RelativeLayout l;
    private TextView m = null;

    public enum a {
        STATE_PIN,
        STATE_REVIEW,
        STATE_CONFIRM_PREAPPROVAL,
        STATE_ERROR
    }

    public g(Context context) {
        super(context);
    }

    private void a(a aVar) {
        this.a = aVar;
        d.AnonymousClass1.b();
    }

    public final void a() {
    }

    public final void a(int i2, Object obj) {
        PayPalActivity.getInstance().paymentSucceeded((String) com.paypal.android.a.b.e().c("PreapprovalKey"), (String) com.paypal.android.a.b.e().c("PaymentExecStatus"), true);
    }

    public final void a(Context context) {
        PayPalPreapproval preapproval = PayPal.getInstance().getPreapproval();
        super.a(context);
        if (preapproval.getPinRequired()) {
            com.paypal.android.a.b.e().a("mpl-preapproval-PIN");
            this.a = a.STATE_PIN;
        } else {
            this.a = a.STATE_REVIEW;
        }
        LinearLayout a2 = com.paypal.android.a.d.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(o.b(o.a.HELVETICA_16_BOLD, context));
        this.b = new b(context, this);
        this.b.a(this);
        a2.addView(this.b);
        addView(a2);
        this.j = new LinearLayout(context);
        this.j.setOrientation(1);
        this.j.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.j.setPadding(5, 5, 5, 5);
        this.j.setBackgroundDrawable(com.paypal.android.a.d.a());
        this.j.addView(new h(com.paypal.android.a.h.a("ANDROID_create_code"), context));
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView.setBackgroundColor(0);
        textView.setTextColor(-13408615);
        textView.setGravity(3);
        textView.setTypeface(Typeface.create("Helvetica", 1));
        textView.setTextSize(12.0f);
        textView.setPadding(5, 5, 5, 5);
        textView.setText(com.paypal.android.a.h.a("ANDROID_require_pin").replace("%m", preapproval.getMerchantName()));
        this.j.addView(textView);
        LinearLayout a3 = com.paypal.android.a.d.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 10, 5, 10);
        this.h = new i(context, i.a.YELLOW_ALERT);
        this.h.a("This page is currently being used to test components.");
        this.h.setPadding(0, 5, 0, 5);
        this.h.setVisibility(8);
        a3.addView(this.h);
        this.j.addView(a3);
        EditText editText = new EditText(context);
        editText.setInputType(3);
        editText.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        editText.setHint(com.paypal.android.a.h.a("ANDROID_enter_code"));
        editText.setSingleLine(true);
        editText.setId(8001);
        editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.j.addView(editText);
        EditText editText2 = new EditText(context);
        editText2.setInputType(3);
        editText2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        editText2.setHint(com.paypal.android.a.h.a("ANDROID_reenter_code"));
        editText2.setSingleLine(true);
        editText2.setId(8002);
        editText2.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.j.addView(editText2);
        LinearLayout a4 = com.paypal.android.a.d.a(context, -1, -2);
        a4.setOrientation(1);
        a4.setGravity(1);
        this.f = new Button(context);
        this.f.setText(com.paypal.android.a.h.a("ANDROID_create"));
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f));
        this.f.setGravity(17);
        this.f.setBackgroundDrawable(com.paypal.android.a.e.a());
        this.f.setTextColor(-16777216);
        this.f.setOnClickListener(this);
        LinearLayout a5 = com.paypal.android.a.d.a(context, -1, -2);
        a5.setOrientation(1);
        a5.setGravity(1);
        a5.addView(this.f);
        a5.setPadding(0, 15, 0, 15);
        a4.addView(a5);
        this.d = new Button(context);
        this.d.setText(com.paypal.android.a.h.a("ANDROID_cancel"));
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f));
        this.d.setGravity(17);
        this.d.setBackgroundDrawable(com.paypal.android.a.e.b());
        this.d.setTextColor(-16777216);
        this.d.setOnClickListener(this);
        a4.addView(this.d);
        this.j.addView(a4);
        addView(this.j);
        this.k = new LinearLayout(context);
        this.k.setOrientation(1);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.k.setPadding(5, 5, 5, 5);
        this.k.setBackgroundDrawable(com.paypal.android.a.d.a());
        this.k.addView(new h(com.paypal.android.a.h.a("ANDROID_review"), context));
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundDrawable(com.paypal.android.a.d.a(-1, -1510918, -7829368));
        linearLayout.setPadding(10, 10, 10, 10);
        this.k.addView(linearLayout);
        TextView textView2 = new TextView(context);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView2.setBackgroundColor(0);
        textView2.setTextColor(-7829368);
        textView2.setGravity(3);
        textView2.setTypeface(Typeface.create("Helvetica", 1));
        textView2.setTextSize(12.0f);
        textView2.setText(com.paypal.android.a.h.a("ANDROID_payment_method"));
        linearLayout.addView(textView2);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout2.setOrientation(0);
        linearLayout.addView(linearLayout2);
        TextView textView3 = new TextView(context);
        textView3.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        textView3.setBackgroundColor(0);
        textView3.setTextColor(-16777216);
        textView3.setGravity(3);
        textView3.setTypeface(Typeface.create("Helvetica", 0));
        textView3.setTextSize(12.0f);
        textView3.setText(com.paypal.android.a.h.a("ANDROID_primary_source") + ":");
        linearLayout2.addView(textView3);
        TextView textView4 = new TextView(context);
        textView4.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView4.setBackgroundColor(0);
        textView4.setTextColor(-16777216);
        textView4.setGravity(5);
        textView4.setTypeface(Typeface.create("Helvetica", 0));
        textView4.setTextSize(12.0f);
        textView4.setText(com.paypal.android.a.h.a("ANDROID_paypal_balance"));
        linearLayout2.addView(textView4);
        TextView textView5 = new TextView(context);
        textView5.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView5.setBackgroundColor(0);
        textView5.setTextColor(-16777216);
        textView5.setGravity(3);
        textView5.setTypeface(Typeface.create("Helvetica", 0));
        textView5.setTextSize(12.0f);
        textView5.setPadding(10, 10, 10, 10);
        String str = com.paypal.android.a.h.a("ANDROID_preapproval_agreement").replace("%m", preapproval.getMerchantName()) + " " + com.paypal.android.a.h.a("ANDROID_view_policies");
        SpannableString spannableString = new SpannableString(str);
        spannableString.setSpan(new UnderlineSpan(), str.indexOf(com.paypal.android.a.h.a("ANDROID_view_policies")), spannableString.length(), 0);
        spannableString.setSpan(new URLSpan(new String("https://www.paypal.com/" + PayPalActivity._paypal.getLanguage().substring(0, 2) + "/cgi-bin/webscr?cmd=xpt/Marketing/popup/FundingMixEducation-outside")), str.indexOf(com.paypal.android.a.h.a("ANDROID_view_policies")), spannableString.length(), 33);
        Linkify.addLinks(spannableString, Pattern.compile(com.paypal.android.a.h.a("ANDROID_view_policies")), AppConstants.HTTPS_PREFIX);
        textView5.setText(spannableString);
        textView5.setMovementMethod(LinkMovementMethod.getInstance());
        this.k.addView(textView5);
        LinearLayout a6 = com.paypal.android.a.d.a(context, -1, -2);
        a6.setOrientation(1);
        a6.setPadding(5, 10, 5, 10);
        this.g = new i(context, i.a.YELLOW_ALERT);
        this.g.a("This page is currently being used to test components.");
        this.g.setPadding(0, 5, 0, 5);
        this.g.setVisibility(8);
        a6.addView(this.g);
        this.k.addView(a6);
        LinearLayout a7 = com.paypal.android.a.d.a(context, -1, -2);
        a7.setOrientation(1);
        a7.setGravity(1);
        this.c = new Button(context);
        if (PayPal.getInstance().getPreapproval().getType() == 1) {
            this.c.setText(com.paypal.android.a.h.a("ANDROID_agree_pay"));
        } else {
            this.c.setText(com.paypal.android.a.h.a("ANDROID_agree"));
        }
        this.c.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f));
        this.c.setGravity(17);
        this.c.setBackgroundDrawable(com.paypal.android.a.e.a());
        this.c.setTextColor(-16777216);
        this.c.setOnClickListener(this);
        LinearLayout a8 = com.paypal.android.a.d.a(context, -1, -2);
        a8.setOrientation(1);
        a8.setGravity(1);
        a8.addView(this.c);
        a8.setPadding(0, 15, 0, 15);
        a7.addView(a8);
        this.e = new Button(context);
        this.e.setText(com.paypal.android.a.h.a("ANDROID_cancel"));
        this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f));
        this.e.setGravity(17);
        this.e.setBackgroundDrawable(com.paypal.android.a.e.b());
        this.e.setTextColor(-16777216);
        this.e.setOnClickListener(this);
        a7.addView(this.e);
        this.k.addView(a7);
        addView(this.k);
        this.l = new RelativeLayout(context);
        this.l.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.l.setBackgroundDrawable(com.paypal.android.a.d.a());
        LinearLayout a9 = com.paypal.android.a.d.a(context, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a9.getLayoutParams());
        layoutParams.addRule(13);
        a9.setLayoutParams(layoutParams);
        a9.setOrientation(1);
        a9.setGravity(1);
        if (n == null) {
            n = new e(context);
        } else {
            ((LinearLayout) n.getParent()).removeAllViews();
        }
        this.m = o.a(o.a.HELVETICA_16_NORMAL, context);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(com.paypal.android.a.h.a("ANDROID_processing_transaction_message"));
        a9.addView(n);
        a9.addView(this.m);
        this.l.addView(a9);
        this.l.setVisibility(8);
        addView(this.l);
        if (preapproval.getPinRequired()) {
            this.k.setVisibility(8);
        } else {
            this.j.setVisibility(8);
        }
    }

    public final void a(com.paypal.android.MEP.b.a aVar, int i2) {
    }

    public final void a(com.paypal.android.b.g gVar, int i2) {
        if (i2 == 1 && this.b != null && gVar != this.b) {
            this.b.a(0);
        }
    }

    public final void a(String str, Object obj) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.b.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.g.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.b.a(boolean, boolean):void */
    public final void b() {
        if (this.a == a.STATE_CONFIRM_PREAPPROVAL) {
            this.b.a(false, true);
            this.j.setVisibility(8);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            n.a();
        } else if (this.a == a.STATE_PIN || this.a == a.STATE_REVIEW || this.a == a.STATE_ERROR) {
            this.b.a(true, false);
            n.b();
            this.l.setVisibility(8);
            if (this.a == a.STATE_PIN) {
                this.j.setVisibility(0);
                this.k.setVisibility(8);
            } else if (this.a == a.STATE_REVIEW) {
                this.j.setVisibility(8);
                this.k.setVisibility(0);
            } else if (this.k.getVisibility() == 0) {
                this.g.a(this.i);
                this.g.setVisibility(0);
            } else {
                this.h.a(this.i);
                this.h.setVisibility(0);
            }
        }
    }

    public final a c() {
        return this.a;
    }

    public final void d(String str) {
        if (this.a == a.STATE_CONFIRM_PREAPPROVAL) {
            this.i = str;
            a(a.STATE_ERROR);
        }
    }

    public final void l() {
    }

    public final void onClick(View view) {
        if (this.d == view || this.e == view) {
            new f(PayPalActivity.getInstance()).show();
        } else if (this.c == view) {
            a(a.STATE_CONFIRM_PREAPPROVAL);
            if (PayPal.getInstance().getServer() == 2) {
                PayPalActivity.getInstance().paymentSucceeded("Demo Preapproval Key", "COMPLETED", true);
                return;
            }
            com.paypal.android.a.b.e().a("delegate", this);
            com.paypal.android.a.b.e().a(14);
        } else if (this.f == view) {
            try {
                ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(findViewById(8001).getWindowToken(), 0);
            } catch (Exception e2) {
            }
            try {
                ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(findViewById(8002).getWindowToken(), 0);
            } catch (Exception e3) {
            }
            Editable text = ((EditText) findViewById(8001)).getText();
            Editable text2 = ((EditText) findViewById(8002)).getText();
            String obj = text.toString();
            String obj2 = text2.toString();
            boolean z = obj == null || obj2 == null;
            if (!obj.equals(obj2)) {
                z = true;
            }
            if (obj.length() < 4 || obj.length() > 8) {
                z = true;
            }
            for (int i2 = 0; i2 < obj.length(); i2++) {
                if (obj.charAt(i2) < '0' || obj.charAt(i2) > '9') {
                    z = true;
                }
            }
            if (z) {
                this.i = com.paypal.android.a.h.a("ANDROID_pin_invalid");
                a(a.STATE_ERROR);
                return;
            }
            com.paypal.android.a.b.e().a("Pin", obj);
            a(a.STATE_REVIEW);
        }
    }
}
