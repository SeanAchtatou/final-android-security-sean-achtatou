package com.paypal.android.MEP;

public class MEPAddress {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;

    public String getCity() {
        return this.c;
    }

    public String getCountrycode() {
        return this.f;
    }

    public String getPostalcode() {
        return this.e;
    }

    public String getState() {
        return this.d;
    }

    public String getStreet1() {
        return this.a;
    }

    public String getStreet2() {
        return this.b;
    }

    public void setCity(String str) {
        this.c = str;
    }

    public void setCountrycode(String str) {
        this.f = str;
    }

    public void setPostalcode(String str) {
        this.e = str;
    }

    public void setState(String str) {
        this.d = str;
    }

    public void setStreet1(String str) {
        this.a = str;
    }

    public void setStreet2(String str) {
        this.b = str;
    }
}
