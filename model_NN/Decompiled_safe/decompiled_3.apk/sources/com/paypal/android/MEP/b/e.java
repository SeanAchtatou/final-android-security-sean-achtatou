package com.paypal.android.MEP.b;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.a.h;

public final class e extends RelativeLayout implements View.OnClickListener {
    private boolean a;
    private int b;
    private LinearLayout c;
    private LinearLayout d;
    private ImageView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    private TextView i;
    private EditText j;
    private EditText k;
    private GradientDrawable l;

    public e(Context context) {
        super(context);
        int i2;
        PayPal instance = PayPal.getInstance();
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        setPadding(5, 5, 5, 5);
        this.c = new LinearLayout(context);
        this.c.setId(5001);
        this.c.setBackgroundColor(0);
        this.c.setOrientation(0);
        this.c.setGravity(3);
        this.c.setPadding(5, 0, 5, 0);
        if (!instance.isLightCountry()) {
            this.e = com.paypal.android.a.e.a(context, "tab-selected-email.png");
            this.e.setPadding(5, 0, 5, 0);
            this.e.setFocusable(false);
            this.c.addView(this.e);
            this.f = com.paypal.android.a.e.a(context, "tab-unselected-email.png");
            this.f.setPadding(5, 0, 5, 0);
            this.f.setOnClickListener(this);
            this.f.setFocusable(false);
            this.c.addView(this.f);
            this.g = com.paypal.android.a.e.a(context, "tab-selected-phone.png");
            this.g.setPadding(5, 0, 5, 0);
            this.g.setFocusable(false);
            this.c.addView(this.g);
            this.h = com.paypal.android.a.e.a(context, "tab-unselected-phone.png");
            this.h.setPadding(5, 0, 5, 0);
            this.h.setOnClickListener(this);
            this.h.setFocusable(false);
            this.c.addView(this.h);
            i2 = -1;
        } else {
            i2 = 0;
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10);
        layoutParams.addRule(9);
        this.c.setLayoutParams(layoutParams);
        this.d = new LinearLayout(context);
        this.d.setId(5002);
        this.l = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-789517, -3355444});
        this.l.setCornerRadius(5.0f);
        this.l.setStroke(1, -5197648);
        this.d.setBackgroundDrawable(this.l);
        this.d.setOrientation(1);
        this.d.setGravity(3);
        this.d.setPadding(10, 5, 10, 5);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(5, this.c.getId());
        layoutParams2.addRule(3, this.c.getId());
        layoutParams2.setMargins(0, i2, 0, 0);
        this.d.setLayoutParams(layoutParams2);
        this.i = new TextView(context);
        this.i.setId(5003);
        this.i.setTypeface(Typeface.create("Helvetica", 1));
        this.i.setTextSize(16.0f);
        this.i.setTextColor(-16777216);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.setMargins(0, 2, 0, 2);
        this.i.setLayoutParams(layoutParams3);
        this.d.addView(this.i);
        this.j = new EditText(context);
        this.j.setId(5004);
        this.j.setLayoutParams(layoutParams3);
        this.j.setSingleLine(true);
        this.d.addView(this.j);
        this.k = new EditText(context);
        this.k.setId(5005);
        this.k.setLayoutParams(layoutParams3);
        this.k.setSingleLine(true);
        this.d.addView(this.k);
        addView(this.c);
        addView(this.d);
        if (!Build.VERSION.SDK.equals("3")) {
            bringChildToFront(this.c);
        }
        PayPal.logd("LoginWidget", "Setup login tab, authMethod " + Integer.toString(instance.getAuthMethod()));
        if (instance.getAuthMethod() != 1 || !instance.isHeavyCountry()) {
            a(instance);
            return;
        }
        this.a = false;
        b(instance);
    }

    private void a(PayPal payPal) {
        PayPal.logd("LoginWidget", "Setup login tab for email");
        if (!payPal.isLightCountry()) {
            this.e.setVisibility(0);
            this.f.setVisibility(8);
            this.g.setVisibility(8);
            this.h.setVisibility(0);
        }
        this.a = true;
        this.i.setText(h.a("ANDROID_login_with_email_and_password") + ":");
        this.j.setVisibility(0);
        this.j.setHint(h.a("ANDROID_email_field"));
        this.j.setInputType(32);
        this.j.setText(payPal.getAccountEmail());
        this.k.setText("");
        this.k.setHint(h.a("ANDROID_password_field"));
        this.k.setInputType(128);
        this.k.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.b = 0;
    }

    private void b(PayPal payPal) {
        int i2 = 0;
        PayPal.logd("LoginWidget", "Setup login tab for PIN");
        if (!payPal.isLightCountry()) {
            this.e.setVisibility(8);
            this.f.setVisibility(0);
            this.g.setVisibility(0);
            this.h.setVisibility(8);
        }
        if (this.a) {
            this.i.setText(h.a("ANDROID_login_with_phone_and_pin") + ":");
        } else {
            this.i.setText(h.a("ANDROID_login_with_pin") + ":");
        }
        EditText editText = this.j;
        if (!this.a) {
            i2 = 8;
        }
        editText.setVisibility(i2);
        this.j.setHint(h.a("ANDROID_phone_field"));
        this.j.setInputType(3);
        this.j.setText(payPal.getAccountPhone());
        this.k.setText("");
        this.k.setHint(h.a("ANDROID_pin_field"));
        this.k.setInputType(3);
        this.k.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.b = 1;
    }

    public final String a() {
        return this.j.getText().toString().replace(" ", "");
    }

    public final String b() {
        return this.k.getText().toString();
    }

    public final EditText c() {
        return this.j;
    }

    public final EditText d() {
        return this.k;
    }

    public final void e() {
        try {
            ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(this.j.getWindowToken(), 0);
        } catch (Exception e2) {
        }
        try {
            ((InputMethodManager) PayPalActivity.getInstance().getSystemService("input_method")).hideSoftInputFromWindow(this.k.getWindowToken(), 0);
        } catch (Exception e3) {
        }
    }

    public final void onClick(View view) {
        if (view == this.f) {
            if (this.b != 0) {
                a(PayPal.getInstance());
            }
        } else if (view == this.h && this.b != 1) {
            b(PayPal.getInstance());
        }
    }
}
