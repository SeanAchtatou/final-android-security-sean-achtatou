package com.paypal.android.MEP.b;

import android.content.Intent;
import android.view.View;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a.a;
import com.paypal.android.MEP.a.d;
import com.paypal.android.a.b;

class d implements View.OnClickListener {
    private /* synthetic */ a a;

    d(a aVar) {
        this.a = aVar;
    }

    public final void onClick(View view) {
        int id = view.getId();
        if (this.a.f != null || (id & 2113929216) == 2113929216) {
            if ((id & 2130706432) == 2130706432) {
                int i = id - 2130706432;
                if (this.a.f != null) {
                    (PayPal.getInstance().getServer() == 2 ? a.a : b.e().g()).put("FundingPlanId", this.a.p.get(i));
                    PayPalActivity.getInstance().sendBroadcast(new Intent(PayPalActivity.CREATE_PAYMENT_SUCCESS));
                }
            } else if ((id & 2113929216) == 2113929216) {
                b.e().a("FeeBearer", id - 2113929216 == 0 ? "ApplyFeeToSender" : "ApplyFeeToReceiver");
            } else {
                (PayPal.getInstance().getServer() == 2 ? a.a : b.e().g()).put("ShippingAddressId", (String) this.a.o.get(id - 2097152000));
                if ((PayPal.getInstance().getServer() != 2 ? b.e().k() : 0) == 0) {
                    PayPalActivity.getInstance().sendBroadcast(new Intent(PayPalActivity.CREATE_PAYMENT_SUCCESS));
                } else {
                    this.a.n.a(a.C0004a.STATE_UPDATING);
                }
            }
            this.a.e = true;
            this.a.a(0);
            if (PayPal.getInstance().getServer() == 2) {
                this.a.a(6, (Object) null);
            }
            d.AnonymousClass1.b();
        }
    }
}
