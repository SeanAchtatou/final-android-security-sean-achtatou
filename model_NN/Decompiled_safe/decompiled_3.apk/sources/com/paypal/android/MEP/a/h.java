package com.paypal.android.MEP.a;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.b.b;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.o;
import com.paypal.android.b.f;
import com.paypal.android.b.g;
import com.paypal.android.b.j;

public final class h extends j implements View.OnClickListener, g.a {
    public static String a = null;
    private b b;
    private Button c;

    public h(Context context) {
        super(context);
    }

    public final void a() {
    }

    public final void a(Context context) {
        PayPal instance = PayPal.getInstance();
        super.a(context);
        LinearLayout a2 = d.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        this.b = new b(context, this);
        this.b.a(this);
        a2.addView(this.b);
        addView(a2);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout.setBackgroundDrawable(d.a());
        if (instance.getTextType() == 1) {
            linearLayout.addView(new com.paypal.android.b.h(com.paypal.android.a.h.a("ANDROID_donation_made"), context));
        } else {
            linearLayout.addView(new com.paypal.android.b.h(com.paypal.android.a.h.a("ANDROID_payment_made"), context));
        }
        f fVar = new f(context, o.a.HELVETICA_16_NORMAL, o.a.HELVETICA_16_NORMAL);
        fVar.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        String a3 = com.paypal.android.a.h.a("ANDROID_successfully_paid_amount_to_recipient");
        if (instance.getTextType() == 1) {
            a3 = com.paypal.android.a.h.a("ANDROID_successfully_donated_amount_to_recipient");
        }
        fVar.a(a3.replace("{1}", instance.getPayment().getTotal().toString()) + ".");
        linearLayout.addView(fVar);
        LinearLayout a4 = d.a(context, -1, -2);
        a4.setOrientation(1);
        a4.setGravity(1);
        this.c = new Button(context);
        this.c.setText(com.paypal.android.a.h.a("ANDROID_done"));
        this.c.setLayoutParams(new RelativeLayout.LayoutParams(-2, d.b()));
        this.c.setBackgroundDrawable(e.a());
        this.c.setTextColor(-16777216);
        this.c.setOnClickListener(this);
        a4.addView(this.c);
        linearLayout.addView(a4);
        addView(linearLayout);
    }

    public final void a(g gVar, int i) {
    }

    public final void b() {
    }

    public final void onClick(View view) {
        if (view == this.c) {
            if (a == null || a.length() == 0) {
                a = "1111111";
            }
            PayPalActivity.getInstance().paymentSucceeded((String) com.paypal.android.a.b.e().c("PayKey"), (String) com.paypal.android.a.b.e().c("PaymentExecStatus"), true);
        }
    }
}
