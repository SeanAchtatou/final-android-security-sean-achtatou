package com.paypal.android.MEP;

public final class a {
    private static a a = null;

    /* renamed from: com.paypal.android.MEP.a$a  reason: collision with other inner class name */
    private class C0003a extends Thread {
        private b a;

        public C0003a(a aVar, b bVar) {
            this.a = bVar;
        }

        public final void run() {
            try {
                Thread.yield();
                Thread.sleep(3000);
            } catch (Throwable th) {
            }
            this.a.l();
        }
    }

    public interface b {
        void a(int i, Object obj);

        void a(String str, Object obj);

        void d(String str);

        void l();
    }

    private a() {
    }

    public static a a() {
        if (a == null) {
            if (a != null) {
                throw new IllegalStateException("Attempted to initialize PPMobileAPIInterface more than once.");
            }
            a = new a();
        }
        return a;
    }

    public final void a(b bVar) {
        new C0003a(this, bVar).start();
    }

    public final void a(b bVar, String str, String str2) {
        C0003a aVar = new C0003a(this, bVar);
        bVar.a("usernameOrPhone", str);
        bVar.a("passwordOrPin", str2);
        aVar.start();
    }

    public final void b(b bVar, String str, String str2) {
        C0003a aVar = new C0003a(this, bVar);
        bVar.a("mobileNumber", str);
        bVar.a("newPIN", str2);
        aVar.start();
    }
}
