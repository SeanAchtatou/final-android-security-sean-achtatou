package com.paypal.android.MEP.a;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a;
import com.paypal.android.MEP.a.d;
import com.paypal.android.MEP.b.a;
import com.paypal.android.MEP.b.b;
import com.paypal.android.MEP.b.f;
import com.paypal.android.a.d;
import com.paypal.android.a.o;
import com.paypal.android.b.e;
import com.paypal.android.b.g;
import com.paypal.android.b.h;
import com.paypal.android.b.i;
import com.paypal.android.b.j;
import java.util.Hashtable;

public final class a extends j implements View.OnClickListener, a.b, a.b, g.a {
    public static Hashtable<String, Object> a = null;
    private static e n = null;
    private C0004a b;
    private b c;
    private Button d;
    private Button e;
    private com.paypal.android.MEP.b.a f;
    private com.paypal.android.MEP.b.a g;
    private com.paypal.android.MEP.b.a h;
    private i i;
    private String j;
    private LinearLayout k;
    private RelativeLayout l;
    private TextView m = null;
    private Context o;

    /* renamed from: com.paypal.android.MEP.a.a$a  reason: collision with other inner class name */
    public enum C0004a {
        STATE_NORMAL,
        STATE_SENDING_PAYMENT,
        STATE_ERROR,
        STATE_UPDATING
    }

    public a(Context context) {
        super(context);
        this.o = context;
    }

    private void a(String str) {
        LinearLayout a2 = d.a(this.o, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2.getLayoutParams());
        layoutParams.addRule(13);
        a2.setLayoutParams(layoutParams);
        a2.setOrientation(1);
        a2.setGravity(1);
        if (n == null) {
            n = new e(this.o);
        } else {
            ((LinearLayout) n.getParent()).removeAllViews();
        }
        this.m = o.a(o.a.HELVETICA_16_NORMAL, this.o);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(str);
        a2.addView(n);
        a2.addView(this.m);
        this.l.removeAllViews();
        this.l.addView(a2);
    }

    public final void a() {
    }

    public final void a(int i2, Object obj) {
        if (this.b == C0004a.STATE_SENDING_PAYMENT) {
            PayPal instance = PayPal.getInstance();
            PayPalActivity instance2 = PayPalActivity.getInstance();
            com.paypal.android.a.b e2 = com.paypal.android.a.b.e();
            String str = (String) e2.c("PayKey");
            String str2 = (String) e2.c("PaymentExecStatus");
            if (instance.getServer() == 2) {
                instance2.setTransactionSuccessful(true);
                instance2.paymentSucceeded(str, str2, false);
                e.a = (String) obj;
                d.AnonymousClass1.b(7);
            } else if (instance.hasCreatedPIN() || instance.isLightCountry() || instance.getPayType() == 3) {
                instance2.paymentSucceeded(str, str2, true);
            } else {
                instance2.setTransactionSuccessful(true);
                instance2.paymentSucceeded(str, str2, false);
                e.a = (String) obj;
                d.AnonymousClass1.b(7);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.b.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.g.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.b.a(boolean, boolean):void */
    public final void a(Context context) {
        PayPal instance = PayPal.getInstance();
        super.a(context);
        this.o = context;
        this.b = C0004a.STATE_NORMAL;
        LinearLayout a2 = com.paypal.android.a.d.a(context, -1, -2);
        a2.setOrientation(1);
        a2.setPadding(5, 5, 5, 15);
        a2.addView(o.b(o.a.HELVETICA_16_BOLD, context));
        this.c = new b(context, this);
        this.c.a(this);
        a2.addView(this.c);
        addView(a2);
        this.k = new LinearLayout(context);
        this.k.setOrientation(1);
        this.k.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.k.setPadding(5, 5, 5, 5);
        this.k.setBackgroundDrawable(com.paypal.android.a.d.a());
        this.k.addView(new h(com.paypal.android.a.h.a("ANDROID_review"), context));
        LinearLayout a3 = com.paypal.android.a.d.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setPadding(5, 10, 5, 10);
        this.i = new i(context, i.a.YELLOW_ALERT);
        this.i.a("This page is currently being used to test components.");
        this.i.setPadding(0, 5, 0, 5);
        this.i.setVisibility(8);
        a3.addView(this.i);
        this.k.addView(a3);
        this.g = new com.paypal.android.MEP.b.a(context, a.C0005a.PAYMENT_DETAILS_FEES, this);
        this.g.a((g.a) this);
        this.g.setPadding(0, 5, 0, 5);
        this.g.a((a.b) this);
        if (PayPal.getInstance().shouldShowFees()) {
            this.k.addView(this.g);
        }
        this.f = new com.paypal.android.MEP.b.a(context, a.C0005a.PAYMENT_DETAILS_FUNDING, this);
        this.f.a((g.a) this);
        this.f.setPadding(0, 5, 0, 5);
        this.f.a((a.b) this);
        this.k.addView(this.f);
        this.h = new com.paypal.android.MEP.b.a(context, a.C0005a.PAYMENT_DETAILS_SHIPPING, this);
        this.h.a((g.a) this);
        this.h.setPadding(0, 5, 0, 5);
        this.h.a((a.b) this);
        if (instance.getShippingEnabled()) {
            this.k.addView(this.h);
        }
        LinearLayout a4 = com.paypal.android.a.d.a(context, -1, -2);
        a4.setGravity(1);
        a4.setPadding(5, 10, 5, 5);
        this.e = new Button(context);
        if (instance.getTextType() == 1) {
            this.e.setText(com.paypal.android.a.h.a("ANDROID_donate"));
        } else {
            this.e.setText(com.paypal.android.a.h.a("ANDROID_pay"));
        }
        this.e.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f));
        this.e.setGravity(17);
        this.e.setBackgroundDrawable(com.paypal.android.a.e.a());
        this.e.setTextColor(-16777216);
        this.e.setOnClickListener(this);
        a4.addView(this.e);
        this.k.addView(a4);
        LinearLayout a5 = com.paypal.android.a.d.a(context, -1, -2);
        a5.setGravity(1);
        a5.setPadding(5, 5, 5, 10);
        this.d = new Button(context);
        this.d.setText(com.paypal.android.a.h.a("ANDROID_cancel"));
        this.d.setLayoutParams(new LinearLayout.LayoutParams(-1, com.paypal.android.a.d.b(), 0.5f));
        this.d.setGravity(17);
        this.d.setBackgroundDrawable(com.paypal.android.a.e.b());
        this.d.setTextColor(-16777216);
        this.d.setOnClickListener(this);
        a5.addView(this.d);
        this.k.addView(a5);
        addView(this.k);
        this.l = new RelativeLayout(context);
        this.l.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.l.setBackgroundDrawable(com.paypal.android.a.d.a());
        LinearLayout a6 = com.paypal.android.a.d.a(context, -1, -2);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a6.getLayoutParams());
        layoutParams.addRule(13);
        a6.setLayoutParams(layoutParams);
        a6.setOrientation(1);
        a6.setGravity(1);
        if (n == null) {
            n = new e(context);
        } else {
            ((LinearLayout) n.getParent()).removeAllViews();
        }
        this.m = o.a(o.a.HELVETICA_16_NORMAL, context);
        this.m.setGravity(1);
        this.m.setTextColor(-13408615);
        this.m.setText(com.paypal.android.a.h.a("ANDROID_processing_transaction_message"));
        a6.addView(n);
        a6.addView(this.m);
        this.l.addView(a6);
        this.l.setVisibility(8);
        addView(this.l);
        if (!PayPal.getInstance().canShowCart()) {
            this.c.a(false, false);
        }
    }

    public final void a(C0004a aVar) {
        this.b = aVar;
        d.AnonymousClass1.b();
    }

    public final void a(com.paypal.android.MEP.b.a aVar, int i2) {
        switch (aVar.b()) {
            case PAYMENT_DETAILS_FEES:
                if (this.f != null) {
                    this.f.setNextFocusUpId(i2);
                    return;
                }
                return;
            case PAYMENT_DETAILS_FUNDING:
                if (this.h != null) {
                    this.h.setNextFocusUpId(i2);
                    return;
                }
                return;
            case PAYMENT_DETAILS_SHIPPING:
                if (this.e != null) {
                    this.e.setNextFocusUpId(i2);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void a(g gVar, int i2) {
        if (i2 == 1) {
            if (!(this.c == null || gVar == this.c)) {
                this.c.a(0);
            }
            if (!(this.f == null || gVar == this.f)) {
                this.f.a(0);
            }
            if (!(this.g == null || gVar == this.g)) {
                this.g.a(0);
            }
            if (this.h != null && gVar != this.h) {
                this.h.a(0);
            }
        }
    }

    public final void a(String str, Object obj) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.MEP.b.b.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.paypal.android.b.g.a(android.widget.LinearLayout$LayoutParams, int):void
      com.paypal.android.MEP.b.b.a(boolean, boolean):void */
    public final void b() {
        if (this.f != null) {
            this.f.c();
        }
        if (this.g != null) {
            this.g.c();
        }
        if (this.h != null) {
            this.h.c();
        }
        if (this.b == C0004a.STATE_SENDING_PAYMENT) {
            a(com.paypal.android.a.h.a("ANDROID_processing_transaction_message"));
            this.c.a(false, true);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            n.a();
        } else if (this.b == C0004a.STATE_UPDATING) {
            a(com.paypal.android.a.h.a("ANDROID_getting_information"));
            this.c.a(false, true);
            this.k.setVisibility(8);
            this.l.setVisibility(0);
            n.a();
        } else if (this.b == C0004a.STATE_NORMAL || this.b == C0004a.STATE_ERROR) {
            if (PayPal.getInstance().canShowCart()) {
                this.c.a(true, false);
            } else {
                this.c.a(false, false);
            }
            n.b();
            this.l.setVisibility(8);
            this.k.setVisibility(0);
            if (this.b == C0004a.STATE_ERROR) {
                this.i.a(this.j);
                this.i.setVisibility(0);
            }
        }
    }

    public final C0004a c() {
        return this.b;
    }

    public final void d() {
        if (this.c != null) {
            this.c.a(0);
        }
        if (this.f != null) {
            this.f.a(0);
        }
        if (this.g != null) {
            this.g.a(0);
        }
        if (this.h != null) {
            this.h.a(0);
        }
    }

    public final void d(String str) {
        if (this.b == C0004a.STATE_SENDING_PAYMENT) {
            this.j = str;
            a(C0004a.STATE_ERROR);
        }
    }

    public final void l() {
        com.paypal.android.a.b.e().a("delegate", this);
        com.paypal.android.a.b.e().a(4);
    }

    public final void onClick(View view) {
        if (this.d == view) {
            new f(PayPalActivity.getInstance()).show();
        } else if (this.e == view) {
            a(C0004a.STATE_SENDING_PAYMENT);
            if (PayPal.getInstance().getServer() == 2) {
                a(4, "10088342");
            } else {
                com.paypal.android.MEP.a.a().a(this);
            }
        } else if ((view.getId() & 2130706432) == 2130706432) {
            this.f.a(0);
        } else if ((view.getId() & 2113929216) == 2113929216) {
            this.g.a(0);
        } else if ((view.getId() & 2097152000) == 2097152000) {
            this.h.a(0);
        }
    }
}
