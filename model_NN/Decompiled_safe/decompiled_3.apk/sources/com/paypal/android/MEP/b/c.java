package com.paypal.android.MEP.b;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPalInvoiceItem;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.h;
import com.paypal.android.a.l;
import com.paypal.android.a.o;
import com.paypal.android.b.f;
import java.math.BigDecimal;

public final class c extends com.paypal.android.b.c implements View.OnTouchListener {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public c(Context context, PayPalReceiverDetails payPalReceiverDetails, String str) {
        super(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(3, 3, 3, 3);
        a(layoutParams, 0);
        a(layoutParams, 1);
        setBackgroundColor(0);
        a(e.a(130087, 2921));
        b(e.a(63352, 2927));
        setOnTouchListener(this);
        this.a.setPadding(5, 0, 5, 0);
        this.a.setBackgroundColor(-2763307);
        this.a.setGravity(16);
        this.a.addView(this.c);
        this.c.setVisibility(0);
        f fVar = new f(context, o.a.HELVETICA_14_BOLD, o.a.HELVETICA_14_BOLD);
        fVar.setPadding(3, 0, 0, 0);
        String merchantName = payPalReceiverDetails.getMerchantName();
        merchantName = (merchantName == null || merchantName.length() == 0) ? payPalReceiverDetails.getRecipient() : merchantName;
        String a = l.a(payPalReceiverDetails.getTotal(), str);
        fVar.a(merchantName);
        fVar.b(a);
        this.a.addView(fVar);
        this.b.setPadding(10, 0, 0, 0);
        this.b.setOrientation(1);
        if (payPalReceiverDetails.getInvoiceData() == null || payPalReceiverDetails.getInvoiceData().getInvoiceItems() == null || payPalReceiverDetails.getInvoiceData().getInvoiceItems().size() <= 0) {
            BigDecimal subtotal = payPalReceiverDetails.getSubtotal();
            if (subtotal != null) {
                f fVar2 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
                fVar2.a(h.a("ANDROID_total"));
                fVar2.b(l.a(subtotal, str));
                this.b.addView(fVar2);
            }
        } else {
            for (int i = 0; i < payPalReceiverDetails.getInvoiceData().getInvoiceItems().size(); i++) {
                PayPalInvoiceItem payPalInvoiceItem = payPalReceiverDetails.getInvoiceData().getInvoiceItems().get(i);
                String name = payPalInvoiceItem.getName();
                String id = payPalInvoiceItem.getID();
                BigDecimal totalPrice = payPalInvoiceItem.getTotalPrice();
                BigDecimal unitPrice = payPalInvoiceItem.getUnitPrice();
                int quantity = payPalInvoiceItem.getQuantity();
                if (payPalInvoiceItem.isValid()) {
                    if (name != null && name.length() > 0) {
                        f fVar3 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
                        fVar3.a(h.a("ANDROID_item") + ": " + payPalInvoiceItem.getName());
                        if (totalPrice == null || totalPrice.toString().length() <= 0) {
                            fVar3.b("");
                        } else {
                            fVar3.b(l.a(totalPrice, str));
                        }
                        this.b.addView(fVar3);
                    } else if (totalPrice != null && totalPrice.compareTo(BigDecimal.ZERO) > 0) {
                        f fVar4 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
                        fVar4.a(h.a("ANDROID_item") + ": " + h.a("ANDROID_item") + " " + (i + 1));
                        fVar4.b(totalPrice.toString());
                        this.b.addView(fVar4);
                    }
                    if (id != null && id.length() > 0) {
                        TextView a2 = o.a(o.a.HELVETICA_12_NORMAL, context);
                        a2.setText(h.a("ANDROID_item_num") + ": " + id);
                        this.b.addView(a2);
                    }
                    if (unitPrice != null && unitPrice.compareTo(BigDecimal.ZERO) > 0) {
                        TextView a3 = o.a(o.a.HELVETICA_12_NORMAL, context);
                        a3.setText(h.a("ANDROID_item_price") + ": " + l.a(unitPrice, str));
                        this.b.addView(a3);
                    }
                    if (quantity > 0) {
                        TextView a4 = o.a(o.a.HELVETICA_12_NORMAL, context);
                        a4.setText(h.a("ANDROID_quantity") + ": " + quantity);
                        this.b.addView(a4);
                    }
                    if (i != payPalReceiverDetails.getInvoiceData().getInvoiceItems().size() - 1) {
                        LinearLayout a5 = d.a(context, 5, 5);
                        a5.setVisibility(4);
                        this.b.addView(a5);
                    }
                }
            }
        }
        BigDecimal tax = payPalReceiverDetails.getInvoiceData() != null ? payPalReceiverDetails.getInvoiceData().getTax() : null;
        if (tax != null && tax.compareTo(BigDecimal.ZERO) > 0) {
            f fVar5 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
            fVar5.a(h.a("ANDROID_tax"));
            fVar5.b(l.a(tax, str));
            this.b.addView(fVar5);
        }
        BigDecimal shipping = payPalReceiverDetails.getInvoiceData() != null ? payPalReceiverDetails.getInvoiceData().getShipping() : null;
        if (shipping != null && shipping.compareTo(BigDecimal.ZERO) > 0) {
            f fVar6 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
            fVar6.a(h.a("ANDROID_shipping"));
            fVar6.b(l.a(shipping, str));
            this.b.addView(fVar6);
        }
    }

    public static LinearLayout a(Context context, PayPalReceiverDetails payPalReceiverDetails, String str) {
        int i = 0;
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.gravity = 1;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setPadding(5, 0, 5, 5);
        linearLayout.setPadding(10, 0, 0, 0);
        linearLayout.setOrientation(1);
        if (payPalReceiverDetails.getInvoiceData() != null && payPalReceiverDetails.getInvoiceData().getInvoiceItems() != null && payPalReceiverDetails.getInvoiceData().getInvoiceItems().size() > 0) {
            while (true) {
                int i2 = i;
                if (i2 >= payPalReceiverDetails.getInvoiceData().getInvoiceItems().size()) {
                    break;
                }
                PayPalInvoiceItem payPalInvoiceItem = payPalReceiverDetails.getInvoiceData().getInvoiceItems().get(i2);
                String name = payPalInvoiceItem.getName();
                String id = payPalInvoiceItem.getID();
                BigDecimal totalPrice = payPalInvoiceItem.getTotalPrice();
                BigDecimal unitPrice = payPalInvoiceItem.getUnitPrice();
                int quantity = payPalInvoiceItem.getQuantity();
                if (payPalInvoiceItem.isValid()) {
                    if (name != null && name.length() > 0) {
                        f fVar = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
                        fVar.a(h.a("ANDROID_item") + ": " + payPalInvoiceItem.getName());
                        if (totalPrice == null || totalPrice.toString().length() <= 0) {
                            fVar.b("");
                        } else {
                            fVar.b(l.a(totalPrice, str));
                        }
                        linearLayout.addView(fVar);
                    } else if (totalPrice != null && totalPrice.compareTo(BigDecimal.ZERO) > 0) {
                        f fVar2 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
                        fVar2.a(h.a("ANDROID_item") + ": " + h.a("ANDROID_item") + " " + (i2 + 1));
                        fVar2.b(totalPrice.toString());
                        linearLayout.addView(fVar2);
                    }
                    if (id != null && id.length() > 0) {
                        TextView a = o.a(o.a.HELVETICA_12_NORMAL, context);
                        a.setText(h.a("ANDROID_item_num") + ": " + id);
                        linearLayout.addView(a);
                    }
                    if (unitPrice != null && unitPrice.compareTo(BigDecimal.ZERO) > 0) {
                        TextView a2 = o.a(o.a.HELVETICA_12_NORMAL, context);
                        a2.setText(h.a("ANDROID_item_price") + ": " + l.a(unitPrice, str));
                        linearLayout.addView(a2);
                    }
                    if (quantity > 0) {
                        TextView a3 = o.a(o.a.HELVETICA_12_NORMAL, context);
                        a3.setText(h.a("ANDROID_quantity") + ": " + quantity);
                        linearLayout.addView(a3);
                    }
                    if (i2 != payPalReceiverDetails.getInvoiceData().getInvoiceItems().size() - 1) {
                        LinearLayout a4 = d.a(context, 5, 5);
                        a4.setVisibility(4);
                        linearLayout.addView(a4);
                    }
                }
                i = i2 + 1;
            }
        } else {
            BigDecimal subtotal = payPalReceiverDetails.getSubtotal();
            if (subtotal != null) {
                f fVar3 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
                fVar3.a(h.a("ANDROID_total"));
                fVar3.b(l.a(subtotal, str));
                linearLayout.addView(fVar3);
            }
        }
        BigDecimal tax = payPalReceiverDetails.getInvoiceData() != null ? payPalReceiverDetails.getInvoiceData().getTax() : null;
        if (tax != null && tax.compareTo(BigDecimal.ZERO) > 0) {
            f fVar4 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
            fVar4.a(h.a("ANDROID_tax"));
            fVar4.b(l.a(tax, str));
            linearLayout.addView(fVar4);
        }
        BigDecimal shipping = payPalReceiverDetails.getInvoiceData() != null ? payPalReceiverDetails.getInvoiceData().getShipping() : null;
        if (shipping != null && shipping.compareTo(BigDecimal.ZERO) > 0) {
            f fVar5 = new f(context, o.a.HELVETICA_14_NORMAL, o.a.HELVETICA_14_NORMAL);
            fVar5.a(h.a("ANDROID_shipping"));
            fVar5.b(l.a(shipping, str));
            linearLayout.addView(fVar5);
        }
        return linearLayout;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
