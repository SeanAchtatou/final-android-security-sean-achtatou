package com.paypal.android.MEP.b;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.biznessapps.constants.AppConstants;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.a;
import com.paypal.android.MEP.a.d;
import com.paypal.android.a.a.c;
import com.paypal.android.a.a.g;
import com.paypal.android.a.d;
import com.paypal.android.a.h;
import com.paypal.android.a.o;
import com.paypal.android.b.e;
import com.paypal.android.b.k;
import java.util.Hashtable;
import java.util.Vector;

public final class a extends k implements View.OnTouchListener, a.b {
    private static e l = null;
    private static boolean q = false;
    boolean e = false;
    Vector<Hashtable> f = null;
    private GradientDrawable g;
    private GradientDrawable h;
    private C0005a i;
    private LinearLayout j;
    private LinearLayout k = null;
    private TextView m = null;
    /* access modifiers changed from: private */
    public com.paypal.android.MEP.a.a n = null;
    /* access modifiers changed from: private */
    public Vector<String> o = new Vector<>(3);
    /* access modifiers changed from: private */
    public Vector<String> p = new Vector<>(3);
    private int r = 0;
    private b s = null;
    private View.OnClickListener t = new d(this);
    private String u = null;

    /* renamed from: com.paypal.android.MEP.b.a$a  reason: collision with other inner class name */
    public enum C0005a {
        PAYMENT_DETAILS_FUNDING,
        PAYMENT_DETAILS_FEES,
        PAYMENT_DETAILS_SHIPPING
    }

    public interface b {
        void a(a aVar, int i);
    }

    public a(Context context, C0005a aVar, com.paypal.android.MEP.a.a aVar2) {
        super(context);
        setOnTouchListener(this);
        this.n = aVar2;
        this.i = aVar;
        setBackgroundColor(-16711681);
        a(new LinearLayout.LayoutParams(-1, -2), 0);
        a(new LinearLayout.LayoutParams(-1, -2), 1);
        this.g = d.a(-1, -1510918, -3154193);
        this.h = d.a(-1, -4336918, -3154193);
        setBackgroundDrawable(this.g);
        setGravity(16);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-10066330, -3487030});
        gradientDrawable.setCornerRadii(new float[]{0.0f, 0.0f, 0.0f, 0.0f, 5.0f, 5.0f, 5.0f, 5.0f});
        gradientDrawable.setStroke(2, -10066330);
        c(gradientDrawable);
        a(com.paypal.android.a.e.a(111410, 464));
        b(com.paypal.android.a.e.a(118786, 430));
        this.a.setOrientation(1);
        LinearLayout a = d.a(context, -1, -2);
        a.setOrientation(0);
        a.setGravity(16);
        TextView a2 = o.a(o.a.HELVETICA_14_BOLD, context);
        a2.setText(this.i == C0005a.PAYMENT_DETAILS_FUNDING ? h.a("ANDROID_funding") + ":" : this.i == C0005a.PAYMENT_DETAILS_FEES ? h.a("ANDROID_fee") + ":" : PayPal.getInstance().getTextType() == 1 ? h.a("ANDROID_mailing_address") + ":" : h.a("ANDROID_ship_to") + ":");
        a2.setGravity(3);
        a.addView(a2);
        a.addView(this.c);
        this.c.setVisibility(0);
        this.a.addView(a);
        this.k = d.a(context, -1, -2);
        this.k.setOrientation(1);
        a(context, this.k);
        this.a.addView(this.k);
        this.j = d.a(context, -1, -2);
        this.j.setOrientation(0);
        this.j.setGravity(16);
        LinearLayout a3 = d.a(context, -1, -2);
        a3.setOrientation(1);
        a3.setGravity(1);
        this.m = o.a(o.a.HELVETICA_12_NORMAL, context);
        this.m.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m.setTextColor(-13408615);
        this.m.setText(h.a("ANDROID_getting_information"));
        this.m.setGravity(1);
        this.m.setSingleLine(false);
        a3.addView(this.m);
        if (this.i == C0005a.PAYMENT_DETAILS_FUNDING) {
            if (l == null) {
                l = new e(context);
            } else {
                ((LinearLayout) l.getParent()).removeAllViews();
            }
            l.setGravity(1);
            this.j.addView(l);
            this.j.addView(a3);
            this.j.setVisibility(8);
        }
        this.a.addView(this.j);
        if (this.i == C0005a.PAYMENT_DETAILS_FEES) {
            a(false);
        }
        q = false;
    }

    private Button a(Context context, String str, int i2) {
        Button button = new Button(context);
        button.setText(str);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, d.b());
        layoutParams.setMargins(0, 3, 0, 2);
        button.setLayoutParams(layoutParams);
        button.setGravity(17);
        StateListDrawable stateListDrawable = new StateListDrawable();
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-328451, -4336918});
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-6702886, -11966331});
        GradientDrawable gradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-13605994, -16764058});
        GradientDrawable gradientDrawable4 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{2140780762, 2135517317});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(1, -6307088);
        gradientDrawable2.setCornerRadius(5.0f);
        gradientDrawable2.setStroke(1, -10650469);
        gradientDrawable3.setCornerRadius(5.0f);
        gradientDrawable3.setStroke(1, -16764058);
        gradientDrawable4.setCornerRadius(5.0f);
        gradientDrawable4.setStroke(1, 2136833179);
        stateListDrawable.addState(new int[]{16842910, -16842919, -16842908}, gradientDrawable);
        stateListDrawable.addState(new int[]{16842910, -16842919, 16842908}, gradientDrawable2);
        stateListDrawable.addState(new int[]{16842910, 16842919, -16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{16842910, 16842919, 16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{-16842910, -16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, -16842919, 16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, 16842908}, gradientDrawable4);
        button.setBackgroundDrawable(stateListDrawable);
        button.setTextColor(-16777216);
        button.setFocusable(true);
        button.setOnClickListener(this.t);
        if (this.i == C0005a.PAYMENT_DETAILS_FUNDING) {
            button.setId(2130706432 | i2);
        } else if (this.i == C0005a.PAYMENT_DETAILS_FEES) {
            button.setId(2113929216 | i2);
        } else if (this.i == C0005a.PAYMENT_DETAILS_SHIPPING) {
            button.setId(2097152000 | i2);
        }
        return button;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v20, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: com.paypal.android.a.a.c} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r15, android.widget.LinearLayout r16) {
        /*
            r14 = this;
            com.paypal.android.MEP.PayPal r1 = com.paypal.android.MEP.PayPal.getInstance()
            int r1 = r1.getServer()
            r2 = 2
            if (r1 != r2) goto L_0x016e
            java.util.Hashtable<java.lang.String, java.lang.Object> r1 = com.paypal.android.MEP.a.a.a
            r3 = r1
        L_0x000e:
            com.paypal.android.MEP.b.a$a r1 = r14.i
            com.paypal.android.MEP.b.a$a r2 = com.paypal.android.MEP.b.a.C0005a.PAYMENT_DETAILS_FUNDING
            if (r1 != r2) goto L_0x0451
            java.lang.String r1 = "FundingPlans"
            java.lang.Object r1 = r3.get(r1)
            java.util.Vector r1 = (java.util.Vector) r1
            if (r1 == 0) goto L_0x0024
            int r2 = r1.size()
            if (r2 != 0) goto L_0x0179
        L_0x0024:
            java.lang.String r2 = "DefaultFundingPlan"
            java.lang.Object r2 = r3.get(r2)
            com.paypal.android.a.a.c r2 = (com.paypal.android.a.a.c) r2
            r3 = r2
        L_0x002d:
            r2 = 0
            r7 = r2
        L_0x002f:
            java.util.Vector<com.paypal.android.a.a.k> r2 = r3.d
            int r2 = r2.size()
            if (r7 >= r2) goto L_0x03c3
            java.util.Vector<com.paypal.android.a.a.k> r2 = r3.d
            java.lang.Object r2 = r2.get(r7)
            com.paypal.android.a.a.k r2 = (com.paypal.android.a.a.k) r2
            com.paypal.android.a.a.a r4 = r2.a
            java.lang.String r6 = r4.b()
            com.paypal.android.a.a.a r4 = r2.a
            java.math.BigDecimal r8 = r4.a()
            com.paypal.android.a.a.g r4 = r2.b
            java.lang.String r9 = r4.c()
            com.paypal.android.a.a.g r4 = r2.b
            java.lang.String r4 = r4.b()
            if (r4 != 0) goto L_0x005b
            java.lang.String r4 = ""
        L_0x005b:
            com.paypal.android.b.f r10 = new com.paypal.android.b.f
            com.paypal.android.a.o$a r5 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            com.paypal.android.a.o$a r11 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            r10.<init>(r15, r5, r11)
            com.paypal.android.MEP.PayPal r5 = com.paypal.android.MEP.PayPal.getInstance()
            java.lang.String r5 = r5.getLanguage()
            java.lang.String r11 = "fr_"
            boolean r5 = r5.contains(r11)
            if (r5 != 0) goto L_0x019b
            r5 = 1
        L_0x0075:
            java.lang.String r11 = "BALANCE"
            int r11 = r9.indexOf(r11)
            r12 = -1
            if (r11 == r12) goto L_0x01c6
            if (r5 == 0) goto L_0x019e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r4 = "):"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
        L_0x00a6:
            java.lang.String r2 = com.paypal.android.a.h.a(r8, r6)
            r10.b(r2)
            r0 = r16
            r0.addView(r10)
            java.lang.String r2 = "BANK_DELAYED"
            boolean r2 = r9.equals(r2)
            if (r2 == 0) goto L_0x00e5
            com.paypal.android.a.o$a r2 = com.paypal.android.a.o.a.HELVETICA_12_NORMAL
            android.widget.TextView r2 = com.paypal.android.a.o.a(r2, r15)
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams
            r5 = -1
            r9 = -2
            r10 = 1056964608(0x3f000000, float:0.5)
            r4.<init>(r5, r9, r10)
            r2.setLayoutParams(r4)
            r4 = 2
            r5 = 2
            r9 = 2
            r10 = 2
            r2.setPadding(r4, r5, r9, r10)
            r4 = 0
            r2.setBackgroundColor(r4)
            java.lang.String r4 = "ANDROID_echeck_note"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            r2.setText(r4)
            r0 = r16
            r0.addView(r2)
        L_0x00e5:
            r4 = 0
            java.math.BigDecimal r2 = new java.math.BigDecimal
            java.lang.String r5 = "0"
            r2.<init>(r5)
            com.paypal.android.a.a.a r5 = r3.b
            if (r5 == 0) goto L_0x00ff
            com.paypal.android.a.a.a r5 = r3.b
            java.math.BigDecimal r5 = r5.a()
            if (r5 == 0) goto L_0x00ff
            com.paypal.android.a.a.a r2 = r3.b
            java.math.BigDecimal r2 = r2.a()
        L_0x00ff:
            com.paypal.android.MEP.PayPal r5 = com.paypal.android.MEP.PayPal.getInstance()
            com.paypal.android.MEP.PayPalAdvancedPayment r5 = r5.getPayment()
            java.math.BigDecimal r5 = r5.getTotal()
            com.paypal.android.MEP.PayPal r9 = com.paypal.android.MEP.PayPal.getInstance()
            com.paypal.android.MEP.PayPalAdvancedPayment r9 = r9.getPayment()
            java.lang.String r9 = r9.getCurrencyType()
            boolean r6 = r6.equals(r9)
            if (r6 != 0) goto L_0x03b7
            com.paypal.android.a.a.a r2 = r3.a
            java.math.BigDecimal r2 = r2.a()
            com.paypal.android.a.a.f r4 = r3.c
            com.paypal.android.a.a.a r4 = r4.b
            java.math.BigDecimal r4 = r4.a()
            java.math.BigDecimal r5 = r2.subtract(r4)
            com.paypal.android.a.a.f r2 = r3.c
            com.paypal.android.a.a.a r2 = r2.a
            java.math.BigDecimal r4 = r2.a()
            r2 = 0
            r6 = r2
        L_0x0139:
            java.util.Vector<com.paypal.android.a.a.k> r2 = r3.d
            int r2 = r2.size()
            if (r6 >= r2) goto L_0x0372
            java.util.Vector<com.paypal.android.a.a.k> r2 = r3.d
            java.lang.Object r2 = r2.elementAt(r6)
            com.paypal.android.a.a.k r2 = (com.paypal.android.a.a.k) r2
            com.paypal.android.a.a.a r8 = r3.a
            java.lang.String r8 = r8.b()
            com.paypal.android.a.a.a r9 = r2.a
            java.lang.String r9 = r9.b()
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x0365
            com.paypal.android.a.a.a r2 = r2.a
            java.math.BigDecimal r2 = r2.a()
            java.math.BigDecimal r2 = r5.subtract(r2)
            r13 = r4
            r4 = r2
            r2 = r13
        L_0x0168:
            int r5 = r6 + 1
            r6 = r5
            r5 = r4
            r4 = r2
            goto L_0x0139
        L_0x016e:
            com.paypal.android.a.b r1 = com.paypal.android.a.b.e()
            java.util.Hashtable r1 = r1.g()
            r3 = r1
            goto L_0x000e
        L_0x0179:
            r2 = 0
            java.lang.Object r2 = r1.elementAt(r2)
            com.paypal.android.a.a.c r2 = (com.paypal.android.a.a.c) r2
            java.lang.String r4 = "FundingPlanId"
            java.lang.Object r3 = r3.get(r4)     // Catch:{ Exception -> 0x0197 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0197 }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x0197 }
            java.lang.Object r3 = r1.elementAt(r3)     // Catch:{ Exception -> 0x0197 }
            r0 = r3
            com.paypal.android.a.a.c r0 = (com.paypal.android.a.a.c) r0     // Catch:{ Exception -> 0x0197 }
            r2 = r0
            r3 = r2
            goto L_0x002d
        L_0x0197:
            r3 = move-exception
            r3 = r2
            goto L_0x002d
        L_0x019b:
            r5 = 0
            goto L_0x0075
        L_0x019e:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r2 = r2.append(r6)
            java.lang.String r4 = ") :"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x01c6:
            java.lang.String r11 = "BANK_DELAYED"
            boolean r11 = r9.equals(r11)
            if (r11 == 0) goto L_0x02a6
            if (r4 == 0) goto L_0x01d6
            int r11 = r4.length()
            if (r11 > 0) goto L_0x0234
        L_0x01d6:
            if (r5 == 0) goto L_0x0206
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "ANDROID_echeck"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "):"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x0206:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "ANDROID_echeck"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ") :"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x0234:
            if (r5 == 0) goto L_0x026e
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " x"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "ANDROID_echeck"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "):"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x026e:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " x"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = " ("
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = "ANDROID_echeck"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ") :"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x02a6:
            java.lang.String r5 = "BANK_INSTANT"
            boolean r5 = r9.equals(r5)
            if (r5 == 0) goto L_0x02fc
            if (r4 == 0) goto L_0x02b6
            int r5 = r4.length()
            if (r5 > 0) goto L_0x02d4
        L_0x02b6:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = ":"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x02d4:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " x"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ":"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x02fc:
            java.lang.String r5 = "CREDITCARD"
            boolean r5 = r9.equals(r5)
            if (r5 != 0) goto L_0x030c
            java.lang.String r5 = "DEBITCARD"
            boolean r5 = r9.equals(r5)
            if (r5 == 0) goto L_0x035a
        L_0x030c:
            if (r4 == 0) goto L_0x0314
            int r5 = r4.length()
            if (r5 > 0) goto L_0x0332
        L_0x0314:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r4 = ":"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x0332:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r2 = r5.append(r2)
            java.lang.String r5 = " x"
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ":"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r10.a(r2)
            goto L_0x00a6
        L_0x035a:
            com.paypal.android.a.a.g r2 = r2.b
            java.lang.String r2 = r2.a()
            r10.a(r2)
            goto L_0x00a6
        L_0x0365:
            com.paypal.android.a.a.a r2 = r2.a
            java.math.BigDecimal r2 = r2.a()
            java.math.BigDecimal r2 = r4.subtract(r2)
            r4 = r5
            goto L_0x0168
        L_0x0372:
            java.math.BigDecimal r2 = java.math.BigDecimal.ZERO
            int r2 = r5.compareTo(r2)
            if (r2 != 0) goto L_0x0382
            java.math.BigDecimal r2 = java.math.BigDecimal.ZERO
            int r2 = r4.compareTo(r2)
            if (r2 == 0) goto L_0x03b5
        L_0x0382:
            r2 = 1
        L_0x0383:
            if (r2 == 0) goto L_0x03b0
            com.paypal.android.a.o$a r2 = com.paypal.android.a.o.a.HELVETICA_12_NORMAL
            android.widget.TextView r2 = com.paypal.android.a.o.a(r2, r15)
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams
            r5 = -1
            r6 = -2
            r8 = 1056964608(0x3f000000, float:0.5)
            r4.<init>(r5, r6, r8)
            r2.setLayoutParams(r4)
            r4 = 2
            r5 = 2
            r6 = 2
            r8 = 2
            r2.setPadding(r4, r5, r6, r8)
            r4 = 0
            r2.setBackgroundColor(r4)
            java.lang.String r4 = "ANDROID_negative_balance"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            r2.setText(r4)
            r0 = r16
            r0.addView(r2)
        L_0x03b0:
            int r2 = r7 + 1
            r7 = r2
            goto L_0x002f
        L_0x03b5:
            r2 = 0
            goto L_0x0383
        L_0x03b7:
            java.math.BigDecimal r2 = r5.add(r2)
            int r2 = r8.compareTo(r2)
            if (r2 <= 0) goto L_0x0604
            r2 = 1
            goto L_0x0383
        L_0x03c3:
            com.paypal.android.a.a.f r2 = r3.c
            if (r2 == 0) goto L_0x0418
            java.lang.String r4 = r2.a()
            float r4 = java.lang.Float.parseFloat(r4)
            com.paypal.android.b.f r5 = new com.paypal.android.b.f
            com.paypal.android.a.o$a r6 = com.paypal.android.a.o.a.HELVETICA_12_NORMAL
            com.paypal.android.a.o$a r7 = com.paypal.android.a.o.a.HELVETICA_12_NORMAL
            r5.<init>(r15, r6, r7)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "1 "
            java.lang.StringBuilder r6 = r6.append(r7)
            com.paypal.android.a.a.a r7 = r2.a
            java.lang.String r7 = r7.b()
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = " = "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r4 = r6.append(r4)
            java.lang.String r6 = " "
            java.lang.StringBuilder r4 = r4.append(r6)
            com.paypal.android.a.a.a r2 = r2.b
            java.lang.String r2 = r2.b()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            r5.a(r2)
            java.lang.String r2 = ""
            r5.b(r2)
            r0 = r16
            r0.addView(r5)
        L_0x0418:
            r2 = 1
            if (r1 == 0) goto L_0x042a
            int r4 = r1.size()
            if (r4 <= 0) goto L_0x042a
            int r1 = r1.size()
            r2 = 1
            if (r1 <= r2) goto L_0x044f
            r1 = 1
        L_0x0429:
            r2 = r1
        L_0x042a:
            java.util.Vector<com.paypal.android.a.a.k> r1 = r3.d
            r4 = 0
            java.lang.Object r1 = r1.elementAt(r4)
            com.paypal.android.a.a.k r1 = (com.paypal.android.a.a.k) r1
            com.paypal.android.a.a.g r1 = r1.b
            java.lang.String r1 = r1.c()
            java.lang.String r4 = "BALANCE"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x044b
            java.util.Vector<com.paypal.android.a.a.k> r1 = r3.d
            int r1 = r1.size()
            r3 = 1
            if (r1 != r3) goto L_0x044b
            r2 = 0
        L_0x044b:
            r14.a(r2)
        L_0x044e:
            return
        L_0x044f:
            r1 = 0
            goto L_0x0429
        L_0x0451:
            com.paypal.android.MEP.b.a$a r1 = r14.i
            com.paypal.android.MEP.b.a$a r2 = com.paypal.android.MEP.b.a.C0005a.PAYMENT_DETAILS_FEES
            if (r1 != r2) goto L_0x04e9
            java.lang.String r1 = "FundingPlanId"
            java.lang.Object r1 = r3.get(r1)     // Catch:{ Exception -> 0x0601 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0601 }
            int r2 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0601 }
            java.lang.String r1 = "FundingPlans"
            java.lang.Object r1 = r3.get(r1)     // Catch:{ Exception -> 0x0601 }
            java.util.Vector r1 = (java.util.Vector) r1     // Catch:{ Exception -> 0x0601 }
            if (r1 == 0) goto L_0x0473
            int r4 = r1.size()     // Catch:{ Exception -> 0x0601 }
            if (r4 != 0) goto L_0x04c0
        L_0x0473:
            java.lang.String r1 = "DefaultFundingPlan"
            java.lang.Object r1 = r3.get(r1)     // Catch:{ Exception -> 0x0601 }
            com.paypal.android.a.a.c r1 = (com.paypal.android.a.a.c) r1     // Catch:{ Exception -> 0x0601 }
        L_0x047b:
            if (r1 == 0) goto L_0x044e
            com.paypal.android.a.a.a r1 = r1.b
            if (r1 == 0) goto L_0x04d1
            java.math.BigDecimal r2 = r1.a()
            java.lang.String r1 = r1.b()
            com.paypal.android.b.f r3 = new com.paypal.android.b.f
            com.paypal.android.a.o$a r4 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            com.paypal.android.a.o$a r5 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            r3.<init>(r15, r4, r5)
            java.math.BigDecimal r4 = java.math.BigDecimal.ZERO
            int r4 = r2.compareTo(r4)
            if (r4 <= 0) goto L_0x04c7
            java.lang.String r4 = "ANDROID_i_pay"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            r3.a(r4)
        L_0x04a3:
            java.lang.String r1 = com.paypal.android.a.h.a(r2, r1)
            r3.b(r1)
            r0 = r16
            r0.addView(r3)
            java.math.BigDecimal r1 = java.math.BigDecimal.ZERO
            int r1 = r2.compareTo(r1)
            if (r1 > 0) goto L_0x04bb
            r1 = 0
            r14.a(r1)
        L_0x04bb:
            r1 = 0
            r14.a(r1)
            goto L_0x044e
        L_0x04c0:
            java.lang.Object r1 = r1.elementAt(r2)     // Catch:{ Exception -> 0x0601 }
            com.paypal.android.a.a.c r1 = (com.paypal.android.a.a.c) r1     // Catch:{ Exception -> 0x0601 }
            goto L_0x047b
        L_0x04c7:
            java.lang.String r4 = "ANDROID_free"
            java.lang.String r4 = com.paypal.android.a.h.a(r4)
            r3.a(r4)
            goto L_0x04a3
        L_0x04d1:
            com.paypal.android.b.f r1 = new com.paypal.android.b.f
            com.paypal.android.a.o$a r2 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            com.paypal.android.a.o$a r3 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            r1.<init>(r15, r2, r3)
            java.lang.String r2 = "ANDROID_free"
            java.lang.String r2 = com.paypal.android.a.h.a(r2)
            r1.a(r2)
            r0 = r16
            r0.addView(r1)
            goto L_0x04bb
        L_0x04e9:
            com.paypal.android.MEP.b.a$a r1 = r14.i
            com.paypal.android.MEP.b.a$a r2 = com.paypal.android.MEP.b.a.C0005a.PAYMENT_DETAILS_SHIPPING
            if (r1 != r2) goto L_0x044e
            java.lang.String r1 = "AvailableAddresses"
            java.lang.Object r1 = r3.get(r1)
            java.util.Vector r1 = (java.util.Vector) r1
            java.lang.String r2 = "ShippingAddressId"
            java.lang.Object r2 = r3.get(r2)
            java.lang.String r2 = (java.lang.String) r2
            if (r1 == 0) goto L_0x044e
            int r3 = r1.size()
            if (r3 <= 0) goto L_0x044e
            r4 = 0
            r3 = 0
            r13 = r3
            r3 = r4
            r4 = r13
        L_0x050c:
            int r5 = r1.size()
            if (r4 >= r5) goto L_0x0525
            java.lang.Object r3 = r1.elementAt(r4)
            com.paypal.android.a.a.h r3 = (com.paypal.android.a.a.h) r3
            java.lang.String r5 = r3.h()
            boolean r5 = r5.equals(r2)
            if (r5 != 0) goto L_0x0525
            int r4 = r4 + 1
            goto L_0x050c
        L_0x0525:
            if (r3 == 0) goto L_0x044e
            java.lang.String r2 = r3.a()
            java.lang.String r4 = r3.d()
            java.lang.String r5 = r3.e()
            java.lang.String r6 = r3.b()
            java.lang.String r7 = r3.g()
            java.lang.String r3 = r3.f()
            com.paypal.android.b.f r8 = new com.paypal.android.b.f
            com.paypal.android.a.o$a r9 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            com.paypal.android.a.o$a r10 = com.paypal.android.a.o.a.HELVETICA_14_NORMAL
            r8.<init>(r15, r9, r10)
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            if (r2 == 0) goto L_0x056b
            int r10 = r2.length()
            if (r10 <= 0) goto L_0x056b
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.StringBuilder r2 = r10.append(r2)
            java.lang.String r10 = "\n"
            java.lang.StringBuilder r2 = r2.append(r10)
            java.lang.String r2 = r2.toString()
            r9.append(r2)
        L_0x056b:
            if (r4 == 0) goto L_0x0583
            int r2 = r4.length()
            if (r2 <= 0) goto L_0x0583
            r9.append(r4)
            if (r5 == 0) goto L_0x0583
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x0583
            java.lang.String r2 = ", "
            r9.append(r2)
        L_0x0583:
            if (r5 == 0) goto L_0x058e
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x058e
            r9.append(r5)
        L_0x058e:
            if (r4 == 0) goto L_0x0596
            int r2 = r4.length()
            if (r2 > 0) goto L_0x059e
        L_0x0596:
            if (r5 == 0) goto L_0x05a3
            int r2 = r5.length()
            if (r2 <= 0) goto L_0x05a3
        L_0x059e:
            java.lang.String r2 = "\n"
            r9.append(r2)
        L_0x05a3:
            if (r6 == 0) goto L_0x05c3
            int r2 = r6.length()
            if (r2 <= 0) goto L_0x05c3
            r9.append(r6)
            if (r7 == 0) goto L_0x05b6
            int r2 = r7.length()
            if (r2 > 0) goto L_0x05be
        L_0x05b6:
            if (r3 == 0) goto L_0x05c3
            int r2 = r3.length()
            if (r2 <= 0) goto L_0x05c3
        L_0x05be:
            java.lang.String r2 = ", "
            r9.append(r2)
        L_0x05c3:
            if (r7 == 0) goto L_0x05db
            int r2 = r7.length()
            if (r2 <= 0) goto L_0x05db
            r9.append(r7)
            if (r3 == 0) goto L_0x05db
            int r2 = r3.length()
            if (r2 <= 0) goto L_0x05db
            java.lang.String r2 = " "
            r9.append(r2)
        L_0x05db:
            if (r3 == 0) goto L_0x05e6
            int r2 = r3.length()
            if (r2 <= 0) goto L_0x05e6
            r9.append(r3)
        L_0x05e6:
            java.lang.String r2 = r9.toString()
            r8.a(r2)
            int r1 = r1.size()
            r2 = 1
            if (r1 <= r2) goto L_0x05ff
            r1 = 1
        L_0x05f5:
            r14.a(r1)
            r0 = r16
            r0.addView(r8)
            goto L_0x044e
        L_0x05ff:
            r1 = 0
            goto L_0x05f5
        L_0x0601:
            r1 = move-exception
            goto L_0x044e
        L_0x0604:
            r2 = r4
            goto L_0x0383
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.MEP.b.a.a(android.content.Context, android.widget.LinearLayout):void");
    }

    private void c(boolean z) {
        if (z) {
            this.k.setVisibility(8);
            this.j.setVisibility(0);
            l.a();
            return;
        }
        l.b();
        this.j.setVisibility(8);
        this.k.setVisibility(0);
    }

    private void e() {
        this.f = new Vector<>();
        Vector vector = (Vector) (PayPal.getInstance().getServer() == 2 ? com.paypal.android.MEP.a.a.a : com.paypal.android.a.b.e().g()).get("FundingPlans");
        for (int i2 = 0; i2 < vector.size(); i2++) {
            c cVar = (c) vector.get(i2);
            com.paypal.android.a.a.k kVar = cVar.d.get(0);
            g gVar = kVar.b;
            String c = gVar.c();
            String a = gVar.a();
            String str = c.equals("BALANCE") ? a + "(" + kVar.a.b() + ")" : ((c.equals("BANK_DELAYED") || c.equals("BANK_INSTANT") || c.equals("CREDITCARD") || c.equals("DEBITCARD")) && gVar.b() != null && gVar.b().length() > 0) ? a + " x" + gVar.b() : a;
            if (c.equals("BANK_DELAYED")) {
                str = str + " (" + h.a("ANDROID_echeck") + ")";
            }
            Hashtable hashtable = new Hashtable();
            hashtable.put("label", str);
            hashtable.put("plan", cVar);
            this.f.add(hashtable);
        }
        d.AnonymousClass1.b();
    }

    public final void a(int i2) {
        int id;
        this.r = i2;
        if (i2 == 1) {
            switch (this.i) {
                case PAYMENT_DETAILS_FUNDING:
                    Vector vector = (Vector) (PayPal.getInstance().getServer() == 2 ? com.paypal.android.MEP.a.a.a : com.paypal.android.a.b.e().g()).get("FundingPlans");
                    if (vector == null || vector.size() == 0) {
                        c(true);
                        com.paypal.android.a.b.e().a("delegate", this);
                        com.paypal.android.a.b.e().a(5);
                        return;
                    }
                    e();
                    super.a(i2);
                    return;
                case PAYMENT_DETAILS_FEES:
                    d();
                    TextView a = o.a(o.a.HELVETICA_14_NORMAL, getContext());
                    a.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                    a.setBackgroundColor(0);
                    a.setText(h.a("ANDROID_choose_who_pays_the_fee") + ":");
                    a.setTextColor(-1);
                    a(a);
                    if (((String) ((Hashtable) ((Vector) com.paypal.android.a.b.e().g().get("PricingDetails")).get(0)).get("FeeBearer")).compareTo("ApplyFeeToReceiver") == 0) {
                        Button a2 = a(getContext(), h.a("ANDROID_i_pay"), 0);
                        id = a2.getId();
                        a(a2);
                        a2.setNextFocusUpId(getId());
                    } else {
                        Button a3 = a(getContext(), h.a("NDROID_recipient_pays"), 1);
                        id = a3.getId();
                        a(a3);
                        a3.setNextFocusUpId(getId());
                    }
                    setNextFocusDownId(id);
                    if (this.s != null) {
                        this.s.a(this, id);
                    }
                    super.a(i2);
                    return;
                case PAYMENT_DETAILS_SHIPPING:
                    if (PayPal.getInstance().getServer() != 2) {
                        if (((Vector) PayPalActivity._networkHandler.g().get("AvailableAddresses")) == null) {
                            this.f = null;
                        } else {
                            this.f = new Vector<>();
                        }
                    } else if (((Vector) com.paypal.android.MEP.a.a.a.get("AvailableAddresses")) == null) {
                        this.f = null;
                    } else {
                        this.f = new Vector<>();
                    }
                    d.AnonymousClass1.b();
                    super.a(i2);
                    return;
                default:
                    return;
            }
        } else {
            d();
            this.f = null;
            super.a(i2);
        }
    }

    public final void a(int i2, Object obj) {
        q = false;
        switch (i2) {
            case 3:
                com.paypal.android.a.b.e().a(6);
                return;
            case 4:
            default:
                return;
            case 5:
                e();
                if (((Vector) com.paypal.android.a.b.e().c("FundingPlans")).size() == 1) {
                    a(false);
                    PayPalActivity.getInstance().sendBroadcast(new Intent(PayPalActivity.CREATE_PAYMENT_SUCCESS));
                    return;
                }
                return;
            case 6:
                try {
                    if (PayPal.getInstance().getServer() != 2) {
                        com.paypal.android.MEP.a.a.a = com.paypal.android.a.b.e().g();
                    }
                    PayPalActivity.getInstance().sendBroadcast(new Intent(PayPalActivity.CREATE_PAYMENT_SUCCESS));
                    return;
                } catch (Throwable th) {
                    return;
                }
        }
    }

    public final void a(b bVar) {
        this.s = bVar;
    }

    public final void a(String str, Object obj) {
    }

    public final C0005a b() {
        return this.i;
    }

    public final void b(boolean z) {
        if (z) {
            setBackgroundDrawable(this.h);
        } else {
            setBackgroundDrawable(this.g);
        }
    }

    public final void c() {
        boolean z;
        int i2 = 0;
        if (this.u != null) {
            this.n.d(this.u);
            this.u = null;
            c(false);
        }
        if (this.f != null) {
            c(false);
            super.a(1);
            d();
            switch (this.i) {
                case PAYMENT_DETAILS_FUNDING:
                    TextView a = o.a(o.a.HELVETICA_14_NORMAL, getContext());
                    a.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                    a.setBackgroundColor(0);
                    a.setText(h.a("ANDROID_change_funding") + ":");
                    a.setTextColor(-1);
                    if (this.f.size() > 0) {
                        a(a);
                    }
                    int i3 = 0;
                    int i4 = 0;
                    boolean z2 = false;
                    int i5 = 0;
                    while (i3 < this.f.size()) {
                        try {
                            Hashtable hashtable = this.f.get(i3);
                            c cVar = (c) hashtable.get("plan");
                            if (!((String) (PayPal.getInstance().getServer() == 2 ? com.paypal.android.MEP.a.a.a : com.paypal.android.a.b.e().g()).get("FundingPlanId")).equals(cVar.a())) {
                                Button a2 = a(getContext(), (String) hashtable.get("label"), i5);
                                int id = a2.getId();
                                this.p.add(cVar.a());
                                a(a2);
                                if (!z2) {
                                    setNextFocusDownId(id);
                                    z2 = true;
                                }
                                i5++;
                                i4 = id;
                            }
                        } catch (Throwable th) {
                        }
                        i3++;
                        z2 = z2;
                        i5 = i5;
                        i4 = i4;
                    }
                    if (!(i4 == 0 || this.s == null)) {
                        this.s.a(this, i4);
                    }
                    if (i5 == 0) {
                        a.setText(" ");
                        return;
                    }
                    return;
                case PAYMENT_DETAILS_FEES:
                default:
                    return;
                case PAYMENT_DETAILS_SHIPPING:
                    Hashtable<String, Object> g2 = PayPal.getInstance().getServer() == 2 ? com.paypal.android.MEP.a.a.a : com.paypal.android.a.b.e().g();
                    Vector vector = (Vector) g2.get("AvailableAddresses");
                    String str = (String) g2.get("ShippingAddressId");
                    if (vector != null && vector.size() > 1) {
                        TextView a3 = o.a(o.a.HELVETICA_14_NORMAL, getContext());
                        a3.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                        a3.setBackgroundColor(0);
                        if (PayPal.getInstance().getTextType() == 1) {
                            a3.setText(h.a("ANDROID_change_mailing_address_to") + ":");
                        } else {
                            a3.setText(h.a("ANDROID_change_shipping_to") + ":");
                        }
                        a3.setTextColor(-1);
                        a(a3);
                        this.o.removeAllElements();
                        int i6 = 0;
                        boolean z3 = false;
                        while (true) {
                            int i7 = i2;
                            if (i6 < vector.size()) {
                                com.paypal.android.a.a.h hVar = (com.paypal.android.a.a.h) vector.elementAt(i6);
                                try {
                                    String d = hVar.d();
                                    String e2 = hVar.e();
                                    String b2 = hVar.b();
                                    String g3 = hVar.g();
                                    String f2 = hVar.f();
                                    String str2 = d + (d.length() > 0 ? AppConstants.NEW_LINE : "");
                                    if (e2 != null) {
                                        str2 = str2 + e2 + (e2.length() > 0 ? AppConstants.NEW_LINE : "");
                                    }
                                    String str3 = str2 + b2;
                                    if (g3 != null) {
                                        str3 = str3 + ", " + g3;
                                    }
                                    if (f2 != null) {
                                        str3 = str3 + " " + f2;
                                    }
                                    if (str == null || !str.equals(hVar.h())) {
                                        Button a4 = a(getContext(), str3, i7);
                                        int id2 = a4.getId();
                                        this.o.add(hVar.h());
                                        a(a4);
                                        if (!z3) {
                                            setNextFocusDownId(id2);
                                            z = true;
                                        } else {
                                            z = z3;
                                        }
                                        boolean z4 = z;
                                        i2 = i7 + 1;
                                        z3 = z4;
                                        i6++;
                                    } else {
                                        i2 = i7;
                                        i6++;
                                    }
                                } catch (Throwable th2) {
                                    i2 = i7;
                                }
                            } else {
                                if (this.f.size() == 0) {
                                    View view = new View(getContext());
                                    view.setMinimumWidth(10);
                                    view.setMinimumHeight(10);
                                    a(view);
                                }
                                if (i7 == 0) {
                                    a3.setText(" ");
                                    return;
                                }
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                    break;
            }
        }
    }

    public final void d(String str) {
        this.u = str;
        q = false;
        d.AnonymousClass1.b();
    }

    public final void l() {
        switch (this.i) {
            case PAYMENT_DETAILS_FEES:
                if (this.e) {
                    this.e = false;
                    com.paypal.android.a.b.e().a("delegate", this);
                    return;
                }
                return;
            case PAYMENT_DETAILS_SHIPPING:
                com.paypal.android.a.b.e().a("delegate", this);
                if (this.e) {
                    this.e = false;
                    return;
                } else {
                    com.paypal.android.a.b.e().a(7);
                    return;
                }
            default:
                return;
        }
    }

    public final void onClick(View view) {
        if (this.r == 0) {
            this.n.d();
        }
        super.onClick(view);
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (!isClickable()) {
                    return false;
                }
                setBackgroundDrawable(this.h);
                return false;
            default:
                setBackgroundDrawable(this.g);
                return false;
        }
    }
}
