package com.paypal.android.MEP.a;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.a.d;
import com.paypal.android.a.b;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.o;
import com.paypal.android.b.h;
import com.paypal.android.b.j;

public final class f extends j implements View.OnClickListener {
    private Button a;

    public f(Context context) {
        super(context);
    }

    public final void a() {
    }

    /* access modifiers changed from: protected */
    public final void a(Context context) {
        String str;
        super.a(context);
        setId(9006);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        linearLayout.setBackgroundDrawable(d.a());
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setGravity(1);
        linearLayout.addView(new h(com.paypal.android.a.h.a("ANDROID_help"), context));
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout2.setPadding(0, 0, 0, 15);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -1510918, -1510918, -1510918, -1510918, -1510918});
        gradientDrawable.setCornerRadius(10.0f);
        gradientDrawable.setStroke(2, -8280890);
        linearLayout2.setBackgroundDrawable(gradientDrawable);
        TextView a2 = o.a(o.a.HELVETICA_16_BOLD, context);
        a2.setText(com.paypal.android.a.h.a("ANDROID_about_paypal"));
        linearLayout2.addView(a2);
        TextView a3 = o.a(o.a.HELVETICA_16_NORMAL, context);
        a3.setText(com.paypal.android.a.h.a("ANDROID_help_string"));
        Linkify.addLinks(a3, 1);
        linearLayout2.addView(a3);
        TextView a4 = o.a(o.a.HELVETICA_16_BOLD, context);
        a4.setText(com.paypal.android.a.h.a("ANDROID_sign_up"));
        linearLayout2.addView(a4);
        TextView a5 = o.a(o.a.HELVETICA_16_NORMAL, context);
        a5.setText(com.paypal.android.a.h.a("ANDROID_no_account"));
        Linkify.addLinks(a5, 1);
        linearLayout2.addView(a5);
        TextView a6 = o.a(o.a.HELVETICA_16_BOLD, context);
        a6.setText(com.paypal.android.a.h.a("ANDROID_forgot_password"));
        linearLayout2.addView(a6);
        TextView a7 = o.a(o.a.HELVETICA_16_NORMAL, context);
        a7.setText(com.paypal.android.a.h.a("ANDROID_forgot_password_body"));
        Linkify.addLinks(a7, 1);
        linearLayout2.addView(a7);
        if (PayPal.getInstance().shouldShowFees()) {
            TextView a8 = o.a(o.a.HELVETICA_16_BOLD, context);
            a8.setText(com.paypal.android.a.h.a("ANDROID_help_fees_header"));
            linearLayout2.addView(a8);
            TextView a9 = o.a(o.a.HELVETICA_16_NORMAL, context);
            a9.setText(com.paypal.android.a.h.a("ANDROID_help_fees_body"));
            Linkify.addLinks(a9, 1);
            linearLayout2.addView(a9);
        }
        TelephonyManager telephonyManager = (TelephonyManager) PayPal.getInstance().getParentContext().getSystemService("phone");
        String str2 = telephonyManager.getPhoneType() == 1 ? "IMEI" : "MEID";
        TextView a10 = o.a(o.a.HELVETICA_16_BOLD, context);
        a10.setPadding(5, 1, 5, 2);
        a10.setText(com.paypal.android.a.h.a("ANDROID_debug_support"));
        linearLayout2.addView(a10);
        TextView a11 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a11.setPadding(5, 1, 5, 2);
        a11.setText(com.paypal.android.a.h.a("ANDROID_debug_version") + ": " + PayPal.getVersionWithoutBuild());
        linearLayout2.addView(a11);
        TextView a12 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a12.setPadding(5, 1, 5, 2);
        a12.setText(com.paypal.android.a.h.a("ANDROID_debug_build") + ": " + PayPal.getBuild());
        linearLayout2.addView(a12);
        TextView a13 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a13.setPadding(5, 1, 5, 2);
        a13.setText(com.paypal.android.a.h.a("ANDROID_debug_platform") + ": " + "Android");
        linearLayout2.addView(a13);
        TextView a14 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a14.setPadding(5, 1, 5, 2);
        a14.setText(com.paypal.android.a.h.a("ANDROID_debug_model") + ": " + Build.MODEL);
        linearLayout2.addView(a14);
        TextView a15 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a15.setPadding(5, 1, 5, 2);
        a15.setText(com.paypal.android.a.h.a("ANDROID_debug_os") + ": " + Build.VERSION.RELEASE);
        linearLayout2.addView(a15);
        switch (PayPal.getInstance().getServer()) {
            case 0:
                str = "Sandbox";
                break;
            case 1:
            default:
                str = "Live";
                break;
            case 2:
                str = "Demo";
                break;
            case 3:
                str = "Stage (" + b.b() + ")";
                break;
        }
        TextView a16 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a16.setPadding(5, 1, 5, 2);
        a16.setText(com.paypal.android.a.h.a("ANDROID_debug_server") + ": " + str);
        linearLayout2.addView(a16);
        TextView a17 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a17.setPadding(5, 1, 5, 2);
        a17.setText(str2 + ": " + telephonyManager.getDeviceId());
        linearLayout2.addView(a17);
        linearLayout.addView(linearLayout2);
        this.a = new Button(context);
        this.a.setText(com.paypal.android.a.h.a("ANDROID_ok"));
        this.a.setLayoutParams(new RelativeLayout.LayoutParams(-1, d.b()));
        this.a.setGravity(17);
        this.a.setBackgroundDrawable(e.a());
        this.a.setTextColor(-16777216);
        this.a.setOnClickListener(this);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(1);
        linearLayout3.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout3.setPadding(0, 15, 0, 0);
        linearLayout3.addView(this.a);
        linearLayout.addView(linearLayout3);
        addView(linearLayout);
    }

    public final void b() {
    }

    public final void onClick(View view) {
        if (view == this.a) {
            d.AnonymousClass1.a();
        }
    }
}
