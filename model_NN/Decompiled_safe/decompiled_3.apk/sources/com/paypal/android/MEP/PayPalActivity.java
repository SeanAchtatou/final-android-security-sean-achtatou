package com.paypal.android.MEP;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import com.paypal.android.MEP.a.a;
import com.paypal.android.MEP.a.c;
import com.paypal.android.MEP.a.d;
import com.paypal.android.MEP.a.e;
import com.paypal.android.MEP.a.f;
import com.paypal.android.MEP.a.g;
import com.paypal.android.MEP.a.h;
import com.paypal.android.a.b;
import com.paypal.android.b.j;
import com.paypal.android.b.l;
import java.util.Hashtable;
import java.util.Vector;

public final class PayPalActivity extends Activity implements Animation.AnimationListener {
    public static String AUTH_SUCCESS = null;
    public static String CREATE_PAYMENT_FAIL = null;
    public static String CREATE_PAYMENT_SUCCESS = null;
    public static final String EXTRA_CORRELATION_ID = "com.paypal.android.CORRELATION_ID";
    public static final String EXTRA_ERROR_ID = "com.paypal.android.ERROR_ID";
    public static final String EXTRA_ERROR_MESSAGE = "com.paypal.android.ERROR_MESSAGE";
    public static final String EXTRA_PAYMENT_ADJUSTER = "com.paypal.android.PAYMENT_ADJUSTER";
    public static final String EXTRA_PAYMENT_INFO = "com.paypal.android.PAYPAL_PAYMENT";
    public static final String EXTRA_PAYMENT_STATUS = "com.paypal.android.PAYMENT_STATUS";
    public static final String EXTRA_PAY_KEY = "com.paypal.android.PAY_KEY";
    public static final String EXTRA_PREAPPROVAL_INFO = "com.paypal.android.PAYPAL_PREAPPROVAL";
    public static final String EXTRA_RESULT_DELEGATE = "com.paypal.android.RESULT_DELEGATE";
    public static String FATAL_ERROR = null;
    public static String LOGIN_FAIL = null;
    public static String LOGIN_SUCCESS = null;
    public static final int RESULT_FAILURE = 2;
    public static final int VIEW_ABOUT = 2;
    public static final int VIEW_CREATE_PIN = 7;
    public static final int VIEW_FATAL_ERROR = 5;
    public static final int VIEW_INFO = 1;
    public static final int VIEW_LOGIN = 0;
    public static final int VIEW_NUM_VIEWS = 9;
    public static final int VIEW_PREAPPROVAL = 6;
    public static final int VIEW_REVIEW = 3;
    public static final int VIEW_SUCCESS = 4;
    public static final int VIEW_TEST = 8;
    public static b _networkHandler = null;
    public static PayPal _paypal;
    public static String _popIntent;
    public static String _pushIntent;
    public static String _replaceIntent;
    public static String _updateIntent;
    /* access modifiers changed from: private */
    public static PayPalActivity c = null;
    /* access modifiers changed from: private */
    public static String h = null;
    private PaymentAdjuster a;
    private PayPalResultDelegate b;
    private Vector<j> d;
    private Animation e;
    /* access modifiers changed from: private */
    public Intent f = null;
    private boolean g = false;
    private BroadcastReceiver i = new c(this);
    private BroadcastReceiver j = new b(this);
    public boolean transactionSuccessful = false;

    /* access modifiers changed from: private */
    public boolean a(int i2) {
        j eVar;
        if (i2 == 0) {
            b.e().a("mpl-login");
            eVar = new d(this);
        } else if (i2 == 1) {
            b.e().a("mpl-help-binding");
            eVar = new com.paypal.android.MEP.a.b(this);
        } else if (i2 == 2) {
            b.e().a("mpl-help");
            eVar = new f(this);
        } else if (i2 == 3) {
            eVar = new a(this);
        } else if (i2 == 4) {
            eVar = new h(this);
        } else if (i2 == 5) {
            eVar = new c(this, this.f);
        } else if (i2 == 6) {
            eVar = new g(this);
        } else if (i2 != 7) {
            return false;
        } else {
            b.e().a("mpl-create-PIN");
            eVar = new e(this);
        }
        j lastElement = this.d.size() > 0 ? this.d.lastElement() : null;
        setContentView(eVar);
        this.d.add(eVar);
        if (lastElement != null) {
            lastElement.a();
        }
        if (i2 == 0) {
            ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 2, 0.5f, 2, 0.5f);
            scaleAnimation.setDuration(500);
            scaleAnimation.setRepeatCount(0);
            scaleAnimation.setAnimationListener(this);
            eVar.setAnimation(scaleAnimation);
            if (h != null) {
                ((d) eVar).a(d.a.STATE_LOGGING_OUT);
                ((d) eVar).d(h);
                h = null;
            }
        }
        return true;
    }

    static /* synthetic */ boolean a(PayPalActivity payPalActivity) {
        j lastElement = c.d.lastElement();
        if (payPalActivity.d.size() != 1) {
            payPalActivity.d.remove(lastElement);
            j lastElement2 = payPalActivity.d.lastElement();
            if (lastElement2 != null) {
                payPalActivity.setContentView(lastElement2);
            }
        }
        lastElement.a();
        return true;
    }

    private void b() {
        String appID = PayPal.getInstance().getAppID();
        _pushIntent = appID + "PUSH_DIALOG_";
        _popIntent = appID + "POP_DIALOG";
        _replaceIntent = appID + "REPLACE_DIALOG_";
        _updateIntent = appID + "UPDATE_VIEW";
        LOGIN_SUCCESS = appID + "LOGIN_SUCCESS";
        LOGIN_FAIL = appID + "LOGIN_FAIL";
        AUTH_SUCCESS = appID + "AUTH_SUCCESS";
        CREATE_PAYMENT_SUCCESS = appID + "CREATE_PAYMENT_SUCCESS";
        CREATE_PAYMENT_FAIL = appID + "CREATE_PAYMENT_FAIL";
        FATAL_ERROR = appID + "FATAL_ERROR";
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(_popIntent);
        for (int i2 = 0; i2 < 9; i2++) {
            intentFilter.addAction(_pushIntent + i2);
            intentFilter.addAction(_replaceIntent + i2);
        }
        intentFilter.addAction(_updateIntent);
        registerReceiver(this.j, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction(LOGIN_SUCCESS);
        intentFilter2.addAction(LOGIN_FAIL);
        intentFilter2.addAction(CREATE_PAYMENT_SUCCESS);
        intentFilter2.addAction(CREATE_PAYMENT_FAIL);
        intentFilter2.addAction(FATAL_ERROR);
        registerReceiver(this.i, intentFilter2);
        b.c();
    }

    static /* synthetic */ boolean b(PayPalActivity payPalActivity, int i2) {
        j lastElement = payPalActivity.d.lastElement();
        if (!payPalActivity.a(i2)) {
            return false;
        }
        payPalActivity.d.remove(lastElement);
        return true;
    }

    private void c() {
        if (this.d == null || this.d.size() <= 0) {
            finish();
            c = null;
            return;
        }
        j lastElement = this.d.lastElement();
        this.e = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 2, 0.5f, 2, 0.5f);
        this.e.setDuration(500);
        this.e.setRepeatCount(0);
        this.e.setAnimationListener(this);
        lastElement.setAnimation(this.e);
        runOnUiThread(new l(lastElement, this.e));
    }

    public static PayPalActivity getInstance() {
        return c;
    }

    public final MEPAmounts AdjustAmounts(MEPAddress mEPAddress, String str, String str2, String str3, String str4) {
        if (this.a != null) {
            return this.a.adjustAmount(mEPAddress, str, str2, str3, str4);
        }
        return null;
    }

    public final Vector<MEPReceiverAmounts> adjustAmountsAdvanced(MEPAddress mEPAddress, String str, Vector<MEPReceiverAmounts> vector) {
        if (this.a != null) {
            return this.a.adjustAmountsAdvanced(mEPAddress, str, vector);
        }
        return null;
    }

    public final j getDialog() {
        return this.d.lastElement();
    }

    public final void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 3) {
            setResult(i3, intent);
            c();
        }
    }

    public final void onAnimationEnd(Animation animation) {
        if (animation == this.e) {
            c.finish();
            c = null;
        }
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        int i2 = 0;
        super.onCreate(bundle);
        this.g = false;
        Hashtable hashtable = (Hashtable) getLastNonConfigurationInstance();
        if (hashtable != null) {
            _paypal = (PayPal) hashtable.get("PayPal");
            Vector vector = (Vector) hashtable.get("ViewStack");
            this.d = new Vector<>(0);
            _networkHandler = (b) hashtable.get("NetworkHandler");
            if (hashtable.get("ReviewViewInfo") != null) {
                a.a = (Hashtable) hashtable.get("ReviewViewInfo");
            }
            Object obj = null;
            b();
            while (true) {
                int i3 = i2;
                Object obj2 = obj;
                if (i3 >= vector.size()) {
                    break;
                }
                int intValue = ((Integer) vector.elementAt(i3)).intValue();
                obj = intValue == 0 ? new d(this) : intValue == 1 ? new com.paypal.android.MEP.a.b(this) : intValue == 2 ? new f(this) : intValue == 3 ? new a(this) : intValue == 4 ? new h(this) : intValue == 5 ? new c(this) : intValue == 6 ? new g(this) : intValue == 7 ? new e(this) : obj2;
                this.d.add(obj);
                i2 = i3 + 1;
            }
            Editable editable = (Editable) hashtable.get("UserString");
            EditText editText = (EditText) findViewById(5004);
            if (!(editText == null || editable == null || editable.length() <= 0)) {
                editText.setText(editable);
            }
            Editable editable2 = (Editable) hashtable.get("PasswordString");
            EditText editText2 = (EditText) findViewById(5005);
            if (!(editText2 == null || editable2 == null || editable2.length() <= 0)) {
                editText2.setText(editable2);
            }
            setContentView(this.d.lastElement());
            this.d.lastElement();
            c = this;
            return;
        }
        Intent intent = getIntent();
        if (intent.hasExtra(EXTRA_PAYMENT_INFO) || intent.hasExtra(EXTRA_PREAPPROVAL_INFO)) {
            if (intent.hasExtra(EXTRA_PAYMENT_ADJUSTER)) {
                this.a = (PaymentAdjuster) intent.getSerializableExtra(EXTRA_PAYMENT_ADJUSTER);
            }
            if (intent.hasExtra(EXTRA_RESULT_DELEGATE)) {
                this.b = (PayPalResultDelegate) intent.getSerializableExtra(EXTRA_RESULT_DELEGATE);
            }
            b();
            PayPal instance = PayPal.getInstance();
            if (instance.getPayType() == 3) {
                PayPalPreapproval payPalPreapproval = (PayPalPreapproval) intent.getSerializableExtra(EXTRA_PREAPPROVAL_INFO);
                if (payPalPreapproval == null) {
                    paymentFailed((String) b.e().c("CorrelationId"), (String) b.e().c("PayKey"), "-1", com.paypal.android.a.h.a("ANDROID_no_payment"), true, true);
                    return;
                } else if (!payPalPreapproval.isValid() || instance.getPreapprovalKey() == null || instance.getPreapprovalKey().length() <= 0) {
                    paymentFailed((String) b.e().c("CorrelationId"), (String) b.e().c("PayKey"), "-1", com.paypal.android.a.h.a("ANDROID_invalid_payment"), true, true);
                    return;
                } else if (!b.a()) {
                    paymentFailed((String) b.e().c("CorrelationId"), (String) b.e().c("PayKey"), "-1", com.paypal.android.a.h.a("ANDROID_application_not_authorized"), true, true);
                    return;
                }
            } else {
                PayPalAdvancedPayment payPalAdvancedPayment = (PayPalAdvancedPayment) intent.getSerializableExtra(EXTRA_PAYMENT_INFO);
                if (payPalAdvancedPayment == null) {
                    paymentFailed((String) b.e().c("CorrelationId"), (String) b.e().c("PayKey"), "-1", com.paypal.android.a.h.a("ANDROID_no_payment"), true, true);
                    return;
                } else if (!payPalAdvancedPayment.isValid()) {
                    paymentFailed((String) b.e().c("CorrelationId"), (String) b.e().c("PayKey"), "-1", com.paypal.android.a.h.a("ANDROID_invalid_payment"), true, true);
                    return;
                } else if (!b.a()) {
                    paymentFailed((String) b.e().c("CorrelationId"), (String) b.e().c("PayKey"), "-1", com.paypal.android.a.h.a("ANDROID_application_not_authorized"), true, true);
                    return;
                }
            }
            c = this;
            if (this.d != null) {
                this.d.setSize(0);
            } else {
                this.d = new Vector<>(0);
            }
            d.AnonymousClass1.a(0);
            return;
        }
        throw new NullPointerException("PayPalPayment/Preapproval object does not exist in intent");
    }

    /* access modifiers changed from: protected */
    public final void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(this.j);
            unregisterReceiver(this.i);
        } catch (Exception e2) {
        }
    }

    public final boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (this.d.lastElement() instanceof d) {
            if (((d) this.d.lastElement()).c() != d.a.STATE_LOGGING_IN) {
                new com.paypal.android.MEP.b.f(this).show();
            }
            return true;
        } else if (this.d.lastElement() instanceof a) {
            if (!(((a) this.d.lastElement()).c() == a.C0004a.STATE_SENDING_PAYMENT || ((a) this.d.lastElement()).c() == a.C0004a.STATE_UPDATING)) {
                new com.paypal.android.MEP.b.f(this).show();
            }
            return true;
        } else if (this.d.lastElement() instanceof g) {
            if (((g) this.d.lastElement()).c() != g.a.STATE_CONFIRM_PREAPPROVAL) {
                new com.paypal.android.MEP.b.f(this).show();
            }
            return true;
        } else if ((this.d.lastElement() instanceof f) || (this.d.lastElement() instanceof com.paypal.android.MEP.a.b)) {
            d.AnonymousClass1.a();
            return true;
        } else if (!(this.d.lastElement() instanceof h) && !(this.d.lastElement() instanceof e)) {
            return super.onKeyDown(i2, keyEvent);
        } else {
            if (!this.g) {
                this.g = true;
                paymentSucceeded((String) _networkHandler.c("PayKey"), (String) _networkHandler.c("PaymentExecStatus"), true);
            }
            return true;
        }
    }

    public final Object onRetainNonConfigurationInstance() {
        Editable text;
        Editable text2;
        Hashtable hashtable = new Hashtable();
        hashtable.put("PayPal", _paypal);
        Vector vector = new Vector();
        for (int i2 = 0; i2 < this.d.size(); i2++) {
            j elementAt = this.d.elementAt(i2);
            vector.add(new Integer(elementAt instanceof d ? 0 : elementAt instanceof com.paypal.android.MEP.a.b ? 1 : elementAt instanceof f ? 2 : elementAt instanceof a ? 3 : elementAt instanceof h ? 4 : elementAt instanceof c ? 5 : elementAt instanceof g ? 6 : elementAt instanceof e ? 7 : 0));
        }
        hashtable.put("ViewStack", vector);
        hashtable.put("NetworkHandler", _networkHandler);
        if (a.a != null) {
            hashtable.put("ReviewViewInfo", a.a);
        }
        EditText editText = (EditText) findViewById(5004);
        if (!(editText == null || (text2 = editText.getText()) == null || text2.length() <= 0)) {
            hashtable.put("UserString", text2);
        }
        EditText editText2 = (EditText) findViewById(5005);
        if (!(editText2 == null || (text = editText2.getText()) == null || text.length() <= 0)) {
            hashtable.put("PasswordString", text);
        }
        return hashtable;
    }

    public final void paymentCanceled() {
        if (this.transactionSuccessful) {
            paymentSucceeded((String) b.e().c("PayKey"), (String) b.e().c("PaymentExecStatus"));
            return;
        }
        if (this.b != null) {
            this.b.onPaymentCanceled("OTHER");
        }
        Intent intent = new Intent();
        intent.putExtra(EXTRA_PAYMENT_STATUS, "OTHER");
        setResult(0, intent);
        c();
    }

    public final void paymentFailed(String str, String str2, String str3, String str4, boolean z, boolean z2) {
        if (this.transactionSuccessful) {
            paymentSucceeded((String) b.e().c("PayKey"), (String) b.e().c("PaymentExecStatus"));
            return;
        }
        if (this.b != null && z2) {
            this.b.onPaymentFailed("OTHER", str, str2, str3, str4);
        }
        if (z) {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_CORRELATION_ID, str);
            intent.putExtra(EXTRA_PAY_KEY, str2);
            intent.putExtra(EXTRA_ERROR_ID, str3);
            intent.putExtra(EXTRA_ERROR_MESSAGE, str4);
            intent.putExtra(EXTRA_PAYMENT_STATUS, "OTHER");
            setResult(2, intent);
            c();
        }
    }

    public final void paymentSucceeded(String str, String str2) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_PAY_KEY, str);
        intent.putExtra(EXTRA_PAYMENT_STATUS, str2);
        setResult(-1, intent);
        c();
    }

    public final void paymentSucceeded(String str, String str2, boolean z) {
        if (this.b != null) {
            this.b.onPaymentSucceeded(str, str2);
        }
        if (z) {
            paymentSucceeded(str, str2);
        }
    }

    public final void setTransactionSuccessful(boolean z) {
        this.transactionSuccessful = z;
    }
}
