package com.paypal.android.MEP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.paypal.android.MEP.a.d;
import com.paypal.android.a.b;

class c extends BroadcastReceiver {
    private /* synthetic */ PayPalActivity a;

    c(PayPalActivity payPalActivity) {
        this.a = payPalActivity;
    }

    public final void onReceive(Context context, Intent intent) {
        String unused = PayPalActivity.h = (String) null;
        String action = intent.getAction();
        if (action.equals(PayPalActivity.LOGIN_SUCCESS)) {
            if (PayPal.getInstance().getPayType() == 3) {
                d.AnonymousClass1.b(6);
                return;
            }
            b.e().a("mpl-review");
            d.AnonymousClass1.b(3);
        } else if (action.equals(PayPalActivity.LOGIN_FAIL)) {
            d.AnonymousClass1.b(0);
            if (intent.getStringExtra("ERROR_TIMEOUT") != null) {
                String unused2 = PayPalActivity.h = intent.getStringExtra("ERROR_TIMEOUT");
            }
        } else if (action.equals(PayPalActivity.CREATE_PAYMENT_SUCCESS)) {
            if (PayPal.getInstance().getPayType() == 3) {
                d.AnonymousClass1.b(6);
                return;
            }
            b.e().a("mpl-review");
            d.AnonymousClass1.b(3);
        } else if (action.equals(PayPalActivity.FATAL_ERROR)) {
            Intent unused3 = this.a.f = intent;
            d.AnonymousClass1.b(5);
        }
    }
}
