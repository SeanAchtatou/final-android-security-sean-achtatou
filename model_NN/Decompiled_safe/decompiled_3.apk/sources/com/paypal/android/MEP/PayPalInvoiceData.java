package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;

public class PayPalInvoiceData implements Serializable {
    private static final long serialVersionUID = 4;
    ArrayList<PayPalInvoiceItem> a;
    private BigDecimal b;
    private BigDecimal c;

    public PayPalInvoiceData() {
        this.a = new ArrayList<>();
        this.b = null;
        this.c = null;
    }

    public PayPalInvoiceData(BigDecimal bigDecimal, BigDecimal bigDecimal2) {
        this.a = new ArrayList<>();
        this.b = bigDecimal;
        this.c = bigDecimal2;
    }

    public void add(PayPalInvoiceItem payPalInvoiceItem) {
        this.a.add(payPalInvoiceItem);
    }

    public ArrayList<PayPalInvoiceItem> getInvoiceItems() {
        return this.a;
    }

    public BigDecimal getShipping() {
        return this.c;
    }

    public BigDecimal getTax() {
        return this.b;
    }

    public void setInvoiceItems(ArrayList<PayPalInvoiceItem> arrayList) {
        this.a = arrayList;
    }

    public void setShipping(BigDecimal bigDecimal) {
        this.c = bigDecimal.setScale(2, 4);
    }

    public void setTax(BigDecimal bigDecimal) {
        this.b = bigDecimal.setScale(2, 4);
    }
}
