package com.paypal.android.MEP;

import java.math.BigDecimal;

public class MEPAmounts {
    private String a;
    private BigDecimal b;
    private BigDecimal c;
    private BigDecimal d;

    public String getCurrency() {
        return this.a;
    }

    public BigDecimal getPaymentAmount() {
        return this.b;
    }

    public BigDecimal getShipping() {
        return this.d;
    }

    public BigDecimal getTax() {
        return this.c;
    }

    public void setCurrency(String str) {
        this.a = str;
    }

    public void setPaymentAmount(String str) {
        try {
            this.b = new BigDecimal(str);
        } catch (NumberFormatException e) {
            this.b = new BigDecimal("0.0");
        }
    }

    public void setPaymentAmount(BigDecimal bigDecimal) {
        this.b = bigDecimal;
    }

    public void setShipping(String str) {
        try {
            this.d = new BigDecimal(str);
        } catch (NumberFormatException e) {
            this.d = new BigDecimal("0.0");
        }
    }

    public void setShipping(BigDecimal bigDecimal) {
        this.d = bigDecimal;
    }

    public void setTax(String str) {
        try {
            this.c = new BigDecimal(str);
        } catch (NumberFormatException e) {
            this.c = new BigDecimal("0.0");
        }
    }

    public void setTax(BigDecimal bigDecimal) {
        this.c = bigDecimal;
    }
}
