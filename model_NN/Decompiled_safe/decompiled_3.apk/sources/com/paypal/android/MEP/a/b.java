package com.paypal.android.MEP.a;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.paypal.android.MEP.a.d;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.o;
import com.paypal.android.b.h;
import com.paypal.android.b.j;

public final class b extends j implements View.OnClickListener {
    private Button a;

    public b(Context context) {
        super(context);
        setId(9005);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        linearLayout.setBackgroundDrawable(d.a());
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setGravity(1);
        linearLayout.addView(new h(com.paypal.android.a.h.a("ANDROID_about_quickpay"), context));
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout2.setPadding(0, 0, 0, 15);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -1510918, -1510918, -1510918, -1510918, -1510918});
        gradientDrawable.setCornerRadius(10.0f);
        gradientDrawable.setStroke(2, -8280890);
        linearLayout2.setBackgroundDrawable(gradientDrawable);
        TextView a2 = o.a(o.a.HELVETICA_16_BOLD, context);
        a2.setText(com.paypal.android.a.h.a("ANDROID_for_checkout"));
        linearLayout2.addView(a2);
        TextView a3 = o.a(o.a.HELVETICA_16_NORMAL, context);
        a3.setText(com.paypal.android.a.h.a("ANDROID_quickpay_help"));
        linearLayout2.addView(a3);
        linearLayout.addView(linearLayout2);
        this.a = new Button(context);
        this.a.setText(com.paypal.android.a.h.a("ANDROID_ok"));
        this.a.setLayoutParams(new RelativeLayout.LayoutParams(-1, d.b()));
        this.a.setGravity(17);
        this.a.setBackgroundDrawable(e.a());
        this.a.setTextColor(-16777216);
        this.a.setOnClickListener(this);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(1);
        linearLayout3.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout3.setPadding(0, 15, 0, 0);
        linearLayout3.addView(this.a);
        linearLayout.addView(linearLayout3);
        addView(linearLayout);
    }

    public final void a() {
    }

    public final void b() {
    }

    public final void onClick(View view) {
        if (view == this.a) {
            d.AnonymousClass1.a();
        }
    }
}
