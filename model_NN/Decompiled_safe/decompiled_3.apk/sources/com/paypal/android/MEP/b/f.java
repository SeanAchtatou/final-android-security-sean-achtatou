package com.paypal.android.MEP.b;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalAdvancedPayment;
import com.paypal.android.a.d;
import com.paypal.android.a.e;
import com.paypal.android.a.h;
import com.paypal.android.a.o;

public final class f extends Dialog implements View.OnClickListener {
    private Button a;
    private Button b;

    public f(Context context) {
        super(context);
        String a2;
        setCancelable(false);
        setCanceledOnTouchOutside(false);
        requestWindowFeature(1);
        LinearLayout a3 = d.a(context, -1, -1);
        a3.setOrientation(1);
        a3.setGravity(1);
        a3.setPadding(10, 5, 10, 5);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1510918, -4336918});
        gradientDrawable.setCornerRadius(3.0f);
        gradientDrawable.setStroke(0, 0);
        a3.setBackgroundDrawable(gradientDrawable);
        TextView a4 = o.a(o.a.HELVETICA_16_BOLD, context);
        a4.setText(h.a("ANDROID_cancel_transaction"));
        a4.setGravity(17);
        a4.setPadding(0, 0, 0, 10);
        a3.addView(a4);
        String a5 = h.a("ANDROID_go_back_to_merchant");
        PayPal instance = PayPal.getInstance();
        PayPalAdvancedPayment payment = instance.getPayment();
        if (instance.getPayType() == 3) {
            a2 = instance.getPreapproval().getMerchantName();
        } else if (instance.getPayType() == 0) {
            if (instance.isPersonalPayment()) {
                a2 = payment.getReceivers().get(0).getRecipient();
            } else {
                a2 = payment.getReceivers().get(0).getMerchantName();
                if (a2 == null || a2.length() <= 0) {
                    a2 = payment.getReceivers().get(0).getRecipient();
                }
            }
        } else if (instance.getPayType() == 2) {
            a2 = payment.getPrimaryReceiver().getMerchantName();
            if (a2 == null || a2.length() <= 0) {
                a2 = payment.getPrimaryReceiver().getRecipient();
            }
        } else {
            a2 = h.a("ANDROID_the_merchant");
        }
        String replace = a5.replace("%m", a2);
        TextView a6 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a6.setText(replace);
        a6.setGravity(17);
        a3.addView(a6);
        TextView a7 = o.a(o.a.HELVETICA_14_NORMAL, context);
        a7.setText(h.a("ANDROID_lose_all_information"));
        a7.setGravity(17);
        a3.addView(a7);
        LinearLayout a8 = d.a(context, -1, -2);
        a8.setOrientation(0);
        a8.setGravity(16);
        a8.setPadding(0, 10, 0, 10);
        LinearLayout a9 = d.a(context, -1, -2, 0.5f);
        a9.setOrientation(1);
        a9.setGravity(1);
        a9.setPadding(0, 0, 3, 0);
        this.a = new Button(context);
        this.a.setText(h.a("ANDROID_ok"));
        this.a.setTextColor(-16777216);
        this.a.setLayoutParams(new LinearLayout.LayoutParams(-1, d.b()));
        this.a.setGravity(17);
        this.a.setBackgroundDrawable(e.a());
        this.a.setOnClickListener(this);
        a9.addView(this.a);
        a8.addView(a9);
        LinearLayout a10 = d.a(context, -1, -2, 0.5f);
        a10.setOrientation(1);
        a10.setGravity(1);
        a10.setPadding(3, 0, 0, 0);
        this.b = new Button(context);
        this.b.setText(h.a("ANDROID_cancel"));
        this.b.setTextColor(-16777216);
        this.b.setLayoutParams(new LinearLayout.LayoutParams(-1, d.b()));
        this.b.setGravity(17);
        this.b.setBackgroundDrawable(e.b());
        this.b.setOnClickListener(this);
        a10.addView(this.b);
        a8.addView(a10);
        a3.addView(a8);
        setContentView(a3);
    }

    public final void onClick(View view) {
        if (view == this.a) {
            dismiss();
            PayPalActivity.getInstance().paymentCanceled();
        } else if (view == this.b) {
            dismiss();
        }
    }
}
