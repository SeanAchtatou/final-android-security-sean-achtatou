package com.paypal.android.MEP;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Iterator;

public class PayPalPayment implements Serializable {
    private static final long serialVersionUID = 1;
    private String a;
    private String b;
    private BigDecimal c;
    private PayPalInvoiceData d;
    private int e;
    private int f;
    private String g;
    private String h;
    private String i;
    private String j;

    public PayPalPayment() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = 3;
        this.f = 22;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
    }

    public PayPalPayment(String str, Currency currency, int i2, PayPalInvoiceData payPalInvoiceData, String str2, String str3, String str4, String str5) {
        this.b = str.replace(" ", "");
        this.a = currency.getCurrencyCode();
        this.d = payPalInvoiceData;
        this.e = i2;
        this.f = 22;
        this.g = str3;
        this.h = str2;
        this.i = str4;
        this.j = str5;
        this.c = new BigDecimal(0);
        Iterator<PayPalInvoiceItem> it = payPalInvoiceData.a.iterator();
        while (it.hasNext()) {
            this.c = this.c.add(it.next().a);
        }
        this.c = this.c.setScale(2, 4);
    }

    public String getCurrencyType() {
        return this.a;
    }

    public String getCustomID() {
        return this.g;
    }

    @Deprecated
    public String getDescription() {
        return this.j;
    }

    public PayPalInvoiceData getInvoiceData() {
        return this.d;
    }

    public String getIpnUrl() {
        return this.i;
    }

    public String getMemo() {
        return this.j;
    }

    public String getMerchantName() {
        return this.h;
    }

    public int getPaymentSubtype() {
        return this.f;
    }

    public int getPaymentType() {
        return this.e;
    }

    public String getRecipient() {
        return this.b;
    }

    public BigDecimal getSubtotal() {
        return this.c;
    }

    public void setCurrencyType(String str) {
        this.a = str.toUpperCase();
    }

    public void setCurrencyType(Currency currency) {
        this.a = currency.getCurrencyCode();
    }

    public void setCustomID(String str) {
        this.g = str;
    }

    @Deprecated
    public void setDescription(String str) {
        this.j = str;
    }

    public void setInvoiceData(PayPalInvoiceData payPalInvoiceData) {
        this.d = payPalInvoiceData;
    }

    public void setIpnUrl(String str) {
        this.i = str;
    }

    public void setMemo(String str) {
        this.j = str;
    }

    public void setMerchantName(String str) {
        this.h = str;
    }

    public void setPaymentSubtype(int i2) {
        this.f = i2;
    }

    public void setPaymentType(int i2) {
        this.e = i2;
    }

    public void setRecipient(String str) {
        this.b = str.replace(" ", "");
    }

    public void setSubtotal(BigDecimal bigDecimal) {
        this.c = bigDecimal.setScale(2, 4);
    }
}
