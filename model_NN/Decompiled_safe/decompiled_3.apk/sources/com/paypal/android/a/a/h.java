package com.paypal.android.a.a;

import com.facebook.internal.ServerProtocol;
import com.paypal.android.a.m;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class h {
    private String a = null;
    private String b = null;
    private String c = null;
    private String d = null;
    private String e = null;
    private String f = null;
    private String g = null;
    private String h = null;

    public final String a() {
        return this.a;
    }

    public final void a(String str) {
        this.b = str;
    }

    public final boolean a(Element element) {
        NodeList elementsByTagName = element.getElementsByTagName("baseAddress");
        if (elementsByTagName.getLength() == 0) {
            return false;
        }
        Element element2 = (Element) elementsByTagName.item(0);
        NodeList elementsByTagName2 = element2.getElementsByTagName("line1");
        if (elementsByTagName2.getLength() == 0) {
            return false;
        }
        this.d = m.a(((Element) elementsByTagName2.item(0)).getChildNodes());
        NodeList elementsByTagName3 = element2.getElementsByTagName("line2");
        if (elementsByTagName3.getLength() > 0) {
            this.e = m.a(((Element) elementsByTagName3.item(0)).getChildNodes());
        }
        NodeList elementsByTagName4 = element2.getElementsByTagName("city");
        if (elementsByTagName4.getLength() == 0) {
            return false;
        }
        this.b = m.a(((Element) elementsByTagName4.item(0)).getChildNodes());
        NodeList elementsByTagName5 = element2.getElementsByTagName("state");
        if (elementsByTagName5.getLength() > 0) {
            this.g = m.a(((Element) elementsByTagName5.item(0)).getChildNodes());
        }
        NodeList elementsByTagName6 = element2.getElementsByTagName("postalCode");
        if (elementsByTagName6.getLength() > 0) {
            this.f = m.a(((Element) elementsByTagName6.item(0)).getChildNodes());
        }
        NodeList elementsByTagName7 = element2.getElementsByTagName("countryCode");
        if (elementsByTagName7.getLength() == 0) {
            return false;
        }
        this.c = m.a(((Element) elementsByTagName7.item(0)).getChildNodes());
        NodeList elementsByTagName8 = element2.getElementsByTagName(ServerProtocol.DIALOG_PARAM_TYPE);
        if (elementsByTagName8.getLength() > 0) {
            m.a(((Element) elementsByTagName8.item(0)).getChildNodes());
        }
        NodeList elementsByTagName9 = element.getElementsByTagName("addressId");
        if (elementsByTagName9.getLength() == 0) {
            return false;
        }
        this.h = m.a(((Element) elementsByTagName9.item(0)).getChildNodes());
        NodeList elementsByTagName10 = element.getElementsByTagName("addresseeName");
        if (elementsByTagName10.getLength() > 0) {
            this.a = m.a(((Element) elementsByTagName10.item(0)).getChildNodes());
        }
        return true;
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.c = str;
    }

    public final String c() {
        return this.c;
    }

    public final void c(String str) {
        this.d = str;
    }

    public final String d() {
        return this.d;
    }

    public final void d(String str) {
        this.e = str;
    }

    public final String e() {
        return this.e;
    }

    public final void e(String str) {
        this.f = str;
    }

    public final String f() {
        return this.f;
    }

    public final void f(String str) {
        this.g = str;
    }

    public final String g() {
        return this.g;
    }

    public final void g(String str) {
        this.h = str;
    }

    public final String h() {
        return this.h;
    }
}
