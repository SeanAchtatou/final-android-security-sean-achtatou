package com.paypal.android.a;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalAdvancedPayment;

public final class o {

    public enum a {
        HELVETICA_16_BOLD,
        HELVETICA_16_NORMAL,
        HELVETICA_14_BOLD,
        HELVETICA_14_NORMAL,
        HELVETICA_12_BOLD,
        HELVETICA_12_NORMAL,
        HELVETICA_10_BOLD,
        HELVETICA_10_NORMAL
    }

    public static TextView a(a aVar, Context context) {
        Rect rect = new Rect(5, 2, 5, 3);
        Rect rect2 = new Rect(0, 0, 0, 0);
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2, 0.5f);
        layoutParams.setMargins(rect2.left, rect2.top, rect2.right, rect2.bottom);
        textView.setLayoutParams(layoutParams);
        textView.setBackgroundColor(0);
        textView.setTextColor(-16777216);
        textView.setGravity(3);
        textView.setPadding(rect.left, rect.top, rect.right, rect.bottom);
        if (aVar == a.HELVETICA_16_BOLD || aVar == a.HELVETICA_14_BOLD || aVar == a.HELVETICA_12_BOLD || aVar == a.HELVETICA_10_BOLD) {
            textView.setTypeface(Typeface.create("Helvetica", 1));
        } else if (aVar != a.HELVETICA_16_NORMAL && aVar != a.HELVETICA_14_NORMAL && aVar != a.HELVETICA_12_NORMAL && aVar != a.HELVETICA_10_NORMAL) {
            return null;
        } else {
            textView.setTypeface(Typeface.create("Helvetica", 0));
        }
        if (aVar == a.HELVETICA_16_BOLD || aVar == a.HELVETICA_16_NORMAL) {
            textView.setTextSize(16.0f);
        } else if (aVar == a.HELVETICA_14_BOLD || aVar == a.HELVETICA_14_NORMAL) {
            textView.setTextSize(14.0f);
        } else if (aVar == a.HELVETICA_12_BOLD || aVar == a.HELVETICA_12_NORMAL) {
            textView.setTextSize(12.0f);
        } else if (aVar != a.HELVETICA_10_BOLD && aVar != a.HELVETICA_10_NORMAL) {
            return null;
        } else {
            textView.setTextSize(10.0f);
        }
        return textView;
    }

    public static TextView b(a aVar, Context context) {
        int i = 0;
        TextView a2 = a(aVar, context);
        PayPal instance = PayPal.getInstance();
        PayPalAdvancedPayment payment = instance.getPayment();
        if (instance.getPayType() == 3) {
            a2.setText(instance.getPreapproval().getMerchantName());
        } else if (instance.getPayType() == 0) {
            if (instance.isPersonalPayment()) {
                a2.setText(h.a("ANDROID_send_to") + ": " + payment.getReceivers().get(0).getRecipient());
            } else {
                String merchantName = PayPal.getInstance().getPayment().getMerchantName();
                if (merchantName == null || merchantName.length() <= 0) {
                    String merchantName2 = payment.getReceivers().get(0).getMerchantName();
                    if (merchantName2 == null || merchantName2.length() <= 0) {
                        a2.setText(h.a("ANDROID_send_to") + ": " + payment.getReceivers().get(0).getRecipient());
                    } else {
                        a2.setText(merchantName2);
                    }
                } else {
                    a2.setText(merchantName);
                }
            }
        } else if (instance.getPayType() == 2) {
            String merchantName3 = PayPal.getInstance().getPayment().getMerchantName();
            if (merchantName3 == null || merchantName3.length() <= 0) {
                String merchantName4 = payment.getPrimaryReceiver().getMerchantName();
                if (merchantName4 == null || merchantName4.length() <= 0) {
                    a2.setText(h.a("ANDROID_send_to") + ": " + payment.getPrimaryReceiver().getRecipient());
                } else {
                    a2.setText(merchantName4);
                }
            } else {
                a2.setText(merchantName3);
            }
        } else {
            String merchantName5 = PayPal.getInstance().getPayment().getMerchantName();
            if (merchantName5 == null || merchantName5.length() <= 0) {
                int i2 = 0;
                while (true) {
                    if (i2 < payment.getReceivers().size()) {
                        String merchantName6 = payment.getReceivers().get(i2).getMerchantName();
                        if (merchantName6 != null && merchantName6.length() > 0) {
                            a2.setText(merchantName6);
                            break;
                        }
                        i2++;
                    } else {
                        break;
                    }
                }
            } else {
                a2.setText(merchantName5);
            }
            if (a2.getText() == null || a2.getText().length() <= 0) {
                while (true) {
                    if (i < payment.getReceivers().size()) {
                        String recipient = payment.getReceivers().get(i).getRecipient();
                        if (recipient != null && recipient.length() > 0) {
                            a2.setText(h.a("ANDROID_send_to") + ": " + recipient);
                            break;
                        }
                        i++;
                    } else {
                        break;
                    }
                }
            }
        }
        a2.setGravity(17);
        a2.setPadding(5, 2, 5, 3);
        return a2;
    }
}
