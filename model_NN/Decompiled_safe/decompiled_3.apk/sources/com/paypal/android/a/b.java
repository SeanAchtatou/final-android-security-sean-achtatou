package com.paypal.android.a;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.biznessapps.constants.ReservationSystemConstants;
import com.paypal.android.MEP.MEPAddress;
import com.paypal.android.MEP.MEPAmounts;
import com.paypal.android.MEP.MEPReceiverAmounts;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalAdvancedPayment;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.paypal.android.MEP.a;
import com.paypal.android.a.a.h;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerPNames;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import twitter4j.conf.PropertyConfiguration;

public final class b implements a.b {
    protected static com.paypal.android.c.a a = new i();
    protected static com.paypal.android.c.a b = new j();
    /* access modifiers changed from: private */
    public static List<String> k = new ArrayList();
    private static a l = null;
    private static byte[] m = {48, -126, 3, 115, 48, -126, 2, 91, 2, 4, 77, 122, -66, 90, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 5, 5, 0, 48, 126, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 11, 48, 9, 6, 3, 85, 4, 8, 19, 2, 67, 65, 49, 17, 48, 15, 6, 3, 85, 4, 7, 19, 8, 83, 97, 110, 32, 74, 111, 115, 101, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 80, 97, 121, 80, 97, 108, 44, 32, 73, 110, 99, 46, 49, 20, 48, 18, 6, 3, 85, 4, 11, 12, 11, 115, 116, 97, 103, 101, 95, 99, 101, 114, 116, 115, 49, 34, 48, 32, 6, 3, 85, 4, 3, 12, 25, 115, 116, 97, 103, 101, 95, 109, 112, 108, 95, 101, 110, 99, 114, 121, 112, 116, 105, 111, 110, 95, 99, 101, 114, 116, 48, 30, 23, 13, 49, 49, 48, 51, 49, 50, 48, 48, 50, 57, 49, 52, 90, 23, 13, 51, 54, 48, 51, 48, 53, 48, 48, 50, 57, 49, 52, 90, 48, 126, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 11, 48, 9, 6, 3, 85, 4, 8, 19, 2, 67, 65, 49, 17, 48, 15, 6, 3, 85, 4, 7, 19, 8, 83, 97, 110, 32, 74, 111, 115, 101, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 80, 97, 121, 80, 97, 108, 44, 32, 73, 110, 99, 46, 49, 20, 48, 18, 6, 3, 85, 4, 11, 12, 11, 115, 116, 97, 103, 101, 95, 99, 101, 114, 116, 115, 49, 34, 48, 32, 6, 3, 85, 4, 3, 12, 25, 115, 116, 97, 103, 101, 95, 109, 112, 108, 95, 101, 110, 99, 114, 121, 112, 116, 105, 111, 110, 95, 99, 101, 114, 116, 48, -126, 1, 34, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -126, 1, 15, 0, 48, -126, 1, 10, 2, -126, 1, 1, 0, -103, -59, -112, -109, 34, 95, -44, 88, -8, 47, -24, 97, -96, 24, -27, -16, 96, 78, -96, 101, 96, -55, 117, -103, 29, 116, 85, 25, -52, 5, 71, -21, 1, -114, -57, -50, 36, -31, 103, 39, -58, 32, 21, -82, -3, 118, 42, 1, -93, -111, 114, -11, 73, 63, 99, 97, -26, 103, 97, 49, 39, 33, -12, 31, -40, 14, -100, -99, -80, -53, -105, -48, 28, 72, 53, 0, 89, -44, -97, -45, 39, -96, -9, 1, 73, -23, 58, 3, 126, -61, -86, 5, -64, -10, -107, -64, 50, 79, 64, 42, 114, 33, 121, 59, 121, -122, -105, 6, -70, 19, 83, -91, 48, -9, 78, 112, 91, 71, -30, -61, 3, 113, 84, -47, -9, 103, -4, 40, 20, -29, -95, -124, -125, Byte.MIN_VALUE, 42, -24, -37, 40, 74, -121, -45, -87, -10, -42, -63, 45, 111, -30, -3, 68, 8, -60, -22, -8, -25, -99, 12, -89, -117, 1, -95, -110, -1, -58, 12, -7, 59, -22, -61, 9, 122, -27, -52, -114, -92, 17, 59, -16, 95, -111, -124, -110, -31, -5, 116, -112, -121, -123, -11, -69, -24, 46, 117, -8, 120, -40, 101, -124, 70, 113, -79, -25, 120, -14, -49, -32, -53, -17, -4, -85, -102, 85, -85, 44, 95, 69, 92, -49, 3, -13, 14, 86, -74, 72, 93, -45, -114, 32, 43, -4, -95, -26, 118, -78, 122, -73, 25, 118, -82, -108, -66, 107, 98, -102, 91, 46, 123, 81, -113, 30, -54, 120, -44, -26, -84, -97, 2, 3, 1, 0, 1, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 5, 5, 0, 3, -126, 1, 1, 0, 123, -27, -3, -116, -46, -77, 0, -88, -121, -22, -28, -105, -75, 59, -109, Byte.MIN_VALUE, 87, 99, 8, 92, -122, 44, -88, -116, 0, -92, 69, 89, 32, 2, -41, 9, -90, -20, 73, 5, -40, 123, 73, 33, 40, -45, -124, -45, -88, 50, 66, -102, -103, -38, 18, 5, -17, 28, 47, 82, -28, 87, 7, -50, 108, 88, 120, 119, 105, 32, Byte.MIN_VALUE, -48, -30, -50, -2, -58, -69, -126, -80, 104, -101, -11, 27, 13, -36, 112, -36, 77, 16, 78, -76, -91, -119, -56, -32, 98, 68, -44, 86, 91, 57, 85, 29, 60, 34, 7, -96, 42, 60, -101, -40, -62, -41, -109, 17, -50, -86, 58, 82, 64, -72, 73, -95, -91, -13, -57, -33, -76, 9, -15, 85, 125, -17, 83, -61, -52, 92, -30, -76, -118, 0, -101, 12, -109, 20, 74, -114, -21, -32, -15, 68, -58, -126, -102, 86, -87, 68, -34, -101, -108, 43, 70, -75, -88, 84, 101, -30, -57, -59, 90, -102, 49, 121, 34, -24, 30, -65, -59, -12, 21, 8, -115, 102, -104, -81, 7, 126, -8, -8, -124, 12, 123, 42, 54, 9, 63, 22, 11, -15, 124, 51, -76, 68, -87, -113, -127, -93, -32, 58, 30, 2, -90, -15, -83, -81, 117, -61, 24, -25, -120, 1, -109, 69, 92, 92, -8, 103, 7, -63, -107, 54, 55, 89, 50, -13, 44, 34, 87, -88, -98, 60, 103, 4, 93, -89, 0, 37, 118, -113, 59, -114, -9, 106, -92, -110, 81, 105, 109, -87, 6};
    private static byte[] n = {48, -126, 2, 106, 48, -126, 1, -45, 2, 4, 77, -90, -88, -61, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 5, 5, 0, 48, 124, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 11, 48, 9, 6, 3, 85, 4, 8, 19, 2, 67, 65, 49, 17, 48, 15, 6, 3, 85, 4, 7, 19, 8, 83, 97, 110, 32, 74, 111, 115, 101, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 80, 97, 121, 80, 97, 108, 44, 32, 73, 110, 99, 46, 49, 19, 48, 17, 6, 3, 85, 4, 11, 12, 10, 108, 105, 118, 101, 95, 99, 101, 114, 116, 115, 49, 33, 48, 31, 6, 3, 85, 4, 3, 12, 24, 108, 105, 118, 101, 95, 109, 112, 108, 95, 101, 110, 99, 114, 121, 112, 116, 105, 111, 110, 95, 99, 101, 114, 116, 48, 30, 23, 13, 49, 49, 48, 52, 49, 52, 48, 55, 53, 54, 53, 49, 90, 23, 13, 49, 51, 48, 54, 50, 50, 48, 55, 53, 54, 53, 49, 90, 48, 124, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 11, 48, 9, 6, 3, 85, 4, 8, 19, 2, 67, 65, 49, 17, 48, 15, 6, 3, 85, 4, 7, 19, 8, 83, 97, 110, 32, 74, 111, 115, 101, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 80, 97, 121, 80, 97, 108, 44, 32, 73, 110, 99, 46, 49, 19, 48, 17, 6, 3, 85, 4, 11, 12, 10, 108, 105, 118, 101, 95, 99, 101, 114, 116, 115, 49, 33, 48, 31, 6, 3, 85, 4, 3, 12, 24, 108, 105, 118, 101, 95, 109, 112, 108, 95, 101, 110, 99, 114, 121, 112, 116, 105, 111, 110, 95, 99, 101, 114, 116, 48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127, -115, 0, 48, -127, -119, 2, -127, -127, 0, -82, Byte.MIN_VALUE, -69, 76, -124, -63, 108, -113, -33, 17, 7, -86, -95, -7, 114, -125, -63, -125, 14, -67, 18, 68, -95, -61, 17, -55, -17, -28, -18, -28, -117, -37, -9, 59, -39, 46, 54, 1, -64, 14, -78, 6, -29, -41, 90, -20, 108, 58, -60, 22, -68, -125, 74, -14, 18, -123, -36, -41, 77, -106, -25, -74, -5, 112, 126, 108, 86, 44, 0, -61, 121, -21, -43, -3, 74, -125, -122, 114, 42, 109, -48, 61, 77, -52, 121, -59, 51, -108, -52, -44, 64, 41, 84, -92, -43, 97, -77, 81, -88, -111, 124, 16, -110, -85, -11, 2, -76, 103, 50, -56, -93, 1, -115, 82, 43, 74, 79, 110, -16, -68, 12, 23, -56, 31, -108, -113, 82, 47, 2, 3, 1, 0, 1, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 5, 5, 0, 3, -127, -127, 0, 118, -105, -7, 69, -119, -75, 31, 110, 91, 10, 124, -36, -41, -111, 100, -55, -103, -71, 118, -18, -31, 55, -84, -22, 10, -15, 114, 39, -118, -4, 33, -72, -112, 5, 106, 100, -127, 58, 71, 26, -2, 86, -111, 31, -15, 89, 23, 91, 66, 65, 52, 89, 115, -103, -4, -94, 29, 90, -15, 8, 41, 61, 29, 42, -111, 25, -94, 29, 23, 70, -48, 59, -37, -32, 102, 111, 33, 33, -34, -106, -60, 121, -127, -115, -79, -29, -9, 82, -97, -11, -51, 15, -125, -20, 30, -11, 75, -20, -19, 97, 105, 56, -9, -118, 34, 4, 67, -49, -42, 83, 96, -14, -59, 39, -9, -100, -68, -13, -76, 41, 11, -59, -34, -79, 8, -40, -94, -75};
    private static byte[] o = {48, -126, 2, 120, 48, -126, 1, -31, 2, 4, 77, -90, -94, 69, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 5, 5, 0, 48, -127, -126, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 11, 48, 9, 6, 3, 85, 4, 8, 19, 2, 67, 65, 49, 17, 48, 15, 6, 3, 85, 4, 7, 19, 8, 83, 97, 110, 32, 74, 111, 115, 101, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 80, 97, 121, 80, 97, 108, 44, 32, 73, 110, 99, 46, 49, 22, 48, 20, 6, 3, 85, 4, 11, 12, 13, 115, 97, 110, 100, 98, 111, 120, 95, 99, 101, 114, 116, 115, 49, 36, 48, 34, 6, 3, 85, 4, 3, 12, 27, 115, 97, 110, 100, 98, 111, 120, 95, 109, 112, 108, 95, 101, 110, 99, 114, 121, 112, 116, 105, 111, 110, 95, 99, 101, 114, 116, 48, 30, 23, 13, 49, 49, 48, 52, 49, 52, 48, 55, 50, 57, 48, 57, 90, 23, 13, 51, 54, 48, 52, 48, 55, 48, 55, 50, 57, 48, 57, 90, 48, -127, -126, 49, 11, 48, 9, 6, 3, 85, 4, 6, 19, 2, 85, 83, 49, 11, 48, 9, 6, 3, 85, 4, 8, 19, 2, 67, 65, 49, 17, 48, 15, 6, 3, 85, 4, 7, 19, 8, 83, 97, 110, 32, 74, 111, 115, 101, 49, 21, 48, 19, 6, 3, 85, 4, 10, 19, 12, 80, 97, 121, 80, 97, 108, 44, 32, 73, 110, 99, 46, 49, 22, 48, 20, 6, 3, 85, 4, 11, 12, 13, 115, 97, 110, 100, 98, 111, 120, 95, 99, 101, 114, 116, 115, 49, 36, 48, 34, 6, 3, 85, 4, 3, 12, 27, 115, 97, 110, 100, 98, 111, 120, 95, 109, 112, 108, 95, 101, 110, 99, 114, 121, 112, 116, 105, 111, 110, 95, 99, 101, 114, 116, 48, -127, -97, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 1, 5, 0, 3, -127, -115, 0, 48, -127, -119, 2, -127, -127, 0, -122, 60, -17, -98, 126, 97, -124, -43, 10, -104, 124, 117, -41, -15, -106, 43, -52, -80, -89, -13, -9, 71, -122, 28, 97, 30, Byte.MIN_VALUE, 25, 33, 59, -66, 11, 75, -79, 91, -67, 124, -16, -54, 22, 71, 83, -89, 5, -104, 96, -82, 124, -55, -64, 118, 120, -122, 116, -99, 106, -29, -85, -54, 16, 34, -22, -82, -30, -11, 62, 39, 123, 18, -40, 6, -106, 29, -25, 47, 55, 28, 28, 52, 13, 63, -2, 76, 94, 74, 55, -97, -19, 27, -83, 93, 57, 64, -63, -57, -56, -57, -88, 19, 52, -124, -52, 37, -28, -82, -58, 100, 121, 79, 22, 15, 78, -33, -1, -30, 50, -5, -32, 122, -6, -116, -116, 84, -105, 42, 86, -30, 99, 2, 3, 1, 0, 1, 48, 13, 6, 9, 42, -122, 72, -122, -9, 13, 1, 1, 5, 5, 0, 3, -127, -127, 0, 46, 29, 11, -21, -95, -37, -60, 16, 5, 46, 112, -28, -72, 2, 44, -41, 38, 93, -73, 15, 112, -42, -15, -15, -80, -120, 55, -35, -36, 7, 111, -74, -52, -81, 60, -125, 69, 117, -103, 79, 113, 25, 36, 44, 29, 97, 97, -66, -8, 94, -85, 28, 96, -106, 24, 64, -1, -55, -112, -52, -57, 35, -36, 122, -21, 122, 37, 54, -68, -48, 36, 11, 11, 91, 58, -100, 126, 28, -109, 19, 95, 49, -46, 25, 77, -110, 80, 122, -22, -41, -98, 60, -118, 30, -111, 62, -89, 9, -20, -104, -100, -120, 69, -110, -4, 18, 110, -50, 83, -17, 115, -76, 111, 95, -106, -5, 8, -93, -31, 13, 54, -122, -97, -41, 9, -63, 96, 32};
    private static String[] p = {"1", "27", "30", "31", "32", "33", "34", "36", "39", "41", "43", "44", "45", "46", "47", "48", "49", "52", "54", "55", "56", "58", "60", "61", "64", "65", "66", "81", "82", "86", "90", "91", "351", "352", "353", "354", "356", "357", "358", "370", "371", "372", "377", "386", "420", "421", "506", "593", "598", "852", "886", "972"};
    private static String q = "";
    private HttpPost c;
    private DefaultHttpClient d = null;
    /* access modifiers changed from: private */
    public int e = -1;
    private int f = -1;
    /* access modifiers changed from: private */
    public int g = -1;
    /* access modifiers changed from: private */
    public Hashtable<String, Object> h;
    /* access modifiers changed from: private */
    public boolean i = false;
    private final Thread j = new k(this);

    private static class a extends Thread {
        private boolean a = false;

        public a() {
            start();
        }

        public final void run() {
            while (true) {
                String str = null;
                synchronized (b.k) {
                    if (b.k.size() == 0) {
                        try {
                            b.k.wait();
                        } catch (InterruptedException e) {
                        }
                    } else {
                        str = (String) b.k.remove(0);
                    }
                }
                if (str != null) {
                    try {
                        b.h().execute(new HttpGet(str));
                        PayPal.logd("NetworkHandler", "TrackingPostThread (), posted tracking " + str);
                    } catch (ClientProtocolException e2) {
                        PayPal.loge("NetworkHandler", "TrackingPostThread (), exception " + e2.getMessage());
                    } catch (IOException e3) {
                        PayPal.loge("NetworkHandler", "TrackingPostThread (), exception " + e3.getMessage());
                    }
                }
            }
        }
    }

    private static String A() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e2) {
            PayPal.loge("Exception", e2.toString());
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean B() {
        String a2 = m.a();
        String str = b() + q() + "/DeviceInterrogation";
        PayPal.logd("MPL", "start makeDeviceInterrogationRequest Post");
        String a3 = a(a2, str, true);
        PayPal.logd("MPL", "end makeDeviceInterrogationRequest Post");
        PayPal instance = PayPal.getInstance();
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(a3.getBytes("UTF-8")));
            if (a(parse, a3)) {
                a("makeDeviceInterrogationRequest", str, a2, a3);
                try {
                    m.a(parse, this.h);
                } catch (Throwable th) {
                    PayPal.loge("NetworkHandler", "makeDeviceInterrogationRequest caught exception " + th.getMessage());
                }
                instance.resetAccount();
                return false;
            } else if (m.a(parse, this.h)) {
                return true;
            } else {
                instance.resetAccount();
                this.f = -1;
                PayPal.loge("NetworkHandler", "makeDeviceInterrogationRequest something failed");
                return false;
            }
        } catch (ParserConfigurationException e2) {
            a("makeDeviceInterrogationRequest", str, a2, "exception " + e2.getMessage());
            instance.resetAccount();
            return false;
        } catch (UnsupportedEncodingException e3) {
            a("makeDeviceInterrogationRequest", str, a2, "exception " + e3.getMessage());
            instance.resetAccount();
            return false;
        } catch (SAXException e4) {
            a("makeDeviceInterrogationRequest", str, a2, "exception " + e4.getMessage());
            instance.resetAccount();
            return false;
        } catch (NullPointerException e5) {
            a("makeDeviceInterrogationRequest", str, a2, "exception " + e5.getMessage());
            instance.resetAccount();
            return false;
        } catch (IOException e6) {
            a("makeDeviceInterrogationRequest", str, a2, "exception " + e6.getMessage());
            instance.resetAccount();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean C() {
        try {
            String a2 = m.a(this.h, j((String) this.h.get("NewPin")));
            String str = b() + q() + "/DeviceCreatePin";
            String a3 = a(a2, str, true);
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(a3.getBytes("UTF-8")));
            if (!a(parse, a3)) {
                m.b(parse);
                return true;
            }
            a("createPIN", str, a2, a3);
            this.f = -1;
            return false;
        } catch (f e2) {
            PayPal.loge("NetworkHandler", "createPIN caught BadXMLException " + e2.getMessage());
        } catch (n e3) {
            PayPal.loge("NetworkHandler", "createPIN caught BadPhoneNumberException " + e3.getMessage());
        } catch (ParserConfigurationException e4) {
            PayPal.loge("NetworkHandler", "Exception " + e4.getMessage());
        } catch (UnsupportedEncodingException e5) {
            PayPal.loge("NetworkHandler", "Exception " + e5.getMessage());
        } catch (SAXException e6) {
            PayPal.loge("NetworkHandler", "Exception " + e6.getMessage());
        } catch (IOException e7) {
            PayPal.loge("NetworkHandler", "Exception " + e7.getMessage());
        } catch (CertificateExpiredException e8) {
            PayPal.loge("NetworkHandler", "Exception " + e8.getMessage());
        } catch (CertificateNotYetValidException e9) {
            PayPal.loge("NetworkHandler", "Exception " + e9.getMessage());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean D() {
        String a2 = a(m.c(), b() + q() + "/RemoveDeviceAuthorization", true);
        if (k(a2)) {
            try {
                m.f(a2, this.h);
                return false;
            } catch (Throwable th) {
                PayPal.loge("NetworkHandler", "readRemoveDeviceAuthorization caught exception " + th.getMessage());
                return false;
            }
        } else if (m.f(a2, this.h)) {
            return true;
        } else {
            this.f = -1;
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x019a A[SYNTHETIC, Splitter:B:39:0x019a] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x019f A[SYNTHETIC, Splitter:B:42:0x019f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r11, java.lang.String r12, boolean r13) {
        /*
            r10 = this;
            r4 = 0
            r1 = 0
            org.apache.http.impl.client.DefaultHttpClient r0 = r10.d     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            if (r0 != 0) goto L_0x000c
            org.apache.http.impl.client.DefaultHttpClient r0 = h()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r10.d = r0     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
        L_0x000c:
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r0.<init>(r12)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r10.c = r0     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.entity.StringEntity r0 = new org.apache.http.entity.StringEntity     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r2 = "UTF-8"
            java.nio.charset.Charset r2 = java.nio.charset.Charset.forName(r2)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r2 = r2.name()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r0.<init>(r11, r2)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            if (r13 == 0) goto L_0x00c9
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "CLIENT-AUTH"
            java.lang.String r5 = "No cert"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-MESSAGE-PROTOCOL"
            java.lang.String r5 = "SOAP11"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-APPLICATION-ID"
            com.paypal.android.MEP.PayPal r5 = com.paypal.android.MEP.PayPalActivity._paypal     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r5 = r5.getAppID()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-REQUEST-SOURCE"
            java.lang.String r5 = com.paypal.android.MEP.PayPal.getVersionWithoutBuild()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-REQUEST-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-RESPONSE-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-RESPONSE-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-RESPONSE-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-RESPONSE-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "x-paypal-service-version"
            java.lang.String r5 = "1.0.0"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "x-paypal-element-ordering-preserve"
            java.lang.String r5 = "false"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setEntity(r0)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
        L_0x0092:
            java.lang.String r0 = "NetworkHandler"
            java.lang.String r2 = "postXML do execute"
            com.paypal.android.MEP.PayPal.logd(r0, r2)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.impl.client.DefaultHttpClient r0 = r10.d     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.HttpResponse r0 = r0.execute(r2)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.HttpEntity r5 = r0.getEntity()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.Header[] r3 = r0.getAllHeaders()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            if (r3 == 0) goto L_0x01ae
            int r6 = r3.length     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2 = r4
            r0 = r4
        L_0x00ae:
            if (r2 >= r6) goto L_0x01af
            r7 = r3[r2]     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r8 = r7.getName()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r9 = "Content-Length"
            int r8 = r8.compareTo(r9)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            if (r8 != 0) goto L_0x00c6
            java.lang.String r0 = r7.getValue()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
        L_0x00c6:
            int r2 = r2 + 1
            goto L_0x00ae
        L_0x00c9:
            java.lang.String r2 = p()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            boolean r2 = r12.contains(r2)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            if (r2 == 0) goto L_0x01a7
            com.paypal.android.MEP.PayPal r2 = com.paypal.android.MEP.PayPal.getInstance()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            int r2 = r2.getServer()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r3 = 3
            if (r2 != r3) goto L_0x0178
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "CLIENT-AUTH"
            java.lang.String r5 = "No cert"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
        L_0x00e7:
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-MESSAGE-PROTOCOL"
            java.lang.String r5 = "SOAP11"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-DEVICE-IPADDRESS"
            java.lang.String r5 = A()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-APPLICATION-ID"
            com.paypal.android.MEP.PayPal r5 = com.paypal.android.MEP.PayPalActivity._paypal     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r5 = r5.getAppID()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-DEVICE-AUTH-TOKEN"
            java.lang.String r5 = com.paypal.android.a.b.q     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-REQUEST-SOURCE"
            java.lang.String r5 = com.paypal.android.MEP.PayPal.getVersionWithoutBuild()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-REQUEST-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-RESPONSE-DATA-FORMAT"
            java.lang.String r5 = "XML"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "x-paypal-service-version"
            java.lang.String r5 = "1.0.0"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "x-paypal-element-ordering-preserve"
            java.lang.String r5 = "false"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            r2.setEntity(r0)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            goto L_0x0092
        L_0x0147:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x014a:
            java.lang.String r5 = "NetworkHandler"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x027e }
            r6.<init>()     // Catch:{ all -> 0x027e }
            java.lang.String r7 = "postXML caught exception doing I/O, "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x027e }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x027e }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ all -> 0x027e }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x027e }
            com.paypal.android.MEP.PayPal.loge(r5, r0)     // Catch:{ all -> 0x027e }
            if (r2 == 0) goto L_0x016b
            r2.close()     // Catch:{ Exception -> 0x0274 }
        L_0x016b:
            if (r3 == 0) goto L_0x0170
            r3.close()     // Catch:{ Exception -> 0x0234 }
        L_0x0170:
            r10.c = r1
            r10.d = r1
            r10.f = r4
            r0 = r1
        L_0x0177:
            return r0
        L_0x0178:
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-SECURITY-PASSWORD"
            java.lang.String r5 = "MPL"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-SECURITY-USERID"
            java.lang.String r5 = "MPL"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            org.apache.http.client.methods.HttpPost r2 = r10.c     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.lang.String r3 = "X-PAYPAL-SECURITY-SIGNATURE"
            java.lang.String r5 = "MPL"
            r2.setHeader(r3, r5)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            goto L_0x00e7
        L_0x0195:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x0198:
            if (r2 == 0) goto L_0x019d
            r2.close()     // Catch:{ Exception -> 0x0277 }
        L_0x019d:
            if (r3 == 0) goto L_0x01a2
            r3.close()     // Catch:{ Exception -> 0x0253 }
        L_0x01a2:
            r10.c = r1
            r10.d = r1
            throw r0
        L_0x01a7:
            java.lang.String r0 = "ErrorId=-1"
            r10.c = r1
            r10.d = r1
            goto L_0x0177
        L_0x01ae:
            r0 = r4
        L_0x01af:
            java.lang.String r2 = "NetworkHandler"
            java.lang.String r3 = "postXML setup to read reponse"
            com.paypal.android.MEP.PayPal.logd(r2, r3)     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.io.InputStream r3 = r5.getContent()     // Catch:{ Throwable -> 0x0147, all -> 0x0195 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0281, all -> 0x027a }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x0281, all -> 0x027a }
            int r6 = r3.available()     // Catch:{ Throwable -> 0x0213 }
            if (r6 <= r0) goto L_0x01c9
            int r0 = r3.available()     // Catch:{ Throwable -> 0x0213 }
        L_0x01c9:
            int r6 = r2.available()     // Catch:{ Throwable -> 0x0213 }
            if (r6 <= r0) goto L_0x01d3
            int r0 = r2.available()     // Catch:{ Throwable -> 0x0213 }
        L_0x01d3:
            java.lang.String r6 = "NetworkHandler"
            java.lang.String r7 = "postXML do read response"
            com.paypal.android.MEP.PayPal.logd(r6, r7)     // Catch:{ Throwable -> 0x0213 }
            if (r0 == 0) goto L_0x0207
            byte[] r6 = new byte[r0]     // Catch:{ Throwable -> 0x0213 }
            r2.readFully(r6)     // Catch:{ Throwable -> 0x0213 }
            java.lang.String r0 = new java.lang.String     // Catch:{ Throwable -> 0x0213 }
            java.lang.String r7 = "UTF-8"
            r0.<init>(r6, r7)     // Catch:{ Throwable -> 0x0213 }
        L_0x01e8:
            if (r5 == 0) goto L_0x01ed
            r5.consumeContent()     // Catch:{ Throwable -> 0x0213 }
        L_0x01ed:
            r2.close()     // Catch:{ Throwable -> 0x0213 }
            r3.close()     // Catch:{ Throwable -> 0x0213 }
            r5 = 0
            r10.c = r5     // Catch:{ Throwable -> 0x0213 }
            r5 = 0
            r10.d = r5     // Catch:{ Throwable -> 0x0213 }
            r2.close()     // Catch:{ Exception -> 0x0272 }
        L_0x01fc:
            if (r3 == 0) goto L_0x0201
            r3.close()     // Catch:{ Exception -> 0x0216 }
        L_0x0201:
            r10.c = r1
            r10.d = r1
            goto L_0x0177
        L_0x0207:
            java.lang.String r0 = new java.lang.String     // Catch:{ Throwable -> 0x0213 }
            byte[] r6 = a(r2)     // Catch:{ Throwable -> 0x0213 }
            java.lang.String r7 = "UTF-8"
            r0.<init>(r6, r7)     // Catch:{ Throwable -> 0x0213 }
            goto L_0x01e8
        L_0x0213:
            r0 = move-exception
            goto L_0x014a
        L_0x0216:
            r2 = move-exception
            java.lang.String r3 = "NetworkHandler"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "postXML caught exception closing streams"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r2.getMessage()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            com.paypal.android.MEP.PayPal.loge(r3, r2)
            goto L_0x0201
        L_0x0234:
            r0 = move-exception
            java.lang.String r2 = "NetworkHandler"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "postXML caught exception closing streams"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            com.paypal.android.MEP.PayPal.loge(r2, r0)
            goto L_0x0170
        L_0x0253:
            r2 = move-exception
            java.lang.String r3 = "NetworkHandler"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "postXML caught exception closing streams"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r2 = r2.getMessage()
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r2 = r2.toString()
            com.paypal.android.MEP.PayPal.loge(r3, r2)
            goto L_0x01a2
        L_0x0272:
            r2 = move-exception
            goto L_0x01fc
        L_0x0274:
            r0 = move-exception
            goto L_0x016b
        L_0x0277:
            r2 = move-exception
            goto L_0x019d
        L_0x027a:
            r0 = move-exception
            r2 = r1
            goto L_0x0198
        L_0x027e:
            r0 = move-exception
            goto L_0x0198
        L_0x0281:
            r0 = move-exception
            r2 = r1
            goto L_0x014a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String");
    }

    static /* synthetic */ void a(b bVar, int i2, com.paypal.android.c.a aVar) {
        String f2 = bVar.f();
        aVar.b(i2, f2);
        PayPal.logd("MPL", "end " + i2 + " fail, " + f2);
    }

    static /* synthetic */ void a(b bVar, String str, a.b bVar2) {
        String f2 = bVar.f();
        bVar2.d(f2);
        PayPal.logd("MPL", "end " + str + " fail, " + f2);
    }

    private static void a(String str, String str2, String str3, String str4) {
        PayPal.logd("NetworkHandler", str + " error endpoint - " + str2);
        PayPal.logd("NetworkHandler", str + " error request - " + str3);
        if (str4 != null) {
            PayPal.logd("NetworkHandler", str + " error reply - " + str4);
        }
    }

    public static final boolean a() {
        try {
            return ((String) PayPalActivity._networkHandler.h.get("payButtonEnable")).compareToIgnoreCase("true") == 0;
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    static /* synthetic */ boolean a(b bVar, String str, String str2) throws n, f, CertificateExpiredException, CertificateNotYetValidException {
        boolean z = false;
        if (!x()) {
            return false;
        }
        q = "";
        PayPal instance = PayPal.getInstance();
        StringBuilder sb = new StringBuilder();
        if (str.indexOf("@") > 0) {
            z = true;
        }
        if (z) {
            m.a(sb, "authorizationType", "Email");
            m.a(sb, ReservationSystemConstants.RECOVERY_PARAM_EMAIL, b(str));
            m.a(sb, PropertyConfiguration.PASSWORD, j(str2));
        } else {
            m.a(sb, "authorizationType", "Phone");
            sb.append("<phone>");
            m.a(sb, "countryCode", instance.getAccountCountryDialingCode());
            m.a(sb, "phoneNumber", e(str));
            sb.append("</phone>");
            m.a(sb, PropertyConfiguration.PASSWORD, j(str2));
        }
        m.a(sb, "authorizeDevice", instance.getIsRememberMe());
        String str3 = b() + r() + "/DeviceAuthenticateUser";
        String c2 = m.c(sb.toString());
        return bVar.a(str3, c2, bVar.a(c2, str3, true));
    }

    private boolean a(String str, String str2, String str3) throws f {
        PayPal.getInstance();
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str3.getBytes("UTF-8")));
            if (a(parse, str3)) {
                a("createQuickPayment", str, str2, str3);
                return false;
            }
            NodeList elementsByTagName = parse.getElementsByTagName("pinEstablished");
            if (elementsByTagName.getLength() != 1) {
                throw new f("Not exactly one pinEstablished tag");
            }
            PayPal.getInstance().setPINCreated(Boolean.parseBoolean(elementsByTagName.item(0).getChildNodes().item(0).getNodeValue()));
            NodeList elementsByTagName2 = parse.getElementsByTagName("sessionToken");
            if (elementsByTagName2.getLength() != 1) {
                throw new f("Not exactly one sessionToken tag");
            }
            q = elementsByTagName2.item(0).getChildNodes().item(0).getNodeValue();
            NodeList elementsByTagName3 = parse.getElementsByTagName("deviceAuthorized");
            if (elementsByTagName3.getLength() != 1) {
                throw new f("Not exactly one deviceAuthorized tag");
            }
            this.h.put("AuthorizedDevice", elementsByTagName3.item(0).getChildNodes().item(0).getNodeValue());
            m.a(parse);
            return q.length() > 0;
        } catch (ParserConfigurationException e2) {
            a("parseDeviceAuthenticateUser", str, str2, "Exception " + e2.getMessage());
            return false;
        } catch (UnsupportedEncodingException e3) {
            a("parseDeviceAuthenticateUser", str, str2, "Exception " + e3.getMessage());
            return false;
        } catch (SAXException e4) {
            a("parseDeviceAuthenticateUser", str, str2, "Exception " + e4.getMessage());
            return false;
        } catch (NullPointerException e5) {
            if (str3 == null) {
                a("parseDeviceAuthenticateUser", str, str2, "null response from server");
                return false;
            }
            a("parseDeviceAuthenticateUser", str, str2, "Exception " + e5.getMessage());
            return false;
        } catch (IOException e6) {
            a("parseDeviceAuthenticateUser", str, str2, "Exception " + e6.getMessage());
            return false;
        }
    }

    private boolean a(Document document, String str) {
        Intent intent;
        String str2;
        String[] a2;
        this.f = -1;
        if (str == null || str.length() <= 0) {
            this.f = 408;
            return true;
        }
        int c2 = m.c(document);
        if (str == null) {
            this.f = 408;
            return true;
        }
        if (str.contains("ErrorId=")) {
            this.f = Integer.parseInt(str.substring(str.indexOf("ErrorId=") + "ErrorId=".length()));
        } else if (c2 != 200) {
            this.f = c2;
        } else if (str.contains("<SOAP-ENV:Body") && !str.contains("</SOAP-ENV:Body")) {
            this.f = 0;
            return true;
        }
        if (this.f == -1) {
            return false;
        }
        String str3 = "" + this.f;
        String a3 = h.a("ANDROID_" + this.f);
        if (g("" + this.f) && (a2 = m.a(str)) != null) {
            String str4 = a3;
            for (int i2 = 0; i2 < a2.length; i2++) {
                str4 = str4 + " " + a2[i2] + (i2 + 1 == a2.length ? "." : ",");
            }
            a3 = str4;
        }
        if (i(str3)) {
            Intent putExtra = new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", str3);
            if (str3.equals("569060")) {
                str2 = h.a("ANDROID_no_personal_payments");
            } else if (str3.equals("500000")) {
                str2 = h.a("ANDROID_10001");
            } else if (str3.equals("580001")) {
                try {
                    if (document.getElementsByTagName("parameter").item(0).getChildNodes().item(0).getNodeValue().length() == 3) {
                        a3 = h.a("ANDROID_580001_4");
                    }
                    str2 = a3;
                } catch (Throwable th) {
                    str2 = a3;
                }
            } else {
                if (str3.equals("520009")) {
                    try {
                        if (document.getElementsByTagName("parameter").item(0).getChildNodes().item(0).getNodeValue().length() > 0) {
                            a3 = h.a("ANDROID_520009_2");
                        }
                        str2 = a3;
                    } catch (Throwable th2) {
                    }
                }
                str2 = a3;
            }
            if (!(this.e == 11 || PayPalActivity.getInstance() == null)) {
                PayPalActivity.getInstance().paymentFailed((String) PayPalActivity._networkHandler.c("CorrelationId"), (String) PayPalActivity._networkHandler.c("PayKey"), str3, str2, false, true);
            }
            intent = putExtra.putExtra("FATAL_ERROR_MESSAGE", str2);
        } else {
            intent = (!h(str3) || this.e == 11) ? (!h(str3) || this.e != 11) ? new Intent(PayPalActivity.CREATE_PAYMENT_FAIL) : new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", str3).putExtra("FATAL_ERROR_MESSAGE", h.a("ANDROID_pin_creation_timeout")).putExtra("ERROR_TIMEOUT", a3) : new Intent(PayPalActivity.LOGIN_FAIL).putExtra("FATAL_ERROR_ID", str3).putExtra("ERROR_TIMEOUT", a3);
        }
        try {
            PayPalActivity.getInstance().sendBroadcast(intent);
            return true;
        } catch (Exception e2) {
            return true;
        }
    }

    private static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i2 = 0;
        byte[] bArr = null;
        while (true) {
            try {
                int read = inputStream.read();
                if (read == -1) {
                    break;
                }
                if (bArr == null) {
                    bArr = new byte[1024];
                }
                if (i2 == 1024) {
                    byteArrayOutputStream.write(bArr);
                    try {
                        bArr = new byte[1024];
                        i2 = 0;
                    } catch (IOException e2) {
                        return null;
                    }
                }
                bArr[i2] = (byte) read;
                i2++;
            } catch (IOException e3) {
                return bArr;
            }
        }
        if (i2 != 0) {
            for (int i3 = 0; i3 < i2; i3++) {
                byteArrayOutputStream.write(bArr[i3]);
            }
        }
        byte[] bArr2 = new byte[byteArrayOutputStream.size()];
        bArr = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return bArr;
    }

    public static String b() {
        switch (PayPal.getInstance().getServer()) {
            case 0:
                return "https://mobileclient.sandbox.paypal.com/";
            case 1:
            default:
                return "https://mobileclient.paypal.com/";
            case 2:
                return "";
            case 3:
                return "https://www.stage2mb101.paypal.com:10521/";
        }
    }

    public static String b(String str) {
        return str != null ? str.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("\"", "&quot;").replace("'", "&apos;") : str;
    }

    public static void c() {
        if (l == null) {
            l = new a();
        }
        if (PayPalActivity._networkHandler == null) {
            PayPalActivity._networkHandler = new b();
        }
        if (PayPalActivity._networkHandler.h == null) {
            PayPalActivity._networkHandler.h = new Hashtable<>();
        }
        if (PayPal.getInstance().getServer() == 2) {
            PayPalActivity._networkHandler.h.put("payButtonEnable", "true");
            PayPal.getInstance().setLibraryInitialized(true);
            return;
        }
        if (!PayPalActivity._networkHandler.j.isAlive()) {
            PayPalActivity._networkHandler.j.start();
        }
        PayPalActivity._networkHandler.a(8);
    }

    public static void d() {
        if (PayPalActivity._networkHandler != null) {
            b bVar = PayPalActivity._networkHandler;
            bVar.i = true;
            while (bVar.j.isAlive()) {
                PayPal.logd("NetworkHandler", "waiting for thread to stop");
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e2) {
                    PayPal.logd("NetworkHandler", "waiting for thread to stop");
                }
            }
            PayPal.logd("NetworkHandler", "thread has stopped");
            PayPalActivity._networkHandler = null;
        }
    }

    public static b e() {
        return PayPalActivity._networkHandler;
    }

    public static String e(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        if (!str.startsWith("+")) {
            return str;
        }
        String substring = str.substring(1);
        for (String str2 : p) {
            if (substring.startsWith(str2)) {
                return substring.substring(str2.length());
            }
        }
        return substring;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    static /* synthetic */ boolean e(b bVar) throws f {
        if (!x()) {
            return false;
        }
        q = "";
        StringBuilder sb = new StringBuilder();
        m.a(sb, "authorizationType", "Device");
        m.a(sb, "authorizeDevice", "true");
        String str = b() + r() + "/DeviceAuthenticateUser";
        String c2 = m.c(sb.toString());
        return bVar.a(str, c2, bVar.a(c2, str, true));
    }

    public static String f(String str) {
        if (str != null && str.length() > 0 && str.charAt(0) == '+') {
            String substring = str.substring(1);
            for (String str2 : p) {
                if (substring.startsWith(str2)) {
                    return str2;
                }
            }
        }
        return m();
    }

    private static boolean g(String str) {
        String[] strArr = {"560027", "580022", "580023"};
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    static /* synthetic */ String h(b bVar) {
        String a2 = m.a(bVar.h);
        String str = p() + "ExecutePayment/";
        String a3 = bVar.a(a2, str, false);
        if (bVar.k(a3)) {
            a("sendPayment", str, a2, a3);
            return null;
        }
        String b2 = m.b(a3);
        return b2 == null ? "-1" : b2;
    }

    public static final DefaultHttpClient h() {
        if (PayPal.getInstance().getServer() != 3) {
            return new DefaultHttpClient();
        }
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        try {
            schemeRegistry.register(new Scheme("https", new c(), 443));
        } catch (KeyManagementException e2) {
            e2.printStackTrace();
        } catch (NoSuchAlgorithmException e3) {
            e3.printStackTrace();
        } catch (KeyStoreException e4) {
            e4.printStackTrace();
        } catch (UnrecoverableKeyException e5) {
            e5.printStackTrace();
        }
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        basicHttpParams.setParameter(ConnManagerPNames.MAX_TOTAL_CONNECTIONS, 30);
        basicHttpParams.setParameter(ConnManagerPNames.MAX_CONNECTIONS_PER_ROUTE, new ConnPerRouteBean(30));
        basicHttpParams.setParameter("http.protocol.expect-continue", false);
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        return new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    private static boolean h(String str) {
        String[] strArr = {"10818", "10897", "10898", "10899", "520003"};
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private static boolean i(String str) {
        String[] strArr = {"10001", "10004", "10800", "10801", "10802", "10804", "10805", "10806", "10808", "10809", "10810", "10811", "10812", "10813", "10815", "10819", "10820", "10821", "10822", "10823", "10824", "10825", "10849", "10850", "10858", "10859", "10860", "10861", "10862", "10863", "10864", "10867", "99999", "520002", "520009", "539041", "540031", "550001", "550006", "559044", "560027", "569000", "569042", "569056", "569060", "579007", "579017", "579033", "579040", "579045", "579047", "579048", "580001", "580022", "580023", "580028", "580030", "580031", "580032", "580033", "580034", "589009", "589019", "500000"};
        for (String equals : strArr) {
            if (equals.equals(str)) {
                return true;
            }
        }
        return false;
    }

    private static String j(String str) throws CertificateExpiredException, CertificateNotYetValidException {
        ByteArrayInputStream byteArrayInputStream;
        try {
            switch (PayPal.getInstance().getServer()) {
                case 0:
                    byteArrayInputStream = new ByteArrayInputStream(o);
                    break;
                case 1:
                case 2:
                default:
                    byteArrayInputStream = new ByteArrayInputStream(n);
                    break;
                case 3:
                    byteArrayInputStream = new ByteArrayInputStream(m);
                    break;
            }
            X509Certificate x509Certificate = (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(byteArrayInputStream);
            byteArrayInputStream.close();
            x509Certificate.checkValidity(new Date());
            Cipher instance = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            instance.init(1, (RSAPublicKey) x509Certificate.getPublicKey());
            return b(a.a(instance.doFinal(str.getBytes()), 8));
        } catch (FileNotFoundException e2) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e2.getMessage());
            return "";
        } catch (CertificateException e3) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e3.getMessage());
            return "";
        } catch (IOException e4) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e4.getMessage());
            return "";
        } catch (NoSuchAlgorithmException e5) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e5.getMessage());
            return "";
        } catch (NoSuchPaddingException e6) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e6.getMessage());
            return "";
        } catch (InvalidKeyException e7) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e7.getMessage());
            return "";
        } catch (IllegalBlockSizeException e8) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e8.getMessage());
            return "";
        } catch (BadPaddingException e9) {
            PayPal.loge("NetworkHandler", "encryptPassword faulted " + e9.getMessage());
            return "";
        }
    }

    private boolean k(String str) {
        Object e2 = null;
        try {
            if (!a(DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))), str)) {
                return false;
            }
        } catch (IOException | ParserConfigurationException | SAXException e3) {
            e2 = e3;
        }
        if (e2 == null) {
            PayPal.loge("NetworkHandler", "Response contains an error, " + str);
        } else {
            PayPal.loge("NetworkHandler", "Exception checking for error in response, " + str);
        }
        return true;
    }

    public static String m() {
        String upperCase = Locale.getDefault().getCountry().toUpperCase();
        return (upperCase.compareTo("US") == 0 || upperCase.compareTo("CA") == 0) ? "1" : upperCase.compareTo("GB") == 0 ? "44" : upperCase.compareTo("AU") == 0 ? "61" : upperCase.compareTo("FR") == 0 ? "33" : upperCase.compareTo("ES") == 0 ? "34" : upperCase.compareTo("IT") == 0 ? "39" : "1";
    }

    public static String n() {
        return q;
    }

    private static String p() {
        switch (PayPal.getInstance().getServer()) {
            case 0:
                return "https://svcs.sandbox.paypal.com/AdaptivePayments/";
            case 1:
            default:
                return "https://svcs.paypal.com/AdaptivePayments/";
            case 2:
                return "";
            case 3:
                return "https://www.stage2mb101.paypal.com:10279/AdaptivePayments/";
        }
    }

    private static String q() {
        switch (PayPal.getInstance().getServer()) {
            case 0:
            case 3:
                return "GMAdapter";
            case 1:
            default:
                return "GMAdapter";
            case 2:
                return "";
        }
    }

    private static String r() {
        switch (PayPal.getInstance().getServer()) {
            case 0:
            case 3:
                return "GMAdapter";
            case 1:
            default:
                return "GMAdapter";
            case 2:
                return "";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean s() {
        try {
            String g2 = m.g(this.h);
            String str = p() + "SetPaymentOptions/";
            String a2 = a(g2, str, false);
            if (k(a2)) {
                a("setPaymentOptions", str, g2, a2);
                try {
                    m.a(a2, this.h);
                    return false;
                } catch (Throwable th) {
                    return false;
                }
            } else {
                m.a(a2, this.h);
                return true;
            }
        } catch (n e2) {
            PayPal.loge("NetworkHandler", "Exception " + e2.getMessage());
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean t() {
        String h2 = m.h(this.h);
        String str = p() + "GetAvailableShippingAddresses/";
        String a2 = a(h2, str, false);
        if (k(a2)) {
            a("getAddresses", str, h2, a2);
            try {
                m.g(a2, this.h);
                return false;
            } catch (Throwable th) {
                return false;
            }
        } else {
            m.g(a2, this.h);
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean u() {
        String b2 = m.b(this.h);
        String str = p() + "Preapproval/";
        String a2 = a(b2, str, false);
        if (k(a2)) {
            a("createPreapprovalRequest", str, b2, a2);
            try {
                m.b(a2, this.h);
                return false;
            } catch (Throwable th) {
                return false;
            }
        } else {
            m.b(a2, this.h);
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean v() {
        String a2 = a(m.d(this.h), p() + "PreapprovalDetails/", false);
        if (k(a2)) {
            try {
                m.c(a2, this.h);
                return false;
            } catch (Throwable th) {
                return false;
            }
        } else {
            m.c(a2, this.h);
            if (!this.h.get("Approved").equals("true")) {
                return true;
            }
            Intent putExtra = new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", "-1").putExtra("FATAL_ERROR_MESSAGE", h.a("ANDROID_preapproval_already_approved"));
            PayPalActivity.getInstance().paymentFailed((String) PayPalActivity._networkHandler.c("CorrelationId"), (String) PayPalActivity._networkHandler.c("PayKey"), "-1", h.a("ANDROID_preapproval_already_approved"), false, true);
            PayPalActivity.getInstance().sendBroadcast(putExtra);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public boolean w() {
        String c2 = m.c(this.h);
        String str = p() + "ConfirmPreapproval/";
        String a2 = a(c2, str, false);
        if (k(a2)) {
            a("confirmPreapprovalRequest", str, c2, a2);
            try {
                m.d(a2, this.h);
                return false;
            } catch (Throwable th) {
                return false;
            }
        } else {
            m.d(a2, this.h);
            return true;
        }
    }

    private static boolean x() {
        String deviceId = ((TelephonyManager) PayPal.getInstance().getParentContext().getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            deviceId = ((WifiManager) PayPal.getInstance().getParentContext().getSystemService("wifi")).getConnectionInfo().getMacAddress();
        }
        if (PayPal.getInstance().getServer() != 1 || !deviceId.equals("000000000000000")) {
            return true;
        }
        Intent putExtra = new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", "-1").putExtra("FATAL_ERROR_MESSAGE", h.a("ANDROID_simulator_payment_block"));
        PayPalActivity.getInstance().paymentFailed((String) PayPalActivity._networkHandler.c("CorrelationId"), (String) PayPalActivity._networkHandler.c("PayKey"), "-1", h.a("ANDROID_simulator_payment_block"), false, true);
        PayPalActivity.getInstance().sendBroadcast(putExtra);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public Hashtable<String, Object> y() {
        try {
            String f2 = m.f(this.h);
            if (f2 == null) {
                return null;
            }
            String str = p() + "Pay/";
            String a2 = a(f2, str, false);
            if (k(a2)) {
                a("createPayment", str, f2, a2);
                if (!a2.contains("<errorId>580022</errorId>")) {
                    return null;
                }
                String a3 = h.a("ANDROID_580022");
                Intent putExtra = new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", "580022").putExtra("FATAL_ERROR_MESSAGE", a3);
                PayPalActivity.getInstance().paymentFailed((String) PayPalActivity._networkHandler.c("CorrelationId"), (String) PayPalActivity._networkHandler.c("PayKey"), "580022", a3, false, true);
                try {
                    PayPalActivity.getInstance().sendBroadcast(putExtra);
                } catch (Exception e2) {
                }
                return null;
            } else if (!m.a(a2, this.h)) {
                this.f = -1;
                return null;
            } else if (a2.contains("defaultFundingPlan") || !((String) this.h.get("ActionType")).equals("CREATE")) {
                return this.h;
            } else {
                Intent putExtra2 = new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", "589009").putExtra("FATAL_ERROR_MESSAGE", h.a("ANDROID_589009"));
                PayPalActivity.getInstance().paymentFailed((String) PayPalActivity._networkHandler.c("CorrelationId"), (String) PayPalActivity._networkHandler.c("PayKey"), "589009", h.a("ANDROID_589009"), false, true);
                try {
                    PayPalActivity.getInstance().sendBroadcast(putExtra2);
                } catch (Exception e3) {
                }
                return null;
            }
        } catch (n e4) {
            PayPal.loge("NetworkHandler", "Exception " + e4.getMessage());
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      com.paypal.android.a.b.a(com.paypal.android.a.b, int, com.paypal.android.c.a):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, com.paypal.android.MEP.a$b):void
      com.paypal.android.a.b.a(com.paypal.android.a.b, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.paypal.android.a.b.a(java.lang.String, java.lang.String, boolean):java.lang.String */
    /* access modifiers changed from: private */
    public Hashtable<String, Object> z() {
        String e2 = m.e(this.h);
        String str = p() + "GetFundingPlans/";
        String a2 = a(e2, str, false);
        if (k(a2)) {
            a("createFundingRequest", str, e2, a2);
            try {
                m.e(a2, this.h);
            } catch (Throwable th) {
            }
            return null;
        } else if (!m.e(a2, this.h)) {
            this.f = -1;
            return null;
        } else if (((Vector) this.h.get("FundingPlans")).size() != 0) {
            return this.h;
        } else {
            try {
                PayPalActivity.getInstance().sendBroadcast(new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", "589009").putExtra("FATAL_ERROR_MESSAGE", h.a("ANDROID_589009")));
            } catch (Exception e3) {
            }
            return null;
        }
    }

    public final void a(int i2) {
        this.g = i2;
        this.e = i2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(int r10, java.lang.Object r11) {
        /*
            r9 = this;
            r8 = 4
            r7 = 7
            r6 = 1
            r5 = 0
            com.paypal.android.MEP.PayPal r4 = com.paypal.android.MEP.PayPal.getInstance()
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.getInstance()
            com.paypal.android.a.b r3 = com.paypal.android.MEP.PayPalActivity._networkHandler
            switch(r10) {
                case 3: goto L_0x0012;
                case 4: goto L_0x003d;
                case 5: goto L_0x0073;
                case 6: goto L_0x0011;
                case 7: goto L_0x008f;
                case 8: goto L_0x0011;
                case 9: goto L_0x006b;
                case 10: goto L_0x0011;
                case 11: goto L_0x0011;
                case 12: goto L_0x0011;
                case 13: goto L_0x0112;
                default: goto L_0x0011;
            }
        L_0x0011:
            return
        L_0x0012:
            java.util.Hashtable<java.lang.String, java.lang.Object> r1 = r9.h
            java.lang.String r2 = "quickPay"
            java.lang.Object r1 = r1.get(r2)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "true"
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0028
            r9.a(r8)
            goto L_0x0011
        L_0x0028:
            boolean r1 = r4.getShippingEnabled()
            if (r1 == 0) goto L_0x0032
            r9.a(r7)
            goto L_0x0011
        L_0x0032:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.CREATE_PAYMENT_SUCCESS
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0011
        L_0x003d:
            java.lang.String r1 = "PayKey"
            java.lang.Object r1 = r3.c(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "PaymentExecStatus"
            java.lang.Object r2 = r3.c(r2)
            java.lang.String r2 = (java.lang.String) r2
            boolean r3 = r4.isHeavyCountry()
            if (r3 == 0) goto L_0x0067
            boolean r3 = r4.hasCreatedPIN()
            if (r3 != 0) goto L_0x0067
            r0.setTransactionSuccessful(r6)
            r0.paymentSucceeded(r1, r2, r5)
            java.lang.String r11 = (java.lang.String) r11
            com.paypal.android.MEP.a.e.a = r11
            com.paypal.android.MEP.a.d.AnonymousClass1.b(r7)
            goto L_0x0011
        L_0x0067:
            r0.paymentSucceeded(r1, r2, r6)
            goto L_0x0011
        L_0x006b:
            java.lang.String r11 = (java.lang.String) r11
            com.paypal.android.MEP.a.h.a = r11
            com.paypal.android.MEP.a.d.AnonymousClass1.a(r8)
            goto L_0x0011
        L_0x0073:
            java.lang.String r1 = "FundingPlanId"
            java.lang.String r2 = "0"
            r9.a(r1, r2)
            boolean r1 = r4.getShippingEnabled()
            if (r1 == 0) goto L_0x0084
            r9.a(r7)
            goto L_0x0011
        L_0x0084:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.CREATE_PAYMENT_SUCCESS
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0011
        L_0x008f:
            int r1 = r4.getServer()
            r2 = 2
            if (r1 != r2) goto L_0x010b
            java.util.Hashtable<java.lang.String, java.lang.Object> r1 = com.paypal.android.MEP.a.a.a
        L_0x0098:
            java.lang.String r2 = "AvailableAddresses"
            java.lang.Object r1 = r1.get(r2)
            java.util.Vector r1 = (java.util.Vector) r1
            if (r1 == 0) goto L_0x011e
            int r2 = r1.size()
            if (r2 <= 0) goto L_0x011e
            java.lang.Object r1 = r1.get(r5)
            com.paypal.android.a.a.h r1 = (com.paypal.android.a.a.h) r1
            java.lang.String r2 = "ShippingAddressId"
            java.lang.Object r2 = r9.c(r2)
            java.lang.String r2 = (java.lang.String) r2
            if (r2 != 0) goto L_0x011e
            java.lang.String r2 = "ShippingAddressId"
            java.lang.String r1 = r1.h()
            r9.a(r2, r1)
            int r1 = r9.k()
        L_0x00c5:
            if (r1 != 0) goto L_0x00d1
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r7 = com.paypal.android.MEP.PayPalActivity.CREATE_PAYMENT_SUCCESS
            r2.<init>(r7)
            r0.sendBroadcast(r2)
        L_0x00d1:
            r2 = -1
            if (r1 != r2) goto L_0x0011
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.FATAL_ERROR
            r1.<init>(r2)
            java.lang.String r2 = "FATAL_ERROR_ID"
            java.lang.String r7 = "-1"
            android.content.Intent r1 = r1.putExtra(r2, r7)
            java.lang.String r2 = "FATAL_ERROR_MESSAGE"
            java.lang.String r7 = r4.getAdjustPaymentError()
            android.content.Intent r7 = r1.putExtra(r2, r7)
            java.lang.String r1 = "CorrelationId"
            java.lang.Object r1 = r3.c(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r2 = "PayKey"
            java.lang.Object r2 = r3.c(r2)
            java.lang.String r2 = (java.lang.String) r2
            java.lang.String r3 = "-1"
            java.lang.String r4 = r4.getAdjustPaymentError()
            r0.paymentFailed(r1, r2, r3, r4, r5, r6)
            r0.sendBroadcast(r7)
            goto L_0x0011
        L_0x010b:
            java.util.Hashtable r11 = (java.util.Hashtable) r11
            com.paypal.android.MEP.a.a.a = r11
            java.util.Hashtable<java.lang.String, java.lang.Object> r1 = r3.h
            goto L_0x0098
        L_0x0112:
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = com.paypal.android.MEP.PayPalActivity.CREATE_PAYMENT_SUCCESS
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0011
        L_0x011e:
            r1 = r5
            goto L_0x00c5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.b.a(int, java.lang.Object):void");
    }

    public final void a(String str) {
        PayPal instance = PayPal.getInstance();
        if (instance.getServer() == 1) {
            PayPal.logd("MPL Tracking", "Post: " + str);
            String str2 = "Device";
            if (instance.getAuthMethod() == 0 || instance.getAuthMethod() == 3) {
                str2 = "Password";
            } else if (instance.getAuthMethod() == 1) {
                str2 = "PIN";
            }
            String str3 = "Simple";
            if (instance.getPayType() == 3) {
                str3 = "Preapproval";
            } else if (instance.getPayType() == 2) {
                str3 = "Chained";
            } else if (instance.getPayType() == 1) {
                str3 = "Parallel";
            }
            StringBuilder sb = new StringBuilder("https://sstats.paypal-metrics.com/b/ss/paypalwireless/5/H.5--WAP/12345?pageName=android/");
            sb.append("mpl-").append(str);
            sb.append("&c1=").append(Locale.getDefault().getCountry().toUpperCase());
            sb.append("&c4=ver").append(PayPal.getVersion());
            sb.append("&c5=").append("Android");
            sb.append("&c6=").append(instance.getParentContext().getPackageName());
            sb.append("&c7=").append(str2);
            sb.append("&c9=").append(str3);
            sb.append("&c10=").append(instance.getShippingEnabled() ? "Enabled" : "Disabled");
            String sb2 = sb.toString();
            PayPal.logd("NetworkHandler", "queueTrackingPost (), queue tracking " + sb2);
            synchronized (k) {
                k.add(sb2);
                k.notifyAll();
            }
        }
    }

    public final void a(String str, Object obj) {
        if (obj != null) {
            this.h.put(str, obj);
        } else {
            this.h.remove(str);
        }
    }

    public final Object c(String str) {
        return this.h.get(str);
    }

    public final void d(String str) {
        Intent putExtra = new Intent(PayPalActivity.FATAL_ERROR).putExtra("FATAL_ERROR_ID", "-1").putExtra("FATAL_ERROR_MESSAGE", str);
        PayPalActivity.getInstance().paymentFailed((String) PayPalActivity._networkHandler.c("CorrelationId"), (String) PayPalActivity._networkHandler.c("PayKey"), "-1", str, false, true);
        PayPalActivity.getInstance().sendBroadcast(putExtra);
    }

    public final String f() {
        if (this.f == 0) {
            this.f = 408;
        }
        return h.a("ANDROID_" + this.f);
    }

    public final Hashtable<String, Object> g() {
        return this.h;
    }

    public final void i() {
        String str;
        PayPal instance = PayPal.getInstance();
        PayPalAdvancedPayment payment = instance.getPayment();
        this.h.remove("ShippingAddressId");
        a("PaymentCurrencyID", payment.getCurrencyType());
        a("CancelUrl", instance.getCancelUrl());
        a("ReturnUrl", instance.getReturnUrl());
        switch (instance.getFeesPayer()) {
            case 1:
                str = "SENDER";
                break;
            case 2:
                str = "PRIMARYRECEIVER";
                break;
            case 3:
                str = "SECONDARYONLY";
                break;
            default:
                str = "EACHRECEIVER";
                break;
        }
        a("FeesPayer", str);
        a("ActionType", "CREATE");
        a("Receivers", payment.getReceivers());
        if (payment.getIpnUrl() != null && !payment.getIpnUrl().equals("")) {
            a("IpnNotificationUrl", payment.getIpnUrl());
        }
        if (payment.getMemo() != null && !payment.getMemo().equals("")) {
            a("Memo", payment.getMemo());
        }
        a("delegate", this);
        PayPalActivity._networkHandler.a(3);
    }

    public final void j() {
        a("PreapprovalKey", PayPal.getInstance().getPreapprovalKey());
        a("delegate", this);
        PayPalActivity._networkHandler.a(13);
    }

    public final int k() {
        h hVar;
        PayPal instance = PayPal.getInstance();
        if (instance.getDynamicAmountCalculationEnabled() && instance.getShippingEnabled()) {
            String str = (String) this.h.get("ShippingAddressId");
            Vector vector = (Vector) (PayPal.getInstance().getServer() == 2 ? com.paypal.android.MEP.a.a.a : this.h).get("AvailableAddresses");
            int i2 = 0;
            while (true) {
                if (i2 >= vector.size()) {
                    hVar = null;
                    break;
                }
                hVar = (h) vector.elementAt(i2);
                if (hVar.h().equals(str)) {
                    break;
                }
                i2++;
            }
            MEPAddress mEPAddress = new MEPAddress();
            String d2 = hVar.d();
            String e2 = hVar.e();
            String b2 = hVar.b();
            String g2 = hVar.g();
            String f2 = hVar.f();
            String c2 = hVar.c();
            mEPAddress.setStreet1(d2);
            mEPAddress.setStreet2(e2);
            mEPAddress.setCity(b2);
            mEPAddress.setState(g2);
            mEPAddress.setPostalcode(f2);
            mEPAddress.setCountrycode(c2);
            PayPalAdvancedPayment payment = PayPal.getInstance().getPayment();
            ArrayList<PayPalReceiverDetails> receivers = payment.getReceivers();
            Vector vector2 = new Vector();
            for (int i3 = 0; i3 < payment.getReceivers().size(); i3++) {
                PayPalReceiverDetails payPalReceiverDetails = payment.getReceivers().get(i3);
                MEPReceiverAmounts mEPReceiverAmounts = new MEPReceiverAmounts();
                mEPReceiverAmounts.amounts = new MEPAmounts();
                mEPReceiverAmounts.receiver = payPalReceiverDetails.getRecipient();
                if (payPalReceiverDetails.getInvoiceData().getTax() != null) {
                    mEPReceiverAmounts.amounts.setTax(payPalReceiverDetails.getInvoiceData().getTax());
                }
                if (payPalReceiverDetails.getInvoiceData().getShipping() != null) {
                    mEPReceiverAmounts.amounts.setShipping(payPalReceiverDetails.getInvoiceData().getShipping());
                }
                if (payment.getCurrencyType() != null) {
                    mEPReceiverAmounts.amounts.setCurrency(payment.getCurrencyType());
                }
                if (payPalReceiverDetails.getSubtotal() != null) {
                    mEPReceiverAmounts.amounts.setPaymentAmount(payPalReceiverDetails.getSubtotal());
                }
                vector2.add(mEPReceiverAmounts);
            }
            Vector<MEPReceiverAmounts> adjustAmountsAdvanced = PayPalActivity.getInstance().adjustAmountsAdvanced(mEPAddress, payment.getCurrencyType(), vector2);
            if (adjustAmountsAdvanced == null) {
                return -1;
            }
            if (adjustAmountsAdvanced.size() == receivers.size()) {
                int i4 = 0;
                boolean z = false;
                while (i4 < adjustAmountsAdvanced.size()) {
                    PayPalReceiverDetails payPalReceiverDetails2 = receivers.get(i4);
                    MEPReceiverAmounts elementAt = adjustAmountsAdvanced.elementAt(i4);
                    BigDecimal tax = payPalReceiverDetails2.getInvoiceData().getTax() != null ? payPalReceiverDetails2.getInvoiceData().getTax() : new BigDecimal("0.0");
                    BigDecimal shipping = payPalReceiverDetails2.getInvoiceData().getShipping() != null ? payPalReceiverDetails2.getInvoiceData().getShipping() : new BigDecimal("0.0");
                    BigDecimal subtotal = payPalReceiverDetails2.getSubtotal() != null ? payPalReceiverDetails2.getSubtotal() : new BigDecimal("0.0");
                    String recipient = payPalReceiverDetails2.getRecipient();
                    BigDecimal tax2 = elementAt.amounts.getTax();
                    BigDecimal shipping2 = elementAt.amounts.getShipping();
                    BigDecimal paymentAmount = elementAt.amounts.getPaymentAmount();
                    String str2 = elementAt.receiver;
                    if (paymentAmount != subtotal || !recipient.equals(str2)) {
                        throw new IllegalArgumentException();
                    }
                    i4++;
                    z = (tax2 == tax && shipping2 == shipping) ? z : true;
                }
                if (z) {
                    for (int i5 = 0; i5 < adjustAmountsAdvanced.size(); i5++) {
                        PayPalReceiverDetails payPalReceiverDetails3 = receivers.get(i5);
                        MEPReceiverAmounts elementAt2 = adjustAmountsAdvanced.elementAt(i5);
                        payPalReceiverDetails3.getInvoiceData().setShipping(elementAt2.amounts.getShipping());
                        payPalReceiverDetails3.getInvoiceData().setTax(elementAt2.amounts.getTax());
                    }
                    a("PaymentCurrencyID", payment.getCurrencyType());
                    a("Receivers", payment.getReceivers());
                    if (payment.getIpnUrl() != null && !payment.getIpnUrl().equals("")) {
                        a("IpnNotificationUrl", payment.getIpnUrl());
                    }
                    if (payment.getMemo() != null && !payment.getMemo().equals("")) {
                        a("Memo", payment.getMemo());
                    }
                    a("ActionType", "CREATE");
                    a("delegate", this);
                    a(3);
                    return 1;
                }
            } else {
                throw new IllegalArgumentException();
            }
        }
        return 0;
    }

    public final void l() {
    }
}
