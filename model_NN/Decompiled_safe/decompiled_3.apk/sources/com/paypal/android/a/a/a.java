package com.paypal.android.a.a;

import com.biznessapps.constants.ReservationSystemConstants;
import com.paypal.android.a.m;
import java.math.BigDecimal;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class a {
    private BigDecimal a;
    private String b;

    public static a a(Element element) {
        if (element == null) {
            return null;
        }
        a aVar = new a();
        NodeList elementsByTagName = element.getElementsByTagName("code");
        if (elementsByTagName.getLength() != 1) {
            return null;
        }
        aVar.b = m.a(((Element) elementsByTagName.item(0)).getChildNodes());
        NodeList elementsByTagName2 = element.getElementsByTagName(ReservationSystemConstants.AMOUNT_EXTRA);
        if (elementsByTagName2.getLength() != 1) {
            return null;
        }
        aVar.a(m.a(((Element) elementsByTagName2.item(0)).getChildNodes()));
        return aVar;
    }

    public final BigDecimal a() {
        return this.a;
    }

    public final void a(String str) {
        try {
            this.a = new BigDecimal(str);
        } catch (NumberFormatException e) {
            this.a = new BigDecimal("0.0");
        }
    }

    public final String b() {
        return this.b;
    }

    public final void b(String str) {
        this.b = str;
    }
}
