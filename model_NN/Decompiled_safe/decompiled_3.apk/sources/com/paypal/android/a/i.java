package com.paypal.android.a;

import com.paypal.android.MEP.PayPal;
import com.paypal.android.c.a;
import junit.framework.Assert;

class i implements a {
    i() {
    }

    public final void a(int i, Object obj) {
        Assert.assertTrue(i == 8);
        PayPal.getInstance().setLibraryInitialized(true);
        PayPal.getInstance().onInitializeCompletedOK(i, obj);
    }

    public final void b(int i, Object obj) {
        Assert.assertTrue(i == 8);
        PayPal.getInstance().setLibraryInitialized(false);
        PayPal.getInstance().onInitializeCompletedError(i, obj);
    }
}
