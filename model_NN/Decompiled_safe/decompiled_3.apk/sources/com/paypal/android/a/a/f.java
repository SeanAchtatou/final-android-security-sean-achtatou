package com.paypal.android.a.a;

import com.paypal.android.a.m;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class f {
    public a a;
    public a b;
    private String c;

    public static f a(Element element) {
        if (element == null) {
            return null;
        }
        f fVar = new f();
        NodeList elementsByTagName = element.getElementsByTagName("from");
        if (elementsByTagName.getLength() == 1) {
            fVar.a = a.a((Element) elementsByTagName.item(0));
        }
        NodeList elementsByTagName2 = element.getElementsByTagName("to");
        if (elementsByTagName2.getLength() == 1) {
            fVar.b = a.a((Element) elementsByTagName2.item(0));
        }
        NodeList elementsByTagName3 = element.getElementsByTagName("exchangeRate");
        if (elementsByTagName3.getLength() != 1) {
            return null;
        }
        fVar.c = m.a(((Element) elementsByTagName3.item(0)).getChildNodes());
        return fVar;
    }

    public final String a() {
        return this.c;
    }
}
