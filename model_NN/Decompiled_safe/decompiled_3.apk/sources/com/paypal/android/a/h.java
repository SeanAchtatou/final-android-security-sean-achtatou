package com.paypal.android.a;

import android.util.Log;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ReservationSystemConstants;
import com.paypal.android.MEP.PayPal;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Hashtable;
import java.util.Locale;

public final class h {
    private static String[] a = {"de_at", "de_ch", "de_de", "en_ar", "en_at", "en_au", "en_be", "en_br", "en_ca", "en_ch", "en_de", "en_es", "en_fr", "en_gb", "en_hk", "en_in", "en_it", "en_jp", "en_mx", "en_nl", "en_pl", "en_sg", "en_tw", "en_us", "es_ar", "es_es", "es_mx", "fr_be", "fr_ca", "fr_ch", "fr_fr", "it_it", "ja_jp", "nl_be", "nl_nl", "pl_pl", "pt_br", "zh_hk", "zh_tw"};
    private static Hashtable<String, String> b;
    private static NumberFormat c;
    private static Object d = new Object();

    public static String a(String str) {
        while (b == null) {
            try {
                Log.d("StringUtil", "wait for _strings to be allocated");
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }
        }
        synchronized (d) {
            String str2 = b.get(str);
            if (str2 != null) {
                str = str2;
            }
        }
        return str;
    }

    public static String a(String str, int i) {
        String[] split = str.substring(0, str.indexOf(NDEFRecord.TEXT_WELL_KNOWN_TYPE)).split("-");
        if (split.length < 3) {
            return "Error with timestamp.";
        }
        int parseInt = Integer.parseInt(split[0]);
        int parseInt2 = Integer.parseInt(split[1]);
        int parseInt3 = Integer.parseInt(split[2]);
        String language = PayPal.getInstance().getLanguage();
        if (language.contains("AU") || language.contains("DE") || language.contains("BR") || language.contains("FR") || language.contains("IN") || language.contains("NL") || language.contains("PL") || language.contains("ES") || language.contains("GB") || language.contains("AT") || language.contains("CH") || language.contains("MX") || language.contains("SG") || language.contains("IT") || language.contains("AR")) {
            i = 0;
        } else if (language.contains("US")) {
            i = 2;
        } else if (language.contains("BE") || language.contains("CA") || language.contains("JP") || language.contains("HK") || language.contains("TW")) {
            i = 5;
        }
        switch (i) {
            case 0:
                return parseInt3 + "/" + parseInt2 + "/" + parseInt;
            case 5:
                return parseInt + "/" + parseInt2 + "/" + parseInt3;
            default:
                return parseInt2 + "/" + parseInt3 + "/" + parseInt;
        }
    }

    public static String a(BigDecimal bigDecimal, String str) {
        c.setCurrency(Currency.getInstance(str));
        return c.format(bigDecimal) + " (" + str + ")";
    }

    public static void a() {
        b = null;
        c = null;
    }

    public static boolean b(String str) {
        String lowerCase = str.toLowerCase();
        if (lowerCase.indexOf(95) == -1 || lowerCase.length() != 5 || !g(lowerCase.substring(0, 1)) || !g(lowerCase.substring(3, 5))) {
            return false;
        }
        for (String equals : a) {
            if (equals.equals(lowerCase)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static void c(String str) {
        int i;
        int i2 = 17877;
        synchronized (d) {
            if (str == null) {
                str = ReservationSystemConstants.DEFAULT_LANGUAGE;
            }
            String lowerCase = str.toLowerCase();
            String str2 = !b(lowerCase) ? ReservationSystemConstants.DEFAULT_LANGUAGE : lowerCase;
            if (b != null) {
                b = null;
            }
            c = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.getDefault());
            if (str2.equals("de_at")) {
                i2 = 407851;
                i = 19844;
            } else if (str2.equals("de_ch")) {
                i = 19922;
            } else if (str2.equals("de_de")) {
                i2 = 367816;
                i = 19844;
            } else if (str2.equals("en_ar")) {
                i2 = 57219;
                i = 19391;
            } else if (str2.equals("en_at")) {
                i2 = 175128;
                i = 20185;
            } else if (str2.equals("en_au")) {
                i2 = 563442;
                i = 18501;
            } else if (str2.equals("en_be")) {
                i2 = 742893;
                i = 19328;
            } else if (str2.equals("en_br")) {
                i2 = 135958;
                i = 19762;
            } else if (str2.equals("en_ca")) {
                i2 = 683782;
                i = 21569;
            } else if (str2.equals("en_ch")) {
                i2 = 523349;
                i = 20439;
            } else if (str2.equals("en_de")) {
                i2 = 95688;
                i = 20185;
            } else if (str2.equals("en_es")) {
                i2 = 705351;
                i = 19362;
            } else if (str2.equals("en_fr")) {
                i2 = 309212;
                i = 20370;
            } else if (str2.equals("en_gb")) {
                i2 = 487773;
                i = 18217;
            } else if (str2.equals("en_hk")) {
                i2 = 505990;
                i = 17359;
            } else if (str2.equals("en_in")) {
                i2 = 329582;
                i = 18162;
            } else if (str2.equals("en_it")) {
                i2 = 543788;
                i = 19654;
            } else if (str2.equals("en_jp")) {
                i2 = 427695;
                i = 23230;
            } else if (str2.equals("en_mx")) {
                i2 = 155720;
                i = 19408;
            } else if (str2.equals("en_nl")) {
                i2 = 468459;
                i = 19314;
            } else if (str2.equals("en_pl")) {
                i2 = 581943;
                i = 20507;
            } else if (str2.equals("en_sg")) {
                i2 = 195313;
                i = 18176;
            } else if (str2.equals("en_tw")) {
                i = 17877;
                i2 = 0;
            } else if (str2.equals("en_us")) {
                i2 = 724713;
                i = 18180;
            } else if (str2.equals("es_ar")) {
                i2 = 251927;
                i = 19188;
            } else if (str2.equals("es_es")) {
                i2 = 76610;
                i = 19078;
            } else if (str2.equals("es_mx")) {
                i2 = 271115;
                i = 19157;
            } else if (str2.equals("fr_be")) {
                i2 = 347744;
                i = 20072;
            } else if (str2.equals("fr_ca")) {
                i2 = 230568;
                i = 21359;
            } else if (str2.equals("fr_ch")) {
                i2 = 115873;
                i = 20085;
            } else if (str2.equals("fr_fr")) {
                i2 = 663766;
                i = 20016;
            } else if (str2.equals("it_it")) {
                i2 = 37799;
                i = 19420;
            } else if (str2.equals("ja_jp")) {
                i2 = 640855;
                i = 22911;
            } else if (str2.equals("nl_be")) {
                i2 = 602450;
                i = 18954;
            } else if (str2.equals("nl_nl")) {
                i2 = 290272;
                i = 18940;
            } else if (str2.equals("pl_pl")) {
                i2 = 387660;
                i = 20191;
            } else if (str2.equals("pt_br")) {
                i2 = 621404;
                i = 19451;
            } else if (str2.equals("zh_hk")) {
                i2 = 213489;
                i = 17079;
            } else if (str2.equals("zh_tw")) {
                i2 = 450925;
                i = 17534;
            } else {
                i = 0;
                i2 = 0;
            }
            String[] split = new String(g.a(i2, i, g.a("com/paypal/android/utils/data/locale.bin"))).split(AppConstants.NEW_LINE);
            b = new Hashtable<>();
            for (String str3 : split) {
                if (str3.contains("\" = \"")) {
                    String[] split2 = str3.split("\" = \"");
                    b.put(split2[0].replace('\"', ' ').trim(), split2[1].replace("\";", " ").trim());
                }
            }
        }
    }

    public static boolean d(String str) {
        int indexOf = str.indexOf(64);
        int lastIndexOf = str.lastIndexOf(46);
        return (indexOf == -1 || lastIndexOf == -1 || lastIndexOf + -1 <= indexOf || lastIndexOf == str.length() + -1) ? false : true;
    }

    public static boolean e(String str) {
        if (str.length() <= 0) {
            return false;
        }
        StringBuffer stringBuffer = new StringBuffer(str.length());
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if ((charAt >= '0' && charAt <= '9') || charAt == '+') {
                stringBuffer.append(charAt);
            }
        }
        return str.equals(stringBuffer.toString());
    }

    public static boolean f(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt < '0' || charAt > '9') {
                return false;
            }
        }
        return true;
    }

    private static boolean g(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && ((charAt < 'A' || charAt > 'Z') && (charAt < '0' || charAt > '9'))) {
                return false;
            }
        }
        return true;
    }
}
