package com.paypal.android.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.a.a.j;

public final class e {
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003f A[SYNTHETIC, Splitter:B:17:0x003f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable a(int r5, int r6) {
        /*
            r0 = 0
            byte[] r2 = com.paypal.android.a.g.a(r5, r6)     // Catch:{ Throwable -> 0x002f, all -> 0x0039 }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Throwable -> 0x002f, all -> 0x0039 }
            r1.<init>(r2)     // Catch:{ Throwable -> 0x002f, all -> 0x0039 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            r2.<init>()     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            java.lang.String r3 = ""
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            java.lang.String r3 = "."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r1, r2)     // Catch:{ Throwable -> 0x0049, all -> 0x0047 }
            r1.close()     // Catch:{ Throwable -> 0x0043 }
        L_0x002e:
            return r0
        L_0x002f:
            r1 = move-exception
            r1 = r0
        L_0x0031:
            if (r1 == 0) goto L_0x002e
            r1.close()     // Catch:{ Throwable -> 0x0037 }
            goto L_0x002e
        L_0x0037:
            r1 = move-exception
            goto L_0x002e
        L_0x0039:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x003d:
            if (r1 == 0) goto L_0x0042
            r1.close()     // Catch:{ Throwable -> 0x0045 }
        L_0x0042:
            throw r0
        L_0x0043:
            r1 = move-exception
            goto L_0x002e
        L_0x0045:
            r1 = move-exception
            goto L_0x0042
        L_0x0047:
            r0 = move-exception
            goto L_0x003d
        L_0x0049:
            r2 = move-exception
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.e.a(int, int):android.graphics.drawable.Drawable");
    }

    public static StateListDrawable a() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-6733, -22016});
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-211356, -1937101});
        GradientDrawable gradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-803493, -3845098});
        GradientDrawable gradientDrawable4 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{2147272292, 2145546547});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(1, -3637191);
        gradientDrawable2.setCornerRadius(5.0f);
        gradientDrawable2.setStroke(1, -1858224);
        gradientDrawable3.setCornerRadius(5.0f);
        gradientDrawable3.setStroke(1, -3042498);
        gradientDrawable4.setCornerRadius(5.0f);
        gradientDrawable4.setStroke(1, 2145625424);
        stateListDrawable.addState(new int[]{16842910, -16842919, -16842908}, gradientDrawable);
        stateListDrawable.addState(new int[]{16842910, -16842919, 16842908}, gradientDrawable2);
        stateListDrawable.addState(new int[]{16842910, 16842919, -16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{16842910, 16842919, 16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{-16842910, -16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, -16842919, 16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, 16842908}, gradientDrawable4);
        return stateListDrawable;
    }

    public static ImageView a(Context context, String str) {
        ImageView imageView = new ImageView(context);
        Drawable a = a(j.a.get(str).intValue(), j.b.get(str).intValue());
        imageView.setLayoutParams(new LinearLayout.LayoutParams((int) (((double) a.getIntrinsicWidth()) * Math.pow((double) PayPal.getInstance().getDensity(), 2.0d)), (int) (((double) a.getIntrinsicHeight()) * Math.pow((double) PayPal.getInstance().getDensity(), 2.0d))));
        imageView.setImageDrawable(a);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imageView;
    }

    public static StateListDrawable b() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-197380, -3355444});
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3487030, -6645094});
        GradientDrawable gradientDrawable3 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-7302507, -10395036});
        GradientDrawable gradientDrawable4 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{2143996618, 2140838554});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(1, -10066330);
        gradientDrawable2.setCornerRadius(5.0f);
        gradientDrawable2.setStroke(1, -12303292);
        gradientDrawable3.setCornerRadius(5.0f);
        gradientDrawable3.setStroke(1, -13421773);
        gradientDrawable4.setCornerRadius(5.0f);
        gradientDrawable4.setStroke(1, 2135180356);
        stateListDrawable.addState(new int[]{16842910, -16842919, -16842908}, gradientDrawable);
        stateListDrawable.addState(new int[]{16842910, -16842919, 16842908}, gradientDrawable2);
        stateListDrawable.addState(new int[]{16842910, 16842919, -16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{16842910, 16842919, 16842908}, gradientDrawable3);
        stateListDrawable.addState(new int[]{-16842910, -16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, -16842919, 16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, -16842908}, gradientDrawable4);
        stateListDrawable.addState(new int[]{-16842910, 16842919, 16842908}, gradientDrawable4);
        return stateListDrawable;
    }
}
