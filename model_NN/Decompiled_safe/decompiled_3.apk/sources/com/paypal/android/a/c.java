package com.paypal.android.a;

import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLSocketFactory;

public final class c extends SSLSocketFactory {
    private javax.net.ssl.SSLSocketFactory a;

    public c() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException {
        super((KeyStore) null);
        try {
            SSLContext instance = SSLContext.getInstance(SSLSocketFactory.TLS);
            instance.init(null, new TrustManager[]{new p()}, null);
            this.a = instance.getSocketFactory();
            setHostnameVerifier(new AllowAllHostnameVerifier());
        } catch (Exception e) {
        }
    }

    public final Socket createSocket() throws IOException {
        return this.a.createSocket();
    }

    public final Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        return this.a.createSocket(socket, str, i, z);
    }
}
