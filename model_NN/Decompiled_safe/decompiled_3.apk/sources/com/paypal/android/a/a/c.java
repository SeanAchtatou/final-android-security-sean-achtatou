package com.paypal.android.a.a;

import com.paypal.android.a.m;
import java.util.Vector;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class c {
    public a a;
    public a b;
    public f c;
    public Vector<k> d;
    private String e;

    public static c a(Element element) {
        if (element == null) {
            return null;
        }
        c cVar = new c();
        NodeList elementsByTagName = element.getElementsByTagName("fundingPlanId");
        if (elementsByTagName.getLength() != 1) {
            return null;
        }
        cVar.e = m.a(((Element) elementsByTagName.item(0)).getChildNodes());
        NodeList elementsByTagName2 = element.getElementsByTagName("fundingAmount");
        if (elementsByTagName2.getLength() != 1) {
            return null;
        }
        cVar.a = a.a((Element) elementsByTagName2.item(0));
        NodeList elementsByTagName3 = element.getElementsByTagName("backupFundingSource");
        if (elementsByTagName3.getLength() == 1) {
            g.a((Element) elementsByTagName3.item(0));
        }
        NodeList elementsByTagName4 = element.getElementsByTagName("senderFees");
        if (elementsByTagName4.getLength() == 1) {
            cVar.b = a.a((Element) elementsByTagName4.item(0));
        }
        NodeList elementsByTagName5 = element.getElementsByTagName("currencyConversion");
        if (elementsByTagName5.getLength() == 1) {
            cVar.c = f.a((Element) elementsByTagName5.item(0));
        }
        cVar.d = new Vector<>();
        NodeList elementsByTagName6 = element.getElementsByTagName("charge");
        for (int i = 0; i < elementsByTagName6.getLength(); i++) {
            k a2 = k.a((Element) elementsByTagName6.item(i));
            if (a2 != null) {
                cVar.d.add(a2);
            }
        }
        return cVar;
    }

    public final String a() {
        return this.e;
    }

    public final void a(String str) {
        this.e = str;
    }
}
