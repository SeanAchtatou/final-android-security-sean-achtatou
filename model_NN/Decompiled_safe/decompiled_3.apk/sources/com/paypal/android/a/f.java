package com.paypal.android.a;

import com.paypal.android.MEP.PayPal;

public final class f extends Exception {
    private static final long serialVersionUID = 1;

    public f(String str) {
        super(str);
        PayPal.loge("BadXMLException", "BadXMLException: " + str);
    }
}
