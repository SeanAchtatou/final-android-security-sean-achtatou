package com.paypal.android.a.a;

import com.facebook.internal.ServerProtocol;
import com.paypal.android.a.h;
import com.paypal.android.a.m;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class g {
    private String a;
    private String b;

    public static g a(Element element) {
        if (element == null) {
            return null;
        }
        g gVar = new g();
        NodeList elementsByTagName = element.getElementsByTagName(ServerProtocol.DIALOG_PARAM_TYPE);
        if (elementsByTagName.getLength() != 1) {
            return null;
        }
        gVar.b = m.a(((Element) elementsByTagName.item(0)).getChildNodes());
        NodeList elementsByTagName2 = element.getElementsByTagName("lastFourOfAccountNumber");
        if (elementsByTagName2.getLength() == 1) {
            gVar.a = m.a(((Element) elementsByTagName2.item(0)).getChildNodes());
        }
        NodeList elementsByTagName3 = element.getElementsByTagName("displayName");
        if (elementsByTagName3.getLength() == 1) {
            m.a(((Element) elementsByTagName3.item(0)).getChildNodes());
        }
        return gVar;
    }

    public final String a() {
        return this.b != null ? this.b.equals("BALANCE") ? h.a("ANDROID_balance") : (this.b.equals("BANK_DELAYED") || this.b.equals("BANK_INSTANT")) ? h.a("ANDROID_bank") : (this.b.equals("CREDITCARD") || this.b.equals("DEBITCARD")) ? h.a("ANDROID_card") : this.b : "";
    }

    public final void a(String str) {
        this.a = str;
    }

    public final String b() {
        return this.a;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final String c() {
        return this.b;
    }
}
