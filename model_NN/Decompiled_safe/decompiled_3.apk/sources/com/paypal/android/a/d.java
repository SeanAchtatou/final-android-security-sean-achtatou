package com.paypal.android.a;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.widget.LinearLayout;

public final class d {
    public static GradientDrawable a() {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -1510918, -1510918, -1510918, -1510918, -1510918});
        gradientDrawable.setCornerRadius(10.0f);
        gradientDrawable.setStroke(2, -7829368);
        return gradientDrawable;
    }

    public static GradientDrawable a(int i, int i2, int i3) {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, i2, i2, i2, i2, i2});
        gradientDrawable.setCornerRadius(5.0f);
        gradientDrawable.setStroke(2, i3);
        return gradientDrawable;
    }

    public static LinearLayout a(Context context, int i, int i2) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(i, i2));
        return linearLayout;
    }

    public static LinearLayout a(Context context, int i, int i2, float f) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.5f));
        return linearLayout;
    }

    public static int b() {
        return Build.VERSION.SDK.equals("3") ? 40 : -2;
    }
}
