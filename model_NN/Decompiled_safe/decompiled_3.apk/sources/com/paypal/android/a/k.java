package com.paypal.android.a;

class k extends Thread {
    private /* synthetic */ b a;

    k(b bVar) {
        this.a = bVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00f0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r11 = this;
            r10 = 8
            r9 = 4
            r3 = 0
            r8 = 0
            r7 = -1
        L_0x0006:
            com.paypal.android.a.b r0 = r11.a
            boolean r0 = r0.i
            if (r0 != 0) goto L_0x0336
            com.paypal.android.a.b r0 = r11.a
            java.util.Hashtable r0 = r0.h
            java.lang.String r1 = "delegate"
            java.lang.Object r0 = r0.get(r1)
            com.paypal.android.MEP.a$b r0 = (com.paypal.android.MEP.a.b) r0
            com.paypal.android.a.b r1 = r11.a
            int r1 = r1.g
            switch(r1) {
                case 0: goto L_0x0030;
                case 1: goto L_0x0025;
                case 2: goto L_0x0025;
                case 3: goto L_0x0100;
                case 4: goto L_0x0169;
                case 5: goto L_0x01db;
                case 6: goto L_0x00f9;
                case 7: goto L_0x020a;
                case 8: goto L_0x0267;
                case 9: goto L_0x0162;
                case 10: goto L_0x00a2;
                case 11: goto L_0x0231;
                case 12: goto L_0x029c;
                case 13: goto L_0x02d5;
                case 14: goto L_0x02f7;
                case 15: goto L_0x0319;
                default: goto L_0x0025;
            }
        L_0x0025:
            r0 = 100
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x002b }
            goto L_0x0006
        L_0x002b:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0006
        L_0x0030:
            java.lang.String r1 = "MPL"
            java.lang.String r2 = "start LOGIN"
            com.paypal.android.MEP.PayPal.logd(r1, r2)
            com.paypal.android.a.b r1 = com.paypal.android.a.b.e()
            java.lang.String r2 = "mpl-review"
            r1.a(r2)
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r4 = r11.a     // Catch:{ Exception -> 0x007b }
            com.paypal.android.a.b r1 = r11.a     // Catch:{ Exception -> 0x007b }
            java.util.Hashtable r1 = r1.h     // Catch:{ Exception -> 0x007b }
            java.lang.String r2 = "usernameOrPhone"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ Exception -> 0x007b }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x007b }
            com.paypal.android.a.b r2 = r11.a     // Catch:{ Exception -> 0x007b }
            java.util.Hashtable r2 = r2.h     // Catch:{ Exception -> 0x007b }
            java.lang.String r5 = "passwordOrPin"
            java.lang.Object r2 = r2.get(r5)     // Catch:{ Exception -> 0x007b }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x007b }
            boolean r1 = com.paypal.android.a.b.a(r4, r1, r2)     // Catch:{ Exception -> 0x007b }
        L_0x0067:
            if (r1 != 0) goto L_0x006e
            com.paypal.android.a.b r2 = r11.a
            int unused = r2.e = -1
        L_0x006e:
            if (r1 == 0) goto L_0x009a
            r0.a(r3, r8)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end LOGIN ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            goto L_0x0025
        L_0x007b:
            r1 = move-exception
            java.lang.String r2 = "Login"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Error during call to log in. "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r1 = r1.toString()
            com.paypal.android.MEP.PayPal.loge(r2, r1)
            r1 = r3
            goto L_0x0067
        L_0x009a:
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "LOGIN"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x00a2:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            java.lang.String r1 = "MPL"
            java.lang.String r2 = "start QUICK_LOGIN"
            com.paypal.android.MEP.PayPal.logd(r1, r2)
            com.paypal.android.a.b r1 = r11.a     // Catch:{ Exception -> 0x00d1 }
            boolean r2 = r1.B()     // Catch:{ Exception -> 0x00d1 }
            com.paypal.android.a.b r1 = r11.a     // Catch:{ Exception -> 0x033e }
            boolean r2 = com.paypal.android.a.b.e(r1)     // Catch:{ Exception -> 0x033e }
        L_0x00ba:
            if (r2 != 0) goto L_0x00c1
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
        L_0x00c1:
            if (r2 == 0) goto L_0x00f0
            r1 = 10
            r0.a(r1, r8)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end QUICK_LOGIN ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            goto L_0x0025
        L_0x00d1:
            r1 = move-exception
            r2 = r3
        L_0x00d3:
            java.lang.String r4 = "Login"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Error during call to log in. "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r1 = r1.getMessage()
            java.lang.StringBuilder r1 = r5.append(r1)
            java.lang.String r1 = r1.toString()
            android.util.Log.e(r4, r1)
            goto L_0x00ba
        L_0x00f0:
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "QUICK_LOGIN"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x00f9:
            java.lang.String r0 = "UPDATE_PAYMENT is supposed to be dead code"
            junit.framework.Assert.assertTrue(r0, r3)
            goto L_0x0025
        L_0x0100:
            java.lang.String r1 = "MPL"
            java.lang.String r2 = "start CREATE_PAYMENT"
            com.paypal.android.MEP.PayPal.logd(r1, r2)
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = com.paypal.android.a.b.e()
            java.lang.String r2 = "FundingPlanId"
            java.lang.String r4 = "0"
            r1.a(r2, r4)
            com.paypal.android.a.b r1 = com.paypal.android.a.b.e()
            java.lang.String r2 = "FundingPlans"
            r1.a(r2, r8)
            com.paypal.android.a.b r1 = com.paypal.android.a.b.e()
            java.lang.String r2 = "DefaultFundingPlan"
            r1.a(r2, r8)
            com.paypal.android.a.b r1 = r11.a
            java.util.Hashtable r2 = r1.y()
            if (r2 == 0) goto L_0x0154
            java.lang.String r1 = "ActionType"
            java.lang.Object r1 = r2.get(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.lang.String r4 = "PAY"
            boolean r1 = r1.equals(r4)
            if (r1 == 0) goto L_0x014f
            java.lang.String r1 = "-1"
            r0.a(r9, r1)
        L_0x0146:
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end CREATE_PAYMENT ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            goto L_0x0025
        L_0x014f:
            r1 = 3
            r0.a(r1, r2)
            goto L_0x0146
        L_0x0154:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "CREATE_PAYMENT"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x0162:
            java.lang.String r0 = "QUICK_PAY is supposed to be dead code"
            junit.framework.Assert.assertTrue(r0, r3)
            goto L_0x0025
        L_0x0169:
            java.lang.String r1 = "MPL"
            java.lang.String r2 = "start SEND_PAYMENT"
            com.paypal.android.MEP.PayPal.logd(r1, r2)
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = r11.a
            boolean r1 = r1.s()
            if (r1 != 0) goto L_0x0196
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r1 = r1.f()
            r0.d(r1)
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.getInstance()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "CHANGE_STRING"
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0025
        L_0x0196:
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r1 = com.paypal.android.a.b.h(r1)
            if (r1 == 0) goto L_0x01cd
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x01cd
            com.paypal.android.a.b r2 = r11.a
            java.lang.String r4 = "mpl-success"
            r2.a(r4)
        L_0x01ab:
            if (r1 == 0) goto L_0x01d3
            int r2 = r1.length()
            if (r2 <= 0) goto L_0x01d3
            r0.a(r9, r1)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end SEND_PAYMENT ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
        L_0x01bd:
            com.paypal.android.MEP.PayPalActivity r0 = com.paypal.android.MEP.PayPalActivity.getInstance()
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "CHANGE_STRING"
            r1.<init>(r2)
            r0.sendBroadcast(r1)
            goto L_0x0025
        L_0x01cd:
            com.paypal.android.a.b r2 = r11.a
            int unused = r2.e = -1
            goto L_0x01ab
        L_0x01d3:
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "SEND_PAYMENT"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x01bd
        L_0x01db:
            java.lang.String r1 = "MPL"
            java.lang.String r2 = "start FUNDING"
            com.paypal.android.MEP.PayPal.logd(r1, r2)
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = r11.a
            java.util.Hashtable r1 = r1.z()
            if (r1 == 0) goto L_0x01fc
            r2 = 5
            r0.a(r2, r1)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end FUNDING ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            goto L_0x0025
        L_0x01fc:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "FUNDING"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x020a:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = r11.a
            boolean r1 = r1.t()
            if (r1 == 0) goto L_0x0223
            r1 = 7
            com.paypal.android.a.b r2 = r11.a
            java.util.Hashtable r2 = r2.h
            r0.a(r1, r2)
            goto L_0x0025
        L_0x0223:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "GET_SHIPPING_ADDRESSES"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x0231:
            java.lang.String r1 = "MPL"
            java.lang.String r2 = "start CREATE_PIN"
            com.paypal.android.MEP.PayPal.logd(r1, r2)
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = r11.a
            boolean r1 = r1.C()
            if (r1 == 0) goto L_0x0259
            r1 = 11
            com.paypal.android.a.b r2 = r11.a
            java.util.Hashtable r2 = r2.h
            r0.a(r1, r2)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end CREATE_PIN ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            goto L_0x0025
        L_0x0259:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "CREATE_PIN"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x0267:
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "start CHECK_AUTH"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            com.paypal.android.a.b r0 = r11.a
            int unused = r0.g = -1
            com.paypal.android.a.b r0 = r11.a
            boolean r0 = r0.B()
            if (r0 == 0) goto L_0x0294
            com.paypal.android.c.a r0 = com.paypal.android.a.b.a
            com.paypal.android.a.b r1 = r11.a
            java.util.Hashtable r1 = r1.h
            r0.a(r10, r1)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end CHECK_AUTH ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
        L_0x028d:
            com.paypal.android.a.b r0 = r11.a
            int unused = r0.e = -1
            goto L_0x0025
        L_0x0294:
            com.paypal.android.a.b r0 = r11.a
            com.paypal.android.c.a r1 = com.paypal.android.a.b.a
            com.paypal.android.a.b.a(r0, r10, r1)
            goto L_0x028d
        L_0x029c:
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "start REMOVE_AUTH"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            com.paypal.android.a.b r0 = r11.a
            int unused = r0.g = -1
            com.paypal.android.a.b r0 = r11.a
            boolean r0 = r0.D()
            if (r0 == 0) goto L_0x02cb
            com.paypal.android.c.a r0 = com.paypal.android.a.b.b
            r1 = 12
            com.paypal.android.a.b r2 = r11.a
            java.util.Hashtable r2 = r2.h
            r0.a(r1, r2)
            java.lang.String r0 = "MPL"
            java.lang.String r1 = "end REMOVE_AUTH ok"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
        L_0x02c4:
            com.paypal.android.a.b r0 = r11.a
            int unused = r0.e = -1
            goto L_0x0025
        L_0x02cb:
            com.paypal.android.a.b r0 = r11.a
            r1 = 12
            com.paypal.android.c.a r2 = com.paypal.android.a.b.b
            com.paypal.android.a.b.a(r0, r1, r2)
            goto L_0x02c4
        L_0x02d5:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = r11.a
            boolean r1 = r1.v()
            if (r1 == 0) goto L_0x02e9
            r1 = 13
            r0.a(r1, r8)
            goto L_0x0025
        L_0x02e9:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "PREAPPROVAL_DETAILS"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x02f7:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.g = -1
            com.paypal.android.a.b r1 = r11.a
            boolean r1 = r1.w()
            if (r1 == 0) goto L_0x030b
            r1 = 14
            r0.a(r1, r8)
            goto L_0x0025
        L_0x030b:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "PREAPPROVAL_CONFIRM"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x0319:
            com.paypal.android.a.b r1 = r11.a
            boolean r1 = r1.u()
            if (r1 == 0) goto L_0x0328
            r1 = 15
            r0.a(r1, r8)
            goto L_0x0025
        L_0x0328:
            com.paypal.android.a.b r1 = r11.a
            int unused = r1.e = -1
            com.paypal.android.a.b r1 = r11.a
            java.lang.String r2 = "CREATE_PREAPPROVAL"
            com.paypal.android.a.b.a(r1, r2, r0)
            goto L_0x0025
        L_0x0336:
            java.lang.String r0 = "NetworkHandler"
            java.lang.String r1 = "thread exiting"
            com.paypal.android.MEP.PayPal.logd(r0, r1)
            return
        L_0x033e:
            r1 = move-exception
            goto L_0x00d3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.k.run():void");
    }
}
