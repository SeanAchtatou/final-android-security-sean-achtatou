package com.paypal.android.a.a;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public final class k {
    public a a = null;
    public g b = null;

    public static k a(Element element) {
        g a2;
        if (element == null) {
            return null;
        }
        k kVar = new k();
        kVar.a = null;
        kVar.b = null;
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Element element2 = (Element) childNodes.item(i);
            String tagName = element2.getTagName();
            if (tagName.equals("charge")) {
                a a3 = a.a(element2);
                if (a3 != null) {
                    kVar.a = a3;
                }
            } else if (tagName.equals("fundingSource") && (a2 = g.a(element2)) != null) {
                kVar.b = a2;
            }
        }
        if (kVar.a == null || kVar.b == null) {
            return null;
        }
        return kVar;
    }
}
