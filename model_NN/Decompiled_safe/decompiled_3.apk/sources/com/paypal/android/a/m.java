package com.paypal.android.a;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.biznessapps.constants.ReservationSystemConstants;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalInvoiceItem;
import com.paypal.android.MEP.PayPalPreapproval;
import com.paypal.android.MEP.PayPalReceiverDetails;
import com.paypal.android.a.a.b;
import com.paypal.android.a.a.c;
import com.paypal.android.a.a.d;
import com.paypal.android.a.a.e;
import com.paypal.android.a.a.h;
import com.paypal.android.a.a.i;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import javax.xml.parsers.DocumentBuilderFactory;
import junit.framework.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public final class m {
    private static String[] a = {"+1", "+27", "+30", "+31", "+32", "+33", "+34", "+36", "+39", "+41", "+43", "+44", "+45", "+46", "+47", "+48", "+49", "+52", "+54", "+55", "+56", "+58", "+60", "+61", "+64", "+65", "+66", "+81", "+82", "+86", "+90", "+91", "+351", "+352", "+353", "+354", "+356", "+357", "+358", "+370", "+371", "+372", "+377", "+386", "+420", "+421", "+506", "+593", "+598", "+852", "+886", "+972"};

    public static String a() {
        Exception exc;
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        String str6;
        String str7;
        String str8;
        String str9;
        String str10;
        String str11;
        String str12;
        String str13;
        String obj;
        String str14 = "";
        String str15 = "";
        String str16 = "";
        String str17 = "";
        String str18 = "";
        String str19 = "";
        String str20 = "";
        String str21 = "";
        String str22 = "";
        String str23 = "";
        try {
            str14 = b.b(PayPal.getInstance().getAppID());
            TelephonyManager telephonyManager = (TelephonyManager) PayPal.getInstance().getParentContext().getSystemService("phone");
            str22 = "MEID";
            String str24 = "AndroidCDMA";
            if (telephonyManager.getPhoneType() == 1) {
                str22 = "IMEI";
                str24 = "AndroidGSM";
            }
            String deviceId = telephonyManager.getDeviceId();
            str21 = "Phone";
            if (deviceId == null) {
                deviceId = ((WifiManager) PayPal.getInstance().getParentContext().getSystemService("wifi")).getConnectionInfo().getMacAddress();
                str22 = "MAC";
                str24 = "AndroidGSM";
                str21 = "Tablet";
            }
            str16 = b.b(deviceId);
            str18 = b.b(Build.DEVICE);
            str17 = b.b(Build.MODEL);
            str19 = "Android";
            str20 = b.b(Build.VERSION.SDK);
            str15 = b.b(PayPal.getVersionWithoutBuild());
            str = PayPal.getInstance().getParentContext().getPackageName();
            try {
                Context parentContext = PayPal.getInstance().getParentContext();
                PackageManager packageManager = parentContext.getPackageManager();
                obj = packageManager.getPackageInfo(parentContext.getPackageName(), 0).applicationInfo.loadLabel(packageManager).toString();
                if (obj == null || obj.length() <= 0) {
                    obj = str;
                }
                if (obj == null) {
                    obj = str;
                }
            } catch (Exception e) {
                exc = e;
                str23 = str;
                str3 = str24;
                str2 = str;
                exc.printStackTrace();
                String str25 = str2;
                str4 = str23;
                str5 = str22;
                str6 = str21;
                str7 = str20;
                str8 = str19;
                str9 = str18;
                str10 = str17;
                str11 = str16;
                str12 = "false";
                str13 = str25;
                StringBuilder sb = new StringBuilder();
                sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<soap:Envelope ").append("xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" ").append("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" ").append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ").append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ").append("xmlns:GMAdapter=\"http://svcs.paypal.com/mobile/adapter/types/pt\" ").append("xmlns:tns1=\"http://svcs.paypal.com/types/common\" ").append("xmlns:jaxb=\"http://java.sun.com/xml/ns/jaxb\" ").append("xmlns:annox=\"http://annox.dev.java.net\" ").append("xmlns:ov=\"http://annox.dev.java.net/net.sf.oval.constraint\" ").append("xsl:version=\"1.0\">").append("<soap:Body>").append("<DeviceInterrogationRequest>");
                a(sb, "version", "2.0");
                a(sb, "paypalAppId", str14);
                a(sb, "mplVersion", str15);
                a(sb, "deviceReferenceToken", d("DeviceReferenceToken"));
                sb.append("<deviceDetails>").append("<deviceId>");
                a(sb, "deviceIdType", str5);
                a(sb, "deviceIdentifier", str11);
                sb.append("</deviceId>");
                a(sb, "deviceName", str9);
                a(sb, "deviceModel", str10);
                a(sb, "systemName", str8);
                a(sb, "systemVersion", str7);
                a(sb, "deviceCategory", str6);
                a(sb, "deviceSimulator", str12);
                sb.append("</deviceDetails>");
                sb.append("<embeddingApplicationDetails>");
                a(sb, "deviceAppId", str);
                a(sb, "deviceAppName", str4);
                a(sb, "deviceAppDisplayName", str13);
                a(sb, "clientPlatform", str3);
                a(sb, "deviceAppVersion", str15);
                sb.append("</embeddingApplicationDetails>");
                sb.append("<securityDetails>");
                a(sb, "applicationNonce", e());
                a(sb, "deviceNonce", d());
                sb.append("</securityDetails>");
                sb.append("</DeviceInterrogationRequest>");
                sb.append("</soap:Body>");
                sb.append("</soap:Envelope>");
                return sb.toString();
            }
            try {
                str13 = obj;
                str5 = str22;
                str6 = str21;
                str7 = str20;
                str8 = str19;
                str9 = str18;
                str10 = str17;
                str11 = str16;
                str12 = str16.equals("000000000000000") ? "true" : "false";
                str3 = str24;
                str4 = str;
            } catch (Exception e2) {
                exc = e2;
                str3 = str24;
                str2 = obj;
                str23 = str;
                exc.printStackTrace();
                String str252 = str2;
                str4 = str23;
                str5 = str22;
                str6 = str21;
                str7 = str20;
                str8 = str19;
                str9 = str18;
                str10 = str17;
                str11 = str16;
                str12 = "false";
                str13 = str252;
                StringBuilder sb2 = new StringBuilder();
                sb2.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<soap:Envelope ").append("xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" ").append("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" ").append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ").append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ").append("xmlns:GMAdapter=\"http://svcs.paypal.com/mobile/adapter/types/pt\" ").append("xmlns:tns1=\"http://svcs.paypal.com/types/common\" ").append("xmlns:jaxb=\"http://java.sun.com/xml/ns/jaxb\" ").append("xmlns:annox=\"http://annox.dev.java.net\" ").append("xmlns:ov=\"http://annox.dev.java.net/net.sf.oval.constraint\" ").append("xsl:version=\"1.0\">").append("<soap:Body>").append("<DeviceInterrogationRequest>");
                a(sb2, "version", "2.0");
                a(sb2, "paypalAppId", str14);
                a(sb2, "mplVersion", str15);
                a(sb2, "deviceReferenceToken", d("DeviceReferenceToken"));
                sb2.append("<deviceDetails>").append("<deviceId>");
                a(sb2, "deviceIdType", str5);
                a(sb2, "deviceIdentifier", str11);
                sb2.append("</deviceId>");
                a(sb2, "deviceName", str9);
                a(sb2, "deviceModel", str10);
                a(sb2, "systemName", str8);
                a(sb2, "systemVersion", str7);
                a(sb2, "deviceCategory", str6);
                a(sb2, "deviceSimulator", str12);
                sb2.append("</deviceDetails>");
                sb2.append("<embeddingApplicationDetails>");
                a(sb2, "deviceAppId", str);
                a(sb2, "deviceAppName", str4);
                a(sb2, "deviceAppDisplayName", str13);
                a(sb2, "clientPlatform", str3);
                a(sb2, "deviceAppVersion", str15);
                sb2.append("</embeddingApplicationDetails>");
                sb2.append("<securityDetails>");
                a(sb2, "applicationNonce", e());
                a(sb2, "deviceNonce", d());
                sb2.append("</securityDetails>");
                sb2.append("</DeviceInterrogationRequest>");
                sb2.append("</soap:Body>");
                sb2.append("</soap:Envelope>");
                return sb2.toString();
            }
        } catch (Exception e3) {
            Exception exc2 = e3;
            str3 = "";
            str2 = "";
            str = "";
            exc = exc2;
            exc.printStackTrace();
            String str2522 = str2;
            str4 = str23;
            str5 = str22;
            str6 = str21;
            str7 = str20;
            str8 = str19;
            str9 = str18;
            str10 = str17;
            str11 = str16;
            str12 = "false";
            str13 = str2522;
            StringBuilder sb22 = new StringBuilder();
            sb22.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<soap:Envelope ").append("xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" ").append("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" ").append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ").append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ").append("xmlns:GMAdapter=\"http://svcs.paypal.com/mobile/adapter/types/pt\" ").append("xmlns:tns1=\"http://svcs.paypal.com/types/common\" ").append("xmlns:jaxb=\"http://java.sun.com/xml/ns/jaxb\" ").append("xmlns:annox=\"http://annox.dev.java.net\" ").append("xmlns:ov=\"http://annox.dev.java.net/net.sf.oval.constraint\" ").append("xsl:version=\"1.0\">").append("<soap:Body>").append("<DeviceInterrogationRequest>");
            a(sb22, "version", "2.0");
            a(sb22, "paypalAppId", str14);
            a(sb22, "mplVersion", str15);
            a(sb22, "deviceReferenceToken", d("DeviceReferenceToken"));
            sb22.append("<deviceDetails>").append("<deviceId>");
            a(sb22, "deviceIdType", str5);
            a(sb22, "deviceIdentifier", str11);
            sb22.append("</deviceId>");
            a(sb22, "deviceName", str9);
            a(sb22, "deviceModel", str10);
            a(sb22, "systemName", str8);
            a(sb22, "systemVersion", str7);
            a(sb22, "deviceCategory", str6);
            a(sb22, "deviceSimulator", str12);
            sb22.append("</deviceDetails>");
            sb22.append("<embeddingApplicationDetails>");
            a(sb22, "deviceAppId", str);
            a(sb22, "deviceAppName", str4);
            a(sb22, "deviceAppDisplayName", str13);
            a(sb22, "clientPlatform", str3);
            a(sb22, "deviceAppVersion", str15);
            sb22.append("</embeddingApplicationDetails>");
            sb22.append("<securityDetails>");
            a(sb22, "applicationNonce", e());
            a(sb22, "deviceNonce", d());
            sb22.append("</securityDetails>");
            sb22.append("</DeviceInterrogationRequest>");
            sb22.append("</soap:Body>");
            sb22.append("</soap:Envelope>");
            return sb22.toString();
        }
        StringBuilder sb222 = new StringBuilder();
        sb222.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<soap:Envelope ").append("xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" ").append("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" ").append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ").append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ").append("xmlns:GMAdapter=\"http://svcs.paypal.com/mobile/adapter/types/pt\" ").append("xmlns:tns1=\"http://svcs.paypal.com/types/common\" ").append("xmlns:jaxb=\"http://java.sun.com/xml/ns/jaxb\" ").append("xmlns:annox=\"http://annox.dev.java.net\" ").append("xmlns:ov=\"http://annox.dev.java.net/net.sf.oval.constraint\" ").append("xsl:version=\"1.0\">").append("<soap:Body>").append("<DeviceInterrogationRequest>");
        a(sb222, "version", "2.0");
        a(sb222, "paypalAppId", str14);
        a(sb222, "mplVersion", str15);
        a(sb222, "deviceReferenceToken", d("DeviceReferenceToken"));
        sb222.append("<deviceDetails>").append("<deviceId>");
        a(sb222, "deviceIdType", str5);
        a(sb222, "deviceIdentifier", str11);
        sb222.append("</deviceId>");
        a(sb222, "deviceName", str9);
        a(sb222, "deviceModel", str10);
        a(sb222, "systemName", str8);
        a(sb222, "systemVersion", str7);
        a(sb222, "deviceCategory", str6);
        a(sb222, "deviceSimulator", str12);
        sb222.append("</deviceDetails>");
        sb222.append("<embeddingApplicationDetails>");
        a(sb222, "deviceAppId", str);
        a(sb222, "deviceAppName", str4);
        a(sb222, "deviceAppDisplayName", str13);
        a(sb222, "clientPlatform", str3);
        a(sb222, "deviceAppVersion", str15);
        sb222.append("</embeddingApplicationDetails>");
        sb222.append("<securityDetails>");
        a(sb222, "applicationNonce", e());
        a(sb222, "deviceNonce", d());
        sb222.append("</securityDetails>");
        sb222.append("</DeviceInterrogationRequest>");
        sb222.append("</soap:Body>");
        sb222.append("</soap:Envelope>");
        return sb222.toString();
    }

    public static String a(Hashtable<String, Object> hashtable) {
        StringBuilder sb = new StringBuilder();
        sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n").append("<soapenv:Body id=\"_0\">\n").append("<ns2:ExecutePaymentRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n");
        a(sb, "payKey", b.b((String) hashtable.get("PayKey")));
        sb.append("<requestEnvelope>\n");
        a(sb, "errorLanguage", PayPalActivity._paypal.getLanguage());
        sb.append("</requestEnvelope>\n");
        b(sb, "actionType", (String) hashtable.get("ActionType"));
        b(sb, "fundingPlanId", (String) hashtable.get("FundingPlanId"));
        sb.append("</ns2:ExecutePaymentRequest>").append("</soapenv:Body>\n").append("</soapenv:Envelope>");
        return sb.toString();
    }

    public static String a(Hashtable<String, Object> hashtable, String str) throws n {
        String str2 = (String) hashtable.get("NewPhone");
        StringBuilder sb = new StringBuilder();
        sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"").append(" xmlns:pt=\"http://svcs.paypal.com/mobile/adapter/types/pt\">").append("<soapenv:Header/>").append("<soapenv:Body>").append("<pt:DeviceCreatePinRequest>");
        a(sb, "sessionToken", b.n());
        a(sb, "version", "1.0");
        sb.append("<phone>");
        a(sb, "countryCode", b.f(str2));
        a(sb, "phoneNumber", b.e(str2));
        sb.append("</phone>");
        a(sb, "pin", str);
        sb.append("<securityDetails>");
        a(sb, "applicationNonce", e());
        a(sb, "deviceNonce", d());
        sb.append("</securityDetails>");
        sb.append("</pt:DeviceCreatePinRequest>").append("</soapenv:Body>").append("</soapenv:Envelope>");
        return sb.toString();
    }

    public static String a(NodeList nodeList) {
        String str = "";
        for (int i = 0; i < nodeList.getLength(); i++) {
            str = str + nodeList.item(i).getNodeValue();
        }
        return str;
    }

    protected static StringBuilder a(StringBuilder sb, String str, String str2) {
        sb.append("<").append(str).append(">").append(str2).append("</").append(str).append(">");
        return sb;
    }

    protected static StringBuilder a(StringBuilder sb, String str, boolean z) {
        sb.append("<").append(str).append(">").append(z ? "true" : "false").append("</").append(str).append(">");
        return sb;
    }

    private static void a(String str, String str2) {
        try {
            FileOutputStream openFileOutput = PayPal.getInstance().getParentContext().openFileOutput(str, 2);
            openFileOutput.write(str2.getBytes());
            openFileOutput.flush();
            openFileOutput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
        }
    }

    private static void a(Hashtable<String, Object> hashtable, Node node, String str) {
        String nodeValue = node.getChildNodes().item(0).getNodeValue();
        hashtable.put(str, nodeValue);
        a("DeviceReferenceToken", nodeValue);
    }

    public static void a(Document document) throws f {
        NodeList elementsByTagName = document.getElementsByTagName("securityDetails");
        if (elementsByTagName.getLength() != 1) {
            throw new f("Not exactly one securityDetails tag");
        }
        a(elementsByTagName.item(0));
    }

    private static void a(Node node) {
        NodeList childNodes = node.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            Node item = childNodes.item(i);
            String nodeName = item.getNodeName();
            if (item.getChildNodes().getLength() > 0) {
                String nodeValue = item.getChildNodes().item(0).getNodeValue();
                if (nodeName.compareTo("applicationNonce") == 0) {
                    a("AppNonce", nodeValue);
                }
                if (nodeName.compareTo("deviceNonce") == 0) {
                    a("DeviceNonce", nodeValue);
                }
            }
        }
    }

    public static boolean a(String str, Hashtable<String, Object> hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("payKey");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            hashtable.put("PayKey", a(((Element) elementsByTagName6.item(0)).getChildNodes()));
            NodeList elementsByTagName7 = parse.getElementsByTagName("paymentExecStatus");
            if (elementsByTagName7.getLength() == 0) {
                return false;
            }
            hashtable.put("PaymentExecStatus", a(((Element) elementsByTagName7.item(0)).getChildNodes()));
            NodeList elementsByTagName8 = parse.getElementsByTagName("defaultFundingPlan");
            if (elementsByTagName8.getLength() != 0) {
                hashtable.put("DefaultFundingPlan", c.a((Element) elementsByTagName8.item(0)));
            }
            NodeList elementsByTagName9 = parse.getElementsByTagName("payErrorList");
            return elementsByTagName9.getLength() <= 0 || ((Element) elementsByTagName9.item(0)).getElementsByTagName("payError").getLength() != 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean a(Document document, Hashtable<String, Object> hashtable) {
        PayPal instance = PayPal.getInstance();
        try {
            NodeList elementsByTagName = document.getElementsByTagName("ns2:DeviceInterrogationResponse");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    String nodeName = item.getNodeName();
                    if (nodeName.equals("payButtonEnable")) {
                        hashtable.put(nodeName, item.getChildNodes().item(0).getNodeValue());
                    } else if (nodeName.equals("deviceReferenceToken")) {
                        a(hashtable, item, nodeName);
                    } else if (nodeName.equals("deviceAuthDetails")) {
                        NodeList childNodes2 = item.getChildNodes();
                        for (int i3 = 0; i3 < childNodes2.getLength(); i3++) {
                            Node item2 = childNodes2.item(i3);
                            String nodeName2 = item2.getNodeName();
                            if (item2.getChildNodes().getLength() > 0) {
                                String nodeValue = item2.getChildNodes().item(0).getNodeValue();
                                if (nodeValue == null) {
                                    nodeValue = "";
                                }
                                if (nodeName2.equals("userName")) {
                                    instance.setAccountName(nodeValue);
                                } else if (nodeName2.equals(ReservationSystemConstants.RECOVERY_PARAM_EMAIL)) {
                                    instance.setAccountEmail(nodeValue);
                                } else if (nodeName2.equals("phone")) {
                                    NodeList childNodes3 = item2.getChildNodes();
                                    Assert.assertTrue("Bad XML, <phone> doesn't have 2 children", childNodes3.getLength() == 2);
                                    String str = "";
                                    NodeList childNodes4 = childNodes3.item(0).getChildNodes();
                                    if (childNodes4.getLength() > 0) {
                                        str = childNodes4.item(0).getNodeValue();
                                    }
                                    instance.setAccountCountryDialingCode(str);
                                    String str2 = "";
                                    NodeList childNodes5 = childNodes3.item(1).getChildNodes();
                                    if (childNodes5.getLength() > 0) {
                                        str2 = childNodes5.item(0).getNodeValue();
                                    }
                                    instance.setAccountPhone("+" + b.m() + str2);
                                } else if (nodeName2.equals("authMethod")) {
                                    int parseInt = Integer.parseInt(nodeValue);
                                    instance.setAuthMethod(parseInt);
                                    instance.setIsRememberMe(parseInt == 2);
                                } else if (nodeName2.equals("authSetting")) {
                                    instance.setAuthSetting(Integer.parseInt(nodeValue));
                                }
                            }
                        }
                    } else if (nodeName.equals("securityDetails")) {
                        a(item);
                    }
                }
            }
            return true;
        } catch (Exception e) {
            PayPal.loge("XMLBuilder", "parseAuthenticationRequest caught exception " + e.getMessage());
            return false;
        }
    }

    public static String[] a(String str) {
        int i = 0;
        Vector vector = new Vector();
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("parameter");
            if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
                return null;
            }
            for (int i2 = 0; i2 < elementsByTagName.getLength(); i2++) {
                vector.add(elementsByTagName.item(i2).getChildNodes().item(0).getNodeValue());
            }
            String[] strArr = new String[vector.size()];
            while (true) {
                int i3 = i;
                if (i3 >= vector.size()) {
                    return strArr;
                }
                strArr[i3] = (String) vector.get(i3);
                i = i3 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String b() {
        return d("DeviceReferenceToken");
    }

    public static String b(String str) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("TransactionID");
            if (elementsByTagName == null || elementsByTagName.getLength() == 0) {
                return null;
            }
            return elementsByTagName.item(0).getChildNodes().item(0).getNodeValue();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String b(Hashtable<String, Object> hashtable) {
        String language = PayPalActivity._paypal.getLanguage();
        e eVar = (e) hashtable.get("ClientDetails");
        String str = (String) hashtable.get("DateOfMonth");
        String str2 = (String) hashtable.get("DayOfWeek");
        String b = b.b((String) hashtable.get("IpnNotificationUrl"));
        String str3 = (String) hashtable.get("MaxAmountPerPayment");
        String str4 = (String) hashtable.get("MaxNumberOfPayments");
        String str5 = (String) hashtable.get("MaxNumberOfPaymentsPerPeriod");
        String b2 = b.b((String) hashtable.get("Memo"));
        String str6 = (String) hashtable.get("PaymentPeriod");
        String str7 = (String) hashtable.get("PinType");
        String b3 = b.b((String) hashtable.get("SenderEmail"));
        String str8 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:PreapprovalRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<requestEnvelope>\n<errorLanguage>" + language + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<cancelUrl>" + "https://www.paypal.com" + "</cancelUrl>\n" + "<currencyCode>" + ((String) hashtable.get("CurrencyCode")) + "</currencyCode>\n" + "<endingDate>" + ((String) hashtable.get("EndingDate")) + "</endingDate>\n" + "<maxTotalAmountOfAllPayments>" + ((String) hashtable.get("MaxTotalAmountOfAllPayments")) + "</maxTotalAmountOfAllPayments>\n" + "<returnUrl>" + "https://www.paypal.com" + "</returnUrl>\n" + "<startingDate>" + ((String) hashtable.get("StartingDate")) + "</startingDate>\n";
        if (eVar != null) {
        }
        String str9 = (str == null || str.length() <= 0) ? str8 : str8 + "<dateOfMonth>" + str + "</dateOfMonth>\n";
        if (str2 != null && str2.length() > 0) {
            str9 = str9 + "<dayOfWeek>" + str2 + "</dayOfWeek>\n";
        }
        if (b != null && b.length() > 0) {
            str9 = str9 + "<ipnNotificationUrl>" + b.b(b) + "</ipnNotificationUrl>\n";
        }
        if (str3 != null && str3.length() > 0) {
            str9 = str9 + "<maxAmountPerPayment>" + str3 + "</maxAmountPerPayment>\n";
        }
        if (str4 != null && str4.length() > 0) {
            str9 = str9 + "<maxNumberOfPayments>" + str4 + "</maxNumberOfPayments>\n";
        }
        if (str5 != null && str5.length() > 0) {
            str9 = str9 + "<maxNumberOfPaymentsPerPeriod>" + str5 + "</maxNumberOfPaymentsPerPeriod>\n";
        }
        if (b2 != null && b2.length() > 0) {
            str9 = str9 + "<memo>" + b.b(b2) + "</memo>\n";
        }
        if (str6 != null && str6.length() > 0) {
            str9 = str9 + "<paymentPeriod>" + str6 + "</paymentPeriod>\n";
        }
        if (str7 != null && str7.length() > 0) {
            str9 = str9 + "<pinType>" + str7 + "</pinType>\n";
        }
        if (b3 != null && b3.length() > 0) {
            str9 = str9 + "<senderEmail>" + b.b(b3) + "</senderEmail>\n";
        }
        return str9 + "</ns2:PreapprovalRequest></soapenv:Body>\n</soapenv:Envelope>";
    }

    private static void b(StringBuilder sb, String str, String str2) {
        if (str2 != null) {
            a(sb, str, str2);
        }
    }

    public static boolean b(String str, Hashtable<String, Object> hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("preapprovalKey");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            hashtable.put("PreapprovalKey", a(((Element) elementsByTagName6.item(0)).getChildNodes()));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean b(Document document) throws f {
        NodeList elementsByTagName = document.getElementsByTagName("securityDetails");
        if (elementsByTagName.getLength() != 1) {
            throw new f("");
        }
        a(elementsByTagName.item(0));
        return true;
    }

    public static int c(Document document) {
        try {
            NodeList elementsByTagName = document.getElementsByTagName("ErrorCode");
            if ((elementsByTagName == null || elementsByTagName.getLength() == 0) && ((elementsByTagName = document.getElementsByTagName("errorId")) == null || elementsByTagName.getLength() == 0)) {
                return 200;
            }
            return Integer.parseInt(elementsByTagName.item(0).getChildNodes().item(0).getNodeValue());
        } catch (Exception e) {
            e.printStackTrace();
            return 10004;
        }
    }

    public static String c() {
        StringBuilder sb = new StringBuilder();
        sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"").append(" xmlns:pt=\"http://svcs.paypal.com/mobile/adapter/types/pt\">").append("<soapenv:Header/>").append("<soapenv:Body>").append("<pt:RemoveDeviceAuthorizationRequest>");
        a(sb, "deviceReferenceToken", d("DeviceReferenceToken"));
        sb.append("<securityDetails>");
        a(sb, "applicationNonce", e());
        a(sb, "deviceNonce", d());
        sb.append("</securityDetails>").append("</pt:RemoveDeviceAuthorizationRequest>").append("</soapenv:Body>").append("</soapenv:Envelope>");
        return sb.toString();
    }

    public static String c(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<soap:Envelope ").append("xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" ").append("xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" ").append("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ").append("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" ").append("xmlns:GMAdapter=\"http://svcs.paypal.com/mobile/adapter/types/pt\" ").append("xmlns:tns1=\"http://svcs.paypal.com/types/common\" ").append("xmlns:jaxb=\"http://java.sun.com/xml/ns/jaxb\" ").append("xmlns:annox=\"http://annox.dev.java.net\" ").append("xmlns:ov=\"http://annox.dev.java.net/net.sf.oval.constraint\" ").append("xsl:version=\"1.0\">").append("<soap:Body>").append("<GMAdapter:DeviceAuthenticationRequest>");
        a(sb, "version", "1.0");
        a(sb, "paypalAppId", b.b(PayPal.getInstance().getAppID()));
        a(sb, "mplVersion", PayPal.getVersionWithoutBuild());
        if (str.length() > 0) {
            sb.append(str);
        }
        a(sb, "deviceReferenceToken", d("DeviceReferenceToken"));
        sb.append("<securityDetails>");
        a(sb, "applicationNonce", e());
        a(sb, "deviceNonce", d());
        sb.append("</securityDetails>");
        sb.append("</GMAdapter:DeviceAuthenticationRequest>");
        sb.append("</soap:Body>");
        sb.append("</soap:Envelope>");
        return sb.toString();
    }

    public static String c(Hashtable<String, Object> hashtable) {
        String language = PayPalActivity._paypal.getLanguage();
        String b = b.b((String) hashtable.get("PreapprovalKey"));
        String str = (String) hashtable.get("Pin");
        String str2 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:ConfirmPreapprovalRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<requestEnvelope>\n<errorLanguage>" + language + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<preapprovalKey>" + b + "</preapprovalKey>\n";
        return (str != null ? str2 + "<pin>" + str + "</pin>\n" : str2) + "</ns2:ConfirmPreapprovalRequest></soapenv:Body>\n</soapenv:Envelope>";
    }

    public static boolean c(String str, Hashtable<String, Object> hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            PayPal instance = PayPal.getInstance();
            PayPalPreapproval preapproval = instance.getPreapproval();
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("approved");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            String a2 = a(((Element) elementsByTagName6.item(0)).getChildNodes());
            preapproval.setIsApproved(a2.equals("true"));
            hashtable.put("Approved", a2);
            NodeList elementsByTagName7 = parse.getElementsByTagName("cancelUrl");
            if (elementsByTagName7.getLength() == 0) {
                return false;
            }
            String a3 = a(((Element) elementsByTagName7.item(0)).getChildNodes());
            instance.setCancelUrl(a3);
            hashtable.put("CancelUrl", a3);
            NodeList elementsByTagName8 = parse.getElementsByTagName("currencyCode");
            if (elementsByTagName8.getLength() == 0) {
                return false;
            }
            String a4 = a(((Element) elementsByTagName8.item(0)).getChildNodes());
            preapproval.setCurrencyType(a4);
            hashtable.put("CurrencyCode", a4);
            NodeList elementsByTagName9 = parse.getElementsByTagName("dateOfMonth");
            if (elementsByTagName9.getLength() > 0) {
                String a5 = a(((Element) elementsByTagName9.item(0)).getChildNodes());
                preapproval.setDayOfMonth(Integer.parseInt(a5));
                hashtable.put("DateOfMonth", a5);
            }
            NodeList elementsByTagName10 = parse.getElementsByTagName("dayOfWeek");
            if (elementsByTagName10.getLength() > 0) {
                String a6 = a(((Element) elementsByTagName10.item(0)).getChildNodes());
                preapproval.setDayOfWeek(preapproval.getDayOfWeekInt(a6));
                hashtable.put("DayOfWeek", a6);
            }
            NodeList elementsByTagName11 = parse.getElementsByTagName("endingDate");
            if (elementsByTagName11.getLength() > 0) {
                String a7 = a(((Element) elementsByTagName11.item(0)).getChildNodes());
                preapproval.setEndDate(a7);
                hashtable.put("EndingDate", a7);
            }
            NodeList elementsByTagName12 = parse.getElementsByTagName("ipnNotificationUrl");
            if (elementsByTagName12.getLength() > 0) {
                String a8 = a(((Element) elementsByTagName12.item(0)).getChildNodes());
                preapproval.setIpnUrl(a8);
                hashtable.put("IpnNotificationUrl", a8);
            }
            NodeList elementsByTagName13 = parse.getElementsByTagName("maxAmountPerPayment");
            if (elementsByTagName13.getLength() > 0) {
                String a9 = a(((Element) elementsByTagName13.item(0)).getChildNodes());
                preapproval.setMaxAmountPerPayment(new BigDecimal(a9));
                hashtable.put("MaxAmountPerPayment", a9);
            }
            NodeList elementsByTagName14 = parse.getElementsByTagName("maxNumberOfPayments");
            if (elementsByTagName14.getLength() > 0) {
                String a10 = a(((Element) elementsByTagName14.item(0)).getChildNodes());
                preapproval.setMaxNumberOfPayments(Integer.parseInt(a10));
                hashtable.put("MaxNumberOfPayments", a10);
            }
            NodeList elementsByTagName15 = parse.getElementsByTagName("maxNumberOfPaymentsPerPeriod");
            if (elementsByTagName15.getLength() > 0) {
                String a11 = a(((Element) elementsByTagName15.item(0)).getChildNodes());
                preapproval.setMaxNumberOfPaymentsPerPeriod(Integer.parseInt(a11));
                hashtable.put("MaxNumberOfPaymentsPerPeriod", a11);
            }
            NodeList elementsByTagName16 = parse.getElementsByTagName("maxTotalAmountOfAllPayments");
            if (elementsByTagName16.getLength() == 0) {
                return false;
            }
            String a12 = a(((Element) elementsByTagName16.item(0)).getChildNodes());
            preapproval.setMaxTotalAmountOfAllPayments(new BigDecimal(a12));
            hashtable.put("MaxTotalAmountOfAllPayments", a12);
            NodeList elementsByTagName17 = parse.getElementsByTagName("memo");
            if (elementsByTagName17.getLength() > 0) {
                String a13 = a(((Element) elementsByTagName17.item(0)).getChildNodes());
                preapproval.setMemo(a13);
                hashtable.put("Memo", a13);
            }
            NodeList elementsByTagName18 = parse.getElementsByTagName("paymentPeriod");
            if (elementsByTagName18.getLength() > 0) {
                String a14 = a(((Element) elementsByTagName18.item(0)).getChildNodes());
                preapproval.setPaymentPeriod(preapproval.getPaymentPeriodInt(a14));
                hashtable.put("PaymentPeriod", a14);
            }
            NodeList elementsByTagName19 = parse.getElementsByTagName("pinType");
            if (elementsByTagName19.getLength() > 0) {
                String a15 = a(((Element) elementsByTagName19.item(0)).getChildNodes());
                preapproval.setPinRequired(a15.equals("REQUIRED"));
                hashtable.put("PinType", a15);
            }
            NodeList elementsByTagName20 = parse.getElementsByTagName("returnUrl");
            if (elementsByTagName20.getLength() == 0) {
                return false;
            }
            String a16 = a(((Element) elementsByTagName20.item(0)).getChildNodes());
            instance.setReturnUrl(a16);
            hashtable.put("ReturnUrl", a16);
            NodeList elementsByTagName21 = parse.getElementsByTagName("senderEmail");
            if (elementsByTagName21.getLength() > 0) {
                hashtable.put("SenderEmail", a(((Element) elementsByTagName21.item(0)).getChildNodes()));
            }
            NodeList elementsByTagName22 = parse.getElementsByTagName("startingDate");
            if (elementsByTagName22.getLength() == 0) {
                return false;
            }
            String a17 = a(((Element) elementsByTagName22.item(0)).getChildNodes());
            preapproval.setStartDate(a17);
            hashtable.put("StartingDate", a17);
            hashtable.put("PreapprovalDetails", preapproval);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static String d() {
        String d = d("DeviceNonce");
        return d.length() > 0 ? d : "None";
    }

    private static String d(String str) {
        String str2;
        try {
            FileInputStream openFileInput = PayPal.getInstance().getParentContext().openFileInput(str);
            byte[] bArr = new byte[openFileInput.available()];
            openFileInput.read(bArr);
            openFileInput.close();
            str2 = new String(bArr);
        } catch (Exception e) {
            str2 = null;
        }
        return str2 == null ? "" : str2;
    }

    public static String d(Hashtable<String, Object> hashtable) {
        String language = PayPalActivity._paypal.getLanguage();
        String b = b.b((String) hashtable.get("PreapprovalKey"));
        String b2 = b.b((String) hashtable.get("GetBillingAddress"));
        String str = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:PreapprovalDetailsRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<requestEnvelope>\n<errorLanguage>" + language + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<preapprovalKey>" + b + "</preapprovalKey>\n";
        if (b2 != null) {
            str = str + "<getBillingAddress>" + b2 + "</getBillingAddress>\n";
        }
        return str + "</ns2:PreapprovalDetailsRequest></soapenv:Body>\n</soapenv:Envelope>";
    }

    public static boolean d(String str, Hashtable<String, Object> hashtable) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static String e() {
        String d = d("AppNonce");
        return d.length() > 0 ? d : "None";
    }

    private static String e(String str) throws n {
        Assert.assertTrue("phone number must have +, country dialing code, phone number", str.indexOf("+") == 0);
        for (int i = 0; i < a.length; i++) {
            if (str.indexOf(a[i]) >= 0) {
                return str.replace(a[i], "");
            }
        }
        throw new n("unusable phone number " + str);
    }

    public static String e(Hashtable hashtable) {
        String b = b.b((String) hashtable.get("PayKey"));
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:GetFundingPlansRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<payKey>" + b + "</payKey>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + PayPalActivity._paypal.getLanguage() + "</errorLanguage>\n" + "</requestEnvelope>\n" + "</ns2:GetFundingPlansRequest>" + "</soapenv:Body>\n" + "</soapenv:Envelope>";
    }

    public static boolean e(String str, Hashtable<String, Object> hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            Vector vector = new Vector();
            NodeList elementsByTagName6 = parse.getElementsByTagName("fundingPlan");
            for (int i = 0; i < elementsByTagName6.getLength(); i++) {
                vector.add(c.a((Element) elementsByTagName6.item(i)));
            }
            hashtable.put("FundingPlans", vector);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static String f(String str) throws n {
        Assert.assertTrue("phone number must have +, country dialing code, phone number", str.indexOf("+") == 0);
        for (int i = 0; i < a.length; i++) {
            if (str.indexOf(a[i]) >= 0) {
                return a[i].substring(1);
            }
        }
        throw new n("unusable phone number " + str);
    }

    public static String f(Hashtable<String, Object> hashtable) throws n {
        BigDecimal bigDecimal;
        String str;
        String str2 = (String) hashtable.get("PaymentCurrencyID");
        String language = PayPalActivity._paypal.getLanguage();
        String str3 = (String) hashtable.get("TrackingId");
        String str4 = (String) hashtable.get("ReverseAllParallelPaymentsOnError");
        String str5 = str4 == null ? "true" : str4;
        String str6 = (String) hashtable.get("Pin");
        String b = b.b((String) hashtable.get("IpnNotificationUrl"));
        String str7 = (String) hashtable.get("FeesPayer");
        String str8 = (String) hashtable.get("FundingType");
        String b2 = b.b((String) hashtable.get("Memo"));
        e eVar = (e) hashtable.get("ClientDetails");
        ArrayList arrayList = (ArrayList) hashtable.get("Receivers");
        if (arrayList == null) {
            return null;
        }
        String str9 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"><soapenv:Body>\n<ns2:Pay xmlns:ns2=\"http://svcs.paypal.com/services\">\n<requestEnvelope>\n<errorLanguage>" + language + "</errorLanguage>\n" + "</requestEnvelope>\n" + "<sender>\n" + "<useCredentials>true</useCredentials>\n" + "</sender>\n" + "<actionType>" + ((String) hashtable.get("ActionType")) + "</actionType>\n" + "<cancelUrl>" + "https://www.paypal.com" + "</cancelUrl>\n" + "<returnUrl>" + "https://www.paypal.com" + "</returnUrl>\n" + "<currencyCode>" + str2 + "</currencyCode>\n" + "<receiverList>\n";
        int i = 0;
        String str10 = str9;
        while (true) {
            int i2 = i;
            if (i2 < arrayList.size()) {
                BigDecimal subtotal = ((PayPalReceiverDetails) arrayList.get(i2)).getSubtotal();
                if (((PayPalReceiverDetails) arrayList.get(i2)).getInvoiceData() != null) {
                    BigDecimal shipping = ((PayPalReceiverDetails) arrayList.get(i2)).getInvoiceData().getShipping();
                    BigDecimal tax = ((PayPalReceiverDetails) arrayList.get(i2)).getInvoiceData().getTax();
                    BigDecimal add = shipping != null ? subtotal.add(shipping) : subtotal;
                    bigDecimal = tax != null ? add.add(tax) : add;
                } else {
                    bigDecimal = subtotal;
                }
                bigDecimal.setScale(2, 4);
                subtotal.setScale(2, 4);
                int paymentType = ((PayPalReceiverDetails) arrayList.get(i2)).getPaymentType();
                int paymentSubtype = ((PayPalReceiverDetails) arrayList.get(i2)).getPaymentSubtype();
                boolean isPrimary = ((PayPalReceiverDetails) arrayList.get(i2)).getIsPrimary();
                String str11 = (str10 + "<receiver>\n") + "<amount>" + bigDecimal.toString() + "</amount>\n";
                String recipient = ((PayPalReceiverDetails) arrayList.get(i2)).getRecipient();
                if (recipient == null || recipient.length() <= 0) {
                    return null;
                }
                if (((PayPalReceiverDetails) arrayList.get(i2)).isEmailRecipient()) {
                    str = str11 + "<email>" + b.b(recipient) + "</email>\n";
                } else if (!((PayPalReceiverDetails) arrayList.get(i2)).isPhoneRecipient()) {
                    return null;
                } else {
                    String str12 = str11 + "<phone>\n";
                    String f = f(recipient);
                    if (f == null || f.length() == 0) {
                        f = b.m();
                    }
                    str = ((str12 + "<countryCode>" + f + "</countryCode>") + "<phoneNumber>" + e(recipient) + "</phoneNumber>") + "</phone>\n";
                }
                if (paymentType != 3) {
                    str = str + "<paymentType>" + PayPal.getPayType(paymentType) + "</paymentType>\n";
                }
                if (paymentSubtype != 22) {
                    str = str + "<paymentSubtype>" + PayPal.getPaySubtype(paymentSubtype) + "</paymentSubtype>\n";
                }
                if (isPrimary) {
                    str = str + "<primary>" + isPrimary + "</primary>\n";
                }
                str10 = str + "</receiver>\n";
                i = i2 + 1;
            } else {
                String str13 = str10 + "</receiverList>\n";
                if (str3 != null && str3.length() > 0) {
                    str13 = str13 + "<trackingId>" + b.b(str3) + "</trackingId>\n";
                }
                if (str5 != null && str5.length() > 0) {
                    str13 = str13 + "<reverseAllParallelPaymentsOnError>" + str5 + "</reverseAllParallelPaymentsOnError>\n";
                }
                if (str6 != null && str6.length() > 0) {
                    str13 = str13 + "<pin>" + str6 + "</pin>\n";
                }
                if (b != null && b.length() > 0) {
                    str13 = str13 + "<ipnNotificationUrl>" + b.b(b) + "</ipnNotificationUrl>\n";
                }
                if (str7 != null && str7.length() > 0) {
                    str13 = str13 + "<feesPayer>" + str7 + "</feesPayer>\n";
                }
                if (str8 != null && str8.length() > 0) {
                    str13 = str13 + "<fundingConstraint>\n<allowedFundingType>\n<fundingTypeInfo>\n<fundingType>" + str8 + "</fundingType>\n" + "</fundingTypeInfo>\n" + "</allowedFundingType>\n" + "</fundingConstraint>\n";
                }
                if (eVar != null) {
                }
                if (b2 != null && b2.length() > 0) {
                    str13 = str13 + "<memo>" + b.b(b2) + "</memo>\n";
                }
                String str14 = str13 + "</ns2:Pay></soapenv:Body>\n</soapenv:Envelope>";
                try {
                    str14.getBytes(Charset.forName("UTF-8").name());
                    return str14;
                } catch (IllegalCharsetNameException e) {
                    PayPal.loge("XMLBuilder", "Exception " + e.getMessage());
                    return str14;
                } catch (UnsupportedCharsetException e2) {
                    PayPal.loge("XMLBuilder", "Exception " + e2.getMessage());
                    return str14;
                } catch (UnsupportedEncodingException e3) {
                    PayPal.loge("XMLBuilder", "Exception " + e3.getMessage());
                    return str14;
                }
            }
        }
        return null;
    }

    public static boolean f(String str, Hashtable<String, Object> hashtable) {
        try {
            NodeList elementsByTagName = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8"))).getElementsByTagName("MEPRemoveDeviceAuthorizationResponseType");
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NodeList childNodes = elementsByTagName.item(i).getChildNodes();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    String nodeName = item.getNodeName();
                    if (nodeName.compareTo("DeviceReferenceToken") == 0) {
                        a(hashtable, item, nodeName);
                    }
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String g(Hashtable<String, Object> hashtable) throws n {
        String str;
        String str2;
        String language = PayPalActivity._paypal.getLanguage();
        b bVar = (b) hashtable.get("DisplayOptions");
        d dVar = (d) hashtable.get("InstitutionCustomer");
        String str3 = (String) hashtable.get("ShippingAddressId");
        i iVar = (i) hashtable.get("SenderOptions");
        ArrayList arrayList = (ArrayList) hashtable.get("Receivers");
        String str4 = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:SetPaymentOptionsRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<payKey>" + b.b((String) hashtable.get("PayKey")) + "</payKey>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + language + "</errorLanguage>\n" + "</requestEnvelope>\n";
        if (bVar != null) {
        }
        if (dVar != null) {
        }
        if (str3 != null) {
            str4 = str4 + "<shippingAddressId>" + b.b(str3) + "</shippingAddressId>\n";
        }
        if (iVar != null) {
            str4 = str4 + "<senderOptions>\n<sharePhoneNumber>" + false + "</sharePhoneNumber>\n" + "<shareAddress>" + false + "</shareAddress>\n" + "<requireShippingAddressSelection>" + false + "</requireShippingAddressSelection>\n" + "</senderOptions>\n";
        }
        if (arrayList != null) {
            int i = 0;
            str = str4;
            while (i < arrayList.size()) {
                PayPalReceiverDetails payPalReceiverDetails = (PayPalReceiverDetails) arrayList.get(i);
                String str5 = str + "<receiverOptions>\n";
                if (payPalReceiverDetails.getDescription() != null && payPalReceiverDetails.getDescription().length() > 0) {
                    str5 = str5 + "<description>" + b.b(payPalReceiverDetails.getDescription()) + "</description>\n";
                }
                if (payPalReceiverDetails.getCustomID() != null && payPalReceiverDetails.getCustomID().length() > 0) {
                    str5 = str5 + "<customId>" + b.b(payPalReceiverDetails.getCustomID()) + "</customId>\n";
                }
                if (payPalReceiverDetails.getInvoiceData() != null) {
                    String str6 = str5 + "<invoiceData>\n";
                    for (int i2 = 0; i2 < payPalReceiverDetails.getInvoiceData().getInvoiceItems().size(); i2++) {
                        PayPalInvoiceItem payPalInvoiceItem = payPalReceiverDetails.getInvoiceData().getInvoiceItems().get(i2);
                        String str7 = str6 + "<item>\n";
                        if (payPalInvoiceItem.getName() != null && payPalInvoiceItem.getName().length() > 0) {
                            str7 = str7 + "<name>" + b.b(payPalInvoiceItem.getName()) + "</name>\n";
                        }
                        if (payPalInvoiceItem.getID() != null && payPalInvoiceItem.getID().length() > 0) {
                            str7 = str7 + "<identifier>" + b.b(payPalInvoiceItem.getID()) + "</identifier>\n";
                        }
                        if (payPalInvoiceItem.getTotalPrice() != null && payPalInvoiceItem.getTotalPrice().toString().length() > 0) {
                            str7 = str7 + "<price>" + b.b(payPalInvoiceItem.getTotalPrice().toString()) + "</price>\n";
                        }
                        if (payPalInvoiceItem.getUnitPrice() != null && payPalInvoiceItem.getUnitPrice().toString().length() > 0) {
                            str7 = str7 + "<itemPrice>" + b.b(payPalInvoiceItem.getUnitPrice().toString()) + "</itemPrice>\n";
                        }
                        if (payPalInvoiceItem.getQuantity() > 0) {
                            str7 = str7 + "<itemCount>" + b.b("" + payPalInvoiceItem.getQuantity()) + "</itemCount>\n";
                        }
                        str6 = str7 + "</item>\n";
                    }
                    if (payPalReceiverDetails.getInvoiceData().getTax() != null && payPalReceiverDetails.getInvoiceData().getTax().compareTo(BigDecimal.ZERO) > 0) {
                        str6 = str6 + "<totalTax>" + b.b(payPalReceiverDetails.getInvoiceData().getTax().toString()) + "</totalTax>\n";
                    }
                    if (payPalReceiverDetails.getInvoiceData().getShipping() != null && payPalReceiverDetails.getInvoiceData().getShipping().compareTo(BigDecimal.ZERO) > 0) {
                        str6 = str6 + "<totalShipping>" + b.b(payPalReceiverDetails.getInvoiceData().getShipping().toString()) + "</totalShipping>\n";
                    }
                    str5 = str6 + "</invoiceData>\n";
                }
                String str8 = str5 + "<receiver>\n";
                if (payPalReceiverDetails.isEmailRecipient()) {
                    str2 = str8 + "<email>" + b.b(payPalReceiverDetails.getRecipient()) + "</email>\n";
                } else if (((PayPalReceiverDetails) arrayList.get(i)).isPhoneRecipient()) {
                    String str9 = str8 + "<phone>\n";
                    String f = f(payPalReceiverDetails.getRecipient());
                    if (f == null || f.length() == 0) {
                        f = b.m();
                    }
                    str2 = ((str9 + "<countryCode>" + f + "</countryCode>") + "<phoneNumber>" + e(payPalReceiverDetails.getRecipient()) + "</phoneNumber>") + "</phone>\n";
                } else {
                    str2 = str8;
                }
                i++;
                str = str2 + "</receiver>\n</receiverOptions>\n";
            }
        } else {
            str = str4;
        }
        return str + "</ns2:SetPaymentOptionsRequest></soapenv:Body>\n</soapenv:Envelope>";
    }

    public static boolean g(String str, Hashtable<String, Object> hashtable) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes("UTF-8")));
            NodeList elementsByTagName = parse.getElementsByTagName("responseEnvelope");
            if (elementsByTagName.getLength() == 0) {
                return false;
            }
            Element element = (Element) elementsByTagName.item(0);
            NodeList elementsByTagName2 = element.getElementsByTagName("timestamp");
            if (elementsByTagName2.getLength() == 0) {
                return false;
            }
            hashtable.put("TimeStamp", a(((Element) elementsByTagName2.item(0)).getChildNodes()));
            NodeList elementsByTagName3 = element.getElementsByTagName("ack");
            if (elementsByTagName3.getLength() == 0) {
                return false;
            }
            hashtable.put("Ack", a(((Element) elementsByTagName3.item(0)).getChildNodes()));
            NodeList elementsByTagName4 = element.getElementsByTagName("correlationId");
            if (elementsByTagName4.getLength() == 0) {
                return false;
            }
            hashtable.put("CorrelationId", a(((Element) elementsByTagName4.item(0)).getChildNodes()));
            NodeList elementsByTagName5 = element.getElementsByTagName("build");
            if (elementsByTagName5.getLength() == 0) {
                return false;
            }
            hashtable.put("Build", a(((Element) elementsByTagName5.item(0)).getChildNodes()));
            NodeList elementsByTagName6 = parse.getElementsByTagName("availableAddress");
            if (elementsByTagName6.getLength() == 0) {
                return false;
            }
            Vector vector = new Vector();
            for (int i = 0; i < elementsByTagName6.getLength(); i++) {
                h hVar = new h();
                if (hVar.a((Element) elementsByTagName6.item(i))) {
                    vector.add(hVar);
                }
            }
            hashtable.put("AvailableAddresses", vector);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String h(Hashtable<String, Object> hashtable) {
        String b = b.b((String) hashtable.get("PayKey"));
        return "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soapenv:Body id=\"_0\">\n<ns2:GetAvailableShippingAddressesRequest xmlns:ns2=\"http://svcs.paypal.com/types/ap\">\n<key>" + b + "</key>\n" + "<requestEnvelope>\n" + "<errorLanguage>" + PayPalActivity._paypal.getLanguage() + "</errorLanguage>\n" + "</requestEnvelope>\n" + "</ns2:GetAvailableShippingAddressesRequest>" + "</soapenv:Body>\n" + "</soapenv:Envelope>";
    }
}
