package com.paypal.android.a;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPOutputStream;

public final class a {
    private static final byte[] a = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] b = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    private static final byte[] c = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] d = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    private static final byte[] e = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] f = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, -9, -9, -9, -9};

    /* renamed from: com.paypal.android.a.a$a  reason: collision with other inner class name */
    public static class C0006a extends FilterOutputStream {
        private boolean a;
        private int b;
        private byte[] c;
        private int d;
        private int e;
        private boolean f;
        private byte[] g;
        private boolean h;
        private int i;
        private byte[] j;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C0006a(OutputStream outputStream, int i2) {
            super(outputStream);
            boolean z = true;
            this.f = (i2 & 8) != 8;
            this.a = (i2 & 1) != 1 ? false : z;
            this.d = this.a ? 3 : 4;
            this.c = new byte[this.d];
            this.b = 0;
            this.e = 0;
            this.h = false;
            this.g = new byte[4];
            this.i = i2;
            this.j = a.b(i2);
        }

        public final void close() throws IOException {
            if (this.b > 0) {
                if (this.a) {
                    this.out.write(a.a(this.c, 0, this.b, this.g, 0, this.i));
                    this.b = 0;
                } else {
                    throw new IOException("Base64 input not properly padded.");
                }
            }
            super.close();
            this.c = null;
            this.out = null;
        }

        public final void write(int i2) throws IOException {
            if (this.a) {
                byte[] bArr = this.c;
                int i3 = this.b;
                this.b = i3 + 1;
                bArr[i3] = (byte) i2;
                if (this.b >= this.d) {
                    this.out.write(a.a(this.c, 0, this.d, this.g, 0, this.i));
                    this.e += 4;
                    if (this.f && this.e >= 76) {
                        this.out.write(10);
                        this.e = 0;
                    }
                    this.b = 0;
                }
            } else if (this.j[i2 & 127] > -5) {
                byte[] bArr2 = this.c;
                int i4 = this.b;
                this.b = i4 + 1;
                bArr2[i4] = (byte) i2;
                if (this.b >= this.d) {
                    this.out.write(this.g, 0, a.b(this.c, 0, this.g, 0, this.i));
                    this.b = 0;
                }
            } else if (this.j[i2 & 127] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public final void write(byte[] bArr, int i2, int i3) throws IOException {
            for (int i4 = 0; i4 < i3; i4++) {
                write(bArr[i2 + i4]);
            }
        }
    }

    private a() {
    }

    public static String a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, 8);
    }

    private static String a(byte[] bArr, int i, int i2, int i3) {
        ByteArrayOutputStream byteArrayOutputStream;
        GZIPOutputStream gZIPOutputStream;
        C0006a aVar;
        Throwable th;
        int i4 = i3 & 8;
        if ((i3 & 2) == 2) {
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    aVar = new C0006a(byteArrayOutputStream, i3 | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(aVar);
                        try {
                            gZIPOutputStream.write(bArr, 0, i2);
                            gZIPOutputStream.close();
                            try {
                                gZIPOutputStream.close();
                            } catch (Exception e2) {
                            }
                            try {
                                aVar.close();
                            } catch (Exception e3) {
                            }
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e4) {
                            }
                            try {
                                return new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                            } catch (UnsupportedEncodingException e5) {
                                return new String(byteArrayOutputStream.toByteArray());
                            }
                        } catch (IOException e6) {
                            e = e6;
                            try {
                                e.printStackTrace();
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e7) {
                                }
                                try {
                                    aVar.close();
                                } catch (Exception e8) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                    return null;
                                } catch (Exception e9) {
                                    return null;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                try {
                                    gZIPOutputStream.close();
                                } catch (Exception e10) {
                                }
                                try {
                                    aVar.close();
                                } catch (Exception e11) {
                                }
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e12) {
                                }
                                throw th;
                            }
                        }
                    } catch (IOException e13) {
                        e = e13;
                        gZIPOutputStream = null;
                        e.printStackTrace();
                        gZIPOutputStream.close();
                        aVar.close();
                        byteArrayOutputStream.close();
                        return null;
                    } catch (Throwable th3) {
                        gZIPOutputStream = null;
                        th = th3;
                        gZIPOutputStream.close();
                        aVar.close();
                        byteArrayOutputStream.close();
                        throw th;
                    }
                } catch (IOException e14) {
                    e = e14;
                    aVar = null;
                    gZIPOutputStream = null;
                    e.printStackTrace();
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    return null;
                } catch (Throwable th4) {
                    aVar = null;
                    gZIPOutputStream = null;
                    th = th4;
                    gZIPOutputStream.close();
                    aVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e15) {
                e = e15;
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                e.printStackTrace();
                gZIPOutputStream.close();
                aVar.close();
                byteArrayOutputStream.close();
                return null;
            } catch (Throwable th5) {
                aVar = null;
                gZIPOutputStream = null;
                byteArrayOutputStream = null;
                th = th5;
                gZIPOutputStream.close();
                aVar.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = i4 == 0;
            int i5 = (i2 << 2) / 3;
            byte[] bArr2 = new byte[((z ? i5 / 76 : 0) + i5 + (i2 % 3 > 0 ? 4 : 0))];
            int i6 = i2 - 2;
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            while (i8 < i6) {
                a(bArr, i8, 3, bArr2, i9, i3);
                int i10 = i7 + 4;
                if (z && i10 == 76) {
                    bArr2[i9 + 4] = 10;
                    i9++;
                    i10 = 0;
                }
                i8 += 3;
                i9 += 4;
                i7 = i10;
            }
            if (i8 < i2) {
                a(bArr, i8, i2 - i8, bArr2, i9, i3);
                i9 += 4;
            }
            try {
                return new String(bArr2, 0, i9, "UTF-8");
            } catch (UnsupportedEncodingException e16) {
                return new String(bArr2, 0, i9);
            }
        }
    }

    /* access modifiers changed from: private */
    public static byte[] a(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        int i5 = 0;
        byte[] bArr3 = (i4 & 16) == 16 ? c : (i4 & 32) == 32 ? e : a;
        int i6 = (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0);
        if (i2 > 2) {
            i5 = (bArr[i + 2] << 24) >>> 24;
        }
        int i7 = i5 | i6;
        switch (i2) {
            case 1:
                bArr2[i3] = bArr3[i7 >>> 18];
                bArr2[i3 + 1] = bArr3[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = 61;
                bArr2[i3 + 3] = 61;
                break;
            case 2:
                bArr2[i3] = bArr3[i7 >>> 18];
                bArr2[i3 + 1] = bArr3[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = bArr3[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = 61;
                break;
            case 3:
                bArr2[i3] = bArr3[i7 >>> 18];
                bArr2[i3 + 1] = bArr3[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = bArr3[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = bArr3[i7 & 63];
                break;
        }
        return bArr2;
    }

    /* access modifiers changed from: private */
    public static int b(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        byte[] b2 = b(i3);
        if (bArr[i + 2] == 61) {
            bArr2[i2] = (byte) ((((b2[bArr[i + 1]] & 255) << 12) | ((b2[bArr[i]] & 255) << 18)) >>> 16);
            return 1;
        } else if (bArr[i + 3] == 61) {
            int i4 = ((b2[bArr[i + 2]] & 255) << 6) | ((b2[bArr[i]] & 255) << 18) | ((b2[bArr[i + 1]] & 255) << 12);
            bArr2[i2] = (byte) (i4 >>> 16);
            bArr2[i2 + 1] = (byte) (i4 >>> 8);
            return 2;
        } else {
            try {
                byte b3 = ((b2[bArr[i]] & 255) << 18) | ((b2[bArr[i + 1]] & 255) << 12) | ((b2[bArr[i + 2]] & 255) << 6) | (b2[bArr[i + 3]] & 255);
                bArr2[i2] = (byte) (b3 >> 16);
                bArr2[i2 + 1] = (byte) (b3 >> 8);
                bArr2[i2 + 2] = (byte) b3;
                return 3;
            } catch (Exception e2) {
                System.out.println("" + ((int) bArr[i]) + ": " + ((int) b2[bArr[i]]));
                System.out.println("" + ((int) bArr[i + 1]) + ": " + ((int) b2[bArr[i + 1]]));
                System.out.println("" + ((int) bArr[i + 2]) + ": " + ((int) b2[bArr[i + 2]]));
                System.out.println("" + ((int) bArr[i + 3]) + ": " + ((int) b2[bArr[i + 3]]));
                return -1;
            }
        }
    }

    /* access modifiers changed from: private */
    public static final byte[] b(int i) {
        return (i & 16) == 16 ? d : (i & 32) == 32 ? f : b;
    }
}
