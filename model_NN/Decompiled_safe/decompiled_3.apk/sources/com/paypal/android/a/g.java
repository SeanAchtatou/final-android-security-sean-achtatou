package com.paypal.android.a;

public final class g {
    private static byte[] a;
    private static g b = null;

    public static void a() {
        if (b == null) {
            b = new g();
        }
        a = b.b("com/paypal/android/utils/data/data.bin");
    }

    public static byte[] a(int i, int i2) {
        return a(i, i2, a);
    }

    public static byte[] a(int i, int i2, byte[] bArr) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return bArr2;
    }

    public static byte[] a(String str) {
        return b.b(str);
    }

    public static void b() {
        a = null;
        b = null;
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:0x0041 */
    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r1v3, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r1v9, types: [byte[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0031 A[SYNTHETIC, Splitter:B:17:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0036 A[SYNTHETIC, Splitter:B:20:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0056 A[SYNTHETIC, Splitter:B:39:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x005b A[SYNTHETIC, Splitter:B:42:0x005b] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] b(java.lang.String r8) {
        /*
            r7 = this;
            r1 = 0
            java.lang.Class r0 = r7.getClass()     // Catch:{ IOException -> 0x0074, all -> 0x0052 }
            java.lang.ClassLoader r0 = r0.getClassLoader()     // Catch:{ IOException -> 0x0074, all -> 0x0052 }
            if (r0 == 0) goto L_0x0049
            java.io.InputStream r0 = r0.getResourceAsStream(r8)     // Catch:{ IOException -> 0x0074, all -> 0x0052 }
            if (r0 == 0) goto L_0x004a
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0078, all -> 0x0069 }
            r2.<init>()     // Catch:{ IOException -> 0x0078, all -> 0x0069 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x002b, all -> 0x006f }
        L_0x001a:
            r4 = 0
            int r5 = r3.length     // Catch:{ IOException -> 0x002b, all -> 0x006f }
            int r4 = r0.read(r3, r4, r5)     // Catch:{ IOException -> 0x002b, all -> 0x006f }
            r5 = -1
            if (r4 == r5) goto L_0x003a
            r5 = 0
            r2.write(r3, r5, r4)     // Catch:{ IOException -> 0x002b, all -> 0x006f }
            java.lang.Thread.yield()     // Catch:{ IOException -> 0x002b, all -> 0x006f }
            goto L_0x001a
        L_0x002b:
            r3 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
        L_0x002f:
            if (r0 == 0) goto L_0x0034
            r0.close()     // Catch:{ Throwable -> 0x0061 }
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ Throwable -> 0x0063 }
        L_0x0039:
            return r1
        L_0x003a:
            byte[] r1 = r2.toByteArray()     // Catch:{ IOException -> 0x002b, all -> 0x006f }
            r2.close()     // Catch:{ Throwable -> 0x005f }
        L_0x0041:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ Throwable -> 0x0047 }
            goto L_0x0039
        L_0x0047:
            r0 = move-exception
            goto L_0x0039
        L_0x0049:
            r0 = r1
        L_0x004a:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ Throwable -> 0x0050 }
            goto L_0x0039
        L_0x0050:
            r0 = move-exception
            goto L_0x0039
        L_0x0052:
            r0 = move-exception
            r2 = r1
        L_0x0054:
            if (r2 == 0) goto L_0x0059
            r2.close()     // Catch:{ Throwable -> 0x0065 }
        L_0x0059:
            if (r1 == 0) goto L_0x005e
            r1.close()     // Catch:{ Throwable -> 0x0067 }
        L_0x005e:
            throw r0
        L_0x005f:
            r2 = move-exception
            goto L_0x0041
        L_0x0061:
            r0 = move-exception
            goto L_0x0034
        L_0x0063:
            r0 = move-exception
            goto L_0x0039
        L_0x0065:
            r2 = move-exception
            goto L_0x0059
        L_0x0067:
            r1 = move-exception
            goto L_0x005e
        L_0x0069:
            r2 = move-exception
            r6 = r2
            r2 = r1
            r1 = r0
            r0 = r6
            goto L_0x0054
        L_0x006f:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0054
        L_0x0074:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x002f
        L_0x0078:
            r2 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.paypal.android.a.g.b(java.lang.String):byte[]");
    }
}
