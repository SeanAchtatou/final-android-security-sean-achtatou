package com.facebook.android;

public final class R {

    public static final class attr {
        public static final int confirm_logout = 2130771980;
        public static final int done_button_background = 2130771974;
        public static final int done_button_text = 2130771972;
        public static final int extra_fields = 2130771969;
        public static final int fetch_user_info = 2130771981;
        public static final int is_cropped = 2130771985;
        public static final int login_text = 2130771982;
        public static final int logout_text = 2130771983;
        public static final int multi_select = 2130771975;
        public static final int preset_size = 2130771984;
        public static final int radius_in_meters = 2130771976;
        public static final int results_limit = 2130771977;
        public static final int search_text = 2130771978;
        public static final int show_pictures = 2130771968;
        public static final int show_search_box = 2130771979;
        public static final int show_title_bar = 2130771970;
        public static final int title_bar_background = 2130771973;
        public static final int title_text = 2130771971;
    }

    public static final class color {
        public static final int com_facebook_blue = 2131165186;
        public static final int com_facebook_loginview_text_color = 2131165190;
        public static final int com_facebook_picker_search_bar_background = 2131165184;
        public static final int com_facebook_picker_search_bar_text = 2131165185;
        public static final int com_facebook_usersettingsfragment_connected_shadow_color = 2131165188;
        public static final int com_facebook_usersettingsfragment_connected_text_color = 2131165187;
        public static final int com_facebook_usersettingsfragment_not_connected_text_color = 2131165189;
    }

    public static final class dimen {
        public static final int com_facebook_loginview_compound_drawable_padding = 2131099656;
        public static final int com_facebook_loginview_padding_bottom = 2131099655;
        public static final int com_facebook_loginview_padding_left = 2131099652;
        public static final int com_facebook_loginview_padding_right = 2131099653;
        public static final int com_facebook_loginview_padding_top = 2131099654;
        public static final int com_facebook_loginview_text_size = 2131099657;
        public static final int com_facebook_picker_divider_width = 2131099649;
        public static final int com_facebook_picker_place_image_size = 2131099648;
        public static final int com_facebook_profilepictureview_preset_size_large = 2131099660;
        public static final int com_facebook_profilepictureview_preset_size_normal = 2131099659;
        public static final int com_facebook_profilepictureview_preset_size_small = 2131099658;
        public static final int com_facebook_usersettingsfragment_profile_picture_height = 2131099651;
        public static final int com_facebook_usersettingsfragment_profile_picture_width = 2131099650;
    }

    public static final class drawable {
        public static final int com_facebook_button_blue = 2130837571;
        public static final int com_facebook_button_blue_focused = 2130837572;
        public static final int com_facebook_button_blue_normal = 2130837573;
        public static final int com_facebook_button_blue_pressed = 2130837574;
        public static final int com_facebook_button_check = 2130837575;
        public static final int com_facebook_button_check_off = 2130837576;
        public static final int com_facebook_button_check_on = 2130837577;
        public static final int com_facebook_button_grey_focused = 2130837578;
        public static final int com_facebook_button_grey_normal = 2130837579;
        public static final int com_facebook_button_grey_pressed = 2130837580;
        public static final int com_facebook_close = 2130837581;
        public static final int com_facebook_inverse_icon = 2130837582;
        public static final int com_facebook_list_divider = 2130837583;
        public static final int com_facebook_list_section_header_background = 2130837584;
        public static final int com_facebook_loginbutton_silver = 2130837585;
        public static final int com_facebook_logo = 2130837586;
        public static final int com_facebook_picker_default_separator_color = 2130837858;
        public static final int com_facebook_picker_item_background = 2130837587;
        public static final int com_facebook_picker_list_focused = 2130837588;
        public static final int com_facebook_picker_list_longpressed = 2130837589;
        public static final int com_facebook_picker_list_pressed = 2130837590;
        public static final int com_facebook_picker_list_selector = 2130837591;
        public static final int com_facebook_picker_list_selector_background_transition = 2130837592;
        public static final int com_facebook_picker_list_selector_disabled = 2130837593;
        public static final int com_facebook_picker_magnifier = 2130837594;
        public static final int com_facebook_picker_top_button = 2130837595;
        public static final int com_facebook_place_default_icon = 2130837596;
        public static final int com_facebook_profile_default_icon = 2130837597;
        public static final int com_facebook_profile_picture_blank_portrait = 2130837598;
        public static final int com_facebook_profile_picture_blank_square = 2130837599;
        public static final int com_facebook_top_background = 2130837600;
        public static final int com_facebook_top_button = 2130837601;
        public static final int com_facebook_usersettingsfragment_background_gradient = 2130837602;
    }

    public static final class id {
        public static final int com_facebook_login_activity_progress_bar = 2131034180;
        public static final int com_facebook_picker_activity_circle = 2131034179;
        public static final int com_facebook_picker_checkbox = 2131034182;
        public static final int com_facebook_picker_checkbox_stub = 2131034186;
        public static final int com_facebook_picker_divider = 2131034190;
        public static final int com_facebook_picker_done_button = 2131034189;
        public static final int com_facebook_picker_image = 2131034183;
        public static final int com_facebook_picker_list_section_header = 2131034187;
        public static final int com_facebook_picker_list_view = 2131034178;
        public static final int com_facebook_picker_profile_pic_stub = 2131034184;
        public static final int com_facebook_picker_row_activity_circle = 2131034181;
        public static final int com_facebook_picker_search_text = 2131034195;
        public static final int com_facebook_picker_title = 2131034185;
        public static final int com_facebook_picker_title_bar = 2131034192;
        public static final int com_facebook_picker_title_bar_stub = 2131034191;
        public static final int com_facebook_picker_top_bar = 2131034188;
        public static final int com_facebook_search_bar_view = 2131034194;
        public static final int com_facebook_usersettingsfragment_login_button = 2131034198;
        public static final int com_facebook_usersettingsfragment_logo_image = 2131034196;
        public static final int com_facebook_usersettingsfragment_profile_name = 2131034197;
        public static final int large = 2131034114;
        public static final int normal = 2131034113;
        public static final int picker_subtitle = 2131034193;
        public static final int small = 2131034112;
    }

    public static final class layout {
        public static final int com_facebook_friendpickerfragment = 2130903050;
        public static final int com_facebook_login_activity_layout = 2130903051;
        public static final int com_facebook_picker_activity_circle_row = 2130903052;
        public static final int com_facebook_picker_checkbox = 2130903053;
        public static final int com_facebook_picker_image = 2130903054;
        public static final int com_facebook_picker_list_row = 2130903055;
        public static final int com_facebook_picker_list_section_header = 2130903056;
        public static final int com_facebook_picker_search_box = 2130903057;
        public static final int com_facebook_picker_title_bar = 2130903058;
        public static final int com_facebook_picker_title_bar_stub = 2130903059;
        public static final int com_facebook_placepickerfragment = 2130903060;
        public static final int com_facebook_placepickerfragment_list_row = 2130903061;
        public static final int com_facebook_search_bar_layout = 2130903062;
        public static final int com_facebook_usersettingsfragment = 2130903063;
    }

    public static final class string {
        public static final int com_facebook_choose_friends = 2131230735;
        public static final int com_facebook_dialogloginactivity_ok_button = 2131230720;
        public static final int com_facebook_internet_permission_error_message = 2131230739;
        public static final int com_facebook_internet_permission_error_title = 2131230738;
        public static final int com_facebook_loading = 2131230737;
        public static final int com_facebook_loginview_cancel_action = 2131230726;
        public static final int com_facebook_loginview_log_in_button = 2131230722;
        public static final int com_facebook_loginview_log_out_action = 2131230725;
        public static final int com_facebook_loginview_log_out_button = 2131230721;
        public static final int com_facebook_loginview_logged_in_as = 2131230723;
        public static final int com_facebook_loginview_logged_in_using_facebook = 2131230724;
        public static final int com_facebook_logo_content_description = 2131230727;
        public static final int com_facebook_nearby = 2131230736;
        public static final int com_facebook_picker_done_button_text = 2131230734;
        public static final int com_facebook_placepicker_subtitle_catetory_only_format = 2131230732;
        public static final int com_facebook_placepicker_subtitle_format = 2131230731;
        public static final int com_facebook_placepicker_subtitle_were_here_only_format = 2131230733;
        public static final int com_facebook_requesterror_password_changed = 2131230742;
        public static final int com_facebook_requesterror_permissions = 2131230744;
        public static final int com_facebook_requesterror_reconnect = 2131230743;
        public static final int com_facebook_requesterror_relogin = 2131230741;
        public static final int com_facebook_requesterror_web_login = 2131230740;
        public static final int com_facebook_usersettingsfragment_log_in_button = 2131230728;
        public static final int com_facebook_usersettingsfragment_logged_in = 2131230729;
        public static final int com_facebook_usersettingsfragment_not_logged_in = 2131230730;
    }

    public static final class style {
        public static final int com_facebook_loginview_default_style = 2131296256;
        public static final int com_facebook_loginview_silver_style = 2131296257;
    }

    public static final class styleable {
        public static final int[] com_facebook_friend_picker_fragment = {com.app_luigi.layout.R.attr.multi_select};
        public static final int com_facebook_friend_picker_fragment_multi_select = 0;
        public static final int[] com_facebook_login_view = {com.app_luigi.layout.R.attr.confirm_logout, com.app_luigi.layout.R.attr.fetch_user_info, com.app_luigi.layout.R.attr.login_text, com.app_luigi.layout.R.attr.logout_text};
        public static final int com_facebook_login_view_confirm_logout = 0;
        public static final int com_facebook_login_view_fetch_user_info = 1;
        public static final int com_facebook_login_view_login_text = 2;
        public static final int com_facebook_login_view_logout_text = 3;
        public static final int[] com_facebook_picker_fragment = {com.app_luigi.layout.R.attr.show_pictures, com.app_luigi.layout.R.attr.extra_fields, com.app_luigi.layout.R.attr.show_title_bar, com.app_luigi.layout.R.attr.title_text, com.app_luigi.layout.R.attr.done_button_text, com.app_luigi.layout.R.attr.title_bar_background, com.app_luigi.layout.R.attr.done_button_background};
        public static final int com_facebook_picker_fragment_done_button_background = 6;
        public static final int com_facebook_picker_fragment_done_button_text = 4;
        public static final int com_facebook_picker_fragment_extra_fields = 1;
        public static final int com_facebook_picker_fragment_show_pictures = 0;
        public static final int com_facebook_picker_fragment_show_title_bar = 2;
        public static final int com_facebook_picker_fragment_title_bar_background = 5;
        public static final int com_facebook_picker_fragment_title_text = 3;
        public static final int[] com_facebook_place_picker_fragment = {com.app_luigi.layout.R.attr.radius_in_meters, com.app_luigi.layout.R.attr.results_limit, com.app_luigi.layout.R.attr.search_text, com.app_luigi.layout.R.attr.show_search_box};
        public static final int com_facebook_place_picker_fragment_radius_in_meters = 0;
        public static final int com_facebook_place_picker_fragment_results_limit = 1;
        public static final int com_facebook_place_picker_fragment_search_text = 2;
        public static final int com_facebook_place_picker_fragment_show_search_box = 3;
        public static final int[] com_facebook_profile_picture_view = {com.app_luigi.layout.R.attr.preset_size, com.app_luigi.layout.R.attr.is_cropped};
        public static final int com_facebook_profile_picture_view_is_cropped = 1;
        public static final int com_facebook_profile_picture_view_preset_size = 0;
    }
}
