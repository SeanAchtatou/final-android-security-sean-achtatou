package com.facebook;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.biznessapps.constants.AppConstants;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.PlatformServiceClient;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import org.json.JSONException;
import org.json.JSONObject;

public class AppLinkData {
    private static final String APPLINK_BRIDGE_ARGS_KEY = "bridge_args";
    private static final String APPLINK_METHOD_ARGS_KEY = "method_args";
    private static final String APPLINK_VERSION_KEY = "version";
    public static final String ARGUMENTS_TAPTIME_KEY = "com.facebook.platform.APPLINK_TAP_TIME_UTC";
    private static final String BRIDGE_ARGS_METHOD_KEY = "method";
    private static final String BUNDLE_APPLINK_ARGS_KEY = "com.facebook.platform.APPLINK_ARGS";
    private static final String METHOD_ARGS_REF_KEY = "ref";
    private static final String METHOD_ARGS_TARGET_URL_KEY = "target_url";
    /* access modifiers changed from: private */
    public static final String TAG = AppLinkData.class.getCanonicalName();
    private JSONObject arguments;
    private String[] ref;
    private Uri targetUri;
    private String version;

    public interface CompletionHandler {
        void onDeferredAppLinkDataFetched(AppLinkData appLinkData);
    }

    public static void fetchDeferredAppLinkData(Context context, CompletionHandler completionHandler) {
        fetchDeferredAppLinkData(context, null, completionHandler);
    }

    public static void fetchDeferredAppLinkData(Context context, String applicationId, final CompletionHandler completionHandler) {
        Validate.notNull(context, "context");
        Validate.notNull(completionHandler, "completionHandler");
        if (applicationId == null) {
            applicationId = Utility.getMetadataApplicationId(context);
        }
        Validate.notNull(applicationId, "applicationId");
        DeferredAppLinkDataClient client = new DeferredAppLinkDataClient(context, applicationId);
        client.setCompletedListener(new PlatformServiceClient.CompletedListener() {
            public void completed(Bundle result) {
                AppLinkData appLinkData = null;
                if (result != null) {
                    String appLinkArgsJsonString = result.getString(AppLinkData.BUNDLE_APPLINK_ARGS_KEY);
                    long tapTimeUtc = result.getLong(AppLinkData.ARGUMENTS_TAPTIME_KEY, -1);
                    appLinkData = AppLinkData.createFromJson(appLinkArgsJsonString);
                    if (tapTimeUtc != -1) {
                        try {
                            appLinkData.getArguments().put(AppLinkData.ARGUMENTS_TAPTIME_KEY, tapTimeUtc);
                        } catch (JSONException e) {
                            Log.d(AppLinkData.TAG, "Unable to put tap time in AppLinkData.arguments");
                        }
                    }
                }
                completionHandler.onDeferredAppLinkDataFetched(appLinkData);
            }
        });
        if (!client.start()) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    completionHandler.onDeferredAppLinkDataFetched(null);
                }
            });
        }
    }

    public static AppLinkData createFromActivity(Activity activity) {
        Validate.notNull(activity, "activity");
        Intent intent = activity.getIntent();
        if (intent == null) {
            return null;
        }
        AppLinkData appLinkData = createFromJson(intent.getStringExtra(BUNDLE_APPLINK_ARGS_KEY));
        if (appLinkData == null) {
            return createFromUri(intent.getData());
        }
        return appLinkData;
    }

    /* access modifiers changed from: private */
    public static AppLinkData createFromJson(String jsonString) {
        String ref2;
        if (jsonString == null) {
            return null;
        }
        try {
            JSONObject appLinkArgsJson = new JSONObject(jsonString);
            String version2 = appLinkArgsJson.getString("version");
            if (appLinkArgsJson.getJSONObject(APPLINK_BRIDGE_ARGS_KEY).getString(BRIDGE_ARGS_METHOD_KEY).equals("applink") && version2.equals(AppConstants.TWITTER_USER_TYPE)) {
                AppLinkData appLinkData = new AppLinkData();
                appLinkData.version = version2;
                appLinkData.arguments = appLinkArgsJson.getJSONObject(APPLINK_METHOD_ARGS_KEY);
                if (appLinkData.arguments.has(METHOD_ARGS_REF_KEY) && (ref2 = appLinkData.arguments.getString(METHOD_ARGS_REF_KEY)) != null) {
                    appLinkData.ref = ref2.split(",");
                }
                if (!appLinkData.arguments.has(METHOD_ARGS_TARGET_URL_KEY)) {
                    return appLinkData;
                }
                appLinkData.targetUri = Uri.parse(appLinkData.arguments.getString(METHOD_ARGS_TARGET_URL_KEY));
                return appLinkData;
            }
        } catch (JSONException e) {
            Log.d(TAG, "Unable to parse AppLink JSON");
        }
        return null;
    }

    private static AppLinkData createFromUri(Uri appLinkDataUri) {
        if (appLinkDataUri == null) {
        }
        return null;
    }

    private AppLinkData() {
    }

    public Uri getTargetUri() {
        return this.targetUri;
    }

    public String[] getRef() {
        return this.ref;
    }

    public JSONObject getArguments() {
        return this.arguments;
    }

    static final class DeferredAppLinkDataClient extends PlatformServiceClient {
        DeferredAppLinkDataClient(Context context, String applicationId) {
            super(context, NativeProtocol.MESSAGE_GET_INSTALL_DATA_REQUEST, NativeProtocol.MESSAGE_GET_INSTALL_DATA_REPLY, NativeProtocol.PROTOCOL_VERSION_20130618, applicationId);
        }

        /* access modifiers changed from: protected */
        public void populateRequestBundle(Bundle data) {
            data.putString(NativeProtocol.EXTRA_GET_INSTALL_DATA_PACKAGE, getContext().getPackageName());
        }
    }
}
