package com.facebook.model;

import com.biznessapps.constants.ServerConstants;
import com.facebook.internal.NativeProtocol;
import java.util.Date;
import java.util.List;

public interface OpenGraphObject extends GraphObject {
    GraphObject getApplication();

    GraphObjectList<GraphObject> getAudio();

    @PropertyName(NativeProtocol.OPEN_GRAPH_CREATE_OBJECT_KEY)
    boolean getCreateObject();

    Date getCreatedTime();

    GraphObject getData();

    String getDescription();

    String getDeterminer();

    String getId();

    GraphObjectList<GraphObject> getImage();

    boolean getIsScraped();

    String getPostActionId();

    List<String> getSeeAlso();

    String getSiteName();

    String getTitle();

    String getType();

    Date getUpdatedTime();

    String getUrl();

    GraphObjectList<GraphObject> getVideo();

    void setApplication(GraphObject graphObject);

    void setAudio(GraphObjectList<GraphObject> graphObjectList);

    @PropertyName(NativeProtocol.OPEN_GRAPH_CREATE_OBJECT_KEY)
    void setCreateObject(boolean z);

    void setCreatedTime(Date date);

    void setData(GraphObject graphObject);

    void setDescription(String str);

    void setDeterminer(String str);

    void setId(String str);

    void setImage(GraphObjectList<GraphObject> graphObjectList);

    @CreateGraphObject(NativeProtocol.IMAGE_URL_KEY)
    @PropertyName(ServerConstants.POST_IMAGE_PARAM)
    void setImageUrls(List<String> list);

    void setIsScraped(boolean z);

    void setPostActionId(String str);

    void setSeeAlso(List<String> list);

    void setSiteName(String str);

    void setTitle(String str);

    void setType(String str);

    void setUpdatedTime(Date date);

    void setUrl(String str);

    void setVideo(GraphObjectList<GraphObject> graphObjectList);

    public static final class Factory {
        public static OpenGraphObject createForPost(String type) {
            return createForPost(OpenGraphObject.class, type);
        }

        public static <T extends OpenGraphObject> T createForPost(Class<T> graphObjectClass, String type) {
            return createForPost(graphObjectClass, type, null, null, null, null);
        }

        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public static <T extends com.facebook.model.OpenGraphObject> T createForPost(java.lang.Class<T> r4, java.lang.String r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9) {
            /*
                r3 = 1
                com.facebook.model.GraphObject r0 = com.facebook.model.GraphObject.Factory.create(r4)
                com.facebook.model.OpenGraphObject r0 = (com.facebook.model.OpenGraphObject) r0
                if (r5 == 0) goto L_0x000c
                r0.setType(r5)
            L_0x000c:
                if (r6 == 0) goto L_0x0011
                r0.setTitle(r6)
            L_0x0011:
                if (r7 == 0) goto L_0x001f
                java.lang.String[] r1 = new java.lang.String[r3]
                r2 = 0
                r1[r2] = r7
                java.util.List r1 = java.util.Arrays.asList(r1)
                r0.setImageUrls(r1)
            L_0x001f:
                if (r8 == 0) goto L_0x0024
                r0.setUrl(r8)
            L_0x0024:
                if (r9 == 0) goto L_0x0029
                r0.setDescription(r9)
            L_0x0029:
                r0.setCreateObject(r3)
                com.facebook.model.GraphObject r1 = com.facebook.model.GraphObject.Factory.create()
                r0.setData(r1)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.model.OpenGraphObject.Factory.createForPost(java.lang.Class, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):com.facebook.model.OpenGraphObject");
        }
    }
}
