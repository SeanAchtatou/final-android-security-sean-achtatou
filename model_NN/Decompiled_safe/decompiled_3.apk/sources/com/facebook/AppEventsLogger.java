package com.facebook;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import com.biznessapps.utils.DateUtils;
import com.facebook.Request;
import com.facebook.internal.Logger;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import com.facebook.model.GraphObject;
import com.facebook.widget.PlacePickerFragment;
import com.google.analytics.tracking.android.ModelFields;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AppEventsLogger {
    public static final String ACTION_APP_EVENTS_FLUSHED = "com.facebook.sdk.APP_EVENTS_FLUSHED";
    public static final String APP_EVENTS_EXTRA_FLUSH_RESULT = "com.facebook.sdk.APP_EVENTS_FLUSH_RESULT";
    public static final String APP_EVENTS_EXTRA_NUM_EVENTS_FLUSHED = "com.facebook.sdk.APP_EVENTS_NUM_EVENTS_FLUSHED";
    private static final String APP_EVENT_PREFERENCES = "com.facebook.sdk.appEventPreferences";
    private static final int APP_SUPPORTS_ATTRIBUTION_ID_RECHECK_PERIOD_IN_SECONDS = 86400;
    private static final int FLUSH_PERIOD_IN_SECONDS = 60;
    private static final int NUM_LOG_EVENTS_TO_TRY_TO_FLUSH_AFTER = 100;
    /* access modifiers changed from: private */
    public static final String TAG = AppEventsLogger.class.getCanonicalName();
    private static Context applicationContext;
    private static FlushBehavior flushBehavior = FlushBehavior.AUTO;
    private static Timer flushTimer;
    private static boolean requestInFlight;
    /* access modifiers changed from: private */
    public static Map<AccessTokenAppIdPair, SessionEventsState> stateMap = new ConcurrentHashMap();
    /* access modifiers changed from: private */
    public static Object staticLock = new Object();
    private static Timer supportsAttributionRecheckTimer;
    private final AccessTokenAppIdPair accessTokenAppId;
    private final Context context;

    public enum FlushBehavior {
        AUTO,
        EXPLICIT_ONLY
    }

    private enum FlushReason {
        EXPLICIT,
        TIMER,
        SESSION_CHANGE,
        PERSISTED_EVENTS,
        EVENT_THRESHOLD,
        EAGER_FLUSHING_EVENT
    }

    private enum FlushResult {
        SUCCESS,
        SERVER_ERROR,
        NO_CONNECTIVITY,
        UNKNOWN_ERROR
    }

    private static class AccessTokenAppIdPair implements Serializable {
        private static final long serialVersionUID = 1;
        private final String accessToken;
        private final String applicationId;

        AccessTokenAppIdPair(Session session) {
            this(session.getAccessToken(), session.getApplicationId());
        }

        AccessTokenAppIdPair(String accessToken2, String applicationId2) {
            this.accessToken = Utility.isNullOrEmpty(accessToken2) ? null : accessToken2;
            this.applicationId = applicationId2;
        }

        /* access modifiers changed from: package-private */
        public String getAccessToken() {
            return this.accessToken;
        }

        /* access modifiers changed from: package-private */
        public String getApplicationId() {
            return this.applicationId;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.accessToken == null ? 0 : this.accessToken.hashCode();
            if (this.applicationId != null) {
                i = this.applicationId.hashCode();
            }
            return hashCode ^ i;
        }

        public boolean equals(Object o) {
            if (!(o instanceof AccessTokenAppIdPair)) {
                return false;
            }
            AccessTokenAppIdPair p = (AccessTokenAppIdPair) o;
            if (!Utility.areObjectsEqual(p.accessToken, this.accessToken) || !Utility.areObjectsEqual(p.applicationId, this.applicationId)) {
                return false;
            }
            return true;
        }

        private static class SerializationProxyV1 implements Serializable {
            private static final long serialVersionUID = -2488473066578201069L;
            private final String accessToken;
            private final String appId;

            private SerializationProxyV1(String accessToken2, String appId2) {
                this.accessToken = accessToken2;
                this.appId = appId2;
            }

            private Object readResolve() {
                return new AccessTokenAppIdPair(this.accessToken, this.appId);
            }
        }

        private Object writeReplace() {
            return new SerializationProxyV1(this.accessToken, this.applicationId);
        }
    }

    public static boolean getLimitEventUsage(Context context2) {
        return context2.getSharedPreferences(APP_EVENT_PREFERENCES, 0).getBoolean("limitEventUsage", false);
    }

    public static void setLimitEventUsage(Context context2, boolean limitEventUsage) {
        SharedPreferences.Editor editor = context2.getSharedPreferences(APP_EVENT_PREFERENCES, 0).edit();
        editor.putBoolean("limitEventUsage", limitEventUsage);
        editor.commit();
    }

    public static void activateApp(Context context2) {
        activateApp(context2, Utility.getMetadataApplicationId(context2));
    }

    public static void activateApp(Context context2, String applicationId) {
        if (context2 == null || applicationId == null) {
            throw new IllegalArgumentException("Both context and applicationId must be non-null");
        }
        Settings.publishInstallAsync(context2, applicationId);
        new AppEventsLogger(context2, applicationId, null).logEvent(AppEventsConstants.EVENT_NAME_ACTIVATED_APP);
    }

    public static AppEventsLogger newLogger(Context context2) {
        return new AppEventsLogger(context2, null, null);
    }

    public static AppEventsLogger newLogger(Context context2, Session session) {
        return new AppEventsLogger(context2, null, session);
    }

    public static AppEventsLogger newLogger(Context context2, String applicationId, Session session) {
        return new AppEventsLogger(context2, applicationId, session);
    }

    public static AppEventsLogger newLogger(Context context2, String applicationId) {
        return new AppEventsLogger(context2, applicationId, null);
    }

    public static FlushBehavior getFlushBehavior() {
        FlushBehavior flushBehavior2;
        synchronized (staticLock) {
            flushBehavior2 = flushBehavior;
        }
        return flushBehavior2;
    }

    public static void setFlushBehavior(FlushBehavior flushBehavior2) {
        synchronized (staticLock) {
            flushBehavior = flushBehavior2;
        }
    }

    public void logEvent(String eventName) {
        logEvent(eventName, (Bundle) null);
    }

    public void logEvent(String eventName, double valueToSum) {
        logEvent(eventName, valueToSum, (Bundle) null);
    }

    public void logEvent(String eventName, Bundle parameters) {
        logEvent(eventName, null, parameters, false);
    }

    public void logEvent(String eventName, double valueToSum, Bundle parameters) {
        logEvent(eventName, Double.valueOf(valueToSum), parameters, false);
    }

    public void logPurchase(BigDecimal purchaseAmount, Currency currency) {
        logPurchase(purchaseAmount, currency, null);
    }

    public void logPurchase(BigDecimal purchaseAmount, Currency currency, Bundle parameters) {
        if (purchaseAmount == null) {
            notifyDeveloperError("purchaseAmount cannot be null");
        } else if (currency == null) {
            notifyDeveloperError("currency cannot be null");
        } else {
            if (parameters == null) {
                parameters = new Bundle();
            }
            parameters.putString(AppEventsConstants.EVENT_PARAM_CURRENCY, currency.getCurrencyCode());
            logEvent(AppEventsConstants.EVENT_NAME_PURCHASED, purchaseAmount.doubleValue(), parameters);
            eagerFlush();
        }
    }

    public void flush() {
        flush(FlushReason.EXPLICIT);
    }

    public static void onContextStop() {
        PersistedEvents.persistEvents(applicationContext, stateMap);
    }

    /* access modifiers changed from: package-private */
    public boolean isValidForSession(Session session) {
        return this.accessTokenAppId.equals(new AccessTokenAppIdPair(session));
    }

    public void logSdkEvent(String eventName, Double valueToSum, Bundle parameters) {
        logEvent(eventName, valueToSum, parameters, true);
    }

    public String getApplicationId() {
        return this.accessTokenAppId.getApplicationId();
    }

    private AppEventsLogger(Context context2, String applicationId, Session session) {
        Validate.notNull(context2, "context");
        this.context = context2;
        session = session == null ? Session.getActiveSession() : session;
        if (session != null) {
            this.accessTokenAppId = new AccessTokenAppIdPair(session);
        } else {
            this.accessTokenAppId = new AccessTokenAppIdPair(null, applicationId == null ? Utility.getMetadataApplicationId(context2) : applicationId);
        }
        synchronized (staticLock) {
            if (applicationContext == null) {
                applicationContext = context2.getApplicationContext();
            }
        }
        initializeTimersIfNeeded();
    }

    private static void initializeTimersIfNeeded() {
        synchronized (staticLock) {
            if (flushTimer == null) {
                flushTimer = new Timer();
                supportsAttributionRecheckTimer = new Timer();
                flushTimer.schedule(new TimerTask() {
                    public void run() {
                        if (AppEventsLogger.getFlushBehavior() != FlushBehavior.EXPLICIT_ONLY) {
                            AppEventsLogger.flushAndWait(FlushReason.TIMER);
                        }
                    }
                }, 0, 60000);
                supportsAttributionRecheckTimer.schedule(new TimerTask() {
                    public void run() {
                        Set<String> applicationIds = new HashSet<>();
                        synchronized (AppEventsLogger.staticLock) {
                            for (AccessTokenAppIdPair accessTokenAppId : AppEventsLogger.stateMap.keySet()) {
                                applicationIds.add(accessTokenAppId.getApplicationId());
                            }
                        }
                        for (String applicationId : applicationIds) {
                            Utility.queryAppSettings(applicationId, true);
                        }
                    }
                }, 0, (long) DateUtils.MILLI_SEC_IN_DAY);
            }
        }
    }

    private void logEvent(String eventName, Double valueToSum, Bundle parameters, boolean isImplicitlyLogged) {
        logEvent(this.context, new AppEvent(eventName, valueToSum, parameters, isImplicitlyLogged), this.accessTokenAppId);
    }

    private static void logEvent(Context context2, AppEvent event, AccessTokenAppIdPair accessTokenAppId2) {
        getSessionEventsState(context2, accessTokenAppId2).addEvent(event);
        flushIfNecessary();
    }

    static void eagerFlush() {
        if (getFlushBehavior() != FlushBehavior.EXPLICIT_ONLY) {
            flush(FlushReason.EAGER_FLUSHING_EVENT);
        }
    }

    private static void flushIfNecessary() {
        synchronized (staticLock) {
            if (getFlushBehavior() != FlushBehavior.EXPLICIT_ONLY && getAccumulatedEventCount() > 100) {
                flush(FlushReason.EVENT_THRESHOLD);
            }
        }
    }

    private static int getAccumulatedEventCount() {
        int result;
        synchronized (staticLock) {
            result = 0;
            for (SessionEventsState state : stateMap.values()) {
                result += state.getAccumulatedEventCount();
            }
        }
        return result;
    }

    private static SessionEventsState getSessionEventsState(Context context2, AccessTokenAppIdPair accessTokenAppId2) {
        SessionEventsState state;
        synchronized (staticLock) {
            state = stateMap.get(accessTokenAppId2);
            if (state == null) {
                state = new SessionEventsState(Settings.getAttributionId(context2.getContentResolver()), context2.getPackageName());
                stateMap.put(accessTokenAppId2, state);
            }
        }
        return state;
    }

    private static SessionEventsState getSessionEventsState(AccessTokenAppIdPair accessTokenAppId2) {
        SessionEventsState sessionEventsState;
        synchronized (staticLock) {
            sessionEventsState = stateMap.get(accessTokenAppId2);
        }
        return sessionEventsState;
    }

    private static void flush(final FlushReason reason) {
        Settings.getExecutor().execute(new Runnable() {
            public void run() {
                AppEventsLogger.flushAndWait(reason);
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r1 = buildAndExecuteRequests(r7, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x004b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x004c, code lost:
        android.util.Log.d(com.facebook.AppEventsLogger.TAG, "Caught unexpected exception while flushing: " + r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0018, code lost:
        accumulatePersistedEvents();
        r1 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void flushAndWait(com.facebook.AppEventsLogger.FlushReason r7) {
        /*
            java.lang.Object r5 = com.facebook.AppEventsLogger.staticLock
            monitor-enter(r5)
            boolean r4 = com.facebook.AppEventsLogger.requestInFlight     // Catch:{ all -> 0x0048 }
            if (r4 == 0) goto L_0x0009
            monitor-exit(r5)     // Catch:{ all -> 0x0048 }
        L_0x0008:
            return
        L_0x0009:
            r4 = 1
            com.facebook.AppEventsLogger.requestInFlight = r4     // Catch:{ all -> 0x0048 }
            java.util.HashSet r3 = new java.util.HashSet     // Catch:{ all -> 0x0048 }
            java.util.Map<com.facebook.AppEventsLogger$AccessTokenAppIdPair, com.facebook.AppEventsLogger$SessionEventsState> r4 = com.facebook.AppEventsLogger.stateMap     // Catch:{ all -> 0x0048 }
            java.util.Set r4 = r4.keySet()     // Catch:{ all -> 0x0048 }
            r3.<init>(r4)     // Catch:{ all -> 0x0048 }
            monitor-exit(r5)     // Catch:{ all -> 0x0048 }
            accumulatePersistedEvents()
            r1 = 0
            com.facebook.AppEventsLogger$FlushStatistics r1 = buildAndExecuteRequests(r7, r3)     // Catch:{ Exception -> 0x004b }
        L_0x0020:
            java.lang.Object r5 = com.facebook.AppEventsLogger.staticLock
            monitor-enter(r5)
            r4 = 0
            com.facebook.AppEventsLogger.requestInFlight = r4     // Catch:{ all -> 0x0069 }
            monitor-exit(r5)     // Catch:{ all -> 0x0069 }
            if (r1 == 0) goto L_0x0008
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r4 = "com.facebook.sdk.APP_EVENTS_FLUSHED"
            r2.<init>(r4)
            java.lang.String r4 = "com.facebook.sdk.APP_EVENTS_NUM_EVENTS_FLUSHED"
            int r5 = r1.numEvents
            r2.putExtra(r4, r5)
            java.lang.String r4 = "com.facebook.sdk.APP_EVENTS_FLUSH_RESULT"
            com.facebook.AppEventsLogger$FlushResult r5 = r1.result
            r2.putExtra(r4, r5)
            android.content.Context r4 = com.facebook.AppEventsLogger.applicationContext
            android.support.v4.content.LocalBroadcastManager r4 = android.support.v4.content.LocalBroadcastManager.getInstance(r4)
            r4.sendBroadcast(r2)
            goto L_0x0008
        L_0x0048:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0048 }
            throw r4
        L_0x004b:
            r0 = move-exception
            java.lang.String r4 = com.facebook.AppEventsLogger.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Caught unexpected exception while flushing: "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = r0.toString()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            android.util.Log.d(r4, r5)
            goto L_0x0020
        L_0x0069:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0069 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.AppEventsLogger.flushAndWait(com.facebook.AppEventsLogger$FlushReason):void");
    }

    private static FlushStatistics buildAndExecuteRequests(FlushReason reason, Set<AccessTokenAppIdPair> keysToFlush) {
        Request request;
        FlushStatistics flushResults = new FlushStatistics();
        boolean limitEventUsage = getLimitEventUsage(applicationContext);
        List<Request> requestsToExecute = new ArrayList<>();
        for (AccessTokenAppIdPair accessTokenAppId2 : keysToFlush) {
            SessionEventsState sessionEventsState = getSessionEventsState(accessTokenAppId2);
            if (!(sessionEventsState == null || (request = buildRequestForSession(accessTokenAppId2, sessionEventsState, limitEventUsage, flushResults)) == null)) {
                requestsToExecute.add(request);
            }
        }
        if (requestsToExecute.size() <= 0) {
            return null;
        }
        Logger.log(LoggingBehavior.APP_EVENTS, TAG, "Flushing %d events due to %s.", Integer.valueOf(flushResults.numEvents), reason.toString());
        for (Request request2 : requestsToExecute) {
            request2.executeAndWait();
        }
        return flushResults;
    }

    private static class FlushStatistics {
        public int numEvents;
        public FlushResult result;

        private FlushStatistics() {
            this.numEvents = 0;
            this.result = FlushResult.SUCCESS;
        }
    }

    private static Request buildRequestForSession(final AccessTokenAppIdPair accessTokenAppId2, final SessionEventsState sessionEventsState, boolean limitEventUsage, final FlushStatistics flushState) {
        String applicationId = accessTokenAppId2.getApplicationId();
        Utility.FetchedAppSettings fetchedAppSettings = Utility.queryAppSettings(applicationId, false);
        final Request postRequest = Request.newPostRequest(null, String.format("%s/activities", applicationId), null, null);
        Bundle requestParameters = postRequest.getParameters();
        if (requestParameters == null) {
            requestParameters = new Bundle();
        }
        requestParameters.putString("access_token", accessTokenAppId2.getAccessToken());
        postRequest.setParameters(requestParameters);
        int numEvents = sessionEventsState.populateRequest(postRequest, fetchedAppSettings.supportsImplicitLogging(), fetchedAppSettings.supportsAttribution(), limitEventUsage);
        if (numEvents == 0) {
            return null;
        }
        flushState.numEvents += numEvents;
        postRequest.setCallback(new Request.Callback() {
            public void onCompleted(Response response) {
                AppEventsLogger.handleResponse(accessTokenAppId2, postRequest, response, sessionEventsState, flushState);
            }
        });
        return postRequest;
    }

    /* access modifiers changed from: private */
    public static void handleResponse(AccessTokenAppIdPair accessTokenAppId2, Request request, Response response, SessionEventsState sessionEventsState, FlushStatistics flushState) {
        String prettyPrintedEvents;
        FacebookRequestError error = response.getError();
        String resultDescription = "Success";
        FlushResult flushResult = FlushResult.SUCCESS;
        if (error != null) {
            if (error.getErrorCode() == -1) {
                resultDescription = "Failed: No Connectivity";
                flushResult = FlushResult.NO_CONNECTIVITY;
            } else {
                resultDescription = String.format("Failed:\n  Response: %s\n  Error %s", response.toString(), error.toString());
                flushResult = FlushResult.SERVER_ERROR;
            }
        }
        if (Settings.isLoggingBehaviorEnabled(LoggingBehavior.APP_EVENTS)) {
            try {
                prettyPrintedEvents = new JSONArray((String) request.getTag()).toString(2);
            } catch (JSONException e) {
                prettyPrintedEvents = "<Can't encode events for debug logging>";
            }
            Logger.log(LoggingBehavior.APP_EVENTS, TAG, "Flush completed\nParams: %s\n  Result: %s\n  Events JSON: %s", request.getGraphObject().toString(), resultDescription, prettyPrintedEvents);
        }
        sessionEventsState.clearInFlightAndStats(error != null);
        if (flushResult == FlushResult.NO_CONNECTIVITY) {
            PersistedEvents.persistEvents(applicationContext, accessTokenAppId2, sessionEventsState);
        }
        if (flushResult != FlushResult.SUCCESS && flushState.result != FlushResult.NO_CONNECTIVITY) {
            flushState.result = flushResult;
        }
    }

    private static int accumulatePersistedEvents() {
        PersistedEvents persistedEvents = PersistedEvents.readAndClearStore(applicationContext);
        int result = 0;
        for (AccessTokenAppIdPair accessTokenAppId2 : persistedEvents.keySet()) {
            SessionEventsState sessionEventsState = getSessionEventsState(applicationContext, accessTokenAppId2);
            List<AppEvent> events = persistedEvents.getEvents(accessTokenAppId2);
            sessionEventsState.accumulatePersistedEvents(events);
            result += events.size();
        }
        return result;
    }

    private static void notifyDeveloperError(String message) {
        Logger.log(LoggingBehavior.DEVELOPER_ERRORS, "AppEvents", message);
    }

    static class SessionEventsState {
        public static final String ENCODED_EVENTS_KEY = "encoded_events";
        public static final String EVENT_COUNT_KEY = "event_count";
        public static final String NUM_SKIPPED_KEY = "num_skipped";
        private final int MAX_ACCUMULATED_LOG_EVENTS = PlacePickerFragment.DEFAULT_RADIUS_IN_METERS;
        private List<AppEvent> accumulatedEvents = new ArrayList();
        private String attributionId;
        private List<AppEvent> inFlightEvents = new ArrayList();
        private int numSkippedEventsDueToFullBuffer;
        private String packageName;

        public SessionEventsState(String attributionId2, String packageName2) {
            this.attributionId = attributionId2;
            this.packageName = packageName2;
        }

        public synchronized void addEvent(AppEvent event) {
            if (this.accumulatedEvents.size() + this.inFlightEvents.size() >= 1000) {
                this.numSkippedEventsDueToFullBuffer++;
            } else {
                this.accumulatedEvents.add(event);
            }
        }

        public synchronized int getAccumulatedEventCount() {
            return this.accumulatedEvents.size();
        }

        public synchronized void clearInFlightAndStats(boolean moveToAccumulated) {
            if (moveToAccumulated) {
                this.accumulatedEvents.addAll(this.inFlightEvents);
            }
            this.inFlightEvents.clear();
            this.numSkippedEventsDueToFullBuffer = 0;
        }

        public int populateRequest(Request request, boolean includeImplicitEvents, boolean includeAttribution, boolean limitEventUsage) {
            synchronized (this) {
                int numSkipped = this.numSkippedEventsDueToFullBuffer;
                this.inFlightEvents.addAll(this.accumulatedEvents);
                this.accumulatedEvents.clear();
                JSONArray jsonArray = new JSONArray();
                for (AppEvent event : this.inFlightEvents) {
                    if (includeImplicitEvents || !event.getIsImplicit()) {
                        jsonArray.put(event.getJSONObject());
                    }
                }
                if (jsonArray.length() == 0) {
                    return 0;
                }
                populateRequest(request, numSkipped, jsonArray, includeAttribution, limitEventUsage);
                return jsonArray.length();
            }
        }

        public synchronized List<AppEvent> getEventsToPersist() {
            List<AppEvent> result;
            result = this.accumulatedEvents;
            this.accumulatedEvents = new ArrayList();
            return result;
        }

        public synchronized void accumulatePersistedEvents(List<AppEvent> events) {
            this.accumulatedEvents.addAll(events);
        }

        private void populateRequest(Request request, int numSkipped, JSONArray events, boolean includeAttribution, boolean limitEventUsage) {
            GraphObject publishParams = GraphObject.Factory.create();
            publishParams.setProperty(ModelFields.EVENT, "CUSTOM_APP_EVENTS");
            if (this.numSkippedEventsDueToFullBuffer > 0) {
                publishParams.setProperty("num_skipped_events", Integer.valueOf(numSkipped));
            }
            if (includeAttribution && this.attributionId != null) {
                publishParams.setProperty("attribution", this.attributionId);
            }
            publishParams.setProperty("application_tracking_enabled", Boolean.valueOf(!limitEventUsage));
            publishParams.setProperty("application_package_name", this.packageName);
            request.setGraphObject(publishParams);
            Bundle requestParameters = request.getParameters();
            if (requestParameters == null) {
                requestParameters = new Bundle();
            }
            String jsonString = events.toString();
            if (jsonString != null) {
                requestParameters.putByteArray("custom_events_file", getStringAsByteArray(jsonString));
                request.setTag(jsonString);
            }
            request.setParameters(requestParameters);
        }

        private byte[] getStringAsByteArray(String jsonString) {
            try {
                return jsonString.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                Utility.logd("Encoding exception: ", e);
                return null;
            }
        }
    }

    static class AppEvent implements Serializable {
        private static final long serialVersionUID = 1;
        private static final HashSet<String> validatedIdentifiers = new HashSet<>();
        private boolean isImplicit;
        private JSONObject jsonObject;

        public AppEvent(String eventName, Double valueToSum, Bundle parameters, boolean isImplicitlyLogged) {
            validateIdentifier(eventName);
            this.isImplicit = isImplicitlyLogged;
            this.jsonObject = new JSONObject();
            try {
                this.jsonObject.put("_eventName", eventName);
                this.jsonObject.put("_logTime", System.currentTimeMillis() / 1000);
                if (valueToSum != null) {
                    this.jsonObject.put("_valueToSum", valueToSum.doubleValue());
                }
                if (this.isImplicit) {
                    this.jsonObject.put("_implicitlyLogged", "1");
                }
                String appVersion = Settings.getAppVersion();
                if (appVersion != null) {
                    this.jsonObject.put("_appVersion", appVersion);
                }
                if (parameters != null) {
                    for (String key : parameters.keySet()) {
                        validateIdentifier(key);
                        Object value = parameters.get(key);
                        if ((value instanceof String) || (value instanceof Number)) {
                            this.jsonObject.put(key, value.toString());
                        } else {
                            throw new FacebookException(String.format("Parameter value '%s' for key '%s' should be a string or a numeric type.", value, key));
                        }
                    }
                }
                if (!this.isImplicit) {
                    Logger.log(LoggingBehavior.APP_EVENTS, "AppEvents", "Created app event '%s'", this.jsonObject.toString());
                }
            } catch (JSONException jsonException) {
                Logger.log(LoggingBehavior.APP_EVENTS, "AppEvents", "JSON encoding for app event failed: '%s'", jsonException.toString());
                this.jsonObject = null;
            }
        }

        private AppEvent(String jsonString, boolean isImplicit2) throws JSONException {
            this.jsonObject = new JSONObject(jsonString);
            this.isImplicit = isImplicit2;
        }

        public boolean getIsImplicit() {
            return this.isImplicit;
        }

        public JSONObject getJSONObject() {
            return this.jsonObject;
        }

        private void validateIdentifier(String identifier) {
            boolean alreadyValidated;
            if (identifier == null || identifier.length() == 0 || identifier.length() > 40) {
                if (identifier == null) {
                    identifier = "<None Provided>";
                }
                throw new FacebookException(String.format("Identifier '%s' must be less than %d characters", identifier, 40));
            }
            synchronized (validatedIdentifiers) {
                alreadyValidated = validatedIdentifiers.contains(identifier);
            }
            if (alreadyValidated) {
                return;
            }
            if (identifier.matches("^[0-9a-zA-Z_]+[0-9a-zA-Z _-]*$")) {
                synchronized (validatedIdentifiers) {
                    validatedIdentifiers.add(identifier);
                }
                return;
            }
            throw new FacebookException(String.format("Skipping event named '%s' due to illegal name - must be under 40 chars and alphanumeric, _, - or space, and not start with a space or hyphen.", identifier));
        }

        private static class SerializationProxyV1 implements Serializable {
            private static final long serialVersionUID = -2488473066578201069L;
            private final boolean isImplicit;
            private final String jsonString;

            private SerializationProxyV1(String jsonString2, boolean isImplicit2) {
                this.jsonString = jsonString2;
                this.isImplicit = isImplicit2;
            }

            private Object readResolve() throws JSONException {
                return new AppEvent(this.jsonString, this.isImplicit);
            }
        }

        private Object writeReplace() {
            return new SerializationProxyV1(this.jsonObject.toString(), this.isImplicit);
        }

        public String toString() {
            return String.format("\"%s\", implicit: %b, json: %s", this.jsonObject.optString("_eventName"), Boolean.valueOf(this.isImplicit), this.jsonObject.toString());
        }
    }

    static class PersistedEvents {
        static final String PERSISTED_EVENTS_FILENAME = "AppEventsLogger.persistedevents";
        private static Object staticLock = new Object();
        private Context context;
        private HashMap<AccessTokenAppIdPair, List<AppEvent>> persistedEvents = new HashMap<>();

        private PersistedEvents(Context context2) {
            this.context = context2;
        }

        public static PersistedEvents readAndClearStore(Context context2) {
            PersistedEvents persistedEvents2;
            synchronized (staticLock) {
                persistedEvents2 = new PersistedEvents(context2);
                persistedEvents2.readAndClearStore();
            }
            return persistedEvents2;
        }

        public static void persistEvents(Context context2, AccessTokenAppIdPair accessTokenAppId, SessionEventsState eventsToPersist) {
            Map<AccessTokenAppIdPair, SessionEventsState> map = new HashMap<>();
            map.put(accessTokenAppId, eventsToPersist);
            persistEvents(context2, map);
        }

        public static void persistEvents(Context context2, Map<AccessTokenAppIdPair, SessionEventsState> eventsToPersist) {
            synchronized (staticLock) {
                PersistedEvents persistedEvents2 = readAndClearStore(context2);
                for (Map.Entry<AccessTokenAppIdPair, SessionEventsState> entry : eventsToPersist.entrySet()) {
                    List<AppEvent> events = ((SessionEventsState) entry.getValue()).getEventsToPersist();
                    if (events.size() != 0) {
                        persistedEvents2.addEvents((AccessTokenAppIdPair) entry.getKey(), events);
                    }
                }
                persistedEvents2.write();
            }
        }

        public Set<AccessTokenAppIdPair> keySet() {
            return this.persistedEvents.keySet();
        }

        public List<AppEvent> getEvents(AccessTokenAppIdPair accessTokenAppId) {
            return this.persistedEvents.get(accessTokenAppId);
        }

        private void write() {
            ObjectOutputStream oos = null;
            try {
                ObjectOutputStream oos2 = new ObjectOutputStream(new BufferedOutputStream(this.context.openFileOutput(PERSISTED_EVENTS_FILENAME, 0)));
                try {
                    oos2.writeObject(this.persistedEvents);
                    Utility.closeQuietly(oos2);
                } catch (Exception e) {
                    e = e;
                    oos = oos2;
                    try {
                        Log.d(AppEventsLogger.TAG, "Got unexpected exception: " + e.toString());
                        Utility.closeQuietly(oos);
                    } catch (Throwable th) {
                        th = th;
                        Utility.closeQuietly(oos);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    oos = oos2;
                    Utility.closeQuietly(oos);
                    throw th;
                }
            } catch (Exception e2) {
                e = e2;
                Log.d(AppEventsLogger.TAG, "Got unexpected exception: " + e.toString());
                Utility.closeQuietly(oos);
            }
        }

        private void readAndClearStore() {
            ObjectInputStream ois = null;
            try {
                ObjectInputStream ois2 = new ObjectInputStream(new BufferedInputStream(this.context.openFileInput(PERSISTED_EVENTS_FILENAME)));
                try {
                    this.context.getFileStreamPath(PERSISTED_EVENTS_FILENAME).delete();
                    this.persistedEvents = (HashMap) ois2.readObject();
                    Utility.closeQuietly(ois2);
                } catch (FileNotFoundException e) {
                    ois = ois2;
                    Utility.closeQuietly(ois);
                } catch (Exception e2) {
                    e = e2;
                    ois = ois2;
                    try {
                        Log.d(AppEventsLogger.TAG, "Got unexpected exception: " + e.toString());
                        Utility.closeQuietly(ois);
                    } catch (Throwable th) {
                        th = th;
                        Utility.closeQuietly(ois);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    ois = ois2;
                    Utility.closeQuietly(ois);
                    throw th;
                }
            } catch (FileNotFoundException e3) {
                Utility.closeQuietly(ois);
            } catch (Exception e4) {
                e = e4;
                Log.d(AppEventsLogger.TAG, "Got unexpected exception: " + e.toString());
                Utility.closeQuietly(ois);
            }
        }

        public void addEvents(AccessTokenAppIdPair accessTokenAppId, List<AppEvent> eventsToPersist) {
            if (!this.persistedEvents.containsKey(accessTokenAppId)) {
                this.persistedEvents.put(accessTokenAppId, new ArrayList());
            }
            this.persistedEvents.get(accessTokenAppId).addAll(eventsToPersist);
        }
    }
}
