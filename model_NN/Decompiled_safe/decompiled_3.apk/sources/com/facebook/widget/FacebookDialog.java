package com.facebook.widget;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import com.biznessapps.constants.ServerConstants;
import com.facebook.FacebookException;
import com.facebook.FacebookGraphObjectException;
import com.facebook.NativeAppCallAttachmentStore;
import com.facebook.NativeAppCallContentProvider;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FacebookDialog {
    public static final String COMPLETION_GESTURE_CANCEL = "cancel";
    private static final String EXTRA_DIALOG_COMPLETE_KEY = "com.facebook.platform.extra.DID_COMPLETE";
    private static final String EXTRA_DIALOG_COMPLETION_GESTURE_KEY = "com.facebook.platform.extra.COMPLETION_GESTURE";
    private static final String EXTRA_DIALOG_COMPLETION_ID_KEY = "com.facebook.platform.extra.POST_ID";
    private static final int MIN_NATIVE_SHARE_PROTOCOL_VERSION = 20130618;
    private static NativeAppCallAttachmentStore attachmentStore;
    private Activity activity;
    private PendingCall appCall;
    private Fragment fragment;
    private OnPresentCallback onPresentCallback;

    public interface Callback {
        void onComplete(PendingCall pendingCall, Bundle bundle);

        void onError(PendingCall pendingCall, Exception exc, Bundle bundle);
    }

    private interface DialogFeature {
        int getMinVersion();
    }

    interface OnPresentCallback {
        void onPresent(Context context) throws Exception;
    }

    public enum ShareDialogFeature implements DialogFeature {
        SHARE_DIALOG(20130618);
        
        private int minVersion;

        private ShareDialogFeature(int minVersion2) {
            this.minVersion = minVersion2;
        }

        public int getMinVersion() {
            return this.minVersion;
        }
    }

    public enum OpenGraphActionDialogFeature implements DialogFeature {
        OG_ACTION_DIALOG(20130618);
        
        private int minVersion;

        private OpenGraphActionDialogFeature(int minVersion2) {
            this.minVersion = minVersion2;
        }

        public int getMinVersion() {
            return this.minVersion;
        }
    }

    public static boolean getNativeDialogDidComplete(Bundle result) {
        return result.getBoolean(EXTRA_DIALOG_COMPLETE_KEY, false);
    }

    public static String getNativeDialogCompletionGesture(Bundle result) {
        return result.getString(EXTRA_DIALOG_COMPLETION_GESTURE_KEY);
    }

    public static String getNativeDialogPostId(Bundle result) {
        return result.getString(EXTRA_DIALOG_COMPLETION_ID_KEY);
    }

    private FacebookDialog(Activity activity2, Fragment fragment2, PendingCall appCall2, OnPresentCallback onPresentCallback2) {
        this.activity = activity2;
        this.fragment = fragment2;
        this.appCall = appCall2;
        this.onPresentCallback = onPresentCallback2;
    }

    public PendingCall present() {
        if (this.onPresentCallback != null) {
            try {
                this.onPresentCallback.onPresent(this.activity);
            } catch (Exception e) {
                throw new FacebookException(e);
            }
        }
        if (this.fragment != null) {
            this.fragment.startActivityForResult(this.appCall.getRequestIntent(), this.appCall.getRequestCode());
        } else {
            this.activity.startActivityForResult(this.appCall.getRequestIntent(), this.appCall.getRequestCode());
        }
        return this.appCall;
    }

    public static boolean handleActivityResult(Context context, PendingCall appCall2, int requestCode, Intent data, Callback callback) {
        if (requestCode != appCall2.getRequestCode()) {
            return false;
        }
        if (attachmentStore != null) {
            attachmentStore.cleanupAttachmentsForCall(context, appCall2.getCallId());
        }
        if (callback != null) {
            if (NativeProtocol.isErrorResult(data)) {
                callback.onError(appCall2, NativeProtocol.getErrorFromResult(data), data.getExtras());
            } else {
                callback.onComplete(appCall2, data.getExtras());
            }
        }
        return true;
    }

    public static boolean canPresentShareDialog(Context context, ShareDialogFeature... features) {
        return handleCanPresent(context, EnumSet.of(ShareDialogFeature.SHARE_DIALOG, features));
    }

    public static boolean canPresentOpenGraphActionDialog(Context context, OpenGraphActionDialogFeature... features) {
        return handleCanPresent(context, EnumSet.of(OpenGraphActionDialogFeature.OG_ACTION_DIALOG, features));
    }

    private static boolean handleCanPresent(Context context, Iterable<? extends DialogFeature> features) {
        return getProtocolVersionForNativeDialog(context, Integer.valueOf(getMinVersionForFeatures(features))) != -1;
    }

    /* access modifiers changed from: private */
    public static int getProtocolVersionForNativeDialog(Context context, Integer requiredVersion) {
        return NativeProtocol.getLatestAvailableProtocolVersion(context, requiredVersion.intValue());
    }

    /* access modifiers changed from: private */
    public static NativeAppCallAttachmentStore getAttachmentStore() {
        if (attachmentStore == null) {
            attachmentStore = new NativeAppCallAttachmentStore();
        }
        return attachmentStore;
    }

    private static int getMinVersionForFeatures(Iterable<? extends DialogFeature> features) {
        int minVersion = Integer.MIN_VALUE;
        for (DialogFeature feature : features) {
            minVersion = Math.max(minVersion, feature.getMinVersion());
        }
        return minVersion;
    }

    private static abstract class Builder<CONCRETE extends Builder<?>> {
        protected final Activity activity;
        protected final PendingCall appCall = new PendingCall((int) NativeProtocol.DIALOG_REQUEST_CODE);
        protected final String applicationId;
        protected String applicationName;
        protected Fragment fragment;

        /* access modifiers changed from: package-private */
        public abstract Intent handleBuild(Bundle bundle);

        Builder(Activity activity2) {
            Validate.notNull(activity2, "activity");
            this.activity = activity2;
            this.applicationId = Utility.getMetadataApplicationId(activity2);
        }

        public CONCRETE setRequestCode(int requestCode) {
            this.appCall.setRequestCode(requestCode);
            return this;
        }

        public CONCRETE setApplicationName(String applicationName2) {
            this.applicationName = applicationName2;
            return this;
        }

        public CONCRETE setFragment(Fragment fragment2) {
            this.fragment = fragment2;
            return this;
        }

        public FacebookDialog build() {
            validate();
            Bundle extras = new Bundle();
            putExtra(extras, NativeProtocol.EXTRA_APPLICATION_ID, this.applicationId);
            putExtra(extras, NativeProtocol.EXTRA_APPLICATION_NAME, this.applicationName);
            Intent intent = handleBuild(extras);
            if (intent == null) {
                throw new FacebookException("Unable to create Intent; this likely means the Facebook app is not installed.");
            }
            this.appCall.setRequestIntent(intent);
            return new FacebookDialog(this.activity, this.fragment, this.appCall, getOnPresentCallback());
        }

        public boolean canPresent() {
            return handleCanPresent();
        }

        /* access modifiers changed from: package-private */
        public boolean handleCanPresent() {
            return FacebookDialog.getProtocolVersionForNativeDialog(this.activity, 20130618) != -1;
        }

        /* access modifiers changed from: package-private */
        public void validate() {
        }

        /* access modifiers changed from: package-private */
        public OnPresentCallback getOnPresentCallback() {
            return null;
        }

        /* access modifiers changed from: package-private */
        public void putExtra(Bundle extras, String key, String value) {
            if (value != null) {
                extras.putString(key, value);
            }
        }
    }

    public static class ShareDialogBuilder extends Builder<ShareDialogBuilder> {
        private String caption;
        private boolean dataErrorsFatal;
        private String description;
        private ArrayList<String> friends;
        private String link;
        private String name;
        private String picture;
        private String place;
        private String ref;

        public /* bridge */ /* synthetic */ FacebookDialog build() {
            return super.build();
        }

        public /* bridge */ /* synthetic */ boolean canPresent() {
            return super.canPresent();
        }

        public ShareDialogBuilder(Activity activity) {
            super(activity);
        }

        public ShareDialogBuilder setName(String name2) {
            this.name = name2;
            return this;
        }

        public ShareDialogBuilder setCaption(String caption2) {
            this.caption = caption2;
            return this;
        }

        public ShareDialogBuilder setDescription(String description2) {
            this.description = description2;
            return this;
        }

        public ShareDialogBuilder setLink(String link2) {
            this.link = link2;
            return this;
        }

        public ShareDialogBuilder setPicture(String picture2) {
            this.picture = picture2;
            return this;
        }

        public ShareDialogBuilder setPlace(String place2) {
            this.place = place2;
            return this;
        }

        public ShareDialogBuilder setFriends(List<String> friends2) {
            this.friends = new ArrayList<>(friends2);
            return this;
        }

        public ShareDialogBuilder setRef(String ref2) {
            this.ref = ref2;
            return this;
        }

        public ShareDialogBuilder setDataErrorsFatal(boolean dataErrorsFatal2) {
            this.dataErrorsFatal = dataErrorsFatal2;
            return this;
        }

        /* access modifiers changed from: package-private */
        public boolean handleCanPresent() {
            return FacebookDialog.canPresentShareDialog(this.activity, ShareDialogFeature.SHARE_DIALOG);
        }

        /* access modifiers changed from: package-private */
        public Intent handleBuild(Bundle extras) {
            putExtra(extras, NativeProtocol.EXTRA_APPLICATION_ID, this.applicationId);
            putExtra(extras, NativeProtocol.EXTRA_APPLICATION_NAME, this.applicationName);
            putExtra(extras, NativeProtocol.EXTRA_TITLE, this.name);
            putExtra(extras, NativeProtocol.EXTRA_SUBTITLE, this.caption);
            putExtra(extras, NativeProtocol.EXTRA_DESCRIPTION, this.description);
            putExtra(extras, NativeProtocol.EXTRA_LINK, this.link);
            putExtra(extras, NativeProtocol.EXTRA_IMAGE, this.picture);
            putExtra(extras, NativeProtocol.EXTRA_PLACE_TAG, this.place);
            putExtra(extras, NativeProtocol.EXTRA_TITLE, this.name);
            putExtra(extras, NativeProtocol.EXTRA_REF, this.ref);
            extras.putBoolean(NativeProtocol.EXTRA_DATA_FAILURES_FATAL, this.dataErrorsFatal);
            if (!Utility.isNullOrEmpty(this.friends)) {
                extras.putStringArrayList(NativeProtocol.EXTRA_FRIEND_TAGS, this.friends);
            }
            return NativeProtocol.createPlatformActivityIntent(this.activity, NativeProtocol.ACTION_FEED_DIALOG, FacebookDialog.getProtocolVersionForNativeDialog(this.activity, 20130618), extras);
        }
    }

    public static class OpenGraphActionDialogBuilder extends Builder<OpenGraphActionDialogBuilder> {
        private OpenGraphAction action;
        private String actionType;
        private boolean dataErrorsFatal;
        /* access modifiers changed from: private */
        public HashMap<String, Bitmap> imageAttachments;
        private String previewPropertyName;

        public /* bridge */ /* synthetic */ FacebookDialog build() {
            return super.build();
        }

        public /* bridge */ /* synthetic */ boolean canPresent() {
            return super.canPresent();
        }

        public OpenGraphActionDialogBuilder(Activity activity, OpenGraphAction action2, String actionType2, String previewPropertyName2) {
            super(activity);
            Validate.notNull(action2, "action");
            Validate.notNullOrEmpty(actionType2, "actionType");
            Validate.notNullOrEmpty(previewPropertyName2, "previewPropertyName");
            if (action2.getProperty(previewPropertyName2) == null) {
                throw new IllegalArgumentException("A property named \"" + previewPropertyName2 + "\" was not found on the action.  The name of " + "the preview property must match the name of an action property.");
            }
            this.action = action2;
            this.actionType = actionType2;
            this.previewPropertyName = previewPropertyName2;
        }

        public OpenGraphActionDialogBuilder setDataErrorsFatal(boolean dataErrorsFatal2) {
            this.dataErrorsFatal = dataErrorsFatal2;
            return this;
        }

        public OpenGraphActionDialogBuilder setImageAttachmentsForAction(List<Bitmap> bitmaps) {
            return setImageAttachmentsForAction(bitmaps, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
         arg types: [java.lang.String, int]
         candidates:
          ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
          ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
        public OpenGraphActionDialogBuilder setImageAttachmentsForAction(List<Bitmap> bitmaps, boolean isUserGenerated) {
            Validate.containsNoNulls(bitmaps, "bitmaps");
            if (this.action == null) {
                throw new FacebookException("Can not set attachments prior to setting action.");
            }
            List<String> attachmentUrls = addImageAttachments(bitmaps);
            if (isUserGenerated) {
                List<JSONObject> attachments = new ArrayList<>(attachmentUrls.size());
                for (String url : attachmentUrls) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(NativeProtocol.IMAGE_URL_KEY, url);
                        jsonObject.put(NativeProtocol.IMAGE_USER_GENERATED_KEY, true);
                        attachments.add(jsonObject);
                    } catch (JSONException e) {
                        throw new FacebookException("Unable to attach images", e);
                    }
                }
                this.action.setImage(attachments);
            } else {
                this.action.setImageUrls(attachmentUrls);
            }
            return this;
        }

        public OpenGraphActionDialogBuilder setImageAttachmentsForObject(String objectProperty, List<Bitmap> bitmaps) {
            return setImageAttachmentsForObject(objectProperty, bitmaps, false);
        }

        public OpenGraphActionDialogBuilder setImageAttachmentsForObject(String objectProperty, List<Bitmap> bitmaps, boolean isUserGenerated) {
            Validate.notNull(objectProperty, "objectProperty");
            Validate.containsNoNulls(bitmaps, "bitmaps");
            if (this.action == null) {
                throw new FacebookException("Can not set attachments prior to setting action.");
            }
            try {
                OpenGraphObject object = (OpenGraphObject) this.action.getPropertyAs(objectProperty, OpenGraphObject.class);
                if (object == null) {
                    throw new IllegalArgumentException("Action does not contain a property" + objectProperty);
                }
                List<String> attachmentUrls = addImageAttachments(bitmaps);
                if (isUserGenerated) {
                    GraphObjectList<GraphObject> attachments = GraphObject.Factory.createList(GraphObject.class);
                    for (String url : attachmentUrls) {
                        GraphObject graphObject = GraphObject.Factory.create();
                        graphObject.setProperty(NativeProtocol.IMAGE_URL_KEY, url);
                        graphObject.setProperty(NativeProtocol.IMAGE_USER_GENERATED_KEY, true);
                        attachments.add(graphObject);
                    }
                    object.setImage(attachments);
                } else {
                    object.setImageUrls(attachmentUrls);
                }
                return this;
            } catch (FacebookGraphObjectException e) {
                throw new IllegalArgumentException("Property " + objectProperty + " is not a graph object.");
            }
        }

        private List<String> addImageAttachments(List<Bitmap> bitmaps) {
            ArrayList<String> attachmentUrls = new ArrayList<>();
            for (Bitmap bitmap : bitmaps) {
                String attachmentName = UUID.randomUUID().toString();
                addImageAttachment(attachmentName, bitmap);
                attachmentUrls.add(NativeAppCallContentProvider.getAttachmentUrl(this.applicationId, this.appCall.getCallId(), attachmentName));
            }
            return attachmentUrls;
        }

        /* access modifiers changed from: package-private */
        public List<String> getImageAttachmentNames() {
            return new ArrayList(this.imageAttachments.keySet());
        }

        /* access modifiers changed from: package-private */
        public boolean handleCanPresent() {
            return FacebookDialog.canPresentOpenGraphActionDialog(this.activity, OpenGraphActionDialogFeature.OG_ACTION_DIALOG);
        }

        /* access modifiers changed from: package-private */
        public Intent handleBuild(Bundle extras) {
            putExtra(extras, NativeProtocol.EXTRA_PREVIEW_PROPERTY_NAME, this.previewPropertyName);
            putExtra(extras, NativeProtocol.EXTRA_ACTION_TYPE, this.actionType);
            extras.putBoolean(NativeProtocol.EXTRA_DATA_FAILURES_FATAL, this.dataErrorsFatal);
            putExtra(extras, NativeProtocol.EXTRA_ACTION, flattenChildrenOfGraphObject(this.action.getInnerJSONObject()).toString());
            return NativeProtocol.createPlatformActivityIntent(this.activity, NativeProtocol.ACTION_OGACTIONPUBLISH_DIALOG, FacebookDialog.getProtocolVersionForNativeDialog(this.activity, 20130618), extras);
        }

        /* access modifiers changed from: package-private */
        public OnPresentCallback getOnPresentCallback() {
            return new OnPresentCallback() {
                public void onPresent(Context context) throws Exception {
                    if (OpenGraphActionDialogBuilder.this.imageAttachments != null && OpenGraphActionDialogBuilder.this.imageAttachments.size() > 0) {
                        FacebookDialog.getAttachmentStore().addAttachmentsForCall(context, OpenGraphActionDialogBuilder.this.appCall.getCallId(), OpenGraphActionDialogBuilder.this.imageAttachments);
                    }
                }
            };
        }

        private OpenGraphActionDialogBuilder addImageAttachment(String imageName, Bitmap bitmap) {
            if (this.imageAttachments == null) {
                this.imageAttachments = new HashMap<>();
            }
            this.imageAttachments.put(imageName, bitmap);
            return this;
        }

        private JSONObject flattenChildrenOfGraphObject(JSONObject graphObject) {
            try {
                JSONObject graphObject2 = new JSONObject(graphObject.toString());
                try {
                    Iterator<String> keys = graphObject2.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        if (!key.equalsIgnoreCase(ServerConstants.POST_IMAGE_PARAM)) {
                            graphObject2.put(key, flattenObject(graphObject2.get(key)));
                        }
                    }
                    return graphObject2;
                } catch (JSONException e) {
                    e = e;
                    throw new FacebookException(e);
                }
            } catch (JSONException e2) {
                e = e2;
                throw new FacebookException(e);
            }
        }

        private Object flattenObject(Object object) throws JSONException {
            if (object == null) {
                return null;
            }
            if (object instanceof JSONObject) {
                JSONObject jsonObject = (JSONObject) object;
                if (jsonObject.optBoolean(NativeProtocol.OPEN_GRAPH_CREATE_OBJECT_KEY)) {
                    return object;
                }
                if (jsonObject.has("id")) {
                    return jsonObject.getString("id");
                }
                if (jsonObject.has(NativeProtocol.IMAGE_URL_KEY)) {
                    return jsonObject.getString(NativeProtocol.IMAGE_URL_KEY);
                }
                return object;
            } else if (!(object instanceof JSONArray)) {
                return object;
            } else {
                JSONArray jsonArray = (JSONArray) object;
                JSONArray newArray = new JSONArray();
                int length = jsonArray.length();
                for (int i = 0; i < length; i++) {
                    newArray.put(flattenObject(jsonArray.get(i)));
                }
                return newArray;
            }
        }
    }

    public static class PendingCall implements Parcelable {
        public static final Parcelable.Creator<PendingCall> CREATOR = new Parcelable.Creator<PendingCall>() {
            public PendingCall createFromParcel(Parcel in) {
                return new PendingCall(in);
            }

            public PendingCall[] newArray(int size) {
                return new PendingCall[size];
            }
        };
        private UUID callId;
        private int requestCode;
        private Intent requestIntent;

        public PendingCall(int requestCode2) {
            this.callId = UUID.randomUUID();
            this.requestCode = requestCode2;
        }

        private PendingCall(Parcel in) {
            this.callId = UUID.fromString(in.readString());
            this.requestIntent = (Intent) in.readParcelable(null);
            this.requestCode = in.readInt();
        }

        /* access modifiers changed from: private */
        public void setRequestIntent(Intent requestIntent2) {
            this.requestIntent = requestIntent2;
            this.requestIntent.putExtra(NativeProtocol.EXTRA_PROTOCOL_CALL_ID, this.callId.toString());
        }

        public Intent getRequestIntent() {
            return this.requestIntent;
        }

        public UUID getCallId() {
            return this.callId;
        }

        /* access modifiers changed from: private */
        public void setRequestCode(int requestCode2) {
            this.requestCode = requestCode2;
        }

        public int getRequestCode() {
            return this.requestCode;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.callId.toString());
            parcel.writeParcelable(this.requestIntent, 0);
            parcel.writeInt(this.requestCode);
        }
    }
}
