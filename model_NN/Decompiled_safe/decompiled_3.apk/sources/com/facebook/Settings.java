package com.facebook;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.facebook.Request;
import com.facebook.internal.Utility;
import com.facebook.internal.Validate;
import com.facebook.model.GraphObject;
import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public final class Settings {
    private static final String ANALYTICS_EVENT = "event";
    private static final String ATTRIBUTION_ID_COLUMN_NAME = "aid";
    private static final Uri ATTRIBUTION_ID_CONTENT_URI = Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider");
    private static final String ATTRIBUTION_KEY = "attribution";
    private static final String ATTRIBUTION_PREFERENCES = "com.facebook.sdk.attributionTracking";
    private static final String AUTO_PUBLISH = "auto_publish";
    private static final int DEFAULT_CORE_POOL_SIZE = 5;
    private static final int DEFAULT_KEEP_ALIVE = 1;
    private static final int DEFAULT_MAXIMUM_POOL_SIZE = 128;
    private static final ThreadFactory DEFAULT_THREAD_FACTORY = new ThreadFactory() {
        private final AtomicInteger counter = new AtomicInteger(0);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "FacebookSdk #" + this.counter.incrementAndGet());
        }
    };
    private static final BlockingQueue<Runnable> DEFAULT_WORK_QUEUE = new LinkedBlockingQueue(10);
    private static final String FACEBOOK_COM = "facebook.com";
    private static final Object LOCK = new Object();
    private static final String MOBILE_INSTALL_EVENT = "MOBILE_APP_INSTALL";
    private static final String PUBLISH_ACTIVITY_PATH = "%s/activities";
    private static final String TAG = Settings.class.getCanonicalName();
    private static volatile String appVersion;
    private static volatile Executor executor;
    private static volatile String facebookDomain = FACEBOOK_COM;
    private static final HashSet<LoggingBehavior> loggingBehaviors = new HashSet<>(Arrays.asList(LoggingBehavior.DEVELOPER_ERRORS));
    private static volatile boolean shouldAutoPublishInstall;

    public static final Set<LoggingBehavior> getLoggingBehaviors() {
        Set<LoggingBehavior> unmodifiableSet;
        synchronized (loggingBehaviors) {
            unmodifiableSet = Collections.unmodifiableSet(new HashSet(loggingBehaviors));
        }
        return unmodifiableSet;
    }

    public static final void addLoggingBehavior(LoggingBehavior behavior) {
        synchronized (loggingBehaviors) {
            loggingBehaviors.add(behavior);
        }
    }

    public static final void removeLoggingBehavior(LoggingBehavior behavior) {
        synchronized (loggingBehaviors) {
            loggingBehaviors.remove(behavior);
        }
    }

    public static final void clearLoggingBehaviors() {
        synchronized (loggingBehaviors) {
            loggingBehaviors.clear();
        }
    }

    public static final boolean isLoggingBehaviorEnabled(LoggingBehavior behavior) {
        synchronized (loggingBehaviors) {
        }
        return false;
    }

    public static Executor getExecutor() {
        synchronized (LOCK) {
            if (executor == null) {
                Executor executor2 = getAsyncTaskExecutor();
                if (executor2 == null) {
                    executor2 = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, DEFAULT_WORK_QUEUE, DEFAULT_THREAD_FACTORY);
                }
                executor = executor2;
            }
        }
        return executor;
    }

    public static void setExecutor(Executor executor2) {
        Validate.notNull(executor2, "executor");
        synchronized (LOCK) {
            executor = executor2;
        }
    }

    public static String getFacebookDomain() {
        return facebookDomain;
    }

    public static void setFacebookDomain(String facebookDomain2) {
        Log.w(TAG, "WARNING: Calling setFacebookDomain from non-DEBUG code.");
        facebookDomain = facebookDomain2;
    }

    private static Executor getAsyncTaskExecutor() {
        try {
            try {
                Object executorObject = AsyncTask.class.getField("THREAD_POOL_EXECUTOR").get(null);
                if (executorObject == null) {
                    return null;
                }
                if (!(executorObject instanceof Executor)) {
                    return null;
                }
                return (Executor) executorObject;
            } catch (IllegalAccessException e) {
                return null;
            }
        } catch (NoSuchFieldException e2) {
            return null;
        }
    }

    @Deprecated
    public static void publishInstallAsync(Context context, String applicationId) {
        publishInstallAsync(context, applicationId, null);
    }

    @Deprecated
    public static void publishInstallAsync(Context context, final String applicationId, final Request.Callback callback) {
        final Context applicationContext = context.getApplicationContext();
        getExecutor().execute(new Runnable() {
            public void run() {
                final Response response = Settings.publishInstallAndWaitForResponse(applicationContext, applicationId);
                if (callback != null) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            callback.onCompleted(response);
                        }
                    });
                }
            }
        });
    }

    @Deprecated
    public static void setShouldAutoPublishInstall(boolean shouldAutoPublishInstall2) {
        shouldAutoPublishInstall = shouldAutoPublishInstall2;
    }

    @Deprecated
    public static boolean getShouldAutoPublishInstall() {
        return shouldAutoPublishInstall;
    }

    @Deprecated
    public static boolean publishInstallAndWait(Context context, String applicationId) {
        Response response = publishInstallAndWaitForResponse(context, applicationId);
        return response != null && response.getError() == null;
    }

    @Deprecated
    public static Response publishInstallAndWaitForResponse(Context context, String applicationId) {
        return publishInstallAndWaitForResponse(context, applicationId, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.facebook.Response.<init>(com.facebook.Request, java.net.HttpURLConnection, com.facebook.model.GraphObject, boolean):void
     arg types: [?[OBJECT, ARRAY], ?[OBJECT, ARRAY], com.facebook.model.GraphObject, int]
     candidates:
      com.facebook.Response.<init>(com.facebook.Request, java.net.HttpURLConnection, com.facebook.model.GraphObjectList<com.facebook.model.GraphObject>, boolean):void
      com.facebook.Response.<init>(com.facebook.Request, java.net.HttpURLConnection, com.facebook.model.GraphObject, boolean):void */
    static Response publishInstallAndWaitForResponse(Context context, String applicationId, boolean isAutoPublish) {
        if (context == null || applicationId == null) {
            try {
                throw new IllegalArgumentException("Both context and applicationId must be non-null");
            } catch (Exception e) {
                Utility.logd("Facebook-publish", e);
                return new Response(null, null, new FacebookRequestError(null, e));
            }
        } else {
            String attributionId = getAttributionId(context.getContentResolver());
            SharedPreferences preferences = context.getSharedPreferences(ATTRIBUTION_PREFERENCES, 0);
            String pingKey = applicationId + "ping";
            String jsonKey = applicationId + "json";
            long lastPing = preferences.getLong(pingKey, 0);
            String lastResponseJSON = preferences.getString(jsonKey, null);
            if (!isAutoPublish) {
                setShouldAutoPublishInstall(false);
            }
            GraphObject publishParams = GraphObject.Factory.create();
            publishParams.setProperty("event", MOBILE_INSTALL_EVENT);
            publishParams.setProperty(ATTRIBUTION_KEY, attributionId);
            publishParams.setProperty(AUTO_PUBLISH, Boolean.valueOf(isAutoPublish));
            publishParams.setProperty("application_tracking_enabled", Boolean.valueOf(!AppEventsLogger.getLimitEventUsage(context)));
            publishParams.setProperty("application_package_name", context.getPackageName());
            Request publishRequest = Request.newPostRequest(null, String.format(PUBLISH_ACTIVITY_PATH, applicationId), publishParams, null);
            if (lastPing != 0) {
                GraphObject graphObject = null;
                if (lastResponseJSON != null) {
                    try {
                        graphObject = GraphObject.Factory.create(new JSONObject(lastResponseJSON));
                    } catch (JSONException e2) {
                    }
                }
                if (graphObject != null) {
                    return new Response((Request) null, (HttpURLConnection) null, graphObject, true);
                }
                return Response.createResponsesFromString("true", null, new RequestBatch(publishRequest), true).get(0);
            } else if (attributionId == null) {
                throw new FacebookException("No attribution id returned from the Facebook application");
            } else if (!Utility.queryAppSettings(applicationId, false).supportsAttribution()) {
                throw new FacebookException("Install attribution has been disabled on the server.");
            } else {
                Response publishResponse = publishRequest.executeAndWait();
                SharedPreferences.Editor editor = preferences.edit();
                editor.putLong(pingKey, System.currentTimeMillis());
                if (!(publishResponse.getGraphObject() == null || publishResponse.getGraphObject().getInnerJSONObject() == null)) {
                    editor.putString(jsonKey, publishResponse.getGraphObject().getInnerJSONObject().toString());
                }
                editor.commit();
                return publishResponse;
            }
        }
    }

    public static String getAttributionId(ContentResolver contentResolver) {
        ContentResolver contentResolver2 = contentResolver;
        Cursor c = contentResolver2.query(ATTRIBUTION_ID_CONTENT_URI, new String[]{"aid"}, null, null, null);
        if (c == null || !c.moveToFirst()) {
            return null;
        }
        String string = c.getString(c.getColumnIndex("aid"));
        c.close();
        return string;
    }

    public static String getAppVersion() {
        return appVersion;
    }

    public static void setAppVersion(String appVersion2) {
        appVersion = appVersion2;
    }

    public static String getSdkVersion() {
        return FacebookSdkVersion.BUILD;
    }

    public static String getMigrationBundle() {
        return FacebookSdkVersion.MIGRATION_BUNDLE;
    }
}
