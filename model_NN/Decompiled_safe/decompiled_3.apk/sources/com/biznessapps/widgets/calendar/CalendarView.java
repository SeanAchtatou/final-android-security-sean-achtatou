package com.biznessapps.widgets.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.MonthDisplayHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.biznessapps.layout.R;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarView extends LinearLayout {
    private Calendar calendarRightNow;
    private GridView calendarView;
    private Date date;
    private ArrayList<String> holidayDays;
    private int[] holidays;
    private MonthDisplayHelper monthHelper;
    private TextView monthYearView;
    private ImageButton nextMonthButton;
    private OnCellTouchListener onCellTouchListener;
    private ImageButton prevMonthButton;

    public interface OnCellTouchListener {
        void onTouch(View view);
    }

    public CalendarView(Context context) {
        this(context, null);
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.calendarRightNow = null;
        this.onCellTouchListener = null;
        ViewGroup root = (ViewGroup) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.calendar_layout, (ViewGroup) null);
        addView(root);
        initViews(root);
        initCalendarView();
        initCells();
    }

    private void initViews(ViewGroup root) {
        this.prevMonthButton = (ImageButton) root.findViewById(R.id.calendar_prev_month);
        this.nextMonthButton = (ImageButton) root.findViewById(R.id.calendar_next_month);
        this.monthYearView = (TextView) root.findViewById(R.id.calendar_month_year);
        this.calendarView = (GridView) root.findViewById(R.id.calendar_grid);
        this.prevMonthButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CalendarView.this.previousMonth();
            }
        });
        this.nextMonthButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CalendarView.this.nextMonth();
            }
        });
    }

    private void initCalendarView() {
        this.calendarRightNow = Calendar.getInstance();
        this.monthHelper = new MonthDisplayHelper(this.calendarRightNow.get(1), this.calendarRightNow.get(2));
    }

    private boolean isSelectedDay(CalendarCellData obj) {
        int year = this.calendarRightNow.get(1);
        int month = this.calendarRightNow.get(2);
        int day = this.calendarRightNow.get(5);
        if (year == obj.year && month == obj.month && day == obj.day) {
            return true;
        }
        return false;
    }

    private void initCells() {
        CalendarCellData[][] tmp = (CalendarCellData[][]) Array.newInstance(CalendarCellData.class, 6, 7);
        List<CalendarCellData> cellData = new ArrayList<>();
        int selectedItem = -1;
        for (int i = 0; i < tmp.length; i++) {
            int[] n = this.monthHelper.getDigitsForRow(i);
            if (!weekIsOutMonth(i)) {
                for (int d = 0; d < n.length; d++) {
                    if (this.monthHelper.isWithinCurrentMonth(i, d)) {
                        tmp[i][d] = new CalendarCellData(n[d], this.monthHelper.getMonth(), this.monthHelper.getYear(), d);
                    } else {
                        int year = this.monthHelper.getYear();
                        int month = this.monthHelper.getMonth();
                        if (i == 0) {
                            int prevMonth = month - 1;
                            int prevYear = year;
                            if (prevMonth < 0) {
                                prevMonth = 11;
                                int year2 = year - 1;
                            }
                            tmp[i][d] = new CalendarCellData(n[d], prevMonth, prevYear, d);
                        } else {
                            int nextMonth = month + 1;
                            int nextYear = year;
                            if (nextMonth >= 12) {
                                nextMonth = 0;
                                nextYear++;
                            }
                            tmp[i][d] = new CalendarCellData(n[d], nextMonth, nextYear, d);
                        }
                    }
                    if (isSelectedDay(tmp[i][d])) {
                        selectedItem = (i * 7) + d;
                    }
                    cellData.add(tmp[i][d]);
                }
            }
        }
        CalendarAdapter adapter = new CalendarAdapter(this, cellData, this.monthHelper);
        adapter.setSelectedItem(selectedItem);
        this.calendarView.setAdapter((ListAdapter) adapter);
        updateTitle();
    }

    public void setTimeInMillis(long milliseconds) {
        this.calendarRightNow.setTimeInMillis(milliseconds);
        this.monthHelper = new MonthDisplayHelper(this.calendarRightNow.get(1), this.calendarRightNow.get(2));
        this.date = this.calendarRightNow.getTime();
        initCells();
    }

    public void selectDate(Date date2) {
        setTimeInMillis(date2.getTime());
    }

    public int getYear() {
        return this.monthHelper.getYear();
    }

    public int getMonth() {
        return this.monthHelper.getMonth();
    }

    /* access modifiers changed from: protected */
    public boolean weekIsOutMonth(int w) {
        for (int d = 0; d < 7; d++) {
            if (this.monthHelper.isWithinCurrentMonth(w, d)) {
                return false;
            }
        }
        return true;
    }

    public void nextMonth() {
        this.monthHelper.nextMonth();
        initCells();
    }

    public void previousMonth() {
        this.monthHelper.previousMonth();
        initCells();
    }

    /* access modifiers changed from: protected */
    public void updateTitle() {
        Date date2 = new Date(this.monthHelper.getYear() - 1900, this.monthHelper.getMonth(), 1);
        this.monthYearView.setText(new SimpleDateFormat("MMMM yyyy").format(date2));
    }

    public boolean firstDay(int day) {
        return day == 1;
    }

    public boolean lastDay(int day) {
        return this.monthHelper.getNumberOfDaysInMonth() == day;
    }

    public void goToday() {
        Calendar cal = Calendar.getInstance();
        this.monthHelper = new MonthDisplayHelper(cal.get(1), cal.get(2));
        initCells();
    }

    public Date getDate() {
        return this.date;
    }

    /* access modifiers changed from: protected */
    public void setDate(Date date2) {
        this.date = date2;
    }

    public void setOnCellTouchListener(OnCellTouchListener p) {
        this.onCellTouchListener = p;
    }

    public OnCellTouchListener getOnCellTouchListener() {
        return this.onCellTouchListener;
    }

    public int[] getHolidays() {
        return this.holidays;
    }

    public void setHolidays(int[] holidays2) {
        this.holidays = holidays2;
    }

    public ArrayList<String> getHolidayDays() {
        return this.holidayDays;
    }

    public void setHolidayDays(ArrayList<String> holidayDays2) {
        this.holidayDays = holidayDays2;
    }

    public boolean isInHolidays(int date2) {
        if (this.holidays == null || this.holidays.length == 0) {
            return false;
        }
        for (int i : this.holidays) {
            if (i == date2) {
                return true;
            }
        }
        return false;
    }

    public boolean isInHolidayDays(int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(7, day + 1);
        String dayString = new SimpleDateFormat("EEEE").format(calendar.getTime());
        if (this.holidayDays == null || this.holidayDays.size() == 0 || !this.holidayDays.contains(dayString)) {
            return false;
        }
        return true;
    }
}
