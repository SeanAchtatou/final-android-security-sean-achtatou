package com.biznessapps.widgets.calendar;

import com.biznessapps.widgets.calendar.CalendarAdapter;
import java.util.Calendar;

public class CalendarCellData {
    public CalendarAdapter.CalendarCell cell = null;
    public int day;
    public int dayOfWeek;
    public int month;
    public int year;

    public CalendarCellData(int d, int m, int y, int dayOfWeek2) {
        this.day = d;
        this.month = m;
        this.year = y;
        this.dayOfWeek = dayOfWeek2;
    }

    public boolean isToday() {
        Calendar calendar = Calendar.getInstance();
        if (calendar.get(1) == this.year && calendar.get(2) == this.month && calendar.get(5) == this.day) {
            return true;
        }
        return false;
    }

    public boolean beforeToday() {
        Calendar calendar = Calendar.getInstance();
        if (calendar.get(1) < this.year) {
            return false;
        }
        if (calendar.get(1) > this.year) {
            return true;
        }
        if (calendar.get(2) < this.month) {
            return false;
        }
        if (calendar.get(2) > this.month) {
            return true;
        }
        if (calendar.get(5) < this.day || calendar.get(5) <= this.day) {
            return false;
        }
        return true;
    }

    public boolean afterToday() {
        Calendar calendar = Calendar.getInstance();
        if (calendar.get(1) > this.year) {
            return false;
        }
        if (calendar.get(1) < this.year) {
            return true;
        }
        if (calendar.get(2) > this.month) {
            return false;
        }
        if (calendar.get(2) < this.month) {
            return true;
        }
        if (calendar.get(5) > this.day || calendar.get(5) >= this.day) {
            return false;
        }
        return true;
    }

    public String toString() {
        return this.year + "-" + (this.month + 1) + "-" + this.day;
    }
}
