package com.biznessapps.widgets.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatePickerBar extends LinearLayout {
    private ImageButton backButton;
    private DatePickerBarButtonListener listener;
    private ImageButton nextButton;
    private Date pickerDate;
    private TextView titleView;

    public interface DatePickerBarButtonListener {
        void onDateChanged(DatePickerBar datePickerBar);
    }

    public Date getPickerDate() {
        return this.pickerDate;
    }

    public void setPickerDate(Date pickerDate2) {
        this.pickerDate = pickerDate2;
        updateTitle();
        if (this.listener != null) {
            this.listener.onDateChanged(this);
        }
    }

    public DatePickerBarButtonListener getListener() {
        return this.listener;
    }

    public void setListener(DatePickerBarButtonListener listener2) {
        this.listener = listener2;
    }

    public DatePickerBar(Context context) {
        this(context, null);
    }

    public DatePickerBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.pickerDate = new Date();
        ViewGroup root = (ViewGroup) ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.datepickerbar_layout, (ViewGroup) null);
        addView(root);
        initViews(root);
    }

    private void initViews(ViewGroup root) {
        this.backButton = (ImageButton) root.findViewById(R.id.datepickerbar_left_button);
        this.nextButton = (ImageButton) root.findViewById(R.id.datepickerbar_right_button);
        this.titleView = (TextView) root.findViewById(R.id.datepickerbar_date);
        updateTitle();
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerBar.this.previousDate();
            }
        });
        this.nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                DatePickerBar.this.nextDate();
            }
        });
    }

    public void previousDate() {
        setPickerDate(new Date(this.pickerDate.getTime() - DateUtils.MILLI_SEC_IN_DAY));
    }

    public void nextDate() {
        setPickerDate(new Date(this.pickerDate.getTime() + DateUtils.MILLI_SEC_IN_DAY));
    }

    public void updateTitle() {
        this.titleView.setText(new SimpleDateFormat(DateUtils.DATE_PICKER_TITLE_FORMAT).format(this.pickerDate));
    }
}
