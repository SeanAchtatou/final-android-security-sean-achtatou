package com.biznessapps.widgets.calendar;

import android.content.Context;
import android.util.MonthDisplayHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.biznessapps.layout.R;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarAdapter extends BaseAdapter {
    static final /* synthetic */ boolean $assertionsDisabled = (!CalendarAdapter.class.desiredAssertionStatus());
    /* access modifiers changed from: private */
    public CalendarView calendarView;
    /* access modifiers changed from: private */
    public List<CalendarCellData> cellData;
    private Context context;
    private LayoutInflater inflater;
    private MonthDisplayHelper monthHelper;
    private int selectedItem;

    public CalendarAdapter(CalendarView view, List<CalendarCellData> cellData2, MonthDisplayHelper helper) {
        this(view, cellData2);
        this.monthHelper = helper;
    }

    public CalendarAdapter(CalendarView view, List<CalendarCellData> cellData2) {
        if ($assertionsDisabled || view != null) {
            this.calendarView = view;
            this.context = view.getContext();
            this.cellData = cellData2;
            Calendar calendar = Calendar.getInstance();
            this.monthHelper = new MonthDisplayHelper(calendar.get(1), calendar.get(2));
            this.selectedItem = -1;
            this.inflater = (LayoutInflater) this.context.getSystemService("layout_inflater");
            return;
        }
        throw new AssertionError();
    }

    public int getCount() {
        if (this.cellData == null) {
            return 0;
        }
        return this.cellData.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public int getSelectedItem() {
        return this.selectedItem;
    }

    public void setSelectedItem(int selectedItem2) {
        this.selectedItem = selectedItem2;
    }

    public CalendarView getCalendarView() {
        return this.calendarView;
    }

    public void setCalendarView(CalendarView calendarView2) {
        this.calendarView = calendarView2;
    }

    private boolean isInCurrentMonth(CalendarCellData obj) {
        return this.monthHelper.getYear() == obj.year && this.monthHelper.getMonth() == obj.month;
    }

    /* access modifiers changed from: private */
    public boolean isInPreviousMonth(CalendarCellData obj) {
        int thisYear = this.monthHelper.getYear();
        int thisMonth = this.monthHelper.getMonth();
        if (obj.year < thisYear) {
            return true;
        }
        if (obj.year > thisYear) {
            return false;
        }
        if (obj.month >= thisMonth) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public boolean isInNextMonth(CalendarCellData obj) {
        int thisYear = this.monthHelper.getYear();
        int thisMonth = this.monthHelper.getMonth();
        if (obj.year > thisYear) {
            return true;
        }
        if (obj.year < thisYear) {
            return false;
        }
        if (obj.month <= thisMonth) {
            return false;
        }
        return true;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        CalendarCell cell;
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.calendar_cell, (ViewGroup) null);
            cell = new CalendarCell();
            cell.setDateView((TextView) convertView.findViewById(R.id.calendar_cell_date));
            cell.getDateView().setTag(Integer.valueOf(position));
            convertView.setTag(cell);
            convertView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CalendarCell cell = (CalendarCell) v.getTag();
                    if (cell.isEnabled()) {
                        int selectedItem = ((Integer) cell.getDateView().getTag()).intValue();
                        CalendarCellData data = (CalendarCellData) CalendarAdapter.this.cellData.get(selectedItem);
                        if (CalendarAdapter.this.isInPreviousMonth(data)) {
                            CalendarAdapter.this.calendarView.previousMonth();
                        } else if (CalendarAdapter.this.isInNextMonth(data)) {
                            CalendarAdapter.this.calendarView.nextMonth();
                        } else {
                            cell.setBackground(R.drawable.datecellselected, false);
                            cell.setTextColor(-1, false);
                            if (CalendarAdapter.this.getSelectedItem() > -1) {
                                CalendarCell oldCell = ((CalendarCellData) CalendarAdapter.this.cellData.get(CalendarAdapter.this.getSelectedItem())).cell;
                                oldCell.restoreBackground();
                                oldCell.restoreTextColor();
                            }
                            CalendarAdapter.this.setSelectedItem(selectedItem);
                            CalendarAdapter.this.calendarView.setDate(new Date(data.year - 1900, data.month, data.day));
                            if (CalendarAdapter.this.calendarView.getOnCellTouchListener() != null) {
                                CalendarAdapter.this.calendarView.getOnCellTouchListener().onTouch(v);
                            }
                        }
                    }
                }
            });
        } else {
            cell = (CalendarCell) convertView.getTag();
        }
        CalendarCellData obj = this.cellData.get(position);
        cell.setText(String.format("%d", Integer.valueOf(obj.day)));
        if (obj.isToday()) {
            cell.setTextColor(-1);
        } else if (isInCurrentMonth(obj)) {
            cell.setTextColor(-12891816);
        } else {
            cell.setTextColor(-8355712);
        }
        if (obj.isToday()) {
            cell.setBackground(R.drawable.today);
            cell.setEnabled(false);
        } else if (obj.beforeToday()) {
            cell.setBackground(R.drawable.today);
            cell.setEnabled(false);
        } else if (this.calendarView.isInHolidayDays(obj.dayOfWeek) || this.calendarView.isInHolidays(obj.day)) {
            cell.setBackground(R.drawable.today);
            cell.setEnabled(false);
        } else {
            cell.setBackground(R.drawable.datecell);
            cell.setEnabled(true);
        }
        if (position == this.selectedItem) {
            cell.setBackground(R.drawable.datecellselected, false);
            cell.setTextColor(-1, false);
        }
        obj.cell = cell;
        return convertView;
    }

    public class CalendarCell {
        private int backgroundId;
        private TextView dateView;
        private boolean enabled;
        private int textColor;

        public CalendarCell() {
        }

        public TextView getDateView() {
            return this.dateView;
        }

        public void setDateView(TextView dateView2) {
            this.dateView = dateView2;
        }

        public boolean isEnabled() {
            return this.enabled;
        }

        public void setEnabled(boolean enabled2) {
            this.enabled = enabled2;
        }

        public void setTextColor(int color) {
            setTextColor(color, true);
        }

        public void setTextColor(int color, boolean saveOldState) {
            if (saveOldState) {
                this.textColor = color;
            }
            this.dateView.setTextColor(color);
        }

        public void restoreTextColor() {
            this.dateView.setTextColor(this.textColor);
        }

        public void setBackground(int background) {
            setBackground(background, true);
        }

        public void setBackground(int background, boolean saveOldState) {
            if (saveOldState) {
                this.backgroundId = background;
            }
            this.dateView.setBackgroundResource(background);
        }

        public void restoreBackground() {
            if (this.backgroundId > 0) {
                this.dateView.setBackgroundResource(this.backgroundId);
            }
        }

        public void setText(CharSequence text) {
            this.dateView.setText(text);
        }

        public CharSequence getText() {
            return this.dateView.getText();
        }
    }
}
