package com.biznessapps.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.StateListDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewParent;
import android.widget.RemoteViews;
import com.biznessapps.layout.R;

@RemoteViews.RemoteView
public class VerticalProgressBar extends View {
    private static final int MAX_LEVEL = 10000;
    private Drawable currentDrawable;
    private boolean inDrawing;
    int maxHeight;
    private int maxValue;
    int maxWidth;
    int minHeight;
    int minWidth;
    private boolean noInvalidate;
    protected int paddingBottom;
    protected int paddingLeft;
    protected int paddingRight;
    protected int paddingTop;
    protected ViewParent parentView;
    private Drawable progressDrawable;
    private int progressValue;
    /* access modifiers changed from: private */
    public RefreshProgressRunnable refreshProgressRunnable;
    Bitmap sampleTile;
    protected int scrollXPos;
    protected int scrollYPos;
    private int secondaryProgressValue;
    private long uiThreadId;

    public VerticalProgressBar(Context context) {
        this(context, null);
    }

    public VerticalProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 16842871);
    }

    public VerticalProgressBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.uiThreadId = Thread.currentThread().getId();
        initProgressBar();
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ProgressBar, defStyle, 0);
        this.noInvalidate = true;
        Drawable drawable = a.getDrawable(5);
        if (drawable != null) {
            setProgressDrawable(tileify(drawable, false));
        }
        this.minWidth = a.getDimensionPixelSize(6, this.minWidth);
        this.maxWidth = a.getDimensionPixelSize(0, this.maxWidth);
        this.minHeight = a.getDimensionPixelSize(7, this.minHeight);
        this.maxHeight = a.getDimensionPixelSize(1, this.maxHeight);
        setMax(a.getInt(2, this.maxValue));
        setProgress(a.getInt(3, this.progressValue));
        setSecondaryProgress(a.getInt(4, this.secondaryProgressValue));
        this.noInvalidate = false;
        a.recycle();
    }

    private Drawable tileify(Drawable drawable, boolean clip) {
        if (drawable instanceof LayerDrawable) {
            LayerDrawable background = (LayerDrawable) drawable;
            int N = background.getNumberOfLayers();
            Drawable[] outDrawables = new Drawable[N];
            for (int i = 0; i < N; i++) {
                int id = background.getId(i);
                outDrawables[i] = tileify(background.getDrawable(i), id == 16908301 || id == 16908303);
            }
            LayerDrawable newBg = new LayerDrawable(outDrawables);
            for (int i2 = 0; i2 < N; i2++) {
                newBg.setId(i2, background.getId(i2));
            }
            return newBg;
        } else if (drawable instanceof StateListDrawable) {
            return new StateListDrawable();
        } else {
            if (!(drawable instanceof BitmapDrawable)) {
                return drawable;
            }
            Bitmap tileBitmap = ((BitmapDrawable) drawable).getBitmap();
            if (this.sampleTile == null) {
                this.sampleTile = tileBitmap;
            }
            Drawable shapeDrawable = new ShapeDrawable(getDrawableShape());
            if (clip) {
                shapeDrawable = new ClipDrawable(shapeDrawable, 3, 1);
            }
            return shapeDrawable;
        }
    }

    /* access modifiers changed from: package-private */
    public Shape getDrawableShape() {
        return new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null);
    }

    private void initProgressBar() {
        this.maxValue = 100;
        this.progressValue = 0;
        this.secondaryProgressValue = 0;
        this.minWidth = 24;
        this.maxWidth = 48;
        this.minHeight = 24;
        this.maxHeight = 48;
    }

    public Drawable getProgressDrawable() {
        return this.progressDrawable;
    }

    public void setProgressDrawable(Drawable d) {
        if (d != null) {
            d.setCallback(this);
            int drawableHeight = d.getMinimumHeight();
            if (this.maxHeight < drawableHeight) {
                this.maxHeight = drawableHeight;
                requestLayout();
            }
        }
        this.progressDrawable = d;
        this.currentDrawable = d;
        postInvalidate();
    }

    /* access modifiers changed from: package-private */
    public Drawable getCurrentDrawable() {
        return this.currentDrawable;
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable who) {
        return who == this.progressDrawable || super.verifyDrawable(who);
    }

    public void postInvalidate() {
        if (!this.noInvalidate) {
            super.postInvalidate();
        }
    }

    private class RefreshProgressRunnable implements Runnable {
        private boolean fromUser;
        private int id;
        private int progress;

        RefreshProgressRunnable(int id2, int progress2, boolean fromUser2) {
            this.id = id2;
            this.progress = progress2;
            this.fromUser = fromUser2;
        }

        public void run() {
            VerticalProgressBar.this.doRefreshProgress(this.id, this.progress, this.fromUser);
            RefreshProgressRunnable unused = VerticalProgressBar.this.refreshProgressRunnable = this;
        }

        public void setup(int id2, int progress2, boolean fromUser2) {
            this.id = id2;
            this.progress = progress2;
            this.fromUser = fromUser2;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void doRefreshProgress(int id, int progress, boolean fromUser) {
        float scale = this.maxValue > 0 ? ((float) progress) / ((float) this.maxValue) : 0.0f;
        Drawable d = this.currentDrawable;
        if (d != null) {
            Drawable progressDrawable2 = null;
            if (d instanceof LayerDrawable) {
                progressDrawable2 = ((LayerDrawable) d).findDrawableByLayerId(id);
            }
            int level = (int) (10000.0f * scale);
            if (progressDrawable2 == null) {
                progressDrawable2 = d;
            }
            progressDrawable2.setLevel(level);
        } else {
            invalidate();
        }
        if (id == 16908301) {
            onProgressRefresh(scale, fromUser);
        }
    }

    /* access modifiers changed from: package-private */
    public void onProgressRefresh(float scale, boolean fromUser) {
    }

    private synchronized void refreshProgress(int id, int progress, boolean fromUser) {
        RefreshProgressRunnable r;
        if (this.uiThreadId == Thread.currentThread().getId()) {
            doRefreshProgress(id, progress, fromUser);
        } else {
            if (this.refreshProgressRunnable != null) {
                r = this.refreshProgressRunnable;
                this.refreshProgressRunnable = null;
                r.setup(id, progress, fromUser);
            } else {
                r = new RefreshProgressRunnable(id, progress, fromUser);
            }
            post(r);
        }
    }

    public synchronized void setProgress(int progress) {
        setProgress(progress, false);
    }

    /* access modifiers changed from: package-private */
    public synchronized void setProgress(int progress, boolean fromUser) {
        if (progress < 0) {
            progress = 0;
        }
        if (progress > this.maxValue) {
            progress = this.maxValue;
        }
        if (progress != this.progressValue) {
            this.progressValue = progress;
            refreshProgress(16908301, this.progressValue, fromUser);
        }
    }

    public synchronized void setSecondaryProgress(int secondaryProgress) {
        if (secondaryProgress < 0) {
            secondaryProgress = 0;
        }
        if (secondaryProgress > this.maxValue) {
            secondaryProgress = this.maxValue;
        }
        if (secondaryProgress != this.secondaryProgressValue) {
            this.secondaryProgressValue = secondaryProgress;
            refreshProgress(16908303, this.secondaryProgressValue, false);
        }
    }

    @ViewDebug.ExportedProperty
    public synchronized int getProgress() {
        return this.progressValue;
    }

    @ViewDebug.ExportedProperty
    public synchronized int getSecondaryProgress() {
        return this.secondaryProgressValue;
    }

    @ViewDebug.ExportedProperty
    public synchronized int getMax() {
        return this.maxValue;
    }

    public synchronized void setMax(int max) {
        if (max < 0) {
            max = 0;
        }
        if (max != this.maxValue) {
            this.maxValue = max;
            postInvalidate();
            if (this.progressValue > max) {
                this.progressValue = max;
                refreshProgress(16908301, this.progressValue, false);
            }
        }
    }

    public final synchronized void incrementProgressBy(int diff) {
        setProgress(this.progressValue + diff);
    }

    public final synchronized void incrementSecondaryProgressBy(int diff) {
        setSecondaryProgress(this.secondaryProgressValue + diff);
    }

    public void setVisibility(int v) {
        if (getVisibility() != v) {
            super.setVisibility(v);
        }
    }

    public void invalidateDrawable(Drawable dr) {
        if (this.inDrawing) {
            return;
        }
        if (verifyDrawable(dr)) {
            Rect dirty = dr.getBounds();
            int scrollX = this.scrollXPos + this.paddingLeft;
            int scrollY = this.scrollYPos + this.paddingTop;
            invalidate(dirty.left + scrollX, dirty.top + scrollY, dirty.right + scrollX, dirty.bottom + scrollY);
            return;
        }
        super.invalidateDrawable(dr);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        int right = (w - this.paddingRight) - this.paddingLeft;
        int bottom = (h - this.paddingBottom) - this.paddingTop;
        if (this.progressDrawable != null) {
            this.progressDrawable.setBounds(0, 0, right, bottom);
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Drawable d = this.currentDrawable;
        if (d != null) {
            canvas.save();
            canvas.translate((float) this.paddingLeft, (float) this.paddingTop);
            d.draw(canvas);
            canvas.restore();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = this.currentDrawable;
        int dw = 0;
        int dh = 0;
        if (d != null) {
            dw = Math.max(this.minWidth, Math.min(this.maxWidth, d.getIntrinsicWidth()));
            dh = Math.max(this.minHeight, Math.min(this.maxHeight, d.getIntrinsicHeight()));
        }
        setMeasuredDimension(resolveSize(dw + this.paddingLeft + this.paddingRight, widthMeasureSpec), resolveSize(dh + this.paddingTop + this.paddingBottom, heightMeasureSpec));
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] state = getDrawableState();
        if (this.progressDrawable != null && this.progressDrawable.isStateful()) {
            this.progressDrawable.setState(state);
        }
    }

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        int progress;
        int secondaryProgress;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.progress = in.readInt();
            this.secondaryProgress = in.readInt();
        }

        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.progress);
            out.writeInt(this.secondaryProgress);
        }
    }

    public Parcelable onSaveInstanceState() {
        SavedState ss = new SavedState(super.onSaveInstanceState());
        ss.progress = this.progressValue;
        ss.secondaryProgress = this.secondaryProgressValue;
        return ss;
    }

    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());
        setProgress(ss.progress);
        setSecondaryProgress(ss.secondaryProgress);
    }
}
