package com.biznessapps.widgets;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Scroller;
import java.util.LinkedList;
import java.util.Queue;

public class HorizontalListView extends AdapterView<ListAdapter> {
    public boolean alwaysOverrideTouch = true;
    protected int currentX;
    /* access modifiers changed from: private */
    public boolean dataChanged = false;
    private DataSetObserver dataObserver = new DataSetObserver() {
        public void onChanged() {
            synchronized (HorizontalListView.this) {
                boolean unused = HorizontalListView.this.dataChanged = true;
            }
            HorizontalListView.this.invalidate();
            HorizontalListView.this.requestLayout();
        }

        public void onInvalidated() {
            HorizontalListView.this.reset();
            HorizontalListView.this.invalidate();
            HorizontalListView.this.requestLayout();
        }
    };
    private int displayOffset = 0;
    private GestureDetector gestureDetector;
    /* access modifiers changed from: private */
    public int leftViewIndex = -1;
    protected ListAdapter listAdapter;
    private int maxX = Integer.MAX_VALUE;
    protected int nextX;
    private GestureDetector.OnGestureListener onGesture = new GestureDetector.SimpleOnGestureListener() {
        public boolean onDown(MotionEvent e) {
            return HorizontalListView.this.onDown(e);
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return HorizontalListView.this.onFling(e1, e2, velocityX, velocityY);
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            synchronized (HorizontalListView.this) {
                HorizontalListView.this.nextX += (int) distanceX;
            }
            HorizontalListView.this.requestLayout();
            return true;
        }

        public boolean onSingleTapConfirmed(MotionEvent e) {
            for (int i = 0; i < HorizontalListView.this.getChildCount(); i++) {
                View child = HorizontalListView.this.getChildAt(i);
                if (isEventWithinView(e, child)) {
                    if (HorizontalListView.this.onItemClicked != null) {
                        HorizontalListView.this.onItemClicked.onItemClick(HorizontalListView.this, child, HorizontalListView.this.leftViewIndex + 1 + i, HorizontalListView.this.listAdapter.getItemId(HorizontalListView.this.leftViewIndex + 1 + i));
                    }
                    if (HorizontalListView.this.onItemSelected == null) {
                        return true;
                    }
                    HorizontalListView.this.onItemSelected.onItemSelected(HorizontalListView.this, child, HorizontalListView.this.leftViewIndex + 1 + i, HorizontalListView.this.listAdapter.getItemId(HorizontalListView.this.leftViewIndex + 1 + i));
                    return true;
                }
            }
            return true;
        }

        public void onLongPress(MotionEvent e) {
            int childCount = HorizontalListView.this.getChildCount();
            int i = 0;
            while (i < childCount) {
                View child = HorizontalListView.this.getChildAt(i);
                if (!isEventWithinView(e, child)) {
                    i++;
                } else if (HorizontalListView.this.onItemLongClicked != null) {
                    HorizontalListView.this.onItemLongClicked.onItemLongClick(HorizontalListView.this, child, HorizontalListView.this.leftViewIndex + 1 + i, HorizontalListView.this.listAdapter.getItemId(HorizontalListView.this.leftViewIndex + 1 + i));
                    return;
                } else {
                    return;
                }
            }
        }

        private boolean isEventWithinView(MotionEvent e, View child) {
            Rect viewRect = new Rect();
            int[] childPosition = new int[2];
            child.getLocationOnScreen(childPosition);
            int left = childPosition[0];
            int top = childPosition[1];
            viewRect.set(left, top, left + child.getWidth(), top + child.getHeight());
            return viewRect.contains((int) e.getRawX(), (int) e.getRawY());
        }
    };
    /* access modifiers changed from: private */
    public AdapterView.OnItemClickListener onItemClicked;
    /* access modifiers changed from: private */
    public AdapterView.OnItemLongClickListener onItemLongClicked;
    /* access modifiers changed from: private */
    public AdapterView.OnItemSelectedListener onItemSelected;
    private Queue<View> removedViewQueue = new LinkedList();
    private int rightViewIndex = 0;
    protected Scroller scroller;

    public HorizontalListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    private synchronized void initView() {
        this.leftViewIndex = -1;
        this.rightViewIndex = 0;
        this.displayOffset = 0;
        this.currentX = 0;
        this.nextX = 0;
        this.maxX = Integer.MAX_VALUE;
        this.scroller = new Scroller(getContext());
        this.gestureDetector = new GestureDetector(getContext(), this.onGesture);
    }

    public void setOnItemSelectedListener(AdapterView.OnItemSelectedListener listener) {
        this.onItemSelected = listener;
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener listener) {
        this.onItemClicked = listener;
    }

    public void setOnItemLongClickListener(AdapterView.OnItemLongClickListener listener) {
        this.onItemLongClicked = listener;
    }

    public ListAdapter getAdapter() {
        return this.listAdapter;
    }

    public View getSelectedView() {
        return null;
    }

    public void setAdapter(ListAdapter adapter) {
        if (this.listAdapter != null) {
            this.listAdapter.unregisterDataSetObserver(this.dataObserver);
        }
        this.listAdapter = adapter;
        this.listAdapter.registerDataSetObserver(this.dataObserver);
        reset();
    }

    /* access modifiers changed from: private */
    public synchronized void reset() {
        initView();
        removeAllViewsInLayout();
        requestLayout();
    }

    public void setSelection(int position) {
    }

    private void addAndMeasureChild(View child, int viewPos) {
        ViewGroup.LayoutParams params = child.getLayoutParams();
        if (params == null) {
            params = new ViewGroup.LayoutParams(-1, -1);
        }
        addViewInLayout(child, viewPos, params, true);
        child.measure(View.MeasureSpec.makeMeasureSpec(getWidth(), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(getHeight(), Integer.MIN_VALUE));
    }

    /* access modifiers changed from: protected */
    public synchronized void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (this.listAdapter != null) {
            if (this.dataChanged) {
                int oldCurrentX = this.currentX;
                initView();
                removeAllViewsInLayout();
                this.nextX = oldCurrentX;
                this.dataChanged = false;
            }
            if (this.scroller.computeScrollOffset()) {
                this.nextX = this.scroller.getCurrX();
            }
            if (this.nextX <= 0) {
                this.nextX = 0;
                this.scroller.forceFinished(true);
            }
            if (this.nextX >= this.maxX) {
                this.nextX = this.maxX;
                this.scroller.forceFinished(true);
            }
            int dx = this.currentX - this.nextX;
            removeNonVisibleItems(dx);
            fillList(dx);
            positionItems(dx);
            this.currentX = this.nextX;
            if (!this.scroller.isFinished()) {
                post(new Runnable() {
                    public void run() {
                        HorizontalListView.this.requestLayout();
                    }
                });
            }
        }
    }

    private void fillList(int dx) {
        int edge = 0;
        View child = getChildAt(getChildCount() - 1);
        if (child != null) {
            edge = child.getRight();
        }
        fillListRight(edge, dx);
        int edge2 = 0;
        View child2 = getChildAt(0);
        if (child2 != null) {
            edge2 = child2.getLeft();
        }
        fillListLeft(edge2, dx);
    }

    private void fillListRight(int rightEdge, int dx) {
        while (rightEdge + dx < getWidth() && this.rightViewIndex < this.listAdapter.getCount()) {
            View child = this.listAdapter.getView(this.rightViewIndex, this.removedViewQueue.poll(), this);
            addAndMeasureChild(child, -1);
            rightEdge += child.getMeasuredWidth();
            if (this.rightViewIndex == this.listAdapter.getCount() - 1) {
                this.maxX = (this.currentX + rightEdge) - getWidth();
            }
            if (this.maxX < 0) {
                this.maxX = 0;
            }
            this.rightViewIndex++;
        }
    }

    private void fillListLeft(int leftEdge, int dx) {
        while (leftEdge + dx > 0 && this.leftViewIndex >= 0) {
            View child = this.listAdapter.getView(this.leftViewIndex, this.removedViewQueue.poll(), this);
            addAndMeasureChild(child, 0);
            leftEdge -= child.getMeasuredWidth();
            this.leftViewIndex--;
            this.displayOffset -= child.getMeasuredWidth();
        }
    }

    private void removeNonVisibleItems(int dx) {
        View child = getChildAt(0);
        while (child != null && child.getRight() + dx <= 0) {
            this.displayOffset += child.getMeasuredWidth();
            this.removedViewQueue.offer(child);
            removeViewInLayout(child);
            this.leftViewIndex++;
            child = getChildAt(0);
        }
        View child2 = getChildAt(getChildCount() - 1);
        while (child2 != null && child2.getLeft() + dx >= getWidth()) {
            this.removedViewQueue.offer(child2);
            removeViewInLayout(child2);
            this.rightViewIndex--;
            child2 = getChildAt(getChildCount() - 1);
        }
    }

    private void positionItems(int dx) {
        if (getChildCount() > 0) {
            this.displayOffset += dx;
            int left = this.displayOffset;
            for (int i = 0; i < getChildCount(); i++) {
                View child = getChildAt(i);
                int childWidth = child.getMeasuredWidth();
                child.layout(left, 0, left + childWidth, child.getMeasuredHeight());
                left += childWidth;
            }
        }
    }

    public synchronized void scrollTo(int x) {
        this.scroller.startScroll(this.nextX, 0, x - this.nextX, 0);
        requestLayout();
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev) | this.gestureDetector.onTouchEvent(ev);
    }

    /* access modifiers changed from: protected */
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        synchronized (this) {
            this.scroller.fling(this.nextX, 0, (int) (-velocityX), 0, 0, this.maxX, 0, 0);
        }
        requestLayout();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onDown(MotionEvent e) {
        this.scroller.forceFinished(true);
        return true;
    }
}
