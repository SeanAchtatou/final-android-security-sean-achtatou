package com.biznessapps.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.biznessapps.layout.R;

public class AbsVerticalSeekBar extends VerticalProgressBar {
    private static final int NO_ALPHA = 255;
    private float disabledAlpha;
    boolean isUserSeekable = true;
    private int keyProgressIncrement = 1;
    private Drawable thumbDrawable;
    private int thumbOffset;
    float touchProgressOffset;

    public AbsVerticalSeekBar(Context context) {
        super(context);
    }

    public AbsVerticalSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AbsVerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SeekBar, defStyle, 0);
        setThumb(a.getDrawable(0));
        setThumbOffset(a.getDimensionPixelOffset(1, getThumbOffset()));
        a.recycle();
        TypedArray a2 = context.obtainStyledAttributes(attrs, R.styleable.Theme, 0, 0);
        this.disabledAlpha = a2.getFloat(0, 0.5f);
        a2.recycle();
    }

    public void setThumb(Drawable thumb) {
        if (thumb != null) {
            thumb.setCallback(this);
            this.thumbOffset = thumb.getIntrinsicHeight() / 2;
        }
        this.thumbDrawable = thumb;
        invalidate();
    }

    public int getThumbOffset() {
        return this.thumbOffset;
    }

    public void setThumbOffset(int thumbOffset2) {
        this.thumbOffset = thumbOffset2;
        invalidate();
    }

    public void setKeyProgressIncrement(int increment) {
        if (increment < 0) {
            increment = -increment;
        }
        this.keyProgressIncrement = increment;
    }

    public int getKeyProgressIncrement() {
        return this.keyProgressIncrement;
    }

    public synchronized void setMax(int max) {
        super.setMax(max);
        if (this.keyProgressIncrement == 0 || getMax() / this.keyProgressIncrement > 20) {
            setKeyProgressIncrement(Math.max(1, Math.round(((float) getMax()) / 20.0f)));
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable who) {
        return who == this.thumbDrawable || super.verifyDrawable(who);
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        Drawable progressDrawable = getProgressDrawable();
        if (progressDrawable != null) {
            progressDrawable.setAlpha(isEnabled() ? 255 : (int) (255.0f * this.disabledAlpha));
        }
        if (this.thumbDrawable != null && this.thumbDrawable.isStateful()) {
            this.thumbDrawable.setState(getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public void onProgressRefresh(float scale, boolean fromUser) {
        Drawable thumb = this.thumbDrawable;
        if (thumb != null) {
            setThumbPos(getHeight(), thumb, scale, Integer.MIN_VALUE);
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        Drawable d = getCurrentDrawable();
        Drawable thumb = this.thumbDrawable;
        int thumbWidth = thumb == null ? 0 : thumb.getIntrinsicWidth();
        int trackWidth = Math.min(this.maxWidth, (w - this.paddingRight) - this.paddingLeft);
        int max = getMax();
        float scale = max > 0 ? ((float) getProgress()) / ((float) max) : 0.0f;
        if (thumbWidth > trackWidth) {
            int gapForCenteringTrack = (thumbWidth - trackWidth) / 2;
            if (thumb != null) {
                setThumbPos(h, thumb, scale, gapForCenteringTrack * -1);
            }
            if (d != null) {
                d.setBounds(gapForCenteringTrack, 0, ((w - this.paddingRight) - this.paddingLeft) - gapForCenteringTrack, (h - this.paddingBottom) - this.paddingTop);
                return;
            }
            return;
        }
        if (d != null) {
            d.setBounds(0, 0, (w - this.paddingRight) - this.paddingLeft, (h - this.paddingBottom) - this.paddingTop);
        }
        int gap = (trackWidth - thumbWidth) / 2;
        if (thumb != null) {
            setThumbPos(h, thumb, scale, gap);
        }
    }

    private void setThumbPos(int h, Drawable thumb, float scale, int gap) {
        int leftBound;
        int rightBound;
        int thumbWidth = thumb.getIntrinsicWidth();
        int thumbHeight = thumb.getIntrinsicHeight();
        int thumbPos = (int) ((1.0f - scale) * ((float) ((((h - this.paddingTop) - this.paddingBottom) - thumbHeight) + (this.thumbOffset * 2))));
        if (gap == Integer.MIN_VALUE) {
            Rect oldBounds = thumb.getBounds();
            leftBound = oldBounds.left;
            rightBound = oldBounds.right;
        } else {
            leftBound = gap;
            rightBound = gap + thumbWidth;
        }
        thumb.setBounds(leftBound, thumbPos, rightBound, thumbPos + thumbHeight);
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.thumbDrawable != null) {
            canvas.save();
            canvas.translate((float) this.paddingLeft, (float) (this.paddingTop - this.thumbOffset));
            this.thumbDrawable.draw(canvas);
            canvas.restore();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable d = getCurrentDrawable();
        int thumbWidth = this.thumbDrawable == null ? 0 : this.thumbDrawable.getIntrinsicWidth();
        int dw = 0;
        int dh = 0;
        if (d != null) {
            int dw2 = Math.max(this.minWidth, Math.min(this.maxWidth, d.getIntrinsicWidth()));
            dw = Math.max(thumbWidth, 0);
            dh = Math.max(this.minHeight, Math.min(this.maxHeight, d.getIntrinsicHeight()));
        }
        setMeasuredDimension(resolveSize(dw + this.paddingLeft + this.paddingRight, widthMeasureSpec), resolveSize(dh + this.paddingTop + this.paddingBottom, heightMeasureSpec));
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!this.isUserSeekable || !isEnabled()) {
            return false;
        }
        switch (event.getAction()) {
            case 0:
                setPressed(true);
                onStartTrackingTouch();
                trackTouchEvent(event);
                return true;
            case 1:
                trackTouchEvent(event);
                onStopTrackingTouch();
                setPressed(false);
                invalidate();
                return true;
            case 2:
                trackTouchEvent(event);
                attemptClaimDrag();
                return true;
            case 3:
                onStopTrackingTouch();
                setPressed(false);
                invalidate();
                return true;
            default:
                return true;
        }
    }

    private void trackTouchEvent(MotionEvent event) {
        float scale;
        int height = getHeight();
        int available = (height - this.paddingTop) - this.paddingBottom;
        int y = height - ((int) event.getY());
        float progress = 0.0f;
        if (y < this.paddingBottom) {
            scale = 0.0f;
        } else if (y > height - this.paddingTop) {
            scale = 1.0f;
        } else {
            scale = ((float) (y - this.paddingBottom)) / ((float) available);
            progress = this.touchProgressOffset;
        }
        setProgress((int) (progress + (((float) getMax()) * scale)), true);
    }

    private void attemptClaimDrag() {
        if (this.parentView != null) {
            this.parentView.requestDisallowInterceptTouchEvent(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void onStartTrackingTouch() {
    }

    /* access modifiers changed from: package-private */
    public void onStopTrackingTouch() {
    }

    /* access modifiers changed from: package-private */
    public void onKeyChange() {
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int progress = getProgress();
        switch (keyCode) {
            case 19:
                if (progress < getMax()) {
                    setProgress(this.keyProgressIncrement + progress, true);
                    onKeyChange();
                    return true;
                }
                break;
            case 20:
                if (progress > 0) {
                    setProgress(progress - this.keyProgressIncrement, true);
                    onKeyChange();
                    return true;
                }
                break;
        }
        return super.onKeyDown(keyCode, event);
    }
}
