package com.biznessapps.widgets;

import android.content.Context;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.Gallery;

public class TabGallery extends Gallery {
    private final Camera camera;
    private int galleryCenter;
    private int maxRotationAngle;
    private int maxZoom;

    public TabGallery(Context context) {
        super(context);
        this.camera = new Camera();
        this.maxZoom = -100;
        this.maxRotationAngle = 70;
        setStaticTransformationsEnabled(true);
    }

    public TabGallery(Context context, AttributeSet attrs) {
        this(context, attrs, 16842864);
    }

    public TabGallery(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.camera = new Camera();
        this.maxZoom = -100;
        this.maxRotationAngle = 70;
        setStaticTransformationsEnabled(true);
    }

    private int getGalleryCenter() {
        return (((getWidth() - getPaddingLeft()) - getPaddingRight()) / 2) + getPaddingLeft();
    }

    private static int getViewCenter(View view) {
        return view.getLeft() + (view.getWidth() / 2);
    }

    /* access modifiers changed from: protected */
    public boolean getChildStaticTransformation(View child, Transformation t) {
        int childCenter = getViewCenter(child);
        int childWidth = child.getWidth();
        t.clear();
        t.setTransformationType(Transformation.TYPE_MATRIX);
        if (childCenter == this.galleryCenter) {
            transformImageBitmap(child, t, 0);
            return true;
        }
        int rotationAngle = (int) ((((float) (this.galleryCenter - childCenter)) / ((float) childWidth)) * ((float) this.maxRotationAngle));
        if (Math.abs(rotationAngle) > this.maxRotationAngle) {
            rotationAngle = rotationAngle < 0 ? -this.maxRotationAngle : this.maxRotationAngle;
        }
        transformImageBitmap(child, t, rotationAngle);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        this.galleryCenter = getGalleryCenter();
        super.onSizeChanged(width, height, oldWidth, oldHeight);
    }

    private void transformImageBitmap(View child, Transformation t, int rotationAngle) {
        this.camera.save();
        Matrix imageMatrix = t.getMatrix();
        int height = child.getMeasuredHeight();
        int width = child.getMeasuredWidth();
        int rotation = Math.abs(rotationAngle);
        this.camera.translate(0.0f, 0.0f, 100.0f);
        if (rotation < this.maxRotationAngle) {
            this.camera.translate(0.0f, 0.0f, (float) (((double) this.maxZoom) + (((double) rotation) * 1.5d)));
        }
        this.camera.rotateY((float) rotationAngle);
        this.camera.getMatrix(imageMatrix);
        imageMatrix.preTranslate(-(((float) width) / 2.0f), -(((float) height) / 2.0f));
        imageMatrix.postTranslate(((float) width) / 2.0f, ((float) height) / 2.0f);
        this.camera.restore();
    }
}
