package com.biznessapps.widgets.wheel;

import android.content.Context;

public class DigitalWheelAdapter extends AbstractWheelTextAdapter {
    private String format;
    private int maxValue;
    private int minValue;

    public DigitalWheelAdapter(Context context, int minValue2, int maxValue2) {
        this(context, minValue2, maxValue2, null);
    }

    public DigitalWheelAdapter(Context context, int minValue2, int maxValue2, String format2) {
        super(context);
        this.minValue = minValue2;
        this.maxValue = maxValue2;
        this.format = format2;
    }

    public CharSequence getItemText(int index) {
        if (index < 0 || index >= getItemsCount()) {
            return null;
        }
        int value = this.minValue + index;
        if (this.format == null) {
            return Integer.toString(value);
        }
        return String.format(this.format, Integer.valueOf(value));
    }

    public int getItemsCount() {
        return (this.maxValue - this.minValue) + 1;
    }
}
