package com.biznessapps.widgets;

import android.content.Context;
import android.util.AttributeSet;

public class VerticalSeekBar extends AbsVerticalSeekBar {
    private OnSeekBarChangeListener onSeekBarChangeListener;

    public interface OnSeekBarChangeListener {
        void onProgressChanged(VerticalSeekBar verticalSeekBar, int i, boolean z);

        void onStartTrackingTouch(VerticalSeekBar verticalSeekBar);

        void onStopTrackingTouch(VerticalSeekBar verticalSeekBar);
    }

    public VerticalSeekBar(Context context) {
        this(context, null);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 16842875);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: package-private */
    public void onProgressRefresh(float scale, boolean fromUser) {
        super.onProgressRefresh(scale, fromUser);
        if (this.onSeekBarChangeListener != null) {
            this.onSeekBarChangeListener.onProgressChanged(this, getProgress(), fromUser);
        }
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener l) {
        this.onSeekBarChangeListener = l;
    }

    /* access modifiers changed from: package-private */
    public void onStartTrackingTouch() {
        if (this.onSeekBarChangeListener != null) {
            this.onSeekBarChangeListener.onStartTrackingTouch(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void onStopTrackingTouch() {
        if (this.onSeekBarChangeListener != null) {
            this.onSeekBarChangeListener.onStopTrackingTouch(this);
        }
    }
}
