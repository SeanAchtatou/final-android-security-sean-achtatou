package com.biznessapps.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.biznessapps.adapters.AbstractAdapter;

public class RefreshableListView extends ListView {
    private AbstractAdapter.PositionListener positionListener;

    public void setPositionListener(AbstractAdapter.PositionListener positionListener2) {
        this.positionListener = positionListener2;
    }

    public RefreshableListView(Context context) {
        super(context);
    }

    public RefreshableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RefreshableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
        if (adapter instanceof AbstractAdapter) {
            ((AbstractAdapter) adapter).addPositionListener(this.positionListener);
        }
    }

    public void removeListeners() {
        if (getAdapter() != null && (getAdapter() instanceof AbstractAdapter)) {
            ((AbstractAdapter) getAdapter()).removePositionListener(this.positionListener);
        }
    }
}
