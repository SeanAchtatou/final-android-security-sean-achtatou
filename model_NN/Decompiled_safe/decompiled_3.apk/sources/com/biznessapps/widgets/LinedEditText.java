package com.biznessapps.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;

public class LinedEditText extends EditText {
    private Paint mPaint = new Paint();
    private Rect mRect = new Rect();

    public LinedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.mPaint.setColor(-16777216);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int count = getHeight() / getLineHeight();
        if (getLineCount() > count) {
            count = getLineCount();
        }
        Rect r = this.mRect;
        Paint paint = this.mPaint;
        int baseline = getLineBounds(0, r);
        for (int i = 0; i < count; i++) {
            canvas.drawLine((float) r.left, (float) (baseline + 1), (float) r.right, (float) (baseline + 1), paint);
            baseline += getLineHeight();
        }
        super.onDraw(canvas);
    }
}
