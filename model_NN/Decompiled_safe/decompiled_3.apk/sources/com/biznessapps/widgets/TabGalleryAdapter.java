package com.biznessapps.widgets;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.layout.R;
import java.util.List;

public class TabGalleryAdapter extends AbstractAdapter<Button> {
    public TabGalleryAdapter(Context context, List<Button> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View viewItem = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
        ViewGroup tabContainer = (ViewGroup) viewItem.findViewById(R.id.item_layout);
        Button item = (Button) this.items.get(position);
        if (item != null) {
            if (((ViewGroup) item.getParent()) != null) {
                return (ViewGroup) item.getParent();
            }
            tabContainer.addView(item);
        }
        return viewItem;
    }
}
