package com.biznessapps.utils.google.caching;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.View;
import android.widget.ImageView;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.utils.google.caching.ImageCache;
import java.lang.ref.WeakReference;

public abstract class ImageWorker {
    private static final int MESSAGE_CLEAR = 0;
    private static final int MESSAGE_CLOSE = 3;
    private static final int MESSAGE_FLUSH = 2;
    private static final int MESSAGE_INIT_DISK_CACHE = 1;
    /* access modifiers changed from: private */
    public ImageCache mImageCache;
    private ImageCache.ImageCacheParams mImageCacheParams;
    private Bitmap mLoadingBitmap;
    protected Resources mResources;

    public interface ImageLoadCallback {
        void onImageLoading(Bitmap bitmap, View view);
    }

    /* access modifiers changed from: protected */
    public abstract Bitmap processBitmap(Object obj, int i);

    protected ImageWorker(Context context) {
        this.mResources = context.getResources();
    }

    public void loadBigImage(String data, View view) {
        loadImage(data, view, null, null, 3);
    }

    public void loadGalleryImage(String data, View view) {
        loadImage(data, view, null, null, 4);
    }

    public void loadImage(String data, View view) {
        loadImage(data, view, null, null, 1);
    }

    public void loadAppImage(String data, View view) {
        loadImage(data, view, null, null, 2);
    }

    public void loadImage(String url, View view, ImageLoadCallback loadCallback) {
        loadImage(url, view, loadCallback, null, 2);
    }

    public void loadImage(String url, View view, TintContainer tint) {
        loadImage(url, view, null, tint, 2);
    }

    public void loadImage(String url, View view, ImageLoadCallback loadCallback, TintContainer tint, int imageType) {
        ImageLoadParams params = new ImageLoadParams();
        params.setView(view);
        params.setLoadCallback(loadCallback);
        params.setTint(tint);
        params.setUrl(url);
        params.setImageType(imageType);
        loadImage(params);
    }

    public void loadImage(ImageLoadParams params) {
        String url = params.getUrl();
        TintContainer tint = params.getTint();
        View view = params.getView();
        if (!StringUtils.isEmpty(url)) {
            Bitmap bitmap = null;
            if (this.mImageCache != null) {
                bitmap = this.mImageCache.getBitmapFromMemCache(url + params.getImageType());
            }
            if (bitmap != null) {
                if (params.getLoadCallback() != null) {
                    params.getLoadCallback().onImageLoading(bitmap, params.getView());
                } else if (params.isImageSrc() && (params.getView() instanceof ImageView)) {
                    ((ImageView) params.getView()).setImageBitmap(bitmap);
                } else if (tint != null) {
                    params.getView().setBackgroundDrawable(composeDrawable(new BitmapDrawable(bitmap), tint));
                } else {
                    params.getView().setBackgroundDrawable(new BitmapDrawable(bitmap));
                }
            } else if (cancelPotentialWork(url, params)) {
                BitmapWorkerTask task = new BitmapWorkerTask(params);
                AsyncDrawable asyncDrawable = new AsyncDrawable(this.mResources, this.mLoadingBitmap, task);
                if (params.getView() instanceof ImageView) {
                    ((ImageView) params.getView()).setImageDrawable(asyncDrawable);
                } else {
                    params.getView().setTag(asyncDrawable);
                }
                task.executeOnExecutor(AsyncTask.DUAL_THREAD_EXECUTOR, url);
            }
        } else if (tint != null) {
            view.setBackgroundDrawable(composeDrawable(tint));
        }
    }

    public void setLoadingImage(Bitmap bitmap) {
        this.mLoadingBitmap = bitmap;
    }

    public void setLoadingImage(int resId) {
        this.mLoadingBitmap = null;
    }

    public void addImageCache(ImageCache.ImageCacheParams cacheParams) {
        this.mImageCacheParams = cacheParams;
        setImageCache(ImageCache.findOrCreateCache(this.mImageCacheParams.cacheName, this.mImageCacheParams));
        new CacheAsyncTask().execute(1);
    }

    public void setImageCache(ImageCache imageCache) {
        this.mImageCache = imageCache;
    }

    public static void cancelWork(View view) {
        BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(view);
        if (bitmapWorkerTask != null) {
            bitmapWorkerTask.cancel(true);
        }
    }

    public static boolean cancelPotentialWork(Object data, ImageLoadParams params) {
        BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(params.getView());
        if (bitmapWorkerTask == null) {
            return true;
        }
        String bitmapData = bitmapWorkerTask.dataString;
        if (bitmapData != null && bitmapData.equals(data)) {
            return false;
        }
        bitmapWorkerTask.cancel(true);
        return true;
    }

    /* access modifiers changed from: private */
    public static BitmapWorkerTask getBitmapWorkerTask(View view) {
        Drawable drawable;
        if (view != null) {
            if (view instanceof ImageView) {
                drawable = ((ImageView) view).getDrawable();
            } else {
                drawable = (Drawable) view.getTag();
            }
            if (drawable instanceof AsyncDrawable) {
                return ((AsyncDrawable) drawable).getBitmapWorkerTask();
            }
        }
        return null;
    }

    private class BitmapWorkerTask extends AsyncTask<Object, Void, Bitmap> {
        /* access modifiers changed from: private */
        public String dataString;
        private final ImageLoadParams imageParams;
        private final WeakReference<View> imageViewReference;

        public BitmapWorkerTask(ImageLoadParams imageParams2) {
            this.imageParams = imageParams2;
            this.imageViewReference = new WeakReference<>(imageParams2.getView());
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(Object... params) {
            this.dataString = (String) params[0];
            Bitmap bitmap = null;
            if (!(ImageWorker.this.mImageCache == null || isCancelled() || getAttachedImageView() == null)) {
                bitmap = ImageWorker.this.mImageCache.getBitmapFromDiskCache(this.dataString + this.imageParams.getImageType());
            }
            if (bitmap == null && !isCancelled() && getAttachedImageView() != null) {
                bitmap = ImageWorker.this.processBitmap(params[0], this.imageParams.getImageType());
            }
            if (!(bitmap == null || ImageWorker.this.mImageCache == null)) {
                ImageWorker.this.mImageCache.addBitmapToCache(this.dataString + this.imageParams.getImageType(), bitmap);
            }
            return bitmap;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            View view = getAttachedImageView();
            if (bitmap != null && view != null) {
                ImageWorker.this.setImageBitmap(view, this.imageParams, bitmap);
            }
        }

        private View getAttachedImageView() {
            View view = this.imageViewReference.get();
            if (this == ImageWorker.getBitmapWorkerTask(view)) {
                return view;
            }
            return null;
        }
    }

    private static class AsyncDrawable extends ColorDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(0);
            this.bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return this.bitmapWorkerTaskReference.get();
        }
    }

    /* access modifiers changed from: private */
    public void setImageBitmap(View view, ImageLoadParams params, Bitmap bitmap) {
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            if (params.getLoadCallback() != null) {
                params.getLoadCallback().onImageLoading(bitmap, imageView);
                return;
            }
            TintContainer tint = params.getTint();
            if (params.isImageSrc()) {
                imageView.setImageBitmap(bitmap);
            } else if (tint != null) {
                imageView.setBackgroundDrawable(composeDrawable(new BitmapDrawable(bitmap), tint));
            } else {
                imageView.setBackgroundDrawable(new BitmapDrawable(bitmap));
            }
        } else {
            TintContainer tint2 = params.getTint();
            if (tint2 != null) {
                view.setBackgroundDrawable(composeDrawable(new BitmapDrawable(bitmap), tint2));
            } else {
                view.setBackgroundDrawable(new BitmapDrawable(bitmap));
            }
        }
    }

    protected class CacheAsyncTask extends AsyncTask<Object, Void, Void> {
        protected CacheAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public Void doInBackground(Object... params) {
            switch (((Integer) params[0]).intValue()) {
                case 0:
                    ImageWorker.this.clearCacheInternal();
                    return null;
                case 1:
                    ImageWorker.this.initDiskCacheInternal();
                    return null;
                case 2:
                    ImageWorker.this.flushCacheInternal();
                    return null;
                case 3:
                    ImageWorker.this.closeCacheInternal();
                    return null;
                default:
                    return null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void initDiskCacheInternal() {
        if (this.mImageCache != null) {
            this.mImageCache.initDiskCache();
        }
    }

    /* access modifiers changed from: protected */
    public void clearCacheInternal() {
        if (this.mImageCache != null) {
            this.mImageCache.clearCache();
        }
    }

    /* access modifiers changed from: protected */
    public void flushCacheInternal() {
        if (this.mImageCache != null) {
            this.mImageCache.flush();
        }
    }

    /* access modifiers changed from: protected */
    public void closeCacheInternal() {
        if (this.mImageCache != null) {
            this.mImageCache.close();
            this.mImageCache = null;
        }
    }

    public ImageCache getCache() {
        return this.mImageCache;
    }

    public void clearCache() {
        new CacheAsyncTask().execute(0);
    }

    public void flushCache() {
        new CacheAsyncTask().execute(2);
    }

    public void closeCache() {
        new CacheAsyncTask().execute(3);
    }

    private static Drawable composeDrawable(Drawable level1, TintContainer tint) {
        Drawable[] layers = {level1, new ColorDrawable(ViewUtils.getColor(tint.getTintColor(), 0))};
        layers[1].setAlpha((int) tint.getTintOpacity());
        return new LayerDrawable(layers).getCurrent();
    }

    private static Drawable composeDrawable(TintContainer tint) {
        Drawable[] layers = {new ColorDrawable(ViewUtils.getColor(tint.getTintColor(), 0))};
        layers[0].setAlpha((int) tint.getTintOpacity());
        return new LayerDrawable(layers).getCurrent();
    }

    public static class ImageLoadParams {
        private int imageType = 1;
        private boolean isImageSrc;
        private ImageLoadCallback loadCallback;
        private TintContainer tint;
        private String url;
        private View view;

        public View getView() {
            return this.view;
        }

        public void setView(View view2) {
            this.view = view2;
        }

        public ImageLoadCallback getLoadCallback() {
            return this.loadCallback;
        }

        public void setLoadCallback(ImageLoadCallback loadCallback2) {
            this.loadCallback = loadCallback2;
        }

        public TintContainer getTint() {
            return this.tint;
        }

        public void setTint(TintContainer tint2) {
            this.tint = tint2;
        }

        public boolean isImageSrc() {
            return this.isImageSrc;
        }

        public String getUrl() {
            return this.url;
        }

        public void setUrl(String url2) {
            this.url = url2;
        }

        public void setImageSrc(boolean isImageSrc2) {
            this.isImageSrc = isImageSrc2;
        }

        public int getImageType() {
            return this.imageType;
        }

        public void setImageType(int imageType2) {
            this.imageType = imageType2;
        }
    }

    public static class TintContainer {
        private String tintColor;
        private float tintOpacity;

        public String getTintColor() {
            return this.tintColor;
        }

        public void setTintColor(String tintColor2) {
            this.tintColor = tintColor2;
        }

        public float getTintOpacity() {
            return this.tintOpacity;
        }

        public void setTintOpacity(float tintOpacity2) {
            this.tintOpacity = tintOpacity2;
        }
    }
}
