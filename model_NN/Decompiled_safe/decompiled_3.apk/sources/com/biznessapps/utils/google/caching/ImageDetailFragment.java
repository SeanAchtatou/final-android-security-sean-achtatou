package com.biznessapps.utils.google.caching;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ImageDetailFragment extends Fragment {
    private static final String IMAGE_DATA_EXTRA = "extra_image_data";
    private static final String IMAGE_VIEW_EXTRA = "extra_imageview_id";
    private static final String LAYOUT_EXTRA = "extra_layout_id";
    private ImageFetcher mImageFetcher;
    private String mImageUrl;
    private ImageView mImageView;
    private int mImageViewId;
    private int mLayoutId;

    public static ImageDetailFragment newInstance(ImageFetcher fetcher, String imageUrl, int layoutId, int imageViewId) {
        ImageDetailFragment f = new ImageDetailFragment();
        f.setImageFetcher(fetcher);
        Bundle args = new Bundle();
        args.putString(IMAGE_DATA_EXTRA, imageUrl);
        args.putInt(LAYOUT_EXTRA, layoutId);
        args.putInt(IMAGE_VIEW_EXTRA, imageViewId);
        f.setArguments(args);
        return f;
    }

    public void setImageFetcher(ImageFetcher fetcher) {
        this.mImageFetcher = fetcher;
    }

    public void onCreate(Bundle savedInstanceState) {
        String str;
        Integer num;
        Integer num2 = null;
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            str = getArguments().getString(IMAGE_DATA_EXTRA);
        } else {
            str = null;
        }
        this.mImageUrl = str;
        if (getArguments() != null) {
            num = Integer.valueOf(getArguments().getInt(LAYOUT_EXTRA));
        } else {
            num = null;
        }
        this.mLayoutId = num.intValue();
        if (getArguments() != null) {
            num2 = Integer.valueOf(getArguments().getInt(IMAGE_VIEW_EXTRA));
        }
        this.mImageViewId = num2.intValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(this.mLayoutId, container, false);
        this.mImageView = (ImageView) v.findViewById(this.mImageViewId);
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mImageFetcher.loadAppImage(this.mImageUrl, this.mImageView);
        if (View.OnClickListener.class.isInstance(getActivity()) && Utils.hasHoneycomb()) {
            this.mImageView.setOnClickListener((View.OnClickListener) getActivity());
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mImageView != null) {
            ImageWorker.cancelWork(this.mImageView);
            this.mImageView.setImageDrawable(null);
        }
    }
}
