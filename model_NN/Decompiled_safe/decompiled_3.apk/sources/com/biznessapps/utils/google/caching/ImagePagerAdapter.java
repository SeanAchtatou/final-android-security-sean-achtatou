package com.biznessapps.utils.google.caching;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ImagePagerAdapter extends FragmentStatePagerAdapter {
    private ImageFetcher mImageFetcher;
    private String[] mImageUrls;
    private int mImageViewId;
    private int mLayoutId;

    public ImagePagerAdapter(FragmentManager fm, ImageFetcher fetcher, String[] imageUrls, int layoutId, int imageViewId) {
        super(fm);
        this.mImageFetcher = fetcher;
        this.mImageUrls = imageUrls;
        this.mImageViewId = imageViewId;
        this.mLayoutId = layoutId;
    }

    public int getCount() {
        return this.mImageUrls.length;
    }

    public Fragment getItem(int position) {
        return ImageDetailFragment.newInstance(this.mImageFetcher, this.mImageUrls[position], this.mLayoutId, this.mImageViewId);
    }
}
