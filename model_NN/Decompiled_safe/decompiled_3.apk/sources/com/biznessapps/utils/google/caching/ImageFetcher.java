package com.biznessapps.utils.google.caching;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.biznessapps.images.BitmapDownloader;
import java.io.File;
import java.io.IOException;

public class ImageFetcher extends ImageResizer {
    private static final int DISK_CACHE_INDEX = 0;
    private static final String HTTP_CACHE_DIR = "http";
    private static final int HTTP_CACHE_SIZE = 1048576;
    private static final int IO_BUFFER_SIZE = 8192;
    private static final String TAG = "ImageFetcher";
    private File mHttpCacheDir;
    private DiskLruCache mHttpDiskCache;
    private final Object mHttpDiskCacheLock = new Object();
    private boolean mHttpDiskCacheStarting = true;

    private ImageFetcher(Context context, int imageWidth, int imageHeight) {
        super(context, imageWidth, imageHeight);
    }

    public ImageFetcher(Context context, int imageSize, String cacheName) {
        super(context, imageSize);
    }

    private void init(Context context, String cacheName) {
        checkConnection(context);
        this.mHttpCacheDir = ImageCache.getDiskCacheDir(context, HTTP_CACHE_DIR + cacheName);
    }

    /* access modifiers changed from: protected */
    public void initDiskCacheInternal() {
        super.initDiskCacheInternal();
    }

    private void initHttpDiskCache() {
        if (!this.mHttpCacheDir.exists()) {
            this.mHttpCacheDir.mkdirs();
        }
        synchronized (this.mHttpDiskCacheLock) {
            if (ImageCache.getUsableSpace(this.mHttpCacheDir) > 1048576) {
                try {
                    this.mHttpDiskCache = DiskLruCache.open(this.mHttpCacheDir, 1, 1, 1048576);
                } catch (IOException e) {
                    this.mHttpDiskCache = null;
                }
            }
            this.mHttpDiskCacheStarting = false;
            this.mHttpDiskCacheLock.notifyAll();
        }
    }

    /* access modifiers changed from: protected */
    public void clearCacheInternal() {
        super.clearCacheInternal();
    }

    /* access modifiers changed from: protected */
    public void flushCacheInternal() {
        super.flushCacheInternal();
    }

    /* access modifiers changed from: protected */
    public void closeCacheInternal() {
        super.closeCacheInternal();
    }

    private void checkConnection(Context context) {
        NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
        }
    }

    private Bitmap processBitmap(String data, int imageType) {
        int imageSize = getImageSize(imageType);
        return BitmapDownloader.downloadBitmap(data, imageSize, imageSize);
    }

    /* access modifiers changed from: protected */
    public Bitmap processBitmap(Object data, int imageType) {
        return processBitmap(String.valueOf(data), imageType);
    }

    /* JADX WARN: Type inference failed for: r9v5, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0052 A[SYNTHETIC, Splitter:B:17:0x0052] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0057 A[Catch:{ IOException -> 0x0089 }] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0077 A[SYNTHETIC, Splitter:B:35:0x0077] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007c A[Catch:{ IOException -> 0x0080 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean downloadUrlToStream(java.lang.String r13, java.io.OutputStream r14) {
        /*
            r12 = this;
            disableConnectionReuseIfNecessary()
            r8 = 0
            r5 = 0
            r3 = 0
            java.net.URL r7 = new java.net.URL     // Catch:{ IOException -> 0x008b }
            r7.<init>(r13)     // Catch:{ IOException -> 0x008b }
            java.net.URLConnection r9 = r7.openConnection()     // Catch:{ IOException -> 0x008b }
            r0 = r9
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x008b }
            r8 = r0
            java.io.BufferedInputStream r4 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x008b }
            java.io.InputStream r9 = r8.getInputStream()     // Catch:{ IOException -> 0x008b }
            r10 = 8192(0x2000, float:1.14794E-41)
            r4.<init>(r9, r10)     // Catch:{ IOException -> 0x008b }
            java.io.BufferedOutputStream r6 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x008d, all -> 0x0082 }
            r9 = 8192(0x2000, float:1.14794E-41)
            r6.<init>(r14, r9)     // Catch:{ IOException -> 0x008d, all -> 0x0082 }
        L_0x0025:
            int r1 = r4.read()     // Catch:{ IOException -> 0x0030, all -> 0x0085 }
            r9 = -1
            if (r1 == r9) goto L_0x005c
            r6.write(r1)     // Catch:{ IOException -> 0x0030, all -> 0x0085 }
            goto L_0x0025
        L_0x0030:
            r2 = move-exception
            r3 = r4
            r5 = r6
        L_0x0033:
            java.lang.String r9 = "ImageFetcher"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x006f }
            r10.<init>()     // Catch:{ all -> 0x006f }
            java.lang.String r11 = "Error in downloadBitmap - "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ all -> 0x006f }
            java.lang.StringBuilder r10 = r10.append(r2)     // Catch:{ all -> 0x006f }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x006f }
            android.util.Log.e(r9, r10)     // Catch:{ all -> 0x006f }
            if (r8 == 0) goto L_0x0050
            r8.disconnect()
        L_0x0050:
            if (r5 == 0) goto L_0x0055
            r5.close()     // Catch:{ IOException -> 0x0089 }
        L_0x0055:
            if (r3 == 0) goto L_0x005a
            r3.close()     // Catch:{ IOException -> 0x0089 }
        L_0x005a:
            r9 = 0
        L_0x005b:
            return r9
        L_0x005c:
            r9 = 1
            if (r8 == 0) goto L_0x0062
            r8.disconnect()
        L_0x0062:
            if (r6 == 0) goto L_0x0067
            r6.close()     // Catch:{ IOException -> 0x0090 }
        L_0x0067:
            if (r4 == 0) goto L_0x006c
            r4.close()     // Catch:{ IOException -> 0x0090 }
        L_0x006c:
            r3 = r4
            r5 = r6
            goto L_0x005b
        L_0x006f:
            r9 = move-exception
        L_0x0070:
            if (r8 == 0) goto L_0x0075
            r8.disconnect()
        L_0x0075:
            if (r5 == 0) goto L_0x007a
            r5.close()     // Catch:{ IOException -> 0x0080 }
        L_0x007a:
            if (r3 == 0) goto L_0x007f
            r3.close()     // Catch:{ IOException -> 0x0080 }
        L_0x007f:
            throw r9
        L_0x0080:
            r10 = move-exception
            goto L_0x007f
        L_0x0082:
            r9 = move-exception
            r3 = r4
            goto L_0x0070
        L_0x0085:
            r9 = move-exception
            r3 = r4
            r5 = r6
            goto L_0x0070
        L_0x0089:
            r9 = move-exception
            goto L_0x005a
        L_0x008b:
            r2 = move-exception
            goto L_0x0033
        L_0x008d:
            r2 = move-exception
            r3 = r4
            goto L_0x0033
        L_0x0090:
            r10 = move-exception
            goto L_0x006c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.utils.google.caching.ImageFetcher.downloadUrlToStream(java.lang.String, java.io.OutputStream):boolean");
    }

    public static void disableConnectionReuseIfNecessary() {
        if (Build.VERSION.SDK_INT < 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }
}
