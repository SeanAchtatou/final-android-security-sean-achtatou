package com.biznessapps.utils.google.caching;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import com.biznessapps.utils.google.caching.ImageCache;
import java.util.List;

public class BitmapCacheHelper {
    private static String DEFAULT_CACHE_DIR = "imgCache";
    private static float DEFAULT_CACHE_SIZE = 0.25f;

    public static ImageFetcher createImageFetcher(Context context, int resId, int imageSize) {
        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(context, DEFAULT_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(DEFAULT_CACHE_SIZE);
        ImageFetcher mImageFetcher = new ImageFetcher(context, imageSize);
        mImageFetcher.setLoadingImage(resId);
        mImageFetcher.addImageCache(cacheParams);
        return mImageFetcher;
    }

    public static ImageFetcher createImageFetcher(Context context, int resId, int imageSize, ImageCache.ImageCacheParams cacheParams) {
        ImageFetcher mImageFetcher = new ImageFetcher(context, imageSize);
        mImageFetcher.setLoadingImage(resId);
        mImageFetcher.addImageCache(cacheParams);
        return mImageFetcher;
    }

    public static ImageFetcher createImageFetcher(Context context, int resId, int imageSize, String cacheName, float cacheSize) {
        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(context, cacheName);
        cacheParams.setMemCacheSizePercent(cacheSize);
        ImageFetcher mImageFetcher = new ImageFetcher(context, imageSize);
        mImageFetcher.setLoadingImage(resId);
        mImageFetcher.addImageCache(cacheParams);
        return mImageFetcher;
    }

    public static ImageGridAdapter createImageGridAdapter(Context context, ImageFetcher fetcher, List<String> imageUrls) {
        return new ImageGridAdapter(context, fetcher, imageUrls);
    }

    public static ImagePagerAdapter createImagePagerAdapter(FragmentManager fm, ImageFetcher fetcher, String[] imageUrls, int layoutId, int imageViewId) {
        return new ImagePagerAdapter(fm, fetcher, imageUrls, layoutId, imageViewId);
    }
}
