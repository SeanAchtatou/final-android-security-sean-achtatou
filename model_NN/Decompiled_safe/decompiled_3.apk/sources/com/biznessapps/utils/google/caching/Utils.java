package com.biznessapps.utils.google.caching;

import android.os.Build;

public class Utils {
    private Utils() {
    }

    public static boolean hasFroyo() {
        return Build.VERSION.SDK_INT >= 8;
    }

    public static boolean hasGingerbread() {
        return false;
    }

    public static boolean hasHoneycomb() {
        return false;
    }

    public static boolean hasHoneycombMR1() {
        return false;
    }

    public static boolean hasJellyBean() {
        return false;
    }
}
