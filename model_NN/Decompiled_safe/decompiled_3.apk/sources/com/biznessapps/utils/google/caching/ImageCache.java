package com.biznessapps.utils.google.caching;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;
import android.util.Log;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.utils.google.caching.DiskLruCache;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class ImageCache {
    private static final String DEFAULT_CACHE_NAME = "memCache";
    private static final boolean DEFAULT_CLEAR_DISK_CACHE_ON_START = false;
    /* access modifiers changed from: private */
    public static final Bitmap.CompressFormat DEFAULT_COMPRESS_FORMAT = Bitmap.CompressFormat.PNG;
    private static final int DEFAULT_COMPRESS_QUALITY = 70;
    private static final boolean DEFAULT_DISK_CACHE_ENABLED = true;
    private static final int DEFAULT_DISK_CACHE_SIZE = 73400320;
    private static final boolean DEFAULT_INIT_DISK_CACHE_ON_CREATE = false;
    private static final boolean DEFAULT_MEM_CACHE_ENABLED = true;
    private static final int DEFAULT_MEM_CACHE_SIZE = 10240;
    private static final int DISK_CACHE_INDEX = 0;
    private static final String TAG = "ImageCache";
    private static HashMap<String, ImageCache> instances = new HashMap<>();
    private ImageCacheParams mCacheParams;
    private final Object mDiskCacheLock = new Object();
    private boolean mDiskCacheStarting = true;
    private DiskLruCache mDiskLruCache;
    private LruCache<String, Bitmap> mMemoryCache;

    private ImageCache(ImageCacheParams cacheParams) {
        init(cacheParams);
    }

    private ImageCache(Context context, String uniqueName) {
        init(new ImageCacheParams(context, uniqueName));
    }

    public static ImageCache findOrCreateCache(String key, ImageCacheParams cacheParams) {
        if (!instances.containsKey(key)) {
            instances.put(key, new ImageCache(cacheParams));
        }
        return instances.get(key);
    }

    private void init(ImageCacheParams cacheParams) {
        this.mCacheParams = cacheParams;
        if (this.mCacheParams.memoryCacheEnabled) {
            this.mMemoryCache = new LruCache<String, Bitmap>(this.mCacheParams.memCacheSize) {
                /* access modifiers changed from: protected */
                public int sizeOf(String key, Bitmap bitmap) {
                    int bitmapSize = ImageCache.getBitmapSize(bitmap) / 1024;
                    if (bitmapSize == 0) {
                        return 1;
                    }
                    return bitmapSize;
                }
            };
        }
        if (cacheParams.initDiskCacheOnCreate) {
            initDiskCache();
        }
    }

    public void initDiskCache() {
        synchronized (this.mDiskCacheLock) {
            if (this.mDiskLruCache == null || this.mDiskLruCache.isClosed()) {
                File diskCacheDir = this.mCacheParams.diskCacheDir;
                if (this.mCacheParams.diskCacheEnabled && diskCacheDir != null) {
                    if (!diskCacheDir.exists()) {
                        diskCacheDir.mkdirs();
                    }
                    if (getUsableSpace(diskCacheDir) > ((long) this.mCacheParams.diskCacheSize)) {
                        try {
                            this.mDiskLruCache = DiskLruCache.open(diskCacheDir, 1, 1, (long) this.mCacheParams.diskCacheSize);
                        } catch (IOException e) {
                            this.mCacheParams.diskCacheDir = null;
                            Log.e(TAG, "initDiskCache - " + e);
                        }
                    }
                }
            }
            this.mDiskCacheStarting = false;
            this.mDiskCacheLock.notifyAll();
        }
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x004d=Splitter:B:23:0x004d, B:58:0x00ae=Splitter:B:58:0x00ae} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void addBitmapToCache(java.lang.String r10, android.graphics.Bitmap r11) {
        /*
            r9 = this;
            if (r10 == 0) goto L_0x0004
            if (r11 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            android.support.v4.util.LruCache<java.lang.String, android.graphics.Bitmap> r5 = r9.mMemoryCache
            if (r5 == 0) goto L_0x0016
            android.support.v4.util.LruCache<java.lang.String, android.graphics.Bitmap> r5 = r9.mMemoryCache
            java.lang.Object r5 = r5.get(r10)
            if (r5 != 0) goto L_0x0016
            android.support.v4.util.LruCache<java.lang.String, android.graphics.Bitmap> r5 = r9.mMemoryCache
            r5.put(r10, r11)
        L_0x0016:
            java.lang.Object r6 = r9.mDiskCacheLock
            monitor-enter(r6)
            com.biznessapps.utils.google.caching.DiskLruCache r5 = r9.mDiskLruCache     // Catch:{ all -> 0x004f }
            if (r5 == 0) goto L_0x004d
            java.lang.String r2 = hashKeyForDisk(r10)     // Catch:{ all -> 0x004f }
            r3 = 0
            com.biznessapps.utils.google.caching.DiskLruCache r5 = r9.mDiskLruCache     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            com.biznessapps.utils.google.caching.DiskLruCache$Snapshot r4 = r5.get(r2)     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            if (r4 != 0) goto L_0x0052
            com.biznessapps.utils.google.caching.DiskLruCache r5 = r9.mDiskLruCache     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            com.biznessapps.utils.google.caching.DiskLruCache$Editor r1 = r5.edit(r2)     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            if (r1 == 0) goto L_0x0048
            r5 = 0
            java.io.OutputStream r3 = r1.newOutputStream(r5)     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            com.biznessapps.utils.google.caching.ImageCache$ImageCacheParams r5 = r9.mCacheParams     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            android.graphics.Bitmap$CompressFormat r5 = r5.compressFormat     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            com.biznessapps.utils.google.caching.ImageCache$ImageCacheParams r7 = r9.mCacheParams     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            int r7 = r7.compressQuality     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            r11.compress(r5, r7, r3)     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            r1.commit()     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            r3.close()     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
        L_0x0048:
            if (r3 == 0) goto L_0x004d
            r3.close()     // Catch:{ IOException -> 0x007f }
        L_0x004d:
            monitor-exit(r6)     // Catch:{ all -> 0x004f }
            goto L_0x0004
        L_0x004f:
            r5 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x004f }
            throw r5
        L_0x0052:
            r5 = 0
            java.io.InputStream r5 = r4.getInputStream(r5)     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            r5.close()     // Catch:{ IOException -> 0x005b, Exception -> 0x0084 }
            goto L_0x0048
        L_0x005b:
            r0 = move-exception
            java.lang.String r5 = "ImageCache"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r7.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r8 = "addBitmapToCache - "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00a8 }
            android.util.Log.e(r5, r7)     // Catch:{ all -> 0x00a8 }
            if (r3 == 0) goto L_0x004d
            r3.close()     // Catch:{ IOException -> 0x007a }
            goto L_0x004d
        L_0x007a:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            goto L_0x004d
        L_0x007f:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            goto L_0x004d
        L_0x0084:
            r0 = move-exception
            java.lang.String r5 = "ImageCache"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a8 }
            r7.<init>()     // Catch:{ all -> 0x00a8 }
            java.lang.String r8 = "addBitmapToCache - "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x00a8 }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ all -> 0x00a8 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x00a8 }
            android.util.Log.e(r5, r7)     // Catch:{ all -> 0x00a8 }
            if (r3 == 0) goto L_0x004d
            r3.close()     // Catch:{ IOException -> 0x00a3 }
            goto L_0x004d
        L_0x00a3:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            goto L_0x004d
        L_0x00a8:
            r5 = move-exception
            if (r3 == 0) goto L_0x00ae
            r3.close()     // Catch:{ IOException -> 0x00af }
        L_0x00ae:
            throw r5     // Catch:{ all -> 0x004f }
        L_0x00af:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            goto L_0x00ae
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.utils.google.caching.ImageCache.addBitmapToCache(java.lang.String, android.graphics.Bitmap):void");
    }

    public Bitmap getBitmapFromMemCache(String data) {
        Bitmap memBitmap;
        if (this.mMemoryCache == null || (memBitmap = this.mMemoryCache.get(data)) == null) {
            return null;
        }
        return memBitmap;
    }

    public Bitmap getBitmapFromDiskCache(String data) {
        Bitmap bitmap;
        String key = hashKeyForDisk(data);
        synchronized (this.mDiskCacheLock) {
            while (this.mDiskCacheStarting) {
                try {
                    this.mDiskCacheLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (this.mDiskLruCache != null) {
                InputStream inputStream = null;
                try {
                    DiskLruCache.Snapshot snapshot = this.mDiskLruCache.get(key);
                    if (snapshot != null && (inputStream = snapshot.getInputStream(0)) != null) {
                        bitmap = BitmapFactory.decodeStream(inputStream);
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e2) {
                                e2.printStackTrace();
                            }
                        }
                    } else if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                    }
                } catch (IOException e4) {
                    Log.e(TAG, "getBitmapFromDiskCache - " + e4);
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e5) {
                            e5.printStackTrace();
                        }
                    }
                } catch (Throwable th) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e6) {
                            e6.printStackTrace();
                        }
                    }
                    throw th;
                }
            }
            bitmap = null;
        }
        return bitmap;
    }

    public void clearCache() {
        if (this.mMemoryCache != null) {
            this.mMemoryCache.evictAll();
        }
        synchronized (this.mDiskCacheLock) {
            this.mDiskCacheStarting = true;
            if (this.mDiskLruCache != null && !this.mDiskLruCache.isClosed()) {
                try {
                    this.mDiskLruCache.delete();
                } catch (IOException e) {
                    Log.e(TAG, "clearCache - " + e);
                }
                this.mDiskLruCache = null;
                initDiskCache();
            }
        }
    }

    public void flush() {
        synchronized (this.mDiskCacheLock) {
            if (this.mDiskLruCache != null) {
                try {
                    this.mDiskLruCache.flush();
                } catch (IOException e) {
                    Log.e(TAG, "flush - " + e);
                }
            }
        }
    }

    public void close() {
        synchronized (this.mDiskCacheLock) {
            if (this.mDiskLruCache != null) {
                try {
                    if (!this.mDiskLruCache.isClosed()) {
                        this.mDiskLruCache.close();
                        this.mDiskLruCache = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "close - " + e);
                }
            }
        }
    }

    public static class ImageCacheParams {
        public String cacheName = ImageCache.DEFAULT_CACHE_NAME;
        public boolean clearDiskCacheOnStart = false;
        public Bitmap.CompressFormat compressFormat = ImageCache.DEFAULT_COMPRESS_FORMAT;
        public int compressQuality = ImageCache.DEFAULT_COMPRESS_QUALITY;
        public File diskCacheDir;
        public boolean diskCacheEnabled = true;
        public int diskCacheSize = ImageCache.DEFAULT_DISK_CACHE_SIZE;
        public boolean initDiskCacheOnCreate = false;
        public int memCacheSize = ImageCache.DEFAULT_MEM_CACHE_SIZE;
        public boolean memoryCacheEnabled = true;

        public ImageCacheParams(Context context, String uniqueName) {
            this.diskCacheDir = ImageCache.getDiskCacheDir(context, uniqueName);
            this.cacheName = uniqueName;
        }

        public ImageCacheParams(File diskCacheDir2) {
            this.diskCacheDir = diskCacheDir2;
            this.cacheName = diskCacheDir2.getName();
        }

        public void setMemCacheSizePercent(float percent) {
            if (percent < 0.05f || percent > 0.8f) {
                throw new IllegalArgumentException("setMemCacheSizePercent - percent must be between 0.05 and 0.8 (inclusive)");
            }
            this.memCacheSize = Math.round((((float) Runtime.getRuntime().maxMemory()) * percent) / 1024.0f);
        }
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        return new File((("mounted".equals(Environment.getExternalStorageState()) || getExternalCacheDir(context) != null) ? getExternalCacheDir(context).getPath() : context.getCacheDir().getPath()) + File.separator + uniqueName);
    }

    public static String hashKeyForDisk(String key) {
        try {
            MessageDigest mDigest = MessageDigest.getInstance(AppConstants.MD5);
            mDigest.update(key.getBytes());
            return bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            return String.valueOf(key.hashCode());
        }
    }

    private static String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            String hex = Integer.toHexString(b & 255);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    public static boolean isExternalStorageRemovable() {
        return true;
    }

    public static File getExternalCacheDir(Context context) {
        if (Utils.hasFroyo()) {
            return context.getExternalCacheDir();
        }
        return new File(Environment.getExternalStorageDirectory().getPath() + ("/Android/data/" + context.getPackageName() + "/cache/"));
    }

    public static long getUsableSpace(File path) {
        StatFs stats = new StatFs(path.getPath());
        return ((long) stats.getBlockSize()) * ((long) stats.getAvailableBlocks());
    }

    public static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
        RetainFragment mRetainFragment = (RetainFragment) fm.findFragmentByTag(TAG);
        if (mRetainFragment != null) {
            return mRetainFragment;
        }
        RetainFragment mRetainFragment2 = new RetainFragment();
        fm.beginTransaction().add(mRetainFragment2, TAG).commitAllowingStateLoss();
        return mRetainFragment2;
    }

    public static class RetainFragment extends Fragment {
        private Object mObject;

        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }

        public void setObject(Object object) {
            this.mObject = object;
        }

        public Object getObject() {
            return this.mObject;
        }
    }
}
