package com.biznessapps.utils.google.caching;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.biznessapps.api.AppCore;
import com.biznessapps.layout.R;
import java.io.FileDescriptor;

public class ImageResizer extends ImageWorker {
    public static final int APP_IMAGE_TYPE = 2;
    public static final int GALLERY_THUMBNAIL_TYPE = 4;
    public static final int LIST_ICON_TYPE = 1;
    public static final int PREVIEW_IMAGE_TYPE = 3;
    protected int mImageHeight;
    protected int mImageWidth;

    public ImageResizer(Context context, int imageWidth, int imageHeight) {
        super(context);
        setImageSize(imageWidth, imageHeight);
    }

    public ImageResizer(Context context, int imageSize) {
        super(context);
        setImageSize(imageSize);
    }

    public void setImageSize(int width, int height) {
        this.mImageWidth = width;
        this.mImageHeight = height;
    }

    public void setImageSize(int size) {
        setImageSize(size, size);
    }

    private Bitmap processBitmap(int resId, int imageType) {
        this.mImageWidth = getImageSize(imageType);
        this.mImageHeight = this.mImageWidth;
        return decodeSampledBitmapFromResource(this.mResources, resId, this.mImageWidth, this.mImageHeight);
    }

    /* access modifiers changed from: protected */
    public int getImageSize(int imageType) {
        if (imageType == 2 || imageType == 4) {
            return this.mResources.getDimensionPixelSize(R.dimen.image_thumbnail_size);
        }
        if (imageType != 3) {
            return this.mResources.getDimensionPixelSize(R.dimen.image_list_size);
        }
        int height = AppCore.getInstance().getDeviceHeight();
        int width = AppCore.getInstance().getDeviceWidth();
        if (height <= width) {
            height = width;
        }
        return height / 2;
    }

    /* access modifiers changed from: protected */
    public Bitmap processBitmap(Object data, int imageType) {
        return processBitmap(Integer.parseInt(String.valueOf(data)), imageType);
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    public static Bitmap decodeSampledBitmapFromDescriptor(FileDescriptor fileDescriptor, int reqWidth, int reqHeight) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int heightRatio = Math.round(((float) height) / ((float) reqHeight));
            int widthRatio = Math.round(((float) width) / ((float) reqWidth));
            if (heightRatio < widthRatio) {
                inSampleSize = heightRatio;
            } else {
                inSampleSize = widthRatio;
            }
            while (((float) (width * height)) / ((float) (inSampleSize * inSampleSize)) > ((float) (reqWidth * reqHeight * 2))) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }
}
