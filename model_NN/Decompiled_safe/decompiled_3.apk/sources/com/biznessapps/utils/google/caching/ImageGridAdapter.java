package com.biznessapps.utils.google.caching;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.util.List;

public class ImageGridAdapter extends BaseAdapter {
    private int mActionBarHeight = 0;
    private final Context mContext;
    private ImageFetcher mImageFetcher;
    private List<String> mImageUrls;
    private AbsListView.LayoutParams mImageViewLayoutParams;
    private int mItemHeight = 0;
    private int mNumColumns = 0;

    public ImageGridAdapter(Context context, ImageFetcher imageFetcher, List<String> imageUrls) {
        this.mImageFetcher = imageFetcher;
        this.mImageUrls = imageUrls;
        this.mContext = context;
        this.mImageViewLayoutParams = new AbsListView.LayoutParams(-1, -1);
    }

    public int getCount() {
        return this.mImageUrls.size() + this.mNumColumns;
    }

    public Object getItem(int position) {
        if (position < this.mNumColumns) {
            return null;
        }
        return this.mImageUrls.get(position - this.mNumColumns);
    }

    public long getItemId(int position) {
        if (position < this.mNumColumns) {
            return 0;
        }
        return (long) (position - this.mNumColumns);
    }

    public int getViewTypeCount() {
        return 2;
    }

    public int getItemViewType(int position) {
        return position < this.mNumColumns ? 1 : 0;
    }

    public boolean hasStableIds() {
        return true;
    }

    public View getView(int position, View convertView, ViewGroup container) {
        ImageView imageView;
        if (position < this.mNumColumns) {
            if (convertView == null) {
                convertView = new View(this.mContext);
            }
            convertView.setLayoutParams(new AbsListView.LayoutParams(-1, this.mActionBarHeight));
            return convertView;
        }
        if (convertView == null) {
            imageView = new ImageView(this.mContext);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(this.mImageViewLayoutParams);
        } else {
            imageView = (ImageView) convertView;
        }
        if (imageView.getLayoutParams().height != this.mItemHeight) {
            imageView.setLayoutParams(this.mImageViewLayoutParams);
        }
        this.mImageFetcher.loadGalleryImage(this.mImageUrls.get(position - this.mNumColumns), imageView);
        return imageView;
    }

    public void setItemHeight(int height) {
        if (height != this.mItemHeight) {
            this.mItemHeight = height;
            this.mImageViewLayoutParams = new AbsListView.LayoutParams(-1, this.mItemHeight);
            this.mImageFetcher.setImageSize(height);
            notifyDataSetChanged();
        }
    }

    public void setNumColumns(int numColumns) {
        this.mNumColumns = numColumns;
    }

    public int getNumColumns() {
        return this.mNumColumns;
    }
}
