package com.biznessapps.utils;

import com.biznessapps.api.AppCore;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.news.NewsSettings;
import com.biznessapps.fragments.news.SearchItem;
import com.biznessapps.fragments.reservation.ReservationDataKeeper;
import com.biznessapps.model.App;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.AroundUsItem;
import com.biznessapps.model.CallUsItem;
import com.biznessapps.model.CommonDataItem;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.CouponItem;
import com.biznessapps.model.EmailPhotoItem;
import com.biznessapps.model.EventItem;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.model.FanWallItem;
import com.biznessapps.model.GalleryData;
import com.biznessapps.model.LocationItem;
import com.biznessapps.model.LocationOpeningTime;
import com.biznessapps.model.LoyaltyItem;
import com.biznessapps.model.MailingListEntity;
import com.biznessapps.model.MenuSectionItem;
import com.biznessapps.model.MessageItem;
import com.biznessapps.model.Mortgage;
import com.biznessapps.model.PlaylistItem;
import com.biznessapps.model.ReservationItem;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.model.ReservationTimeItem;
import com.biznessapps.model.RssItem;
import com.biznessapps.model.StatFieldsItem;
import com.biznessapps.model.Tab;
import com.biznessapps.model.Tip;
import com.biznessapps.model.WebTierItem;
import com.biznessapps.model.YoutubeRssItem;
import com.biznessapps.model.flickr.Galleries;
import com.biznessapps.model.flickr.Photo;
import com.biznessapps.model.flickr.Photos;
import com.biznessapps.model.flickr.Photoset;
import com.biznessapps.model.flickr.RespGalleries;
import com.biznessapps.model.flickr.RespGalleryPhotos;
import com.biznessapps.model.flickr.RespUser;
import com.biznessapps.model.flickr.User;
import com.biznessapps.pushnotifications.RichPushNotification;
import com.facebook.AppEventsConstants;
import com.facebook.internal.NativeProtocol;
import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonParserUtils {
    private static final String ACCESS_TOKEN = "access_token";
    private static final String ACTIVE = "active";
    private static final String ADDRESSES = "addresses";
    private static final String ADDRESS_1 = "address_1";
    private static final String ADDRESS_2 = "address_2";
    private static final String ADMIN_EMAIL = "adminEmail";
    private static final String AD_WHIRL_ID = "AdWhirlID";
    private static final String API_KEY = "APIKEY";
    private static final String APP_ID = "AppID";
    private static final String AUDIO = "audio";
    private static final String BACKGROUND = "background";
    private static final String BUTTON_BG_COLOR = "ButtonBgColor";
    private static final String BUTTON_TEXT_COLOR = "ButtonTextColor";
    private static final String CALL_BUTTON = "CallButton";
    private static final String CATEGORIES = "categories";
    private static final String CAT_ID = "cat_id";
    private static final String CHECKIN_INTERVAL = "checkin_interval";
    private static final String CHECKIN_TARGET = "checkin_target";
    private static final String CITY = "city";
    private static final String CODE = "code";
    private static final String COLOR = "color";
    private static final String COLS = "cols";
    private static final String COMMENT = "comment";
    private static final String COMMENTS = "comments";
    private static final String CONSUMER_KEY = "consumer_key";
    private static final String CONSUMER_SECRET = "consumer_secret";
    private static final String COUPONS_DATA = "couponsData";
    private static final String COUPON_CODE = "couponCode";
    private static final String COUPON_ID = "couponID";
    private static final String COVERFLOW = "coverflow";
    private static final String CREATED_AT = "created_at";
    private static final String CREATOR = "creator";
    private static final String CURRENCY = "currency";
    private static final String CURRENCY_SIGN = "currency_sign";
    private static final String CUSTOM_DESIGN = "custom_design";
    private static final String CUSTOM_ICON = "Custom_Icon";
    private static final String DATE = "date";
    private static final String DAY = "day";
    private static final String DESCRIPTION = "description";
    private static final String DIRECTION_BUTTON = "DirectionButton";
    private static final String DISPLAY = "display";
    private static final String DISTANCE = "distance";
    private static final String EACH_BACKGROUND = "each_background";
    private static final String EMAIL = "email";
    private static final String EMPTY_STRING = "";
    private static final String END_DATE = "end_date";
    private static final String ERROR = "error";
    private static final String EVEN_ROW_COLOR = "EvenRowColor";
    private static final String EVEN_ROW_TEXT_COLOR = "EvenRowTextColor";
    private static final String FACEBOOK_API_KEY = "facebook_api_key";
    private static final String FEATURE_TEXT_COLOR = "FeatureTextColor";
    private static final String FEED_LINK_COUNT_HIHT = "gd:feedlink_countHint";
    private static final String FEED_LINK_HREF = "gd:feedlink_href";
    private static final String FIELDS = "fields";
    private static final int FIRST_RECORD = 0;
    private static final String FOOTER_TINT = "footer_tint";
    private static final String GALLERY_TYPE = "gallery_type";
    public static final String GATEWAY_APP_ID = "gateway_appid";
    public static final String GATEWAY_KEY = "gateway_key";
    public static final String GATEWAY_TYPE = "gateway_type";
    private static final String GA_PROPERTY_ID = "ga_property_id";
    private static final String GLOBAL_BACKGROUND_COLOR = "global_background_color";
    private static final String GLOBAL_HEADER = "global_header";
    private static final String GOOGLE = "google";
    private static final String GREEN_COLOR = "green_color";
    private static final String GREEN_TEXT_COLOR = "green_color_text";
    private static final String GREEN_TITLE = "green_title";
    private static final String HEADER_IMAGE = "header_image";
    private static final String HEADER_SRC = "header_src";
    private static final String HEADER_TINT = "header_tint";
    private static final String HEADER_TINT_OPACITY = "header_tint_opacity";
    private static final String HEIGHT = "height";
    private static final String HOME_SUB_TABS = "home_sub_tabs";
    private static final String ICON = "icon";
    private static final String ID = "id";
    private static final String IMAGE = "image";
    private static final String IMAGES = "images";
    private static final String IMAGES_IN_ORDER = "imagesInOrder";
    private static final String IMAGE_URL = "imageurl";
    private static final String INFO = "info";
    private static final String INTEREST = "interest";
    private static final String IS_ACTIVE = "is_active";
    private static final String IS_NEW_DESIGN = "isNewDesign";
    private static final String IS_PROTECTED = "is_protected";
    private static final String ITEM_ID = "item_id";
    private static final String KEY = "key";
    private static final String LAST_UPDATED = "LastUpdated";
    private static final String LATITUDE = "latitude";
    private static final String LINK = "link";
    private static final String LINKED_TABS = "linkedTabs";
    private static final String LOCATIONS = "locations";
    private static final String LOCKER_IMAGE = "locker_image";
    private static final String LONGITUDE = "longitude";
    private static final String MAILING_LIST_PROMPT = "MailingListPrompt";
    private static final String MANY_IMAGES = "manyImages";
    private static final String MEDIA_THUMBNAIL_URL = "media:thumbnail_url";
    private static final String MESSAGE = "message";
    private static final String MESSAGE_ICON_LINKED_TAB = "message_icon_linked_tab";
    private static final String MESSAGE_ICON_ON = "message_icon_on";
    private static final String MESSAGE_ICON_OPACITY = "message_icon_opacity";
    private static final String MESSAGE_ICON_POS_H = "message_icon_pos_h";
    private static final String MINS = "mins";
    private static final String MONTH = "month";
    private static final String MORE_BUTTON_NAVIGATION = "moreButtonNavigation";
    private static final String MORE_BUTTON_NAVIGATION_ICON = "moreButtonNavigationIcon";
    private static final String MORE_TAB_BG_IPAD = "moreTabBackgroundForiPad";
    private static final String MORE_TAB_BG_PHONE = "moreTabBackgroundForiPhone";
    private static final String MUSIC_ALBUM = "album";
    private static final String MUSIC_ALBUM_ART = "album_art";
    private static final String MUSIC_ARTIST = "artist";
    private static final String MUSIC_BACKGROUND_IMG = "background";
    private static final String MUSIC_HEADER_IMG = "header";
    private static final String MUSIC_ID = "id";
    private static final String MUSIC_ITUNE = "itune";
    private static final String MUSIC_ONSALE = "onsale";
    private static final String MUSIC_ON_FRONT = "MusicOnFront";
    private static final String MUSIC_ON_TOP = "MusicOnTop";
    private static final String MUSIC_PREVIEW_URL = "previewUrl";
    private static final String MUSIC_TINT_COLOR = "tint";
    private static final String NAME = "name";
    private static final String NAVIGATION_BAR_COLOR = "NavigationBarColor";
    private static final String NAVIGATION_TEXT_COLOR = "NavigationTextColor";
    private static final String NAVIGATION_TEXT_SHADOW_COLOR = "NavigationTextShadowColor";
    private static final String NAVIG_CONTROLLER = "NavigationController";
    private static final String NAV_TINT_OPACITY = "nav_tint_opacity";
    private static final String NEW_NAV = "NewNav";
    private static final String NOTE = "note";
    private static final int NO_ERROR_VALUE = 0;
    private static final String ODD_ROW_COLOR = "OddRowColor";
    private static final String ODD_ROW_TEXT_COLOR = "OddRowTextColor";
    private static final String OPENING_TIMES = "opening_times";
    private static final String OPEN_FROM = "open_from";
    private static final String OPEN_TO = "open_to";
    private static final String PHONE = "phone";
    private static final String POI = "poi";
    private static final String PREMIUM_NAVIGATION_POSITION = "premium_navigation_position";
    private static final String PRICE = "price";
    private static final String PROFILE_IMAGE_URL = "profile_image_url";
    private static final String PUBLISHED = "published";
    private static final String PURPLE_COLOR = "purple_color";
    private static final String PURPLE_TEXT_COLOR = "purple_color_text";
    private static final String PURPLE_TITLE = "purple_title";
    private static final String PUSHING_ADDRESS = "pushing_address";
    private static final String RATING_AVERAGE = "gd:rating_average";
    private static final String READONLY = "readonly";
    private static final String RECURRING = "recurring";
    private static final String RECURRING_DAY = "recurring_day";
    private static final String RED_COLOR = "red_color";
    private static final String RED_TEXT_COLOR = "red_color_text";
    private static final String RED_TITLE = "red_title";
    private static final String REPLIES = "replies";
    private static final String RESERVATION_APP_ID = "app_id";
    private static final String RESERVATION_BILLING_ADDRESS_ID = "billing_address_id";
    private static final String RESERVATION_CHECKOUT_METHOD = "checkout_method";
    private static final String RESERVATION_COST = "cost";
    private static final String RESERVATION_DATE = "date";
    private static final String RESERVATION_DURATION = "duration";
    private static final String RESERVATION_ID = "id";
    private static final String RESERVATION_IMAGE_URL = "image_url";
    private static final String RESERVATION_ITEM_ID = "item_id";
    private static final String RESERVATION_LOC_ID = "loc_id";
    private static final String RESERVATION_NOTE = "note";
    private static final String RESERVATION_ORDER_STATE = "order_state";
    private static final String RESERVATION_PAID_AMOUNT = "paid_amount";
    private static final String RESERVATION_PLACED_ON = "placed_on";
    private static final String RESERVATION_SERVICE_NAME = "service_name";
    private static final String RESERVATION_TAB_ID = "tab_id";
    private static final String RESERVATION_THUMBNAIL = "thumbnail";
    private static final String RESERVATION_TIME_FROM = "time_from";
    private static final String RESERVATION_TIME_TO = "time_to";
    private static final String RESERVATION_TRANSACTION_ID = "transaction_id";
    private static final String RESERVATION_USER_ID = "user_id";
    private static final String RESERV_FEE = "reserv_fee";
    private static final String REST_WEEK = "rest_week";
    private static final String REUSABLE = "reusable";
    private static final String REWARD_ID = "RewardID";
    private static final String REWARD_ITEMS = "rewardItems";
    private static final String REWARD_ITEM_IMAGE = "rewardItemImage";
    private static final String RICH_CAT_ID = "rich_cat_id";
    private static final String RICH_DETAIL_ID = "rich_detail_id";
    private static final String RICH_TAB_ID = "rich_tab_id";
    private static final String RICH_TYPE = "rich_type";
    private static final String RICH_URL = "rich_url";
    private static final String ROWS = "rows";
    private static final String RSS_ICON = "RSSIcon";
    private static final String SCREEN_NAME = "screen_name";
    private static final String SECTION = "section";
    private static final String SECTION_BAR_COLOR = "SectionBarColor";
    private static final String SECTION_BAR_TEXT_COLOR = "SectionBarTextColor";
    private static final String SECTION_ID = "section_id";
    private static final String SEQ = "seq";
    private static final String SLIDING_ENABLED = "slidingEnabled";
    private static final String START_DATE = "start_date";
    private static final String STATE = "state";
    private static final String STATISTICS_VIEW_COUNT = "yt:statistics_viewCount";
    private static final String STATUSES = "statuses";
    private static final String SUBJECT = "subject";
    private static final String SUBTITLE = "subtitle";
    private static final String SUMMARY = "summary";
    private static final String TAB_FONT = "tab_font";
    private static final String TAB_ICON = "tab_icon";
    private static final String TAB_ID = "tab_id";
    private static final String TAB_IMAGE = "TabImage";
    private static final String TAB_LABEL = "TabLabel";
    private static final String TAB_LABEL_FONT = "TabLabelFont";
    private static final String TAB_LABEL_TEXT = "TabLabelText";
    private static final String TAB_LABEL_TEXT_BG_COLOR = "TabLableTextBackgroundColor";
    private static final String TAB_LABEL_TEXT_COLOR = "TabLableTextColor";
    private static final String TAB_SHOWTEXT = "tab_showtext";
    private static final String TAB_SRC = "tab_src";
    private static final String TAB_TEXT = "tab_text";
    private static final String TAB_TINT = "tab_tint";
    private static final String TAB_TINT_OPACITY = "tab_tint_opacity";
    private static final String TELEPHONE = "telephone";
    private static final String TELEPHONE_DISPLAY = "telephone_display";
    private static final String TELL_FRIEND_BUTTON = "TellFriendButton";
    private static final String TEXT = "text";
    private static final String THUMBNAIL = "thumbnail";
    private static final String TIMEFROM = "timefrom";
    private static final String TIMETO = "timeto";
    private static final String TIMEZONE = "timezone";
    private static final String TIME_AGO = "time_ago";
    private static final String TITLE = "title";
    private static final String TOKEN = "token";
    private static final String TWITTER = "twitter";
    private static final String UPLOADED_IMAGE = "uploadedImage";
    private static final String URL = "URL";
    private static final String USER = "user";
    private static final String USERID = "userId";
    private static final String USER_ID = "user_id";
    private static final String USER_INFO = "user_info";
    private static final String USE_TEXT_COLORS = "UseTextColors";
    private static final String VIEW = "view";
    private static final String VIEW_CONTROLLER = "ViewController";
    private static final String WEBSITE = "website";
    private static final String WIDTH = "width";
    private static final String YES = "yes";
    private static final String ZIP = "zip";

    public static final Tip parseTip(String data) {
        Tip item = new Tip();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                item.setImage(getValue(jsonArray.getJSONObject(i), "image"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    public static final List<Tab> parseTabs(String data) {
        List<Tab> tabList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            Random r = new Random();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                Tab tab = new Tab();
                tab.setLabel(getValue(obj, TAB_LABEL));
                tab.setImage(getValue(obj, TAB_IMAGE));
                tab.setViewController(getValue(obj, VIEW_CONTROLLER));
                tab.setNavigationController(getValue(obj, NAVIG_CONTROLLER));
                tab.setLastUpdated(getValue(obj, LAST_UPDATED));
                tab.setUrl(getValue(obj, "URL"));
                tab.setItemId(getValue(obj, "item_id"));
                tab.setSectionId(getValue(obj, SECTION_ID));
                if (getValue(obj, CUSTOM_DESIGN).equalsIgnoreCase(YES)) {
                    tab.setHasCustomDesign(true);
                    tab.setTabSrc(getValue(obj, TAB_SRC));
                }
                if (getValue(obj, TAB_SHOWTEXT) != null && getValue(obj, TAB_SHOWTEXT).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    tab.setShowText(false);
                }
                tab.setId(System.currentTimeMillis() + ((long) r.nextInt()));
                tab.setTabId(getValue(obj, ServerConstants.POST_TAB_ID_PARAM));
                tab.setTabIcon(getValue(obj, TAB_ICON));
                tab.setTabTint(getValue(obj, TAB_TINT));
                tab.setTabTintOpacity(getFloatValue(obj, TAB_TINT_OPACITY));
                tab.setTabText(getValue(obj, TAB_TEXT));
                tabList.add(tab);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tabList;
    }

    public static final Mortgage parseMortgage(String data) {
        Mortgage mortgage = new Mortgage();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                mortgage.setInterest(getValue(obj, INTEREST));
                mortgage.setReadOnly(getValue(obj, READONLY));
                mortgage.setImage(getValue(obj, "image"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mortgage;
    }

    public static final EmailPhotoItem parseEmailPhoto(String data) {
        EmailPhotoItem item = new EmailPhotoItem();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                item.setDescription(getValue(obj, "description"));
                item.setEmail(getValue(obj, "email"));
                item.setImage(getValue(obj, "image"));
                item.setSubject(getValue(obj, SUBJECT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    public static List<FanWallComment> parseFanComments(String data) {
        List<FanWallComment> items = new ArrayList<>();
        try {
            JSONArray root = new JSONArray(data);
            for (int i = 0; i < root.length(); i++) {
                JSONObject commentData = root.getJSONObject(i);
                FanWallComment comment = new FanWallComment();
                comment.setComment(getValue(commentData, "comment"));
                comment.setId(getValue(commentData, "id"));
                comment.setImage(getValue(commentData, "image"));
                comment.setTitle(getValue(commentData, "name"));
                comment.setReplies(getValue(commentData, REPLIES));
                comment.setTimeAgo(getValue(commentData, TIME_AGO));
                comment.setLatitude(getDoubleValue(commentData, "latitude"));
                comment.setLongitude(getDoubleValue(commentData, "longitude"));
                comment.setBackground(getValue(commentData, "background"));
                comment.setUploadImageUrl(getValue(commentData, UPLOADED_IMAGE));
                items.add(comment);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public static List<FanWallItem> parseFanWallData(String data) {
        List<FanWallItem> items = new ArrayList<>();
        try {
            JSONArray root = new JSONArray(data);
            for (int i = 0; i < root.length(); i++) {
                FanWallItem item = new FanWallItem();
                JSONObject obj = root.getJSONObject(i);
                item.setImage(getValue(obj, "image"));
                JSONArray jsonArray = obj.getJSONArray(COMMENTS);
                for (int j = 0; j < jsonArray.length(); j++) {
                    JSONObject commentData = jsonArray.getJSONObject(j);
                    FanWallComment comment = new FanWallComment();
                    comment.setComment(getValue(commentData, "comment"));
                    comment.setId(getValue(commentData, "id"));
                    comment.setImage(getValue(commentData, "image"));
                    comment.setTitle(getValue(commentData, "name"));
                    comment.setReplies(getValue(commentData, REPLIES));
                    comment.setTimeAgo(getValue(commentData, TIME_AGO));
                    item.addComment(comment);
                }
                items.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public static AroundUsItem parseAroundUsData(String data) {
        AroundUsItem item = null;
        try {
            JSONArray root = new JSONArray(data);
            AroundUsItem item2 = new AroundUsItem();
            int i = 0;
            while (i < root.length()) {
                try {
                    JSONObject obj = root.getJSONObject(i);
                    item2.setImage(getValue(obj, "background"));
                    item2.setGreenTitle(getValue(obj, GREEN_TITLE));
                    item2.setGreenColor(getValue(obj, GREEN_COLOR));
                    item2.setGreenTextColor(getValue(obj, GREEN_TEXT_COLOR));
                    item2.setPurpleTitle(getValue(obj, PURPLE_TITLE));
                    item2.setPurpleColor(getValue(obj, PURPLE_COLOR));
                    item2.setPurpleTextColor(getValue(obj, PURPLE_TEXT_COLOR));
                    item2.setRedTitle(getValue(obj, RED_TITLE));
                    item2.setRedColor(getValue(obj, RED_COLOR));
                    item2.setRedTextColor(getValue(obj, RED_TEXT_COLOR));
                    JSONArray jsonArray = obj.getJSONArray(POI);
                    ArrayList<AroundUsItem.PoiItem> poi = new ArrayList<>();
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject poiData = jsonArray.getJSONObject(j);
                        AroundUsItem.PoiItem poiItem = new AroundUsItem.PoiItem();
                        LocationItem location = new LocationItem();
                        location.setAddress1(getValue(obj, ADDRESS_1));
                        location.setAddress2(getValue(obj, ADDRESS_2));
                        location.setCity(getValue(obj, CITY));
                        location.setState(getValue(obj, STATE));
                        location.setZip(getValue(obj, ZIP));
                        poiItem.setName(getValue(poiData, "name"));
                        poiItem.setColor(getValue(poiData, COLOR));
                        poiItem.setDescription(getValue(poiData, "description"));
                        poiItem.setLongitude(getValue(poiData, "longitude"));
                        poiItem.setLatitude(getValue(poiData, "latitude"));
                        poiItem.setLocation(location);
                        poi.add(poiItem);
                    }
                    item2.setPoi(poi);
                    i++;
                } catch (Exception e) {
                    e = e;
                    item = item2;
                    e.printStackTrace();
                    return item;
                }
            }
            return item2;
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return item;
        }
    }

    public static final MailingListEntity parseMailingList(String data) {
        MailingListEntity item = new MailingListEntity();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                item.setImage(getValue(obj, "image"));
                item.setDescription(getValue(obj, "description"));
                JSONArray categoriesInJson = obj.getJSONArray(CATEGORIES);
                if (categoriesInJson.length() > 0) {
                    List<MailingListEntity.Category> categories = new ArrayList<>();
                    for (int j = 0; j < categoriesInJson.length(); j++) {
                        JSONArray jsonItem = categoriesInJson.getJSONArray(j);
                        MailingListEntity.Category category = new MailingListEntity.Category();
                        category.setId(jsonItem.getInt(0));
                        category.setName(jsonItem.getString(1));
                        categories.add(category);
                    }
                    item.setCategories(categories);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    public static final List<CallUsItem> parseCallUsItems(String data) {
        List<CallUsItem> items = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                CallUsItem item = new CallUsItem();
                item.setTitle(getValue(obj, "title"));
                item.setPhone(getValue(obj, PHONE));
                item.setId(getValue(obj, "id"));
                item.setBackground(getValue(obj, "background"));
                items.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public static final List<MessageItem> parseMessages(String data) {
        List<MessageItem> messageList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                MessageItem message = new MessageItem();
                message.setId(getValue(obj, "id"));
                message.setTitle(getValue(obj, "title"));
                message.setDate(getValue(obj, "date"));
                message.setBackground(getValue(obj, "background"));
                message.setTabId(getValue(obj, RICH_TAB_ID));
                message.setType(getIntValue(obj, RICH_TYPE));
                message.setUrl(getValue(obj, RICH_URL));
                message.setCategoryId(getValue(obj, RICH_CAT_ID));
                message.setDetailId(getValue(obj, RICH_DETAIL_ID));
                messageList.add(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return messageList;
    }

    public static final List<CommonListEntity> parseMenuList(String data) {
        List<CommonListEntity> menuSectionList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                CommonListEntity item = new CommonListEntity();
                item.setId(getValue(obj, "id"));
                item.setSection(getValue(obj, SECTION));
                item.setTitle(getValue(obj, "title"));
                item.setBackground(getValue(obj, "background"));
                menuSectionList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuSectionList;
    }

    public static final List<MenuSectionItem> parseMenuItemsList(String data) {
        List<MenuSectionItem> menuSectionItemList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                MenuSectionItem menuSectionItem = new MenuSectionItem();
                menuSectionItem.setId(getValue(obj, "id"));
                menuSectionItem.setPrice(getValue(obj, PRICE));
                menuSectionItem.setTitle(getValue(obj, "title"));
                menuSectionItem.setBackground(getValue(obj, "background"));
                menuSectionItemList.add(menuSectionItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return menuSectionItemList;
    }

    public static final CommonListEntity parseInfo(String data) {
        CommonListEntity infoItem = new CommonListEntity();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                infoItem.setId(getValue(obj, "id"));
                infoItem.setTitle(getValue(obj, "title"));
                infoItem.setDescription(getValue(obj, "description"));
                infoItem.setImage(getValue(obj, "image"));
                infoItem.setHeaderImage(getValue(obj, HEADER_IMAGE));
                if (getValue(obj, IS_NEW_DESIGN).equals("1")) {
                    infoItem.setHasNewDesign(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return infoItem;
    }

    public static final List<CommonListEntity> parseInfoList(String data) {
        List<CommonListEntity> infoItemList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                CommonListEntity infoItem = new CommonListEntity();
                JSONObject obj = jsonArray.getJSONObject(i);
                infoItem.setId(getValue(obj, "id"));
                infoItem.setTitle(getValue(obj, "title"));
                infoItem.setDescription(getValue(obj, "description"));
                String thumbnail = getValue(obj, "thumbnail");
                if (StringUtils.isEmpty(thumbnail)) {
                    thumbnail = getValue(obj, "image");
                }
                if (getValue(obj, IS_NEW_DESIGN).equals("1")) {
                    infoItem.setHasNewDesign(true);
                }
                infoItem.setImage(thumbnail);
                infoItem.setSection(getValue(obj, SECTION));
                infoItem.setBackground(getValue(obj, "background"));
                infoItemList.add(infoItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return infoItemList;
    }

    public static final List<EventItem> parseEvents(String data) {
        List<EventItem> eventList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                EventItem event = new EventItem();
                event.setId(getValue(obj, "id"));
                event.setTitle(getValue(obj, "title"));
                event.setSection(getValue(obj, SECTION));
                event.setMonth(getValue(obj, MONTH));
                event.setDay(getValue(obj, DAY));
                event.setDate(getValue(obj, "date"));
                event.setBackground(getValue(obj, "background"));
                if (obj.has(RECURRING) && obj.has(RECURRING_DAY)) {
                    event.setRecurring(getValue(obj, RECURRING).equalsIgnoreCase(YES));
                    event.setRecurringDay(EventItem.RecurringDay.findDay(getIntValue(obj, RECURRING_DAY)));
                }
                event.setTimeFrom(getValue(obj, TIMEFROM));
                event.setTimeTo(getValue(obj, TIMETO));
                event.setDatetimeBegin(DateUtils.getDateBySec(getValue(obj, START_DATE)));
                event.setDatetimeEnd(DateUtils.getDateBySec(getValue(obj, END_DATE)));
                eventList.add(event);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return eventList;
    }

    public static final List<CouponItem> parseCoupons(String data) {
        List<CouponItem> couponList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                CouponItem coupon = new CouponItem();
                coupon.setId(getValue(obj, "id"));
                coupon.setTitle(getValue(obj, "title"));
                coupon.setStartDate(DateUtils.getDateBySec(getValue(obj, START_DATE)));
                coupon.setEndDate(DateUtils.getDateBySec(getValue(obj, END_DATE)));
                coupon.setCheckinTarget(getIntValue(obj, CHECKIN_TARGET));
                coupon.setCheckinTargetMax(getIntValue(obj, CHECKIN_TARGET));
                coupon.setCheckinInterval(getIntValue(obj, CHECKIN_INTERVAL));
                if (getValue(obj, REUSABLE).equals("1")) {
                    coupon.setReusable(true);
                }
                coupon.setLongitude(getValue(obj, "longitude"));
                coupon.setLatitude(getValue(obj, "latitude"));
                coupon.setDistance(getValue(obj, DISTANCE));
                coupon.setCode(getValue(obj, CODE));
                coupon.setBackground(getValue(obj, "background"));
                if (obj.has(LOCATIONS)) {
                    coupon.setLocations(parseCouponsLocation(obj.getString(LOCATIONS)));
                }
                couponList.add(coupon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return couponList;
    }

    public static final StatFieldsItem parseStatFields(String data) {
        StatFieldsItem item = new StatFieldsItem();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                item.setEmail(getValue(obj, "email"));
                item.setImage(getValue(obj, "image"));
                item.setMessage(getValue(obj, MESSAGE));
                List<String> fieldNames = new ArrayList<>();
                JSONArray fields = obj.getJSONArray(FIELDS);
                for (int j = 0; j < fields.length(); j++) {
                    fieldNames.add(fields.getString(j));
                }
                item.setFields(fieldNames);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    public static final CommonDataItem parseCommonData(String data) {
        CommonDataItem item = new CommonDataItem();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                item.setImage(getValue(jsonArray.getJSONObject(i), "image"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return item;
    }

    public static final App parseNewDesignData(String data) {
        App appInfo = new App();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                appInfo.setImageUrl(getValue(obj, "image"));
                JSONArray imagesInUrlJson = obj.getJSONArray(IMAGES_IN_ORDER);
                if (getValue(obj, MANY_IMAGES).equalsIgnoreCase(YES)) {
                    appInfo.setHasManyImages(true);
                }
                List<String> imagesUrls = new ArrayList<>();
                for (int j = 0; j < imagesInUrlJson.length(); j++) {
                    imagesUrls.add(imagesInUrlJson.getString(j));
                }
                appInfo.setImagesInOrder(imagesUrls);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return appInfo;
    }

    public static final void updateInitStateWithData(String jsonData) {
        try {
            JSONObject root = new JSONArray(jsonData).getJSONObject(0);
            AppCore.getInstance().setAppSettings(parseAppSettings(root.getJSONObject("settings")));
            AppCore.getInstance().getCachingManager().saveData(CachingConstants.APP_INFO_PROPERTY, parseAppInfo(root.getJSONObject("home")));
            AppCore.getInstance().getCachingManager().setTabList(parseTabs(root.getString("tabs")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final App parseAppInfo(JSONObject obj) throws JSONException {
        App appInfo = new App();
        appInfo.setImageUrl(getValue(obj, "image"));
        if (obj.has(IMAGES_IN_ORDER)) {
            JSONArray imagesInUrlJson = obj.getJSONArray(IMAGES_IN_ORDER);
            if (getValue(obj, MANY_IMAGES).equalsIgnoreCase(YES)) {
                appInfo.setHasManyImages(true);
            }
            List<String> imagesUrls = new ArrayList<>();
            for (int j = 0; j < imagesInUrlJson.length(); j++) {
                imagesUrls.add(imagesInUrlJson.getString(j));
            }
            appInfo.setImagesInOrder(imagesUrls);
        }
        if (obj.has(LINKED_TABS)) {
            JSONArray linkedTabsJSON = obj.getJSONArray(LINKED_TABS);
            List<Tab> linkedTabs = new ArrayList<>();
            for (int j2 = 0; j2 < linkedTabsJSON.length(); j2++) {
                JSONObject jsonItem = linkedTabsJSON.getJSONObject(j2);
                Tab tab = new Tab();
                tab.setTabId(getValue(jsonItem, ServerConstants.POST_TAB_ID_PARAM));
                tab.setItemId(getValue(jsonItem, "item_id"));
                tab.setSectionId(getValue(jsonItem, CAT_ID));
                tab.setViewController(getValue(jsonItem, VIEW));
                linkedTabs.add(tab);
            }
            appInfo.setImagesLinkedTabs(linkedTabs);
        }
        appInfo.setName(getValue(obj, "name"));
        appInfo.setTelephone(getValue(obj, TELEPHONE));
        appInfo.setLatitude(getValue(obj, "latitude"));
        appInfo.setLongitude(getValue(obj, "longitude"));
        if (obj.has(ADDRESSES)) {
            appInfo.getLocations().addAll(parseLocation(obj.get(ADDRESSES).toString()));
        }
        if (obj.has(HOME_SUB_TABS)) {
            JSONArray subTabsJson = obj.getJSONArray(HOME_SUB_TABS);
            Random r = new Random();
            List<Tab> subTabs = new ArrayList<>();
            for (int j3 = 0; j3 < subTabsJson.length(); j3++) {
                JSONObject subItem = subTabsJson.getJSONObject(j3);
                Tab subTab = new Tab();
                subTab.setTabId(getValue(subItem, ServerConstants.POST_TAB_ID_PARAM));
                subTab.setActive(getValue(subItem, IS_ACTIVE).equals("1"));
                subTab.setImage(getValue(subItem, TAB_IMAGE));
                if (getValue(subItem, CUSTOM_ICON).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    subTab.setTabImageUrl("http://www.biznessapps.com/images/subtabicons/" + subTab.getImage());
                } else {
                    subTab.setTabImageUrl(String.format("http://biznessapps.com/uploads/cutom_icon/%s/%s", AppCore.getInstance().getAppSettings().getAppId(), subTab.getImage()));
                }
                subTab.setViewController(getValue(subItem, VIEW_CONTROLLER));
                subTab.setNavigationController(getValue(subItem, NAVIG_CONTROLLER));
                subTab.setLastUpdated(getValue(subItem, LAST_UPDATED));
                subTab.setLabel(getValue(subItem, TAB_LABEL_TEXT));
                subTab.setTabLabelFont(getValue(subItem, TAB_LABEL_FONT));
                subTab.setTabLabelTextColor(getValue(subItem, TAB_LABEL_TEXT_COLOR));
                subTab.setTabLabelTextBgColor(getValue(subItem, TAB_LABEL_TEXT_BG_COLOR));
                subTab.setSeq(getIntValue(subItem, SEQ));
                subTab.setId(System.currentTimeMillis() + ((long) r.nextInt()));
                subTabs.add(subTab);
            }
            if (!subTabs.isEmpty()) {
                appInfo.setSubTabs(subTabs);
            }
        }
        return appInfo;
    }

    public static final String getTellFriendImage(String data) {
        String result = null;
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                result = getValue(jsonArray.getJSONObject(i), "image");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static final AppSettings parseAppSettings(JSONObject obj) throws JSONException {
        AppSettings appSettings = new AppSettings();
        appSettings.setAdWhirlID(getValue(obj, AD_WHIRL_ID));
        appSettings.setAppId(getValue(obj, APP_ID));
        appSettings.setConsumerKey(getValue(obj, CONSUMER_KEY));
        appSettings.setConsumerSecret(getValue(obj, CONSUMER_SECRET));
        appSettings.setGaAccountId(getValue(obj, GA_PROPERTY_ID));
        appSettings.setGlobalBgColor(getValue(obj, GLOBAL_BACKGROUND_COLOR));
        appSettings.setEvenRowColor(getValue(obj, EVEN_ROW_COLOR));
        appSettings.setOddRowColor(getValue(obj, ODD_ROW_COLOR));
        appSettings.setFeatureTextColor(getValue(obj, FEATURE_TEXT_COLOR));
        appSettings.setButtonTextColor(getValue(obj, BUTTON_TEXT_COLOR));
        appSettings.setButtonBgColor(getValue(obj, BUTTON_BG_COLOR));
        appSettings.setEvenRowTextColor(getValue(obj, EVEN_ROW_TEXT_COLOR));
        appSettings.setOddRowTextColor(getValue(obj, ODD_ROW_TEXT_COLOR));
        appSettings.setNavigBarColor(getValue(obj, NAVIGATION_BAR_COLOR));
        appSettings.setNavigBarTextColor(getValue(obj, NAVIGATION_TEXT_COLOR));
        appSettings.setNavigBarTextShadowColor(getValue(obj, NAVIGATION_TEXT_SHADOW_COLOR));
        appSettings.setSectionBarColor(getValue(obj, SECTION_BAR_COLOR));
        appSettings.setSectionBarTextColor(getValue(obj, SECTION_BAR_TEXT_COLOR));
        appSettings.setNavigationMenuType(getIntValue(obj, PREMIUM_NAVIGATION_POSITION));
        appSettings.setMessateLinkedTab(getValue(obj, MESSAGE_ICON_LINKED_TAB));
        appSettings.setMessageIconOpacity(getIntValue(obj, MESSAGE_ICON_OPACITY));
        appSettings.setPushingIp(getValue(obj, PUSHING_ADDRESS));
        if (getValue(obj, IS_PROTECTED).equals("1")) {
            appSettings.setProtected(true);
        }
        if (getValue(obj, MAILING_LIST_PROMPT).equals("1")) {
            appSettings.setMailingListPrompt(true);
        }
        if (getValue(obj, MUSIC_ON_FRONT).equals("1")) {
            appSettings.setMusicOnFront(true);
        }
        if (getValue(obj, MUSIC_ON_TOP).equals("1")) {
            appSettings.setMusicOnTop(true);
        }
        if (getValue(obj, MESSAGE_ICON_ON).equals("1")) {
            appSettings.setMessageIconUsed(true);
        }
        if (getValue(obj, MESSAGE_ICON_POS_H).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            appSettings.setMessageIconLeft(true);
        }
        if (getValue(obj, IS_PROTECTED).equals("1")) {
            appSettings.setMessageIconTop(true);
        }
        appSettings.setHasCallButton(getValue(obj, CALL_BUTTON).equalsIgnoreCase(YES));
        appSettings.setHasDirectionButton(getValue(obj, DIRECTION_BUTTON).equalsIgnoreCase(YES));
        appSettings.setHasTellFriendButton(getValue(obj, TELL_FRIEND_BUTTON).equalsIgnoreCase(YES));
        appSettings.setAnimationMode(getIntValue(obj, SLIDING_ENABLED));
        appSettings.setUseTextColors(getValue(obj, USE_TEXT_COLORS));
        appSettings.setMoreTabBg(getValue(obj, MORE_TAB_BG_PHONE));
        appSettings.setMoreTabTabletBg(getValue(obj, MORE_TAB_BG_IPAD));
        if (getValue(obj, MORE_BUTTON_NAVIGATION).equalsIgnoreCase(YES)) {
            appSettings.setMoreButtonNavigation(true);
        }
        appSettings.setMoreIconUrl(getValue(obj, MORE_BUTTON_NAVIGATION_ICON));
        appSettings.setRows(getIntValue(obj, ROWS));
        appSettings.setCols(getIntValue(obj, COLS));
        appSettings.setTabSrc(getValue(obj, TAB_SRC));
        appSettings.setTabTint(getValue(obj, TAB_TINT));
        appSettings.setTabTintOpacity(getFloatValue(obj, TAB_TINT_OPACITY));
        appSettings.setTabIcon(getValue(obj, TAB_ICON));
        appSettings.setTabText(getValue(obj, TAB_TEXT));
        appSettings.setTabFont(getValue(obj, TAB_FONT));
        if (getValue(obj, TAB_SHOWTEXT) != null && getValue(obj, TAB_SHOWTEXT).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            appSettings.setShowText(false);
        }
        appSettings.setHeaderSrc(getValue(obj, HEADER_SRC));
        appSettings.setHeaderTint(getValue(obj, HEADER_TINT));
        appSettings.setHeaderTintOpacity(getValue(obj, HEADER_TINT_OPACITY));
        appSettings.setFacebookAppId(getValue(obj, FACEBOOK_API_KEY));
        appSettings.setFooterTint(getValue(obj, FOOTER_TINT));
        appSettings.setGlobalHeaderUrl(getValue(obj, GLOBAL_HEADER));
        appSettings.setNavTintOpacity(getValue(obj, NAV_TINT_OPACITY));
        appSettings.setRssIconUrl(getValue(obj, RSS_ICON));
        appSettings.setUseNewDesign(getValue(obj, NEW_NAV).equalsIgnoreCase(YES));
        return appSettings;
    }

    public static final GalleryData parseGalleryMetadata(String data) {
        GalleryData resultData = new GalleryData();
        List<GalleryData.Item> galleryItems = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                resultData.setUseCoverflow(getValue(obj, COVERFLOW).equalsIgnoreCase("y"));
                JSONArray dataArray = obj.getJSONArray(IMAGES);
                for (int j = 0; j < dataArray.length(); j++) {
                    JSONObject jsonItem = dataArray.getJSONObject(j);
                    GalleryData.Item item = new GalleryData.Item();
                    item.setId(getValue(jsonItem, "id"));
                    item.setHeight(getValue(jsonItem, HEIGHT));
                    item.setWidth(getValue(jsonItem, WIDTH));
                    item.setInfo(getValue(jsonItem, INFO));
                    galleryItems.add(item);
                }
                resultData.setItems(galleryItems);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultData;
    }

    public static final GalleryData parseFlickrData(String data) {
        boolean z;
        boolean z2 = true;
        GalleryData resultData = new GalleryData();
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (0 < jsonArray.length()) {
                JSONObject obj = jsonArray.getJSONObject(0);
                if (getValue(obj, GALLERY_TYPE).equalsIgnoreCase(COVERFLOW)) {
                    z = true;
                } else {
                    z = false;
                }
                resultData.setUseCoverflow(z);
                resultData.setUserId(getValue(obj, USERID));
                resultData.setApiKey(getValue(obj, API_KEY));
                if (!getValue(obj, "display").equalsIgnoreCase("photo_set")) {
                    z2 = false;
                }
                resultData.setDisplayPhotosets(z2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultData;
    }

    public static final GalleryData parsePicasaData(String data) {
        GalleryData resultData = new GalleryData();
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (0 < jsonArray.length()) {
                JSONObject obj = jsonArray.getJSONObject(0);
                resultData.setUseCoverflow(getValue(obj, GALLERY_TYPE).equalsIgnoreCase(COVERFLOW));
                resultData.setUserId(getValue(obj, USERID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultData;
    }

    public static final GalleryData parseInstagramData(String data) {
        GalleryData resultData = new GalleryData();
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (0 < jsonArray.length()) {
                JSONObject obj = jsonArray.getJSONObject(0);
                resultData.setUseCoverflow(getValue(obj, GALLERY_TYPE).equalsIgnoreCase(COVERFLOW));
                resultData.setUserId(getValue(obj, ServerConstants.POST_USER_ID_PARAM));
                resultData.setApiKey(getValue(obj, "token"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultData;
    }

    public static final void parseInstagramImages(GalleryData galleryData, String metadata) {
        List<GalleryData.Item> galleryItems = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(metadata).getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj3 = jsonArray.getJSONObject(i).getJSONObject(IMAGES).getJSONObject("standard_resolution");
                GalleryData.Item item = new GalleryData.Item();
                item.setHeight(getValue(obj3, HEIGHT));
                item.setWidth(getValue(obj3, WIDTH));
                item.setFullUrl(getValue(obj3, NativeProtocol.IMAGE_URL_KEY));
                galleryItems.add(item);
                galleryData.setItems(galleryItems);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static User parseFlickrUser(String data) {
        try {
            return ((RespUser) new Gson().fromJson(data, RespUser.class)).getUser();
        } catch (Exception e) {
            return null;
        }
    }

    public static List<Photoset> getPhotosets(String data) {
        List<Photoset> result = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(data).getJSONObject("photosets").getJSONArray("photoset");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                Photoset item = new Photoset();
                item.setId(getValue(obj, "id"));
                item.setTitle(getValue(obj.getJSONObject("title"), "_content"));
                item.setFarm(getValue(obj, "farm"));
                item.setSecret(getValue(obj, "secret"));
                item.setServer(getValue(obj, "server"));
                item.setPrimary(getValue(obj, "primary"));
                item.setDescription(getValue(obj.getJSONObject("description"), "_content"));
                result.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public static Photoset getPhotosInPhotoset(String data) {
        Photoset result = new Photoset();
        List<Photo> photoList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(data).getJSONObject("photoset").getJSONArray("photo");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                Photo item = new Photo();
                item.setId(getValue(obj, "id"));
                item.setTitle(getValue(obj, "title"));
                item.setUrl(getValue(obj, "url_m"));
                item.setFarm(getValue(obj, "farm"));
                item.setSecret(getValue(obj, "secret"));
                item.setServer(getValue(obj, "server"));
                photoList.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        result.setPhotos(photoList);
        return result;
    }

    public static Galleries getGalleries(String data) {
        try {
            return ((RespGalleries) new Gson().fromJson(data, RespGalleries.class)).getGalleries();
        } catch (Exception e) {
            return null;
        }
    }

    public static Photos getPhotosInGallery(String data) {
        try {
            return ((RespGalleryPhotos) new Gson().fromJson(data, RespGalleryPhotos.class)).getPhotos();
        } catch (Exception e) {
            return null;
        }
    }

    public static final List<LocationItem> parseLocation(String data) {
        List<LocationItem> locations = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                LocationItem location = new LocationItem();
                location.setTitle(getValue(obj, "name"));
                location.setImage(getValue(obj, "image"));
                location.setTelephone(getValue(obj, TELEPHONE));
                location.setTelephoneDisplay(getValue(obj, TELEPHONE_DISPLAY));
                location.setEmail(getValue(obj, "email"));
                location.setWebsite(getValue(obj, WEBSITE));
                location.setLatitude(getValue(obj, "latitude"));
                location.setLongitude(getValue(obj, "longitude"));
                location.setAddress1(getValue(obj, ADDRESS_1));
                location.setAddress2(getValue(obj, ADDRESS_2));
                location.setCity(getValue(obj, CITY));
                location.setState(getValue(obj, STATE));
                location.setZip(getValue(obj, ZIP));
                location.setComment(getValue(obj, "comment"));
                try {
                    if (obj.has(OPENING_TIMES) && StringUtils.isNotEmpty(obj.getString(OPENING_TIMES)) && !obj.get(OPENING_TIMES).toString().equalsIgnoreCase("null")) {
                        JSONArray openTimes = new JSONArray(obj.get(OPENING_TIMES).toString());
                        for (int j = 0; j < openTimes.length(); j++) {
                            JSONObject obj2 = openTimes.getJSONObject(j);
                            LocationOpeningTime openingTime = new LocationOpeningTime();
                            openingTime.setDay(getValue(obj2, DAY));
                            openingTime.setOpenFrom(getValue(obj2, OPEN_FROM));
                            openingTime.setOpenTo(getValue(obj2, OPEN_TO));
                            location.getOpeningTimes().add(openingTime);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                locations.add(location);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return locations;
    }

    public static final List<LocationItem> parseLocationList(String data) {
        List<LocationItem> locationList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                LocationItem item = new LocationItem();
                item.setId(getValue(obj, "id"));
                item.setTitle(getValue(obj, "title"));
                item.setLatitude(getValue(obj, "latitude"));
                item.setLongitude(getValue(obj, "longitude"));
                item.setAddress1(getValue(obj, ADDRESS_1));
                item.setAddress2(getValue(obj, ADDRESS_2));
                item.setCity(getValue(obj, CITY));
                item.setState(getValue(obj, STATE));
                item.setZip(getValue(obj, ZIP));
                item.setTelephone(getValue(obj, TELEPHONE));
                item.setEmail(getValue(obj, "email"));
                item.setWebsite(getValue(obj, WEBSITE));
                item.setBackground(getValue(obj, "background"));
                item.setItemBgUrl(getValue(obj, EACH_BACKGROUND));
                item.setTimeZone(getValue(obj, TIMEZONE));
                locationList.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return locationList;
    }

    public static final List<LoyaltyItem> parseLoyaltyList(String data) {
        List<LoyaltyItem> loyaltyList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(data).getJSONArray(REWARD_ITEMS);
            String lockerImageUrl = null;
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                LoyaltyItem item = new LoyaltyItem();
                item.setId(getValue(obj, REWARD_ID));
                item.setTitle(getValue(obj, TEXT));
                item.setImage(getValue(obj, REWARD_ITEM_IMAGE));
                item.setBackground(getValue(obj, "background"));
                if (i == 0) {
                    lockerImageUrl = getValue(obj, LOCKER_IMAGE);
                    item.setLockerImageUrl(lockerImageUrl);
                } else {
                    item.setLockerImageUrl(lockerImageUrl);
                }
                List<LoyaltyItem.LoyaltyCardItem> coupons = new ArrayList<>();
                JSONArray couponsJson = obj.getJSONArray(COUPONS_DATA);
                for (int j = 0; j < couponsJson.length(); j++) {
                    JSONObject itemJson = couponsJson.getJSONObject(j);
                    LoyaltyItem.LoyaltyCardItem coupon = new LoyaltyItem.LoyaltyCardItem();
                    coupon.setCouponCode(getValue(itemJson, COUPON_CODE));
                    coupon.setCouponId(getValue(itemJson, COUPON_ID));
                    if (j == couponsJson.length() - 1) {
                        coupon.setLast(true);
                    }
                    coupons.add(coupon);
                }
                item.setCoupons(coupons);
                loyaltyList.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return loyaltyList;
    }

    public static final List<YoutubeRssItem> parseYoutubeRssList(String data) {
        List<YoutubeRssItem> dataList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                YoutubeRssItem item = new YoutubeRssItem();
                item.setCreator(getValue(obj, CREATOR));
                item.setDescription(getValue(obj, "description"));
                item.setFeedlinkCountHint(getValue(obj, FEED_LINK_COUNT_HIHT));
                item.setFeedlinkHref(getValue(obj, FEED_LINK_HREF));
                item.setId(getValue(obj, "id"));
                item.setImageUrl(getValue(obj, IMAGE_URL));
                item.setLink(getValue(obj, LINK));
                item.setMediaThumbnailUrl(getValue(obj, MEDIA_THUMBNAIL_URL));
                item.setPublished(getValue(obj, PUBLISHED));
                item.setRatingAverage(getValue(obj, RATING_AVERAGE));
                item.setSection(getValue(obj, SECTION));
                item.setStatisticsViewCount(getValue(obj, STATISTICS_VIEW_COUNT));
                item.setSubtitle(getValue(obj, SUBTITLE));
                item.setSummary(getValue(obj, SUMMARY));
                item.setTitle(getValue(obj, "title"));
                item.setBackground(getValue(obj, "background"));
                if (i == 0) {
                    item.setNote(getValue(obj, "note"));
                }
                dataList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public static final List<SearchItem> parseTwitterSearchList(String data) {
        List<SearchItem> dataList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONObject(data).getJSONArray(STATUSES);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                SearchItem item = new SearchItem();
                item.setDate(getValue(obj, CREATED_AT));
                JSONObject userObject = obj.getJSONObject("user");
                if (userObject != null) {
                    item.setTitle(getValue(userObject, "name"));
                    if (userObject.has(SCREEN_NAME)) {
                        item.setName("@" + getValue(userObject, SCREEN_NAME));
                    }
                    item.setImage(getValue(userObject, PROFILE_IMAGE_URL));
                }
                item.setText(getValue(obj, TEXT));
                dataList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public static final List<RssItem> parseRssList(String data) {
        List<RssItem> dataList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                RssItem item = new RssItem();
                if (i == 0) {
                    item.setBackground(getValue(obj, "background"));
                    item.setTintColor(getValue(obj, MUSIC_TINT_COLOR));
                }
                item.setAudioUrl(getValue(obj, AUDIO));
                item.setCreator(getValue(obj, CREATOR));
                item.setDescription(getValue(obj, "description"));
                item.setId(getValue(obj, "id"));
                item.setImageUrl(getValue(obj, IMAGE_URL));
                item.setLink(getValue(obj, LINK));
                item.setSection(getValue(obj, SECTION));
                item.setSubtitle(getValue(obj, SUBTITLE));
                item.setSummary(getValue(obj, SUMMARY));
                item.setTitle(getValue(obj, "title"));
                item.setIcon(getValue(obj, ICON));
                dataList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public static final RichPushNotification parseRichNotification(String data) {
        RichPushNotification result = new RichPushNotification();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                result.setId(getValue(obj, "id"));
                if (!getValue(obj, RICH_TAB_ID).equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    System.out.println("!!!!!!!! getValue(obj, RICH_TAB_ID) = " + getValue(obj, RICH_TAB_ID));
                    result.setTabId(getValue(obj, RICH_TAB_ID));
                }
                System.out.println("!!!!!!!!! result.getTabId() =" + result.getTabId());
                result.setType(getIntValue(obj, RICH_TYPE));
                result.setUrl(getValue(obj, RICH_URL));
                result.setCatId(getValue(obj, RICH_CAT_ID));
                result.setDetailId(getValue(obj, RICH_DETAIL_ID));
                result.setLatitude(getValue(obj, "latitude"));
                result.setLongitude(getValue(obj, "longitude"));
                result.setRadius(getValue(obj, "radius"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static final List<PlaylistItem> parseMusicList(String data) {
        List<PlaylistItem> dataList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                PlaylistItem item = new PlaylistItem();
                if (i == 0) {
                    item.setBackground(getValue(obj, "background"));
                    item.setHeader(getValue(obj, MUSIC_HEADER_IMG));
                    item.setTintColor(getValue(obj, MUSIC_TINT_COLOR));
                }
                item.setArtist(getValue(obj, MUSIC_ARTIST));
                item.setAlbum(getValue(obj, MUSIC_ALBUM));
                item.setId(getValue(obj, "id"));
                item.setItune(getValue(obj, MUSIC_ITUNE));
                item.setDescription(getValue(obj, "note"));
                item.setOnSale(getIntValue(obj, MUSIC_ONSALE));
                item.setAlbumArt(getValue(obj, MUSIC_ALBUM_ART));
                item.setPreviewUrl(getValue(obj, MUSIC_PREVIEW_URL));
                item.setTitle(getValue(obj, "title"));
                dataList.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dataList;
    }

    public static final List<CouponItem.CouponsLocation> parseCouponsLocation(String data) {
        List<CouponItem.CouponsLocation> locations = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                CouponItem.CouponsLocation item = new CouponItem.CouponsLocation();
                item.setLatitude(getValue(obj, "latitude"));
                item.setLongitude(getValue(obj, "longitude"));
                locations.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return locations;
    }

    public static final List<WebTierItem> parseWebTiers(String data) {
        List<WebTierItem> webTiers = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                WebTierItem item = new WebTierItem();
                item.setId(getValue(obj, "id"));
                item.setUrl(getValue(obj, "URL".toLowerCase()));
                item.setTitle(getValue(obj, "title"));
                item.setBackground(getValue(obj, "background"));
                item.setImage(getValue(obj, "thumbnail"));
                webTiers.add(item);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return webTiers;
    }

    public static final ReservationDataKeeper.PaymentData parsePaymentData(String data) {
        ReservationDataKeeper.PaymentData resultData = new ReservationDataKeeper.PaymentData();
        try {
            JSONArray paymentGateways = new JSONArray(data);
            if (0 < paymentGateways.length()) {
                JSONObject gateway = paymentGateways.optJSONObject(0);
                int gatewayType = gateway.optInt(GATEWAY_TYPE);
                resultData.setGatewayType(gatewayType);
                if (gatewayType == 1) {
                    resultData.setPaypalAppID(gateway.optString(GATEWAY_APP_ID));
                    resultData.setRecipientEmail(gateway.optString(GATEWAY_KEY));
                } else if (gatewayType == 3) {
                    resultData.setAuthorizeNetLoginId(gateway.optString(GATEWAY_APP_ID));
                    resultData.setAuthorizeNetTransKey(gateway.optString(GATEWAY_KEY));
                } else if (gatewayType == 2) {
                    resultData.setMerchantId(gateway.optString(GATEWAY_APP_ID));
                    resultData.setMerchantKey(gateway.optString(GATEWAY_KEY));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultData;
    }

    public static final ReservationDataKeeper parseSessionToken(String data) {
        ReservationDataKeeper resultData = new ReservationDataKeeper();
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (0 < jsonArray.length()) {
                JSONObject obj = jsonArray.getJSONObject(0);
                resultData.setSessionToken(getValue(obj, "token"));
                JSONObject userInfo = obj.optJSONObject(USER_INFO);
                if (userInfo != null) {
                    resultData.setUserEmail(userInfo.optString("u"));
                    resultData.setUserFirstName(userInfo.optString("f"));
                    resultData.setUserLastName(userInfo.optString(ReservationSystemConstants.USER_LAST_NAME));
                    resultData.setUserPhone(userInfo.optString("c"));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return resultData;
    }

    public static final List<ReservationItem> parseReservationData(String data) {
        List<ReservationItem> reservations = new ArrayList<>();
        try {
            JSONObject rootObject = new JSONArray(data).getJSONObject(0);
            if (rootObject.has("error") && Integer.parseInt(rootObject.optString("error")) == 9) {
                return null;
            }
            JSONArray jsonArray = rootObject.getJSONArray("orders");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                ReservationItem item = new ReservationItem();
                item.setId(getValue(obj, "id"));
                item.setDate(getValue(obj, "date"));
                item.setTimeFrom(getValue(obj, RESERVATION_TIME_FROM));
                item.setTimeTo(getValue(obj, RESERVATION_TIME_TO));
                item.setUserId(getValue(obj, ServerConstants.POST_USER_ID_PARAM));
                item.setItemId(getValue(obj, "item_id"));
                item.setPaidAmount(getFloatValue(obj, RESERVATION_PAID_AMOUNT));
                item.setServiceName(getValue(obj, RESERVATION_SERVICE_NAME));
                item.setOrderState(getValue(obj, RESERVATION_ORDER_STATE));
                item.setNote(getValue(obj, "note"));
                item.setAppId(getValue(obj, "app_id"));
                item.setTabId(getValue(obj, ServerConstants.POST_TAB_ID_PARAM));
                item.setPlacedOn(getValue(obj, RESERVATION_PLACED_ON));
                item.setCurrency(getValue(obj, CURRENCY));
                item.setCurrencySign(getValue(obj, CURRENCY_SIGN));
                item.setLocId(getValue(obj, "loc_id"));
                item.setTransactionId(getValue(obj, RESERVATION_TRANSACTION_ID));
                item.setCheckoutMethod(getValue(obj, RESERVATION_CHECKOUT_METHOD));
                item.setBillingAddressId(getValue(obj, RESERVATION_BILLING_ADDRESS_ID));
                item.setCost(getFloatValue(obj, RESERVATION_COST));
                item.setDuration(getValue(obj, RESERVATION_DURATION));
                item.setImageUrl(getValue(obj, RESERVATION_IMAGE_URL));
                item.setThumbnail(getValue(obj, "thumbnail"));
                reservations.add(item);
            }
            return reservations;
        } catch (Exception ex) {
            ex.printStackTrace();
            return reservations;
        }
    }

    public static final List<ReservationServiceItem> parseReservationServiceData(String data) {
        List<ReservationServiceItem> items = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (!hasDataError(data)) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.optJSONObject(i);
                    ReservationServiceItem item = new ReservationServiceItem();
                    item.setId(getValue(jsonObject, "id"));
                    item.setName(getValue(jsonObject, "name"));
                    item.setMins(getIntValue(jsonObject, MINS));
                    item.setPrice(getFloatValue(jsonObject, PRICE));
                    item.setCurrency(getValue(jsonObject, CURRENCY));
                    item.setCurrencySign(getValue(jsonObject, CURRENCY_SIGN));
                    item.setNote(getValue(jsonObject, "note"));
                    item.setReserveFee(getFloatValue(jsonObject, RESERV_FEE));
                    item.setThumbnail(getValue(jsonObject, "thumbnail"));
                    JSONArray restWeeksArray = jsonObject.optJSONArray(REST_WEEK);
                    ArrayList<String> restWeeks = new ArrayList<>();
                    for (int j = 0; j < restWeeksArray.length(); j++) {
                        restWeeks.add(restWeeksArray.optString(j).toLowerCase());
                    }
                    item.setRestWeeks(restWeeks);
                    items.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public static List<ReservationTimeItem> parseReservationTimeData(String data) {
        List<ReservationTimeItem> items = null;
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (jsonArray.optJSONObject(0).has("error") && Integer.parseInt(jsonArray.optJSONObject(0).optString("error")) == 9) {
                return null;
            }
            if (jsonArray != null) {
                items = new ArrayList<>();
            }
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                ReservationTimeItem item = new ReservationTimeItem();
                item.setFrom(getIntValue(obj, "from"));
                item.setTo(getIntValue(obj, "to"));
                items.add(item);
            }
            return items;
        } catch (Exception e) {
        }
    }

    public static ReservationDataKeeper parseReservationCenterData(String jsonData) {
        ReservationDataKeeper dataKeeper = new ReservationDataKeeper();
        try {
            JSONArray jsonArray = new JSONArray(jsonData);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                dataKeeper.setAdminEmail(getValue(obj, ADMIN_EMAIL));
                dataKeeper.setBackground(getValue(obj, "background"));
                dataKeeper.setLocations(parseLocationList(getValue(obj, LOCATIONS)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return dataKeeper;
    }

    public static NewsSettings getNewsSettings(String data) {
        NewsSettings result = new NewsSettings();
        try {
            JSONObject rootObject = new JSONArray(data).getJSONObject(0);
            result.setGoogleSearchKey(rootObject.getJSONObject(GOOGLE).getString(KEY));
            result.setTwitterSearchKey(rootObject.getJSONObject(TWITTER).getString(KEY));
            if (getValue(rootObject.getJSONObject(GOOGLE), ACTIVE).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                result.setGoogleActive(false);
            }
            if (getValue(rootObject.getJSONObject(TWITTER), ACTIVE).equals(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                result.setTwitterActive(false);
            }
            result.setBackground(rootObject.getString("background"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getBearerToken(String data) {
        try {
            return new JSONObject(data).getString("access_token");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTwitterIconUrl(String data) {
        try {
            return new JSONObject(data).getString(PROFILE_IMAGE_URL);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean hasDataError(String data) {
        try {
            JSONArray jsonArray = new JSONArray(data);
            if (0 >= jsonArray.length() || getIntValue(jsonArray.getJSONObject(0), "error") == 0) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    private static String getValue(JSONObject obj, String key) throws JSONException {
        return obj.has(key) ? obj.get(key).toString() : EMPTY_STRING;
    }

    private static int getIntValue(JSONObject obj, String key) throws JSONException {
        if (!obj.has(key) || !StringUtils.isNotEmpty(obj.get(key).toString())) {
            return 0;
        }
        try {
            return Integer.parseInt(obj.get(key).toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static float getFloatValue(JSONObject obj, String key) throws JSONException {
        if (!obj.has(key) || !StringUtils.isNotEmpty(obj.get(key).toString())) {
            return 0.0f;
        }
        try {
            return Float.parseFloat(obj.get(key).toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0f;
        }
    }

    private static double getDoubleValue(JSONObject obj, String key) throws JSONException {
        if (!obj.has(key) || !StringUtils.isNotEmpty(obj.get(key).toString())) {
            return 0.0d;
        }
        try {
            return Double.parseDouble(obj.get(key).toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0d;
        }
    }
}
