package com.biznessapps.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.AppSettings;
import com.facebook.AppEventsConstants;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;

public class CommonUtils {
    private static final int BUFFER_SIZE = 50000;

    public static String getMD5Hash(String stringToHash) {
        try {
            MessageDigest m = MessageDigest.getInstance(AppConstants.MD5);
            m.reset();
            m.update(stringToHash.getBytes());
            String hashtext = new BigInteger(1, m.digest()).toString(16);
            while (hashtext.length() < 32) {
                hashtext = AppEventsConstants.EVENT_PARAM_VALUE_NO + hashtext;
            }
            return hashtext;
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap getMadePhoto(Intent data) {
        if (data == null || data.getExtras() == null) {
            return null;
        }
        return Bitmap.createScaledBitmap((Bitmap) data.getExtras().get("data"), 800, 800, true);
    }

    public static byte[] convertImageToBytes(Bitmap scaledBitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 75, baos);
        return baos.toByteArray();
    }

    public static LayerDrawable getCompositeDrawable(Resources res, int colorForBg, int drawableResId) {
        Bitmap drawableBitmap = BitmapFactory.decodeResource(res, drawableResId);
        Bitmap bgBitmap = drawableBitmap.copy(Bitmap.Config.ARGB_8888, true);
        for (int x = 0; x < bgBitmap.getWidth(); x++) {
            for (int y = 0; y < bgBitmap.getHeight(); y++) {
                if (bgBitmap.getPixel(x, y) != 0) {
                    bgBitmap.setPixel(x, y, colorForBg);
                }
            }
        }
        return new LayerDrawable(new Drawable[]{new BitmapDrawable(bgBitmap), new BitmapDrawable(drawableBitmap)});
    }

    public static BitmapDrawable getColoredDrawable(Resources res, int colorForBg, int drawableResId) {
        Bitmap drawableBitmap = BitmapFactory.decodeResource(res, drawableResId);
        Bitmap bgBitmap = drawableBitmap.copy(Bitmap.Config.ARGB_8888, true);
        for (int x = 0; x < bgBitmap.getWidth(); x++) {
            for (int y = 0; y < bgBitmap.getHeight(); y++) {
                if (bgBitmap.getPixel(x, y) != 0) {
                    bgBitmap.setPixel(x, y, colorForBg);
                }
            }
        }
        BitmapDrawable coloredDrawable = new BitmapDrawable(bgBitmap);
        drawableBitmap.recycle();
        return coloredDrawable;
    }

    public static void overrideImageColor(int color, Drawable drawable) {
        drawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    public static void overrideMediumButtonColor(int color, Drawable drawable) {
        drawable.setColorFilter(color, PorterDuff.Mode.MULTIPLY);
    }

    public static void customizeFooterNavigationBar(View footerContainer) {
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        if (-16777216 != settings.getNavigationBarColor()) {
            footerContainer.setBackgroundColor(settings.getNavigationBarColor());
        }
    }

    public static StateListDrawable getButtonDrawable(Drawable selectedState, Drawable unSelectedState) {
        StateListDrawable listItemDrawable = new StateListDrawable();
        listItemDrawable.addState(new int[]{-16842919, -16842913}, unSelectedState);
        listItemDrawable.addState(new int[]{16842919}, selectedState);
        listItemDrawable.addState(new int[]{16842913}, selectedState);
        return listItemDrawable;
    }

    public static String getPath(Uri uri, Activity activity) {
        Cursor cursor = activity.managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static byte[] convertFileToBytes(File file) {
        if (file == null) {
            return null;
        }
        try {
            FileInputStream fio = new FileInputStream(file);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            copy(fio, baos);
            return baos.toByteArray();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e2) {
            return null;
        }
    }

    public static void copy(InputStream input, OutputStream output) throws IOException {
        byte[] buf = new byte[BUFFER_SIZE];
        while (true) {
            int len = input.read(buf);
            if (len > 0) {
                output.write(buf, 0, len);
            } else {
                output.close();
                return;
            }
        }
    }

    public static List<String> getUrlsFromPlsStream(String plsUrl) {
        List<String> streams = new ArrayList<>();
        try {
            byte[] buf = new byte[10240];
            new URL(plsUrl).openStream().read(buf, 0, 10240);
            String[] lines = new String(buf).split(AppConstants.NEW_LINE);
            if (lines != null && lines.length > 0) {
                for (String line : lines) {
                    String[] splitedLine = line.split("=");
                    if (splitedLine.length == 2 && "file1".equals(splitedLine[0].toLowerCase())) {
                        String streamUrl = splitedLine[1];
                        if (streamUrl.charAt(streamUrl.length() - 1) == 13) {
                            streamUrl = streamUrl.substring(0, streamUrl.length() - 2);
                        }
                        streams.add(streamUrl);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return streams;
    }

    public static void sendAnalyticsEvent(AnalyticItem item) {
        Tracker tracker;
        String defaultApp = item.getContext().getString(R.string.code_name);
        boolean isPreviewApp = defaultApp.equalsIgnoreCase(AppConstants.BIZNESS_APPS_CODE) || defaultApp.equalsIgnoreCase(AppConstants.PREVIEW_APPS_CODE);
        if (StringUtils.isNotEmpty(item.getAccountId()) && (tracker = GoogleAnalytics.getInstance(item.getContext()).getTracker(item.getAccountId())) != null) {
            String appId = item.getAppId();
            String tabId = item.getTabId();
            String itemId = StringUtils.isNotEmpty(item.getItemId()) ? item.getItemId() : AppEventsConstants.EVENT_PARAM_VALUE_NO;
            String catId = StringUtils.isNotEmpty(item.getCatId()) ? item.getCatId() : AppEventsConstants.EVENT_PARAM_VALUE_NO;
            tracker.setCustomDimension(1, appId);
            String myVersion = Build.VERSION.RELEASE;
            String str = Build.MODEL;
            tracker.setCustomDimension(2, "Android " + myVersion);
            tracker.setCustomDimension(3, str);
            if (!isPreviewApp) {
                defaultApp = null;
            }
            tracker.setCustomDimension(4, defaultApp);
            if (tabId != null) {
                tracker.sendEvent(tabId, itemId, catId, 0L);
            }
        }
    }

    public static void sendTimingEvent(Context context, String categoryName, long loadingTime) {
        Tracker tracker;
        String defaultApp = context.getString(R.string.code_name);
        boolean isPreviewApp = defaultApp.equalsIgnoreCase(AppConstants.BIZNESS_APPS_CODE) || defaultApp.equalsIgnoreCase(AppConstants.PREVIEW_APPS_CODE);
        AppSettings settings = AppCore.getInstance().getAppSettings();
        if (StringUtils.isNotEmpty(settings.getGaAccountId()) && (tracker = GoogleAnalytics.getInstance(context).getTracker(settings.getGaAccountId())) != null) {
            String myVersion = Build.VERSION.RELEASE;
            String str = Build.MODEL;
            tracker.setCustomDimension(1, settings.getAppId());
            tracker.setCustomDimension(2, "Android " + myVersion);
            tracker.setCustomDimension(3, str);
            if (!isPreviewApp) {
                defaultApp = null;
            }
            tracker.setCustomDimension(4, defaultApp);
            tracker.sendTiming(categoryName, loadingTime, "Loading time", AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
    }
}
