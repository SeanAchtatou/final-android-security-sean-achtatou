package com.biznessapps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHandlers;
import com.biznessapps.api.CachingManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.MusicDelegate;
import com.biznessapps.layout.R;
import com.biznessapps.model.AppSettings;
import com.biznessapps.utils.google.caching.ImageWorker;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class ViewUtils {
    public static final String DRAWABLE_PACKAGE = "drawable/";
    private static final float SHADOW_VALUE = 1.2f;
    private static final int VIEW_PADDING = 5;

    public static View loadLayout(Context context, int id) {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(id, (ViewGroup) null);
    }

    public static void updateMusicBarBottomMargin(MusicDelegate musicDelegate, int bottomMargin) {
        if (musicDelegate != null && musicDelegate.isActive()) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) musicDelegate.getRootView().getLayoutParams();
            lp.setMargins(0, 0, 0, bottomMargin);
            musicDelegate.getRootView().setLayoutParams(lp);
        }
    }

    public static int getColor(String color) {
        try {
            return Color.parseColor("#" + color);
        } catch (Exception e) {
            return -16777216;
        }
    }

    public static int getColor(String color, int defaultColor) {
        int colorValue = defaultColor;
        try {
            return Color.parseColor("#" + color);
        } catch (Exception e) {
            return colorValue;
        }
    }

    public static void showMailingListPropmt(final CommonFragmentActivity holdActivity) {
        final Context context = holdActivity.getApplicationContext();
        SharedPreferences sp = context.getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0);
        final AppSettings appSettings = AppCore.getInstance().getAppSettings();
        if (sp.getBoolean(AppConstants.SHOW_MAILING_DIALOG, true) && appSettings != null && appSettings.isMailingListPrompt()) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(holdActivity);
            alertBuilder.setTitle(R.string.mailing_list);
            alertBuilder.setMessage(R.string.mailing_list_question);
            alertBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(context, SingleFragmentActivity.class);
                    intent.putExtra(AppConstants.TAB_LABEL, holdActivity.getIntent().getStringExtra(AppConstants.TAB_LABEL));
                    intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.MAILING_LIST_VIEW_CONTROLLER);
                    if (holdActivity.getNavigationManager() != null) {
                        holdActivity.getNavigationManager().setTabSelection(appSettings.getMailingListTabId());
                    }
                    holdActivity.startActivity(intent);
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            alertBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            alertBuilder.create().show();
            sp.edit().putBoolean(AppConstants.SHOW_MAILING_DIALOG, false).commit();
        }
    }

    public static void showOfflineCachingPropmt(CommonFragmentActivity holdActivity) {
        SharedPreferences sp = holdActivity.getApplicationContext().getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0);
        AppSettings appSettings = AppCore.getInstance().getAppSettings();
        if (sp.getBoolean(AppConstants.SHOW_OFFLINE_CACHING_DIALOG, true) && appSettings != null && appSettings.isOfflineCachingPrompt()) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(holdActivity);
            alertBuilder.setTitle(R.string.offline_caching_dialog_title);
            alertBuilder.setMessage(R.string.offline_caching_dialog_message);
            alertBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AppCore.getInstance().setUseOfflineMode(true);
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            alertBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    AppCore.getInstance().setUseOfflineMode(false);
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                }
            });
            alertBuilder.create().show();
            sp.edit().putBoolean(AppConstants.SHOW_OFFLINE_CACHING_DIALOG, false).commit();
        }
    }

    public static ViewGroup getProgressBar(Context appContext) {
        LinearLayout ll = new LinearLayout(appContext);
        ll.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ll.setPadding(5, 5, 5, 5);
        ll.setGravity(17);
        ll.setBackgroundResource(R.drawable.progress_bar_bg);
        ProgressBar progressBar = new ProgressBar(appContext);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(30, 30));
        ll.addView(progressBar);
        TextView textView = new TextView(appContext);
        textView.setText(R.string.loading);
        textView.setTextColor(-3355444);
        textView.setTextSize(15.0f);
        textView.setShadowLayer(SHADOW_VALUE, SHADOW_VALUE, SHADOW_VALUE, R.color.red_black);
        textView.setPadding(5, 0, 5, 0);
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        ll.addView(textView);
        return ll;
    }

    public static ViewGroup getProgressBar(Context appContext, int messageResourceId) {
        LinearLayout ll = new LinearLayout(appContext);
        ll.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ll.setPadding(5, 5, 5, 5);
        ll.setGravity(17);
        ll.setBackgroundResource(R.drawable.progress_bar_bg);
        ProgressBar progressBar = new ProgressBar(appContext);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(20, 20));
        ll.addView(progressBar);
        TextView textView = new TextView(appContext);
        textView.setText(messageResourceId);
        textView.setTextColor(-3355444);
        textView.setShadowLayer(SHADOW_VALUE, SHADOW_VALUE, SHADOW_VALUE, R.color.red_black);
        textView.setPadding(5, 0, 5, 0);
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        ll.addView(textView);
        return ll;
    }

    public static void tweetAppName(Context context) {
        tweet(context, context.getString(R.string.check_out_app) + " " + AppConstants.MARKET_TEMPLATE_URL + context.getPackageName());
    }

    public static void tweetAppName(Context context, String tweetText) {
        tweet(context, tweetText);
    }

    public static void tweet(final Context context, final String dataToTweet) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    CachingManager cacher = AppCore.getInstance().getCachingManager();
                    AccessToken accessToken = new AccessToken(cacher.getTwitterOauthToken(), cacher.getTwitterOauthSecret());
                    Twitter twitter = new TwitterFactory().getInstance();
                    twitter.setOAuthConsumer("ibeMh2JAmmQw09B1nfap5Q", "dkomjgXm50XtNmWDn0FhJJpswGvdfIPqfYwfxqMar38");
                    twitter.setOAuthAccessToken(accessToken);
                    twitter.updateStatus(dataToTweet);
                    AppHandlers.getUiHandler().post(new Runnable() {
                        public void run() {
                            ViewUtils.showShortToast(context, R.string.twitting_successful);
                        }
                    });
                } catch (Exception e) {
                    AppHandlers.getUiHandler().post(new Runnable() {
                        public void run() {
                            ViewUtils.showShortToast(context, R.string.twitting_failure);
                        }
                    });
                }
            }
        }).start();
    }

    public static ViewGroup getImageProgressBar(Context appContext) {
        LinearLayout ll = new LinearLayout(appContext);
        ll.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        ll.setPadding(5, 5, 5, 5);
        ll.setGravity(17);
        ProgressBar progressBar = new ProgressBar(appContext);
        progressBar.setLayoutParams(new ViewGroup.LayoutParams(100, 100));
        ll.addView(progressBar);
        return ll;
    }

    public static void makeCall(Context context, String tel) {
        if (StringUtils.isNotEmpty(tel)) {
            Intent intent = new Intent("android.intent.action.DIAL", Uri.parse(AppConstants.TEL_TYPE + tel));
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
    }

    public static TextView.OnEditorActionListener getOnEditorListener(final Button buttonToClick) {
        return new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean actionDone;
                boolean needToPost = false;
                if (actionId == 6) {
                    actionDone = true;
                } else {
                    actionDone = false;
                }
                if (event != null && event.getAction() == 0 && (event.getKeyCode() == 66 || event.getKeyCode() == 23)) {
                    needToPost = true;
                }
                if (actionDone || needToPost) {
                    buttonToClick.performClick();
                }
                return true;
            }
        };
    }

    public static View.OnKeyListener getOnEnterKeyListener(final Runnable runnable) {
        return new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode != 66) {
                    return false;
                }
                runnable.run();
                return false;
            }
        };
    }

    public static void openLinkInBrowser(Context context, String url) {
        if (StringUtils.isNotEmpty(url)) {
            if (!url.startsWith(AppConstants.HTTP_PREFIX) && !url.startsWith(AppConstants.HTTPS_PREFIX)) {
                url = AppConstants.HTTP_PREFIX + url;
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
    }

    public static void plubWebViewWithoutZooming(WebView webView) {
        if (webView != null) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setPluginsEnabled(true);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setDomStorageEnabled(true);
        }
    }

    public static void plubWebView(WebView webView) {
        if (webView != null) {
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setPluginsEnabled(true);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDomStorageEnabled(true);
        }
    }

    public static void showShortToast(Context context, int messageId) {
        try {
            Toast.makeText(context, messageId, 0).show();
        } catch (Exception e) {
        }
    }

    public static void email(Activity activity, String[] emails, String subject, String text) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType(AppConstants.PLAIN_TEXT);
        if (StringUtils.isNotEmpty(emails)) {
            intent.putExtra("android.intent.extra.EMAIL", emails);
        }
        if (StringUtils.isNotEmpty(subject)) {
            intent.putExtra("android.intent.extra.SUBJECT", subject);
        }
        if (StringUtils.isNotEmpty(text)) {
            intent.putExtra("android.intent.extra.TEXT", text);
        }
        intent.addFlags(268435456);
        activity.startActivity(Intent.createChooser(intent, "Email"));
    }

    public static void call(Context context, String telephoneNumber) {
        if (StringUtils.isNotEmpty(telephoneNumber)) {
            Intent intent = new Intent("android.intent.action.DIAL", Uri.parse(AppConstants.TEL_TYPE + telephoneNumber));
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
    }

    public static void openGoogleMap(Context appContext, String longitude, String latitude) {
        String mapsUrl = ("geo:0,0?q=" + latitude + "," + longitude) + "(" + appContext.getString(R.string.direction_label) + ")";
        Location currentLocation = AppCore.getInstance().getLocationFinder().getCurrentLocation();
        if (currentLocation != null) {
            mapsUrl = String.format("http://maps.google.com/maps?saddr=%s,%s&daddr=%s,%s", Double.valueOf(currentLocation.getLatitude()), Double.valueOf(currentLocation.getLongitude()), latitude, longitude);
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(mapsUrl));
        intent.addCategory("android.intent.category.BROWSABLE");
        intent.putExtra("com.android.browser.application_id", appContext.getPackageName());
        intent.addFlags(268435456);
        intent.addFlags(67108864);
        try {
            appContext.startActivity(intent);
        } catch (ActivityNotFoundException e) {
        }
    }

    public static void showWebViewData(Activity activity, String url, int titleResourceId) {
        Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle(titleResourceId);
        dialog.setCancelable(true);
        final WebView webView = (WebView) dialog.findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                webView.setVisibility(0);
                webView.requestFocus(130);
            }
        });
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.loadUrl(url);
        dialog.show();
    }

    public static String getExtraKey(Bundle extras, String key, String defaultValue) {
        return (extras == null || !extras.containsKey(key)) ? defaultValue : extras.getString(key);
    }

    public static boolean hasExtraValue(Intent intent, String extraKey) {
        return intent.getExtras() != null && intent.getExtras().containsKey(extraKey) && StringUtils.isNotEmpty(intent.getExtras().getString(extraKey));
    }

    public static boolean hasLongExtraValue(Intent intent, String extraKey) {
        return (intent.getExtras() == null || !intent.getExtras().containsKey(extraKey) || intent.getExtras().getLong(extraKey, 0) == 0) ? false : true;
    }

    public static ProgressDialog getProgressDialog(Context context) {
        return getProgressDialog(context, R.string.waiting);
    }

    public static ProgressDialog getProgressDialog(Context context, int messageId) {
        return getProgressDialog(context, messageId, false);
    }

    public static ProgressDialog getProgressDialog(Context context, int messageId, boolean cancelable) {
        ProgressDialog dialog = new ProgressDialog(context);
        dialog.setMessage(context.getResources().getString(messageId));
        dialog.setIndeterminate(true);
        dialog.setCancelable(cancelable);
        return dialog;
    }

    public static void showDialog(Activity activity, int messageId) {
        showDialog(activity, activity.getResources().getString(messageId));
    }

    public static void showDialog(Activity activity, String message) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        alertBuilder.setView(loadLayout(activity.getApplicationContext(), R.layout.common_dialog_layout));
        alertBuilder.setMessage(message);
        AlertDialog dialog = alertBuilder.create();
        dialog.setButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    public static void showDialog(Activity activity, String message, final Runnable runnable) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        alertBuilder.setView(loadLayout(activity.getApplicationContext(), R.layout.common_dialog_layout));
        alertBuilder.setMessage(message);
        AlertDialog dialog = alertBuilder.create();
        dialog.setButton(activity.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                runnable.run();
            }
        });
        dialog.show();
    }

    public static void checkGpsEnabling(Activity activity) {
        if (!((LocationManager) activity.getSystemService("location")).isProviderEnabled("gps")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(R.string.gps_disabled_info).setCancelable(false).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            builder.create().show();
        }
    }

    public static boolean showAdsIfNeeded(Activity holdActivity, ViewGroup rootContainer, boolean isAdsContainer) {
        AppSettings appSettings = AppCore.getInstance().getAppSettings();
        if (!StringUtils.isNotEmpty(appSettings.getAdWhirlID())) {
            return false;
        }
        initAds(holdActivity, rootContainer, isAdsContainer, appSettings.getAdWhirlID());
        return true;
    }

    public static void showTitleBar(ViewGroup titleBarRoot, Intent intent, boolean isAdsShown) {
        int i = 0;
        if (titleBarRoot != null) {
            TextView titleTextView = (TextView) titleBarRoot.findViewById(R.id.tab_title_text);
            AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
            if (titleTextView != null) {
                if (intent.getBooleanExtra(AppConstants.CHILDREN_TAB_LABEL_PRESENT, false)) {
                    titleTextView.setText(intent.getStringExtra(AppConstants.CHILDREN_TAB_LABEL));
                } else {
                    titleTextView.setText(intent.getStringExtra(AppConstants.TAB_LABEL));
                }
                titleTextView.setTextColor(uiSettings.getNavigationTextColor());
                titleTextView.setShadowLayer(SHADOW_VALUE, SHADOW_VALUE, SHADOW_VALUE, uiSettings.getNavigationTextShadowColor());
                if (isAdsShown) {
                    i = 4;
                }
                titleTextView.setVisibility(i);
            }
            if (!isAdsShown) {
                AppSettings appSettings = AppCore.getInstance().getAppSettings();
                if (StringUtils.isNotEmpty(appSettings.getGlobalHeaderUrl())) {
                    ImageWorker.TintContainer tint = new ImageWorker.TintContainer();
                    tint.setTintColor(appSettings.getNavigBarColor());
                    tint.setTintOpacity(128.0f);
                    AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadImage(appSettings.getGlobalHeaderUrl(), titleBarRoot, tint);
                    return;
                }
                titleBarRoot.setBackgroundDrawable(getTitleBg());
            }
        }
    }

    public static void setGlobalBackgroundColor(View view) {
        if (view != null) {
            view.setBackgroundColor(AppCore.getInstance().getUiSettings().getGlobalBgColor());
        }
    }

    private static GradientDrawable getTitleBg() {
        AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        int originalColor = uiSettings.getNavigationBarColor();
        int topColor = Color.argb((int) AppConstants.TRANSPARENT_BG_VALUE, Color.red(originalColor), Color.green(originalColor), Color.blue(originalColor));
        GradientDrawable gradientBg = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{uiSettings.getNavigationBarColor(), topColor});
        gradientBg.setCornerRadius(0.0f);
        return gradientBg;
    }

    private static void initAds(Activity holdActivity, ViewGroup rootContainer, boolean isAdsContainer, String adsId) {
        System.out.println("!!!!!!! adsId " + adsId);
        AdView adView = new AdView(holdActivity, AdSize.SMART_BANNER, adsId);
        adView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        if (!isAdsContainer) {
            ViewGroup adsLayout = (ViewGroup) rootContainer.findViewById(R.id.ads_layout_container);
            if (adsLayout != null) {
                adsLayout.removeAllViews();
                adsLayout.addView(adView);
                adView.loadAd(new AdRequest());
                adsLayout.setVisibility(0);
            }
        } else if (rootContainer != null) {
            rootContainer.removeAllViews();
            rootContainer.addView(adView);
            adView.loadAd(new AdRequest());
            rootContainer.setVisibility(0);
        }
    }

    public static class HideAnimationListener implements Animation.AnimationListener {
        private View v = null;

        public HideAnimationListener(View v2) {
            this.v = v2;
        }

        public void onAnimationEnd(Animation animation) {
            this.v.setVisibility(8);
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }
}
