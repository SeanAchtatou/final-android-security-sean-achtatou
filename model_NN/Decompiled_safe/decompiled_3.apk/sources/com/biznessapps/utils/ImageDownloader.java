package com.biznessapps.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.widget.ImageView;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.HttpUtils;
import com.biznessapps.constants.ServerConstants;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ImageDownloader {
    private static final int DELAY_BEFORE_PURGE = 10000;
    private static final int HARD_CACHE_CAPACITY = 10;
    private static final String LOG_TAG = "ImageDownloader";
    /* access modifiers changed from: private */
    public static final ConcurrentHashMap<String, SoftReference<Bitmap>> sSoftBitmapCache = new ConcurrentHashMap<>(5);
    private final Handler purgeHandler = new Handler();
    private final Runnable purger = new Runnable() {
        public void run() {
            ImageDownloader.this.clearCache();
        }
    };
    private final HashMap<String, Bitmap> sHardBitmapCache = new LinkedHashMap<String, Bitmap>(5, 0.75f, true) {
        private static final long serialVersionUID = -9051221262797106515L;

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, Bitmap> eldest) {
            if (size() <= 10) {
                return false;
            }
            synchronized (ImageDownloader.sSoftBitmapCache) {
                ImageDownloader.sSoftBitmapCache.put(eldest.getKey(), new SoftReference(eldest.getValue()));
            }
            return true;
        }
    };

    public void download(String url, ImageView imageView) {
        if (StringUtils.isNotEmpty(url)) {
            download(url, imageView, false);
        }
    }

    public void download(String url, ImageView imageView, boolean useImageReflection) {
        resetPurgeTimer();
        Bitmap bitmap = getBitmapFromCache(url);
        if (bitmap == null) {
            forceDownload(url, imageView, useImageReflection);
            return;
        }
        cancelPotentialDownload(url, imageView);
        imageView.setImageBitmap(bitmap);
    }

    private void forceDownload(String url, ImageView imageView, boolean useImageReflection) {
        if (url != null && cancelPotentialDownload(url, imageView)) {
            BitmapDownloaderTask task = new BitmapDownloaderTask(imageView, useImageReflection);
            imageView.setImageDrawable(new DownloadedDrawable(task));
            imageView.setMinimumHeight(156);
            task.execute(url);
        }
    }

    private static boolean cancelPotentialDownload(String url, ImageView imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
        if (bitmapDownloaderTask == null) {
            return true;
        }
        String bitmapUrl = bitmapDownloaderTask.url;
        if (bitmapUrl != null && bitmapUrl.equals(url)) {
            return false;
        }
        bitmapDownloaderTask.cancel(true);
        return true;
    }

    /* access modifiers changed from: private */
    public static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                return ((DownloadedDrawable) drawable).getBitmapDownloaderTask();
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ab  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap downloadBitmap(java.lang.String r13) {
        /*
            r12 = this;
            r8 = 0
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>()
            r3 = 0
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x00d1, IOException -> 0x0069, IllegalStateException -> 0x0088, Exception -> 0x00a8 }
            r4.<init>(r13)     // Catch:{ IllegalArgumentException -> 0x00d1, IOException -> 0x0069, IllegalStateException -> 0x0088, Exception -> 0x00a8 }
            org.apache.http.HttpResponse r6 = r0.execute(r4)     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
            org.apache.http.StatusLine r9 = r6.getStatusLine()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
            int r7 = r9.getStatusCode()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
            r9 = 200(0xc8, float:2.8E-43)
            if (r7 == r9) goto L_0x001e
            r3 = r4
        L_0x001d:
            return r8
        L_0x001e:
            org.apache.http.HttpEntity r2 = r6.getEntity()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
            if (r2 == 0) goto L_0x0067
            r5 = 0
            java.io.InputStream r5 = r2.getContent()     // Catch:{ all -> 0x003d }
            com.biznessapps.utils.ImageDownloader$FlushedInputStream r9 = new com.biznessapps.utils.ImageDownloader$FlushedInputStream     // Catch:{ all -> 0x003d }
            r9.<init>(r5)     // Catch:{ all -> 0x003d }
            android.graphics.Bitmap r9 = android.graphics.BitmapFactory.decodeStream(r9)     // Catch:{ all -> 0x003d }
            if (r5 == 0) goto L_0x0037
            r5.close()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
        L_0x0037:
            r2.consumeContent()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
            r3 = r4
            r8 = r9
            goto L_0x001d
        L_0x003d:
            r9 = move-exception
            if (r5 == 0) goto L_0x0043
            r5.close()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
        L_0x0043:
            r2.consumeContent()     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
            throw r9     // Catch:{ IllegalArgumentException -> 0x0047, IOException -> 0x00ce, IllegalStateException -> 0x00cb, Exception -> 0x00c8 }
        L_0x0047:
            r1 = move-exception
            r3 = r4
        L_0x0049:
            if (r3 == 0) goto L_0x004e
            r3.abort()
        L_0x004e:
            java.lang.String r9 = "ImageDownloader"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Incorrect passed url "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r13)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r9, r10, r1)
            goto L_0x001d
        L_0x0067:
            r3 = r4
            goto L_0x001d
        L_0x0069:
            r1 = move-exception
        L_0x006a:
            if (r3 == 0) goto L_0x006f
            r3.abort()
        L_0x006f:
            java.lang.String r9 = "ImageDownloader"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "I/O error while retrieving bitmap from "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r13)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r9, r10, r1)
            goto L_0x001d
        L_0x0088:
            r1 = move-exception
        L_0x0089:
            if (r3 == 0) goto L_0x008e
            r3.abort()
        L_0x008e:
            java.lang.String r9 = "ImageDownloader"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Incorrect URL: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r13)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r9, r10)
            goto L_0x001d
        L_0x00a8:
            r1 = move-exception
        L_0x00a9:
            if (r3 == 0) goto L_0x00ae
            r3.abort()
        L_0x00ae:
            java.lang.String r9 = "ImageDownloader"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Error while retrieving bitmap from "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r13)
            java.lang.String r10 = r10.toString()
            android.util.Log.w(r9, r10, r1)
            goto L_0x001d
        L_0x00c8:
            r1 = move-exception
            r3 = r4
            goto L_0x00a9
        L_0x00cb:
            r1 = move-exception
            r3 = r4
            goto L_0x0089
        L_0x00ce:
            r1 = move-exception
            r3 = r4
            goto L_0x006a
        L_0x00d1:
            r1 = move-exception
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.utils.ImageDownloader.downloadBitmap(java.lang.String):android.graphics.Bitmap");
    }

    static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private final WeakReference<ImageView> imageViewReference;
        /* access modifiers changed from: private */
        public String url;
        private boolean useImageReflection;

        public BitmapDownloaderTask(ImageView imageView, boolean useImageReflection2) {
            this.imageViewReference = new WeakReference<>(imageView);
            this.useImageReflection = useImageReflection2;
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... params) {
            this.url = params[0];
            this.url = ImageDownloader.this.grabTwitterUrlIfNeed(this.url);
            Bitmap avatarBitmap = ImageDownloader.this.getBitmapFromCache(this.url);
            Bitmap bitmap = avatarBitmap != null ? avatarBitmap : ImageDownloader.this.downloadBitmap(this.url);
            if (!this.useImageReflection || bitmap == null) {
                return bitmap;
            }
            return ImageDownloader.this.createReflectedImages(bitmap);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            if (bitmap != null) {
                ImageDownloader.this.addBitmapToCache(this.url, bitmap);
                if (this.imageViewReference != null) {
                    ImageView imageView = this.imageViewReference.get();
                    if (this == ImageDownloader.getBitmapDownloaderTask(imageView)) {
                        imageView.setImageBitmap(bitmap);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public String grabTwitterUrlIfNeed(String url) {
        if (url.contains("api.twitter.com")) {
            int startIndex = url.indexOf("profile_image");
            int endIndex = url.indexOf(".json");
            if (startIndex > 0 && endIndex > 0) {
                String screenName = url.substring(startIndex + 14, endIndex);
                url = JsonParserUtils.getTwitterIconUrl(HttpUtils.getTwitterData(ServerConstants.GET_TWITTER_PROFILE_URL + screenName, AppCore.getInstance().getBearerAccessToken()));
                if (getBitmapFromCache(url) == null) {
                    HttpUtils.updateAvatarImage(String.format(ServerConstants.UPDATE_AVATAR_URL_FORMAT, screenName, url));
                }
            }
        }
        return url;
    }

    static class DownloadedDrawable extends ColorDrawable {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
            super(0);
            this.bitmapDownloaderTaskReference = new WeakReference<>(bitmapDownloaderTask);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask() {
            return this.bitmapDownloaderTaskReference.get();
        }
    }

    /* access modifiers changed from: private */
    public void addBitmapToCache(String url, Bitmap bitmap) {
        if (bitmap != null) {
            synchronized (this.sHardBitmapCache) {
                this.sHardBitmapCache.put(url, bitmap);
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0022, code lost:
        r1 = com.biznessapps.utils.ImageDownloader.sSoftBitmapCache.get(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x002a, code lost:
        if (r1 == null) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002c, code lost:
        r0 = r1.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        if (r0 != null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0034, code lost:
        com.biznessapps.utils.ImageDownloader.sSoftBitmapCache.remove(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap getBitmapFromCache(java.lang.String r6) {
        /*
            r5 = this;
            r2 = 0
            if (r6 != 0) goto L_0x0005
            r0 = r2
        L_0x0004:
            return r0
        L_0x0005:
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r3 = r5.sHardBitmapCache
            monitor-enter(r3)
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r4 = r5.sHardBitmapCache     // Catch:{ all -> 0x001e }
            java.lang.Object r0 = r4.get(r6)     // Catch:{ all -> 0x001e }
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0     // Catch:{ all -> 0x001e }
            if (r0 == 0) goto L_0x0021
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r2 = r5.sHardBitmapCache     // Catch:{ all -> 0x001e }
            r2.remove(r6)     // Catch:{ all -> 0x001e }
            java.util.HashMap<java.lang.String, android.graphics.Bitmap> r2 = r5.sHardBitmapCache     // Catch:{ all -> 0x001e }
            r2.put(r6, r0)     // Catch:{ all -> 0x001e }
            monitor-exit(r3)     // Catch:{ all -> 0x001e }
            goto L_0x0004
        L_0x001e:
            r2 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x001e }
            throw r2
        L_0x0021:
            monitor-exit(r3)     // Catch:{ all -> 0x001e }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.ref.SoftReference<android.graphics.Bitmap>> r3 = com.biznessapps.utils.ImageDownloader.sSoftBitmapCache
            java.lang.Object r1 = r3.get(r6)
            java.lang.ref.SoftReference r1 = (java.lang.ref.SoftReference) r1
            if (r1 == 0) goto L_0x0039
            java.lang.Object r0 = r1.get()
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            if (r0 != 0) goto L_0x0004
            java.util.concurrent.ConcurrentHashMap<java.lang.String, java.lang.ref.SoftReference<android.graphics.Bitmap>> r3 = com.biznessapps.utils.ImageDownloader.sSoftBitmapCache
            r3.remove(r6)
        L_0x0039:
            r0 = r2
            goto L_0x0004
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.utils.ImageDownloader.getBitmapFromCache(java.lang.String):android.graphics.Bitmap");
    }

    public void clearCache() {
        this.sHardBitmapCache.clear();
        sSoftBitmapCache.clear();
    }

    private void resetPurgeTimer() {
        this.purgeHandler.removeCallbacks(this.purger);
        this.purgeHandler.postDelayed(this.purger, 10000);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: private */
    public Bitmap createReflectedImages(Bitmap originalImage) {
        if (originalImage == null) {
            return null;
        }
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, (int) (((float) height) * 0.5f), width, (int) (((float) height) - (((float) height) * 0.5f)), matrix, false);
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (int) (((float) height) + (((float) height) * 0.5f)), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapWithReflection);
        canvas.drawBitmap(originalImage, 0.0f, 0.0f, (Paint) null);
        new Paint().setColor(17170445);
        canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) originalImage.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
        originalImage.recycle();
        reflectionImage.recycle();
        return bitmapWithReflection;
    }
}
