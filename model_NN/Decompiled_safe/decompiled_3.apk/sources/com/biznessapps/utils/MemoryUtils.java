package com.biznessapps.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MemoryUtils {
    private static final String CACHED_IMAGES = "cached_images_";
    private static final String CLOSE_RESOURCE_ERROR = "Unable to close resource";
    private static final String DCIM = "/DCIM/";
    private static final String DEFAULT_NAME = "imageN";
    private static final int FILE_1_MB = 1048576;
    private static final int FILE_200_KB = 204800;
    private static final int FILE_2_MB = 2097152;
    private static final int FILE_300_KB = 307200;
    private static final int FILE_3_MB = 3145728;
    private static final int FILE_400_KB = 409600;
    private static final int FILE_4_MB = 4194304;
    private static final int FILE_500_KB = 512000;
    private static final int FILE_5_MB = 5242880;
    private static final int FILE_700_KB = 716800;
    private static final String JPG_EXTENTION = ".jpg";
    private static final String LOG_TAG = "Tag";
    private static final int STREAM_BUFFER_SIZE = 65535;
    private static Context appContext;

    public static void setContext(Context context) {
        appContext = context;
    }

    public static String saveAndGetFilePath(String response) {
        if (response == null || response.trim().length() == 0) {
            return null;
        }
        String fileName = DEFAULT_NAME + response.hashCode() + ".jpg";
        File fileDir = null;
        if (getExternalPath() != null) {
            try {
                File fileDir2 = new File(getExternalPath() + DCIM + CACHED_IMAGES);
                try {
                    if (!fileDir2.exists()) {
                        fileDir2.mkdir();
                    }
                    fileDir = fileDir2;
                } catch (Exception e) {
                    fileDir = fileDir2;
                }
            } catch (Exception e2) {
            }
        } else {
            fileDir = appContext.getDir(CACHED_IMAGES, 0);
            fileDir.delete();
            fileDir.mkdir();
        }
        try {
            File file = new File(fileDir, fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            String filePath = file.getAbsolutePath();
            Base64OutputStream output = null;
            output.write(response.getBytes());
            close(output);
            return filePath;
        } catch (FileNotFoundException e3) {
            throw new RuntimeException(e3);
        } catch (IOException e4) {
            throw new RuntimeException(e4);
        } catch (Throwable th) {
            close(null);
            throw th;
        }
    }

    private static String getExternalPath() {
        File path;
        if (!"mounted".equals(Environment.getExternalStorageState()) || (path = Environment.getExternalStorageDirectory()) == null) {
            return null;
        }
        return path.toString();
    }

    public static Drawable getBitmapByData(String data, boolean useMemory) {
        if (useMemory) {
            try {
                byte[] decodedButtonImage = Base64.decode(data, 0);
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inTempStorage = new byte[65536];
                options.inSampleSize = 3;
                options.inPurgeable = true;
                Bitmap bitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(decodedButtonImage), null, options);
                if (bitmap != null) {
                    return new BitmapDrawable(bitmap);
                }
                return null;
            } catch (Throwable e) {
                throw new RuntimeException(e);
            }
        } else {
            Drawable drawable = null;
            if (data != null) {
                try {
                    File file = new File(data);
                    if (file != null && file.isFile()) {
                        long fileLength = file.length();
                        BitmapFactory.Options options2 = new BitmapFactory.Options();
                        int sampleSize = 1;
                        if (fileLength >= 204800) {
                            if (fileLength >= 204800 && fileLength < 307200) {
                                sampleSize = 2;
                            } else if (fileLength >= 307200 && fileLength < 409600) {
                                sampleSize = 3;
                            } else if (fileLength >= 409600 && fileLength < 512000) {
                                sampleSize = 4;
                            } else if (fileLength >= 512000 && fileLength < 716800) {
                                sampleSize = 6;
                            } else if (fileLength >= 716800 && fileLength < 1048576) {
                                sampleSize = 8;
                            } else if (fileLength >= 1048576 && fileLength < 2097152) {
                                sampleSize = 10;
                            } else if (fileLength >= 2097152 && fileLength < 3145728) {
                                sampleSize = 12;
                            } else if (fileLength >= 3145728 && fileLength < 4194304) {
                                sampleSize = 14;
                            } else if (fileLength < 4194304 || fileLength >= 5242880) {
                                sampleSize = 20;
                            } else {
                                sampleSize = 16;
                            }
                        }
                        options2.inTempStorage = new byte[65536];
                        options2.inSampleSize = sampleSize;
                        options2.inPurgeable = true;
                        Bitmap bitmap2 = BitmapFactory.decodeFile(data, options2);
                        if (bitmap2 != null) {
                            drawable = new BitmapDrawable(bitmap2);
                        }
                    }
                } catch (Throwable e2) {
                    throw new RuntimeException(e2);
                }
            }
            return drawable;
        }
    }

    public static void close(Closeable resource) {
        if (resource != null) {
            try {
                resource.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, CLOSE_RESOURCE_ERROR, e);
            }
        }
    }

    public static void copy(InputStream input, OutputStream output) throws IOException {
        byte[] buf = new byte[STREAM_BUFFER_SIZE];
        while (true) {
            int len = input.read(buf);
            if (len > 0) {
                output.write(buf, 0, len);
            } else {
                return;
            }
        }
    }
}
