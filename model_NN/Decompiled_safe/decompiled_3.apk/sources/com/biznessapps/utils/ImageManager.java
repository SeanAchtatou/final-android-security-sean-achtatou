package com.biznessapps.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ImageManager {
    private static final float LOAD_FACTOR = 0.75f;
    private static final String LOG_TAG = "ImageManager";
    private static final int MAX_LIGTH_CACHER_CAPACITY = 40;
    private static final int MAX_MAIN_CACHER_CAPACITY = 7;
    private static final int VIEW_TASK_MAP_CAPACITY = 7;
    /* access modifiers changed from: private */
    public Map<String, Object> lightDrawableRefCacher = new LinkedHashMap<String, Object>(MAX_LIGTH_CACHER_CAPACITY, LOAD_FACTOR, true) {
        private static final long serialVersionUID = -8296798937008399757L;

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, Object> eldest) {
            boolean shouldBeRemoved = size() > ImageManager.MAX_LIGTH_CACHER_CAPACITY;
            if (shouldBeRemoved && eldest != null) {
                ImageManager.this.lightDrawableRefCacher.put(eldest.getKey(), null);
            }
            return shouldBeRemoved;
        }
    };
    /* access modifiers changed from: private */
    public Map<String, Bitmap> lightHardRefCacher = new LinkedHashMap<String, Bitmap>(MAX_LIGTH_CACHER_CAPACITY, LOAD_FACTOR, true) {
        private static final long serialVersionUID = -8296798937008399757L;

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, Bitmap> eldest) {
            boolean shouldBeRemoved = size() > ImageManager.MAX_LIGTH_CACHER_CAPACITY;
            if (shouldBeRemoved && eldest != null) {
                Bitmap bitmap = eldest.getValue();
                synchronized (ImageManager.this.lightHardRefCacher) {
                    ImageManager.this.lightHardRefCacher.put(eldest.getKey(), null);
                    if (bitmap != null) {
                    }
                }
            }
            return shouldBeRemoved;
        }
    };
    private Map<ImageLoadTask, String> loadTasksQueue = Collections.synchronizedMap(new HashMap());
    private Handler mainHandler;
    /* access modifiers changed from: private */
    public Map<String, Bitmap> mainWeakRefCacher = new LinkedHashMap<String, Bitmap>(7, LOAD_FACTOR, true) {
        private static final long serialVersionUID = -3338259608597581144L;

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, Bitmap> eldest) {
            boolean shouldBeRemoved = size() > 7;
            if (shouldBeRemoved && eldest != null) {
                Bitmap bitmap = eldest.getValue();
                synchronized (ImageManager.this.mainWeakRefCacher) {
                    ImageManager.this.mainWeakRefCacher.put(eldest.getKey(), null);
                    if (bitmap != null) {
                    }
                }
            }
            return shouldBeRemoved;
        }
    };
    private Map<ImageLoadTask, String> runningTasksQueue = Collections.synchronizedMap(new HashMap());
    private boolean useReflection = false;
    private Map<String, ImageLoadTask> viewTaskMap = new LinkedHashMap<String, ImageLoadTask>(7, LOAD_FACTOR, true) {
        private static final long serialVersionUID = -2605344163855879383L;

        /* access modifiers changed from: protected */
        public boolean removeEldestEntry(Map.Entry<String, ImageLoadTask> entry) {
            return size() > 7;
        }
    };

    public interface ImageLoadCallback {
        void onPostImageLoading(String str, Bitmap bitmap);

        void onPreImageLoading();
    }

    public void downloadImageToCache(final String url, final boolean isLightWeight) {
        if (StringUtils.isNotEmpty(url) && getBitmap(url) == null) {
            getMainHandler().post(new Runnable() {
                public void run() {
                    ImageManager.this.saveOrRun(new ImageLoadTask(isLightWeight), url);
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, boolean):void
     arg types: [java.lang.String, android.view.View, int]
     candidates:
      com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, com.biznessapps.utils.ImageManager$TintContainer):void
      com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, boolean):void */
    public void download(String url, View view) {
        download(url, view, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, boolean):void
     arg types: [java.lang.String, android.view.View, int]
     candidates:
      com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, com.biznessapps.utils.ImageManager$TintContainer):void
      com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, boolean):void */
    public void downloadLigthweight(String url, View view) {
        download(url + AppConstants.ADD_ICON_WIDTH, view, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, boolean):void
     arg types: [java.lang.String, android.view.View, int]
     candidates:
      com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, com.biznessapps.utils.ImageManager$TintContainer):void
      com.biznessapps.utils.ImageManager.download(java.lang.String, android.view.View, boolean):void */
    public void downloadBgUrl(String primaryUrl, View view) {
        String url = primaryUrl;
        if (AppCore.getInstance().getDeviceWidth() > 0) {
            url = url + AppConstants.WIDTH_URL_PARAM + AppCore.getInstance().getDeviceWidth();
        }
        download(url, view, false);
    }

    private void download(String url, View view, boolean isLightWeight) {
        download(url, view, false, isLightWeight);
    }

    public void download(final String url, final View view, boolean useImageReflection, final boolean isLightWeight) {
        if (!StringUtils.isEmpty(url)) {
            this.useReflection = useImageReflection;
            Bitmap bitmap = getBitmap(url);
            if (bitmap == null) {
                checkViewsTopicality(view);
                getMainHandler().post(new Runnable() {
                    public void run() {
                        ImageManager.this.saveOrRun(new ImageLoadTask(view, null, isLightWeight), url);
                    }
                });
                return;
            }
            view.setBackgroundDrawable(new BitmapDrawable(bitmap));
        }
    }

    public void download(final String url, final View view, final TintContainer tint) {
        if (!StringUtils.isEmpty(url)) {
            Bitmap bitmap = getBitmap(url);
            if (bitmap == null) {
                checkViewsTopicality(view);
                getMainHandler().post(new Runnable() {
                    public void run() {
                        ImageManager.this.saveOrRun(new ImageLoadTask(view, tint, false), url);
                    }
                });
            } else if (tint != null) {
                view.setBackgroundDrawable(composeDrawable(new BitmapDrawable(bitmap), tint));
            } else {
                view.setBackgroundDrawable(new BitmapDrawable(bitmap));
            }
        } else if (tint != null) {
            view.setBackgroundDrawable(composeDrawable(tint));
        }
    }

    public void download(final String url, final ImageLoadCallback loadCallback) {
        if (url != null) {
            Bitmap bitmap = getBitmap(url);
            if (bitmap == null) {
                getMainHandler().post(new Runnable() {
                    public void run() {
                        ImageManager.this.saveOrRun(new ImageLoadTask(loadCallback), url);
                    }
                });
            } else {
                loadCallback.onPostImageLoading(url, bitmap);
            }
        }
    }

    public void clear() {
        this.mainWeakRefCacher.clear();
        this.lightHardRefCacher.clear();
        this.lightDrawableRefCacher.clear();
        this.loadTasksQueue.clear();
        this.runningTasksQueue.clear();
    }

    public void loadCompositeDrawable(Resources res, int colorForBg, int drawableResId, View view) {
        Drawable drawable = (Drawable) this.lightDrawableRefCacher.get("" + drawableResId);
        if (drawable != null) {
            view.setBackgroundDrawable(drawable);
            return;
        }
        final Resources resources = res;
        final int i = colorForBg;
        final int i2 = drawableResId;
        final View view2 = view;
        new AsyncTask<Void, Void, Drawable>() {
            /* access modifiers changed from: protected */
            public Drawable doInBackground(Void... params) {
                return CommonUtils.getCompositeDrawable(resources, i, i2);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Drawable result) {
                if (result != null) {
                    ImageManager.this.lightDrawableRefCacher.put("" + i2, result);
                    view2.setBackgroundDrawable(result);
                }
            }
        }.execute(new Void[0]);
    }

    public Bitmap getBitmap(String url) {
        Bitmap storedBitmap = this.lightHardRefCacher.get(url);
        if (storedBitmap == null) {
            return this.mainWeakRefCacher.get(url);
        }
        return storedBitmap;
    }

    public void saveBitmap(Bitmap bitmap, String url, boolean isLightWeight) {
        if (isLightWeight) {
            this.lightHardRefCacher.put(url, bitmap);
        } else {
            this.mainWeakRefCacher.put(url, bitmap);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c2, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c3, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e4, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00e5, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c2 A[ExcHandler: IllegalArgumentException (r2v1 'e' java.lang.IllegalArgumentException A[CUSTOM_DECLARE]), Splitter:B:4:0x000d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap downloadBitmap(java.lang.String r15) {
        /*
            r14 = this;
            android.graphics.Bitmap r10 = r14.getBitmap(r15)
            if (r10 == 0) goto L_0x000d
            boolean r11 = r10.isRecycled()
            if (r11 != 0) goto L_0x000d
        L_0x000c:
            return r10
        L_0x000d:
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r1.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r4.<init>(r15)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            org.apache.http.HttpResponse r7 = r1.execute(r4)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            org.apache.http.StatusLine r11 = r7.getStatusLine()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            int r9 = r11.getStatusCode()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r11 = 200(0xc8, float:2.8E-43)
            if (r9 == r11) goto L_0x004b
            java.lang.String r11 = "ImageDownloader"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r12.<init>()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.String r13 = "Error "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.StringBuilder r12 = r12.append(r9)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.String r13 = " while retrieving bitmap from "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.StringBuilder r12 = r12.append(r15)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            android.util.Log.w(r11, r12)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r10 = 0
            goto L_0x000c
        L_0x004b:
            org.apache.http.HttpEntity r3 = r7.getEntity()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            if (r3 == 0) goto L_0x00a2
            r5 = 0
            java.io.InputStream r5 = r3.getContent()     // Catch:{ all -> 0x007c }
            android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x007c }
            r6.<init>()     // Catch:{ all -> 0x007c }
            com.biznessapps.utils.ImageManager$FlushedInputStream r11 = new com.biznessapps.utils.ImageManager$FlushedInputStream     // Catch:{ all -> 0x007c }
            r11.<init>(r5)     // Catch:{ all -> 0x007c }
            r12 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r11, r12, r6)     // Catch:{ all -> 0x007c }
            boolean r11 = r14.useReflection     // Catch:{ all -> 0x007c }
            if (r11 == 0) goto L_0x007a
            android.graphics.Bitmap r8 = r14.createReflectedImages(r0)     // Catch:{ all -> 0x007c }
            r0.recycle()     // Catch:{ all -> 0x007c }
        L_0x0070:
            if (r5 == 0) goto L_0x0075
            r5.close()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
        L_0x0075:
            r3.consumeContent()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r10 = r8
            goto L_0x000c
        L_0x007a:
            r8 = r0
            goto L_0x0070
        L_0x007c:
            r11 = move-exception
            if (r5 == 0) goto L_0x0082
            r5.close()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
        L_0x0082:
            r3.consumeContent()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            throw r11     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
        L_0x0086:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r11 = "ImageManager"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r12.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r13 = "I/O error while retrieving bitmap from "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.StringBuilder r12 = r12.append(r15)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r12 = r12.toString()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            android.util.Log.w(r11, r12, r2)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
        L_0x00a2:
            r10 = 0
            goto L_0x000c
        L_0x00a5:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r11 = "ImageManager"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r12.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r13 = "Incorrect URL: "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.StringBuilder r12 = r12.append(r15)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r12 = r12.toString()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            android.util.Log.w(r11, r12)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            goto L_0x00a2
        L_0x00c2:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00a2
        L_0x00c7:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r11 = "ImageManager"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r12.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r13 = "Error while retrieving bitmap from "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.StringBuilder r12 = r12.append(r15)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r12 = r12.toString()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            android.util.Log.w(r11, r12, r2)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            goto L_0x00a2
        L_0x00e4:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.utils.ImageManager.downloadBitmap(java.lang.String):android.graphics.Bitmap");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    private Bitmap createReflectedImages(Bitmap originalImage) {
        if (originalImage == null) {
            return null;
        }
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, (int) (((float) height) * 0.5f), width, (int) (((float) height) - (((float) height) * 0.5f)), matrix, false);
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, (int) (((float) height) + (((float) height) * 0.5f)), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapWithReflection);
        canvas.drawBitmap(originalImage, 0.0f, 0.0f, (Paint) null);
        new Paint().setColor(17170445);
        canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) originalImage.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.CLAMP));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
        originalImage.recycle();
        reflectionImage.recycle();
        return bitmapWithReflection;
    }

    /* access modifiers changed from: private */
    public void saveOrRun(ImageLoadTask imageLoadTask, String url) {
        if (imageLoadTask != null && StringUtils.isNotEmpty(url)) {
            if (this.runningTasksQueue.size() < 6) {
                this.runningTasksQueue.put(imageLoadTask, url);
                imageLoadTask.execute(url);
                storeImageLoadTask(imageLoadTask);
                return;
            }
            this.loadTasksQueue.put(imageLoadTask, url);
        }
    }

    /* access modifiers changed from: private */
    public void launchNewTaskIfNeeded(ImageLoadTask taskToRemove, String url) {
        synchronized (this.runningTasksQueue) {
            this.runningTasksQueue.remove(taskToRemove);
            Set<ImageLoadTask> storedTasks = this.loadTasksQueue.keySet();
            synchronized (this.loadTasksQueue) {
                this.loadTasksQueue.remove(taskToRemove);
                Iterator i$ = storedTasks.iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    ImageLoadTask newTask = i$.next();
                    if (newTask.getStatus() != AsyncTask.Status.RUNNING && !newTask.equals(taskToRemove)) {
                        newTask.execute(this.loadTasksQueue.get(newTask));
                        storeImageLoadTask(newTask);
                        break;
                    }
                }
            }
        }
    }

    private void storeImageLoadTask(ImageLoadTask task) {
        View view = task.getView();
        if (view != null) {
            this.viewTaskMap.put(view.toString(), task);
        }
    }

    private void checkViewsTopicality(View view) {
        if (view != null) {
            ImageLoadTask task = this.viewTaskMap.get(view.toString());
            if (task != null) {
                task.setHasLosedView(true);
            }
            this.viewTaskMap.remove(view.toString());
        }
    }

    private class ImageLoadTask extends AsyncTask<String, Void, Bitmap> {
        private boolean haslosedView;
        private boolean isLightWeightImage;
        private ImageLoadCallback loadCallback;
        private TintContainer tint;
        private String url;
        private WeakReference<View> viewReference;

        public ImageLoadTask(View view, TintContainer tint2, boolean isLightWeightImage2) {
            this.viewReference = new WeakReference<>(view);
            this.tint = tint2;
            this.isLightWeightImage = isLightWeightImage2;
        }

        public ImageLoadTask(ImageLoadCallback loadCallback2) {
            this.loadCallback = loadCallback2;
        }

        public ImageLoadTask(boolean isLightWeightImage2) {
            this.isLightWeightImage = isLightWeightImage2;
        }

        public void setHasLosedView(boolean haslosedView2) {
            this.haslosedView = haslosedView2;
        }

        public View getView() {
            if (this.viewReference != null) {
                return this.viewReference.get();
            }
            return null;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.loadCallback != null) {
                this.loadCallback.onPreImageLoading();
            }
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... params) {
            this.url = params[0];
            return ImageManager.this.downloadBitmap(this.url);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            View view;
            if (bitmap != null && !bitmap.isRecycled()) {
                ImageManager.this.saveBitmap(bitmap, this.url, this.isLightWeightImage);
                if (!(this.haslosedView || this.viewReference == null || (view = this.viewReference.get()) == null)) {
                    if (this.tint != null) {
                        view.setBackgroundDrawable(ImageManager.composeDrawable(new BitmapDrawable(bitmap), this.tint));
                    } else {
                        view.setBackgroundDrawable(new BitmapDrawable(bitmap));
                    }
                }
            }
            if (this.loadCallback != null) {
                this.loadCallback.onPostImageLoading(this.url, bitmap);
            }
            ImageManager.this.launchNewTaskIfNeeded(this, this.url);
        }
    }

    /* access modifiers changed from: private */
    public static Drawable composeDrawable(Drawable level1, TintContainer tint) {
        Drawable[] layers = {level1, new ColorDrawable(ViewUtils.getColor(tint.getTintColor(), 0))};
        layers[1].setAlpha((int) tint.getTintOpacity());
        return new LayerDrawable(layers).getCurrent();
    }

    private static Drawable composeDrawable(TintContainer tint) {
        Drawable[] layers = {new ColorDrawable(ViewUtils.getColor(tint.getTintColor(), 0))};
        layers[0].setAlpha((int) tint.getTintOpacity());
        return new LayerDrawable(layers).getCurrent();
    }

    public static class TintContainer {
        private String tintColor;
        private float tintOpacity;

        public String getTintColor() {
            return this.tintColor;
        }

        public void setTintColor(String tintColor2) {
            this.tintColor = tintColor2;
        }

        public float getTintOpacity() {
            return this.tintOpacity;
        }

        public void setTintOpacity(float tintOpacity2) {
            this.tintOpacity = tintOpacity2;
        }
    }

    private static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    public static class CustomButtonData {
        private Drawable customButtonDrawable;
        private String customButtonUrl;
        private int drawableResourceId;
        private boolean needInvertIcon;

        public CustomButtonData() {
        }

        public CustomButtonData(int textColorId, int drawableResourceId2) {
            this.drawableResourceId = drawableResourceId2;
        }

        public CustomButtonData(int textColorId, int drawableResourceId2, boolean needInvertIcon2) {
            this(textColorId, drawableResourceId2);
            this.needInvertIcon = needInvertIcon2;
        }

        public String getCustomButtonUrl() {
            return this.customButtonUrl;
        }

        public void setCustomButtonUrl(String customButtonUrl2) {
            this.customButtonUrl = customButtonUrl2;
        }

        public Drawable getCustomButtonDrawable() {
            return this.customButtonDrawable;
        }

        public void setCustomButtonDrawable(Drawable customButtonDrawable2) {
            this.customButtonDrawable = customButtonDrawable2;
        }

        public int getDrawableResourceId() {
            return this.drawableResourceId;
        }

        public void setDrawableResourceId(int drawableResourceId2) {
            this.drawableResourceId = drawableResourceId2;
        }

        public boolean isNeedInvertIcon() {
            return this.needInvertIcon;
        }

        public void setNeedInvertIcon(boolean needInvertIcon2) {
            this.needInvertIcon = needInvertIcon2;
        }
    }

    private Handler getMainHandler() {
        if (this.mainHandler == null) {
            this.mainHandler = new Handler(Looper.getMainLooper());
        }
        return this.mainHandler;
    }
}
