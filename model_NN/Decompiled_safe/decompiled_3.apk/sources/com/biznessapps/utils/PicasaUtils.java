package com.biznessapps.utils;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.model.flickr.GalleryAlbum;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public final class PicasaUtils {
    private static final String CONTENT_TAG = "content";
    private static final String ENTRY_TAG = "entry";
    private static final String PHOTO_ID_TAG = "gphoto:id";
    private static final String SRC_TAG = "src";
    private static final String SUMMARY_TAG = "summary";
    private static final String TITLE_TAG = "title";

    public static String getPicasaData(String albumUrl) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(((HttpURLConnection) new URL(albumUrl).openConnection()).getInputStream(), Charset.forName(AppConstants.UTF_8_CHARSET)));
            StringBuilder builder = new StringBuilder();
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    return builder.toString();
                }
                builder.append(line + AppConstants.NEW_LINE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<GalleryAlbum> parseGalleryList(String resp) {
        List<GalleryAlbum> result = new ArrayList<>();
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(resp));
            NodeList entries = db.parse(is).getElementsByTagName(ENTRY_TAG);
            for (int i = 0; i < entries.getLength(); i++) {
                Element element = (Element) entries.item(i);
                GalleryAlbum album = new GalleryAlbum();
                album.setId(convertToStringFromElement((Element) element.getElementsByTagName(PHOTO_ID_TAG).item(0)));
                album.setTitle(convertToStringFromElement((Element) element.getElementsByTagName("title").item(0)));
                album.setDescription(convertToStringFromElement((Element) element.getElementsByTagName(SUMMARY_TAG).item(0)));
                result.add(album);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static List<String> parsePhotosList(String resp) {
        List<String> result = new ArrayList<>();
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(resp));
            NodeList entries = db.parse(is).getElementsByTagName(ENTRY_TAG);
            for (int i = 0; i < entries.getLength(); i++) {
                result.add(((Element) ((Element) entries.item(i)).getElementsByTagName(CONTENT_TAG).item(0)).getAttributeNode("src").getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String convertToStringFromElement(Element element) {
        Node child = element.getFirstChild();
        if (child != null) {
            return ((CharacterData) child).getData();
        }
        return null;
    }
}
