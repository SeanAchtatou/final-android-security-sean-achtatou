package com.biznessapps.utils;

import android.content.Context;
import com.biznessapps.layout.R;
import com.facebook.AppEventsConstants;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {
    public static final String DATE_PICKER_TITLE_FORMAT = "EEE MMM d yyyy";
    public static final String EVENT_DATE_FORMAT = "MM dd yyyy h:m a";
    public static final long MILLI_SEC_IN_DAY = 86400000;
    public static final int MILLI_SEC_IN_HOUR = 3600000;
    private static final long MSEC_IN_ONE_DAY = 86400000;
    private static final long MSEC_IN_ONE_HOUR = 3600000;
    private static final long MSEC_IN_ONE_MIN = 60000;
    private static final long MSEC_IN_ONE_SEC = 1000;
    private static final long MSEC_IN_ONE_WEEK = 604800000;
    public static final long SECONDS_IN_HOUR = 3600000;

    public static Date getDateBySec(String dateInSec) {
        if (dateInSec == null || dateInSec.equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            return null;
        }
        try {
            return new Date(Long.parseLong(dateInSec) * MSEC_IN_ONE_SEC);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date getTomorrowDate(Date currentDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentDate.getTime() + 86400000);
        return calendar.getTime();
    }

    public static Date getYesterdayDate(Date currentDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(currentDate.getTime() - 86400000);
        return calendar.getTime();
    }

    public static boolean isTheSameDate(Date firstDate, Date secondDate) {
        Calendar date1 = Calendar.getInstance();
        date1.setTime(firstDate);
        Calendar date2 = Calendar.getInstance();
        date2.setTime(secondDate);
        if (date1.get(1) == date2.get(1) && date1.get(2) == date2.get(2) && date1.get(5) == date2.get(5)) {
            return true;
        }
        return false;
    }

    public static boolean isBiggerThan(Date firstDate, Date secondDate) {
        Calendar date1 = Calendar.getInstance();
        date1.setTime(firstDate);
        Calendar date2 = Calendar.getInstance();
        date2.setTime(secondDate);
        if (date1.get(1) > date2.get(1)) {
            return true;
        }
        if (date1.get(1) != date2.get(1)) {
            return false;
        }
        if (date1.get(2) > date2.get(2)) {
            return true;
        }
        if (date1.get(2) != date2.get(2) || date1.get(5) <= date2.get(5)) {
            return false;
        }
        return true;
    }

    public static boolean isBiggerOrEqual(Date firstDate, Date secondDate) {
        return isBiggerThan(firstDate, secondDate) || isTheSameDate(firstDate, secondDate);
    }

    public static String getStringInterval(Context context, String primaryDate) {
        try {
            Calendar calTarget = new GregorianCalendar();
            calTarget.setTime(new Date(primaryDate));
            Calendar calCurrent = new GregorianCalendar();
            calCurrent.setTime(new Date());
            long interval = calCurrent.getTimeInMillis() - calTarget.getTimeInMillis();
            int value = calculateYears(calTarget, calCurrent);
            if (value > 0) {
                return context.getString(R.string.years_template, Integer.valueOf(value));
            }
            int value2 = getMonth(calTarget, calCurrent);
            if (value2 > 0) {
                return context.getString(R.string.months_template, Integer.valueOf(value2));
            } else if (interval >= MSEC_IN_ONE_WEEK) {
                return context.getString(R.string.weeks_template, Long.valueOf(interval / MSEC_IN_ONE_WEEK));
            } else if (interval >= 86400000) {
                return context.getString(R.string.days_template, Long.valueOf(interval / 86400000));
            } else if (interval >= 3600000) {
                return context.getString(R.string.hours_template, Long.valueOf(interval / 3600000));
            } else if (interval >= MSEC_IN_ONE_MIN) {
                return context.getString(R.string.mins_template, Long.valueOf(interval / MSEC_IN_ONE_MIN));
            } else if (interval >= MSEC_IN_ONE_SEC) {
                return context.getString(R.string.secs_template, Long.valueOf(interval / MSEC_IN_ONE_SEC));
            } else if (interval >= MSEC_IN_ONE_SEC) {
                return "";
            } else {
                return context.getString(R.string.secs_template, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getStringInterval(Context context, Date primaryDate) {
        try {
            Calendar calTarget = new GregorianCalendar();
            calTarget.setTime(primaryDate);
            Calendar calCurrent = new GregorianCalendar();
            calCurrent.setTime(new Date());
            long interval = calCurrent.getTimeInMillis() - calTarget.getTimeInMillis();
            int value = calculateYears(calTarget, calCurrent);
            if (value > 0) {
                return context.getString(R.string.years_template, Integer.valueOf(value));
            }
            int value2 = getMonth(calTarget, calCurrent);
            if (value2 > 0) {
                return context.getString(R.string.months_template, Integer.valueOf(value2));
            } else if (interval >= MSEC_IN_ONE_WEEK) {
                return context.getString(R.string.weeks_template, Long.valueOf(interval / MSEC_IN_ONE_WEEK));
            } else if (interval >= 86400000) {
                return context.getString(R.string.days_template, Long.valueOf(interval / 86400000));
            } else if (interval >= 3600000) {
                return context.getString(R.string.hours_template, Long.valueOf(interval / 3600000));
            } else if (interval >= MSEC_IN_ONE_MIN) {
                return context.getString(R.string.mins_template, Long.valueOf(interval / MSEC_IN_ONE_MIN));
            } else if (interval >= MSEC_IN_ONE_SEC) {
                return context.getString(R.string.secs_template, Long.valueOf(interval / MSEC_IN_ONE_SEC));
            } else if (interval >= MSEC_IN_ONE_SEC) {
                return "";
            } else {
                return context.getString(R.string.secs_template, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static int calculateYears(Calendar calTarget, Calendar calCurrent) {
        int result = 0;
        Calendar tmp = (Calendar) calTarget.clone();
        tmp.add(1, 1);
        while (tmp.getTimeInMillis() < calCurrent.getTimeInMillis()) {
            tmp.add(1, 1);
            result++;
        }
        return result;
    }

    private static int getMonth(Calendar calTarget, Calendar calCurrent) {
        int result = 0;
        Calendar tmp = (Calendar) calTarget.clone();
        tmp.add(2, 1);
        while (tmp.getTimeInMillis() < calCurrent.getTimeInMillis()) {
            tmp.add(2, 1);
            result++;
        }
        return result;
    }
}
