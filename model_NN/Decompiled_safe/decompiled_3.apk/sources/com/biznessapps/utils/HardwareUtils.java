package com.biznessapps.utils;

import android.os.Environment;
import java.io.File;
import java.util.UUID;

public final class HardwareUtils {
    private HardwareUtils() {
    }

    public static boolean hasExternalStorage() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    public static String getExternalPath() {
        File path;
        if (!hasExternalStorage() || (path = Environment.getExternalStorageDirectory()) == null) {
            return null;
        }
        return path.toString();
    }

    public static String getDeviceToken(String telephonyId, String googleDeviceId) {
        int telephoneHashCode = 0;
        int googleHashCode = 0;
        if (StringUtils.isNotEmpty(telephonyId)) {
            telephoneHashCode = telephonyId.hashCode();
        }
        if (StringUtils.isNotEmpty(googleDeviceId)) {
            googleHashCode = googleDeviceId.hashCode();
        }
        return new UUID((long) telephoneHashCode, (long) googleHashCode).toString();
    }
}
