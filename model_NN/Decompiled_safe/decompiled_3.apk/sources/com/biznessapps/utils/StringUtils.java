package com.biznessapps.utils;

import android.content.Context;
import java.net.URLDecoder;
import java.util.regex.Pattern;

public class StringUtils {
    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_PATTERN_TEXT);
    private static final String EMAIL_PATTERN_TEXT = "(^[\\w\\.=-]+)@([\\w]+[\\.-][\\w]+)$";
    private static final String MUSIC_SONG_FORMAT = "%d:%02d";

    public static boolean isNotEmpty(String s) {
        if (s == null || s.trim().length() <= 0 || s.equalsIgnoreCase("null")) {
            return false;
        }
        return true;
    }

    public static boolean isEmpty(String s) {
        return !isNotEmpty(s);
    }

    public static boolean checkTextFieldsOnEmpty(String... checkedValues) {
        if (checkedValues.length == 0) {
            return false;
        }
        for (String checkedValue : checkedValues) {
            if (!isNotEmpty(checkedValue)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isCorrectEmail(String email) {
        if (isNotEmpty(email) || EMAIL_PATTERN.matcher(email).find()) {
            return false;
        }
        return true;
    }

    public static boolean isNotEmpty(String[] s) {
        if (s == null || s.length <= 0) {
            return false;
        }
        return true;
    }

    public static String decode(String primaryStr) {
        String result;
        String s = null;
        if (primaryStr == null) {
            return "";
        }
        try {
            String s2 = new String(primaryStr);
            try {
                s = s2.replaceAll("!", "%21").replaceAll("#", "%23");
                result = URLDecoder.decode(s);
            } catch (IllegalArgumentException e) {
                s = s2;
                try {
                    result = URLDecoder.decode(URLDecoder.decode(s.replaceAll("%", "%25")));
                } catch (IllegalArgumentException e2) {
                    result = primaryStr;
                }
                return result;
            }
        } catch (IllegalArgumentException e3) {
        }
        return result;
    }

    public static String makeTimeString(Context context, long secs) {
        long minutes = secs / ((long) 60);
        long secs2 = secs % ((long) 60);
        if (minutes > 10) {
            return "";
        }
        return String.format(MUSIC_SONG_FORMAT, Long.valueOf(minutes), Long.valueOf(secs2));
    }
}
