package com.biznessapps.components;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.activities.SociableActivityInterface;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.facebook.AsyncRequestListener;
import com.biznessapps.facebook.SessionEvents;
import com.biznessapps.layout.R;
import com.biznessapps.utils.ViewUtils;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.json.JSONObject;

public class SocialNetworkAccessor {
    private static final String USER_LIKES = "user_likes";
    private static final String USER_STATUS = "user_status";
    private List<SocialNetworkListener> authorizationListeners = new ArrayList();
    /* access modifiers changed from: private */
    public ImageButton chooseAccountsButton;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public LoginButton facebookAuthButton;

    public SocialNetworkAccessor(final SociableActivityInterface activity, ViewGroup root) {
        this.context = ((Activity) activity).getApplicationContext();
        final ViewGroup accountsContentView = (ViewGroup) root.findViewById(R.id.choose_accounts_content);
        this.chooseAccountsButton = (ImageButton) root.findViewById(R.id.choose_login_account);
        this.facebookAuthButton = (LoginButton) root.findViewById(R.id.facebook_login_button);
        this.facebookAuthButton.setApplicationId(AppCore.getInstance().getAppSettings().getFacebookAppId());
        this.facebookAuthButton.setReadPermissions(Arrays.asList(USER_LIKES, USER_STATUS));
        this.facebookAuthButton.setSessionStatusCallback(new Session.StatusCallback() {
            public void call(Session session, SessionState state, Exception exception) {
                if (session.isOpened()) {
                    Request.executeMeRequestAsync(session, new Request.GraphUserCallback() {
                        public void onCompleted(GraphUser user, Response response) {
                            if (user != null) {
                                String uid = user.getId();
                                SocialNetworkAccessor.this.cacher().setFacebookUserName(user.getName());
                                SocialNetworkAccessor.this.cacher().setFacebookUid(uid);
                                SocialNetworkAccessor.this.cacher().setLastLoginType(1);
                                SocialNetworkAccessor.this.notifyAboutAuthSuccess();
                                return;
                            }
                            ViewUtils.showShortToast(SocialNetworkAccessor.this.context, R.string.facebook_login_failure);
                        }
                    });
                }
            }
        });
        this.chooseAccountsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SocialNetworkAccessor.this.openLoginAccountsDialog(accountsContentView);
            }
        });
        ((Button) root.findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SocialNetworkAccessor.this.closeLoginAccountsDialog(accountsContentView);
            }
        });
        ((Button) root.findViewById(R.id.add_via_twitter_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SocialNetworkAccessor.this.cacher().hasTwitterData()) {
                    SocialNetworkAccessor.this.cacher().setLastLoginType(2);
                    SocialNetworkAccessor.this.notifyAboutAuthSuccess();
                } else {
                    SocialNetworkAccessor.this.openTwitterLoginView((Activity) activity);
                }
                SocialNetworkAccessor.this.closeLoginAccountsDialog(accountsContentView);
            }
        });
        ((Button) root.findViewById(R.id.add_via_facebook_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (SocialNetworkAccessor.this.cacher().hasFacebookData()) {
                    SocialNetworkAccessor.this.cacher().setLastLoginType(1);
                    SocialNetworkAccessor.this.notifyAboutAuthSuccess();
                } else {
                    SocialNetworkAccessor.this.facebookAuthButton.performClick();
                }
                SocialNetworkAccessor.this.closeLoginAccountsDialog(accountsContentView);
            }
        });
    }

    public void setAddCommentListener(View view) {
        if (view != null) {
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    SocialNetworkAccessor.this.chooseAccountsButton.performClick();
                }
            });
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (2 == resultCode) {
            cacher().setLastLoginType(2);
            notifyAboutAuthSuccess();
        } else if (3 == resultCode) {
            ViewUtils.showShortToast(this.context, R.string.twitter_login_failure);
        }
    }

    public void addAuthorizationListener(SocialNetworkListener listenerToAdd) {
        this.authorizationListeners.add(listenerToAdd);
    }

    /* access modifiers changed from: private */
    public void openTwitterLoginView(Activity activity) {
        Intent intent = new Intent(this.context, SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.TWITTER_LOGIN_FRAGMENT);
        activity.startActivityForResult(intent, 2);
    }

    private class CustomAuthListener implements SessionEvents.AuthListener {
        private CustomAuthListener() {
        }

        public void onAuthSucceed() {
            new AsyncFacebookRunner(SocialNetworkAccessor.access$900(SocialNetworkAccessor.this)).request(AppConstants.GRAPH_PATH_ME, new AsyncRequestListener() {
                public void onComplete(JSONObject obj) {
                    if (obj == null) {
                        ViewUtils.showShortToast(SocialNetworkAccessor.access$800(SocialNetworkAccessor.this), R.string.facebook_login_failure);
                        return;
                    }
                    String uid = obj.optString("id");
                    SocialNetworkAccessor.access$400(SocialNetworkAccessor.this).setFacebookUserName(obj.optString(ServerConstants.POST_NAME_PARAM));
                    SocialNetworkAccessor.access$400(SocialNetworkAccessor.this).setFacebookUid(uid);
                    SocialNetworkAccessor.access$400(SocialNetworkAccessor.this).setLastLoginType(1);
                    SocialNetworkAccessor.access$500(SocialNetworkAccessor.this);
                }
            });
        }

        public void onAuthFail(String error) {
        }
    }

    /* access modifiers changed from: private */
    public void openLoginAccountsDialog(ViewGroup rootContainer) {
        if (rootContainer.getVisibility() == 8) {
            rootContainer.setVisibility(0);
            rootContainer.startAnimation(AnimationUtils.loadAnimation(this.context, R.anim.show_tell_friends_dialog));
        }
    }

    /* access modifiers changed from: private */
    public void closeLoginAccountsDialog(ViewGroup rootContainer) {
        Animation animation = AnimationUtils.loadAnimation(this.context, R.anim.hide_tell_friends_dialog);
        rootContainer.startAnimation(animation);
        animation.setAnimationListener(new ViewUtils.HideAnimationListener(rootContainer));
    }

    /* access modifiers changed from: private */
    public void notifyAboutAuthSuccess() {
        for (SocialNetworkListener listener : this.authorizationListeners) {
            listener.onAuthSucceed();
        }
    }

    /* access modifiers changed from: private */
    public CachingManager cacher() {
        return AppCore.getInstance().getCachingManager();
    }

    private class CustomLogoutListener implements SessionEvents.LogoutListener {
        private CustomLogoutListener() {
        }

        public void onLogoutBegin() {
        }

        public void onLogoutFinish() {
        }
    }

    public static abstract class SocialNetworkListener {
        private SocialNetworkAccessor socialAccessor;

        public SocialNetworkListener(SocialNetworkAccessor socialAccessor2) {
            this.socialAccessor = socialAccessor2;
        }

        public void onAuthSucceed() {
        }

        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
            Session sess = Session.getActiveSession();
            if (sess != null) {
                sess.onActivityResult(activity, requestCode, resultCode, data);
            }
            if (this.socialAccessor != null) {
                this.socialAccessor.onActivityResult(requestCode, resultCode, data);
            }
        }
    }
}
