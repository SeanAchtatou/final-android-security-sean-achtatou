package com.biznessapps.components;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.biznessapps.api.AppCore;
import java.util.ArrayList;
import java.util.List;

public class LocationFinder {
    private static final int TWO_MINUTES_DELAY = 120000;
    private Location currentBestLocation;
    private LocationListener gpsLocationListener;
    private List<LocationListener> listeners = new ArrayList();
    private LocationManager locationManager;
    private LocationListener networkLocationListener;

    public LocationFinder(Context context) {
        this.locationManager = (LocationManager) context.getSystemService("location");
        getNetworkLocationListener();
        getGpsLocationListener();
    }

    public void startSearching() {
        this.locationManager.requestLocationUpdates("network", 120000, 0.0f, this.networkLocationListener);
        this.locationManager.requestLocationUpdates("gps", 120000, 0.0f, this.gpsLocationListener);
    }

    public void stopSearching() {
        boolean z;
        boolean z2 = true;
        if (this.locationManager != null) {
            if (this.networkLocationListener != null) {
                z = true;
            } else {
                z = false;
            }
            if (this.gpsLocationListener == null) {
                z2 = false;
            }
            if (z && z2) {
                this.locationManager.removeUpdates(this.networkLocationListener);
                this.locationManager.removeUpdates(this.gpsLocationListener);
            }
        }
    }

    public void destroy() {
        this.listeners = null;
        this.networkLocationListener = null;
        this.gpsLocationListener = null;
    }

    public void addLocationListener(LocationListener listenerToAdd) {
        if (listenerToAdd != null) {
            this.listeners.add(listenerToAdd);
        }
    }

    public void removeLocationListener(LocationListener listenerToRemove) {
        this.listeners.remove(listenerToRemove);
    }

    public Location getCurrentLocation() {
        if (this.currentBestLocation != null) {
            return this.currentBestLocation;
        }
        Location lastKnownGpsLoc = this.locationManager.getLastKnownLocation("gps");
        Location lastKnownNetworkLoc = this.locationManager.getLastKnownLocation("network");
        if (lastKnownNetworkLoc == null || !isBetterLocation(lastKnownNetworkLoc, lastKnownGpsLoc)) {
            return lastKnownGpsLoc;
        }
        return lastKnownNetworkLoc;
    }

    private LocationListener getGpsLocationListener() {
        if (this.gpsLocationListener == null) {
            this.gpsLocationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    LocationFinder.this.processNewLocation(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
        }
        return this.gpsLocationListener;
    }

    private LocationListener getNetworkLocationListener() {
        if (this.networkLocationListener == null) {
            this.networkLocationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    LocationFinder.this.processNewLocation(location);
                    AppCore.getInstance().getLocationFinder().stopSearching();
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
        }
        return this.networkLocationListener;
    }

    /* access modifiers changed from: private */
    public synchronized void processNewLocation(Location location) {
        if (isBetterLocation(location, this.currentBestLocation)) {
            this.currentBestLocation = location;
            for (LocationListener listener : this.listeners) {
                listener.onLocationChanged(this.currentBestLocation);
            }
        }
    }

    private boolean isBetterLocation(Location location, Location currentBestLocation2) {
        if (currentBestLocation2 == null) {
            return true;
        }
        long timeDelta = location.getTime() - currentBestLocation2.getTime();
        boolean isSignificantlyNewer = timeDelta > 120000;
        boolean isSignificantlyOlder = timeDelta < -120000;
        boolean isNewer = timeDelta > 0;
        if (isSignificantlyNewer) {
            return true;
        }
        if (isSignificantlyOlder) {
            return false;
        }
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation2.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation2.getProvider());
        if (isMoreAccurate) {
            return true;
        }
        if (isNewer && !isLessAccurate) {
            return true;
        }
        if (!isNewer || isSignificantlyLessAccurate || !isFromSameProvider) {
            return false;
        }
        return true;
    }

    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
