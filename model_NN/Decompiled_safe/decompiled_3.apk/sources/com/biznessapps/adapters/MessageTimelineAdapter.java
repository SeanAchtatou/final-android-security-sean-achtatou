package com.biznessapps.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.fragments.lists.MessageListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.MessageItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.DateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class MessageTimelineAdapter extends AbstractAdapter<MessageItem> {
    /* access modifiers changed from: private */
    public MessageListFragment.MessageItemListener leftItemListener;
    /* access modifiers changed from: private */
    public MessageListFragment.MessageItemListener rightItemListener;

    public void setLeftItemListener(MessageListFragment.MessageItemListener leftItemListener2) {
        this.leftItemListener = leftItemListener2;
    }

    public void setRightItemListener(MessageListFragment.MessageItemListener rightItemListener2) {
        this.rightItemListener = rightItemListener2;
    }

    public MessageTimelineAdapter(Context context, List<MessageItem> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View rowView, ViewGroup parent) {
        if (rowView == null) {
            rowView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            rowView.setTag(new ListItemHolder.MessageItem());
        } else {
            ListItemHolder.MessageItem messageItem = (ListItemHolder.MessageItem) rowView.getTag();
        }
        ImageView imageViewLeft = (ImageView) rowView.findViewById(R.id.message_left_image);
        ImageView imageViewRight = (ImageView) rowView.findViewById(R.id.message_right_image);
        ImageView imageViewLeftFunctional = (ImageView) rowView.findViewById(R.id.message_left_image_functional);
        ImageView imageViewRightFunctional = (ImageView) rowView.findViewById(R.id.message_right_image_functional);
        ImageView horizontalDivider = (ImageView) rowView.findViewById(R.id.message_divider_horizontal);
        ImageView verticalDivider = (ImageView) rowView.findViewById(R.id.message_divider_vertical);
        TextView textViewLeft = (TextView) rowView.findViewById(R.id.message_left_text);
        TextView textViewRight = (TextView) rowView.findViewById(R.id.message_right_text);
        TextView textDateView = (TextView) rowView.findViewById(R.id.message_date_text);
        TextView messagesNumberView = (TextView) rowView.findViewById(R.id.message_numbers_text);
        messagesNumberView.setText(this.items.size() + " " + getContext().getString(R.string.messages));
        final MessageItem item = (MessageItem) this.items.get(position);
        if (item != null) {
            boolean isOrdinaryMessage = item.getType() == 0;
            int rightText = R.string.message_text_type;
            int rightIconId = R.drawable.message_text_icon;
            if (item.getType() == 1) {
                rightText = R.string.message_web_type;
                rightIconId = R.drawable.message_web_icon;
            } else if (item.getType() == 2) {
                rightText = R.string.message_tab_content_type;
                rightIconId = R.drawable.message_tab_content_icon;
            } else if (item.getType() == 3) {
                rightText = R.string.message_template_type;
                rightIconId = R.drawable.message_template_icon;
            }
            textViewRight.setText(rightText);
            imageViewRight.setImageResource(rightIconId);
            imageViewRight.setVisibility(isOrdinaryMessage ? 8 : 0);
            textViewRight.setVisibility(isOrdinaryMessage ? 8 : 0);
            horizontalDivider.setVisibility(isOrdinaryMessage ? 8 : 0);
            messagesNumberView.setVisibility(position != 0 ? 8 : 0);
            verticalDivider.setVisibility(position == 0 ? 8 : 0);
            if (!isOrdinaryMessage) {
                Animation hyperspaceJumpAnimation2 = AnimationUtils.loadAnimation(getContext(), R.anim.message_right_item_anim);
                imageViewRight.startAnimation(hyperspaceJumpAnimation2);
                textViewRight.startAnimation(hyperspaceJumpAnimation2);
                Animation hyperspaceJumpAnimation1 = AnimationUtils.loadAnimation(getContext(), R.anim.message_left_item_anim);
                imageViewLeft.startAnimation(hyperspaceJumpAnimation1);
                textViewLeft.startAnimation(hyperspaceJumpAnimation1);
            } else {
                Animation hyperspaceJumpAnimation3 = AnimationUtils.loadAnimation(getContext(), R.anim.message_center_anim);
                imageViewRight.startAnimation(hyperspaceJumpAnimation3);
                textViewRight.startAnimation(hyperspaceJumpAnimation3);
                imageViewLeft.startAnimation(hyperspaceJumpAnimation3);
                textViewLeft.startAnimation(hyperspaceJumpAnimation3);
            }
            final int i = position;
            imageViewLeft.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MessageTimelineAdapter.this.leftItemListener != null) {
                        MessageTimelineAdapter.this.leftItemListener.onItemSelected(item, i);
                    }
                }
            });
            final int i2 = position;
            imageViewLeftFunctional.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MessageTimelineAdapter.this.leftItemListener != null) {
                        MessageTimelineAdapter.this.leftItemListener.onItemSelected(item, i2);
                    }
                }
            });
            final int i3 = position;
            imageViewRightFunctional.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MessageTimelineAdapter.this.rightItemListener != null) {
                        MessageTimelineAdapter.this.rightItemListener.onItemSelected(item, i3);
                    }
                }
            });
            if (item.getDate() != null) {
                try {
                    textDateView.setText(DateUtils.getStringInterval(getContext(), new SimpleDateFormat("yyyy-MM-dd").parse(item.getDate())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (!this.wasOvercolored) {
                CommonUtils.overrideImageColor(AppCore.getInstance().getUiSettings().getButtonBgColor(), imageViewLeft.getBackground());
                this.wasOvercolored = true;
            }
            setTextColorToView(this.settings.getFeatureTextColor(), textViewLeft, textViewRight, textDateView);
        }
        return rowView;
    }
}
