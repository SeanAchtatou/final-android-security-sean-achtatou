package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import java.util.List;

public class TwitterTagsAdapter extends AbstractAdapter<CommonListEntity> {
    public TwitterTagsAdapter(Context context, List<CommonListEntity> items) {
        super(context, items, R.layout.twitter_tags_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        final CommonListEntity item = (CommonListEntity) this.items.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
            CheckBox cb = (CheckBox) convertView.findViewById(R.id.simple_check_box);
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
                    item.setSelected(arg1);
                }
            });
            holder.setCheckbox(cb);
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
            if (item != null) {
                item.setSelected(holder.getCheckbox().isSelected());
            }
        }
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getTitle()));
            holder.getCheckbox().setSelected(item.isSelected());
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
            }
        }
        return convertView;
    }
}
