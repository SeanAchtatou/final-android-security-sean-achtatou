package com.biznessapps.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class ImageAdapter extends AbstractAdapter<GalleryData.Item> {
    public ImageAdapter(Context context, List<GalleryData.Item> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.GalleryItem holder;
        String url;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.GalleryItem();
            holder.setImage((ImageView) convertView.findViewById(R.id.image_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.GalleryItem) convertView.getTag();
        }
        GalleryData.Item item = (GalleryData.Item) this.items.get(position);
        if (item != null) {
            if (StringUtils.isEmpty(item.getFullUrl())) {
                url = String.format(ServerConstants.GALLERY_THUMBNAILS, item.getId());
            } else {
                url = item.getFullUrl() + AppConstants.WIDTH_URL_PARAM + 100;
            }
            this.imageFetcher.loadImage(url, holder.getImage());
        }
        return convertView;
    }
}
