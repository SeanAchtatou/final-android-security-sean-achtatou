package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.layout.R;
import com.biznessapps.model.TabButton;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class MoreTabAdapter extends AbstractAdapter<TabButton> {
    public MoreTabAdapter(Context context, List<TabButton> items) {
        super(context, items, R.layout.common_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
            holder.setImageView((ImageView) convertView.findViewById(R.id.row_icon));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
        }
        TabButton item = (TabButton) this.items.get(position);
        if (item != null) {
            String imageUrl = AppCore.getInstance().getAppSettings().getTabIcon() + item.getTab().getImage();
            if (StringUtils.isNotEmpty(imageUrl)) {
                this.imageFetcher.loadImage(imageUrl, holder.getImageView());
                holder.getImageView().setVisibility(0);
            } else {
                holder.getImageView().setVisibility(8);
            }
            holder.getTextViewTitle().setText(Html.fromHtml(item.getTab().getLabel()));
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
            }
        }
        return convertView;
    }
}
