package com.biznessapps.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.RssItem;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class PodcastAdapter extends AbstractAdapter<RssItem> {
    private String podcastUrl;

    public PodcastAdapter(Context context, List<RssItem> items) {
        super(context, items, R.layout.podcast_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ViewHolder();
            holder.subtitle = (TextView) convertView.findViewById(R.id.podcast_subtitle_text);
            holder.summary = (TextView) convertView.findViewById(R.id.podcast_summary_text);
            holder.title = (TextView) convertView.findViewById(R.id.podcast_title_text);
            holder.podcastItem = (ImageView) convertView.findViewById(R.id.podcast_item_image);
            holder.playItem = (ImageView) convertView.findViewById(R.id.podcast_play_item_image);
            convertView.setTag(R.id.account_settings_textview, holder);
        } else {
            holder = (ViewHolder) convertView.getTag(R.id.account_settings_textview);
        }
        RssItem item = (RssItem) this.items.get(position);
        if (item != null) {
            convertView.setTag(item);
            holder.playItem.setVisibility(position == this.currentItemIndex ? 0 : 4);
            holder.subtitle.setText(item.getSubtitle());
            holder.summary.setText(item.getSummary());
            holder.title.setText(item.getTitle());
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.summary, holder.title, holder.subtitle);
            }
            String url = item.getImageUrl();
            if (StringUtils.isEmpty(url)) {
                if (StringUtils.isNotEmpty(item.getIcon())) {
                    url = item.getIcon() + AppConstants.ADD_ICON_WIDTH;
                    this.podcastUrl = url;
                } else if (StringUtils.isNotEmpty(this.podcastUrl)) {
                    url = this.podcastUrl;
                } else {
                    url = AppCore.getInstance().getAppSettings().getRssIconUrl();
                }
            }
            this.imageFetcher.loadImage(url, holder.podcastItem);
        }
        return convertView;
    }

    static class ViewHolder {
        ImageView playItem;
        ImageView podcastItem;
        TextView subtitle;
        TextView summary;
        TextView title;

        ViewHolder() {
        }
    }
}
