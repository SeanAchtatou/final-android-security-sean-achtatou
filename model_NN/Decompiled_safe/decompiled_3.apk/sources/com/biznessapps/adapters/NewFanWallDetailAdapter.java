package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class NewFanWallDetailAdapter extends AbstractAdapter<FanWallComment> {
    public NewFanWallDetailAdapter(Context context, List<FanWallComment> items) {
        super(context, items, R.layout.new_fan_wall_detail_item);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.FanWallItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.FanWallItem();
            holder.setCommentTextView((TextView) convertView.findViewById(R.id.simple_bottom_text_view));
            holder.setNameTextView((TextView) convertView.findViewById(R.id.simple_text_view));
            holder.setTimeAgoTextView((TextView) convertView.findViewById(R.id.fan_wall_time_ago));
            holder.setFanWallImageView((ImageView) convertView.findViewById(R.id.row_icon));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.FanWallItem) convertView.getTag();
        }
        FanWallComment item = (FanWallComment) this.items.get(position);
        if (item != null) {
            if (StringUtils.isEmpty(item.getId())) {
                ViewGroup commentsContainer = (ViewGroup) convertView.findViewById(R.id.comment_item_detail_container);
                commentsContainer.removeAllViews();
                commentsContainer.setBackgroundResource(R.drawable.no_comments_bg);
                TextView noTextView = new TextView(getContext());
                noTextView.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                noTextView.setText(R.string.no_comments_yet);
                noTextView.setTextSize(13.0f);
                noTextView.setTextColor(-7829368);
                noTextView.setShadowLayer(2.0f, 0.0f, 0.0f, -1);
                noTextView.setPadding(0, 10, 0, 0);
                noTextView.setGravity(17);
                commentsContainer.addView(noTextView);
                holder.getFanWallImageView().setBackgroundResource(R.drawable.portrait);
            } else {
                holder.getCommentTextView().setText(Html.fromHtml(item.getComment()));
                holder.getNameTextView().setText(item.getTitle());
                holder.getTimeAgoTextView().setText(Html.fromHtml(item.getTimeAgo()));
                if (StringUtils.isNotEmpty(item.getImage())) {
                    this.imageFetcher.loadImage(item.getImage(), holder.getFanWallImageView());
                } else {
                    holder.getFanWallImageView().setBackgroundResource(R.drawable.portrait);
                }
                if (item.hasColor()) {
                    convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                }
            }
        }
        return convertView;
    }
}
