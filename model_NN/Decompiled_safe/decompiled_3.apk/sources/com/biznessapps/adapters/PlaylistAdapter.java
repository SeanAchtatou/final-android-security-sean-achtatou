package com.biznessapps.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.fragments.music.MusicListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.PlaylistItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class PlaylistAdapter extends AbstractAdapter<PlaylistItem> {
    private static final String ALBUM_FORMAT = "%s: %s";
    /* access modifiers changed from: private */
    public MusicListFragment parentFragment;
    private AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();

    public PlaylistAdapter(Context context, List<PlaylistItem> items) {
        super(context, items, R.layout.playlist_row);
    }

    public MusicListFragment getParentFragment() {
        return this.parentFragment;
    }

    public void setParentFragment(MusicListFragment parentFragment2) {
        this.parentFragment = parentFragment2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.PlaylistItem holder;
        if (convertView == null) {
            holder = new ListItemHolder.PlaylistItem();
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder.setTitleView((TextView) convertView.findViewById(R.id.playlist_title_text));
            holder.setAlbumView((TextView) convertView.findViewById(R.id.playlist_album_text));
            holder.setBuyTextView((TextView) convertView.findViewById(R.id.buy_text));
            holder.setPlayItemView((ImageView) convertView.findViewById(R.id.playlist_item_image));
            holder.setPlayItemBuyView((ViewGroup) convertView.findViewById(R.id.playlist_item_buy_container));
            CommonUtils.overrideImageColor(this.settings.getButtonTextColor(), ((ImageView) holder.getPlayItemBuyView().findViewById(R.id.buy_icon)).getBackground());
            CommonUtils.overrideImageColor(this.settings.getButtonBgColor(), holder.getPlayItemBuyView().getBackground());
            CommonUtils.overrideImageColor(this.settings.getButtonTextColor(), holder.getPlayItemView().getDrawable());
            CommonUtils.overrideImageColor(this.settings.getButtonBgColor(), holder.getPlayItemView().getBackground());
            holder.getBuyTextView().setTextColor(this.settings.getButtonTextColor());
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.PlaylistItem) convertView.getTag();
        }
        final PlaylistItem item = (PlaylistItem) this.items.get(position);
        if (item != null) {
            holder.getTitleView().setText(item.getTitle());
            if (StringUtils.isNotEmpty(item.getAlbum())) {
                holder.getAlbumView().setText(String.format(ALBUM_FORMAT, getContext().getString(R.string.album), item.getAlbum()));
            }
            holder.getPlayItemBuyView().setVisibility(StringUtils.isNotEmpty(item.getItune()) ? 0 : 4);
            holder.getPlayItemBuyView().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PlaylistAdapter.this.parentFragment.buyItem(item);
                }
            });
            holder.getPlayItemView().setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PlaylistAdapter.this.parentFragment.playMusic(item);
                }
            });
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTitleView(), holder.getAlbumView());
            }
        }
        return convertView;
    }
}
