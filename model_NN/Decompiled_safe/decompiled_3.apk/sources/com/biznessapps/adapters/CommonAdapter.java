package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class CommonAdapter<T extends CommonListEntity> extends AbstractAdapter<CommonListEntity> {
    public CommonAdapter(Context context, List<CommonListEntity> items) {
        super(context, items, R.layout.info_item_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setFrameContainer((ViewGroup) convertView.findViewById(R.id.info_item_container));
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
            holder.setImageView((ImageView) convertView.findViewById(R.id.row_icon));
            holder.setRightArrowView((ImageView) convertView.findViewById(R.id.right_arrow_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
        }
        CommonListEntity item = (CommonListEntity) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getTitle()));
            if (StringUtils.isNotEmpty(item.getImage())) {
                this.imageFetcher.loadImage(item.getImage(), holder.getImageView());
                holder.getImageView().setVisibility(0);
            } else if (item.getImageId() > 0) {
                holder.getImageView().setBackgroundResource(item.getImageId());
                holder.getImageView().setVisibility(0);
            } else {
                holder.getImageView().setVisibility(8);
            }
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
            }
        }
        checkPositioning(position, convertView, parent);
        return convertView;
    }
}
