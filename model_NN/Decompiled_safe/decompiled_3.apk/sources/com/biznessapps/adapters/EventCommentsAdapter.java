package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class EventCommentsAdapter extends AbstractAdapter<FanWallComment> {
    public EventCommentsAdapter(Context context, List<FanWallComment> items) {
        super(context, items, R.layout.list_event_comment_item);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
            holder.setBottomTextView((TextView) convertView.findViewById(R.id.simple_bottom_text_view));
            holder.setImageView((ImageView) convertView.findViewById(R.id.row_icon));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
        }
        FanWallComment item = (FanWallComment) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getComment()));
            if (StringUtils.isNotEmpty(item.getTitle())) {
                holder.getBottomTextView().setText("By " + item.getTitle() + " " + item.getTimeAgo());
            }
            if (StringUtils.isNotEmpty(item.getImage())) {
                this.imageFetcher.loadImage(item.getImage(), holder.getImageView());
                holder.getImageView().setVisibility(0);
            } else if (item.getImageId() > 0) {
                holder.getImageView().setBackgroundResource(item.getImageId());
                holder.getImageView().setVisibility(0);
            } else {
                holder.getImageView().setVisibility(8);
            }
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
            }
        }
        return convertView;
    }
}
