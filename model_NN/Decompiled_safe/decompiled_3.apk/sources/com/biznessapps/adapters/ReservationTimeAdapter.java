package com.biznessapps.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationTimeItem;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReservationTimeAdapter extends AbstractAdapter<ReservationTimeItem> {
    public ReservationTimeAdapter(Context context, List<ReservationTimeItem> items) {
        super(context, items, R.layout.service_time_cell);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.ReservationTimeItem holder;
        if (convertView == null) {
            holder = new ListItemHolder.ReservationTimeItem();
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder.setTimeFromView((TextView) convertView.findViewById(R.id.service_time_cell_from));
            holder.setTimeToView((TextView) convertView.findViewById(R.id.service_time_cell_to));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.ReservationTimeItem) convertView.getTag();
        }
        ReservationTimeItem item = (ReservationTimeItem) this.items.get(position);
        Date dateFrom = new Date(0, 0, 1, item.getFrom() / 60, item.getFrom() % 60);
        Date dateTo = new Date(0, 0, 1, item.getTo() / 60, item.getTo() % 60);
        SimpleDateFormat formatter = new SimpleDateFormat(AppConstants.RESERVATION_ITEM_HOURS_FORMAT);
        holder.getTimeFromView().setText(formatter.format(dateFrom));
        holder.getTimeToView().setText(formatter.format(dateTo));
        return convertView;
    }
}
