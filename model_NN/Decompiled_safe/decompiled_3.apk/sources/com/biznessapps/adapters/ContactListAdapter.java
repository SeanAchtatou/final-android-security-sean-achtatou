package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.LocationItem;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class ContactListAdapter extends AbstractAdapter<LocationItem> {
    public ContactListAdapter(Context context, List<LocationItem> items) {
        super(context, items, R.layout.location_item);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.LocationItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.LocationItem();
            holder.setTextViewCity((TextView) convertView.findViewById(R.id.location_city_text));
            holder.setTextViewAddress((TextView) convertView.findViewById(R.id.location_address_text));
            holder.setRightArrowView((ImageView) convertView.findViewById(R.id.right_arrow_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.LocationItem) convertView.getTag();
        }
        LocationItem item = (LocationItem) this.items.get(position);
        if (item != null) {
            String titleMessage = item.getCity();
            String state = item.getState();
            if (StringUtils.isNotEmpty(state)) {
                titleMessage = String.format("%s, %s", titleMessage, state);
            }
            holder.getTextViewCity().setText(Html.fromHtml(titleMessage));
            holder.getTextViewAddress().setText(Html.fromHtml(item.getAddress1()));
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewCity(), holder.getTextViewAddress());
            }
        }
        return convertView;
    }
}
