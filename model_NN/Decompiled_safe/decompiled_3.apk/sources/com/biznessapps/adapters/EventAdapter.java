package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.EventItem;
import java.util.List;

public class EventAdapter extends AbstractAdapter<EventItem> {
    public EventAdapter(Context context, List<EventItem> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.EventItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.EventItem();
            holder.setTextViewText((TextView) convertView.findViewById(R.id.event_text));
            holder.setDateTextView((TextView) convertView.findViewById(R.id.event_date_text));
            holder.setTextViewCalendar((TextView) convertView.findViewById(R.id.calendar_text));
            holder.setRightArrowView((ImageView) convertView.findViewById(R.id.right_arrow_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.EventItem) convertView.getTag();
        }
        EventItem item = (EventItem) this.items.get(position);
        if (item != null) {
            TextView monthTextView = (TextView) convertView.findViewById(R.id.calendar_month_text);
            monthTextView.setText(item.getMonth());
            holder.getTextViewText().setText(Html.fromHtml(item.getTitle()));
            holder.getTextViewCalendar().setText(item.getDay());
            if (holder.getDateTextView() != null) {
                holder.getDateTextView().setText(item.getFullDate());
            }
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewText(), holder.getDateTextView(), holder.getTextViewCalendar(), monthTextView);
            }
        }
        return convertView;
    }
}
