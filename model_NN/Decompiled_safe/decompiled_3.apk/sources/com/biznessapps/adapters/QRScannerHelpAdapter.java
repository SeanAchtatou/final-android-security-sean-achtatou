package com.biznessapps.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import java.util.List;

public class QRScannerHelpAdapter extends AbstractAdapter<CommonListEntity> {
    public QRScannerHelpAdapter(Context context, List<CommonListEntity> items) {
        super(context, items, R.layout.qr_scanner_help_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.MessageItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.MessageItem();
            holder.setTextViewText((TextView) convertView.findViewById(R.id.help_description_text));
            holder.setTextViewDate((TextView) convertView.findViewById(R.id.help_position_text));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.MessageItem) convertView.getTag();
        }
        CommonListEntity item = (CommonListEntity) this.items.get(position);
        if (item != null) {
            holder.getTextViewText().setText(Html.fromHtml(item.getTitle()));
            holder.getTextViewDate().setText(Html.fromHtml("" + (position + 1)));
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewText(), holder.getTextViewDate());
            }
        }
        return convertView;
    }
}
