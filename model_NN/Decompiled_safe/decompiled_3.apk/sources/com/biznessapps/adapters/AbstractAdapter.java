package com.biznessapps.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.utils.google.caching.ImageFetcher;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractAdapter<T> extends ArrayAdapter<T> implements AppConstants {
    protected int currentItemIndex;
    protected ImageFetcher imageFetcher;
    protected LayoutInflater inflater;
    protected List<T> items;
    protected int layoutItemResourceId;
    private List<PositionListener> positionListeners;
    protected AppCore.UiSettings settings;
    protected boolean wasOvercolored;

    public interface PositionListener {
        void onLastPositionAchieved(int i);
    }

    public AbstractAdapter(Activity activity, List list, int layoutItemResourceId2) {
        this(activity.getApplicationContext(), list, layoutItemResourceId2);
    }

    public AbstractAdapter(Context context, List list, int layoutItemResourceId2) {
        super(context, 0, list);
        this.inflater = LayoutInflater.from(getContext());
        this.currentItemIndex = -1;
        this.positionListeners = new ArrayList();
        this.settings = AppCore.getInstance().getUiSettings();
        this.items = list;
        this.layoutItemResourceId = layoutItemResourceId2;
        this.imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
    }

    public void setCurrentItemIndex(int currentItemIndex2) {
        this.currentItemIndex = currentItemIndex2;
    }

    public int getCount() {
        return this.items.size();
    }

    public T getItem(int position) {
        return this.items.get(position);
    }

    /* access modifiers changed from: protected */
    public void checkPositioning(int position, View convertView, ViewGroup parent) {
        if (position == this.items.size() - 1) {
            for (PositionListener item : this.positionListeners) {
                if (item != null) {
                    item.onLastPositionAchieved(position);
                }
            }
        }
    }

    public void addPositionListener(PositionListener listenerToAdd) {
        this.positionListeners.add(listenerToAdd);
    }

    public void removePositionListener(PositionListener listenerToRemove) {
        this.positionListeners.remove(listenerToRemove);
    }

    /* access modifiers changed from: protected */
    public StateListDrawable getListItemDrawable(int unselectedColor) {
        StateListDrawable listItemDrawable = new StateListDrawable();
        ColorDrawable unselectedState = new ColorDrawable(unselectedColor);
        ColorDrawable selectedState = new ColorDrawable(Color.parseColor("#999999"));
        listItemDrawable.addState(new int[]{-16842919, -16842908}, unselectedState);
        listItemDrawable.addState(new int[]{16842919}, selectedState);
        listItemDrawable.addState(new int[]{16842908}, selectedState);
        return listItemDrawable;
    }

    /* access modifiers changed from: protected */
    public void setTextColorToView(int textColor, TextView... views) {
        if (views != null && views.length > 0) {
            for (TextView textColor2 : views) {
                textColor2.setTextColor(textColor);
            }
        }
    }
}
