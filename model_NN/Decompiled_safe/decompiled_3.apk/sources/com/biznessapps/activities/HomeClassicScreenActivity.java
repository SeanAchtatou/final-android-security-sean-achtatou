package com.biznessapps.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.SwipeyTabsAdapter;
import com.biznessapps.fragments.single.ImageFragment;
import com.biznessapps.fragments.utils.TellFriendDelegate;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.AbstractAdapter;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.App;
import com.biznessapps.model.LocationItem;
import com.biznessapps.model.Tab;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.facebook.AppEventsConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HomeClassicScreenActivity extends CommonTabFragmentActivity {
    /* access modifiers changed from: private */
    public App appInfo;
    /* access modifiers changed from: private */
    public List<View> buttonList = new ArrayList();
    /* access modifiers changed from: private */
    public Button callUsButton;
    /* access modifiers changed from: private */
    public Button directionButton;
    /* access modifiers changed from: private */
    public GridView gallery;
    private ViewGroup horizontalContainer;
    private ViewGroup layout;
    private AsyncTask<Void, Void, App> loadDataTask;
    /* access modifiers changed from: private */
    public Button tellFriendButton;
    private ViewGroup tellFriendContentView;
    private ViewGroup verticalContainer;

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.loadDataTask != null && this.loadDataTask.getStatus().equals(AsyncTask.Status.RUNNING) && !this.loadDataTask.isCancelled()) {
            this.loadDataTask.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public void loadData() {
        final String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        this.loadDataTask = new AsyncTask<Void, Void, App>() {
            /* access modifiers changed from: protected */
            public App doInBackground(Void... params) {
                App unused = HomeClassicScreenActivity.this.appInfo = (App) HomeClassicScreenActivity.this.cacher().getData(CachingConstants.APP_INFO_PROPERTY + tabId);
                if (HomeClassicScreenActivity.this.appInfo == null) {
                    String url = String.format(ServerConstants.HOME_URL_FORMAT, HomeClassicScreenActivity.this.cacher().getAppCode(), tabId);
                    if (AppCore.getInstance().isTablet()) {
                        url = url + ServerConstants.TABLET_PARAM;
                    }
                    App unused2 = HomeClassicScreenActivity.this.appInfo = JsonParserUtils.parseApp(AppHttpUtils.executeGetSyncRequest(url));
                    HomeClassicScreenActivity.this.cacher().saveData(CachingConstants.APP_INFO_PROPERTY + tabId, HomeClassicScreenActivity.this.appInfo);
                }
                return HomeClassicScreenActivity.this.appInfo;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(App result) {
                super.onPostExecute((Object) result);
                HomeClassicScreenActivity.this.setBackgrounds();
                if (result == null || HomeClassicScreenActivity.this.appInfo.getSubTabs() == null) {
                    HomeClassicScreenActivity.this.gallery.setVisibility(8);
                    return;
                }
                HomeClassicScreenActivity.this.addTabButtons(HomeClassicScreenActivity.this);
                HomeClassicScreenActivity.this.gallery.setVisibility(0);
            }
        };
        this.loadDataTask.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.home_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        int i;
        int i2;
        int i3 = 0;
        super.initViews();
        this.layout = (ViewGroup) findViewById(R.id.home_layout_container);
        this.gallery = (GridView) findViewById(R.id.home_tab_gallery_view);
        this.tellFriendContentView = (ViewGroup) findViewById(R.id.tell_friends_content);
        this.horizontalContainer = (ViewGroup) findViewById(R.id.home_horizontal_container);
        this.verticalContainer = (ViewGroup) findViewById(R.id.home_vertical_container);
        if (AppCore.getInstance().getAppSettings().isUseNewDesign()) {
            this.horizontalContainer.setVisibility(8);
            this.verticalContainer.setVisibility(0);
            this.viewPager.setVisibility(8);
            this.callUsButton = (Button) this.verticalContainer.findViewById(R.id.call_us_button);
            this.directionButton = (Button) this.verticalContainer.findViewById(R.id.direction_button);
            this.tellFriendButton = (Button) this.verticalContainer.findViewById(R.id.tell_friend_button);
        } else {
            this.viewPager.setVisibility(0);
            this.horizontalContainer.setVisibility(0);
            this.verticalContainer.setVisibility(8);
            this.callUsButton = (Button) this.horizontalContainer.findViewById(R.id.call_us_button);
            this.directionButton = (Button) this.horizontalContainer.findViewById(R.id.direction_button);
            this.tellFriendButton = (Button) this.horizontalContainer.findViewById(R.id.tell_friend_button);
        }
        Tab tab = (Tab) getIntent().getSerializableExtra(AppConstants.TAB);
        if (tab != null) {
            Button button = this.callUsButton;
            if (!tab.hasCallButton() || AppCore.getInstance().isTablet()) {
                i = 8;
            } else {
                i = 0;
            }
            button.setVisibility(i);
            Button button2 = this.directionButton;
            if (tab.hasDirectionButton()) {
                i2 = 0;
            } else {
                i2 = 8;
            }
            button2.setVisibility(i2);
            Button button3 = this.tellFriendButton;
            if (!tab.hasTellFriendButton()) {
                i3 = 8;
            }
            button3.setVisibility(i3);
            AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
            this.callUsButton.setTextColor(settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.callUsButton.getBackground());
            this.directionButton.setTextColor(settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.directionButton.getBackground());
            this.tellFriendButton.setTextColor(settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.tellFriendButton.getBackground());
            initButtonListeners();
        }
        TellFriendDelegate.initTellFriends(this, this.tellFriendContentView);
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
    }

    /* access modifiers changed from: protected */
    public void afterViewsInitialization() {
        super.afterViewsInitialization();
        loadData();
    }

    /* access modifiers changed from: protected */
    public List<Fragment> loadFragments() {
        List<Fragment> fragments = new ArrayList<>();
        App newDesignApp = (App) AppCore.getInstance().getCachingManager().getData(CachingConstants.APP_INFO_PROPERTY);
        if (newDesignApp != null && newDesignApp.hasManyImages()) {
            List<String> imagesUrl = newDesignApp.getImagesInOrder();
            for (String url : imagesUrl) {
                ImageFragment imageFragment = new ImageFragment();
                imageFragment.setUrl(url);
                imageFragment.setImageOrderIndex(imagesUrl.indexOf(url));
                imageFragment.setRetainInstance(true);
                fragments.add(imageFragment);
            }
        } else {
            ImageFragment imageFragment2 = new ImageFragment();
            imageFragment2.setRetainInstance(true);
            fragments.add(imageFragment2);
        }
        return fragments;
    }

    private void initButtonListeners() {
        this.callUsButton.setOnClickListener(getOnClickListener());
        this.directionButton.setOnClickListener(getOnClickListener());
        this.tellFriendButton.setOnClickListener(getOnClickListener());
    }

    private View.OnClickListener getOnClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                if (HomeClassicScreenActivity.this.callUsButton.equals(v)) {
                    HomeClassicScreenActivity.this.callUs();
                } else if (HomeClassicScreenActivity.this.directionButton.equals(v)) {
                    HomeClassicScreenActivity.this.showDirections();
                } else if (HomeClassicScreenActivity.this.tellFriendButton.equals(v)) {
                    HomeClassicScreenActivity.this.openFriendContent();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void openFriendContent() {
        if (this.tellFriendContentView.getVisibility() == 8) {
            this.tellFriendContentView.setVisibility(0);
            this.tellFriendContentView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.show_tell_friends_dialog));
        }
    }

    /* access modifiers changed from: private */
    public void callUs() {
        showMultipleDialog(R.string.branch_call_title, new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r4, android.view.View r5, int r6, long r7) {
                /*
                    r3 = this;
                    android.widget.Adapter r2 = r4.getAdapter()
                    java.lang.Object r0 = r2.getItem(r6)
                    com.biznessapps.model.LocationItem r0 = (com.biznessapps.model.LocationItem) r0
                    java.lang.String r1 = r0.getTelephone()
                    com.biznessapps.activities.HomeClassicScreenActivity r2 = com.biznessapps.activities.HomeClassicScreenActivity.this
                    r2.makeCall(r1)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.HomeClassicScreenActivity.AnonymousClass3.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        }, true);
    }

    /* access modifiers changed from: private */
    public void showDirections() {
        showMultipleDialog(R.string.branch_directions_title, new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
                /*
                    r2 = this;
                    android.widget.Adapter r1 = r3.getAdapter()
                    java.lang.Object r0 = r1.getItem(r5)
                    com.biznessapps.model.LocationItem r0 = (com.biznessapps.model.LocationItem) r0
                    com.biznessapps.activities.HomeClassicScreenActivity r1 = com.biznessapps.activities.HomeClassicScreenActivity.this
                    r1.showMap(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.HomeClassicScreenActivity.AnonymousClass4.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        }, false);
    }

    private void showMultipleDialog(int dialogTitle, AdapterView.OnItemClickListener listener, boolean makeCall) {
        List<LocationItem> locations = this.appInfo == null ? null : this.appInfo.getLocations();
        if (locations == null || locations.size() != 1) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            ViewGroup listViewRoot = (ViewGroup) ViewUtils.loadLayout(getApplicationContext(), R.layout.multiple_item_dialog);
            ListView listView = (ListView) listViewRoot.findViewById(R.id.list_view);
            listView.setItemsCanFocus(false);
            if (locations != null && !locations.isEmpty()) {
                listView.setAdapter((ListAdapter) new MulitipleItemAdapter(getApplicationContext(), locations));
            }
            listView.setOnItemClickListener(listener);
            alertBuilder.setView(listViewRoot);
            alertBuilder.setMessage(dialogTitle);
            alertBuilder.show();
        } else if (makeCall) {
            makeCall(locations.get(0).getTelephone());
        } else {
            showMap(locations.get(0));
        }
    }

    /* access modifiers changed from: private */
    public void makeCall(String tel) {
        if (StringUtils.isNotEmpty(tel)) {
            startActivity(new Intent("android.intent.action.DIAL", Uri.parse(AppConstants.TEL_TYPE + tel)));
        }
    }

    /* access modifiers changed from: private */
    public void showMap(LocationItem location) {
        if (location != null) {
            ViewUtils.openGoogleMap(getApplicationContext(), location.getLongitude(), location.getLatitude());
        }
    }

    /* access modifiers changed from: private */
    public void addTabButtons(final Activity holdActivity) {
        List<Tab> subTabs = this.appInfo.getSubTabs();
        this.buttonList.clear();
        if (subTabs != null && subTabs.size() > 0) {
            for (final Tab tab : subTabs) {
                if (tab.isActive() && !tab.getTabId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    ViewGroup view = (ViewGroup) ViewUtils.loadLayout(holdActivity.getApplicationContext(), R.layout.sub_tab_layout);
                    ImageView button = (ImageView) view.findViewById(R.id.sub_home_image);
                    TextView textView = (TextView) view.findViewById(R.id.sub_home_text);
                    textView.setTextColor(Color.parseColor("#" + tab.getTabLabelTextColor()));
                    textView.setText(tab.getLabel());
                    getImageManager().downloadLigthweight(tab.getTabImageUrl(), button);
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Tab tabToUse = null;
                            Iterator i$ = HomeClassicScreenActivity.this.cacher().getTabList().iterator();
                            while (true) {
                                if (!i$.hasNext()) {
                                    break;
                                }
                                Tab item = i$.next();
                                if (item.getTabId().equalsIgnoreCase(tab.getTabId())) {
                                    tabToUse = item;
                                    break;
                                }
                            }
                            if (tabToUse != null) {
                                Intent intent = new Intent(holdActivity.getApplicationContext(), SingleFragmentActivity.class);
                                if (StringUtils.isNotEmpty(tabToUse.getUrl())) {
                                    intent.putExtra(AppConstants.URL, tabToUse.getUrl());
                                }
                                intent.putExtra(AppConstants.TAB_ID, tabToUse.getId());
                                intent.putExtra(AppConstants.TAB_LABEL, tabToUse.getLabel());
                                intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabToUse.getTabId());
                                intent.putExtra(AppConstants.ITEM_ID, tabToUse.getItemId());
                                intent.putExtra(AppConstants.SECTION_ID, tabToUse.getSectionId());
                                intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, tab.getViewController());
                                intent.putExtra(AppConstants.TAB, tab);
                                HomeClassicScreenActivity.this.startActivity(intent);
                            }
                        }
                    });
                    button.setClickable(true);
                    this.buttonList.add(view);
                }
            }
            if (this.buttonList.size() > 0) {
                this.gallery.setAdapter((ListAdapter) new AppsAdapter());
            }
        }
    }

    /* access modifiers changed from: private */
    public void setBackgrounds() {
        if (this.appInfo != null && StringUtils.isNotEmpty(this.appInfo.getImageUrl())) {
            getImageManager().downloadBgUrl(this.appInfo.getImageUrl(), this.layout);
            AppCore.getInstance().getUiSettings().setHomeBgUrl(this.appInfo.getImageUrl());
        }
    }

    protected class SwipeyTabsPagerAdapter extends FragmentPagerAdapter implements SwipeyTabsAdapter {
        private final List<Fragment> fragments;

        public SwipeyTabsPagerAdapter(FragmentManager fm, List<Fragment> fragments2) {
            super(fm);
            this.fragments = fragments2;
        }

        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        public int getCount() {
            return this.fragments.size();
        }

        public View getTab(int position) {
            return new TextView(HomeClassicScreenActivity.this.getApplicationContext());
        }
    }

    /* access modifiers changed from: private */
    public String getFullAddress(LocationItem location) {
        String address1 = location.getAddress1();
        String address2 = location.getAddress2();
        String city = location.getCity();
        String state = location.getState();
        String zip = location.getZip();
        String address = "";
        if (StringUtils.isNotEmpty(address1)) {
            address = String.format("%s %s", address, address1);
        }
        if (StringUtils.isNotEmpty(address2)) {
            address = String.format("%s, %s", address, address2);
        }
        if (StringUtils.isNotEmpty(city)) {
            address = String.format("%s, %s", address, city);
        }
        if (StringUtils.isNotEmpty(state)) {
            address = String.format("%s, %s", address, state);
        }
        if (!StringUtils.isNotEmpty(zip)) {
            return address;
        }
        return String.format("%s, %s", address, zip);
    }

    public class MulitipleItemAdapter extends AbstractAdapter<LocationItem> {
        public MulitipleItemAdapter(Context context, List<LocationItem> items) {
            super(context, items, R.layout.multiple_row);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemHolder.CommonItem holder;
            LocationItem item = (LocationItem) this.items.get(position);
            if (convertView == null) {
                convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
                holder = new ListItemHolder.CommonItem();
                holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
                convertView.setTag(holder);
            } else {
                holder = (ListItemHolder.CommonItem) convertView.getTag();
            }
            if (item != null) {
                holder.getTextViewTitle().setText(HomeClassicScreenActivity.this.getFullAddress(item));
            }
            return convertView;
        }
    }

    public class AppsAdapter extends BaseAdapter {
        public AppsAdapter() {
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            return (View) HomeClassicScreenActivity.this.buttonList.get(position);
        }

        public final int getCount() {
            return HomeClassicScreenActivity.this.buttonList.size();
        }

        public final Object getItem(int position) {
            return HomeClassicScreenActivity.this.buttonList.get(position);
        }

        public final long getItemId(int position) {
            return (long) position;
        }
    }
}
