package com.biznessapps.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.MarkerOverlay;
import com.biznessapps.utils.ViewUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CarFinderActivity extends CommonMapActivity {
    private static final int DECREASE_ALARM_EVENT = 2;
    private static final int DELAY_TIME = 1000;
    private static final String EMAIL_PHOTO_IMAGE = "email_photo_image.jpg";
    private static final int INCREASE_TIMER_EVENT = 1;
    private static final String LABEL_DATE_FORMAT = "MMM d, yyyy h:m a";
    private static final int SECS_IN_MINUTE = 60;
    private static Ringtone alarmRingtone;
    /* access modifiers changed from: private */
    public static Location carLocation;
    private static long lastAlarmValue;
    /* access modifiers changed from: private */
    public static long lastTimerValue;
    /* access modifiers changed from: private */
    public ImageButton alarmButton;
    /* access modifiers changed from: private */
    public int alarmCounter;
    /* access modifiers changed from: private */
    public TextView alarmTextView;
    /* access modifiers changed from: private */
    public ViewGroup bottomContainer;
    private Handler eventHandler;
    /* access modifiers changed from: private */
    public boolean isCarLocationSet;
    private LocationListener locationListener;
    /* access modifiers changed from: private */
    public MapView map;
    /* access modifiers changed from: private */
    public Location myLocation;
    private File photoImage;
    private String selectedImagePath;
    /* access modifiers changed from: private */
    public int timerCounter;
    /* access modifiers changed from: private */
    public TextView timerTextView;

    static /* synthetic */ int access$1510(CarFinderActivity x0) {
        int i = x0.alarmCounter;
        x0.alarmCounter = i - 1;
        return i;
    }

    static /* synthetic */ int access$2008(CarFinderActivity x0) {
        int i = x0.timerCounter;
        x0.timerCounter = i + 1;
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        initMap();
        initLocation();
        initTimers();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.car_finder_layout;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        AppCore.getInstance().getLocationFinder().removeLocationListener(getLocationListener());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AppCore.getInstance().getLocationFinder().startSearching();
        checkLastValues();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        AppCore.getInstance().getLocationFinder().stopSearching();
        if (this.timerCounter > 0) {
            lastTimerValue = System.currentTimeMillis() - ((long) (this.timerCounter * 1000));
        }
        if (this.alarmCounter > 0) {
            lastAlarmValue = System.currentTimeMillis() + ((long) (this.alarmCounter * 1000));
        }
        stopTimer();
        stopAlarmTimer();
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    /* access modifiers changed from: private */
    public void initMap() {
        this.map = findViewById(R.id.mapView);
        this.map.setBuiltInZoomControls(true);
        this.map.setClickable(true);
    }

    private void initLocation() {
        this.myLocation = AppCore.getInstance().getLocationFinder().getCurrentLocation();
        AppCore.getInstance().getLocationFinder().addLocationListener(getLocationListener());
    }

    /* access modifiers changed from: private */
    public void initPois() {
        this.map.getOverlays().clear();
        addPoint(this.myLocation, false);
        if (this.isCarLocationSet) {
            addPoint(carLocation, true);
        }
    }

    private void addPoint(Location location, boolean isCarLocation) {
        if (location != null) {
            GeoPoint p = new GeoPoint((int) (location.getLatitude() * 1000000.0d), (int) (location.getLongitude() * 1000000.0d));
            MapController mapController = this.map.getController();
            mapController.setZoom(15);
            mapController.animateTo(p);
            MarkerOverlay overlay = new MarkerOverlay(p, isCarLocation ? BitmapFactory.decodeResource(getResources(), R.drawable.pin_red) : BitmapFactory.decodeResource(getResources(), R.drawable.my_location_bubble));
            overlay.setShouldShowRadius(isCarLocation);
            overlay.setPointTitle(getString(R.string.current_location));
            if (isCarLocation) {
                overlay.setLabelTitle(new SimpleDateFormat(LABEL_DATE_FORMAT).format(Calendar.getInstance().getTime()));
                overlay.setArrowBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.arrow));
            }
            this.map.getOverlays().add(overlay);
        }
    }

    private void initViews() {
        this.bottomContainer = (ViewGroup) findViewById(R.id.car_finder_bottom_container);
        this.timerTextView = (TextView) findViewById(R.id.car_finder_timer_text);
        this.alarmTextView = (TextView) findViewById(R.id.car_finder_alarm_text);
        this.timerTextView.setText(getString(R.string.set));
        this.alarmTextView.setText(getString(R.string.set_timer));
        initButtons();
    }

    private void initButtons() {
        this.alarmButton = (ImageButton) findViewById(R.id.car_finder_alarm_button);
        final Button mapModeButton = (Button) findViewById(R.id.car_finder_map_mode_button);
        final Button hybModeButton = (Button) findViewById(R.id.car_finder_hyb_mode_button);
        mapModeButton.setSelected(true);
        this.alarmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                CarFinderActivity.this.startActivityForResult(new Intent(CarFinderActivity.this.getApplicationContext(), ChooseTimerActivity.class), 20);
            }
        });
        mapModeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                mapModeButton.setSelected(true);
                hybModeButton.setSelected(false);
                CarFinderActivity.this.initMap();
                CarFinderActivity.this.initPois();
                CarFinderActivity.this.map.setSatellite(false);
                CarFinderActivity.this.map.setStreetView(true);
                CarFinderActivity.this.map.invalidate();
            }
        });
        hybModeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hybModeButton.setSelected(true);
                mapModeButton.setSelected(false);
                CarFinderActivity.this.initMap();
                CarFinderActivity.this.initPois();
                CarFinderActivity.this.map.setSatellite(true);
                CarFinderActivity.this.map.setStreetView(false);
                CarFinderActivity.this.map.invalidate();
            }
        });
        ((ImageButton) findViewById(R.id.car_finder_directions_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (CarFinderActivity.this.myLocation != null) {
                    ViewUtils.openGoogleMap(CarFinderActivity.this.getApplicationContext(), "" + CarFinderActivity.this.myLocation.getLongitude(), "" + CarFinderActivity.this.myLocation.getLatitude());
                }
            }
        });
        ((ImageButton) findViewById(R.id.car_finder_photo_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                CarFinderActivity.this.emailPhoto();
            }
        });
        ((ImageButton) findViewById(R.id.car_finder_email_button)).setOnClickListener(new View.OnClickListener() {
            /* JADX WARN: Type inference failed for: r2v6, types: [com.biznessapps.activities.CarFinderActivity, android.app.Activity] */
            public void onClick(View arg0) {
                ViewUtils.email(CarFinderActivity.this, new String[]{""}, CarFinderActivity.this.getString(R.string.current_location), CarFinderActivity.this.getString(R.string.car_finder_email_message) + AppConstants.NEW_LINE + String.format(AppConstants.GOOGLE_MAP_LINK_FORMAT, Double.valueOf(CarFinderActivity.this.myLocation.getLatitude()), Double.valueOf(CarFinderActivity.this.myLocation.getLongitude())));
            }
        });
        ((ImageButton) findViewById(R.id.car_finder_trash_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                CarFinderActivity.this.stopTimer();
                CarFinderActivity.this.stopAlarmTimer();
                CarFinderActivity.this.reinitAlarm();
                long unused = CarFinderActivity.lastTimerValue = 0;
                boolean unused2 = CarFinderActivity.this.isCarLocationSet = false;
                Location unused3 = CarFinderActivity.carLocation = null;
                CarFinderActivity.this.initPois();
                CarFinderActivity.this.bottomContainer.setVisibility(8);
            }
        });
        ((ImageButton) findViewById(R.id.car_finder_pin_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (CarFinderActivity.this.myLocation != null) {
                    Location unused = CarFinderActivity.carLocation = CarFinderActivity.this.myLocation;
                    boolean unused2 = CarFinderActivity.this.isCarLocationSet = true;
                    CarFinderActivity.this.initPois();
                    CarFinderActivity.this.bottomContainer.setVisibility(0);
                    CarFinderActivity.this.sendChangeTabMessage(1);
                }
            }
        });
        ((ImageButton) findViewById(R.id.car_finder_location_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
            }
        });
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [android.content.Context, com.biznessapps.activities.CarFinderActivity] */
    /* access modifiers changed from: private */
    public void emailPhoto() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View view = ViewUtils.loadLayout(getApplicationContext(), R.layout.email_photo_dialog);
        alertBuilder.setView(view);
        alertBuilder.setMessage(R.string.choose_photo);
        final AlertDialog dialog = alertBuilder.create();
        ((Button) view.findViewById(R.id.take_photo_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CarFinderActivity.this.openStandartPhotoView();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        ((Button) view.findViewById(R.id.chose_from_library_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CarFinderActivity.this.choseFromLibrary();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        ((Button) view.findViewById(R.id.email_photo_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void choseFromLibrary() {
        Intent intent = new Intent();
        intent.setType(AppConstants.IMAGE_TYPE);
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), 3);
    }

    /* access modifiers changed from: private */
    public void openStandartPhotoView() {
        Intent intent = new Intent(AppConstants.IMAGE_CAPTURE_INTENT_NAME);
        if ("mounted".equals(Environment.getExternalStorageState())) {
            this.photoImage = new File(Environment.getExternalStorageDirectory(), EMAIL_PHOTO_IMAGE);
            intent.putExtra("output", Uri.fromFile(this.photoImage));
            startActivityForResult(intent, 2);
            return;
        }
        Toast.makeText(getApplicationContext(), R.string.sdcard_missed, 1).show();
    }

    /* JADX WARN: Type inference failed for: r14v0, types: [com.biznessapps.activities.CarFinderActivity, android.app.Activity, com.biznessapps.activities.CommonMapActivity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public void onActivityResult(int r15, int r16, android.content.Intent r17) {
        /*
            r14 = this;
            switch(r15) {
                case 2: goto L_0x000d;
                case 3: goto L_0x0027;
                default: goto L_0x0003;
            }
        L_0x0003:
            super.onActivityResult(r15, r16, r17)
        L_0x0006:
            switch(r16) {
                case 20: goto L_0x004b;
                case 21: goto L_0x0068;
                default: goto L_0x0009;
            }
        L_0x0009:
            super.onActivityResult(r15, r16, r17)
        L_0x000c:
            return
        L_0x000d:
            java.io.File r6 = new java.io.File
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.lang.String r2 = "email_photo_image.jpg"
            r6.<init>(r1, r2)
            android.content.Context r2 = r14.getApplicationContext()
            java.lang.String r3 = ""
            java.lang.String r4 = ""
            java.lang.String r5 = ""
            r1 = r14
            r1.email(r2, r3, r4, r5, r6)
            goto L_0x0006
        L_0x0027:
            android.net.Uri r13 = r17.getData()
            java.lang.String r1 = com.biznessapps.utils.CommonUtils.getPath(r13, r14)
            r14.selectedImagePath = r1
            java.lang.String r1 = r14.selectedImagePath
            if (r1 == 0) goto L_0x0006
            java.io.File r12 = new java.io.File
            java.lang.String r1 = r14.selectedImagePath
            r12.<init>(r1)
            android.content.Context r8 = r14.getApplicationContext()
            java.lang.String r9 = ""
            java.lang.String r10 = ""
            java.lang.String r11 = ""
            r7 = r14
            r7.email(r8, r9, r10, r11, r12)
            goto L_0x0006
        L_0x004b:
            java.lang.String r1 = "HOURS_EXTRA"
            r2 = 0
            r0 = r17
            int r1 = r0.getIntExtra(r1, r2)
            java.lang.String r2 = "MINUTES_EXTRA"
            r3 = 0
            r0 = r17
            int r2 = r0.getIntExtra(r2, r3)
            int r1 = r14.getTimeValue(r1, r2)
            r14.alarmCounter = r1
            r1 = 2
            r14.sendChangeTabMessage(r1)
            goto L_0x000c
        L_0x0068:
            r14.stopAlarmTimer()
            r14.reinitAlarm()
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.CarFinderActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    /* access modifiers changed from: private */
    public void reinitAlarm() {
        this.alarmTextView.setTextColor(-1);
        this.alarmButton.setImageResource(R.drawable.alarm_on);
        lastAlarmValue = 0;
    }

    /* access modifiers changed from: private */
    public void stopAlarmTimer() {
        this.alarmTextView.setText(getString(R.string.set_timer));
        this.alarmCounter = 0;
        stopTimer(2);
        this.alarmTextView.setTextColor(-1);
        stopAlarmSound();
    }

    /* access modifiers changed from: private */
    public void startAlarmSound() {
        Uri alert = RingtoneManager.getDefaultUri(4);
        if (alert == null) {
            alert = RingtoneManager.getDefaultUri(2);
        }
        if (alert != null) {
            if (alarmRingtone == null) {
                alarmRingtone = RingtoneManager.getRingtone(getApplicationContext(), alert);
            }
            if (!alarmRingtone.isPlaying()) {
                alarmRingtone.play();
            }
        }
    }

    private void stopAlarmSound() {
        if (alarmRingtone != null) {
            alarmRingtone.stop();
            alarmRingtone = null;
        }
    }

    /* access modifiers changed from: private */
    public void stopTimer() {
        this.timerTextView.setText(getString(R.string.set));
        this.timerCounter = 0;
        stopTimer(1);
    }

    private void email(Context context, String emailTo, String subject, String emailText, File fileToSend) {
        Intent intent = new Intent("android.intent.action.SEND_MULTIPLE");
        intent.setFlags(268435456);
        intent.setType(AppConstants.PLAIN_TEXT);
        intent.putExtra("android.intent.extra.EMAIL", new String[]{emailTo});
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", emailText);
        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(Uri.fromFile(fileToSend));
        intent.putParcelableArrayListExtra("android.intent.extra.STREAM", uris);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_email)));
    }

    private int getTimeValue(int hours, int minutes) {
        return (minutes * SECS_IN_MINUTE) + (hours * 3600);
    }

    /* access modifiers changed from: private */
    public String getSecInTimeFormat(int totalSecNumber) {
        boolean isNegative;
        if (totalSecNumber < 0) {
            isNegative = true;
        } else {
            isNegative = false;
        }
        int numberOfSec = Math.abs(totalSecNumber) % SECS_IN_MINUTE;
        int numberOfMin = (Math.abs(totalSecNumber) / SECS_IN_MINUTE) % SECS_IN_MINUTE;
        return (isNegative ? "-" : "") + String.format("%02d:%02d:%02d", Integer.valueOf((Math.abs(totalSecNumber) / SECS_IN_MINUTE) / SECS_IN_MINUTE), Integer.valueOf(numberOfMin), Integer.valueOf(numberOfSec));
    }

    private void checkLastValues() {
        if (lastTimerValue > 0) {
            this.bottomContainer.setVisibility(0);
            this.timerCounter = (int) ((System.currentTimeMillis() - lastTimerValue) / 1000);
            this.timerCounter++;
            this.timerTextView.setText(getSecInTimeFormat(this.timerCounter));
            sendChangeTabMessage(1);
        }
        if (lastAlarmValue > 0) {
            this.alarmCounter = (int) ((lastAlarmValue - System.currentTimeMillis()) / 1000);
            this.alarmTextView.setText(getSecInTimeFormat(this.alarmCounter));
            sendChangeTabMessage(2);
        }
        if (carLocation != null) {
            this.isCarLocationSet = true;
            initPois();
        }
    }

    private void initTimers() {
        this.eventHandler = new Handler(getMainLooper()) {
            public void handleMessage(Message newMessage) {
                switch (newMessage.what) {
                    case 1:
                        CarFinderActivity.this.timerTextView.setText(CarFinderActivity.this.getSecInTimeFormat(CarFinderActivity.this.timerCounter));
                        CarFinderActivity.access$2008(CarFinderActivity.this);
                        CarFinderActivity.this.sendChangeTabMessage(1);
                        return;
                    case 2:
                        if (CarFinderActivity.this.alarmCounter >= 0) {
                            CarFinderActivity.this.alarmTextView.setTextColor(-1);
                            CarFinderActivity.this.alarmButton.setImageResource(R.drawable.alarm_on);
                        } else {
                            CarFinderActivity.this.alarmButton.setImageResource(R.drawable.alarm_red);
                            CarFinderActivity.this.alarmTextView.setTextColor(-65536);
                            CarFinderActivity.this.startAlarmSound();
                        }
                        CarFinderActivity.this.alarmTextView.setText(CarFinderActivity.this.getSecInTimeFormat(CarFinderActivity.this.alarmCounter));
                        CarFinderActivity.access$1510(CarFinderActivity.this);
                        CarFinderActivity.this.sendChangeTabMessage(2);
                        return;
                    default:
                        return;
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void sendChangeTabMessage(int eventId) {
        Message message = this.eventHandler.obtainMessage(eventId);
        this.eventHandler.removeMessages(eventId);
        this.eventHandler.sendMessageDelayed(message, 1000);
    }

    private void stopTimer(int eventId) {
        this.eventHandler.removeMessages(eventId);
    }

    private LocationListener getLocationListener() {
        if (this.locationListener == null) {
            this.locationListener = new LocationListener() {
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }

                public void onLocationChanged(Location location) {
                    Location unused = CarFinderActivity.this.myLocation = location;
                    CarFinderActivity.this.initPois();
                }
            };
        }
        return this.locationListener;
    }
}
