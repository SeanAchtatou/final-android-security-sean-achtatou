package com.biznessapps.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.LoadingDataInterface;
import com.biznessapps.api.navigation.NavigationManager;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.delegate.MusicDelegate;
import com.biznessapps.images.NewImageManager;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.AppSettings;
import com.biznessapps.player.MusicItem;
import com.biznessapps.player.PlayerService;
import com.biznessapps.player.PlayerStateListener;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.google.caching.ImageWorker;
import com.google.android.maps.MapActivity;
import java.util.List;

public abstract class CommonMapActivity extends MapActivity implements AppConstants, LoadingDataInterface, SociableActivityInterface {
    protected static final int FIRST_LOAD_TAB_INDEX = 0;
    protected static final long NO_TAB_DEFINED = -1;
    protected String bgUrl;
    private boolean isTabMenuUsed;
    protected SocialNetworkAccessor.SocialNetworkListener listener;
    protected MusicDelegate musicDelegate = new MusicDelegate();
    private ViewGroup musicRootLayout;
    protected NavigationManager navigManager;
    protected FrameLayout navigationContainer;
    private PlayerStateListener playerListener = new PlayerStateListener() {
        public void onStart(MusicItem item) {
            super.onStart(item);
            CommonMapActivity.this.musicDelegate.updateTrackInfo(item);
        }

        public void onStateChanged(int newState) {
            CommonMapActivity.this.musicDelegate.updatePlayerState(newState);
        }
    };

    public NavigationManager getNavigationManager() {
        return this.navigManager;
    }

    public void onCreate(Bundle savedInstanceState) {
        CommonMapActivity.super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(getLayoutId());
        if (!AppCore.getInstance().isInitialized()) {
            AppCore.getInstance().init(getApplicationContext());
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        initNavigationManager();
        initMusicPanel();
    }

    private void initMusicPanel() {
        boolean showPanel;
        ViewGroup musicBottomContainer;
        if (!AppCore.getInstance().getAppSettings().isMusicOnTop() && (musicBottomContainer = (ViewGroup) findViewById(R.id.music_control_container_bottom)) != null) {
            musicBottomContainer.setVisibility(0);
            this.musicRootLayout = (ViewGroup) musicBottomContainer.findViewById(R.id.music_control_root);
        }
        if (this.musicRootLayout == null) {
            ViewGroup musicTopContainer = (ViewGroup) findViewById(R.id.music_control_container);
            if (musicTopContainer != null) {
                musicTopContainer.setVisibility(0);
                this.musicRootLayout = (ViewGroup) musicTopContainer.findViewById(R.id.music_control_root);
            }
            if (this.musicRootLayout == null) {
                this.musicRootLayout = (ViewGroup) findViewById(R.id.music_control_root);
            }
        }
        if (this.musicRootLayout != null) {
            if (AppCore.getInstance().getAppSettings().isMusicOnFront()) {
                this.musicDelegate.initViews(this.musicRootLayout, getApplicationContext());
                List<MusicItem> songsList = this.musicDelegate.getPlayerServiceAccessor().getSongs();
                if (songsList == null || songsList.isEmpty()) {
                    showPanel = false;
                } else {
                    showPanel = true;
                }
                if (showPanel) {
                    this.musicRootLayout.setVisibility(0);
                }
                this.musicDelegate.updateTrackInfo();
            } else {
                this.musicRootLayout.setVisibility(8);
            }
            PlayerService.addListener(this.playerListener);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService.removeListener(this.playerListener);
        CommonMapActivity.super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = new AnalyticItem();
        AppSettings settings = AppCore.getInstance().getAppSettings();
        data.setContext(getApplicationContext());
        data.setAccountId(settings.getGaAccountId());
        data.setAppId(settings.getAppId());
        data.setTabId(getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        return data;
    }

    public ViewGroup getProgressBarContainer() {
        throw new IllegalArgumentException("This method should be implemented");
    }

    /* access modifiers changed from: protected */
    public void initTitleBar(boolean isAdsShown) {
        ViewGroup titleBarRoot = (ViewGroup) findViewById(R.id.tab_title_container);
        if (titleBarRoot != null) {
            TextView titleTextView = (TextView) titleBarRoot.findViewById(R.id.tab_title_text);
            if (getIntent().getBooleanExtra(AppConstants.CHILDREN_TAB_LABEL_PRESENT, false)) {
                titleTextView.setText(getIntent().getStringExtra(AppConstants.CHILDREN_TAB_LABEL));
            } else {
                titleTextView.setText(getIntent().getStringExtra(AppConstants.TAB_LABEL));
            }
            AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
            titleTextView.setTextColor(uiSettings.getNavigationTextColor());
            titleTextView.setShadowLayer(1.2f, 1.2f, 1.2f, uiSettings.getNavigationTextShadowColor());
            AppSettings appSettings = AppCore.getInstance().getAppSettings();
            if (StringUtils.isNotEmpty(appSettings.getGlobalHeaderUrl())) {
                ImageWorker.TintContainer tint = new ImageWorker.TintContainer();
                tint.setTintColor(appSettings.getNavigBarColor());
                tint.setTintOpacity(128.0f);
                AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadImage(appSettings.getGlobalHeaderUrl(), titleBarRoot, tint);
                return;
            }
            titleBarRoot.setBackgroundDrawable(getTitleBg());
        }
    }

    public GradientDrawable getTitleBg() {
        AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        int originalColor = uiSettings.getNavigationBarColor();
        int topColor = Color.argb((int) AppConstants.TRANSPARENT_BG_VALUE, Color.red(originalColor), Color.green(originalColor), Color.blue(originalColor));
        GradientDrawable gradientBg = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{uiSettings.getNavigationBarColor(), topColor});
        gradientBg.setCornerRadius(0.0f);
        return gradientBg;
    }

    /* access modifiers changed from: protected */
    public CachingManager cacher() {
        return AppCore.getInstance().getCachingManager();
    }

    /* access modifiers changed from: protected */
    public NewImageManager getNewImageManager() {
        return AppCore.getInstance().getNewImageManager();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.biznessapps.activities.CommonMapActivity, android.app.Activity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void initNavigationManager() {
        /*
            r3 = this;
            r2 = 0
            com.biznessapps.api.navigation.NavigationManager r0 = new com.biznessapps.api.navigation.NavigationManager
            r0.<init>(r3)
            r3.navigManager = r0
            int r0 = com.biznessapps.layout.R.id.bottom_navig_conrol_container
            android.view.View r0 = r3.findViewById(r0)
            android.widget.FrameLayout r0 = (android.widget.FrameLayout) r0
            r3.navigationContainer = r0
            com.biznessapps.api.navigation.NavigationManager r0 = r3.navigManager
            android.widget.FrameLayout r1 = r3.navigationContainer
            r0.addLayoutTo(r1)
            r3.isTabMenuUsed = r2
            boolean r0 = r3.isTabMenuUsed
            if (r0 == 0) goto L_0x002b
            boolean r0 = r3.hasNavigationMenu()
            if (r0 == 0) goto L_0x002b
            android.widget.FrameLayout r0 = r3.navigationContainer
            r0.setVisibility(r2)
        L_0x002a:
            return
        L_0x002b:
            android.widget.FrameLayout r0 = r3.navigationContainer
            r1 = 8
            r0.setVisibility(r1)
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.CommonMapActivity.initNavigationManager():void");
    }

    public long getTabId() {
        if (getIntent() != null) {
            return getIntent().getLongExtra(AppConstants.TAB_ID, -1);
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public boolean hasNavigationMenu() {
        return true;
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [com.google.android.maps.MapActivity, com.biznessapps.activities.CommonMapActivity, android.app.Activity] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onResume() {
        /*
            r8 = this;
            com.biznessapps.activities.CommonMapActivity.super.onResume()
            boolean r4 = r8.isTabMenuUsed
            if (r4 == 0) goto L_0x0032
            java.util.List r2 = com.biznessapps.api.navigation.NavigationManager.getTabsItems()
            if (r2 == 0) goto L_0x001d
            int r4 = r2.size()
            if (r4 <= 0) goto L_0x001d
            com.biznessapps.api.navigation.NavigationManager r4 = r8.navigManager
            r4.clearTabs()
            com.biznessapps.api.navigation.NavigationManager r4 = r8.navigManager
            r4.updateTabs()
        L_0x001d:
            long r4 = r8.getTabId()
            r6 = -1
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 != 0) goto L_0x004f
            com.biznessapps.api.navigation.NavigationManager r4 = r8.navigManager
            com.biznessapps.api.navigation.NavigationManager r5 = r8.navigManager
            long r5 = r5.getCurrentTabSelection()
            r4.setTabSelection(r5)
        L_0x0032:
            int r4 = com.biznessapps.layout.R.id.ads_layout_container
            android.view.View r0 = r8.findViewById(r4)
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            r4 = 1
            boolean r1 = com.biznessapps.utils.ViewUtils.showAdsIfNeeded(r8, r0, r4)
            int r4 = com.biznessapps.layout.R.id.tab_title_container
            android.view.View r3 = r8.findViewById(r4)
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
            android.content.Intent r4 = r8.getIntent()
            com.biznessapps.utils.ViewUtils.showTitleBar(r3, r4, r1)
            return
        L_0x004f:
            com.biznessapps.api.navigation.NavigationManager r4 = r8.navigManager
            long r5 = r8.getTabId()
            r4.setTabSelection(r5)
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.CommonMapActivity.onResume():void");
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.google.android.maps.MapActivity, com.biznessapps.activities.CommonMapActivity, android.app.Activity] */
    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        CommonMapActivity.super.onActivityResult(requestCode, resultCode, data);
        if (this.listener != null) {
            this.listener.onActivityResult(this, requestCode, resultCode, data);
        }
    }

    public void setSocialNetworkListener(SocialNetworkAccessor.SocialNetworkListener listener2) {
        this.listener = listener2;
    }
}
