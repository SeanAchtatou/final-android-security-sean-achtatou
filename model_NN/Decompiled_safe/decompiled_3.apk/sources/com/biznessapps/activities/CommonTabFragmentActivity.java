package com.biznessapps.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Display;
import android.view.View;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CustomizableViewPager;
import com.biznessapps.fragments.SwipeyTabsAdapter;
import com.biznessapps.layout.MainController;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.AppSettings;
import java.util.List;

public abstract class CommonTabFragmentActivity extends CommonFragmentActivity {
    protected static final int CHANGE_FRAGMENT_EVENT = 2;
    protected static final int CHANGE_TAB_EVENT = 1;
    protected static final int FIRST_INDEX = 0;
    protected static final int FIRST_LOAD_TAB_INDEX = 0;
    protected static final int FRAGMENT_SWITCHING_DELAY_TIME = 5000;
    protected static final int STARTUP_DELAY_TIME = 5000;
    protected static final int TAB_SWITCHING_DELAY_TIME = 100;
    /* access modifiers changed from: private */
    public int currentFragmentIndex = 0;
    /* access modifiers changed from: private */
    public Handler fragmentsHandler;
    protected List<Fragment> fragmentsList;
    protected List<Fragment> tabFragmentsList;
    protected ViewPager tabViewPager;
    protected CustomizableViewPager viewPager;

    /* access modifiers changed from: protected */
    public abstract List<Fragment> loadFragments();

    static /* synthetic */ int access$008(CommonTabFragmentActivity x0) {
        int i = x0.currentFragmentIndex;
        x0.currentFragmentIndex = i + 1;
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!AppCore.getInstance().getAppSettings().isActive() || getIntent() == null) {
            startActivity(new Intent(getApplicationContext(), MainController.class));
            finish();
            return;
        }
        this.fragmentsList = loadFragments();
        this.tabFragmentsList = loadTabFragments();
        initViews();
        afterViewsInitialization();
        if (AppCore.getInstance().getAppSettings().getAnimationMode() == 1) {
            initFragmentsSliderAnimation();
        }
    }

    public CustomizableViewPager getViewPager() {
        return this.viewPager;
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = new AnalyticItem();
        AppSettings settings = AppCore.getInstance().getAppSettings();
        data.setContext(getApplicationContext());
        data.setAccountId(settings.getGaAccountId());
        data.setAppId(settings.getAppId());
        data.setTabId(getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        return data;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        this.viewPager = (CustomizableViewPager) findViewById(R.id.viewpager);
        this.viewPager.setAdapter(new SwipeyTabsPagerAdapter(getSupportFragmentManager(), this.fragmentsList));
        this.viewPager.setCurrentItem(this.currentFragmentIndex);
    }

    /* access modifiers changed from: protected */
    public void afterViewsInitialization() {
    }

    /* access modifiers changed from: protected */
    public boolean isLandscapeMode() {
        Display display = getWindowManager().getDefaultDisplay();
        return display.getWidth() > display.getHeight();
    }

    /* access modifiers changed from: protected */
    public List<Fragment> loadTabFragments() {
        return null;
    }

    private void initFragmentsSliderAnimation() {
        this.fragmentsHandler = new Handler(getMainLooper()) {
            public void handleMessage(Message newMessage) {
                switch (newMessage.what) {
                    case 2:
                        CommonTabFragmentActivity.access$008(CommonTabFragmentActivity.this);
                        if (CommonTabFragmentActivity.this.fragmentsList.size() == CommonTabFragmentActivity.this.currentFragmentIndex) {
                            int unused = CommonTabFragmentActivity.this.currentFragmentIndex = 0;
                        }
                        CommonTabFragmentActivity.this.viewPager.setCurrentItem(CommonTabFragmentActivity.this.currentFragmentIndex);
                        CommonTabFragmentActivity.this.sendChangeTabMessage(AppConstants.START_MUSIC_DELAY, CommonTabFragmentActivity.this.fragmentsHandler, 2);
                        return;
                    default:
                        return;
                }
            }
        };
        sendChangeTabMessage(AppConstants.START_MUSIC_DELAY, this.fragmentsHandler, 2);
    }

    /* access modifiers changed from: protected */
    public void sendChangeTabMessage(int delayTime, Handler handler, int eventId) {
        Message message = handler.obtainMessage(eventId);
        handler.removeMessages(eventId);
        handler.sendMessageDelayed(message, (long) delayTime);
    }

    protected class SwipeyTabsPagerAdapter extends FragmentPagerAdapter implements SwipeyTabsAdapter {
        private final List<Fragment> fragments;

        public SwipeyTabsPagerAdapter(FragmentManager fm, List<Fragment> fragments2) {
            super(fm);
            this.fragments = fragments2;
        }

        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        public int getCount() {
            return this.fragments.size();
        }

        public View getTab(int position) {
            return new TextView(CommonTabFragmentActivity.this.getApplicationContext());
        }
    }
}
