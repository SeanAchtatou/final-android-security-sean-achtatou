package com.biznessapps.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.google.caching.ImageFetcher;
import com.biznessapps.utils.google.caching.ImageWorker;
import java.util.List;

public class GalleryPreviewActivity extends CommonFragmentActivity {
    private static final int DELAY_TIME = 5000;
    private static final int SWITCH_IMAGE_EVENT = 1;
    /* access modifiers changed from: private */
    public static List<GalleryData.Item> galleryItems;
    /* access modifiers changed from: private */
    public int currentPos = 0;
    private Handler eventHandler;
    /* access modifiers changed from: private */
    public final GestureDetector gestureListener = new GestureDetector(new ImageGestureListener());
    private TextView imageTextWebView;
    private ImageView imageView;
    private boolean isSlideShowMode;

    static /* synthetic */ int access$208(GalleryPreviewActivity x0) {
        int i = x0.currentPos;
        x0.currentPos = i + 1;
        return i;
    }

    static /* synthetic */ int access$210(GalleryPreviewActivity x0) {
        int i = x0.currentPos;
        x0.currentPos = i - 1;
        return i;
    }

    public static List<GalleryData.Item> getGalleryItems() {
        return galleryItems;
    }

    public static void setGalleryItems(List<GalleryData.Item> galleryItems2) {
        galleryItems = galleryItems2;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.imageView = (ImageView) findViewById(R.id.gallery_image_preview);
        this.imageTextWebView = (TextView) findViewById(R.id.gallery_text_preview);
        this.currentPos = getIntent().getIntExtra(AppConstants.GALLERY_CURRENT_POS_EXTRA, 0);
        setImage((GalleryData.Item) getIntent().getSerializableExtra(AppConstants.GALLERY_PREVIEW_EXTRA));
        ((ViewGroup) findViewById(R.id.gallery_preview_root)).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                GalleryPreviewActivity.this.gestureListener.onTouchEvent(event);
                return true;
            }
        });
        this.isSlideShowMode = getIntent().getBooleanExtra(AppConstants.SLIDESHOW_MODE_EXTRA, false);
        if (this.isSlideShowMode) {
            initSlideShow();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        stopSlideShow();
        if (this.imageView != null) {
            ImageWorker.cancelWork(this.imageView);
            this.imageView.setImageBitmap(null);
            this.imageView.setImageDrawable(null);
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.gallery_preview_layout;
    }

    /* access modifiers changed from: protected */
    public boolean hasNavigationMenu() {
        return false;
    }

    /* access modifiers changed from: private */
    public void setImage(GalleryData.Item item) {
        if (item != null) {
            String url = item.getFullUrl();
            ImageFetcher imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
            if (StringUtils.isEmpty(url)) {
                String url2 = getNewImageManager().addWidthParam(String.format(ServerConstants.GALLERY_PREVIEW_FORMAT, item.getId()), AppCore.getInstance().getDeviceWidth());
                ImageWorker.ImageLoadParams params = new ImageWorker.ImageLoadParams();
                params.setImageSrc(true);
                params.setUrl(url2);
                params.setView(this.imageView);
                params.setImageType(3);
                imageFetcher.loadImage(params);
                if (StringUtils.isNotEmpty(item.getInfo())) {
                    this.imageTextWebView.setVisibility(0);
                    this.imageTextWebView.setText(Html.fromHtml(item.getInfo().replace("<p>", "<p align=\"center|justify\">")));
                    return;
                }
                this.imageTextWebView.setVisibility(8);
                return;
            }
            this.imageTextWebView.setVisibility(8);
            String url3 = getNewImageManager().addWidthParam(url, AppCore.getInstance().getDeviceWidth());
            ImageWorker.ImageLoadParams params2 = new ImageWorker.ImageLoadParams();
            params2.setImageSrc(true);
            params2.setUrl(url3);
            params2.setView(this.imageView);
            params2.setImageType(3);
            imageFetcher.loadImage(params2);
        }
    }

    private class ImageGestureListener extends GestureDetector.SimpleOnGestureListener {
        private ImageGestureListener() {
        }

        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX() - e2.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f) {
                if (e2.getX() - e1.getX() <= 120.0f || Math.abs(velocityX) <= 200.0f || GalleryPreviewActivity.this.currentPos <= 0) {
                    return true;
                }
                GalleryPreviewActivity.access$210(GalleryPreviewActivity.this);
                GalleryPreviewActivity.this.setImage((GalleryData.Item) GalleryPreviewActivity.galleryItems.get(GalleryPreviewActivity.this.currentPos));
                return true;
            } else if (GalleryPreviewActivity.this.currentPos == -1 || GalleryPreviewActivity.this.currentPos + 1 >= GalleryPreviewActivity.galleryItems.size()) {
                return true;
            } else {
                GalleryPreviewActivity.access$208(GalleryPreviewActivity.this);
                GalleryPreviewActivity.this.setImage((GalleryData.Item) GalleryPreviewActivity.galleryItems.get(GalleryPreviewActivity.this.currentPos));
                return true;
            }
        }
    }

    private void initSlideShow() {
        this.eventHandler = new Handler(getMainLooper()) {
            public void handleMessage(Message newMessage) {
                switch (newMessage.what) {
                    case 1:
                        if (GalleryPreviewActivity.this.currentPos < GalleryPreviewActivity.galleryItems.size()) {
                            GalleryPreviewActivity.this.setImage((GalleryData.Item) GalleryPreviewActivity.galleryItems.get(GalleryPreviewActivity.this.currentPos));
                            GalleryPreviewActivity.this.sendChangeImageMessage(5000);
                            GalleryPreviewActivity.access$208(GalleryPreviewActivity.this);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        this.currentPos = 0;
        sendChangeImageMessage(5000);
    }

    private void stopSlideShow() {
        this.eventHandler = null;
    }

    /* access modifiers changed from: private */
    public void sendChangeImageMessage(int delayTime) {
        if (this.eventHandler != null) {
            Message message = this.eventHandler.obtainMessage(1);
            this.eventHandler.removeMessages(1);
            this.eventHandler.sendMessageDelayed(message, (long) delayTime);
        }
    }
}
