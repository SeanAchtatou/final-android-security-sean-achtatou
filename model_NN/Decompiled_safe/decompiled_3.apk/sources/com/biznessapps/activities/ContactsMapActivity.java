package com.biznessapps.activities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.LoadDataTaskExternal;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.SitesOverlay;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.LocationItem;
import com.biznessapps.model.LocationOpeningTime;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import java.util.ArrayList;
import java.util.List;

public class ContactsMapActivity extends CommonMapActivity {
    private static final int FIRST_INDEX = 0;
    private TextView addressTextView;
    private TextView callUsButton;
    private TextView commentWebView;
    private TextView emailUsButton;
    private ImageView getHereButton;
    private LoadDataTaskExternal.LoadDataRunnable handleInBgRunnable;
    private SitesOverlay itemizedOverlay;
    private String locationId;
    /* access modifiers changed from: private */
    public LocationItem locationInfo;
    private MapView map;
    private MapController mc;
    private TextView nameTextView;
    private LoadDataTaskExternal.LoadDataRunnable parseDataRunnable;
    private String tabId;
    private LoadDataTaskExternal.LoadDataRunnable updateControlsRunnable;
    private LoadDataTaskExternal.LoadDataRunnable useCachingRunnable;
    private TextView viewWebsiteButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
        initListeners();
        loadContent();
        initMap();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.contact_layout;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ViewUtils.setGlobalBackgroundColor(findViewById(R.id.contact_layout));
        loadBgUrl();
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    public ViewGroup getProgressBarContainer() {
        return (ViewGroup) findViewById(R.id.progress_bar_container);
    }

    /* JADX WARN: Type inference failed for: r10v0, types: [com.biznessapps.activities.ContactsMapActivity, android.app.Activity] */
    private void loadContent() {
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        this.locationId = getIntent().getStringExtra(AppConstants.LOCATION_ID);
        String requestUrl = String.format(ServerConstants.LOCATION_URL_FORMAT, cacher().getAppCode());
        if (StringUtils.isNotEmpty(this.locationId)) {
            requestUrl = requestUrl + String.format(ServerConstants.LOCATION_ID_PARAM, this.locationId);
        }
        if (StringUtils.isNotEmpty(this.tabId)) {
            requestUrl = requestUrl + String.format(ServerConstants.TAB_ID_PARAM, this.tabId);
        }
        LoadDataTaskExternal loadDataTask = new LoadDataTaskExternal(this, ViewUtils.getProgressBar(getApplicationContext()), new ArrayList<>());
        loadDataTask.setRequestUrl(requestUrl);
        this.useCachingRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                boolean unused = ContactsMapActivity.this.canUseCaching();
            }
        };
        loadDataTask.setCanUseCachingRunnable(this.useCachingRunnable);
        this.handleInBgRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                ContactsMapActivity.this.handleInBackground();
            }
        };
        loadDataTask.setHandleInBgRunnable(this.handleInBgRunnable);
        this.parseDataRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                boolean unused = ContactsMapActivity.this.tryParseData(getDataToParse());
            }
        };
        loadDataTask.setParseDataRunnable(this.parseDataRunnable);
        this.updateControlsRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                ContactsMapActivity.this.updateControlsWithData(getActivity());
            }
        };
        loadDataTask.setUpdateControlsRunnable(this.updateControlsRunnable);
        loadDataTask.launch();
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setItemId(getIntent().getStringExtra(AppConstants.LOCATION_ID));
        return data;
    }

    /* access modifiers changed from: private */
    public void handleInBackground() {
    }

    private void loadBgUrl() {
        String bgUrl = null;
        if (this.locationInfo != null) {
            bgUrl = this.locationInfo.getImage();
        }
        if (StringUtils.isNotEmpty(bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(bgUrl, findViewById(R.id.contact_layout));
        }
    }

    /* access modifiers changed from: private */
    public boolean tryParseData(String dataToParse) {
        boolean isNotEmptyList;
        List<LocationItem> locations = JsonParserUtils.parseLocation(dataToParse);
        if (locations == null || locations.isEmpty()) {
            isNotEmptyList = false;
        } else {
            isNotEmptyList = true;
        }
        if (isNotEmptyList) {
            this.locationInfo = locations.get(0);
            cacher().saveData(CachingConstants.LOCATIONS_MAP_PROPERTY + this.locationId, this.locationInfo);
        }
        if (this.locationInfo != null) {
            return true;
        }
        return false;
    }

    private void initViews() {
        ViewGroup buttonsContainer = (ViewGroup) findViewById(R.id.contact_buttons_container);
        if (AppCore.getInstance().isTablet()) {
            findViewById(R.id.call_us_image_separator).setVisibility(8);
            this.callUsButton = (TextView) findViewById(R.id.call_us_button);
            this.callUsButton.setVisibility(8);
            this.viewWebsiteButton = (TextView) findViewById(R.id.view_website_button);
            this.emailUsButton = (TextView) findViewById(R.id.email_us_button);
        } else {
            this.callUsButton = (TextView) findViewById(R.id.call_us_button);
            this.viewWebsiteButton = (TextView) findViewById(R.id.view_website_button);
            this.emailUsButton = (TextView) findViewById(R.id.email_us_button);
        }
        this.nameTextView = (TextView) findViewById(R.id.title_label);
        this.addressTextView = (TextView) findViewById(R.id.title_address_label);
        this.commentWebView = (TextView) findViewById(R.id.contact_comment_text);
        this.getHereButton = (ImageView) findViewById(R.id.get_here_icon);
        this.getHereButton.setVisibility(0);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.callUsButton.setTextColor(settings.getButtonTextColor());
        this.viewWebsiteButton.setTextColor(settings.getButtonTextColor());
        this.emailUsButton.setTextColor(settings.getButtonTextColor());
        this.nameTextView.setTextColor(settings.getFeatureTextColor());
        this.addressTextView.setTextColor(settings.getFeatureTextColor());
        this.commentWebView.setTextColor(settings.getFeatureTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), buttonsContainer.getBackground());
        CommonUtils.overrideImageColor(settings.getButtonTextColor(), this.callUsButton.getCompoundDrawables()[1]);
        CommonUtils.overrideImageColor(settings.getButtonTextColor(), this.viewWebsiteButton.getCompoundDrawables()[1]);
        CommonUtils.overrideImageColor(settings.getButtonTextColor(), this.emailUsButton.getCompoundDrawables()[1]);
    }

    private void initListeners() {
        this.callUsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ContactsMapActivity.this.locationInfo != null) {
                    ViewUtils.call(ContactsMapActivity.this.getApplicationContext(), ContactsMapActivity.this.locationInfo.getTelephone());
                }
            }
        });
        this.viewWebsiteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ContactsMapActivity.this.locationInfo != null) {
                    ViewUtils.openLinkInBrowser(ContactsMapActivity.this.getApplicationContext(), ContactsMapActivity.this.locationInfo.getWebsite());
                }
            }
        });
        this.emailUsButton.setOnClickListener(new View.OnClickListener() {
            /* JADX WARN: Type inference failed for: r0v2, types: [com.biznessapps.activities.ContactsMapActivity, android.app.Activity] */
            public void onClick(View v) {
                if (ContactsMapActivity.this.locationInfo != null) {
                    ViewUtils.email(ContactsMapActivity.this, new String[]{ContactsMapActivity.this.locationInfo.getEmail()}, null, null);
                }
            }
        });
        this.getHereButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (ContactsMapActivity.this.locationInfo != null) {
                    ViewUtils.openGoogleMap(ContactsMapActivity.this.getApplicationContext(), ContactsMapActivity.this.locationInfo.getLongitude(), ContactsMapActivity.this.locationInfo.getLatitude());
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateControlsWithData(Activity holdActivity) {
        loadBgUrl();
        if (this.locationInfo != null) {
            if (this.locationInfo == null || !StringUtils.isNotEmpty(this.locationInfo.getLatitude()) || !StringUtils.isNotEmpty(this.locationInfo.getLongitude())) {
                this.map.setVisibility(8);
            } else {
                GeoPoint p = new GeoPoint((int) (Double.parseDouble(this.locationInfo.getLatitude()) * 1000000.0d), (int) (Double.parseDouble(this.locationInfo.getLongitude()) * 1000000.0d));
                addGeoPoint(p, "", "");
                new ArrayList<>().add(p);
                this.mc.setZoom(15);
                this.map.setVisibility(0);
            }
            if (StringUtils.isNotEmpty(this.locationInfo.getCity())) {
                this.nameTextView.setText(this.locationInfo.getCity());
                this.nameTextView.setVisibility(0);
            } else {
                this.nameTextView.setVisibility(8);
            }
            if (StringUtils.isNotEmpty(getFullAddress(this.locationInfo))) {
                this.addressTextView.setText(getFullAddress(this.locationInfo));
                this.addressTextView.setVisibility(0);
            } else {
                this.addressTextView.setVisibility(8);
            }
            if (StringUtils.isNotEmpty(this.locationInfo.getComment())) {
                this.commentWebView.setText(Html.fromHtml(this.locationInfo.getComment()));
                this.commentWebView.setVisibility(0);
            } else {
                this.commentWebView.setVisibility(8);
            }
            TextView text = (TextView) findViewById(R.id.opening_times_header_label);
            TableLayout table = (TableLayout) findViewById(R.id.opening_times_layout);
            if (this.locationInfo.getOpeningTimes().size() > 0) {
                for (LocationOpeningTime time : this.locationInfo.getOpeningTimes()) {
                    TableRow row = new TableRow(holdActivity.getApplicationContext());
                    row.setLayoutParams(new TableRow.LayoutParams(-1, -2));
                    TextView openingTimeText = new TextView(holdActivity.getApplicationContext());
                    openingTimeText.setLayoutParams(new TableRow.LayoutParams(-2, -2));
                    openingTimeText.setTextColor(-16777216);
                    if (StringUtils.isNotEmpty(time.getDay())) {
                        openingTimeText.setText(time.getDay());
                    }
                    row.addView(openingTimeText);
                    TextView openingTimeText2 = new TextView(holdActivity.getApplicationContext());
                    openingTimeText2.setLayoutParams(new TableRow.LayoutParams(-2, -2));
                    openingTimeText2.setTextColor(-16777216);
                    if (StringUtils.isNotEmpty(time.getOpenFrom())) {
                        StringBuilder timeText = new StringBuilder(time.getOpenFrom());
                        if (StringUtils.isNotEmpty(time.getOpenTo())) {
                            timeText.append(" ").append(getString(R.string.to)).append(" ").append(time.getOpenTo());
                        }
                        openingTimeText2.setText(timeText);
                    }
                    row.addView(openingTimeText2);
                    table.addView(row);
                }
                text.setVisibility(0);
                table.setVisibility(0);
                return;
            }
            text.setVisibility(8);
            table.setVisibility(8);
            findViewById(R.id.open_hours_container).setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public boolean canUseCaching() {
        this.locationInfo = (LocationItem) cacher().getData(CachingConstants.LOCATIONS_MAP_PROPERTY + this.locationId);
        return this.locationInfo != null;
    }

    private void initMap() {
        this.map = findViewById(R.id.mapview);
        this.map.setBuiltInZoomControls(true);
        this.map.setSatellite(false);
        this.map.setStreetView(true);
        this.map.setClickable(true);
        this.mc = this.map.getController();
        initItemizedOverlay();
    }

    private void initItemizedOverlay() {
        this.itemizedOverlay = new SitesOverlay(getApplicationContext(), getMarker());
        this.map.getOverlays().add(this.itemizedOverlay);
        this.map.invalidate();
    }

    private Drawable getMarker() {
        Drawable marker = getResources().getDrawable(R.drawable.bubble);
        marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker.getIntrinsicHeight());
        return marker;
    }

    private void addGeoPoint(GeoPoint p, String title, String snippet) {
        this.mc.animateTo(p);
        this.itemizedOverlay.createNewOverlay(p, title, snippet);
    }

    private String getFullAddress(LocationItem location) {
        String address1 = location.getAddress1();
        String address2 = location.getAddress2();
        String city = location.getCity();
        String state = location.getState();
        String zip = location.getZip();
        String address = "";
        if (StringUtils.isNotEmpty(address1)) {
            address = String.format("%s %s", address, address1);
        }
        if (StringUtils.isNotEmpty(address2)) {
            address = String.format("%s, %s", address, address2);
        }
        if (StringUtils.isNotEmpty(city)) {
            address = String.format("%s, %s", address, city);
        }
        if (StringUtils.isNotEmpty(state)) {
            address = String.format("%s, %s", address, state);
        }
        if (!StringUtils.isNotEmpty(zip)) {
            return address;
        }
        return String.format("%s, %s", address, zip);
    }
}
