package com.biznessapps.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.widgets.wheel.DigitalWheelAdapter;
import com.biznessapps.widgets.wheel.WheelView;

public class ChooseTimerActivity extends CommonFragmentActivity {
    /* access modifiers changed from: private */
    public int numberOfHours;
    /* access modifiers changed from: private */
    public int numberOfMinutes;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final WheelView hours = (WheelView) findViewById(R.id.hour);
        hours.setViewAdapter(new DigitalWheelAdapter(this, 0, 23));
        final WheelView mins = (WheelView) findViewById(R.id.mins);
        mins.setViewAdapter(new DigitalWheelAdapter(this, 0, 59, "%02d"));
        mins.setCyclic(true);
        hours.setCurrentItem(0);
        mins.setCurrentItem(1);
        WheelView.OnStateChangedListener wheelListener = new WheelView.OnStateChangedListener() {
            public void onStateChanged(WheelView wheel) {
                int unused = ChooseTimerActivity.this.numberOfHours = hours.getCurrentItem();
                int unused2 = ChooseTimerActivity.this.numberOfMinutes = mins.getCurrentItem();
            }
        };
        hours.addChangingListener(wheelListener);
        mins.addChangingListener(wheelListener);
        ((Button) findViewById(R.id.set_timer_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent newIntent = new Intent();
                int unused = ChooseTimerActivity.this.numberOfHours = hours.getCurrentItem();
                int unused2 = ChooseTimerActivity.this.numberOfMinutes = mins.getCurrentItem();
                newIntent.putExtra(AppConstants.HOURS_EXTRA, ChooseTimerActivity.this.numberOfHours);
                newIntent.putExtra(AppConstants.MINUTES_EXTRA, ChooseTimerActivity.this.numberOfMinutes);
                ChooseTimerActivity.this.setResult(20, newIntent);
                ChooseTimerActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.stop_timer_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ChooseTimerActivity.this.setResult(21);
                ChooseTimerActivity.this.finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.choose_time_layout;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean hasNavigationMenu() {
        return false;
    }
}
