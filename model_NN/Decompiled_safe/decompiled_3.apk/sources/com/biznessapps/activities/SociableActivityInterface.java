package com.biznessapps.activities;

import com.biznessapps.components.SocialNetworkAccessor;

public interface SociableActivityInterface {
    void setSocialNetworkListener(SocialNetworkAccessor.SocialNetworkListener socialNetworkListener);
}
