package com.biznessapps.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import com.biznessapps.api.AppFragmentManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.lists.PodcastsListFragment;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SingleFragmentActivity extends CommonTabFragmentActivity {
    protected Map<String, Class<?>> activityFragmentMap = new HashMap();

    /* access modifiers changed from: protected */
    public Map<String, Class<?>> getActivityFragmentMap() {
        if (this.activityFragmentMap.isEmpty()) {
            this.activityFragmentMap.put(ServerConstants.CAR_FINDER_VIEW_CONTROLLER, CarFinderActivity.class);
            this.activityFragmentMap.put(ServerConstants.AROUND_US_VIEW_CONTROLLER, AroundUsActivity.class);
            this.activityFragmentMap.put(ServerConstants.FAN_WALL_NEW_VIEW_CONTROLLER, NewFanWallActivity.class);
            this.activityFragmentMap.put(AppConstants.CONTACTS_FRAGMENT, ContactsMapActivity.class);
            this.activityFragmentMap.put(ServerConstants.WEB_VIEW_CONTROLLER, WebSingleFragmentActivity.class);
            this.activityFragmentMap.put(ServerConstants.CONTENT_CHANGER_VIEW_CONTROLLER, WebSingleFragmentActivity.class);
            this.activityFragmentMap.put(ServerConstants.PROTECTED_VIEW_CONTROLLER, WebSingleFragmentActivity.class);
        }
        return this.activityFragmentMap;
    }

    /* access modifiers changed from: protected */
    public List<Fragment> loadFragments() {
        Class<?> className;
        Fragment singleFragment = AppFragmentManager.getFragmentByController(getIntent());
        if ((singleFragment instanceof CommonFragment) && StringUtils.isNotEmpty(((CommonFragment) singleFragment).getFragmentName()) && (className = getActivityFragmentMap().get(((CommonFragment) singleFragment).getFragmentName())) != null) {
            Intent intent = new Intent(getApplicationContext(), className);
            intent.putExtras(getIntent());
            startActivity(intent);
            finish();
        }
        if (singleFragment instanceof PodcastsListFragment) {
            this.hasPodcastTab = true;
        }
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(singleFragment);
        return fragments;
    }
}
