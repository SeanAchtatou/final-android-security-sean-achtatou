package com.biznessapps.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHandlers;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.api.UnModalAsyncTask;
import com.biznessapps.api.navigation.NavigationManager;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.MusicDelegate;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.images.NewImageManager;
import com.biznessapps.layout.R;
import com.biznessapps.model.Tab;
import com.biznessapps.player.MusicItem;
import com.biznessapps.player.PlayerService;
import com.biznessapps.player.PlayerStateListener;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.ViewUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class CommonFragmentActivity extends FragmentActivity implements AppConstants, SociableActivityInterface {
    protected static final long NO_TAB_DEFINED = -1;
    private List<BackPressed> backPressedListeners = new ArrayList();
    protected boolean hasPodcastTab = false;
    private boolean isTabMenuUsed;
    private SocialNetworkAccessor.SocialNetworkListener listener;
    protected MusicDelegate musicDelegate = new MusicDelegate();
    private ViewGroup musicRootLayout;
    protected NavigationManager navigManager;
    protected FrameLayout navigationContainer;
    private PlayerStateListener playerListener = new PlayerStateListener() {
        public void onStart(MusicItem item) {
            super.onStart(item);
            CommonFragmentActivity.this.musicDelegate.updateTrackInfo(item);
        }

        public void onStateChanged(int newState) {
            CommonFragmentActivity.this.musicDelegate.updatePlayerState(newState);
        }
    };
    protected ViewGroup progressBarContainer;
    protected ViewGroup rootLayout;
    private boolean showMusicPanelAlways;
    protected String tabId;

    public interface BackPressed {
        boolean onBackPressed();
    }

    public void displayMusicPanel() {
        if (this.musicRootLayout != null) {
            this.showMusicPanelAlways = true;
            this.musicRootLayout.setVisibility(0);
            this.musicDelegate.initViews(this.musicRootLayout, getApplicationContext());
        }
    }

    public MusicDelegate getMusicDelegate() {
        return this.musicDelegate;
    }

    public NavigationManager getNavigationManager() {
        return this.navigManager;
    }

    public ViewGroup getProgressBarContainer() {
        return this.progressBarContainer;
    }

    public void addBackPressedListener(BackPressed listenerToAdd) {
        this.backPressedListeners.add(listenerToAdd);
    }

    public void removeBackPressedListener(BackPressed listenerToRemove) {
        this.backPressedListeners.remove(listenerToRemove);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!AppCore.getInstance().isInitialized()) {
            AppCore.getInstance().init(getApplicationContext());
        }
        requestWindowFeature(1);
        AppHandlers.getUiHandler();
        onPreInit();
        setContentView(getLayoutId());
        this.progressBarContainer = (ViewGroup) findViewById(R.id.progress_bar_container);
        initNavigationManager();
        initMusicPanel();
    }

    private void initMusicPanel() {
        boolean showPanel;
        ViewGroup musicBottomContainer;
        if (!AppCore.getInstance().getAppSettings().isMusicOnTop() && (musicBottomContainer = (ViewGroup) findViewById(R.id.music_control_container_bottom)) != null) {
            musicBottomContainer.setVisibility(0);
            this.musicRootLayout = (ViewGroup) musicBottomContainer.findViewById(R.id.music_control_root);
        }
        if (this.musicRootLayout == null) {
            ViewGroup musicTopContainer = (ViewGroup) findViewById(R.id.music_control_container);
            if (musicTopContainer != null) {
                musicTopContainer.setVisibility(0);
                this.musicRootLayout = (ViewGroup) musicTopContainer.findViewById(R.id.music_control_root);
            }
            if (this.musicRootLayout == null) {
                this.musicRootLayout = (ViewGroup) findViewById(R.id.music_control_root);
            }
        }
        if (this.musicRootLayout != null) {
            if (AppCore.getInstance().getAppSettings().isMusicOnFront()) {
                this.musicDelegate.initViews(this.musicRootLayout, getApplicationContext());
                List<MusicItem> songsList = this.musicDelegate.getPlayerServiceAccessor().getSongs();
                if (songsList == null || songsList.isEmpty()) {
                    showPanel = false;
                } else {
                    showPanel = true;
                }
                if (showPanel) {
                    this.musicRootLayout.setVisibility(0);
                }
            } else {
                this.musicRootLayout.setVisibility(8);
            }
            PlayerService.addListener(this.playerListener);
        }
    }

    public void onDestroy() {
        PlayerService.removeListener(this.playerListener);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPreInit() {
    }

    public void onBackPressed() {
        boolean handleByActivity = true;
        for (BackPressed listener2 : this.backPressedListeners) {
            if (listener2.onBackPressed()) {
                handleByActivity = false;
            }
        }
        if (handleByActivity) {
            super.onBackPressed();
        }
    }

    public long getTabId() {
        if (getIntent() != null) {
            return getIntent().getLongExtra(AppConstants.TAB_ID, -1);
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean hasNavigationMenu() {
        return true;
    }

    public CachingManager cacher() {
        return AppCore.getInstance().getCachingManager();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.common_fragment_layout;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.isTabMenuUsed) {
            List<Tab> tabs = NavigationManager.getTabsItems();
            if (tabs != null && tabs.size() > 0) {
                this.navigManager.clearTabs();
                this.navigManager.updateTabs();
            }
            if (getTabId() == -1) {
                this.navigManager.setTabSelection(this.navigManager.getCurrentTabSelection());
            } else {
                this.navigManager.setTabSelection(getTabId());
            }
        }
        if (AppCore.getInstance().getAppSettings().isMusicOnFront() || this.showMusicPanelAlways) {
            if (this.hasPodcastTab) {
                this.musicRootLayout.setVisibility(8);
            } else {
                this.musicDelegate.onResume();
            }
        }
        boolean isAdsShown = ViewUtils.showAdsIfNeeded(this, (ViewGroup) findViewById(R.id.ads_layout_container), true);
        if (hasTitleBar()) {
            ViewUtils.showTitleBar((ViewGroup) findViewById(R.id.tab_title_container), getIntent(), isAdsShown);
        }
        ViewUtils.showMailingListPropmt(this);
        ViewUtils.showOfflineCachingPropmt(this);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if ((AppCore.getInstance().getAppSettings().isMusicOnFront() || this.showMusicPanelAlways) && !this.hasPodcastTab) {
            this.musicDelegate.onStop();
        }
        super.onStop();
    }

    private void initNavigationManager() {
        this.navigManager = new NavigationManager(this);
        this.navigationContainer = (FrameLayout) findViewById(R.id.bottom_navig_conrol_container);
        this.navigManager.addLayoutTo(this.navigationContainer);
        this.isTabMenuUsed = false;
        if (!this.isTabMenuUsed || !hasNavigationMenu()) {
            this.navigationContainer.setVisibility(8);
        } else {
            this.navigationContainer.setVisibility(0);
        }
    }

    /* access modifiers changed from: protected */
    public void loadData() {
        if (this != null) {
            preDataLoading(this);
            if (canUseCachedData()) {
                updateControlsWithData(this);
            } else {
                new LoadDataTask(this, ViewUtils.getProgressBar(getApplicationContext()), getViewsRef()).execute(new Map[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public NewImageManager getNewImageManager() {
        return AppCore.getInstance().getNewImageManager();
    }

    /* access modifiers changed from: protected */
    public BitmapDownloader getBitmapDownloader() {
        return AppCore.getInstance().getNewImageManager().getBitmapDownloader();
    }

    /* access modifiers changed from: protected */
    public List<WeakReference<View>> getViewsRef() {
        return new ArrayList<>();
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return "";
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void handleInBackground() {
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
    }

    /* access modifiers changed from: protected */
    public boolean showToastIfDataFailed() {
        return true;
    }

    private class LoadDataTask extends UnModalAsyncTask<Map<String, String>, Void, Boolean> {
        private long loadingTime;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object[] x0) {
            return doInBackground((Map<String, String>[]) ((Map[]) x0));
        }

        public LoadDataTask(Activity activity, View progressBar, List<WeakReference<View>> refOfViews) {
            super(activity, progressBar, refOfViews);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Map<String, String>... params) {
            String url = CommonFragmentActivity.this.getRequestUrl();
            if (AppCore.getInstance().isTablet()) {
                url = url + ServerConstants.TABLET_PARAM;
            }
            boolean isCorrectData = CommonFragmentActivity.this.tryParseData(DataSource.getInstance().getData(url, params));
            if (this.activity != null && isCorrectData) {
                CommonFragmentActivity.this.handleInBackground();
            }
            return Boolean.valueOf(isCorrectData);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.loadingTime = System.currentTimeMillis();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean isCorrectData) {
            super.onPostExecute((Object) isCorrectData);
            if (isCorrectData.booleanValue()) {
                if (this.activity != null && (this.activity instanceof CommonFragmentActivity)) {
                    ((CommonFragmentActivity) this.activity).getProgressBarContainer().removeAllViews();
                    CommonFragmentActivity.this.updateControlsWithData(this.activity);
                }
            } else if (this.activity != null && CommonFragmentActivity.this.showToastIfDataFailed()) {
                Toast.makeText(this.activity.getApplicationContext(), R.string.data_loading_failure, 1).show();
            }
            if (this.activity != null && CommonFragmentActivity.this.getIntent() != null) {
                this.loadingTime = System.currentTimeMillis() - this.loadingTime;
                CommonUtils.sendTimingEvent(this.activity.getApplicationContext(), CommonFragmentActivity.this.getIntent().getStringExtra(AppConstants.TAB_FRAGMENT_EXTRA), this.loadingTime);
            }
        }

        /* access modifiers changed from: protected */
        public void placeProgressBar() {
            if (this.activity != null && (this.activity instanceof CommonFragmentActivity)) {
                ((CommonFragmentActivity) this.activity).getProgressBarContainer().addView(this.progressBar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (this.listener != null) {
            this.listener.onActivityResult(this, requestCode, resultCode, data);
        }
    }

    public void setSocialNetworkListener(SocialNetworkAccessor.SocialNetworkListener listener2) {
        this.listener = listener2;
    }
}
