package com.biznessapps.activities;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.biznessapps.activities.CommonTabFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.navigation.NavigationManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CustomizableViewPager;
import com.biznessapps.fragments.single.HomePartFragment;
import com.biznessapps.fragments.single.ImageFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.App;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.Tab;
import com.biznessapps.player.MusicPlayer;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.List;

public class HomeScreenActivity extends CommonTabFragmentActivity {
    private static final int COLUMNS_LIMIT = 6;
    private static final int DEFAULT_COLUMNS_COUNT = 5;
    private static final int DEFAULT_ROWS_COUNT = 1;
    private static final int ROWS_LIMIT = 4;
    private int columnCount;
    /* access modifiers changed from: private */
    public int currentTabIndex;
    private boolean hasManyImages;
    private boolean hasMoreButtonNavigation;
    private ImageView headerView;
    private int layoutId;
    private App newDesignApp;
    private ViewGroup rootView;
    private int rowCount;
    /* access modifiers changed from: private */
    public ViewGroup tabCircles;
    /* access modifiers changed from: private */
    public Handler tabEventHandler;
    private List<Tab> tabs;

    static /* synthetic */ int access$110(HomeScreenActivity x0) {
        int i = x0.currentTabIndex;
        x0.currentTabIndex = i - 1;
        return i;
    }

    /* access modifiers changed from: protected */
    public void onPreInit() {
        super.onPreInit();
        this.newDesignApp = (App) AppCore.getInstance().getCachingManager().getData(CachingConstants.APP_INFO_PROPERTY);
        this.hasMoreButtonNavigation = AppCore.getInstance().getAppSettings().hasMoreButtonNavigation();
        defineRowLayout();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return this.layoutId;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        super.initViews();
        this.rootView = (ViewGroup) findViewById(R.id.home_screen_container);
        this.headerView = (ImageView) findViewById(R.id.home_screen_header);
        this.tabViewPager = (CustomizableViewPager) findViewById(R.id.tab_viewpager);
        this.tabViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageSelected(int selectedPage) {
                if (HomeScreenActivity.this.tabCircles != null) {
                    int childCount = HomeScreenActivity.this.tabCircles.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        ImageView child = (ImageView) HomeScreenActivity.this.tabCircles.getChildAt(i);
                        if (i == selectedPage) {
                            child.setImageResource(R.drawable.selected_circle);
                        } else {
                            child.setImageResource(R.drawable.unselected_circle);
                        }
                    }
                }
            }

            public void onPageScrollStateChanged(int arg0) {
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }
        });
        if (this.tabFragmentsList == null) {
            this.tabViewPager.setVisibility(8);
        } else {
            this.tabViewPager.setAdapter(new CommonTabFragmentActivity.SwipeyTabsPagerAdapter(getSupportFragmentManager(), this.tabFragmentsList));
            this.tabViewPager.setVisibility(0);
            initTabStartupAnimation();
        }
        setRootBackground();
        setHeaderBackground();
        defineNavigation();
        defineMessageShortLink();
        HomeScreenHelper.initOptionsViews(this, this.rootView);
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.musicDelegate != null && this.musicDelegate.isActive()) {
            this.musicDelegate.hideCollapseButton();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (MusicPlayer.getInstance().getServiceAccessor() != null) {
            MusicPlayer.getInstance().getServiceAccessor().stop();
        }
    }

    /* access modifiers changed from: protected */
    public List<Fragment> loadFragments() {
        List<Fragment> fragments = new ArrayList<>();
        this.hasManyImages = this.newDesignApp != null && this.newDesignApp.hasManyImages();
        if (this.hasManyImages) {
            List<String> imagesUrl = this.newDesignApp.getImagesInOrder();
            for (String url : imagesUrl) {
                ImageFragment imageFragment = new ImageFragment();
                imageFragment.setUrl(url);
                imageFragment.setImageOrderIndex(imagesUrl.indexOf(url));
                imageFragment.setRetainInstance(true);
                fragments.add(imageFragment);
            }
        } else {
            ImageFragment imageFragment2 = new ImageFragment();
            imageFragment2.setRetainInstance(true);
            fragments.add(imageFragment2);
        }
        return fragments;
    }

    /* access modifiers changed from: protected */
    public List<Fragment> loadTabFragments() {
        List<Fragment> fragments = new ArrayList<>();
        this.tabs = NavigationManager.getTabsItems();
        if (this.hasMoreButtonNavigation) {
            HomePartFragment hp = new HomePartFragment();
            hp.setTabs(this.tabs);
            hp.setRowCount(this.rowCount);
            hp.setColumnCount(this.columnCount);
            hp.setHasMoreButtonNavigation(this.hasMoreButtonNavigation);
            hp.setRetainInstance(true);
            fragments.add(hp);
        } else {
            int fragmentsNumber = 0;
            int tabCount = this.tabs.size();
            while (tabCount > 0) {
                fragmentsNumber++;
                tabCount -= this.rowCount * this.columnCount;
            }
            if (fragmentsNumber > 0) {
                this.tabCircles = (ViewGroup) findViewById(R.id.tabs_navigation_container);
                this.tabCircles.setVisibility(0);
            }
            for (int i = 0; i < fragmentsNumber; i++) {
                HomePartFragment hp2 = new HomePartFragment();
                int highLimit = (i + 1) * this.rowCount * this.columnCount;
                if (highLimit > this.tabs.size()) {
                    highLimit = this.tabs.size();
                }
                hp2.setTabs(new ArrayList(this.tabs.subList(this.rowCount * this.columnCount * i, highLimit)));
                hp2.setRowCount(this.rowCount);
                hp2.setColumnCount(this.columnCount);
                hp2.setRetainInstance(true);
                fragments.add(hp2);
                ImageView circleImage = (ImageView) ViewUtils.loadLayout(getApplicationContext(), R.layout.circle_image);
                circleImage.setPadding(4, 0, 4, 0);
                if (i == 0) {
                    circleImage.setImageResource(R.drawable.selected_circle);
                } else {
                    circleImage.setImageResource(R.drawable.unselected_circle);
                }
                this.tabCircles.addView(circleImage);
            }
        }
        return fragments;
    }

    private void defineMessageShortLink() {
        ImageView messageLinkIcon = (ImageView) findViewById(R.id.message_link_icon);
        final AppSettings appSettings = AppCore.getInstance().getAppSettings();
        messageLinkIcon.setVisibility(appSettings.isMessageIconUsed() ? 0 : 8);
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) messageLinkIcon.getLayoutParams();
        if (appSettings.isMessageIconLeft()) {
            lp.gravity = 51;
        } else {
            lp.gravity = 53;
        }
        messageLinkIcon.setLayoutParams(lp);
        messageLinkIcon.getBackground().setAlpha((appSettings.getMessageIconOpacity() * MotionEventCompat.ACTION_MASK) / 100);
        messageLinkIcon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Intent intent = new Intent(HomeScreenActivity.this.getApplicationContext(), SingleFragmentActivity.class);
                intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.MESSAGE_VIEW_CONTROLLER);
                intent.putExtra(AppConstants.TAB_SPECIAL_ID, appSettings.getMessateLinkedTab());
                HomeScreenActivity.this.startActivity(intent);
            }
        });
    }

    private void defineNavigation() {
        AppSettings appSettings = AppCore.getInstance().getAppSettings();
        ViewGroup horizontalNavContainer = (ViewGroup) findViewById(R.id.horisontal_tab_container);
        if (appSettings.getNavigationMenuType() == 1) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1, -2);
            params.setMargins(0, 0, 0, 0);
            params.gravity = 80;
            ((ViewGroup) findViewById(R.id.music_control_container)).setLayoutParams(params);
            FrameLayout.LayoutParams params2 = new FrameLayout.LayoutParams(-1, -2);
            params2.setMargins(0, 0, 0, 0);
            params2.gravity = 48;
            horizontalNavContainer.setLayoutParams(params2);
            horizontalNavContainer.setVisibility(0);
        } else if (appSettings.getNavigationMenuType() == 0) {
            ViewGroup leftGrid = (ViewGroup) findViewById(R.id.myGrid);
            leftGrid.setVisibility(0);
            addRow(leftGrid, this.tabs, this.hasMoreButtonNavigation);
            horizontalNavContainer.setVisibility(8);
        } else if (appSettings.getNavigationMenuType() == 2) {
            ViewGroup rightGrid = (ViewGroup) findViewById(R.id.myGrid2);
            rightGrid.setVisibility(0);
            addRow(rightGrid, this.tabs, this.hasMoreButtonNavigation);
            horizontalNavContainer.setVisibility(8);
        }
    }

    private void addRow(ViewGroup row, List<Tab> rowsTab, boolean useMoreOption) {
        NavigationManager navigManagerRow = new NavigationManager(this, 2);
        if (useMoreOption) {
            navigManagerRow.setSideTabLimit();
        } else {
            navigManagerRow.setUseUnlimitTabCount(true);
        }
        navigManagerRow.setRowTabsItems(rowsTab);
        navigManagerRow.addLayoutTo(row);
        navigManagerRow.updateTabs();
        navigManagerRow.resetTabsSelection();
    }

    private void defineRowLayout() {
        AppSettings settings = AppCore.getInstance().getAppSettings();
        if (this.hasMoreButtonNavigation) {
            this.rowCount = 1;
            this.columnCount = 5;
            this.layoutId = R.layout.home_screen_layout_1row;
            return;
        }
        this.rowCount = settings.getRows();
        this.columnCount = settings.getCols();
        if (settings.getNavigationMenuType() != 3) {
            this.rowCount = 1;
        }
        if (this.rowCount <= 0 || this.rowCount > 4) {
            this.rowCount = 1;
        }
        if (this.columnCount <= 0 || this.columnCount > 6) {
            this.columnCount = 5;
        }
        if (this.rowCount == 2) {
            this.layoutId = R.layout.home_screen_layout_2row;
        } else if (this.rowCount == 3) {
            this.layoutId = R.layout.home_screen_layout_3row;
        } else if (this.rowCount == 4) {
            this.layoutId = R.layout.home_screen_layout_4row;
        } else {
            this.layoutId = R.layout.home_screen_layout_1row;
        }
    }

    private void initTabStartupAnimation() {
        this.currentTabIndex = this.tabFragmentsList.size() - 1;
        this.tabViewPager.setCurrentItem(this.currentTabIndex);
        this.tabEventHandler = new Handler(getMainLooper()) {
            public void handleMessage(Message newMessage) {
                switch (newMessage.what) {
                    case 1:
                        if (HomeScreenActivity.this.currentTabIndex > 0) {
                            HomeScreenActivity.access$110(HomeScreenActivity.this);
                            HomeScreenActivity.this.tabViewPager.setCurrentItem(HomeScreenActivity.this.currentTabIndex);
                            HomeScreenActivity.this.sendChangeTabMessage(100, HomeScreenActivity.this.tabEventHandler, 1);
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        sendChangeTabMessage(AppConstants.START_MUSIC_DELAY, this.tabEventHandler, 1);
    }

    private void setHeaderBackground() {
        String headerSrc = AppCore.getInstance().getAppSettings().getHeaderSrc();
        if (StringUtils.isNotEmpty(headerSrc)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadAppImage(headerSrc, this.headerView);
        } else {
            this.headerView.setVisibility(8);
        }
    }

    private void setRootBackground() {
        boolean hasSlideImages = true;
        AppSettings settings = AppCore.getInstance().getAppSettings();
        ViewUtils.setGlobalBackgroundColor(this.rootView);
        List<String> imagesUrls = cacher().getAppInfo().getImagesInOrder();
        if (imagesUrls == null || imagesUrls.size() <= 1) {
            hasSlideImages = false;
        }
        if (settings.getAnimationMode() == 2 && hasSlideImages) {
            int deviceWidth = (int) (((float) AppCore.getInstance().getDeviceWidth()) * 0.85f);
            int deviceHeight = (int) (((float) AppCore.getInstance().getDeviceHeight()) * 0.85f);
        }
        if (this.newDesignApp != null && this.newDesignApp.getImageUrl() != null) {
            try {
                AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(getNewImageManager().addWidthParam(this.newDesignApp.getImageUrl(), AppCore.getInstance().getDeviceWidth()), this.rootView);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
