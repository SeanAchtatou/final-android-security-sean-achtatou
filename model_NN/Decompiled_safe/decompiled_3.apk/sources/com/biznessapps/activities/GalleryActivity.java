package com.biznessapps.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.GalleryAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.utils.google.caching.ImageFetcher;
import com.biznessapps.utils.google.caching.ImageGridAdapter;
import com.biznessapps.widgets.TabGallery;

public class GalleryActivity extends CommonFragmentActivity {
    /* access modifiers changed from: private */
    public int currentPosition;
    /* access modifiers changed from: private */
    public GalleryData galleryData;
    private AbstractAdapter<GalleryData.Item> imageAdapter;
    /* access modifiers changed from: private */
    public ImageGridAdapter mAdapter;
    private ImageFetcher mImageFetcher;
    /* access modifiers changed from: private */
    public int mImageThumbSize;
    /* access modifiers changed from: private */
    public int mImageThumbSpacing;
    private ImageButton slideshowButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.galleryData = (GalleryData) getIntent().getSerializableExtra(AppConstants.GALLERY_DATA_EXTRA);
        this.slideshowButton = (ImageButton) findViewById(R.id.slideshow_button);
        this.slideshowButton.setVisibility(0);
        this.slideshowButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                GalleryActivity.this.showPreviewItems();
            }
        });
        if (!this.galleryData.isUseCoverflow()) {
            this.mImageThumbSize = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_size);
            this.mImageThumbSpacing = getResources().getDimensionPixelSize(R.dimen.image_thumbnail_spacing);
            this.mImageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
            this.mAdapter = AppCore.getInstance().getImageFetcherAccessor().createImageGridAdapter(this, this.mImageFetcher, this.galleryData.getItemsUrls());
            final GridView galleryView = (GridView) findViewById(R.id.gallery_view);
            galleryView.setAdapter((ListAdapter) this.mAdapter);
            galleryView.setSelection(this.currentPosition);
            galleryView.setOnItemClickListener(getOnGridItemClickListener());
            galleryView.setVisibility(0);
            galleryView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    int numColumns;
                    if (GalleryActivity.this.mAdapter.getNumColumns() == 0 && (numColumns = (int) Math.floor((double) (galleryView.getWidth() / (GalleryActivity.this.mImageThumbSize + GalleryActivity.this.mImageThumbSpacing)))) > 0) {
                        int columnWidth = (galleryView.getWidth() / numColumns) - GalleryActivity.this.mImageThumbSpacing;
                        GalleryActivity.this.mAdapter.setNumColumns(numColumns);
                        GalleryActivity.this.mAdapter.setItemHeight(columnWidth);
                    }
                }
            });
        }
        ViewUtils.setGlobalBackgroundColor(findViewById(R.id.gallery_root));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (this.galleryData.isUseCoverflow()) {
            TabGallery coverGallery = (TabGallery) findViewById(R.id.gallery_view_coverflow);
            this.imageAdapter = getCoverFlowAdapter(getApplicationContext());
            coverGallery.setAdapter((SpinnerAdapter) this.imageAdapter);
            coverGallery.setSelection(this.currentPosition);
            coverGallery.setOnItemClickListener(getOnCoverflowItemClickListener());
            coverGallery.setVisibility(0);
        } else if (this.mImageFetcher != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mImageFetcher != null) {
            this.mImageFetcher.flushCache();
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.gallery_layout;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return true;
    }

    private AdapterView.OnItemClickListener getOnCoverflowItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                if (position >= 0 && position < GalleryActivity.this.galleryData.getItems().size()) {
                    int unused = GalleryActivity.this.currentPosition = position;
                    GalleryData.Item currentItem = GalleryActivity.this.galleryData.getItems().get(position);
                    if (currentItem.getInfo() != null) {
                        GalleryActivity.this.openWebView(currentItem.getInfo());
                    }
                }
            }
        };
    }

    private AdapterView.OnItemClickListener getOnGridItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long position) {
                if (position >= 0 && position < ((long) GalleryActivity.this.galleryData.getItems().size())) {
                    int unused = GalleryActivity.this.currentPosition = (int) position;
                    Intent previewIntent = new Intent(GalleryActivity.this.getApplicationContext(), GalleryPreviewActivity.class);
                    GalleryPreviewActivity.setGalleryItems(GalleryActivity.this.galleryData.getItems());
                    previewIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.GALLERY_PREVIEW_FRAGMENT);
                    previewIntent.putExtra(AppConstants.GALLERY_PREVIEW_EXTRA, GalleryActivity.this.galleryData.getItems().get(GalleryActivity.this.currentPosition));
                    previewIntent.putExtra(AppConstants.GALLERY_CURRENT_POS_EXTRA, position);
                    GalleryActivity.this.startActivity(previewIntent);
                }
            }
        };
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void showPreviewItems() {
        Intent previewIntent = new Intent(getApplicationContext(), GalleryPreviewActivity.class);
        GalleryPreviewActivity.setGalleryItems(this.galleryData.getItems());
        previewIntent.putExtra(AppConstants.SLIDESHOW_MODE_EXTRA, true);
        startActivity(previewIntent);
    }

    /* access modifiers changed from: private */
    public void openWebView(String webData) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        if (StringUtils.isNotEmpty(webData)) {
            intent.putExtra(AppConstants.WEB_DATA, webData);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
            intent.putExtra(AppConstants.TAB_ID, getIntent().getLongExtra(AppConstants.TAB_ID, 0));
            intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
            startActivity(intent);
        }
    }

    private AbstractAdapter<GalleryData.Item> getCoverFlowAdapter(Context appContext) {
        return new GalleryAdapter(appContext, this.galleryData.getItems(), R.layout.coverflow_item_layout, true);
    }
}
