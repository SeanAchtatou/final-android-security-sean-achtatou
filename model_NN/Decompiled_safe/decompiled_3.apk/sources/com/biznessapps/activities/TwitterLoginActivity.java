package com.biznessapps.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.utils.ViewUtils;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class TwitterLoginActivity extends Activity implements AppConstants {
    /* access modifiers changed from: private */
    public static String CALLBACK_URL = "http://www.google.com/callback";
    public static final String DATA_TO_TWEET = "data_to_tweet";
    private static String OAUTH_VERIFIER = "oauth_verifier";
    public static final String TWITTER_CONSUMER_KEY = "ibeMh2JAmmQw09B1nfap5Q";
    public static final String TWITTER_CONSUMER_SECRET = "dkomjgXm50XtNmWDn0FhJJpswGvdfIPqfYwfxqMar38";
    /* access modifiers changed from: private */
    public static RequestToken requestToken;
    /* access modifiers changed from: private */
    public static Twitter twitter;
    /* access modifiers changed from: private */
    public ProgressDialog progress;
    /* access modifiers changed from: private */
    public WebView twitterOauthWebview;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.twitter_login_view);
        this.twitterOauthWebview = (WebView) findViewById(R.id.twitter_oauth_internal_webview);
        this.twitterOauthWebview.setWebViewClient(getWebViewClient());
        this.progress = ViewUtils.getProgressDialog(this);
        this.progress.show();
        new Thread(new Runnable() {
            public void run() {
                try {
                    Twitter unused = TwitterLoginActivity.twitter = new TwitterFactory().getInstance();
                    TwitterLoginActivity.twitter.setOAuthConsumer("ibeMh2JAmmQw09B1nfap5Q", "dkomjgXm50XtNmWDn0FhJJpswGvdfIPqfYwfxqMar38");
                    RequestToken unused2 = TwitterLoginActivity.requestToken = TwitterLoginActivity.twitter.getOAuthRequestToken(TwitterLoginActivity.CALLBACK_URL);
                    TwitterLoginActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            ViewUtils.plubWebView(TwitterLoginActivity.this.twitterOauthWebview);
                            TwitterLoginActivity.this.twitterOauthWebview.loadUrl(TwitterLoginActivity.requestToken.getAuthorizationURL());
                        }
                    });
                } catch (Exception e) {
                    TwitterLoginActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            if (TwitterLoginActivity.this.progress != null) {
                                TwitterLoginActivity.this.progress.dismiss();
                            }
                            ViewUtils.showShortToast(TwitterLoginActivity.this, R.string.twitting_failure);
                            TwitterLoginActivity.this.finish();
                        }
                    });
                }
            }
        }).start();
    }

    private WebViewClient getWebViewClient() {
        return new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith(TwitterLoginActivity.CALLBACK_URL)) {
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            public void onPageStarted(WebView view, final String url, Bitmap favicon) {
                if (url.startsWith(TwitterLoginActivity.CALLBACK_URL)) {
                    new Thread(new Runnable() {
                        public void run() {
                            TwitterLoginActivity.this.handleAuthResponse(url);
                        }
                    }).start();
                } else {
                    super.onPageStarted(view, url, favicon);
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (TwitterLoginActivity.this.progress != null && TwitterLoginActivity.this.progress.isShowing()) {
                        TwitterLoginActivity.this.progress.dismiss();
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void handleAuthResponse(String url) {
        try {
            AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, Uri.parse(url).getQueryParameter(OAUTH_VERIFIER));
            cacher().setTwitterOauthSecret(accessToken.getTokenSecret());
            cacher().setTwitterOauthToken(accessToken.getToken());
            cacher().setTwitterUid("" + accessToken.getUserId());
            cacher().setTwitterUserName(accessToken.getScreenName());
            ViewUtils.tweetAppName(getApplicationContext());
            finish();
        } catch (Exception e) {
            ViewUtils.showShortToast(getApplicationContext(), R.string.twitting_failure);
            finish();
        }
    }

    private CachingManager cacher() {
        return AppCore.getInstance().getCachingManager();
    }
}
