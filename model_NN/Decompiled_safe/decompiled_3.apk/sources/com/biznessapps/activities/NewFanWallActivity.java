package com.biznessapps.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.adapters.ImageAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.LoadDataTaskExternal;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.fanwall.NewFanWallAdapter;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.MapUtils;
import com.biznessapps.layout.views.map.MarkerOverlay;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import java.util.ArrayList;
import java.util.List;

public class NewFanWallActivity extends CommonMapActivity {
    private List<FanWallComment> comments;
    /* access modifiers changed from: private */
    public ViewGroup commentsContainer;
    private ListView commentsListView;
    /* access modifiers changed from: private */
    public TextView commentsTitleView;
    /* access modifiers changed from: private */
    public Location currentLocation;
    BitmapDownloader downloader = AppCore.getInstance().getNewImageManager().getBitmapDownloader();
    private GridView galleryView;
    private LoadDataTaskExternal.LoadDataRunnable handleInBgRunnable;
    /* access modifiers changed from: private */
    public boolean isNearByUsed;
    /* access modifiers changed from: private */
    public ViewGroup mapContainer;
    /* access modifiers changed from: private */
    public ImageView mapPointerView;
    /* access modifiers changed from: private */
    public TextView mapTitleView;
    private Button nearByButton;
    private Drawable nearByDrawable;
    private LoadDataTaskExternal.LoadDataRunnable parseDataRunnable;
    private List<String> photos = new ArrayList();
    /* access modifiers changed from: private */
    public ViewGroup photosContainer;
    /* access modifiers changed from: private */
    public ImageView photosPointerView;
    /* access modifiers changed from: private */
    public TextView photosTitleView;
    private Button recentsButton;
    private Drawable recentsDrawable;
    private ViewGroup root;
    /* access modifiers changed from: private */
    public State state = new State();
    private String tabId;
    private LoadDataTaskExternal.LoadDataRunnable updateControlsRunnable;
    private LoadDataTaskExternal.LoadDataRunnable useCachingRunnable;
    /* access modifiers changed from: private */
    public ImageView wallPointerView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initControls();
        initListeners();
        loadContent();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.new_fan_wall_layout;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (resultCode) {
            case 4:
                this.state.setNeedToReload(true);
                loadContent();
                return;
            default:
                return;
        }
    }

    public ViewGroup getProgressBarContainer() {
        return (ViewGroup) findViewById(R.id.progress_bar_container);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AppCore.getInstance().getLocationFinder().startSearching();
        loadBgUrl();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        AppCore.getInstance().getLocationFinder().stopSearching();
        clearBackground();
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    private void initControls() {
        this.root = (ViewGroup) findViewById(R.id.root_container);
        this.commentsContainer = (ViewGroup) findViewById(R.id.fan_wall_comments_container);
        this.mapContainer = (ViewGroup) findViewById(R.id.fan_wall_map_container);
        this.photosContainer = (ViewGroup) findViewById(R.id.fan_wall_photos_container);
        this.commentsTitleView = (TextView) findViewById(R.id.messages_textview);
        this.mapTitleView = (TextView) findViewById(R.id.map_textview);
        this.photosTitleView = (TextView) findViewById(R.id.photos_textview);
        this.wallPointerView = (ImageView) findViewById(R.id.wall_pointer);
        this.mapPointerView = (ImageView) findViewById(R.id.map_pointer);
        this.photosPointerView = (ImageView) findViewById(R.id.photos_pointer);
        this.recentsButton = (Button) findViewById(R.id.recents_button);
        this.nearByButton = (Button) findViewById(R.id.near_by_button);
        updateButtonsState(this.isNearByUsed);
        AppSettings settings = AppCore.getInstance().getAppSettings();
        ((ViewGroup) findViewById(R.id.fan_wall_button_container)).setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.commentsTitleView.setTextColor(ViewUtils.getColor(settings.getNavigBarTextColor()));
        this.mapTitleView.setTextColor(ViewUtils.getColor(settings.getNavigBarTextColor()));
        this.photosTitleView.setTextColor(ViewUtils.getColor(settings.getNavigBarTextColor()));
        this.wallPointerView.setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.mapPointerView.setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.photosPointerView.setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.commentsListView = (ListView) findViewById(R.id.comments_list_view);
        ViewUtils.setGlobalBackgroundColor(this.commentsListView);
        this.galleryView = (GridView) findViewById(R.id.gallery_view);
        ViewUtils.setGlobalBackgroundColor(this.galleryView);
        AppCore.getInstance().getLocationFinder().addLocationListener(getLocationListener());
        SocialNetworkAccessor socialAccessor = new SocialNetworkAccessor(this, this.root);
        this.listener = new SocialNetworkAccessor.SocialNetworkListener(socialAccessor) {
            public void onAuthSucceed() {
                NewFanWallActivity.this.addComment();
            }
        };
        setSocialNetworkListener(this.listener);
        socialAccessor.addAuthorizationListener(this.listener);
    }

    private void initListeners() {
        this.commentsTitleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (NewFanWallActivity.this.state.isDataLoaded()) {
                    NewFanWallActivity.this.activateControls(NewFanWallActivity.this.commentsContainer, NewFanWallActivity.this.commentsTitleView, NewFanWallActivity.this.wallPointerView);
                    NewFanWallActivity.this.openCommentsContainer();
                }
            }
        });
        this.mapTitleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (NewFanWallActivity.this.state.isDataLoaded()) {
                    NewFanWallActivity.this.activateControls(NewFanWallActivity.this.mapContainer, NewFanWallActivity.this.mapTitleView, NewFanWallActivity.this.mapPointerView);
                    NewFanWallActivity.this.openMapContainer();
                }
            }
        });
        this.photosTitleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (NewFanWallActivity.this.state.isDataLoaded()) {
                    NewFanWallActivity.this.activateControls(NewFanWallActivity.this.photosContainer, NewFanWallActivity.this.photosTitleView, NewFanWallActivity.this.photosPointerView);
                    NewFanWallActivity.this.openPhotosContainer();
                }
            }
        });
        this.nearByButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                boolean unused = NewFanWallActivity.this.isNearByUsed = true;
                NewFanWallActivity.this.updateButtonsState(NewFanWallActivity.this.isNearByUsed);
                NewFanWallActivity.this.loadContent();
            }
        });
        this.recentsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                boolean unused = NewFanWallActivity.this.isNearByUsed = false;
                NewFanWallActivity.this.updateButtonsState(NewFanWallActivity.this.isNearByUsed);
                NewFanWallActivity.this.loadContent();
            }
        });
    }

    /* access modifiers changed from: private */
    public void activateControls(ViewGroup tabToActivate, TextView viewToActivite, ImageView pointer) {
        this.commentsContainer.setVisibility(8);
        this.mapContainer.setVisibility(8);
        this.photosContainer.setVisibility(8);
        tabToActivate.setVisibility(0);
        this.wallPointerView.setVisibility(8);
        this.mapPointerView.setVisibility(8);
        this.photosPointerView.setVisibility(8);
        pointer.setVisibility(0);
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [android.app.Activity, com.biznessapps.activities.NewFanWallActivity] */
    /* access modifiers changed from: private */
    public void loadContent() {
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        String requestUrl = String.format(ServerConstants.COMMENT_LIST_FORMAT, cacher().getAppCode(), "", this.tabId);
        if (this.isNearByUsed) {
            if (this.currentLocation != null) {
                requestUrl = requestUrl + "&latitude=" + this.currentLocation.getLatitude() + "&longitude=" + this.currentLocation.getLongitude();
            } else {
                Toast.makeText(getApplicationContext(), R.string.location_not_defined, 1).show();
                return;
            }
        }
        LoadDataTaskExternal loadDataTask = new LoadDataTaskExternal(this, ViewUtils.getProgressBar(getApplicationContext()), new ArrayList<>());
        loadDataTask.setRequestUrl(requestUrl);
        this.useCachingRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                boolean unused = NewFanWallActivity.this.canUseCaching();
            }
        };
        loadDataTask.setCanUseCachingRunnable(this.useCachingRunnable);
        this.handleInBgRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                NewFanWallActivity.this.handleInBackground();
            }
        };
        loadDataTask.setHandleInBgRunnable(this.handleInBgRunnable);
        this.parseDataRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                boolean unused = NewFanWallActivity.this.tryParseData(getDataToParse());
            }
        };
        loadDataTask.setParseDataRunnable(this.parseDataRunnable);
        this.updateControlsRunnable = new LoadDataTaskExternal.LoadDataRunnable() {
            public void run() {
                NewFanWallActivity.this.updateControlsWithData(getActivity());
            }
        };
        loadDataTask.setUpdateControlsRunnable(this.updateControlsRunnable);
        loadDataTask.launch();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.app.Activity] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: Multi-variable type inference failed */
    public void addComment() {
        /*
            r6 = this;
            r5 = 1
            r0 = r6
            if (r0 == 0) goto L_0x005e
            android.content.Intent r1 = new android.content.Intent
            android.content.Context r3 = r0.getApplicationContext()
            java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r4 = com.biznessapps.activities.SingleFragmentActivity.class
            r1.<init>(r3, r4)
            android.content.Intent r3 = r0.getIntent()
            java.lang.String r4 = "TAB_SPECIAL_ID"
            java.lang.String r2 = r3.getStringExtra(r4)
            java.lang.String r3 = "TAB_SPECIAL_ID"
            r1.putExtra(r3, r2)
            java.lang.String r3 = "TAB_LABEL"
            int r4 = com.biznessapps.layout.R.string.comments
            java.lang.String r4 = r6.getString(r4)
            r1.putExtra(r3, r4)
            java.lang.String r3 = "FAN_WALL_USE_PHOTO_OPTION"
            r1.putExtra(r3, r5)
            java.lang.String r3 = "YOUTUBE_MODE"
            r4 = 0
            r1.putExtra(r3, r4)
            java.lang.String r3 = "TAB_FRAGMENT"
            java.lang.String r4 = "FAN_ADD_COMMENT_FRAGMENT"
            r1.putExtra(r3, r4)
            java.lang.String r3 = "USE_SPECIAL_MD5_HASH_EXTRA"
            r1.putExtra(r3, r5)
            android.location.Location r3 = r6.currentLocation
            if (r3 == 0) goto L_0x005a
            java.lang.String r3 = "longitude"
            android.location.Location r4 = r6.currentLocation
            double r4 = r4.getLongitude()
            r1.putExtra(r3, r4)
            java.lang.String r3 = "latitude"
            android.location.Location r4 = r6.currentLocation
            double r4 = r4.getLatitude()
            r1.putExtra(r3, r4)
        L_0x005a:
            r3 = 4
            r0.startActivityForResult(r1, r3)
        L_0x005e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.NewFanWallActivity.addComment():void");
    }

    /* access modifiers changed from: private */
    public boolean tryParseData(String dataToParse) {
        boolean isDataNotEmpty;
        this.comments = JsonParserUtils.parseFanComments(dataToParse);
        if (this.comments == null || this.comments.isEmpty()) {
            isDataNotEmpty = false;
        } else {
            isDataNotEmpty = true;
        }
        if (isDataNotEmpty) {
            this.state.setDataLoaded(true);
            this.state.setNeedToReload(false);
            for (FanWallComment comment : this.comments) {
                if (StringUtils.isNotEmpty(comment.getUploadImageUrl())) {
                    this.photos.add(comment.getUploadImageUrl());
                }
            }
            cacher().saveData(CachingConstants.FAN_WALL_INFO_PROPERTY + this.tabId + this.isNearByUsed, this.comments);
        }
        return isDataNotEmpty;
    }

    /* access modifiers changed from: private */
    public void handleInBackground() {
        for (FanWallComment comment : this.comments) {
            if (StringUtils.isNotEmpty(comment.getImage())) {
                BitmapDownloader bitmapDownloader = this.downloader;
                Bitmap iconBitmap = BitmapDownloader.downloadBitmap(comment.getImage());
                if (iconBitmap != null) {
                    this.downloader.saveBitmap(iconBitmap, comment.getImage());
                }
            }
        }
        if (this.recentsDrawable == null || this.nearByDrawable == null) {
            AppSettings settings = AppCore.getInstance().getAppSettings();
            LayerDrawable activeStateRight = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_active_right);
            LayerDrawable passiveStateRight = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_passive_right);
            LayerDrawable activeStateLeft = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_active_left);
            LayerDrawable passiveStateLeft = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_passive_left);
            this.nearByDrawable = CommonUtils.getButtonDrawable(activeStateRight, passiveStateRight);
            this.recentsDrawable = CommonUtils.getButtonDrawable(activeStateLeft, passiveStateLeft);
        }
    }

    /* access modifiers changed from: private */
    public void updateControlsWithData(Activity holdActivity) {
        if (this.state.isDataLoaded) {
            this.commentsTitleView.performClick();
            this.bgUrl = this.comments.get(0).getBackground();
            loadBgUrl();
        }
        this.nearByButton.setBackgroundDrawable(this.nearByDrawable);
        this.recentsButton.setBackgroundDrawable(this.recentsDrawable);
    }

    private void loadBgUrl() {
        if (StringUtils.isNotEmpty(this.bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(this.bgUrl, this.commentsListView);
        }
    }

    private void clearBackground() {
        this.commentsContainer.setBackgroundDrawable(null);
        this.photosContainer.setBackgroundDrawable(null);
    }

    /* access modifiers changed from: private */
    public boolean canUseCaching() {
        this.comments = (List) cacher().getData(CachingConstants.FAN_WALL_INFO_PROPERTY + this.tabId + this.isNearByUsed);
        return (this.comments == null || this.state.isNeedToReload() || this.nearByDrawable == null || this.recentsDrawable == null) ? false : true;
    }

    /* access modifiers changed from: private */
    public void openPhotosContainer() {
        if (StringUtils.isNotEmpty(this.bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(this.bgUrl, this.photosContainer);
        }
        final GalleryData galleryData = new GalleryData();
        galleryData.setImageItems(this.photos);
        this.galleryView.setAdapter((ListAdapter) new ImageAdapter(getApplicationContext(), galleryData.getItems(), R.layout.gallery_item_layout));
        this.galleryView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
                if (position >= 0 && position < galleryData.getItems().size()) {
                    Intent previewIntent = new Intent(NewFanWallActivity.this.getApplicationContext(), GalleryPreviewActivity.class);
                    GalleryPreviewActivity.setGalleryItems(galleryData.getItems());
                    previewIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.GALLERY_PREVIEW_FRAGMENT);
                    previewIntent.putExtra(AppConstants.GALLERY_PREVIEW_EXTRA, galleryData.getItems().get(position));
                    previewIntent.putExtra(AppConstants.GALLERY_CURRENT_POS_EXTRA, position);
                    NewFanWallActivity.this.startActivity(previewIntent);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void openCommentsContainer() {
        if (StringUtils.isNotEmpty(this.bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(this.bgUrl, this.commentsContainer);
        }
        if (this.comments != null && !this.comments.isEmpty()) {
            this.commentsListView.setAdapter((ListAdapter) new NewFanWallAdapter(getApplicationContext(), this.comments, true));
            this.commentsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                /* JADX WARN: Type inference failed for: r3v0, types: [android.widget.Adapter] */
                /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                    jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                    	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                    	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                    	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                    	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                    	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                    	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                    */
                public void onItemClick(android.widget.AdapterView<?> r7, android.view.View r8, int r9, long r10) {
                    /*
                        r6 = this;
                        android.widget.Adapter r3 = r7.getAdapter()
                        java.lang.Object r1 = r3.getItem(r9)
                        com.biznessapps.model.FanWallComment r1 = (com.biznessapps.model.FanWallComment) r1
                        if (r1 == 0) goto L_0x0057
                        android.content.Intent r0 = new android.content.Intent
                        com.biznessapps.activities.NewFanWallActivity r3 = com.biznessapps.activities.NewFanWallActivity.this
                        android.content.Context r3 = r3.getApplicationContext()
                        java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r4 = com.biznessapps.activities.SingleFragmentActivity.class
                        r0.<init>(r3, r4)
                        java.lang.String r3 = "parent_id"
                        java.lang.String r4 = r1.getId()
                        r0.putExtra(r3, r4)
                        com.biznessapps.activities.NewFanWallActivity r3 = com.biznessapps.activities.NewFanWallActivity.this
                        android.content.Intent r3 = r3.getIntent()
                        java.lang.String r4 = "TAB_SPECIAL_ID"
                        java.lang.String r2 = r3.getStringExtra(r4)
                        java.lang.String r3 = "TAB_FRAGMENT"
                        java.lang.String r4 = "NewFanWallViewFragment"
                        r0.putExtra(r3, r4)
                        java.lang.String r3 = "TAB_LABEL"
                        com.biznessapps.activities.NewFanWallActivity r4 = com.biznessapps.activities.NewFanWallActivity.this
                        int r5 = com.biznessapps.layout.R.string.comments
                        java.lang.String r4 = r4.getString(r5)
                        r0.putExtra(r3, r4)
                        java.lang.String r3 = "HAS_CHILDS"
                        r4 = 0
                        r0.putExtra(r3, r4)
                        java.lang.String r3 = "FAN_WALL_EXTRA"
                        r0.putExtra(r3, r1)
                        java.lang.String r3 = "TAB_SPECIAL_ID"
                        r0.putExtra(r3, r2)
                        com.biznessapps.activities.NewFanWallActivity r3 = com.biznessapps.activities.NewFanWallActivity.this
                        r3.startActivity(r0)
                    L_0x0057:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.NewFanWallActivity.AnonymousClass12.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void openMapContainer() {
        MapView map = findViewById(R.id.map_view);
        map.setBuiltInZoomControls(true);
        map.setClickable(true);
        map.setSatellite(false);
        map.setStreetView(true);
        map.getOverlays().clear();
        List<GeoPoint> points = new ArrayList<>();
        for (FanWallComment comment : this.comments) {
            Bitmap defaultIcon = null;
            if (comment.getLatitude() != 0.0d || comment.getLongitude() != 0.0d) {
                GeoPoint p = new GeoPoint((int) (comment.getLatitude() * 1000000.0d), (int) (comment.getLongitude() * 1000000.0d));
                BitmapDownloader bitmapDownloader = this.downloader;
                Bitmap pointIcon = BitmapDownloader.getBitmap(comment.getImage());
                if (pointIcon == null) {
                    if (0 == 0) {
                        defaultIcon = BitmapFactory.decodeResource(getResources(), R.drawable.portrait);
                    }
                    pointIcon = defaultIcon;
                }
                addPoint(p, map, pointIcon, comment.getTitle());
                points.add(p);
            }
        }
        if (points.size() == 1) {
            map.getController().setZoom(15);
            map.getController().animateTo((GeoPoint) points.get(0));
        } else if (!points.isEmpty()) {
            MapUtils.SpanData span = MapUtils.defineSpan(points);
            map.getController().zoomToSpan(span.getLat(), span.getLon());
            map.getController().setCenter(span.getCenterPoint());
        }
        map.invalidate();
    }

    private void addPoint(GeoPoint p, MapView map, Bitmap pointIcon, String pointLabel) {
        if (p != null) {
            MarkerOverlay overlay = new MarkerOverlay(p, pointIcon, BitmapFactory.decodeResource(getResources(), R.drawable.icon_border));
            overlay.setPointTitle(pointLabel);
            map.getOverlays().add(overlay);
        }
    }

    /* access modifiers changed from: private */
    public void updateButtonsState(boolean isNearByUsed2) {
        this.recentsButton.setSelected(!isNearByUsed2);
        this.nearByButton.setSelected(isNearByUsed2);
    }

    private LocationListener getLocationListener() {
        return new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }

            public void onLocationChanged(Location location) {
                Location unused = NewFanWallActivity.this.currentLocation = location;
            }
        };
    }

    public static class State {
        /* access modifiers changed from: private */
        public boolean isDataLoaded;
        private boolean needToReload;

        public boolean isDataLoaded() {
            return this.isDataLoaded;
        }

        public void setDataLoaded(boolean isDataLoaded2) {
            this.isDataLoaded = isDataLoaded2;
        }

        public boolean isNeedToReload() {
            return this.needToReload;
        }

        public void setNeedToReload(boolean needToReload2) {
            this.needToReload = needToReload2;
        }
    }
}
