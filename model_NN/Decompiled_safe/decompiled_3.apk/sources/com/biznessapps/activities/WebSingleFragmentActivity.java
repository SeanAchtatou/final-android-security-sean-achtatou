package com.biznessapps.activities;

import java.util.Map;

public class WebSingleFragmentActivity extends SingleFragmentActivity {
    /* access modifiers changed from: protected */
    public Map<String, Class<?>> getActivityFragmentMap() {
        return this.activityFragmentMap;
    }
}
