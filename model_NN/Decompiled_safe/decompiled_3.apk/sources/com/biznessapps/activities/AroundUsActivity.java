package com.biznessapps.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHandlers;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.MapUtils;
import com.biznessapps.layout.views.map.SitesOverlay;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.AroundUsItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AroundUsActivity extends CommonMapActivity {
    private ViewGroup buttonContainer;
    private Set<String> colorKeys;
    private WeakReference<Drawable> drawableBgIcon;
    private int drawableBgResId;
    private Button greenButton;
    /* access modifiers changed from: private */
    public boolean isListMode = false;
    /* access modifiers changed from: private */
    public AroundUsItem item = new AroundUsItem();
    private SitesOverlay itemizedOverlay;
    /* access modifiers changed from: private */
    public ImageView listPointerView;
    private TextView listTitleView;
    /* access modifiers changed from: private */
    public ListView listView;
    private AsyncTask<Void, Void, List<AroundUsItem.PoiItem>> loadDataTask;
    /* access modifiers changed from: private */
    public MapView map;
    /* access modifiers changed from: private */
    public ImageView mapPointerView;
    private Map<String, ArrayList<AroundUsItem.PoiItem>> mapPoints = new HashMap();
    private TextView mapTitleView;
    /* access modifiers changed from: private */
    public List<AroundUsItem.PoiItem> points;
    private ProgressDialog progressBar;
    private Button purpleButton;
    private Button redButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppHandlers.getUiHandler();
        initMap();
        initButtons();
        loadData();
        this.map.invalidate();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.around_us_layout;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.loadDataTask != null && this.loadDataTask.getStatus().equals(AsyncTask.Status.RUNNING) && !this.loadDataTask.isCancelled()) {
            this.loadDataTask.cancel(true);
            stopProgressBar();
        }
    }

    private void initButtons() {
        AppSettings settings = AppCore.getInstance().getAppSettings();
        ((ViewGroup) findViewById(R.id.around_us_top_button_container)).setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.mapTitleView = (TextView) findViewById(R.id.map_textview);
        this.listTitleView = (TextView) findViewById(R.id.list_textview);
        this.listPointerView = (ImageView) findViewById(R.id.list_pointer);
        this.mapPointerView = (ImageView) findViewById(R.id.map_pointer);
        this.mapTitleView.setTextColor(ViewUtils.getColor(settings.getNavigBarTextColor()));
        this.listTitleView.setTextColor(ViewUtils.getColor(settings.getNavigBarTextColor()));
        this.listPointerView.setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.mapPointerView.setBackgroundColor(ViewUtils.getColor(settings.getNavigBarColor()));
        this.buttonContainer = (ViewGroup) findViewById(R.id.bottom_buttons_container);
        CommonUtils.customizeFooterNavigationBar(this.buttonContainer);
        this.greenButton = (Button) findViewById(R.id.around_us_green_button);
        this.redButton = (Button) findViewById(R.id.around_us_red_button);
        this.purpleButton = (Button) findViewById(R.id.around_us_purple_button);
        this.listView = (ListView) findViewById(R.id.list_view);
        ViewUtils.setGlobalBackgroundColor(this.listView);
        this.greenButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AroundUsActivity.this.updateItems(AroundUsActivity.this.item.getGreenColor());
            }
        });
        this.redButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AroundUsActivity.this.updateItems(AroundUsActivity.this.item.getRedColor());
            }
        });
        this.purpleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AroundUsActivity.this.updateItems(AroundUsActivity.this.item.getPurpleColor());
            }
        });
        this.listTitleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AroundUsActivity.this.map.setVisibility(8);
                AroundUsActivity.this.listView.setVisibility(0);
                boolean unused = AroundUsActivity.this.isListMode = true;
                AroundUsActivity.this.plugListView(AroundUsActivity.this.getApplicationContext(), AroundUsActivity.this.item.getPoi());
                AroundUsActivity.this.listPointerView.setVisibility(0);
                AroundUsActivity.this.mapPointerView.setVisibility(8);
            }
        });
        this.mapTitleView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AroundUsActivity.this.map.setVisibility(0);
                AroundUsActivity.this.listView.setVisibility(8);
                boolean unused = AroundUsActivity.this.isListMode = false;
                AroundUsActivity.this.listPointerView.setVisibility(8);
                AroundUsActivity.this.mapPointerView.setVisibility(0);
            }
        });
    }

    private void addGeoPoint(GeoPoint p, String title, String snippet, AroundUsItem.PoiItem item2) {
        this.itemizedOverlay.createNewOverlay(p, title, snippet, item2);
        this.map.invalidate();
    }

    private void initItemizedOverlay() {
        this.itemizedOverlay = new SitesOverlay(getApplicationContext(), getMarker(), getTapHandler());
        this.map.getOverlays().clear();
        this.map.getOverlays().add(this.itemizedOverlay);
        this.map.invalidate();
    }

    private void initMap() {
        this.map = findViewById(R.id.around_us_mapview);
        this.map.setBuiltInZoomControls(true);
        this.map.setSatellite(false);
        this.map.setClickable(true);
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    private Drawable getMarker() {
        Drawable marker = getResources().getDrawable(R.drawable.bubble);
        marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker.getIntrinsicHeight());
        return marker;
    }

    private void loadData() {
        showProgressBar();
        final String tabId = getIntent().getExtras().getString(AppConstants.TAB_SPECIAL_ID);
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        this.loadDataTask = new AsyncTask<Void, Void, List<AroundUsItem.PoiItem>>() {
            private long loadingTime;

            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
                onPostExecute((List<AroundUsItem.PoiItem>) ((List) x0));
            }

            /* access modifiers changed from: protected */
            public List<AroundUsItem.PoiItem> doInBackground(Void... params) {
                AroundUsItem unused = AroundUsActivity.this.item = (AroundUsItem) cacher.getData(CachingConstants.AROUND_INFO_PROPERTY + tabId);
                if (AroundUsActivity.this.item == null) {
                    AroundUsItem unused2 = AroundUsActivity.this.item = JsonParserUtils.parseAroundUsData(DataSource.getInstance().getData(String.format(ServerConstants.AROUND_US, cacher.getAppCode(), tabId)));
                    if (AroundUsActivity.this.item == null) {
                        ViewUtils.showShortToast(AroundUsActivity.this.getApplicationContext(), R.string.data_not_available);
                        AroundUsActivity.this.finish();
                    }
                }
                AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(AroundUsActivity.this.item.getImage(), AroundUsActivity.this.listView);
                List unused3 = AroundUsActivity.this.points = AroundUsActivity.this.item.getPoi();
                return AroundUsActivity.this.points;
            }

            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                this.loadingTime = System.currentTimeMillis();
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(List<AroundUsItem.PoiItem> points) {
                super.onPostExecute((Object) points);
                if (points != null) {
                    cacher.saveData(CachingConstants.AROUND_INFO_PROPERTY + tabId, AroundUsActivity.this.item);
                    AroundUsActivity.this.stopProgressBar();
                    AroundUsActivity.this.initPointsMap(points);
                    AroundUsActivity.this.initPoints(points);
                    AroundUsActivity.this.setButtonsData();
                    AroundUsActivity.this.defineButtonsVisibility();
                    if (AroundUsActivity.this.getIntent() != null) {
                        this.loadingTime = System.currentTimeMillis() - this.loadingTime;
                        CommonUtils.sendTimingEvent(AroundUsActivity.this.getApplicationContext(), AroundUsActivity.this.getIntent().getStringExtra(AppConstants.TAB_FRAGMENT_EXTRA), this.loadingTime);
                    }
                }
            }
        };
        this.loadDataTask.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void initPointsMap(List<AroundUsItem.PoiItem> points2) {
        ArrayList<AroundUsItem.PoiItem> pois;
        if (points2 != null && points2.size() > 0) {
            for (AroundUsItem.PoiItem poi : points2) {
                if (this.mapPoints.containsKey(poi.getColor())) {
                    pois = this.mapPoints.get(poi.getColor());
                } else {
                    pois = new ArrayList<>();
                }
                if (poi.getColor().equalsIgnoreCase(this.item.getRedColor())) {
                    poi.setCategoryName(this.item.getRedTitle());
                } else if (poi.getColor().equalsIgnoreCase(this.item.getGreenColor())) {
                    poi.setCategoryName(this.item.getGreenTitle());
                } else if (poi.getColor().equalsIgnoreCase(this.item.getPurpleColor())) {
                    poi.setCategoryName(this.item.getPurpleTitle());
                }
                pois.add(poi);
                this.mapPoints.put(poi.getColor(), pois);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateItems(String itemsColor) {
        Set<String> keys = this.mapPoints.keySet();
        if (this.colorKeys == null) {
            this.colorKeys = new HashSet(keys);
        }
        if (this.colorKeys.contains(itemsColor)) {
            this.colorKeys.remove(itemsColor);
        } else {
            this.colorKeys.add(itemsColor);
        }
        List<AroundUsItem.PoiItem> newItems = new ArrayList<>();
        for (String key : this.colorKeys) {
            if (this.mapPoints.get(key) != null) {
                newItems.addAll(this.mapPoints.get(key));
            }
        }
        if (this.isListMode) {
            plugListView(getApplicationContext(), newItems);
        } else {
            initPoints(newItems);
        }
    }

    /* access modifiers changed from: private */
    public void initPoints(List<AroundUsItem.PoiItem> poi) {
        if (poi != null) {
            initItemizedOverlay();
            if (poi != null && !poi.isEmpty()) {
                List<GeoPoint> points2 = new ArrayList<>();
                for (AroundUsItem.PoiItem item2 : poi) {
                    if (!(item2.getLatitude() == null || item2.getLongitude() == null)) {
                        GeoPoint p = new GeoPoint((int) (Double.parseDouble(item2.getLatitude()) * 1000000.0d), (int) (Double.parseDouble(item2.getLongitude()) * 1000000.0d));
                        addGeoPoint(p, item2.getName(), item2.getName(), item2);
                        points2.add(p);
                    }
                }
                if (points2.size() == 1) {
                    this.map.getController().setZoom(15);
                    this.map.getController().animateTo((GeoPoint) points2.get(0));
                    return;
                }
                MapUtils.SpanData span = MapUtils.defineSpan(points2);
                this.map.getController().zoomToSpan(span.getLat(), span.getLon());
                this.map.getController().setCenter(span.getCenterPoint());
            }
        }
    }

    /* access modifiers changed from: private */
    public void setButtonsData() {
        this.greenButton.setVisibility(0);
        this.redButton.setVisibility(0);
        this.purpleButton.setVisibility(0);
        this.greenButton.setTextColor(ViewUtils.getColor(this.item.getGreenTextColor()));
        this.greenButton.setText(this.item.getGreenTitle());
        this.redButton.setTextColor(ViewUtils.getColor(this.item.getRedTextColor()));
        this.redButton.setText(this.item.getRedTitle());
        this.purpleButton.setTextColor(ViewUtils.getColor(this.item.getPurpleTextColor()));
        this.purpleButton.setText(this.item.getPurpleTitle());
        CommonUtils.overrideImageColor(ViewUtils.getColor(this.item.getRedColor()), this.redButton.getBackground());
        CommonUtils.overrideImageColor(ViewUtils.getColor(this.item.getPurpleColor()), this.purpleButton.getBackground());
        CommonUtils.overrideImageColor(ViewUtils.getColor(this.item.getGreenColor()), this.greenButton.getBackground());
    }

    /* access modifiers changed from: private */
    public void defineButtonsVisibility() {
        if (!this.mapPoints.containsKey(this.item.getGreenColor())) {
            this.greenButton.setVisibility(8);
        }
        if (!this.mapPoints.containsKey(this.item.getRedColor())) {
            this.redButton.setVisibility(8);
        }
        if (!this.mapPoints.containsKey(this.item.getPurpleColor())) {
            this.purpleButton.setVisibility(8);
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [com.biznessapps.activities.AroundUsActivity, android.content.Context] */
    /* access modifiers changed from: protected */
    public void showProgressBar() {
        this.progressBar = ProgressDialog.show(this, "", getResources().getString(R.string.loading));
        this.progressBar.setCancelable(true);
    }

    /* access modifiers changed from: protected */
    public void stopProgressBar() {
        try {
            if (this.progressBar != null && this.progressBar.isShowing()) {
                this.progressBar.dismiss();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private SitesOverlay.TapHandler getTapHandler() {
        return new SitesOverlay.TapHandler() {
            public void overlayItemTapped(SitesOverlay.WrappedOverlayItem oi) {
                AroundUsActivity.this.showDialog(oi.getPoiInfo());
            }
        };
    }

    /* JADX WARN: Type inference failed for: r7v0, types: [com.biznessapps.activities.AroundUsActivity, android.content.Context] */
    /* access modifiers changed from: private */
    public void showDialog(final AroundUsItem.PoiItem poiItem) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View view = ViewUtils.loadLayout(getApplicationContext(), R.layout.pin_dialog);
        alertBuilder.setView(view);
        alertBuilder.setMessage(poiItem.getName());
        final AlertDialog dialog = alertBuilder.create();
        Button moreInfoButton = (Button) view.findViewById(R.id.more_info_button);
        Button directionsButton = (Button) view.findViewById(R.id.pin_location_button);
        if (this.drawableBgResId > 0) {
            moreInfoButton.setBackgroundResource(this.drawableBgResId);
            directionsButton.setBackgroundResource(this.drawableBgResId);
        } else if (!(this.drawableBgIcon == null || this.drawableBgIcon.get() == null)) {
            moreInfoButton.setBackgroundDrawable(this.drawableBgIcon.get());
            directionsButton.setBackgroundDrawable(this.drawableBgIcon.get());
        }
        moreInfoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AroundUsActivity.this.openWebView(poiItem.getDescription());
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        directionsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (poiItem != null) {
                    ViewUtils.openGoogleMap(AroundUsActivity.this.getApplicationContext(), poiItem.getLongitude(), poiItem.getLatitude());
                }
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void openWebView(String webData) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        if (StringUtils.isNotEmpty(webData)) {
            intent.putExtra(AppConstants.WEB_DATA, webData);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
            intent.putExtra(AppConstants.TAB_ID, getIntent().getLongExtra(AppConstants.TAB_ID, 0));
            intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
            startActivity(intent);
            return;
        }
        Toast.makeText(getApplicationContext(), R.string.data_not_available, 1).show();
    }

    /* access modifiers changed from: private */
    public void plugListView(Context context, List<AroundUsItem.PoiItem> list) {
        if (list != null) {
            List<AroundUsItem.PoiItem> sectionList = new LinkedList<>();
            for (AroundUsItem.PoiItem item2 : list) {
                sectionList.add(getWrappedItem(item2, sectionList));
            }
            this.listView.setAdapter((ListAdapter) new AroundUsListAdapter(context, sectionList));
            this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
                /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                    jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                    	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                    	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                    	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                    	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                    	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                    	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                    	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                    */
                public void onItemClick(android.widget.AdapterView<?> r8, android.view.View r9, int r10, long r11) {
                    /*
                        r7 = this;
                        android.widget.Adapter r2 = r8.getAdapter()
                        java.lang.Object r1 = r2.getItem(r10)
                        com.biznessapps.model.AroundUsItem$PoiItem r1 = (com.biznessapps.model.AroundUsItem.PoiItem) r1
                        android.content.Intent r0 = new android.content.Intent
                        com.biznessapps.activities.AroundUsActivity r2 = com.biznessapps.activities.AroundUsActivity.this
                        android.content.Context r2 = r2.getApplicationContext()
                        java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r3 = com.biznessapps.activities.SingleFragmentActivity.class
                        r0.<init>(r2, r3)
                        java.lang.String r2 = r1.getDescription()
                        boolean r2 = com.biznessapps.utils.StringUtils.isNotEmpty(r2)
                        if (r2 == 0) goto L_0x005a
                        java.lang.String r2 = "WEB_DATA"
                        java.lang.String r3 = r1.getDescription()
                        r0.putExtra(r2, r3)
                        java.lang.String r2 = "TAB_FRAGMENT"
                        java.lang.String r3 = "WEB_VIEW_SINGLE_FRAGMENT"
                        r0.putExtra(r2, r3)
                        java.lang.String r2 = "TAB_UNIQUE_ID"
                        com.biznessapps.activities.AroundUsActivity r3 = com.biznessapps.activities.AroundUsActivity.this
                        android.content.Intent r3 = r3.getIntent()
                        java.lang.String r4 = "TAB_UNIQUE_ID"
                        r5 = 0
                        long r3 = r3.getLongExtra(r4, r5)
                        r0.putExtra(r2, r3)
                        java.lang.String r2 = "TAB_LABEL"
                        com.biznessapps.activities.AroundUsActivity r3 = com.biznessapps.activities.AroundUsActivity.this
                        android.content.Intent r3 = r3.getIntent()
                        java.lang.String r4 = "TAB_LABEL"
                        java.lang.String r3 = r3.getStringExtra(r4)
                        r0.putExtra(r2, r3)
                        com.biznessapps.activities.AroundUsActivity r2 = com.biznessapps.activities.AroundUsActivity.this
                        r2.startActivity(r0)
                    L_0x005a:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.AroundUsActivity.AnonymousClass10.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
                }
            });
        }
    }

    private AroundUsItem.PoiItem getWrappedItem(AroundUsItem.PoiItem item2, List<AroundUsItem.PoiItem> itemsList) {
        int bgColor;
        int textColor;
        AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        if (uiSettings.isHasColor()) {
            if (itemsList.size() % 2 == 0) {
                bgColor = uiSettings.getOddRowColor();
                textColor = uiSettings.getOddRowTextColor();
            } else {
                bgColor = uiSettings.getEvenRowColor();
                textColor = uiSettings.getEvenRowTextColor();
            }
            item2.setItemColor(bgColor);
            item2.setItemTextColor(textColor);
        }
        return item2;
    }

    public class AroundUsListAdapter extends AbstractAdapter<AroundUsItem.PoiItem> {
        private String greenColor;
        private Drawable greenIconDrawable;
        private String purpleColor;
        private Drawable purpleIconDrawable;
        private String redColor;
        private Drawable redIconDrawable;

        public AroundUsListAdapter(Context context, List<AroundUsItem.PoiItem> items) {
            super(context, items, R.layout.around_us_row);
            this.redIconDrawable = AroundUsActivity.this.getResources().getDrawable(R.drawable.around_us_red_icon_bg);
            this.greenIconDrawable = AroundUsActivity.this.getResources().getDrawable(R.drawable.around_us_green_icon_bg);
            this.purpleIconDrawable = AroundUsActivity.this.getResources().getDrawable(R.drawable.around_us_purple_icon_bg);
            this.greenColor = AroundUsActivity.this.item.getGreenColor();
            this.redColor = AroundUsActivity.this.item.getRedColor();
            this.purpleColor = AroundUsActivity.this.item.getPurpleColor();
            CommonUtils.overrideImageColor(ViewUtils.getColor(this.redColor), this.redIconDrawable);
            CommonUtils.overrideImageColor(ViewUtils.getColor(this.purpleColor), this.purpleIconDrawable);
            CommonUtils.overrideImageColor(ViewUtils.getColor(this.greenColor), this.greenIconDrawable);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemHolder.RssItem holder;
            if (convertView == null) {
                convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
                holder = new ListItemHolder.RssItem();
                holder.setTextViewTitle((TextView) convertView.findViewById(R.id.around_us_description));
                holder.setTextViewSummary((TextView) convertView.findViewById(R.id.around_ud_category_name));
                holder.setImageView((ImageView) convertView.findViewById(R.id.icon_image));
                convertView.setTag(holder);
            } else {
                holder = (ListItemHolder.RssItem) convertView.getTag();
            }
            AroundUsItem.PoiItem item = (AroundUsItem.PoiItem) this.items.get(position);
            if (item != null) {
                holder.getTextViewSummary().setText(item.getCategoryName());
                holder.getTextViewTitle().setText(item.getName());
                if (item.hasColor()) {
                    convertView.setBackgroundDrawable(new ColorDrawable(item.getItemColor()));
                    setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
                }
                setTextColorToView(ViewUtils.getColor(item.getColor()), holder.getTextViewSummary());
                ImageView iconBgImage = (ImageView) convertView.findViewById(R.id.icon_image);
                ImageView rightArrowBgImage = (ImageView) convertView.findViewById(R.id.right_arrow_view);
                Drawable iconBgDrawable = null;
                if (item.getColor().equalsIgnoreCase(this.greenColor)) {
                    iconBgDrawable = this.greenIconDrawable;
                } else if (item.getColor().equalsIgnoreCase(this.redColor)) {
                    iconBgDrawable = this.redIconDrawable;
                } else if (item.getColor().equalsIgnoreCase(this.purpleColor)) {
                    iconBgDrawable = this.purpleIconDrawable;
                }
                iconBgImage.setBackgroundDrawable(iconBgDrawable);
                rightArrowBgImage.setBackgroundDrawable(iconBgDrawable);
            }
            convertView.setClickable(false);
            return convertView;
        }
    }
}
