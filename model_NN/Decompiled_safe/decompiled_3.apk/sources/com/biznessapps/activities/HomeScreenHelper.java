package com.biznessapps.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.delegate.TellFriendDelegate;
import com.biznessapps.layout.R;
import com.biznessapps.model.App;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.LocationItem;
import com.biznessapps.model.Tab;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.utils.google.caching.ImageFetcher;
import com.facebook.AppEventsConstants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HomeScreenHelper {
    public static void initOptionsViews(Activity activity, ViewGroup root) {
        AppSettings appSettings = AppCore.getInstance().getAppSettings();
        boolean hasMoreButton = appSettings.hasMoreButtonNavigation();
        if (appSettings.getRows() <= 1 || hasMoreButton) {
            ViewGroup buttonsContainer = (ViewGroup) root.findViewById(R.id.home_horizontal_container);
            if (!AppCore.getInstance().getAppSettings().isMusicOnTop()) {
                ((LinearLayout.LayoutParams) buttonsContainer.getLayoutParams()).setMargins(0, 0, 0, activity.getResources().getDimensionPixelSize(R.dimen.tabs_full_height_1row));
            }
            GridView gallery = (GridView) root.findViewById(R.id.home_tab_gallery_view);
            Button callUsButton = (Button) root.findViewById(R.id.call_us_button);
            Button directionButton = (Button) root.findViewById(R.id.direction_button);
            Button tellFriendButton = (Button) root.findViewById(R.id.tell_friend_button);
            boolean hasSubButtons = appSettings.getNavigationMenuType() == 3;
            callUsButton.setVisibility((!hasSubButtons || !appSettings.hasCallButton() || AppCore.getInstance().isTablet()) ? 8 : 0);
            directionButton.setVisibility((!hasSubButtons || !appSettings.hasDirectionButton()) ? 8 : 0);
            tellFriendButton.setVisibility((!hasSubButtons || !appSettings.hasTellFriendButton()) ? 8 : 0);
            final ViewGroup tellFriendContentView = (ViewGroup) root.findViewById(R.id.tell_friends_content);
            AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
            callUsButton.setTextColor(settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), callUsButton.getBackground());
            directionButton.setTextColor(settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), directionButton.getBackground());
            tellFriendButton.setTextColor(settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), tellFriendButton.getBackground());
            TellFriendDelegate.initTellFriends(activity, tellFriendContentView);
            defineSubButtons(gallery, activity);
            final Activity activity2 = activity;
            callUsButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    HomeScreenHelper.callUs(activity2);
                }
            });
            final Activity activity3 = activity;
            directionButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HomeScreenHelper.showDirections(activity3);
                }
            });
            final Activity activity4 = activity;
            tellFriendButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HomeScreenHelper.openFriendContent(tellFriendContentView, activity4.getApplicationContext());
                }
            });
        }
    }

    private static void defineSubButtons(GridView gallery, Activity activity) {
        List<Tab> subTabs;
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        App appInfo = cacher.getAppInfo();
        final List<View> buttonList = new ArrayList<>();
        boolean hasSubTabs = (appInfo == null || appInfo.getSubTabs() == null) ? false : true;
        gallery.setVisibility(hasSubTabs ? 0 : 8);
        if (hasSubTabs && (subTabs = appInfo.getSubTabs()) != null && subTabs.size() > 0) {
            ImageFetcher imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
            for (final Tab tab : subTabs) {
                if (tab.isActive() && !tab.getTabId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    ViewGroup view = (ViewGroup) ViewUtils.loadLayout(activity.getApplicationContext(), R.layout.sub_tab_layout);
                    ImageView button = (ImageView) view.findViewById(R.id.sub_home_image);
                    TextView textView = (TextView) view.findViewById(R.id.sub_home_text);
                    textView.setTextColor(Color.parseColor("#" + tab.getTabLabelTextColor()));
                    textView.setText(tab.getLabel());
                    imageFetcher.loadImage(tab.getTabImageUrl(), button);
                    final Activity activity2 = activity;
                    button.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            Tab tabToUse = null;
                            Iterator i$ = cacher.getTabList().iterator();
                            while (true) {
                                if (!i$.hasNext()) {
                                    break;
                                }
                                Tab item = i$.next();
                                if (item.getTabId().equalsIgnoreCase(tab.getTabId())) {
                                    tabToUse = item;
                                    break;
                                }
                            }
                            if (tabToUse != null) {
                                Intent intent = new Intent(activity2.getApplicationContext(), SingleFragmentActivity.class);
                                if (StringUtils.isNotEmpty(tabToUse.getUrl())) {
                                    intent.putExtra(AppConstants.URL, tabToUse.getUrl());
                                }
                                intent.putExtra(AppConstants.TAB_ID, tabToUse.getId());
                                intent.putExtra(AppConstants.TAB_LABEL, tabToUse.getLabel());
                                intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabToUse.getTabId());
                                intent.putExtra(AppConstants.ITEM_ID, tabToUse.getItemId());
                                intent.putExtra(AppConstants.SECTION_ID, tabToUse.getSectionId());
                                intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, tab.getViewController());
                                intent.putExtra(AppConstants.TAB, tab);
                                activity2.startActivity(intent);
                            }
                        }
                    });
                    button.setClickable(true);
                    buttonList.add(view);
                }
            }
            if (buttonList.size() > 0) {
                gallery.setAdapter((ListAdapter) new BaseAdapter() {
                    public View getView(int position, View convertView, ViewGroup parent) {
                        return (View) buttonList.get(position);
                    }

                    public long getItemId(int position) {
                        return (long) position;
                    }

                    public Object getItem(int position) {
                        return buttonList.get(position);
                    }

                    public int getCount() {
                        return buttonList.size();
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    public static void openFriendContent(View tellFriendContentView, Context context) {
        if (tellFriendContentView.getVisibility() == 8) {
            tellFriendContentView.setVisibility(0);
            tellFriendContentView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.show_tell_friends_dialog));
        }
    }

    /* access modifiers changed from: private */
    public static void callUs(final Activity activity) {
        showMultipleDialog(activity, R.string.branch_call_title, new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r4, android.view.View r5, int r6, long r7) {
                /*
                    r3 = this;
                    android.widget.Adapter r2 = r4.getAdapter()
                    java.lang.Object r0 = r2.getItem(r6)
                    com.biznessapps.model.LocationItem r0 = (com.biznessapps.model.LocationItem) r0
                    java.lang.String r1 = r0.getTelephone()
                    android.app.Activity r2 = r3
                    android.content.Context r2 = r2.getApplicationContext()
                    com.biznessapps.utils.ViewUtils.call(r2, r1)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.HomeScreenHelper.AnonymousClass6.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        }, true);
    }

    /* access modifiers changed from: private */
    public static void showDirections(final Activity activity) {
        showMultipleDialog(activity, R.string.branch_directions_title, new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
                /*
                    r4 = this;
                    android.widget.Adapter r1 = r5.getAdapter()
                    java.lang.Object r0 = r1.getItem(r7)
                    com.biznessapps.model.LocationItem r0 = (com.biznessapps.model.LocationItem) r0
                    android.app.Activity r1 = r3
                    android.content.Context r1 = r1.getApplicationContext()
                    java.lang.String r2 = r0.getLongitude()
                    java.lang.String r3 = r0.getLatitude()
                    com.biznessapps.utils.ViewUtils.openGoogleMap(r1, r2, r3)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.activities.HomeScreenHelper.AnonymousClass7.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        }, false);
    }

    private static void showMultipleDialog(Activity activity, int dialogTitle, AdapterView.OnItemClickListener listener, boolean makeCall) {
        App appInfo = AppCore.getInstance().getCachingManager().getAppInfo();
        List<LocationItem> locations = appInfo == null ? null : appInfo.getLocations();
        if (locations == null || locations.size() != 1) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
            ViewGroup listViewRoot = (ViewGroup) ViewUtils.loadLayout(activity.getApplicationContext(), R.layout.multiple_item_dialog);
            ListView listView = (ListView) listViewRoot.findViewById(R.id.list_view);
            listView.setItemsCanFocus(false);
            if (locations != null && !locations.isEmpty()) {
                listView.setAdapter((ListAdapter) new MulitipleItemAdapter(activity.getApplicationContext(), locations));
            }
            listView.setOnItemClickListener(listener);
            alertBuilder.setView(listViewRoot);
            alertBuilder.setMessage(dialogTitle);
            alertBuilder.show();
        } else if (makeCall) {
            ViewUtils.call(activity.getApplicationContext(), locations.get(0).getTelephone());
        } else {
            LocationItem location = locations.get(0);
            ViewUtils.openGoogleMap(activity.getApplicationContext(), location.getLongitude(), location.getLatitude());
        }
    }

    public static class MulitipleItemAdapter extends AbstractAdapter<LocationItem> {
        public MulitipleItemAdapter(Context context, List<LocationItem> items) {
            super(context, items, R.layout.multiple_row);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemHolder.CommonItem holder;
            LocationItem item = (LocationItem) this.items.get(position);
            if (convertView == null) {
                convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
                holder = new ListItemHolder.CommonItem();
                holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
                convertView.setTag(holder);
            } else {
                holder = (ListItemHolder.CommonItem) convertView.getTag();
            }
            if (item != null) {
                holder.getTextViewTitle().setText(HomeScreenHelper.getFullAddress(item));
            }
            return convertView;
        }
    }

    /* access modifiers changed from: private */
    public static String getFullAddress(LocationItem location) {
        String address1 = location.getAddress1();
        String address2 = location.getAddress2();
        String city = location.getCity();
        String state = location.getState();
        String zip = location.getZip();
        String address = "";
        if (StringUtils.isNotEmpty(address1)) {
            address = String.format("%s %s", address, address1);
        }
        if (StringUtils.isNotEmpty(address2)) {
            address = String.format("%s, %s", address, address2);
        }
        if (StringUtils.isNotEmpty(city)) {
            address = String.format("%s, %s", address, city);
        }
        if (StringUtils.isNotEmpty(state)) {
            address = String.format("%s, %s", address, state);
        }
        if (!StringUtils.isNotEmpty(zip)) {
            return address;
        }
        return String.format("%s, %s", address, zip);
    }
}
