package com.biznessapps.constants;

public interface ReservationSystemConstants {
    public static final String AMOUNT_EXTRA = "amount";
    public static final String BOOKING_DATE_PARAM = "d";
    public static final String BOOKING_ITEM_ID_PARAM = "i";
    public static final String BOOKING_LOCATION_ID_PARAM = "loc_id";
    public static final String BOOKING_NOTE_PARAM = "n";
    public static final String BOOKING_PAID_AMOUNT_PARAM = "pa";
    public static final String BOOKING_PAY_METHOD_PARAM = "cm";
    public static final String BOOKING_SERVICE_NAME_PARAM = "sn";
    public static final String BOOKING_TIME_FROM_PARAM = "tf";
    public static final String BOOKING_TIME_TO_PARAM = "tt";
    public static final String BOOKING_TRANSACTION_ID_PARAM = "ti";
    public static final String DEFAULT_LANGUAGE = "en_US";
    public static final String ERROR = "error";
    public static final String LOGIN = "LOGIN";
    public static final String PAYMENT = "PAYMENT";
    public static final String PRE_PAYMENT_DESCRIPTION = "Pre-payment";
    public static final String RECOVERY_PARAM_EMAIL = "email";
    public static final String USER_EMAIL = "u";
    public static final String USER_FIRST_NAME = "f";
    public static final String USER_LAST_NAME = "l";
    public static final String USER_PASSWORD = "p";
    public static final String USER_PHONE = "c";
}
