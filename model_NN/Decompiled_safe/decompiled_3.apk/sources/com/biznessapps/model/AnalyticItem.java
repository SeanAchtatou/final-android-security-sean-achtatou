package com.biznessapps.model;

import android.content.Context;

public class AnalyticItem {
    private String accountId;
    private String appId;
    private String catId;
    private Context context;
    private String itemId;
    private String tabId;

    public String getTabId() {
        return this.tabId;
    }

    public void setTabId(String tabId2) {
        this.tabId = tabId2;
    }

    public String getCatId() {
        return this.catId;
    }

    public void setCatId(String catId2) {
        this.catId = catId2;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId2) {
        this.appId = appId2;
    }

    public String getItemId() {
        return this.itemId;
    }

    public void setItemId(String itemId2) {
        this.itemId = itemId2;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public String getAccountId() {
        return this.accountId;
    }

    public void setAccountId(String accountId2) {
        this.accountId = accountId2;
    }
}
