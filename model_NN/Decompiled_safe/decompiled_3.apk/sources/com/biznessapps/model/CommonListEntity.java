package com.biznessapps.model;

import com.facebook.AppEventsConstants;
import java.io.Serializable;

public class CommonListEntity implements Serializable {
    private static final long serialVersionUID = -5462919420360654508L;
    private String background;
    private String description = "";
    private boolean hasColor = false;
    private boolean hasNewDesign;
    private String headerImage;
    private String id;
    private String image;
    private int imageId;
    private boolean isSelected = false;
    private int itemColor;
    private int itemTextColor;
    private String section;
    private boolean showSection;
    private String title = "";

    public CommonListEntity(int itemColor2, int itemTextColor2, String title2) {
        this.title = title2;
        this.itemColor = itemColor2;
        this.itemTextColor = itemTextColor2;
    }

    public CommonListEntity(String title2) {
        this.title = title2;
    }

    public CommonListEntity() {
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        if (id2 == null || id2.equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
            id2 = "";
        }
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        if (description2 == null) {
            this.description = "";
        } else {
            this.description = description2;
        }
    }

    public String getSection() {
        return this.section;
    }

    public void setSection(String section2) {
        this.section = section2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getHeaderImage() {
        return this.headerImage;
    }

    public void setHeaderImage(String headerImage2) {
        this.headerImage = headerImage2;
    }

    public boolean hasColor() {
        return this.hasColor;
    }

    public void setHasColor(boolean hasColor2) {
        this.hasColor = hasColor2;
    }

    public int getImageId() {
        return this.imageId;
    }

    public void setImageId(int imageId2) {
        this.imageId = imageId2;
    }

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean isSelected2) {
        this.isSelected = isSelected2;
    }

    public int getItemColor() {
        return this.itemColor;
    }

    public void setItemColor(int itemColor2) {
        this.itemColor = itemColor2;
        setHasColor(true);
    }

    public int getItemTextColor() {
        return this.itemTextColor;
    }

    public void setItemTextColor(int itemTextColor2) {
        this.itemTextColor = itemTextColor2;
    }

    public String getBackground() {
        return this.background;
    }

    public void setBackground(String background2) {
        this.background = background2;
    }

    public boolean isShowSection() {
        return this.showSection;
    }

    public void setShowSection(boolean showSection2) {
        this.showSection = showSection2;
    }

    public boolean hasNewDesign() {
        return this.hasNewDesign;
    }

    public void setHasNewDesign(boolean hasNewDesign2) {
        this.hasNewDesign = hasNewDesign2;
    }
}
