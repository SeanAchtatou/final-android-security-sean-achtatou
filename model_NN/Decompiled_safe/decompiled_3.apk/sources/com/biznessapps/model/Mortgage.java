package com.biznessapps.model;

public class Mortgage {
    private String customButton;
    private String image;
    private String interest;
    private String readOnly;
    private boolean useInMemoryImage;

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }

    public String getInterest() {
        return this.interest;
    }

    public void setInterest(String interest2) {
        this.interest = interest2;
    }

    public String getReadOnly() {
        return this.readOnly;
    }

    public void setReadOnly(String readOnly2) {
        this.readOnly = readOnly2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getCustomButton() {
        return this.customButton;
    }

    public void setCustomButton(String customButton2) {
        this.customButton = customButton2;
    }
}
