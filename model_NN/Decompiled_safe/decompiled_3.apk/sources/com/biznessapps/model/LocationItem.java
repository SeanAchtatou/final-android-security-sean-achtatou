package com.biznessapps.model;

import java.util.ArrayList;
import java.util.List;

public class LocationItem extends CommonListEntity {
    private String address1;
    private String address2;
    private String city;
    private String comment;
    private String customButton;
    private String email;
    private String itemBgUrl;
    private String latitude;
    private String longitude;
    private List<LocationOpeningTime> openingTimes = new ArrayList();
    private String state;
    private String telephone;
    private String telephoneDisplay;
    private String timeZone;
    private String website;
    private String zip;

    public String getCustomButton() {
        return this.customButton;
    }

    public void setCustomButton(String customButton2) {
        this.customButton = customButton2;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone2) {
        this.telephone = telephone2;
    }

    public String getTelephoneDisplay() {
        return this.telephoneDisplay;
    }

    public void setTelephoneDisplay(String telephoneDisplay2) {
        this.telephoneDisplay = telephoneDisplay2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getWebsite() {
        return this.website;
    }

    public void setWebsite(String website2) {
        this.website = website2;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address12) {
        this.address1 = address12;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address22) {
        this.address2 = address22;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state2) {
        this.state = state2;
    }

    public String getZip() {
        return this.zip;
    }

    public void setZip(String zip2) {
        this.zip = zip2;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public List<LocationOpeningTime> getOpeningTimes() {
        return this.openingTimes;
    }

    public void setOpeningTimes(List<LocationOpeningTime> openingTimes2) {
        this.openingTimes = openingTimes2;
    }

    public String getItemBgUrl() {
        return this.itemBgUrl;
    }

    public void setItemBgUrl(String itemBgUrl2) {
        this.itemBgUrl = itemBgUrl2;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(String timeZone2) {
        this.timeZone = timeZone2;
    }
}
