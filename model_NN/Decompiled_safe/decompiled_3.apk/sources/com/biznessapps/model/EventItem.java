package com.biznessapps.model;

import android.util.Log;
import com.facebook.AppEventsConstants;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class EventItem extends CommonListEntity {
    private static final String AM_POSTFIX = " AM";
    private static final String CALENDAR_LOG = "CalendarEvent";
    private static final String DATETIME_PARSE_ERROR_MSG = "Date parse error";
    private static final String DATETIME_PATTERN = "dd-MM-yyyy HH:mm";
    private static final String NO_TIME = "--";
    private static final String PM_POSTFIX = " PM";
    private static final String TIME_DELIMETER = ":";
    private String date;
    private GregorianCalendar datetimeBegin;
    private GregorianCalendar datetimeEnd;
    private String day;
    private boolean isRecurring;
    private String month;
    private RecurringDay recurringDay;
    private String section;
    private String timeFrom;
    private String timeTo;
    private String title;

    public enum RecurringDay {
        SU,
        MO,
        TU,
        WE,
        TH,
        FR,
        SA;

        public static RecurringDay findDay(int value) {
            for (RecurringDay w : values()) {
                if (w.ordinal() == value) {
                    return w;
                }
            }
            return SU;
        }
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getSection() {
        return this.section;
    }

    public void setSection(String section2) {
        this.section = section2;
    }

    public String getMonth() {
        return this.month;
    }

    public void setMonth(String month2) {
        this.month = month2;
    }

    public String getDay() {
        return this.day;
    }

    public void setDay(String day2) {
        this.day = day2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getFullDate() {
        int year;
        try {
            year = this.datetimeBegin.getTime().getYear() + 1900;
        } catch (Exception e) {
            year = Calendar.getInstance().get(1);
        }
        return this.month + " " + this.day + ", " + year + "  " + getTimeIn12ClockSystem(this.timeFrom) + " - " + getTimeIn12ClockSystem(this.timeTo);
    }

    public boolean isRecurring() {
        return this.isRecurring;
    }

    public void setRecurring(boolean isRecurring2) {
        this.isRecurring = isRecurring2;
    }

    public RecurringDay getRecurringDay() {
        return this.recurringDay;
    }

    public void setRecurringDay(RecurringDay recurringDay2) {
        this.recurringDay = recurringDay2;
    }

    public GregorianCalendar getDatetimeBegin() {
        return this.datetimeBegin;
    }

    public void setDatetimeBegin(Date date2) {
        if (date2 != null) {
            try {
                this.datetimeBegin = new GregorianCalendar();
                this.datetimeBegin.setTime(date2);
            } catch (Exception e) {
                Log.e(CALENDAR_LOG, DATETIME_PARSE_ERROR_MSG, e);
            }
        }
    }

    public GregorianCalendar getDatetimeEnd() {
        return this.datetimeEnd;
    }

    public void setDatetimeEnd(Date date2) {
        if (this.datetimeEnd != null) {
            try {
                this.datetimeEnd = new GregorianCalendar();
                this.datetimeEnd.setTime(date2);
            } catch (Exception e) {
                Log.e(CALENDAR_LOG, DATETIME_PARSE_ERROR_MSG, e);
            }
        }
    }

    public void setDatetimeEnd(GregorianCalendar datetimeEnd2) {
        this.datetimeEnd = datetimeEnd2;
    }

    public String getTimeFrom() {
        return this.timeFrom;
    }

    public void setTimeFrom(String timeFrom2) {
        this.timeFrom = timeFrom2;
    }

    public String getTimeTo() {
        return this.timeTo;
    }

    public void setTimeTo(String timeTo2) {
        this.timeTo = timeTo2;
    }

    private String getTimeIn12ClockSystem(String time) {
        boolean isPmTime = false;
        String result = time;
        try {
            int delimeterIndex = time.lastIndexOf(TIME_DELIMETER);
            if (delimeterIndex <= 0) {
                return result;
            }
            String firstPart = time.substring(0, delimeterIndex);
            String secondPart = time.substring(delimeterIndex + 1, time.length());
            int hours = Integer.parseInt(firstPart);
            int minutes = Integer.parseInt(secondPart);
            if (hours == 24 || hours == 0) {
                hours = 12;
            } else if (hours == 12) {
                isPmTime = true;
            } else if (hours > 12) {
                isPmTime = true;
                hours -= 12;
            }
            return "" + hours + TIME_DELIMETER + (minutes < 10 ? AppEventsConstants.EVENT_PARAM_VALUE_NO + minutes : "" + minutes) + (isPmTime ? PM_POSTFIX : AM_POSTFIX);
        } catch (Exception e) {
            return NO_TIME;
        }
    }
}
