package com.biznessapps.model;

public class ReservationTimeItem extends CommonListEntity {
    private static final long serialVersionUID = 3657741278201636112L;
    protected int from;
    protected int to;

    public int getFrom() {
        return this.from;
    }

    public void setFrom(int from2) {
        this.from = from2;
    }

    public int getTo() {
        return this.to;
    }

    public void setTo(int to2) {
        this.to = to2;
    }
}
