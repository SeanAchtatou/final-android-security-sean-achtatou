package com.biznessapps.model;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;

public class ReservationServiceItem extends CommonListEntity {
    private static final long serialVersionUID = -8329032123918455984L;
    private String currency = AppConstants.DEFAULT_CURRENCY;
    private String currencySign = AppConstants.DEFAULT_CURRENCY_SIGN;
    private int mins;
    private String name;
    private String note;
    private float price;
    private float reserveFee;
    private ArrayList<String> restWeeks;
    private String thumbnail;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public int getMins() {
        return this.mins;
    }

    public void setMins(int mins2) {
        this.mins = mins2;
    }

    public float getPrice() {
        return this.price;
    }

    public void setPrice(float price2) {
        this.price = price2;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency2) {
        if (StringUtils.isNotEmpty(currency2)) {
            this.currency = currency2;
        }
    }

    public String getCurrencySign() {
        return this.currencySign;
    }

    public void setCurrencySign(String currencySign2) {
        if (StringUtils.isNotEmpty(currencySign2)) {
            this.currencySign = currencySign2;
        }
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note2) {
        this.note = note2;
    }

    public float getReserveFee() {
        return this.reserveFee;
    }

    public void setReserveFee(float reserveFee2) {
        this.reserveFee = reserveFee2;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2;
    }

    public ArrayList<String> getRestWeeks() {
        return this.restWeeks;
    }

    public void setRestWeeks(ArrayList<String> restWeeks2) {
        this.restWeeks = restWeeks2;
    }
}
