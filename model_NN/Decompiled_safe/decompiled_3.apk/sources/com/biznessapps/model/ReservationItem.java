package com.biznessapps.model;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.utils.StringUtils;

public class ReservationItem extends CommonListEntity {
    private static final long serialVersionUID = -8538932652205780119L;
    private String appId;
    private String billingAddressId;
    private String checkoutMethod;
    private float cost;
    private String currency = AppConstants.DEFAULT_CURRENCY;
    private String currencySign = AppConstants.DEFAULT_CURRENCY_SIGN;
    private String date;
    private String duration;
    private String imageUrl;
    private String itemId;
    private String locId;
    private String note;
    private String orderState;
    private float paidAmount;
    private String placedOn;
    private String serviceName;
    private String tabId;
    private String thumbnail;
    private String timeFrom;
    private String timeTo;
    private String transactionId;
    private String userId;

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getTimeFrom() {
        return this.timeFrom;
    }

    public void setTimeFrom(String timeFrom2) {
        this.timeFrom = timeFrom2;
    }

    public String getTimeTo() {
        return this.timeTo;
    }

    public void setTimeTo(String timeTo2) {
        this.timeTo = timeTo2;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getItemId() {
        return this.itemId;
    }

    public void setItemId(String itemId2) {
        this.itemId = itemId2;
    }

    public String getServiceName() {
        return this.serviceName;
    }

    public void setServiceName(String serviceName2) {
        this.serviceName = serviceName2;
    }

    public float getPaidAmount() {
        return this.paidAmount;
    }

    public void setPaidAmount(float paidAmount2) {
        this.paidAmount = paidAmount2;
    }

    public String getOrderState() {
        return this.orderState;
    }

    public void setOrderState(String orderState2) {
        this.orderState = orderState2;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId2) {
        this.appId = appId2;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note2) {
        this.note = note2;
    }

    public String getTabId() {
        return this.tabId;
    }

    public void setTabId(String tabId2) {
        this.tabId = tabId2;
    }

    public String getPlacedOn() {
        return this.placedOn;
    }

    public void setPlacedOn(String placedOn2) {
        this.placedOn = placedOn2;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency2) {
        if (StringUtils.isNotEmpty(currency2)) {
            this.currency = currency2;
        }
    }

    public String getCurrencySign() {
        return this.currencySign;
    }

    public void setCurrencySign(String currencySign2) {
        if (StringUtils.isNotEmpty(currencySign2)) {
            this.currencySign = currencySign2;
        }
    }

    public String getLocId() {
        return this.locId;
    }

    public void setLocId(String locId2) {
        this.locId = locId2;
    }

    public String getTransactionId() {
        return this.transactionId;
    }

    public void setTransactionId(String transactionId2) {
        this.transactionId = transactionId2;
    }

    public String getCheckoutMethod() {
        return this.checkoutMethod;
    }

    public void setCheckoutMethod(String checkoutMethod2) {
        this.checkoutMethod = checkoutMethod2;
    }

    public String getBillingAddressId() {
        return this.billingAddressId;
    }

    public void setBillingAddressId(String billingAddressId2) {
        this.billingAddressId = billingAddressId2;
    }

    public float getCost() {
        return this.cost;
    }

    public void setCost(float cost2) {
        this.cost = cost2;
    }

    public String getDuration() {
        return this.duration;
    }

    public void setDuration(String duration2) {
        this.duration = duration2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getThumbnail() {
        return this.thumbnail;
    }

    public void setThumbnail(String thumbnail2) {
        this.thumbnail = thumbnail2;
    }
}
