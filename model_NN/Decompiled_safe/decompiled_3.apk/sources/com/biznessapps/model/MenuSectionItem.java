package com.biznessapps.model;

public class MenuSectionItem extends CommonListEntity {
    private String price;
    private String title = "";

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String price2) {
        this.price = price2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }
}
