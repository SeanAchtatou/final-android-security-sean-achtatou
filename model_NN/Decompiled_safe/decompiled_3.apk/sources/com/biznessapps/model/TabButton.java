package com.biznessapps.model;

public class TabButton extends CommonListEntity {
    private Tab tab;

    public Tab getTab() {
        return this.tab;
    }

    public void setTab(Tab tab2) {
        this.tab = tab2;
    }

    public TabButton(Tab tab2) {
        this.tab = tab2;
    }
}
