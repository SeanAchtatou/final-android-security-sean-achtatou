package com.biznessapps.model;

public class RssItem extends CommonListEntity {
    private String audioUrl;
    private String creator;
    private String description;
    private String icon;
    private String imageUrl;
    private String link;
    private String section;
    private String subtitle;
    private String summary;
    private String tintColor;
    private String title;

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary2) {
        this.summary = summary2;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator2) {
        this.creator = creator2;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public void setSubtitle(String subtitle2) {
        this.subtitle = subtitle2;
    }

    public String getAudioUrl() {
        return this.audioUrl;
    }

    public void setAudioUrl(String audioUrl2) {
        this.audioUrl = audioUrl2;
    }

    public String getSection() {
        return this.section;
    }

    public void setSection(String section2) {
        this.section = section2;
    }

    public String getTintColor() {
        return this.tintColor;
    }

    public void setTintColor(String tintColor2) {
        this.tintColor = tintColor2;
    }
}
