package com.biznessapps.model;

public class LocationOpeningTime {
    private String day = "";
    private String openFrom = "";
    private String openTo = "";

    public String getDay() {
        return this.day;
    }

    public void setDay(String day2) {
        this.day = day2;
    }

    public String getOpenFrom() {
        return this.openFrom;
    }

    public void setOpenFrom(String openFrom2) {
        this.openFrom = openFrom2;
    }

    public String getOpenTo() {
        return this.openTo;
    }

    public void setOpenTo(String openTo2) {
        this.openTo = openTo2;
    }
}
