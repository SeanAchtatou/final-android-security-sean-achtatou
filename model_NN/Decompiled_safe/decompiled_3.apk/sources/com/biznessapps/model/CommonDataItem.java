package com.biznessapps.model;

public class CommonDataItem {
    protected String image;

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }
}
