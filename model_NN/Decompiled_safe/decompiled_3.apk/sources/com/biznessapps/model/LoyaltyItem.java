package com.biznessapps.model;

import java.io.Serializable;
import java.util.List;

public class LoyaltyItem extends CommonListEntity {
    private List<LoyaltyCardItem> coupons;
    private String lockerImageUrl;

    public String getLockerImageUrl() {
        return this.lockerImageUrl;
    }

    public void setLockerImageUrl(String lockerImageUrl2) {
        this.lockerImageUrl = lockerImageUrl2;
    }

    public List<LoyaltyCardItem> getCoupons() {
        return this.coupons;
    }

    public void setCoupons(List<LoyaltyCardItem> coupons2) {
        this.coupons = coupons2;
    }

    public static class LoyaltyCardItem implements Serializable {
        private String couponCode;
        private String couponId;
        private boolean isApproved;
        private boolean isLast;
        private boolean isLocked = true;

        public boolean isLocked() {
            return this.isLocked;
        }

        public void setLocked(boolean isLocked2) {
            this.isLocked = isLocked2;
        }

        public boolean isApproved() {
            return this.isApproved;
        }

        public void setApproved(boolean isApproved2) {
            this.isApproved = isApproved2;
        }

        public String getCouponCode() {
            return this.couponCode;
        }

        public void setCouponCode(String couponCode2) {
            this.couponCode = couponCode2;
        }

        public String getCouponId() {
            return this.couponId;
        }

        public void setCouponId(String couponId2) {
            this.couponId = couponId2;
        }

        public boolean isLast() {
            return this.isLast;
        }

        public void setLast(boolean isLast2) {
            this.isLast = isLast2;
        }
    }
}
