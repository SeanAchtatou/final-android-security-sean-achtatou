package com.biznessapps.model;

import java.io.Serializable;
import java.util.ArrayList;

public class AroundUsItem implements Serializable {
    private String greenColor = "00FF00";
    private String greenTextColor = "000000";
    private String greenTitle;
    private String image;
    private ArrayList<PoiItem> poi = new ArrayList<>();
    private String purpleColor = "800080";
    private String purpleTextColor = "FFFFFF";
    private String purpleTitle;
    private String redColor = "FF0000";
    private String redTextColor = "FFFFFF";
    private String redTitle;
    private boolean useInMemoryImage;

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }

    public String getGreenTitle() {
        return this.greenTitle;
    }

    public void setGreenTitle(String greenTitle2) {
        this.greenTitle = greenTitle2;
    }

    public String getPurpleTitle() {
        return this.purpleTitle;
    }

    public void setPurpleTitle(String purpleTitle2) {
        this.purpleTitle = purpleTitle2;
    }

    public String getRedTitle() {
        return this.redTitle;
    }

    public void setRedTitle(String redTitle2) {
        this.redTitle = redTitle2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getGreenColor() {
        return this.greenColor;
    }

    public void setGreenColor(String greenColor2) {
        this.greenColor = greenColor2;
    }

    public String getGreenTextColor() {
        return this.greenTextColor;
    }

    public void setGreenTextColor(String greenTextColor2) {
        this.greenTextColor = greenTextColor2;
    }

    public String getPurpleColor() {
        return this.purpleColor;
    }

    public void setPurpleColor(String purpleColor2) {
        this.purpleColor = purpleColor2;
    }

    public String getPurpleTextColor() {
        return this.purpleTextColor;
    }

    public void setPurpleTextColor(String purpleTextColor2) {
        this.purpleTextColor = purpleTextColor2;
    }

    public String getRedColor() {
        return this.redColor;
    }

    public void setRedColor(String redColor2) {
        this.redColor = redColor2;
    }

    public String getRedTextColor() {
        return this.redTextColor;
    }

    public void setRedTextColor(String redTextColor2) {
        this.redTextColor = redTextColor2;
    }

    public ArrayList<PoiItem> getPoi() {
        return this.poi;
    }

    public void setPoi(ArrayList<PoiItem> poi2) {
        this.poi = poi2;
    }

    public static class PoiItem extends CommonListEntity {
        private String categoryName;
        private String color;
        private String latitude;
        private LocationItem location;
        private String longitude;
        private String name;

        public LocationItem getLocation() {
            return this.location;
        }

        public void setLocation(LocationItem location2) {
            this.location = location2;
        }

        public String getColor() {
            return this.color;
        }

        public void setColor(String color2) {
            this.color = color2;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public String getLatitude() {
            return this.latitude;
        }

        public void setLatitude(String latitude2) {
            this.latitude = latitude2;
        }

        public String getLongitude() {
            return this.longitude;
        }

        public void setLongitude(String longitude2) {
            this.longitude = longitude2;
        }

        public String getCategoryName() {
            return this.categoryName;
        }

        public void setCategoryName(String categoryName2) {
            this.categoryName = categoryName2;
        }
    }
}
