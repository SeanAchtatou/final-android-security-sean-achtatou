package com.biznessapps.model.flickr;

import com.google.gson.annotations.SerializedName;

public class Photosets {
    @SerializedName("photoset")
    private Photoset[] photosetsArray;

    public Photoset[] getPhotosetsArray() {
        return this.photosetsArray;
    }

    public void setPhotosetsArray(Photoset[] photosetsArray2) {
        this.photosetsArray = photosetsArray2;
    }
}
