package com.biznessapps.model.flickr;

import com.google.gson.annotations.SerializedName;

public class Title {
    @SerializedName("_content")
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }
}
