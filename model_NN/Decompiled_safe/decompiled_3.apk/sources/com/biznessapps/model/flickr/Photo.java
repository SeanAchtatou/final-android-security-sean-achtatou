package com.biznessapps.model.flickr;

public class Photo {
    private String farm;
    private String id;
    private String secret;
    private String server;
    private String title;
    private String url;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public String getFarm() {
        return this.farm;
    }

    public void setFarm(String farm2) {
        this.farm = farm2;
    }
}
