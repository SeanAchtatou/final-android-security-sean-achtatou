package com.biznessapps.model.flickr;

public class RespGalleryPhotos {
    private Photos photos;

    public Photos getPhotos() {
        return this.photos;
    }

    public void setPhotos(Photos photos2) {
        this.photos = photos2;
    }
}
