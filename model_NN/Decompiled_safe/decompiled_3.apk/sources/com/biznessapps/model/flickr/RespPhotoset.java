package com.biznessapps.model.flickr;

public class RespPhotoset {
    private Photoset photoset;

    public Photoset getPhotoset() {
        return this.photoset;
    }

    public void setPhotoset(Photoset photoset2) {
        this.photoset = photoset2;
    }
}
