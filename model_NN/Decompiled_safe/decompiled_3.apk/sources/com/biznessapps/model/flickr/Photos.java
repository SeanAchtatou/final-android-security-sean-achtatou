package com.biznessapps.model.flickr;

import com.google.gson.annotations.SerializedName;

public class Photos {
    @SerializedName("photo")
    private Photo[] photosArray;

    public Photo[] getPhotosArray() {
        return this.photosArray;
    }

    public void setPhotosArray(Photo[] photosArray2) {
        this.photosArray = photosArray2;
    }
}
