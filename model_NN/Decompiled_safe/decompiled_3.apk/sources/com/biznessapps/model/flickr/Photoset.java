package com.biznessapps.model.flickr;

import java.util.List;

public class Photoset {
    private String description;
    private String farm;
    private String id;
    private List<Photo> photos;
    private String primary;
    private String secret;
    private String server;
    private String title;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public List<Photo> getPhotos() {
        return this.photos;
    }

    public void setPhotos(List<Photo> photos2) {
        this.photos = photos2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getPrimary() {
        return this.primary;
    }

    public void setPrimary(String primary2) {
        this.primary = primary2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public String getFarm() {
        return this.farm;
    }

    public void setFarm(String farm2) {
        this.farm = farm2;
    }
}
