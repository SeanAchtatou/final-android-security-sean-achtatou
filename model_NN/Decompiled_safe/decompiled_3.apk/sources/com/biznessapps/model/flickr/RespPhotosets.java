package com.biznessapps.model.flickr;

public class RespPhotosets {
    private Photosets photosets;

    public Photosets getPhotosets() {
        return this.photosets;
    }

    public void setPhotosets(Photosets photosets2) {
        this.photosets = photosets2;
    }
}
