package com.biznessapps.model.flickr;

public class RespUser {
    private User user;

    public User getUser() {
        return this.user;
    }

    public void setUser(User user2) {
        this.user = user2;
    }
}
