package com.biznessapps.model.flickr;

import com.google.gson.annotations.SerializedName;

public class Galleries {
    @SerializedName("gallery")
    private Gallery[] galleriesArray;

    public Gallery[] getGalleriesArray() {
        return this.galleriesArray;
    }

    public void setGalleriesArray(Gallery[] galleriesArray2) {
        this.galleriesArray = galleriesArray2;
    }
}
