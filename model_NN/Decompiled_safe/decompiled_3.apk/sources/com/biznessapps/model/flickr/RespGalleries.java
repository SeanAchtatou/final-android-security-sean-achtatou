package com.biznessapps.model.flickr;

public class RespGalleries {
    private Galleries galleries;

    public Galleries getGalleries() {
        return this.galleries;
    }

    public void setGalleries(Galleries galleries2) {
        this.galleries = galleries2;
    }
}
