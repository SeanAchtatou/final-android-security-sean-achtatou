package com.biznessapps.model.flickr;

import com.biznessapps.model.CommonListEntity;
import java.util.List;

public class GalleryAlbum extends CommonListEntity {
    private static final long serialVersionUID = 5974460151577451900L;
    private List<String> urls;

    public List<String> getUrls() {
        return this.urls;
    }

    public void setUrls(List<String> urls2) {
        this.urls = urls2;
    }
}
