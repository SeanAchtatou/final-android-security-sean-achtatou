package com.biznessapps.model.flickr;

import com.google.gson.annotations.SerializedName;

public class Gallery {
    private String farm;
    private String id;
    @SerializedName("photo")
    private Photo[] photosArray;
    private String primary;
    private String secret;
    private String server;
    private Title title;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public String getPrimary() {
        return this.primary;
    }

    public void setPrimary(String primary2) {
        this.primary = primary2;
    }

    public String getSecret() {
        return this.secret;
    }

    public void setSecret(String secret2) {
        this.secret = secret2;
    }

    public String getServer() {
        return this.server;
    }

    public void setServer(String server2) {
        this.server = server2;
    }

    public String getFarm() {
        return this.farm;
    }

    public void setFarm(String farm2) {
        this.farm = farm2;
    }

    public Title getTitle() {
        return this.title;
    }

    public void setTitle(Title title2) {
        this.title = title2;
    }

    public Photo[] getPhotosArray() {
        return this.photosArray;
    }

    public void setPhotosArray(Photo[] photosArray2) {
        this.photosArray = photosArray2;
    }
}
