package com.biznessapps.model;

public class MessageItem extends CommonListEntity {
    public static final int MESSAGE_ORDINARY_TYPE = 0;
    public static final int MESSAGE_TAB_CONTENT_TYPE = 2;
    public static final int MESSAGE_TEMPLATE_TYPE = 3;
    public static final int MESSAGE_WEB_URL_TYPE = 1;
    private String categoryId;
    private String date;
    private String detailId;
    private String tabId;
    private int type;
    private String url;

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getTabId() {
        return this.tabId;
    }

    public void setTabId(String tabId2) {
        this.tabId = tabId2;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public void setCategoryId(String categoryId2) {
        this.categoryId = categoryId2;
    }

    public String getDetailId() {
        return this.detailId;
    }

    public void setDetailId(String detailId2) {
        this.detailId = detailId2;
    }
}
