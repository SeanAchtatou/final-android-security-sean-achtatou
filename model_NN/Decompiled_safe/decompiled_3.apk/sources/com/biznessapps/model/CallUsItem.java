package com.biznessapps.model;

public class CallUsItem extends CommonListEntity {
    private String phone;

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }
}
