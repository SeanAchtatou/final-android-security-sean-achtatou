package com.biznessapps.model;

public class PlaylistItem extends CommonListEntity {
    private static final long serialVersionUID = 2431180497571441726L;
    private String album;
    private String albumArt;
    private String artist;
    private String background;
    private String header;
    private String itune;
    private int onSale;
    private String previewUrl;
    private String tintColor;

    public String getArtist() {
        return this.artist;
    }

    public void setArtist(String artist2) {
        this.artist = artist2;
    }

    public String getAlbum() {
        return this.album;
    }

    public void setAlbum(String album2) {
        this.album = album2;
    }

    public String getItune() {
        return this.itune;
    }

    public void setItune(String itune2) {
        this.itune = itune2;
    }

    public String getAlbumArt() {
        return this.albumArt;
    }

    public void setAlbumArt(String albumArt2) {
        this.albumArt = albumArt2;
    }

    public String getPreviewUrl() {
        return this.previewUrl;
    }

    public void setPreviewUrl(String previewUrl2) {
        this.previewUrl = previewUrl2;
    }

    public int getOnSale() {
        return this.onSale;
    }

    public void setOnSale(int onSale2) {
        this.onSale = onSale2;
    }

    public String getBackground() {
        return this.background;
    }

    public void setBackground(String background2) {
        this.background = background2;
    }

    public String getHeader() {
        return this.header;
    }

    public void setHeader(String header2) {
        this.header = header2;
    }

    public String getTintColor() {
        return this.tintColor;
    }

    public void setTintColor(String tintColor2) {
        this.tintColor = tintColor2;
    }
}
