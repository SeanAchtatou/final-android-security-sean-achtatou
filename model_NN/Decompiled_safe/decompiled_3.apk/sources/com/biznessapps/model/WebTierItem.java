package com.biznessapps.model;

public class WebTierItem extends CommonListEntity {
    private String url;

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
