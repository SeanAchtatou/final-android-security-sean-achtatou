package com.biznessapps.model;

public class FanWallComment extends CommonListEntity {
    private static final long serialVersionUID = -5518546941056570486L;
    private String comment;
    private double latitude;
    private double longitude;
    private String replies;
    private String timeAgo;
    private String uploadImageUrl;

    public String getTimeAgo() {
        return this.timeAgo;
    }

    public void setTimeAgo(String timeAgo2) {
        this.timeAgo = timeAgo2;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public String getReplies() {
        return this.replies;
    }

    public void setReplies(String replies2) {
        this.replies = replies2;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public String getUploadImageUrl() {
        return this.uploadImageUrl;
    }

    public void setUploadImageUrl(String uploadImageUrl2) {
        this.uploadImageUrl = uploadImageUrl2;
    }
}
