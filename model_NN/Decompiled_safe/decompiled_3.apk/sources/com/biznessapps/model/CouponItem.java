package com.biznessapps.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CouponItem extends CommonListEntity {
    private int checkinInterval;
    private int checkinTarget;
    private int checkinTargetMax;
    private String code;
    private String distance;
    private boolean enabled = true;
    private Date endDate;
    private long lastCheckinTime;
    private long lastRedeemedTime;
    private String latitude;
    private List<CouponsLocation> locations;
    private String longitude;
    private boolean reusable;
    private Date startDate;
    private String title;

    public void copyTo(CouponItem item) {
        if (this.lastCheckinTime > 0) {
            item.setLastCheckinTime(this.lastCheckinTime);
        }
        if (this.lastRedeemedTime > 0) {
            item.setLastRedeemedTime(this.lastRedeemedTime);
        }
        item.setCheckinTarget(this.checkinTarget);
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate2) {
        this.startDate = startDate2;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    public void setEndDate(Date endDate2) {
        this.endDate = endDate2;
    }

    public int getCheckinTarget() {
        return this.checkinTarget;
    }

    public void setCheckinTarget(int checkinTarget2) {
        this.checkinTarget = checkinTarget2;
    }

    public int getCheckinTargetMax() {
        return this.checkinTargetMax;
    }

    public void setCheckinTargetMax(int checkinTargetMax2) {
        this.checkinTargetMax = checkinTargetMax2;
    }

    public int getCheckinInterval() {
        return this.checkinInterval;
    }

    public void setCheckinInterval(int checkinInterval2) {
        this.checkinInterval = checkinInterval2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getDistance() {
        return this.distance;
    }

    public void setDistance(String distance2) {
        this.distance = distance2;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code2) {
        this.code = code2;
    }

    public long getLastRedeemedTime() {
        return this.lastRedeemedTime;
    }

    public void setLastRedeemedTime(long lastRedeemedTime2) {
        this.lastRedeemedTime = lastRedeemedTime2;
    }

    public long getLastCheckinTime() {
        return this.lastCheckinTime;
    }

    public void setLastCheckinTime(long lastCheckinTime2) {
        this.lastCheckinTime = lastCheckinTime2;
    }

    public List<CouponsLocation> getLocations() {
        return this.locations;
    }

    public void setLocations(List<CouponsLocation> locations2) {
        this.locations = locations2;
    }

    public boolean isReusable() {
        return this.reusable;
    }

    public void setReusable(boolean reusable2) {
        this.reusable = reusable2;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled2) {
        this.enabled = enabled2;
    }

    public static class CouponsLocation implements Serializable {
        private static final long serialVersionUID = 429401972520836615L;
        private String couponName;
        private String latitude;
        private String longitude;

        public String getCouponName() {
            return this.couponName;
        }

        public void setCouponName(String couponName2) {
            this.couponName = couponName2;
        }

        public String getLatitude() {
            return this.latitude;
        }

        public void setLatitude(String latitude2) {
            this.latitude = latitude2;
        }

        public String getLongitude() {
            return this.longitude;
        }

        public void setLongitude(String longitude2) {
            this.longitude = longitude2;
        }
    }
}
