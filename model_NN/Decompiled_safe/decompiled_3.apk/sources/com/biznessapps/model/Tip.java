package com.biznessapps.model;

public class Tip {
    private String customButton;
    private String image;
    private boolean useInMemoryImage;

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getCustomButton() {
        return this.customButton;
    }

    public void setCustomButton(String customButton2) {
        this.customButton = customButton2;
    }
}
