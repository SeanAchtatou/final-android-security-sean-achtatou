package com.biznessapps.model;

import com.biznessapps.constants.ServerConstants;
import com.biznessapps.model.flickr.GalleryAlbum;
import com.biznessapps.utils.StringUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GalleryData implements Serializable {
    private List<GalleryAlbum> albums;
    private String apiKey;
    private boolean displayPhotosets;
    private List<Item> items = new ArrayList();
    private boolean useCoverflow;
    private String userId;

    public List<GalleryAlbum> getAlbums() {
        return this.albums;
    }

    public void setAlbums(List<GalleryAlbum> albums2) {
        this.albums = albums2;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId2) {
        this.userId = userId2;
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public void setApiKey(String apiKey2) {
        this.apiKey = apiKey2;
    }

    public boolean isDisplayPhotosets() {
        return this.displayPhotosets;
    }

    public void setDisplayPhotosets(boolean displayPhotosets2) {
        this.displayPhotosets = displayPhotosets2;
    }

    public boolean isUseCoverflow() {
        return this.useCoverflow;
    }

    public void setUseCoverflow(boolean useCoverflow2) {
        this.useCoverflow = useCoverflow2;
    }

    public List<String> getItemsUrls() {
        String url;
        List<String> urls = new ArrayList<>();
        for (Item item : this.items) {
            if (StringUtils.isEmpty(item.getFullUrl())) {
                url = String.format(ServerConstants.GALLERY_THUMBNAILS, item.getId());
            } else {
                url = item.getFullUrl();
            }
            urls.add(url);
        }
        return urls;
    }

    public List<Item> getItems() {
        return this.items;
    }

    public void setItems(List<Item> items2) {
        this.items = items2;
    }

    public void setImageItems(List<String> urls) {
        List<Item> newItems = new ArrayList<>();
        for (String url : urls) {
            Item item = new Item();
            item.setFullUrl(url);
            newItems.add(item);
        }
        this.items = newItems;
    }

    public static class Item implements Serializable {
        private String fullUrl;
        private String height;
        private String id;
        private String info;
        private String width;

        public String getFullUrl() {
            return this.fullUrl;
        }

        public void setFullUrl(String fullUrl2) {
            this.fullUrl = fullUrl2;
        }

        public String getInfo() {
            return this.info;
        }

        public void setInfo(String info2) {
            this.info = info2;
        }

        public String getId() {
            return this.id;
        }

        public void setId(String id2) {
            this.id = id2;
        }

        public String getWidth() {
            return this.width;
        }

        public void setWidth(String width2) {
            this.width = width2;
        }

        public String getHeight() {
            return this.height;
        }

        public void setHeight(String height2) {
            this.height = height2;
        }
    }
}
