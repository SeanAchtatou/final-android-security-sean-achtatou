package com.biznessapps.model;

public class EmailPhotoItem {
    private String customButton;
    private String description;
    private String email;
    private String image;
    private String subject;
    private boolean useInMemoryImage;

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getCustomButton() {
        return this.customButton;
    }

    public void setCustomButton(String customButton2) {
        this.customButton = customButton2;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public String getSubject() {
        return this.subject;
    }

    public void setSubject(String subject2) {
        this.subject = subject2;
    }
}
