package com.biznessapps.model;

public class ReservationBillingAddressItem extends CommonListEntity {
    private static final long serialVersionUID = 1589082511549401884L;
    private String address1;
    private String address2;
    private String city;
    private String company;
    private String country;
    private String fax;
    private String firstName;
    private String lastName;
    private String phone;
    private String state;
    private String zipCode;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName2) {
        this.firstName = firstName2;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName2) {
        this.lastName = lastName2;
    }

    public String getAddress1() {
        return this.address1;
    }

    public void setAddress1(String address12) {
        this.address1 = address12;
    }

    public String getAddress2() {
        return this.address2;
    }

    public void setAddress2(String address22) {
        this.address2 = address22;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country2) {
        this.country = country2;
    }

    public String getCity() {
        return this.city;
    }

    public void setCity(String city2) {
        this.city = city2;
    }

    public String getZipCode() {
        return this.zipCode;
    }

    public void setZipCode(String zipCode2) {
        this.zipCode = zipCode2;
    }

    public String getState() {
        return this.state;
    }

    public void setState(String state2) {
        this.state = state2;
    }

    public String getCompany() {
        return this.company;
    }

    public void setCompany(String company2) {
        this.company = company2;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax2) {
        this.fax = fax2;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String phone2) {
        this.phone = phone2;
    }
}
