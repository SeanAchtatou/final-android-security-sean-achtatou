package com.biznessapps.model;

public class YoutubeRssItem extends CommonListEntity {
    private String creator;
    private String description;
    private String feedlinkCountHint;
    private String feedlinkHref;
    private String imageUrl;
    private String link;
    private String mediaThumbnailUrl;
    private String note;
    private String published;
    private String ratingAverage;
    private String section;
    private String statisticsViewCount;
    private String subtitle;
    private String summary;
    private String title;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public String getSummary() {
        return this.summary;
    }

    public void setSummary(String summary2) {
        this.summary = summary2;
    }

    public String getCreator() {
        return this.creator;
    }

    public void setCreator(String creator2) {
        this.creator = creator2;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2;
    }

    public String getSubtitle() {
        return this.subtitle;
    }

    public void setSubtitle(String subtitle2) {
        this.subtitle = subtitle2;
    }

    public String getSection() {
        return this.section;
    }

    public void setSection(String section2) {
        this.section = section2;
    }

    public String getPublished() {
        return this.published;
    }

    public void setPublished(String published2) {
        this.published = published2;
    }

    public String getFeedlinkHref() {
        return this.feedlinkHref;
    }

    public void setFeedlinkHref(String feedlinkHref2) {
        this.feedlinkHref = feedlinkHref2;
    }

    public String getFeedlinkCountHint() {
        return this.feedlinkCountHint;
    }

    public void setFeedlinkCountHint(String feedlinkCountHint2) {
        this.feedlinkCountHint = feedlinkCountHint2;
    }

    public String getMediaThumbnailUrl() {
        return this.mediaThumbnailUrl;
    }

    public void setMediaThumbnailUrl(String mediaThumbnailUrl2) {
        this.mediaThumbnailUrl = mediaThumbnailUrl2;
    }

    public String getRatingAverage() {
        return this.ratingAverage;
    }

    public void setRatingAverage(String ratingAverage2) {
        this.ratingAverage = ratingAverage2;
    }

    public String getStatisticsViewCount() {
        return this.statisticsViewCount;
    }

    public void setStatisticsViewCount(String statisticsViewCount2) {
        this.statisticsViewCount = statisticsViewCount2;
    }

    public String getNote() {
        return this.note;
    }

    public void setNote(String note2) {
        this.note = note2;
    }
}
