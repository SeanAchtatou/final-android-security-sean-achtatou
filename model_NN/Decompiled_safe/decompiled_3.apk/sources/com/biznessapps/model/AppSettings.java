package com.biznessapps.model;

public class AppSettings {
    public static final int BOTTOM_MENU_NAVIGATION = 3;
    public static final int FADE_MODE = 2;
    public static final int LEFT_MENU_NAVIGATION = 0;
    public static final int NO_ANIMATION_MODE = 0;
    public static final int RIGHT_MENU_NAVIGATION = 2;
    public static final int SLIDING_MODE = 1;
    public static final int TOP_MENU_NAVIGATION = 1;
    private static final String TRANSPARENCY_ROW_VALUE = "80";
    private String adWhirlID;
    private int animationMode = 0;
    private String appId;
    private String buttonBgColor;
    private String buttonTextColor;
    private int cols;
    private String consumerKey;
    private String consumerSecret;
    private String defaultListBgColor;
    private String evenRowColor;
    private String evenRowTextColor;
    private String facebookAppId;
    private String featureTextColor;
    private String footerTint;
    private String gaAccountId;
    private String globalBgColor;
    private String globalHeaderUrl;
    private boolean hasCallButton;
    private boolean hasDirectionButton;
    private boolean hasTellFriendButton;
    private String headerSrc;
    private String headerTint;
    private String headerTintOpacity;
    private boolean isActive;
    private boolean isMessageIconLeft;
    private boolean isMessageIconTop;
    private boolean isMessageIconUsed;
    private boolean isProtected;
    private boolean mailingListPrompt;
    private int messageIconOpacity;
    private String messateLinkedTab;
    private boolean moreButtonNavigation;
    private String moreIconUrl;
    private String moreTabBg;
    private String moreTabTabletBg;
    private boolean musicOnFront;
    private boolean musicOnTop;
    private String navTintOpacity;
    private String navigBarColor;
    private String navigBarTextColor;
    private String navigBarTextShadowColor;
    private int navigationMenuType = 3;
    private String oddRowColor;
    private String oddRowTextColor;
    private boolean offlineCachingPrompt;
    private String pushingIp;
    private int rows;
    private String rssIconUrl;
    private String sectionBarColor;
    private String sectionBarTextColor;
    private boolean showText = true;
    private String tabFont;
    private String tabIcon;
    private long tabId;
    private String tabSrc;
    private String tabText;
    private String tabTint;
    private float tabTintOpacity;
    private boolean useNewDesign;
    private String useTextColors;

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive2) {
        this.isActive = isActive2;
    }

    public int getRows() {
        return this.rows;
    }

    public void setRows(int rows2) {
        this.rows = rows2;
    }

    public int getCols() {
        return this.cols;
    }

    public void setCols(int cols2) {
        this.cols = cols2;
    }

    public String getTabSrc() {
        return this.tabSrc;
    }

    public void setTabSrc(String tabSrc2) {
        this.tabSrc = tabSrc2;
    }

    public String getTabTint() {
        return this.tabTint;
    }

    public void setTabTint(String tabTint2) {
        this.tabTint = tabTint2;
    }

    public float getTabTintOpacity() {
        return this.tabTintOpacity;
    }

    public void setTabTintOpacity(float tabTintOpacity2) {
        this.tabTintOpacity = tabTintOpacity2;
    }

    public String getTabIcon() {
        return this.tabIcon;
    }

    public void setTabIcon(String tabIcon2) {
        this.tabIcon = tabIcon2;
    }

    public String getTabText() {
        return this.tabText;
    }

    public void setTabText(String tabText2) {
        this.tabText = tabText2;
    }

    public String getTabFont() {
        return this.tabFont;
    }

    public void setTabFont(String tabFont2) {
        this.tabFont = tabFont2;
    }

    public String getHeaderSrc() {
        return this.headerSrc;
    }

    public void setHeaderSrc(String headerSrc2) {
        this.headerSrc = headerSrc2;
    }

    public String getHeaderTint() {
        return this.headerTint;
    }

    public void setHeaderTint(String headerTint2) {
        this.headerTint = headerTint2;
    }

    public String getHeaderTintOpacity() {
        return this.headerTintOpacity;
    }

    public void setHeaderTintOpacity(String headerTintOpacity2) {
        this.headerTintOpacity = headerTintOpacity2;
    }

    public String getFooterTint() {
        return this.footerTint;
    }

    public void setFooterTint(String footerTint2) {
        this.footerTint = footerTint2;
    }

    public String getGlobalHeaderUrl() {
        return this.globalHeaderUrl;
    }

    public void setGlobalHeaderUrl(String globalHeaderUrl2) {
        this.globalHeaderUrl = globalHeaderUrl2;
    }

    public String getNavTintOpacity() {
        return this.navTintOpacity;
    }

    public void setNavTintOpacity(String navTintOpacity2) {
        this.navTintOpacity = navTintOpacity2;
    }

    public boolean isUseNewDesign() {
        return this.useNewDesign;
    }

    public void setUseNewDesign(boolean useNewDesign2) {
        this.useNewDesign = useNewDesign2;
    }

    public String getRssIconUrl() {
        return this.rssIconUrl;
    }

    public void setRssIconUrl(String rssIconUrl2) {
        this.rssIconUrl = rssIconUrl2;
    }

    public String getOddRowColor() {
        return this.oddRowColor;
    }

    public void setOddRowColor(String oddRowColor2) {
        this.oddRowColor = TRANSPARENCY_ROW_VALUE.concat(oddRowColor2);
    }

    public String getButtonTextColor() {
        return this.buttonTextColor;
    }

    public void setButtonTextColor(String buttonTextColor2) {
        this.buttonTextColor = buttonTextColor2;
    }

    public String getButtonBgColor() {
        return this.buttonBgColor;
    }

    public void setButtonBgColor(String buttonBgColor2) {
        this.buttonBgColor = buttonBgColor2;
    }

    public String getFeatureTextColor() {
        return this.featureTextColor;
    }

    public void setFeatureTextColor(String featureTextColor2) {
        this.featureTextColor = featureTextColor2;
    }

    public String getEvenRowColor() {
        return this.evenRowColor;
    }

    public void setEvenRowColor(String evenRowColor2) {
        this.evenRowColor = TRANSPARENCY_ROW_VALUE.concat(evenRowColor2);
    }

    public String getDefaultListBgColor() {
        this.defaultListBgColor = this.globalBgColor;
        return this.defaultListBgColor;
    }

    public String getMoreTabBg() {
        return this.moreTabBg;
    }

    public void setMoreTabBg(String moreTabBg2) {
        this.moreTabBg = moreTabBg2;
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String appId2) {
        this.appId = appId2;
    }

    public String getOddRowTextColor() {
        return this.oddRowTextColor;
    }

    public void setOddRowTextColor(String oddRowTextColor2) {
        this.oddRowTextColor = oddRowTextColor2;
    }

    public String getEvenRowTextColor() {
        return this.evenRowTextColor;
    }

    public void setEvenRowTextColor(String evenRowTextColor2) {
        this.evenRowTextColor = evenRowTextColor2;
    }

    public String getNavigBarColor() {
        return this.navigBarColor;
    }

    public void setNavigBarColor(String navigBarColor2) {
        this.navigBarColor = navigBarColor2;
    }

    public String getAdWhirlID() {
        return this.adWhirlID;
    }

    public void setAdWhirlID(String adWhirlID2) {
        this.adWhirlID = adWhirlID2;
    }

    public String getUseTextColors() {
        return this.useTextColors;
    }

    public void setUseTextColors(String useTextColors2) {
        this.useTextColors = useTextColors2;
    }

    public boolean isMailingListPrompt() {
        return this.mailingListPrompt;
    }

    public void setMailingListPrompt(boolean mailingListPrompt2) {
        this.mailingListPrompt = mailingListPrompt2;
    }

    public boolean isOfflineCachingPrompt() {
        return this.offlineCachingPrompt;
    }

    public void setOfflineCachingPrompt(boolean offlineCachingPrompt2) {
        this.offlineCachingPrompt = offlineCachingPrompt2;
    }

    public boolean isMusicOnFront() {
        return this.musicOnFront;
    }

    public void setMusicOnFront(boolean musicOnFront2) {
        this.musicOnFront = musicOnFront2;
    }

    public boolean isMusicOnTop() {
        return this.musicOnTop;
    }

    public void setMusicOnTop(boolean musicOnTop2) {
        this.musicOnTop = musicOnTop2;
    }

    public long getMailingListTabId() {
        return this.tabId;
    }

    public void setMailingListTabId(long tabId2) {
        this.tabId = tabId2;
    }

    public String getNavigBarTextColor() {
        return this.navigBarTextColor;
    }

    public void setNavigBarTextColor(String navigBarTextColor2) {
        this.navigBarTextColor = navigBarTextColor2;
    }

    public String getNavigBarTextShadowColor() {
        return this.navigBarTextShadowColor;
    }

    public void setNavigBarTextShadowColor(String navigBarTextShadowColor2) {
        this.navigBarTextShadowColor = navigBarTextShadowColor2;
    }

    public String getSectionBarColor() {
        return this.sectionBarColor;
    }

    public void setSectionBarColor(String sectionBarColor2) {
        this.sectionBarColor = sectionBarColor2;
    }

    public String getSectionBarTextColor() {
        return this.sectionBarTextColor;
    }

    public void setSectionBarTextColor(String sectionBarTextColor2) {
        this.sectionBarTextColor = sectionBarTextColor2;
    }

    public String getMoreTabTabletBg() {
        return this.moreTabTabletBg;
    }

    public void setMoreTabTabletBg(String moreTabTabletBg2) {
        this.moreTabTabletBg = moreTabTabletBg2;
    }

    public String getMoreIconUrl() {
        return this.moreIconUrl;
    }

    public void setMoreIconUrl(String moreIconUrl2) {
        this.moreIconUrl = moreIconUrl2;
    }

    public boolean hasMoreButtonNavigation() {
        return this.moreButtonNavigation;
    }

    public void setMoreButtonNavigation(boolean moreButtonNavigation2) {
        this.moreButtonNavigation = moreButtonNavigation2;
    }

    public int getAnimationMode() {
        return this.animationMode;
    }

    public void setAnimationMode(int animationMode2) {
        this.animationMode = animationMode2;
    }

    public int getNavigationMenuType() {
        return this.navigationMenuType;
    }

    public void setNavigationMenuType(int navigationMenuType2) {
        this.navigationMenuType = navigationMenuType2;
    }

    public String getFacebookAppId() {
        return this.facebookAppId;
    }

    public void setFacebookAppId(String facebookAppId2) {
        this.facebookAppId = facebookAppId2;
    }

    public String getPushingIp() {
        return this.pushingIp;
    }

    public void setPushingIp(String pushingIp2) {
        this.pushingIp = pushingIp2;
    }

    public String getGaAccountId() {
        return this.gaAccountId;
    }

    public void setGaAccountId(String gaAccountId2) {
        this.gaAccountId = gaAccountId2;
    }

    public boolean isShowText() {
        return this.showText;
    }

    public void setShowText(boolean showText2) {
        this.showText = showText2;
    }

    public boolean isProtected() {
        return this.isProtected;
    }

    public void setProtected(boolean isProtected2) {
        this.isProtected = isProtected2;
    }

    public boolean isMessageIconUsed() {
        return this.isMessageIconUsed;
    }

    public void setMessageIconUsed(boolean isMessageIconUsed2) {
        this.isMessageIconUsed = isMessageIconUsed2;
    }

    public boolean isMessageIconLeft() {
        return this.isMessageIconLeft;
    }

    public void setMessageIconLeft(boolean isMessageIconLeft2) {
        this.isMessageIconLeft = isMessageIconLeft2;
    }

    public boolean isMessageIconTop() {
        return this.isMessageIconTop;
    }

    public void setMessageIconTop(boolean isMessageIconTop2) {
        this.isMessageIconTop = isMessageIconTop2;
    }

    public String getMessateLinkedTab() {
        return this.messateLinkedTab;
    }

    public void setMessateLinkedTab(String messateLinkedTab2) {
        this.messateLinkedTab = messateLinkedTab2;
    }

    public int getMessageIconOpacity() {
        return this.messageIconOpacity;
    }

    public void setMessageIconOpacity(int messageIconOpacity2) {
        this.messageIconOpacity = messageIconOpacity2;
    }

    public String getConsumerKey() {
        return this.consumerKey;
    }

    public void setConsumerKey(String consumerKey2) {
        this.consumerKey = consumerKey2;
    }

    public String getConsumerSecret() {
        return this.consumerSecret;
    }

    public void setConsumerSecret(String consumerSecret2) {
        this.consumerSecret = consumerSecret2;
    }

    public boolean hasCallButton() {
        return this.hasCallButton;
    }

    public void setHasCallButton(boolean hasCallButton2) {
        this.hasCallButton = hasCallButton2;
    }

    public boolean hasDirectionButton() {
        return this.hasDirectionButton;
    }

    public void setHasDirectionButton(boolean hasDirectionButton2) {
        this.hasDirectionButton = hasDirectionButton2;
    }

    public boolean hasTellFriendButton() {
        return this.hasTellFriendButton;
    }

    public void setHasTellFriendButton(boolean hasTellFriendButton2) {
        this.hasTellFriendButton = hasTellFriendButton2;
    }

    public String getGlobalBgColor() {
        return this.globalBgColor;
    }

    public void setGlobalBgColor(String globalBgColor2) {
        this.globalBgColor = globalBgColor2;
    }
}
