package com.biznessapps.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class App implements Serializable {
    private static final long serialVersionUID = -4252209056937922440L;
    private boolean hasManyImages;
    private String homeBackgroundImage;
    private String imageUrl;
    private List<String> imagesInOrder;
    private List<Tab> imagesLinkedTabs;
    private String latitude;
    private List<LocationItem> locations = new ArrayList();
    private String longitude;
    private String name;
    private List<Tab> subTabs;
    private String telephone;
    private boolean useInMemoryImage;

    public boolean hasManyImages() {
        return this.hasManyImages;
    }

    public void setHasManyImages(boolean hasManyImages2) {
        this.hasManyImages = hasManyImages2;
    }

    public List<String> getImagesInOrder() {
        return this.imagesInOrder;
    }

    public void setImagesInOrder(List<String> imagesInOrder2) {
        this.imagesInOrder = imagesInOrder2;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public List<Tab> getSubTabs() {
        return this.subTabs;
    }

    public void setSubTabs(List<Tab> subTabs2) {
        this.subTabs = subTabs2;
    }

    public List<Tab> getImagesLinkedTabs() {
        return this.imagesLinkedTabs;
    }

    public void setImagesLinkedTabs(List<Tab> imagesLinkedTabs2) {
        this.imagesLinkedTabs = imagesLinkedTabs2;
    }

    public boolean isHasManyImages() {
        return this.hasManyImages;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone2) {
        this.telephone = telephone2;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getHomeBackgroundImage() {
        return this.homeBackgroundImage;
    }

    public void setHomeBackgroundImage(String homeBackgroundImage2) {
        this.homeBackgroundImage = homeBackgroundImage2;
    }

    public List<LocationItem> getLocations() {
        return this.locations;
    }

    public void setLocations(List<LocationItem> locations2) {
        this.locations = locations2;
    }
}
