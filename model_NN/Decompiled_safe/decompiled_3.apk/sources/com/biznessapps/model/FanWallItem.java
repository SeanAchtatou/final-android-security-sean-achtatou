package com.biznessapps.model;

import java.util.ArrayList;
import java.util.List;

public class FanWallItem {
    private List<FanWallComment> comments = new ArrayList();
    private String image;
    private boolean useInMemoryImage;

    public List<FanWallComment> getComments() {
        return this.comments;
    }

    public void addComment(FanWallComment comment) {
        this.comments.add(comment);
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }
}
