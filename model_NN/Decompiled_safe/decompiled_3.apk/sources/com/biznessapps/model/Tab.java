package com.biznessapps.model;

import java.io.Serializable;
import java.util.List;

public class Tab implements Serializable {
    private static final long serialVersionUID = 1;
    private String customIcon;
    private boolean hasCustomDesign;
    private long id;
    private String image = "";
    private boolean isActive = true;
    private String itemId = "";
    private String label = "";
    private String lastUpdated = "";
    private String navigationController = "";
    private String sectionId = "";
    private int seq;
    private boolean showText = true;
    private List<Tab> subTabs;
    private String tabIcon;
    private String tabId;
    private String tabImageUrl;
    private String tabLabelFont;
    private String tabLabelText;
    private String tabLabelTextBgColor;
    private String tabLabelTextColor;
    private String tabSrc;
    private String tabText;
    private String tabTint;
    private float tabTintOpacity;
    private String url = "";
    private String viewController = "";

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label2) {
        this.label = label2;
    }

    public String getTabIcon() {
        return this.tabIcon;
    }

    public void setTabIcon(String tabIcon2) {
        this.tabIcon = tabIcon2;
    }

    public String getTabImageUrl() {
        return this.tabImageUrl;
    }

    public void setTabImageUrl(String tabImageUrl2) {
        this.tabImageUrl = tabImageUrl2;
    }

    public String getTabSrc() {
        return this.tabSrc;
    }

    public void setTabSrc(String tabSrc2) {
        this.tabSrc = tabSrc2;
    }

    public String getTabLabelFont() {
        return this.tabLabelFont;
    }

    public void setTabLabelFont(String tabLabelFont2) {
        this.tabLabelFont = tabLabelFont2;
    }

    public String getTabLabelTextColor() {
        return this.tabLabelTextColor;
    }

    public void setTabLabelTextColor(String tabLabelTextColor2) {
        this.tabLabelTextColor = tabLabelTextColor2;
    }

    public String getTabLabelText() {
        return this.tabLabelText;
    }

    public void setTabLabelText(String tabLabelText2) {
        this.tabLabelText = tabLabelText2;
    }

    public String getCustomIcon() {
        return this.customIcon;
    }

    public void setCustomIcon(String customIcon2) {
        this.customIcon = customIcon2;
    }

    public String getTabLabelTextBgColor() {
        return this.tabLabelTextBgColor;
    }

    public void setTabLabelTextBgColor(String tabLabelTextBgColor2) {
        this.tabLabelTextBgColor = tabLabelTextBgColor2;
    }

    public int getSeq() {
        return this.seq;
    }

    public void setSeq(int seq2) {
        this.seq = seq2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public void setActive(boolean isActive2) {
        this.isActive = isActive2;
    }

    public String getViewController() {
        return this.viewController;
    }

    public void setViewController(String viewController2) {
        this.viewController = viewController2;
    }

    public String getNavigationController() {
        return this.navigationController;
    }

    public void setNavigationController(String navigationController2) {
        this.navigationController = navigationController2;
    }

    public String getLastUpdated() {
        return this.lastUpdated;
    }

    public void setLastUpdated(String lastUpdated2) {
        this.lastUpdated = lastUpdated2;
    }

    public boolean hasCustomDesign() {
        return this.hasCustomDesign;
    }

    public void setHasCustomDesign(boolean hasCustomDesign2) {
        this.hasCustomDesign = hasCustomDesign2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getItemId() {
        return this.itemId;
    }

    public void setItemId(String itemId2) {
        this.itemId = itemId2;
    }

    public String getSectionId() {
        return this.sectionId;
    }

    public void setSectionId(String sectionId2) {
        this.sectionId = sectionId2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id2) {
        this.id = id2;
    }

    public String getTabId() {
        return this.tabId;
    }

    public void setTabId(String tabId2) {
        this.tabId = tabId2;
    }

    public List<Tab> getSubTabs() {
        return this.subTabs;
    }

    public void setSubTabs(List<Tab> subTabs2) {
        this.subTabs = subTabs2;
    }

    public boolean isShowText() {
        return this.showText;
    }

    public void setShowText(boolean showText2) {
        this.showText = showText2;
    }

    public String getTabTint() {
        return this.tabTint;
    }

    public void setTabTint(String tabTint2) {
        this.tabTint = tabTint2;
    }

    public float getTabTintOpacity() {
        return this.tabTintOpacity;
    }

    public void setTabTintOpacity(float tabTintOpacity2) {
        this.tabTintOpacity = tabTintOpacity2;
    }

    public String getTabText() {
        return this.tabText;
    }

    public void setTabText(String tabText2) {
        this.tabText = tabText2;
    }
}
