package com.biznessapps.model;

import java.util.List;

public class MailingListEntity {
    private List<Category> categories;
    private String customButton;
    private String description;
    private String image;
    private boolean useInMemoryImage;

    public boolean isUseInMemoryImage() {
        return this.useInMemoryImage;
    }

    public void setUseInMemoryImage(boolean useInMemoryImage2) {
        this.useInMemoryImage = useInMemoryImage2;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image2) {
        this.image = image2;
    }

    public String getCustomButton() {
        return this.customButton;
    }

    public void setCustomButton(String customButton2) {
        this.customButton = customButton2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public List<Category> getCategories() {
        return this.categories;
    }

    public void setCategories(List<Category> categories2) {
        this.categories = categories2;
    }

    public static class Category {
        private int id;
        private String name;
        private boolean selected = true;

        public int getId() {
            return this.id;
        }

        public void setId(int id2) {
            this.id = id2;
        }

        public String getName() {
            return this.name;
        }

        public void setName(String name2) {
            this.name = name2;
        }

        public boolean isSelected() {
            return this.selected;
        }

        public void setSelected(boolean selected2) {
            this.selected = selected2;
        }
    }
}
