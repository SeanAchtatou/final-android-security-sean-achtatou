package com.biznessapps.player;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.IInterface;
import android.util.Log;

public abstract class BaseServiceAccessor<T extends IInterface> {
    private static final String TAG = BaseServiceAccessor.class.getCanonicalName();
    private static final String UNBIND_SERVICE_ERROR = "Cann't unbind service";
    private Context context;
    protected T service;
    protected ServiceConnection serviceConnection;
    private Intent serviceIntent = null;

    /* access modifiers changed from: protected */
    public abstract T createServiceStub(IBinder iBinder);

    /* access modifiers changed from: protected */
    public abstract Class<?> getServiceClass();

    public T getService() {
        return this.service;
    }

    /* access modifiers changed from: protected */
    public ServiceConnection getServiceConnection() {
        if (this.serviceConnection == null) {
            this.serviceConnection = new ServiceConnection() {
                public void onServiceConnected(ComponentName classname, IBinder binder) {
                    BaseServiceAccessor.this.service = BaseServiceAccessor.this.createServiceStub(binder);
                }

                public void onServiceDisconnected(ComponentName name) {
                    BaseServiceAccessor.this.service = null;
                }
            };
        }
        return this.serviceConnection;
    }

    public BaseServiceAccessor(Context context2) {
        this.context = context2;
        onCreate();
    }

    public void onCreate() {
        this.serviceIntent = new Intent(this.context, getServiceClass());
        this.context.bindService(this.serviceIntent, getServiceConnection(), 1);
    }

    public void onDestroy() {
        try {
            this.context.unbindService(getServiceConnection());
        } catch (Exception e) {
            Log.e(TAG, UNBIND_SERVICE_ERROR, e);
        }
    }
}
