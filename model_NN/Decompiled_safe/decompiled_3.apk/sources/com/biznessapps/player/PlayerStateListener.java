package com.biznessapps.player;

public class PlayerStateListener {
    public void onStart(MusicItem item) {
    }

    public void onStop() {
    }

    public void onPause() {
    }

    public void onError(int error) {
    }

    public void onStateChanged(int newState) {
    }
}
