package com.biznessapps.player;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.biznessapps.player.IPlayerService;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PlayerService extends Service {
    private static List<PlayerStateListener> listeners = new ArrayList();
    /* access modifiers changed from: private */
    public int currentPosition = 0;
    /* access modifiers changed from: private */
    public MusicItem currentSong = null;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer;
    private MediaPlayer.OnBufferingUpdateListener onBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
        }
    };
    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            mp.stop();
            PlayerService.this.setState(2);
            Log.d("PreviewMediaPlayer", "onCompletion OK");
            synchronized (this) {
                PlayerService.access$608(PlayerService.this);
                if (PlayerService.this.currentPosition >= PlayerService.this.queue.size()) {
                    int unused = PlayerService.this.currentPosition = 0;
                }
                if (!PlayerService.this.queue.isEmpty()) {
                    PlayerService.this.play((MusicItem) PlayerService.this.queue.get(PlayerService.this.currentPosition));
                }
            }
        }
    };
    private MediaPlayer.OnErrorListener onErrorListener = new MediaPlayer.OnErrorListener() {
        public boolean onError(MediaPlayer mp, int what, int extra) {
            return true;
        }
    };
    private MediaPlayer.OnPreparedListener onPreparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mp) {
            if (mp != null) {
                mp.start();
            }
        }
    };
    /* access modifiers changed from: private */
    public PlayerState playerState;
    /* access modifiers changed from: private */
    public List<MusicItem> queue = new ArrayList();

    static /* synthetic */ int access$608(PlayerService x0) {
        int i = x0.currentPosition;
        x0.currentPosition = i + 1;
        return i;
    }

    public IBinder onBind(Intent intent) {
        return new IPlayerService.Stub() {
            public void play(MusicItem item) throws RemoteException {
                PlayerService.this.play(item);
            }

            public void stop() throws RemoteException {
                PlayerService.this.stop();
            }

            public void pause() throws RemoteException {
                PlayerService.this.pause();
            }

            public PlayerState getState() throws RemoteException {
                return PlayerService.this.getPlayerState();
            }

            public boolean isInState(int state) throws RemoteException {
                return PlayerService.this.isInState(state);
            }

            public long duration() throws RemoteException {
                return PlayerService.this.duration();
            }

            public void seek(long position) throws RemoteException {
                PlayerService.this.seek(position);
            }

            public long position() throws RemoteException {
                return PlayerService.this.position();
            }

            public void addUrlsQueue(List<MusicItem> items) throws RemoteException {
                PlayerService.this.addUrlsQueue(items);
            }

            public void addUrlQueue(MusicItem item) throws RemoteException {
                PlayerService.this.addUrlQueue(item);
            }

            public void clearQueue() throws RemoteException {
                PlayerService.this.clearQueue();
            }

            public int getCurrentPosition() throws RemoteException {
                return PlayerService.this.getCurrentPosition();
            }

            public void setCurrentPosition(int position) throws RemoteException {
                PlayerService.this.setCurrentPosition(position);
            }

            public List<MusicItem> getSongs() throws RemoteException {
                return PlayerService.this.getSongs();
            }

            public MusicItem getCurrentSong() throws RemoteException {
                return PlayerService.this.currentSong;
            }
        };
    }

    public void onCreate() {
        super.onCreate();
        this.playerState = new PlayerState();
        initPlayer();
    }

    /* access modifiers changed from: private */
    public void initPlayer() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
        this.mediaPlayer = new MediaPlayer();
        this.mediaPlayer.setAudioStreamType(3);
        this.mediaPlayer.setOnPreparedListener(this.onPreparedListener);
        this.mediaPlayer.setOnBufferingUpdateListener(this.onBufferingUpdateListener);
        this.mediaPlayer.setOnCompletionListener(this.onCompletionListener);
        this.mediaPlayer.setOnErrorListener(this.onErrorListener);
        setState(2);
    }

    /* access modifiers changed from: protected */
    public PlayerState getPlayerState() {
        return this.playerState;
    }

    public void play(final MusicItem musicItem) {
        new Thread(new Runnable() {
            public void run() {
                PlayerService.this.updateCurrentPosition(musicItem);
                try {
                    if (PlayerService.this.playerState.getState() == 3) {
                        PlayerService.this.mediaPlayer.start();
                        PlayerService.this.setState(1);
                    } else if (PlayerService.this.playerState.getState() == 1) {
                        PlayerService.this.mediaPlayer.pause();
                        PlayerService.this.setState(3);
                    } else {
                        PlayerService.this.initPlayer();
                        MusicItem currentItem = null;
                        if (musicItem != null && musicItem.isSingle()) {
                            currentItem = musicItem;
                        } else if (!PlayerService.this.queue.isEmpty()) {
                            currentItem = (MusicItem) PlayerService.this.queue.get(PlayerService.this.currentPosition);
                        }
                        if (currentItem != null) {
                            MusicItem unused = PlayerService.this.currentSong = currentItem;
                            PlayerService.this.mediaPlayer.setDataSource(currentItem.getUrl());
                            PlayerService.this.mediaPlayer.prepare();
                            PlayerService.this.mediaPlayer.start();
                            PlayerService.this.firePlayerStartEvent(currentItem);
                            PlayerService.this.setState(1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void pause() {
        if (this.mediaPlayer.isPlaying()) {
            this.mediaPlayer.pause();
        }
        setState(3);
    }

    public void onDestroy() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
        }
    }

    public boolean isInState(int state) {
        return this.playerState.getState() == state;
    }

    public void stop() {
        try {
            if (this.mediaPlayer.isPlaying()) {
                this.mediaPlayer.stop();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        setState(2);
    }

    public long duration() {
        return (long) this.mediaPlayer.getDuration();
    }

    public long position() {
        if (isInState(4)) {
            return -1;
        }
        return (long) this.mediaPlayer.getCurrentPosition();
    }

    public void seek(long position) {
        this.mediaPlayer.seekTo((int) position);
    }

    /* access modifiers changed from: protected */
    public void firePlayerStartEvent(MusicItem item) {
        for (PlayerStateListener listener : listeners) {
            listener.onStart(item);
        }
    }

    /* access modifiers changed from: protected */
    public void fireStateChangedEvent(int newState) {
        for (PlayerStateListener listener : listeners) {
            listener.onStateChanged(newState);
        }
    }

    /* access modifiers changed from: protected */
    public void firePlayerPauseEvent() {
    }

    /* access modifiers changed from: protected */
    public void firePlayerStopEvent() {
    }

    /* access modifiers changed from: protected */
    public void firePlayingEvent() {
    }

    /* access modifiers changed from: protected */
    public void setState(int state) {
        this.playerState.setState(state);
        fireStateChangedEvent(state);
    }

    public static void addListener(PlayerStateListener listener) {
        if (!listeners.contains(listener)) {
            listeners.add(listener);
        }
    }

    public static void removeListener(PlayerStateListener listener) {
        listeners.remove(listener);
    }

    public void addUrlQueue(MusicItem url) {
        if (!this.queue.contains(url)) {
            this.queue.add(url);
        }
    }

    public synchronized void addUrlsQueue(List<MusicItem> items) {
        for (MusicItem item : items) {
            addUrlQueue(item);
        }
    }

    public void clearQueue() {
        this.currentPosition = 0;
        this.queue.clear();
    }

    public void setCurrentPosition(int position) {
        if (position < 0) {
            this.currentPosition = this.queue.size() - 1;
        } else {
            this.currentPosition = position;
        }
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public List<MusicItem> getSongs() {
        return this.queue;
    }

    /* access modifiers changed from: private */
    public void updateCurrentPosition(MusicItem item) {
        if (item != null && StringUtils.isNotEmpty(item.getUrl()) && !item.isSingle()) {
            boolean isNewItem = true;
            Iterator i$ = this.queue.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                MusicItem element = i$.next();
                if (element.getUrl().equalsIgnoreCase(item.getUrl())) {
                    isNewItem = false;
                    item = element;
                    break;
                }
            }
            if (isNewItem) {
                this.queue.add(item);
            }
            this.currentPosition = this.queue.indexOf(item);
        }
    }
}
