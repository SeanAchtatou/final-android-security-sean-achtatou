package com.biznessapps.player;

import android.os.Parcel;
import android.os.Parcelable;

public class PlayerState implements Parcelable {
    public static final Parcelable.Creator<PlayerState> CREATOR = new Parcelable.Creator<PlayerState>() {
        public PlayerState createFromParcel(Parcel in) {
            return new PlayerState(in);
        }

        public PlayerState[] newArray(int size) {
            return new PlayerState[size];
        }
    };
    public static final int ERROR = 4;
    public static final int PAUSED = 3;
    public static final int PLAYING = 1;
    public static final int STOPPED = 2;
    private int state;

    public int getState() {
        return this.state;
    }

    public void setState(int newState) {
        this.state = newState;
    }

    public PlayerState() {
    }

    public PlayerState(Parcel in) {
        this.state = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.state);
    }
}
