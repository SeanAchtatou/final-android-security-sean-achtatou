package com.biznessapps.player;

import android.content.Context;

public class MusicPlayer {
    private static MusicPlayer instance;
    private static PlayerServiceAccessor playerServiceAccessor;
    private Context context;

    public static MusicPlayer getInstance() {
        if (instance == null) {
            instance = new MusicPlayer();
        }
        return instance;
    }

    public void init(Context context2) {
        this.context = context2;
        if (playerServiceAccessor == null) {
            playerServiceAccessor = new PlayerServiceAccessor(context2);
        }
    }

    public boolean isInited() {
        return (this.context == null || playerServiceAccessor == null) ? false : true;
    }

    public PlayerServiceAccessor getServiceAccessor() {
        return playerServiceAccessor;
    }

    public void destroy() {
        playerServiceAccessor = null;
    }
}
