package com.biznessapps.player;

import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.biznessapps.player.IPlayerService;
import java.util.List;

public class PlayerServiceAccessor extends BaseServiceAccessor<IPlayerService> {
    private static final String COMMON_EXCEPTION = "Exception";
    private static final String REMOTE_EXCEPTION = "RemoteException";
    private static final String TAG = "ServiceAccessor";

    public PlayerServiceAccessor(Context context) {
        super(context);
    }

    public void play(MusicItem item) {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).play(item);
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public MusicItem getCurrentSong() {
        try {
            if (getService() != null) {
                return ((IPlayerService) getService()).getCurrentSong();
            }
            return null;
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return null;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return null;
        }
    }

    public long getDuration() {
        try {
            if (getService() != null) {
                return ((IPlayerService) getService()).duration();
            }
            return 0;
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return 0;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return 0;
        }
    }

    public int getCurrentPosition() {
        try {
            if (getService() != null) {
                return ((IPlayerService) getService()).getCurrentPosition();
            }
            return 0;
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return 0;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return 0;
        }
    }

    public List<MusicItem> getSongs() {
        try {
            if (getService() != null) {
                return ((IPlayerService) getService()).getSongs();
            }
            return null;
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return null;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return null;
        }
    }

    public void setCurrentPosition(int currentPosition) {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).setCurrentPosition(currentPosition);
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public PlayerState getPlayerState() {
        try {
            if (getService() != null) {
                return ((IPlayerService) getService()).getState();
            }
            return null;
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return null;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return null;
        }
    }

    public long getPosition() {
        try {
            if (getService() != null) {
                return ((IPlayerService) getService()).position();
            }
            return 0;
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return 0;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return 0;
        }
    }

    public void seek(long position) {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).seek(position);
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public void addUrlsQueue(List<MusicItem> items) {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).addUrlsQueue(items);
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public void addUrlQueue(MusicItem item) {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).addUrlQueue(item);
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public void clearQueue() {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).clearQueue();
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public void pause() {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).pause();
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public void stop() {
        try {
            if (getService() != null) {
                ((IPlayerService) getService()).stop();
            }
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
        }
    }

    public boolean isInState(int state) {
        try {
            return ((IPlayerService) getService()).isInState(state);
        } catch (RemoteException e) {
            Log.e(TAG, REMOTE_EXCEPTION, e);
            return false;
        } catch (Exception e2) {
            Log.e(TAG, COMMON_EXCEPTION, e2);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public IPlayerService createServiceStub(IBinder binder) {
        return IPlayerService.Stub.asInterface(binder);
    }

    /* access modifiers changed from: protected */
    public Class<?> getServiceClass() {
        return PlayerService.class;
    }
}
