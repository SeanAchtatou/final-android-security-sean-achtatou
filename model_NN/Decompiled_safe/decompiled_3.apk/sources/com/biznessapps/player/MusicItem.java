package com.biznessapps.player;

import android.os.Parcel;
import android.os.Parcelable;

public class MusicItem implements Parcelable {
    public static final Parcelable.Creator<MusicItem> CREATOR = new Parcelable.Creator<MusicItem>() {
        public MusicItem createFromParcel(Parcel in) {
            return new MusicItem(in);
        }

        public MusicItem[] newArray(int size) {
            return new MusicItem[size];
        }
    };
    private String albumName;
    private byte isSingle;
    private String songInfo;
    private String url;

    public MusicItem() {
    }

    public MusicItem(Parcel in) {
        this.url = in.readString();
        this.songInfo = in.readString();
        this.albumName = in.readString();
        this.isSingle = in.readByte();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.songInfo);
        dest.writeString(this.albumName);
        dest.writeByte(this.isSingle);
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getSongInfo() {
        return this.songInfo;
    }

    public void setSongInfo(String songInfo2) {
        this.songInfo = songInfo2;
    }

    public String getAlbumName() {
        return this.albumName;
    }

    public void setAlbumName(String albumName2) {
        this.albumName = albumName2;
    }

    public boolean isSingle() {
        return this.isSingle == 1;
    }

    public void setSingle() {
        this.isSingle = 1;
    }
}
