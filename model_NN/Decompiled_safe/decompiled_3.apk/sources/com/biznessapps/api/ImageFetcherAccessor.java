package com.biznessapps.api;

import android.content.Context;
import com.biznessapps.layout.R;
import com.biznessapps.utils.google.caching.ImageCache;
import com.biznessapps.utils.google.caching.ImageFetcher;
import com.biznessapps.utils.google.caching.ImageGridAdapter;
import java.util.List;

public class ImageFetcherAccessor {
    private static final String IMAGE_CACHE_DIR = "app_images3";
    private Context context;
    private ImageFetcher imageFetcher;
    private int listThumbSize;

    public ImageFetcherAccessor(Context context2) {
        this.context = context2;
        getImageFetcher();
    }

    public ImageFetcher getImageFetcher() {
        if (this.imageFetcher == null) {
            this.imageFetcher = createImageFetcher(this.context, R.drawable.product_default, this.listThumbSize, IMAGE_CACHE_DIR, 0.25f);
        }
        return this.imageFetcher;
    }

    public void cleanCache() {
        if (this.imageFetcher != null) {
            this.imageFetcher.clearCache();
            this.imageFetcher.closeCache();
            this.imageFetcher = null;
        }
    }

    public ImageGridAdapter createImageGridAdapter(Context context2, ImageFetcher fetcher, List<String> imageUrls) {
        return new ImageGridAdapter(context2, fetcher, imageUrls);
    }

    private ImageFetcher createImageFetcher(Context context2, int resId, int imageSize, String cacheName, float cacheSize) {
        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(context2, cacheName);
        cacheParams.setMemCacheSizePercent(cacheSize);
        ImageFetcher mImageFetcher = new ImageFetcher(context2, imageSize, cacheName);
        mImageFetcher.setLoadingImage(resId);
        mImageFetcher.addImageCache(cacheParams);
        return mImageFetcher;
    }
}
