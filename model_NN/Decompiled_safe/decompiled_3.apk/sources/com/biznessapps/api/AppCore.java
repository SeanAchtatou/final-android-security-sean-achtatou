package com.biznessapps.api;

import android.content.Context;
import android.net.ConnectivityManager;
import com.biznessapps.components.LocationFinder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.images.NewImageManager;
import com.biznessapps.layout.R;
import com.biznessapps.model.AppSettings;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;

public class AppCore {
    private static AppCore instance;
    private AppSettings appSettings;
    private String bearerAccessToken;
    private CachingManager cachingManager = new CachingManager();
    private Context context;
    private int deviceHeight;
    private int deviceWidth;
    private ImageFetcherAccessor imageFetcherAccessor;
    private boolean isTablet;
    private LocationFinder locationFinder;
    private NewImageManager newImageManager = new NewImageManager();
    private UiSettings uiSettings = new UiSettings();

    public static AppCore getInstance() {
        if (instance == null) {
            instance = new AppCore();
        }
        return instance;
    }

    public int getDeviceWidth() {
        return this.deviceWidth;
    }

    public void setDeviceWidth(int deviceWidth2) {
        this.deviceWidth = deviceWidth2;
    }

    public int getDeviceHeight() {
        return this.deviceHeight;
    }

    public void setDeviceHeight(int deviceHeight2) {
        this.deviceHeight = deviceHeight2;
    }

    public boolean isTablet() {
        return this.isTablet;
    }

    public ImageFetcherAccessor getImageFetcherAccessor() {
        return this.imageFetcherAccessor;
    }

    public NewImageManager getNewImageManager() {
        return this.newImageManager;
    }

    public AppSettings getAppSettings() {
        if (this.appSettings == null) {
            this.appSettings = new AppSettings();
        }
        return this.appSettings;
    }

    public void setAppSettings(AppSettings appSettings2) {
        if (appSettings2 != null) {
            this.appSettings = appSettings2;
            this.appSettings.setActive(true);
        }
    }

    public void init(Context appContext) {
        this.context = appContext;
        if (this.imageFetcherAccessor == null) {
            this.imageFetcherAccessor = new ImageFetcherAccessor(this.context);
        }
        this.uiSettings.reset();
        initItemColors();
        initLocationFinder(appContext);
        this.isTablet = appContext.getResources().getBoolean(R.bool.isTablet);
    }

    public void initLocationFinder(Context appContext) {
        if (this.locationFinder == null) {
            this.locationFinder = new LocationFinder(appContext);
        }
    }

    public boolean isInitialized() {
        return (this.context == null || this.locationFinder == null) ? false : true;
    }

    public void clear() {
        this.appSettings = null;
    }

    public UiSettings getUiSettings() {
        return this.uiSettings;
    }

    public CachingManager getCachingManager() {
        return this.cachingManager;
    }

    public LocationFinder getLocationFinder() {
        return this.locationFinder;
    }

    public String getBearerAccessToken() {
        return this.bearerAccessToken;
    }

    public void setBearerAccessToken(String bearerAccessToken2) {
        this.bearerAccessToken = bearerAccessToken2;
    }

    public boolean canUseOfflineMode() {
        return this.context.getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0).getBoolean(AppConstants.OFFLINE_CACHING_KEY, false);
    }

    public void setUseOfflineMode(boolean useOfflineMode) {
        this.context.getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0).edit().putBoolean(AppConstants.OFFLINE_CACHING_KEY, useOfflineMode).commit();
    }

    public boolean isAnyConnectionAvailable() {
        if (this.context == null || ((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo() == null) {
            return false;
        }
        return true;
    }

    private void initItemColors() {
        if (this.appSettings != null) {
            try {
                if (StringUtils.isNotEmpty(this.appSettings.getEvenRowColor())) {
                    this.uiSettings.setEvenRowColor(ViewUtils.getColor(this.appSettings.getEvenRowColor()));
                }
                this.uiSettings.setDefaultListBgColor(ViewUtils.getColor(this.appSettings.getDefaultListBgColor()));
                this.uiSettings.setButtonTextColor(ViewUtils.getColor(this.appSettings.getButtonTextColor()));
                this.uiSettings.setButtonBgColor(ViewUtils.getColor(this.appSettings.getButtonBgColor()));
                this.uiSettings.setFeatureTextColor(ViewUtils.getColor(this.appSettings.getFeatureTextColor()));
                if (StringUtils.isNotEmpty(this.appSettings.getOddRowColor())) {
                    this.uiSettings.setOddRowColor(ViewUtils.getColor(this.appSettings.getOddRowColor()));
                    this.uiSettings.setHasColor(true);
                }
                if (StringUtils.isNotEmpty(this.appSettings.getEvenRowTextColor())) {
                    this.uiSettings.setEvenRowTextColor(ViewUtils.getColor(this.appSettings.getEvenRowTextColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getOddRowTextColor())) {
                    this.uiSettings.setOddRowTextColor(ViewUtils.getColor(this.appSettings.getOddRowTextColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getNavigBarColor())) {
                    this.uiSettings.setNavigationBarColor(ViewUtils.getColor(this.appSettings.getNavigBarColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getNavigBarTextColor())) {
                    this.uiSettings.setNavigationTextColor(ViewUtils.getColor(this.appSettings.getNavigBarTextColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getNavigBarTextShadowColor())) {
                    this.uiSettings.setNavigationTextShadowColor(ViewUtils.getColor(this.appSettings.getNavigBarTextShadowColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getSectionBarColor())) {
                    this.uiSettings.setSectionBarColor(ViewUtils.getColor(this.appSettings.getSectionBarColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getSectionBarTextColor())) {
                    this.uiSettings.setSectionTextColor(ViewUtils.getColor(this.appSettings.getSectionBarTextColor()));
                }
                if (StringUtils.isNotEmpty(this.appSettings.getGlobalBgColor())) {
                    this.uiSettings.setGlobalBgColor(ViewUtils.getColor(this.appSettings.getGlobalBgColor()));
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
        }
    }

    public static class UiSettings {
        private int buttonBgColor;
        private int buttonTextColor;
        private int defaultListBgColor;
        private int evenRowColor;
        private int evenRowTextColor;
        private int featureTextColor;
        private int globalBgColor;
        private boolean hasColor;
        private int navigationBarColor;
        private int navigationTextColor;
        private int navigationTextShadowColor;
        private int oddRowColor;
        private int oddRowTextColor;
        private int sectionBarColor;
        private int sectionTextColor;

        public UiSettings() {
            reset();
        }

        public void reset() {
            this.hasColor = false;
            this.evenRowColor = -1;
            this.oddRowColor = -1;
            this.evenRowTextColor = -16777216;
            this.oddRowTextColor = -16777216;
            this.navigationBarColor = -3355444;
            this.navigationTextColor = -16777216;
            this.navigationTextShadowColor = -16777216;
            this.sectionBarColor = -3355444;
            this.sectionTextColor = -16777216;
        }

        public int getFeatureTextColor() {
            return this.featureTextColor;
        }

        public void setFeatureTextColor(int featureTextColor2) {
            this.featureTextColor = featureTextColor2;
        }

        public int getButtonTextColor() {
            return this.buttonTextColor;
        }

        public void setButtonTextColor(int buttonTextColor2) {
            this.buttonTextColor = buttonTextColor2;
        }

        public int getButtonBgColor() {
            return this.buttonBgColor;
        }

        public void setButtonBgColor(int buttonBgColor2) {
            this.buttonBgColor = buttonBgColor2;
        }

        public int getEvenRowColor() {
            return this.evenRowColor;
        }

        public void setEvenRowColor(int evenRowColor2) {
            this.evenRowColor = evenRowColor2;
        }

        public int getOddRowColor() {
            return this.oddRowColor;
        }

        public void setOddRowColor(int oddRowColor2) {
            this.oddRowColor = oddRowColor2;
        }

        public int getEvenRowTextColor() {
            return this.evenRowTextColor;
        }

        public void setEvenRowTextColor(int evenRowTextColor2) {
            this.evenRowTextColor = evenRowTextColor2;
        }

        public int getOddRowTextColor() {
            return this.oddRowTextColor;
        }

        public void setOddRowTextColor(int oddRowTextColor2) {
            this.oddRowTextColor = oddRowTextColor2;
        }

        public int getNavigationBarColor() {
            return this.navigationBarColor;
        }

        public void setNavigationBarColor(int navigationBarColor2) {
            this.navigationBarColor = navigationBarColor2;
        }

        public int getNavigationTextColor() {
            return this.navigationTextColor;
        }

        public void setNavigationTextColor(int navigationTextColor2) {
            this.navigationTextColor = navigationTextColor2;
        }

        public int getNavigationTextShadowColor() {
            return this.navigationTextShadowColor;
        }

        public void setNavigationTextShadowColor(int navigationTextShadowColor2) {
            this.navigationTextShadowColor = navigationTextShadowColor2;
        }

        public int getSectionBarColor() {
            return this.sectionBarColor;
        }

        public void setSectionBarColor(int sectionBarColor2) {
            this.sectionBarColor = sectionBarColor2;
        }

        public int getSectionTextColor() {
            return this.sectionTextColor;
        }

        public void setSectionTextColor(int sectionTextColor2) {
            this.sectionTextColor = sectionTextColor2;
        }

        public boolean isHasColor() {
            return this.hasColor;
        }

        public void setHasColor(boolean hasColor2) {
            this.hasColor = hasColor2;
        }

        public int getDefaultListBgColor() {
            return this.defaultListBgColor;
        }

        public void setDefaultListBgColor(int defaultListBgColor2) {
            this.defaultListBgColor = defaultListBgColor2;
        }

        public int getGlobalBgColor() {
            return this.globalBgColor;
        }

        public void setGlobalBgColor(int globalBgColor2) {
            this.globalBgColor = globalBgColor2;
        }
    }
}
