package com.biznessapps.api;

import android.view.ViewGroup;

public interface LoadingDataInterface {
    ViewGroup getProgressBarContainer();
}
