package com.biznessapps.api;

import android.app.Activity;
import android.view.View;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.utils.CommonUtils;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Map;

public class LoadDataTaskExternal extends UnModalAsyncTask<Map<String, String>, Void, Boolean> {
    private static final String INCORRECT_STATE_MESSAGE = "Incorrect state: check url and runnables";
    private LoadDataRunnable canUseCachingRunnable;
    private LoadDataRunnable handleInBgRunnable;
    private long loadingTime;
    private Map<String, String> params;
    private LoadDataRunnable parseDataRunnable;
    private LoadDataRunnable preDataLoadingRunnable;
    private String requestUrl;
    private LoadDataRunnable updateControlsRunnable;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ Object doInBackground(Object[] x0) {
        return doInBackground((Map<String, String>[]) ((Map[]) x0));
    }

    public LoadDataTaskExternal(Activity activity, View progressBar, List<WeakReference<View>> refOfViews) {
        super(activity, progressBar, refOfViews);
    }

    public void setRequestUrl(String url) {
        this.requestUrl = url;
    }

    public void setParseDataRunnable(LoadDataRunnable parseDataRunnable2) {
        this.parseDataRunnable = parseDataRunnable2;
    }

    public void setHandleInBgRunnable(LoadDataRunnable handleInBgRunnable2) {
        this.handleInBgRunnable = handleInBgRunnable2;
    }

    public void setPreDataLoadingRunnable(LoadDataRunnable preDataLoadingRunnable2) {
        this.preDataLoadingRunnable = preDataLoadingRunnable2;
    }

    public void setCanUseCachingRunnable(LoadDataRunnable canUseCachingRunnable2) {
        this.canUseCachingRunnable = canUseCachingRunnable2;
    }

    public void setUpdateControlsRunnable(LoadDataRunnable updateControlsRunnable2) {
        this.updateControlsRunnable = updateControlsRunnable2;
    }

    public void launch() {
        checkState();
        Activity holdActivity = this.activity;
        if (holdActivity != null) {
            if (this.preDataLoadingRunnable != null) {
                this.preDataLoadingRunnable.setActivity(holdActivity);
                this.preDataLoadingRunnable.run();
            }
            if (this.canUseCachingRunnable != null) {
                this.canUseCachingRunnable.run();
                if (this.canUseCachingRunnable.canUseCaching()) {
                    this.updateControlsRunnable.setActivity(holdActivity);
                    this.updateControlsRunnable.run();
                } else if (this.params != null) {
                    execute(new Map[]{this.params});
                } else {
                    execute(new Map[0]);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Map<String, String>... params2) {
        if (AppCore.getInstance().isTablet()) {
            this.requestUrl += ServerConstants.TABLET_PARAM;
        }
        this.parseDataRunnable.setDataToParse(DataSource.getInstance().getData(this.requestUrl, params2));
        this.parseDataRunnable.run();
        boolean isCorrectData = this.parseDataRunnable.isCorrectData();
        if (this.activity != null && isCorrectData) {
            this.handleInBgRunnable.run();
        }
        return Boolean.valueOf(isCorrectData);
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.loadingTime = System.currentTimeMillis();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Boolean isCorrectData) {
        super.onPostExecute((Object) isCorrectData);
        if (isCorrectData.booleanValue()) {
            if (this.activity != null && (this.activity instanceof LoadingDataInterface)) {
                ((LoadingDataInterface) this.activity).getProgressBarContainer().removeAllViews();
                this.updateControlsRunnable.setActivity(this.activity);
                this.updateControlsRunnable.run();
            }
        } else if (this.activity != null) {
        }
        if (this.activity != null && this.activity.getIntent() != null) {
            this.loadingTime = System.currentTimeMillis() - this.loadingTime;
            CommonUtils.sendTimingEvent(this.activity.getApplicationContext(), this.activity.getIntent().getStringExtra(AppConstants.TAB_FRAGMENT_EXTRA), this.loadingTime);
        }
    }

    /* access modifiers changed from: protected */
    public void placeProgressBar() {
        if (this.activity != null && (this.activity instanceof LoadingDataInterface)) {
            ((LoadingDataInterface) this.activity).getProgressBarContainer().addView(this.progressBar);
        }
    }

    private void checkState() {
        if (this.requestUrl == null || this.updateControlsRunnable == null || this.parseDataRunnable == null || this.handleInBgRunnable == null) {
            throw new IllegalStateException(INCORRECT_STATE_MESSAGE);
        }
    }

    public void setParams(Map<String, String> params2) {
        this.params = params2;
    }

    public static class LoadDataRunnable implements Runnable {
        private Activity activity;
        private boolean canUseCaching;
        private String dataToParse;
        private boolean isCorrectData = true;

        public Activity getActivity() {
            return this.activity;
        }

        public void setActivity(Activity activity2) {
            this.activity = activity2;
        }

        public String getDataToParse() {
            return this.dataToParse;
        }

        public void setDataToParse(String dataToParse2) {
            this.dataToParse = dataToParse2;
        }

        public boolean isCorrectData() {
            return this.isCorrectData;
        }

        public void setCorrectData(boolean isCorrectData2) {
            this.isCorrectData = isCorrectData2;
        }

        public boolean canUseCaching() {
            return this.canUseCaching;
        }

        public void canUseCaching(boolean canUseCaching2) {
            this.canUseCaching = canUseCaching2;
        }

        public void run() {
        }
    }
}
