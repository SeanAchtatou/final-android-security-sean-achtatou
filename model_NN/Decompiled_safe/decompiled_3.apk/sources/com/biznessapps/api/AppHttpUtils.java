package com.biznessapps.api;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.shoppingcart.utils.XMLUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class AppHttpUtils {
    private static final String APP_CODE = "app_code";
    private static final String CATEGORIES = "categories";
    private static final String EMAIL = "email";
    private static final String NAME = "name";
    private static final String TAB_ID = "tab_id";
    private static final String VERSION = "version";
    private static HttpUtils http;

    public static HttpUtils http() {
        if (http == null) {
            http = new HttpUtils();
        }
        return http;
    }

    public static void addToMailingList(AsyncCallback<?> callback, String name, String email, String appCode, String tabId, String categories) {
        Map<String, String> params = getEmptyParams();
        params.put("name", name);
        params.put("email", email);
        params.put("tab_id", tabId);
        params.put("version", "4");
        if (StringUtils.isNotEmpty(categories)) {
            params.put(CATEGORIES, categories);
        }
        params.put("app_code", appCode);
        executePostRequest(ServerConstants.MAILING_LIST_SAVE, params, callback);
    }

    public static String postEventDataSync(String url, String appCode, String tabId, String id, String userType, String userId, String name, String comment, String hash, String parentId) {
        Map<String, String> params = getEmptyParams();
        params.put("app_code", appCode);
        params.put("tab_id", tabId);
        params.put("id", id);
        params.put(ServerConstants.POST_USER_TYPE_PARAM, userType);
        params.put(ServerConstants.POST_USER_ID_PARAM, userId);
        params.put("name", name);
        if (StringUtils.isNotEmpty(comment)) {
            params.put("comment", comment);
        }
        if (StringUtils.isNotEmpty(parentId)) {
            params.put("parent_id", parentId);
        }
        params.put(ServerConstants.POST_HASH_PARAM, hash);
        return executePostRequestSync(url, params);
    }

    public static void postCommentAsync(String url, String appCode, String tabId, String id, String userType, String userId, String name, String comment, String hash, String parentId, double longitude, double latitude, byte[] imageData, AsyncCallback<?> callback) {
        http().postCommentAsync(url, appCode, tabId, id, userType, userId, name, comment, hash, parentId, longitude, latitude, imageData, callback);
    }

    public static String postGoogleCheckoutRequest(String url, String merchantId, String merchantKey, String body) {
        try {
            HttpURLConnection connection = HttpUtils.makeConnection(url, merchantId, merchantKey);
            Writer writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body);
            writer.close();
            InputStreamReader reader = new InputStreamReader(connection.getInputStream());
            char[] buf = new char[10000];
            StringBuilder sb = new StringBuilder();
            while (true) {
                int i = reader.read(buf);
                if (i <= 0) {
                    return XMLUtils.ParseGoogleCheckoutResponseUrl(sb.toString());
                }
                sb.append(buf, 0, i);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void sendPhoto(byte[] data, String appCode, String tabId, String eventId) {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(ServerConstants.PHOTO_POST);
            StringBody appCodeBody = new StringBody(appCode);
            StringBody tabIdBody = new StringBody(tabId);
            StringBody eventIdBody = new StringBody(eventId);
            ByteArrayBody bab = new ByteArrayBody(data, AppConstants.IMAGE_MIME_TYPE, "");
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart("app_code", appCodeBody);
            reqEntity.addPart("tab_id", tabIdBody);
            reqEntity.addPart(ServerConstants.POST_IMAGE_PARAM, bab);
            reqEntity.addPart("id", eventIdBody);
            postRequest.setEntity(reqEntity);
            httpClient.execute(postRequest);
        } catch (Throwable th) {
        }
    }

    public static void postCommentAsync(AsyncCallback<?> callback, boolean useFacebookCred, String hash, String tabId, String facebookId, String twitterId, String name, String comment, String appCode, String commentParentId, boolean hasYoutubeFormat) {
        http().postFanCommentAsync(callback, useFacebookCred, hash, tabId, facebookId, twitterId, name, comment, appCode, commentParentId, hasYoutubeFormat);
    }

    public static void executeGetRequest(String url, AsyncCallback<?> callback) {
        Map<String, String> params = getEmptyParams();
        http().executeGetRequestAsync(url, getKeys(params), getValues(params), callback);
    }

    public static String getBearerAccessToken(String consumerKey, String consumerSecret) {
        return JsonParserUtils.getBearerToken(HttpUtils.getBearerAccessTokenData(consumerKey, consumerSecret));
    }

    public static void sendPhotoAsync(byte[] data, String appCode, String tabId, String eventId, AsyncCallback<?> callback) {
        http().sendPhotoAsync(data, appCode, tabId, eventId, callback);
    }

    public static String executeGetSyncRequest(String url) {
        return http().executeRequestSync(url);
    }

    public static String executeGetSyncRequest(String url, String userName, String userPassword) {
        return http().executeRequestSync(url, userName, userPassword);
    }

    public static Map<String, String> getEmptyParams() {
        return new LinkedHashMap();
    }

    public static String executePostRequestSync(String url, Map<String, String> params) {
        return http().executePostRequestSync(url, getKeys(params), getValues(params));
    }

    public static void executePostRequest(String url, Map<String, String> params, AsyncCallback<?> callback) {
        http().executePostRequestAsync(url, getKeys(params), getValues(params), callback);
    }

    private static String getParamForGetRequests(Map<String, String> params) {
        String result = "";
        if (params.size() == 0) {
            return result;
        }
        String[] keys = getKeys(params);
        String[] values = getValues(params);
        for (int i = 0; i < keys.length; i++) {
            result = result + keys[i] + "=" + values[i] + "&";
        }
        return result.substring(0, result.length() - 1);
    }

    private static String[] getKeys(Map<String, String> params) {
        if (params != null) {
            return (String[]) params.keySet().toArray(new String[0]);
        }
        return null;
    }

    private static String[] getValues(Map<String, String> params) {
        if (params != null) {
            return (String[]) params.values().toArray(new String[0]);
        }
        return null;
    }
}
