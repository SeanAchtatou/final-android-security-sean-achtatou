package com.biznessapps.api;

import android.os.Handler;

public class AppHandlers {
    private static Handler uiHandler;

    public static synchronized Handler getUiHandler() {
        Handler handler;
        synchronized (AppHandlers.class) {
            if (uiHandler == null) {
                uiHandler = new Handler();
            }
            handler = uiHandler;
        }
        return handler;
    }

    public static synchronized Handler getHandler() {
        Handler handler;
        synchronized (AppHandlers.class) {
            handler = uiHandler;
        }
        return handler;
    }
}
