package com.biznessapps.api;

import android.app.Activity;
import android.os.AsyncTask;

public abstract class CommonTask<TParams, TProgress, TResult> extends AsyncTask<TParams, TProgress, TResult> {
    protected Activity activity;

    public CommonTask(Activity activity2) {
        this.activity = activity2;
    }

    public void setActivity(Activity activity2) {
        if (activity2 == null) {
            onActivityDetached();
            this.activity = activity2;
            return;
        }
        this.activity = activity2;
        onActivityAttached();
    }

    /* access modifiers changed from: protected */
    public void onActivityAttached() {
    }

    /* access modifiers changed from: protected */
    public void onActivityDetached() {
    }
}
