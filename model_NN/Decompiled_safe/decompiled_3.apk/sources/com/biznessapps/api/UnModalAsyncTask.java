package com.biznessapps.api;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.List;

public abstract class UnModalAsyncTask<TParams, TProgress, TResult> extends CommonTask<TParams, TProgress, TResult> {
    private boolean isActive;
    protected View progressBar;
    private List<WeakReference<View>> refOfViews;

    /* access modifiers changed from: protected */
    public abstract void placeProgressBar();

    public UnModalAsyncTask(Activity activity, View progressBar2, List<WeakReference<View>> refOfViews2) {
        super(activity);
        this.progressBar = progressBar2;
        this.refOfViews = refOfViews2;
    }

    /* access modifiers changed from: protected */
    public void onActivityAttached() {
        this.isActive = !getStatus().equals(AsyncTask.Status.FINISHED) && !isCancelled();
        refreshControls();
    }

    /* access modifiers changed from: protected */
    public void onActivityDetached() {
        this.isActive = false;
        this.progressBar.setVisibility(8);
        refreshControls();
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
        this.isActive = true;
        refreshControls();
        placeProgressBar();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Object obj) {
        super.onPostExecute(obj);
        this.isActive = false;
        this.progressBar.setVisibility(8);
        refreshControls();
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        super.onCancelled();
        this.isActive = false;
        this.progressBar.setVisibility(8);
        refreshControls();
    }

    private void refreshControls() {
        if (this.refOfViews != null) {
            for (WeakReference<View> viewRef : this.refOfViews) {
                if (!(viewRef == null || viewRef.get() == null)) {
                    ((View) viewRef.get()).setEnabled(!this.isActive);
                }
            }
        }
    }
}
