package com.biznessapps.api;

import java.util.List;

public abstract class AsyncCallback<T> {
    public void onResult(String result) {
        if (result != null) {
            onResult();
        } else {
            onError("Results is null", (Throwable) null);
        }
    }

    public void onResult(List<?> list) {
        if (list == null || list.size() <= 0) {
            onResult();
        } else {
            onResult(list.get(0));
        }
    }

    public void onResult(Object obj) {
        onResult();
    }

    public void onResult() {
    }

    public void onError(String message, Throwable throwable) {
    }

    public void onError(int messageId, Throwable throwable) {
    }
}
