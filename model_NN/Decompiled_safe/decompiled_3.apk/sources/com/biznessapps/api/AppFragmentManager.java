package com.biznessapps.api;

import android.content.Intent;
import android.support.v4.app.Fragment;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.contacts.ContactsListFragment;
import com.biznessapps.fragments.coupons.CouponDetailFragment;
import com.biznessapps.fragments.coupons.CouponsListFragment;
import com.biznessapps.fragments.events.EventDetailsFragment;
import com.biznessapps.fragments.events.EventV2DetailsFragment;
import com.biznessapps.fragments.events.EventsListFragment;
import com.biznessapps.fragments.events.EventsV2ListFragment;
import com.biznessapps.fragments.fanwall.FanAddCommentsFragment;
import com.biznessapps.fragments.fanwall.FanWallFragment;
import com.biznessapps.fragments.fanwall.NewFanWallFragment;
import com.biznessapps.fragments.infoitems.InfoDetailFragment;
import com.biznessapps.fragments.infoitems.InfoItemsFragment;
import com.biznessapps.fragments.infoitems.InfoSectionsFragment;
import com.biznessapps.fragments.lists.CallUsFragment;
import com.biznessapps.fragments.lists.DirectionsFragment;
import com.biznessapps.fragments.lists.GalleryListFragment;
import com.biznessapps.fragments.lists.MessageListFragment;
import com.biznessapps.fragments.lists.PodcastsListFragment;
import com.biznessapps.fragments.loyalty.LoyaltyDetailFragment;
import com.biznessapps.fragments.loyalty.LoyaltyListFragment;
import com.biznessapps.fragments.menuitems.MenuItemDetailFragment;
import com.biznessapps.fragments.menuitems.MenuSectionItemsListFragment;
import com.biznessapps.fragments.menuitems.MenuSectionsListFragment;
import com.biznessapps.fragments.music.MusicListFragment;
import com.biznessapps.fragments.music.SongInfoFragment;
import com.biznessapps.fragments.news.NewsDetailFragment;
import com.biznessapps.fragments.news.NewsFragment;
import com.biznessapps.fragments.notepad.NotepadEditFragment;
import com.biznessapps.fragments.notepad.NotepadListFragment;
import com.biznessapps.fragments.qr.QrScannerFragment;
import com.biznessapps.fragments.qr.QrScannerHelpFragment;
import com.biznessapps.fragments.reservation.PaymentIntegrateFragment;
import com.biznessapps.fragments.reservation.ReservationAccountFragment;
import com.biznessapps.fragments.reservation.ReservationAccountRecoveryFragment;
import com.biznessapps.fragments.reservation.ReservationAccountRegisterFragment;
import com.biznessapps.fragments.reservation.ReservationApptDetailFragment;
import com.biznessapps.fragments.reservation.ReservationApptHistoryFragment;
import com.biznessapps.fragments.reservation.ReservationBookFragment;
import com.biznessapps.fragments.reservation.ReservationLoginFragment;
import com.biznessapps.fragments.reservation.ReservationMainFragment;
import com.biznessapps.fragments.reservation.ReservationTimeFragment;
import com.biznessapps.fragments.reservation.location.ReservationLocationDetailFragment;
import com.biznessapps.fragments.reservation.location.ReservationLocationListFragment;
import com.biznessapps.fragments.reservation.service.ReservationServiceFragment;
import com.biznessapps.fragments.rss.RssDetailFragment;
import com.biznessapps.fragments.rss.RssListFragment;
import com.biznessapps.fragments.shoppingcart.GoogleCheckoutFragment;
import com.biznessapps.fragments.shoppingcart.ShoppingCartCategoryFragmentNew;
import com.biznessapps.fragments.shoppingcart.ShoppingCartCheckoutFragment;
import com.biznessapps.fragments.shoppingcart.ShoppingCartProducDetailsFragment;
import com.biznessapps.fragments.shoppingcart.ShoppingCartProductsFragmentNew;
import com.biznessapps.fragments.single.EmailPhotoFragment;
import com.biznessapps.fragments.single.FlexibleCounterFragment;
import com.biznessapps.fragments.single.GalleryFragment;
import com.biznessapps.fragments.single.MailingFragment;
import com.biznessapps.fragments.single.MoreFragment;
import com.biznessapps.fragments.single.MortgageCalculatorFragment;
import com.biznessapps.fragments.single.PreviewAppFragment;
import com.biznessapps.fragments.single.ProtectedAppFragment;
import com.biznessapps.fragments.single.TellFriendsFragment;
import com.biznessapps.fragments.single.TipCalculatorFragment;
import com.biznessapps.fragments.single.VoiceRecordingFragment;
import com.biznessapps.fragments.twitter.HashTagsListFragment;
import com.biznessapps.fragments.twitter.RecipientsListFragment;
import com.biznessapps.fragments.twitter.TweetFragment;
import com.biznessapps.fragments.twitter.TwitterLoginFragment;
import com.biznessapps.fragments.web.WebViewFragment;
import com.biznessapps.fragments.web.WebViewTiersFragment;
import com.biznessapps.fragments.youtube.YoutubeFragment;
import com.biznessapps.fragments.youtube.YoutubeListFragment;
import com.biznessapps.utils.ViewUtils;

public class AppFragmentManager implements AppConstants {
    public static Fragment getFragmentByController(Intent comingIntent) {
        String fragmentExtra = comingIntent.getStringExtra(AppConstants.TAB_FRAGMENT_EXTRA);
        if (fragmentExtra == null) {
            throw new IllegalStateException(AppConstants.EXCEPTION_MESSAGE);
        } else if (ServerConstants.CONTENT_CHANGER_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
            PreviewAppFragment previewAppFragment = new PreviewAppFragment();
            previewAppFragment.setFragmentName(ServerConstants.CONTENT_CHANGER_VIEW_CONTROLLER);
            return previewAppFragment;
        } else if (ServerConstants.PROTECTED_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
            ProtectedAppFragment protectedAppFragment = new ProtectedAppFragment();
            protectedAppFragment.setFragmentName(ServerConstants.PROTECTED_VIEW_CONTROLLER);
            return protectedAppFragment;
        } else if (ServerConstants.WEB_TIER_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
            if ("http://twitter.com/#!/LuceLadies/".equals(comingIntent.getStringExtra(AppConstants.URL))) {
                return new TweetFragment();
            }
            return new WebViewTiersFragment();
        } else if (ServerConstants.WEB_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
            WebViewFragment webViewFragment = new WebViewFragment();
            webViewFragment.setFragmentName(ServerConstants.WEB_VIEW_CONTROLLER);
            return webViewFragment;
        } else if (AppConstants.WEB_VIEW_SINGLE_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
            return new WebViewFragment();
        } else {
            if (ServerConstants.MESSAGE_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new MessageListFragment();
            }
            if (ServerConstants.VOICE_RECORDING_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new VoiceRecordingFragment();
            }
            if (ServerConstants.FAN_WALL_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new FanWallFragment();
            }
            if (ServerConstants.NEW_FAN_WALL_VIEW_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new NewFanWallFragment();
            }
            if (AppConstants.FAN_ADD_COMMENT_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new FanAddCommentsFragment();
            }
            if (AppConstants.TWITTER_LOGIN_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new TwitterLoginFragment();
            }
            if (ServerConstants.RSS_FEED_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new RssListFragment();
            }
            if (AppConstants.RSS_DETAIL_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new RssDetailFragment();
            }
            if (ServerConstants.TIP_CALCULATOR_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new TipCalculatorFragment();
            }
            if (AppConstants.RECIPIENTS_LIST_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new RecipientsListFragment();
            }
            if (AppConstants.HASH_TAGS_LIST_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new HashTagsListFragment();
            }
            if (ServerConstants.GALLERY_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra) || ServerConstants.GALLERY_COVERFLOW_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new GalleryFragment();
            }
            if (AppConstants.GALLERY_LISTVIEW_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                return new GalleryListFragment();
            }
            if (ServerConstants.PICASA_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                GalleryFragment galleryFragment = new GalleryFragment();
                galleryFragment.setPicasaUsed(true);
                return galleryFragment;
            } else if (ServerConstants.FLICKR_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                GalleryFragment galleryFragment2 = new GalleryFragment();
                galleryFragment2.setFlickrUsed(true);
                return galleryFragment2;
            } else if (ServerConstants.INSTAGRAM_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                GalleryFragment galleryFragment3 = new GalleryFragment();
                galleryFragment3.setInstagramUsed(true);
                return galleryFragment3;
            } else if (ServerConstants.AROUND_US_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                CommonFragment commonFragment = new CommonFragment();
                commonFragment.setFragmentName(ServerConstants.AROUND_US_VIEW_CONTROLLER);
                return commonFragment;
            } else if (ServerConstants.LOCATION_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra) || ServerConstants.LOCATION_LIST_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                return new ContactsListFragment();
            } else {
                if (ServerConstants.CAR_FINDER_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                    CommonFragment commonFragment2 = new CommonFragment();
                    commonFragment2.setFragmentName(ServerConstants.CAR_FINDER_VIEW_CONTROLLER);
                    return commonFragment2;
                } else if (ServerConstants.FAN_WALL_NEW_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                    CommonFragment commonFragment3 = new CommonFragment();
                    commonFragment3.setFragmentName(ServerConstants.FAN_WALL_NEW_VIEW_CONTROLLER);
                    return commonFragment3;
                } else if (AppConstants.CONTACTS_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                    CommonFragment commonFragment4 = new CommonFragment();
                    commonFragment4.setFragmentName(AppConstants.CONTACTS_FRAGMENT);
                    return commonFragment4;
                } else if (ServerConstants.QR_COUPON_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                    CouponsListFragment couponsListFragment = new CouponsListFragment();
                    couponsListFragment.setQrCoupons(true);
                    return couponsListFragment;
                } else if (ServerConstants.COUPONS_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                    return new CouponsListFragment();
                } else {
                    if (AppConstants.COUPON_DETAIL_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                        return new CouponDetailFragment();
                    }
                    if (ServerConstants.QR_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new QrScannerFragment();
                    }
                    if (AppConstants.QR_SCANNER_HELP_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                        return new QrScannerHelpFragment();
                    }
                    if (ServerConstants.PODCAST_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new PodcastsListFragment();
                    }
                    if (ServerConstants.YOUTUBE_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new YoutubeListFragment();
                    }
                    if (AppConstants.YOUTUBE_SINGLE_VIEW_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                        return new YoutubeFragment();
                    }
                    if (ServerConstants.MAILING_LIST_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new MailingFragment();
                    }
                    if (ServerConstants.STAT_RECORDER_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new FlexibleCounterFragment();
                    }
                    if (ServerConstants.EMAIL_PHOTO_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new EmailPhotoFragment();
                    }
                    if (ServerConstants.REPAYMENT_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new MortgageCalculatorFragment();
                    }
                    if (ServerConstants.INFO_DETAIL_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new InfoDetailFragment();
                    }
                    if (ServerConstants.INFO_SECTIONS_VIEW_CONTROOLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new InfoSectionsFragment();
                    }
                    if (ServerConstants.INFO_ITEMS_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new InfoItemsFragment();
                    }
                    if (ServerConstants.EVENTS_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new EventsListFragment();
                    }
                    if (AppConstants.EVENTS_DETAIL_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                        return new EventDetailsFragment();
                    }
                    if (ServerConstants.EVENTS_V2_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new EventsV2ListFragment();
                    }
                    if (AppConstants.EVENTS_V2_DETAIL_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                        return new EventV2DetailsFragment();
                    }
                    if (ServerConstants.MENU_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        if (ViewUtils.hasExtraValue(comingIntent, AppConstants.MENU_ITEM_ID)) {
                            return new MenuSectionItemsListFragment();
                        }
                        if (ViewUtils.hasExtraValue(comingIntent, AppConstants.MENU_ITEM_DETAIL_ID)) {
                            return new MenuItemDetailFragment();
                        }
                        return new MenuSectionsListFragment();
                    } else if (ServerConstants.MORE_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                        return new MoreFragment();
                    } else {
                        if (ServerConstants.NEWS_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new NewsFragment();
                        }
                        if (AppConstants.NEWS_DETAIL_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                            return new NewsDetailFragment();
                        }
                        if (ServerConstants.CALL_US_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new CallUsFragment();
                        }
                        if (ServerConstants.DIRECTIONS_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new DirectionsFragment();
                        }
                        if (ServerConstants.TELL_FRIEND_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new TellFriendsFragment();
                        }
                        if (ServerConstants.NOTEPAD_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new NotepadListFragment();
                        }
                        if (AppConstants.NOTEPAD_EDIT_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                            return new NotepadEditFragment();
                        }
                        if (ServerConstants.SHOPPPING_CART_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ShoppingCartCategoryFragmentNew();
                        }
                        if (AppConstants.SHOPPING_CART_PRODUCTS_LIST.equalsIgnoreCase(fragmentExtra)) {
                            return new ShoppingCartProductsFragmentNew();
                        }
                        if (AppConstants.SHOPPING_CART_PRODUCTS_DETAILS.equalsIgnoreCase(fragmentExtra)) {
                            return new ShoppingCartProducDetailsFragment();
                        }
                        if (AppConstants.SHOPPING_CART_CHECKOUT.equalsIgnoreCase(fragmentExtra)) {
                            return new ShoppingCartCheckoutFragment();
                        }
                        if (AppConstants.GOOGLE_CHECKOUT_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                            return new GoogleCheckoutFragment();
                        }
                        if (ServerConstants.LOYALTY_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new LoyaltyListFragment();
                        }
                        if (AppConstants.LOYALTY_DETAIL_FRAGMENT.equalsIgnoreCase(fragmentExtra)) {
                            return new LoyaltyDetailFragment();
                        }
                        if (ServerConstants.MUSIC_PLAYER_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new MusicListFragment();
                        }
                        if (ServerConstants.SONG_INFO_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new SongInfoFragment();
                        }
                        if (ServerConstants.RESERVATION_MAIN_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationMainFragment();
                        }
                        if (ServerConstants.RESERVATION_LOGIN_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationLoginFragment();
                        }
                        if (ServerConstants.RESERVATION_HISTORY_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationApptHistoryFragment();
                        }
                        if (ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_HISTORY.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationApptDetailFragment(ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_HISTORY);
                        }
                        if (ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_MAIN.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationApptDetailFragment(ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_MAIN);
                        }
                        if (ServerConstants.RESERVATION_ACCOUNT_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationAccountFragment();
                        }
                        if (ServerConstants.RESERVATION_BOOK_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationBookFragment();
                        }
                        if (ServerConstants.RESERVATION_LOCATION_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationLocationListFragment();
                        }
                        if (ServerConstants.RESERVATION_SERVICE_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationServiceFragment();
                        }
                        if (ServerConstants.RESERVATION_TIME_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationTimeFragment();
                        }
                        if (ServerConstants.RESERVATION_PAYMENT_INTEGRATE_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new PaymentIntegrateFragment();
                        }
                        if (ServerConstants.RESERVATION_REGISTER_ACCOUNT_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationAccountRegisterFragment();
                        }
                        if (ServerConstants.RESERVATION_ACCOUNT_RECOVERY_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationAccountRecoveryFragment();
                        }
                        if (ServerConstants.RESERVATION_LOCATION_DETAIL_VIEW_CONTROLLER.equalsIgnoreCase(fragmentExtra)) {
                            return new ReservationLocationDetailFragment();
                        }
                        return null;
                    }
                }
            }
        }
    }
}
