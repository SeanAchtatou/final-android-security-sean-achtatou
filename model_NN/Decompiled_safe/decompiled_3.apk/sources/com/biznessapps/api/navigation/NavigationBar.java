package com.biznessapps.api.navigation;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.biznessapps.api.AppCore;
import com.biznessapps.layout.R;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NavigationBar {
    public static final int HORISONTAL_ORIENTATION = 1;
    public static final int VERTICAL_ORIENTATION = 2;
    private static long currentTabId = 0;
    private final Context context;
    private boolean isSelectionSet = false;
    private int orientation = 1;
    private int tabCount;
    private ViewGroup tabRootLayout;
    private final List<TabView> tabs = new ArrayList();
    private ViewGroup tabsContainer;

    public NavigationBar(Context context2, int orientation2) {
        this.context = context2;
        this.orientation = orientation2;
        initTabContainerLayout();
    }

    public void setTabCount(int tabCount2) {
        this.tabCount = tabCount2;
    }

    public void addTab(TabView tab, int countPerPage) {
        this.tabs.add(tab);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, -1);
        lp.weight = 1.0f;
        if (tab.withOldDesign()) {
            LinearLayout.LayoutParams sidesLp = new LinearLayout.LayoutParams(1, -1);
            LinearLayout leftSide = new LinearLayout(this.context);
            LinearLayout rightSide = new LinearLayout(this.context);
            leftSide.setBackgroundResource(R.drawable.side_line_background);
            rightSide.setBackgroundResource(R.drawable.side_line_background);
            this.tabsContainer.addView(leftSide, sidesLp);
            this.tabsContainer.addView(tab.getView(), lp);
            this.tabsContainer.addView(rightSide, sidesLp);
        } else if (this.orientation == 1) {
            this.tabsContainer.addView(tab.getView(), lp);
        } else {
            float navigationMargin = this.context.getResources().getDimension(R.dimen.vertical_navigation_margin);
            if (this.tabCount < countPerPage) {
                lp.height = ((int) (((float) AppCore.getInstance().getDeviceHeight()) - navigationMargin)) / this.tabCount;
            } else {
                lp.height = ((int) (((float) AppCore.getInstance().getDeviceHeight()) - navigationMargin)) / countPerPage;
            }
            this.tabsContainer.addView(tab.getView(), lp);
        }
    }

    public boolean openFirstTab() {
        if (this.tabs.size() <= 0) {
            return false;
        }
        this.tabs.get(0).getView().performClick();
        return true;
    }

    public void clearTabs() {
        for (TabView tab : this.tabs) {
            this.tabsContainer.removeView(tab.getView());
        }
        this.tabsContainer.removeAllViewsInLayout();
        this.tabs.clear();
    }

    public ViewGroup getLayout() {
        return this.tabRootLayout;
    }

    public void resetTabsSelection() {
        for (TabView tab : this.tabs) {
            tab.setSelected(false);
        }
        this.isSelectionSet = false;
    }

    public void setActiveTab(long tabId) {
        resetTabsSelection();
        Iterator i$ = this.tabs.iterator();
        while (true) {
            if (!i$.hasNext()) {
                break;
            }
            TabView tab = i$.next();
            if (tab.shouldBeSelectedForView(tabId)) {
                tab.setSelected(true);
                this.isSelectionSet = true;
                currentTabId = tabId;
                break;
            }
        }
        if (!this.isSelectionSet && this.tabs.size() > 0) {
            this.tabs.get(this.tabs.size() - 1).setSelected(true);
            this.isSelectionSet = true;
        }
    }

    public long getCurrentTab() {
        return currentTabId;
    }

    private void initTabContainerLayout() {
        this.tabRootLayout = (ViewGroup) ViewUtils.loadLayout(this.context, this.orientation == 1 ? R.layout.navigation_bar : R.layout.navigation_bar_vertical);
        this.tabsContainer = (ViewGroup) this.tabRootLayout.findViewById(R.id.navigation_tab_container);
        this.tabsContainer.removeAllViews();
    }
}
