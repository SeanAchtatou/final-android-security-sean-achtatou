package com.biznessapps.api.navigation;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.utils.google.caching.ImageFetcher;
import com.biznessapps.utils.google.caching.ImageWorker;

public class TabView {
    /* access modifiers changed from: private */
    public final Runnable clickAction;
    private final Context context;
    private final int imageResourceId;
    private final String imageResourceUrl;
    private ImageView imageView;
    private boolean isTabActive;
    private ViewGroup tabRootLayout;
    private TabViewSettings tabSettings;
    private TextView titleView;
    private boolean withOldDesign;

    public TabView(Context context2, int imageResourceId2, String imageResourceUrl2, Runnable clickAction2, boolean withOldDesign2, boolean isTabActive2, TabViewSettings tabSettings2) {
        this.context = context2;
        this.imageResourceId = imageResourceId2;
        this.clickAction = clickAction2;
        this.withOldDesign = withOldDesign2;
        this.isTabActive = isTabActive2;
        this.imageResourceUrl = imageResourceUrl2;
        this.tabSettings = tabSettings2;
        initTabViews();
        this.tabRootLayout.setOnClickListener(getTabOnClickListener());
    }

    public boolean withOldDesign() {
        return this.withOldDesign;
    }

    public void setSelected(boolean isSelected) {
        if (this.withOldDesign) {
            if (isSelected) {
                this.tabRootLayout.setBackgroundResource(R.drawable.selected_tab_background);
                this.titleView.setTextColor(-1);
            } else {
                this.tabRootLayout.setBackgroundResource(R.drawable.navigation_background);
                this.titleView.setTextColor(-3355444);
            }
            this.imageView.setSelected(isSelected);
        }
    }

    public void setEnable(boolean isEnabled) {
        this.imageView.setEnabled(isEnabled);
    }

    public boolean shouldBeSelectedForView(long tabId) {
        return tabId == this.tabSettings.getTabId();
    }

    public ViewGroup getView() {
        return this.tabRootLayout;
    }

    private View.OnClickListener getTabOnClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                if (TabView.this.clickAction != null) {
                    TabView.this.clickAction.run();
                    TabView.this.setSelected(true);
                }
            }
        };
    }

    private void initTabViews() {
        int i;
        this.tabRootLayout = (ViewGroup) ViewUtils.loadLayout(this.context, R.layout.tab);
        this.imageView = (ImageView) this.tabRootLayout.findViewById(R.id.navigation_image_view);
        ImageFetcher imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
        if (this.imageResourceId > 0) {
            this.imageView.setImageResource(this.imageResourceId);
        } else if (StringUtils.isNotEmpty(this.imageResourceUrl)) {
            imageFetcher.loadImage(this.imageResourceUrl, this.imageView);
        }
        this.titleView = (TextView) this.tabRootLayout.findViewById(R.id.navigation_text_view);
        this.titleView.setText(this.tabSettings.getTabName());
        this.titleView.setTextColor(-3355444);
        if (!this.isTabActive) {
            this.tabRootLayout.setVisibility(4);
            this.tabRootLayout.setEnabled(false);
        }
        if (!this.withOldDesign) {
            this.titleView.setTextColor(ViewUtils.getColor(this.tabSettings.getTabTextColor()));
            ImageWorker.TintContainer tint = new ImageWorker.TintContainer();
            tint.setTintColor(this.tabSettings.getTabTint());
            tint.setTintOpacity(this.tabSettings.getTabTintOpacity());
            TextView textView = this.titleView;
            if (this.tabSettings.isShowText()) {
                i = 0;
            } else {
                i = 4;
            }
            textView.setVisibility(i);
            imageFetcher.loadImage(this.tabSettings.getTabBgUrl(), this.tabRootLayout, tint);
        }
    }
}
