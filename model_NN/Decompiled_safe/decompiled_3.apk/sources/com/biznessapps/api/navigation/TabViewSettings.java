package com.biznessapps.api.navigation;

public class TabViewSettings {
    private boolean showText;
    private String tabBgUrl;
    private long tabId;
    private int tabImageId;
    private String tabImageUrl;
    private String tabName;
    private String tabTextColor;
    private String tabTint;
    private float tabTintOpacity;

    public boolean isShowText() {
        return this.showText;
    }

    public void setShowText(boolean showText2) {
        this.showText = showText2;
    }

    public String getTabTint() {
        return this.tabTint;
    }

    public void setTabTint(String tabTint2) {
        this.tabTint = tabTint2;
    }

    public float getTabTintOpacity() {
        return this.tabTintOpacity;
    }

    public void setTabTintOpacity(float tabTintOpacity2) {
        this.tabTintOpacity = tabTintOpacity2;
    }

    public String getTabTextColor() {
        return this.tabTextColor;
    }

    public void setTabTextColor(String tabTextColor2) {
        this.tabTextColor = tabTextColor2;
    }

    public String getTabName() {
        return this.tabName;
    }

    public void setTabName(String tabName2) {
        this.tabName = tabName2;
    }

    public String getTabImageUrl() {
        return this.tabImageUrl;
    }

    public void setTabImageUrl(String tabImageUrl2) {
        this.tabImageUrl = tabImageUrl2;
    }

    public int getTabImageId() {
        return this.tabImageId;
    }

    public void setTabImageId(int tabImageId2) {
        this.tabImageId = tabImageId2;
    }

    public long getTabId() {
        return this.tabId;
    }

    public void setTabId(long tabId2) {
        this.tabId = tabId2;
    }

    public String getTabBgUrl() {
        return this.tabBgUrl;
    }

    public void setTabBgUrl(String tabBgUrl2) {
        this.tabBgUrl = tabBgUrl2;
    }
}
