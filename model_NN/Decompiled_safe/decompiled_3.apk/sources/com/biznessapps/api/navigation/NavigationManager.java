package com.biznessapps.api.navigation;

import android.app.Activity;
import android.content.Intent;
import android.view.ViewGroup;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.single.MoreFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.Tab;
import com.biznessapps.model.TabButton;
import java.util.ArrayList;
import java.util.List;

public class NavigationManager {
    private static final int DEFAULT_TAB_LIMIT = 5;
    private static final int SIDE_TAB_LIMIT = 6;
    private static List<Tab> tabsItems;
    private final Activity activity;
    private NavigationBar navigationBar;
    private List<Tab> rowTabsItems;
    private int tabLimit = 5;
    private boolean useUnlimitTabCount;
    private boolean withOldDesign;

    public void setSideTabLimit() {
        this.tabLimit = 6;
    }

    public static List<Tab> getTabsItems() {
        return tabsItems;
    }

    public static void setTabsItems(List<Tab> tabsItems2) {
        tabsItems = tabsItems2;
    }

    public static void clear() {
        if (tabsItems != null) {
            tabsItems.clear();
        }
    }

    public void setTabCount(int tabCount) {
        this.navigationBar.setTabCount(tabCount);
    }

    public boolean useUnlimitTabCount() {
        return this.useUnlimitTabCount;
    }

    public void setUseUnlimitTabCount(boolean useUnlimitTabCount2) {
        this.useUnlimitTabCount = useUnlimitTabCount2;
    }

    public List<Tab> getRowTabsItems() {
        return this.rowTabsItems;
    }

    public void setRowTabsItems(List<Tab> rowTabsItems2) {
        this.rowTabsItems = rowTabsItems2;
        setTabCount(rowTabsItems2.size());
    }

    public NavigationManager(Activity activity2) {
        this.activity = activity2;
        initNavigationBar(1);
        this.withOldDesign = true;
    }

    public NavigationManager(Activity activity2, int orientation) {
        this.activity = activity2;
        initNavigationBar(orientation);
    }

    public NavigationManager(Activity activity2, boolean withOldDesign2) {
        this.activity = activity2;
        initNavigationBar(1);
        this.withOldDesign = withOldDesign2;
    }

    private void initNavigationBar(int orientation) {
        this.navigationBar = new NavigationBar(this.activity.getApplicationContext(), orientation);
    }

    public void clearTabs() {
        this.navigationBar.clearTabs();
    }

    public void updateTabs() {
        if (this.rowTabsItems == null) {
            this.rowTabsItems = new ArrayList();
        }
        int tabCount = 1;
        List<TabButton> tabButtons = new ArrayList<>();
        for (Tab tab : this.rowTabsItems) {
            if (tabCount < this.tabLimit || this.rowTabsItems.size() == this.tabLimit || this.useUnlimitTabCount) {
                addNewTab(tab, tab.getLabel(), false);
                tabCount++;
            } else {
                tabButtons.add(new TabButton(tab));
            }
        }
        if (!tabButtons.isEmpty()) {
            Tab moreTab = new Tab();
            moreTab.setViewController(ServerConstants.MORE_VIEW_CONTROLLER);
            MoreFragment.setTabButtons(tabButtons);
            addNewTab(moreTab, this.activity.getString(R.string.more), true);
        }
    }

    public void setTabSelection(long tabId) {
        this.navigationBar.setActiveTab(tabId);
    }

    public long getCurrentTabSelection() {
        return this.navigationBar.getCurrentTab();
    }

    public boolean openFirstTab() {
        return this.navigationBar.openFirstTab();
    }

    public void addLayoutTo(ViewGroup view) {
        view.addView(this.navigationBar.getLayout());
    }

    public void resetTabsSelection() {
        this.navigationBar.resetTabsSelection();
    }

    /* access modifiers changed from: private */
    public void onViewChanged(Intent intent) {
        this.navigationBar.resetTabsSelection();
        goToNewView(intent);
    }

    private void goToNewView(Intent intent) {
        this.activity.startActivity(intent);
    }

    private void addNewTab(Tab tab, String tabName, boolean isMoreTab) {
        String imageUrl;
        final Intent comingIntent = new Intent(this.activity.getApplicationContext(), SingleFragmentActivity.class);
        comingIntent.putExtra(AppConstants.TAB_ID, tab.getId());
        comingIntent.putExtra(AppConstants.ITEM_ID, tab.getItemId());
        comingIntent.putExtra(AppConstants.TAB_SPECIAL_ID, tab.getTabId());
        comingIntent.putExtra(AppConstants.URL, tab.getUrl());
        comingIntent.putExtra(AppConstants.SECTION_ID, tab.getSectionId());
        comingIntent.putExtra(AppConstants.TAB_LABEL, tabName);
        comingIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, tab.getViewController());
        comingIntent.putExtra(AppConstants.TAB, tab);
        TabViewSettings tabSettings = new TabViewSettings();
        int imageId = 0;
        AppSettings settings = AppCore.getInstance().getAppSettings();
        if (tab.hasCustomDesign()) {
            imageUrl = tab.getTabIcon() + tab.getImage();
        } else {
            imageUrl = settings.getTabIcon() + tab.getImage();
        }
        if (isMoreTab) {
            if (settings.hasMoreButtonNavigation()) {
                imageUrl = settings.getMoreIconUrl();
            } else {
                imageId = R.drawable.icon_more;
            }
        }
        tabSettings.setShowText(tab.hasCustomDesign() ? tab.isShowText() : settings.isShowText());
        tabSettings.setTabTint(tab.hasCustomDesign() ? tab.getTabTint() : settings.getTabTint());
        tabSettings.setTabTintOpacity(tab.hasCustomDesign() ? tab.getTabTintOpacity() : settings.getTabTintOpacity());
        tabSettings.setTabTextColor(tab.hasCustomDesign() ? tab.getTabText() : settings.getTabText());
        tabSettings.setTabBgUrl(tab.hasCustomDesign() ? tab.getTabSrc() : settings.getTabSrc());
        tabSettings.setTabName(tabName);
        tabSettings.setTabId(tab.getId());
        this.navigationBar.addTab(new TabView(this.activity.getApplicationContext(), imageId, imageUrl, new Runnable() {
            public void run() {
                NavigationManager.this.onViewChanged(comingIntent);
            }
        }, this.withOldDesign, tab.isActive(), tabSettings), this.tabLimit);
    }
}
