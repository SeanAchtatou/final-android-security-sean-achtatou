package com.biznessapps.api;

import com.biznessapps.storage.StorageKeeper;
import java.util.Map;

public class DataSource {
    private static DataSource instance;

    public static DataSource getInstance() {
        if (instance == null) {
            instance = new DataSource();
        }
        return instance;
    }

    private DataSource() {
    }

    public String getData(String url) {
        return getData(url, (Map<String, String>[]) null);
    }

    public String getData(String url, String userName, String userPassword) {
        if (isOnline()) {
            String data = AppHttpUtils.executeGetSyncRequest(url, userName, userPassword);
            StorageKeeper.instance().saveCacheItem(url + userName + userPassword, data);
            return data;
        } else if (canUseOfflineData()) {
            return StorageKeeper.instance().getCachedItem(url + userName + userPassword);
        } else {
            return null;
        }
    }

    public String getData(String url, Map<String, String>... params) {
        String data;
        if (isOnline()) {
            if (params == null || params.length <= 0) {
                data = AppHttpUtils.executeGetSyncRequest(url);
            } else {
                data = AppHttpUtils.executePostRequestSync(url, params[0]);
            }
            StorageKeeper.instance().saveCacheItem(url, data);
            return data;
        } else if (canUseOfflineData()) {
            return StorageKeeper.instance().getCachedItem(url);
        } else {
            return null;
        }
    }

    public void getData(AsyncCallback<?> callback, String url) {
        if (isOnline()) {
            AppHttpUtils.executeGetRequest(url, callback);
        } else if (canUseOfflineData()) {
            callback.onResult(StorageKeeper.instance().getCachedItem(url));
        } else {
            callback.onError((String) null, (Throwable) null);
        }
    }

    public String getBearerAccessToken(String consumerKey, String consumerSecret) {
        if (isOnline()) {
            String data = AppHttpUtils.getBearerAccessToken(consumerKey, consumerSecret);
            StorageKeeper.instance().saveCacheItem(consumerKey + consumerSecret, data);
            return data;
        } else if (canUseOfflineData()) {
            return StorageKeeper.instance().getCachedItem(consumerKey + consumerSecret);
        } else {
            return null;
        }
    }

    private boolean isOnline() {
        return AppCore.getInstance().isAnyConnectionAvailable();
    }

    private boolean canUseOfflineData() {
        return AppCore.getInstance().canUseOfflineMode();
    }
}
