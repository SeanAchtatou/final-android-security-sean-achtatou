package com.biznessapps.api;

import android.os.Handler;
import android.util.Base64;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpUtils {
    private static final String AUTHORIZATION = "Authorization";
    private static final String BASIC = "Basic ";
    private static final String BEARER = "Bearer ";
    private static final String CLIENT_CREDENTIALS = "client_credentials";
    private static final int CONNECTION_TIMEOUT = 60000;
    private static final String CREDENTIALS_FORMAT = "%s:%s";
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final String GRANT_TYPE = "grant_type";
    private static final String POST = "POST";
    private static final int SOCKET_TIMEOUT = 60000;
    private static final String TAG = "HttpUtils";
    private static final int THREAD_POOL_SIZE = 5;
    private ExecutorService executors;

    public HttpUtils(ExecutorService executors2) {
        this.executors = null;
        this.executors = executors2;
    }

    public HttpUtils() {
        this(Executors.newFixedThreadPool(5));
    }

    public void executeGetRequestAsync(String baseUrl, String[] names, String[] values, AsyncCallback<?> callback) {
        executeRequestAsync(baseUrl, null, null, callback);
    }

    public void executePostRequestAsync(String baseUrl, String[] names, String[] values, AsyncCallback<?> callback) {
        executeRequestAsync(baseUrl, names, values, callback);
    }

    public String executePostRequestSync(String url, String[] names, String[] values) {
        try {
            return responseToString(executePostHttpRequest(url, names, values));
        } catch (ClientProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public synchronized void sendPhotoAsync(byte[] data, String appCode, String tabId, String eventId, AsyncCallback<?> callback) {
        final byte[] bArr = data;
        final String str = appCode;
        final String str2 = tabId;
        final String str3 = eventId;
        final AsyncCallback<?> asyncCallback = callback;
        this.executors.execute(new Runnable() {
            public void run() {
                HttpUtils.this.sendPhoto(bArr, str, str2, str3, asyncCallback);
            }
        });
    }

    public synchronized void postCommentAsync(String url, String appCode, String tabId, String id, String userType, String userId, String name, String comment, String hash, String parentId, double longitude, double latitude, byte[] imageData, AsyncCallback<?> callback) {
        final String str = url;
        final String str2 = appCode;
        final String str3 = tabId;
        final String str4 = id;
        final String str5 = userType;
        final String str6 = userId;
        final String str7 = name;
        final String str8 = comment;
        final String str9 = hash;
        final String str10 = parentId;
        final double d = longitude;
        final double d2 = latitude;
        final byte[] bArr = imageData;
        final AsyncCallback<?> asyncCallback = callback;
        this.executors.execute(new Runnable() {
            public void run() {
                HttpUtils.this.postComment(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, d, d2, bArr, asyncCallback);
            }
        });
    }

    public synchronized void postFanCommentAsync(AsyncCallback<?> callback, boolean useFacebookCred, String hash, String tabId, String facebookId, String twitterId, String name, String comment, String appCode, String commentParentId, boolean hasYoutubeFormat) {
        final AsyncCallback<?> asyncCallback = callback;
        final boolean z = useFacebookCred;
        final String str = hash;
        final String str2 = tabId;
        final String str3 = facebookId;
        final String str4 = twitterId;
        final String str5 = name;
        final String str6 = comment;
        final String str7 = appCode;
        final String str8 = commentParentId;
        final boolean z2 = hasYoutubeFormat;
        this.executors.execute(new Runnable() {
            public void run() {
                HttpUtils.this.postFanComment(asyncCallback, z, str, str2, str3, str4, str5, str6, str7, str8, z2);
            }
        });
    }

    public synchronized String executeRequestSync(String url) {
        String resultData;
        String resultData2 = "";
        try {
            HttpResponse response = executeGetHttpRequest(url);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine == null || !(statusLine == null || statusLine.getStatusCode() == 200)) {
                System.gc();
                resultData = resultData2;
            } else {
                resultData2 = responseToString(response);
                System.gc();
                resultData = resultData2;
            }
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
            System.gc();
        } catch (Throwable th) {
            System.gc();
            throw th;
        }
        return resultData;
    }

    public synchronized String executeRequestSync(String url, String username, String password) {
        String resultData;
        String resultData2 = "";
        try {
            HttpResponse response = executeGetHttpRequest(url, username, password);
            StatusLine statusLine = response.getStatusLine();
            if (statusLine == null || !(statusLine == null || statusLine.getStatusCode() == 200)) {
                System.gc();
                resultData = resultData2;
            } else {
                resultData2 = responseToString(response);
                System.gc();
                resultData = resultData2;
            }
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
            System.gc();
        } catch (Throwable th) {
            System.gc();
            throw th;
        }
        return resultData;
    }

    public static HttpURLConnection makeConnection(String toUrl, String userName, String password) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(toUrl).openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setReadTimeout(0);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestProperty("Authorization", getAuthorizationHeader(userName, password));
        connection.setRequestMethod("POST");
        return connection;
    }

    private static String getAuthorizationHeader(String userName, String userPassword) throws IOException {
        return BASIC + Base64.encodeToString(String.format(CREDENTIALS_FORMAT, userName, userPassword).getBytes(DEFAULT_CHARSET), 2);
    }

    private void passResult(final AsyncCallback<?> callback, final String result) {
        Handler handler = AppHandlers.getHandler();
        if (handler != null) {
            handler.post(new Runnable() {
                public void run() {
                    if (callback != null) {
                        callback.onResult(result);
                    }
                }
            });
        }
    }

    private void passError(final AsyncCallback<?> callback, final String message, final Throwable throwable) {
        Handler handler = AppHandlers.getHandler();
        if (handler != null) {
            handler.post(new Runnable() {
                public void run() {
                    if (callback != null) {
                        callback.onError(message, throwable);
                    }
                }
            });
        }
    }

    private synchronized void executeRequestAsync(String baseUrl, String[] names, String[] values, AsyncCallback<?> callback) {
        if (!(this.executors.isShutdown() || this.executors.isTerminated())) {
            final String str = baseUrl;
            final String[] strArr = names;
            final String[] strArr2 = values;
            final AsyncCallback<?> asyncCallback = callback;
            this.executors.execute(new Runnable() {
                public void run() {
                    HttpUtils.this.executeRequest(str, strArr, strArr2, asyncCallback);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void postFanComment(AsyncCallback<?> callback, boolean useFacebookCred, String hash, String tabId, String facebookId, String twitterId, String name, String comment, String appCode, String commentParentId, boolean hasYoutubeFormat) {
        String avatarProfileImage = null;
        if (StringUtils.isNotEmpty(twitterId)) {
            try {
                avatarProfileImage = JsonParserUtils.getTwitterIconUrl(getTwitterData(ServerConstants.GET_TWITTER_PROFILE_URL + name, AppCore.getInstance().getBearerAccessToken()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        StringBuilder requestString = new StringBuilder();
        requestString.append("?hash=").append(hash);
        requestString.append("&tab_id=").append(tabId);
        if (useFacebookCred) {
            requestString.append("&fb_id=").append(facebookId);
        } else {
            requestString.append("&tw_id=").append(twitterId);
        }
        requestString.append("&name=").append(name);
        requestString.append("&comment=").append(URLEncoder.encode(comment));
        requestString.append("&app_code=").append(appCode);
        if (StringUtils.isNotEmpty(commentParentId)) {
            if (hasYoutubeFormat) {
                requestString.append("&yt_id=").append(commentParentId);
            } else {
                requestString.append("&parent_id=").append(commentParentId);
            }
        }
        if (StringUtils.isNotEmpty(avatarProfileImage)) {
            requestString.append("&avatar=").append(avatarProfileImage);
        }
        executeGetRequestAsync(ServerConstants.FAN_WALL_POST + requestString.toString(), null, null, callback);
    }

    /* access modifiers changed from: private */
    public void postComment(String url, String appCode, String tabId, String id, String userType, String userId, String name, String comment, String hash, String parentId, double longitude, double latitude, byte[] imageData, AsyncCallback<?> callback) {
        String avatarProfileImage = null;
        if (userType == AppConstants.TWITTER_USER_TYPE) {
            try {
                avatarProfileImage = JsonParserUtils.getTwitterIconUrl(getTwitterData(ServerConstants.GET_TWITTER_PROFILE_URL + name, AppCore.getInstance().getBearerAccessToken()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(ServerConstants.COMMENT_POST_FORMAT);
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart(ServerConstants.POST_APP_CODE_PARAM, new StringBody(appCode));
            reqEntity.addPart(ServerConstants.POST_TAB_ID_PARAM, new StringBody(tabId));
            reqEntity.addPart(ServerConstants.POST_USER_TYPE_PARAM, new StringBody(userType));
            reqEntity.addPart(ServerConstants.POST_USER_ID_PARAM, new StringBody(userId));
            reqEntity.addPart(ServerConstants.POST_NAME_PARAM, new StringBody(name));
            reqEntity.addPart(ServerConstants.POST_HASH_PARAM, new StringBody(hash));
            if (StringUtils.isNotEmpty(comment)) {
                reqEntity.addPart("comment", new StringBody(comment));
            }
            if (StringUtils.isNotEmpty(parentId)) {
                reqEntity.addPart("parent_id", new StringBody(parentId));
            }
            if (longitude > 0.0d) {
                reqEntity.addPart("longitude", new StringBody("" + longitude));
            }
            if (latitude > 0.0d) {
                reqEntity.addPart("latitude", new StringBody("" + latitude));
            }
            if (StringUtils.isNotEmpty(id)) {
                reqEntity.addPart("id", new StringBody(id));
            }
            if (StringUtils.isNotEmpty(avatarProfileImage)) {
                reqEntity.addPart(ServerConstants.POST_AVATAR_PARAM, new StringBody(avatarProfileImage));
            }
            if (imageData != null) {
                reqEntity.addPart(ServerConstants.POST_IMAGE_PARAM, new ByteArrayBody(imageData, AppConstants.IMAGE_MIME_TYPE, "image.jpg"));
            }
            postRequest.setEntity(reqEntity);
            passResult(callback, httpClient.execute(postRequest).getStatusLine().toString());
        } catch (Throwable e2) {
            passError(callback, "image upload failed", e2);
        }
    }

    /* access modifiers changed from: private */
    public void sendPhoto(byte[] data, String appCode, String tabId, String eventId, AsyncCallback<?> callback) {
        try {
            HttpClient httpClient = new DefaultHttpClient();
            HttpPost postRequest = new HttpPost(ServerConstants.PHOTO_POST);
            StringBody appCodeBody = new StringBody(appCode);
            StringBody tabIdBody = new StringBody(tabId);
            StringBody eventIdBody = new StringBody(eventId);
            ByteArrayBody bab = new ByteArrayBody(data, AppConstants.IMAGE_MIME_TYPE, "caption.jpg");
            MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            reqEntity.addPart(ServerConstants.POST_APP_CODE_PARAM, appCodeBody);
            reqEntity.addPart(ServerConstants.POST_TAB_ID_PARAM, tabIdBody);
            reqEntity.addPart(ServerConstants.POST_IMAGE_PARAM, bab);
            reqEntity.addPart("id", eventIdBody);
            postRequest.setEntity(reqEntity);
            passResult(callback, httpClient.execute(postRequest).getStatusLine().toString());
        } catch (Throwable e) {
            passError(callback, "image upload failed", e);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0015 A[Catch:{ Throwable -> 0x002c }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0023 A[Catch:{ Throwable -> 0x002c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void executeRequest(java.lang.String r12, java.lang.String[] r13, java.lang.String[] r14, com.biznessapps.api.AsyncCallback<?> r15) {
        /*
            r11 = this;
            r6 = 1
            r7 = 0
            java.lang.String r5 = r11.replaceBadSymbols(r12)
            r3 = 0
            if (r13 == 0) goto L_0x0021
            if (r14 == 0) goto L_0x0021
            int r8 = r13.length     // Catch:{ Throwable -> 0x002c }
            if (r8 <= 0) goto L_0x0021
            int r8 = r14.length     // Catch:{ Throwable -> 0x002c }
            if (r8 <= 0) goto L_0x0021
            r2 = r6
        L_0x0012:
            r4 = 0
            if (r2 == 0) goto L_0x0023
            org.apache.http.HttpResponse r3 = executePostHttpRequest(r5, r13, r14)     // Catch:{ Throwable -> 0x002c }
            java.lang.String r4 = responseToString(r3)     // Catch:{ Throwable -> 0x002c }
        L_0x001d:
            r11.passResult(r15, r4)     // Catch:{ Throwable -> 0x002c }
        L_0x0020:
            return
        L_0x0021:
            r2 = r7
            goto L_0x0012
        L_0x0023:
            org.apache.http.HttpResponse r3 = executeGetHttpRequest(r5)     // Catch:{ Throwable -> 0x002c }
            java.lang.String r4 = responseToString(r3)     // Catch:{ Throwable -> 0x002c }
            goto L_0x001d
        L_0x002c:
            r0 = move-exception
            java.lang.String r1 = "Couldn't connect to server"
            java.lang.String r8 = "HttpUtils"
            java.lang.String r9 = "Throwable: %s"
            java.lang.Object[] r10 = new java.lang.Object[r6]
            if (r0 == 0) goto L_0x0048
            java.lang.String r6 = r0.toString()
        L_0x003b:
            r10[r7] = r6
            java.lang.String r6 = java.lang.String.format(r9, r10)
            android.util.Log.d(r8, r6)
            r11.passError(r15, r1, r0)
            goto L_0x0020
        L_0x0048:
            r6 = r1
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.api.HttpUtils.executeRequest(java.lang.String, java.lang.String[], java.lang.String[], com.biznessapps.api.AsyncCallback):void");
    }

    private String replaceBadSymbols(String url) {
        return url.replace(" ", "%20");
    }

    private static HttpClient newHttpClientInstance() {
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 60000);
        HttpConnectionParams.setSoTimeout(client.getParams(), 60000);
        HttpProtocolParams.setContentCharset(client.getParams(), AppConstants.UTF_8_CHARSET);
        return client;
    }

    private static HttpResponse executeGetHttpRequest(String url) throws ClientProtocolException, IOException {
        HttpClient client = newHttpClientInstance();
        HttpUriRequest request = new HttpGet(url);
        request.addHeader(AppConstants.ACCEPT_ENCODING, AppConstants.GZIP);
        return client.execute(request);
    }

    private static HttpResponse executeGetHttpRequest(String url, String userName, String userPassword) throws ClientProtocolException, IOException {
        HttpClient client = newHttpClientInstance();
        HttpUriRequest request = new HttpGet(url);
        request.addHeader("Authorization", getAuthorizationHeader(userName, userPassword));
        request.addHeader(AppConstants.ACCEPT_ENCODING, AppConstants.GZIP);
        return client.execute(request);
    }

    public static HttpResponse executePostHttpRequest(String baseUrl, String[] names, String[] values) throws ClientProtocolException, IOException {
        HttpClient client = newHttpClientInstance();
        HttpPost request = new HttpPost(baseUrl);
        if ((names == null || values == null) ? false : true) {
            List<NameValuePair> nameValuePairs = new ArrayList<>(values.length);
            for (int i = 0; i < values.length; i++) {
                nameValuePairs.add(new BasicNameValuePair(names[i], values[i]));
            }
            try {
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs, AppConstants.UTF_8_CHARSET));
            } catch (UnsupportedEncodingException e) {
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            }
        }
        request.addHeader("Content-Type", AppConstants.CONTENT_TYPE_DESCRIPTION);
        return client.execute(request);
    }

    public static String getTwitterData(String url, String bearerAccessToken) {
        try {
            HttpClient client = newHttpClientInstance();
            HttpUriRequest request = new HttpGet(url);
            request.setHeader(AppConstants.ACCEPT_ENCODING, AppConstants.GZIP);
            request.setHeader("Authorization", BEARER + bearerAccessToken);
            return responseToString(client.execute(request));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void updateAvatarImage(String url) {
        try {
            DataSource.getInstance().getData(url);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getBearerAccessTokenData(String consumerKey, String consumerSecret) {
        try {
            String encodedKey = URLEncoder.encode(consumerKey);
            String encodedBearerTokenCred = Base64.encodeToString((encodedKey + ":" + URLEncoder.encode(consumerSecret)).getBytes(AppConstants.UTF_8_CHARSET), 2);
            HttpClient client = newHttpClientInstance();
            HttpPost request = new HttpPost("https://api.twitter.com/oauth2/token");
            request.setHeader("Authorization", BASIC + encodedBearerTokenCred);
            List<NameValuePair> nameValuePairs = new ArrayList<>(1);
            nameValuePairs.add(new BasicNameValuePair(GRANT_TYPE, CLIENT_CREDENTIALS));
            try {
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs, AppConstants.UTF_8_CHARSET));
            } catch (UnsupportedEncodingException e) {
                request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            }
            return responseToString(client.execute(request));
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: finally extract failed */
    private static String convertStreamToString(InputStream in, Charset c) throws IOException {
        GZIPInputStream is = new GZIPInputStream(in);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, c));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line != null) {
                    sb.append(line + AppConstants.NEW_LINE);
                } else {
                    is.close();
                    return sb.toString();
                }
            } catch (Throwable th) {
                is.close();
                throw th;
            }
        }
    }

    private static final String responseToString(HttpResponse response) throws IOException {
        if (response.getEntity().getContentEncoding() != null && AppConstants.GZIP.equalsIgnoreCase(response.getEntity().getContentEncoding().getValue())) {
            return convertStreamToString(response.getEntity().getContent(), Charset.forName("US-ASCII"));
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), Charset.forName("US-ASCII")));
        StringBuilder builder = new StringBuilder();
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            builder.append(line);
        }
        return builder.toString();
    }
}
