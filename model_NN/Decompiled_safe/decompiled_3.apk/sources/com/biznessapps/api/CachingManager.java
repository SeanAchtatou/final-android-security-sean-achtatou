package com.biznessapps.api;

import android.content.Context;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.fragments.reservation.ReservationDataKeeper;
import com.biznessapps.model.App;
import com.biznessapps.model.Tab;
import com.biznessapps.storage.StorageKeeper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CachingManager implements AppConstants {
    private String facebookUid;
    private String facebookUserName;
    private int lastLoginType;
    private String musicTabId;
    private Map<String, Object> propertiesMap = new HashMap();
    private ReservationDataKeeper reservSystemCacher = new ReservationDataKeeper();
    private List<Tab> tabList = new ArrayList();
    private String twitterOauthSecret;
    private String twitterOauthToken;
    private String twitterUid;
    private String twitterUserName;

    public void clearCache() {
        this.propertiesMap.clear();
        this.tabList.clear();
        this.reservSystemCacher = new ReservationDataKeeper();
    }

    public void saveInSharedPreferences(Context context, String dataToSave, String key) {
        context.getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0).edit().putString(key, dataToSave).commit();
    }

    public String getFromSharedPreferences(Context context, String key) {
        return context.getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0).getString(key, null);
    }

    public ReservationDataKeeper getReservSystemCacher() {
        return this.reservSystemCacher;
    }

    public <T> boolean saveData(String key, T dataToSave) {
        boolean correctdata = false;
        if (dataToSave != null) {
            if (dataToSave instanceof List) {
                correctdata = !((List) dataToSave).isEmpty();
            } else {
                correctdata = true;
            }
        }
        if (correctdata) {
            this.propertiesMap.put(key, dataToSave);
        }
        return correctdata;
    }

    public <T> T getData(String key) {
        return this.propertiesMap.get(key);
    }

    public void removeData(String key) {
        this.propertiesMap.remove(key);
    }

    public <T> boolean saveDataToDB(String key, T dataToSave) {
        boolean correctdata = false;
        if (dataToSave != null) {
            if (dataToSave instanceof List) {
                correctdata = !((List) dataToSave).isEmpty();
            } else {
                correctdata = true;
            }
        }
        if (correctdata) {
            StorageKeeper.instance().put(key, dataToSave);
        }
        return correctdata;
    }

    public String getDataFromDB(String key) {
        return StorageKeeper.instance().get(key);
    }

    public boolean isDataDamaged() {
        if (this.propertiesMap.size() == 0) {
            return true;
        }
        for (String property : this.propertiesMap.keySet()) {
            if (this.propertiesMap.get(property) == null) {
                return true;
            }
        }
        return false;
    }

    public boolean isLoggenInSocial() {
        return (this.facebookUid == null && this.twitterUid == null) ? false : true;
    }

    public String getFacebookUid() {
        return this.facebookUid;
    }

    public int getLastLoginType() {
        return this.lastLoginType;
    }

    public void setLastLoginType(int lastLoginType2) {
        this.lastLoginType = lastLoginType2;
    }

    public void setFacebookUid(String facebookUid2) {
        this.facebookUid = facebookUid2;
    }

    public String getFacebookUserName() {
        return this.facebookUserName;
    }

    public void setFacebookUserName(String facebookUserName2) {
        this.facebookUserName = facebookUserName2;
    }

    public boolean hasFacebookData() {
        return (getFacebookUid() == null || getFacebookUserName() == null) ? false : true;
    }

    public boolean hasTwitterData() {
        return (getTwitterOauthSecret() == null || getTwitterOauthToken() == null) ? false : true;
    }

    public String getTwitterUid() {
        return this.twitterUid;
    }

    public void setTwitterUid(String twitterUid2) {
        this.twitterUid = twitterUid2;
    }

    public String getTwitterUserName() {
        return this.twitterUserName;
    }

    public void setTwitterUserName(String twitterUserName2) {
        this.twitterUserName = twitterUserName2;
    }

    public String getTwitterOauthToken() {
        return this.twitterOauthToken;
    }

    public void setTwitterOauthToken(String twitterOauthToken2) {
        this.twitterOauthToken = twitterOauthToken2;
    }

    public String getTwitterOauthSecret() {
        return this.twitterOauthSecret;
    }

    public void setTwitterOauthSecret(String twitterOauthSecret2) {
        this.twitterOauthSecret = twitterOauthSecret2;
    }

    public App getAppInfo() {
        return (App) this.propertiesMap.get(CachingConstants.APP_INFO_PROPERTY);
    }

    public void setAppInfo(App appInfo) {
        this.propertiesMap.put(CachingConstants.APP_INFO_PROPERTY, appInfo);
    }

    public String getAppCode() {
        return (String) this.propertiesMap.get(CachingConstants.APP_CODE_PROPERTY);
    }

    public void setAppCode(String appCode) {
        this.propertiesMap.put(CachingConstants.APP_CODE_PROPERTY, appCode);
    }

    public List<Tab> getTabList() {
        return this.tabList;
    }

    public void setTabList(List<Tab> tabList2) {
        this.tabList = tabList2;
    }

    public String getMusicTabId() {
        return this.musicTabId;
    }

    public void setMusicTabId(String musicTabId2) {
        this.musicTabId = musicTabId2;
    }
}
