package com.biznessapps.images;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;
import com.biznessapps.api.AppCore;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.google.caching.ImageCache;
import com.biznessapps.utils.google.caching.ImageFetcher;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BitmapDownloader {
    private static final int FIRST_INDEX = 0;
    private static final String LOG_TAG = "Downloader";
    private static final int MAX_ASYNC_TASKS_COUNT = 8;
    /* access modifiers changed from: private */
    public List<BitmapDownloaderTask> activeTasks = Collections.synchronizedList(new LinkedList());
    private List<BitmapDownloaderTask> waitingTasks = Collections.synchronizedList(new LinkedList());
    private List<String> waitingTasksUrls = Collections.synchronizedList(new LinkedList());

    public void download(UsingParams usingParams) {
        String url = usingParams.getUrl();
        View view = usingParams.getView();
        BitmapLoadCallback callback = usingParams.getCallback();
        Bitmap cachedBitmap = getBitmapCacher().getBitmapFromMemCache(url);
        if (cachedBitmap == null) {
            cachedBitmap = getBitmapCacher().getBitmapFromDiskCache(url);
        }
        if (cachedBitmap != null) {
            cancelPotentialDownload(url, view);
            if (callback != null) {
                callback.onPostImageLoading(new BitmapWrapper(cachedBitmap, url), view);
            }
        } else if (AppCore.getInstance().isAnyConnectionAvailable()) {
            forceDownload(usingParams);
        }
    }

    private boolean hasRightBitmap(BitmapWrapper bitmapWrapper) {
        if (bitmapWrapper == null || bitmapWrapper.getBitmap() == null || bitmapWrapper.getBitmap().isRecycled()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public static ImageCache getBitmapCacher() {
        return AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().getCache();
    }

    private void forceDownload(UsingParams usingParams) {
        String url = usingParams.getUrl();
        View view = usingParams.getView();
        if (cancelPotentialDownload(url, view)) {
            BitmapDownloaderTask task = new BitmapDownloaderTask(usingParams);
            if (view != null) {
                view.setTag(task);
                if (view instanceof ImageView) {
                    ((ImageView) view).setImageBitmap(null);
                } else {
                    view.setBackgroundDrawable(null);
                }
            }
            if (this.activeTasks.size() > 8) {
                this.waitingTasks.add(task);
                this.waitingTasksUrls.add(url);
                return;
            }
            task.execute(new String[0]);
        }
    }

    /* access modifiers changed from: private */
    public void launchNewTaskIfAllowed() {
        if (this.activeTasks.size() < 8 && !this.waitingTasks.isEmpty() && !this.waitingTasksUrls.isEmpty()) {
            BitmapDownloaderTask taskToLaunch = this.waitingTasks.get(0);
            String url = this.waitingTasksUrls.get(0);
            if (taskToLaunch != null && StringUtils.isNotEmpty(url) && taskToLaunch.getStatus() != AsyncTask.Status.RUNNING && taskToLaunch.getStatus() != AsyncTask.Status.FINISHED) {
                taskToLaunch.execute(url);
                this.waitingTasks.remove(taskToLaunch);
                this.waitingTasksUrls.remove(url);
            }
        }
    }

    private boolean cancelPotentialDownload(String url, View imageView) {
        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
        if (bitmapDownloaderTask == null) {
            return true;
        }
        String bitmapUrl = bitmapDownloaderTask.getUrl();
        if (bitmapUrl != null && bitmapUrl.equals(url)) {
            return false;
        }
        this.waitingTasks.remove(bitmapDownloaderTask);
        this.waitingTasksUrls.remove(bitmapUrl);
        bitmapDownloaderTask.cancel(true);
        this.activeTasks.remove(bitmapDownloaderTask);
        launchNewTaskIfAllowed();
        return true;
    }

    /* access modifiers changed from: private */
    public BitmapDownloaderTask getBitmapDownloaderTask(View view) {
        if (view != null) {
            return (BitmapDownloaderTask) view.getTag();
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0088  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.Bitmap downloadBitmap(java.lang.String r14, int r15) {
        /*
            r13 = this;
            r9 = 0
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
            r0.<init>()
            r3 = 0
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x00ee, IOException -> 0x0085, IllegalStateException -> 0x00a5, Exception -> 0x00c5 }
            r4.<init>(r14)     // Catch:{ IllegalArgumentException -> 0x00ee, IOException -> 0x0085, IllegalStateException -> 0x00a5, Exception -> 0x00c5 }
            org.apache.http.HttpResponse r7 = r0.execute(r4)     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
            org.apache.http.StatusLine r10 = r7.getStatusLine()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
            int r8 = r10.getStatusCode()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
            r10 = 200(0xc8, float:2.8E-43)
            if (r8 == r10) goto L_0x001e
            r3 = r4
        L_0x001d:
            return r9
        L_0x001e:
            org.apache.http.HttpEntity r2 = r7.getEntity()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
            if (r2 == 0) goto L_0x0083
            r5 = 0
            java.io.InputStream r5 = r2.getContent()     // Catch:{ all -> 0x0059 }
            android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x0059 }
            r6.<init>()     // Catch:{ all -> 0x0059 }
            r10 = 32768(0x8000, float:4.5918E-41)
            byte[] r10 = new byte[r10]     // Catch:{ all -> 0x0059 }
            r6.inTempStorage = r10     // Catch:{ all -> 0x0059 }
            r10 = 0
            r6.inDither = r10     // Catch:{ all -> 0x0059 }
            r6.inSampleSize = r15     // Catch:{ all -> 0x0059 }
            r10 = 1
            r6.inPurgeable = r10     // Catch:{ all -> 0x0059 }
            r10 = 1
            r6.inInputShareable = r10     // Catch:{ all -> 0x0059 }
            android.graphics.Bitmap$Config r10 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ all -> 0x0059 }
            r6.inPreferredConfig = r10     // Catch:{ all -> 0x0059 }
            com.biznessapps.images.BitmapDownloader$FlushedInputStream r10 = new com.biznessapps.images.BitmapDownloader$FlushedInputStream     // Catch:{ all -> 0x0059 }
            r10.<init>(r5)     // Catch:{ all -> 0x0059 }
            r11 = 0
            android.graphics.Bitmap r10 = android.graphics.BitmapFactory.decodeStream(r10, r11, r6)     // Catch:{ all -> 0x0059 }
            if (r5 == 0) goto L_0x0053
            r5.close()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
        L_0x0053:
            r2.consumeContent()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
            r3 = r4
            r9 = r10
            goto L_0x001d
        L_0x0059:
            r10 = move-exception
            if (r5 == 0) goto L_0x005f
            r5.close()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
        L_0x005f:
            r2.consumeContent()     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
            throw r10     // Catch:{ IllegalArgumentException -> 0x0063, IOException -> 0x00eb, IllegalStateException -> 0x00e8, Exception -> 0x00e5 }
        L_0x0063:
            r1 = move-exception
            r3 = r4
        L_0x0065:
            if (r3 == 0) goto L_0x006a
            r3.abort()
        L_0x006a:
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Incorrect passed url "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r14)
            java.lang.String r11 = r11.toString()
            android.util.Log.w(r10, r11, r1)
            goto L_0x001d
        L_0x0083:
            r3 = r4
            goto L_0x001d
        L_0x0085:
            r1 = move-exception
        L_0x0086:
            if (r3 == 0) goto L_0x008b
            r3.abort()
        L_0x008b:
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "I/O error while retrieving bitmap from "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r14)
            java.lang.String r11 = r11.toString()
            android.util.Log.w(r10, r11, r1)
            goto L_0x001d
        L_0x00a5:
            r1 = move-exception
        L_0x00a6:
            if (r3 == 0) goto L_0x00ab
            r3.abort()
        L_0x00ab:
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Incorrect URL: "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r14)
            java.lang.String r11 = r11.toString()
            android.util.Log.w(r10, r11)
            goto L_0x001d
        L_0x00c5:
            r1 = move-exception
        L_0x00c6:
            if (r3 == 0) goto L_0x00cb
            r3.abort()
        L_0x00cb:
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Error while retrieving bitmap from "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r14)
            java.lang.String r11 = r11.toString()
            android.util.Log.w(r10, r11, r1)
            goto L_0x001d
        L_0x00e5:
            r1 = move-exception
            r3 = r4
            goto L_0x00c6
        L_0x00e8:
            r1 = move-exception
            r3 = r4
            goto L_0x00a6
        L_0x00eb:
            r1 = move-exception
            r3 = r4
            goto L_0x0086
        L_0x00ee:
            r1 = move-exception
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.images.BitmapDownloader.downloadBitmap(java.lang.String, int):android.graphics.Bitmap");
    }

    public void saveBitmap(Bitmap bitmap, String url) {
        AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().getCache().addBitmapToCache(url, bitmap);
    }

    public static Bitmap getBitmap(String url) {
        ImageFetcher imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
        Bitmap resultBitmap = imageFetcher.getCache().getBitmapFromMemCache(url);
        if (resultBitmap != null) {
            return imageFetcher.getCache().getBitmapFromDiskCache(url);
        }
        return resultBitmap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00c2, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c3, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e4, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00e5, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c2 A[ExcHandler: IllegalArgumentException (r2v1 'e' java.lang.IllegalArgumentException A[CUSTOM_DECLARE]), Splitter:B:4:0x000e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap downloadBitmap(java.lang.String r14) {
        /*
            r10 = 0
            android.graphics.Bitmap r9 = getBitmap(r14)
            if (r9 == 0) goto L_0x000e
            boolean r11 = r9.isRecycled()
            if (r11 != 0) goto L_0x000e
        L_0x000d:
            return r9
        L_0x000e:
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r1.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r4.<init>(r14)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            org.apache.http.HttpResponse r7 = r1.execute(r4)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            org.apache.http.StatusLine r11 = r7.getStatusLine()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            int r8 = r11.getStatusCode()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r11 = 200(0xc8, float:2.8E-43)
            if (r8 == r11) goto L_0x004c
            java.lang.String r11 = "ImageDownloader"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r12.<init>()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.String r13 = "Error "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.StringBuilder r12 = r12.append(r8)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.String r13 = " while retrieving bitmap from "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.StringBuilder r12 = r12.append(r14)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            java.lang.String r12 = r12.toString()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            android.util.Log.w(r11, r12)     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r9 = r10
            goto L_0x000d
        L_0x004c:
            org.apache.http.HttpEntity r3 = r7.getEntity()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            if (r3 == 0) goto L_0x00a2
            r5 = 0
            java.io.InputStream r5 = r3.getContent()     // Catch:{ all -> 0x007c }
            android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x007c }
            r6.<init>()     // Catch:{ all -> 0x007c }
            r11 = 0
            r6.inJustDecodeBounds = r11     // Catch:{ all -> 0x007c }
            java.net.URL r11 = new java.net.URL     // Catch:{ all -> 0x007c }
            r11.<init>(r14)     // Catch:{ all -> 0x007c }
            java.io.InputStream r5 = r11.openStream()     // Catch:{ all -> 0x007c }
            com.biznessapps.images.BitmapDownloader$FlushedInputStream r11 = new com.biznessapps.images.BitmapDownloader$FlushedInputStream     // Catch:{ all -> 0x007c }
            r11.<init>(r5)     // Catch:{ all -> 0x007c }
            r12 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r11, r12, r6)     // Catch:{ all -> 0x007c }
            if (r5 == 0) goto L_0x0077
            r5.close()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
        L_0x0077:
            r3.consumeContent()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            r9 = r0
            goto L_0x000d
        L_0x007c:
            r11 = move-exception
            if (r5 == 0) goto L_0x0082
            r5.close()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
        L_0x0082:
            r3.consumeContent()     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
            throw r11     // Catch:{ IOException -> 0x0086, IllegalStateException -> 0x00a5, Exception -> 0x00c7, IllegalArgumentException -> 0x00c2 }
        L_0x0086:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r11 = "Downloader"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r12.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r13 = "I/O error while retrieving bitmap from "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.StringBuilder r12 = r12.append(r14)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r12 = r12.toString()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            android.util.Log.w(r11, r12, r2)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
        L_0x00a2:
            r9 = r10
            goto L_0x000d
        L_0x00a5:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r11 = "Downloader"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r12.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r13 = "Incorrect URL: "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.StringBuilder r12 = r12.append(r14)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r12 = r12.toString()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            android.util.Log.w(r11, r12)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            goto L_0x00a2
        L_0x00c2:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00a2
        L_0x00c7:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r11 = "Downloader"
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            r12.<init>()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r13 = "Error while retrieving bitmap from "
            java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.StringBuilder r12 = r12.append(r14)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            java.lang.String r12 = r12.toString()     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            android.util.Log.w(r11, r12, r2)     // Catch:{ IllegalArgumentException -> 0x00c2, Exception -> 0x00e4 }
            goto L_0x00a2
        L_0x00e4:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00a2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.images.BitmapDownloader.downloadBitmap(java.lang.String):android.graphics.Bitmap");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00d3, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00d4, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00f5, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00f6, code lost:
        r2.printStackTrace();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d3 A[ExcHandler: IllegalArgumentException (r2v1 'e' java.lang.IllegalArgumentException A[CUSTOM_DECLARE]), Splitter:B:4:0x000d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap downloadBitmap(java.lang.String r13, int r14, int r15) {
        /*
            android.graphics.Bitmap r9 = getBitmap(r13)
            if (r9 == 0) goto L_0x000d
            boolean r10 = r9.isRecycled()
            if (r10 != 0) goto L_0x000d
        L_0x000c:
            return r9
        L_0x000d:
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            r1.<init>()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            org.apache.http.client.methods.HttpGet r4 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            r4.<init>(r13)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            org.apache.http.HttpResponse r7 = r1.execute(r4)     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            org.apache.http.StatusLine r10 = r7.getStatusLine()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            int r8 = r10.getStatusCode()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            r10 = 200(0xc8, float:2.8E-43)
            if (r8 == r10) goto L_0x004b
            java.lang.String r10 = "ImageDownloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            r11.<init>()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            java.lang.String r12 = "Error "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            java.lang.StringBuilder r11 = r11.append(r8)     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            java.lang.String r12 = " while retrieving bitmap from "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            java.lang.StringBuilder r11 = r11.append(r13)     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            android.util.Log.w(r10, r11)     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            r9 = 0
            goto L_0x000c
        L_0x004b:
            org.apache.http.HttpEntity r3 = r7.getEntity()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            if (r3 == 0) goto L_0x00b3
            r5 = 0
            java.io.InputStream r5 = r3.getContent()     // Catch:{ all -> 0x008d }
            android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch:{ all -> 0x008d }
            r6.<init>()     // Catch:{ all -> 0x008d }
            r10 = 1
            r6.inJustDecodeBounds = r10     // Catch:{ all -> 0x008d }
            com.biznessapps.images.BitmapDownloader$FlushedInputStream r10 = new com.biznessapps.images.BitmapDownloader$FlushedInputStream     // Catch:{ all -> 0x008d }
            r10.<init>(r5)     // Catch:{ all -> 0x008d }
            r11 = 0
            android.graphics.BitmapFactory.decodeStream(r10, r11, r6)     // Catch:{ all -> 0x008d }
            int r10 = calculateInSampleSize2(r6, r14, r15)     // Catch:{ all -> 0x008d }
            r6.inSampleSize = r10     // Catch:{ all -> 0x008d }
            r10 = 0
            r6.inJustDecodeBounds = r10     // Catch:{ all -> 0x008d }
            java.net.URL r10 = new java.net.URL     // Catch:{ all -> 0x008d }
            r10.<init>(r13)     // Catch:{ all -> 0x008d }
            java.io.InputStream r5 = r10.openStream()     // Catch:{ all -> 0x008d }
            com.biznessapps.images.BitmapDownloader$FlushedInputStream r10 = new com.biznessapps.images.BitmapDownloader$FlushedInputStream     // Catch:{ all -> 0x008d }
            r10.<init>(r5)     // Catch:{ all -> 0x008d }
            r11 = 0
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r10, r11, r6)     // Catch:{ all -> 0x008d }
            if (r5 == 0) goto L_0x0088
            r5.close()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
        L_0x0088:
            r3.consumeContent()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            r9 = r0
            goto L_0x000c
        L_0x008d:
            r10 = move-exception
            if (r5 == 0) goto L_0x0093
            r5.close()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
        L_0x0093:
            r3.consumeContent()     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
            throw r10     // Catch:{ IOException -> 0x0097, IllegalStateException -> 0x00b6, Exception -> 0x00d8, IllegalArgumentException -> 0x00d3 }
        L_0x0097:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            r11.<init>()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r12 = "I/O error while retrieving bitmap from "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.StringBuilder r11 = r11.append(r13)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r11 = r11.toString()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            android.util.Log.w(r10, r11, r2)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
        L_0x00b3:
            r9 = 0
            goto L_0x000c
        L_0x00b6:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            r11.<init>()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r12 = "Incorrect URL: "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.StringBuilder r11 = r11.append(r13)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r11 = r11.toString()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            android.util.Log.w(r10, r11)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            goto L_0x00b3
        L_0x00d3:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00b3
        L_0x00d8:
            r2 = move-exception
            r4.abort()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r10 = "Downloader"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            r11.<init>()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r12 = "Error while retrieving bitmap from "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.StringBuilder r11 = r11.append(r13)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            java.lang.String r11 = r11.toString()     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            android.util.Log.w(r10, r11, r2)     // Catch:{ IllegalArgumentException -> 0x00d3, Exception -> 0x00f5 }
            goto L_0x00b3
        L_0x00f5:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.images.BitmapDownloader.downloadBitmap(java.lang.String, int, int):android.graphics.Bitmap");
    }

    public static void loadCompositeDrawable(final Resources res, final int colorForBg, final int drawableResId, final View view) {
        new AsyncTask<Void, Void, Drawable>() {
            /* access modifiers changed from: protected */
            public Drawable doInBackground(Void... params) {
                return CommonUtils.getCompositeDrawable(res, colorForBg, drawableResId);
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Drawable result) {
                if (result != null) {
                    view.setBackgroundDrawable(result);
                }
            }
        }.execute(new Void[0]);
    }

    private static class FlushedInputStream extends FilterInputStream {
        public FlushedInputStream(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long n) throws IOException {
            long totalBytesSkipped = 0;
            while (totalBytesSkipped < n) {
                long bytesSkipped = this.in.skip(n - totalBytesSkipped);
                if (bytesSkipped == 0) {
                    if (read() < 0) {
                        break;
                    }
                    bytesSkipped = 1;
                }
                totalBytesSkipped += bytesSkipped;
            }
            return totalBytesSkipped;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, int, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    /* access modifiers changed from: private */
    public Bitmap createReflectedImages(Bitmap originalImage) {
        if (originalImage == null) {
            return null;
        }
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        int reflectionHeight = (int) (((double) height) * 0.25d);
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap reflectionImage = Bitmap.createBitmap(originalImage, 0, reflectionHeight, width, reflectionHeight, matrix, false);
        Bitmap bitmapWithReflection = Bitmap.createBitmap(width, height + reflectionHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmapWithReflection);
        canvas.drawBitmap(originalImage, 0.0f, 0.0f, (Paint) null);
        new Paint().setColor(17170445);
        canvas.drawBitmap(reflectionImage, 0.0f, (float) (height + 4), (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) originalImage.getHeight(), 0.0f, (float) (bitmapWithReflection.getHeight() + 4), 1895825407, 16777215, Shader.TileMode.MIRROR));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (bitmapWithReflection.getHeight() + 4), paint);
        originalImage.recycle();
        reflectionImage.recycle();
        return bitmapWithReflection;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0129  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap decodeSampledBitmap(java.lang.String r21, int r22, int r23) {
        /*
            org.apache.http.impl.client.DefaultHttpClient r4 = new org.apache.http.impl.client.DefaultHttpClient
            r4.<init>()
            r7 = 0
            org.apache.http.client.methods.HttpGet r8 = new org.apache.http.client.methods.HttpGet     // Catch:{ IllegalArgumentException -> 0x0157, IOException -> 0x00da, IllegalStateException -> 0x0102, Exception -> 0x0126 }
            r0 = r21
            r8.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x0157, IOException -> 0x00da, IllegalStateException -> 0x0102, Exception -> 0x0126 }
            org.apache.http.HttpResponse r15 = r4.execute(r8)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            org.apache.http.StatusLine r18 = r15.getStatusLine()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            int r17 = r18.getStatusCode()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r18 = 200(0xc8, float:2.8E-43)
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x0025
            r16 = 0
            r7 = r8
        L_0x0024:
            return r16
        L_0x0025:
            org.apache.http.HttpEntity r6 = r15.getEntity()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            if (r6 == 0) goto L_0x00d8
            java.io.InputStream r9 = r6.getContent()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r2.<init>()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r18 = 1024(0x400, float:1.435E-42)
            r0 = r18
            byte[] r3 = new byte[r0]     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
        L_0x003a:
            int r12 = r9.read(r3)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r18 = -1
            r0 = r18
            if (r12 <= r0) goto L_0x0076
            r18 = 0
            r0 = r18
            r2.write(r3, r0, r12)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            goto L_0x003a
        L_0x004c:
            r5 = move-exception
            r7 = r8
        L_0x004e:
            if (r7 == 0) goto L_0x0053
            r7.abort()
        L_0x0053:
            java.lang.String r18 = "Downloader"
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "Incorrect passed url "
            java.lang.StringBuilder r19 = r19.append(r20)
            r0 = r19
            r1 = r21
            java.lang.StringBuilder r19 = r0.append(r1)
            java.lang.String r19 = r19.toString()
            r0 = r18
            r1 = r19
            android.util.Log.w(r0, r1, r5)
        L_0x0073:
            r16 = 0
            goto L_0x0024
        L_0x0076:
            r2.flush()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            java.io.ByteArrayInputStream r10 = new java.io.ByteArrayInputStream     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            byte[] r18 = r2.toByteArray()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r0 = r18
            r10.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            java.io.ByteArrayInputStream r11 = new java.io.ByteArrayInputStream     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            byte[] r18 = r2.toByteArray()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r0 = r18
            r11.<init>(r0)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            android.graphics.BitmapFactory$Options r13 = new android.graphics.BitmapFactory$Options     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r13.<init>()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r18 = 1
            r0 = r18
            r13.inJustDecodeBounds = r0     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            com.biznessapps.images.BitmapDownloader$FlushedInputStream r18 = new com.biznessapps.images.BitmapDownloader$FlushedInputStream     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r0 = r18
            r0.<init>(r10)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r19 = 0
            r0 = r18
            r1 = r19
            android.graphics.BitmapFactory.decodeStream(r0, r1, r13)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r10.close()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            android.graphics.BitmapFactory$Options r14 = new android.graphics.BitmapFactory$Options     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r14.<init>()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r0 = r22
            r1 = r23
            int r18 = calculateInSampleSize(r13, r0, r1)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r0 = r18
            r14.inSampleSize = r0     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            com.biznessapps.images.BitmapDownloader$FlushedInputStream r18 = new com.biznessapps.images.BitmapDownloader$FlushedInputStream     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r0 = r18
            r0.<init>(r11)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r19 = 0
            r0 = r18
            r1 = r19
            android.graphics.Bitmap r16 = android.graphics.BitmapFactory.decodeStream(r0, r1, r14)     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r11.close()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r9.close()     // Catch:{ IllegalArgumentException -> 0x004c, IOException -> 0x0154, IllegalStateException -> 0x0151, Exception -> 0x014e }
            r7 = r8
            goto L_0x0024
        L_0x00d8:
            r7 = r8
            goto L_0x0073
        L_0x00da:
            r5 = move-exception
        L_0x00db:
            if (r7 == 0) goto L_0x00e0
            r7.abort()
        L_0x00e0:
            java.lang.String r18 = "Downloader"
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "I/O error while retrieving bitmap from "
            java.lang.StringBuilder r19 = r19.append(r20)
            r0 = r19
            r1 = r21
            java.lang.StringBuilder r19 = r0.append(r1)
            java.lang.String r19 = r19.toString()
            r0 = r18
            r1 = r19
            android.util.Log.w(r0, r1, r5)
            goto L_0x0073
        L_0x0102:
            r5 = move-exception
        L_0x0103:
            if (r7 == 0) goto L_0x0108
            r7.abort()
        L_0x0108:
            java.lang.String r18 = "Downloader"
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "Incorrect URL: "
            java.lang.StringBuilder r19 = r19.append(r20)
            r0 = r19
            r1 = r21
            java.lang.StringBuilder r19 = r0.append(r1)
            java.lang.String r19 = r19.toString()
            android.util.Log.w(r18, r19)
            goto L_0x0073
        L_0x0126:
            r5 = move-exception
        L_0x0127:
            if (r7 == 0) goto L_0x012c
            r7.abort()
        L_0x012c:
            java.lang.String r18 = "Downloader"
            java.lang.StringBuilder r19 = new java.lang.StringBuilder
            r19.<init>()
            java.lang.String r20 = "Error while retrieving bitmap from "
            java.lang.StringBuilder r19 = r19.append(r20)
            r0 = r19
            r1 = r21
            java.lang.StringBuilder r19 = r0.append(r1)
            java.lang.String r19 = r19.toString()
            r0 = r18
            r1 = r19
            android.util.Log.w(r0, r1, r5)
            goto L_0x0073
        L_0x014e:
            r5 = move-exception
            r7 = r8
            goto L_0x0127
        L_0x0151:
            r5 = move-exception
            r7 = r8
            goto L_0x0103
        L_0x0154:
            r5 = move-exception
            r7 = r8
            goto L_0x00db
        L_0x0157:
            r5 = move-exception
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.images.BitmapDownloader.decodeSampledBitmap(java.lang.String, int, int):android.graphics.Bitmap");
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        if (height <= reqHeight && width <= reqWidth) {
            return 1;
        }
        if (width > height) {
            return Math.round(((float) height) / ((float) reqHeight));
        }
        return Math.round(((float) width) / ((float) reqWidth));
    }

    public static int calculateInSampleSize2(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int heightRatio = Math.round(((float) height) / ((float) reqHeight));
            int widthRatio = Math.round(((float) width) / ((float) reqWidth));
            if (heightRatio < widthRatio) {
                inSampleSize = heightRatio;
            } else {
                inSampleSize = widthRatio;
            }
            while (((float) (width * height)) / ((float) (inSampleSize * inSampleSize)) > ((float) (reqWidth * reqHeight * 2))) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private BitmapLoadCallback callback;
        private boolean isLight;
        private int sampleSize;
        private String url;
        private boolean useImageReflection;
        private UsingParams usingParams;
        private final WeakReference<View> viewReference;

        public BitmapDownloaderTask(UsingParams usingParams2) {
            this.usingParams = usingParams2;
            this.viewReference = new WeakReference<>(usingParams2.getView());
            this.useImageReflection = usingParams2.isUseReflection();
            this.isLight = usingParams2.isLight();
            this.callback = usingParams2.getCallback();
            this.url = usingParams2.getUrl();
            this.sampleSize = usingParams2.getSampleSize();
        }

        public String getUrl() {
            return this.url;
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.callback != null) {
                this.callback.onPreImageLoading();
            }
        }

        /* access modifiers changed from: protected */
        public Bitmap doInBackground(String... params) {
            Bitmap bitmap;
            Bitmap resultBitmap;
            BitmapDownloader.this.activeTasks.add(this);
            if (this.usingParams.getReqHeight() <= 0 || this.usingParams.getReqWidth() <= 0) {
                bitmap = BitmapDownloader.this.downloadBitmap(this.url, this.sampleSize);
            } else {
                bitmap = BitmapDownloader.decodeSampledBitmap(this.usingParams.getUrl(), this.usingParams.getReqWidth(), this.usingParams.getReqHeight());
            }
            if (!this.useImageReflection || bitmap == null) {
                resultBitmap = bitmap;
            } else {
                resultBitmap = BitmapDownloader.this.createReflectedImages(bitmap);
            }
            BitmapDownloader.this.activeTasks.remove(this);
            return resultBitmap;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }
            if (bitmap != null) {
                BitmapWrapper bitmapWrapper = new BitmapWrapper(bitmap, this.url);
                BitmapDownloader.getBitmapCacher().addBitmapToCache(this.url, bitmap);
                if (this.viewReference != null) {
                    View view = this.viewReference.get();
                    BitmapDownloaderTask bitmapDownloaderTask = BitmapDownloader.this.getBitmapDownloaderTask(view);
                    if (view != null) {
                        view.setTag(null);
                    }
                    if (this == bitmapDownloaderTask && this.callback != null) {
                        this.callback.onPostImageLoading(bitmapWrapper, view);
                    }
                }
            }
            BitmapDownloader.this.launchNewTaskIfAllowed();
        }
    }

    public static class UsingParams {
        private static final int DEFAULT_BITMAP_SAMPLE_SIZE = 1;
        private BitmapLoadCallback callback;
        private boolean isLight;
        private int reqHeight;
        private int reqWidth;
        private int sampleSize;
        private TintContainer tint;
        private String url;
        private boolean useReflection;
        private View view;

        public UsingParams(String url2) {
            this(null, url2);
        }

        public UsingParams(View view2, String url2) {
            this(view2, url2, null);
        }

        public UsingParams(View view2, String url2, BitmapLoadCallback callback2) {
            this(view2, url2, callback2, false, false);
        }

        public UsingParams(View view2, String url2, BitmapLoadCallback callback2, boolean isLight2) {
            this(view2, url2, callback2, isLight2, false);
        }

        public UsingParams(View view2, String url2, BitmapLoadCallback callback2, boolean isLight2, boolean useReflection2) {
            this.sampleSize = 1;
            this.url = url2;
            this.view = view2;
            this.isLight = isLight2;
            this.useReflection = useReflection2;
            this.callback = callback2;
        }

        public int getReqWidth() {
            return this.reqWidth;
        }

        public void setReqWidth(int reqWidth2) {
            this.reqWidth = reqWidth2;
        }

        public int getReqHeight() {
            return this.reqHeight;
        }

        public void setReqHeight(int reqHeight2) {
            this.reqHeight = reqHeight2;
        }

        public View getView() {
            return this.view;
        }

        public String getUrl() {
            return this.url;
        }

        public boolean isLight() {
            return this.isLight;
        }

        public void setLight(boolean isLight2) {
            this.isLight = isLight2;
        }

        public boolean isUseReflection() {
            return this.useReflection;
        }

        public BitmapLoadCallback getCallback() {
            return this.callback;
        }

        public void setCallback(BitmapLoadCallback callback2) {
            this.callback = callback2;
        }

        public void setSampleSize(int sampleSize2) {
            this.sampleSize = sampleSize2;
        }

        public int getSampleSize() {
            return this.sampleSize;
        }
    }

    public static class TintContainer {
        private String tintColor;
        private float tintOpacity;

        public String getTintColor() {
            return this.tintColor;
        }

        public void setTintColor(String tintColor2) {
            this.tintColor = tintColor2;
        }

        public float getTintOpacity() {
            return this.tintOpacity;
        }

        public void setTintOpacity(float tintOpacity2) {
            this.tintOpacity = tintOpacity2;
        }
    }

    public static abstract class BitmapLoadCallback {
        public void onPreImageLoading() {
        }

        public void onPostImageLoading(BitmapWrapper bitmapWrapper, View view) {
            if (view != null) {
                view.setBackgroundDrawable(new BitmapDrawable(bitmapWrapper.getBitmap()));
            }
        }
    }
}
