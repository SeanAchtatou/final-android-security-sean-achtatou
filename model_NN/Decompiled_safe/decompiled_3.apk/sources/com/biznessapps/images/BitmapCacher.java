package com.biznessapps.images;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BitmapCacher {
    public static final int MAX_CACHER_CAPACITY = 9;
    private static final int MAX_LIGHT_CACHER_CAPACITY = 25;
    private List<String> hardUrls = new ArrayList();
    private List<BitmapWrapper> hardWrappers = new ArrayList();
    private List<String> lightUrls = new ArrayList();
    private List<BitmapWrapper> lightWrappers = new ArrayList();

    public boolean containsInCache(BitmapWrapper bitmapWrapper) {
        return this.hardUrls.contains(bitmapWrapper.getUrl()) || this.lightUrls.contains(bitmapWrapper.getUrl());
    }

    public void addBitmapToCache(BitmapWrapper bitmapWrapper) {
        if (bitmapWrapper.isLight()) {
            addToCache(bitmapWrapper, this.lightWrappers, this.lightUrls, MAX_LIGHT_CACHER_CAPACITY);
        } else {
            addToCache(bitmapWrapper, this.hardWrappers, this.hardUrls, 9);
        }
    }

    public BitmapWrapper getBitmapFromCache(String url) {
        int index;
        BitmapWrapper result = null;
        int index2 = this.hardUrls.indexOf(url);
        if (index2 >= 0) {
            result = this.hardWrappers.get(index2);
        }
        if (result != null || (index = this.lightUrls.indexOf(url)) < 0) {
            return result;
        }
        return this.lightWrappers.get(index);
    }

    public void cleanUnusedBitmaps() {
        for (int countToRemove = this.hardWrappers.size(); countToRemove > 0; countToRemove--) {
            removeFirstSuitable(this.hardWrappers, this.hardUrls);
        }
        for (int countToRemove2 = this.lightWrappers.size(); countToRemove2 > 0; countToRemove2--) {
            removeFirstSuitable(this.lightWrappers, this.lightUrls);
        }
    }

    public void updateCache(BitmapWrapper bitmapWrapper) {
        if (bitmapWrapper.isLight()) {
            if (this.lightWrappers.size() > MAX_LIGHT_CACHER_CAPACITY) {
                removeFromCache(bitmapWrapper, this.lightWrappers, this.lightUrls);
            }
        } else if (this.hardWrappers.size() > 9) {
            removeFromCache(bitmapWrapper, this.hardWrappers, this.hardUrls);
        }
    }

    private void removeFromCache(BitmapWrapper bitmapWrapper, List<BitmapWrapper> items, List<String> urls) {
        if (bitmapWrapper == null) {
            removeFirstSuitable(items, urls);
        } else if (!items.contains(bitmapWrapper)) {
            removeFirstSuitable(items, urls);
        } else if (bitmapWrapper.isLinked() || bitmapWrapper.isNeeded()) {
            removeFirstSuitable(items, urls);
        } else {
            bitmapWrapper.getBitmap().recycle();
            items.remove(bitmapWrapper);
            urls.remove(bitmapWrapper.getUrl());
        }
    }

    private void addToCache(BitmapWrapper bitmapWrapper, List<BitmapWrapper> items, List<String> urls, int limit) {
        if (items.contains(bitmapWrapper)) {
            return;
        }
        if (items.size() > limit) {
            removeFirstSuitable(items, urls);
            items.add(bitmapWrapper);
            urls.add(bitmapWrapper.getUrl());
            return;
        }
        items.add(bitmapWrapper);
        urls.add(bitmapWrapper.getUrl());
    }

    private boolean removeFirstSuitable(List<BitmapWrapper> items, List<String> urls) {
        boolean result = false;
        BitmapWrapper itemToRemove = null;
        Iterator i$ = items.iterator();
        while (true) {
            if (!i$.hasNext()) {
                break;
            }
            BitmapWrapper item = i$.next();
            if (!item.isLinked() && !item.isNeeded()) {
                item.getBitmap().recycle();
                itemToRemove = item;
                result = true;
                break;
            }
        }
        if (itemToRemove != null) {
            items.remove(itemToRemove);
            urls.remove(itemToRemove.getUrl());
        }
        return result;
    }
}
