package com.biznessapps.images;

import android.graphics.Bitmap;

public class BitmapWrapper {
    private Bitmap bitmap;
    private String url;

    public BitmapWrapper(Bitmap bitmap2, String url2) {
        this.bitmap = bitmap2;
        this.url = url2;
    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }

    public void setBitmap(Bitmap bitmap2) {
        this.bitmap = bitmap2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }
}
