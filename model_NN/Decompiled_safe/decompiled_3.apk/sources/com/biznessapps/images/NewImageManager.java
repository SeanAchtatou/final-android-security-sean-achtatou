package com.biznessapps.images;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.utils.StringUtils;

public class NewImageManager {
    private static final float REDUCING_COEFF = 0.8f;
    private static final int STANDART_DEVICE_WIDTH = 480;
    private BitmapDownloader bitmapDownloader = new BitmapDownloader();

    public BitmapDownloader getBitmapDownloader() {
        return this.bitmapDownloader;
    }

    public String addWidthParam(String url, int width) {
        if (width > STANDART_DEVICE_WIDTH) {
            width = (int) (((float) width) * REDUCING_COEFF);
        }
        if (!StringUtils.isNotEmpty(url) || width <= 0) {
            return url;
        }
        return url + AppConstants.WIDTH_URL_PARAM + width;
    }
}
