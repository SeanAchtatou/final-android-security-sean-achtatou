package com.biznessapps.layout;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.widget.Toast;
import com.biznessapps.activities.HomeScreenActivity;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppFragmentManager;
import com.biznessapps.api.AppHandlers;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.api.AsyncCallback;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.api.navigation.NavigationManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.MusicDelegate;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.PlaylistItem;
import com.biznessapps.model.Tab;
import com.biznessapps.player.MusicItem;
import com.biznessapps.player.MusicPlayer;
import com.biznessapps.player.PlayerServiceAccessor;
import com.biznessapps.pushnotifications.RichPushNotification;
import com.biznessapps.storage.StorageKeeper;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.HardwareUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.facebook.AppEventsConstants;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.ExceptionReporter;
import com.google.analytics.tracking.android.GAServiceManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainController extends Activity implements AppConstants {
    /* access modifiers changed from: private */
    public long loadingTimeOffset;
    /* access modifiers changed from: private */
    public boolean needMessagesTab = false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.needMessagesTab = getIntent().getBooleanExtra(CachingConstants.OPEN_MESSAGE_TAB_PROPERTY, false);
        requestWindowFeature(1);
        setContentView(R.layout.main);
        clearOldState();
        initNewState();
        loadApp();
        AppCore.getInstance().initLocationFinder(getApplicationContext());
        AppCore.getInstance().getLocationFinder().startSearching();
    }

    private void clearOldState() {
        AppCore.getInstance().clear();
        NavigationManager.clear();
        ShoppingCart.getInstance().clear();
    }

    private void initNewState() {
        if (!AppCore.getInstance().isInitialized()) {
            AppCore.getInstance().init(getApplicationContext());
        }
        AppHandlers.getUiHandler();
        StorageKeeper.init(getApplicationContext());
        MusicPlayer.getInstance().init(getApplicationContext());
        defineDeviceParams();
    }

    private void loadApp() {
        final boolean isLogged;
        final String defaultApp = getString(R.string.code_name);
        final String appCode = ViewUtils.getExtraKey(getIntent().getExtras(), AppConstants.APPCODE, defaultApp);
        if (getIntent().getExtras() != null) {
            isLogged = getIntent().getExtras().getBoolean(AppConstants.IS_LOGGED_WITH_PROTECTION);
        } else {
            isLogged = false;
        }
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        this.loadingTimeOffset = System.currentTimeMillis();
        if (!appCode.equalsIgnoreCase(AppConstants.BIZNESS_APPS_CODE) && !appCode.equalsIgnoreCase(AppConstants.PREVIEW_APPS_CODE)) {
            if (!StringUtils.isNotEmpty(cacher.getAppCode()) || cacher.getAppCode().equalsIgnoreCase(appCode)) {
            }
            cacher.setAppCode(appCode);
        }
        AsyncCallback<String> callback = new AsyncCallback<String>() {
            public void onResult(String result) {
                JsonParserUtils.updateInitStateWithData(result);
                final List<Tab> tabList = cacher.getTabList();
                Object[] objArr = new Object[1];
                objArr[0] = appCode != null ? appCode : defaultApp;
                StorageKeeper.instance().saveCacheItem(String.format(ServerConstants.INIT_URL_FORMAT, objArr), result);
                new Thread(new Runnable() {
                    public void run() {
                        AppSettings appSettings = AppCore.getInstance().getAppSettings();
                        AppCore.getInstance().init(MainController.this.getApplicationContext());
                        AppCore.getInstance().setBearerAccessToken(DataSource.getInstance().getBearerAccessToken(appSettings.getConsumerKey(), appSettings.getConsumerSecret()));
                        List<Tab> tabsToDisplay = new ArrayList<>();
                        String musicTabId = null;
                        for (Tab tab : tabList) {
                            if (!((tab.getImage() == null || tab.getImage().trim().length() == 0) && (tab.getLabel() == null || tab.getLabel().trim().length() == 0))) {
                                Intent checkingIntent = new Intent();
                                checkingIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, tab.getViewController());
                                if (AppFragmentManager.getFragmentByController(checkingIntent) != null) {
                                    if (ServerConstants.MESSAGE_VIEW_CONTROLLER.equalsIgnoreCase(tab.getViewController())) {
                                        MainController.this.setMessageProperty(tab);
                                    }
                                    if (ServerConstants.MUSIC_PLAYER_VIEW_CONTROLLER.equalsIgnoreCase(tab.getViewController())) {
                                        musicTabId = tab.getTabId();
                                    }
                                    if (ServerConstants.MAILING_LIST_VIEW_CONTROLLER.equalsIgnoreCase(tab.getViewController())) {
                                        appSettings.setMailingListTabId(tab.getId());
                                    }
                                    tabsToDisplay.add(tab);
                                }
                            }
                        }
                        if (!defaultApp.equalsIgnoreCase(AppConstants.BIZNESS_APPS_CODE) && !defaultApp.equalsIgnoreCase(AppConstants.PREVIEW_APPS_CODE)) {
                            MainController.this.sendUserLocation(appCode);
                        }
                        MainController.this.loadMusicData(musicTabId);
                        NavigationManager.setTabsItems(tabsToDisplay);
                        if (!tabsToDisplay.isEmpty()) {
                            if (tabsToDisplay.size() == 1) {
                                Intent firstScreenIntent = new Intent(MainController.this.getApplicationContext(), SingleFragmentActivity.class);
                                firstScreenIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ((Tab) tabsToDisplay.get(0)).getViewController());
                                firstScreenIntent.putExtra(AppConstants.TAB_SPECIAL_ID, ((Tab) tabsToDisplay.get(0)).getTabId());
                                firstScreenIntent.putExtra(AppConstants.URL, ((Tab) tabsToDisplay.get(0)).getUrl());
                                firstScreenIntent.putExtra(AppConstants.TAB_LABEL, ((Tab) tabsToDisplay.get(0)).getLabel());
                                firstScreenIntent.putExtra(CachingConstants.OPEN_MESSAGE_TAB_PROPERTY, MainController.this.needMessagesTab);
                                MainController.this.startActivity(firstScreenIntent);
                            } else {
                                MainController.this.initExceptionHandling();
                                appSettings.setOfflineCachingPrompt(true);
                                long unused = MainController.this.loadingTimeOffset = System.currentTimeMillis() - MainController.this.loadingTimeOffset;
                                CommonUtils.sendTimingEvent(MainController.this.getApplicationContext(), "Application Loading", MainController.this.loadingTimeOffset);
                                if (!appSettings.isProtected() || isLogged) {
                                    MainController.this.registerPushNotifications();
                                    RichPushNotification richNotification = (RichPushNotification) MainController.this.getIntent().getSerializableExtra(CachingConstants.RICH_PUSH_PROPERTY);
                                    if (!MainController.this.needMessagesTab || richNotification == null) {
                                        MainController.this.startActivity(new Intent(MainController.this.getApplicationContext(), HomeScreenActivity.class));
                                    } else {
                                        MainController.this.openLinkedTab(richNotification);
                                    }
                                } else {
                                    Intent firstScreenIntent2 = new Intent(MainController.this.getApplicationContext(), SingleFragmentActivity.class);
                                    firstScreenIntent2.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.PROTECTED_VIEW_CONTROLLER);
                                    MainController.this.startActivity(firstScreenIntent2);
                                }
                            }
                        }
                        MainController.this.finish();
                    }
                }).start();
            }

            public void onError(String message, Throwable throwable) {
                Toast.makeText(MainController.this.getApplicationContext(), R.string.server_connection_failure, 1).show();
                MainController.this.finish();
            }
        };
        Object[] objArr = new Object[1];
        if (appCode == null) {
            appCode = defaultApp;
        }
        objArr[0] = appCode;
        String url = String.format(ServerConstants.INIT_URL_FORMAT, objArr);
        if (AppCore.getInstance().isTablet()) {
            url = url + ServerConstants.TABLET_PARAM;
        }
        DataSource.getInstance().getData(callback, url);
    }

    /* access modifiers changed from: private */
    public void loadMusicData(String musicTabId) {
        getPlayerServiceAccessor().stop();
        getPlayerServiceAccessor().clearQueue();
        if (StringUtils.isNotEmpty(musicTabId)) {
            List<PlaylistItem> items = JsonParserUtils.parseMusicList(DataSource.getInstance().getData(String.format(ServerConstants.MUSIC_PLAYLIST_FORMAT, AppCore.getInstance().getCachingManager().getAppCode(), musicTabId)));
            if (items == null || items.isEmpty()) {
                AppCore.getInstance().getAppSettings().setMusicOnFront(false);
                return;
            }
            PlaylistItem item = items.get(0);
            if (item != null) {
                MusicDelegate.setTintColor(ViewUtils.getColor(item.getTintColor()));
            }
            AppCore.getInstance().getCachingManager().setMusicTabId(musicTabId);
            AppCore.getInstance().getCachingManager().saveData(CachingConstants.MUSIC_PLAYLIST_PROPERTY + musicTabId, items);
            if (AppCore.getInstance().getAppSettings().isMusicOnFront()) {
                getPlayerServiceAccessor().addUrlsQueue(extractUrlsFromData(items));
                AppHandlers.getUiHandler().postDelayed(new Runnable() {
                    public void run() {
                        MainController.this.getPlayerServiceAccessor().play(null);
                    }
                }, 5000);
            }
        }
    }

    private List<MusicItem> extractUrlsFromData(List<PlaylistItem> items) {
        List<MusicItem> previewUrls = new ArrayList<>();
        for (PlaylistItem item : items) {
            MusicItem musicItem = new MusicItem();
            if (StringUtils.isNotEmpty(item.getPreviewUrl())) {
                musicItem.setUrl(item.getPreviewUrl());
                musicItem.setSongInfo(item.getTitle());
                musicItem.setAlbumName(item.getAlbum());
                previewUrls.add(musicItem);
            }
        }
        return previewUrls;
    }

    /* access modifiers changed from: private */
    public PlayerServiceAccessor getPlayerServiceAccessor() {
        if (!MusicPlayer.getInstance().isInited()) {
            MusicPlayer.getInstance().init(getApplicationContext());
        }
        return MusicPlayer.getInstance().getServiceAccessor();
    }

    /* access modifiers changed from: private */
    public void registerPushNotifications() {
        Intent intent = new Intent(AppConstants.C2DM_INTENT_NAME);
        intent.putExtra(AppConstants.APP_PARAM_NAME, PendingIntent.getBroadcast(this, 0, new Intent(), 0));
        intent.putExtra(AppConstants.SENDER_PARAM_NAME, AppConstants.GCM_SENDER_ID);
        startService(intent);
    }

    /* access modifiers changed from: private */
    public void sendUserLocation(String appCode) {
        Location currentLocation = AppCore.getInstance().getLocationFinder().getCurrentLocation();
        if (currentLocation != null) {
            Map<String, String> params = new LinkedHashMap<>();
            params.put(ServerConstants.POST_APP_CODE_PARAM, appCode);
            params.put("latitude", "" + currentLocation.getLatitude());
            params.put("longitude", "" + currentLocation.getLongitude());
            params.put(ServerConstants.POST_DEVICE_PARAM, ServerConstants.ANDROID_DEVICE_VALUE);
            params.put(ServerConstants.POST_DEVTOKEN_PARAM, HardwareUtils.getDeviceToken(((TelephonyManager) getSystemService("phone")).getDeviceId(), Settings.Secure.getString(getContentResolver(), "android_id")));
            AppHttpUtils.executePostRequestSync(ServerConstants.APNS_URL, params);
        }
    }

    private void defineDeviceParams() {
        int deviceWidth;
        Display display = getWindowManager().getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        if (width < height) {
            deviceWidth = width;
        } else {
            deviceWidth = height;
        }
        AppCore.getInstance().setDeviceWidth(deviceWidth);
        AppCore.getInstance().setDeviceHeight(height);
    }

    /* access modifiers changed from: private */
    public void setMessageProperty(Tab tab) {
        SharedPreferences.Editor edit = getSharedPreferences(AppConstants.SETTINGS_ROOT_NAME, 0).edit();
        edit.putString(CachingConstants.MESSAGE_TAB_LABEL_PROPERTY, tab.getLabel());
        edit.putString(AppConstants.TAB_SPECIAL_ID, "" + tab.getTabId());
        edit.commit();
    }

    /* access modifiers changed from: private */
    public void initExceptionHandling() {
        GAServiceManager.getInstance().setDispatchPeriod(20);
        EasyTracker.getInstance().setContext(getApplicationContext());
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionReporter(EasyTracker.getTracker(), GAServiceManager.getInstance(), Thread.getDefaultUncaughtExceptionHandler(), getApplicationContext()));
    }

    /* access modifiers changed from: private */
    public void openLinkedTab(RichPushNotification richNotification) {
        if (StringUtils.isNotEmpty(richNotification.getUrl())) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            String url = richNotification.getUrl();
            if (!url.contains(AppConstants.HTTP_PREFIX) && !url.contains(AppConstants.HTTPS_PREFIX)) {
                url = AppConstants.HTTP_PREFIX + url;
            }
            intent.putExtra(AppConstants.URL, url);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
            startActivity(intent);
        } else if (StringUtils.isNotEmpty(richNotification.getTabId())) {
            Tab tabToUse = null;
            Iterator i$ = AppCore.getInstance().getCachingManager().getTabList().iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Tab item = i$.next();
                if (item.getTabId().equalsIgnoreCase(richNotification.getTabId())) {
                    tabToUse = item;
                    break;
                }
            }
            if (tabToUse != null) {
                Intent intent2 = new Intent(getApplicationContext(), SingleFragmentActivity.class);
                if (StringUtils.isNotEmpty(tabToUse.getUrl())) {
                    intent2.putExtra(AppConstants.URL, tabToUse.getUrl());
                }
                intent2.putExtra(AppConstants.TAB_ID, tabToUse.getId());
                intent2.putExtra(AppConstants.TAB_LABEL, tabToUse.getLabel());
                intent2.putExtra(AppConstants.TAB_SPECIAL_ID, tabToUse.getTabId());
                if (StringUtils.isNotEmpty(richNotification.getDetailId()) && !richNotification.getDetailId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    intent2.putExtra(AppConstants.ITEM_ID, richNotification.getDetailId());
                }
                if (!StringUtils.isNotEmpty(richNotification.getCategoryId()) || richNotification.getCategoryId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    intent2.putExtra(AppConstants.SECTION_ID, tabToUse.getSectionId());
                } else {
                    intent2.putExtra(AppConstants.SECTION_ID, richNotification.getCategoryId());
                }
                intent2.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, tabToUse.getViewController());
                intent2.putExtra(AppConstants.TAB, tabToUse);
                startActivity(intent2);
            }
        } else {
            startActivity(new Intent(getApplicationContext(), HomeScreenActivity.class));
        }
    }
}
