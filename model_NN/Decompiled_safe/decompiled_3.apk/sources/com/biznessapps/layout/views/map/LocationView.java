package com.biznessapps.layout.views.map;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.MapUtils;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.AppSettings;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import java.util.ArrayList;
import java.util.List;

public class LocationView extends MapActivity {
    public static final String HAS_USER_LOCATION = "has_user_location";
    public static final String LATITUDES = "latitudes";
    public static final String LOCATIONS_NAME = "ocations_name";
    public static final String LONGITUDES = "longitudes";
    private SitesOverlay itemizedOverlay;
    private MapView map;

    public void onCreate(Bundle savedInstanceState) {
        LocationView.super.onCreate(savedInstanceState);
        setContentView(R.layout.location_layout);
        initMap();
        initItemizedOverlay();
        try {
            ArrayList<String> longitudes = getIntent().getStringArrayListExtra(LONGITUDES);
            ArrayList<String> latitudes = getIntent().getStringArrayListExtra(LATITUDES);
            ArrayList<String> locNames = getIntent().getStringArrayListExtra(LOCATIONS_NAME);
            boolean hasUserLocation = getIntent().getBooleanExtra(HAS_USER_LOCATION, false);
            List<GeoPoint> points = new ArrayList<>();
            if (longitudes != null && longitudes.size() > 0 && latitudes != null && latitudes.size() > 0 && longitudes.size() == latitudes.size()) {
                for (int i = 0; i < longitudes.size(); i++) {
                    String latitude = latitudes.get(i);
                    String longitude = longitudes.get(i);
                    String locName = locNames.get(i);
                    if (i != 0 || !hasUserLocation) {
                        if (!StringUtils.checkTextFieldsOnEmpty(latitude, longitude)) {
                            GeoPoint p = new GeoPoint((int) (Double.parseDouble(latitude) * 1000000.0d), (int) (Double.parseDouble(longitude) * 1000000.0d));
                            this.itemizedOverlay.createNewOverlay(p, locName, "");
                            points.add(p);
                        }
                    } else {
                        GeoPoint p2 = new GeoPoint((int) (Double.parseDouble(latitude) * 1000000.0d), (int) (Double.parseDouble(longitude) * 1000000.0d));
                        this.itemizedOverlay.createNewOverlay(p2, locName, "");
                        points.add(p2);
                    }
                }
                if (points.size() == 1) {
                    this.map.getController().setZoom(15);
                    this.map.getController().animateTo((GeoPoint) points.get(0));
                } else {
                    MapUtils.SpanData span = MapUtils.defineSpan(points);
                    this.map.getController().zoomToSpan(span.getLat(), span.getLon());
                    this.map.getController().setCenter(span.getCenterPoint());
                }
                this.map.invalidate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = new AnalyticItem();
        AppSettings settings = AppCore.getInstance().getAppSettings();
        data.setContext(getApplicationContext());
        data.setAccountId(settings.getGaAccountId());
        data.setAppId(settings.getAppId());
        data.setTabId(getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        return data;
    }

    private void initItemizedOverlay() {
        this.itemizedOverlay = new SitesOverlay(getApplicationContext(), getMarker());
        this.map.getOverlays().add(this.itemizedOverlay);
        this.map.invalidate();
    }

    private void initMap() {
        this.map = findViewById(R.id.mapview);
        this.map.setBuiltInZoomControls(true);
        this.map.setSatellite(false);
        this.map.setStreetView(true);
        this.map.setClickable(true);
    }

    /* access modifiers changed from: protected */
    public boolean isRouteDisplayed() {
        return false;
    }

    private Drawable getMarker() {
        Drawable marker = getResources().getDrawable(R.drawable.bubble);
        marker.setBounds(0, 0, marker.getIntrinsicWidth(), marker.getIntrinsicHeight());
        return marker;
    }
}
