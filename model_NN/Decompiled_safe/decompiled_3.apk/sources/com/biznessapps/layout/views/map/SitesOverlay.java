package com.biznessapps.layout.views.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import com.biznessapps.layout.R;
import com.biznessapps.model.AroundUsItem;
import com.biznessapps.utils.ViewUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.ItemizedOverlay;
import com.google.android.maps.OverlayItem;
import java.util.ArrayList;
import java.util.List;

public class SitesOverlay extends ItemizedOverlay<OverlayItem> {
    private Context context;
    private List<WrappedOverlayItem> overlayList;
    private TapHandler tapHandler;

    public interface TapHandler {
        void overlayItemTapped(WrappedOverlayItem wrappedOverlayItem);
    }

    public SitesOverlay(Context context2, Drawable marker) {
        super(boundCenterBottom(marker));
        this.overlayList = new ArrayList();
        this.context = context2;
        populate();
    }

    public SitesOverlay(Context context2, Drawable marker, TapHandler tapHandler2) {
        this(context2, marker);
        this.tapHandler = tapHandler2;
    }

    /* access modifiers changed from: protected */
    public OverlayItem createItem(int i) {
        return this.overlayList.get(i);
    }

    public int size() {
        return this.overlayList.size();
    }

    public void createNewOverlay(GeoPoint p, String title, String snippet, AroundUsItem.PoiItem item) {
        addOverlay(getNewOverlayItem(p, title, snippet, item, ViewUtils.getColor(item.getColor())), true);
    }

    public void createNewOverlay(GeoPoint p, String title, String snippet) {
        addOverlay(getNewOverlayItem(p, title, snippet, null, 0), false);
    }

    /* access modifiers changed from: protected */
    public boolean onTap(int index) {
        if (this.tapHandler == null || this.overlayList == null || this.overlayList.get(index) == null) {
            return SitesOverlay.super.onTap(index);
        }
        this.tapHandler.overlayItemTapped(this.overlayList.get(index));
        return true;
    }

    private void addOverlay(WrappedOverlayItem overlay, boolean useCustomization) {
        if (useCustomization) {
            customizeMarker(overlay);
        }
        this.overlayList.add(overlay);
        populate();
    }

    private WrappedOverlayItem getNewOverlayItem(GeoPoint p, String title, String snippet, AroundUsItem.PoiItem item, int color) {
        return new WrappedOverlayItem(p, title, snippet, item, color);
    }

    private void customizeMarker(WrappedOverlayItem overlay) {
        int newColor = overlay.getColor();
        Bitmap bitmap = BitmapFactory.decodeResource(this.context.getResources(), R.drawable.bubble1);
        float flagWidth = this.context.getResources().getDimension(R.dimen.bubble_size);
        Bitmap newBmp = Bitmap.createScaledBitmap(bitmap, (int) flagWidth, (int) ((float) ((int) ((((float) bitmap.getHeight()) * flagWidth) / ((float) bitmap.getWidth())))), false);
        int oldColor = newBmp.getPixel(newBmp.getWidth() / 2, newBmp.getHeight() / 2);
        int r1 = Color.red(oldColor);
        int g1 = Color.green(oldColor);
        int b1 = Color.blue(oldColor);
        int r2 = Color.red(newColor);
        float dr = ((float) r2) / ((float) r1);
        float dg = ((float) Color.green(newColor)) / ((float) g1);
        float db = ((float) Color.blue(newColor)) / ((float) b1);
        for (int i = 0; i < newBmp.getWidth(); i++) {
            for (int j = 0; j < newBmp.getHeight(); j++) {
                int pixel = newBmp.getPixel(i, j);
                int r = (int) (((float) Color.red(pixel)) * dr);
                int g = (int) (((float) Color.green(pixel)) * dg);
                int b = (int) (((float) Color.blue(pixel)) * db);
                if (r > 255) {
                    r = MotionEventCompat.ACTION_MASK;
                }
                if (g > 255) {
                    g = MotionEventCompat.ACTION_MASK;
                }
                if (b > 255) {
                    b = MotionEventCompat.ACTION_MASK;
                }
                newBmp.setPixel(i, j, Color.argb(Color.alpha(pixel), r, g, b));
            }
        }
        overlay.setMarker(boundCenterBottom(new BitmapDrawable(newBmp)));
    }

    public class WrappedOverlayItem extends OverlayItem {
        private int color;
        private AroundUsItem.PoiItem poiInfo;

        public WrappedOverlayItem(GeoPoint point, String title, String snippet) {
            super(point, title, snippet);
        }

        public WrappedOverlayItem(GeoPoint point, String title, String snippet, AroundUsItem.PoiItem poiInfo2, int color2) {
            super(point, title, snippet);
            this.poiInfo = poiInfo2;
            this.color = color2;
        }

        public AroundUsItem.PoiItem getPoiInfo() {
            return this.poiInfo;
        }

        public int getColor() {
            return this.color;
        }
    }
}
