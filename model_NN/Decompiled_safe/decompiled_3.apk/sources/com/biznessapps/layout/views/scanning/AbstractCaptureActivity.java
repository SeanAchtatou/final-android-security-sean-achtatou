package com.biznessapps.layout.views.scanning;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Handler;
import com.google.zxing.Result;

public abstract class AbstractCaptureActivity extends Activity {
    /* access modifiers changed from: package-private */
    public abstract void drawViewfinder();

    /* access modifiers changed from: package-private */
    public abstract Handler getHandler();

    /* access modifiers changed from: package-private */
    public abstract ViewfinderView getViewfinderView();

    /* access modifiers changed from: package-private */
    public abstract void handleDecode(Result result, Bitmap bitmap);
}
