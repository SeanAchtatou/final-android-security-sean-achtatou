package com.biznessapps.layout.views.map;

import com.google.android.maps.GeoPoint;
import java.util.List;

public class MapUtils {
    public static final int SINGLE_ZOOM_LEVEL = 15;

    public static SpanData defineSpan(List<GeoPoint> items) {
        SpanData result = null;
        if (items != null && !items.isEmpty()) {
            result = new SpanData();
            int minLat = items.get(0).getLatitudeE6();
            int maxLat = items.get(0).getLatitudeE6();
            int minLon = items.get(0).getLongitudeE6();
            int maxLon = items.get(0).getLongitudeE6();
            for (GeoPoint item : items) {
                int lat = item.getLatitudeE6();
                int lon = item.getLongitudeE6();
                maxLat = Math.max(lat, maxLat);
                minLat = Math.min(lat, minLat);
                maxLon = Math.max(lon, maxLon);
                minLon = Math.min(lon, minLon);
            }
            result.setCenterPoint(new GeoPoint((maxLat + minLat) / 2, (maxLon + minLon) / 2));
            result.setLat(Math.abs(maxLat - minLat));
            result.setLon((int) (((double) Math.abs(maxLon - minLon)) * 1.23d));
        }
        return result;
    }

    public static class SpanData {
        private GeoPoint centerPoint;
        private int lat;
        private int lon;

        public int getLat() {
            return this.lat;
        }

        public void setLat(int lat2) {
            this.lat = lat2;
        }

        public int getLon() {
            return this.lon;
        }

        public void setLon(int lon2) {
            this.lon = lon2;
        }

        public GeoPoint getCenterPoint() {
            return this.centerPoint;
        }

        public void setCenterPoint(GeoPoint centerPoint2) {
            this.centerPoint = centerPoint2;
        }
    }
}
