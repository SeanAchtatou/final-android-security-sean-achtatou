package com.biznessapps.layout.views.map;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import com.biznessapps.utils.ViewUtils;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class MarkerOverlay extends Overlay {
    private static final String DEFAULT_LABEL_TITLE = "";
    private static final int DEFAULT_MARKER_ICON_WIDTH = 40;
    private static final int DEFAULT_RADIUS_SIZE = 5000;
    private static final String INCORRECT_ARGUMENTS_MESSAGE = "Please check input params while creating Marker overlay";
    private Bitmap arrowBitmap;
    private float buttonSize;
    private float dashHeight;
    private float dashOffsetX;
    private float dashOffsetY;
    private float dashWidth;
    private float dateFontSize;
    private float flagSize;
    private GeoPoint geoPoint;
    private boolean isDashVisible;
    private String labelTitle;
    private Bitmap markerBitmap;
    private Bitmap markerBorderBitmap;
    private float offsetButtonRight;
    private float offsetDateX;
    private float offsetDateY;
    private float offsetTitleX;
    private float offsetTitleY;
    private Paint paintDate;
    private Paint paintFill;
    private Paint paintLabel;
    private Paint paintSt;
    private String pointTitle;
    private int radiusDistance;
    private float roundRadius;
    private boolean shouldShowRadius;
    private float titleFontSize;

    public MarkerOverlay(GeoPoint geoPoint2, Bitmap markerBitmap2) {
        this(geoPoint2, markerBitmap2, null);
    }

    public MarkerOverlay(GeoPoint geoPoint2, Bitmap markerBitmap2, Bitmap markerBorderBitmap2) {
        this.dashWidth = 310.0f;
        this.dashHeight = 100.0f;
        this.dashOffsetX = -80.0f;
        this.dashOffsetY = -60.0f;
        this.buttonSize = 50.0f;
        this.titleFontSize = 26.0f;
        this.dateFontSize = 20.0f;
        this.roundRadius = 5.0f;
        this.offsetTitleX = 20.0f;
        this.offsetTitleY = 45.0f;
        this.offsetDateX = 20.0f;
        this.offsetDateY = 20.0f;
        this.offsetButtonRight = 10.0f;
        this.flagSize = 50.0f;
        this.isDashVisible = false;
        this.radiusDistance = 5000;
        this.labelTitle = DEFAULT_LABEL_TITLE;
        if (geoPoint2 == null || markerBitmap2 == null) {
            throw new IllegalArgumentException(INCORRECT_ARGUMENTS_MESSAGE);
        }
        this.geoPoint = geoPoint2;
        this.markerBitmap = markerBitmap2;
        this.markerBitmap = Bitmap.createScaledBitmap(this.markerBitmap, DEFAULT_MARKER_ICON_WIDTH, DEFAULT_MARKER_ICON_WIDTH, false);
        if (markerBorderBitmap2 != null) {
            this.markerBorderBitmap = Bitmap.createScaledBitmap(markerBorderBitmap2, 50, 55, false);
        }
        this.paintFill = new Paint();
        this.paintFill.setFlags(1);
        this.paintFill.setStyle(Paint.Style.FILL);
        this.paintFill.setColor(ViewUtils.getColor("103f79fd"));
        this.paintFill.setAlpha(50);
        this.paintLabel = new Paint();
        this.paintLabel.setFlags(1);
        this.paintLabel.setStyle(Paint.Style.FILL);
        this.paintLabel.setColor(ViewUtils.getColor("50121847"));
        this.paintLabel.setAlpha(50);
        this.paintSt = new Paint();
        this.paintSt.setFlags(1);
        this.paintSt.setStyle(Paint.Style.STROKE);
        this.paintSt.setColor(-1);
        this.paintSt.setTextSize(this.titleFontSize);
        this.paintDate = new Paint();
        this.paintDate.setFlags(1);
        this.paintDate.setStyle(Paint.Style.STROKE);
        this.paintDate.setColor(-1);
        this.paintDate.setTextSize(this.dateFontSize);
    }

    public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) {
        MarkerOverlay.super.draw(canvas, mapView, shadow);
        Point center = new Point();
        mapView.getProjection().toPixels(this.geoPoint, center);
        GeoPoint p2 = new GeoPoint(this.geoPoint.getLatitudeE6() + this.radiusDistance, this.geoPoint.getLongitudeE6() + this.radiusDistance);
        Point radPts = new Point();
        mapView.getProjection().toPixels(p2, radPts);
        int radius = Math.abs(radPts.x - center.x);
        if (this.shouldShowRadius) {
            Paint paint = this.paintFill;
            canvas.drawCircle((float) center.x, (float) center.y, (float) radius, paint);
            Paint paint2 = this.paintSt;
            canvas.drawCircle((float) center.x, (float) center.y, (float) radius, paint2);
        }
        Paint paint3 = this.paintSt;
        canvas.drawBitmap(this.markerBitmap, (float) center.x, (float) (center.y - 50), paint3);
        if (this.markerBorderBitmap != null) {
            Paint paint4 = this.paintSt;
            canvas.drawBitmap(this.markerBorderBitmap, (float) (center.x - 5), (float) ((center.y - 50) - 5), paint4);
        }
        if (!this.isDashVisible) {
            return true;
        }
        float topLeftXDash = ((float) center.x) + this.dashOffsetX;
        float topLeftYDash = (((float) center.y) + this.dashOffsetY) - this.dashHeight;
        float bottomRightXDash = ((float) center.x) + this.dashOffsetX + this.dashWidth;
        float bottomRightYDash = ((float) center.y) + this.dashOffsetY;
        RectF rect = new RectF(topLeftXDash, topLeftYDash, bottomRightXDash, bottomRightYDash);
        canvas.drawRoundRect(rect, this.roundRadius, this.roundRadius, this.paintLabel);
        canvas.drawRoundRect(rect, this.roundRadius, this.roundRadius, this.paintSt);
        Paint paint5 = this.paintSt;
        canvas.drawText(this.pointTitle, this.offsetTitleX + topLeftXDash, this.offsetTitleY + topLeftYDash, paint5);
        Paint paint6 = this.paintDate;
        canvas.drawText(this.labelTitle, this.offsetDateX + topLeftXDash, bottomRightYDash - this.offsetDateY, paint6);
        if (this.arrowBitmap == null) {
            return true;
        }
        float leftTopXButton = (bottomRightXDash - this.buttonSize) - this.offsetButtonRight;
        float leftTopYButton = ((this.dashHeight / 2.0f) + topLeftYDash) - (this.buttonSize / 2.0f);
        canvas.drawBitmap(this.arrowBitmap, leftTopXButton, leftTopYButton, this.paintSt);
        return true;
    }

    public boolean onTap(GeoPoint point, MapView mapView) {
        Point center = new Point();
        mapView.getProjection().toPixels(this.geoPoint, center);
        Point tapPoint = new Point();
        mapView.getProjection().toPixels(point, tapPoint);
        if (this.isDashVisible) {
            float topLeftYDash = (((float) center.y) + this.dashOffsetY) - this.dashHeight;
            float leftTopXButton = (((((float) center.x) + this.dashOffsetX) + this.dashWidth) - this.buttonSize) - this.offsetButtonRight;
            float leftTopYButton = ((this.dashHeight / 2.0f) + topLeftYDash) - (this.buttonSize / 2.0f);
            this.isDashVisible = false;
            if (((float) tapPoint.x) >= leftTopXButton && ((float) tapPoint.y) >= leftTopYButton && ((float) tapPoint.x) <= this.buttonSize + leftTopXButton && ((float) tapPoint.y) <= this.buttonSize + leftTopYButton) {
                return true;
            }
        }
        if (tapPoint.x < center.x || ((float) tapPoint.y) < ((float) center.y) - this.flagSize || ((float) tapPoint.x) > ((float) center.x) + this.flagSize || tapPoint.y > center.y) {
            return false;
        }
        this.isDashVisible = true;
        return true;
    }

    public void setRadiusDistance(int radiusDistance2) {
        this.radiusDistance = radiusDistance2;
    }

    public void setShouldShowRadius(boolean shouldShowRadius2) {
        this.shouldShowRadius = shouldShowRadius2;
    }

    public void setPointTitle(String pointTitle2) {
        this.pointTitle = pointTitle2;
    }

    public void setLabelTitle(String labelTitle2) {
        this.labelTitle = labelTitle2;
    }

    public void setArrowBitmap(Bitmap arrowBitmap2) {
        this.arrowBitmap = arrowBitmap2;
        this.arrowBitmap = Bitmap.createScaledBitmap(this.arrowBitmap, DEFAULT_MARKER_ICON_WIDTH, DEFAULT_MARKER_ICON_WIDTH, false);
    }
}
