package com.biznessapps.layout.views.scanning;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.biznessapps.layout.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import java.io.IOException;
import java.util.Vector;

public class CaptureActivity extends AbstractCaptureActivity implements SurfaceHolder.Callback {
    private static final long INTENT_RESULT_DURATION = 1500;
    public static final int RESULT_CAMERA_INIT_ERROR = 100;
    public static final String SCAN_RESULT = "SCAN_RESULT";
    private static final String TAG = CaptureActivity.class.getSimpleName();
    private String characterSet;
    private Vector<BarcodeFormat> decodeFormats;
    private CaptureActivityHandler handler;
    private boolean hasSurface;
    private InactivityTimer inactivityTimer;
    private Result lastResult;
    private Source source;
    private ViewfinderView viewfinderView;

    private enum Source {
        NATIVE_APP_INTENT,
        PRODUCT_SEARCH_LINK,
        ZXING_LINK,
        NONE
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(128);
        setContentView(R.layout.capture_layout);
        CameraManager.init(getApplication());
        this.viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        this.handler = null;
        this.lastResult = null;
        this.hasSurface = false;
        this.inactivityTimer = new InactivityTimer(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        resetStatusView();
        SurfaceHolder surfaceHolder = ((SurfaceView) findViewById(R.id.preview_view)).getHolder();
        if (this.hasSurface) {
            initCamera(surfaceHolder);
        } else {
            surfaceHolder.addCallback(this);
            surfaceHolder.setType(3);
        }
        this.source = Source.NONE;
        this.decodeFormats = null;
        this.characterSet = null;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.handler != null) {
            this.handler.quitSynchronously();
            this.handler = null;
        }
        CameraManager.get().closeDriver();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.inactivityTimer.shutdown();
        super.onDestroy();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.source == Source.NATIVE_APP_INTENT) {
                setResult(0);
                finish();
                return true;
            } else if ((this.source == Source.NONE || this.source == Source.ZXING_LINK) && this.lastResult != null) {
                resetStatusView();
                if (this.handler == null) {
                    return true;
                }
                this.handler.sendEmptyMessage(R.id.restart_preview);
                return true;
            }
        } else if (keyCode == 80 || keyCode == 27) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        try {
            CameraManager.get().openDriver(surfaceHolder);
            if (this.handler == null) {
                this.handler = new CaptureActivityHandler(this, this.decodeFormats, this.characterSet);
            }
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
            handleCameraInitializationError();
        } catch (RuntimeException e) {
            Log.w(TAG, "Unexpected error initializating camera", e);
            handleCameraInitializationError();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!this.hasSurface) {
            this.hasSurface = true;
            initCamera(holder);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.hasSurface = false;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    /* access modifiers changed from: package-private */
    public void handleDecode(Result rawResult, Bitmap barcode) {
        this.inactivityTimer.onActivity();
        this.lastResult = rawResult;
        if (barcode != null) {
            drawResultPoints(barcode, rawResult);
            handleDecodeExternally(rawResult, barcode);
        }
    }

    private void handleDecodeExternally(Result rawResult, Bitmap barcode) {
        this.viewfinderView.drawResultBitmap(barcode);
        Intent intent = new Intent(getIntent().getAction());
        intent.addFlags(524288);
        intent.putExtra("SCAN_RESULT", rawResult.toString());
        Message message = Message.obtain(this.handler, R.id.return_scan_result);
        message.obj = intent;
        this.handler.sendMessageDelayed(message, INTENT_RESULT_DURATION);
    }

    private void handleCameraInitializationError() {
        setResult(100);
        finish();
    }

    private void drawResultPoints(Bitmap barcode, Result rawResult) {
        ResultPoint[] points = rawResult.getResultPoints();
        if (points != null && points.length > 0) {
            Canvas canvas = new Canvas(barcode);
            Paint paint = new Paint();
            paint.setColor(getResources().getColor(R.color.result_image_border));
            paint.setStrokeWidth(3.0f);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(new Rect(2, 2, barcode.getWidth() - 2, barcode.getHeight() - 2), paint);
            paint.setColor(getResources().getColor(R.color.result_points));
            if (points.length == 2) {
                paint.setStrokeWidth(4.0f);
                drawLine(canvas, paint, points[0], points[1]);
            } else if ((points.length != 4 || !rawResult.getBarcodeFormat().equals(BarcodeFormat.UPC_A)) && !rawResult.getBarcodeFormat().equals(BarcodeFormat.EAN_13)) {
                paint.setStrokeWidth(10.0f);
                for (ResultPoint point : points) {
                    canvas.drawPoint(point.getX(), point.getY(), paint);
                }
            } else {
                drawLine(canvas, paint, points[0], points[1]);
                drawLine(canvas, paint, points[2], points[3]);
            }
        }
    }

    private static void drawLine(Canvas canvas, Paint paint, ResultPoint a, ResultPoint b) {
        canvas.drawLine(a.getX(), a.getY(), b.getX(), b.getY(), paint);
    }

    /* access modifiers changed from: package-private */
    public void drawViewfinder() {
        this.viewfinderView.drawViewfinder();
    }

    /* access modifiers changed from: package-private */
    public Handler getHandler() {
        return this.handler;
    }

    /* access modifiers changed from: package-private */
    public ViewfinderView getViewfinderView() {
        return this.viewfinderView;
    }

    private void resetStatusView() {
        this.viewfinderView.setVisibility(0);
        this.lastResult = null;
    }
}
