package com.biznessapps.layout;

import android.app.Activity;
import android.app.Application;
import com.biznessapps.api.CommonTask;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.acra.annotation.ReportsCrashes;

@ReportsCrashes(formKey = "dFloS296dFc4Zm5PMnktcEJ2R2FpUlE6MQ")
public class BAcore extends Application {
    private final Map<String, List<CommonTask<?, ?, ?>>> activityTaskMap = new HashMap();

    public void onCreate() {
        super.onCreate();
    }

    public void removeTask(CommonTask<?, ?, ?> task) {
        for (Map.Entry<String, List<CommonTask<?, ?, ?>>> entry : this.activityTaskMap.entrySet()) {
            List<CommonTask<?, ?, ?>> tasks = entry.getValue();
            int i = 0;
            while (true) {
                if (i >= tasks.size()) {
                    break;
                } else if (tasks.get(i) == task) {
                    tasks.remove(i);
                    break;
                } else {
                    i++;
                }
            }
            if (tasks.size() == 0) {
                this.activityTaskMap.remove(entry.getKey());
                return;
            }
        }
    }

    public void clearActivityTask(Activity activity) {
        List<CommonTask<?, ?, ?>> taskList = this.activityTaskMap.get(activity.toString());
        if (taskList != null) {
            for (CommonTask<?, ?, ?> task : taskList) {
                task.cancel(true);
            }
        }
    }

    public void addTask(Activity activity, CommonTask<?, ?, ?> task) {
        String key = activity.toString();
        List<CommonTask<?, ?, ?>> tasks = this.activityTaskMap.get(key);
        if (tasks == null) {
            tasks = new ArrayList<>();
            this.activityTaskMap.put(key, tasks);
        }
        tasks.add(task);
    }

    public void detach(Activity activity) {
        List<CommonTask<?, ?, ?>> tasks = this.activityTaskMap.get(activity.toString());
        if (tasks != null) {
            for (CommonTask<?, ?, ?> task : tasks) {
                task.setActivity(null);
            }
        }
    }

    public void attach(Activity activity) {
        List<CommonTask<?, ?, ?>> tasks = this.activityTaskMap.get(activity.toString());
        if (tasks != null) {
            for (CommonTask<?, ?, ?> task : tasks) {
                task.setActivity(activity);
            }
        }
    }
}
