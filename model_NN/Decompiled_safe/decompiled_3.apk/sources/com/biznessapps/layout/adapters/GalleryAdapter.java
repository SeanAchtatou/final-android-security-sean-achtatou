package com.biznessapps.layout.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.images.BitmapCacher;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GalleryAdapter extends AbstractAdapter<GalleryData.Item> {
    private BitmapCacher bitmapCacher;
    private BitmapDownloader bitmapDownloader;
    private List<BitmapWrapper> bitmapsList;
    private Map<BitmapWrapper, List<ImageView>> map;
    private boolean useImageReflection;

    public GalleryAdapter(Context context, List<GalleryData.Item> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
        this.bitmapDownloader = AppCore.getInstance().getNewImageManager().getBitmapDownloader();
        this.bitmapCacher = AppCore.getInstance().getNewImageManager().getBitmapCacher();
        this.map = new HashMap();
        this.bitmapsList = new ArrayList();
    }

    public GalleryAdapter(Context context, List<GalleryData.Item> items, int layoutItemResourceId, boolean useImageReflection2) {
        this(context, items, layoutItemResourceId);
        this.useImageReflection = useImageReflection2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.GalleryItem holder;
        String url;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.GalleryItem();
            holder.setImage((ImageView) convertView.findViewById(R.id.image_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.GalleryItem) convertView.getTag();
        }
        GalleryData.Item item = (GalleryData.Item) this.items.get(position);
        if (item != null) {
            if (StringUtils.isEmpty(item.getFullUrl())) {
                url = String.format(ServerConstants.GALLERY_THUMBNAILS, item.getId());
                if (this.useImageReflection) {
                    url = url + ServerConstants.WIDTH_PARAM;
                }
            } else {
                url = item.getFullUrl() + AppConstants.WIDTH_URL_PARAM + 100;
            }
            final ImageView imageView = holder.getImage();
            this.bitmapDownloader.download(new BitmapDownloader.UsingParams(holder.getImage(), url, new BitmapDownloader.BitmapLoadCallback() {
                public void onPostImageLoading(BitmapWrapper bitmapWrapper, View view) {
                    ((ImageView) view).setImageBitmap(bitmapWrapper.getBitmap());
                    bitmapWrapper.setLinked(true);
                    GalleryAdapter.this.updateMap(bitmapWrapper, imageView);
                }
            }, false, this.useImageReflection));
        }
        return convertView;
    }

    public void clear() {
        for (BitmapWrapper item : this.bitmapsList) {
            resetImagesBg(this.map.get(item), item);
            item.cleanAllLinks();
        }
        this.map.clear();
        this.bitmapsList.clear();
        this.bitmapCacher.cleanUnusedBitmaps();
    }

    /* access modifiers changed from: protected */
    public void updateMap(BitmapWrapper bitmapWrapper, ImageView imageView) {
        if (this.bitmapsList.contains(bitmapWrapper)) {
            this.map.put(bitmapWrapper, updateImagesList(this.map.get(bitmapWrapper), imageView));
            return;
        }
        this.map.put(bitmapWrapper, updateImagesList(this.map.get(bitmapWrapper), imageView));
        this.bitmapsList.add(bitmapWrapper);
        if (this.bitmapsList.size() >= 9) {
            BitmapWrapper wrapper = this.bitmapsList.get(0);
            resetImagesBg(this.map.get(wrapper), wrapper);
            this.bitmapCacher.updateCache(wrapper);
            this.bitmapsList.remove(0);
            this.map.remove(wrapper);
        }
    }

    private void resetImagesBg(List<ImageView> imagesList, BitmapWrapper wrapper) {
        if (imagesList != null) {
            for (ImageView item : imagesList) {
                item.setImageBitmap(null);
                wrapper.setLinked(false);
            }
        }
    }

    private List<ImageView> updateImagesList(List<ImageView> imagesList, ImageView imageToAdd) {
        if (imagesList == null) {
            List<ImageView> imagesList2 = new ArrayList<>();
            imagesList2.add(imageToAdd);
            return imagesList2;
        }
        imagesList.add(imageToAdd);
        return imagesList;
    }
}
