package com.biznessapps.layout.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.YoutubeRssItem;
import java.util.List;

public class YoutubeRssAdapter extends AbstractAdapter<YoutubeRssItem> {
    public YoutubeRssAdapter(Context context, List<YoutubeRssItem> items) {
        super(context, items, R.layout.youtube_rss_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.YoutubeRssItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.YoutubeRssItem();
            holder.setCountHintTextView((TextView) convertView.findViewById(R.id.count_hint_text));
            holder.setRatingAverageTextView((TextView) convertView.findViewById(R.id.rating_average_text));
            holder.setTitleView((TextView) convertView.findViewById(R.id.youtube_item_title));
            holder.setViewCountTextView((TextView) convertView.findViewById(R.id.view_count_text));
            holder.setYoutubeImageView((ImageView) convertView.findViewById(R.id.youtube_item_image));
            holder.setInfoContainer((ViewGroup) convertView.findViewById(R.id.youtube_info_container));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.YoutubeRssItem) convertView.getTag();
        }
        YoutubeRssItem item = (YoutubeRssItem) this.items.get(position);
        if (item != null) {
            holder.getCountHintTextView().setText(item.getFeedlinkCountHint());
            holder.getRatingAverageTextView().setText(item.getRatingAverage());
            holder.getTitleView().setText(item.getTitle());
            holder.getViewCountTextView().setText(item.getStatisticsViewCount());
            this.imageDownloader.download(item.getMediaThumbnailUrl(), holder.getYoutubeImageView());
            if (item.hasColor()) {
                holder.getInfoContainer().setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getCountHintTextView(), holder.getRatingAverageTextView(), holder.getViewCountTextView(), holder.getTitleView());
            }
        }
        return convertView;
    }
}
