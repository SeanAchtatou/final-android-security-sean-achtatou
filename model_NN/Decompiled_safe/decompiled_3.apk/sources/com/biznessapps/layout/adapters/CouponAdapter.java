package com.biznessapps.layout.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.CouponItem;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class CouponAdapter extends AbstractAdapter<CouponItem> {
    public CouponAdapter(Context context, List<CouponItem> items) {
        super(context, items, R.layout.coupon_row);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CouponItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CouponItem();
            holder.setTextViewText((TextView) convertView.findViewById(R.id.coupon_text));
            holder.setTextViewCheckin((TextView) convertView.findViewById(R.id.checkin_text));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CouponItem) convertView.getTag();
        }
        CouponItem item = (CouponItem) this.items.get(position);
        if (item != null) {
            holder.getTextViewText().setText(Html.fromHtml(item.getTitle()));
            if (StringUtils.isNotEmpty(item.getId())) {
                if (item.getLastRedeemedTime() > 0 && !item.isReusable()) {
                    holder.getTextViewCheckin().setText(getContext().getResources().getString(R.string.redeemed));
                } else if (item.getCheckinTarget() == 0) {
                    holder.getTextViewCheckin().setText(getContext().getResources().getString(R.string.unlocked));
                } else {
                    holder.getTextViewCheckin().setText(Html.fromHtml((item.getCheckinTargetMax() - item.getCheckinTarget()) + "/" + item.getCheckinTargetMax()));
                }
            }
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewText(), holder.getTextViewCheckin());
            }
        }
        return convertView;
    }
}
