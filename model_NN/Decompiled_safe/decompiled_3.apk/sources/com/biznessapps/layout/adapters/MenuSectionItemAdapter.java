package com.biznessapps.layout.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.MenuSectionItem;
import java.util.List;

public class MenuSectionItemAdapter extends AbstractAdapter<MenuSectionItem> {
    public MenuSectionItemAdapter(Context context, List<MenuSectionItem> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.MenuItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.MenuItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.menu_text));
            holder.setTextViewPrice((TextView) convertView.findViewById(R.id.menu_price_text));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.MenuItem) convertView.getTag();
        }
        MenuSectionItem item = (MenuSectionItem) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getTitle()));
            String price = item.getPrice();
            if (price == null || price.equals("0.00")) {
                holder.getTextViewPrice().setText(" ");
            } else {
                holder.getTextViewPrice().setText(Html.fromHtml(price, null, null));
            }
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle(), holder.getTextViewPrice());
            }
        }
        return convertView;
    }
}
