package com.biznessapps.layout.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class FallWallAdapter extends AbstractAdapter<FanWallComment> {
    private boolean isParentAdapter;

    public FallWallAdapter(Context context, List<FanWallComment> items, int layoutItemResourceId, boolean isParentAdapter2) {
        super(context, items, layoutItemResourceId);
        this.isParentAdapter = isParentAdapter2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.FanWallItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.FanWallItem();
            holder.setCommentTextView((TextView) convertView.findViewById(R.id.fan_wall_comment));
            holder.setNameTextView((TextView) convertView.findViewById(R.id.fan_wall_name));
            holder.setTimeAgoTextView((TextView) convertView.findViewById(R.id.fan_wall_time_ago));
            holder.setReplyTextView((TextView) convertView.findViewById(R.id.fan_wall_comment_replies));
            holder.setFanWallImageView((ImageView) convertView.findViewById(R.id.fan_wall_comment_item_image));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.FanWallItem) convertView.getTag();
        }
        FanWallComment item = (FanWallComment) this.items.get(position);
        if (item != null) {
            holder.getCommentTextView().setText(Html.fromHtml(item.getComment()));
            holder.getNameTextView().setText(item.getTitle());
            holder.getTimeAgoTextView().setText(Html.fromHtml(item.getTimeAgo()));
            if (StringUtils.isNotEmpty(item.getImage())) {
                this.imageDownloader.download(item.getImage(), holder.getFanWallImageView());
            }
            if (this.isParentAdapter) {
                holder.getReplyTextView().setText(item.getReplies());
            } else {
                holder.getReplyTextView().setVisibility(8);
            }
        }
        return convertView;
    }
}
