package com.biznessapps.layout.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.fragments.music.MusicListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import java.util.List;

public class AlbumAdapter extends AbstractAdapter<String> {
    /* access modifiers changed from: private */
    public MusicListFragment parentFragment;

    public AlbumAdapter(Context context, List<String> items) {
        super(context, items, R.layout.playlist_album_bar_item);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.AlbumItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.AlbumItem();
            TextView tempButton = (TextView) convertView.findViewById(R.id.album_item_button);
            tempButton.setTag(Integer.valueOf(position));
            tempButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    int position = ((Integer) v.getTag()).intValue();
                    if (position != AlbumAdapter.this.parentFragment.getCurrentAlbum()) {
                        AlbumAdapter.this.parentFragment.setCurrentAlbum(position);
                        AlbumAdapter.this.notifyDataSetChanged();
                    }
                }
            });
            holder.setTitleView(tempButton);
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.AlbumItem) convertView.getTag();
        }
        String item = (String) this.items.get(position);
        if (item != null) {
            holder.getTitleView().setText(item);
            if (position == this.parentFragment.getCurrentAlbum()) {
                holder.getTitleView().setTextColor(-16777216);
            } else {
                holder.getTitleView().setTextColor(-1);
            }
        }
        return convertView;
    }

    public MusicListFragment getParentFragment() {
        return this.parentFragment;
    }

    public void setParentFragment(MusicListFragment parentFragment2) {
        this.parentFragment = parentFragment2;
    }
}
