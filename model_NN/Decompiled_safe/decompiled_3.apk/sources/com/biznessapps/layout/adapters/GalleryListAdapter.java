package com.biznessapps.layout.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.flickr.GalleryAlbum;
import com.biznessapps.utils.google.caching.ImageFetcher;
import java.util.List;

public class GalleryListAdapter extends AbstractAdapter<GalleryAlbum> {
    public GalleryListAdapter(Context context, List<GalleryAlbum> items) {
        super(context, items, R.layout.gallery_album_row);
    }

    public GalleryListAdapter(Context context, List<GalleryAlbum> items, ImageFetcher imageFetcher) {
        super(context, items, R.layout.gallery_album_row, imageFetcher);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.biznessapps.utils.google.caching.ImageFetcher.loadImage(java.lang.Object, android.widget.ImageView):void}
     arg types: [java.lang.String, android.widget.ImageView]
     candidates:
      com.biznessapps.utils.google.caching.ImageWorker.loadImage(java.lang.String, android.view.View):void
      SimpleMethodDetails{com.biznessapps.utils.google.caching.ImageFetcher.loadImage(java.lang.Object, android.widget.ImageView):void} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.GalleryAdapterItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.GalleryAdapterItem();
            holder.setTextViewName((TextView) convertView.findViewById(R.id.album_item_name));
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.album_item_title));
            holder.setTextViewComments((TextView) convertView.findViewById(R.id.album_item_comment));
            holder.setImageView((ImageView) convertView.findViewById(R.id.album_item_image));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.GalleryAdapterItem) convertView.getTag();
        }
        GalleryAlbum item = (GalleryAlbum) this.items.get(position);
        if (item != null) {
            if (item.getUrls() == null || item.getUrls().isEmpty()) {
                holder.getTextViewComments().setText(getContext().getString(R.string.total_images) + 0);
            } else {
                holder.getTextViewComments().setText(getContext().getString(R.string.total_images) + item.getUrls().size());
                this.imageFetcher.loadImage((Object) item.getUrls().get(0), holder.getImageView());
            }
            holder.getTextViewName().setText(item.getDescription());
            holder.getTextViewTitle().setText(item.getTitle());
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle(), holder.getTextViewName(), holder.getTextViewComments());
            }
        }
        return convertView;
    }
}
