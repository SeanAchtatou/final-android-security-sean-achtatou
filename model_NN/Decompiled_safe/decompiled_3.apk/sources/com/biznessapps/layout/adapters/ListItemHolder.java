package com.biznessapps.layout.adapters;

import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class ListItemHolder {

    public static class MenuItem {
        private TextView textViewPrice;
        private TextView textViewTitle;

        public TextView getTextViewTitle() {
            return this.textViewTitle;
        }

        public void setTextViewTitle(TextView textViewTitle2) {
            this.textViewTitle = textViewTitle2;
        }

        public TextView getTextViewPrice() {
            return this.textViewPrice;
        }

        public void setTextViewPrice(TextView textViewPrice2) {
            this.textViewPrice = textViewPrice2;
        }
    }

    public static class RssItem {
        private ImageView imageView;
        private ImageView rightArrowView;
        private TextView textViewDate;
        private TextView textViewSummary;
        private TextView textViewTitle;

        public ImageView getRightArrowView() {
            return this.rightArrowView;
        }

        public void setRightArrowView(ImageView rightArrowView2) {
            this.rightArrowView = rightArrowView2;
        }

        public TextView getTextViewDate() {
            return this.textViewDate;
        }

        public void setTextViewDate(TextView textViewDate2) {
            this.textViewDate = textViewDate2;
        }

        public ImageView getImageView() {
            return this.imageView;
        }

        public void setImageView(ImageView imageView2) {
            this.imageView = imageView2;
        }

        public TextView getTextViewTitle() {
            return this.textViewTitle;
        }

        public void setTextViewTitle(TextView textViewTitle2) {
            this.textViewTitle = textViewTitle2;
        }

        public TextView getTextViewSummary() {
            return this.textViewSummary;
        }

        public void setTextViewSummary(TextView textViewSummary2) {
            this.textViewSummary = textViewSummary2;
        }
    }

    public static class MessageItem {
        private TextView textViewDate;
        private TextView textViewText;

        public TextView getTextViewText() {
            return this.textViewText;
        }

        public void setTextViewText(TextView textViewText2) {
            this.textViewText = textViewText2;
        }

        public TextView getTextViewDate() {
            return this.textViewDate;
        }

        public void setTextViewDate(TextView textViewDate2) {
            this.textViewDate = textViewDate2;
        }
    }

    public static class EventItem {
        private TextView dateTextView;
        private ImageView rightArrowView;
        private TextView textViewCalendar;
        private TextView textViewText;

        public ImageView getRightArrowView() {
            return this.rightArrowView;
        }

        public void setRightArrowView(ImageView rightArrowView2) {
            this.rightArrowView = rightArrowView2;
        }

        public TextView getTextViewText() {
            return this.textViewText;
        }

        public void setTextViewText(TextView textViewText2) {
            this.textViewText = textViewText2;
        }

        public TextView getTextViewCalendar() {
            return this.textViewCalendar;
        }

        public void setTextViewCalendar(TextView textViewCalendar2) {
            this.textViewCalendar = textViewCalendar2;
        }

        public TextView getDateTextView() {
            return this.dateTextView;
        }

        public void setDateTextView(TextView dateTextView2) {
            this.dateTextView = dateTextView2;
        }
    }

    public static class CouponItem {
        private TextView textViewCheckin;
        private TextView textViewText;

        public TextView getTextViewText() {
            return this.textViewText;
        }

        public void setTextViewText(TextView textViewText2) {
            this.textViewText = textViewText2;
        }

        public TextView getTextViewCheckin() {
            return this.textViewCheckin;
        }

        public void setTextViewCheckin(TextView textViewCheckin2) {
            this.textViewCheckin = textViewCheckin2;
        }
    }

    public static class TabItem {
        private ViewGroup tabRoot;

        public ViewGroup getTabRoot() {
            return this.tabRoot;
        }

        public void setTabRoot(ViewGroup tabRoot2) {
            this.tabRoot = tabRoot2;
        }
    }

    public static class GalleryItem {
        private ImageView image;

        public ImageView getImage() {
            return this.image;
        }

        public void setImage(ImageView image2) {
            this.image = image2;
        }
    }

    public static class LocationItem {
        private ImageView rightArrowView;
        private TextView textViewAddress;
        private TextView textViewCity;

        public ImageView getRightArrowView() {
            return this.rightArrowView;
        }

        public void setRightArrowView(ImageView rightArrowView2) {
            this.rightArrowView = rightArrowView2;
        }

        public TextView getTextViewCity() {
            return this.textViewCity;
        }

        public void setTextViewCity(TextView textViewCity2) {
            this.textViewCity = textViewCity2;
        }

        public TextView getTextViewAddress() {
            return this.textViewAddress;
        }

        public void setTextViewAddress(TextView textViewAddress2) {
            this.textViewAddress = textViewAddress2;
        }
    }

    public static class FanWallItem {
        private TextView commentTextView;
        private ImageView fanWallImageView;
        private TextView nameTextView;
        private TextView replyTextView;
        private TextView timeAgoTextView;
        private ImageView uploadImageView;

        public TextView getReplyTextView() {
            return this.replyTextView;
        }

        public void setReplyTextView(TextView replyTextView2) {
            this.replyTextView = replyTextView2;
        }

        public TextView getNameTextView() {
            return this.nameTextView;
        }

        public void setNameTextView(TextView nameTextView2) {
            this.nameTextView = nameTextView2;
        }

        public TextView getCommentTextView() {
            return this.commentTextView;
        }

        public void setCommentTextView(TextView commentTextView2) {
            this.commentTextView = commentTextView2;
        }

        public TextView getTimeAgoTextView() {
            return this.timeAgoTextView;
        }

        public void setTimeAgoTextView(TextView timeAgoTextView2) {
            this.timeAgoTextView = timeAgoTextView2;
        }

        public ImageView getFanWallImageView() {
            return this.fanWallImageView;
        }

        public void setFanWallImageView(ImageView fanWallImageView2) {
            this.fanWallImageView = fanWallImageView2;
        }

        public ImageView getUploadImageView() {
            return this.uploadImageView;
        }

        public void setUploadImageView(ImageView uploadImageView2) {
            this.uploadImageView = uploadImageView2;
        }
    }

    public static class YoutubeRssItem {
        private TextView countHintTextView;
        private ViewGroup infoContainer;
        private TextView ratingAverageTextView;
        private TextView titleView;
        private TextView viewCountTextView;
        private ImageView youtubeImageView;

        public ViewGroup getInfoContainer() {
            return this.infoContainer;
        }

        public void setInfoContainer(ViewGroup infoContainer2) {
            this.infoContainer = infoContainer2;
        }

        public ImageView getYoutubeImageView() {
            return this.youtubeImageView;
        }

        public void setYoutubeImageView(ImageView youtubeImageView2) {
            this.youtubeImageView = youtubeImageView2;
        }

        public TextView getTitleView() {
            return this.titleView;
        }

        public void setTitleView(TextView titleView2) {
            this.titleView = titleView2;
        }

        public TextView getViewCountTextView() {
            return this.viewCountTextView;
        }

        public void setViewCountTextView(TextView viewCountTextView2) {
            this.viewCountTextView = viewCountTextView2;
        }

        public TextView getCountHintTextView() {
            return this.countHintTextView;
        }

        public void setCountHintTextView(TextView countHintTextView2) {
            this.countHintTextView = countHintTextView2;
        }

        public TextView getRatingAverageTextView() {
            return this.ratingAverageTextView;
        }

        public void setRatingAverageTextView(TextView ratingAverageTextView2) {
            this.ratingAverageTextView = ratingAverageTextView2;
        }
    }

    public static class PlaylistItem {
        private TextView albumView;
        private TextView buyTextView;
        private ViewGroup playItemBuyView;
        private ImageView playItemView;
        private TextView titleView;

        public TextView getAlbumView() {
            return this.albumView;
        }

        public void setAlbumView(TextView albumView2) {
            this.albumView = albumView2;
        }

        public TextView getBuyTextView() {
            return this.buyTextView;
        }

        public void setBuyTextView(TextView buyTextView2) {
            this.buyTextView = buyTextView2;
        }

        public ImageView getPlayItemView() {
            return this.playItemView;
        }

        public void setPlayItemView(ImageView playItemView2) {
            this.playItemView = playItemView2;
        }

        public TextView getTitleView() {
            return this.titleView;
        }

        public void setTitleView(TextView titleView2) {
            this.titleView = titleView2;
        }

        public ViewGroup getPlayItemBuyView() {
            return this.playItemBuyView;
        }

        public void setPlayItemBuyView(ViewGroup playItemBuyView2) {
            this.playItemBuyView = playItemBuyView2;
        }
    }

    public static class AlbumItem {
        private ImageView arrowImageView;
        private TextView titleView;

        public TextView getTitleView() {
            return this.titleView;
        }

        public void setTitleView(TextView titleView2) {
            this.titleView = titleView2;
        }

        public ImageView getArrowImageView() {
            return this.arrowImageView;
        }

        public void setArrowImageView(ImageView arrowImageView2) {
            this.arrowImageView = arrowImageView2;
        }
    }

    public static class CommonItem {
        private TextView bottomTextView;
        private Button button;
        private CheckBox checkbox;
        private ViewGroup frameContainer;
        private ImageView imageView;
        private ImageView rightArrowView;
        private TextView textViewTitle;

        public ImageView getRightArrowView() {
            return this.rightArrowView;
        }

        public void setRightArrowView(ImageView rightArrowView2) {
            this.rightArrowView = rightArrowView2;
        }

        public Button getButton() {
            return this.button;
        }

        public void setButton(Button button2) {
            this.button = button2;
        }

        public ViewGroup getFrameContainer() {
            return this.frameContainer;
        }

        public void setFrameContainer(ViewGroup frameContainer2) {
            this.frameContainer = frameContainer2;
        }

        public TextView getTextViewTitle() {
            return this.textViewTitle;
        }

        public void setTextViewTitle(TextView textViewTitle2) {
            this.textViewTitle = textViewTitle2;
        }

        public ImageView getImageView() {
            return this.imageView;
        }

        public void setImageView(ImageView imageView2) {
            this.imageView = imageView2;
        }

        public CheckBox getCheckbox() {
            return this.checkbox;
        }

        public void setCheckbox(CheckBox checkbox2) {
            this.checkbox = checkbox2;
        }

        public TextView getBottomTextView() {
            return this.bottomTextView;
        }

        public void setBottomTextView(TextView bottomTextView2) {
            this.bottomTextView = bottomTextView2;
        }
    }

    public static class ReservationServiceHolder {
        private Button bookItButton;
        private TextView serviceNameView;
        private TextView servicePriceView;
        private TextView serviceTimeView;
        private ImageView thumbnailView;

        public TextView getServiceNameView() {
            return this.serviceNameView;
        }

        public void setServiceNameView(TextView serviceNameView2) {
            this.serviceNameView = serviceNameView2;
        }

        public TextView getServicePriceView() {
            return this.servicePriceView;
        }

        public void setServicePriceView(TextView servicePriceView2) {
            this.servicePriceView = servicePriceView2;
        }

        public TextView getServiceTimeView() {
            return this.serviceTimeView;
        }

        public void setServiceTimeView(TextView serviceTimeView2) {
            this.serviceTimeView = serviceTimeView2;
        }

        public Button getBookItButton() {
            return this.bookItButton;
        }

        public void setBookItButton(Button bookItButton2) {
            this.bookItButton = bookItButton2;
        }

        public ImageView getThumbnailView() {
            return this.thumbnailView;
        }

        public void setThumbnailView(ImageView thumbnailView2) {
            this.thumbnailView = thumbnailView2;
        }
    }

    public static class GalleryAdapterItem {
        private ImageView imageView;
        private TextView textViewComments;
        private TextView textViewName;
        private TextView textViewTitle;

        public TextView getTextViewTitle() {
            return this.textViewTitle;
        }

        public void setTextViewTitle(TextView textViewTitle2) {
            this.textViewTitle = textViewTitle2;
        }

        public TextView getTextViewName() {
            return this.textViewName;
        }

        public void setTextViewName(TextView textViewName2) {
            this.textViewName = textViewName2;
        }

        public TextView getTextViewComments() {
            return this.textViewComments;
        }

        public void setTextViewComments(TextView textViewComments2) {
            this.textViewComments = textViewComments2;
        }

        public ImageView getImageView() {
            return this.imageView;
        }

        public void setImageView(ImageView imageView2) {
            this.imageView = imageView2;
        }
    }

    public static class ShoppingCartCategoryItem {
        private TextView catNameView;

        public void setCatNameView(TextView catNameView2) {
            this.catNameView = catNameView2;
        }

        public TextView getCatNameView() {
            return this.catNameView;
        }
    }

    public static class ShoppingCartProductItem {
        private TextView productNameView;
        private TextView productPriceLabelView;
        private TextView productPriceView;
        private ImageView smallImageView;

        public TextView getProductPriceLabelView() {
            return this.productPriceLabelView;
        }

        public void setProductPriceLabelView(TextView productPriceLabelView2) {
            this.productPriceLabelView = productPriceLabelView2;
        }

        public void setProductNameView(TextView productNameView2) {
            this.productNameView = productNameView2;
        }

        public TextView getProductNameView() {
            return this.productNameView;
        }

        public void setProductPriceView(TextView productPriceView2) {
            this.productPriceView = productPriceView2;
        }

        public TextView getProductPriceView() {
            return this.productPriceView;
        }

        public void setProductSmallImageView(ImageView smallimage) {
            this.smallImageView = smallimage;
        }

        public ImageView getProductSmallImageView() {
            return this.smallImageView;
        }
    }

    public static class ShoppingCartCheckoutItem {
        private ImageView productImageView;
        private TextView productNameView;
        private TextView productPriceView;
        private EditText quantityView;
        private Button removeButton;
        private Button updateButton;

        public TextView getProductNameView() {
            return this.productNameView;
        }

        public void setProductNameView(TextView productNameView2) {
            this.productNameView = productNameView2;
        }

        public TextView getProductPriceView() {
            return this.productPriceView;
        }

        public void setProductPriceView(TextView productPriceView2) {
            this.productPriceView = productPriceView2;
        }

        public ImageView getProductImageView() {
            return this.productImageView;
        }

        public void setSmallImageView(ImageView smallImageView) {
            this.productImageView = smallImageView;
        }

        public EditText getQuantityView() {
            return this.quantityView;
        }

        public void setQuantityView(EditText quantityView2) {
            this.quantityView = quantityView2;
        }

        public Button getUpdateButton() {
            return this.updateButton;
        }

        public void setUpdateButton(Button updateButton2) {
            this.updateButton = updateButton2;
        }

        public Button getRemoveButton() {
            return this.removeButton;
        }

        public void setRemoveButton(Button removeButton2) {
            this.removeButton = removeButton2;
        }
    }

    public static class NotepadItem {
        private TextView noteDateView;
        private TextView noteTitleView;

        public TextView getNoteTitleView() {
            return this.noteTitleView;
        }

        public void setNoteTitleView(TextView noteTitleView2) {
            this.noteTitleView = noteTitleView2;
        }

        public TextView getNoteDateView() {
            return this.noteDateView;
        }

        public void setNoteDateView(TextView noteDateView2) {
            this.noteDateView = noteDateView2;
        }
    }

    public static class UpcomingReservationItem {
        private ImageView reservArrowView;
        private TextView reservDayView;
        private TextView reservMonthView;
        private TextView serviceNameView;

        public TextView getReservMonthView() {
            return this.reservMonthView;
        }

        public void setReservMonthView(TextView reservMonthView2) {
            this.reservMonthView = reservMonthView2;
        }

        public TextView getReservDayView() {
            return this.reservDayView;
        }

        public void setReservDayView(TextView reservDayView2) {
            this.reservDayView = reservDayView2;
        }

        public TextView getServiceNameView() {
            return this.serviceNameView;
        }

        public void setServiceNameView(TextView serviceNameView2) {
            this.serviceNameView = serviceNameView2;
        }

        public ImageView getReservArrowView() {
            return this.reservArrowView;
        }

        public void setReservArrowView(ImageView reservArrowView2) {
            this.reservArrowView = reservArrowView2;
        }
    }

    public static class ReservationHistoryItem {
        private TextView serviceNameView;
        private TextView startDateView;
        private TextView startTimeView;

        public TextView getStartDateView() {
            return this.startDateView;
        }

        public void setStartDateView(TextView startDateView2) {
            this.startDateView = startDateView2;
        }

        public TextView getStartTimeView() {
            return this.startTimeView;
        }

        public void setStartTimeView(TextView startTimeView2) {
            this.startTimeView = startTimeView2;
        }

        public TextView getServiceNameView() {
            return this.serviceNameView;
        }

        public void setServiceNameView(TextView serviceNameView2) {
            this.serviceNameView = serviceNameView2;
        }
    }

    public static class ReservationTimeItem {
        private TextView timeFromView;
        private TextView timeToView;

        public TextView getTimeFromView() {
            return this.timeFromView;
        }

        public void setTimeFromView(TextView timeFromView2) {
            this.timeFromView = timeFromView2;
        }

        public TextView getTimeToView() {
            return this.timeToView;
        }

        public void setTimeToView(TextView timeToView2) {
            this.timeToView = timeToView2;
        }
    }
}
