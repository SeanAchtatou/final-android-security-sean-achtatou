package com.biznessapps.layout.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.RssItem;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class RssAdapter extends AbstractAdapter<RssItem> {
    private String rssUrl;

    public RssAdapter(Context context, List<RssItem> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.RssItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.RssItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.rss_title_label));
            holder.setTextViewSummary((TextView) convertView.findViewById(R.id.rss_summary_label));
            holder.setTextViewDate((TextView) convertView.findViewById(R.id.rss_date_label));
            holder.setImageView((ImageView) convertView.findViewById(R.id.rss_item_image));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.RssItem) convertView.getTag();
        }
        RssItem item = (RssItem) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getTitle()));
            holder.getTextViewSummary().setText(Html.fromHtml(item.getSummary()));
            holder.getTextViewDate().setText(Html.fromHtml(item.getSubtitle()));
            defineSectionHeader(item, (TextView) convertView.findViewById(R.id.section_title_view));
            String url = item.getImageUrl();
            if (StringUtils.isEmpty(url)) {
                if (StringUtils.isNotEmpty(item.getIcon())) {
                    url = item.getIcon() + AppConstants.ADD_ICON_WIDTH;
                    this.rssUrl = url;
                } else if (StringUtils.isNotEmpty(this.rssUrl)) {
                    url = this.rssUrl;
                } else {
                    url = AppCore.getInstance().getAppSettings().getRssIconUrl();
                }
            }
            this.imageDownloader.download(url, holder.getImageView());
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle(), holder.getTextViewSummary());
            }
        }
        return convertView;
    }

    private void defineSectionHeader(CommonListEntity item, TextView sectionTitleView) {
        int i = 8;
        String section = item.getSection();
        if (StringUtils.isNotEmpty(section)) {
            sectionTitleView.setTextColor(this.settings.getSectionTextColor());
            sectionTitleView.setBackgroundColor(this.settings.getSectionBarColor());
            if (item.isShowSection()) {
                i = 0;
            }
            sectionTitleView.setVisibility(i);
            sectionTitleView.setText(section);
            return;
        }
        sectionTitleView.setVisibility(8);
    }
}
