package com.biznessapps.layout.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.images.BitmapCacher;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.images.NewImageManager;
import com.biznessapps.utils.ImageDownloader;
import com.biznessapps.utils.ImageManager;
import com.biznessapps.utils.google.caching.ImageFetcher;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractAdapter<T> extends ArrayAdapter<T> implements AppConstants {
    protected BitmapCacher bitmapCacher;
    protected BitmapDownloader bitmapDownloader;
    private List<BitmapWrapper> bitmapsList;
    protected int currentItemIndex;
    protected final ImageDownloader imageDownloader;
    protected ImageFetcher imageFetcher;
    protected LayoutInflater inflater;
    protected List<T> items;
    protected int layoutItemResourceId;
    private Map<ImageView, BitmapWrapper> map;
    private List<PositionListener> positionListeners;
    protected AppCore.UiSettings settings;
    protected boolean wasOvercolored;

    public interface PositionListener {
        void onLastPositionAchieved(int i);
    }

    public AbstractAdapter(Activity activity, List list, int layoutItemResourceId2) {
        this(activity.getApplicationContext(), list, layoutItemResourceId2);
    }

    public AbstractAdapter(Context context, List list, int layoutItemResourceId2) {
        super(context, 0, list);
        this.inflater = LayoutInflater.from(getContext());
        this.imageDownloader = new ImageDownloader();
        this.currentItemIndex = -1;
        this.positionListeners = new ArrayList();
        this.bitmapDownloader = AppCore.getInstance().getNewImageManager().getBitmapDownloader();
        this.bitmapCacher = AppCore.getInstance().getNewImageManager().getBitmapCacher();
        this.map = new HashMap();
        this.bitmapsList = new ArrayList();
        this.settings = AppCore.getInstance().getUiSettings();
        this.items = list;
        this.layoutItemResourceId = layoutItemResourceId2;
    }

    public AbstractAdapter(Context context, List<T> items2, int layoutItemResourceId2, ImageFetcher imageFetcher2) {
        super(context, 0, items2);
        this.inflater = LayoutInflater.from(getContext());
        this.imageDownloader = new ImageDownloader();
        this.currentItemIndex = -1;
        this.positionListeners = new ArrayList();
        this.bitmapDownloader = AppCore.getInstance().getNewImageManager().getBitmapDownloader();
        this.bitmapCacher = AppCore.getInstance().getNewImageManager().getBitmapCacher();
        this.map = new HashMap();
        this.bitmapsList = new ArrayList();
        this.settings = AppCore.getInstance().getUiSettings();
        this.items = items2;
        this.layoutItemResourceId = layoutItemResourceId2;
        this.imageFetcher = imageFetcher2;
    }

    public void setCurrentItemIndex(int currentItemIndex2) {
        this.currentItemIndex = currentItemIndex2;
    }

    public int getCount() {
        return this.items.size();
    }

    public T getItem(int position) {
        return this.items.get(position);
    }

    /* access modifiers changed from: protected */
    public void checkPositioning(int position, View convertView, ViewGroup parent) {
        if (position == this.items.size() - 1) {
            for (PositionListener item : this.positionListeners) {
                if (item != null) {
                    item.onLastPositionAchieved(position);
                }
            }
        }
    }

    public void addPositionListener(PositionListener listenerToAdd) {
        this.positionListeners.add(listenerToAdd);
    }

    public void removePositionListener(PositionListener listenerToRemove) {
        this.positionListeners.remove(listenerToRemove);
    }

    public void clearBitmaps() {
        for (BitmapWrapper item : this.bitmapsList) {
            item.cleanAllLinks();
        }
        this.map.clear();
        this.bitmapsList.clear();
        this.bitmapCacher.cleanUnusedBitmaps();
    }

    /* access modifiers changed from: protected */
    public StateListDrawable getListItemDrawable(int unselectedColor) {
        StateListDrawable listItemDrawable = new StateListDrawable();
        ColorDrawable unselectedState = new ColorDrawable(unselectedColor);
        ColorDrawable selectedState = new ColorDrawable(Color.parseColor("#999999"));
        listItemDrawable.addState(new int[]{-16842919, -16842908}, unselectedState);
        listItemDrawable.addState(new int[]{16842919}, selectedState);
        listItemDrawable.addState(new int[]{16842908}, selectedState);
        return listItemDrawable;
    }

    /* access modifiers changed from: protected */
    public void setTextColorToView(int textColor, TextView... views) {
        if (views != null && views.length > 0) {
            for (TextView textColor2 : views) {
                textColor2.setTextColor(textColor);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void updateMap(BitmapWrapper bitmapWrapper, ImageView imageView) {
        if (!this.bitmapsList.contains(this.bitmapsList)) {
            this.bitmapsList.add(bitmapWrapper);
        }
        BitmapWrapper previousImageWrapper = this.map.remove(imageView);
        if (previousImageWrapper != null && !bitmapWrapper.equals(previousImageWrapper)) {
            previousImageWrapper.cleanAllLinks();
        }
        this.map.put(imageView, bitmapWrapper);
    }

    /* access modifiers changed from: protected */
    public ImageManager getImageManager() {
        return AppCore.getInstance().getImageManager();
    }

    /* access modifiers changed from: protected */
    public NewImageManager getNewImageManager() {
        return AppCore.getInstance().getNewImageManager();
    }
}
