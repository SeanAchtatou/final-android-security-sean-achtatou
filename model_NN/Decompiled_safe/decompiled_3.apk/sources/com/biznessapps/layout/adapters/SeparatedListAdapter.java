package com.biznessapps.layout.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class SeparatedListAdapter<T extends CommonListEntity> extends AbstractAdapter<T> {
    public static final int TYPE_SECTION_HEADER = 0;
    public final ArrayAdapter<String> headers;
    private int sectionBarColorId;
    private int sectionBarTextColorId;
    public final Map<String, Adapter> sections = new LinkedHashMap();

    public SeparatedListAdapter(Context context, int resId, int sectionBarColorId2, int sectionBarTextColorId2) {
        super(context, new ArrayList(), resId);
        this.headers = new ArrayAdapter<>(context, resId);
        this.sectionBarColorId = sectionBarColorId2;
        this.sectionBarTextColorId = sectionBarTextColorId2;
    }

    public void addSection(String section, Adapter adapter) {
        this.headers.add(section);
        this.sections.put(section, adapter);
    }

    public void clear() {
        this.headers.clear();
        this.sections.clear();
        notifyDataSetInvalidated();
    }

    public T getItem(int position) {
        for (String section : this.sections.keySet()) {
            Adapter adapter = this.sections.get(section);
            int size = adapter.getCount() + 1;
            if (position == 0) {
                return (CommonListEntity) section;
            }
            if (position < size) {
                return (CommonListEntity) adapter.getItem(position - 1);
            }
            position -= size;
        }
        return null;
    }

    public int getCount() {
        int total = 0;
        for (Adapter adapter : this.sections.values()) {
            total += adapter.getCount() + 1;
        }
        return total;
    }

    public int getViewTypeCount() {
        int total = 1;
        for (Adapter adapter : this.sections.values()) {
            total += adapter.getViewTypeCount();
        }
        return total;
    }

    public int getItemViewType(int position) {
        int type = 1;
        for (String section : this.sections.keySet()) {
            Adapter adapter = this.sections.get(section);
            int size = adapter.getCount() + 1;
            if (position == 0) {
                return 0;
            }
            if (position < size) {
                return adapter.getItemViewType(position - 1) + type;
            }
            position -= size;
            type += adapter.getViewTypeCount();
        }
        return -1;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int position) {
        return getItemViewType(position) != 0;
    }

    public boolean isEmpty() {
        return getCount() == 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        int sectionnum = 0;
        for (String section : this.sections.keySet()) {
            Adapter adapter = this.sections.get(section);
            int size = adapter.getCount() + 1;
            if (position == 0) {
                String sectionName = this.headers.getItem(sectionnum);
                View view = this.headers.getView(sectionnum, convertView, parent);
                if (!(view instanceof TextView)) {
                    return view;
                }
                ((TextView) view).setTextColor(this.sectionBarTextColorId);
                ((TextView) view).setBackgroundColor(this.sectionBarColorId);
                if (!StringUtils.isEmpty(sectionName)) {
                    return view;
                }
                ((TextView) view).setMaxHeight(2);
                return view;
            } else if (position < size) {
                return adapter.getView(position - 1, convertView, parent);
            } else {
                position -= size;
                sectionnum++;
            }
        }
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public boolean hasStableIds() {
        return false;
    }
}
