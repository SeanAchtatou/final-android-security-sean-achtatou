package com.biznessapps.layout.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.fragments.lists.MessageListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.MessageItem;
import com.biznessapps.utils.CommonUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class MessageAdapter extends AbstractAdapter<MessageItem> {
    /* access modifiers changed from: private */
    public MessageListFragment.MessageItemListener itemListener;

    public void setItemListener(MessageListFragment.MessageItemListener itemListener2) {
        this.itemListener = itemListener2;
    }

    public MessageAdapter(Context context, List<MessageItem> items, int layoutItemResourceId) {
        super(context, items, layoutItemResourceId);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.MessageItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.MessageItem();
            holder.setTextViewText((TextView) convertView.findViewById(R.id.message_text));
            holder.setTextViewDate((TextView) convertView.findViewById(R.id.message_date_text));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.MessageItem) convertView.getTag();
        }
        final MessageItem item = (MessageItem) this.items.get(position);
        ImageView messageIconType = (ImageView) convertView.findViewById(R.id.message_icon_type);
        ImageView shareIcon = (ImageView) convertView.findViewById(R.id.message_share_icon);
        TextView messageTextType = (TextView) convertView.findViewById(R.id.message_text_type);
        if (item != null) {
            boolean isOrdinaryMessage = item.getType() == 0;
            int rightText = R.string.message_text_type;
            int rightIconId = R.drawable.message_text_icon;
            if (item.getType() == 1) {
                rightText = R.string.message_web_type;
                rightIconId = R.drawable.message_web_icon;
            } else if (item.getType() == 2) {
                rightText = R.string.message_tab_content_type;
                rightIconId = R.drawable.message_tab_content_icon;
            } else if (item.getType() == 3) {
                rightText = R.string.message_template_type;
                rightIconId = R.drawable.message_template_icon;
            }
            messageTextType.setText(rightText);
            messageIconType.setImageResource(rightIconId);
            messageIconType.setVisibility(isOrdinaryMessage ? 8 : 0);
            messageTextType.setVisibility(isOrdinaryMessage ? 8 : 0);
            shareIcon.setVisibility(isOrdinaryMessage ? 8 : 0);
            final int i = position;
            shareIcon.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (MessageAdapter.this.itemListener != null) {
                        MessageAdapter.this.itemListener.onItemSelected(item, i);
                    }
                }
            });
            holder.getTextViewText().setText(item.getTitle());
            if (item.getDate() != null) {
                SimpleDateFormat sdfInput = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    holder.getTextViewDate().setText(sdfInput.format(sdfInput.parse(item.getDate())));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (item.hasColor()) {
            }
            if (!this.wasOvercolored) {
                AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
                CommonUtils.overrideImageColor(settings.getButtonBgColor(), messageIconType.getBackground());
                CommonUtils.overrideImageColor(settings.getButtonBgColor(), shareIcon.getDrawable());
                this.wasOvercolored = true;
            }
            setTextColorToView(this.settings.getFeatureTextColor(), holder.getTextViewText(), holder.getTextViewDate(), messageTextType);
        }
        return convertView;
    }
}
