package com.biznessapps.layout.adapters;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.ListItemHolder;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.google.caching.ImageFetcher;
import java.util.List;

public class CommonAdapter<T extends CommonListEntity> extends AbstractAdapter<CommonListEntity> {
    public CommonAdapter(Context context, List<CommonListEntity> items) {
        super(context, items, R.layout.info_item_row);
    }

    public CommonAdapter(Context context, List<CommonListEntity> items, ImageFetcher imageFetcher) {
        super(context, items, R.layout.info_item_row, imageFetcher);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: SimpleMethodDetails{com.biznessapps.utils.google.caching.ImageFetcher.loadImage(java.lang.Object, android.widget.ImageView):void}
     arg types: [java.lang.String, android.widget.ImageView]
     candidates:
      com.biznessapps.utils.google.caching.ImageWorker.loadImage(java.lang.String, android.view.View):void
      SimpleMethodDetails{com.biznessapps.utils.google.caching.ImageFetcher.loadImage(java.lang.Object, android.widget.ImageView):void} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setFrameContainer((ViewGroup) convertView.findViewById(R.id.info_item_container));
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
            holder.setImageView((ImageView) convertView.findViewById(R.id.row_icon));
            holder.setRightArrowView((ImageView) convertView.findViewById(R.id.right_arrow_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
        }
        CommonListEntity item = (CommonListEntity) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getTitle()));
            if (StringUtils.isNotEmpty(item.getImage())) {
                if (this.imageFetcher != null) {
                    this.imageFetcher.loadImage((Object) item.getImage(), holder.getImageView());
                } else {
                    this.imageDownloader.download(item.getImage(), holder.getImageView());
                }
                holder.getImageView().setVisibility(0);
            } else if (item.getImageId() > 0) {
                holder.getImageView().setBackgroundResource(item.getImageId());
                holder.getImageView().setVisibility(0);
            } else {
                holder.getImageView().setVisibility(8);
            }
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
            }
        }
        checkPositioning(position, convertView, parent);
        return convertView;
    }
}
