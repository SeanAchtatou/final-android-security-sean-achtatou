package com.biznessapps.facebook;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import com.biznessapps.api.AppCore;
import com.biznessapps.facebook.SessionEvents;
import com.biznessapps.layout.R;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;

public class LoginButton extends ImageButton {
    /* access modifiers changed from: private */
    public Facebook mFb;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public String[] mPermissions;
    private SessionListener mSessionListener = new SessionListener();
    /* access modifiers changed from: private */
    public boolean needToAuthenticate;

    public LoginButton(Context context) {
        super(context);
    }

    public LoginButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LoginButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void init(Facebook fb, String[] permissions) {
        this.mFb = fb;
        this.mPermissions = permissions;
        this.mHandler = new Handler();
        setBackgroundColor(0);
        setAdjustViewBounds(true);
        setImageResource(fb.isSessionValid() ? R.drawable.logout_button : R.drawable.login_button);
        drawableStateChanged();
        SessionEvents.addAuthListener(this.mSessionListener);
        SessionEvents.addLogoutListener(this.mSessionListener);
        setOnClickListener(new ButtonOnClickListener());
        this.needToAuthenticate = true;
    }

    private final class ButtonOnClickListener implements View.OnClickListener {
        private ButtonOnClickListener() {
        }

        public void onClick(View arg0) {
            if (LoginButton.this.needToAuthenticate || !LoginButton.this.mFb.isSessionValid()) {
                boolean unused = LoginButton.this.needToAuthenticate = false;
                LoginButton.this.mFb.authorize(LoginButton.this.getContext(), AppCore.getInstance().getAppSettings().getFacebookAppId(), LoginButton.this.mPermissions, new LoginDialogListener());
                return;
            }
            SessionEvents.onLogoutBegin();
            new AsyncFacebookRunner(LoginButton.this.mFb).logout(LoginButton.this.getContext(), new LogoutRequestListener());
        }
    }

    private final class LoginDialogListener implements Facebook.DialogListener {
        private LoginDialogListener() {
        }

        public void onComplete(Bundle values) {
            SessionEvents.onLoginSuccess();
        }

        public void onFacebookError(FacebookError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onError(DialogError error) {
            SessionEvents.onLoginError(error.getMessage());
        }

        public void onCancel() {
            SessionEvents.onLoginError("Action Canceled");
        }
    }

    private class LogoutRequestListener extends BaseRequestListener {
        private LogoutRequestListener() {
        }

        public void onComplete(String response) {
            LoginButton.this.mHandler.post(new Runnable() {
                public void run() {
                    SessionEvents.onLogoutFinish();
                }
            });
        }
    }

    private class SessionListener implements SessionEvents.AuthListener, SessionEvents.LogoutListener {
        private SessionListener() {
        }

        public void onAuthSucceed() {
            LoginButton.this.setImageResource(R.drawable.logout_button);
            SessionStore.save(LoginButton.this.mFb, LoginButton.this.getContext());
        }

        public void onAuthFail(String error) {
        }

        public void onLogoutBegin() {
        }

        public void onLogoutFinish() {
            SessionStore.clear(LoginButton.this.getContext());
            LoginButton.this.setImageResource(R.drawable.login_button);
        }
    }
}
