package com.biznessapps.facebook;

import android.content.Context;
import com.facebook.android.Facebook;

public class FacebookUtils {
    public static LoginButton getLogInOutButton(Context context) {
        LoginButton facebookLogInOutButton = new LoginButton(context);
        Facebook mFacebook = new Facebook();
        SessionStore.restore(mFacebook, context);
        facebookLogInOutButton.init(mFacebook, new String[0]);
        return facebookLogInOutButton;
    }
}
