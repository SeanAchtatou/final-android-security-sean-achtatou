package com.biznessapps.fragments;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomizableViewPager extends ViewPager {
    private boolean enableScrolling = true;

    public CustomizableViewPager(Context context) {
        super(context);
    }

    public CustomizableViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void enableScrolling(boolean shouldBeEnabled) {
        this.enableScrolling = shouldBeEnabled;
    }

    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!this.enableScrolling) {
            requestDisallowInterceptTouchEvent(!this.enableScrolling);
        }
        return super.onInterceptTouchEvent(event);
    }
}
