package com.biznessapps.fragments.contacts;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListAdapter;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.ContactListAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.LocationItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedList;
import java.util.List;

public class ContactsListFragment extends CommonListFragment<LocationItem> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.LOCATION_LIST_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseLocationList(dataToParse);
        return cacher().saveData(CachingConstants.LOCATION_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.LOCATION_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.LocationItem r0 = (com.biznessapps.model.LocationItem) r0
            com.biznessapps.activities.CommonFragmentActivity r1 = r2.getHoldActivity()
            r2.startContactActivity(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.contacts.ContactsListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        boolean hasNoData = false;
        if (this.items != null && !this.items.isEmpty()) {
            LocationItem preLoadedItem = null;
            List<LocationItem> locations = new LinkedList<>();
            LocationItem firstItem = (LocationItem) this.items.get(0);
            if (this.items.size() == 1 && StringUtils.isEmpty(firstItem.getId())) {
                hasNoData = true;
            }
            if (hasNoData) {
                firstItem.setTitle(getString(R.string.no_locations));
                locations.add(getWrappedItem(firstItem, locations));
            } else if (this.items.size() == 1) {
                startContactActivity(holdActivity, firstItem);
                holdActivity.finish();
            } else {
                for (LocationItem item : this.items) {
                    if (item.getId().equalsIgnoreCase(this.itemId)) {
                        preLoadedItem = item;
                    }
                    locations.add(getWrappedItem(item, locations));
                }
            }
            this.listView.setAdapter((ListAdapter) new ContactListAdapter(holdActivity.getApplicationContext(), locations));
            initListViewListener();
            startContactActivity(holdActivity, preLoadedItem);
        }
    }

    private void startContactActivity(Activity holdActivity, LocationItem item) {
        if (item != null) {
            Intent intent = new Intent(holdActivity.getApplicationContext(), SingleFragmentActivity.class);
            if (this.items.size() > 1) {
                intent.putExtra(AppConstants.LOCATION_ID, item.getId());
            }
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.CONTACTS_FRAGMENT);
            intent.putExtra(AppConstants.TAB_ID, ((CommonFragmentActivity) holdActivity).getTabId());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_LABEL, holdActivity.getIntent().getStringExtra(AppConstants.TAB_LABEL));
            startActivity(intent);
        }
    }
}
