package com.biznessapps.fragments.events;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.utils.JsonParserUtils;
import java.util.List;

public class EventsV2ListFragment extends EventsListFragment {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.EVENTS_V2_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseEvents(dataToParse);
        return cacher().saveData(CachingConstants.EVENT_V2_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.EVENT_V2_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* access modifiers changed from: protected */
    public String getNameOfEventDetailFragment() {
        return AppConstants.EVENTS_V2_DETAIL_FRAGMENT;
    }

    /* access modifiers changed from: protected */
    public int getListItemLayoutId() {
        return R.layout.list_event_v2_item;
    }
}
