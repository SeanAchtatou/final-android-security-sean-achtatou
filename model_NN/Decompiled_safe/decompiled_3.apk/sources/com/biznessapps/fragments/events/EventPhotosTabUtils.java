package com.biznessapps.fragments.events;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.biznessapps.activities.GalleryActivity;
import com.biznessapps.adapters.CommonAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.api.AsyncCallback;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class EventPhotosTabUtils {
    public static void onActivityResult(int requestCode, int resultCode, Intent data, String appCode, String tabId, String eventId) {
        if (resultCode == -1 && requestCode == 2) {
            Bitmap scaledBitmap = Bitmap.createScaledBitmap((Bitmap) data.getExtras().get("data"), 800, 800, true);
            AppHttpUtils.sendPhotoAsync(CommonUtils.convertImageToBytes(scaledBitmap), appCode, tabId, eventId, new AsyncCallback<String>() {
                public void onResult(String result) {
                }

                public void onError(String message, Throwable throwable) {
                    super.onError(message, throwable);
                }
            });
        }
    }

    public static void loadPhotosData(Activity activity, ViewGroup rootContainer, String eventId, String tabId) {
        final Context appContext = activity.getApplicationContext();
        final ListView listView = (ListView) rootContainer.findViewById(R.id.list_view);
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        ((ViewGroup) rootContainer.findViewById(R.id.photo_list_header)).setBackgroundDrawable(new ColorDrawable(uiSettings.getSectionBarColor()));
        listView.setBackgroundDrawable(new ColorDrawable(uiSettings.getGlobalBgColor()));
        ((TextView) rootContainer.findViewById(R.id.shared_photo_textview)).setTextColor(uiSettings.getEvenRowTextColor());
        final String str = eventId;
        final String str2 = tabId;
        final Activity activity2 = activity;
        new AsyncTask<Void, Void, List<FanWallComment>>() {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
                onPostExecute((List<FanWallComment>) ((List) x0));
            }

            /* access modifiers changed from: protected */
            public List<FanWallComment> doInBackground(Void... params) {
                List<FanWallComment> items = JsonParserUtils.parseFanComments(DataSource.getInstance().getData(String.format(ServerConstants.PHOTO_LIST_FORMAT, cacher.getAppCode(), str, str2)));
                if (items == null || items.isEmpty()) {
                    return null;
                }
                List<FanWallComment> comments = items;
                cacher.saveData(CachingConstants.EVENT_PHOTOS_PROPERTY + str, comments);
                return comments;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(List<FanWallComment> comments) {
                super.onPostExecute((Object) comments);
                if (comments != null) {
                    List<FanWallComment> singleItemList = new ArrayList<>();
                    FanWallComment item = new FanWallComment();
                    singleItemList.add(EventGoingTabUtils.getWrappedItem(item, singleItemList));
                    listView.setAdapter((ListAdapter) new CommonAdapter(appContext, singleItemList));
                    final List<String> urls = new ArrayList<>();
                    for (FanWallComment comment : comments) {
                        if (StringUtils.isNotEmpty(comment.getImage())) {
                            urls.add(comment.getImage());
                        }
                    }
                    item.setTitle(String.format(appContext.getResources().getString(R.string.photos_added), Integer.valueOf(urls.size())));
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> adapterView, View arg1, int arg2, long arg3) {
                            Intent openGalleryIntent = new Intent(activity2, GalleryActivity.class);
                            GalleryData galleryData = new GalleryData();
                            galleryData.setImageItems(urls);
                            openGalleryIntent.putExtra(AppConstants.GALLERY_DATA_EXTRA, galleryData);
                            openGalleryIntent.putExtras(activity2.getIntent());
                            activity2.startActivity(openGalleryIntent);
                        }
                    });
                }
            }
        }.execute(new Void[0]);
    }
}
