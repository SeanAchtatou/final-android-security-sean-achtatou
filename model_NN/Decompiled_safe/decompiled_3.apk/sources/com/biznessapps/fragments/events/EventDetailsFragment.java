package com.biznessapps.fragments.events;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.infoitems.InfoDetailFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.EventItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;

public class EventDetailsFragment extends InfoDetailFragment {
    private static final String BEGIN_TIME = "beginTime";
    private static final String END_TIME = "endTime";
    private static final String EVENT_TYPE = "vnd.android.cursor.item/event";
    private static final String EXRULE_PATT = "FREQ=WEEKLY;WKST=MO;BYDAY=%s";
    private static final String RRULE = "rrule";
    private static final String TITLE = "title";
    private ImageButton addCalendarButton;
    protected String eventDetailId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        final EventItem item = (EventItem) getIntent().getSerializableExtra(AppConstants.EVENT);
        this.addCalendarButton = (ImageButton) root.findViewById(R.id.add_event_to_calendar);
        this.addCalendarButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EventDetailsFragment.this.addEvent(EventDetailsFragment.this.getHoldActivity(), item);
            }
        });
        return root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        super.preDataLoading(holdActivity);
        this.eventDetailId = ((EventItem) getIntent().getSerializableExtra(AppConstants.EVENT)).getId();
    }

    /* access modifiers changed from: protected */
    public boolean canHaveNewDesign() {
        return false;
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setItemId(this.eventDetailId);
        data.setCatId(null);
        return data;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.event_detail_layout;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.EVENT_DETAIL_FORMAT, this.eventDetailId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.infoItem = JsonParserUtils.parseInfo(dataToParse);
        return cacher().saveData(CachingConstants.EVENT_DETAIL_PROPERTY + this.eventDetailId, this.infoItem);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.infoItem = (CommonListEntity) cacher().getData(CachingConstants.EVENT_DETAIL_PROPERTY + this.eventDetailId);
        return this.infoItem != null;
    }

    /* access modifiers changed from: protected */
    public boolean showToastIfDataFailed() {
        return false;
    }

    /* access modifiers changed from: private */
    public void addEvent(Activity activity, EventItem event) {
        Intent intent = new Intent("android.intent.action.EDIT");
        intent.setType(EVENT_TYPE);
        if (StringUtils.isNotEmpty(event.getTitle())) {
            intent.putExtra("title", event.getTitle());
        }
        if (event.getDatetimeBegin() != null) {
            intent.putExtra(BEGIN_TIME, event.getDatetimeBegin().getTimeInMillis());
        }
        if (event.getDatetimeEnd() != null) {
            intent.putExtra(END_TIME, event.getDatetimeEnd().getTimeInMillis());
        }
        if (event.isRecurring()) {
            intent.putExtra(RRULE, String.format(EXRULE_PATT, event.getRecurringDay()));
        }
        activity.startActivity(intent);
    }
}
