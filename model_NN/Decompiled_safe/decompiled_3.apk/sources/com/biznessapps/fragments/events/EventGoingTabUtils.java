package com.biznessapps.fragments.events;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.biznessapps.adapters.CommonAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.LinkedList;
import java.util.List;

public class EventGoingTabUtils {
    public static void initViews(ViewGroup root, final Context appContext, final String tabId, final String eventId) {
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        ((Button) root.findViewById(R.id.event_going_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (cacher.isLoggenInSocial()) {
                    EventGoingTabUtils.postEventData(ServerConstants.GOING_POST_FORMAT, AppConstants.MD5_GOING_RULE, cacher.getAppCode(), tabId, eventId, "", null);
                } else {
                    ViewUtils.showShortToast(appContext, R.string.login_with_social);
                }
            }
        });
    }

    public static void loadGoingData(Context appContext, ViewGroup rootContainer, String eventId, String tabId) {
        final ListView listView = (ListView) rootContainer.findViewById(R.id.list_view);
        final TextView peopleGoing = (TextView) rootContainer.findViewById(R.id.people_going_textview);
        final AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        ((ViewGroup) rootContainer.findViewById(R.id.going_list_header)).setBackgroundDrawable(new ColorDrawable(uiSettings.getSectionBarColor()));
        listView.setBackgroundDrawable(new ColorDrawable(uiSettings.getGlobalBgColor()));
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        final String str = eventId;
        final String str2 = tabId;
        final Context context = appContext;
        new AsyncTask<Void, Void, List<FanWallComment>>() {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
                onPostExecute((List<FanWallComment>) ((List) x0));
            }

            /* access modifiers changed from: protected */
            public List<FanWallComment> doInBackground(Void... params) {
                List<FanWallComment> items = JsonParserUtils.parseFanComments(DataSource.getInstance().getData(String.format(ServerConstants.GOING_LIST_FORMAT, cacher.getAppCode(), str, str2)));
                if (items == null || items.isEmpty()) {
                    return null;
                }
                List<FanWallComment> comments = items;
                cacher.saveData(CachingConstants.EVENT_GOING_PROPERTY + str, comments);
                return comments;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(List<FanWallComment> comments) {
                super.onPostExecute((Object) comments);
                if (comments != null) {
                    List<FanWallComment> resultList = new LinkedList<>();
                    for (FanWallComment item : comments) {
                        if (StringUtils.isNotEmpty(item.getTitle())) {
                            item.setImageId(R.drawable.portrait);
                            resultList.add(EventGoingTabUtils.getWrappedItem(item, resultList));
                        }
                    }
                    listView.setAdapter((ListAdapter) new CommonAdapter(context, resultList));
                    String peopleGoingFormat = context.getResources().getString(R.string.people_going);
                    peopleGoing.setText(String.format(peopleGoingFormat, Integer.valueOf(resultList.size())));
                    peopleGoing.setTextColor(uiSettings.getEvenRowTextColor());
                }
            }
        }.execute(new Void[0]);
    }

    public static FanWallComment getWrappedItem(FanWallComment item, List<FanWallComment> itemsList) {
        int bgColor;
        int textColor;
        AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        if (uiSettings.isHasColor()) {
            if (itemsList.size() % 2 == 0) {
                bgColor = uiSettings.getOddRowColor();
                textColor = uiSettings.getOddRowTextColor();
            } else {
                bgColor = uiSettings.getEvenRowColor();
                textColor = uiSettings.getEvenRowTextColor();
            }
            item.setItemColor(bgColor);
            item.setItemTextColor(textColor);
        }
        return item;
    }

    public static void postEventData(String url, String md5rule, String appCode, String tabId, String id, String comment, String parentId) {
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        final String str = md5rule;
        final String str2 = url;
        final String str3 = appCode;
        final String str4 = tabId;
        final String str5 = id;
        final String str6 = comment;
        final String str7 = parentId;
        new AsyncTask<Void, Void, Void>() {
            /* access modifiers changed from: protected */
            public Void doInBackground(Void... params) {
                String userId;
                String userName;
                String userType;
                if (StringUtils.isNotEmpty(cacher.getFacebookUid())) {
                    userId = "" + cacher.getFacebookUid();
                    userName = cacher.getFacebookUserName();
                    userType = "1";
                } else {
                    userId = "" + cacher.getTwitterUid();
                    userName = cacher.getTwitterUserName();
                    userType = AppConstants.TWITTER_USER_TYPE;
                }
                AppHttpUtils.postEventDataSync(str2, str3, str4, str5, userType, userId, userName, str6, CommonUtils.getMD5Hash(String.format(str, userId, userType)), str7);
                return null;
            }
        }.execute(new Void[0]);
    }
}
