package com.biznessapps.fragments.events;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.biznessapps.adapters.EventCommentsAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.LinkedList;
import java.util.List;

public class EventCommentsTabUtils {
    public static void loadCommentsData(Context appContext, ViewGroup rootContainer, String eventId, String tabId) {
        final ListView listView = (ListView) rootContainer.findViewById(R.id.list_view);
        final CachingManager cacher = AppCore.getInstance().getCachingManager();
        final ViewGroup header = (ViewGroup) rootContainer.findViewById(R.id.comments_list_header);
        header.setBackgroundDrawable(new ColorDrawable(AppCore.getInstance().getUiSettings().getOddRowColor()));
        ViewUtils.setGlobalBackgroundColor(rootContainer);
        final String str = eventId;
        final String str2 = tabId;
        final Context context = appContext;
        new AsyncTask<Void, Void, List<FanWallComment>>() {
            /* access modifiers changed from: protected */
            public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
                onPostExecute((List<FanWallComment>) ((List) x0));
            }

            /* access modifiers changed from: protected */
            public List<FanWallComment> doInBackground(Void... params) {
                List<FanWallComment> items = JsonParserUtils.parseFanComments(DataSource.getInstance().getData(String.format(ServerConstants.COMMENT_LIST_FORMAT, cacher.getAppCode(), str, str2)));
                if (items == null || items.isEmpty()) {
                    return null;
                }
                List<FanWallComment> comments = items;
                cacher.saveData(CachingConstants.FAN_WALL_INFO_PROPERTY + str, comments);
                return comments;
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(List<FanWallComment> comments) {
                boolean hasNotData = true;
                int i = 0;
                super.onPostExecute((Object) comments);
                if (comments != null) {
                    List<FanWallComment> resultList = new LinkedList<>();
                    for (FanWallComment item : comments) {
                        item.setImageId(R.drawable.portrait);
                        resultList.add(EventGoingTabUtils.getWrappedItem(item, resultList));
                    }
                    if (!comments.isEmpty() && (comments.size() != 1 || !StringUtils.isEmpty(comments.get(0).getId()))) {
                        hasNotData = false;
                    }
                    ViewGroup viewGroup = header;
                    if (!hasNotData) {
                        i = 8;
                    }
                    viewGroup.setVisibility(i);
                    if (!hasNotData) {
                        listView.setAdapter((ListAdapter) new EventCommentsAdapter(context, resultList));
                    }
                }
            }
        }.execute(new Void[0]);
    }
}
