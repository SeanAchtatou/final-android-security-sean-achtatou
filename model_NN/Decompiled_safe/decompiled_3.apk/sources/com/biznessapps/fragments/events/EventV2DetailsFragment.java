package com.biznessapps.fragments.events;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.api.AsyncCallback;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.ViewUtils;

public class EventV2DetailsFragment extends EventDetailsFragment {
    /* access modifiers changed from: private */
    public Button commentsButton;
    /* access modifiers changed from: private */
    public ViewGroup commentsTabContainer;
    /* access modifiers changed from: private */
    public Button goingButton;
    /* access modifiers changed from: private */
    public ViewGroup goingTabContainer;
    private ImageView headerImageView;
    /* access modifiers changed from: private */
    public Button infoButton;
    /* access modifiers changed from: private */
    public ViewGroup infoTabContainer;
    /* access modifiers changed from: private */
    public Button photosButton;
    /* access modifiers changed from: private */
    public ViewGroup photosTabContainer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        this.infoTabContainer = (ViewGroup) root.findViewById(R.id.event_info_tab_container);
        this.commentsTabContainer = (ViewGroup) root.findViewById(R.id.event_comments_tab_container);
        this.goingTabContainer = (ViewGroup) root.findViewById(R.id.event_going_tab_container);
        this.photosTabContainer = (ViewGroup) root.findViewById(R.id.event_photo_tab_container);
        EventGoingTabUtils.initViews(this.goingTabContainer, getApplicationContext(), this.tabId, this.eventDetailId);
        this.infoButton = (Button) root.findViewById(R.id.event_new_info_button);
        this.commentsButton = (Button) root.findViewById(R.id.event_new_comments_button);
        this.goingButton = (Button) root.findViewById(R.id.event_new_going_button);
        this.photosButton = (Button) root.findViewById(R.id.event_new_photos_button);
        this.headerImageView = (ImageView) root.findViewById(R.id.info_header_image);
        ViewUtils.setGlobalBackgroundColor(this.headerImageView);
        this.infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EventV2DetailsFragment.this.activateButton(EventV2DetailsFragment.this.infoButton, EventV2DetailsFragment.this.infoTabContainer);
            }
        });
        this.commentsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EventV2DetailsFragment.this.activateButton(EventV2DetailsFragment.this.commentsButton, EventV2DetailsFragment.this.commentsTabContainer);
                EventCommentsTabUtils.loadCommentsData(EventV2DetailsFragment.this.getApplicationContext(), EventV2DetailsFragment.this.commentsTabContainer, EventV2DetailsFragment.this.eventDetailId, EventV2DetailsFragment.this.tabId);
            }
        });
        this.goingButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EventV2DetailsFragment.this.activateButton(EventV2DetailsFragment.this.goingButton, EventV2DetailsFragment.this.goingTabContainer);
                EventGoingTabUtils.loadGoingData(EventV2DetailsFragment.this.getApplicationContext(), EventV2DetailsFragment.this.goingTabContainer, EventV2DetailsFragment.this.eventDetailId, EventV2DetailsFragment.this.tabId);
            }
        });
        this.photosButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                EventV2DetailsFragment.this.activateButton(EventV2DetailsFragment.this.photosButton, EventV2DetailsFragment.this.photosTabContainer);
                EventPhotosTabUtils.loadPhotosData(EventV2DetailsFragment.this.getHoldActivity(), EventV2DetailsFragment.this.photosTabContainer, EventV2DetailsFragment.this.eventDetailId, EventV2DetailsFragment.this.tabId);
            }
        });
        ((Button) root.findViewById(R.id.add_photo_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EventV2DetailsFragment.this.openStandartPhotoView();
            }
        });
        activateButton(this.infoButton, this.infoTabContainer);
        int barColor = getUiSettings().getNavigationBarColor();
        int barTextColor = getUiSettings().getNavigationTextColor();
        ((ViewGroup) root.findViewById(R.id.event_new_buttons_container)).setBackgroundColor(barColor);
        this.infoButton.setTextColor(barTextColor);
        this.commentsButton.setTextColor(barTextColor);
        this.goingButton.setTextColor(barTextColor);
        this.photosButton.setTextColor(barTextColor);
        CommonUtils.customizeFooterNavigationBar(root.findViewById(R.id.choose_login_account_container));
        SocialNetworkAccessor socialAccessor = new SocialNetworkAccessor(getHoldActivity(), (ViewGroup) root);
        SocialNetworkAccessor.SocialNetworkListener listener = new SocialNetworkAccessor.SocialNetworkListener(socialAccessor) {
            public void onAuthSucceed() {
                EventV2DetailsFragment.this.addComment();
            }
        };
        socialAccessor.setAddCommentListener(root.findViewById(R.id.event_comment_button));
        getHoldActivity().setSocialNetworkListener(listener);
        socialAccessor.addAuthorizationListener(listener);
        ViewUtils.updateMusicBarBottomMargin(getHoldActivity().getMusicDelegate(), (int) getResources().getDimension(R.dimen.footer_bar_height));
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return root;
    }

    public void onResume() {
        super.onResume();
        EventCommentsTabUtils.loadCommentsData(getHoldActivity(), this.commentsTabContainer, this.eventDetailId, this.tabId);
        EventPhotosTabUtils.loadPhotosData(getHoldActivity(), this.photosTabContainer, this.eventDetailId, this.tabId);
        EventGoingTabUtils.loadGoingData(getApplicationContext(), this.goingTabContainer, this.eventDetailId, this.tabId);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (resultCode) {
            case 4:
                addComment();
                break;
        }
        if (requestCode == 2) {
            sendPhoto(intent);
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.event_detail_new_layout;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.EVENT_V2_DETAIL_FORMAT, cacher().getAppCode(), this.eventDetailId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.infoItem = JsonParserUtils.parseInfo(dataToParse);
        return cacher().saveData(CachingConstants.EVENT_DETAIL_PROPERTY + this.eventDetailId, this.infoItem);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.infoItem = (CommonListEntity) cacher().getData(CachingConstants.EVENT_DETAIL_PROPERTY + this.eventDetailId);
        return this.infoItem != null;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return this.infoItem != null ? this.infoItem.getImage() : "";
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.headerImageView;
    }

    /* access modifiers changed from: private */
    public void activateButton(Button buttonToActivate, ViewGroup tabToActivate) {
        this.infoButton.setBackgroundResource(R.drawable.tab_bar);
        this.commentsButton.setBackgroundResource(R.drawable.tab_bar);
        this.goingButton.setBackgroundResource(R.drawable.tab_bar);
        this.photosButton.setBackgroundResource(R.drawable.tab_bar);
        buttonToActivate.setBackgroundResource(R.drawable.tab_button_active);
        this.infoTabContainer.setVisibility(8);
        this.commentsTabContainer.setVisibility(8);
        this.goingTabContainer.setVisibility(8);
        this.photosTabContainer.setVisibility(8);
        tabToActivate.setVisibility(0);
    }

    private void sendPhoto(Intent data) {
        if (data != null && data.getExtras() != null) {
            Bitmap scaledBitmap = Bitmap.createScaledBitmap((Bitmap) data.getExtras().get("data"), 800, 800, true);
            AppHttpUtils.sendPhotoAsync(CommonUtils.convertImageToBytes(scaledBitmap), cacher().getAppCode(), this.tabId, this.eventDetailId, new AsyncCallback<String>() {
                public void onResult(String result) {
                    Activity activity = EventV2DetailsFragment.this.getHoldActivity();
                    if (activity != null) {
                        EventPhotosTabUtils.loadPhotosData(activity, EventV2DetailsFragment.this.photosTabContainer, EventV2DetailsFragment.this.eventDetailId, EventV2DetailsFragment.this.tabId);
                    }
                }

                public void onError(String message, Throwable throwable) {
                    super.onError(message, throwable);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void openStandartPhotoView() {
        startActivityForResult(new Intent(AppConstants.IMAGE_CAPTURE_INTENT_NAME), 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void addComment() {
        Activity activity = getHoldActivity();
        if (activity != null) {
            Intent intent = new Intent(activity.getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, activity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.comments));
            intent.putExtra(AppConstants.YOUTUBE_MODE, false);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.FAN_ADD_COMMENT_FRAGMENT);
            intent.putExtra(AppConstants.USE_SPECIAL_MD5_HASH_EXTRA, true);
            intent.putExtra("id", this.eventDetailId);
            activity.startActivityForResult(intent, 4);
        }
    }
}
