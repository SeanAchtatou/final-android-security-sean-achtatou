package com.biznessapps.fragments.music;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.adapters.PlaylistAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.PlaylistItem;
import com.biznessapps.player.MusicItem;
import com.biznessapps.player.MusicPlayer;
import com.biznessapps.player.PlayerServiceAccessor;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.List;

public class MusicListFragment extends CommonListFragment<PlaylistItem> {
    /* access modifiers changed from: private */
    public Gallery albumGallery;
    private int currentAlbum = 0;
    /* access modifiers changed from: private */
    public int currentPosition = 0;
    private String headerBgUrl;
    private BitmapWrapper headerBgWrapper;
    private ImageView headerView;
    private boolean isTablet;

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.playlist_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.albumGallery = (Gallery) root.findViewById(R.id.playlist_album_gallery);
        this.albumGallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                MusicListFragment.this.setCurrentAlbum(position);
                int unused = MusicListFragment.this.currentPosition = position;
                if (MusicListFragment.this.albumGallery.getAdapter() instanceof AlbumTitleAdapter) {
                    ((AlbumTitleAdapter) MusicListFragment.this.albumGallery.getAdapter()).notifyDataSetChanged();
                }
            }
        });
        this.albumGallery.setBackgroundColor(getUiSettings().getNavigationBarColor());
        DisplayMetrics metrics = new DisplayMetrics();
        getHoldActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = getResources().getDimensionPixelSize(R.dimen.gallery_title_width);
        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) this.albumGallery.getLayoutParams();
        mlp.setMargins(-((metrics.widthPixels / 2) + (width / 2)), mlp.topMargin, mlp.rightMargin, mlp.bottomMargin);
        this.headerView = (ImageView) root.findViewById(R.id.headerImage);
        ViewUtils.setGlobalBackgroundColor(this.headerView);
        this.isTablet = AppCore.getInstance().isTablet();
    }

    public void onResume() {
        super.onResume();
        getHoldActivity().displayMusicPanel();
        loadHeaderBg();
    }

    public void onStop() {
        super.onStop();
        clearHeaderBg();
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MUSIC_PLAYLIST_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        List<PlaylistItem> musicList = JsonParserUtils.parseMusicList(dataToParse);
        if (this.isTablet) {
            if (musicList != null && !musicList.isEmpty()) {
                this.bgUrl = musicList.get(0).getBackground();
            }
            this.items = (List) cacher().getData(CachingConstants.MUSIC_PLAYLIST_PROPERTY + this.tabId);
            if (this.items == null || this.items.isEmpty()) {
                this.items = musicList;
            } else {
                ((PlaylistItem) this.items.get(0)).setBackground(this.bgUrl);
            }
            return cacher().saveData(CachingConstants.MUSIC_PLAYLIST_PROPERTY + this.tabId + this.isTablet, this.items);
        }
        this.items = musicList;
        return cacher().saveData(CachingConstants.MUSIC_PLAYLIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (this.items != null && !this.items.isEmpty()) {
            this.headerBgUrl = ((PlaylistItem) this.items.get(0)).getHeader();
            loadHeaderBg();
            plugListView(holdActivity);
            plugAlbumView(holdActivity);
        }
    }

    private void loadHeaderBg() {
        boolean hasHeaderBg = StringUtils.isNotEmpty(this.headerBgUrl);
        this.headerView.setVisibility(hasHeaderBg ? 0 : 8);
        if (hasHeaderBg) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadAppImage(this.headerBgUrl, this.headerView);
        }
    }

    private void clearHeaderBg() {
        this.headerView.setBackgroundDrawable(null);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        if (this.isTablet) {
            this.items = (List) cacher().getData(CachingConstants.MUSIC_PLAYLIST_PROPERTY + this.tabId + this.isTablet);
        } else {
            this.items = (List) cacher().getData(CachingConstants.MUSIC_PLAYLIST_PROPERTY + this.tabId);
        }
        return this.items != null && !this.items.isEmpty();
    }

    private void plugListView(Activity holdActivity) {
        if (!this.items.isEmpty()) {
            List<PlaylistItem> playlist = new ArrayList<>();
            for (PlaylistItem item : this.items) {
                playlist.add(getWrappedItem(item, playlist));
            }
            initListViewWithData(holdActivity, playlist);
            resetPlaylistData(playlist);
        }
    }

    private void initListViewWithData(Activity holdActivity, List<PlaylistItem> playlist) {
        PlaylistAdapter ex = new PlaylistAdapter(holdActivity.getApplicationContext(), playlist);
        ex.setParentFragment(this);
        this.listView.setAdapter((ListAdapter) ex);
        initListViewListener();
    }

    private void resetPlaylistData(List<PlaylistItem> playlist) {
        getPlayerServiceAccessor().stop();
        getPlayerServiceAccessor().clearQueue();
        getPlayerServiceAccessor().addUrlsQueue(extractUrlsFromData(playlist));
        if (playlist != null && !playlist.isEmpty()) {
            playMusic(playlist.get(0));
        }
    }

    private List<MusicItem> extractUrlsFromData(List<PlaylistItem> items) {
        List<MusicItem> previewUrls = new ArrayList<>();
        for (PlaylistItem item : items) {
            MusicItem musicItem = new MusicItem();
            if (StringUtils.isNotEmpty(item.getPreviewUrl())) {
                musicItem.setUrl(item.getPreviewUrl());
                musicItem.setSongInfo(item.getTitle());
                musicItem.setAlbumName(item.getAlbum());
                previewUrls.add(musicItem);
            }
        }
        return previewUrls;
    }

    private void plugAlbumView(Activity holdActivity) {
        this.albumGallery.setAdapter((SpinnerAdapter) new AlbumTitleAdapter(getApplicationContext(), getUniqueAlbums()));
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.SONG_INFO_VIEW_CONTROLLER);
        intent.putExtra("id", ((PlaylistItem) this.items.get(position)).getId());
        intent.putExtra(AppConstants.BG_URL_EXTRA, ((PlaylistItem) this.items.get(0)).getBackground());
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.music_tab_label));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onAlbumListItemClick(AdapterView<?> adapterView, View view, int position, long id) {
    }

    public void playMusic(PlaylistItem item) {
        if (item != null) {
            try {
                getPlayerServiceAccessor().stop();
                MusicItem musicItem = new MusicItem();
                musicItem.setUrl(item.getPreviewUrl());
                musicItem.setSongInfo(item.getTitle());
                musicItem.setAlbumName(item.getAlbum());
                getPlayerServiceAccessor().play(musicItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void buyItem(PlaylistItem item) {
        ViewUtils.openLinkInBrowser(getApplicationContext(), item.getItune());
    }

    public int getCurrentAlbum() {
        return this.currentAlbum;
    }

    public void setCurrentAlbum(int album) {
        this.currentAlbum = album;
        reloadListView();
    }

    private List<CommonListEntity> getUniqueAlbums() {
        List<CommonListEntity> albums = new ArrayList<>();
        albums.add(new CommonListEntity(getString(R.string.all_tracks_title)));
        if (this.items != null && !this.items.isEmpty()) {
            for (int i = 0; i < this.items.size(); i++) {
                String albumName = ((PlaylistItem) this.items.get(i)).getAlbum();
                boolean found = false;
                int j = 0;
                while (true) {
                    if (j >= albums.size()) {
                        break;
                    } else if (albumName.equalsIgnoreCase(((CommonListEntity) albums.get(j)).getTitle())) {
                        found = true;
                        break;
                    } else {
                        j++;
                    }
                }
                if (!found) {
                    albums.add(new CommonListEntity(albumName));
                }
            }
        }
        return albums;
    }

    private void reloadListView() {
        if (!this.items.isEmpty()) {
            List<PlaylistItem> newItems = new ArrayList<>();
            if (this.currentAlbum != 0) {
                String albumName = getUniqueAlbums().get(this.currentAlbum).getTitle();
                for (PlaylistItem item : this.items) {
                    if (item.getAlbum().equals(albumName)) {
                        newItems.add(getWrappedItem(item, newItems));
                    }
                }
            } else {
                for (PlaylistItem item2 : this.items) {
                    newItems.add(getWrappedItemWithInvertion(item2, newItems));
                }
            }
            initListViewWithData(getHoldActivity(), newItems);
            resetPlaylistData(newItems);
        }
    }

    private PlayerServiceAccessor getPlayerServiceAccessor() {
        if (!MusicPlayer.getInstance().isInited()) {
            MusicPlayer.getInstance().init(getApplicationContext());
        }
        return MusicPlayer.getInstance().getServiceAccessor();
    }

    public class AlbumTitleAdapter<T extends CommonListEntity> extends AbstractAdapter<CommonListEntity> {
        public AlbumTitleAdapter(Context context, List<CommonListEntity> items) {
            super(context, items, R.layout.playlist_title_row);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemHolder.CommonItem holder;
            if (convertView == null) {
                convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
                holder = new ListItemHolder.CommonItem();
                holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
                holder.setRightArrowView((ImageView) convertView.findViewById(R.id.wall_pointer));
                CommonUtils.overrideImageColor(MusicListFragment.this.getUiSettings().getNavigationTextColor(), ((ImageView) convertView.findViewById(R.id.playlist_gallery_icon)).getBackground());
                CommonUtils.overrideImageColor(MusicListFragment.this.getUiSettings().getEvenRowTextColor(), convertView.findViewById(R.id.playlist_gallery_icon).getBackground());
                CommonUtils.overrideImageColor(MusicListFragment.this.getUiSettings().getEvenRowTextColor(), convertView.findViewById(R.id.playlist_vertical_column).getBackground());
                convertView.setTag(holder);
            } else {
                holder = (ListItemHolder.CommonItem) convertView.getTag();
            }
            CommonListEntity item = (CommonListEntity) this.items.get(position);
            if (item != null) {
                holder.getTextViewTitle().setText(Html.fromHtml(item.getTitle()));
                if (MusicListFragment.this.currentPosition == position) {
                    holder.getTextViewTitle().setTextColor(MusicListFragment.this.getUiSettings().getNavigationTextColor());
                    holder.getRightArrowView().setVisibility(0);
                    convertView.findViewById(R.id.playlist_gallery_icon).setVisibility(0);
                } else {
                    holder.getTextViewTitle().setTextColor(MusicListFragment.this.getUiSettings().getNavigationTextColor());
                    holder.getRightArrowView().setVisibility(4);
                    convertView.findViewById(R.id.playlist_gallery_icon).setVisibility(4);
                }
            }
            return convertView;
        }
    }
}
