package com.biznessapps.fragments.music;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.DataSource;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.fanwall.NewFanWallAdapter;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.FanWallItem;
import com.biznessapps.model.PlaylistItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.widgets.RefreshableListView;
import java.util.List;

public class SongInfoFragment extends CommonFragment {
    protected ViewGroup accountsContentView;
    protected Button addViaFacebook;
    protected Button addViaTwitter;
    private String albumBgUrl;
    private BitmapWrapper albumBgWrapper;
    private ImageView albumImageView;
    private TextView albumNameView;
    private TextView artistNameView;
    private ViewGroup buyButtonContainer;
    protected Button cancelAccountsButton;
    protected ImageButton chooseAccountsButton;
    /* access modifiers changed from: private */
    public Button commentsButton;
    private RefreshableListView commentsListView;
    protected FanWallItem info;
    /* access modifiers changed from: private */
    public Button infoButton;
    protected boolean needToReload;
    protected ViewGroup rootView;
    private String songId;
    /* access modifiers changed from: private */
    public PlaylistItem songItem;
    private TextView songTitleView;
    protected String tabId;
    /* access modifiers changed from: private */
    public WebView webView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(getLayoutId(), (ViewGroup) null);
        initViews(this.root);
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onResume() {
        super.onResume();
        loadHeaderBg();
    }

    public void onStop() {
        super.onStop();
        clearHeaderBg();
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return this.info != null ? this.info.getImage() : "";
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    private void loadHeaderBg() {
        boolean hasHeaderBg = StringUtils.isNotEmpty(this.albumBgUrl);
        this.albumImageView.setVisibility(hasHeaderBg ? 0 : 8);
        if (hasHeaderBg) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadAppImage(this.albumBgUrl, this.albumImageView);
        }
    }

    private void clearHeaderBg() {
        this.albumImageView.setBackgroundDrawable(null);
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.albumImageView = (ImageView) root.findViewById(R.id.song_image_view);
        this.artistNameView = (TextView) root.findViewById(R.id.artist_name);
        this.songTitleView = (TextView) root.findViewById(R.id.song_title);
        this.albumNameView = (TextView) root.findViewById(R.id.album_name);
        this.buyButtonContainer = (ViewGroup) root.findViewById(R.id.buy_song_container);
        this.commentsListView = (RefreshableListView) root.findViewById(R.id.comments_list_view);
        ViewUtils.setGlobalBackgroundColor(root.findViewById(R.id.song_info_panel));
        this.bgUrl = getIntent().getStringExtra(AppConstants.BG_URL_EXTRA);
        this.buyButtonContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ViewUtils.openLinkInBrowser(SongInfoFragment.this.getApplicationContext(), SongInfoFragment.this.songItem.getItune());
            }
        });
        ((TextView) root.findViewById(R.id.buy_text)).setTextColor(getUiSettings().getButtonTextColor());
        ViewGroup tabContainer = (ViewGroup) root.findViewById(R.id.commentsTab);
        this.commentsButton = (Button) tabContainer.findViewById(R.id.comments_button);
        this.infoButton = (Button) tabContainer.findViewById(R.id.info_button);
        this.infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SongInfoFragment.this.activateButton(SongInfoFragment.this.infoButton, SongInfoFragment.this.webView);
            }
        });
        this.commentsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                SongInfoFragment.this.activateButton(SongInfoFragment.this.commentsButton, SongInfoFragment.this.rootView);
            }
        });
        int barColor = getUiSettings().getNavigationBarColor();
        int barTextColor = getUiSettings().getNavigationTextColor();
        tabContainer.setBackgroundColor(barColor);
        this.commentsButton.setTextColor(barTextColor);
        this.infoButton.setTextColor(barTextColor);
        this.commentsListView = (RefreshableListView) root.findViewById(R.id.comments_list_view);
        this.rootView = (ViewGroup) root.findViewById(R.id.fan_wall_root_layout);
        this.webView = (WebView) root.findViewById(R.id.webview);
        SocialNetworkAccessor socialAccessor = new SocialNetworkAccessor(getHoldActivity(), root);
        SocialNetworkAccessor.SocialNetworkListener listener = new SocialNetworkAccessor.SocialNetworkListener(socialAccessor) {
            public void onAuthSucceed() {
                SongInfoFragment.this.addComment();
            }
        };
        getHoldActivity().setSocialNetworkListener(listener);
        socialAccessor.addAuthorizationListener(listener);
        activateButton(this.commentsButton, this.rootView);
    }

    /* access modifiers changed from: private */
    public void activateButton(Button buttonToActivate, View viewToActivate) {
        this.infoButton.setBackgroundResource(R.drawable.tab_bar);
        this.commentsButton.setBackgroundResource(R.drawable.tab_bar);
        buttonToActivate.setBackgroundResource(R.drawable.tab_button_active);
        this.rootView.setVisibility(8);
        this.webView.setVisibility(8);
        viewToActivate.setVisibility(0);
    }

    private void loadWebContent() {
        String htmlDescription = this.songItem.getDescription();
        if (StringUtils.isNotEmpty(htmlDescription)) {
            ViewUtils.plubWebView(this.webView);
            this.webView.loadDataWithBaseURL(null, StringUtils.decode(htmlDescription), AppConstants.TEXT_HTML, AppConstants.UTF_8_CHARSET, null);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (resultCode) {
            case 4:
                this.needToReload = true;
                loadData();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        this.songId = holdActivity.getIntent().getStringExtra("id");
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        if (this.songItem != null) {
            data.setItemId(this.songItem.getId());
        }
        return data;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MUSIC_DETAIL_FORMAT, cacher().getAppCode(), this.songId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        List<PlaylistItem> musicList = JsonParserUtils.parseMusicList(dataToParse);
        if (!musicList.isEmpty()) {
            this.songItem = musicList.get(0);
            cacher().saveData(CachingConstants.SONG_INFO_PROPERTY + this.songItem.getId(), this.songItem);
        }
        List<FanWallItem> items = JsonParserUtils.parseFanWallData(DataSource.getInstance().getData(String.format(ServerConstants.FAN_WALL_YOUTUBE_FORMAT, cacher().getAppCode(), this.songId, this.tabId)));
        if (items != null && !items.isEmpty()) {
            this.info = items.get(0);
            cacher().saveData(CachingConstants.FAN_WALL_INFO_PROPERTY + this.songId, this.info);
        }
        return this.info != null;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        boolean canUseCache;
        this.info = (FanWallItem) cacher().getData(CachingConstants.FAN_WALL_INFO_PROPERTY + this.songId);
        this.songItem = (PlaylistItem) cacher().getData(CachingConstants.SONG_INFO_PROPERTY + this.songId);
        if (this.info == null || this.needToReload || this.songItem == null) {
            canUseCache = false;
        } else {
            canUseCache = true;
        }
        this.needToReload = false;
        return canUseCache;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (this.songItem != null) {
            this.artistNameView.setText(this.songItem.getArtist());
            this.songTitleView.setText(this.songItem.getTitle());
            this.albumNameView.setText(this.songItem.getAlbum());
            this.albumBgUrl = this.songItem.getAlbumArt();
            this.buyButtonContainer.setVisibility(StringUtils.isNotEmpty(this.songItem.getItune()) ? 0 : 4);
            loadWebContent();
        }
        loadHeaderBg();
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.songinfo_layout;
    }

    public PlaylistItem getSongItem() {
        return this.songItem;
    }

    public void setSongItem(PlaylistItem songItem2) {
        this.songItem = songItem2;
    }

    private AdapterView.OnItemClickListener getOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r3v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r6, android.view.View r7, int r8, long r9) {
                /*
                    r5 = this;
                    android.widget.Adapter r3 = r6.getAdapter()
                    java.lang.Object r1 = r3.getItem(r8)
                    com.biznessapps.model.FanWallComment r1 = (com.biznessapps.model.FanWallComment) r1
                    if (r1 == 0) goto L_0x003f
                    android.content.Intent r0 = new android.content.Intent
                    com.biznessapps.fragments.music.SongInfoFragment r3 = com.biznessapps.fragments.music.SongInfoFragment.this
                    android.content.Context r3 = r3.getApplicationContext()
                    java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r4 = com.biznessapps.activities.SingleFragmentActivity.class
                    r0.<init>(r3, r4)
                    java.lang.String r3 = "parent_id"
                    java.lang.String r4 = r1.getId()
                    r0.putExtra(r3, r4)
                    com.biznessapps.fragments.music.SongInfoFragment r3 = com.biznessapps.fragments.music.SongInfoFragment.this
                    android.content.Intent r3 = r3.getIntent()
                    java.lang.String r4 = "TAB_SPECIAL_ID"
                    java.lang.String r2 = r3.getStringExtra(r4)
                    java.lang.String r3 = "TAB_FRAGMENT"
                    java.lang.String r4 = "FanWallViewController"
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "TAB_SPECIAL_ID"
                    r0.putExtra(r3, r2)
                    com.biznessapps.fragments.music.SongInfoFragment r3 = com.biznessapps.fragments.music.SongInfoFragment.this
                    r3.startActivity(r0)
                L_0x003f:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.music.SongInfoFragment.AnonymousClass5.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        };
    }

    /* access modifiers changed from: protected */
    public void plugListView(Activity holdActivity) {
        if (this.info != null && !this.info.getComments().isEmpty()) {
            this.commentsListView.setAdapter((ListAdapter) new NewFanWallAdapter(holdActivity.getApplicationContext(), this.info.getComments(), true));
            ViewUtils.setGlobalBackgroundColor(this.commentsListView);
            this.commentsListView.setOnItemClickListener(getOnItemClickListener());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void addComment() {
        Activity activity = getHoldActivity();
        if (activity != null) {
            Intent intent = new Intent(activity.getApplicationContext(), SingleFragmentActivity.class);
            String tabParam = activity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
            intent.putExtra("parent_id", this.songId);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabParam);
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.comments));
            intent.putExtra(AppConstants.YOUTUBE_MODE, true);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.FAN_ADD_COMMENT_FRAGMENT);
            startActivityForResult(intent, 4);
        }
    }
}
