package com.biznessapps.fragments.loyalty;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.CommonAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragmentNew;
import com.biznessapps.layout.R;
import com.biznessapps.model.LoyaltyItem;
import com.biznessapps.utils.JsonParserUtils;
import java.util.LinkedList;
import java.util.List;

public class LoyaltyListFragment extends CommonListFragmentNew<LoyaltyItem> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.LOYALTIES_LIST_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseLoyaltyList(dataToParse);
        return cacher().saveData(CachingConstants.LOYALTY_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.LOYALTY_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.LoyaltyItem r0 = (com.biznessapps.model.LoyaltyItem) r0
            r2.openLoayltyItem(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.loyalty.LoyaltyListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        if (!this.items.isEmpty()) {
            LoyaltyItem preLoadedItem = null;
            List<LoyaltyItem> sectionList = new LinkedList<>();
            for (LoyaltyItem item : this.items) {
                if (item.getId().equalsIgnoreCase(this.itemId)) {
                    preLoadedItem = item;
                }
                sectionList.add(getWrappedItem(item, sectionList));
            }
            this.adapter = new CommonAdapter(holdActivity.getApplicationContext(), sectionList);
            this.listView.setAdapter((ListAdapter) this.adapter);
            initListViewListener();
            openLoayltyItem(preLoadedItem);
            if (this.items.size() == 1) {
                openLoayltyItem((LoyaltyItem) this.items.get(0));
            } else {
                openLoayltyItem(preLoadedItem);
            }
        }
    }

    private void openLoayltyItem(LoyaltyItem item) {
        if (item != null) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.LOYALTY_EXTRA, item);
            intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.LOYALTY_DETAIL_FRAGMENT);
            intent.putExtra(AppConstants.BG_URL_EXTRA, this.bgUrl);
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.coupon_tab_title));
            startActivity(intent);
        }
    }
}
