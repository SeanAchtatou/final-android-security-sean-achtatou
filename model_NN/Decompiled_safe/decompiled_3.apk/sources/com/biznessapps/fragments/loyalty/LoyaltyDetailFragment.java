package com.biznessapps.fragments.loyalty;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.LoyaltyItem;
import com.biznessapps.storage.StorageKeeper;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.List;

public class LoyaltyDetailFragment extends CommonFragment {
    private static final int COLUMN_NUMBER = 5;
    private static final int MAX_PERCENT_VALUE = 100;
    private static final int MIN_PERCENT_VALUE = 0;
    private static final float SEEKBAAR_INFO_WIDTH_FACTOR = 0.93f;
    private static final float SEEKBAAR_WIDTH_FACTOR = 0.8f;
    private static final int TEXT_WIDTH_OFFSET = 4;
    private int appliedCoupons;
    /* access modifiers changed from: private */
    public boolean areItemsApproved;
    private ViewGroup cardContainer;
    /* access modifiers changed from: private */
    public boolean isNextCardFound;
    private LoyaltyItem item;
    private TextView loyaltyCurrentValue;
    /* access modifiers changed from: private */
    public ImageView nextCardView;
    private int seekBarWidth;
    private SeekBar seekbar;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.loyalty_detail, (ViewGroup) null);
        this.item = (LoyaltyItem) getIntent().getSerializableExtra(AppConstants.LOYALTY_EXTRA);
        if (this.item != null) {
            LoyaltyItem storedItem = StorageKeeper.instance().getLoyaltyItem(this.item.getId());
            if (storedItem != null) {
                synchronizeData(storedItem, this.item);
            }
            initViews();
            loadData();
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        ViewUtils.setGlobalBackgroundColor(this.root);
        return this.root;
    }

    public void onPause() {
        super.onPause();
        if (this.item != null) {
            StorageKeeper.instance().saveLoyaltyItem(this.item);
        }
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setTabId(getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        if (this.item != null) {
            data.setItemId(this.item.getId());
        }
        return data;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        this.cardContainer = (ViewGroup) this.root.findViewById(R.id.loyalty_grid_container);
        Button stampCardButton = (Button) this.root.findViewById(R.id.stamp_card_button);
        TextView titleLabel = (TextView) this.root.findViewById(R.id.loyalty_title_label);
        this.seekbar = (SeekBar) this.root.findViewById(R.id.loyalty_seekbar);
        this.seekBarWidth = (int) (((float) AppCore.getInstance().getDeviceWidth()) * SEEKBAAR_WIDTH_FACTOR);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(this.seekBarWidth, -2);
        lp.gravity = 1;
        this.seekbar.setLayoutParams(lp);
        this.seekBarWidth = (int) (((float) this.seekBarWidth) * SEEKBAAR_INFO_WIDTH_FACTOR);
        LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(this.seekBarWidth, -2);
        lp2.gravity = 1;
        ((ViewGroup) this.root.findViewById(R.id.seekbar_information_layout)).setLayoutParams(lp2);
        this.loyaltyCurrentValue = (TextView) this.root.findViewById(R.id.loyalty_current_value);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), stampCardButton.getBackground());
        LayerDrawable ld = (LayerDrawable) this.seekbar.getProgressDrawable();
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ld.getDrawable(1));
        CommonUtils.overrideImageColor(-16777216, ld.getDrawable(0));
        ((TextView) this.root.findViewById(R.id.loyalty_description_label)).setTextColor(settings.getFeatureTextColor());
        titleLabel.setTextColor(settings.getFeatureTextColor());
        stampCardButton.setTextColor(settings.getButtonTextColor());
        stampCardButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (LoyaltyDetailFragment.this.nextCardView != null) {
                    LoyaltyDetailFragment.this.nextCardView.performClick();
                } else if (LoyaltyDetailFragment.this.areItemsApproved) {
                    LoyaltyDetailFragment.this.tryToRedeem();
                }
            }
        });
        this.bgUrl = getIntent().getStringExtra(AppConstants.BG_URL_EXTRA);
        titleLabel.setText(this.item.getTitle());
    }

    /* access modifiers changed from: protected */
    public void loadData() {
        LoyaltyItem.LoyaltyCardItem card;
        if (this.item.getCoupons() != null && !this.item.getCoupons().isEmpty()) {
            this.cardContainer.removeAllViews();
            int couponsSize = this.item.getCoupons().size();
            int rowSize = couponsSize % 5 == 0 ? couponsSize / 5 : (couponsSize / 5) + 1;
            for (int i = 0; i < rowSize; i++) {
                addRow(this.cardContainer, i, this.item.getCoupons());
            }
            this.appliedCoupons = 0;
            for (LoyaltyItem.LoyaltyCardItem card2 : this.item.getCoupons()) {
                if (card2.isApproved()) {
                    changeSeekbarProgress(false);
                }
            }
            if (!this.isNextCardFound && (card = this.item.getCoupons().get(couponsSize - 1)) != null && card.isApproved()) {
                this.areItemsApproved = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void tryToRedeem() {
        if (this.areItemsApproved) {
            showRedeemDialog();
        } else {
            Toast.makeText(getApplicationContext(), R.string.loyalty_warning_message, 1).show();
        }
    }

    private void addRow(ViewGroup cardContainer2, int row, List<LoyaltyItem.LoyaltyCardItem> coupons) {
        final AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        ViewGroup rowLayout = (ViewGroup) ViewUtils.loadLayout(getApplicationContext(), R.layout.loyalty_card_layout);
        int startIndex = row * 5;
        int endIndex = startIndex + 5 > coupons.size() ? coupons.size() : startIndex + 5;
        while (startIndex < endIndex) {
            ViewGroup columnLayout = (ViewGroup) ViewUtils.loadLayout(getApplicationContext(), R.layout.loyalty_card_item_layout);
            columnLayout.setPadding(0, 10, 0, 10);
            final ImageView columnImage = (ImageView) columnLayout.findViewById(R.id.loyalty_coupon_image);
            final LoyaltyItem.LoyaltyCardItem card = coupons.get(startIndex);
            if (!card.isApproved() && !this.isNextCardFound) {
                if (card.isLocked()) {
                    card.setLocked(false);
                }
                this.nextCardView = columnImage;
                this.isNextCardFound = true;
            }
            if (card.isApproved()) {
                columnImage.setBackgroundResource(R.drawable.loyalty_coupon_icon_active);
                CommonUtils.overrideImageColor(settings.getButtonBgColor(), columnImage.getBackground());
            }
            columnImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (!card.isLocked() && card.isApproved() && card.isLast()) {
                        boolean unused = LoyaltyDetailFragment.this.areItemsApproved = true;
                        LoyaltyDetailFragment.this.tryToRedeem();
                    } else if (!card.isLocked() && !card.isApproved()) {
                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(LoyaltyDetailFragment.this.getHoldActivity());
                        ViewGroup contentView = (ViewGroup) ViewUtils.loadLayout(LoyaltyDetailFragment.this.getApplicationContext(), R.layout.loyalty_dialog);
                        final EditText secretEditText = (EditText) contentView.findViewById(R.id.loyalty_secret_edittext);
                        alertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        alertBuilder.setPositiveButton(R.string.loyalty_dialog_yes_title, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String enteredSecretCode = secretEditText.getText().toString();
                                if (enteredSecretCode == null || !enteredSecretCode.equalsIgnoreCase(card.getCouponCode())) {
                                    Toast.makeText(LoyaltyDetailFragment.this.getApplicationContext(), R.string.loyalty_wrong_code_message, 1).show();
                                    return;
                                }
                                card.setApproved(true);
                                columnImage.setBackgroundResource(R.drawable.loyalty_coupon_icon_active);
                                CommonUtils.overrideImageColor(settings.getButtonBgColor(), columnImage.getBackground());
                                boolean unused = LoyaltyDetailFragment.this.isNextCardFound = false;
                                dialog.dismiss();
                                LoyaltyDetailFragment.this.loadData();
                            }
                        });
                        alertBuilder.setView(contentView);
                        alertBuilder.setTitle(R.string.loyalty_dialog_title);
                        alertBuilder.show();
                    }
                }
            });
            rowLayout.addView(columnLayout);
            startIndex++;
        }
        cardContainer2.addView(rowLayout);
    }

    private void changeSeekbarProgress(boolean shouldReset) {
        boolean shouldBeVisible;
        int i = 0;
        if (shouldReset) {
            this.appliedCoupons = 0;
        } else {
            this.appliedCoupons++;
        }
        int percentValue = Math.round((float) ((this.appliedCoupons * 100) / this.item.getCoupons().size()));
        this.seekbar.setProgress(percentValue);
        int textWidth = getResources().getDimensionPixelSize(R.dimen.loyalty_digit_box_width);
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) this.loyaltyCurrentValue.getLayoutParams();
        lp.setMargins(((this.seekBarWidth * percentValue) / 100) - (textWidth / 4), 0, 0, 0);
        this.loyaltyCurrentValue.setLayoutParams(lp);
        this.loyaltyCurrentValue.setText("" + percentValue);
        if (percentValue == 0 || percentValue == 100) {
            shouldBeVisible = false;
        } else {
            shouldBeVisible = true;
        }
        TextView textView = this.loyaltyCurrentValue;
        if (!shouldBeVisible) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    private void showRedeemDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getHoldActivity());
        alertBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                LoyaltyDetailFragment.this.showSuccessRedeemDialog();
            }
        });
        alertBuilder.setTitle(R.string.redeem_coupon);
        alertBuilder.show();
    }

    /* access modifiers changed from: private */
    public void showSuccessRedeemDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getHoldActivity());
        alertBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                LoyaltyDetailFragment.this.setInitialStateForCoupon();
            }
        });
        alertBuilder.setTitle(String.format(getString(R.string.loyalty_congrats_message), this.item.getTitle()));
        alertBuilder.show();
    }

    /* access modifiers changed from: private */
    public void setInitialStateForCoupon() {
        this.nextCardView = null;
        changeSeekbarProgress(true);
        if (this.item.getCoupons() != null) {
            for (LoyaltyItem.LoyaltyCardItem card : this.item.getCoupons()) {
                card.setLocked(true);
                card.setApproved(false);
                this.isNextCardFound = false;
                this.areItemsApproved = false;
            }
            loadData();
        }
    }

    private void synchronizeData(LoyaltyItem storedItem, LoyaltyItem newItem) {
        if (newItem != null && storedItem != null) {
            List<LoyaltyItem.LoyaltyCardItem> newCards = newItem.getCoupons();
            List<LoyaltyItem.LoyaltyCardItem> storedCards = storedItem.getCoupons();
            if (storedCards != null && !storedCards.isEmpty() && newCards != null && !newCards.isEmpty()) {
                for (LoyaltyItem.LoyaltyCardItem newCard : newCards) {
                    for (LoyaltyItem.LoyaltyCardItem storedCard : storedCards) {
                        if (newCard.getCouponId().equalsIgnoreCase(storedCard.getCouponId())) {
                            newCard.setApproved(storedCard.isApproved());
                            newCard.setLocked(storedCard.isLocked());
                        }
                    }
                }
            }
        }
    }
}
