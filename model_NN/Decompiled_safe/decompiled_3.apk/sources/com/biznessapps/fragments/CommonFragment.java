package com.biznessapps.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.api.DataSource;
import com.biznessapps.api.UnModalAsyncTask;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.images.NewImageManager;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.AppSettings;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.utils.google.caching.ImageWorker;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CommonFragment extends Fragment implements AppConstants {
    protected static final int FIRST_ITEM_INDEX = 0;
    protected String bgUrl;
    protected String bitmapUrl;
    private String fragmentName;
    protected boolean hasResultError;
    /* access modifiers changed from: protected */
    public String itemId;
    protected Button rightNavigationButton;
    /* access modifiers changed from: protected */
    public ViewGroup root;
    protected String sectionId;
    protected String tabId;
    protected TextView titleTextView;

    public String getFragmentName() {
        return this.fragmentName;
    }

    public void setFragmentName(String fragmentName2) {
        this.fragmentName = fragmentName2;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Window window = activity.getWindow();
        if (window != null) {
            window.setSoftInputMode(3);
        }
    }

    public void onResume() {
        super.onResume();
        boolean isAdsShown = ViewUtils.showAdsIfNeeded(getHoldActivity(), this.root, false);
        if (hasTitleBar()) {
            ViewUtils.showTitleBar((ViewGroup) this.root.findViewById(R.id.tab_title_container), getIntent(), isAdsShown);
        }
        ViewUtils.showMailingListPropmt(getHoldActivity());
        ViewUtils.showOfflineCachingPropmt(getHoldActivity());
        loadBgUrl();
    }

    public void onStop() {
        super.onStop();
        if (StringUtils.isNotEmpty(this.bgUrl) && getViewForBg() != null) {
            getViewForBg().setBackgroundDrawable(null);
            ImageWorker.cancelWork(getViewForBg());
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (!AppCore.getInstance().isInitialized()) {
            AppCore.getInstance().init(getApplicationContext());
        }
        this.root = (ViewGroup) inflater.inflate(getLayoutId(), (ViewGroup) null);
        this.rightNavigationButton = (Button) this.root.findViewById(R.id.right_navigation_button);
        if (this.rightNavigationButton != null) {
            CommonUtils.overrideMediumButtonColor(AppCore.getInstance().getUiSettings().getNavigationBarColor(), this.rightNavigationButton.getBackground());
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = new AnalyticItem();
        AppSettings settings = AppCore.getInstance().getAppSettings();
        data.setContext(getApplicationContext());
        data.setAccountId(settings.getGaAccountId());
        data.setAppId(settings.getAppId());
        data.setTabId(getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        return data;
    }

    private void loadBgUrl() {
        if (StringUtils.isNotEmpty(this.bgUrl) && getViewForBg() != null) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(this.bgUrl, getViewForBg());
        }
    }

    /* access modifiers changed from: protected */
    public void onPreBgLoading() {
    }

    /* access modifiers changed from: protected */
    public void onPostBgLoading() {
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return null;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return 0;
    }

    public Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

    public Intent getIntent() {
        return getActivity().getIntent();
    }

    public CommonFragmentActivity getHoldActivity() {
        return (CommonFragmentActivity) getActivity();
    }

    public AppCore getAppCore() {
        return AppCore.getInstance();
    }

    public CachingManager cacher() {
        return AppCore.getInstance().getCachingManager();
    }

    public AppCore.UiSettings getUiSettings() {
        return AppCore.getInstance().getUiSettings();
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root2) {
    }

    /* access modifiers changed from: protected */
    public void loadData() {
        Activity holdActivity = getHoldActivity();
        if (holdActivity != null) {
            preDataLoading(holdActivity);
            if (canUseCachedData()) {
                updateControlsWithData(holdActivity);
            } else {
                new LoadDataTask(holdActivity, ViewUtils.getProgressBar(getApplicationContext()), getViewsRef()).execute(new Map[0]);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void loadPostData(Map<String, String> params) {
        Activity holdActivity = getHoldActivity();
        if (holdActivity != null) {
            preDataLoading(holdActivity);
            if (canUseCachedData()) {
                updateControlsWithData(holdActivity);
                return;
            }
            new LoadDataTask(holdActivity, ViewUtils.getProgressBar(getApplicationContext()), getViewsRef()).execute(new Map[]{params});
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return true;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return "";
    }

    /* access modifiers changed from: protected */
    public String addOffsetIfNeeded() {
        return "";
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void handleInBackground() {
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        this.bgUrl = defineBgUrl();
        loadBgUrl();
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.itemId = holdActivity.getIntent().getStringExtra(AppConstants.ITEM_ID);
        this.sectionId = holdActivity.getIntent().getStringExtra(AppConstants.SECTION_ID);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return false;
    }

    /* access modifiers changed from: protected */
    public List<WeakReference<View>> getViewsRef() {
        return new ArrayList<>();
    }

    /* access modifiers changed from: protected */
    public boolean showToastIfDataFailed() {
        return true;
    }

    private class LoadDataTask extends UnModalAsyncTask<Map<String, String>, Void, Boolean> {
        private long loadingTime;

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ Object doInBackground(Object[] x0) {
            return doInBackground((Map<String, String>[]) ((Map[]) x0));
        }

        public LoadDataTask(Activity activity, View progressBar, List<WeakReference<View>> refOfViews) {
            super(activity, progressBar, refOfViews);
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Map<String, String>... params) {
            String url = CommonFragment.this.getRequestUrl() + CommonFragment.this.addOffsetIfNeeded();
            if (AppCore.getInstance().isTablet()) {
                url = url + ServerConstants.TABLET_PARAM;
            }
            boolean isCorrectData = CommonFragment.this.tryParseData(DataSource.getInstance().getData(url, params));
            if (this.activity != null && isCorrectData) {
                CommonFragment.this.handleInBackground();
            }
            return Boolean.valueOf(isCorrectData);
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            this.loadingTime = System.currentTimeMillis();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean isCorrectData) {
            super.onPostExecute((Object) isCorrectData);
            if (isCorrectData.booleanValue()) {
                if (this.activity != null && (this.activity instanceof CommonFragmentActivity)) {
                    ((CommonFragmentActivity) this.activity).getProgressBarContainer().removeAllViews();
                    CommonFragment.this.updateControlsWithData(this.activity);
                }
            } else if (this.activity != null && CommonFragment.this.showToastIfDataFailed()) {
                Toast.makeText(this.activity.getApplicationContext(), R.string.data_loading_failure, 1).show();
            }
            if (this.activity != null) {
                this.loadingTime = System.currentTimeMillis() - this.loadingTime;
                CommonUtils.sendTimingEvent(this.activity.getApplicationContext(), this.activity.getIntent().getStringExtra(AppConstants.TAB_FRAGMENT_EXTRA), this.loadingTime);
            }
        }

        /* access modifiers changed from: protected */
        public void placeProgressBar() {
            if (this.activity != null && (this.activity instanceof CommonFragmentActivity)) {
                ((CommonFragmentActivity) this.activity).getProgressBarContainer().addView(this.progressBar);
            }
        }
    }

    /* access modifiers changed from: protected */
    public NewImageManager getNewImageManager() {
        return AppCore.getInstance().getNewImageManager();
    }

    /* access modifiers changed from: protected */
    public BitmapDownloader getBitmapDownloader() {
        return AppCore.getInstance().getNewImageManager().getBitmapDownloader();
    }
}
