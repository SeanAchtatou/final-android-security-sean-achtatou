package com.biznessapps.fragments.news;

import com.biznessapps.model.CommonListEntity;
import java.util.Date;

public class SearchItem extends CommonListEntity {
    private static final long serialVersionUID = 2420184308003921785L;
    private String date;
    private String link;
    private String name;
    private String text;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getText() {
        return this.text;
    }

    public Date getDateTime() {
        try {
            return new Date(this.date);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link2) {
        this.link = link2;
    }
}
