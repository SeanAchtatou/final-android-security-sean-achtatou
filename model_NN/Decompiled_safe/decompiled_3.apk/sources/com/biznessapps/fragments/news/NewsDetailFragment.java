package com.biznessapps.fragments.news;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.delegate.TellFriendDelegate;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;

public class NewsDetailFragment extends CommonFragment {
    private TextView dateView;
    private TextView descriptionView;
    /* access modifiers changed from: private */
    public ImageView favoriteButton;
    private ImageView homeButton;
    private ImageView newsIcon;
    private ImageView shareButton;
    private TextView titleView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        initData();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        SearchItem item = (SearchItem) getIntent().getSerializableExtra(AppConstants.NEWS_ITEM_EXTRA);
        if (item != null) {
            data.setItemId(item.getId());
        }
        return data;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.news_detail_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.dateView = (TextView) root.findViewById(R.id.news_date_label);
        this.titleView = (TextView) root.findViewById(R.id.news_title_label);
        this.descriptionView = (TextView) root.findViewById(R.id.news_description_label);
        this.shareButton = (ImageView) root.findViewById(R.id.share_news_button);
        this.favoriteButton = (ImageView) root.findViewById(R.id.favorite_news_button);
        this.homeButton = (ImageView) root.findViewById(R.id.home_news_button);
        this.newsIcon = (ImageView) root.findViewById(R.id.news_icon);
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.dateView.getBackground());
        CommonUtils.overrideImageColor(-1, this.shareButton.getDrawable());
        CommonUtils.overrideImageColor(-1, this.favoriteButton.getDrawable());
        CommonUtils.overrideImageColor(-1, this.homeButton.getDrawable());
        CommonUtils.customizeFooterNavigationBar(root.findViewById(R.id.buttons_container));
        this.dateView.setTextColor(settings.getButtonTextColor());
        this.titleView.setTextColor(settings.getFeatureTextColor());
        this.descriptionView.setTextColor(settings.getFeatureTextColor());
        ViewUtils.updateMusicBarBottomMargin(getHoldActivity().getMusicDelegate(), (int) getResources().getDimension(R.dimen.footer_bar_height));
        ViewUtils.setGlobalBackgroundColor(root.findViewById(R.id.news_root_container));
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root.findViewById(R.id.news_root_container);
    }

    private void initData() {
        final SearchItem item = (SearchItem) getIntent().getSerializableExtra(AppConstants.NEWS_ITEM_EXTRA);
        if (item != null) {
            this.dateView.setText(Html.fromHtml(item.getDate()));
            this.titleView.setText(Html.fromHtml(item.getTitle()));
            this.descriptionView.setText(Html.fromHtml(item.getText()));
            this.homeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (StringUtils.isNotEmpty(item.getLink())) {
                        Intent intent = new Intent(NewsDetailFragment.this.getApplicationContext(), SingleFragmentActivity.class);
                        intent.putExtra(AppConstants.URL, item.getLink());
                        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
                        intent.putExtra(AppConstants.TAB_ID, NewsDetailFragment.this.getHoldActivity().getTabId());
                        NewsDetailFragment.this.startActivity(intent);
                    }
                }
            });
            if (StringUtils.isEmpty(item.getLink())) {
                this.homeButton.setVisibility(4);
            }
            final ViewGroup tellFriendContent = (ViewGroup) this.root.findViewById(R.id.tell_friends_content);
            this.shareButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    TellFriendDelegate.openFriendContent(NewsDetailFragment.this.getApplicationContext(), tellFriendContent);
                }
            });
            TellFriendDelegate.initTellFriends(getHoldActivity(), tellFriendContent);
            this.favoriteButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (NewsFragment.hasFavorites(item)) {
                        NewsFragment.updateFavorites(item, false);
                        NewsDetailFragment.this.favoriteButton.setImageResource(R.drawable.favorite_icon);
                        ViewUtils.showShortToast(NewsDetailFragment.this.getApplicationContext(), R.string.favorite_news_removed);
                        return;
                    }
                    NewsFragment.updateFavorites(item, true);
                    NewsDetailFragment.this.favoriteButton.setImageResource(R.drawable.favorite_icon_selected);
                    ViewUtils.showShortToast(NewsDetailFragment.this.getApplicationContext(), R.string.favorite_news_added);
                }
            });
            String url = item.getImage();
            if (StringUtils.isNotEmpty(url)) {
                if (!url.contains(AppConstants.HTTP_PREFIX) && !url.contains(AppConstants.HTTPS_PREFIX)) {
                    url = AppConstants.HTTP_PREFIX + url;
                }
                AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadGalleryImage(url, this.newsIcon);
            }
            if (NewsFragment.hasFavorites(item)) {
                this.favoriteButton.setImageResource(R.drawable.favorite_icon_selected);
            }
        }
        if (StringUtils.isEmpty(this.bgUrl)) {
            this.bgUrl = getIntent().getStringExtra(AppConstants.BG_URL_EXTRA);
        }
    }
}
