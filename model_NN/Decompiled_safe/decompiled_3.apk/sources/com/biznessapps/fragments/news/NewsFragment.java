package com.biznessapps.fragments.news;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.HttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AppSettings;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.DateUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.widgets.RefreshableListView;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class NewsFragment extends CommonListFragment<SearchItem> {
    private static final String FACEBOOK_URL_PATTERN = "https://graph.facebook.com/search?q=%s&type=post";
    private static final String TWITTER_URL_PATTERN = "https://api.twitter.com/1.1/search/tweets.json?q=%s&count=50";
    public static List<SearchItem> favoriteSearchList = new LinkedList();
    /* access modifiers changed from: private */
    public ImageButton favoriteNewsButton;
    private Drawable favoriteNewsDrawable;
    /* access modifiers changed from: private */
    public ImageButton googleSearchButton;
    private Drawable googleSearchDrawable;
    /* access modifiers changed from: private */
    public List<SearchItem> googleSearchList = new LinkedList();
    /* access modifiers changed from: private */
    public boolean isGoogleSearchUsed = true;
    /* access modifiers changed from: private */
    public String lastQuery;
    /* access modifiers changed from: private */
    public NewsSettings newsSettings;
    /* access modifiers changed from: private */
    public GoogleNewsReader reader;
    private SearchAsyncTask searchAsyncTask;
    /* access modifiers changed from: private */
    public EditText searchEditText;
    /* access modifiers changed from: private */
    public ImageButton twitterSearchButton;
    private Drawable twitterSearchDrawable;
    /* access modifiers changed from: private */
    public List<SearchItem> twitterSearchList = new LinkedList();

    public static boolean hasFavorites(SearchItem newItem) {
        for (SearchItem item : favoriteSearchList) {
            if (item.getName().equals(newItem.getName()) && item.getDate().equals(newItem.getDate()) && item.getText().equals(newItem.getText())) {
                return true;
            }
        }
        return false;
    }

    public static void updateFavorites(SearchItem itemToChange, boolean isNew) {
        if (isNew) {
            favoriteSearchList.add(itemToChange);
            return;
        }
        Iterator i$ = favoriteSearchList.iterator();
        while (true) {
            if (!i$.hasNext()) {
                break;
            }
            SearchItem item = i$.next();
            if (item.getName().equals(itemToChange.getName()) && item.getDate().equals(itemToChange.getDate()) && item.getText().equals(itemToChange.getText())) {
                itemToChange = item;
                break;
            }
        }
        favoriteSearchList.remove(itemToChange);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.news_layout, (ViewGroup) null);
        initViews(this.root);
        initListeners();
        loadData();
        loadDrawables();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.news_layout;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.NEWS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String data) {
        this.newsSettings = JsonParserUtils.getNewsSettings(data);
        return this.newsSettings != null;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        int i = 0;
        super.updateControlsWithData(holdActivity);
        this.googleSearchButton.setVisibility(this.newsSettings.isGoogleActive() ? 0 : 8);
        ImageButton imageButton = this.twitterSearchButton;
        if (!this.newsSettings.isTwitterActive()) {
            i = 8;
        }
        imageButton.setVisibility(i);
        if (this.newsSettings.isGoogleActive()) {
            this.searchEditText.setText(this.newsSettings.getGoogleSearchKey());
            this.googleSearchButton.performClick();
        } else if (this.newsSettings.isTwitterActive()) {
            this.searchEditText.setText(this.newsSettings.getTwitterSearchKey());
            this.twitterSearchButton.performClick();
            if (this.googleSearchDrawable != null) {
                this.twitterSearchButton.setBackgroundDrawable(this.googleSearchDrawable);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.newsSettings != null) {
            return this.newsSettings.getBackground();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root.findViewById(R.id.list_view_container);
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.searchEditText = (EditText) root.findViewById(R.id.search_textview);
        this.googleSearchButton = (ImageButton) root.findViewById(R.id.google_search_button);
        this.twitterSearchButton = (ImageButton) root.findViewById(R.id.twitter_search_button);
        this.favoriteNewsButton = (ImageButton) root.findViewById(R.id.favorite_news_button);
        this.listView = (RefreshableListView) root.findViewById(R.id.search_results_listview);
        ViewUtils.setGlobalBackgroundColor(this.listView);
    }

    private void initListeners() {
        this.googleSearchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsFragment.this.updateButtonSelection(NewsFragment.this.googleSearchButton);
                NewsFragment.this.searchEditText.setHint(NewsFragment.this.getString(R.string.keyword_hint));
                NewsFragment.this.searchEditText.setText(NewsFragment.this.newsSettings.getGoogleSearchKey());
                boolean unused = NewsFragment.this.isGoogleSearchUsed = true;
                if (NewsFragment.this.googleSearchList.isEmpty()) {
                    String unused2 = NewsFragment.this.lastQuery = NewsFragment.this.searchEditText.getText().toString();
                    NewsFragment.this.search(NewsFragment.this.lastQuery);
                    return;
                }
                NewsFragment.this.listView.setAdapter((ListAdapter) new SearchAdapter(NewsFragment.this.getApplicationContext(), NewsFragment.this.googleSearchList));
            }
        });
        this.twitterSearchButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsFragment.this.updateButtonSelection(NewsFragment.this.twitterSearchButton);
                NewsFragment.this.searchEditText.setHint(NewsFragment.this.getString(R.string.hashtag_hint));
                NewsFragment.this.searchEditText.setText(NewsFragment.this.newsSettings.getTwitterSearchKey());
                boolean unused = NewsFragment.this.isGoogleSearchUsed = false;
                if (NewsFragment.this.twitterSearchList.isEmpty()) {
                    String unused2 = NewsFragment.this.lastQuery = NewsFragment.this.searchEditText.getText().toString();
                    NewsFragment.this.search(NewsFragment.this.lastQuery);
                    return;
                }
                NewsFragment.this.listView.setAdapter((ListAdapter) new SearchAdapter(NewsFragment.this.getApplicationContext(), NewsFragment.this.twitterSearchList));
            }
        });
        this.favoriteNewsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NewsFragment.this.updateButtonSelection(NewsFragment.this.favoriteNewsButton);
                NewsFragment.this.listView.setAdapter((ListAdapter) new SearchAdapter(NewsFragment.this.getApplicationContext(), NewsFragment.favoriteSearchList));
            }
        });
        this.searchEditText.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                String unused = NewsFragment.this.lastQuery = NewsFragment.this.searchEditText.getText().toString();
                NewsFragment.this.search(NewsFragment.this.lastQuery);
            }
        }));
        this.listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (totalItemCount == 0 || firstVisibleItem + visibleItemCount == totalItemCount) {
                }
            }
        });
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
                /*
                    r2 = this;
                    android.widget.Adapter r1 = r3.getAdapter()
                    java.lang.Object r0 = r1.getItem(r5)
                    com.biznessapps.fragments.news.SearchItem r0 = (com.biznessapps.fragments.news.SearchItem) r0
                    com.biznessapps.fragments.news.NewsFragment r1 = com.biznessapps.fragments.news.NewsFragment.this
                    r1.openNewsItem(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.news.NewsFragment.AnonymousClass6.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        });
    }

    public void onResume() {
        super.onResume();
        if (this.listView.getAdapter() != null && (this.listView.getAdapter() instanceof SearchAdapter)) {
            ((SearchAdapter) this.listView.getAdapter()).notifyDataSetChanged();
        }
    }

    private void loadDrawables() {
        if (this.googleSearchDrawable == null || this.twitterSearchDrawable == null || this.favoriteNewsDrawable == null) {
            AppSettings settings = AppCore.getInstance().getAppSettings();
            LayerDrawable activeStateRight = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_active_right);
            LayerDrawable passiveStateRight = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_passive_right);
            LayerDrawable activeStateCenter = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_active_center);
            LayerDrawable passiveStateCenter = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_passive_center);
            LayerDrawable activeStateLeft = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_active_left);
            LayerDrawable passiveStateLeft = CommonUtils.getCompositeDrawable(getResources(), ViewUtils.getColor(settings.getNavigBarColor()), R.drawable.button_toggle_passive_left);
            this.favoriteNewsDrawable = CommonUtils.getButtonDrawable(activeStateRight, passiveStateRight);
            this.twitterSearchDrawable = CommonUtils.getButtonDrawable(activeStateCenter, passiveStateCenter);
            this.googleSearchDrawable = CommonUtils.getButtonDrawable(activeStateLeft, passiveStateLeft);
        }
        this.favoriteNewsButton.setBackgroundDrawable(this.favoriteNewsDrawable);
        this.twitterSearchButton.setBackgroundDrawable(this.twitterSearchDrawable);
        this.googleSearchButton.setBackgroundDrawable(this.googleSearchDrawable);
    }

    /* access modifiers changed from: private */
    public void updateButtonSelection(ImageButton buttonToSelect) {
        this.googleSearchButton.setSelected(false);
        this.twitterSearchButton.setSelected(false);
        this.favoriteNewsButton.setSelected(false);
        buttonToSelect.setSelected(true);
    }

    /* access modifiers changed from: private */
    public void openNewsItem(SearchItem item) {
        if (item != null) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.NEWS_DETAIL_FRAGMENT);
            intent.putExtra(AppConstants.NEWS_ITEM_EXTRA, item);
            intent.putExtra(AppConstants.BG_URL_EXTRA, this.bgUrl);
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void search(String query) {
        if (this.isGoogleSearchUsed) {
            this.googleSearchList.clear();
        } else {
            this.twitterSearchList.clear();
        }
        if (this.listView.getAdapter() != null && (this.listView.getAdapter() instanceof SearchAdapter)) {
            ((SearchAdapter) this.listView.getAdapter()).clear();
        }
        this.searchAsyncTask = new SearchAsyncTask();
        this.searchAsyncTask.execute(query);
    }

    private class SearchAsyncTask extends AsyncTask<String, Void, List<SearchItem>> {
        private SearchAsyncTask() {
        }

        /* access modifiers changed from: protected */
        public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
            onPostExecute((List<SearchItem>) ((List) x0));
        }

        /* access modifiers changed from: protected */
        public List<SearchItem> doInBackground(String... params) {
            List<SearchItem> items = new ArrayList<>();
            String query = params[0];
            if (NewsFragment.this.isGoogleSearchUsed) {
                if (NewsFragment.this.reader != null) {
                    NewsFragment.this.reader.close();
                }
                GoogleNewsReader unused = NewsFragment.this.reader = new GoogleNewsReader(query);
                try {
                    NewsFragment.this.reader.prepare();
                    for (int i = 0; i < 40; i++) {
                        SearchItem item = NewsFragment.this.reader.next();
                        if (item != null) {
                            items.add(item);
                        }
                    }
                    return items;
                } catch (Exception e) {
                    Log.e("News Tab", "GoogleNewsReader.prepare() error", e);
                    return items;
                }
            } else {
                return JsonParserUtils.parseTwitterSearchList(HttpUtils.getTwitterData(String.format(NewsFragment.TWITTER_URL_PATTERN, URLEncoder.encode(query)), AppCore.getInstance().getBearerAccessToken()));
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(List<SearchItem> items) {
            List<SearchItem> resultList;
            super.onPostExecute((Object) items);
            Activity activity = NewsFragment.this.getActivity();
            if (items != null && !items.isEmpty() && activity != null) {
                if (NewsFragment.this.isGoogleSearchUsed) {
                    resultList = NewsFragment.this.googleSearchList;
                } else {
                    resultList = NewsFragment.this.twitterSearchList;
                }
                Collections.sort(items, new Comparator<SearchItem>() {
                    public int compare(SearchItem firstObj, SearchItem secondObj) {
                        if (firstObj.getDateTime() == null || secondObj.getDateTime() == null) {
                            return 0;
                        }
                        if (firstObj.getDateTime().getTime() < secondObj.getDateTime().getTime()) {
                            return 1;
                        }
                        if (firstObj.getDateTime().getTime() != secondObj.getDateTime().getTime()) {
                            return -1;
                        }
                        return 0;
                    }
                });
                for (SearchItem item : items) {
                    resultList.add((SearchItem) NewsFragment.this.getWrappedItem(item, resultList));
                }
                NewsFragment.this.listView.setAdapter((ListAdapter) new SearchAdapter(activity.getApplicationContext(), resultList));
            }
        }
    }

    private class SearchAdapter extends AbstractAdapter<SearchItem> {
        private LayoutInflater inflater;
        private List<SearchItem> items;

        public SearchAdapter(Context context, List<SearchItem> items2) {
            super(context, items2, R.layout.news_item_layout);
            this.items = items2;
            this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = this.inflater.inflate(R.layout.news_item_layout, (ViewGroup) null);
                CommonUtils.overrideImageColor(this.settings.getButtonTextColor(), ((TextView) convertView.findViewById(R.id.date_text_view)).getCompoundDrawables()[0]);
            }
            SearchItem item = this.items.get(position);
            if (item != null) {
                TextView titleView = (TextView) convertView.findViewById(R.id.title_text_view);
                TextView descriptionView = (TextView) convertView.findViewById(R.id.description_text_view);
                TextView dateView = (TextView) convertView.findViewById(R.id.date_text_view);
                TextView nameView = (TextView) convertView.findViewById(R.id.name_text_view);
                ImageView iconView = (ImageView) convertView.findViewById(R.id.row_icon);
                if (StringUtils.isNotEmpty(item.getTitle())) {
                    titleView.setText(Html.fromHtml(item.getTitle()));
                }
                if (StringUtils.isNotEmpty(item.getName())) {
                    nameView.setText(Html.fromHtml(item.getName()));
                }
                if (StringUtils.isNotEmpty(item.getText())) {
                    descriptionView.setText(Html.fromHtml(item.getText()));
                }
                if (StringUtils.isNotEmpty(item.getDate())) {
                    dateView.setText(DateUtils.getStringInterval(NewsFragment.this.getApplicationContext(), item.getDate()));
                }
                String url = item.getImage();
                if (StringUtils.isNotEmpty(url)) {
                    if (!url.contains(AppConstants.HTTP_PREFIX) && !url.contains(AppConstants.HTTPS_PREFIX)) {
                        url = AppConstants.HTTP_PREFIX + url;
                    }
                    this.imageFetcher.loadImage(url, iconView);
                    iconView.setVisibility(0);
                } else {
                    iconView.setVisibility(8);
                }
                if (item.hasColor()) {
                    convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                    setTextColorToView(item.getItemTextColor(), titleView, descriptionView, dateView, nameView);
                }
            }
            return convertView;
        }
    }
}
