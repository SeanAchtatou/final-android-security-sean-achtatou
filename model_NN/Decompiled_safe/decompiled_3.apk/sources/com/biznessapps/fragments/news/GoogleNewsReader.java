package com.biznessapps.fragments.news;

import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import org.xmlpull.v1.XmlPullParserException;

public class GoogleNewsReader {
    private static final String SEARCH_URL_PATTERN = "https://news.google.com/news/feeds?q=%s&output=rss&num=50";
    private InputStream is;
    private RssPullParser parser;
    private String query;

    public GoogleNewsReader(String query2) {
        this.query = query2;
    }

    public void prepare() throws XmlPullParserException, IOException {
        this.is = new URL(String.format(SEARCH_URL_PATTERN, URLEncoder.encode(this.query))).openConnection().getInputStream();
        this.parser = new RssPullParser(this.is);
        this.parser.prepare();
    }

    public SearchItem next() throws XmlPullParserException, IOException {
        return this.parser.next();
    }

    public void close() {
        try {
            this.is.close();
        } catch (Exception e) {
            Log.w("GoogleNewsReader", "InputStream.close() exception");
        }
    }
}
