package com.biznessapps.fragments.news;

public class NewsSettings {
    private String background;
    private String googleSearchKey;
    private boolean isGoogleActive = true;
    private boolean isTwitterActive = true;
    private String twitterSearchKey;

    public String getGoogleSearchKey() {
        return this.googleSearchKey;
    }

    public void setGoogleSearchKey(String googleSearchKey2) {
        this.googleSearchKey = googleSearchKey2;
    }

    public String getTwitterSearchKey() {
        return this.twitterSearchKey;
    }

    public void setTwitterSearchKey(String twitterSearchKey2) {
        this.twitterSearchKey = twitterSearchKey2;
    }

    public String getBackground() {
        return this.background;
    }

    public void setBackground(String background2) {
        this.background = background2;
    }

    public boolean isGoogleActive() {
        return this.isGoogleActive;
    }

    public void setGoogleActive(boolean isGoogleActive2) {
        this.isGoogleActive = isGoogleActive2;
    }

    public boolean isTwitterActive() {
        return this.isTwitterActive;
    }

    public void setTwitterActive(boolean isTwitterActive2) {
        this.isTwitterActive = isTwitterActive2;
    }
}
