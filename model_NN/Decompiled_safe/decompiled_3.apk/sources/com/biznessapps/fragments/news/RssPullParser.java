package com.biznessapps.fragments.news;

import android.util.Log;
import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class RssPullParser {
    static final String CHANNEL = "channel";
    static final String DESCRIPTION = "description";
    static final String ITEM = "item";
    static final String LINK = "link";
    static final String PUB_DATE = "pubDate";
    static final String TAG = "RssPullParser";
    static final String TITLE = "title";
    private SearchItem currentMessage;
    private InputStream is;
    private XmlPullParser parser;

    public RssPullParser(InputStream is2) {
        this.is = is2;
    }

    public void prepare() throws XmlPullParserException, IOException {
        this.parser = Xml.newPullParser();
        this.parser.setInput(this.is, null);
        int eventType = this.parser.getEventType();
        boolean done = false;
        while (eventType != 1 && !done) {
            switch (eventType) {
                case 2:
                    if (!this.parser.getName().equalsIgnoreCase("item")) {
                        break;
                    } else {
                        this.currentMessage = new SearchItem();
                        done = true;
                        Log.d(TAG, "first '<item>' found");
                        break;
                    }
                case 3:
                    if (!this.parser.getName().equalsIgnoreCase(CHANNEL)) {
                        break;
                    } else {
                        done = true;
                        break;
                    }
            }
            eventType = this.parser.next();
        }
    }

    public SearchItem next() throws XmlPullParserException, IOException {
        SearchItem result = null;
        int eventType = this.parser.getEventType();
        boolean done = false;
        while (eventType != 1 && !done) {
            switch (eventType) {
                case 2:
                    String name = this.parser.getName();
                    if (!"item".equalsIgnoreCase(name)) {
                        if (this.currentMessage != null) {
                            if (!LINK.equalsIgnoreCase(name)) {
                                if (!"description".equalsIgnoreCase(name)) {
                                    if (!"title".equalsIgnoreCase(name)) {
                                        if (!PUB_DATE.equalsIgnoreCase(name)) {
                                            break;
                                        } else {
                                            this.currentMessage.setDate(this.parser.nextText());
                                            break;
                                        }
                                    } else {
                                        this.currentMessage.setTitle(this.parser.nextText());
                                        break;
                                    }
                                } else {
                                    parseDescriptionHtml(this.parser.nextText(), this.currentMessage);
                                    break;
                                }
                            } else {
                                this.currentMessage.setLink(this.parser.nextText());
                                break;
                            }
                        } else {
                            break;
                        }
                    } else {
                        Log.d(TAG, "next '<item>' found");
                        this.currentMessage = new SearchItem();
                        done = true;
                        break;
                    }
                case 3:
                    String name2 = this.parser.getName();
                    if (!"item".equalsIgnoreCase(name2) || this.currentMessage == null) {
                        if (!CHANNEL.equalsIgnoreCase(name2)) {
                            break;
                        } else {
                            done = true;
                            break;
                        }
                    } else {
                        result = this.currentMessage;
                        this.currentMessage = null;
                        break;
                    }
            }
            eventType = this.parser.next();
        }
        return result;
    }

    private void parseDescriptionHtml(String html, SearchItem item) {
        if (html == null) {
            Log.w(TAG, "description is null");
            return;
        }
        int index = html.indexOf("<td ", 0);
        if (index < 0) {
            Log.w(TAG, "incorrect description structure");
            return;
        }
        int index2 = html.indexOf("<td ", index + 1);
        if (index2 < 0) {
            Log.w(TAG, "incorrect description structure");
            return;
        }
        int index3 = html.indexOf("</div><div ", index2 + 1);
        if (index3 < 0) {
            Log.w(TAG, "incorrect description structure");
            return;
        }
        int index4 = html.indexOf("<b>", index3 + 1);
        if (index4 < 0) {
            Log.w(TAG, "incorrect description structure");
            return;
        }
        int startIndex = index4;
        int index5 = html.indexOf("<a ", index4 + 1);
        if (index5 < 0) {
            Log.w(TAG, "incorrect description structure");
            return;
        }
        int stopIndex = html.lastIndexOf("<br />", index5);
        if (index5 < 0) {
            Log.w(TAG, "incorrect description structure");
            return;
        }
        String text = new String(html.substring(startIndex, stopIndex));
        int index6 = text.indexOf(">", text.indexOf("<font", text.indexOf("<font", index5)));
        int endIndex = text.indexOf("</font>", index6);
        String name = text.substring(index6 + 1, endIndex);
        int index7 = text.indexOf("<font");
        String text2 = text.substring(endIndex);
        String text3 = text2.substring(text2.indexOf("<font"));
        if (html.contains("<img src=\"")) {
            int urlStartIndex = html.indexOf("<img src=\"") + 12;
            item.setImage(html.substring(urlStartIndex, html.indexOf("\"", urlStartIndex)));
        }
        item.setText(text3);
        item.setName(name);
    }
}
