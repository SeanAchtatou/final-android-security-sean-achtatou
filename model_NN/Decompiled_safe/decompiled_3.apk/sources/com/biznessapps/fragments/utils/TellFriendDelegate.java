package com.biznessapps.fragments.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import com.biznessapps.activities.TwitterLoginActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;

public class TellFriendDelegate implements AppConstants {
    public static void initTellFriends(Activity activity, ViewGroup root) {
        initTellFriends(activity, root, null, null);
    }

    public static void initTellFriends(final Activity activity, ViewGroup root, String sharingText, String sharingUrl) {
        final Context context = activity.getApplicationContext();
        final ViewGroup viewGroup = root;
        ((Button) root.findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TellFriendDelegate.closeFriendContent(activity.getApplicationContext(), viewGroup);
            }
        });
        final String str = sharingText;
        final String str2 = sharingUrl;
        ((Button) root.findViewById(R.id.share_by_email_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String emailBodyFormat = context.getString(R.string.email_body_format);
                String emailSubjectFormat = context.getString(R.string.email_subject_format);
                String combinedUrl = String.format(emailBodyFormat, context.getString(R.string.app_name)) + AppConstants.MARKET_TEMPLATE_URL + context.getPackageName();
                if (StringUtils.isNotEmpty(str)) {
                    combinedUrl = str + str2;
                }
                ViewUtils.email(activity, null, String.format(emailSubjectFormat, context.getString(R.string.app_name)), combinedUrl);
            }
        });
        final String str3 = sharingText;
        final String str4 = sharingUrl;
        final Activity activity2 = activity;
        final ViewGroup viewGroup2 = root;
        ((Button) root.findViewById(R.id.share_on_facebook_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String combinedUrl;
                String emailBodyFormat = context.getString(R.string.email_body_format);
                if (StringUtils.isNotEmpty(str3)) {
                    combinedUrl = String.format(AppConstants.FACEBOOK_SHARE_URL_FORMAT, str3, str4);
                } else {
                    combinedUrl = String.format(AppConstants.FACEBOOK_SHARE_URL_FORMAT, String.format(emailBodyFormat, context.getString(R.string.app_name)), ((String) AppConstants.MARKET_TEMPLATE_URL) + context.getPackageName());
                }
                TellFriendDelegate.shareViaFacebook(activity2, combinedUrl, R.string.facebook);
                TellFriendDelegate.closeFriendContent(activity2.getApplicationContext(), viewGroup2);
            }
        });
        final String str5 = sharingText;
        final String str6 = sharingUrl;
        final Context context2 = context;
        final Activity activity3 = activity;
        final ViewGroup viewGroup3 = root;
        ((Button) root.findViewById(R.id.share_on_twitter_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (!AppCore.getInstance().getCachingManager().hasTwitterData()) {
                    TellFriendDelegate.openTwitterLoginView(activity3);
                } else if (str5 == null || str6 == null) {
                    ViewUtils.tweetAppName(context2);
                } else {
                    ViewUtils.tweetAppName(context2, str5 + str6);
                }
                TellFriendDelegate.closeFriendContent(activity3.getApplicationContext(), viewGroup3);
            }
        });
        final String str7 = sharingText;
        final ViewGroup viewGroup4 = root;
        ((Button) root.findViewById(R.id.share_by_sms_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String emailBodyFormat = context.getString(R.string.email_body_format);
                Intent sendIntent = new Intent("android.intent.action.VIEW");
                String combinedUrl = String.format(emailBodyFormat, context.getString(R.string.app_name)) + AppConstants.MARKET_TEMPLATE_URL + context.getPackageName();
                if (StringUtils.isNotEmpty(str7)) {
                    combinedUrl = str7;
                }
                sendIntent.putExtra(AppConstants.SMS_BODY, combinedUrl);
                sendIntent.setType(AppConstants.SMS_TYPE);
                activity.startActivity(sendIntent);
                TellFriendDelegate.closeFriendContent(activity.getApplicationContext(), viewGroup4);
            }
        });
    }

    public static void openFriendContent(Context context, ViewGroup tellFriendContent) {
        if (tellFriendContent.getVisibility() == 8) {
            tellFriendContent.setVisibility(0);
            tellFriendContent.startAnimation(AnimationUtils.loadAnimation(context, R.anim.show_tell_friends_dialog));
        }
    }

    /* access modifiers changed from: private */
    public static void closeFriendContent(Context context, ViewGroup root) {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.hide_tell_friends_dialog);
        root.startAnimation(animation);
        animation.setAnimationListener(new ViewUtils.HideAnimationListener(root));
    }

    /* access modifiers changed from: private */
    public static void openTwitterLoginView(Activity activity) {
        activity.startActivity(new Intent(activity.getApplicationContext(), TwitterLoginActivity.class));
    }

    /* access modifiers changed from: private */
    public static void shareViaFacebook(final Activity activity, String url, int titleResourceId) {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog);
        dialog.setTitle(titleResourceId);
        dialog.setCancelable(true);
        final WebView webView = (WebView) dialog.findViewById(R.id.webview);
        webView.setWebViewClient(new WebViewClient() {
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                if (url.contains("www.facebook.com/ajax/sharer/submit_page") && dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    ViewUtils.showShortToast(activity.getApplicationContext(), R.string.facebook_sharing_successful);
                }
            }

            public void onPageFinished(WebView view, String url) {
                webView.setVisibility(0);
                webView.requestFocus(130);
            }
        });
        ViewUtils.plubWebView(webView);
        webView.getSettings().setUserAgent(1);
        webView.loadUrl(url);
        dialog.show();
    }
}
