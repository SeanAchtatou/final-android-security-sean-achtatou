package com.biznessapps.fragments.utils;

import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.layout.R;
import com.biznessapps.player.MusicItem;
import com.biznessapps.player.MusicPlayer;
import com.biznessapps.player.PlayerServiceAccessor;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;

public class MusicDelegate {
    private static final int DEFAULT_DELAY_TIME = 500;
    private static final int MAX_PROGRESS_VAL = 1000;
    private static final int REFRESH_TICK = 1;
    public static int TINT_COLOR = 0;
    /* access modifiers changed from: private */
    public static MusicItem currentItem;
    /* access modifiers changed from: private */
    public static boolean isMusicPanelHiden = true;
    /* access modifiers changed from: private */
    public Context appContext;
    private ViewGroup collapseButtonContainer;
    private TextView currentTimeView;
    /* access modifiers changed from: private */
    public long duration;
    /* access modifiers changed from: private */
    public LinearLayout expandBtnContainer;
    private boolean isActive;
    private Handler mainHandler;
    /* access modifiers changed from: private */
    public LinearLayout miniPlayerLayout;
    /* access modifiers changed from: private */
    public TextView miniSongView;
    private ImageButton nextButton;
    private ViewGroup nextButtonContainer;
    /* access modifiers changed from: private */
    public ImageButton playButton;
    private ViewGroup playButtonContainer;
    private SeekBar playSeekBar;
    private ImageButton previousButton;
    private ViewGroup previousButtonContainer;
    private ViewGroup rootView;
    private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStopTrackingTouch(SeekBar seekBar) {
            boolean unused = MusicDelegate.this.seekFromTouch = false;
            long unused2 = MusicDelegate.this.seekPosOverride = -1;
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
            boolean unused = MusicDelegate.this.seekFromTouch = true;
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser && MusicDelegate.this.getPlayerServiceAccessor() != null) {
                long unused = MusicDelegate.this.seekPosOverride = (MusicDelegate.this.duration * ((long) progress)) / 1000;
                MusicDelegate.this.getPlayerServiceAccessor().seek(MusicDelegate.this.seekPosOverride);
                if (!MusicDelegate.this.seekFromTouch) {
                    long unused2 = MusicDelegate.this.refreshNow();
                    long unused3 = MusicDelegate.this.seekPosOverride = -1;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean seekFromTouch;
    /* access modifiers changed from: private */
    public long seekPosOverride = -1;
    private ImageButton volumeButton;

    public static void setTintColor(int tintColor) {
        TINT_COLOR = tintColor;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public ViewGroup getRootView() {
        return this.rootView;
    }

    public void onResume() {
        if (this.isActive) {
            MusicItem song = getPlayerServiceAccessor().getCurrentSong();
            if (song != null) {
                updateTrackInfo(song);
            }
            updateMenuPanelVisibility();
        }
    }

    public void onStop() {
        if (this.isActive) {
            this.mainHandler.removeMessages(1);
        }
    }

    public void initViews(ViewGroup root, Context context) {
        this.rootView = root;
        this.appContext = context;
        getMainHandler();
        this.nextButton = (ImageButton) root.findViewById(R.id.player_next_button);
        if (this.nextButton != null) {
            this.isActive = true;
            this.playButton = (ImageButton) root.findViewById(R.id.player_play_button);
            this.previousButton = (ImageButton) root.findViewById(R.id.player_previous_button);
            this.currentTimeView = (TextView) root.findViewById(R.id.current_time_view);
            this.playSeekBar = (SeekBar) root.findViewById(R.id.seekbar);
            this.playSeekBar.setOnSeekBarChangeListener(this.seekBarListener);
            this.playSeekBar.setClickable(false);
            this.miniSongView = (TextView) root.findViewById(R.id.mini_song_title);
            this.miniPlayerLayout = (LinearLayout) root.findViewById(R.id.mini_player_container);
            this.expandBtnContainer = (LinearLayout) root.findViewById(R.id.player_expand_button_container);
            this.volumeButton = (ImageButton) root.findViewById(R.id.player_volume_button);
            this.playButtonContainer = (ViewGroup) root.findViewById(R.id.player_play_container);
            this.previousButtonContainer = (ViewGroup) root.findViewById(R.id.player_previous_container);
            this.nextButtonContainer = (ViewGroup) root.findViewById(R.id.player_next_container);
            this.collapseButtonContainer = (ViewGroup) root.findViewById(R.id.player_collapse_container);
            AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
            CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.playButton.getBackground());
            CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.previousButton.getBackground());
            CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.nextButton.getBackground());
            CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.volumeButton.getBackground());
            CommonUtils.overrideImageColor(TINT_COLOR, this.expandBtnContainer.getBackground());
            CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((LayerDrawable) this.playSeekBar.getProgressDrawable()).getDrawable(1));
            this.miniPlayerLayout.setBackgroundColor(TINT_COLOR);
            this.miniSongView.setTextColor(settings.getButtonBgColor());
            this.currentTimeView.setTextColor(settings.getButtonBgColor());
            this.nextButtonContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        int currentPosition = MusicDelegate.this.getPlayerServiceAccessor().getCurrentPosition() + 1;
                        if (currentPosition >= MusicDelegate.this.getPlayerServiceAccessor().getSongs().size()) {
                            currentPosition = 0;
                        }
                        MusicDelegate.this.getPlayerServiceAccessor().setCurrentPosition(currentPosition);
                        MusicDelegate.this.getPlayerServiceAccessor().stop();
                        MusicDelegate.this.getPlayerServiceAccessor().play(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            this.previousButtonContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        int currentPosition = MusicDelegate.this.getPlayerServiceAccessor().getCurrentPosition() - 1;
                        if (currentPosition < 0) {
                            currentPosition = MusicDelegate.this.getPlayerServiceAccessor().getSongs().size() - 1;
                        }
                        MusicDelegate.this.getPlayerServiceAccessor().setCurrentPosition(currentPosition);
                        MusicDelegate.this.getPlayerServiceAccessor().stop();
                        MusicDelegate.this.getPlayerServiceAccessor().play(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            this.playButtonContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MusicDelegate.this.getPlayerServiceAccessor().play(null);
                }
            });
            this.collapseButtonContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MusicDelegate.this.miniPlayerLayout.setVisibility(8);
                    MusicDelegate.this.expandBtnContainer.setVisibility(0);
                    MusicDelegate.this.miniPlayerLayout.startAnimation(AnimationUtils.makeOutAnimation(MusicDelegate.this.appContext, false));
                    boolean unused = MusicDelegate.isMusicPanelHiden = true;
                }
            });
            this.expandBtnContainer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    MusicDelegate.this.expandBtnContainer.setVisibility(8);
                    MusicDelegate.this.miniPlayerLayout.setVisibility(0);
                    MusicDelegate.this.miniPlayerLayout.startAnimation(AnimationUtils.makeInAnimation(MusicDelegate.this.appContext, true));
                    boolean unused = MusicDelegate.isMusicPanelHiden = false;
                }
            });
            updateMenuPanelVisibility();
            updatePlayerState(getPlayerServiceAccessor().getPlayerState().getState());
        }
    }

    public void setPlayerBgColor(int color) {
        this.miniPlayerLayout.setBackgroundColor(color);
    }

    public void hideCollapseButton() {
        this.collapseButtonContainer.setVisibility(4);
        this.expandBtnContainer.setVisibility(8);
        this.miniPlayerLayout.setVisibility(0);
        this.miniPlayerLayout.startAnimation(AnimationUtils.makeInAnimation(this.appContext, true));
    }

    private void updateMenuPanelVisibility() {
        if (isMusicPanelHiden) {
            this.collapseButtonContainer.performClick();
        } else {
            this.expandBtnContainer.performClick();
        }
    }

    /* access modifiers changed from: private */
    public void queueNextRefresh(long delay) {
        Message msg = this.mainHandler.obtainMessage(1);
        this.mainHandler.removeMessages(1);
        this.mainHandler.sendMessageDelayed(msg, delay);
    }

    /* access modifiers changed from: private */
    public long refreshNow() {
        try {
            long pos = this.seekPosOverride < 0 ? getPlayerServiceAccessor().getPosition() : this.seekPosOverride;
            long remaining = 1000 - (pos % 1000);
            if (pos >= 0) {
                this.currentTimeView.setText(StringUtils.makeTimeString(this.appContext, pos / 1000));
                this.playSeekBar.setProgress((int) ((1000 * pos) / this.duration));
                return remaining;
            }
            this.currentTimeView.setText("");
            return remaining;
        } catch (Exception e) {
            e.printStackTrace();
            return 500;
        }
    }

    public void updateTrackInfo(final MusicItem item) {
        if (this.isActive) {
            currentItem = item;
            getMainHandler().post(new Runnable() {
                public void run() {
                    MusicDelegate.this.miniSongView.setText(String.format("%s  %s", item.getSongInfo(), item.getAlbumName()));
                    long unused = MusicDelegate.this.duration = MusicDelegate.this.getPlayerServiceAccessor().getDuration();
                    MusicDelegate.this.queueNextRefresh(MusicDelegate.this.refreshNow());
                }
            });
        }
    }

    public void updateTrackInfo() {
        if (this.isActive && currentItem != null) {
            getMainHandler().post(new Runnable() {
                public void run() {
                    MusicDelegate.this.miniSongView.setText(String.format("%s  %s", MusicDelegate.currentItem.getSongInfo(), MusicDelegate.currentItem.getAlbumName()));
                    long unused = MusicDelegate.this.duration = MusicDelegate.this.getPlayerServiceAccessor().getDuration();
                    MusicDelegate.this.queueNextRefresh(MusicDelegate.this.refreshNow());
                }
            });
        }
    }

    public void updatePlayerState(final int newState) {
        if (this.isActive) {
            getMainHandler().post(new Runnable() {
                public void run() {
                    if (newState == 1) {
                        MusicDelegate.this.playButton.setBackgroundResource(R.drawable.pause_icon);
                    } else {
                        MusicDelegate.this.playButton.setBackgroundResource(R.drawable.play_icon);
                    }
                    CommonUtils.overrideImageColor(AppCore.getInstance().getUiSettings().getButtonBgColor(), MusicDelegate.this.playButton.getBackground());
                }
            });
        }
    }

    public PlayerServiceAccessor getPlayerServiceAccessor() {
        if (!MusicPlayer.getInstance().isInited()) {
            MusicPlayer.getInstance().init(this.appContext);
        }
        return MusicPlayer.getInstance().getServiceAccessor();
    }

    private Handler getMainHandler() {
        if (this.mainHandler == null) {
            this.mainHandler = new Handler(Looper.getMainLooper()) {
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case 1:
                            MusicDelegate.this.queueNextRefresh(MusicDelegate.this.refreshNow());
                            return;
                        default:
                            return;
                    }
                }
            };
        }
        return this.mainHandler;
    }
}
