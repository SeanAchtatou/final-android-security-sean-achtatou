package com.biznessapps.fragments.youtube;

import android.app.Activity;
import android.widget.ListAdapter;
import com.biznessapps.adapters.YoutubeRssAdapter;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.YoutubeRssItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class YoutubeListFragment extends CommonListFragment<YoutubeRssItem> {
    private String note;

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RSS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseYoutubeRssList(dataToParse);
        return cacher().saveData(CachingConstants.YOUTUBE_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.YOUTUBE_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r6, android.view.View r7, int r8, long r9) {
        /*
            r5 = this;
            android.widget.Adapter r2 = r6.getAdapter()
            java.lang.Object r1 = r2.getItem(r8)
            com.biznessapps.model.YoutubeRssItem r1 = (com.biznessapps.model.YoutubeRssItem) r1
            if (r1 == 0) goto L_0x008d
            java.lang.String r2 = r1.getId()
            boolean r2 = com.biznessapps.utils.StringUtils.isNotEmpty(r2)
            if (r2 == 0) goto L_0x008d
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r2 = r5.getApplicationContext()
            java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r3 = com.biznessapps.activities.SingleFragmentActivity.class
            r0.<init>(r2, r3)
            java.lang.String r2 = "parent_id"
            java.lang.String r3 = r1.getId()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "LINK"
            java.lang.String r3 = r1.getLink()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "NOTE_DATA"
            java.lang.String r3 = r5.note
            r0.putExtra(r2, r3)
            java.lang.String r2 = "YOUTUBE_MODE"
            r3 = 1
            r0.putExtra(r2, r3)
            java.lang.String r2 = "IMAGE_URL"
            java.lang.String r3 = r1.getImageUrl()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_FRAGMENT"
            java.lang.String r3 = "YOUTUBE_SINGLE_VIEW_FRAGMENT"
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_UNIQUE_ID"
            com.biznessapps.activities.CommonFragmentActivity r3 = r5.getHoldActivity()
            long r3 = r3.getTabId()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_SPECIAL_ID"
            android.content.Intent r3 = r5.getIntent()
            java.lang.String r4 = "TAB_SPECIAL_ID"
            java.lang.String r3 = r3.getStringExtra(r4)
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_LABEL"
            android.content.Intent r3 = r5.getIntent()
            java.lang.String r4 = "TAB_LABEL"
            java.lang.String r3 = r3.getStringExtra(r4)
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_SPECIAL_ID"
            android.content.Intent r3 = r5.getIntent()
            java.lang.String r4 = "TAB_SPECIAL_ID"
            java.lang.String r3 = r3.getStringExtra(r4)
            r0.putExtra(r2, r3)
            r5.startActivity(r0)
        L_0x008d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.youtube.YoutubeListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        boolean hasNoData = true;
        if (this.items != null && !this.items.isEmpty()) {
            List<YoutubeRssItem> itemsList = new ArrayList<>();
            if (this.items.size() != 1 || !StringUtils.isEmpty(((YoutubeRssItem) this.items.get(0)).getId())) {
                hasNoData = false;
            }
            if (hasNoData) {
                ((YoutubeRssItem) this.items.get(0)).setTitle(getString(R.string.no_comments));
            } else {
                this.note = ((YoutubeRssItem) this.items.get(0)).getNote();
            }
            for (YoutubeRssItem item : this.items) {
                itemsList.add(getWrappedItem(item, itemsList));
            }
            this.listView.setAdapter((ListAdapter) new YoutubeRssAdapter(holdActivity.getApplicationContext(), itemsList));
            initListViewListener();
        }
    }
}
