package com.biznessapps.fragments.youtube;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.fanwall.FanWallFragment;
import com.biznessapps.fragments.fanwall.NewFanWallAdapter;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.player.MusicPlayer;
import com.biznessapps.player.PlayerServiceAccessor;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.List;

public class YoutubeFragment extends FanWallFragment {
    private static final int NO_TAG_FOUND = -1;
    private static final String UPLOADS_TAG = "uploads/";
    /* access modifiers changed from: private */
    public Button commentsButton;
    private BitmapWrapper imageBgWrapper;
    private String imageUrl;
    /* access modifiers changed from: private */
    public Button infoButton;
    private String note;
    /* access modifiers changed from: private */
    public WebView webView;
    private ImageView youtubeItemImage;
    /* access modifiers changed from: private */
    public String youtubeLink;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.youtube_detail_layout, (ViewGroup) null);
        this.root.setBackgroundColor(-1);
        initViews(this.root);
        loadData();
        defineBgUrl();
        this.imageUrl = getIntent().getStringExtra(AppConstants.IMAGE_URL);
        this.note = getIntent().getStringExtra(AppConstants.NOTE_DATA);
        loadWebContent();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onResume() {
        super.onResume();
        loadImageBg();
    }

    public void onStop() {
        super.onStop();
        clearImageBg();
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.youtubeItemImage = (ImageView) root.findViewById(R.id.youtube_item_image);
        this.youtubeItemImage.setOnClickListener(getOnClickListener());
        ((ImageView) root.findViewById(R.id.youtube_play_item_image)).setOnClickListener(getOnClickListener());
        this.youtubeLink = getIntent().getStringExtra(AppConstants.YOUTUBE_LINK_DATA);
        this.webView = (WebView) root.findViewById(R.id.webview);
        ViewGroup tabContainer = (ViewGroup) root.findViewById(R.id.commentsTab);
        this.commentsButton = (Button) tabContainer.findViewById(R.id.comments_button);
        this.infoButton = (Button) tabContainer.findViewById(R.id.info_button);
        this.infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                YoutubeFragment.this.activateButton(YoutubeFragment.this.infoButton, YoutubeFragment.this.webView);
            }
        });
        this.commentsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                YoutubeFragment.this.activateButton(YoutubeFragment.this.commentsButton, YoutubeFragment.this.commentsListView);
            }
        });
        int barColor = getUiSettings().getNavigationBarColor();
        int barTextColor = getUiSettings().getNavigationTextColor();
        tabContainer.setBackgroundColor(barColor);
        this.commentsButton.setTextColor(barTextColor);
        this.infoButton.setTextColor(barTextColor);
        activateButton(this.commentsButton, this.commentsListView);
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.FAN_WALL_YOUTUBE_FORMAT, cacher().getAppCode(), this.commentParentId, this.tabId);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        loadImageBg();
    }

    /* access modifiers changed from: protected */
    public void plugListView(Activity holdActivity) {
        boolean hasNoData;
        if (info != null && !info.getComments().isEmpty()) {
            List<FanWallComment> comments = info.getComments();
            if (comments.size() != 1 || !StringUtils.isEmpty(comments.get(0).getId())) {
                hasNoData = false;
            } else {
                hasNoData = true;
            }
            if (hasNoData) {
                comments.get(0).setComment(getString(R.string.no_comments));
            }
            this.commentsListView.setAdapter((ListAdapter) new NewFanWallAdapter(holdActivity.getApplicationContext(), comments, true));
            this.commentsListView.setOnItemClickListener(getOnItemClickListener());
        }
    }

    private View.OnClickListener getOnClickListener() {
        return new View.OnClickListener() {
            public void onClick(View v) {
                int idIndex;
                if (YoutubeFragment.this.youtubeLink != null) {
                    int idIndex2 = YoutubeFragment.this.youtubeLink.indexOf(YoutubeFragment.UPLOADS_TAG);
                    if (idIndex2 > -1 && (idIndex = idIndex2 + YoutubeFragment.UPLOADS_TAG.length()) < YoutubeFragment.this.youtubeLink.length()) {
                        String videoId = YoutubeFragment.this.youtubeLink.substring(idIndex);
                        Intent videoClient = new Intent("android.intent.action.VIEW");
                        videoClient.setData(Uri.parse(AppConstants.YOUTUBE_URL + videoId));
                        YoutubeFragment.this.startActivity(videoClient);
                    }
                    if (YoutubeFragment.this.getHoldActivity().getMusicDelegate() != null && YoutubeFragment.this.getPlayerServiceAccessor().getPlayerState().getState() == 1) {
                        YoutubeFragment.this.getPlayerServiceAccessor().pause();
                        YoutubeFragment.this.getHoldActivity().getMusicDelegate().updatePlayerState(YoutubeFragment.this.getPlayerServiceAccessor().getPlayerState().getState());
                    }
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public PlayerServiceAccessor getPlayerServiceAccessor() {
        if (!MusicPlayer.getInstance().isInited()) {
            MusicPlayer.getInstance().init(getApplicationContext());
        }
        return MusicPlayer.getInstance().getServiceAccessor();
    }

    private AdapterView.OnItemClickListener getOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r3v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r8, android.view.View r9, int r10, long r11) {
                /*
                    r7 = this;
                    android.widget.Adapter r3 = r8.getAdapter()
                    java.lang.Object r1 = r3.getItem(r10)
                    com.biznessapps.model.FanWallComment r1 = (com.biznessapps.model.FanWallComment) r1
                    if (r1 == 0) goto L_0x0062
                    android.content.Intent r0 = new android.content.Intent
                    com.biznessapps.fragments.youtube.YoutubeFragment r3 = com.biznessapps.fragments.youtube.YoutubeFragment.this
                    android.content.Context r3 = r3.getApplicationContext()
                    java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r4 = com.biznessapps.activities.SingleFragmentActivity.class
                    r0.<init>(r3, r4)
                    java.lang.String r3 = "parent_id"
                    java.lang.String r4 = r1.getId()
                    r0.putExtra(r3, r4)
                    com.biznessapps.fragments.youtube.YoutubeFragment r3 = com.biznessapps.fragments.youtube.YoutubeFragment.this
                    android.content.Intent r3 = r3.getIntent()
                    java.lang.String r4 = "TAB_SPECIAL_ID"
                    java.lang.String r2 = r3.getStringExtra(r4)
                    java.lang.String r3 = "TAB_SPECIAL_ID"
                    r0.putExtra(r3, r2)
                    java.lang.String r3 = "YOUTUBE_MODE"
                    com.biznessapps.fragments.youtube.YoutubeFragment r4 = com.biznessapps.fragments.youtube.YoutubeFragment.this
                    android.content.Intent r4 = r4.getIntent()
                    java.lang.String r5 = "YOUTUBE_MODE"
                    r6 = 0
                    boolean r4 = r4.getBooleanExtra(r5, r6)
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "TAB_LABEL"
                    com.biznessapps.fragments.youtube.YoutubeFragment r4 = com.biznessapps.fragments.youtube.YoutubeFragment.this
                    android.content.Intent r4 = r4.getIntent()
                    java.lang.String r5 = "TAB_LABEL"
                    java.lang.String r4 = r4.getStringExtra(r5)
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "TAB_FRAGMENT"
                    java.lang.String r4 = "FanWallViewController"
                    r0.putExtra(r3, r4)
                    com.biznessapps.fragments.youtube.YoutubeFragment r3 = com.biznessapps.fragments.youtube.YoutubeFragment.this
                    r3.startActivity(r0)
                L_0x0062:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.youtube.YoutubeFragment.AnonymousClass4.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        };
    }

    private void loadImageBg() {
        boolean hasHeaderBg = StringUtils.isNotEmpty(this.imageUrl);
        this.youtubeItemImage.setVisibility(hasHeaderBg ? 0 : 8);
        if (hasHeaderBg) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(this.imageUrl, this.youtubeItemImage);
        }
    }

    private void clearImageBg() {
        this.youtubeItemImage.setBackgroundDrawable(null);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        this.bgUrl = info != null ? info.getImage() : "";
        return this.bgUrl;
    }

    /* access modifiers changed from: private */
    public void activateButton(Button buttonToActivate, View viewToActivate) {
        this.infoButton.setBackgroundResource(R.drawable.tab_bar);
        this.commentsButton.setBackgroundResource(R.drawable.tab_bar);
        buttonToActivate.setBackgroundResource(R.drawable.tab_button_active);
        this.commentsListView.setVisibility(8);
        this.webView.setVisibility(8);
        viewToActivate.setVisibility(0);
    }

    private void loadWebContent() {
        if (StringUtils.isNotEmpty(this.note)) {
            ViewUtils.plubWebView(this.webView);
            this.webView.loadDataWithBaseURL(null, StringUtils.decode(this.note), AppConstants.TEXT_HTML, AppConstants.UTF_8_CHARSET, null);
        }
    }
}
