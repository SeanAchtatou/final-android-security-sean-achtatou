package com.biznessapps.fragments.coupons;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.TellFriendDelegate;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.LocationView;
import com.biznessapps.layout.views.scanning.CaptureActivity;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.CouponItem;
import com.biznessapps.storage.StorageKeeper;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CouponDetailFragment extends CommonFragment {
    private static final String CHECKIN_FORMAT = "%d/%d";
    /* access modifiers changed from: private */
    public int checkinCount = 0;
    /* access modifiers changed from: private */
    public List<CouponItem.CouponsLocation> checkinLocations = new ArrayList();
    /* access modifiers changed from: private */
    public CouponItem coupon;
    private TextView couponCheckinView;
    private ImageView couponImage;
    /* access modifiers changed from: private */
    public Location currentLocation;
    private TextView description;
    private TextView imageTextView;
    private CommonListEntity infoItem;
    /* access modifiers changed from: private */
    public boolean isQrCoupon;
    private LocationListener locListener;
    private CommonFragmentActivity.BackPressed onBackPressedListener = new CommonFragmentActivity.BackPressed() {
        public boolean onBackPressed() {
            CommonFragmentActivity activity = CouponDetailFragment.this.getHoldActivity();
            if (activity != null) {
                Intent data = new Intent();
                data.putExtra(AppConstants.COUPON_EXTRA, CouponDetailFragment.this.coupon);
                activity.setResult(1, data);
                activity.finish();
            }
            return true;
        }
    };
    /* access modifiers changed from: private */
    public Button redeemButton;
    private TextView redeemTextView;
    private Button shareButton;
    private String tabId;
    /* access modifiers changed from: private */
    public ViewGroup tellFriendContainer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.coupon_detail_layout, (ViewGroup) null);
        this.tellFriendContainer = (ViewGroup) this.root.findViewById(R.id.tell_friends_content);
        TellFriendDelegate.initTellFriends(getHoldActivity(), this.tellFriendContainer, String.format(getString(R.string.share_coupon_info), getString(R.string.app_name)), AppConstants.MARKET_TEMPLATE_URL + getHoldActivity().getPackageName());
        this.isQrCoupon = getIntent().getBooleanExtra(AppConstants.QR_COUPON_TYPE, false);
        if (!this.isQrCoupon) {
            this.currentLocation = AppCore.getInstance().getLocationFinder().getCurrentLocation();
            AppCore.getInstance().getLocationFinder().addLocationListener(getLocationListener());
            ViewUtils.checkGpsEnabling(getHoldActivity());
        }
        this.coupon = (CouponItem) getIntent().getSerializableExtra(AppConstants.COUPON_EXTRA);
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        ViewUtils.setGlobalBackgroundColor(this.root);
        return this.root;
    }

    public void onResume() {
        super.onResume();
        this.checkinCount = this.coupon.getCheckinTarget();
        initCheckinLocations(this.coupon);
        if (!this.isQrCoupon) {
            AppCore.getInstance().getLocationFinder().startSearching();
        }
        loadData();
        getHoldActivity().addBackPressedListener(this.onBackPressedListener);
    }

    public void onStop() {
        super.onStop();
        if (!this.isQrCoupon) {
            AppCore.getInstance().getLocationFinder().stopSearching();
        }
    }

    public void onDestroy() {
        CommonFragmentActivity activity = getHoldActivity();
        if (activity != null) {
            activity.removeBackPressedListener(this.onBackPressedListener);
        }
        if (!this.isQrCoupon) {
            AppCore.getInstance().getLocationFinder().removeLocationListener(getLocationListener());
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(this.isQrCoupon ? ServerConstants.QR_COUPON_DETAIL_FORMAT : ServerConstants.COUPON_DETAIL_FORMAT, this.coupon.getId(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.infoItem = JsonParserUtils.parseInfo(dataToParse);
        return cacher().saveData(CachingConstants.COUPONS_MAP_PROPERTY + this.coupon.getId(), this.infoItem);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        this.bitmapUrl = this.infoItem.getImage();
        initViewsWithData(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.infoItem = (CommonListEntity) cacher().getData(CachingConstants.COUPONS_MAP_PROPERTY + this.coupon.getId());
        return this.infoItem != null;
    }

    /* access modifiers changed from: protected */
    public List<WeakReference<View>> getViewsRef() {
        List<WeakReference<View>> viewsRef = new ArrayList<>();
        viewsRef.add(new WeakReference(this.redeemButton));
        viewsRef.add(new WeakReference(this.shareButton));
        return viewsRef;
    }

    private void initViewsWithData(Activity holdActivity) {
        TextView title = (TextView) this.root.findViewById(R.id.coupon_title_label);
        this.description = (TextView) this.root.findViewById(R.id.coupon_description_label);
        this.redeemTextView = (TextView) this.root.findViewById(R.id.redeem_textview);
        this.couponCheckinView = (TextView) this.root.findViewById(R.id.coupon_checkin_textview);
        this.imageTextView = (TextView) this.root.findViewById(R.id.coupon_image_text);
        this.redeemButton = (Button) this.root.findViewById(R.id.redeem_button);
        this.shareButton = (Button) this.root.findViewById(R.id.coupon_share_button);
        this.couponImage = (ImageView) this.root.findViewById(R.id.coupon_image);
        if (this.infoItem.getTitle() != null) {
            title.setText(this.infoItem.getTitle());
        }
        updateRedeemState();
        this.shareButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TellFriendDelegate.openFriendContent(CouponDetailFragment.this.getApplicationContext(), CouponDetailFragment.this.tellFriendContainer);
            }
        });
        this.redeemButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CouponDetailFragment.this.checkinCount <= 0 || CouponDetailFragment.this.redeemButton.getText() == CouponDetailFragment.this.getString(R.string.redeem)) {
                    CouponDetailFragment.this.tryToRedeem();
                } else if (CouponDetailFragment.this.isQrCoupon) {
                    CouponDetailFragment.this.doQrScanning();
                } else {
                    CouponDetailFragment.this.doGpsCheckin();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void updateRedeemState() {
        if (this.coupon.getLastRedeemedTime() <= 0 || this.checkinCount != this.coupon.getCheckinTargetMax()) {
            if (this.checkinCount > 0) {
                String format = getString(R.string.coupons_description_format);
                this.description.setText(String.format(format, Integer.valueOf(this.checkinCount)));
                updateRedeemText(false);
                this.redeemTextView.setVisibility(4);
                showCouponImageAndText(false);
                updateCheckinTitleVisibility(true);
            } else if (this.checkinCount == 0) {
                updateCheckinTitleVisibility(false);
                updateRedeemText(true);
                this.description.setText("");
                showCouponImageAndText(true);
            }
        } else if (this.coupon.isReusable()) {
            updateRedeemText(false);
            this.redeemTextView.setVisibility(4);
            updateCheckinTitleVisibility(true);
            showCouponImageAndText(false);
            String format2 = getString(R.string.coupons_description_format);
            this.description.setText(String.format(format2, Integer.valueOf(this.checkinCount)));
        } else {
            updateRedeemText(true);
            this.redeemTextView.setVisibility(0);
            updateCheckinTitleVisibility(false);
            showCouponImageAndText(true);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        boolean canChecking;
        if (intent != null && requestCode == 1) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            if (StringUtils.isNotEmpty(contents)) {
                if (System.currentTimeMillis() - this.coupon.getLastCheckinTime() > ((long) (this.coupon.getCheckinInterval() * 3600000))) {
                    canChecking = true;
                } else {
                    canChecking = false;
                }
                if (canChecking) {
                    int checkinResult = updateQrCheckin(contents);
                    if (checkinResult == 1) {
                        ViewUtils.showDialog(getHoldActivity(), R.string.have_unlocked_coupon);
                        updateRedeemState();
                    } else if (checkinResult == 0) {
                        ViewUtils.showDialog(getHoldActivity(), R.string.checking_more);
                        updateRedeemState();
                    } else if (checkinResult == -1) {
                        ViewUtils.showDialog(getHoldActivity(), R.string.qr_unsuccess_scanning);
                    }
                } else {
                    ViewUtils.showDialog(getHoldActivity(), String.format(getString(R.string.checkin_interval), Integer.valueOf(this.coupon.getCheckinInterval() * 1)));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        if (this.coupon != null) {
            data.setItemId(this.coupon.getId());
        }
        return data;
    }

    private void showCouponImageAndText(boolean showIt) {
        if (showIt) {
            this.bitmapUrl = this.infoItem.getImage();
            this.couponImage.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            this.couponImage.setScaleType(ImageView.ScaleType.FIT_XY);
            this.couponImage.setBackgroundColor(0);
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadAppImage(this.bitmapUrl, this.couponImage);
            this.imageTextView.setText(this.infoItem.getDescription());
            return;
        }
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-2, -2);
        lp.gravity = 17;
        this.couponImage.setLayoutParams(lp);
        this.couponImage.setImageBitmap(null);
        this.couponImage.setBackgroundResource(R.drawable.coupon_locked);
        this.imageTextView.setText("");
    }

    private void updateCheckinTitleVisibility(boolean showCheckingTitle) {
        if (showCheckingTitle) {
            this.couponCheckinView.setVisibility(0);
            this.couponCheckinView.setText(String.format(CHECKIN_FORMAT, Integer.valueOf(this.coupon.getCheckinTargetMax() - this.checkinCount), Integer.valueOf(this.coupon.getCheckinTargetMax())));
            return;
        }
        this.couponCheckinView.setVisibility(4);
    }

    private void updateRedeemText(boolean showRedeemText) {
        if (showRedeemText) {
            this.redeemButton.setText(getString(R.string.redeem));
        } else if (this.isQrCoupon) {
            this.redeemButton.setText(getString(R.string.scan_qr));
        } else {
            this.redeemButton.setText(getString(R.string.checkin));
        }
    }

    /* access modifiers changed from: private */
    public void tryToRedeem() {
        if (this.coupon.getLastRedeemedTime() == 0 || this.coupon.isReusable()) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getHoldActivity());
            alertBuilder.setView(ViewUtils.loadLayout(getApplicationContext(), R.layout.common_dialog_layout));
            alertBuilder.setMessage(R.string.redeem_coupon);
            AlertDialog dialog = alertBuilder.create();
            dialog.setButton(-1, getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    int unused = CouponDetailFragment.this.checkinCount = CouponDetailFragment.this.coupon.getCheckinTargetMax();
                    CouponDetailFragment.this.coupon.setCheckinTarget(CouponDetailFragment.this.checkinCount);
                    CouponDetailFragment.this.coupon.setLastRedeemedTime(System.currentTimeMillis());
                    CouponDetailFragment.this.coupon.setLastCheckinTime(System.currentTimeMillis());
                    CouponDetailFragment.this.updateRedeemState();
                    CouponDetailFragment.this.storeCoupon(CouponDetailFragment.this.coupon);
                    ViewUtils.showDialog(CouponDetailFragment.this.getHoldActivity(), R.string.successfull_coupon_redeem);
                }
            });
            dialog.setButton(-2, getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
            return;
        }
        ViewUtils.showShortToast(getApplicationContext(), R.string.cant_redeem_again);
    }

    private void initCheckinLocations(CouponItem coupon2) {
        List<CouponItem.CouponsLocation> couponLocations = coupon2.getLocations();
        if (couponLocations != null) {
            for (CouponItem.CouponsLocation item : couponLocations) {
                if (!StringUtils.checkTextFieldsOnEmpty(item.getLatitude(), item.getLongitude())) {
                    item.setCouponName(coupon2.getTitle());
                    this.checkinLocations.add(item);
                }
            }
        }
    }

    private LocationListener getLocationListener() {
        if (this.locListener == null) {
            this.locListener = new LocationListener() {
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }

                public void onLocationChanged(Location location) {
                    CouponDetailFragment.this.updateLocationData(location);
                }
            };
        }
        return this.locListener;
    }

    /* access modifiers changed from: private */
    public void updateLocationData(Location location) {
        if (location != null) {
            this.currentLocation = location;
        }
        float distance = 0.0f;
        for (CouponItem.CouponsLocation checkin : this.checkinLocations) {
            Location locItem = new Location("");
            try {
                locItem.setLatitude(Double.parseDouble(checkin.getLatitude()));
                locItem.setLongitude(Double.parseDouble(checkin.getLongitude()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            float currentDistance = location.distanceTo(locItem);
            if (currentDistance < distance || distance == 0.0f) {
                distance = currentDistance;
            }
        }
    }

    /* access modifiers changed from: private */
    public void doQrScanning() {
        startActivityForResult(new Intent(getApplicationContext(), CaptureActivity.class), 1);
    }

    private int updateQrCheckin(String code) {
        boolean checkinWasChanged = false;
        boolean couponUnlocked = false;
        if (code.equalsIgnoreCase(this.coupon.getCode()) && this.checkinCount > 0) {
            this.checkinCount--;
            if (this.checkinCount == 0) {
                couponUnlocked = true;
            }
            this.coupon.setCheckinTarget(this.checkinCount);
            this.coupon.setLastCheckinTime(System.currentTimeMillis());
            checkinWasChanged = true;
            storeCoupon(this.coupon);
        }
        if (couponUnlocked) {
            return 1;
        }
        if (checkinWasChanged) {
            return 0;
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public void doGpsCheckin() {
        boolean canCheckin;
        if (this.currentLocation == null) {
            ViewUtils.showDialog(getHoldActivity(), R.string.location_absent);
            return;
        }
        if (System.currentTimeMillis() - this.coupon.getLastCheckinTime() > ((long) (this.coupon.getCheckinInterval() * 3600000))) {
            canCheckin = true;
        } else {
            canCheckin = false;
        }
        if (canCheckin) {
            int checkinResult = updateGpsCheckin();
            if (checkinResult > 0) {
                ViewUtils.showDialog(getHoldActivity(), R.string.have_unlocked_coupon);
                updateRedeemState();
            } else if (checkinResult == 0) {
                ViewUtils.showDialog(getHoldActivity(), R.string.checking_more);
                updateRedeemState();
            } else if (checkinResult == -1) {
                ViewUtils.showDialog(getHoldActivity(), getString(R.string.checking_unsuccess), new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                    public void run() {
                        ArrayList<String> longitudes = new ArrayList<>();
                        ArrayList<String> latitudes = new ArrayList<>();
                        ArrayList<String> locationsName = new ArrayList<>();
                        Intent showLocations = new Intent(CouponDetailFragment.this.getApplicationContext(), LocationView.class);
                        if (CouponDetailFragment.this.currentLocation != null) {
                            showLocations.putExtra(LocationView.HAS_USER_LOCATION, true);
                            longitudes.add("" + CouponDetailFragment.this.currentLocation.getLongitude());
                            latitudes.add("" + CouponDetailFragment.this.currentLocation.getLatitude());
                            locationsName.add(CouponDetailFragment.this.getString(R.string.your_location));
                        }
                        for (CouponItem.CouponsLocation checkin : CouponDetailFragment.this.checkinLocations) {
                            longitudes.add("" + checkin.getLongitude());
                            latitudes.add("" + checkin.getLatitude());
                            locationsName.add(checkin.getCouponName());
                        }
                        if (longitudes.size() > 0) {
                            showLocations.putStringArrayListExtra(LocationView.LATITUDES, latitudes);
                            showLocations.putStringArrayListExtra(LocationView.LONGITUDES, longitudes);
                            showLocations.putStringArrayListExtra(LocationView.LOCATIONS_NAME, locationsName);
                            CouponDetailFragment.this.startActivity(showLocations);
                        }
                    }
                });
            }
        } else {
            ViewUtils.showDialog(getHoldActivity(), String.format(getString(R.string.checkin_interval), Integer.valueOf(this.coupon.getCheckinInterval() * 1)));
        }
    }

    private int updateGpsCheckin() {
        boolean checkinWasChanged = false;
        boolean couponUnlocked = false;
        List<CouponItem.CouponsLocation> couponLocations = this.coupon.getLocations();
        if (couponLocations != null) {
            Iterator i$ = couponLocations.iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                CouponItem.CouponsLocation item = i$.next();
                if (!StringUtils.checkTextFieldsOnEmpty(item.getLatitude(), item.getLongitude())) {
                    Location locItem = new Location("");
                    try {
                        locItem.setLatitude(Double.parseDouble(item.getLatitude()));
                        locItem.setLongitude(Double.parseDouble(item.getLongitude()));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (this.currentLocation.distanceTo(locItem) < 487.80487f) {
                        if (this.checkinCount > 0) {
                            this.checkinCount--;
                            if (this.checkinCount == 0) {
                                couponUnlocked = true;
                            }
                            this.coupon.setCheckinTarget(this.checkinCount);
                            this.coupon.setLastCheckinTime(System.currentTimeMillis());
                            checkinWasChanged = true;
                            storeCoupon(this.coupon);
                        }
                    }
                }
            }
        }
        if (couponUnlocked) {
            return 1;
        }
        if (checkinWasChanged) {
            return 0;
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public void storeCoupon(CouponItem coupon2) {
        List<CouponItem> dataToStore = new ArrayList<>();
        dataToStore.add(coupon2);
        StorageKeeper.instance().addCoupons(dataToStore);
    }
}
