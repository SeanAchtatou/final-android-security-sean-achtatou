package com.biznessapps.fragments.coupons;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.CouponAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.map.LocationView;
import com.biznessapps.layout.views.scanning.CaptureActivity;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.CouponItem;
import com.biznessapps.storage.StorageKeeper;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CouponsListFragment extends CommonListFragment<CouponItem> {
    private ImageButton checkinButton;
    /* access modifiers changed from: private */
    public List<CouponItem.CouponsLocation> checkinLocations = new ArrayList();
    /* access modifiers changed from: private */
    public Location currentLocation;
    private boolean isQrCoupons;
    private LocationListener locListener;
    private TextView locationTextView;
    private CouponItem preLoadedItem;
    private ImageButton qrButton;

    public void setQrCoupons(boolean isQrCoupons2) {
        this.isQrCoupons = isQrCoupons2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (this.isQrCoupons) {
            this.qrButton = (ImageButton) this.root.findViewById(R.id.qr_scan_button);
            this.qrButton.setVisibility(0);
            this.qrButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CouponsListFragment.this.startScanning();
                }
            });
        } else {
            ViewGroup gpsCheckinRoot = (ViewGroup) this.root.findViewById(R.id.coupons_gps_checkin);
            gpsCheckinRoot.setVisibility(0);
            this.locationTextView = (TextView) this.root.findViewById(R.id.checkin_location_text);
            this.checkinButton = (ImageButton) this.root.findViewById(R.id.checkin_button);
            AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
            CommonUtils.customizeFooterNavigationBar(gpsCheckinRoot);
            initCheckinButtonListener();
            this.currentLocation = AppCore.getInstance().getLocationFinder().getCurrentLocation();
            AppCore.getInstance().getLocationFinder().addLocationListener(getLocationListener());
            ViewUtils.checkGpsEnabling(getHoldActivity());
            ViewUtils.updateMusicBarBottomMargin(getHoldActivity().getMusicDelegate(), (int) getResources().getDimension(R.dimen.footer_bar_height));
            this.locationTextView.setTextColor(settings.getNavigationTextColor());
        }
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.coupons_layout;
    }

    /* access modifiers changed from: private */
    public void startScanning() {
        startActivityForResult(new Intent(getApplicationContext(), CaptureActivity.class), 1);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(this.isQrCoupons ? ServerConstants.QR_COUPONS_FORMAT : ServerConstants.COUPONS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = doCouponsPrehandling(JsonParserUtils.parseCoupons(dataToParse));
        return cacher().saveData(CachingConstants.COUPONS_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (!this.isQrCoupons) {
            initCheckinLocations();
        }
        plugListView(holdActivity, true);
        startCouponDetailActivity(this.preLoadedItem);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.COUPONS_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    private List<CouponItem> doCouponsPrehandling(List<CouponItem> parsedCoupons) {
        List<CouponItem> resultItems = new ArrayList<>();
        for (CouponItem coupon : parsedCoupons) {
            Date currentDate = new Date(System.currentTimeMillis());
            if ((coupon.getStartDate() == null || !coupon.getStartDate().after(currentDate)) && (coupon.getEndDate() == null || !coupon.getEndDate().before(currentDate))) {
                resultItems.add(coupon);
            }
        }
        return resultItems;
    }

    private void plugListView(Activity holdActivity, boolean updateFromDB) {
        CouponItem couponData;
        boolean hasNoData = true;
        if (this.items != null && !this.items.isEmpty()) {
            if (this.items.size() != 1 || !isCorrectItem((CouponItem) this.items.get(0))) {
                List<CouponItem> sectionList = new LinkedList<>();
                if (this.items.size() != 1 || !StringUtils.isEmpty(((CouponItem) this.items.get(0)).getId())) {
                    hasNoData = false;
                }
                if (hasNoData) {
                    ((CouponItem) this.items.get(0)).setTitle(getString(R.string.no_coupons));
                    sectionList.add(getWrappedItem((CommonListEntity) this.items.get(0), sectionList));
                } else {
                    for (CouponItem item : this.items) {
                        if (item.getId().equalsIgnoreCase(this.itemId)) {
                            this.preLoadedItem = item;
                        }
                        if (updateFromDB && (couponData = StorageKeeper.instance().getCouponData(item.getId())) != null) {
                            couponData.copyTo(item);
                        }
                        sectionList.add(getWrappedItem(item, sectionList));
                    }
                }
                this.listView.setAdapter((ListAdapter) new CouponAdapter(holdActivity.getApplicationContext(), sectionList));
                initListViewListener();
                return;
            }
            CouponItem item2 = (CouponItem) this.items.get(0);
            CouponItem couponData2 = StorageKeeper.instance().getCouponData(item2.getId());
            if (couponData2 != null) {
                couponData2.copyTo(item2);
            }
            startCouponDetailActivity(item2);
            getHoldActivity().finish();
        }
    }

    private boolean isCorrectItem(CouponItem item) {
        return item != null && StringUtils.isNotEmpty(item.getId());
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.CouponItem r0 = (com.biznessapps.model.CouponItem) r0
            r2.startCouponDetailActivity(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.coupons.CouponsListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (1 == resultCode) {
            if (intent != null && intent.getExtras() != null) {
                CouponItem couponExtra = (CouponItem) intent.getSerializableExtra(AppConstants.COUPON_EXTRA);
                for (CouponItem coupon : this.items) {
                    if (coupon != null && coupon.getId().equalsIgnoreCase(couponExtra.getId())) {
                        couponExtra.copyTo(coupon);
                        plugListView(getHoldActivity(), false);
                        return;
                    }
                }
            }
        } else if (intent != null && requestCode == 1) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            if (StringUtils.isNotEmpty(contents)) {
                int checkinResult = increaseCheckin(contents);
                if (checkinResult == 1) {
                    ViewUtils.showDialog(getHoldActivity(), R.string.have_unlocked_coupon);
                } else if (checkinResult == 0) {
                    ViewUtils.showDialog(getHoldActivity(), R.string.checking_more);
                } else if (checkinResult == -1) {
                    ViewUtils.showDialog(getHoldActivity(), R.string.qr_unsuccess_scanning);
                }
            }
        }
    }

    private void startCouponDetailActivity(CouponItem item) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        if (item != null && StringUtils.isNotEmpty(item.getId())) {
            intent.putExtra(AppConstants.COUPON_EXTRA, item);
            intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.COUPON_DETAIL_FRAGMENT);
            intent.putExtra(AppConstants.QR_COUPON_TYPE, this.isQrCoupons);
            intent.putExtra(AppConstants.TAB_LABEL, item.getTitle());
            startActivityForResult(intent, 1);
        }
    }

    private int increaseCheckin(String code) {
        boolean checkinWasChanged = false;
        boolean couponUnlocked = false;
        boolean incorrectCouponInterval = false;
        for (CouponItem coupon : this.items) {
            if (code.equalsIgnoreCase(coupon.getCode())) {
                if (System.currentTimeMillis() - coupon.getLastCheckinTime() < ((long) (coupon.getCheckinInterval() * 3600000))) {
                    ViewUtils.showDialog(getHoldActivity(), String.format(getResources().getString(R.string.checkin_interval), Integer.valueOf(coupon.getCheckinInterval() * 1)));
                    incorrectCouponInterval = true;
                } else {
                    int checkinCount = coupon.getCheckinTarget();
                    if (checkinCount > 0) {
                        int checkinCount2 = checkinCount - 1;
                        if (checkinCount2 == 0) {
                            couponUnlocked = true;
                        }
                        coupon.setCheckinTarget(checkinCount2);
                        checkinWasChanged = true;
                        coupon.setLastCheckinTime(System.currentTimeMillis());
                        storeCoupon(coupon);
                    }
                }
            }
        }
        if (checkinWasChanged) {
            plugListView(getHoldActivity(), false);
        }
        if (couponUnlocked) {
            return 1;
        }
        if (checkinWasChanged) {
            return 0;
        }
        if (incorrectCouponInterval) {
            return -2;
        }
        return -1;
    }

    private void storeCoupon(CouponItem coupon) {
        List<CouponItem> dataToStore = new ArrayList<>();
        dataToStore.add(coupon);
        StorageKeeper.instance().addCoupons(dataToStore);
    }

    public void onResume() {
        super.onResume();
        if (!this.isQrCoupons) {
            AppCore.getInstance().getLocationFinder().startSearching();
        }
    }

    public void onStop() {
        super.onStop();
        if (!this.isQrCoupons) {
            AppCore.getInstance().getLocationFinder().stopSearching();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (!this.isQrCoupons) {
            AppCore.getInstance().getLocationFinder().removeLocationListener(getLocationListener());
        }
    }

    private LocationListener getLocationListener() {
        if (this.locListener == null) {
            this.locListener = new LocationListener() {
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }

                public void onLocationChanged(Location location) {
                    CouponsListFragment.this.updateLocationData(location);
                }
            };
        }
        return this.locListener;
    }

    /* access modifiers changed from: private */
    public void updateLocationData(Location location) {
        if (location != null) {
            this.currentLocation = location;
        }
        float distance = 0.0f;
        for (CouponItem.CouponsLocation checkin : this.checkinLocations) {
            Location locItem = new Location("");
            try {
                locItem.setLatitude(Double.parseDouble(checkin.getLatitude()));
                locItem.setLongitude(Double.parseDouble(checkin.getLongitude()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            float currentDistance = location.distanceTo(locItem);
            if (currentDistance < distance || distance == 0.0f) {
                distance = currentDistance;
            }
        }
        float feetsDistance = distance * 3.28f;
        if (feetsDistance > 5280.0f) {
            String format = getResources().getString(R.string.n_miles_from_location);
            this.locationTextView.setText(String.format(format, Float.valueOf(feetsDistance / 5280.0f)));
            return;
        }
        String format2 = getResources().getString(R.string.n_feet_from_location);
        this.locationTextView.setText(String.format(format2, Float.valueOf(feetsDistance)));
    }

    /* access modifiers changed from: private */
    public int updateGpsCheckins() {
        boolean checkinWasChanged = false;
        boolean couponUnlocked = false;
        for (CouponItem coupon : this.items) {
            if (coupon != null && coupon.getLocations() != null) {
                Iterator i$ = coupon.getLocations().iterator();
                while (true) {
                    if (!i$.hasNext()) {
                        break;
                    }
                    CouponItem.CouponsLocation item = i$.next();
                    if (!StringUtils.checkTextFieldsOnEmpty(item.getLatitude(), item.getLongitude())) {
                        Location locItem = new Location("");
                        try {
                            locItem.setLatitude(Double.parseDouble(item.getLatitude()));
                            locItem.setLongitude(Double.parseDouble(item.getLongitude()));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (this.currentLocation.distanceTo(locItem) >= 487.80487f) {
                            continue;
                        } else if (System.currentTimeMillis() - coupon.getLastCheckinTime() < ((long) (coupon.getCheckinInterval() * 3600000))) {
                            ViewUtils.showDialog(getHoldActivity(), String.format(getResources().getString(R.string.checkin_interval), Integer.valueOf(coupon.getCheckinInterval() * 1)));
                        } else {
                            int checkinCount = coupon.getCheckinTarget();
                            if (checkinCount > 0) {
                                int checkinCount2 = checkinCount - 1;
                                if (checkinCount2 == 0) {
                                    couponUnlocked = true;
                                }
                                coupon.setCheckinTarget(checkinCount2);
                                coupon.setLastCheckinTime(System.currentTimeMillis());
                                checkinWasChanged = true;
                                storeCoupon(coupon);
                            }
                        }
                    }
                }
            }
        }
        if (checkinWasChanged) {
            plugListView(getHoldActivity(), false);
        }
        if (couponUnlocked) {
            return 1;
        }
        if (checkinWasChanged) {
            return 0;
        }
        return -1;
    }

    private void initCheckinButtonListener() {
        this.checkinButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (CouponsListFragment.this.currentLocation == null) {
                    ViewUtils.showDialog(CouponsListFragment.this.getHoldActivity(), R.string.location_absent);
                    return;
                }
                int checkinResult = CouponsListFragment.this.updateGpsCheckins();
                if (checkinResult > 0) {
                    ViewUtils.showDialog(CouponsListFragment.this.getHoldActivity(), R.string.have_unlocked_coupon);
                } else if (checkinResult == 0) {
                    ViewUtils.showDialog(CouponsListFragment.this.getHoldActivity(), R.string.checking_more);
                } else {
                    ViewUtils.showDialog(CouponsListFragment.this.getHoldActivity(), CouponsListFragment.this.getString(R.string.checking_unsuccess), new Runnable() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                         arg types: [java.lang.String, int]
                         candidates:
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                          ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                        public void run() {
                            ArrayList<String> longitudes = new ArrayList<>();
                            ArrayList<String> latitudes = new ArrayList<>();
                            ArrayList<String> locationsName = new ArrayList<>();
                            Intent showLocations = new Intent(CouponsListFragment.this.getApplicationContext(), LocationView.class);
                            if (CouponsListFragment.this.currentLocation != null) {
                                showLocations.putExtra(LocationView.HAS_USER_LOCATION, true);
                                longitudes.add("" + CouponsListFragment.this.currentLocation.getLongitude());
                                latitudes.add("" + CouponsListFragment.this.currentLocation.getLatitude());
                                locationsName.add(CouponsListFragment.this.getString(R.string.your_location));
                            }
                            for (CouponItem.CouponsLocation checkin : CouponsListFragment.this.checkinLocations) {
                                longitudes.add("" + checkin.getLongitude());
                                latitudes.add("" + checkin.getLatitude());
                                locationsName.add(checkin.getCouponName());
                            }
                            if (longitudes.size() > 0) {
                                showLocations.putStringArrayListExtra(LocationView.LATITUDES, latitudes);
                                showLocations.putStringArrayListExtra(LocationView.LONGITUDES, longitudes);
                                showLocations.putStringArrayListExtra(LocationView.LOCATIONS_NAME, locationsName);
                                CouponsListFragment.this.startActivity(showLocations);
                            }
                        }
                    });
                }
            }
        });
    }

    private void initCheckinLocations() {
        for (CouponItem coupon : this.items) {
            List<CouponItem.CouponsLocation> couponLocations = coupon.getLocations();
            if (couponLocations != null) {
                for (CouponItem.CouponsLocation item : couponLocations) {
                    if (!StringUtils.checkTextFieldsOnEmpty(item.getLatitude(), item.getLongitude())) {
                        item.setCouponName(coupon.getTitle());
                        this.checkinLocations.add(item);
                    }
                }
            }
        }
    }

    private void showLocationAbsentDialog() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getHoldActivity());
        alertBuilder.setView(ViewUtils.loadLayout(getApplicationContext(), R.layout.common_dialog_layout));
        alertBuilder.setMessage(R.string.location_absent);
        AlertDialog dialog = alertBuilder.create();
        dialog.setButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }
}
