package com.biznessapps.fragments.webview;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.activities.WebSingleFragmentActivity;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.layout.adapters.CommonAdapter;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.WebTierItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedList;
import java.util.List;

public class WebViewTiersFragment extends CommonListFragment<WebTierItem> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.WEB_TIERS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseWebTiers(dataToParse);
        return cacher().saveData(CachingConstants.WEB_TIERS_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.WEB_TIERS_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    private void plugListView(Activity holdActivity) {
        boolean hasNoData;
        if (this.items != null && !this.items.isEmpty()) {
            WebTierItem preLoadedItem = null;
            List<WebTierItem> webTiers = new LinkedList<>();
            if (this.items.size() != 1 || !StringUtils.isEmpty(((WebTierItem) this.items.get(0)).getId())) {
                hasNoData = false;
            } else {
                hasNoData = true;
            }
            if (hasNoData) {
                ((WebTierItem) this.items.get(0)).setTitle(getString(R.string.no_categories));
                webTiers.add(getWrappedItem((CommonListEntity) this.items.get(0), webTiers));
            } else if (this.items.size() == 1) {
                openWebView(holdActivity, (WebTierItem) this.items.get(0));
                holdActivity.finish();
            } else {
                for (WebTierItem item : this.items) {
                    if (item.getId().equalsIgnoreCase(this.itemId)) {
                        preLoadedItem = item;
                    }
                    webTiers.add(getWrappedItem(item, webTiers));
                }
            }
            this.listView.setAdapter((ListAdapter) new CommonAdapter(holdActivity.getApplicationContext(), webTiers));
            initListViewListener();
            openWebView(holdActivity, preLoadedItem);
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.WebTierItem r0 = (com.biznessapps.model.WebTierItem) r0
            com.biznessapps.activities.CommonFragmentActivity r1 = r2.getHoldActivity()
            r2.openWebView(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.webview.WebViewTiersFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void openWebView(Activity holdActivity, WebTierItem item) {
        if (item != null && StringUtils.isNotEmpty(item.getId())) {
            Intent intent = new Intent(holdActivity.getApplicationContext(), WebSingleFragmentActivity.class);
            String url = item.getUrl();
            if (StringUtils.isEmpty(url)) {
                url = getIntent().getStringExtra(AppConstants.URL);
            }
            if (StringUtils.isNotEmpty(url)) {
                intent.putExtra(AppConstants.URL, url);
                intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
                intent.putExtra(AppConstants.TAB_ID, ((CommonFragmentActivity) holdActivity).getTabId());
                intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
                startActivity(intent);
                return;
            }
            Toast.makeText(holdActivity.getApplicationContext(), R.string.data_not_available, 1).show();
            holdActivity.finish();
        }
    }
}
