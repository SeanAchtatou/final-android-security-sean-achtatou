package com.biznessapps.fragments.webview;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.MailTo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.GeolocationPermissions;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.Toast;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.activities.CommonTabFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.List;

public class WebViewFragment extends CommonFragment {
    private static final int FILECHOOSER_RESULTCODE = 1;
    private static final String IMAGE_BROWSER = "Image Browser";
    private List<String> customUrls = new ArrayList();
    /* access modifiers changed from: private */
    public List<String> historyUrls = new ArrayList();
    private CommonFragmentActivity.BackPressed onBackPressedListener;
    private CommonFragmentActivity.BackPressed onBackPressedListenerForHtml5;
    /* access modifiers changed from: private */
    public View progressBar;
    /* access modifiers changed from: private */
    public ValueCallback<Uri> uploadMessage;
    /* access modifiers changed from: private */
    public String url;
    /* access modifiers changed from: private */
    public String webData;
    /* access modifiers changed from: private */
    public WebView webView;

    public WebViewFragment() {
        this.customUrls.add("http://cevadesign.ro/magic.html");
        this.customUrls.add("http://cevadesign.ro/magic-fm.html");
        this.onBackPressedListener = new CommonFragmentActivity.BackPressed() {
            public boolean onBackPressed() {
                boolean errorIsHappened;
                boolean canGoBack;
                int sizeBeforeChanging = WebViewFragment.this.historyUrls.size();
                if (!AppConstants.BLANK_PAGE_TAG.equalsIgnoreCase(WebViewFragment.this.webView.getUrl())) {
                    WebViewFragment.this.historyUrls.remove(WebViewFragment.this.webView.getUrl());
                } else {
                    WebViewFragment.this.historyUrls.remove(WebViewFragment.this.webData);
                }
                int sizeAfterChanging = WebViewFragment.this.historyUrls.size();
                if (WebViewFragment.this.historyUrls.size() == 1 && sizeBeforeChanging == sizeAfterChanging) {
                    errorIsHappened = true;
                } else {
                    errorIsHappened = false;
                }
                if ((WebViewFragment.this.webView == null || !WebViewFragment.this.webView.canGoBack()) && !errorIsHappened) {
                    canGoBack = false;
                } else {
                    canGoBack = true;
                }
                if (canGoBack) {
                    WebViewFragment.this.webView.goBack();
                }
                if (WebViewFragment.this.historyUrls.size() == 1 && WebViewFragment.this.historyUrls.contains(WebViewFragment.this.webData)) {
                    WebViewFragment.this.loadContent();
                }
                if (WebViewFragment.this.historyUrls.isEmpty() || errorIsHappened) {
                    return false;
                }
                return true;
            }
        };
        this.onBackPressedListenerForHtml5 = new CommonFragmentActivity.BackPressed() {
            public boolean onBackPressed() {
                boolean canGoBack = WebViewFragment.this.webView != null && WebViewFragment.this.webView.canGoBack();
                if (canGoBack) {
                    WebViewFragment.this.webView.goBack();
                }
                return canGoBack;
            }
        };
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == 1 && this.uploadMessage != null) {
            this.uploadMessage.onReceiveValue((intent == null || resultCode != -1) ? null : intent.getData());
            this.uploadMessage = null;
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.webview_layout, (ViewGroup) null);
        this.progressBar = ViewUtils.getProgressBar(getApplicationContext());
        this.progressBar.setVisibility(8);
        getHoldActivity().getProgressBarContainer().addView(this.progressBar);
        initControls(this.root);
        if (StringUtils.isNotEmpty(this.url)) {
            getHoldActivity().addBackPressedListener(this.onBackPressedListenerForHtml5);
        } else {
            getHoldActivity().addBackPressedListener(this.onBackPressedListener);
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if (activity != null && (activity instanceof CommonTabFragmentActivity)) {
            ((CommonTabFragmentActivity) activity).getViewPager().enableScrolling(false);
        }
    }

    public void onDestroy() {
        CommonFragmentActivity activity = getHoldActivity();
        if (activity != null) {
            activity.removeBackPressedListener(this.onBackPressedListener);
        }
        super.onDestroy();
    }

    private void initControls(ViewGroup root) {
        this.webView = (WebView) root.findViewById(R.id.webview);
        ((ImageButton) root.findViewById(R.id.webview_back_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                WebViewFragment.this.getHoldActivity().finish();
            }
        });
        AppCore.UiSettings uiSettings = AppCore.getInstance().getUiSettings();
        this.webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String urlToLoad, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setType(mimetype);
                intent.setData(Uri.parse(urlToLoad));
                WebViewFragment.this.startActivity(intent);
            }
        });
        this.webData = getIntent().getStringExtra(AppConstants.WEB_DATA);
        if (getIntent().getStringExtra(AppConstants.URL) != null) {
            this.progressBar.setVisibility(0);
            this.url = getIntent().getExtras().getString(AppConstants.URL);
            this.webView.setWebViewClient(new WebViewClient() {
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url == null) {
                        return false;
                    }
                    if (url.contains(AppConstants.MAILTO_TYPE)) {
                        MailTo mt = MailTo.parse(url);
                        ViewUtils.email(WebViewFragment.this.getHoldActivity(), new String[]{mt.getTo()}, mt.getSubject(), mt.getBody());
                        return true;
                    } else if (url.contains(AppConstants.TEL_TYPE)) {
                        WebViewFragment.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(url)));
                        return true;
                    } else if (url.endsWith(AppConstants.PDF_TYPE)) {
                        Intent intent = new Intent("android.intent.action.VIEW");
                        intent.setData(Uri.parse(AppConstants.GOOGLE_DOCS_WRAPPER + url));
                        WebViewFragment.this.startActivity(intent);
                        return false;
                    } else {
                        if (!WebViewFragment.this.historyUrls.contains(url) && !url.contains("=&c=0")) {
                            WebViewFragment.this.historyUrls.add(url);
                        }
                        if (WebViewFragment.this.webData != null && !WebViewFragment.this.historyUrls.contains(WebViewFragment.this.webData)) {
                            WebViewFragment.this.historyUrls.add(WebViewFragment.this.webData);
                        }
                        return super.shouldOverrideUrlLoading(view, url);
                    }
                }

                public void onPageFinished(WebView view, String url) {
                    WebViewFragment.this.progressBar.setVisibility(8);
                }
            });
            if (getIntent().getBooleanExtra(AppConstants.SHOW_WEB_ORIGINAL_SIZE, false) || this.url.contains("wufoo.com")) {
                ViewUtils.plubWebView(this.webView);
            } else {
                ViewUtils.plubWebViewWithoutZooming(this.webView);
            }
            this.webView.setWebChromeClient(new WebChromeClient() {
                public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
                    callback.invoke(origin, true, false);
                }

                public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                    openFileChooser(uploadMsg);
                }

                public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                    openFileChooser(uploadMsg);
                }

                public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                    ValueCallback unused = WebViewFragment.this.uploadMessage = uploadMsg;
                    Intent i = new Intent("android.intent.action.GET_CONTENT");
                    i.addCategory("android.intent.category.OPENABLE");
                    i.setType("*/*");
                    WebViewFragment.this.startActivityForResult(Intent.createChooser(i, WebViewFragment.IMAGE_BROWSER), 1);
                }
            });
            if (this.url.endsWith(AppConstants.PDF_TYPE)) {
                this.url = AppConstants.GOOGLE_DOCS_WRAPPER + this.url;
            } else if (this.url.contains("youtube") && this.url.contains("v=")) {
                int indexOfVideoId = this.url.indexOf("v=");
                if (indexOfVideoId != -1) {
                    String videoId = this.url.substring(indexOfVideoId);
                    try {
                        Intent videoClient = new Intent("android.intent.action.VIEW");
                        videoClient.setData(Uri.parse("http://youtube.com/watch?" + videoId));
                        startActivity(videoClient);
                    } catch (ActivityNotFoundException e) {
                        e.printStackTrace();
                    }
                    getHoldActivity().finish();
                    return;
                }
            } else if (this.url.contains(AppConstants.YOUTUBE) || this.url.contains("http://snd.sc/") || this.url.contains("soundcloud.com") || this.url.contains("twitter.com") || this.url.contains(AppConstants.MP3) || this.url.contains("m3u") || this.customUrls.contains(this.url) || this.url.contains(AppConstants.VIMEO) || this.url.contains("peaceloveworld.com") || this.url.contains("benswann.com")) {
                Intent externalIntent = new Intent("android.intent.action.VIEW");
                externalIntent.setData(Uri.parse(this.url));
                startActivity(externalIntent);
                getHoldActivity().finish();
                return;
            } else if (this.url.contains(AppConstants.PLS)) {
                new AsyncTask<Void, Void, List<String>>() {
                    /* access modifiers changed from: protected */
                    public /* bridge */ /* synthetic */ void onPostExecute(Object x0) {
                        onPostExecute((List<String>) ((List) x0));
                    }

                    /* access modifiers changed from: protected */
                    public List<String> doInBackground(Void... params) {
                        return CommonUtils.getUrlsFromPlsStream(WebViewFragment.this.url);
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(List<String> streams) {
                        if (streams != null && !streams.isEmpty()) {
                            Intent externalIntent = new Intent("android.intent.action.VIEW");
                            externalIntent.setData(Uri.parse(streams.get(0)));
                            WebViewFragment.this.startActivity(externalIntent);
                            WebViewFragment.this.getHoldActivity().finish();
                        }
                    }
                }.execute(new Void[0]);
                return;
            }
            this.webView.loadUrl(this.url);
        } else if (StringUtils.isNotEmpty(this.webData)) {
            this.historyUrls.add(this.webData);
            loadContent();
        } else {
            Toast.makeText(getApplicationContext(), R.string.data_not_available, 0).show();
            getHoldActivity().finish();
        }
    }

    /* access modifiers changed from: private */
    public void loadContent() {
        if (StringUtils.isNotEmpty(this.webData)) {
            ViewUtils.plubWebView(this.webView);
            this.webView.loadDataWithBaseURL(null, StringUtils.decode(this.webData), AppConstants.TEXT_HTML, AppConstants.UTF_8_CHARSET, null);
        }
    }
}
