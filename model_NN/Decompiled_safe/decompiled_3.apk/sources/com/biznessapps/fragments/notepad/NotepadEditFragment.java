package com.biznessapps.fragments.notepad;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.storage.StorageKeeper;
import com.biznessapps.utils.CommonUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NotepadEditFragment extends CommonFragment implements View.OnClickListener {
    private TextView currentDateString;
    /* access modifiers changed from: private */
    public NotepadItem currentNote;
    private int currentNotePosition = 0;
    private SimpleDateFormat dateFormat = new SimpleDateFormat(AppConstants.NOTEPAD_DATE_FORMAT);
    private TextView dayString;
    private Button deleteBtn;
    private Button leftBtn;
    private ImageButton newNoteBtn;
    private EditText noteEditText;
    private List<NotepadItem> notelist;
    private Button rightBtn;
    private Button sendMailBtn;
    /* access modifiers changed from: private */
    public boolean shouldNotBeSaved;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.notepad_edit_view, (ViewGroup) null);
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        initViews(this.root);
        initListeners();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.rightBtn = (Button) root.findViewById(R.id.arrow_right);
        this.leftBtn = (Button) root.findViewById(R.id.arrow_left);
        this.deleteBtn = (Button) root.findViewById(R.id.note_delete_btn);
        this.deleteBtn = (Button) root.findViewById(R.id.note_delete_btn);
        this.sendMailBtn = (Button) root.findViewById(R.id.note_send_btn);
        this.newNoteBtn = (ImageButton) root.findViewById(R.id.edit_note_add_note);
        this.dayString = (TextView) root.findViewById(R.id.day_name);
        this.currentDateString = (TextView) root.findViewById(R.id.full_date);
        this.noteEditText = (EditText) root.findViewById(R.id.edit_text);
    }

    private void initListeners() {
        this.rightBtn.setOnClickListener(this);
        this.leftBtn.setOnClickListener(this);
        this.deleteBtn.setOnClickListener(this);
        this.sendMailBtn.setOnClickListener(this);
        this.newNoteBtn.setOnClickListener(this);
    }

    public void loadData() {
        this.notelist = (List) AppCore.getInstance().getCachingManager().getData(CachingConstants.NOTEPAD_NOTE_LIST + this.tabId);
        if (this.notelist == null) {
            this.notelist = new ArrayList();
        }
        this.currentNote = (NotepadItem) getIntent().getSerializableExtra(AppConstants.EDIT_NOTE_EXTRA);
        this.currentNotePosition = getIntent().getIntExtra(AppConstants.LIST_POSITION_EXTRA, 0);
        if (this.currentNote != null) {
            loadNoteData(this.currentNote);
        } else {
            setNewNoteData();
        }
    }

    private void loadNoteData(NotepadItem item) {
        if (item != null) {
            try {
                if (item.getDate() > 0) {
                    Calendar itemCalendar = Calendar.getInstance();
                    itemCalendar.setTime(new Date(item.getDate()));
                    Calendar todayCalendar = Calendar.getInstance();
                    todayCalendar.setTime(new Date());
                    if (itemCalendar.get(5) == todayCalendar.get(5) && itemCalendar.get(2) == todayCalendar.get(2)) {
                        this.dayString.setText(R.string.today);
                    } else {
                        this.dayString.setText(this.dateFormat.format(itemCalendar.getTime()));
                    }
                    this.currentDateString.setText(this.dateFormat.format(itemCalendar.getTime()));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            this.noteEditText.setText(item.getContent());
        }
        updateNavigationButtons();
    }

    private void updateNavigationButtons() {
        if (this.currentNotePosition < this.notelist.size() - 1) {
            this.rightBtn.setBackgroundResource(R.drawable.arrow_right);
            this.rightBtn.setEnabled(true);
        }
        if (this.currentNotePosition > 0) {
            this.leftBtn.setBackgroundResource(R.drawable.arrow_left);
            this.leftBtn.setEnabled(true);
        }
        if (this.currentNotePosition == 0) {
            this.leftBtn.setBackgroundResource(R.drawable.arrow_left_disabled);
            this.leftBtn.setEnabled(false);
        }
        if (this.currentNotePosition == this.notelist.size() - 1) {
            this.rightBtn.setBackgroundResource(R.drawable.arrow_right_disabled);
            this.rightBtn.setEnabled(false);
        }
    }

    private void setNewNoteData() {
        this.currentNote = null;
        this.leftBtn.setBackgroundResource(R.drawable.arrow_left_disabled);
        this.leftBtn.setEnabled(false);
        this.rightBtn.setBackgroundResource(R.drawable.arrow_right_disabled);
        this.rightBtn.setEnabled(false);
        this.dayString.setText(R.string.today);
        this.noteEditText.setText("");
        try {
            this.currentDateString.setText(this.dateFormat.format(new Date()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onClick(View v) {
        if (v == this.deleteBtn) {
            if (this.currentNote != null) {
                showDeleteDialog();
                return;
            }
            this.shouldNotBeSaved = true;
            getHoldActivity().finish();
        } else if (v == this.sendMailBtn) {
            showSendNote();
        } else if (v == this.newNoteBtn) {
            saveCurrentNote();
            this.shouldNotBeSaved = true;
            getHoldActivity().finish();
        } else if (v == this.rightBtn) {
            saveCurrentNote();
            this.currentNotePosition++;
            if (this.currentNotePosition > this.notelist.size()) {
                this.currentNotePosition = this.notelist.size() - 1;
            }
            this.currentNote = this.notelist.get(this.currentNotePosition);
            loadNoteData(this.currentNote);
        } else if (v == this.leftBtn) {
            saveCurrentNote();
            this.currentNotePosition--;
            if (this.currentNotePosition < 0) {
                this.currentNotePosition = 0;
            }
            this.currentNote = this.notelist.get(this.currentNotePosition);
            loadNoteData(this.currentNote);
        }
    }

    private void saveCurrentNote() {
        if (this.currentNote == null) {
            String noteStr = this.noteEditText.getText().toString();
            if (noteStr != null && noteStr.length() != 0) {
                NotepadItem tempNote = new NotepadItem();
                String title = noteStr.split(" ")[0];
                if (title == null || title.length() == 0) {
                    tempNote.setTitle(noteStr);
                } else {
                    tempNote.setTitle(title);
                }
                tempNote.setContent(noteStr);
                tempNote.setDate(System.currentTimeMillis());
                List<NotepadItem> saveNoteList = new ArrayList<>();
                saveNoteList.add(tempNote);
                StorageKeeper.instance().addNotes(saveNoteList);
            }
        } else if (this.notelist.get(this.currentNotePosition) != null) {
            this.currentNote.setContent(this.noteEditText.getText().toString());
            this.notelist.get(this.currentNotePosition).setContent(this.noteEditText.getText().toString());
            ArrayList<NotepadItem> saveNoteList2 = new ArrayList<>();
            saveNoteList2.add(this.currentNote);
            StorageKeeper.instance().addNotes(saveNoteList2);
        }
    }

    private void showSendNote() {
        Intent emailIntent = new Intent("android.intent.action.SEND");
        emailIntent.setType(AppConstants.PLAIN_TEXT);
        emailIntent.putExtra("android.intent.extra.SUBJECT", getString(R.string.my_note_email_title));
        emailIntent.putExtra("android.intent.extra.TEXT", this.noteEditText.getText().toString());
        startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    private void showDeleteDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int choice) {
                if (choice == -1) {
                    StorageKeeper.instance().delNote(NotepadEditFragment.this.currentNote);
                    boolean unused = NotepadEditFragment.this.shouldNotBeSaved = true;
                    NotepadEditFragment.this.getHoldActivity().finish();
                }
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getHoldActivity());
        builder.setMessage(R.string.delete_note);
        builder.setPositiveButton(R.string.yes, dialogClickListener);
        builder.setNegativeButton(R.string.no, dialogClickListener);
        builder.show();
    }

    public void onPause() {
        super.onPause();
        if (!this.shouldNotBeSaved) {
            saveCurrentNote();
        }
    }
}
