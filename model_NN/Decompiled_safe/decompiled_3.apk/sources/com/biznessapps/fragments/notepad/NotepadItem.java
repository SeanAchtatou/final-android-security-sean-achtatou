package com.biznessapps.fragments.notepad;

import com.biznessapps.model.CommonListEntity;

public class NotepadItem extends CommonListEntity {
    private static final long serialVersionUID = -1528621246322105807L;
    private String content;
    private long date;
    private String title;

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title2) {
        this.title = title2;
    }

    public long getDate() {
        return this.date;
    }

    public void setDate(long date2) {
        this.date = date2;
    }
}
