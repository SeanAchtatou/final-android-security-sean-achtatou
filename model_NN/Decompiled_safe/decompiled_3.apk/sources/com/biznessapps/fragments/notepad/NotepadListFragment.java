package com.biznessapps.fragments.notepad;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.storage.StorageKeeper;
import com.biznessapps.widgets.RefreshableListView;

public class NotepadListFragment extends CommonListFragment<NotepadItem> {
    private ImageButton addNoteButton;

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        loadData();
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.listView = (RefreshableListView) ((ViewGroup) root.findViewById(R.id.list_view_root)).findViewById(R.id.list_view);
        this.listView.setItemsCanFocus(false);
        this.addNoteButton = (ImageButton) root.findViewById(R.id.add_note);
        this.addNoteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NotepadListFragment.this.openNotepad(null, 0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = StorageKeeper.instance().getNotes();
        AppCore.getInstance().getCachingManager().saveData(CachingConstants.NOTEPAD_NOTE_LIST + this.tabId, this.items);
        return true;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.notepad_list_view;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.fragments.notepad.NotepadItem r0 = (com.biznessapps.fragments.notepad.NotepadItem) r0
            r2.openNotepad(r0, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.notepad.NotepadListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        if (this.items != null) {
            this.adapter = new NotepadListAdapter(holdActivity.getApplicationContext(), this.items);
            this.listView.setAdapter((ListAdapter) this.adapter);
            initListViewListener();
        }
    }

    /* access modifiers changed from: private */
    public void openNotepad(NotepadItem item, int position) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
        intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.NOTEPAD_EDIT_FRAGMENT);
        if (item != null) {
            intent.putExtra(AppConstants.EDIT_NOTE_EXTRA, item);
            intent.putExtra(AppConstants.LIST_POSITION_EXTRA, position);
        }
        startActivityForResult(intent, 6);
    }
}
