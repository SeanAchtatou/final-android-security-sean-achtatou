package com.biznessapps.fragments.notepad;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NotepadListAdapter extends AbstractAdapter<NotepadItem> {
    private SimpleDateFormat dateFormat = new SimpleDateFormat(AppConstants.NOTEPAD_DATE_FORMAT);
    private Calendar todayCalendar = Calendar.getInstance();

    public NotepadListAdapter(Context context, List<NotepadItem> items) {
        super(context, items, R.layout.notepad_list_row);
        this.todayCalendar.setTime(new Date());
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.NotepadItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.NotepadItem();
            holder.setNoteTitleView((TextView) convertView.findViewById(R.id.note_title));
            holder.setNoteDateView((TextView) convertView.findViewById(R.id.note_date));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.NotepadItem) convertView.getTag();
        }
        NotepadItem item = (NotepadItem) this.items.get(position);
        if (item != null) {
            holder.getNoteTitleView().setText(item.getTitle());
            long dateStr = item.getDate();
            if (dateStr > 0) {
                try {
                    Calendar itemCalendar = Calendar.getInstance();
                    itemCalendar.setTime(new Date(dateStr));
                    if (itemCalendar.get(5) == this.todayCalendar.get(5) && itemCalendar.get(2) == this.todayCalendar.get(2)) {
                        holder.getNoteDateView().setText(R.string.today);
                    } else {
                        holder.getNoteDateView().setText(this.dateFormat.format(itemCalendar.getTime()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return convertView;
    }
}
