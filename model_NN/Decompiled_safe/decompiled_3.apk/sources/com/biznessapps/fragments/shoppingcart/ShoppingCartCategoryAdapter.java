package com.biznessapps.fragments.shoppingcart;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.fragments.shoppingcart.entities.Category;
import com.biznessapps.layout.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ShoppingCartCategoryAdapter extends AbstractAdapter<Category> {
    private categoryFilter filter;
    /* access modifiers changed from: private */
    public ArrayList<Category> filteredItems;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    /* access modifiers changed from: private */
    public ArrayList<Category> originalItems = new ArrayList<>();

    public ShoppingCartCategoryAdapter(Context context, List<Category> items) {
        super(context, items, R.layout.common_row);
        cloneItems(items);
    }

    public void cloneItems(List<Category> items) {
        for (Category categoryItem : items) {
            if (!this.originalItems.contains(categoryItem)) {
                this.originalItems.add(categoryItem);
            }
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
        }
        Category item = (Category) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(item.getTitle());
        }
        if (item.hasColor()) {
            convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
            setTextColorToView(item.getItemTextColor(), holder.getTextViewTitle());
        }
        return convertView;
    }

    public Filter getFilter() {
        if (this.filter == null) {
            this.filter = new categoryFilter();
        }
        return this.filter;
    }

    public ArrayList<Category> getOriginalItems() {
        return this.originalItems;
    }

    public ArrayList<Category> getFilteredItems() {
        return this.filteredItems;
    }

    private class categoryFilter extends Filter {
        private categoryFilter() {
        }

        /* access modifiers changed from: protected */
        public Filter.FilterResults performFiltering(CharSequence prefix) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (prefix == null || prefix.length() == 0) {
                synchronized (ShoppingCartCategoryAdapter.this.mLock) {
                    filterResults.values = ShoppingCartCategoryAdapter.this.originalItems;
                    filterResults.count = ShoppingCartCategoryAdapter.this.originalItems.size();
                }
            } else {
                synchronized (ShoppingCartCategoryAdapter.this.mLock) {
                    String prefixString = prefix.toString().toLowerCase();
                    ArrayList unused = ShoppingCartCategoryAdapter.this.filteredItems = new ArrayList();
                    ArrayList<Category> localItems = new ArrayList<>();
                    localItems.addAll(ShoppingCartCategoryAdapter.this.originalItems);
                    int count = localItems.size();
                    for (int i = 0; i < count; i++) {
                        Category item = (Category) localItems.get(i);
                        String itemName = item.getTitle().toLowerCase();
                        if (itemName.startsWith(prefixString)) {
                            ShoppingCartCategoryAdapter.this.filteredItems.add(item);
                        } else {
                            String[] words = itemName.split(" ");
                            int wordCount = words.length;
                            int k = 0;
                            while (true) {
                                if (k >= wordCount) {
                                    break;
                                } else if (words[k].startsWith(prefixString)) {
                                    ShoppingCartCategoryAdapter.this.filteredItems.add(item);
                                    break;
                                } else {
                                    k++;
                                }
                            }
                        }
                    }
                    filterResults.values = ShoppingCartCategoryAdapter.this.filteredItems;
                    filterResults.count = ShoppingCartCategoryAdapter.this.filteredItems.size();
                }
            }
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence constraint, Filter.FilterResults results) {
            synchronized (ShoppingCartCategoryAdapter.this.mLock) {
                ShoppingCartCategoryAdapter.this.notifyDataSetChanged();
                ShoppingCartCategoryAdapter.this.clear();
                Iterator<Category> iterator = ((ArrayList) results.values).iterator();
                while (iterator.hasNext()) {
                    ShoppingCartCategoryAdapter.this.add((Category) iterator.next());
                }
            }
        }
    }
}
