package com.biznessapps.fragments.shoppingcart;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class ShoppingCartCheckoutAdapter extends AbstractAdapter<Product> {
    /* access modifiers changed from: private */
    public ShoppingCart cart;
    private View.OnClickListener removeProductClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Product product = (Product) v.getTag();
            ShoppingCartCheckoutAdapter.this.items.remove(product);
            ShoppingCartCheckoutAdapter.this.notifyDataSetChanged();
            ShoppingCartCheckoutAdapter.this.cart.getCheckoutProducts().remove(product);
            ShoppingCartCheckoutAdapter.this.shoppingCartCheckoutView.updateCheckoutTotals();
        }
    };
    /* access modifiers changed from: private */
    public ShoppingCartCheckoutFragment shoppingCartCheckoutView;
    private View.OnClickListener updateProductQuantityClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            Integer quantity = Integer.valueOf(Integer.parseInt(((EditText) v.getTag(R.id.quantity_edit_text)).getText().toString()));
            ShoppingCartCheckoutAdapter.this.cart.getCheckoutProducts().put((Product) v.getTag(), quantity);
            ShoppingCartCheckoutAdapter.this.shoppingCartCheckoutView.updateCheckoutTotals();
        }
    };

    public ShoppingCartCheckoutAdapter(Context context, List<Product> items, ShoppingCartCheckoutFragment shoppingCartCheckoutView2) {
        super(context, items, R.layout.shop_checkout_row);
        this.shoppingCartCheckoutView = shoppingCartCheckoutView2;
        this.cart = ShoppingCart.getInstance();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.ShoppingCartCheckoutItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.ShoppingCartCheckoutItem();
            holder.setProductNameView((TextView) convertView.findViewById(R.id.product_name));
            holder.setProductPriceView((TextView) convertView.findViewById(R.id.product_price));
            holder.setSmallImageView((ImageView) convertView.findViewById(R.id.checkout_product_image));
            holder.setQuantityView((EditText) convertView.findViewById(R.id.quantity_edit_text));
            holder.setUpdateButton((Button) convertView.findViewById(R.id.update_btn));
            holder.setRemoveButton((Button) convertView.findViewById(R.id.remove_btn));
            holder.getRemoveButton().setTextColor(this.settings.getButtonTextColor());
            holder.getUpdateButton().setTextColor(this.settings.getButtonTextColor());
            CommonUtils.overrideMediumButtonColor(this.settings.getButtonBgColor(), holder.getRemoveButton().getBackground());
            CommonUtils.overrideMediumButtonColor(this.settings.getButtonBgColor(), holder.getUpdateButton().getBackground());
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.ShoppingCartCheckoutItem) convertView.getTag();
        }
        Product item = (Product) this.items.get(position);
        if (item != null) {
            holder.getProductNameView().setText(item.getTitle());
            holder.getProductPriceView().setText(ShoppingCart.getInstance().getStore().getCurrencySign() + String.valueOf(item.getProductPrice()));
            holder.getQuantityView().setText(this.cart.getCheckoutProducts().get(item).toString());
            holder.getUpdateButton().setTag(item);
            holder.getUpdateButton().setTag(R.id.quantity_edit_text, holder.getQuantityView());
            holder.getUpdateButton().setOnClickListener(this.updateProductQuantityClickListener);
            holder.getRemoveButton().setTag(item);
            holder.getRemoveButton().setOnClickListener(this.removeProductClickListener);
            String url = item.getPrimaryImageUrl();
            if (StringUtils.isNotEmpty(url)) {
                try {
                    url = new URI(url.replace(" ", "%20")).toString();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (this.cart.getStore().getStoreName().equalsIgnoreCase(AppConstants.VOLUSION_STORE)) {
                    this.imageFetcher.loadImage(url + AppConstants.JPG_EXT, holder.getProductImageView());
                } else {
                    this.imageFetcher.loadImage(url, holder.getProductImageView());
                }
            } else {
                holder.getProductImageView().setImageResource(R.drawable.product_default);
            }
        }
        return convertView;
    }
}
