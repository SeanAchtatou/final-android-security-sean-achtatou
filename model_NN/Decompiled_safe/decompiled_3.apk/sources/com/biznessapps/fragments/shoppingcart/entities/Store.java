package com.biznessapps.fragments.shoppingcart.entities;

import android.graphics.drawable.Drawable;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.utils.StringUtils;

public class Store {
    private String apiSecret;
    private String apikey;
    private String appCode;
    private String background;
    private Drawable bgDrawable;
    private String currency = AppConstants.DEFAULT_CURRENCY;
    private String currencySign = AppConstants.DEFAULT_CURRENCY_SIGN;
    private String domain;
    private String googleCheckoutMerchantID;
    private String googleCheckoutMerchantKey;
    private String paypalApplicationID;
    private String paypalPassword;
    private String paypalSignature;
    private String paypalUsername;
    private String storeName;
    private float tax;

    public void setApikey(String apikey2) {
        this.apikey = apikey2;
    }

    public String getApikey() {
        return this.apikey;
    }

    public void setApiSecret(String apisecret) {
        this.apiSecret = apisecret;
    }

    public String getApiSecret() {
        return this.apiSecret;
    }

    public void setDomain(String domain2) {
        this.domain = domain2;
    }

    public String getDomain() {
        return this.domain;
    }

    public void setStoreName(String storeName2) {
        this.storeName = storeName2;
    }

    public String getStoreName() {
        return this.storeName;
    }

    public void setBackgroundURL(String value) {
        this.background = value;
    }

    public String getBackgroundUrl() {
        return this.background;
    }

    public String getPaypalApplicationID() {
        return this.paypalApplicationID;
    }

    public void setPaypalApplicationID(String paypalApplicationID2) {
        this.paypalApplicationID = paypalApplicationID2;
    }

    public String getPaypalUsername() {
        return this.paypalUsername;
    }

    public void setPaypalUsername(String paypalUsername2) {
        this.paypalUsername = paypalUsername2;
    }

    public String getPaypalPassword() {
        return this.paypalPassword;
    }

    public void setPaypalPassword(String paypalPassword2) {
        this.paypalPassword = paypalPassword2;
    }

    public String getPaypalSignature() {
        return this.paypalSignature;
    }

    public void setPaypalSignature(String paypalSignature2) {
        this.paypalSignature = paypalSignature2;
    }

    public Drawable getBackgroundDrawable() {
        return this.bgDrawable;
    }

    public void setBackgroundDrawable(Drawable d) {
        this.bgDrawable = d;
    }

    public void setGoogleCheckoutMerchantID(String id) {
        this.googleCheckoutMerchantID = id;
    }

    public String getGoogleCheckoutMerchantID() {
        return this.googleCheckoutMerchantID;
    }

    public void setGoogleCheckoutMerchantKey(String key) {
        this.googleCheckoutMerchantKey = key;
    }

    public String getGoogleCheckoutMerchantKey() {
        return this.googleCheckoutMerchantKey;
    }

    public void setAppCode(String appCode2) {
        this.appCode = appCode2;
    }

    public String getAppCode() {
        return this.appCode;
    }

    public float getTax() {
        return this.tax;
    }

    public void setTax(float tax2) {
        this.tax = tax2 / 100.0f;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency2) {
        if (StringUtils.isNotEmpty(currency2)) {
            this.currency = currency2;
        }
    }

    public String getCurrencySign() {
        return this.currencySign;
    }

    public void setCurrencySign(String currencySign2) {
        if (StringUtils.isNotEmpty(currencySign2)) {
            this.currencySign = currencySign2;
        }
    }
}
