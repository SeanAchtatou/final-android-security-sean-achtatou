package com.biznessapps.fragments.shoppingcart;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ShoppingCartProductsAdapter extends AbstractAdapter<Product> {
    private ShoppingCart cart;
    private productFilter filter;
    /* access modifiers changed from: private */
    public List<Product> filteredItems;
    /* access modifiers changed from: private */
    public final Object mLock = new Object();
    /* access modifiers changed from: private */
    public List<Product> originalItems = new ArrayList();

    public ShoppingCartProductsAdapter(Context context, List<Product> items) {
        super(context, items, R.layout.shop_product_row);
        cloneItems(items);
        this.cart = ShoppingCart.getInstance();
    }

    /* access modifiers changed from: protected */
    public void cloneItems(List<Product> items) {
        for (Product productItem : items) {
            getOriginalItems().add(productItem);
        }
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.ShoppingCartProductItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.ShoppingCartProductItem();
            holder.setProductNameView((TextView) convertView.findViewById(R.id.product_name));
            holder.setProductPriceView((TextView) convertView.findViewById(R.id.product_price));
            holder.setProductPriceLabelView((TextView) convertView.findViewById(R.id.product_price_label));
            holder.setProductSmallImageView((ImageView) convertView.findViewById(R.id.product_small_image));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.ShoppingCartProductItem) convertView.getTag();
        }
        Product item = (Product) this.items.get(position);
        if (item != null) {
            holder.getProductNameView().setText(item.getTitle());
            if (item.hasColor()) {
                convertView.setBackgroundDrawable(getListItemDrawable(item.getItemColor()));
                setTextColorToView(item.getItemTextColor(), holder.getProductNameView(), holder.getProductPriceView(), holder.getProductPriceLabelView());
            }
            float price = item.getProductPrice();
            String currencySign = ShoppingCart.getInstance().getStore().getCurrencySign();
            holder.getProductPriceView().setText(currencySign + String.format("%.2f", Float.valueOf(price)));
            String url = item.getPrimaryImageUrl();
            if (StringUtils.isNotEmpty(url)) {
                try {
                    url = new URI(url.replace(" ", "%20")).toString();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (this.cart.getStore().getStoreName().equalsIgnoreCase(AppConstants.VOLUSION_STORE)) {
                    this.imageFetcher.loadImage(url + AppConstants.JPG_EXT, holder.getProductSmallImageView());
                } else if (this.cart.getStore().getStoreName().equalsIgnoreCase(AppConstants.CUSTOME_CART_STORE)) {
                    this.imageFetcher.loadImage(url, holder.getProductSmallImageView());
                } else {
                    this.imageFetcher.loadImage(url, holder.getProductSmallImageView());
                }
            } else {
                holder.getProductSmallImageView().setImageResource(R.drawable.product_default);
            }
        }
        return convertView;
    }

    public Filter getFilter() {
        if (this.filter == null) {
            this.filter = new productFilter();
        }
        return this.filter;
    }

    public List<Product> getFilteredItems() {
        return this.filteredItems;
    }

    public List<Product> getOriginalItems() {
        return this.originalItems;
    }

    private class productFilter extends Filter {
        private productFilter() {
        }

        /* access modifiers changed from: protected */
        public Filter.FilterResults performFiltering(CharSequence prefix) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (prefix == null || prefix.length() == 0) {
                synchronized (ShoppingCartProductsAdapter.this.mLock) {
                    filterResults.values = ShoppingCartProductsAdapter.this.originalItems;
                    filterResults.count = ShoppingCartProductsAdapter.this.originalItems.size();
                }
            } else {
                synchronized (ShoppingCartProductsAdapter.this.mLock) {
                    String prefixString = prefix.toString().toLowerCase();
                    List unused = ShoppingCartProductsAdapter.this.filteredItems = new ArrayList();
                    ArrayList<Product> localItems = new ArrayList<>();
                    localItems.addAll(ShoppingCartProductsAdapter.this.originalItems);
                    int count = localItems.size();
                    for (int i = 0; i < count; i++) {
                        Product item = (Product) localItems.get(i);
                        String itemName = item.getTitle().toLowerCase();
                        if (itemName.startsWith(prefixString)) {
                            ShoppingCartProductsAdapter.this.filteredItems.add(item);
                        } else {
                            String[] words = itemName.split(" ");
                            int wordCount = words.length;
                            int k = 0;
                            while (true) {
                                if (k >= wordCount) {
                                    break;
                                } else if (words[k].startsWith(prefixString)) {
                                    ShoppingCartProductsAdapter.this.filteredItems.add(item);
                                    break;
                                } else {
                                    k++;
                                }
                            }
                        }
                    }
                    filterResults.values = ShoppingCartProductsAdapter.this.filteredItems;
                    filterResults.count = ShoppingCartProductsAdapter.this.filteredItems.size();
                }
            }
            return filterResults;
        }

        /* access modifiers changed from: protected */
        public void publishResults(CharSequence constraint, Filter.FilterResults results) {
            synchronized (ShoppingCartProductsAdapter.this.mLock) {
                ShoppingCartProductsAdapter.this.notifyDataSetChanged();
                ShoppingCartProductsAdapter.this.clear();
                Iterator<Product> iterator = ((ArrayList) results.values).iterator();
                while (iterator.hasNext()) {
                    ShoppingCartProductsAdapter.this.add((Product) iterator.next());
                }
            }
        }
    }

    public void sortListDataByName() {
        sort(new Comparator<Product>() {
            public int compare(Product lhs, Product rhs) {
                return lhs.getTitle().compareTo(rhs.getTitle());
            }
        });
        notifyDataSetChanged();
    }

    public void sortListDataByPrice() {
        sort(new Comparator<Product>() {
            public int compare(Product lhs, Product rhs) {
                int result = 0;
                if (lhs.getProductPrice() > rhs.getProductPrice()) {
                    result = 1;
                }
                if (lhs.getProductPrice() < rhs.getProductPrice()) {
                    return -1;
                }
                return result;
            }
        });
        notifyDataSetChanged();
    }
}
