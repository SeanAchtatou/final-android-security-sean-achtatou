package com.biznessapps.fragments.shoppingcart.utils;

import com.biznessapps.fragments.shoppingcart.ShoppingCartCheckoutFragment;
import com.paypal.android.MEP.PayPalResultDelegate;
import java.io.Serializable;

public class PaypalResultDelegate implements PayPalResultDelegate, Serializable {
    private static final long serialVersionUID = 10001;

    public void onPaymentSucceeded(String payKey, String paymentStatus) {
        ShoppingCartCheckoutFragment.resultTitle = "SUCCESS";
        ShoppingCartCheckoutFragment.resultInfo = "You have successfully completed your transaction.";
        ShoppingCartCheckoutFragment.resultExtra = "Key: " + payKey;
    }

    public void onPaymentFailed(String paymentStatus, String correlationID, String payKey, String errorID, String errorMessage) {
        ShoppingCartCheckoutFragment.resultTitle = "FAILURE";
        ShoppingCartCheckoutFragment.resultInfo = errorMessage;
        ShoppingCartCheckoutFragment.resultExtra = "Error ID: " + errorID + "\nCorrelation ID: " + correlationID + "\nPay Key: " + payKey;
    }

    public void onPaymentCanceled(String paymentStatus) {
        ShoppingCartCheckoutFragment.resultTitle = "CANCELED";
        ShoppingCartCheckoutFragment.resultInfo = "The transaction has been cancelled.";
        ShoppingCartCheckoutFragment.resultExtra = "";
    }
}
