package com.biznessapps.fragments.shoppingcart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.fragments.shoppingcart.entities.Category;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.entities.Store;
import com.biznessapps.fragments.shoppingcart.utils.JSONUtils;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.fragments.shoppingcart.utils.VolusionCategoriesHandler;
import com.biznessapps.fragments.shoppingcart.utils.XMLUtils;
import com.biznessapps.layout.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ShoppingCartCategoryFragmentNew extends CommonListFragment<Category> {
    private ViewGroup availableCategoriesLabel;
    private ShoppingCart cart = ShoppingCart.getInstance();
    private Map<String, List<Product>> categoryProductsMap;
    private Store currentStore;
    private EditText filterText;
    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (ShoppingCartCategoryFragmentNew.this.adapter != null) {
                ShoppingCartCategoryFragmentNew.this.adapter.getFilter().filter(s);
                ShoppingCartCategoryFragmentNew.this.adapter.notifyDataSetChanged();
                if (s == null || s.length() == 0) {
                    boolean unused = ShoppingCartCategoryFragmentNew.this.isListFiltered = false;
                } else {
                    boolean unused2 = ShoppingCartCategoryFragmentNew.this.isListFiltered = true;
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public boolean isListFiltered;
    private List<Store> stores;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.listView.setBackgroundColor(getAppCore().getUiSettings().getGlobalBgColor());
        ((TextView) this.root.findViewById(R.id.cart_counter_text)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShoppingCartCategoryFragmentNew.this.showCheckOutActivity();
            }
        });
        ((ImageView) this.root.findViewById(R.id.cart_image)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShoppingCartCategoryFragmentNew.this.showCheckOutActivity();
            }
        });
        this.filterText = (EditText) this.root.findViewById(R.id.shop_search_box);
        this.filterText.addTextChangedListener(this.filterTextWatcher);
        this.availableCategoriesLabel = (ViewGroup) this.root.findViewById(R.id.available_categories_header);
        this.availableCategoriesLabel.setBackgroundColor(getUiSettings().getSectionBarColor());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.SHOPPING_CART_STORES_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.stores = JSONUtils.parseStoreInfoList(dataToParse);
        this.categoryProductsMap = new HashMap();
        loadStoreData();
        this.currentStore = this.cart.getStore();
        cacher().saveData(CachingConstants.SHOPPING_PRODUCTS_MAP_PROPERTY + this.tabId, this.categoryProductsMap);
        return cacher().saveData(CachingConstants.SHOPPING_CATEGORY_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.categoryProductsMap = (Map) cacher().getData(CachingConstants.SHOPPING_PRODUCTS_MAP_PROPERTY + this.tabId);
        this.items = (List) cacher().getData(CachingConstants.SHOPPING_CATEGORY_LIST_PROPERTY + this.tabId);
        return (this.items == null || this.categoryProductsMap == null) ? false : true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return this.currentStore != null ? this.currentStore.getBackgroundUrl() : "";
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.shop_category_list;
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String categoryName;
        if (!this.isListFiltered) {
            categoryName = ((ShoppingCartCategoryAdapter) this.adapter).getOriginalItems().get(position).getTitle();
        } else {
            categoryName = ((ShoppingCartCategoryAdapter) this.adapter).getFilteredItems().get(position).getTitle();
        }
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        intent.putExtra(AppConstants.TAB_LABEL, categoryName);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.SHOPPING_CART_PRODUCTS_LIST);
        if (this.categoryProductsMap != null && this.categoryProductsMap.get(categoryName) != null) {
            this.cart.setCurrentProductList(this.categoryProductsMap.get(categoryName));
            startActivity(intent);
        }
    }

    private void plugListView(Activity holdActivity) {
        if (this.items != null && !this.items.isEmpty()) {
            List<Category> categories = new ArrayList<>();
            for (Category item : this.items) {
                categories.add(getWrappedItem(item, categories));
            }
            this.adapter = new ShoppingCartCategoryAdapter(holdActivity.getApplicationContext(), categories);
            this.listView.setAdapter((ListAdapter) this.adapter);
            this.listView.setTextFilterEnabled(true);
            initListViewListener();
        }
    }

    public void onResume() {
        super.onResume();
        ((TextView) this.root.findViewById(R.id.cart_counter_text)).setText(String.valueOf(this.cart.getCheckoutProducts().size()));
    }

    /* access modifiers changed from: private */
    public void showCheckOutActivity() {
        if (this.cart.getCheckoutProducts().size() > 0) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
            intent.putExtra(AppConstants.TAB_LABEL, AppConstants.CHECKOUT);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.SHOPPING_CART_CHECKOUT);
            startActivity(intent);
            return;
        }
        Toast.makeText(getHoldActivity(), "Your cart is Empty", 0).show();
    }

    private void loadStoreData() {
        if (!this.stores.isEmpty()) {
            this.currentStore = this.stores.get(0);
            this.cart.setStore(this.currentStore);
            if (AppConstants.VOLUSION_STORE.equalsIgnoreCase(this.currentStore.getStoreName())) {
                VolusionCategoriesHandler categoriesHandler = XMLUtils.parseVolusionCategories(String.format(AppConstants.SHOPPING_CART_CATEGORIES_VOLUSION_STORE, this.currentStore.getDomain(), this.currentStore.getApikey(), this.currentStore.getApiSecret()), String.format(AppConstants.SHOPPING_CART_VOLUSION_STORE_FEATURED_PRODUCTS, this.currentStore.getDomain(), this.currentStore.getApikey(), this.currentStore.getApiSecret()));
                this.items = new ArrayList(categoriesHandler.getCategories());
                this.categoryProductsMap = categoriesHandler.getCategoryProducts();
            } else if (AppConstants.CUSTOME_CART_STORE.equalsIgnoreCase(this.currentStore.getStoreName())) {
                this.items = JSONUtils.parseCustomeCartCategoryList(String.format(AppConstants.SHOPPING_CART_CATEGORIES_CUSTOME_STORE, cacher().getAppCode(), this.tabId));
                JSONUtils.parseCustomeCartProductList(cacher().getAppCode(), this.tabId, this.categoryProductsMap, this.items);
            } else if (AppConstants.SHOPIFY_STORE.equalsIgnoreCase(this.currentStore.getStoreName())) {
                JSONUtils.getShopifyCategories(DataSource.getInstance().getData(String.format(AppConstants.SHOPIFY_CATEGORIES_URL_FORMAT, this.currentStore.getApikey(), this.currentStore.getApiSecret(), this.currentStore.getDomain()), this.currentStore.getApikey(), this.currentStore.getApiSecret()), this.categoryProductsMap);
                parsedShopifyItems(this.categoryProductsMap);
            }
        }
    }

    private void parsedShopifyItems(Map<String, List<Product>> productsMap) {
        Set<String> keys = productsMap.keySet();
        this.items = new ArrayList();
        for (String key : keys) {
            this.items.add(new Category(key));
        }
    }
}
