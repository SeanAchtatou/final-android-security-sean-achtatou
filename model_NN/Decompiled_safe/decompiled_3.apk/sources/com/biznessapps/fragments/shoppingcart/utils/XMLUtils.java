package com.biznessapps.fragments.shoppingcart.utils;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.SAXParserFactory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class XMLUtils {
    private static final String CATEGORY = "Category";
    private static final String CATEGORY_ID = "CategoryID";
    private static final String CATEGORY_NAME = "CategoryName";
    private static final String PRODUCT = "Product";
    private static final String PRODUCT_CODE = "ProductCode";
    private static final String PRODUCT_DESCRIPTION = "ProductDescription";
    private static final String PRODUCT_IMAGE = "PhotoURL";
    private static final String PRODUCT_NAME = "ProductName";
    private static final String PRODUCT_PRICE = "ProductPrice";
    private static final String PRODUCT_SMALL_IMAGE = "PhotoURLSmall";
    public static boolean volusionStorelistHaveMoreData;

    public static VolusionCategoriesHandler parseVolusionCategories(String volusionCategoriesUrl, String productsUrl) {
        VolusionProductsHandler productsHandler = parseVolusionFeaturedProducts(productsUrl);
        VolusionCategoriesHandler categoriesHandler = new VolusionCategoriesHandler();
        categoriesHandler.setFeaturedProductsMap(productsHandler.getProducts());
        try {
            BufferedInputStream bis = new BufferedInputStream(new BufferedInputStream(((HttpURLConnection) new URL(volusionCategoriesUrl).openConnection()).getInputStream()));
            SAXParserFactory.newInstance().newSAXParser().parse(bis, categoriesHandler);
            bis.close();
        } catch (Exception e) {
            Log.e("PARSE", e.getMessage());
        }
        return categoriesHandler;
    }

    private static VolusionProductsHandler parseVolusionFeaturedProducts(String urlString) {
        VolusionProductsHandler handler = new VolusionProductsHandler();
        try {
            BufferedInputStream bis = new BufferedInputStream(new BufferedInputStream(((HttpURLConnection) new URL(urlString).openConnection()).getInputStream()));
            SAXParserFactory.newInstance().newSAXParser().parse(bis, handler);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return handler;
    }

    public static String ParseGoogleCheckoutResponseUrl(String response) {
        String url = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(new StringReader(response));
            for (int eventType = xpp.getEventType(); eventType != 1; eventType = xpp.next()) {
                if (eventType == 2 && xpp.getName().equalsIgnoreCase("redirect-url")) {
                    url = xpp.nextText();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        }
        return url;
    }
}
