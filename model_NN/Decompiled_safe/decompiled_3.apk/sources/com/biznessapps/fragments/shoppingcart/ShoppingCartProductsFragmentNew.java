package com.biznessapps.fragments.shoppingcart;

import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.widgets.RefreshableListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShoppingCartProductsFragmentNew extends CommonListFragment<Product> implements View.OnClickListener {
    private ShoppingCart cart = ShoppingCart.getInstance();
    private TextView cartCounterTextView;
    private EditText filterText;
    private TextWatcher filterTextWatcher = new TextWatcher() {
        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            ShoppingCartProductsFragmentNew.this.adapter.getFilter().filter(s);
            ShoppingCartProductsFragmentNew.this.adapter.notifyDataSetChanged();
        }
    };
    private String tabId;

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.cartCounterTextView = (TextView) root.findViewById(R.id.cart_counter_text);
        ((ImageView) root.findViewById(R.id.cart_image)).setOnClickListener(this);
        this.cartCounterTextView.setOnClickListener(this);
        ((Button) root.findViewById(R.id.list_header_name_btn)).setOnClickListener(this);
        ((Button) root.findViewById(R.id.list_header_price_btn)).setOnClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = this.cart.getCurrentProductList();
        return true;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.cart.getStore() != null && StringUtils.isNotEmpty(this.cart.getStore().getBackgroundUrl())) {
            return this.cart.getStore().getBackgroundUrl();
        }
        this.listView.setBackgroundColor(getAppCore().getUiSettings().getGlobalBgColor());
        return null;
    }

    private void plugListView(Activity holdActivity) {
        if (this.items != null && !this.items.isEmpty()) {
            List<Product> products = new ArrayList<>();
            for (Product item : this.items) {
                products.add(getWrappedItem(item, products));
            }
            this.adapter = new ShoppingCartProductsAdapter(holdActivity.getApplicationContext(), products);
            this.listView.setAdapter((ListAdapter) this.adapter);
            this.listView.setTextFilterEnabled(true);
            initListViewListener();
            this.listView.requestFocus();
        }
        initSearchFilter();
    }

    private void initSearchFilter() {
        this.adapter = (ShoppingCartProductsAdapter) this.adapter;
        this.filterText = (EditText) this.root.findViewById(R.id.shop_search_box);
        this.filterText.addTextChangedListener(this.filterTextWatcher);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.shop_products_list;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r6, android.view.View r7, int r8, long r9) {
        /*
            r5 = this;
            android.widget.Adapter r2 = r6.getAdapter()
            java.lang.Object r1 = r2.getItem(r8)
            com.biznessapps.fragments.shoppingcart.entities.Product r1 = (com.biznessapps.fragments.shoppingcart.entities.Product) r1
            if (r1 == 0) goto L_0x0045
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r2 = r5.getApplicationContext()
            java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r3 = com.biznessapps.activities.SingleFragmentActivity.class
            r0.<init>(r2, r3)
            java.lang.String r2 = "TAB_UNIQUE_ID"
            java.lang.String r3 = r5.tabId
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_SPECIAL_ID"
            android.content.Intent r3 = r5.getIntent()
            java.lang.String r4 = "TAB_SPECIAL_ID"
            java.lang.String r3 = r3.getStringExtra(r4)
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_LABEL"
            java.lang.String r3 = r1.getTitle()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_FRAGMENT"
            java.lang.String r3 = "SHOPPPING_CART_PRODUCTS_DETAILS"
            r0.putExtra(r2, r3)
            java.lang.String r2 = "productDetails"
            r0.putExtra(r2, r1)
            r5.startActivity(r0)
        L_0x0045:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.shoppingcart.ShoppingCartProductsFragmentNew.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.list_header_name_btn) {
            Collections.sort(this.items, new Comparator<Product>() {
                public int compare(Product lhs, Product rhs) {
                    return lhs.getTitle().compareTo(rhs.getTitle());
                }
            });
            Collections.sort(this.items, new SortProductsByName());
            plugListView(getActivity());
        } else if (id == R.id.list_header_price_btn) {
            Collections.sort(this.items, new Comparator<Product>() {
                public int compare(Product lhs, Product rhs) {
                    int result = 0;
                    if (lhs.getProductPrice() > rhs.getProductPrice()) {
                        result = 1;
                    }
                    if (lhs.getProductPrice() < rhs.getProductPrice()) {
                        return -1;
                    }
                    return result;
                }
            });
            plugListView(getActivity());
        } else if (id == R.id.cart_image) {
            showCheckOutActivity();
        } else if (id == R.id.cart_counter_text) {
            showCheckOutActivity();
        }
    }

    private void showCheckOutActivity() {
        if (this.cart.getCheckoutProducts().size() > 0) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_ID, this.tabId);
            intent.putExtra(AppConstants.TAB_LABEL, AppConstants.CHECKOUT);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.SHOPPING_CART_CHECKOUT);
            startActivity(intent);
            return;
        }
        Toast.makeText(getHoldActivity(), R.string.cart_is_empty, 0).show();
    }

    public void onResume() {
        super.onResume();
        this.cartCounterTextView.setText(String.valueOf(this.cart.getCheckoutProducts().size()));
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        this.root = (ViewGroup) this.root.findViewById(R.id.list_view_root);
        this.listView = (RefreshableListView) this.root.findViewById(R.id.list_view);
        this.listView.setItemsCanFocus(false);
    }

    private class SortProductsByName implements Comparator<Product> {
        private SortProductsByName() {
        }

        public int compare(Product o1, Product o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return 1;
            }
            if (o2 == null) {
                return -1;
            }
            if (StringUtils.isEmpty(o1.getTitle())) {
                return 1;
            }
            if (StringUtils.isEmpty(o2.getTitle())) {
                return -1;
            }
            return o1.getTitle().compareToIgnoreCase(o2.getTitle());
        }
    }
}
