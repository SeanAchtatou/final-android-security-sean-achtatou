package com.biznessapps.fragments.shoppingcart;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.net.URLEncoder;
import java.util.Map;

public class GoogleCheckoutFragment extends CommonFragment {
    private static final String CHECKOUT_END = " </checkout-shopping-cart>";
    private static final String CHECKOUT_START = "<checkout-shopping-cart xmlns='http://checkout.google.com/schema/2'>  ";
    private static final String CONFIRMATION = "confirmation";
    private static final String ITEM_DESCRIPTION_TAG = "<item-description>%s</item-description>  ";
    private static final String ITEM_END = " </item> ";
    private static final String ITEM_NAME_TAG = "<item-name>%s</item-name>  ";
    private static final String ITEM_PRICE_TAG = "<unit-price currency='%s'>%s</unit-price>  ";
    private static final String ITEM_QUANTITY_TAG = "<quantity>%d</quantity>";
    private static final String ITEM_START = " <item> ";
    private static final String RATE_TAG = "<checkout-flow-support> <merchant-checkout-flow-support> <tax-tables> <default-tax-table> <tax-rules> <default-tax-rule> <shipping-taxed>true</shipping-taxed> <rate>%s</rate> <tax-area> <world-area/> </tax-area> </default-tax-rule> </tax-rules> </default-tax-table> </tax-tables> </merchant-checkout-flow-support> </checkout-flow-support>";
    private static final String SHOPPING_CART_ITEMS = "<shopping-cart>  <items>  ";
    private static final String SHPPING_CART_ITEMS_END = "  </items>  </shopping-cart>";
    private Map<Product, Integer> checkoutProducts;
    private String checkoutUrl;
    private String currency;
    private String merchantId;
    private String merchantKey;
    private float taxRate;
    private String transactionId;
    private WebView webView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.shop_google_checkout, (ViewGroup) null);
        this.webView = (WebView) this.root.findViewById(R.id.google_checkout_webview);
        this.webView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (StringUtils.isNotEmpty(url) && url.toLowerCase().contains(GoogleCheckoutFragment.CONFIRMATION)) {
                    GoogleCheckoutFragment.this.paymentSucceed();
                }
            }
        });
        ViewUtils.plubWebViewWithoutZooming(this.webView);
        loadData();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.currency = holdActivity.getIntent().getStringExtra(AppConstants.CURRENCY_EXTRA);
        this.merchantKey = holdActivity.getIntent().getStringExtra(AppConstants.MERCHANT_KEY_EXTRA);
        this.merchantId = holdActivity.getIntent().getStringExtra(AppConstants.MERCHANT_ID_EXTRA);
        this.checkoutProducts = (Map) holdActivity.getIntent().getSerializableExtra(AppConstants.CHECKOUT_PRODUCTS_EXTRA);
        this.taxRate = holdActivity.getIntent().getFloatExtra(AppConstants.TAX_EXTRA, 0.0f);
        this.transactionId = holdActivity.getIntent().getStringExtra(AppConstants.TRANSACTION_ID_EXTRA);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.checkoutUrl = getRedirectUrl();
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        this.webView.loadUrl(this.checkoutUrl);
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    private String getRedirectUrl() {
        return AppHttpUtils.postGoogleCheckoutRequest(AppConstants.GOOGLE_CHECKOUT_CHECKOUT_URL + this.merchantId, this.merchantId, this.merchantKey, composeGoogleCheckoutRequestBody());
    }

    private String composeGoogleCheckoutRequestBody() {
        StringBuffer buffer = new StringBuffer();
        buffer.append(CHECKOUT_START);
        buffer.append(SHOPPING_CART_ITEMS);
        for (Product product : this.checkoutProducts.keySet()) {
            buffer.append(ITEM_START);
            buffer.append(String.format(ITEM_NAME_TAG, product.getTitle()));
            buffer.append(String.format(ITEM_DESCRIPTION_TAG, URLEncoder.encode(product.getDescription())));
            buffer.append(String.format(ITEM_PRICE_TAG, this.currency, "" + product.getProductPrice()));
            buffer.append(String.format(ITEM_QUANTITY_TAG, this.checkoutProducts.get(product)));
            buffer.append(ITEM_END);
        }
        buffer.append(SHPPING_CART_ITEMS_END);
        buffer.append(String.format(RATE_TAG, "" + this.taxRate));
        buffer.append(CHECKOUT_END);
        return buffer.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void paymentSucceed() {
        Activity activity = getHoldActivity();
        Intent intent = new Intent();
        intent.putExtra(AppConstants.PAYMENT_TRANSACTION_ID_EXTRA, this.transactionId);
        intent.putExtra(AppConstants.PAYMENT_METHOD_EXTRA, 2);
        intent.putExtra(AppConstants.PAYMENT_SUCCESS_EXTRA, true);
        activity.setResult(16, intent);
        activity.finish();
    }
}
