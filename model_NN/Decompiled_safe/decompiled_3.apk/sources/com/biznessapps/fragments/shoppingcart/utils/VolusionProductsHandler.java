package com.biznessapps.fragments.shoppingcart.utils;

import com.biznessapps.fragments.shoppingcart.entities.Product;
import java.util.HashMap;
import java.util.Map;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class VolusionProductsHandler extends DefaultHandler {
    private static final String PHOTO_URL = "PhotoURL";
    private static final String PHOTO_URL_SMALL = "PhotoURLSmall";
    private static final String PRODUCT = "Product";
    private static final String PRODUCT_CODE = "ProductCode";
    private static final String PRODUCT_DESCRIPTION_SHORT = "ProductDescriptionShort";
    private static final String PRODUCT_NAME = "ProductName";
    private static final String PRODUCT_PRICE = "ProductPrice";
    private String currentElement;
    private Product currentProduct;
    private Map<String, Product> productsMap = new HashMap();

    public Map<String, Product> getProducts() {
        return this.productsMap;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        if (PRODUCT_CODE.equals(this.currentElement)) {
            this.currentProduct.setId(makeString(ch, start, length));
        } else if (PRODUCT_NAME.equals(this.currentElement)) {
            this.currentProduct.setTitle(makeString(ch, start, length));
        } else if (PRODUCT_PRICE.equals(this.currentElement)) {
            this.currentProduct.setProductPrice(Float.parseFloat(makeString(ch, start, length)));
        } else if (PRODUCT_DESCRIPTION_SHORT.equals(this.currentElement)) {
            this.currentProduct.setDescription(makeString(ch, start, length));
        } else if (PHOTO_URL_SMALL.equals(this.currentElement)) {
            this.currentProduct.setSmallImageUrl(makeString(ch, start, length));
        } else if (PHOTO_URL.equals(this.currentElement)) {
            this.currentProduct.setImageUrl(makeString(ch, start, length));
        }
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        super.endElement(uri, localName, name);
        this.currentElement = null;
        if (PRODUCT.equals(localName)) {
            this.productsMap.put(this.currentProduct.getId(), this.currentProduct);
        }
    }

    public void startDocument() throws SAXException {
        super.startDocument();
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (PRODUCT.equals(localName)) {
            this.currentProduct = new Product();
        } else {
            this.currentElement = localName;
        }
    }

    private static String makeString(char[] ch, int start, int length) {
        StringBuilder bs = new StringBuilder();
        bs.append(ch, start, length);
        return bs.toString();
    }
}
