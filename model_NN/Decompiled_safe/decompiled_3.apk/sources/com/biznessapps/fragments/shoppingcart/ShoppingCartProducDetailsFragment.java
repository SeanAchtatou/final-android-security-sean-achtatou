package com.biznessapps.fragments.shoppingcart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.TellFriendDelegate;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.widgets.TabGallery;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

public class ShoppingCartProducDetailsFragment extends CommonFragment implements View.OnClickListener {
    private Button addToCartButton;
    /* access modifiers changed from: private */
    public ShoppingCart cart;
    private ImageView cartButton;
    private TextView cartCounterTextView;
    private Button postReviewButton;
    private Product product;
    private LinearLayout productDetails;
    private Button readReviewButton;
    private Button shareButton;
    private ViewGroup tellFriendContentView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.shop_product_details, (ViewGroup) null);
        initViews(this.root);
        initListeners();
        if (StringUtils.isNotEmpty(this.cart.getStore().getBackgroundUrl())) {
            this.bgUrl = this.cart.getStore().getBackgroundUrl();
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.productDetails;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.cart = ShoppingCart.getInstance();
        this.product = (Product) getIntent().getSerializableExtra(AppConstants.PRODUCT_DETAILS);
        ((TabGallery) root.findViewById(R.id.product_images_coverflow)).setAdapter((SpinnerAdapter) getCoverFlowAdapter(getApplicationContext(), this.product.getImageUrls()));
        this.productDetails = (LinearLayout) root.findViewById(R.id.product_details);
        TextView description = (TextView) root.findViewById(R.id.product_details_description);
        if (StringUtils.isNotEmpty(this.product.getDescription())) {
            description.setText(Html.fromHtml(this.product.getDescription()));
            description.setTextColor(AppCore.getInstance().getUiSettings().getFeatureTextColor());
        }
        TextView price = (TextView) root.findViewById(R.id.txt_price);
        float p = this.product.getProductPrice();
        price.setText(ShoppingCart.getInstance().getStore().getCurrencySign() + String.valueOf(p));
        this.addToCartButton = (Button) root.findViewById(R.id.add_to_cart_btn);
        this.shareButton = (Button) root.findViewById(R.id.share_product_btn);
        this.readReviewButton = (Button) root.findViewById(R.id.read_reviews_btn);
        this.postReviewButton = (Button) root.findViewById(R.id.post_review_btn);
        this.cartCounterTextView = (TextView) root.findViewById(R.id.cart_counter_text);
        this.cartButton = (ImageView) root.findViewById(R.id.cart_image);
        this.addToCartButton.requestFocus();
        this.tellFriendContentView = (ViewGroup) root.findViewById(R.id.tell_friends_content);
        TellFriendDelegate.initTellFriends(getHoldActivity(), this.tellFriendContentView);
        int sectionTextColor = getUiSettings().getSectionTextColor();
        int sectionBarColor = getUiSettings().getSectionBarColor();
        TextView productImagesLabel = (TextView) root.findViewById(R.id.product_images_label);
        TextView productDescriptionLabel = (TextView) root.findViewById(R.id.product_description_label);
        TextView productCostLabel = (TextView) root.findViewById(R.id.product_cost_label);
        productImagesLabel.setTextColor(sectionTextColor);
        productImagesLabel.setBackgroundColor(sectionBarColor);
        productDescriptionLabel.setTextColor(sectionTextColor);
        productDescriptionLabel.setBackgroundColor(sectionBarColor);
        productCostLabel.setTextColor(sectionTextColor);
        productCostLabel.setBackgroundColor(sectionBarColor);
        price.setTextColor(sectionTextColor);
        price.setBackgroundColor(sectionBarColor);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.addToCartButton.setTextColor(settings.getButtonTextColor());
        this.postReviewButton.setTextColor(settings.getButtonTextColor());
        this.readReviewButton.setTextColor(settings.getButtonTextColor());
        this.shareButton.setTextColor(settings.getButtonTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.addToCartButton.getBackground());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.postReviewButton.getBackground());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.readReviewButton.getBackground());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.shareButton.getBackground());
    }

    private void initListeners() {
        this.shareButton.setOnClickListener(this);
        this.addToCartButton.setOnClickListener(this);
        this.readReviewButton.setOnClickListener(this);
        this.postReviewButton.setOnClickListener(this);
        this.cartCounterTextView.setOnClickListener(this);
        this.cartButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.add_to_cart_btn) {
            this.cart.getCheckoutProducts().put(this.product, 1);
            this.cartCounterTextView.setText(String.valueOf(this.cart.getCheckoutProducts().size()));
            Toast.makeText(getHoldActivity(), R.string.product_is_added, 0).show();
        } else if (id == R.id.cart_image) {
            showCheckOutActivity();
        } else if (id == R.id.cart_counter_text) {
            showCheckOutActivity();
        } else if (id == R.id.share_product_btn) {
            openFriendContent();
        } else if (id == R.id.post_review_btn) {
            openFanWall();
        } else if (id == R.id.read_reviews_btn) {
            openFanWall();
        }
    }

    private void openFriendContent() {
        if (this.tellFriendContentView.getVisibility() == 8) {
            this.tellFriendContentView.setVisibility(0);
            this.tellFriendContentView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.show_tell_friends_dialog));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void openFanWall() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra("parent_id", this.product.getId());
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.FAN_WALL_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.YOUTUBE_MODE, true);
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        startActivity(intent);
    }

    private void showCheckOutActivity() {
        if (this.cart.getCheckoutProducts().size() > 0) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_LABEL, AppConstants.CHECKOUT);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.SHOPPING_CART_CHECKOUT);
            startActivity(intent);
            return;
        }
        Toast.makeText(getHoldActivity(), R.string.cart_is_empty, 0).show();
    }

    public void onResume() {
        super.onResume();
        this.cartCounterTextView.setText(String.valueOf(this.cart.getCheckoutProducts().size()));
    }

    private ArrayAdapter<String> getCoverFlowAdapter(Context appContext, List<String> urls) {
        return new ProductImagesAdapter(appContext, urls);
    }

    private class ProductImagesAdapter extends AbstractAdapter<String> {
        public ProductImagesAdapter(Context context, List<String> items) {
            super(context, items, R.layout.product_images_item);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemHolder.GalleryItem holder;
            if (convertView == null) {
                convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
                holder = new ListItemHolder.GalleryItem();
                holder.setImage((ImageView) convertView.findViewById(R.id.image_view));
                convertView.setTag(holder);
            } else {
                holder = (ListItemHolder.GalleryItem) convertView.getTag();
            }
            String url = (String) this.items.get(position);
            if (StringUtils.isNotEmpty(url)) {
                try {
                    url = new URI(url.replace(" ", "%20")).toString();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                if (ShoppingCartProducDetailsFragment.this.cart.getStore().getStoreName().equalsIgnoreCase(AppConstants.VOLUSION_STORE)) {
                    this.imageFetcher.loadImage(url + AppConstants.JPG_EXT, holder.getImage());
                } else if (ShoppingCartProducDetailsFragment.this.cart.getStore().getStoreName().equalsIgnoreCase(AppConstants.CUSTOME_CART_STORE)) {
                    this.imageFetcher.loadImage(url, holder.getImage());
                } else {
                    this.imageFetcher.loadImage(url, holder.getImage());
                }
            } else {
                holder.getImage().setImageResource(R.drawable.product_default);
            }
            return convertView;
        }
    }
}
