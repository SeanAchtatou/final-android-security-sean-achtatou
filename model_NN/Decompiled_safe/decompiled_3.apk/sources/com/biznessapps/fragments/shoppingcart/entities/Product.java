package com.biznessapps.fragments.shoppingcart.entities;

import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class Product extends CommonListEntity {
    private static final long serialVersionUID = 6409349146813154536L;
    private List<Category> categories = new ArrayList();
    private List<String> imageUrls = new ArrayList();
    private String primaryImageUrl;
    private float productPrice;
    private String productType;
    private String smallImageUrl;
    private String vendor;

    public String getProductType() {
        return this.productType;
    }

    public void setProductType(String productType2) {
        this.productType = productType2;
    }

    public String getVendor() {
        return this.vendor;
    }

    public void setVendor(String vendor2) {
        this.vendor = vendor2;
    }

    public void setProductPrice(float productPrice2) {
        this.productPrice = productPrice2;
    }

    public float getProductPrice() {
        return this.productPrice;
    }

    public String getSmallImageUrl() {
        return this.smallImageUrl;
    }

    public void setSmallImageUrl(String smallImageUrl2) {
        this.smallImageUrl = smallImageUrl2;
    }

    public String getPrimaryImageUrl() {
        if (!StringUtils.isEmpty(this.primaryImageUrl)) {
            return this.primaryImageUrl;
        }
        if (this.imageUrls == null || this.imageUrls.isEmpty()) {
            return null;
        }
        for (String url : this.imageUrls) {
            if (StringUtils.isNotEmpty(url)) {
                return url;
            }
        }
        return null;
    }

    public void setImageUrl(String primaryImageUrl2) {
        this.primaryImageUrl = primaryImageUrl2;
        this.imageUrls.add(primaryImageUrl2);
    }

    public List<String> getImageUrls() {
        return this.imageUrls;
    }

    public void setImageUrls(List<String> imageUrls2) {
        this.imageUrls.addAll(imageUrls2);
    }

    public List<Category> getCategories() {
        return this.categories;
    }

    public void setCategories(List<Category> categories2) {
        this.categories = categories2;
    }
}
