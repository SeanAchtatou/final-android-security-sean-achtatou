package com.biznessapps.fragments.shoppingcart.utils;

import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.entities.Store;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShoppingCart {
    private static ShoppingCart instance;
    private Map<Product, Integer> checkoutProducts = new HashMap();
    private List<Product> currentProductList;
    private Store store;

    private ShoppingCart() {
    }

    public static ShoppingCart getInstance() {
        if (instance == null) {
            instance = new ShoppingCart();
        }
        return instance;
    }

    public List<Product> getCurrentProductList() {
        return this.currentProductList;
    }

    public void setCurrentProductList(List<Product> currentProductList2) {
        this.currentProductList = currentProductList2;
    }

    public void clear() {
        this.checkoutProducts.clear();
    }

    public Map<Product, Integer> getCheckoutProducts() {
        return this.checkoutProducts;
    }

    public Store getStore() {
        return this.store;
    }

    public void setStore(Store store2) {
        this.store = store2;
    }
}
