package com.biznessapps.fragments.shoppingcart.utils;

import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.shoppingcart.entities.Category;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.entities.Store;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSONUtils {
    private static final String API_KEY = "api_key";
    private static final String API_SECRET = "api_secret";
    private static final String BACKGROUND = "background";
    private static final String BASE_DOMAIN = "base_domain";
    private static final String BODY_HTML = "body_html";
    private static final String CATEGORIES = "Catagories";
    private static final String CATEGORY_ID = "catid";
    private static final String CATEGORY_NAME = "catname";
    private static final String CURRENCY = "currency";
    private static final String CURRENCY_SIGN = "currency_sign";
    private static final String ID = "id";
    private static final String IMAGES = "images";
    private static final String MERCHANT_ID = "merchant_id";
    private static final String MERCHANT_KEY = "merchant_key";
    private static final String PAYMENT_GATEWAYS = "PaymentGateways";
    private static final String PGATEWAY_APPLICATION_ID = "pgateway_applicationID";
    private static final String PGATEWAY_PASSWORD = "pgateway_password";
    private static final String PGATEWAY_SIGNATURE = "pgateway_signature";
    private static final String PGATEWAY_USERNAME = "pgateway_username";
    private static final String PRICE = "price";
    private static final String PRODUCTS = "Products";
    private static final String PRODUCT_DESCRIPTION = "product_Description";
    private static final String PRODUCT_ID = "product_id";
    private static final String PRODUCT_IMAGE = "product_image";
    private static final String PRODUCT_NAME = "product_name";
    private static final String PRODUCT_PRICE = "product_prize";
    private static final String PRODUCT_TYPE = "product_type";
    private static final String RESPONSE = "response";
    private static final String SRC = "src";
    private static final String STORE_NAME = "store_name";
    private static final String TAX = "tax";
    private static final String TITLE = "title";
    private static final String VARIANTS = "variants";
    private static final String VENDOR = "vendor";

    public static List<Category> parseCustomeCartCategoryList(String customeStoreUrlString) {
        List<Category> categories = new ArrayList<>();
        Category cat = null;
        try {
            InputStream inputStream = new DefaultHttpClient().execute(new HttpPost(customeStoreUrlString)).getEntity().getContent();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    break;
                }
                sb.append(line + AppConstants.NEW_LINE);
            }
            inputStream.close();
            JSONArray categoriesJsonArray = ((JSONObject) new JSONTokener(sb.toString()).nextValue()).getJSONObject(RESPONSE).getJSONArray(CATEGORIES);
            if (categoriesJsonArray.length() != 0) {
                int i = 0;
                while (i < categoriesJsonArray.length()) {
                    try {
                        JSONObject jsonObj = categoriesJsonArray.getJSONObject(i);
                        cat = new Category();
                        cat.setId(getValue(jsonObj, CATEGORY_ID));
                        cat.setTitle(getValue(jsonObj, CATEGORY_NAME));
                        categories.add(cat);
                        i++;
                    } catch (Exception e) {
                        e = e;
                        System.out.println("Exception Parsing Custome Type Shopping Cart Categories");
                        e.printStackTrace();
                        return categories;
                    }
                }
            }
        } catch (Exception e2) {
            e = e2;
        }
        return categories;
    }

    private static String getValue(JSONObject obj, String key) throws JSONException {
        if (obj.has(key)) {
            return obj.get(key).toString();
        }
        return null;
    }

    public static void parseCustomeCartProductList(String appCode, String tabId, Map<String, List<Product>> categoryProducts, List<Category> categories) {
        for (Category category : categories) {
            try {
                InputStream inputStream = new DefaultHttpClient().execute(new HttpPost(String.format(AppConstants.SHOPPING_CART_PRODUCTS_CUSTOME_STORE, appCode, tabId, category.getId()))).getEntity().getContent();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String line = bufferedReader.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(line + AppConstants.NEW_LINE);
                }
                inputStream.close();
                JSONArray productsJsonArray = ((JSONObject) new JSONTokener(sb.toString()).nextValue()).getJSONObject(RESPONSE).getJSONArray(PRODUCTS);
                if (productsJsonArray.length() != 0) {
                    for (int i = 0; i < productsJsonArray.length(); i++) {
                        JSONObject jsonObj = productsJsonArray.getJSONObject(i);
                        Product product = new Product();
                        product.setId(getValue(jsonObj, PRODUCT_ID));
                        product.setTitle(getValue(jsonObj, PRODUCT_NAME));
                        product.setProductPrice(Float.parseFloat(getValue(jsonObj, PRODUCT_PRICE)));
                        String imageUrl = getValue(jsonObj, PRODUCT_IMAGE);
                        if (imageUrl != null && !imageUrl.startsWith("http")) {
                            imageUrl = "http://www.biznessapps.com" + imageUrl;
                        }
                        product.setImageUrl(imageUrl);
                        product.setSmallImageUrl(imageUrl);
                        product.setDescription(getValue(jsonObj, PRODUCT_DESCRIPTION));
                        if (categoryProducts.get(category.getTitle()) == null) {
                            categoryProducts.put(category.getTitle(), new ArrayList<>());
                        }
                        categoryProducts.get(category.getTitle()).add(product);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static List<Store> parseStoreInfoList(String data) {
        ArrayList<Store> storeList = new ArrayList<>();
        try {
            JSONObject object = (JSONObject) new JSONTokener(data).nextValue();
            JSONObject response = object.getJSONObject(RESPONSE);
            JSONArray storeJsonArray = response.getJSONArray("Stores");
            JSONObject paypalObject = response.getJSONArray(PAYMENT_GATEWAYS).getJSONObject(0);
            float tax = getFloatValue(response, TAX);
            for (int i = 0; i < storeJsonArray.length(); i++) {
                JSONObject jsonObj = storeJsonArray.getJSONObject(i);
                Store storeObj = new Store();
                storeObj.setStoreName(getValue(jsonObj, STORE_NAME));
                storeObj.setApiSecret(getValue(jsonObj, API_SECRET));
                storeObj.setApikey(getValue(jsonObj, API_KEY));
                storeObj.setDomain(getValue(jsonObj, BASE_DOMAIN));
                storeObj.setBackgroundURL(getValue(object, BACKGROUND));
                storeObj.setPaypalApplicationID(getValue(paypalObject, PGATEWAY_APPLICATION_ID));
                storeObj.setPaypalUsername(getValue(paypalObject, PGATEWAY_USERNAME));
                storeObj.setPaypalPassword(getValue(paypalObject, PGATEWAY_PASSWORD));
                storeObj.setPaypalSignature(getValue(paypalObject, PGATEWAY_SIGNATURE));
                storeObj.setGoogleCheckoutMerchantID(getValue(paypalObject, MERCHANT_ID));
                storeObj.setGoogleCheckoutMerchantKey(getValue(paypalObject, MERCHANT_KEY));
                storeObj.setTax(tax);
                storeObj.setCurrency(getValue(paypalObject, CURRENCY));
                storeObj.setCurrencySign(getValue(paypalObject, CURRENCY_SIGN));
                storeList.add(storeObj);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return storeList;
    }

    public static void getShopifyCategories(String jsonData, Map<String, List<Product>> productsMap) {
        List<Product> productsList;
        try {
            JSONArray jsonArray = new JSONObject(jsonData).getJSONArray(PRODUCTS.toLowerCase());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonItem = jsonArray.getJSONObject(i);
                Product product = new Product();
                product.setId(getValue(jsonItem, "id"));
                product.setProductType(getValue(jsonItem, PRODUCT_TYPE));
                product.setVendor(getValue(jsonItem, VENDOR));
                product.setDescription(getValue(jsonItem, BODY_HTML));
                product.setTitle(getValue(jsonItem, "title"));
                String categoryName = getValue(jsonItem, PRODUCT_TYPE);
                List<String> imageUrls = new ArrayList<>();
                JSONArray imagesJSON = jsonItem.getJSONArray(IMAGES);
                for (int j = 0; j < imagesJSON.length(); j++) {
                    imageUrls.add(getValue(imagesJSON.getJSONObject(j), "src"));
                }
                product.setImageUrls(imageUrls);
                JSONArray variantsJSON = jsonItem.getJSONArray(VARIANTS);
                if (0 < variantsJSON.length()) {
                    product.setProductPrice(getFloatValue(variantsJSON.getJSONObject(0), PRICE));
                }
                if (productsMap.get(categoryName) != null) {
                    productsList = productsMap.get(categoryName);
                } else {
                    productsList = new ArrayList<>();
                }
                productsList.add(product);
                productsMap.put(categoryName, productsList);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static float getFloatValue(JSONObject obj, String key) throws JSONException {
        if (!obj.has(key)) {
            return 0.0f;
        }
        try {
            return Float.parseFloat(obj.get(key).toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0f;
        }
    }
}
