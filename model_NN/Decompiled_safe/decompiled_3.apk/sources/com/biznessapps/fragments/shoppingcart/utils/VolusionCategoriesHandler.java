package com.biznessapps.fragments.shoppingcart.utils;

import com.biznessapps.fragments.shoppingcart.entities.Category;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class VolusionCategoriesHandler extends DefaultHandler {
    private static final String CATEGORY = "Category";
    private static final String CATEGORY_ID = "CategoryID";
    private static final String CATEGORY_NAME = "CategoryName";
    private static final String PRODUCT = "Product";
    private static final String PRODUCT_CODE = "ProductCode";
    private static final String PRODUCT_DESCRIPTION = "ProductDescription";
    private static final String PRODUCT_NAME = "ProductName";
    private static final String PRODUCT_PRICE = "ProductPrice";
    private Map<String, Category> categories = new HashMap();
    private Map<String, List<Product>> categoryProducts = new HashMap();
    private Category currentCategory;
    private String currentElement;
    private Product currentProduct;
    private Map<String, Product> featuredProductsMap;
    private List<Product> products = new ArrayList();
    private StringBuilder tagText = new StringBuilder();

    public Collection<Product> getProducts() {
        return this.products;
    }

    public Collection<Category> getCategories() {
        return this.categories.values();
    }

    public Map<String, List<Product>> getCategoryProducts() {
        return this.categoryProducts;
    }

    public void setFeaturedProductsMap(Map<String, Product> featuredProductsMap2) {
        this.featuredProductsMap = featuredProductsMap2;
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        while (length > 0 && (ch[start] == 10 || ch[start] == ' ')) {
            start++;
            length--;
        }
        while (length > 0 && (ch[(start + length) - 1] == 10 || ch[(start + length) - 1] == ' ')) {
            length--;
        }
        if (length > 0) {
            this.tagText.append(ch, start, length);
        }
    }

    public void endElement(String uri, String localName, String name) throws SAXException {
        super.endElement(uri, localName, name);
        if (PRODUCT_CODE.equals(this.currentElement)) {
            this.currentProduct.setId(this.tagText.toString());
            if (!(this.featuredProductsMap == null || this.featuredProductsMap.get(this.currentProduct.getId()) == null)) {
                this.currentProduct.setImageUrl(this.featuredProductsMap.get(this.currentProduct.getId()).getPrimaryImageUrl());
            }
        } else if (PRODUCT_NAME.equals(this.currentElement)) {
            this.currentProduct.setTitle(this.tagText.toString());
        } else if (PRODUCT_DESCRIPTION.equals(this.currentElement)) {
            this.currentProduct.setDescription(this.tagText.toString());
        } else if (PRODUCT_PRICE.equals(this.currentElement)) {
            this.currentProduct.setProductPrice(Float.parseFloat(this.tagText.toString()));
        } else if (CATEGORY_ID.equals(this.currentElement)) {
            this.currentCategory.setId(this.tagText.toString());
        } else if (CATEGORY_NAME.equals(this.currentElement)) {
            this.currentCategory.setTitle(this.tagText.toString());
        }
        this.tagText.delete(0, this.tagText.length());
        this.currentElement = null;
        if (CATEGORY.equals(localName)) {
            String categoryName = this.currentCategory.getTitle();
            if (!this.categories.containsKey(categoryName)) {
                this.categories.put(categoryName, this.currentCategory);
            }
            List<Product> currentCategoryProductList = this.categoryProducts.get(categoryName);
            if (currentCategoryProductList == null) {
                currentCategoryProductList = new ArrayList<>();
                this.categoryProducts.put(categoryName, currentCategoryProductList);
            }
            currentCategoryProductList.add(this.currentProduct);
        }
    }

    public void startDocument() throws SAXException {
        super.startDocument();
    }

    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        super.startElement(uri, localName, name, attributes);
        if (PRODUCT.equals(localName)) {
            this.currentProduct = new Product();
            this.products.add(this.currentProduct);
        } else if (CATEGORY.equals(localName)) {
            this.currentCategory = new Category();
            this.currentProduct.getCategories().add(this.currentCategory);
        } else {
            this.currentElement = localName;
        }
    }
}
