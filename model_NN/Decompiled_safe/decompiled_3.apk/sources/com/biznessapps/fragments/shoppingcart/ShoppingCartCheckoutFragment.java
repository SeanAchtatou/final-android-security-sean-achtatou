package com.biznessapps.fragments.shoppingcart;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.fragments.shoppingcart.entities.Product;
import com.biznessapps.fragments.shoppingcart.utils.PaypalResultDelegate;
import com.biznessapps.fragments.shoppingcart.utils.ShoppingCart;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.widgets.RefreshableListView;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalInvoiceData;
import com.paypal.android.MEP.PayPalInvoiceItem;
import com.paypal.android.MEP.PayPalPayment;
import com.paypal.android.MEP.PayPalResultDelegate;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

public class ShoppingCartCheckoutFragment extends CommonListFragment<Product> {
    private static final int PAYMENT_BUTTON_HEIGHT = 60;
    private static final int PAYMENT_BUTTON_WIDTH = 200;
    public static PayPal paypal;
    public static String resultExtra;
    public static String resultInfo;
    public static String resultTitle;
    private LinearLayout bodyLayout;
    private BitmapWrapper buttonBgWrapper;
    private LinearLayout buttonLayout;
    /* access modifiers changed from: private */
    public ShoppingCart cart = ShoppingCart.getInstance();
    private String checkoutButtonUrl;
    private ImageButton googleCheckoutBtn;
    private CheckoutButton paypalPaymentBtn;
    /* access modifiers changed from: private */
    public PayPalResultDelegate paypalResultDelegate;
    private TextView subTotalItemsTextView;
    private TextView subTotalTextView;
    private float subtotal = 0.0f;
    private double tax = 0.0d;
    private TextView taxTextView;
    private float total = 0.0f;
    private int totalItems = 0;
    private TextView totalTextView;

    public void onResume() {
        super.onResume();
        if (paypal != null) {
            initPaymentButton();
        }
    }

    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        if (paypal == null && this.cart.getStore().getPaypalApplicationID().length() != 0) {
            paypal = PayPal.initWithAppID(getHoldActivity(), this.cart.getStore().getPaypalApplicationID(), 1);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        updateCheckoutTotals();
        plugListView(holdActivity);
        this.buttonLayout.removeAllViews();
        if (this.googleCheckoutBtn == null && StringUtils.isNotEmpty(this.cart.getStore().getGoogleCheckoutMerchantID())) {
            this.googleCheckoutBtn = new ImageButton(holdActivity.getApplicationContext());
            this.googleCheckoutBtn.setLayoutParams(new ViewGroup.LayoutParams(200, (int) PAYMENT_BUTTON_HEIGHT));
            if (StringUtils.isNotEmpty(this.cart.getStore().getGoogleCheckoutMerchantID())) {
                this.checkoutButtonUrl = String.format(AppConstants.GOOGLE_CHECKOUT_BUTTON_URL, this.cart.getStore().getGoogleCheckoutMerchantID());
                AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadAppImage(this.checkoutButtonUrl, this.googleCheckoutBtn);
            }
            this.googleCheckoutBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String merchantId = ShoppingCartCheckoutFragment.this.cart.getStore().getGoogleCheckoutMerchantID();
                    String merchantKey = ShoppingCartCheckoutFragment.this.cart.getStore().getGoogleCheckoutMerchantKey();
                    String currency = ShoppingCartCheckoutFragment.this.cart.getStore().getCurrency();
                    float taxRate = ShoppingCartCheckoutFragment.this.cart.getStore().getTax();
                    Intent googleCheckoutIntent = new Intent(ShoppingCartCheckoutFragment.this.getHoldActivity(), SingleFragmentActivity.class);
                    googleCheckoutIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.GOOGLE_CHECKOUT_FRAGMENT);
                    googleCheckoutIntent.putExtra(AppConstants.MERCHANT_ID_EXTRA, merchantId);
                    googleCheckoutIntent.putExtra(AppConstants.MERCHANT_KEY_EXTRA, merchantKey);
                    googleCheckoutIntent.putExtra(AppConstants.CURRENCY_EXTRA, currency);
                    googleCheckoutIntent.putExtra(AppConstants.TAX_EXTRA, taxRate);
                    googleCheckoutIntent.putExtra(AppConstants.CHECKOUT_PRODUCTS_EXTRA, (HashMap) ShoppingCartCheckoutFragment.this.cart.getCheckoutProducts());
                    ShoppingCartCheckoutFragment.this.startActivity(googleCheckoutIntent);
                }
            });
            this.buttonLayout.removeView(this.googleCheckoutBtn);
            this.buttonLayout.addView(this.googleCheckoutBtn);
            this.googleCheckoutBtn.setVisibility(8);
        }
        if (StringUtils.isNotEmpty(this.cart.getStore().getPaypalApplicationID()) && paypal != null) {
            initPaymentButton();
        }
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return this.cart.getStore().getBackgroundUrl();
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.bodyLayout;
    }

    private void plugListView(Activity holdActivity) {
        this.items = new ArrayList();
        for (Product product : this.cart.getCheckoutProducts().keySet()) {
            this.items.add(product);
        }
        if (!this.items.isEmpty()) {
            this.adapter = new ShoppingCartCheckoutAdapter(holdActivity.getApplicationContext(), this.items, this);
            this.listView.setAdapter((ListAdapter) this.adapter);
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.shop_checkout_list;
    }

    public void updateCheckoutTotals() {
        this.totalItems = 0;
        this.subtotal = 0.0f;
        this.total = 0.0f;
        this.tax = 0.0d;
        for (Product product : this.cart.getCheckoutProducts().keySet()) {
            this.totalItems = this.cart.getCheckoutProducts().get(product).intValue() + this.totalItems;
            this.subtotal = (((float) this.cart.getCheckoutProducts().get(product).intValue()) * product.getProductPrice()) + this.subtotal;
        }
        this.tax = (double) (this.subtotal * ShoppingCart.getInstance().getStore().getTax());
        this.total = (float) (((double) this.subtotal) + this.tax);
        String currencySign = ShoppingCart.getInstance().getStore().getCurrencySign();
        String textFormat = getString(R.string.subtotal_items);
        this.subTotalItemsTextView.setText(String.format(textFormat, Integer.valueOf(this.totalItems)));
        this.subTotalTextView.setText(currencySign + String.format("%.2f", Float.valueOf(this.subtotal)));
        this.taxTextView.setText(currencySign + String.format("%.2f", Double.valueOf(this.tax)));
        this.totalTextView.setText(currencySign + String.format("%.2f", Float.valueOf(this.total)));
    }

    /* access modifiers changed from: private */
    public PayPalPayment createPaypalPayment() {
        PayPalPayment payment = new PayPalPayment();
        payment.setCurrencyType(ShoppingCart.getInstance().getStore().getCurrency());
        payment.setSubtotal(new BigDecimal((double) this.subtotal));
        payment.setPaymentType(0);
        payment.setRecipient(prehandleRecipientEmail(this.cart.getStore().getPaypalUsername()));
        payment.setMerchantName(cacher().getAppCode());
        PayPalInvoiceData invoice = new PayPalInvoiceData();
        invoice.setTax(new BigDecimal(this.tax));
        for (Product product : this.cart.getCheckoutProducts().keySet()) {
            PayPalInvoiceItem invoiceItem = new PayPalInvoiceItem();
            invoiceItem.setName(product.getTitle());
            invoiceItem.setID(product.getId());
            invoiceItem.setTotalPrice(new BigDecimal((double) (product.getProductPrice() * ((float) this.cart.getCheckoutProducts().get(product).intValue()))));
            invoiceItem.setUnitPrice(new BigDecimal((double) product.getProductPrice()));
            invoiceItem.setQuantity(this.cart.getCheckoutProducts().get(product).intValue());
            invoice.getInvoiceItems().add(invoiceItem);
        }
        payment.setInvoiceData(invoice);
        return payment;
    }

    private String prehandleRecipientEmail(String primaryRecipient) {
        new String();
        if (primaryRecipient.contains(".gmail")) {
            return primaryRecipient.replace(".gmail", "@gmail");
        }
        return primaryRecipient;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != 10001) {
            switch (resultCode) {
                case -1:
                    resultTitle = "SUCCESS";
                    resultInfo = "You have successfully completed this ";
                    resultExtra = "Transaction ID: " + data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY);
                    RemoveCartData();
                    break;
                case 0:
                    resultTitle = "CANCELED";
                    resultInfo = "The transaction has been cancelled.";
                    resultExtra = "";
                    break;
                case 2:
                    resultTitle = "FAILURE";
                    resultInfo = data.getStringExtra(PayPalActivity.EXTRA_ERROR_MESSAGE);
                    resultExtra = "Error ID: " + data.getStringExtra(PayPalActivity.EXTRA_ERROR_ID);
                    break;
            }
            if (resultTitle == "SUCCESS") {
                Toast.makeText(getHoldActivity(), R.string.payment_transaction_successful, 0);
            }
        }
    }

    private void RemoveCartData() {
        this.cart.getCheckoutProducts().clear();
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.listView = (RefreshableListView) ((ViewGroup) root.findViewById(R.id.list_view_root)).findViewById(R.id.list_view);
        this.bodyLayout = (LinearLayout) root.findViewById(R.id.list_view_root);
        this.buttonLayout = (LinearLayout) root.findViewById(R.id.checkout_buttons);
        this.subTotalItemsTextView = (TextView) root.findViewById(R.id.text_subtotal_label);
        this.subTotalTextView = (TextView) root.findViewById(R.id.text_subtotal_value);
        this.taxTextView = (TextView) root.findViewById(R.id.tax_value);
        this.totalTextView = (TextView) root.findViewById(R.id.total_value);
        customizeTextView(this.totalTextView);
        customizeTextView(this.taxTextView);
        customizeTextView(this.subTotalItemsTextView);
        customizeTextView((TextView) root.findViewById(R.id.grand_total_label));
        customizeTextView((TextView) root.findViewById(R.id.sales_tax_label));
        customizeTextView(this.subTotalTextView);
        customizeTextView((TextView) root.findViewById(R.id.product_cost_label));
    }

    private void customizeTextView(TextView view) {
        int textBgColor = getUiSettings().getSectionBarColor();
        view.setTextColor(getUiSettings().getSectionTextColor());
        view.setBackgroundColor(textBgColor);
    }

    private void initPaymentButton() {
        this.buttonLayout.removeAllViews();
        this.paypalPaymentBtn = paypal.getCheckoutButton(getHoldActivity(), 0, 0);
        this.paypalPaymentBtn.setLayoutParams(new ViewGroup.LayoutParams(-1, (int) PAYMENT_BUTTON_HEIGHT));
        this.paypalResultDelegate = new PaypalResultDelegate();
        this.paypalPaymentBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ShoppingCartCheckoutFragment.this.startActivityForResult(ShoppingCartCheckoutFragment.paypal.checkout(ShoppingCartCheckoutFragment.this.createPaypalPayment(), ShoppingCartCheckoutFragment.this.getHoldActivity(), ShoppingCartCheckoutFragment.this.paypalResultDelegate), AppConstants.PAYPAL_ACTIVITY_REQUEST);
            }
        });
        this.buttonLayout.addView(this.paypalPaymentBtn);
    }
}
