package com.biznessapps.fragments.shoppingcart.entities;

import com.biznessapps.model.CommonListEntity;

public class Category extends CommonListEntity {
    private static final long serialVersionUID = -4902897234113036100L;

    public Category() {
    }

    public Category(String title) {
        super(title);
    }
}
