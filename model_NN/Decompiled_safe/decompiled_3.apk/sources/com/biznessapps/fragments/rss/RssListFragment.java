package com.biznessapps.fragments.rss;

import android.app.Activity;
import android.widget.ListAdapter;
import com.biznessapps.adapters.RssAdapter;
import com.biznessapps.adapters.SeparatedListAdapter;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.RssItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RssListFragment extends CommonListFragment<RssItem> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RSS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseRssList(dataToParse);
        return cacher().saveData(CachingConstants.RSS_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.RSS_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r6, android.view.View r7, int r8, long r9) {
        /*
            r5 = this;
            android.widget.Adapter r2 = r6.getAdapter()
            java.lang.Object r1 = r2.getItem(r8)
            com.biznessapps.model.RssItem r1 = (com.biznessapps.model.RssItem) r1
            if (r1 == 0) goto L_0x0071
            java.lang.String r2 = r1.getId()
            boolean r2 = com.biznessapps.utils.StringUtils.isNotEmpty(r2)
            if (r2 == 0) goto L_0x0071
            android.content.Intent r0 = new android.content.Intent
            android.content.Context r2 = r5.getApplicationContext()
            java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r3 = com.biznessapps.activities.SingleFragmentActivity.class
            r0.<init>(r2, r3)
            java.lang.String r2 = "TAB_FRAGMENT"
            java.lang.String r3 = "RSS_DETAIL_FRAGMENT"
            r0.putExtra(r2, r3)
            java.lang.String r2 = "SHOW_WEB_ORIGINAL_SIZE"
            r3 = 1
            r0.putExtra(r2, r3)
            java.lang.String r2 = "WEB_DATA"
            java.lang.String r3 = r1.getDescription()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "RSS_ITEM_EXTRA"
            r0.putExtra(r2, r1)
            java.lang.String r2 = "BG_URL_EXTRA"
            java.lang.String r3 = r5.bgUrl
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_UNIQUE_ID"
            com.biznessapps.activities.CommonFragmentActivity r3 = r5.getHoldActivity()
            long r3 = r3.getTabId()
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_SPECIAL_ID"
            android.content.Intent r3 = r5.getIntent()
            java.lang.String r4 = "TAB_SPECIAL_ID"
            java.lang.String r3 = r3.getStringExtra(r4)
            r0.putExtra(r2, r3)
            java.lang.String r2 = "TAB_LABEL"
            android.content.Intent r3 = r5.getIntent()
            java.lang.String r4 = "TAB_LABEL"
            java.lang.String r3 = r3.getStringExtra(r4)
            r0.putExtra(r2, r3)
            r5.startActivity(r0)
        L_0x0071:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.rss.RssListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        if (this.items != null && !this.items.isEmpty()) {
            this.adapter = new SeparatedListAdapter(holdActivity.getApplicationContext(), R.layout.section_header, getUiSettings().getSectionBarColor(), getUiSettings().getSectionTextColor());
            Map<String, List<RssItem>> sectionsMap = new LinkedHashMap<>();
            for (RssItem item : this.items) {
                String section = item.getSection();
                if (StringUtils.isEmpty(section)) {
                    section = "";
                }
                List<RssItem> itemsList = sectionsMap.get(section);
                if (itemsList == null) {
                    itemsList = new LinkedList<>();
                }
                itemsList.add(getWrappedItem(item, itemsList));
                sectionsMap.put(section, itemsList);
            }
            Set<String> sections = sectionsMap.keySet();
            List<RssItem> resultList = new LinkedList<>();
            for (String section2 : sections) {
                List<RssItem> itemsList2 = sectionsMap.get(section2);
                if (itemsList2 != null && itemsList2.size() > 0) {
                    ((SeparatedListAdapter) this.adapter).addSection(section2, new RssAdapter(holdActivity.getApplicationContext(), itemsList2, R.layout.rss_row));
                    for (int i = 0; i < itemsList2.size(); i++) {
                        RssItem item2 = (RssItem) itemsList2.get(i);
                        if (i == 0) {
                            item2.setShowSection(true);
                        }
                        resultList.add(item2);
                    }
                }
            }
            this.listView.setAdapter((ListAdapter) new RssAdapter(holdActivity.getApplicationContext(), resultList, R.layout.rss_row));
            initListViewListener();
        }
    }
}
