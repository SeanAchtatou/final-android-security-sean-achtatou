package com.biznessapps.fragments.rss;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.RssItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;

public class RssDetailFragment extends CommonFragment {
    private Button readArticleButton;
    private TextView rssDateView;
    private TextView rssDescriptionView;
    private TextView rssTitleView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        initData();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.rss_detail_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.rssDateView = (TextView) root.findViewById(R.id.rss_date_label);
        this.rssTitleView = (TextView) root.findViewById(R.id.rss_title_label);
        this.rssDescriptionView = (TextView) root.findViewById(R.id.rss_description_label);
        this.readArticleButton = (Button) root.findViewById(R.id.read_article_button);
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.readArticleButton.getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.rssDateView.getBackground());
        this.readArticleButton.setTextColor(settings.getButtonTextColor());
        this.rssDateView.setTextColor(settings.getButtonTextColor());
        this.rssTitleView.setTextColor(settings.getFeatureTextColor());
        this.rssDescriptionView.setTextColor(settings.getFeatureTextColor());
        ViewUtils.setGlobalBackgroundColor(root.findViewById(R.id.rss_root_container));
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root.findViewById(R.id.rss_root_container);
    }

    private void initData() {
        final RssItem item = (RssItem) getIntent().getSerializableExtra(AppConstants.RSS_ITEM_EXTRA);
        if (item != null) {
            this.rssDateView.setText(Html.fromHtml(item.getSubtitle()));
            this.rssTitleView.setText(Html.fromHtml(item.getTitle()));
            this.rssDescriptionView.setText(Html.fromHtml(item.getSummary()));
            this.readArticleButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (StringUtils.isNotEmpty(item.getLink())) {
                        Intent intent = new Intent(RssDetailFragment.this.getApplicationContext(), SingleFragmentActivity.class);
                        intent.putExtra(AppConstants.URL, item.getLink());
                        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
                        intent.putExtra(AppConstants.TAB_ID, RssDetailFragment.this.getHoldActivity().getTabId());
                        intent.putExtra(AppConstants.TAB_LABEL, item.getTitle());
                        RssDetailFragment.this.startActivity(intent);
                    }
                }
            });
        }
        if (StringUtils.isEmpty(this.bgUrl)) {
            this.bgUrl = getIntent().getStringExtra(AppConstants.BG_URL_EXTRA);
        }
    }
}
