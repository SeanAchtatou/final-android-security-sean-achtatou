package com.biznessapps.fragments;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SwipeyTabs extends ViewGroup implements ViewPager.OnPageChangeListener {
    protected final String TAG;
    private SwipeyTabsAdapter mAdapter;
    private int mBottomBarColor;
    private int mBottomBarHeight;
    private Paint mCachedTabPaint;
    private int mCurrentPos;
    private int[] mCurrentTabPos;
    private int[] mFrontedTabPos;
    private int[] mLeftTabPos;
    private int[] mRightTabPos;
    private int mTabIndicatorHeight;
    private int mTextColor;
    private int mWidth;

    public SwipeyTabs(Context context) {
        this(context, null);
    }

    public SwipeyTabs(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SwipeyTabs(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.TAG = "SwipeyTabs";
        this.mCurrentPos = -1;
        this.mBottomBarHeight = 2;
        this.mTabIndicatorHeight = 3;
        this.mBottomBarColor = -6903239;
        this.mTextColor = -7039852;
        this.mWidth = -1;
        init();
    }

    private void init() {
        setHorizontalFadingEdgeEnabled(true);
        setFadingEdgeLength((int) ((getResources().getDisplayMetrics().density * 35.0f) + 0.5f));
        setWillNotDraw(false);
        this.mCachedTabPaint = new Paint();
        this.mCachedTabPaint.setColor(this.mBottomBarColor);
    }

    public void setAdapter(SwipeyTabsAdapter adapter) {
        if (this.mAdapter != null) {
        }
        this.mAdapter = adapter;
        this.mCurrentPos = -1;
        this.mFrontedTabPos = null;
        this.mLeftTabPos = null;
        this.mRightTabPos = null;
        this.mCurrentTabPos = null;
        removeAllViews();
        if (this.mAdapter != null) {
            int count = this.mAdapter.getCount();
            for (int i = 0; i < count; i++) {
            }
            this.mCurrentPos = 0;
            this.mFrontedTabPos = new int[count];
            this.mLeftTabPos = new int[count];
            this.mRightTabPos = new int[count];
            this.mCurrentTabPos = new int[count];
            this.mWidth = -1;
            requestLayout();
        }
    }

    private void updateTabPositions(boolean forceLayout) {
        if (this.mAdapter != null) {
            calculateTabPosition(this.mCurrentPos, this.mFrontedTabPos);
            calculateTabPosition(this.mCurrentPos + 1, this.mLeftTabPos);
            calculateTabPosition(this.mCurrentPos - 1, this.mRightTabPos);
            updateEllipsize();
            if (forceLayout) {
                int count = this.mAdapter.getCount();
                for (int i = 0; i < count; i++) {
                    this.mCurrentTabPos[i] = this.mFrontedTabPos[i];
                }
            }
        }
    }

    private void calculateTabPosition(int position, int[] tabPositions) {
        if (this.mAdapter != null) {
            int count = this.mAdapter.getCount();
            if (position < 0 || position >= count) {
                for (int i = 0; i < tabPositions.length; i++) {
                    tabPositions[i] = -1;
                }
                return;
            }
            int width = getMeasuredWidth();
            tabPositions[position] = (width / 2) - (getChildAt(position).getMeasuredWidth() / 2);
            for (int i2 = position - 1; i2 >= 0; i2--) {
                TextView tab = (TextView) getChildAt(i2);
                if (i2 == position - 1) {
                    tabPositions[i2] = 0 - tab.getPaddingLeft();
                } else {
                    tabPositions[i2] = (0 - tab.getMeasuredWidth()) - width;
                }
                tabPositions[i2] = Math.min(tabPositions[i2], tabPositions[i2 + 1] - tab.getMeasuredWidth());
            }
            for (int i3 = position + 1; i3 < count; i3++) {
                TextView tab2 = (TextView) getChildAt(i3);
                if (i3 == position + 1) {
                    tabPositions[i3] = (width - tab2.getMeasuredWidth()) + tab2.getPaddingRight();
                } else {
                    tabPositions[i3] = width * 2;
                }
                tabPositions[i3] = Math.max(tabPositions[i3], tabPositions[i3 - 1] + ((TextView) getChildAt(i3 - 1)).getMeasuredWidth());
            }
        }
    }

    private void updateEllipsize() {
        if (this.mAdapter != null) {
            int count = this.mAdapter.getCount();
            for (int i = 0; i < count; i++) {
                TextView tab = (TextView) getChildAt(i);
                if (i < this.mCurrentPos) {
                    tab.setEllipsize(null);
                    tab.setGravity(21);
                } else if (i == this.mCurrentPos) {
                    tab.setEllipsize(TextUtils.TruncateAt.END);
                    tab.setGravity(19);
                } else if (i > this.mCurrentPos) {
                    tab.setEllipsize(null);
                    tab.setGravity(19);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        measureTabs(widthMeasureSpec, heightMeasureSpec);
        int height = 0;
        View v = getChildAt(0);
        if (v != null) {
            height = v.getMeasuredHeight();
        }
        setMeasuredDimension(resolveSize(getPaddingLeft() + widthSize + getPaddingRight(), widthMeasureSpec), resolveSize(this.mBottomBarHeight + height + getPaddingTop() + getPaddingBottom(), heightMeasureSpec));
        if (this.mWidth != widthSize) {
            this.mWidth = widthSize;
            updateTabPositions(true);
        }
    }

    private void measureTabs(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mAdapter != null) {
            int maxWidth = (int) (((double) View.MeasureSpec.getSize(widthMeasureSpec)) * 0.6d);
            int count = this.mAdapter.getCount();
            for (int i = 0; i < count; i++) {
                ViewGroup.LayoutParams layoutParams = getChildAt(i).getLayoutParams();
                getChildAt(i).measure(View.MeasureSpec.makeMeasureSpec(maxWidth, Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        if (this.mAdapter != null) {
            int count = this.mAdapter.getCount();
            for (int i = 0; i < count; i++) {
                View v = getChildAt(i);
                v.layout(this.mCurrentTabPos[i], getPaddingTop(), this.mCurrentTabPos[i] + v.getMeasuredWidth(), getPaddingTop() + v.getMeasuredHeight());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: protected */
    public void dispatchDraw(Canvas canvas) {
        if (this.mCurrentPos != -1) {
            int tabSelectedTop = ((getHeight() - getPaddingBottom()) - this.mBottomBarHeight) - this.mTabIndicatorHeight;
            View currentTab = getChildAt(this.mCurrentPos);
            int centerOfTab = (this.mCurrentTabPos[this.mCurrentPos] + currentTab.getMeasuredWidth()) - (currentTab.getMeasuredWidth() / 2);
            int center = getWidth() / 2;
            float relativePos = 1.0f - Math.min(Math.abs(((float) (centerOfTab - center)) / ((float) (center / 3))), 1.0f);
            this.mCachedTabPaint.setAlpha((int) (255.0f * relativePos));
            canvas.drawRect((float) this.mCurrentTabPos[this.mCurrentPos], (float) tabSelectedTop, (float) (this.mCurrentTabPos[this.mCurrentPos] + currentTab.getMeasuredWidth()), (float) (this.mTabIndicatorHeight + tabSelectedTop), this.mCachedTabPaint);
            int count = this.mAdapter.getCount();
            for (int i = 0; i < count; i++) {
                TextView tab = (TextView) getChildAt(i);
                if (this.mCurrentPos == i) {
                    tab.setTextColor(interpolateColor(this.mBottomBarColor, this.mTextColor, 1.0f - relativePos));
                } else {
                    tab.setTextColor(this.mTextColor);
                }
            }
        }
        super.dispatchDraw(canvas);
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        int bottomBarTop = (getHeight() - getPaddingBottom()) - this.mBottomBarHeight;
        this.mCachedTabPaint.setAlpha(MotionEventCompat.ACTION_MASK);
        canvas.drawRect(0.0f, (float) bottomBarTop, (float) getWidth(), (float) (this.mBottomBarHeight + bottomBarTop), this.mCachedTabPaint);
    }

    /* access modifiers changed from: protected */
    public float getLeftFadingEdgeStrength() {
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getRightFadingEdgeStrength() {
        return 1.0f;
    }

    public void onPageScrollStateChanged(int state) {
    }

    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        int i;
        if (this.mAdapter != null) {
            int count = this.mAdapter.getCount();
            float x = 0.0f;
            int dir = 0;
            if (positionOffsetPixels != 0 && this.mCurrentPos == position) {
                dir = -1;
                x = positionOffset;
            } else if (!(positionOffsetPixels == 0 || this.mCurrentPos == position)) {
                dir = 1;
                x = 1.0f - positionOffset;
            }
            for (int i2 = 0; i2 < count; i2++) {
                float curX = (float) this.mFrontedTabPos[i2];
                if (dir < 0) {
                    i = this.mLeftTabPos[i2];
                } else if (dir > 0) {
                    i = this.mRightTabPos[i2];
                } else {
                    i = this.mFrontedTabPos[i2];
                }
                this.mCurrentTabPos[i2] = (int) (((float) ((int) (((((float) i) - curX) * x) + 0.5f))) + curX);
            }
            requestLayout();
            invalidate();
        }
    }

    public void onPageSelected(int position) {
        this.mCurrentPos = position;
        updateTabPositions(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private int interpolateColor(int color1, int color2, float fraction) {
        float alpha1 = ((float) Color.alpha(color1)) / 255.0f;
        float red1 = ((float) Color.red(color1)) / 255.0f;
        float green1 = ((float) Color.green(color1)) / 255.0f;
        float blue1 = ((float) Color.blue(color1)) / 255.0f;
        float alpha2 = ((float) Color.alpha(color2)) / 255.0f;
        float red2 = ((float) Color.red(color2)) / 255.0f;
        float blue = blue1 + (((((float) Color.blue(color2)) / 255.0f) - blue1) * fraction);
        return Color.argb((int) (255.0f * Math.max(Math.min(alpha1 + ((alpha2 - alpha1) * fraction), 1.0f), 0.0f)), (int) (255.0f * Math.max(Math.min(red1 + ((red2 - red1) * fraction), 1.0f), 0.0f)), (int) (255.0f * Math.max(Math.min(green1 + (((((float) Color.green(color2)) / 255.0f) - green1) * fraction), 1.0f), 0.0f)), (int) (255.0f * Math.max(Math.min(blue, 1.0f), 0.0f)));
    }
}
