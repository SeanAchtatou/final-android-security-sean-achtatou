package com.biznessapps.fragments.infoitems;

import android.app.Activity;
import android.content.Intent;
import android.net.MailTo;
import android.net.ParseException;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import com.biznessapps.activities.CommonFragmentActivity;
import com.biznessapps.activities.CommonTabFragmentActivity;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.events.EventCommentsTabUtils;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.List;

public class InfoDetailFragment extends CommonFragment {
    private static final String LINK_TEXT_TEMPLATE = "Can not see the above content correctly? Click here";
    private Button backButton;
    private ViewGroup buttonContainer;
    protected Button commentsButton;
    protected ViewGroup commentsTabContainer;
    private List<String> customUrls = new ArrayList();
    private ImageView headerImageColorView;
    protected ImageView headerImageView;
    /* access modifiers changed from: private */
    public List<String> historyUrls = new ArrayList();
    protected Button infoButton;
    protected CommonListEntity infoItem;
    protected ViewGroup infoTabContainer;
    private CommonFragmentActivity.BackPressed onBackPressedListener;
    private ImageButton socialButton;
    /* access modifiers changed from: protected */
    public String tabId;
    protected WebView webView;

    public InfoDetailFragment() {
        this.customUrls.add("inigobar");
        this.onBackPressedListener = new CommonFragmentActivity.BackPressed() {
            public boolean onBackPressed() {
                boolean errorIsHappened;
                boolean canGoBack;
                int sizeBeforeChanging = InfoDetailFragment.this.historyUrls.size();
                if (!AppConstants.BLANK_PAGE_TAG.equalsIgnoreCase(InfoDetailFragment.this.webView.getUrl())) {
                    InfoDetailFragment.this.historyUrls.remove(InfoDetailFragment.this.webView.getUrl());
                } else {
                    InfoDetailFragment.this.historyUrls.remove(InfoDetailFragment.this.infoItem.getDescription());
                }
                if (sizeBeforeChanging == InfoDetailFragment.this.historyUrls.size()) {
                    errorIsHappened = true;
                } else {
                    errorIsHappened = false;
                }
                if ((InfoDetailFragment.this.webView == null || !InfoDetailFragment.this.webView.canGoBack()) && !errorIsHappened) {
                    canGoBack = false;
                } else {
                    canGoBack = true;
                }
                if (canGoBack) {
                    InfoDetailFragment.this.webView.goBack();
                }
                if (InfoDetailFragment.this.infoItem != null && InfoDetailFragment.this.historyUrls.size() == 1 && InfoDetailFragment.this.historyUrls.contains(InfoDetailFragment.this.infoItem.getDescription())) {
                    InfoDetailFragment.this.loadContent();
                }
                if (InfoDetailFragment.this.historyUrls.isEmpty() || errorIsHappened) {
                    return false;
                }
                return true;
            }
        };
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(getLayoutId(), (ViewGroup) null);
        initViews();
        initListeners();
        loadData();
        getHoldActivity().addBackPressedListener(this.onBackPressedListener);
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if (activity != null && (activity instanceof CommonTabFragmentActivity)) {
            ((CommonTabFragmentActivity) activity).getViewPager().enableScrolling(false);
        }
        if (this.commentsTabContainer != null) {
            EventCommentsTabUtils.loadCommentsData(getHoldActivity(), this.commentsTabContainer, this.itemId, this.tabId);
        }
    }

    public void onDestroy() {
        CommonFragmentActivity activity = getHoldActivity();
        if (activity != null) {
            activity.removeBackPressedListener(this.onBackPressedListener);
        }
        super.onDestroy();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (resultCode) {
            case 4:
                addComment();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setItemId(getIntent().getStringExtra(AppConstants.ITEM_ID));
        data.setCatId(getIntent().getStringExtra(AppConstants.SECTION_ID));
        return data;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return getIntent().getBooleanExtra(AppConstants.HAS_NEW_DESIGN, true) ? R.layout.info_detail_new_layout : R.layout.info_detail_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews() {
        this.buttonContainer = (ViewGroup) this.root.findViewById(R.id.event_new_buttons_container);
        this.socialButton = (ImageButton) this.root.findViewById(R.id.choose_login_account);
        this.infoTabContainer = (ViewGroup) this.root.findViewById(R.id.event_info_tab_container);
        this.commentsTabContainer = (ViewGroup) this.root.findViewById(R.id.event_comments_tab_container);
        this.infoButton = (Button) this.root.findViewById(R.id.event_new_info_button);
        if (this.socialButton != null) {
            this.headerImageView = (ImageView) this.root.findViewById(R.id.info_header_image);
            this.headerImageColorView = (ImageView) this.root.findViewById(R.id.info_header_image_color);
            ViewUtils.setGlobalBackgroundColor(this.headerImageColorView);
            this.infoButton = (Button) this.root.findViewById(R.id.event_new_info_button);
            this.commentsButton = (Button) this.root.findViewById(R.id.event_new_comments_button);
            this.infoButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    InfoDetailFragment.this.activateButton(InfoDetailFragment.this.infoButton, InfoDetailFragment.this.infoTabContainer);
                }
            });
            this.commentsButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    InfoDetailFragment.this.activateButton(InfoDetailFragment.this.commentsButton, InfoDetailFragment.this.commentsTabContainer);
                    EventCommentsTabUtils.loadCommentsData(InfoDetailFragment.this.getApplicationContext(), InfoDetailFragment.this.commentsTabContainer, InfoDetailFragment.this.itemId, InfoDetailFragment.this.tabId);
                }
            });
            activateButton(this.infoButton, this.infoTabContainer);
            int barColor = getUiSettings().getNavigationBarColor();
            int barTextColor = getUiSettings().getNavigationTextColor();
            this.buttonContainer.setBackgroundColor(barColor);
            this.infoButton.setTextColor(barTextColor);
            this.commentsButton.setTextColor(barTextColor);
            SocialNetworkAccessor socialAccessor = new SocialNetworkAccessor(getHoldActivity(), this.root);
            SocialNetworkAccessor.SocialNetworkListener listener = new SocialNetworkAccessor.SocialNetworkListener(socialAccessor) {
                public void onAuthSucceed() {
                    InfoDetailFragment.this.addComment();
                }
            };
            socialAccessor.setAddCommentListener(this.root.findViewById(R.id.event_comment_button));
            getHoldActivity().setSocialNetworkListener(listener);
            socialAccessor.addAuthorizationListener(listener);
        }
        this.webView = (WebView) this.root.findViewById(R.id.webview);
        this.webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String urlToLoad, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setType(mimetype);
                intent.setData(Uri.parse(urlToLoad));
                InfoDetailFragment.this.startActivity(intent);
            }
        });
        this.backButton = (Button) this.root.findViewById(R.id.info_detail_back_button);
        if (getIntent().getBooleanExtra(AppConstants.CHILDREN_TAB_LABEL_PRESENT, false)) {
            this.backButton.setText(getIntent().getStringExtra(AppConstants.TAB_LABEL));
            CommonUtils.overrideMediumButtonColor(AppCore.getInstance().getUiSettings().getNavigationBarColor(), this.backButton.getBackground());
            return;
        }
        this.backButton.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return this.infoItem != null ? this.infoItem.getHeaderImage() : "";
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.headerImageView;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        super.preDataLoading(holdActivity);
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.INFO_ITEM_DETAIL, this.itemId, this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.infoItem = JsonParserUtils.parseInfo(dataToParse);
        return cacher().saveData(CachingConstants.INFO_DETAIL_PROPERTY + this.itemId, this.infoItem);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.infoItem = (CommonListEntity) cacher().getData(CachingConstants.INFO_DETAIL_PROPERTY + this.itemId);
        return this.infoItem != null;
    }

    /* access modifiers changed from: protected */
    public boolean canHaveNewDesign() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(final Activity holdActivity) {
        int i;
        int i2;
        int i3 = 0;
        super.updateControlsWithData(holdActivity);
        if (canHaveNewDesign()) {
            if (this.buttonContainer != null) {
                this.buttonContainer.setVisibility(this.infoItem.hasNewDesign() ? 0 : 8);
            }
            if (this.socialButton != null) {
                ImageButton imageButton = this.socialButton;
                if (this.infoItem.hasNewDesign()) {
                    i2 = 0;
                } else {
                    i2 = 8;
                }
                imageButton.setVisibility(i2);
            }
            if (!(this.headerImageView == null || this.headerImageColorView == null)) {
                ImageView imageView = this.headerImageView;
                if (this.infoItem.hasNewDesign()) {
                    i = 0;
                } else {
                    i = 8;
                }
                imageView.setVisibility(i);
                ImageView imageView2 = this.headerImageColorView;
                if (!this.infoItem.hasNewDesign()) {
                    i3 = 8;
                }
                imageView2.setVisibility(i3);
            }
        }
        this.webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url == null) {
                    return false;
                }
                if (url.contains(AppConstants.MAILTO_TYPE)) {
                    try {
                        MailTo mt = MailTo.parse(url);
                        ViewUtils.email(holdActivity, new String[]{mt.getTo()}, null, null);
                    } catch (ParseException e) {
                        Toast.makeText(holdActivity.getApplicationContext(), R.string.data_loading_failure, 0).show();
                    }
                    return true;
                } else if (url.contains(AppConstants.TEL_TYPE)) {
                    InfoDetailFragment.this.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(url)));
                    return true;
                } else if (url.endsWith(AppConstants.PDF_TYPE)) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setData(Uri.parse(AppConstants.GOOGLE_DOCS_WRAPPER + url));
                    InfoDetailFragment.this.startActivity(intent);
                    return false;
                } else if (url.contains(AppConstants.YOUTUBE) || url.contains(AppConstants.VIMEO)) {
                    Intent externalIntent = new Intent("android.intent.action.VIEW");
                    externalIntent.setData(Uri.parse(url));
                    InfoDetailFragment.this.startActivity(externalIntent);
                    return true;
                } else {
                    if (!InfoDetailFragment.this.historyUrls.contains(url)) {
                        InfoDetailFragment.this.historyUrls.add(url);
                    }
                    if (!InfoDetailFragment.this.historyUrls.contains(InfoDetailFragment.this.infoItem.getDescription())) {
                        InfoDetailFragment.this.historyUrls.add(InfoDetailFragment.this.infoItem.getDescription());
                    }
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }
        });
        if (StringUtils.isNotEmpty(this.infoItem.getDescription())) {
            this.historyUrls.add(this.infoItem.getDescription());
            loadContent();
        }
    }

    /* access modifiers changed from: private */
    public void activateButton(Button buttonToActivate, ViewGroup tabToActivate) {
        this.infoButton.setBackgroundResource(R.drawable.tab_bar);
        this.commentsButton.setBackgroundResource(R.drawable.tab_bar);
        buttonToActivate.setBackgroundResource(R.drawable.tab_button_active);
        this.infoTabContainer.setVisibility(8);
        this.commentsTabContainer.setVisibility(8);
        tabToActivate.setVisibility(0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void addComment() {
        Activity activity = getHoldActivity();
        if (activity != null) {
            Intent intent = new Intent(activity.getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, activity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.comments));
            intent.putExtra(AppConstants.YOUTUBE_MODE, false);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.FAN_ADD_COMMENT_FRAGMENT);
            intent.putExtra(AppConstants.USE_SPECIAL_MD5_HASH_EXTRA, true);
            intent.putExtra("id", this.itemId);
            activity.startActivityForResult(intent, 4);
        }
    }

    private void initListeners() {
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                InfoDetailFragment.this.getHoldActivity().finish();
            }
        });
    }

    /* access modifiers changed from: private */
    public void loadContent() {
        String htmlDescription = this.infoItem.getDescription();
        if (StringUtils.isNotEmpty(htmlDescription)) {
            String replacement = getString(R.string.info_tier_link_message);
            while (htmlDescription.contains(LINK_TEXT_TEMPLATE) && !LINK_TEXT_TEMPLATE.equalsIgnoreCase(replacement)) {
                htmlDescription = htmlDescription.replace(LINK_TEXT_TEMPLATE, replacement);
            }
            if (useWithoutZooming(htmlDescription)) {
                ViewUtils.plubWebViewWithoutZooming(this.webView);
            } else {
                ViewUtils.plubWebView(this.webView);
            }
            this.webView.loadDataWithBaseURL(null, StringUtils.decode(htmlDescription), AppConstants.TEXT_HTML, AppConstants.UTF_8_CHARSET, null);
        }
    }

    private boolean useWithoutZooming(String htmlDescription) {
        for (String url : this.customUrls) {
            if (htmlDescription.contains(url)) {
                return true;
            }
        }
        return false;
    }
}
