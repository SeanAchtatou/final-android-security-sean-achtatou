package com.biznessapps.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.google.caching.ImageFetcher;
import com.biznessapps.widgets.RefreshableListView;
import java.util.List;

public abstract class CommonListFragmentNew<T extends CommonListEntity> extends CommonFragment {
    protected static final int FIRST_ITEM = 0;
    protected static final int ONE_ITEM_SIZE = 1;
    protected AbstractAdapter<T> adapter;
    protected int count = 20;
    protected ImageFetcher imageFetcher;
    protected List<T> items;
    protected RefreshableListView listView;
    protected int offset;
    protected final AbstractAdapter.PositionListener positionListener = new AbstractAdapter.PositionListener() {
        public void onLastPositionAchieved(int position) {
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        loadData();
        return this.root;
    }

    public void onResume() {
        super.onResume();
        if (this.adapter != null) {
            this.adapter.notifyDataSetChanged();
        }
    }

    public void onPause() {
        super.onPause();
        this.imageFetcher.flushCache();
    }

    public void onDestroy() {
        super.onDestroy();
        this.listView.removeListeners();
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        super.preDataLoading(holdActivity);
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.items == null || this.items.isEmpty()) {
            return null;
        }
        return ((CommonListEntity) this.items.get(0)).getBackground();
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.listView;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.list_layout;
    }

    /* access modifiers changed from: protected */
    public void initListViewListener() {
        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CommonListFragmentNew.this.onListItemClick(parent, view, position, id);
            }
        });
        this.listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return CommonListFragmentNew.this.onItemLongClick(parent, view, position, id);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(AdapterView<?> adapterView, View view, int position, long id) {
    }

    /* access modifiers changed from: protected */
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
        return false;
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected T getWrappedItem(T r5, java.util.List<T> r6) {
        /*
            r4 = this;
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            boolean r3 = r3.isHasColor()
            if (r3 == 0) goto L_0x002a
            int r1 = r6.size()
            r0 = 0
            r2 = 0
            int r3 = r1 % 2
            if (r3 != 0) goto L_0x002b
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r0 = r3.getOddRowColor()
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r2 = r3.getOddRowTextColor()
        L_0x0024:
            r5.setItemColor(r0)
            r5.setItemTextColor(r2)
        L_0x002a:
            return r5
        L_0x002b:
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r0 = r3.getEvenRowColor()
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r2 = r3.getEvenRowTextColor()
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.CommonListFragmentNew.getWrappedItem(com.biznessapps.model.CommonListEntity, java.util.List):com.biznessapps.model.CommonListEntity");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected T getWrappedItemWithInvertion(T r5, java.util.List<T> r6) {
        /*
            r4 = this;
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            boolean r3 = r3.isHasColor()
            if (r3 == 0) goto L_0x002a
            int r1 = r6.size()
            r0 = 0
            r2 = 0
            int r3 = r1 % 2
            if (r3 != 0) goto L_0x002b
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r0 = r3.getEvenRowColor()
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r2 = r3.getEvenRowTextColor()
        L_0x0024:
            r5.setItemColor(r0)
            r5.setItemTextColor(r2)
        L_0x002a:
            return r5
        L_0x002b:
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r0 = r3.getOddRowColor()
            com.biznessapps.api.AppCore$UiSettings r3 = r4.getUiSettings()
            int r2 = r3.getOddRowTextColor()
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.CommonListFragmentNew.getWrappedItemWithInvertion(com.biznessapps.model.CommonListEntity, java.util.List):com.biznessapps.model.CommonListEntity");
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.listView = (RefreshableListView) ((ViewGroup) root.findViewById(R.id.list_view_root)).findViewById(R.id.list_view);
        this.listView.setPositionListener(this.positionListener);
        this.listView.setItemsCanFocus(false);
        this.listView.setBackgroundColor(getUiSettings().getDefaultListBgColor());
        this.imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
    }
}
