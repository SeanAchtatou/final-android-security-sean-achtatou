package com.biznessapps.fragments.laundry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.reservation.ReservationDataKeeper;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.Map;

public class LaundryCreateAccountFragment extends CommonFragment {
    private EditText userEmailView;
    private EditText userFirstNameView;
    private EditText userLastNameView;
    private EditText userPasswordView;
    private EditText userPhoneView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.userEmailView = (EditText) root.findViewById(R.id.user_email_text);
        this.userFirstNameView = (EditText) root.findViewById(R.id.user_first_name_text);
        this.userLastNameView = (EditText) root.findViewById(R.id.user_last_name_text);
        this.userPhoneView = (EditText) root.findViewById(R.id.user_phone_text);
        this.userPasswordView = (EditText) root.findViewById(R.id.user_password_text);
        final Button registerButton = (Button) root.findViewById(R.id.reservation_register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryCreateAccountFragment.this.registerAccount();
            }
        });
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        registerButton.setTextColor(settings.getButtonTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), registerButton.getBackground());
        this.rightNavigationButton.setVisibility(0);
        this.rightNavigationButton.setText(R.string.register);
        this.rightNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                registerButton.performClick();
            }
        });
        this.bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.laundry_account_registering_layout;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_REGISTER_ACCOUNT_FORMAT, cacher().getAppCode());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        ReservationDataKeeper data = JsonParserUtils.parseSessionToken(dataToParse);
        boolean correctData = StringUtils.isNotEmpty(data.getSessionToken());
        if (correctData) {
            cacher().getReservSystemCacher().setLoggedIn(true);
            cacher().getReservSystemCacher().setSessionToken(data.getSessionToken());
            cacher().getReservSystemCacher().setUserEmail(data.getUserEmail());
            cacher().getReservSystemCacher().setUserFirstName(data.getUserFirstName());
            cacher().getReservSystemCacher().setUserLastName(data.getUserLastName());
            cacher().getReservSystemCacher().setUserPhone(data.getUserPhone());
        }
        return correctData;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        Intent intent = new Intent();
        intent.putExtra(AppConstants.BEFORE_STATE_EXTRA, ReservationSystemConstants.LOGIN);
        holdActivity.setResult(13, intent);
        holdActivity.finish();
    }

    /* access modifiers changed from: private */
    public void registerAccount() {
        String password = this.userPasswordView.getText().toString();
        if (StringUtils.isNotEmpty(password)) {
            Map<String, String> params = AppHttpUtils.getEmptyParams();
            params.put("u", this.userEmailView.getText().toString());
            params.put("f", this.userFirstNameView.getText().toString());
            params.put(ReservationSystemConstants.USER_LAST_NAME, this.userLastNameView.getText().toString());
            params.put("c", this.userPhoneView.getText().toString());
            params.put("p", password);
            loadPostData(params);
            return;
        }
        ViewUtils.showDialog(getHoldActivity(), R.string.reservation_update_password_doesnot_match);
    }
}
