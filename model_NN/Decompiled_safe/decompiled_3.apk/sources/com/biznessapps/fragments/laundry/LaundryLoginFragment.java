package com.biznessapps.fragments.laundry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.reservation.ReservationDataKeeper;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;

public class LaundryLoginFragment extends CommonFragment {
    /* access modifiers changed from: private */
    public Button loginButton;
    private TextView recoveryTextView;
    private EditText userEmailText;
    private EditText userPasswordText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        initListeners();
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Activity activity;
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 13 && (activity = getHoldActivity()) != null) {
            activity.finish();
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.laundry_login_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.userEmailText = (EditText) root.findViewById(R.id.user_email_text);
        this.userPasswordText = (EditText) root.findViewById(R.id.user_password_text);
        this.loginButton = (Button) root.findViewById(R.id.laundry_login_button);
        this.recoveryTextView = (TextView) root.findViewById(R.id.recovery_textview);
        this.rightNavigationButton.setVisibility(0);
        this.rightNavigationButton.setText(R.string.register);
        this.userPasswordText.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                LaundryLoginFragment.this.loginButton.performClick();
            }
        }));
        String backgroundUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        if (StringUtils.isNotEmpty(backgroundUrl)) {
            getImageManager().downloadBgUrl(backgroundUrl, root);
        }
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.loginButton.setTextColor(settings.getButtonTextColor());
        this.recoveryTextView.setTextColor(settings.getFeatureTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.loginButton.getBackground());
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return "";
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        Activity activity = getHoldActivity();
        if (activity != null) {
            activity.setResult(8);
            activity.finish();
        }
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        ReservationDataKeeper data = JsonParserUtils.parseSessionToken(dataToParse);
        boolean correctData = StringUtils.isNotEmpty(data.getSessionToken());
        if (correctData) {
            cacher().getReservSystemCacher().setLoggedIn(true);
            cacher().getReservSystemCacher().setSessionToken(data.getSessionToken());
            cacher().getReservSystemCacher().setUserEmail(data.getUserEmail());
            cacher().getReservSystemCacher().setUserFirstName(data.getUserFirstName());
            cacher().getReservSystemCacher().setUserLastName(data.getUserLastName());
            cacher().getReservSystemCacher().setUserPhone(data.getUserPhone());
        }
        return correctData;
    }

    private void initListeners() {
        this.loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryLoginFragment.this.loadData();
            }
        });
        this.rightNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryLoginFragment.this.openRegisterWindow();
            }
        });
        this.recoveryTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryLoginFragment.this.openRecoveryWindow();
            }
        });
    }

    /* access modifiers changed from: private */
    public void openRegisterWindow() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_REGISTER_ACCOUNT_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_register_account_title));
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void openRecoveryWindow() {
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_ACCOUNT_RECOVERY_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_account_recovery_title));
        startActivityForResult(intent, 0);
    }
}
