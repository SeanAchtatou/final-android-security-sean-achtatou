package com.biznessapps.fragments.laundry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;

public class LaundryStartFragment extends CommonFragment {
    private Button createAccountButton;
    private ViewGroup laundryBgContainer;
    private Button loginButton;
    private Button loginViaFacebook;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.laundry_start_layout, (ViewGroup) null);
        this.laundryBgContainer = (ViewGroup) this.root.findViewById(R.id.laundry_bg_container);
        initViews();
        initListeners();
        loadData();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return "";
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return false;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.laundryBgContainer;
    }

    private void initViews() {
        this.loginButton = (Button) this.root.findViewById(R.id.login_button);
        this.createAccountButton = (Button) this.root.findViewById(R.id.create_account_button);
        this.loginViaFacebook = (Button) this.root.findViewById(R.id.login_with_facebook_button);
        setCustomButtonStyle(this.loginButton);
        setCustomButtonStyle(this.createAccountButton);
        setCustomButtonStyle(this.loginViaFacebook);
        ((TextView) this.root.findViewById(R.id.laundry_start_label)).setTextColor(AppCore.getInstance().getUiSettings().getFeatureTextColor());
        ((TextView) this.root.findViewById(R.id.laundry_start_description)).setTextColor(AppCore.getInstance().getUiSettings().getFeatureTextColor());
    }

    private void initListeners() {
        this.loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryStartFragment.this.login();
            }
        });
        this.createAccountButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryStartFragment.this.createAccount();
            }
        });
        this.loginViaFacebook.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LaundryStartFragment.this.loginWithFacebook();
            }
        });
    }

    /* access modifiers changed from: private */
    public void login() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
        intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, "LAUNDRY_LOGIN_FRAGMENT");
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void createAccount() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
        intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, "LAUNDRY_CREATE_ACCOUNT_FRAGMENT");
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void loginWithFacebook() {
    }
}
