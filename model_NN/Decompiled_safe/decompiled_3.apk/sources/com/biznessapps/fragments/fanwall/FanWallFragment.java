package com.biznessapps.fragments.fanwall;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.model.FanWallItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.List;

public class FanWallFragment extends CommonFragment {
    protected static FanWallItem info;
    protected ViewGroup accountsContentView;
    protected String commentParentId;
    protected ListView commentsListView;
    protected AsyncTask<Void, Void, Void> loadDataTask;
    private boolean needToReload;
    protected ViewGroup rootView;
    protected String tabId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.fan_wall_layout, (ViewGroup) null);
        initViews(this.root);
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (resultCode) {
            case 4:
                this.needToReload = true;
                loadData();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.accountsContentView = (ViewGroup) root.findViewById(R.id.choose_accounts_content);
        this.commentsListView = (ListView) root.findViewById(R.id.comments_list_view);
        this.rootView = (ViewGroup) root.findViewById(R.id.fan_wall_root_layout);
        SocialNetworkAccessor socialAccessor = new SocialNetworkAccessor(getHoldActivity(), root);
        SocialNetworkAccessor.SocialNetworkListener listener = new SocialNetworkAccessor.SocialNetworkListener(socialAccessor) {
            public void onAuthSucceed() {
                FanWallFragment.this.addComment();
            }
        };
        getHoldActivity().setSocialNetworkListener(listener);
        socialAccessor.addAuthorizationListener(listener);
        ViewUtils.setGlobalBackgroundColor(root);
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setItemId(getIntent().getStringExtra("parent_id"));
        return data;
    }

    /* access modifiers changed from: protected */
    public void plugListView(Activity holdActivity) {
        List<FanWallComment> comments;
        boolean hasNoData;
        boolean isParentAdapter = false;
        if (info != null && (comments = info.getComments()) != null && !comments.isEmpty()) {
            if (comments.size() != 1 || !StringUtils.isEmpty(comments.get(0).getId())) {
                hasNoData = false;
            } else {
                hasNoData = true;
            }
            if (hasNoData) {
                comments.get(0).setComment(getString(R.string.no_comments));
            }
            if (this.commentParentId == null) {
                isParentAdapter = true;
            }
            this.commentsListView.setAdapter((ListAdapter) new NewFanWallAdapter(holdActivity.getApplicationContext(), comments, isParentAdapter));
            if (holdActivity.getIntent().getBooleanExtra(AppConstants.HAS_CHILDS, true)) {
                this.commentsListView.setOnItemClickListener(getOnItemClickListener());
            }
        }
    }

    private AdapterView.OnItemClickListener getOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r3v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r6, android.view.View r7, int r8, long r9) {
                /*
                    r5 = this;
                    android.widget.Adapter r3 = r6.getAdapter()
                    java.lang.Object r1 = r3.getItem(r8)
                    com.biznessapps.model.FanWallComment r1 = (com.biznessapps.model.FanWallComment) r1
                    if (r1 == 0) goto L_0x0045
                    android.content.Intent r0 = new android.content.Intent
                    com.biznessapps.fragments.fanwall.FanWallFragment r3 = com.biznessapps.fragments.fanwall.FanWallFragment.this
                    android.content.Context r3 = r3.getApplicationContext()
                    java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r4 = com.biznessapps.activities.SingleFragmentActivity.class
                    r0.<init>(r3, r4)
                    java.lang.String r3 = "parent_id"
                    java.lang.String r4 = r1.getId()
                    r0.putExtra(r3, r4)
                    com.biznessapps.fragments.fanwall.FanWallFragment r3 = com.biznessapps.fragments.fanwall.FanWallFragment.this
                    android.content.Intent r3 = r3.getIntent()
                    java.lang.String r4 = "TAB_SPECIAL_ID"
                    java.lang.String r2 = r3.getStringExtra(r4)
                    java.lang.String r3 = "TAB_FRAGMENT"
                    java.lang.String r4 = "FanWallViewController"
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "HAS_CHILDS"
                    r4 = 0
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "TAB_SPECIAL_ID"
                    r0.putExtra(r3, r2)
                    com.biznessapps.fragments.fanwall.FanWallFragment r3 = com.biznessapps.fragments.fanwall.FanWallFragment.this
                    r3.startActivity(r0)
                L_0x0045:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.fanwall.FanWallFragment.AnonymousClass2.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        };
    }

    /* access modifiers changed from: private */
    public void addComment() {
        Activity activity = getHoldActivity();
        if (activity != null) {
            Intent intent = new Intent(activity.getApplicationContext(), SingleFragmentActivity.class);
            String tabParam = activity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
            intent.putExtra("parent_id", this.commentParentId);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabParam);
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.comments));
            intent.putExtra(AppConstants.YOUTUBE_MODE, activity.getIntent().getBooleanExtra(AppConstants.YOUTUBE_MODE, false));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.FAN_ADD_COMMENT_FRAGMENT);
            startActivityForResult(intent, 4);
        }
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.commentParentId = holdActivity.getIntent().getStringExtra("parent_id");
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        if (getIntent().getBooleanExtra(AppConstants.YOUTUBE_MODE, false)) {
            return String.format(ServerConstants.FAN_WALL_YOUTUBE_FORMAT, cacher().getAppCode(), this.commentParentId, this.tabId);
        }
        return String.format(ServerConstants.FAN_WALL_FORMAT, cacher().getAppCode(), this.commentParentId, this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        boolean canUseCache;
        info = (FanWallItem) cacher().getData(CachingConstants.FAN_WALL_INFO_PROPERTY + this.commentParentId);
        if (info == null || this.needToReload) {
            canUseCache = false;
        } else {
            canUseCache = true;
        }
        this.needToReload = false;
        return canUseCache;
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        List<FanWallItem> items = JsonParserUtils.parseFanWallData(dataToParse);
        if (items != null && !items.isEmpty()) {
            info = items.get(0);
            cacher().saveData(CachingConstants.FAN_WALL_INFO_PROPERTY + this.commentParentId, info);
        }
        return info != null;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (info != null) {
            return info.getImage();
        }
        return null;
    }
}
