package com.biznessapps.fragments.fanwall;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.api.AsyncCallback;
import com.biznessapps.api.CachingManager;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.io.File;

public class FanAddCommentsFragment extends CommonFragment {
    private static final int MESSAGE_TEXT_LIMIT = 250;
    private Button cancelButton;
    private Button clearButton;
    /* access modifiers changed from: private */
    public EditText commentEditView;
    private String commentParentId;
    private byte[] imageToSend;
    private TextView messageCounterView;
    private Button postButton;
    private ImageView takePhotoButton;
    private boolean usePhotoOption;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.add_comments_layout, (ViewGroup) null);
        this.commentParentId = getIntent().getExtras().getString("parent_id");
        this.usePhotoOption = getIntent().getExtras().getBoolean(AppConstants.FAN_WALL_USE_PHOTO_OPTION);
        initViews(this.root);
        initListeners();
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 2:
                this.imageToSend = CommonUtils.convertImageToBytes(CommonUtils.getMadePhoto(data));
                return;
            case 3:
                String selectedImagePath = CommonUtils.getPath(data.getData(), getHoldActivity());
                if (selectedImagePath != null) {
                    this.imageToSend = CommonUtils.convertFileToBytes(new File(selectedImagePath));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.commentEditView = (EditText) root.findViewById(R.id.comment_edit_text);
        this.messageCounterView = (TextView) root.findViewById(R.id.message_countdown);
        this.cancelButton = (Button) root.findViewById(R.id.comments_cancel);
        this.clearButton = (Button) root.findViewById(R.id.comments_clear);
        this.postButton = (Button) root.findViewById(R.id.comments_post);
        this.takePhotoButton = (ImageView) root.findViewById(R.id.comments_take_photo);
        if (!this.usePhotoOption) {
            this.takePhotoButton.setVisibility(8);
        }
        setMessageCounterText(this.commentEditView.getText().toString());
    }

    private void initListeners() {
        this.cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FanAddCommentsFragment.this.getHoldActivity().finish();
            }
        });
        this.clearButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FanAddCommentsFragment.this.commentEditView.setText("");
            }
        });
        this.postButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (FanAddCommentsFragment.this.commentEditView.getText().toString().length() <= FanAddCommentsFragment.MESSAGE_TEXT_LIMIT) {
                    FanAddCommentsFragment.this.postComment();
                }
            }
        });
        this.takePhotoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FanAddCommentsFragment.this.takePhoto();
            }
        });
        this.commentEditView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                FanAddCommentsFragment.this.setMessageCounterText(FanAddCommentsFragment.this.commentEditView.getText().toString());
                if (event.getAction() != 0 || keyCode == 66) {
                }
                return false;
            }
        });
        this.commentEditView.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                FanAddCommentsFragment.this.setMessageCounterText(FanAddCommentsFragment.this.commentEditView.getText().toString());
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void afterTextChanged(Editable arg0) {
            }
        });
    }

    /* access modifiers changed from: private */
    public void postComment() {
        String comment = this.commentEditView.getText().toString();
        String appCode = cacher().getAppCode();
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        if (StringUtils.isEmpty(comment)) {
            Toast.makeText(getApplicationContext(), R.string.comment_must_fill, 1).show();
            return;
        }
        if (StringUtils.checkTextFieldsOnEmpty(tabId, appCode)) {
            closeActivityWithResult(R.string.comment_adding_failure, 5);
        } else if (getIntent().getBooleanExtra(AppConstants.USE_SPECIAL_MD5_HASH_EXTRA, false)) {
            postComment(ServerConstants.COMMENT_POST_FORMAT, AppConstants.MD5_COMMENT_RULE, appCode, tabId, comment, getResultCallback());
        } else {
            postComment(appCode, tabId, comment);
        }
    }

    private void postComment(String appCode, String tabId, String comment) {
        boolean useFacebookCred;
        if (cacher().getLastLoginType() == 1) {
            useFacebookCred = true;
        } else {
            useFacebookCred = false;
        }
        String userName = useFacebookCred ? cacher().getFacebookUserName() : cacher().getTwitterUserName();
        String twitterId = cacher().getTwitterUserName();
        String facebookId = cacher().getFacebookUid();
        Object[] objArr = new Object[1];
        objArr[0] = useFacebookCred ? "" + facebookId : "" + twitterId;
        AppHttpUtils.postCommentAsync(getResultCallback(), useFacebookCred, CommonUtils.getMD5Hash(String.format(AppConstants.DEFAULT_HASH_FORMAT, objArr)), tabId, facebookId, twitterId, userName, comment, appCode, this.commentParentId, getIntent().getBooleanExtra(AppConstants.YOUTUBE_MODE, false));
    }

    private void postComment(String url, String md5rule, String appCode, String tabId, String comment, AsyncCallback<?> callback) {
        CachingManager cacher = AppCore.getInstance().getCachingManager();
        String id = getIntent().getStringExtra("id");
        boolean useFacebookCred = cacher().getLastLoginType() == 1;
        String userId = useFacebookCred ? cacher.getFacebookUid() : cacher.getTwitterUid();
        String userName = useFacebookCred ? cacher.getFacebookUserName() : cacher.getTwitterUserName();
        String userType = useFacebookCred ? "1" : AppConstants.TWITTER_USER_TYPE;
        AppHttpUtils.postCommentAsync(url, appCode, tabId, id, userType, userId, userName, comment, CommonUtils.getMD5Hash(String.format(md5rule, userId, userType)), getIntent().getStringExtra("parent_id"), getIntent().getDoubleExtra("longitude", 0.0d), getIntent().getDoubleExtra("latitude", 0.0d), this.imageToSend, callback);
    }

    private AsyncCallback<String> getResultCallback() {
        return new AsyncCallback<String>() {
            public void onResult(String result) {
                FanAddCommentsFragment.this.closeActivityWithResult(R.string.comment_successfully_added, 4);
            }

            public void onError(String message, Throwable throwable) {
                FanAddCommentsFragment.this.closeActivityWithResult(R.string.comment_adding_failure, 5);
            }
        };
    }

    /* access modifiers changed from: private */
    public void closeActivityWithResult(int toastResId, int resultCode) {
        Activity activity = getHoldActivity();
        if (activity != null) {
            Toast.makeText(activity.getApplicationContext(), toastResId, 1).show();
            activity.setResult(resultCode);
            activity.finish();
        }
    }

    /* access modifiers changed from: private */
    public void takePhoto() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getHoldActivity());
        View view = ViewUtils.loadLayout(getApplicationContext(), R.layout.email_photo_dialog);
        alertBuilder.setView(view);
        alertBuilder.setMessage(R.string.choose_photo);
        final AlertDialog dialog = alertBuilder.create();
        ((Button) view.findViewById(R.id.take_photo_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FanAddCommentsFragment.this.openStandartPhotoView();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        ((Button) view.findViewById(R.id.chose_from_library_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                FanAddCommentsFragment.this.choseFromLibrary();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        ((Button) view.findViewById(R.id.email_photo_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void openStandartPhotoView() {
        startActivityForResult(new Intent(AppConstants.IMAGE_CAPTURE_INTENT_NAME), 2);
    }

    /* access modifiers changed from: private */
    public void choseFromLibrary() {
        Intent intent = new Intent();
        intent.setType(AppConstants.IMAGE_TYPE);
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), 3);
    }

    /* access modifiers changed from: private */
    public void setMessageCounterText(String text) {
        if (text != null) {
            this.messageCounterView.setText(" " + (250 - text.length()));
            if (text.length() > MESSAGE_TEXT_LIMIT) {
                this.messageCounterView.setTextColor(-65536);
            } else {
                this.messageCounterView.setTextColor(-1);
            }
        }
    }
}
