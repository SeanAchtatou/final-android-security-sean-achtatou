package com.biznessapps.fragments.fanwall;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.biznessapps.activities.GalleryPreviewActivity;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.NewFanWallDetailAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.components.SocialNetworkAccessor;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.model.GalleryData;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.utils.google.caching.ImageFetcher;
import java.util.LinkedList;
import java.util.List;

public class NewFanWallFragment extends CommonFragment {
    protected ViewGroup accountsContentView;
    private ImageView attachedImageView;
    protected String commentParentId;
    private List<FanWallComment> comments;
    protected ListView commentsListView;
    protected AsyncTask<Void, Void, Void> loadDataTask;
    private boolean needToReload;
    private TextView parentCommentView;
    private ImageView parentIconView;
    private TextView parentNameView;
    protected ViewGroup rootView;
    private Button seeBigButton;
    protected String tabId;
    private TextView timeAgoView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.new_fan_wall_detail_layout, (ViewGroup) null);
        initViews(this.root);
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch (resultCode) {
            case 4:
                this.needToReload = true;
                loadData();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void initViews(final ViewGroup root) {
        this.accountsContentView = (ViewGroup) root.findViewById(R.id.choose_accounts_content);
        this.commentsListView = (ListView) root.findViewById(R.id.comments_list_view);
        this.rootView = (ViewGroup) root.findViewById(R.id.fan_wall_root_layout);
        this.parentCommentView = (TextView) root.findViewById(R.id.fan_wall_comment);
        this.parentIconView = (ImageView) root.findViewById(R.id.fan_wall_comment_item_image);
        this.attachedImageView = (ImageView) root.findViewById(R.id.fan_wall_attached_image);
        this.seeBigButton = (Button) root.findViewById(R.id.fan_wall_see_big_image);
        this.parentNameView = (TextView) root.findViewById(R.id.fan_wall_name);
        this.timeAgoView = (TextView) root.findViewById(R.id.fan_wall_time_ago);
        SocialNetworkAccessor socialAccessor = new SocialNetworkAccessor(getHoldActivity(), root);
        SocialNetworkAccessor.SocialNetworkListener listener = new SocialNetworkAccessor.SocialNetworkListener(socialAccessor) {
            public void onAuthSucceed() {
                NewFanWallFragment.this.addComment();
            }
        };
        getHoldActivity().setSocialNetworkListener(listener);
        socialAccessor.addAuthorizationListener(listener);
        ((Button) root.findViewById(R.id.send_comment_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ((ImageButton) root.findViewById(R.id.choose_login_account)).performClick();
            }
        });
        ViewUtils.setGlobalBackgroundColor(this.rootView);
    }

    /* access modifiers changed from: protected */
    public void loadData() {
        FanWallComment parentComment = (FanWallComment) getIntent().getSerializableExtra(AppConstants.FAN_WALL_EXTRA);
        if (parentComment != null) {
            this.parentCommentView.setText(parentComment.getComment());
            this.parentNameView.setText(parentComment.getTitle());
            this.timeAgoView.setText(parentComment.getTimeAgo());
            ImageFetcher imageFetcher = AppCore.getInstance().getImageFetcherAccessor().getImageFetcher();
            imageFetcher.loadAppImage(parentComment.getImage(), this.parentIconView);
            final String uploadedImageUrl = parentComment.getUploadImageUrl();
            if (StringUtils.isNotEmpty(uploadedImageUrl)) {
                imageFetcher.loadImage(uploadedImageUrl, this.attachedImageView);
                this.attachedImageView.setVisibility(0);
                this.seeBigButton.setVisibility(0);
                this.seeBigButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        GalleryData.Item item = new GalleryData.Item();
                        item.setFullUrl(uploadedImageUrl);
                        Intent previewIntent = new Intent(NewFanWallFragment.this.getApplicationContext(), GalleryPreviewActivity.class);
                        previewIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.GALLERY_PREVIEW_FRAGMENT);
                        previewIntent.putExtra(AppConstants.GALLERY_PREVIEW_EXTRA, item);
                        NewFanWallFragment.this.startActivity(previewIntent);
                    }
                });
            } else {
                this.seeBigButton.setVisibility(8);
                this.attachedImageView.setVisibility(8);
            }
        }
        super.loadData();
    }

    private void plugListView(Activity holdActivity) {
        boolean hasNoData;
        if (this.comments != null && !this.comments.isEmpty()) {
            List<FanWallComment> resultList = new LinkedList<>();
            if (this.comments.size() != 1 || !StringUtils.isEmpty(this.comments.get(0).getId())) {
                hasNoData = false;
            } else {
                hasNoData = true;
            }
            if (hasNoData) {
                this.comments.get(0).setTitle(getString(R.string.no_comments));
            }
            for (FanWallComment item : this.comments) {
                item.setImageId(R.drawable.portrait);
                resultList.add(item);
            }
            this.commentsListView.setAdapter((ListAdapter) new NewFanWallDetailAdapter(getApplicationContext(), resultList));
            if (holdActivity.getIntent().getBooleanExtra(AppConstants.HAS_CHILDS, true)) {
                this.commentsListView.setOnItemClickListener(getOnItemClickListener());
            }
        }
    }

    private AdapterView.OnItemClickListener getOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            /* JADX WARN: Type inference failed for: r3v0, types: [android.widget.Adapter] */
            /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
                jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
                	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
                	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
                	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
                	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
                	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
                	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
                	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
                */
            public void onItemClick(android.widget.AdapterView<?> r6, android.view.View r7, int r8, long r9) {
                /*
                    r5 = this;
                    android.widget.Adapter r3 = r6.getAdapter()
                    java.lang.Object r1 = r3.getItem(r8)
                    com.biznessapps.model.FanWallComment r1 = (com.biznessapps.model.FanWallComment) r1
                    if (r1 == 0) goto L_0x0045
                    android.content.Intent r0 = new android.content.Intent
                    com.biznessapps.fragments.fanwall.NewFanWallFragment r3 = com.biznessapps.fragments.fanwall.NewFanWallFragment.this
                    android.content.Context r3 = r3.getApplicationContext()
                    java.lang.Class<com.biznessapps.activities.SingleFragmentActivity> r4 = com.biznessapps.activities.SingleFragmentActivity.class
                    r0.<init>(r3, r4)
                    java.lang.String r3 = "parent_id"
                    java.lang.String r4 = r1.getId()
                    r0.putExtra(r3, r4)
                    com.biznessapps.fragments.fanwall.NewFanWallFragment r3 = com.biznessapps.fragments.fanwall.NewFanWallFragment.this
                    android.content.Intent r3 = r3.getIntent()
                    java.lang.String r4 = "TAB_SPECIAL_ID"
                    java.lang.String r2 = r3.getStringExtra(r4)
                    java.lang.String r3 = "TAB_FRAGMENT"
                    java.lang.String r4 = "FanWallViewController"
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "HAS_CHILDS"
                    r4 = 0
                    r0.putExtra(r3, r4)
                    java.lang.String r3 = "TAB_SPECIAL_ID"
                    r0.putExtra(r3, r2)
                    com.biznessapps.fragments.fanwall.NewFanWallFragment r3 = com.biznessapps.fragments.fanwall.NewFanWallFragment.this
                    r3.startActivity(r0)
                L_0x0045:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.fanwall.NewFanWallFragment.AnonymousClass4.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
            }
        };
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: private */
    public void addComment() {
        Activity activity = getHoldActivity();
        if (activity != null) {
            Intent intent = new Intent(activity.getApplicationContext(), SingleFragmentActivity.class);
            String tabParam = activity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
            intent.putExtra("parent_id", this.commentParentId);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabParam);
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.comments));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.FAN_ADD_COMMENT_FRAGMENT);
            intent.putExtra(AppConstants.USE_SPECIAL_MD5_HASH_EXTRA, true);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
            startActivityForResult(intent, 4);
        }
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.commentParentId = holdActivity.getIntent().getStringExtra("parent_id");
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.COMMENT_LIST_FORMAT, cacher().getAppCode(), "", this.tabId) + String.format(ServerConstants.PARENT_ID_PARAM, this.commentParentId);
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setItemId(getIntent().getStringExtra("parent_id"));
        return data;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        boolean canUseCache;
        this.comments = (List) cacher().getData(CachingConstants.FAN_WALL_COMMENT_PROPERTY + this.commentParentId);
        if (this.comments == null || this.needToReload) {
            canUseCache = false;
        } else {
            canUseCache = true;
        }
        this.needToReload = false;
        return canUseCache;
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.comments = JsonParserUtils.parseFanComments(dataToParse);
        return cacher().saveData(CachingConstants.FAN_WALL_COMMENT_PROPERTY + this.commentParentId, this.comments);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.rootView;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.comments == null || this.comments.isEmpty()) {
            return null;
        }
        return this.comments.get(0).getBackground();
    }
}
