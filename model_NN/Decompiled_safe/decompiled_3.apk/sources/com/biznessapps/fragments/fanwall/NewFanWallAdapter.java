package com.biznessapps.fragments.fanwall;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.FanWallComment;
import com.biznessapps.utils.StringUtils;
import java.util.List;

public class NewFanWallAdapter extends AbstractAdapter<FanWallComment> {
    private boolean isParentAdapter;

    public NewFanWallAdapter(Context context, List<FanWallComment> items, boolean isParentAdapter2) {
        super(context, items, R.layout.new_fan_wall_item_layout);
        this.isParentAdapter = isParentAdapter2;
    }

    public NewFanWallAdapter(Context context, List<FanWallComment> items, boolean isParentAdapter2, int layoutResId) {
        super(context, items, layoutResId);
        this.isParentAdapter = isParentAdapter2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.FanWallItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.FanWallItem();
            holder.setCommentTextView((TextView) convertView.findViewById(R.id.fan_wall_comment));
            holder.setNameTextView((TextView) convertView.findViewById(R.id.fan_wall_name));
            holder.setTimeAgoTextView((TextView) convertView.findViewById(R.id.fan_wall_time_ago));
            holder.setReplyTextView((TextView) convertView.findViewById(R.id.fan_wall_comment_replies));
            holder.setFanWallImageView((ImageView) convertView.findViewById(R.id.fan_wall_comment_item_image));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.FanWallItem) convertView.getTag();
        }
        FanWallComment item = (FanWallComment) this.items.get(position);
        if (item != null) {
            holder.getCommentTextView().setText(item.getComment());
            holder.getNameTextView().setText(item.getTitle());
            holder.getTimeAgoTextView().setText(Html.fromHtml(item.getTimeAgo()));
            if (StringUtils.isNotEmpty(item.getImage())) {
                this.imageFetcher.loadImage(item.getImage(), holder.getFanWallImageView());
            }
            holder.getReplyTextView().setVisibility(this.isParentAdapter ? 0 : 8);
        }
        return convertView;
    }
}
