package com.biznessapps.fragments;

import android.view.View;

public interface SwipeyTabsAdapter {
    int getCount();

    View getTab(int i);
}
