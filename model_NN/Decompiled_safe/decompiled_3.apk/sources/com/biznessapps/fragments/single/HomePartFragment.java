package com.biznessapps.fragments.single;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.biznessapps.api.navigation.NavigationManager;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.Tab;
import java.util.ArrayList;
import java.util.List;

public class HomePartFragment extends CommonFragment {
    private int columnCount;
    private boolean hasMoreButtonNavigation;
    private int rowCount;
    private List<Tab> tabs;

    public void setTabs(List<Tab> tabs2) {
        this.tabs = tabs2;
    }

    public void setRowCount(int rowCount2) {
        this.rowCount = rowCount2;
    }

    public void setColumnCount(int columnCount2) {
        this.columnCount = columnCount2;
    }

    public void setHasMoreButtonNavigation(boolean hasMoreButtonNavigation2) {
        this.hasMoreButtonNavigation = hasMoreButtonNavigation2;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.home_part_layout, (ViewGroup) null);
        initTabs();
        return this.root;
    }

    private void initTabs() {
        List<FrameLayout> rows = new ArrayList<>();
        rows.add((FrameLayout) this.root.findViewById(R.id.bottom_navig_conrol_container_row1));
        rows.add((FrameLayout) this.root.findViewById(R.id.bottom_navig_conrol_container_row2));
        rows.add((FrameLayout) this.root.findViewById(R.id.bottom_navig_conrol_container_row3));
        rows.add((FrameLayout) this.root.findViewById(R.id.bottom_navig_conrol_container_row4));
        int i = 0;
        while (i < rows.size() && i < this.rowCount) {
            ((FrameLayout) rows.get(i)).setVisibility(0);
            i++;
        }
        for (int i2 = 0; i2 < this.rowCount; i2++) {
            if (this.tabs.size() > this.columnCount * i2) {
                int highLimit = (i2 + 1) * this.columnCount;
                int missedTabs = highLimit - this.tabs.size();
                if (highLimit > this.tabs.size()) {
                    highLimit = this.tabs.size();
                }
                if (this.hasMoreButtonNavigation && i2 == this.rowCount - 1) {
                    highLimit = this.tabs.size();
                }
                List<Tab> subList = new ArrayList<>(this.tabs.subList(this.columnCount * i2, highLimit));
                for (int j = 0; j < missedTabs; j++) {
                    Tab tab = new Tab();
                    tab.setActive(false);
                    subList.add(tab);
                }
                addRow((FrameLayout) rows.get(i2), new ArrayList(subList));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.biznessapps.api.navigation.NavigationManager.<init>(android.app.Activity, boolean):void
     arg types: [com.biznessapps.activities.CommonFragmentActivity, int]
     candidates:
      com.biznessapps.api.navigation.NavigationManager.<init>(android.app.Activity, int):void
      com.biznessapps.api.navigation.NavigationManager.<init>(android.app.Activity, boolean):void */
    private void addRow(ViewGroup row, List<Tab> rowsTab) {
        NavigationManager navigManagerRow = new NavigationManager((Activity) getHoldActivity(), false);
        navigManagerRow.setRowTabsItems(rowsTab);
        navigManagerRow.addLayoutTo(row);
        navigManagerRow.updateTabs();
        navigManagerRow.resetTabsSelection();
    }
}
