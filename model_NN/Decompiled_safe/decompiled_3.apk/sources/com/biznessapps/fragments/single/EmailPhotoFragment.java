package com.biznessapps.fragments.single;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.EmailPhotoItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.io.File;
import java.util.ArrayList;

public class EmailPhotoFragment extends CommonFragment {
    private static final String EMAIL_PHOTO_IMAGE = "email_photo_image.jpg";
    private static final int PHOTO_WAS_SELECTED = 1;
    private static final int PICTURE_WAS_SELECTED = 2;
    private Button emailPhotoButton;
    private TextView emailPhotoTextView;
    private EmailPhotoItem info;
    private File photoImage;
    private String selectedImagePath;
    private String tabId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.email_photo_layout, (ViewGroup) null);
        initViews();
        initListeners();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.EMAIL_PHOTO_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.info = JsonParserUtils.parseEmailPhoto(dataToParse);
        return cacher().saveData(CachingConstants.EMAIL_PHOTO_INFO_PROPERTY + this.tabId, this.info);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (StringUtils.isNotEmpty(this.info.getDescription())) {
            this.emailPhotoTextView.setText(this.info.getDescription());
        }
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.info != null) {
            return this.info.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root.findViewById(R.id.email_photo_root);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.info = (EmailPhotoItem) cacher().getData(CachingConstants.EMAIL_PHOTO_INFO_PROPERTY + this.tabId);
        return this.info != null;
    }

    private void initViews() {
        this.emailPhotoButton = (Button) this.root.findViewById(R.id.email_photo_button);
        this.emailPhotoTextView = (TextView) this.root.findViewById(R.id.email_photo_description);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.emailPhotoButton.setTextColor(settings.getButtonTextColor());
        ((TextView) this.root.findViewById(R.id.email_photo_title)).setTextColor(settings.getFeatureTextColor());
        this.emailPhotoTextView.setTextColor(settings.getFeatureTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.emailPhotoButton.getBackground());
        ViewUtils.setGlobalBackgroundColor(this.root.findViewById(R.id.email_photo_root));
    }

    private void initListeners() {
        this.emailPhotoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EmailPhotoFragment.this.emailPhoto();
            }
        });
    }

    /* access modifiers changed from: private */
    public void emailPhoto() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getHoldActivity());
        View view = ViewUtils.loadLayout(getApplicationContext(), R.layout.email_photo_dialog);
        alertBuilder.setView(view);
        alertBuilder.setMessage(R.string.choose_photo);
        final AlertDialog dialog = alertBuilder.create();
        ((Button) view.findViewById(R.id.take_photo_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EmailPhotoFragment.this.openStandartPhotoView();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        ((Button) view.findViewById(R.id.chose_from_library_button)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EmailPhotoFragment.this.choseFromLibrary();
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        ((Button) view.findViewById(R.id.email_photo_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    /* access modifiers changed from: private */
    public void choseFromLibrary() {
        Intent intent = new Intent();
        intent.setType(AppConstants.IMAGE_TYPE);
        intent.setAction("android.intent.action.GET_CONTENT");
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), 2);
    }

    /* access modifiers changed from: private */
    public void openStandartPhotoView() {
        Intent intent = new Intent(AppConstants.IMAGE_CAPTURE_INTENT_NAME);
        if ("mounted".equals(Environment.getExternalStorageState())) {
            this.photoImage = new File(Environment.getExternalStorageDirectory(), EMAIL_PHOTO_IMAGE);
            intent.putExtra("output", Uri.fromFile(this.photoImage));
            startActivityForResult(intent, 1);
            return;
        }
        Toast.makeText(getApplicationContext(), R.string.sdcard_missed, 1).show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        this.info = (EmailPhotoItem) cacher().getData(CachingConstants.EMAIL_PHOTO_INFO_PROPERTY + this.tabId);
        if (-1 == resultCode && this.info != null) {
            switch (requestCode) {
                case 1:
                    email(getApplicationContext(), this.info.getEmail(), this.info.getSubject(), this.info.getDescription(), new File(Environment.getExternalStorageDirectory(), EMAIL_PHOTO_IMAGE));
                    return;
                case 2:
                    this.selectedImagePath = getPath(intent.getData());
                    if (this.selectedImagePath != null) {
                        email(getApplicationContext(), this.info.getEmail(), this.info.getSubject(), this.info.getDescription(), new File(this.selectedImagePath));
                        return;
                    }
                    return;
                default:
                    super.onActivityResult(requestCode, resultCode, intent);
                    return;
            }
        }
    }

    private String getPath(Uri uri) {
        Cursor cursor = getHoldActivity().managedQuery(uri, new String[]{"_data"}, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow("_data");
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void email(Context context, String emailTo, String subject, String emailText, File fileToSend) {
        Intent intent = new Intent("android.intent.action.SEND_MULTIPLE");
        intent.setFlags(268435456);
        intent.setType("plain/text");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{emailTo});
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", emailText);
        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(Uri.fromFile(fileToSend));
        intent.putParcelableArrayListExtra("android.intent.extra.STREAM", uris);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_email)));
    }
}
