package com.biznessapps.fragments.single;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.StatFieldsItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FlexibleCounterFragment extends CommonFragment {
    private static final String LINE_END = "\n";
    private static final String STATS_SPACE = "        ";
    private ImageButton clearButton;
    /* access modifiers changed from: private */
    public StatFieldsItem data;
    private Button emailButton;
    private ViewGroup fieldsContainer;
    /* access modifiers changed from: private */
    public List<Integer> fieldsCounters;
    private ViewGroup layout;
    private String tabId;
    /* access modifiers changed from: private */
    public String tabLabel;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.flexible_counter_layout, (ViewGroup) null);
        this.layout = (LinearLayout) this.root.findViewById(R.id.counter_root_layout);
        initViews();
        initListeners();
        loadData();
        if (getIntent().getExtras() != null) {
            this.tabLabel = getIntent().getExtras().getString(AppConstants.TAB_LABEL);
        }
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onPause() {
        super.onPause();
        cacher().saveInSharedPreferences(getApplicationContext(), this.fieldsCounters.toString(), AppConstants.FLEXIBLE_COUNTER_KEY + this.tabId);
    }

    private void initViews() {
        this.fieldsContainer = (ViewGroup) this.root.findViewById(R.id.stat_fields_container);
        this.emailButton = (Button) this.root.findViewById(R.id.email_counter);
        this.clearButton = (ImageButton) this.root.findViewById(R.id.clear_icon);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.emailButton.setTextColor(settings.getButtonTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.emailButton.getBackground());
        ViewUtils.setGlobalBackgroundColor(this.layout);
    }

    private void initListeners() {
        this.emailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                StringBuilder bodyText = new StringBuilder();
                bodyText.append(FlexibleCounterFragment.this.data.getMessage() == null ? " " : FlexibleCounterFragment.this.data.getMessage()).append("\n").append("\n");
                List<String> fieldNames = FlexibleCounterFragment.this.data.getFields();
                if (fieldNames != null && fieldNames.size() > 0) {
                    for (int i = 0; i < fieldNames.size(); i++) {
                        bodyText.append(fieldNames.get(i)).append(FlexibleCounterFragment.STATS_SPACE).append(FlexibleCounterFragment.this.fieldsCounters.get(i)).append("\n");
                    }
                }
                bodyText.append("\n");
                ViewUtils.email(FlexibleCounterFragment.this.getHoldActivity(), new String[]{FlexibleCounterFragment.this.data.getEmail()}, FlexibleCounterFragment.this.tabLabel, bodyText.toString());
            }
        });
        this.clearButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(FlexibleCounterFragment.this.getHoldActivity());
                builder.setMessage(R.string.sport_stats_clear_text).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if (FlexibleCounterFragment.this.fieldsCounters != null && !FlexibleCounterFragment.this.fieldsCounters.isEmpty()) {
                            for (int i = 0; i < FlexibleCounterFragment.this.fieldsCounters.size(); i++) {
                                FlexibleCounterFragment.this.fieldsCounters.set(i, 0);
                            }
                        }
                        FlexibleCounterFragment.this.initFieldsCounters(FlexibleCounterFragment.this.getActivity());
                    }
                });
                builder.create().show();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = getIntent().getExtras().getString(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.STAT_FIEDS_URL_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.data = JsonParserUtils.parseStatFields(dataToParse);
        return cacher().saveData(CachingConstants.FLEXIBLE_COUNTER_PROPERTY + this.tabId, this.data);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.data != null) {
            return this.data.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.layout;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.data = (StatFieldsItem) cacher().getData(CachingConstants.FLEXIBLE_COUNTER_PROPERTY + this.tabId);
        String result = cacher().getFromSharedPreferences(getApplicationContext(), AppConstants.FLEXIBLE_COUNTER_KEY + this.tabId);
        if (result != null) {
            this.fieldsCounters = convertStringToArrayList(result);
        } else {
            this.fieldsCounters = new ArrayList();
        }
        return this.data != null;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        initFieldsCounters(holdActivity);
    }

    /* access modifiers changed from: private */
    public void initFieldsCounters(Activity holdActivity) {
        List<String> fields = this.data.getFields();
        if (fields != null && !fields.isEmpty()) {
            this.fieldsContainer.removeAllViews();
            int i = 0;
            for (String field : fields) {
                final int currentPos = i;
                if (this.fieldsCounters.size() == currentPos) {
                    this.fieldsCounters.add(0);
                }
                ViewGroup fieldView = (ViewGroup) ViewUtils.loadLayout(holdActivity.getApplicationContext(), R.layout.flexible_counter_item);
                TextView counterTextName = (TextView) fieldView.findViewById(R.id.counter_text_name);
                counterTextName.setText(field);
                final TextView counterView = (TextView) fieldView.findViewById(R.id.counter_text);
                counterView.setText("" + this.fieldsCounters.get(currentPos));
                Button counterDecButton = (Button) fieldView.findViewById(R.id.counter_decrease_button);
                counterTextName.setText(field);
                Button counterIncButton = (Button) fieldView.findViewById(R.id.counter_increase_button);
                counterTextName.setText(field);
                counterIncButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        FlexibleCounterFragment.this.fieldsCounters.set(currentPos, Integer.valueOf(((Integer) FlexibleCounterFragment.this.fieldsCounters.get(currentPos)).intValue() + 1));
                        counterView.setText("" + FlexibleCounterFragment.this.fieldsCounters.get(currentPos));
                    }
                });
                counterDecButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        FlexibleCounterFragment.this.fieldsCounters.set(currentPos, Integer.valueOf(((Integer) FlexibleCounterFragment.this.fieldsCounters.get(currentPos)).intValue() - 1));
                        counterView.setText("" + FlexibleCounterFragment.this.fieldsCounters.get(currentPos));
                    }
                });
                AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
                counterDecButton.setTextColor(settings.getButtonTextColor());
                counterIncButton.setTextColor(settings.getButtonTextColor());
                CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), counterDecButton.getBackground());
                CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), counterIncButton.getBackground());
                this.fieldsContainer.addView(fieldView);
                i++;
            }
        }
    }

    private List<Integer> convertStringToArrayList(String stringValues) {
        List<String> items = Arrays.asList(stringValues.replaceAll("\\[", "").replaceAll("\\]", "").split(","));
        List<Integer> numbers = new ArrayList<>();
        for (String item : items) {
            if (StringUtils.isNotEmpty(item)) {
                numbers.add(Integer.valueOf(Integer.parseInt(item.trim())));
            }
        }
        return numbers;
    }
}
