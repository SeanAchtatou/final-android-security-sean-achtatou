package com.biznessapps.fragments.single;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.delegate.TellFriendDelegate;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.ViewUtils;

public class TellFriendsFragment extends CommonFragment {
    private ViewGroup layout;
    private String tabId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.tell_friend_layout, (ViewGroup) null);
        this.layout = (ViewGroup) this.root.findViewById(R.id.home_layout_container);
        initViews();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format("http://biznessapps.com/iphone/tellfriend.php?app_code=%s&version=4&tab_id=%s", cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.bgUrl = JsonParserUtils.getTellFriendImage(dataToParse);
        return cacher().saveData(CachingConstants.TELL_FRIEND_PROPERTY + this.tabId, this.bgUrl);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.bgUrl = (String) cacher().getData(CachingConstants.TELL_FRIEND_PROPERTY + this.tabId);
        return this.bgUrl != null;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return this.bgUrl;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.layout;
    }

    private void initViews() {
        final ViewGroup tellFriendContent = (ViewGroup) this.root.findViewById(R.id.tell_friends_content);
        Button tellButton = (Button) this.root.findViewById(R.id.tell_friend_button);
        tellButton.setVisibility(0);
        tellButton.setText(getString(R.string.share));
        tellButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                TellFriendDelegate.openFriendContent(TellFriendsFragment.this.getApplicationContext(), tellFriendContent);
            }
        });
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), tellButton.getBackground());
        tellButton.setTextColor(settings.getButtonTextColor());
        TellFriendDelegate.initTellFriends(getHoldActivity(), tellFriendContent);
        int footerHeight = (int) getResources().getDimension(R.dimen.footer_bar_height);
        if (getHoldActivity().getMusicDelegate() != null && getHoldActivity().getMusicDelegate().isActive()) {
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) tellButton.getLayoutParams();
            lp.setMargins(0, 0, 0, footerHeight);
            tellButton.setLayoutParams(lp);
        }
        ViewUtils.setGlobalBackgroundColor(this.layout);
    }
}
