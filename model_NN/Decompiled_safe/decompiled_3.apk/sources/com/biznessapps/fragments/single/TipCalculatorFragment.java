package com.biznessapps.fragments.single;

import android.app.Activity;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.Tip;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.text.DecimalFormat;

public class TipCalculatorFragment extends CommonFragment {
    private static final int DEFAULT_TIP_PERCENT = 15;
    private static final int DEFAULT_TIP_VALUE = 0;
    private static final String VALUE_FORMAT = "%s";
    private EditText amountEditText;
    private TextView amountLabel;
    private Button calculateButton;
    private TextView eachPersonLabel;
    private TextView eachPersonText;
    /* access modifiers changed from: private */
    public Button fivePersonCount;
    /* access modifiers changed from: private */
    public Button fourPersonCount;
    private int numberOfPeople;
    private TextView numberOfPeopleLabel;
    /* access modifiers changed from: private */
    public Button onePersonCount;
    private Button resetButton;
    private SeekBar.OnSeekBarChangeListener seekBarListener = new SeekBar.OnSeekBarChangeListener() {
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            int unused = TipCalculatorFragment.this.tipProgressValue = progress;
            TipCalculatorFragment.this.tipPersonText.setText(TipCalculatorFragment.this.tipProgressValue + "%");
            TipCalculatorFragment.this.calculate();
        }
    };
    private Button selectedButton;
    /* access modifiers changed from: private */
    public Button sixPersonCount;
    private String tabId;
    /* access modifiers changed from: private */
    public Button threePersonCount;
    private TextView tipAmountLabel;
    private TextView tipAmountText;
    private TextView tipEachPersonLabel;
    private TextView tipEachPersonText;
    private Tip tipInfo;
    private ViewGroup tipLayout;
    private TextView tipPersentLabel;
    /* access modifiers changed from: private */
    public TextView tipPersonText;
    /* access modifiers changed from: private */
    public int tipProgressValue;
    private SeekBar tipSeekBar;
    private TextView totalTipLabel;
    private TextView totalTipText;
    /* access modifiers changed from: private */
    public Button twoPersonCount;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.tip_calculator_layout, (ViewGroup) null);
        initViews(this.root);
        initListeners();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.TIP_URL_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.tipInfo = JsonParserUtils.parseTip(dataToParse);
        return cacher().saveData(CachingConstants.TIP_INFO_PROPERTY + this.tabId, this.tipInfo);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.tipInfo = (Tip) cacher().getData(CachingConstants.TIP_INFO_PROPERTY + this.tabId);
        return this.tipInfo != null;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.tipInfo != null) {
            return this.tipInfo.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.tipLayout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.amountEditText = (EditText) root.findViewById(R.id.amount_text);
        this.amountLabel = (TextView) root.findViewById(R.id.amount_label);
        this.numberOfPeopleLabel = (TextView) root.findViewById(R.id.people_label);
        this.tipPersentLabel = (TextView) root.findViewById(R.id.tip_percent_label);
        this.totalTipLabel = (TextView) root.findViewById(R.id.total_tip_label);
        this.tipAmountLabel = (TextView) root.findViewById(R.id.total_amount_label);
        this.tipEachPersonLabel = (TextView) root.findViewById(R.id.tip_each_person_label);
        this.eachPersonLabel = (TextView) root.findViewById(R.id.each_person_label);
        this.totalTipText = (TextView) root.findViewById(R.id.total_tip_text);
        this.tipAmountText = (TextView) root.findViewById(R.id.total_amount_text);
        this.tipEachPersonText = (TextView) root.findViewById(R.id.tip_each_person_text);
        this.eachPersonText = (TextView) root.findViewById(R.id.each_person_text);
        this.tipPersonText = (TextView) root.findViewById(R.id.tip_percent_text);
        this.calculateButton = (Button) root.findViewById(R.id.calculate_button);
        this.resetButton = (Button) root.findViewById(R.id.reset_button);
        this.onePersonCount = (Button) root.findViewById(R.id.tip_one_person_count);
        this.twoPersonCount = (Button) root.findViewById(R.id.tip_two_person_count);
        this.threePersonCount = (Button) root.findViewById(R.id.tip_three_person_count);
        this.fourPersonCount = (Button) root.findViewById(R.id.tip_four_person_count);
        this.fivePersonCount = (Button) root.findViewById(R.id.tip_five_person_count);
        this.sixPersonCount = (Button) root.findViewById(R.id.tip_six_person_count);
        selectButton(this.onePersonCount);
        this.tipLayout = (LinearLayout) root.findViewById(R.id.tip_layout);
        this.tipSeekBar = (SeekBar) root.findViewById(R.id.tip_progress_horizontal);
        this.tipSeekBar.setOnSeekBarChangeListener(this.seekBarListener);
        this.tipProgressValue = this.tipSeekBar.getProgress();
        this.tipPersonText.setText(this.tipProgressValue + "%");
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((ImageView) root.findViewById(R.id.check_total_image)).getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((ImageView) root.findViewById(R.id.tip_percentage_image)).getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((ImageView) root.findViewById(R.id.number_of_people_image)).getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), this.amountEditText.getBackground());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.resetButton.getBackground());
        this.amountLabel.setTextColor(settings.getFeatureTextColor());
        this.numberOfPeopleLabel.setTextColor(settings.getFeatureTextColor());
        this.tipPersentLabel.setTextColor(settings.getFeatureTextColor());
        this.totalTipLabel.setTextColor(settings.getFeatureTextColor());
        this.tipAmountLabel.setTextColor(settings.getFeatureTextColor());
        this.tipEachPersonLabel.setTextColor(settings.getFeatureTextColor());
        this.eachPersonLabel.setTextColor(settings.getFeatureTextColor());
        this.totalTipText.setTextColor(settings.getFeatureTextColor());
        this.tipAmountText.setTextColor(settings.getFeatureTextColor());
        this.tipEachPersonText.setTextColor(settings.getFeatureTextColor());
        this.eachPersonText.setTextColor(settings.getFeatureTextColor());
        this.tipPersonText.setTextColor(settings.getFeatureTextColor());
        this.onePersonCount.setTextColor(settings.getButtonTextColor());
        this.twoPersonCount.setTextColor(settings.getButtonTextColor());
        this.threePersonCount.setTextColor(settings.getButtonTextColor());
        this.fourPersonCount.setTextColor(settings.getButtonTextColor());
        this.fivePersonCount.setTextColor(settings.getButtonTextColor());
        this.sixPersonCount.setTextColor(settings.getButtonTextColor());
        this.calculateButton.setTextColor(settings.getButtonTextColor());
        this.resetButton.setTextColor(settings.getButtonTextColor());
        this.amountEditText.setTextColor(settings.getButtonTextColor());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((LayerDrawable) this.tipSeekBar.getProgressDrawable()).getDrawable(1));
        ViewUtils.setGlobalBackgroundColor(this.tipLayout);
    }

    private void initListeners() {
        this.calculateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.calculate();
            }
        });
        this.resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.reset();
            }
        });
        this.onePersonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.selectButton(TipCalculatorFragment.this.onePersonCount);
                TipCalculatorFragment.this.calculate();
            }
        });
        this.twoPersonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.selectButton(TipCalculatorFragment.this.twoPersonCount);
                TipCalculatorFragment.this.calculate();
            }
        });
        this.threePersonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.selectButton(TipCalculatorFragment.this.threePersonCount);
                TipCalculatorFragment.this.calculate();
            }
        });
        this.fourPersonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.selectButton(TipCalculatorFragment.this.fourPersonCount);
                TipCalculatorFragment.this.calculate();
            }
        });
        this.fivePersonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.selectButton(TipCalculatorFragment.this.fivePersonCount);
                TipCalculatorFragment.this.calculate();
            }
        });
        this.sixPersonCount.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TipCalculatorFragment.this.selectButton(TipCalculatorFragment.this.sixPersonCount);
                TipCalculatorFragment.this.calculate();
            }
        });
        this.amountEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                TipCalculatorFragment.this.calculate();
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void calculate() {
        String amountPrice = this.amountEditText.getText().toString();
        if (StringUtils.isNotEmpty(amountPrice)) {
            try {
                double totalCheck = Double.parseDouble(amountPrice);
                this.numberOfPeople = Integer.parseInt(this.selectedButton.getText().toString());
                if (!checkFieldsCorrectness(totalCheck)) {
                    Toast.makeText(getApplicationContext(), R.string.number_incorrect_format_message, 1).show();
                    return;
                }
                double totalTip = (totalCheck / 100.0d) * ((double) this.tipProgressValue);
                double tipEach = totalTip / ((double) this.numberOfPeople);
                double totalEach = (totalCheck / ((double) this.numberOfPeople)) + tipEach;
                DecimalFormat df = new DecimalFormat("##.00");
                this.totalTipText.setText(String.format(VALUE_FORMAT, df.format(totalTip)));
                this.tipAmountText.setText(String.format(VALUE_FORMAT, df.format(totalCheck + totalTip)));
                this.eachPersonText.setText(String.format(VALUE_FORMAT, df.format(totalEach)));
                this.tipEachPersonText.setText(String.format(VALUE_FORMAT, df.format(tipEach)));
                this.calculateButton.setVisibility(0);
            } catch (NumberFormatException e) {
                Toast.makeText(getApplicationContext(), R.string.number_incorrect_format_message, 1).show();
            }
        } else {
            this.calculateButton.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void reset() {
        this.tipProgressValue = 15;
        this.tipPersonText.setText(this.tipProgressValue + "%");
        this.tipSeekBar.setProgress(this.tipProgressValue);
        this.amountEditText.setText("");
        DecimalFormat df = new DecimalFormat("##.00");
        this.totalTipText.setText(String.format(VALUE_FORMAT, df.format(0L)));
        this.tipAmountText.setText(String.format(VALUE_FORMAT, df.format(0L)));
        this.eachPersonText.setText(String.format(VALUE_FORMAT, df.format(0L)));
        this.tipEachPersonText.setText(String.format(VALUE_FORMAT, df.format(0L)));
        selectButton(this.onePersonCount);
    }

    private boolean checkFieldsCorrectness(double amountValue) {
        if (amountValue != 0.0d && amountValue >= 1.0d && amountValue <= 9.9999999E7d) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void selectButton(Button buttonToSelect) {
        this.onePersonCount.setBackgroundResource(R.drawable.tip_left_button_unselected);
        this.twoPersonCount.setBackgroundResource(R.drawable.tip_center_button_unselected);
        this.threePersonCount.setBackgroundResource(R.drawable.tip_center_button_unselected);
        this.fourPersonCount.setBackgroundResource(R.drawable.tip_center_button_unselected);
        this.fivePersonCount.setBackgroundResource(R.drawable.tip_center_button_unselected);
        this.sixPersonCount.setBackgroundResource(R.drawable.tip_right_button_unselected);
        buttonToSelect.setSelected(true);
        if (buttonToSelect.equals(this.onePersonCount)) {
            buttonToSelect.setBackgroundResource(R.drawable.tip_left_button_selected);
        } else if (buttonToSelect.equals(this.sixPersonCount)) {
            buttonToSelect.setBackgroundResource(R.drawable.tip_right_button_selected);
        } else {
            buttonToSelect.setBackgroundResource(R.drawable.tip_center_button_selected);
        }
        this.selectedButton = buttonToSelect;
        CommonUtils.overrideImageColor(AppCore.getInstance().getUiSettings().getButtonBgColor(), this.selectedButton.getBackground());
    }
}
