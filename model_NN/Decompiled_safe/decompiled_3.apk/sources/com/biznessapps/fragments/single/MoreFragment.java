package com.biznessapps.fragments.single;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.MoreTabAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.model.AppSettings;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.TabButton;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedList;
import java.util.List;

public class MoreFragment extends CommonListFragment<TabButton> {
    private static List<TabButton> storedTabButtons;
    private AppSettings appSettings;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ CommonListEntity getWrappedItem(CommonListEntity x0, List x1) {
        return getWrappedItem((TabButton) x0, (List<TabButton>) x1);
    }

    public static void setTabButtons(List<TabButton> newList) {
        storedTabButtons = newList;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        super.preDataLoading(holdActivity);
        this.items = storedTabButtons;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.SETTINGS_URL_FORMAT, cacher().getAppCode());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        return this.appSettings != null;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.appSettings.hasMoreButtonNavigation()) {
            return this.appSettings.getMoreTabBg();
        }
        if (!StringUtils.isNotEmpty(this.appSettings.getMoreTabBg())) {
            return null;
        }
        String appId = AppCore.getInstance().getAppSettings().getAppId();
        if (AppCore.getInstance().isTablet()) {
            return String.format(ServerConstants.MORE_BG_FORMAT, appId, this.appSettings.getMoreTabTabletBg());
        }
        return String.format(ServerConstants.MORE_BG_FORMAT, appId, this.appSettings.getMoreTabBg());
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.appSettings = AppCore.getInstance().getAppSettings();
        return this.appSettings != null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.TabButton r0 = (com.biznessapps.model.TabButton) r0
            com.biznessapps.activities.CommonFragmentActivity r1 = r2.getHoldActivity()
            r2.startContactActivity(r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.single.MoreFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        if (this.items != null && !this.items.isEmpty()) {
            List<TabButton> itemsList = new LinkedList<>();
            if (this.items.size() == 1) {
                startContactActivity(holdActivity, (TabButton) this.items.get(0));
                holdActivity.finish();
                return;
            }
            for (TabButton item : this.items) {
                itemsList.add(getWrappedItem(item, itemsList));
            }
            MoreTabAdapter adapter = new MoreTabAdapter(holdActivity.getApplicationContext(), itemsList);
            this.listView.setDivider(new ColorDrawable(-3355444));
            this.listView.setDividerHeight(1);
            this.listView.setAdapter((ListAdapter) adapter);
            this.listView.setBackgroundColor(getUiSettings().getDefaultListBgColor());
            initListViewListener();
        }
    }

    private void startContactActivity(Activity holdActivity, TabButton item) {
        if (item != null) {
            Intent intent = new Intent(holdActivity.getApplicationContext(), SingleFragmentActivity.class);
            if (StringUtils.isNotEmpty(item.getTab().getUrl())) {
                intent.putExtra(AppConstants.URL, item.getTab().getUrl());
            }
            intent.putExtra(AppConstants.TAB_ID, item.getTab().getId());
            intent.putExtra(AppConstants.TAB_LABEL, item.getTab().getLabel());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, item.getTab().getTabId());
            intent.putExtra(AppConstants.ITEM_ID, item.getTab().getItemId());
            intent.putExtra(AppConstants.URL, item.getTab().getUrl());
            intent.putExtra(AppConstants.SECTION_ID, item.getTab().getSectionId());
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, item.getTab().getViewController());
            intent.putExtra(AppConstants.TAB, item.getTab());
            startActivity(intent);
        }
    }

    /* access modifiers changed from: protected */
    public TabButton getWrappedItem(TabButton item, List<TabButton> itemsList) {
        int bgColor;
        int textColor;
        if (getUiSettings().isHasColor()) {
            if (itemsList.size() % 2 == 0) {
                bgColor = getUiSettings().getOddRowColor();
                textColor = getUiSettings().getOddRowTextColor();
            } else {
                bgColor = getUiSettings().getEvenRowColor();
                textColor = getUiSettings().getEvenRowTextColor();
            }
            item.setItemColor(bgColor);
            item.setItemTextColor(textColor);
        }
        return item;
    }
}
