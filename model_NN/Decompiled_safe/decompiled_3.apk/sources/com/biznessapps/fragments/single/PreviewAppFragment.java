package com.biznessapps.fragments.single;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.MainController;
import com.biznessapps.layout.R;
import com.biznessapps.utils.ViewUtils;

public class PreviewAppFragment extends CommonFragment {
    private Button clearTextButton;
    private EditText demoCodeView;
    /* access modifiers changed from: private */
    public Button loadDemoButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.preview_changer_layout, (ViewGroup) null);
        initViews(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.demoCodeView = (EditText) root.findViewById(R.id.demo_code_edittext);
        this.loadDemoButton = (Button) root.findViewById(R.id.load_demo_button);
        this.clearTextButton = (Button) root.findViewById(R.id.reset_demo_button);
        this.loadDemoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PreviewAppFragment.this.loadDemo();
            }
        });
        this.clearTextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PreviewAppFragment.this.clearText();
            }
        });
        this.demoCodeView.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                PreviewAppFragment.this.loadDemoButton.performClick();
            }
        }));
    }

    /* access modifiers changed from: private */
    public void loadDemo() {
        String demoCode = this.demoCodeView.getText().toString();
        if (demoCode == null || demoCode.trim().length() == 0) {
            Toast.makeText(getApplicationContext(), R.string.load_demo_notification, 0).show();
            return;
        }
        Intent myIntent = new Intent().setClass(getApplicationContext(), MainController.class);
        myIntent.putExtra(AppConstants.APPCODE, demoCode);
        myIntent.putExtra(CachingConstants.OPEN_MESSAGE_TAB_PROPERTY, getIntent().getBooleanExtra(CachingConstants.OPEN_MESSAGE_TAB_PROPERTY, false));
        startActivity(myIntent);
        getActivity().finish();
    }

    /* access modifiers changed from: private */
    public void clearText() {
        this.demoCodeView.setText("");
    }
}
