package com.biznessapps.fragments.single;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.biznessapps.activities.GalleryActivity;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.GalleryData;
import com.biznessapps.model.flickr.Galleries;
import com.biznessapps.model.flickr.Gallery;
import com.biznessapps.model.flickr.GalleryAlbum;
import com.biznessapps.model.flickr.Photo;
import com.biznessapps.model.flickr.Photos;
import com.biznessapps.model.flickr.Photoset;
import com.biznessapps.model.flickr.User;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.PicasaUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends CommonFragment {
    private GalleryData galleryData;
    private boolean isFlickrUsed;
    private boolean isInstagramUsed;
    private boolean isPicasaUsed;
    private String tabId;

    public boolean isPicasaUsed() {
        return this.isPicasaUsed;
    }

    public void setPicasaUsed(boolean isPicasaUsed2) {
        this.isPicasaUsed = isPicasaUsed2;
    }

    public boolean isFlickrUsed() {
        return this.isFlickrUsed;
    }

    public void setFlickrUsed(boolean isFlickrUsed2) {
        this.isFlickrUsed = isFlickrUsed2;
    }

    public boolean isInstagramUsed() {
        return this.isInstagramUsed;
    }

    public void setInstagramUsed(boolean isInstagramUsed2) {
        this.isInstagramUsed = isInstagramUsed2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.gallery_layout, (ViewGroup) null);
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        ViewUtils.setGlobalBackgroundColor(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        String coreUrlFormat;
        if (this.isPicasaUsed) {
            coreUrlFormat = ServerConstants.PICASA_URL_FORMAT;
        } else if (this.isFlickrUsed) {
            coreUrlFormat = ServerConstants.FLICKR_FORMAT;
        } else if (this.isInstagramUsed) {
            coreUrlFormat = ServerConstants.INSTAGRAM_FORMAT;
        } else {
            coreUrlFormat = ServerConstants.GALLERY;
        }
        return String.format(coreUrlFormat, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        if (this.isPicasaUsed) {
            this.galleryData = JsonParserUtils.parsePicasaData(dataToParse);
            definePicasaAlbums(this.galleryData.getUserId());
            return cacher().saveData(CachingConstants.PICASA_DATA_PROPERTY + this.tabId, this.galleryData);
        } else if (this.isInstagramUsed) {
            this.galleryData = JsonParserUtils.parseInstagramData(dataToParse);
            addInstagramImages(this.galleryData);
            return cacher().saveData(CachingConstants.INSTAGRAM_DATA_PROPERTY + this.tabId, this.galleryData);
        } else if (this.isFlickrUsed) {
            this.galleryData = JsonParserUtils.parseFlickrData(dataToParse);
            String apiKey = this.galleryData.getApiKey();
            String userId = this.galleryData.getUserId();
            User u = findFlickrUser(apiKey, userId);
            if (u == null && (u = findFlickrUser((apiKey = ServerConstants.FLICKR_DEFAULT_API_KEY1), userId)) == null) {
                apiKey = ServerConstants.FLICKR_DEFAULT_API_KEY2;
                u = findFlickrUser(apiKey, userId);
            }
            if (u != null) {
                defineFlickrAlbums(this.galleryData.isDisplayPhotosets(), apiKey, u.getId());
            }
            return cacher().saveData(CachingConstants.FLICKR_DATA_PROPERTY + this.tabId, this.galleryData);
        } else {
            this.galleryData = JsonParserUtils.parseGalleryMetadata(dataToParse);
            return cacher().saveData(CachingConstants.GALLERY_DATA_PROPERTY + this.tabId, this.galleryData);
        }
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        if (this.galleryData == null) {
            Toast.makeText(getApplicationContext(), R.string.wait_till_images_loading, 1);
        } else if (this.isFlickrUsed || this.isPicasaUsed) {
            Intent intent = new Intent(holdActivity.getApplicationContext(), SingleFragmentActivity.class);
            String tabParam = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
            intent.putExtra(AppConstants.GALLERY_DATA_EXTRA, this.galleryData);
            intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabParam);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.GALLERY_LISTVIEW_FRAGMENT);
            startActivity(intent);
            holdActivity.finish();
        } else {
            Intent openGalleryIntent = new Intent(holdActivity.getApplicationContext(), GalleryActivity.class);
            openGalleryIntent.putExtra(AppConstants.GALLERY_DATA_EXTRA, this.galleryData);
            openGalleryIntent.putExtras(holdActivity.getIntent());
            startActivity(openGalleryIntent);
            holdActivity.finish();
        }
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        if (this.isPicasaUsed) {
            this.galleryData = (GalleryData) cacher().getData(CachingConstants.PICASA_DATA_PROPERTY + this.tabId);
        } else if (this.isFlickrUsed) {
            this.galleryData = (GalleryData) cacher().getData(CachingConstants.FLICKR_DATA_PROPERTY + this.tabId);
        } else if (this.isInstagramUsed) {
            this.galleryData = (GalleryData) cacher().getData(CachingConstants.INSTAGRAM_DATA_PROPERTY + this.tabId);
        } else {
            this.galleryData = (GalleryData) cacher().getData(CachingConstants.GALLERY_DATA_PROPERTY + this.tabId);
        }
        if (this.galleryData != null) {
            return true;
        }
        return false;
    }

    private void definePicasaAlbums(String userId) {
        List<String> urls = new ArrayList<>();
        List<GalleryAlbum> albums = PicasaUtils.parseGalleryList(PicasaUtils.getPicasaData(ServerConstants.PICASA_FEED_URL + userId));
        for (GalleryAlbum album : albums) {
            List<String> albumUrls = PicasaUtils.parsePhotosList(DataSource.getInstance().getData(String.format(ServerConstants.PICASA_ALBUM_FORMAT, userId, album.getId())));
            album.setUrls(albumUrls);
            urls.addAll(albumUrls);
        }
        this.galleryData.setAlbums(albums);
    }

    private void addInstagramImages(GalleryData galleryData2) {
        if (StringUtils.isNotEmpty(galleryData2.getApiKey()) && StringUtils.isNotEmpty(galleryData2.getUserId())) {
            JsonParserUtils.parseInstagramImages(galleryData2, DataSource.getInstance().getData(String.format(ServerConstants.INSTAGRAM_IMAGES_FORMAT, galleryData2.getUserId(), galleryData2.getApiKey())));
        }
    }

    private User findFlickrUser(String apiKey, String userId) {
        if (StringUtils.isEmpty(apiKey) || StringUtils.isEmpty(userId)) {
            return null;
        }
        return JsonParserUtils.parseFlickrUser(DataSource.getInstance().getData(String.format(ServerConstants.FLICKR_FIND_USER_FORMAT, apiKey, userId)));
    }

    private void defineFlickrAlbums(boolean usePhotosets, String apiKey, String userId) {
        List<GalleryAlbum> albums = new ArrayList<>();
        if (usePhotosets) {
            List<Photoset> photosetsList = JsonParserUtils.getPhotosets(DataSource.getInstance().getData(String.format(ServerConstants.FLICKR_GET_PHOTOSETS_FORMAT, apiKey, userId)));
            if (!photosetsList.isEmpty()) {
                for (Photoset item : photosetsList) {
                    GalleryAlbum album = new GalleryAlbum();
                    Photoset p = JsonParserUtils.getPhotosInPhotoset(DataSource.getInstance().getData(String.format(ServerConstants.FLICKR_GET_PHOTOS_FORMAT, apiKey, item.getId())));
                    if (!(p == null || p.getPhotos() == null)) {
                        ArrayList arrayList = new ArrayList();
                        for (Photo photo : p.getPhotos()) {
                            if (StringUtils.isNotEmpty(photo.getUrl())) {
                                arrayList.add(photo.getUrl());
                            } else {
                                arrayList.add(String.format(ServerConstants.FLICKR_PHOTO_URL_FORMAT, photo.getFarm(), photo.getServer(), photo.getId(), photo.getSecret()));
                            }
                        }
                        if (arrayList.isEmpty()) {
                            arrayList.add(String.format(ServerConstants.FLICKR_GALLERY_URL_FORMAT, item.getFarm(), item.getServer(), item.getPrimary(), item.getSecret()));
                        }
                        album.setUrls(arrayList);
                        album.setTitle(item.getTitle() != null ? item.getTitle() : "");
                        album.setDescription(item.getDescription() != null ? item.getDescription() : "");
                        albums.add(album);
                    }
                }
            }
        } else {
            Galleries gs = JsonParserUtils.getGalleries(DataSource.getInstance().getData(String.format(ServerConstants.FLICKR_GET_GALLERIES_FORMAT, apiKey, userId)));
            if (gs != null && gs.getGalleriesArray() != null && gs.getGalleriesArray().length > 0) {
                Gallery[] arr$ = gs.getGalleriesArray();
                int len$ = arr$.length;
                int i$ = 0;
                while (true) {
                    int i$2 = i$;
                    if (i$2 >= len$) {
                        break;
                    }
                    Gallery item2 = arr$[i$2];
                    GalleryAlbum album2 = new GalleryAlbum();
                    album2.setTitle(item2.getTitle().getTitle());
                    Photos g = JsonParserUtils.getPhotosInGallery(DataSource.getInstance().getData(String.format(ServerConstants.FLICKR_GET_GALLERY_PHOTOS_FORMAT, apiKey, item2.getId())));
                    if (!(g == null || g.getPhotosArray() == null)) {
                        ArrayList arrayList2 = new ArrayList();
                        for (Photo photo2 : g.getPhotosArray()) {
                            if (StringUtils.isNotEmpty(photo2.getUrl())) {
                                arrayList2.add(photo2.getUrl());
                            } else {
                                arrayList2.add(String.format(ServerConstants.FLICKR_PHOTO_URL_FORMAT, photo2.getFarm(), photo2.getServer(), photo2.getId(), photo2.getSecret()));
                            }
                        }
                        if (arrayList2.isEmpty()) {
                            arrayList2.add(String.format(ServerConstants.FLICKR_GALLERY_URL_FORMAT, item2.getFarm(), item2.getServer(), item2.getPrimary(), item2.getSecret()));
                        }
                        album2.setUrls(arrayList2);
                        albums.add(album2);
                    }
                    i$ = i$2 + 1;
                }
            }
        }
        this.galleryData.setAlbums(albums);
    }
}
