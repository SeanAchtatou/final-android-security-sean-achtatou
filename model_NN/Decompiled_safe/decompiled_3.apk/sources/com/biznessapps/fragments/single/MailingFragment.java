package com.biznessapps.fragments.single;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.api.AsyncCallback;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.MailingListEntity;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.biznessapps.widgets.RefreshableListView;
import java.util.List;

public class MailingFragment extends CommonFragment {
    private static final String OFF = "OFF";
    private static final String ON = "ON";
    private List<MailingListEntity.Category> categories;
    private RefreshableListView categoriesListView;
    /* access modifiers changed from: private */
    public Button joinButton;
    private MailingListEntity mailingListInfo;
    private String tabId;
    private TextView userDescription;
    private EditText userEmailText;
    private EditText userNameText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.mailing_list_layout, (ViewGroup) null);
        initViews();
        initListeners();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MAILING_LIST, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.mailingListInfo = JsonParserUtils.parseMailingList(dataToParse);
        return cacher().saveData(CachingConstants.MAILING_INFO_PROPERTY + this.tabId, this.mailingListInfo);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (this.mailingListInfo != null) {
            if (StringUtils.isNotEmpty(this.mailingListInfo.getDescription())) {
                this.userDescription.setText(this.mailingListInfo.getDescription());
                this.userDescription.setVisibility(0);
            }
            this.categories = this.mailingListInfo.getCategories();
            if (this.categories != null) {
                this.categoriesListView.setAdapter((ListAdapter) new CategoriesAdapter(holdActivity.getApplicationContext(), this.categories));
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.mailingListInfo = (MailingListEntity) cacher().getData(CachingConstants.MAILING_INFO_PROPERTY + this.tabId);
        return this.mailingListInfo != null;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.mailingListInfo != null) {
            return this.mailingListInfo.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    private void initViews() {
        this.userNameText = (EditText) this.root.findViewById(R.id.user_name_text);
        this.userEmailText = (EditText) this.root.findViewById(R.id.user_email_text);
        this.userDescription = (TextView) this.root.findViewById(R.id.mailing_list_description);
        this.joinButton = (Button) this.root.findViewById(R.id.join_button);
        this.categoriesListView = (RefreshableListView) this.root.findViewById(R.id.list_view);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.joinButton.setTextColor(settings.getButtonTextColor());
        ((TextView) this.root.findViewById(R.id.mailing_title_label)).setTextColor(settings.getFeatureTextColor());
        ((TextView) this.root.findViewById(R.id.mailing_list_description)).setTextColor(settings.getFeatureTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.joinButton.getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((ImageView) this.root.findViewById(R.id.mailing_sign_up_icon)).getBackground());
        ViewUtils.setGlobalBackgroundColor(this.root);
    }

    private void initListeners() {
        this.joinButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MailingFragment.this.calculate();
            }
        });
        this.userEmailText.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                MailingFragment.this.joinButton.performClick();
            }
        }));
    }

    /* access modifiers changed from: private */
    public void calculate() {
        String userName = this.userNameText.getText().toString();
        String userEmail = this.userEmailText.getText().toString();
        if (!StringUtils.isNotEmpty(userName) || !StringUtils.isNotEmpty(userEmail)) {
            Toast.makeText(getApplicationContext(), R.string.field_not_filled_message, 1).show();
            return;
        }
        AppHttpUtils.addToMailingList(new AsyncCallback<String>() {
            public void onResult(String result) {
                Activity activity = MailingFragment.this.getHoldActivity();
                if (activity != null) {
                    Toast.makeText(activity.getApplicationContext(), R.string.mailing_list_add_success, 1).show();
                    activity.finish();
                }
            }

            public void onError(String message, Throwable throwable) {
                super.onError(message, throwable);
                Activity activity = MailingFragment.this.getHoldActivity();
                if (activity != null) {
                    Toast.makeText(activity.getApplicationContext(), R.string.mailing_list_add_failure, 1).show();
                }
            }
        }, userName, userEmail, cacher().getAppCode(), getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID), getSelectedCategories(this.categories));
    }

    private String getSelectedCategories(List<MailingListEntity.Category> categories2) {
        String result = "";
        if (categories2 != null) {
            for (MailingListEntity.Category category : categories2) {
                if (category.isSelected()) {
                    result = result + category.getId() + ",";
                }
            }
        }
        return result;
    }

    public static class CategoriesAdapter extends AbstractAdapter<MailingListEntity.Category> {
        public CategoriesAdapter(Context context, List<MailingListEntity.Category> items) {
            super(context, items, R.layout.mailing_list_item_layout);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ListItemHolder.CommonItem holder;
            final MailingListEntity.Category item = (MailingListEntity.Category) this.items.get(position);
            if (convertView == null) {
                convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
                holder = new ListItemHolder.CommonItem();
                holder.setTextViewTitle((TextView) convertView.findViewById(R.id.simple_text_view));
                holder.setButton((Button) convertView.findViewById(R.id.turn_function_button));
                CommonUtils.overrideMediumButtonColor(this.settings.getButtonBgColor(), holder.getButton().getBackground());
                holder.getButton().setTextColor(this.settings.getButtonTextColor());
                final Button turnButton = holder.getButton();
                turnButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        item.setSelected(!item.isSelected());
                        turnButton.setText(item.isSelected() ? MailingFragment.ON : MailingFragment.OFF);
                    }
                });
                holder.getButton().setText(item.isSelected() ? MailingFragment.ON : MailingFragment.OFF);
                convertView.setTag(holder);
            } else {
                holder = (ListItemHolder.CommonItem) convertView.getTag();
            }
            if (item != null) {
                holder.getTextViewTitle().setText(Html.fromHtml(item.getName()));
            }
            return convertView;
        }
    }
}
