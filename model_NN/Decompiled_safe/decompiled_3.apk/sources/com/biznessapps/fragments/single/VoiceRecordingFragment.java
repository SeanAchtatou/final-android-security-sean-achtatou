package com.biznessapps.fragments.single;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.EmailPhotoItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.HardwareUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class VoiceRecordingFragment extends CommonFragment {
    private static final String LOG_TAG = "AudioRecordTest";
    private static final String RECORDED_AUDIO_NAME = "recorded_audio.aac";
    private TextView descriptionTextView;
    private File fileDir;
    private String fileName;
    private EmailPhotoItem info;
    private ImageButton playButton;
    /* access modifiers changed from: private */
    public MediaPlayer player = null;
    private ImageButton recordButton;
    private MediaRecorder recorder = null;
    private ViewGroup rootView;
    private ImageButton sendEmailButton;
    /* access modifiers changed from: private */
    public boolean startPlaying = false;
    /* access modifiers changed from: private */
    public boolean startRecording = false;
    private String tabId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.voice_recording_layout, (ViewGroup) null);
        initViews(this.root);
        initListeners();
        loadData();
        initFileDir();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onPause() {
        super.onPause();
        if (this.recorder != null) {
            this.recorder.release();
            this.recorder = null;
        }
        if (this.player != null) {
            this.player.release();
            this.player = null;
        }
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.playButton = (ImageButton) root.findViewById(R.id.play_button);
        this.recordButton = (ImageButton) root.findViewById(R.id.record_button);
        this.sendEmailButton = (ImageButton) root.findViewById(R.id.send_email_button);
        this.descriptionTextView = (TextView) root.findViewById(R.id.voice_recording_description);
        this.rootView = (ViewGroup) root.findViewById(R.id.voice_recording_root);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        CommonUtils.customizeFooterNavigationBar(root.findViewById(R.id.voice_recording_buttons_container));
        this.descriptionTextView.setTextColor(settings.getFeatureTextColor());
        ViewUtils.updateMusicBarBottomMargin(getHoldActivity().getMusicDelegate(), (int) getResources().getDimension(R.dimen.footer_bar_height));
        ViewUtils.setGlobalBackgroundColor(this.rootView);
    }

    private void initListeners() {
        this.playButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VoiceRecordingFragment.this.onPlay(VoiceRecordingFragment.this.startPlaying);
                VoiceRecordingFragment.this.refreshButtons();
            }
        });
        this.recordButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VoiceRecordingFragment.this.onRecord(VoiceRecordingFragment.this.startRecording);
                VoiceRecordingFragment.this.refreshButtons();
            }
        });
        this.sendEmailButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                VoiceRecordingFragment.this.sendEmail();
            }
        });
    }

    private void initFileDir() {
        if (HardwareUtils.getExternalPath() != null) {
            try {
                this.fileDir = new File(HardwareUtils.getExternalPath() + "/" + AppConstants.STORED_FILES);
                if (this.fileDir != null) {
                    if (!this.fileDir.exists()) {
                        this.fileDir.mkdir();
                    }
                    this.fileName = this.fileDir.getAbsolutePath() + "/" + RECORDED_AUDIO_NAME;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.sdcard_missed, 1).show();
        }
    }

    /* access modifiers changed from: private */
    public void refreshButtons() {
        if (this.startPlaying) {
            this.playButton.setImageResource(R.drawable.voice_tab_pause);
        } else {
            this.playButton.setImageResource(R.drawable.voice_tab_play);
        }
        if (this.startRecording) {
            this.recordButton.setImageResource(R.drawable.voice_tab_pause);
        } else {
            this.recordButton.setImageResource(R.drawable.voice_tab_record);
        }
    }

    /* access modifiers changed from: private */
    public void sendEmail() {
        if (this.fileDir != null) {
            File record = new File(this.fileDir, RECORDED_AUDIO_NAME);
            if (this.info != null && record != null) {
                email(getApplicationContext(), this.info.getEmail(), this.info.getSubject(), this.info.getDescription(), record);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.VOICE_RECORDING_URL_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.info = JsonParserUtils.parseEmailPhoto(dataToParse);
        return cacher().saveData(CachingConstants.VOICE_RECORDING_INFO_PROPERTY + this.tabId, this.info);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (StringUtils.isNotEmpty(this.info.getDescription())) {
            this.descriptionTextView.setText(this.info.getDescription());
        }
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.info != null) {
            return this.info.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.rootView;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.info = (EmailPhotoItem) cacher().getData(CachingConstants.VOICE_RECORDING_INFO_PROPERTY + this.tabId);
        return this.info != null;
    }

    /* access modifiers changed from: private */
    public void onRecord(boolean isRecorded) {
        if (this.fileName == null) {
            Toast.makeText(getApplicationContext(), R.string.sdcard_missed, 1).show();
        } else if (!isRecorded) {
            startRecording();
            this.startRecording = true;
        } else {
            stopRecording();
            this.startRecording = false;
        }
    }

    /* access modifiers changed from: private */
    public void onPlay(boolean isPlayed) {
        if (this.fileName == null) {
            Toast.makeText(getApplicationContext(), R.string.sdcard_missed, 1).show();
        } else if (!isPlayed) {
            startPlaying();
            this.startPlaying = true;
        } else {
            stopPlaying();
            this.startPlaying = false;
        }
    }

    private void startPlaying() {
        this.player = new MediaPlayer();
        try {
            this.player.setDataSource(this.fileName);
            this.player.prepare();
            this.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    VoiceRecordingFragment.this.player.stop();
                    boolean unused = VoiceRecordingFragment.this.startPlaying = false;
                    VoiceRecordingFragment.this.refreshButtons();
                }
            });
            this.player.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    private void stopPlaying() {
        if (this.player != null) {
            this.player.release();
            this.player = null;
        }
    }

    private void startRecording() {
        this.recorder = new MediaRecorder();
        this.recorder.setAudioSource(1);
        this.recorder.setOutputFormat(2);
        this.recorder.setOutputFile(this.fileName);
        this.recorder.setAudioEncoder(3);
        try {
            this.recorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
        this.recorder.start();
    }

    private void stopRecording() {
        if (this.recorder != null) {
            this.recorder.stop();
            this.recorder.release();
            this.recorder = null;
        }
    }

    private void email(Context context, String emailTo, String subject, String emailText, File fileToSend) {
        Intent intent = new Intent("android.intent.action.SEND_MULTIPLE");
        intent.setFlags(268435456);
        intent.setType(AppConstants.PLAIN_TEXT);
        intent.putExtra("android.intent.extra.EMAIL", new String[]{emailTo});
        intent.putExtra("android.intent.extra.SUBJECT", subject);
        intent.putExtra("android.intent.extra.TEXT", emailText);
        ArrayList<Uri> uris = new ArrayList<>();
        uris.add(Uri.fromFile(fileToSend));
        intent.putParcelableArrayListExtra("android.intent.extra.STREAM", uris);
        startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_email)));
    }
}
