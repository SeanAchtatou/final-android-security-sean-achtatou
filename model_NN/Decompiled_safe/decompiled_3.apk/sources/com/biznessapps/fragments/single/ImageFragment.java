package com.biznessapps.fragments.single;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.images.BitmapWrapper;
import com.biznessapps.layout.R;
import com.biznessapps.model.Tab;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.facebook.AppEventsConstants;
import java.util.Iterator;
import java.util.List;

public class ImageFragment extends CommonFragment {
    private static final int FADE_IN_DELAY = 5000;
    private static final int LOAD_NEW_IMAGE_EVENT = 2;
    /* access modifiers changed from: private */
    public int fadeImageIndex = 0;
    private Handler fadeImagesHandler;
    protected Animation fadeInAnimation;
    protected Animation fadeOutAnimation;
    /* access modifiers changed from: private */
    public int imageOrderIndex;
    /* access modifiers changed from: private */
    public View imageView;
    private View imageViewNext;
    /* access modifiers changed from: private */
    public List<String> imagesUrls;
    private ViewGroup progressContainer;

    static /* synthetic */ int access$008(ImageFragment x0) {
        int i = x0.fadeImageIndex;
        x0.fadeImageIndex = i + 1;
        return i;
    }

    public void setUrl(String url) {
        this.bgUrl = url;
    }

    public void setImageOrderIndex(int imageOrderIndex2) {
        this.imageOrderIndex = imageOrderIndex2;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.image_fragment_layout, (ViewGroup) null);
        this.imageView = this.root.findViewById(R.id.home_image_view1);
        this.imageViewNext = this.root.findViewById(R.id.home_image_view2);
        this.progressContainer = (ViewGroup) this.root.findViewById(R.id.progress_bar_container);
        initFadeAnimation();
        this.imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if (AppCore.getInstance().getAppSettings().getAnimationMode() == 2) {
                    ImageFragment.this.openLinkedTab(ImageFragment.this.fadeImageIndex - 1);
                } else {
                    ImageFragment.this.openLinkedTab(ImageFragment.this.imageOrderIndex);
                }
            }
        });
        return this.root;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.imageView;
    }

    /* access modifiers changed from: protected */
    public void onPreBgLoading() {
        Activity activity = getHoldActivity();
        if (activity != null) {
            this.progressContainer.addView(ViewUtils.getImageProgressBar(activity.getApplicationContext()));
        }
    }

    /* access modifiers changed from: protected */
    public void onPostBgLoading() {
        this.progressContainer.removeAllViews();
    }

    private void initFadeAnimation() {
        if (AppCore.getInstance().getAppSettings().getAnimationMode() == 2) {
            this.imagesUrls = cacher().getAppInfo().getImagesInOrder();
            if (this.imagesUrls != null && this.imagesUrls.size() > 1) {
                this.bgUrl = null;
                initFadeHandler();
                this.fadeImageIndex = this.imageOrderIndex;
                this.fadeOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out);
                this.fadeInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
            }
        }
    }

    private void initFadeHandler() {
        this.fadeImagesHandler = new Handler(getApplicationContext().getMainLooper()) {
            public void handleMessage(Message newMessage) {
                switch (newMessage.what) {
                    case 2:
                        if (ImageFragment.this.fadeImageIndex == ImageFragment.this.imagesUrls.size()) {
                            int unused = ImageFragment.this.fadeImageIndex = 0;
                        }
                        ImageFragment.this.loadFadeImages(ImageFragment.this.fadeImageIndex, ImageFragment.this.imageView);
                        ImageFragment.access$008(ImageFragment.this);
                        ImageFragment.this.sendMessageToHandler(2, 5000);
                        return;
                    default:
                        return;
                }
            }
        };
        sendMessageToHandler(2, 0);
    }

    /* access modifiers changed from: private */
    public void sendMessageToHandler(int messageId, int delayTime) {
        Message message = this.fadeImagesHandler.obtainMessage(messageId);
        this.fadeImagesHandler.removeMessages(messageId);
        this.fadeImagesHandler.sendMessageDelayed(message, (long) delayTime);
    }

    /* access modifiers changed from: private */
    public void loadFadeImages(int fadeImageIndex2, View viewToLoad) {
        BitmapDownloader.BitmapLoadCallback callback = new BitmapDownloader.BitmapLoadCallback() {
            public void onPostImageLoading(BitmapWrapper bitmapWrapper, View view) {
                ImageFragment.this.setImageWithAnimation(bitmapWrapper.getBitmap());
            }
        };
        int deviceWidth = (int) (((float) AppCore.getInstance().getDeviceWidth()) * 0.85f);
        int deviceHeight = (int) (((float) AppCore.getInstance().getDeviceHeight()) * 0.85f);
        BitmapDownloader.UsingParams params = new BitmapDownloader.UsingParams(viewToLoad, this.imagesUrls.get(fadeImageIndex2) + AppConstants.WIDTH_URL_PARAM + deviceWidth, callback);
        params.setReqHeight(deviceHeight);
        params.setReqWidth(deviceWidth);
        getBitmapDownloader().download(params);
        int nextFadeIndex = fadeImageIndex2 + 1;
        if (nextFadeIndex == this.imagesUrls.size()) {
            nextFadeIndex = 0;
        }
        BitmapDownloader.UsingParams nextUrlparams = new BitmapDownloader.UsingParams(null, this.imagesUrls.get(nextFadeIndex) + AppConstants.WIDTH_URL_PARAM + deviceWidth, null);
        nextUrlparams.setReqHeight(deviceHeight);
        nextUrlparams.setReqWidth(deviceWidth);
        getBitmapDownloader().download(nextUrlparams);
    }

    /* access modifiers changed from: private */
    public void setImageWithAnimation(final Bitmap b) {
        this.fadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ImageFragment.this.imageView.setBackgroundDrawable(new BitmapDrawable(b));
                ImageFragment.this.imageView.startAnimation(ImageFragment.this.fadeInAnimation);
            }
        });
        this.imageViewNext.setBackgroundDrawable(new BitmapDrawable(b));
        this.imageView.startAnimation(this.fadeOutAnimation);
    }

    /* access modifiers changed from: private */
    public void openLinkedTab(int tabIndex) {
        Tab linkedTab;
        List<Tab> linkedTabs = cacher().getAppInfo().getImagesLinkedTabs();
        if (linkedTabs != null && tabIndex >= 0 && tabIndex < linkedTabs.size() && (linkedTab = linkedTabs.get(tabIndex)) != null) {
            Tab tabToUse = null;
            Iterator i$ = cacher().getTabList().iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Tab item = i$.next();
                if (item.getTabId().equalsIgnoreCase(linkedTab.getTabId())) {
                    tabToUse = item;
                    break;
                }
            }
            if (tabToUse != null) {
                Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
                if (StringUtils.isNotEmpty(tabToUse.getUrl())) {
                    intent.putExtra(AppConstants.URL, tabToUse.getUrl());
                }
                intent.putExtra(AppConstants.TAB_ID, tabToUse.getId());
                intent.putExtra(AppConstants.TAB_LABEL, tabToUse.getLabel());
                intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabToUse.getTabId());
                intent.putExtra(AppConstants.ITEM_ID, linkedTab.getItemId());
                if (!StringUtils.isNotEmpty(linkedTab.getSectionId()) || linkedTab.getSectionId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    intent.putExtra(AppConstants.SECTION_ID, tabToUse.getSectionId());
                } else {
                    intent.putExtra(AppConstants.SECTION_ID, linkedTab.getSectionId());
                }
                intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, linkedTab.getViewController());
                intent.putExtra(AppConstants.TAB, tabToUse);
                startActivity(intent);
            }
        }
    }
}
