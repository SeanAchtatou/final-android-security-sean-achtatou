package com.biznessapps.fragments.single;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.MainController;
import com.biznessapps.layout.R;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.Map;

public class ProtectedAppFragment extends CommonFragment {
    /* access modifiers changed from: private */
    public Button confirmButton;
    private boolean isCorrectData;
    private EditText passwordView;
    private EditText usernameView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.protected_layout, (ViewGroup) null);
        initViews(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public boolean hasTitleBar() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.usernameView = (EditText) root.findViewById(R.id.username_edittext);
        this.passwordView = (EditText) root.findViewById(R.id.password_edittext);
        this.confirmButton = (Button) root.findViewById(R.id.confirm_button);
        this.confirmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ProtectedAppFragment.this.confirm();
            }
        });
        this.usernameView.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                ProtectedAppFragment.this.confirmButton.performClick();
            }
        }));
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.PROTECT_URL, cacher().getAppCode());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.isCorrectData = !JsonParserUtils.hasDataError(dataToParse);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (this.isCorrectData) {
            Intent myIntent = new Intent().setClass(getApplicationContext(), MainController.class);
            myIntent.putExtra(AppConstants.IS_LOGGED_WITH_PROTECTION, true);
            myIntent.putExtra(AppConstants.APPCODE, AppCore.getInstance().getCachingManager().getAppCode());
            startActivity(myIntent);
            getActivity().finish();
            return;
        }
        Toast.makeText(getApplicationContext(), R.string.wrong_credentials, 0).show();
    }

    /* access modifiers changed from: private */
    public void confirm() {
        String username = this.usernameView.getText().toString();
        String password = this.passwordView.getText().toString();
        if (!StringUtils.isNotEmpty(username) || !StringUtils.isNotEmpty(password)) {
            Toast.makeText(getApplicationContext(), R.string.field_not_filled_message, 0).show();
            return;
        }
        Map<String, String> params = AppHttpUtils.getEmptyParams();
        params.put("user", username);
        params.put(ServerConstants.POST_PASS_PARAM, password);
        loadPostData(params);
    }
}
