package com.biznessapps.fragments.single;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.Mortgage;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.text.DecimalFormat;

public class MortgageCalculatorFragment extends CommonFragment {
    private EditText amountEditText;
    /* access modifiers changed from: private */
    public Button calculateButton;
    private ViewGroup layout;
    private TextView monthRepaymentTextView;
    private Mortgage mortgageInfo;
    private EditText rateEditText;
    private String tabId;
    private EditText termEditText;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.mortgage_calculator_layout, (ViewGroup) null);
        this.layout = (ViewGroup) this.root.findViewById(R.id.mortgage_layout);
        initViews();
        initListeners();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.LOAN_URL_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.mortgageInfo = JsonParserUtils.parseMortgage(dataToParse);
        return cacher().saveData(CachingConstants.MORTGAGE_INFO_PROPERTY + this.tabId, this.mortgageInfo);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.mortgageInfo != null) {
            return this.mortgageInfo.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.layout;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.mortgageInfo = (Mortgage) cacher().getData(CachingConstants.MORTGAGE_INFO_PROPERTY + this.tabId);
        return this.mortgageInfo != null;
    }

    private void initViews() {
        this.amountEditText = (EditText) this.root.findViewById(R.id.amount_text);
        this.termEditText = (EditText) this.root.findViewById(R.id.term_text);
        this.rateEditText = (EditText) this.root.findViewById(R.id.rate_text);
        this.monthRepaymentTextView = (TextView) this.root.findViewById(R.id.each_month_text);
        this.calculateButton = (Button) this.root.findViewById(R.id.calculate_button);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.calculateButton.getBackground());
        this.calculateButton.setTextColor(settings.getButtonTextColor());
        ((TextView) this.root.findViewById(R.id.amount_label)).setTextColor(settings.getFeatureTextColor());
        ((TextView) this.root.findViewById(R.id.term_label)).setTextColor(settings.getFeatureTextColor());
        ((TextView) this.root.findViewById(R.id.rate_label)).setTextColor(settings.getFeatureTextColor());
        ((TextView) this.root.findViewById(R.id.each_month_label)).setTextColor(settings.getFeatureTextColor());
        ((TextView) this.root.findViewById(R.id.each_month_text)).setTextColor(settings.getFeatureTextColor());
        ViewUtils.setGlobalBackgroundColor(this.layout);
    }

    private void initListeners() {
        this.calculateButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MortgageCalculatorFragment.this.calculate();
            }
        });
        this.rateEditText.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                MortgageCalculatorFragment.this.calculateButton.performClick();
            }
        }));
    }

    /* access modifiers changed from: private */
    public void calculate() {
        String priceAmount = this.amountEditText.getText().toString();
        String term = this.termEditText.getText().toString();
        String rate = this.rateEditText.getText().toString();
        if (StringUtils.checkTextFieldsOnEmpty(priceAmount, term, rate)) {
            Toast.makeText(getApplicationContext(), R.string.field_not_filled_message, 1).show();
            return;
        }
        try {
            double amountValue = Double.parseDouble(priceAmount);
            double termValue = Double.parseDouble(term);
            double rateValue = Double.parseDouble(rate);
            if (!checkOnCorrectness(amountValue, termValue, rateValue)) {
                Toast.makeText(getApplicationContext(), R.string.number_incorrect_format_message, 1).show();
                return;
            }
            double rateValue2 = rateValue / 1200.0d;
            this.monthRepaymentTextView.setText(new DecimalFormat("##.00").format(((rateValue2 / (Math.pow(1.0d + rateValue2, 12.0d * termValue) - 1.0d)) + rateValue2) * amountValue));
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), R.string.number_incorrect_format_message, 1).show();
        }
    }

    private boolean checkOnCorrectness(double amountValue, double termValue, double rateValue) {
        if (amountValue == 0.0d || rateValue == 0.0d || termValue == 0.0d || amountValue < 1.0d || amountValue > 9.9999999E7d || rateValue < 0.001d || rateValue > 1000.0d || termValue < 1.0d || termValue > 50.0d) {
            return false;
        }
        return true;
    }
}
