package com.biznessapps.fragments.twitter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.ViewUtils;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

public class TweetFragment extends CommonFragment {
    private static final String COUNTER_TEXT = "Counter: ";
    private static final String LUCE_LADIES = "@LuceLadies";
    private static final String hashTags = "hashTags";
    private static final String message = "Clare Boothe Luce Policy Institute (CBLPI) is exempt from federal income taxation under IRCsection 501(c)(3) and is not permitted to, and does not, engage in supporting or opposingcandidates for public office. Please help us by not using our Twitter communications for suchmatters.";
    private static final String recipients = "recipients";
    private static final String title = "disclosure";
    private static final String tweeting = "Tweeting...";
    private TextView counterTextView;
    private ImageButton hashtagsButton;
    private ImageButton infoButton;
    /* access modifiers changed from: private */
    public EditText messageTextView;
    private ImageButton recipientButton;
    private ImageButton tweetButton;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.twitter_layout, (ViewGroup) null);
        initViews(this.root);
        initListeners();
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (intent != null) {
            if (requestCode == 1) {
                String currentText = this.messageTextView.getText().toString() + intent.getStringExtra(AppConstants.SELECTED_RESULT);
                setTweetCounddown(currentText);
                this.messageTextView.setText(currentText);
            } else if (requestCode == 2) {
                String contents = intent.getStringExtra(AppConstants.SELECTED_RESULT) + this.messageTextView.getText().toString();
                setTweetCounddown(contents);
                this.messageTextView.setText(contents);
            }
        } else if (3 == resultCode) {
            ViewUtils.showShortToast(getApplicationContext(), R.string.twitting_failure);
        } else if (2 == requestCode) {
            tweet(this.messageTextView.getText().toString());
        }
    }

    /* access modifiers changed from: private */
    public void openTwitterLoginView() {
        if (cacher().getTwitterOauthSecret() == null || cacher().getTwitterOauthToken() == null) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.TWITTER_LOGIN_FRAGMENT);
            startActivityForResult(intent, 2);
            return;
        }
        tweet(this.messageTextView.getText().toString());
    }

    private void tweet(final String dataToTweet) {
        new AsyncTask<Void, Void, Exception>() {
            /* access modifiers changed from: protected */
            public void onPreExecute() {
                super.onPreExecute();
                Toast.makeText(TweetFragment.this.getApplicationContext(), TweetFragment.tweeting, 0).show();
            }

            /* access modifiers changed from: protected */
            public Exception doInBackground(Void... params) {
                try {
                    AccessToken accessToken = new AccessToken(TweetFragment.this.cacher().getTwitterOauthToken(), TweetFragment.this.cacher().getTwitterOauthSecret());
                    Twitter twitter = new TwitterFactory().getInstance();
                    twitter.setOAuthConsumer("ibeMh2JAmmQw09B1nfap5Q", "dkomjgXm50XtNmWDn0FhJJpswGvdfIPqfYwfxqMar38");
                    twitter.setOAuthAccessToken(accessToken);
                    twitter.updateStatus(dataToTweet);
                    return null;
                } catch (Exception e) {
                    return e;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Exception result) {
                if (result != null) {
                    ViewUtils.showShortToast(TweetFragment.this.getApplicationContext(), R.string.twitting_failure);
                } else {
                    ViewUtils.showShortToast(TweetFragment.this.getApplicationContext(), R.string.twitting_successful);
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.recipientButton = (ImageButton) root.findViewById(R.id.twitter_recipients_button);
        this.tweetButton = (ImageButton) root.findViewById(R.id.twitter_tweet_button);
        this.hashtagsButton = (ImageButton) root.findViewById(R.id.twitter_hashtags_button);
        this.infoButton = (ImageButton) root.findViewById(R.id.twitter_info_button);
        this.counterTextView = (TextView) root.findViewById(R.id.twitter_text_counter);
        this.counterTextView.setText(COUNTER_TEXT);
        this.messageTextView = (EditText) root.findViewById(R.id.twitter_text_edittext);
        this.messageTextView.setText(LUCE_LADIES);
        setTweetCounddown(this.messageTextView.getText().toString());
    }

    private void initListeners() {
        this.recipientButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent recipientsIntent = new Intent(TweetFragment.this.getApplicationContext(), SingleFragmentActivity.class);
                recipientsIntent.putExtra(AppConstants.TAB_LABEL, TweetFragment.this.getIntent().getStringExtra(AppConstants.TAB_LABEL));
                recipientsIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.RECIPIENTS_LIST_FRAGMENT);
                recipientsIntent.putExtra(AppConstants.CHILDREN_TAB_LABEL, TweetFragment.recipients);
                TweetFragment.this.startActivityForResult(recipientsIntent, 1);
            }
        });
        this.hashtagsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent hashtagsIntent = new Intent(TweetFragment.this.getApplicationContext(), SingleFragmentActivity.class);
                hashtagsIntent.putExtra(AppConstants.TAB_LABEL, TweetFragment.this.getIntent().getStringExtra(AppConstants.TAB_LABEL));
                hashtagsIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.HASH_TAGS_LIST_FRAGMENT);
                hashtagsIntent.putExtra(AppConstants.CHILDREN_TAB_LABEL, TweetFragment.hashTags);
                TweetFragment.this.startActivityForResult(hashtagsIntent, 2);
            }
        });
        this.tweetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TweetFragment.this.openTwitterLoginView();
            }
        });
        this.infoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TweetFragment.this.showInfoDialog();
            }
        });
        this.messageTextView.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                TweetFragment.this.setTweetCounddown(TweetFragment.this.messageTextView.getText().toString());
            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            public void afterTextChanged(Editable arg0) {
            }
        });
        this.messageTextView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0 || keyCode == 66) {
                }
                return false;
            }
        });
    }

    /* access modifiers changed from: private */
    public void showInfoDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getHoldActivity());
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void setTweetCounddown(String text) {
        this.counterTextView.setText(COUNTER_TEXT + text.length());
        if (text.length() > 160) {
            this.tweetButton.setEnabled(false);
            this.counterTextView.setTextColor(-65536);
            return;
        }
        this.tweetButton.setEnabled(true);
        this.counterTextView.setTextColor(-16777216);
    }
}
