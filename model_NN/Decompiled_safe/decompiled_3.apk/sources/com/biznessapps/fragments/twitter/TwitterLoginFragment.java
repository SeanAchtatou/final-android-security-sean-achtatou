package com.biznessapps.fragments.twitter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.biznessapps.activities.CommonTabFragmentActivity;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.ViewUtils;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

public class TwitterLoginFragment extends CommonFragment {
    /* access modifiers changed from: private */
    public static String CALLBACK_URL = "http://www.google.com/callback";
    public static final String DATA_TO_TWEET = "data_to_tweet";
    /* access modifiers changed from: private */
    public static String OAUTH_VERIFIER = "oauth_verifier";
    public static final String TWITTER_CONSUMER_KEY = "ibeMh2JAmmQw09B1nfap5Q";
    public static final String TWITTER_CONSUMER_SECRET = "dkomjgXm50XtNmWDn0FhJJpswGvdfIPqfYwfxqMar38";
    /* access modifiers changed from: private */
    public static RequestToken requestToken;
    /* access modifiers changed from: private */
    public static Twitter twitter;
    /* access modifiers changed from: private */
    public ProgressDialog progress;
    /* access modifiers changed from: private */
    public WebView twitterOauthWebview;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.twitter_login_view, (ViewGroup) null);
        this.twitterOauthWebview = (WebView) this.root.findViewById(R.id.twitter_oauth_internal_webview);
        this.twitterOauthWebview.setWebViewClient(getWebViewClient());
        final Activity activity = getHoldActivity();
        if (activity != null) {
            this.progress = ViewUtils.getProgressDialog(activity);
            this.progress.show();
            new Thread(new Runnable() {
                public void run() {
                    try {
                        Twitter unused = TwitterLoginFragment.twitter = new TwitterFactory().getInstance();
                        TwitterLoginFragment.twitter.setOAuthConsumer("ibeMh2JAmmQw09B1nfap5Q", "dkomjgXm50XtNmWDn0FhJJpswGvdfIPqfYwfxqMar38");
                        RequestToken unused2 = TwitterLoginFragment.requestToken = TwitterLoginFragment.twitter.getOAuthRequestToken(TwitterLoginFragment.CALLBACK_URL);
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                ViewUtils.plubWebView(TwitterLoginFragment.this.twitterOauthWebview);
                                TwitterLoginFragment.this.twitterOauthWebview.loadUrl(TwitterLoginFragment.requestToken.getAuthorizationURL());
                            }
                        });
                    } catch (Exception e) {
                        activity.runOnUiThread(new Runnable() {
                            public void run() {
                                if (TwitterLoginFragment.this.progress != null) {
                                    TwitterLoginFragment.this.progress.dismiss();
                                }
                                ViewUtils.showShortToast(activity, R.string.twitting_failure);
                                activity.finish();
                            }
                        });
                    }
                }
            }).start();
        }
        return this.root;
    }

    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if (activity != null && (activity instanceof CommonTabFragmentActivity)) {
            ((CommonTabFragmentActivity) activity).getViewPager().enableScrolling(false);
        }
    }

    private WebViewClient getWebViewClient() {
        return new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                if (url.startsWith(TwitterLoginFragment.CALLBACK_URL)) {
                    TwitterLoginFragment.this.handleAuthResponse(url);
                } else {
                    super.onPageStarted(view, url, favicon);
                }
            }

            public void onPageFinished(WebView view, String url) {
                try {
                    if (TwitterLoginFragment.this.progress != null && TwitterLoginFragment.this.progress.isShowing()) {
                        TwitterLoginFragment.this.progress.dismiss();
                    }
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void handleAuthResponse(final String url) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    AccessToken accessToken = TwitterLoginFragment.twitter.getOAuthAccessToken(TwitterLoginFragment.requestToken, Uri.parse(url).getQueryParameter(TwitterLoginFragment.OAUTH_VERIFIER));
                    TwitterLoginFragment.this.cacher().setTwitterOauthSecret(accessToken.getTokenSecret());
                    TwitterLoginFragment.this.cacher().setTwitterOauthToken(accessToken.getToken());
                    TwitterLoginFragment.this.cacher().setTwitterUid("" + accessToken.getUserId());
                    TwitterLoginFragment.this.cacher().setTwitterUserName(accessToken.getScreenName());
                    TwitterLoginFragment.this.getHoldActivity().setResult(2);
                    TwitterLoginFragment.this.getHoldActivity().finish();
                } catch (Exception e) {
                    TwitterLoginFragment.this.getHoldActivity().setResult(3);
                    TwitterLoginFragment.this.getHoldActivity().finish();
                }
            }
        }).start();
    }
}
