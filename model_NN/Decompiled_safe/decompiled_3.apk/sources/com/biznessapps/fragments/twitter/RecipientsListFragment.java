package com.biznessapps.fragments.twitter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListAdapter;
import com.biznessapps.adapters.TwitterTagsAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import java.util.ArrayList;

public class RecipientsListFragment extends CommonListFragment<CommonListEntity> {
    private Button backButton;

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.common_list_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.backButton = (Button) root.findViewById(R.id.common_back_button);
        this.backButton.setText(getIntent().getStringExtra(AppConstants.TAB_LABEL));
        this.titleTextView.setText(getIntent().getStringExtra(AppConstants.CHILDREN_TAB_LABEL));
        this.backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                String result = "";
                for (int i = 0; i < RecipientsListFragment.this.listView.getCount(); i++) {
                    CommonListEntity item = (CommonListEntity) RecipientsListFragment.this.listView.getItemAtPosition(i);
                    if (item.isSelected()) {
                        result = result + item.getTitle() + " ";
                    }
                }
                Intent data = new Intent();
                data.putExtra(AppConstants.SELECTED_RESULT, result);
                RecipientsListFragment.this.getHoldActivity().setResult(-1, data);
                RecipientsListFragment.this.getHoldActivity().finish();
            }
        });
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        int oddRowColor = getUiSettings().getOddRowColor();
        int oddRowTextColor = getUiSettings().getOddRowTextColor();
        int evenRowColor = getUiSettings().getEvenRowColor();
        int evenRowTextColor = getUiSettings().getEvenRowTextColor();
        this.items = new ArrayList();
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, "@secupp"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(evenRowColor, evenRowTextColor, "@andreatantaros"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, "@annmcelhinney"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(evenRowColor, evenRowTextColor, "@anncoulter"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, "@michellemalkin"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(evenRowColor, evenRowTextColor, "@kateobenshain"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, "@monicacrowley"), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(evenRowColor, evenRowTextColor, "@kathrynlopez"), this.items));
        plugListView(holdActivity);
    }

    private void plugListView(Activity holdActivity) {
        if (!this.items.isEmpty()) {
            this.listView.setAdapter((ListAdapter) new TwitterTagsAdapter(holdActivity.getApplicationContext(), this.items));
        }
    }
}
