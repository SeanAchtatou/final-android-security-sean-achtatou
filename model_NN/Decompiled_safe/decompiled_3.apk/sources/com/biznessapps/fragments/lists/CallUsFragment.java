package com.biznessapps.fragments.lists;

import android.app.Activity;
import android.widget.ListAdapter;
import com.biznessapps.adapters.CommonAdapter;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.CallUsItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedList;
import java.util.List;

public class CallUsFragment extends CommonListFragment<CallUsItem> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.CALL_US_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        boolean z;
        this.offsetItems = JsonParserUtils.parseCallUsItems(dataToParse);
        if (this.offsetItems != null) {
            if (this.items == null) {
                this.items = this.offsetItems;
            } else {
                if (this.offsetItems.size() != 1 || !StringUtils.isEmpty(((CallUsItem) this.offsetItems.get(0)).getId())) {
                    z = false;
                } else {
                    z = true;
                }
                this.noDataMore = z;
                if (!this.noDataMore) {
                    this.items.addAll(this.offsetItems);
                }
            }
        }
        return cacher().saveData(CachingConstants.CALL_US_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.CALL_US_LIST_PROPERTY + this.tabId);
        return this.items != null && !this.isOffsetLoading;
    }

    /* access modifiers changed from: protected */
    public String addOffsetIfNeeded() {
        return String.format(ServerConstants.OFFSET_FORMAT, Integer.valueOf(this.offset), Integer.valueOf(this.count));
    }

    /* access modifiers changed from: protected */
    public boolean isUsedPositioning() {
        return true;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r4, android.view.View r5, int r6, long r7) {
        /*
            r3 = this;
            android.widget.Adapter r2 = r4.getAdapter()
            java.lang.Object r0 = r2.getItem(r6)
            com.biznessapps.model.CallUsItem r0 = (com.biznessapps.model.CallUsItem) r0
            java.lang.String r1 = r0.getPhone()
            android.content.Context r2 = r3.getApplicationContext()
            com.biznessapps.utils.ViewUtils.makeCall(r2, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.lists.CallUsFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        boolean hasNoData = true;
        if (this.items != null && !this.items.isEmpty()) {
            if (this.items.size() != 1 || !StringUtils.isEmpty(((CallUsItem) this.items.get(0)).getId())) {
                hasNoData = false;
            }
            if (hasNoData) {
                ((CallUsItem) this.items.get(0)).setTitle(getString(R.string.no_locations));
            }
            List<CallUsItem> sectionList = new LinkedList<>();
            for (CallUsItem item : this.items) {
                sectionList.add(getWrappedItem(item, sectionList));
            }
            this.listView.setAdapter((ListAdapter) new CommonAdapter(holdActivity.getApplicationContext(), sectionList));
            this.listView.setSelection(this.currentItemIndex);
            initListViewListener();
        }
    }
}
