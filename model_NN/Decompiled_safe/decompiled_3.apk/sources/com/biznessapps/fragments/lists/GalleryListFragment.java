package com.biznessapps.fragments.lists;

import android.app.Activity;
import android.widget.ListAdapter;
import com.biznessapps.adapters.GalleryListAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonListFragmentNew;
import com.biznessapps.model.GalleryData;
import com.biznessapps.model.flickr.GalleryAlbum;
import java.util.LinkedList;
import java.util.List;

public class GalleryListFragment extends CommonListFragmentNew<GalleryAlbum> {
    private GalleryListAdapter adapter;
    private GalleryData galleryData;

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.galleryData = (GalleryData) getIntent().getSerializableExtra(AppConstants.GALLERY_DATA_EXTRA);
        this.items = this.galleryData.getAlbums();
        return true;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
        /*
            r4 = this;
            android.widget.Adapter r2 = r5.getAdapter()
            java.lang.Object r0 = r2.getItem(r7)
            com.biznessapps.model.flickr.GalleryAlbum r0 = (com.biznessapps.model.flickr.GalleryAlbum) r0
            android.content.Intent r1 = new android.content.Intent
            com.biznessapps.activities.CommonFragmentActivity r2 = r4.getHoldActivity()
            android.content.Context r2 = r2.getApplicationContext()
            java.lang.Class<com.biznessapps.activities.GalleryActivity> r3 = com.biznessapps.activities.GalleryActivity.class
            r1.<init>(r2, r3)
            com.biznessapps.model.GalleryData r2 = r4.galleryData
            java.util.List r3 = r0.getUrls()
            r2.setImageItems(r3)
            java.lang.String r2 = "GALLERY_DATA_EXTRA"
            com.biznessapps.model.GalleryData r3 = r4.galleryData
            r1.putExtra(r2, r3)
            com.biznessapps.activities.CommonFragmentActivity r2 = r4.getHoldActivity()
            android.content.Intent r2 = r2.getIntent()
            r1.putExtras(r2)
            r4.startActivity(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.lists.GalleryListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        if (this.items != null && !this.items.isEmpty()) {
            List<GalleryAlbum> sectionList = new LinkedList<>();
            for (GalleryAlbum item : this.items) {
                sectionList.add(getWrappedItem(item, sectionList));
            }
            this.adapter = new GalleryListAdapter(holdActivity.getApplicationContext(), sectionList);
            this.listView.setAdapter((ListAdapter) this.adapter);
            initListViewListener();
        }
    }
}
