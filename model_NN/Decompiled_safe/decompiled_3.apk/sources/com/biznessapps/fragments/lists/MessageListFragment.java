package com.biznessapps.fragments.lists;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.MessageAdapter;
import com.biznessapps.adapters.MessageTimelineAdapter;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.TellFriendDelegate;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.model.MessageItem;
import com.biznessapps.model.Tab;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.facebook.AppEventsConstants;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MessageListFragment extends CommonListFragment<MessageItem> {
    /* access modifiers changed from: private */
    public boolean isTimelineActive;
    private ImageButton listTabButton;
    /* access modifiers changed from: private */
    public int selectedPosition = 0;
    private ImageButton timelineTabButton;

    public interface MessageItemListener {
        void onItemSelected(MessageItem messageItem, int i);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MESSAGES_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.timelineTabButton = (ImageButton) root.findViewById(R.id.timeline_tab);
        this.listTabButton = (ImageButton) root.findViewById(R.id.list_tab);
        this.timelineTabButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                boolean unused = MessageListFragment.this.isTimelineActive = true;
                MessageListFragment.this.updateControlsWithData(MessageListFragment.this.getHoldActivity());
            }
        });
        this.listTabButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                boolean unused = MessageListFragment.this.isTimelineActive = false;
                MessageListFragment.this.updateControlsWithData(MessageListFragment.this.getHoldActivity());
            }
        });
        ((ViewGroup) root.findViewById(R.id.tab_buttons_container)).setBackgroundColor(AppCore.getInstance().getUiSettings().getButtonBgColor());
        ViewUtils.updateMusicBarBottomMargin(getHoldActivity().getMusicDelegate(), (int) getResources().getDimension(R.dimen.footer_bar_height));
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.message_list_layout;
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseMessages(dataToParse);
        return this.items != null;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        updateTabsState();
        plugListView(holdActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.MessageItem r0 = (com.biznessapps.model.MessageItem) r0
            if (r0 == 0) goto L_0x000f
            r2.checkRichMessage(r0)
        L_0x000f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.lists.MessageListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    private void plugListView(Activity holdActivity) {
        AbstractAdapter<MessageItem> adapter;
        boolean hasNoData = true;
        if (this.items != null && !this.items.isEmpty()) {
            Collections.sort(this.items, new SortMessagesByDate());
            List<MessageItem> sectionList = new LinkedList<>();
            if (this.items.size() != 1 || !StringUtils.isEmpty(((MessageItem) this.items.get(0)).getId())) {
                hasNoData = false;
            }
            if (hasNoData) {
                ((MessageItem) this.items.get(0)).setTitle(getString(R.string.no_messages));
                sectionList.add(getWrappedItem((CommonListEntity) this.items.get(0), sectionList));
            } else {
                for (MessageItem item : this.items) {
                    sectionList.add(getWrappedItem(item, sectionList));
                }
            }
            Context context = holdActivity.getApplicationContext();
            if (this.isTimelineActive) {
                this.selectedPosition = 0;
                AbstractAdapter<MessageItem> adapter2 = new MessageTimelineAdapter(context, sectionList, R.layout.messages_timeline_item_layout);
                ((MessageTimelineAdapter) adapter2).setLeftItemListener(new MessageItemListener() {
                    public void onItemSelected(MessageItem item, int position) {
                        boolean unused = MessageListFragment.this.isTimelineActive = false;
                        int unused2 = MessageListFragment.this.selectedPosition = position;
                        MessageListFragment.this.updateControlsWithData(MessageListFragment.this.getHoldActivity());
                    }
                });
                ((MessageTimelineAdapter) adapter2).setRightItemListener(new MessageItemListener() {
                    public void onItemSelected(MessageItem item, int position) {
                        MessageListFragment.this.checkRichMessage(item);
                    }
                });
                adapter = adapter2;
            } else {
                AbstractAdapter<MessageItem> messageAdapter = new MessageAdapter(context, sectionList, R.layout.message_row);
                ((MessageAdapter) messageAdapter).setItemListener(new MessageItemListener() {
                    public void onItemSelected(MessageItem item, int position) {
                        String messageInfo;
                        if (StringUtils.isNotEmpty(item.getUrl())) {
                            messageInfo = item.getUrl();
                        } else {
                            messageInfo = MessageListFragment.this.getTabByMessage(item).getLabel();
                        }
                        ViewGroup tellFriendContainer = (ViewGroup) MessageListFragment.this.root.findViewById(R.id.tell_friends_content);
                        TellFriendDelegate.initTellFriends(MessageListFragment.this.getHoldActivity(), tellFriendContainer, String.format(MessageListFragment.this.getString(R.string.share_message_info), messageInfo, MessageListFragment.this.getString(R.string.app_name)), AppConstants.MARKET_TEMPLATE_URL + MessageListFragment.this.getHoldActivity().getPackageName());
                        TellFriendDelegate.openFriendContent(MessageListFragment.this.getApplicationContext(), tellFriendContainer);
                    }
                });
                adapter = messageAdapter;
            }
            this.listView.setAdapter((ListAdapter) adapter);
            this.listView.setSelection(this.selectedPosition);
            initListViewListener();
        }
    }

    private void updateTabsState() {
        this.timelineTabButton.setSelected(this.isTimelineActive);
        this.listTabButton.setSelected(!this.isTimelineActive);
    }

    /* access modifiers changed from: private */
    public Tab getTabByMessage(MessageItem richMessage) {
        if (!StringUtils.isNotEmpty(richMessage.getTabId())) {
            return null;
        }
        for (Tab item : AppCore.getInstance().getCachingManager().getTabList()) {
            if (item.getTabId().equalsIgnoreCase(richMessage.getTabId())) {
                return item;
            }
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void checkRichMessage(MessageItem richMessage) {
        if (StringUtils.isNotEmpty(richMessage.getUrl())) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            String url = richMessage.getUrl();
            if (!url.contains(AppConstants.HTTP_PREFIX) && !url.contains(AppConstants.HTTPS_PREFIX)) {
                url = AppConstants.HTTP_PREFIX + url;
            }
            intent.putExtra(AppConstants.URL, url);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
            startActivity(intent);
        } else if (StringUtils.isNotEmpty(richMessage.getTabId())) {
            Tab tabToUse = null;
            Iterator i$ = AppCore.getInstance().getCachingManager().getTabList().iterator();
            while (true) {
                if (!i$.hasNext()) {
                    break;
                }
                Tab item = i$.next();
                if (item.getTabId().equalsIgnoreCase(richMessage.getTabId())) {
                    tabToUse = item;
                    break;
                }
            }
            if (tabToUse != null) {
                Intent intent2 = new Intent(getApplicationContext(), SingleFragmentActivity.class);
                if (StringUtils.isNotEmpty(tabToUse.getUrl())) {
                    intent2.putExtra(AppConstants.URL, tabToUse.getUrl());
                }
                intent2.putExtra(AppConstants.TAB_ID, tabToUse.getId());
                intent2.putExtra(AppConstants.TAB_LABEL, tabToUse.getLabel());
                intent2.putExtra(AppConstants.TAB_SPECIAL_ID, tabToUse.getTabId());
                if (StringUtils.isNotEmpty(richMessage.getDetailId()) && !richMessage.getDetailId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    intent2.putExtra(AppConstants.ITEM_ID, richMessage.getDetailId());
                }
                if (!StringUtils.isNotEmpty(richMessage.getCategoryId()) || richMessage.getCategoryId().equalsIgnoreCase(AppEventsConstants.EVENT_PARAM_VALUE_NO)) {
                    intent2.putExtra(AppConstants.SECTION_ID, tabToUse.getSectionId());
                } else {
                    intent2.putExtra(AppConstants.SECTION_ID, richMessage.getCategoryId());
                }
                intent2.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, tabToUse.getViewController());
                intent2.putExtra(AppConstants.TAB, tabToUse);
                startActivity(intent2);
            }
        }
    }

    private class SortMessagesByDate implements Comparator<MessageItem> {
        private SortMessagesByDate() {
        }

        public int compare(MessageItem o1, MessageItem o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return 1;
            }
            if (o2 == null) {
                return -1;
            }
            return o2.getDate().compareTo(o1.getDate());
        }
    }
}
