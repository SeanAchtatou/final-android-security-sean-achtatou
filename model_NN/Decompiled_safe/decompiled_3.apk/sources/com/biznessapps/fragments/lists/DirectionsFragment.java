package com.biznessapps.fragments.lists;

import android.app.Activity;
import android.widget.ListAdapter;
import com.biznessapps.adapters.CommonAdapter;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.LocationItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedList;
import java.util.List;

public class DirectionsFragment extends CommonListFragment<LocationItem> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.DIRECTION_LIST_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseLocationList(dataToParse);
        return cacher().saveData(CachingConstants.DIRECTIONS_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.DIRECTIONS_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
        /*
            r4 = this;
            android.widget.Adapter r1 = r5.getAdapter()
            java.lang.Object r0 = r1.getItem(r7)
            com.biznessapps.model.LocationItem r0 = (com.biznessapps.model.LocationItem) r0
            if (r0 == 0) goto L_0x001b
            android.content.Context r1 = r4.getApplicationContext()
            java.lang.String r2 = r0.getLongitude()
            java.lang.String r3 = r0.getLatitude()
            com.biznessapps.utils.ViewUtils.openGoogleMap(r1, r2, r3)
        L_0x001b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.lists.DirectionsFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        boolean hasNoData = true;
        if (this.items != null && !this.items.isEmpty()) {
            List<LocationItem> sectionList = new LinkedList<>();
            if (this.items.size() != 1 || !StringUtils.isEmpty(((LocationItem) this.items.get(0)).getId())) {
                hasNoData = false;
            }
            if (hasNoData) {
                ((LocationItem) this.items.get(0)).setTitle(getString(R.string.no_locations));
            }
            for (LocationItem item : this.items) {
                sectionList.add(getWrappedItem(item, sectionList));
            }
            this.listView.setAdapter((ListAdapter) new CommonAdapter(holdActivity.getApplicationContext(), sectionList));
            initListViewListener();
        }
    }
}
