package com.biznessapps.fragments.lists;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.PodcastAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.delegate.MusicDelegate;
import com.biznessapps.fragments.CommonListFragmentNew;
import com.biznessapps.layout.R;
import com.biznessapps.model.RssItem;
import com.biznessapps.player.MusicItem;
import com.biznessapps.player.MusicPlayer;
import com.biznessapps.player.PlayerService;
import com.biznessapps.player.PlayerServiceAccessor;
import com.biznessapps.player.PlayerStateListener;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.ArrayList;
import java.util.List;

public class PodcastsListFragment extends CommonListFragmentNew<RssItem> {
    private int currentPosition = 0;
    private boolean isPlayFirstItem = false;
    /* access modifiers changed from: private */
    public MusicDelegate musicDelegate = new MusicDelegate();
    private ImageButton nextButton;
    /* access modifiers changed from: private */
    public ImageButton playButton;
    private PlayerStateListener playerListener = new PlayerStateListener() {
        public void onStart(MusicItem item) {
            super.onStart(item);
            PodcastsListFragment.this.musicDelegate.updateTrackInfo(item);
        }

        public void onStateChanged(int newState) {
            PodcastsListFragment.this.musicDelegate.updatePlayerState(newState);
        }
    };
    private ImageButton previousButton;

    static /* synthetic */ int access$104(PodcastsListFragment x0) {
        int i = x0.currentPosition + 1;
        x0.currentPosition = i;
        return i;
    }

    static /* synthetic */ int access$106(PodcastsListFragment x0) {
        int i = x0.currentPosition - 1;
        x0.currentPosition = i;
        return i;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.podcast_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        ViewGroup podcastPlayerContainer = (ViewGroup) root.findViewById(R.id.music_control_root);
        this.musicDelegate.initViews(podcastPlayerContainer, getApplicationContext());
        this.musicDelegate.hideCollapseButton();
        this.nextButton = (ImageButton) root.findViewById(R.id.player_next_button);
        this.playButton = (ImageButton) root.findViewById(R.id.player_player_button);
        this.previousButton = (ImageButton) root.findViewById(R.id.player_previous_button);
        ((ViewGroup) podcastPlayerContainer.findViewById(R.id.player_next_container)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PodcastsListFragment.access$104(PodcastsListFragment.this);
                PodcastsListFragment.this.updateCurrentPosition();
                PodcastsListFragment.this.getPlayerServiceAccessor().stop();
                PodcastsListFragment.this.updateListViewAdapter();
                PodcastsListFragment.this.playPodcast();
            }
        });
        ((ViewGroup) podcastPlayerContainer.findViewById(R.id.player_previous_container)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PodcastsListFragment.access$106(PodcastsListFragment.this);
                PodcastsListFragment.this.updateCurrentPosition();
                PodcastsListFragment.this.getPlayerServiceAccessor().stop();
                PodcastsListFragment.this.updateListViewAdapter();
                PodcastsListFragment.this.playPodcast();
            }
        });
        ((ViewGroup) podcastPlayerContainer.findViewById(R.id.player_play_container)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                PodcastsListFragment.this.updateListViewAdapter();
                PodcastsListFragment.this.playPodcast();
            }
        });
        PlayerService.addListener(this.playerListener);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.podcast_layout, (ViewGroup) null);
        initViews(this.root);
        loadData();
        return this.root;
    }

    public void onDestroy() {
        super.onDestroy();
        PlayerService.removeListener(this.playerListener);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RSS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseRssList(dataToParse);
        this.isPlayFirstItem = true;
        return cacher().saveData(CachingConstants.PODCAST_LIST_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.PODCAST_LIST_PROPERTY + this.tabId);
        return this.items != null;
    }

    private void plugListView(Activity holdActivity) {
        RssItem firstItem;
        if (!this.items.isEmpty()) {
            List<RssItem> podcasts = new ArrayList<>();
            List<String> podcastUrls = new ArrayList<>();
            for (RssItem item : this.items) {
                podcasts.add(getWrappedItem(item, podcasts));
                podcastUrls.add(item.getAudioUrl());
            }
            if (!podcasts.isEmpty() && (firstItem = (RssItem) podcasts.get(0)) != null) {
                this.musicDelegate.setPlayerBgColor(ViewUtils.getColor(firstItem.getTintColor()));
            }
            MusicItem currentSong = getPlayerServiceAccessor().getCurrentSong();
            if (currentSong != null) {
                if (!podcastUrls.contains(currentSong.getUrl())) {
                    getPlayerServiceAccessor().stop();
                } else {
                    updatePositionByUrl(currentSong.getUrl());
                    this.musicDelegate.updateTrackInfo(currentSong);
                }
            }
            this.adapter = new PodcastAdapter(holdActivity.getApplicationContext(), podcasts);
            this.listView.setAdapter((ListAdapter) this.adapter);
            initListViewListener();
            if (this.isPlayFirstItem) {
                playPodcast((RssItem) this.items.get(0));
            }
            updateListViewAdapter();
        }
    }

    /* access modifiers changed from: private */
    public void updateListViewAdapter() {
        boolean isTheSame;
        int i;
        if (this.listView.getAdapter() != null && (this.listView.getAdapter() instanceof PodcastAdapter)) {
            ((PodcastAdapter) this.listView.getAdapter()).setCurrentItemIndex(this.currentPosition);
            int childCount = this.listView.getChildCount();
            if (childCount > 0) {
                for (int i2 = 0; i2 < childCount; i2++) {
                    ViewGroup convertView = (ViewGroup) this.listView.getChildAt(i2);
                    if (((RssItem) convertView.getTag()) == ((RssItem) this.items.get(this.currentPosition))) {
                        isTheSame = true;
                    } else {
                        isTheSame = false;
                    }
                    View findViewById = convertView.findViewById(R.id.podcast_play_item_image);
                    if (isTheSame) {
                        i = 0;
                    } else {
                        i = 4;
                    }
                    findViewById.setVisibility(i);
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.RssItem r0 = (com.biznessapps.model.RssItem) r0
            r2.playPodcast(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.lists.PodcastsListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void updatePositionByUrl(String url) {
        if (this.items != null && url != null) {
            for (RssItem item : this.items) {
                if (url.equalsIgnoreCase(item.getAudioUrl())) {
                    this.currentPosition = this.items.indexOf(item);
                    return;
                }
            }
        }
    }

    private void playPodcast(RssItem item) {
        if (item == null) {
            return;
        }
        if (!item.getAudioUrl().contains(AppConstants.MP3)) {
            openWebView(item.getAudioUrl());
            return;
        }
        updatePositionByUrl(item.getAudioUrl());
        updateListViewAdapter();
        getPlayerServiceAccessor().stop();
        MusicItem musicItem = new MusicItem();
        musicItem.setSingle();
        musicItem.setUrl(item.getAudioUrl());
        musicItem.setSongInfo(((RssItem) this.items.get(this.currentPosition)).getTitle());
        getPlayerServiceAccessor().play(musicItem);
    }

    private void openWebView(String url) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        if (!url.contains(AppConstants.HTTP_PREFIX) && !url.contains(AppConstants.HTTPS_PREFIX)) {
            url = AppConstants.HTTP_PREFIX + url;
        }
        intent.putExtra(AppConstants.URL, url);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
        intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
        intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void playPodcast() {
        if (this.items != null && !this.items.isEmpty() && this.currentPosition >= 0 && this.currentPosition < this.items.size()) {
            MusicItem musicItem = new MusicItem();
            musicItem.setSingle();
            musicItem.setUrl(((RssItem) this.items.get(this.currentPosition)).getAudioUrl());
            musicItem.setSongInfo(((RssItem) this.items.get(this.currentPosition)).getTitle());
            getPlayerServiceAccessor().play(musicItem);
        }
    }

    /* access modifiers changed from: protected */
    public void updateButtonState(final int newState) {
        Activity holdActivity = getHoldActivity();
        if (holdActivity != null) {
            holdActivity.runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        if (newState == 1) {
                            PodcastsListFragment.this.playButton.setBackgroundResource(R.drawable.podcast_pause_button_src);
                        } else {
                            PodcastsListFragment.this.playButton.setBackgroundResource(R.drawable.podcast_play_button_src);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void updateCurrentPosition() {
        if (this.items == null || this.items.isEmpty()) {
            this.nextButton.setEnabled(false);
            this.playButton.setEnabled(false);
            this.previousButton.setEnabled(false);
            return;
        }
        if (this.currentPosition < 0) {
            this.currentPosition = this.items.size() - 1;
        } else if (this.currentPosition >= this.items.size()) {
            this.currentPosition = 0;
        }
        this.nextButton.setEnabled(true);
        this.playButton.setEnabled(true);
        this.previousButton.setEnabled(true);
    }

    /* access modifiers changed from: private */
    public PlayerServiceAccessor getPlayerServiceAccessor() {
        if (!MusicPlayer.getInstance().isInited()) {
            MusicPlayer.getInstance().init(getHoldActivity().getApplicationContext());
        }
        return MusicPlayer.getInstance().getServiceAccessor();
    }
}
