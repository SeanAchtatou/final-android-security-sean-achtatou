package com.biznessapps.fragments.qr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.layout.views.scanning.CaptureActivity;
import com.biznessapps.model.CommonDataItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;

public class QrScannerFragment extends CommonFragment {
    private static final int SCAN_ACTIVITY_CODE = 1;
    private static final String SCAN_RESULT = "SCAN_RESULT";
    private Button chooseCameraButton;
    private CommonDataItem data;
    private ViewGroup headerContainer;
    private ViewGroup helpContainer;
    private ViewGroup layout;
    private TextView resultScanCodeView;
    private String tabId;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.root = (ViewGroup) inflater.inflate(R.layout.qr_scanner_layout, (ViewGroup) null);
        this.layout = (ViewGroup) this.root.findViewById(R.id.scan_code_root_container);
        initViews();
        initListeners();
        loadData();
        CommonUtils.sendAnalyticsEvent(getAnalyticData());
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (intent != null && requestCode == 1) {
            String contents = intent.getStringExtra("SCAN_RESULT");
            if (StringUtils.isNotEmpty(contents)) {
                this.resultScanCodeView.setVisibility(0);
                this.resultScanCodeView.setText(contents);
                if (contents != null) {
                    openWebView(contents);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.QR_READER_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.data = JsonParserUtils.parseCommonData(dataToParse);
        return cacher().saveData(CachingConstants.QR_SCANNER_PROPERTY + this.tabId, this.data);
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        if (this.data != null) {
            return this.data.getImage();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.layout;
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.data = (CommonDataItem) cacher().getData(CachingConstants.QR_SCANNER_PROPERTY + this.tabId);
        return this.data != null;
    }

    private void openWebView(String webData) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        if (!webData.contains(AppConstants.HTTP_PREFIX) && !webData.contains(AppConstants.HTTPS_PREFIX)) {
            webData = AppConstants.HTTP_PREFIX + webData;
        }
        intent.putExtra(AppConstants.URL, webData);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.WEB_VIEW_SINGLE_FRAGMENT);
        intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
        intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
        startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void startScanning() {
        startActivityForResult(new Intent(getApplicationContext(), CaptureActivity.class), 1);
    }

    private void initViews() {
        this.headerContainer = (ViewGroup) this.root.findViewById(R.id.scan_code_header_container);
        this.resultScanCodeView = (TextView) this.root.findViewById(R.id.scan_code_result_view);
        this.chooseCameraButton = (Button) this.root.findViewById(R.id.scan_code_button);
        this.helpContainer = (ViewGroup) this.root.findViewById(R.id.qr_help_container);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.chooseCameraButton.setTextColor(settings.getButtonTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.chooseCameraButton.getBackground());
        this.headerContainer.setBackgroundColor(getUiSettings().getOddRowColor());
        ((TextView) this.headerContainer.findViewById(R.id.scan_code_description)).setTextColor(getUiSettings().getOddRowTextColor());
        this.helpContainer.setBackgroundColor(getUiSettings().getEvenRowColor());
        this.resultScanCodeView.setTextColor(getUiSettings().getOddRowTextColor());
        ((TextView) this.headerContainer.findViewById(R.id.find_qr_code_title)).setTextColor(getUiSettings().getOddRowTextColor());
        ((TextView) this.helpContainer.findViewById(R.id.need_help_text)).setTextColor(getUiSettings().getEvenRowTextColor());
        ViewUtils.setGlobalBackgroundColor(this.layout);
    }

    private void initListeners() {
        this.chooseCameraButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                QrScannerFragment.this.startScanning();
            }
        });
        this.helpContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent helpIntent = new Intent(QrScannerFragment.this.getApplicationContext(), SingleFragmentActivity.class);
                helpIntent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, AppConstants.QR_SCANNER_HELP_FRAGMENT);
                helpIntent.putExtra(AppConstants.TAB_LABEL, QrScannerFragment.this.getString(R.string.help));
                QrScannerFragment.this.startActivity(helpIntent);
            }
        });
    }
}
