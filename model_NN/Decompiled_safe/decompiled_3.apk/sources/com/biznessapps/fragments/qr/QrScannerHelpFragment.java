package com.biznessapps.fragments.qr;

import android.app.Activity;
import android.widget.ListAdapter;
import com.biznessapps.adapters.QRScannerHelpAdapter;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import java.util.ArrayList;

public class QrScannerHelpFragment extends CommonListFragment<CommonListEntity> {
    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        int oddRowColor = getUiSettings().getOddRowColor();
        int oddRowTextColor = getUiSettings().getOddRowTextColor();
        int evenRowColor = getUiSettings().getEvenRowColor();
        int evenRowTextColor = getUiSettings().getEvenRowTextColor();
        this.items = new ArrayList();
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, getString(R.string.find_qr_code_for_app)), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(evenRowColor, evenRowTextColor, getString(R.string.have_good_lighting)), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, getString(R.string.line_up_camera_view)), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(evenRowColor, evenRowTextColor, getString(R.string.wait_for_camera)), this.items));
        this.items.add(getWrappedItem(new CommonListEntity(oddRowColor, oddRowTextColor, getString(R.string.display_scanned_code)), this.items));
        plugListView(holdActivity);
    }

    private void plugListView(Activity holdActivity) {
        if (!this.items.isEmpty()) {
            this.listView.setAdapter((ListAdapter) new QRScannerHelpAdapter(holdActivity.getApplicationContext(), this.items));
        }
    }
}
