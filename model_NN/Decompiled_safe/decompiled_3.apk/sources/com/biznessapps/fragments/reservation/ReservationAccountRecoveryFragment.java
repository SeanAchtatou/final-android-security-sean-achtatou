package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.Map;

public class ReservationAccountRecoveryFragment extends CommonFragment {
    private EditText userEmailView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        ViewUtils.setGlobalBackgroundColor(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_account_recovery_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.userEmailView = (EditText) root.findViewById(R.id.user_email_text);
        this.userEmailView.setOnKeyListener(ViewUtils.getOnEnterKeyListener(new Runnable() {
            public void run() {
                ReservationAccountRecoveryFragment.this.recoverPassword();
            }
        }));
        String bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        if (StringUtils.isNotEmpty(bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(bgUrl, root);
        }
        ((TextView) root.findViewById(R.id.recovery_label)).setTextColor(AppCore.getInstance().getUiSettings().getFeatureTextColor());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String data) {
        this.hasResultError = JsonParserUtils.hasDataError(data);
        return true;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_ACCOUNT_RECOVERY_FORMAT, cacher().getAppCode());
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (this.hasResultError) {
            ViewUtils.showShortToast(holdActivity.getApplicationContext(), R.string.reservation_recover_failed);
        } else {
            holdActivity.finish();
        }
    }

    /* access modifiers changed from: private */
    public void recoverPassword() {
        if (StringUtils.isNotEmpty(this.userEmailView.getText().toString())) {
            Map<String, String> params = AppHttpUtils.getEmptyParams();
            params.put(ReservationSystemConstants.RECOVERY_PARAM_EMAIL, this.userEmailView.getText().toString());
            loadPostData(params);
        }
    }
}
