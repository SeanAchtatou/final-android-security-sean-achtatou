package com.biznessapps.fragments.reservation.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.utils.CommonUtils;
import java.text.DecimalFormat;
import java.util.List;

public class ReservationServiceAdapter extends AbstractAdapter<ReservationServiceItem> {
    private Activity holdActivity;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.biznessapps.adapters.AbstractAdapter.<init>(android.app.Activity, java.util.List, int):void
     arg types: [android.app.Activity, java.util.List<com.biznessapps.model.ReservationServiceItem>, int]
     candidates:
      com.biznessapps.adapters.AbstractAdapter.<init>(android.content.Context, java.util.List, int):void
      com.biznessapps.adapters.AbstractAdapter.<init>(android.app.Activity, java.util.List, int):void */
    public ReservationServiceAdapter(Activity activity, List<ReservationServiceItem> items) {
        super(activity, (List) items, R.layout.reservation_service_item_layout);
        this.holdActivity = activity;
    }

    public ReservationServiceAdapter(Context context, List<ReservationServiceItem> items) {
        super(context, items, R.layout.reservation_service_item_layout);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.ReservationServiceHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.ReservationServiceHolder();
            holder.setServiceNameView((TextView) convertView.findViewById(R.id.reservation_service_name));
            holder.setServicePriceView((TextView) convertView.findViewById(R.id.reservation_service_price));
            holder.setServiceTimeView((TextView) convertView.findViewById(R.id.reservation_service_time));
            holder.setThumbnailView((ImageView) convertView.findViewById(R.id.reservation_service_thumbnail));
            holder.setBookItButton((Button) convertView.findViewById(R.id.reservation_service_bookit));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.ReservationServiceHolder) convertView.getTag();
        }
        final ReservationServiceItem item = (ReservationServiceItem) this.items.get(position);
        if (item != null) {
            holder.getBookItButton().setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    ReservationServiceAdapter.this.bookIt(item);
                }
            });
            AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
            CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), holder.getBookItButton().getBackground());
            holder.getBookItButton().setTextColor(settings.getButtonTextColor());
            holder.getServicePriceView().setText(item.getCurrencySign() + new DecimalFormat("##.00").format((double) item.getPrice()));
            holder.getServiceTimeView().setText(item.getMins() + " " + getContext().getString(R.string.mins));
            this.imageFetcher.loadImage(item.getThumbnail(), holder.getThumbnailView());
            holder.getServiceNameView().setText(item.getName());
        }
        return convertView;
    }

    /* access modifiers changed from: private */
    public void bookIt(ReservationServiceItem item) {
        if (item != null) {
            Intent intent = new Intent();
            intent.putExtra(AppConstants.SERVICE_EXTRA, item);
            this.holdActivity.setResult(10, intent);
            this.holdActivity.finish();
        }
    }
}
