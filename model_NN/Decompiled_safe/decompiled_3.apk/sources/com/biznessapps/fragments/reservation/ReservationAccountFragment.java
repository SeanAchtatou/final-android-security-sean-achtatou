package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.Map;

public class ReservationAccountFragment extends CommonFragment {
    private ImageButton registerTopButton;
    private String token;
    private EditText userEmailView;
    private EditText userFirstNameView;
    private EditText userLastNameView;
    private EditText userPasswordConfirmView;
    private EditText userPasswordView;
    private EditText userPhoneView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.userEmailView = (EditText) root.findViewById(R.id.user_email_text);
        this.userFirstNameView = (EditText) root.findViewById(R.id.user_first_name_text);
        this.userLastNameView = (EditText) root.findViewById(R.id.user_last_name_text);
        this.userPhoneView = (EditText) root.findViewById(R.id.user_phone_text);
        this.userPasswordView = (EditText) root.findViewById(R.id.user_password_text);
        this.userPasswordConfirmView = (EditText) root.findViewById(R.id.user_password_confirm_text);
        ((Button) root.findViewById(R.id.reservation_register_button)).setVisibility(8);
        this.registerTopButton = (ImageButton) root.findViewById(R.id.register_button);
        this.registerTopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationAccountFragment.this.updateProfile();
            }
        });
        initValues();
        ViewUtils.setGlobalBackgroundColor(root);
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_account_register_layout;
    }

    private void initValues() {
        this.token = cacher().getReservSystemCacher().getSessionToken();
        this.userEmailView.setText(cacher().getReservSystemCacher().getUserEmail());
        this.userFirstNameView.setText(cacher().getReservSystemCacher().getUserFirstName());
        this.userLastNameView.setText(cacher().getReservSystemCacher().getUserLastName());
        this.userPhoneView.setText(cacher().getReservSystemCacher().getUserPhone());
        this.bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_UPDATE_PROFILE_FORMAT, cacher().getAppCode(), this.token);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String data) {
        this.hasResultError = JsonParserUtils.hasDataError(data);
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        if (this.hasResultError) {
            ViewUtils.showShortToast(holdActivity.getApplicationContext(), R.string.reservation_update_profile_failed);
            return;
        }
        cacher().getReservSystemCacher().setUserEmail(this.userEmailView.getText().toString());
        cacher().getReservSystemCacher().setUserFirstName(this.userFirstNameView.getText().toString());
        cacher().getReservSystemCacher().setUserLastName(this.userLastNameView.getText().toString());
        cacher().getReservSystemCacher().setUserPhone(this.userPhoneView.getText().toString());
        holdActivity.finish();
    }

    /* access modifiers changed from: private */
    public void updateProfile() {
        String password = this.userPasswordView.getText().toString();
        String passwordConfirm = this.userPasswordConfirmView.getText().toString();
        if (!StringUtils.isNotEmpty(password) || !StringUtils.isNotEmpty(passwordConfirm) || !password.equalsIgnoreCase(passwordConfirm)) {
            ViewUtils.showShortToast(getApplicationContext(), R.string.reservation_update_password_doesnot_match);
            return;
        }
        Map<String, String> params = AppHttpUtils.getEmptyParams();
        params.put("u", this.userEmailView.getText().toString());
        params.put("f", this.userFirstNameView.getText().toString());
        params.put(ReservationSystemConstants.USER_LAST_NAME, this.userLastNameView.getText().toString());
        params.put("c", this.userPhoneView.getText().toString());
        if (StringUtils.isNotEmpty(password)) {
            params.put("p", password);
        }
        loadPostData(params);
    }
}
