package com.biznessapps.fragments.reservation;

import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.paypal.android.MEP.MEPAddress;
import com.paypal.android.MEP.MEPAmounts;
import com.paypal.android.MEP.MEPReceiverAmounts;
import com.paypal.android.MEP.PaymentAdjuster;
import java.io.Serializable;
import java.util.Vector;
import org.json.JSONObject;

public class ReservationAmountsAdjuster implements PaymentAdjuster, Serializable {
    private static final long serialVersionUID = 3219917316411763076L;

    public MEPAmounts adjustAmount(MEPAddress address, String currency, String amount, String tax, String shipping) {
        MEPAmounts amounts = new MEPAmounts();
        amounts.setCurrency(currency);
        amounts.setPaymentAmount(amount);
        amounts.setTax("0.00");
        amounts.setShipping("0.00");
        String street2 = address.getStreet2();
        if (street2 == null) {
            street2 = "";
        }
        JSONObject billAddress = new JSONObject();
        try {
            billAddress.put("address1", address.getStreet1());
            billAddress.put("address2", street2);
            billAddress.put("country", address.getCountrycode());
            billAddress.put("city", address.getCity());
            billAddress.put("zipcode", address.getPostalcode());
            billAddress.put("state", address.getState());
            billAddress.put("company", "");
            billAddress.put("fax", "");
            billAddress.put("phone", "");
            AppCore.getInstance().getCachingManager().saveData(AppConstants.BILL_ADDRESS_EXTRA, billAddress);
        } catch (Exception e) {
        }
        return amounts;
    }

    public Vector<MEPReceiverAmounts> adjustAmountsAdvanced(MEPAddress address, String currency, Vector<MEPReceiverAmounts> vector) {
        return null;
    }
}
