package com.biznessapps.fragments.reservation.service;

import android.app.Activity;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.utils.JsonParserUtils;
import java.util.LinkedList;
import java.util.List;

public class ReservationServiceFragment extends CommonListFragment<ReservationServiceItem> {
    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return cacher().getReservSystemCacher().getBackgroundUrl();
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_SERVICE_ITEMS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseReservationServiceData(dataToParse);
        cacher().saveData(CachingConstants.RESERVATION_SERVICE_INFO_PROPERTY + this.tabId, this.items);
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.RESERVATION_SERVICE_INFO_PROPERTY + this.tabId);
        return this.items != null && !this.items.isEmpty();
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
        /*
            r4 = this;
            android.widget.Adapter r3 = r5.getAdapter()
            java.lang.Object r2 = r3.getItem(r7)
            com.biznessapps.model.LocationItem r2 = (com.biznessapps.model.LocationItem) r2
            if (r2 == 0) goto L_0x0022
            com.biznessapps.activities.CommonFragmentActivity r0 = r4.getHoldActivity()
            android.content.Intent r1 = new android.content.Intent
            r1.<init>()
            java.lang.String r3 = "LOCATION_EXTRA"
            r1.putExtra(r3, r2)
            r3 = 9
            r0.setResult(r3, r1)
            r0.finish()
        L_0x0022:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.reservation.service.ReservationServiceFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity holdActivity) {
        if (!this.items.isEmpty()) {
            List<ReservationServiceItem> sectionList = new LinkedList<>();
            for (ReservationServiceItem item : this.items) {
                sectionList.add(getWrappedItem(item, sectionList));
            }
            this.listView.setAdapter((ListAdapter) new ReservationServiceAdapter(holdActivity, sectionList));
            initListViewListener();
        } else if (holdActivity != null) {
            Toast.makeText(holdActivity.getApplicationContext(), R.string.data_not_available, 1).show();
        }
    }
}
