package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.util.Map;

public class ReservationAccountRegisterFragment extends CommonFragment {
    private ImageButton registerTopButton;
    private EditText userEmailView;
    private EditText userFirstNameView;
    private EditText userLastNameView;
    private EditText userPasswordConfirmView;
    private EditText userPasswordView;
    private EditText userPhoneView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        ViewUtils.setGlobalBackgroundColor(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.userEmailView = (EditText) root.findViewById(R.id.user_email_text);
        this.userFirstNameView = (EditText) root.findViewById(R.id.user_first_name_text);
        this.userLastNameView = (EditText) root.findViewById(R.id.user_last_name_text);
        this.userPhoneView = (EditText) root.findViewById(R.id.user_phone_text);
        this.userPasswordView = (EditText) root.findViewById(R.id.user_password_text);
        this.userPasswordConfirmView = (EditText) root.findViewById(R.id.user_password_confirm_text);
        this.registerTopButton = (ImageButton) root.findViewById(R.id.register_button);
        final Button registerButton = (Button) root.findViewById(R.id.reservation_register_button);
        registerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationAccountRegisterFragment.this.registerAccount();
            }
        });
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        registerButton.setTextColor(settings.getButtonTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), registerButton.getBackground());
        this.registerTopButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                registerButton.performClick();
            }
        });
        this.bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_account_register_layout;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_REGISTER_ACCOUNT_FORMAT, cacher().getAppCode());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        ReservationDataKeeper data = JsonParserUtils.parseSessionToken(dataToParse);
        boolean correctData = StringUtils.isNotEmpty(data.getSessionToken());
        if (correctData) {
            cacher().getReservSystemCacher().setLoggedIn(true);
            cacher().getReservSystemCacher().setSessionToken(data.getSessionToken());
            cacher().getReservSystemCacher().setUserEmail(data.getUserEmail());
            cacher().getReservSystemCacher().setUserFirstName(data.getUserFirstName());
            cacher().getReservSystemCacher().setUserLastName(data.getUserLastName());
            cacher().getReservSystemCacher().setUserPhone(data.getUserPhone());
        }
        return correctData;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        Intent intent = new Intent();
        intent.putExtra(AppConstants.BEFORE_STATE_EXTRA, ReservationSystemConstants.LOGIN);
        holdActivity.setResult(13, intent);
        holdActivity.finish();
    }

    /* access modifiers changed from: private */
    public void registerAccount() {
        String password = this.userPasswordView.getText().toString();
        String passwordConfirm = this.userPasswordConfirmView.getText().toString();
        if (!StringUtils.isNotEmpty(password) || !StringUtils.isNotEmpty(passwordConfirm) || !password.equalsIgnoreCase(passwordConfirm)) {
            ViewUtils.showDialog(getHoldActivity(), R.string.reservation_update_password_doesnot_match);
            return;
        }
        Map<String, String> params = AppHttpUtils.getEmptyParams();
        params.put("u", this.userEmailView.getText().toString());
        params.put("f", this.userFirstNameView.getText().toString());
        params.put(ReservationSystemConstants.USER_LAST_NAME, this.userLastNameView.getText().toString());
        params.put("c", this.userPhoneView.getText().toString());
        params.put("p", password);
        loadPostData(params);
    }
}
