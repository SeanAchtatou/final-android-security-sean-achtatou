package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.Toast;
import com.biznessapps.adapters.ReservationTimeAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.LocationItem;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.model.ReservationTimeItem;
import com.biznessapps.utils.DateUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.widgets.calendar.CalendarView;
import com.biznessapps.widgets.calendar.DatePickerBar;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ReservationTimeFragment extends CommonListFragment<ReservationTimeItem> implements DatePickerBar.DatePickerBarButtonListener {
    private static final String[] days = {"sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"};
    private CalendarView calendarView;
    private Date currentTime;
    private DatePickerBar datePicker;
    private LocationItem location;
    private Date selectedDate;
    private ReservationServiceItem service;

    public void onDateChanged(DatePickerBar datePickerBar) {
        Date newPickerDate = datePickerBar.getPickerDate();
        if (isAvailableDate(newPickerDate)) {
            this.selectedDate = newPickerDate;
            loadData();
        } else if (DateUtils.isBiggerThan(newPickerDate, this.selectedDate)) {
            datePickerBar.setPickerDate(nextAvailableDate(newPickerDate));
        } else {
            Date previousDate = prevAvailableDate(newPickerDate);
            if (previousDate != null) {
                datePickerBar.setPickerDate(previousDate);
            } else {
                datePickerBar.setPickerDate(DateUtils.getTomorrowDate(newPickerDate));
            }
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_time_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.service = (ReservationServiceItem) getIntent().getSerializableExtra(AppConstants.SERVICE_EXTRA);
        this.calendarView = (CalendarView) root.findViewById(R.id.calendar);
        this.calendarView.setHolidayDays(this.service.getRestWeeks());
        this.datePicker = (DatePickerBar) root.findViewById(R.id.datepickerbar);
        this.datePicker.setListener(this);
        this.rightNavigationButton.setVisibility(0);
        this.rightNavigationButton.setBackgroundResource(R.drawable.calendar);
        this.rightNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationTimeFragment.this.showCalendar();
            }
        });
        this.location = (LocationItem) getIntent().getSerializableExtra(AppConstants.LOCATION_EXTRA);
        this.currentTime = new Date();
        Date savedDate = (Date) getIntent().getSerializableExtra(AppConstants.APPT_DATE_EXTRA);
        if (savedDate != null) {
            this.selectedDate = savedDate;
        } else {
            this.selectedDate = nextAvailableDate(correctDateWithLocationTimezone(this.currentTime));
        }
        this.calendarView.selectDate(this.selectedDate);
        this.datePicker.setPickerDate(this.selectedDate);
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(this.selectedDate);
        return String.format(ServerConstants.RESERVATION_TIME_FORMAT, cacher().getAppCode(), this.tabId, this.service.getId(), days[calendar.get(7) - 1], formatter.format(this.selectedDate), cacher().getReservSystemCacher().getUserEmail());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseReservationTimeData(dataToParse);
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        ReservationTimeItem item = (ReservationTimeItem) this.items.get(position);
        if (item != null) {
            Intent intent = new Intent();
            Activity activity = getHoldActivity();
            intent.putExtra(AppConstants.APPT_DATE_EXTRA, this.selectedDate);
            intent.putExtra(AppConstants.APPT_TIME_EXTRA, item);
            activity.setResult(11, intent);
            activity.finish();
        }
    }

    private void plugListView(Activity activity) {
        if (this.items != null && !this.items.isEmpty()) {
            this.listView.setAdapter((ListAdapter) new ReservationTimeAdapter(activity, this.items));
            initListViewListener();
        } else if (activity != null) {
            Toast.makeText(activity.getApplicationContext(), R.string.data_not_available, 1).show();
        }
    }

    private Date correctDateWithLocationTimezone(Date primaryDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis((primaryDate.getTime() + ((long) ((int) (3600000.0f * Float.parseFloat(this.location.getTimeZone()))))) - ((long) calendar.getTimeZone().getRawOffset()));
        Calendar correctedDate = Calendar.getInstance();
        correctedDate.set(calendar.get(1), calendar.get(2), calendar.get(5), 0, 0, 0);
        return correctedDate.getTime();
    }

    private Date nextAvailableDate(Date currentDate) {
        Date nextDate = DateUtils.getTomorrowDate(currentDate);
        Calendar calendar = Calendar.getInstance();
        while (true) {
            calendar.setTime(nextDate);
            if (!this.service.getRestWeeks().contains(days[calendar.get(7) - 1]) && DateUtils.isBiggerThan(nextDate, currentDate)) {
                return nextDate;
            }
            nextDate = DateUtils.getTomorrowDate(nextDate);
        }
    }

    private Date prevAvailableDate(Date currentDate) {
        Date previousDate = DateUtils.getYesterdayDate(currentDate);
        Date tomorrowsDate = correctDateWithLocationTimezone(DateUtils.getTomorrowDate(this.currentTime));
        Calendar calendar = Calendar.getInstance();
        while (true) {
            calendar.setTime(previousDate);
            if (!this.service.getRestWeeks().contains(days[calendar.get(7) - 1])) {
                if (DateUtils.isBiggerThan(tomorrowsDate, previousDate)) {
                    return null;
                }
                if (DateUtils.isBiggerThan(currentDate, previousDate)) {
                    return previousDate;
                }
            }
            previousDate = DateUtils.getYesterdayDate(previousDate);
        }
    }

    private boolean isAvailableDate(Date dateToCheck) {
        Date tomorrowsDate = correctDateWithLocationTimezone(DateUtils.getTomorrowDate(this.currentTime));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateToCheck);
        if (!DateUtils.isBiggerOrEqual(dateToCheck, tomorrowsDate)) {
            return false;
        }
        if (!this.service.getRestWeeks().contains(days[calendar.get(7) - 1])) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void showCalendar() {
        this.calendarView.setTimeInMillis(this.datePicker.getPickerDate().getTime());
        this.calendarView.setVisibility(0);
        this.calendarView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.show_fade_effect));
        this.rightNavigationButton.setText(getString(R.string.done));
        this.rightNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationTimeFragment.this.closeCalendar();
            }
        });
    }

    /* access modifiers changed from: private */
    public void closeCalendar() {
        this.calendarView.setVisibility(8);
        this.calendarView.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.hide_fade_effect));
        this.selectedDate = this.calendarView.getDate();
        this.datePicker.setPickerDate(this.selectedDate);
        this.rightNavigationButton.setBackgroundResource(R.drawable.calendar);
        this.rightNavigationButton.setText("");
        this.rightNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationTimeFragment.this.showCalendar();
            }
        });
    }
}
