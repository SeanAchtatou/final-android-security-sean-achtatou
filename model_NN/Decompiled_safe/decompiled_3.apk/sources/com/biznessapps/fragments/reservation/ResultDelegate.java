package com.biznessapps.fragments.reservation;

import com.paypal.android.MEP.PayPalResultDelegate;
import java.io.Serializable;

public class ResultDelegate implements PayPalResultDelegate, Serializable {
    private static final long serialVersionUID = 10001;

    public void onPaymentCanceled(String paymentStatus) {
        System.out.println("PayPal paymentStatus= " + paymentStatus);
    }

    public void onPaymentFailed(String paymentStatus, String correlationID, String payKey, String errorID, String errorMessage) {
        System.out.println("PayPal paymentStatus= " + paymentStatus);
        System.out.println("PayPal correlationID= " + correlationID);
        System.out.println("PayPal payKey= " + payKey);
        System.out.println("PayPal errorID= " + errorID);
        System.out.println("PayPal errorMessage= " + errorMessage);
    }

    public void onPaymentSucceeded(String payKey, String paymentStatus) {
        System.out.println("PayPal payKey= " + payKey);
        System.out.println("PayPal paymentStatus= " + paymentStatus);
    }
}
