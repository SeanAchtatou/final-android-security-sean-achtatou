package com.biznessapps.fragments.reservation;

import com.biznessapps.model.LocationItem;
import java.util.List;

public class ReservationDataKeeper {
    private String adminEmail;
    private String backgroundUrl;
    private boolean isLoggedIn;
    private List<LocationItem> locations;
    private PaymentData paymentData;
    private String sessionToken;
    private String userEmail;
    private String userFirstName;
    private String userLastName;
    private String userPhone;

    public String getAdminEmail() {
        return this.adminEmail;
    }

    public void setAdminEmail(String adminEmail2) {
        this.adminEmail = adminEmail2;
    }

    public String getBackgroundUrl() {
        return this.backgroundUrl;
    }

    public void setBackground(String backgroundUrl2) {
        this.backgroundUrl = backgroundUrl2;
    }

    public List<LocationItem> getLocations() {
        return this.locations;
    }

    public void setLocations(List<LocationItem> locations2) {
        this.locations = locations2;
    }

    public boolean isLoggedIn() {
        return this.isLoggedIn && this.sessionToken != null;
    }

    public void setLoggedIn(boolean isLoggedIn2) {
        this.isLoggedIn = isLoggedIn2;
        if (!isLoggedIn2) {
            this.sessionToken = null;
        }
    }

    public String getSessionToken() {
        return this.sessionToken;
    }

    public void setSessionToken(String sessionToken2) {
        this.sessionToken = sessionToken2;
    }

    public String getUserEmail() {
        return this.userEmail;
    }

    public void setUserEmail(String userEmail2) {
        this.userEmail = userEmail2;
    }

    public String getUserFirstName() {
        return this.userFirstName;
    }

    public void setUserFirstName(String userFirstName2) {
        this.userFirstName = userFirstName2;
    }

    public String getUserLastName() {
        return this.userLastName;
    }

    public void setUserLastName(String userLastName2) {
        this.userLastName = userLastName2;
    }

    public String getUserPhone() {
        return this.userPhone;
    }

    public void setUserPhone(String userPhone2) {
        this.userPhone = userPhone2;
    }

    public PaymentData getPaymentData() {
        return this.paymentData;
    }

    public void setPaymentData(PaymentData paymentData2) {
        this.paymentData = paymentData2;
    }

    public static class PaymentData {
        private String authorizeNetLoginId;
        private String authorizeNetTransKey;
        private int gatewayType;
        private String merchantId;
        private String merchantKey;
        private String paypalAppID;
        private String recipientEmail;

        public int getGatewayType() {
            return this.gatewayType;
        }

        public void setGatewayType(int gatewayType2) {
            this.gatewayType = gatewayType2;
        }

        public String getPaypalAppID() {
            return this.paypalAppID;
        }

        public void setPaypalAppID(String paypalAppID2) {
            this.paypalAppID = paypalAppID2;
        }

        public String getRecipientEmail() {
            return this.recipientEmail;
        }

        public void setRecipientEmail(String recipientEmail2) {
            this.recipientEmail = recipientEmail2;
        }

        public String getAuthorizeNetLoginId() {
            return this.authorizeNetLoginId;
        }

        public void setAuthorizeNetLoginId(String authorizeNetLoginId2) {
            this.authorizeNetLoginId = authorizeNetLoginId2;
        }

        public String getAuthorizeNetTransKey() {
            return this.authorizeNetTransKey;
        }

        public void setAuthorizeNetTransKey(String authorizeNetTransKey2) {
            this.authorizeNetTransKey = authorizeNetTransKey2;
        }

        public String getMerchantId() {
            return this.merchantId;
        }

        public void setMerchantId(String merchantId2) {
            this.merchantId = merchantId2;
        }

        public String getMerchantKey() {
            return this.merchantKey;
        }

        public void setMerchantKey(String merchantKey2) {
            this.merchantKey = merchantKey2;
        }
    }
}
