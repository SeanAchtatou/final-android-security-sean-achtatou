package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationBillingAddressItem;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.utils.ViewUtils;
import java.math.BigDecimal;
import net.authorize.Environment;
import net.authorize.Merchant;
import net.authorize.TransactionType;
import net.authorize.aim.Result;
import net.authorize.aim.Transaction;
import net.authorize.data.Customer;
import net.authorize.data.Order;
import net.authorize.data.creditcard.CreditCard;

public class ANetPaymentFragment extends CommonFragment {
    private EditText addressView;
    private TextView amountView;
    private String authorizeNetLoginId;
    private String authorizeNetTransKey;
    private ReservationBillingAddressItem billAddress = new ReservationBillingAddressItem();
    private EditText cardCodeView;
    private EditText cardNumberView;
    private EditText cityView;
    private EditText companyView;
    private EditText countryView;
    private String currencySign;
    private EditText expirationMonthView;
    private EditText expirationYearView;
    private EditText faxView;
    private EditText firstNameView;
    private EditText lastNameView;
    private EditText phoneView;
    private ReservationServiceItem service;
    private EditText stateView;
    private EditText zipCodeView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_anet_payment_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.amountView = (TextView) root.findViewById(R.id.anet_amount_label);
        this.cardNumberView = (EditText) root.findViewById(R.id.anet_card_number);
        this.expirationMonthView = (EditText) root.findViewById(R.id.anet_card_expiration_mm);
        this.expirationYearView = (EditText) root.findViewById(R.id.anet_card_expiration_yy);
        this.cardCodeView = (EditText) root.findViewById(R.id.anet_card_card_code);
        this.firstNameView = (EditText) root.findViewById(R.id.user_first_name_text);
        this.lastNameView = (EditText) root.findViewById(R.id.user_last_name_text);
        this.companyView = (EditText) root.findViewById(R.id.user_company_text);
        this.addressView = (EditText) root.findViewById(R.id.user_address_text);
        this.cityView = (EditText) root.findViewById(R.id.user_city_text);
        this.stateView = (EditText) root.findViewById(R.id.user_state_text);
        this.zipCodeView = (EditText) root.findViewById(R.id.user_zipcode_text);
        this.countryView = (EditText) root.findViewById(R.id.user_country_text);
        this.phoneView = (EditText) root.findViewById(R.id.user_phone_text);
        this.faxView = (EditText) root.findViewById(R.id.user_fax_text);
        this.rightNavigationButton.setVisibility(0);
        this.rightNavigationButton.setText(getString(R.string.checkout));
        this.rightNavigationButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ANetPaymentFragment.this.authorizeNetPurchase();
            }
        });
        initValues();
    }

    private void initValues() {
        this.service = (ReservationServiceItem) getIntent().getSerializableExtra(AppConstants.SERVICE_EXTRA);
        this.currencySign = getIntent().getStringExtra(AppConstants.CURRENCY_SIGN_EXTRA);
        this.amountView.setText(this.currencySign + String.format("%.2f", Float.valueOf(this.service.getReserveFee())));
        this.authorizeNetLoginId = cacher().getReservSystemCacher().getPaymentData().getAuthorizeNetLoginId();
        this.authorizeNetTransKey = cacher().getReservSystemCacher().getPaymentData().getAuthorizeNetTransKey();
    }

    /* access modifiers changed from: private */
    public void authorizeNetPurchase() {
        CreditCard creditCard = CreditCard.createCreditCard();
        creditCard.setCreditCardNumber(this.cardNumberView.getText().toString());
        creditCard.setExpirationYear(this.expirationYearView.getText().toString());
        creditCard.setExpirationMonth(this.expirationMonthView.getText().toString());
        creditCard.setCardCode(this.cardCodeView.getText().toString());
        Customer customer = Customer.createCustomer();
        customer.setFirstName(this.firstNameView.getText().toString());
        customer.setLastName(this.lastNameView.getText().toString());
        customer.setCompany(this.companyView.getText().toString());
        customer.setAddress(this.addressView.getText().toString());
        customer.setCity(this.cityView.getText().toString());
        customer.setState(this.stateView.getText().toString());
        customer.setZipPostalCode(this.zipCodeView.getText().toString());
        customer.setCountry(this.countryView.getText().toString());
        customer.setPhone(this.phoneView.getText().toString());
        customer.setFax(this.faxView.getText().toString());
        this.billAddress.setFirstName(customer.getFirstName());
        this.billAddress.setLastName(customer.getLastName());
        this.billAddress.setAddress1(customer.getAddress());
        this.billAddress.setAddress2("");
        this.billAddress.setCountry(customer.getCountry());
        this.billAddress.setCity(customer.getCity());
        this.billAddress.setZipCode(customer.getZipPostalCode());
        this.billAddress.setState(customer.getState());
        this.billAddress.setCompany(customer.getCompany());
        this.billAddress.setFax(customer.getFax());
        this.billAddress.setPhone(customer.getPhone());
        Order order = Order.createOrder();
        order.setTotalAmount(new BigDecimal((double) this.service.getReserveFee()));
        order.setDescription(this.service.getName());
        Merchant merchant = Merchant.createMerchant(Environment.SANDBOX, this.authorizeNetLoginId, this.authorizeNetTransKey);
        Transaction authCaptureTransaction = merchant.createAIMTransaction(TransactionType.AUTH_CAPTURE, new BigDecimal((double) this.service.getReserveFee()));
        authCaptureTransaction.setCreditCard(creditCard);
        authCaptureTransaction.setCustomer(customer);
        authCaptureTransaction.setOrder(order);
        Result<Transaction> result = (Result) merchant.postTransaction(authCaptureTransaction);
        if (result.isApproved()) {
            paymentSucceed((Transaction) result.getTarget());
        } else {
            ViewUtils.showDialog(getHoldActivity(), "Purchase transaction fails");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void paymentSucceed(Transaction transaction) {
        Intent intent = new Intent();
        Activity activity = getHoldActivity();
        intent.putExtra(AppConstants.PAYMENT_TRANSACTION_ID_EXTRA, transaction.getTransactionId());
        intent.putExtra(AppConstants.PAYMENT_METHOD_EXTRA, 3);
        intent.putExtra(AppConstants.PAYMENT_SUCCESS_EXTRA, true);
        intent.putExtra(AppConstants.BILL_ADDRESS_EXTRA, this.billAddress);
        activity.setResult(14, intent);
        activity.finish();
    }
}
