package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.ViewUtils;

public class ReservationApptHistoryFragment extends CommonListFragment<ReservationItem> {
    private String token;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        TextView historyTitle = (TextView) root.findViewById(R.id.reservation_history_title);
        historyTitle.setText(getIntent().getStringExtra(AppConstants.TAB_LABEL));
        historyTitle.setTextColor(AppCore.getInstance().getUiSettings().getFeatureTextColor());
        this.listView.setBackgroundColor(getResources().getColor(R.color.transparent));
        ViewUtils.setGlobalBackgroundColor(root);
        return root;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_ORDER_FORMAT, cacher().getAppCode(), this.tabId, this.token);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseReservationData(dataToParse);
        return cacher().saveData(CachingConstants.RESERVATION_HISTORY_INFO_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        plugListView(holdActivity);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_history_layout;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return cacher().getReservSystemCacher().getBackgroundUrl();
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    private void plugListView(Activity holdActivity) {
        this.listView.setAdapter((ListAdapter) new ReservationAdapter(holdActivity.getApplicationContext(), this.items));
        initListViewListener();
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        openAppointmentDetail(position);
    }

    private void openAppointmentDetail(int position) {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_HISTORY);
        intent.putExtra(AppConstants.TAB_FRAGMENT_DATA, (ReservationItem) this.items.get(position));
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_detail_title));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
        startActivityForResult(intent, 17);
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.token = cacher().getReservSystemCacher().getSessionToken();
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
    }
}
