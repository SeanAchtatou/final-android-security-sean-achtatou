package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReservationMainFragment extends CommonListFragment<ReservationItem> {
    private static final int ACCOUNT_SETTINS = 2;
    private static final int APPT_HISTORY = 1;
    private static final int CREATE_SCHEDULE = 3;
    private int beforeState;
    private TextView historyTextView;
    private ImageButton loginButton;
    private Button scheduleButton;
    private TextView settingsTextView;
    private String token;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        initListeners();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_main_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        super.initViews(root);
        this.scheduleButton = (Button) root.findViewById(R.id.schedule_appointment_button);
        this.historyTextView = (TextView) root.findViewById(R.id.appointment_history_textview);
        this.settingsTextView = (TextView) root.findViewById(R.id.account_settings_textview);
        this.loginButton = (ImageButton) root.findViewById(R.id.login_button);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.scheduleButton.setTextColor(settings.getButtonTextColor());
        this.historyTextView.setTextColor(settings.getFeatureTextColor());
        this.settingsTextView.setTextColor(settings.getFeatureTextColor());
        ((TextView) root.findViewById(R.id.upcoming_appointment_title)).setTextColor(settings.getFeatureTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.scheduleButton.getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((Button) root.findViewById(R.id.reservation_settings_image)).getBackground());
        CommonUtils.overrideImageColor(settings.getButtonBgColor(), ((Button) root.findViewById(R.id.reservation_history_image)).getBackground());
        this.listView.setBackgroundColor(getResources().getColor(R.color.transparent));
        ViewUtils.setGlobalBackgroundColor(root);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.ReservationItem r0 = (com.biznessapps.model.ReservationItem) r0
            r2.openAppointmentDetail(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.reservation.ReservationMainFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        return cacher().getReservSystemCacher().getBackgroundUrl();
    }

    private void openAppointmentDetail(ReservationItem item) {
        if (item != null) {
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_MAIN);
            intent.putExtra(AppConstants.TAB_FRAGMENT_DATA, item);
            intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_detail_title));
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
            startActivityForResult(intent, 0);
        }
    }

    private void initListeners() {
        this.scheduleButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationMainFragment.this.openScheduleWindow();
            }
        });
        this.historyTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationMainFragment.this.openAppointmentHistory();
            }
        });
        this.settingsTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationMainFragment.this.openAccountSettings();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        this.tabId = holdActivity.getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        this.token = cacher().getReservSystemCacher().getSessionToken();
        configLoginButton();
        if (StringUtils.isEmpty(cacher().getReservSystemCacher().getBackgroundUrl())) {
            new LoadBackgroundTask().execute(new String[0]);
        } else {
            this.bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        }
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_ORDER_WITH_SINCE_FORMAT, cacher().getAppCode(), this.tabId, this.token, URLEncoder.encode(new SimpleDateFormat(AppConstants.FULL_DATE_FORMAT).format(new Date())));
    }

    /* access modifiers changed from: protected */
    public boolean showToastIfDataFailed() {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        if (JsonParserUtils.hasDataError(dataToParse)) {
            cacher().getReservSystemCacher().setLoggedIn(false);
            return false;
        }
        this.items = JsonParserUtils.parseReservationData(dataToParse);
        return cacher().saveData(CachingConstants.RESERVATION_INFO_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.RESERVATION_INFO_PROPERTY + this.tabId);
        return this.items != null && !this.items.isEmpty();
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        configLoginButton();
        plugListView(holdActivity);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (8 != resultCode) {
            super.onActivityResult(requestCode, resultCode, intent);
        } else if (this.beforeState == 1) {
            openAppointmentHistory();
        } else if (this.beforeState == 2) {
            openAccountSettings();
        } else if (this.beforeState == 3) {
            openScheduleWindow();
        }
        cacher().removeData(CachingConstants.RESERVATION_INFO_PROPERTY + this.tabId);
        loadData();
    }

    private void plugListView(Activity holdActivity) {
        this.listView.setAdapter((ListAdapter) new ReservationAdapter(holdActivity.getApplicationContext(), this.items));
        initListViewListener();
    }

    /* access modifiers changed from: private */
    public String getReservationCenterUrl() {
        return String.format(ServerConstants.RESERVATION_CENTER_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: private */
    public void openLoginWindow() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_LOGIN_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.login));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void logout() {
        cacher().getReservSystemCacher().setLoggedIn(false);
        this.loginButton.setBackgroundResource(R.drawable.reservation_login_icon);
        this.loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationMainFragment.this.openLoginWindow();
            }
        });
        openLoginWindow();
    }

    private void configLoginButton() {
        if (!cacher().getReservSystemCacher().isLoggedIn()) {
            this.loginButton.setBackgroundResource(R.drawable.reservation_login_icon);
            this.loginButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    ReservationMainFragment.this.openLoginWindow();
                }
            });
            return;
        }
        this.loginButton.setBackgroundResource(R.drawable.reservation_logout_icon);
        this.loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationMainFragment.this.logout();
            }
        });
    }

    /* access modifiers changed from: private */
    public void openAppointmentHistory() {
        if (!cacher().getReservSystemCacher().isLoggedIn()) {
            this.beforeState = 1;
            openLoginWindow();
            return;
        }
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_HISTORY_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_history_title));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void openAccountSettings() {
        if (!cacher().getReservSystemCacher().isLoggedIn()) {
            this.beforeState = 2;
            openLoginWindow();
            return;
        }
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_ACCOUNT_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_account_title));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void openScheduleWindow() {
        if (!cacher().getReservSystemCacher().isLoggedIn()) {
            this.beforeState = 3;
            openLoginWindow();
            return;
        }
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        String tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_BOOK_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_book_title));
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, tabId);
        startActivityForResult(intent, 0);
    }

    private class LoadBackgroundTask extends AsyncTask<String, Integer, String> {
        private LoadBackgroundTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            ReservationDataKeeper data = JsonParserUtils.parseReservationCenterData(DataSource.getInstance().getData(ReservationMainFragment.this.getReservationCenterUrl()));
            ReservationMainFragment.this.cacher().getReservSystemCacher().setAdminEmail(data.getAdminEmail());
            ReservationMainFragment.this.cacher().getReservSystemCacher().setBackground(data.getBackgroundUrl());
            ReservationMainFragment.this.cacher().getReservSystemCacher().setLocations(data.getLocations());
            return data.getBackgroundUrl();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String bgUrl) {
            super.onPostExecute((Object) bgUrl);
            if (StringUtils.isNotEmpty(bgUrl)) {
                AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(bgUrl, ReservationMainFragment.this.root);
            }
        }
    }
}
