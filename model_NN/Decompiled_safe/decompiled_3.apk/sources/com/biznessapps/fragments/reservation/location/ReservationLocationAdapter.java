package com.biznessapps.fragments.reservation.location;

import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.layout.R;
import com.biznessapps.model.LocationItem;
import java.util.List;

public class ReservationLocationAdapter extends AbstractAdapter<LocationItem> {
    public ReservationLocationAdapter(Context context, List<LocationItem> items) {
        super(context, items, R.layout.reservation_location_item_layout);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.CommonItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.CommonItem();
            holder.setTextViewTitle((TextView) convertView.findViewById(R.id.reservation_location_name));
            holder.setBottomTextView((TextView) convertView.findViewById(R.id.reservation_location_place));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.CommonItem) convertView.getTag();
        }
        LocationItem item = (LocationItem) this.items.get(position);
        if (item != null) {
            holder.getTextViewTitle().setText(Html.fromHtml(item.getAddress1()));
            holder.getBottomTextView().setText(Html.fromHtml(getFullAddress(item)));
        }
        return convertView;
    }

    private String getFullAddress(LocationItem location) {
        String address1 = location.getAddress1();
        String address2 = location.getAddress2();
        String city = location.getCity();
        String state = location.getState();
        String zip = location.getZip();
        String address = "";
        if (!address1.equals("")) {
            address = String.format("%s%s\n", address, address1);
        }
        if (!address2.equals("")) {
            String address3 = String.format("%s%s\n", address, address2);
        }
        return String.format("%s, %s  %s", city, state, zip);
    }
}
