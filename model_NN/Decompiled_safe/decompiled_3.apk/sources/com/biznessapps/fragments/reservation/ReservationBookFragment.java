package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.LocationItem;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.model.ReservationTimeItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.ViewUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ReservationBookFragment extends CommonFragment {
    private Date apptDate;
    private ReservationTimeItem apptTime;
    private Button confirmButton;
    private LocationItem location;
    private List<LocationItem> locationList;
    private TextView locationNameView;
    private TextView locationPlaceView;
    private TextView locationTitleView;
    private ViewGroup locationViewContainer;
    private String paymentMethod;
    private boolean paymentSuccess;
    private String paymentTransactionID;
    private ReservationServiceItem service;
    private TextView serviceNameView;
    private TextView serviceTitleView;
    private ViewGroup serviceViewContainer;
    private TextView timeTitleView;
    private TextView timeValueView;
    private ViewGroup timeViewContainer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        initListeners();
        initData();
        return this.root;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 9) {
            this.location = (LocationItem) data.getSerializableExtra(AppConstants.LOCATION_EXTRA);
            this.locationPlaceView.setVisibility(0);
            this.locationPlaceView.setText(getLocationFullAddress(this.location));
            this.locationNameView.setVisibility(0);
            this.locationNameView.setText(this.location.getAddress1());
            this.serviceViewContainer.setVisibility(0);
        } else if (resultCode == 10) {
            this.service = (ReservationServiceItem) data.getSerializableExtra(AppConstants.SERVICE_EXTRA);
            this.serviceNameView.setVisibility(0);
            this.serviceNameView.setText(this.service.getName());
            this.timeViewContainer.setVisibility(0);
        } else if (resultCode == 11) {
            this.apptTime = (ReservationTimeItem) data.getSerializableExtra(AppConstants.APPT_TIME_EXTRA);
            this.apptDate = (Date) data.getSerializableExtra(AppConstants.APPT_DATE_EXTRA);
            this.timeValueView.setVisibility(0);
            this.timeValueView.setText(getTimeValue(this.apptTime));
            this.confirmButton.setVisibility(0);
        } else if (resultCode == 12) {
            this.paymentSuccess = data.getBooleanExtra(AppConstants.PAYMENT_SUCCESS_EXTRA, false);
            String beforeState = data.getStringExtra(AppConstants.BEFORE_STATE_EXTRA);
            this.paymentTransactionID = data.getStringExtra(AppConstants.PAYMENT_TRANSACTION_ID_EXTRA);
            this.paymentMethod = String.format("%d", Integer.valueOf(data.getIntExtra(AppConstants.PAYMENT_METHOD_EXTRA, 0)));
            if (ReservationSystemConstants.PAYMENT.equalsIgnoreCase(beforeState) && cacher().getReservSystemCacher().isLoggedIn() && this.paymentSuccess) {
                processConfirmAppt();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.confirmButton = (Button) root.findViewById(R.id.reservation_confirm_button);
        this.locationTitleView = (TextView) root.findViewById(R.id.reservation_location_title);
        this.locationNameView = (TextView) root.findViewById(R.id.reservation_location_name);
        this.locationPlaceView = (TextView) root.findViewById(R.id.reservation_location_place);
        this.serviceTitleView = (TextView) root.findViewById(R.id.reservation_service_title);
        this.serviceNameView = (TextView) root.findViewById(R.id.reservation_service_name);
        this.timeTitleView = (TextView) root.findViewById(R.id.reservation_time_title);
        this.timeValueView = (TextView) root.findViewById(R.id.reservation_time_value);
        this.locationViewContainer = (ViewGroup) root.findViewById(R.id.reservation_location_container);
        this.serviceViewContainer = (ViewGroup) root.findViewById(R.id.reservation_service_container);
        this.timeViewContainer = (ViewGroup) root.findViewById(R.id.reservation_time_container);
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.confirmButton.setTextColor(settings.getButtonTextColor());
        this.locationTitleView.setTextColor(settings.getFeatureTextColor());
        this.serviceTitleView.setTextColor(settings.getFeatureTextColor());
        this.timeTitleView.setTextColor(settings.getFeatureTextColor());
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.confirmButton.getBackground());
        ViewUtils.setGlobalBackgroundColor(root);
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    private void initListeners() {
        this.confirmButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                ReservationBookFragment.this.confirmPayment();
            }
        });
        this.locationViewContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationBookFragment.this.openLocationWindow();
            }
        });
        this.serviceViewContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationBookFragment.this.openServiceWindow();
            }
        });
        this.timeViewContainer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationBookFragment.this.openTimeWindow();
            }
        });
    }

    /* access modifiers changed from: private */
    public void openLocationWindow() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_location_title));
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_LOCATION_VIEW_CONTROLLER);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void openServiceWindow() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_services_title));
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_SERVICE_VIEW_CONTROLLER);
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void openTimeWindow() {
        Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
        intent.putExtra(AppConstants.TAB_LABEL, getString(R.string.reservation_time_title));
        intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_TIME_VIEW_CONTROLLER);
        intent.putExtra(AppConstants.SERVICE_EXTRA, this.service);
        intent.putExtra(AppConstants.REST_WEEK_DAYS_EXTRA, this.service.getRestWeeks());
        intent.putExtra(AppConstants.LOCATION_EXTRA, this.location);
        if (this.apptDate != null) {
            intent.putExtra(AppConstants.APPT_DATE_EXTRA, this.apptDate);
        }
        startActivityForResult(intent, 0);
    }

    /* access modifiers changed from: private */
    public void confirmPayment() {
        this.paymentSuccess = false;
        if (this.service.getReserveFee() > 0.0f) {
            Toast.makeText(getApplicationContext(), R.string.booking_after_prepayment, 1).show();
            Resources resources = getApplicationContext().getResources();
            Intent intent = new Intent(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, this.tabId);
            intent.putExtra(AppConstants.TAB_LABEL, resources.getString(R.string.pre_payment_label));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.RESERVATION_PAYMENT_INTEGRATE_VIEW_CONTROLLER);
            intent.putExtra(AppConstants.CURRENCY_EXTRA, this.service.getCurrency());
            intent.putExtra(AppConstants.CURRENCY_SIGN_EXTRA, this.service.getCurrencySign());
            intent.putExtra(AppConstants.SERVICE_NAME_EXTRA, this.service.getName());
            intent.putExtra(AppConstants.SERVICE_PRICE_EXTRA, this.service.getPrice());
            intent.putExtra(AppConstants.SUBTOTAL_EXTRA, this.service.getReserveFee());
            intent.putExtra(AppConstants.SERVICE_EXTRA, this.service);
            startActivityForResult(intent, 0);
        } else if (this.service.getReserveFee() == 0.0f) {
            processConfirmAppt();
            Toast.makeText(getApplicationContext(), R.string.booking_successful, 1).show();
        }
    }

    private void initData() {
        this.bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        this.tabId = getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID);
        this.paymentMethod = "";
        this.paymentTransactionID = "";
        this.locationList = cacher().getReservSystemCacher().getLocations();
        if (this.locationList == null || this.locationList.isEmpty()) {
            this.locationList = new ArrayList();
            this.location = new LocationItem();
        } else if (this.locationList.size() <= 1) {
            this.location = this.locationList.get(0);
        }
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_book_layout;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_CONFIRM_APPOINTMENT_FORMAT, cacher().getAppCode(), this.tabId, cacher().getReservSystemCacher().getSessionToken());
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.hasResultError = JsonParserUtils.hasDataError(dataToParse);
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (!this.hasResultError) {
            holdActivity.setResult(15, new Intent());
            holdActivity.finish();
        }
    }

    private void processConfirmAppt() {
        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(this.apptDate);
        String timeFrom = String.format("%d", Integer.valueOf(this.apptTime.getFrom()));
        String timeTo = String.format("%d", Integer.valueOf(this.apptTime.getTo()));
        String itemId = this.service.getId();
        String paidAmount = String.format("%f", Float.valueOf(this.service.getReserveFee()));
        String serviceName = this.service.getName();
        String note = this.service.getNote();
        String transactionId = this.paymentTransactionID;
        String payMethod = this.paymentMethod;
        String locationId = this.location.getId();
        Map<String, String> params = AppHttpUtils.getEmptyParams();
        params.put(ReservationSystemConstants.BOOKING_DATE_PARAM, dateString);
        params.put(ReservationSystemConstants.BOOKING_TIME_FROM_PARAM, timeFrom);
        params.put(ReservationSystemConstants.BOOKING_TIME_TO_PARAM, timeTo);
        params.put("i", itemId);
        params.put(ReservationSystemConstants.BOOKING_PAID_AMOUNT_PARAM, paidAmount);
        params.put(ReservationSystemConstants.BOOKING_SERVICE_NAME_PARAM, serviceName);
        params.put(ReservationSystemConstants.BOOKING_NOTE_PARAM, note);
        params.put(ReservationSystemConstants.BOOKING_TRANSACTION_ID_PARAM, transactionId);
        params.put(ReservationSystemConstants.BOOKING_PAY_METHOD_PARAM, payMethod);
        params.put(ReservationSystemConstants.BOOKING_LOCATION_ID_PARAM, locationId);
        loadPostData(params);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String getTimeValue(com.biznessapps.model.ReservationTimeItem r16) {
        /*
            r15 = this;
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r11 = "HH:mm"
            r1.<init>(r11)
            int r5 = r16.getFrom()
            int r6 = r16.getTo()
            java.lang.String r11 = "%02d:%02d"
            r12 = 2
            java.lang.Object[] r12 = new java.lang.Object[r12]
            r13 = 0
            int r14 = r5 / 60
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r12[r13] = r14
            r13 = 1
            int r14 = r5 % 60
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r12[r13] = r14
            java.lang.String r7 = java.lang.String.format(r11, r12)
            java.lang.String r11 = "%02d:%02d"
            r12 = 2
            java.lang.Object[] r12 = new java.lang.Object[r12]
            r13 = 0
            int r14 = r6 / 60
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r12[r13] = r14
            r13 = 1
            int r14 = r6 % 60
            java.lang.Integer r14 = java.lang.Integer.valueOf(r14)
            r12[r13] = r14
            java.lang.String r9 = java.lang.String.format(r11, r12)
            java.lang.String r3 = ""
            java.util.Date r8 = r1.parse(r7)     // Catch:{ Exception -> 0x008b }
            java.util.Date r10 = r1.parse(r9)     // Catch:{ Exception -> 0x008b }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x008b }
            java.lang.String r11 = "h:mm a"
            r2.<init>(r11)     // Catch:{ Exception -> 0x008b }
            java.lang.String r11 = "%s to %s"
            r12 = 2
            java.lang.Object[] r12 = new java.lang.Object[r12]     // Catch:{ Exception -> 0x00f2 }
            r13 = 0
            java.lang.String r14 = r2.format(r8)     // Catch:{ Exception -> 0x00f2 }
            r12[r13] = r14     // Catch:{ Exception -> 0x00f2 }
            r13 = 1
            java.lang.String r14 = r2.format(r10)     // Catch:{ Exception -> 0x00f2 }
            r12[r13] = r14     // Catch:{ Exception -> 0x00f2 }
            java.lang.String r3 = java.lang.String.format(r11, r12)     // Catch:{ Exception -> 0x00f2 }
            r1 = r2
        L_0x006e:
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r11 = "MMM d, yyyy"
            r1.<init>(r11)
            java.util.Date r11 = r15.apptDate
            java.lang.String r0 = r1.format(r11)
            java.lang.String r11 = "%s - %s"
            r12 = 2
            java.lang.Object[] r12 = new java.lang.Object[r12]
            r13 = 0
            r12[r13] = r0
            r13 = 1
            r12[r13] = r3
            java.lang.String r11 = java.lang.String.format(r11, r12)
            return r11
        L_0x008b:
            r4 = move-exception
        L_0x008c:
            r11 = 720(0x2d0, float:1.009E-42)
            if (r5 < r11) goto L_0x00ca
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.StringBuilder r11 = r11.append(r7)
            java.lang.String r12 = " PM"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r7 = r11.toString()
        L_0x00a3:
            r11 = 720(0x2d0, float:1.009E-42)
            if (r6 < r11) goto L_0x00de
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.StringBuilder r11 = r11.append(r9)
            java.lang.String r12 = " PM"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r9 = r11.toString()
        L_0x00ba:
            java.lang.String r11 = "%s to %s"
            r12 = 2
            java.lang.Object[] r12 = new java.lang.Object[r12]
            r13 = 0
            r12[r13] = r7
            r13 = 1
            r12[r13] = r9
            java.lang.String r3 = java.lang.String.format(r11, r12)
            goto L_0x006e
        L_0x00ca:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.StringBuilder r11 = r11.append(r7)
            java.lang.String r12 = " AM"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r7 = r11.toString()
            goto L_0x00a3
        L_0x00de:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.StringBuilder r11 = r11.append(r9)
            java.lang.String r12 = " AM"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.String r9 = r11.toString()
            goto L_0x00ba
        L_0x00f2:
            r4 = move-exception
            r1 = r2
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.reservation.ReservationBookFragment.getTimeValue(com.biznessapps.model.ReservationTimeItem):java.lang.String");
    }

    private String getLocationFullAddress(LocationItem location2) {
        String address1 = location2.getAddress1();
        String address2 = location2.getAddress2();
        String city = location2.getCity();
        String state = location2.getState();
        String zip = location2.getZip();
        String address = "";
        if (!address1.equals("")) {
            address = String.format("%s%s\n", address, address1);
        }
        if (!address2.equals("")) {
            String address3 = String.format("%s%s\n", address, address2);
        }
        return String.format("%s, %s  %s", city, state, zip);
    }
}
