package com.biznessapps.fragments.reservation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.AppHttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.images.BitmapDownloader;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.ReservationItem;
import com.biznessapps.utils.CommonUtils;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@SuppressLint({"ValidFragment"})
public class ReservationApptDetailFragment extends CommonFragment {
    private ReservationItem apptItem;
    private Button cancelApptButton;
    private TextView costView;
    private TextView createdDateView;
    private TextView durationEndViewPart1;
    private TextView durationEndViewPart2;
    private TextView durationStartViewPart1;
    private TextView durationStartViewPart2;
    private String mode;
    private ImageView serviceImageView;
    private TextView serviceNameView;
    private TextView startDateViewPart1;
    private TextView startDateViewPart2;

    public ReservationApptDetailFragment(String mode2) {
        this.mode = mode2;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        this.bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_detail_layout;
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        this.apptItem = (ReservationItem) getIntent().getSerializableExtra(AppConstants.TAB_FRAGMENT_DATA);
        if (this.apptItem != null) {
            data.setItemId(this.apptItem.getId());
        }
        return data;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        String url;
        AppCore.UiSettings settings = AppCore.getInstance().getUiSettings();
        this.serviceImageView = (ImageView) root.findViewById(R.id.reservation_service_thumbnail);
        this.serviceNameView = (TextView) root.findViewById(R.id.service_appt_name);
        this.createdDateView = (TextView) root.findViewById(R.id.service_created_date);
        this.startDateViewPart1 = (TextView) root.findViewById(R.id.service_start_date_part1);
        this.startDateViewPart2 = (TextView) root.findViewById(R.id.service_start_date_part2);
        this.durationStartViewPart1 = (TextView) root.findViewById(R.id.service_duration_start_date_part1);
        this.durationStartViewPart2 = (TextView) root.findViewById(R.id.service_duration_start_date_part2);
        this.durationEndViewPart1 = (TextView) root.findViewById(R.id.service_duration_end_date_part1);
        this.durationEndViewPart2 = (TextView) root.findViewById(R.id.service_duration_end_date_part2);
        this.costView = (TextView) root.findViewById(R.id.service_cost);
        this.costView.setTextColor(settings.getNavigationBarColor());
        this.cancelApptButton = (Button) root.findViewById(R.id.reservation_cancel_appt);
        this.cancelApptButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ReservationApptDetailFragment.this.cancelAppointment();
            }
        });
        CommonUtils.overrideMediumButtonColor(settings.getButtonBgColor(), this.cancelApptButton.getBackground());
        BitmapDownloader.loadCompositeDrawable(getResources(), settings.getButtonBgColor(), R.drawable.time_arrow_separator, (ImageView) root.findViewById(R.id.time_arrow_separator));
        this.cancelApptButton.setTextColor(settings.getButtonTextColor());
        if (ServerConstants.RESERVATION_DETAIL_VIEW_CONTROLLER_FROM_HISTORY.equalsIgnoreCase(this.mode)) {
            this.cancelApptButton.setVisibility(8);
        }
        this.apptItem = (ReservationItem) getIntent().getSerializableExtra(AppConstants.TAB_FRAGMENT_DATA);
        if (StringUtils.isNotEmpty(this.apptItem.getThumbnail())) {
            int paramIndex = this.apptItem.getThumbnail().indexOf("?");
            if (paramIndex > 0) {
                url = this.apptItem.getThumbnail().substring(0, paramIndex);
            } else {
                url = this.apptItem.getThumbnail();
            }
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(url, this.serviceImageView);
        }
        initValues();
        ViewUtils.setGlobalBackgroundColor(root);
    }

    /* access modifiers changed from: protected */
    public View getViewForBg() {
        return this.root;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        String token = cacher().getReservSystemCacher().getSessionToken();
        return String.format(ServerConstants.RESERVATION_ORDER_CANCEL_FORMAT, cacher().getAppCode(), getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID), token);
    }

    private void initValues() {
        this.serviceNameView.setText(this.apptItem.getServiceName());
        this.costView.setText(String.format("%s%.2f", this.apptItem.getCurrencySign(), Float.valueOf(this.apptItem.getCost())));
        try {
            Date placedOnDate = new SimpleDateFormat(AppConstants.FULL_DATE_FORMAT).parse(this.apptItem.getPlacedOn());
            SimpleDateFormat dateFormatter = new SimpleDateFormat(AppConstants.RESERVATION_ITEM_CREATE_DATE_FORMAT);
            this.createdDateView.setText(String.format("%s %s", getString(R.string.reservation_created_date_label), dateFormatter.format(placedOnDate)));
            Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(this.apptItem.getDate());
            this.startDateViewPart1.setText(new SimpleDateFormat(AppConstants.ONLY_MONTH_DATE_FORMAT).format(startDate));
            this.startDateViewPart2.setText(new SimpleDateFormat(AppConstants.ONLY_DAY_DATE_FORMAT).format(startDate));
            int minFrom = Integer.parseInt(this.apptItem.getTimeFrom());
            int minTo = Integer.parseInt(this.apptItem.getTimeTo());
            SimpleDateFormat dateFormatter2 = new SimpleDateFormat(AppConstants.RESERVATION_ITEM_HOURS_FORMAT);
            Date fromDate = new Date(0, 0, 1, minFrom / 60, minFrom % 60);
            Date toDate = new Date(0, 0, 1, minTo / 60, minTo % 60);
            String durationStartDate = dateFormatter2.format(fromDate);
            String durationEndDate = dateFormatter2.format(toDate);
            this.durationStartViewPart1.setText(durationStartDate.substring(0, durationStartDate.length() - 2));
            this.durationStartViewPart2.setText(durationStartDate.substring(durationStartDate.length() - 3, durationStartDate.length()));
            this.durationEndViewPart1.setText(durationEndDate.substring(0, durationEndDate.length() - 2));
            this.durationEndViewPart2.setText(durationEndDate.substring(durationEndDate.length() - 3, durationEndDate.length()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String data) {
        this.hasResultError = JsonParserUtils.hasDataError(data);
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        super.updateControlsWithData(holdActivity);
        if (this.hasResultError) {
            ViewUtils.showShortToast(holdActivity.getApplicationContext(), R.string.reservation_cancel_appt_message);
        } else {
            holdActivity.finish();
        }
    }

    /* access modifiers changed from: private */
    public void cancelAppointment() {
        Map<String, String> params = AppHttpUtils.getEmptyParams();
        params.put("id", this.apptItem.getId());
        loadPostData(params);
    }
}
