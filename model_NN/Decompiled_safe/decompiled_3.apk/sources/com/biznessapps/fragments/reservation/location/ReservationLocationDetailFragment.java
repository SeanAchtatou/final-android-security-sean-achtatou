package com.biznessapps.fragments.reservation.location;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biznessapps.api.AppCore;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.layout.R;
import com.biznessapps.utils.StringUtils;
import com.google.android.maps.MapView;
import org.json.JSONObject;

public class ReservationLocationDetailFragment extends CommonFragment {
    private TextView address1TextView;
    private TextView address2TextView;
    private TextView cityTextView;
    private TextView commentTextView;
    private TextView emailTextView;
    private JSONObject location;
    private MapView mapView;
    private TextView stateTextView;
    private TextView telephoneTextView;
    private TextView websiteTextView;
    private TextView zipTextView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_location_detail_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.address1TextView = (TextView) root.findViewById(R.id.address_address1_text);
        this.address2TextView = (TextView) root.findViewById(R.id.address_address2_text);
        this.cityTextView = (TextView) root.findViewById(R.id.address_city_text);
        this.stateTextView = (TextView) root.findViewById(R.id.address_state_text);
        this.zipTextView = (TextView) root.findViewById(R.id.address_zip_text);
        this.telephoneTextView = (TextView) root.findViewById(R.id.address_telephone_text);
        this.emailTextView = (TextView) root.findViewById(R.id.address_email_text);
        this.websiteTextView = (TextView) root.findViewById(R.id.address_website_text);
        this.commentTextView = (TextView) root.findViewById(R.id.address_comment_text);
        String bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        if (StringUtils.isNotEmpty(bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(bgUrl, root);
        }
        try {
            this.location = new JSONObject(getIntent().getStringExtra("location"));
            this.address1TextView.setText(this.location.getString("address_1"));
            this.address2TextView.setText(this.location.getString("address_2"));
            this.cityTextView.setText(this.location.getString("city"));
            this.stateTextView.setText(this.location.getString("state"));
            this.zipTextView.setText(this.location.getString("zip"));
            this.telephoneTextView.setText(this.location.getString("telephone_display"));
            this.emailTextView.setText(this.location.getString(ReservationSystemConstants.RECOVERY_PARAM_EMAIL));
            this.websiteTextView.setText(this.location.getString("website"));
            this.commentTextView.setText(this.location.getString("comments"));
        } catch (Exception e) {
        }
    }
}
