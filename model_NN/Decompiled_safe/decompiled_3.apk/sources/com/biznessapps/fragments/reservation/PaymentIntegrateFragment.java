package com.biznessapps.fragments.reservation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.HttpUtils;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.ReservationSystemConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonFragment;
import com.biznessapps.fragments.reservation.ReservationDataKeeper;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationBillingAddressItem;
import com.biznessapps.model.ReservationServiceItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import com.biznessapps.utils.ViewUtils;
import com.google.analytics.tracking.android.ModelFields;
import com.paypal.android.MEP.CheckoutButton;
import com.paypal.android.MEP.PayPal;
import com.paypal.android.MEP.PayPalActivity;
import com.paypal.android.MEP.PayPalPayment;
import java.math.BigDecimal;

public class PaymentIntegrateFragment extends CommonFragment {
    private static final int PAYMENT_BUTTON_HEIGHT = 60;
    private static final int PAYMENT_STATUS_CANCELLED = 3;
    private static final int PAYMENT_STATUS_FAILED = 2;
    private static final int PAYMENT_STATUS_SUCCESS = 1;
    private ReservationBillingAddressItem billAddress;
    private String currency;
    private String currencySign;
    private LinearLayout paypalButtonContainer;
    private TextView prepaymentView;
    private TextView priceView;
    private ReservationServiceItem service;
    private String serviceName;
    private TextView serviceNameView;
    private float servicePrice;
    private int status;
    private float subTotal;
    private String tabId;
    private String token;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        initViews(this.root);
        initValues();
        loadData();
        return this.root;
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.reservation_payment_layout;
    }

    /* access modifiers changed from: protected */
    public void initViews(ViewGroup root) {
        this.serviceNameView = (TextView) root.findViewById(R.id.reservation_payment_servicename);
        this.priceView = (TextView) root.findViewById(R.id.reservation_payment_serviceprice);
        this.prepaymentView = (TextView) root.findViewById(R.id.reservation_payment_serviceprepayment);
        this.paypalButtonContainer = (LinearLayout) root.findViewById(R.id.reservation_paypal_button_container);
        ViewUtils.setGlobalBackgroundColor(root);
    }

    /* access modifiers changed from: protected */
    public void initValues() {
        Intent intent = getIntent();
        this.currency = intent.getStringExtra(AppConstants.CURRENCY_EXTRA);
        this.currencySign = intent.getStringExtra(AppConstants.CURRENCY_SIGN_EXTRA);
        this.serviceName = intent.getStringExtra(AppConstants.SERVICE_NAME_EXTRA);
        this.servicePrice = intent.getFloatExtra(AppConstants.SERVICE_PRICE_EXTRA, 0.0f);
        this.subTotal = intent.getFloatExtra(AppConstants.SUBTOTAL_EXTRA, 0.0f);
        this.tabId = intent.getStringExtra(AppConstants.TAB_SPECIAL_ID);
        this.service = (ReservationServiceItem) intent.getSerializableExtra(AppConstants.SERVICE_EXTRA);
        this.serviceNameView.setText(this.serviceName);
        this.priceView.setText(this.currencySign + this.servicePrice);
        this.prepaymentView.setText(this.currencySign + this.subTotal);
        String bgUrl = cacher().getReservSystemCacher().getBackgroundUrl();
        if (StringUtils.isNotEmpty(bgUrl)) {
            AppCore.getInstance().getImageFetcherAccessor().getImageFetcher().loadBigImage(bgUrl, this.root);
        }
        this.token = cacher().getReservSystemCacher().getSessionToken();
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.RESERVATION_PAYMENT_GATEWAY_FORMAT, cacher().getAppCode(), this.tabId, this.token);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        ReservationDataKeeper.PaymentData paymentData = JsonParserUtils.parsePaymentData(dataToParse);
        cacher().getReservSystemCacher().setPaymentData(paymentData);
        if (paymentData.getGatewayType() == 1) {
            PayPal.initWithAppID(getHoldActivity(), paymentData.getPaypalAppID(), 1);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity holdActivity) {
        switch (cacher().getReservSystemCacher().getPaymentData().getGatewayType()) {
            case 1:
                addPaypalButton();
                return;
            case 2:
            case 3:
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        return cacher().getReservSystemCacher().getPaymentData() != null;
    }

    /* access modifiers changed from: protected */
    public void addPaypalButton() {
        PayPal paypal = PayPal.getInstance();
        this.paypalButtonContainer.removeAllViews();
        CheckoutButton paypalPaymentBtn = paypal.getCheckoutButton(getHoldActivity(), 0, 0);
        paypalPaymentBtn.setLayoutParams(new ViewGroup.LayoutParams(-1, (int) PAYMENT_BUTTON_HEIGHT));
        paypalPaymentBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                PaymentIntegrateFragment.this.paypalPayment();
            }
        });
        this.paypalButtonContainer.addView(paypalPaymentBtn);
    }

    /* access modifiers changed from: private */
    public void paypalPayment() {
        String recipientEmail = cacher().getReservSystemCacher().getPaymentData().getRecipientEmail();
        if (StringUtils.isNotEmpty(recipientEmail)) {
            PayPalPayment newPayment = new PayPalPayment();
            newPayment.setSubtotal(new BigDecimal((double) this.subTotal));
            newPayment.setCurrencyType(this.currency);
            newPayment.setRecipient(recipientEmail);
            newPayment.setMerchantName(this.serviceName);
            startActivityForResult(PayPal.getInstance().checkout(newPayment, getHoldActivity()), 18);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 18) {
            if (resultCode == -1) {
                paymentSuccessWithKey(data.getStringExtra(PayPalActivity.EXTRA_PAY_KEY), 1);
            } else if (resultCode == 0) {
                paymentCancelled();
            } else if (resultCode == 2) {
                paymentFailedWithCorrelationID(data.getStringExtra(PayPalActivity.EXTRA_CORRELATION_ID));
            }
        }
        addPaypalButton();
        paymentLibraryExit();
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void paymentSuccessWithKey(String payKey, int paymentMethod) {
        this.status = 1;
        cacher().saveData(AppConstants.PAYMENT_TRANSACTION_ID_EXTRA, payKey);
        cacher().saveData(AppConstants.PAYMENT_METHOD_EXTRA, Integer.valueOf(paymentMethod));
    }

    private void paymentFailedWithCorrelationID(String correlationID) {
        this.status = 2;
    }

    private void paymentCancelled() {
        this.status = 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void paymentLibraryExit() {
        switch (this.status) {
            case 1:
                Activity activity = getHoldActivity();
                Intent intent = new Intent();
                intent.putExtra(AppConstants.PAYMENT_SUCCESS_EXTRA, true);
                intent.putExtra(AppConstants.BEFORE_STATE_EXTRA, ReservationSystemConstants.PAYMENT);
                intent.putExtra(AppConstants.PAYMENT_TRANSACTION_ID_EXTRA, (String) cacher().getData(AppConstants.PAYMENT_TRANSACTION_ID_EXTRA));
                intent.putExtra(AppConstants.PAYMENT_METHOD_EXTRA, (Integer) cacher().getData(AppConstants.PAYMENT_METHOD_EXTRA));
                sendBillingAddressInfo();
                activity.setResult(12, intent);
                activity.finish();
                return;
            case 2:
                Toast.makeText(getApplicationContext(), R.string.paypal_payment_failed, 1);
                return;
            case 3:
                Toast.makeText(getApplicationContext(), R.string.paypal_payment_canceled, 1);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public String getBillingAddressUrl() {
        return String.format(ServerConstants.RESERVATION_BILLING_ADDRESS_FORMAT, cacher().getAppCode(), this.token);
    }

    private void sendBillingAddressInfo() {
        if (this.billAddress != null) {
            final String[] names = {"f", ReservationSystemConstants.USER_LAST_NAME, "a1", "a2", "cr", "ct", ModelFields.CACHE_BUSTER, "s", "wc", "fx", "c"};
            final String[] values = {this.billAddress.getFirstName(), this.billAddress.getLastName(), this.billAddress.getAddress1(), this.billAddress.getAddress2(), this.billAddress.getCountry(), this.billAddress.getCity(), this.billAddress.getZipCode(), this.billAddress.getState(), this.billAddress.getCompany(), this.billAddress.getFax(), this.billAddress.getPhone()};
            new Thread(new Runnable() {
                public void run() {
                    try {
                        HttpUtils.executePostHttpRequest(PaymentIntegrateFragment.this.getBillingAddressUrl(), names, values);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
