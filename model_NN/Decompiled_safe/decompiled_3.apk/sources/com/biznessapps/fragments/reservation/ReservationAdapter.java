package com.biznessapps.fragments.reservation;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biznessapps.adapters.AbstractAdapter;
import com.biznessapps.adapters.ListItemHolder;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.layout.R;
import com.biznessapps.model.ReservationItem;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ReservationAdapter extends AbstractAdapter<ReservationItem> {
    public ReservationAdapter(Context context, List<ReservationItem> items) {
        super(context, items, R.layout.upcoming_reserv_item_layout);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ListItemHolder.UpcomingReservationItem holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(this.layoutItemResourceId, (ViewGroup) null);
            holder = new ListItemHolder.UpcomingReservationItem();
            holder.setServiceNameView((TextView) convertView.findViewById(R.id.upcoming_reser_item_service_name));
            holder.setReservMonthView((TextView) convertView.findViewById(R.id.upcoming_reserv_item_month));
            holder.setReservDayView((TextView) convertView.findViewById(R.id.upcoming_reserv_item_day));
            holder.setReservArrowView((ImageView) convertView.findViewById(R.id.upcoming_reserv_item_arrow));
            convertView.setTag(holder);
        } else {
            holder = (ListItemHolder.UpcomingReservationItem) convertView.getTag();
        }
        ReservationItem item = (ReservationItem) this.items.get(position);
        if (item != null) {
            try {
                Date dateStart = new SimpleDateFormat("yyyy-MM-dd").parse(item.getDate());
                holder.getServiceNameView().setText(item.getServiceName());
                holder.getReservMonthView().setText(new SimpleDateFormat(AppConstants.ONLY_MONTH_DATE_FORMAT).format(dateStart));
                holder.getReservDayView().setText(new SimpleDateFormat(AppConstants.ONLY_DAY_DATE_FORMAT).format(dateStart));
                holder.getReservArrowView().setBackgroundResource(position % 2 != 0 ? R.drawable.red_arrow_next : R.drawable.gray_arrow_next);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return convertView;
    }
}
