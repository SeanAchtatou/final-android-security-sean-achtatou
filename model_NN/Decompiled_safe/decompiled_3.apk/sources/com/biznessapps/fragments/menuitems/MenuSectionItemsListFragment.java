package com.biznessapps.fragments.menuitems;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.MenuSectionItemAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.MenuSectionItem;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedList;
import java.util.List;

public class MenuSectionItemsListFragment extends CommonListFragment<MenuSectionItem> {
    private String menuItemId;

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        super.preDataLoading(holdActivity);
        this.menuItemId = holdActivity.getIntent().getStringExtra(AppConstants.MENU_ITEM_ID);
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MENU_ITEMS_FORMAT, this.menuItemId, this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseMenuItemsList(dataToParse);
        return cacher().saveData(CachingConstants.MENU_SECTIONS_PROPERTY + this.menuItemId + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setCatId(getIntent().getStringExtra(AppConstants.MENU_ITEM_ID));
        return data;
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity activity) {
        super.updateControlsWithData(activity);
        plugListView(activity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.MENU_SECTIONS_PROPERTY + this.menuItemId + this.tabId);
        return this.items != null;
    }

    /* access modifiers changed from: protected */
    public String defineBgUrl() {
        String bgUrl = super.defineBgUrl();
        if (StringUtils.isEmpty(bgUrl)) {
            return getIntent().getStringExtra(AppConstants.BG_URL_EXTRA);
        }
        return bgUrl;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.MenuSectionItem r0 = (com.biznessapps.model.MenuSectionItem) r0
            r2.openMenuItems(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.menuitems.MenuSectionItemsListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity activity) {
        boolean hasNoData = true;
        if (this.items != null && !this.items.isEmpty()) {
            MenuSectionItem preLoadedItem = null;
            List<MenuSectionItem> sectionList = new LinkedList<>();
            if (this.items.size() != 1 || !StringUtils.isEmpty(((MenuSectionItem) this.items.get(0)).getId())) {
                hasNoData = false;
            }
            if (hasNoData) {
                ((MenuSectionItem) this.items.get(0)).setTitle(getString(R.string.no_items));
            }
            for (MenuSectionItem item : this.items) {
                if (item.getId().equalsIgnoreCase(this.itemId)) {
                    preLoadedItem = item;
                }
                sectionList.add(getWrappedItem(item, sectionList));
            }
            this.listView.setAdapter((ListAdapter) new MenuSectionItemAdapter(activity.getApplicationContext(), sectionList, R.layout.menu_row));
            initListViewListener();
            openMenuItems(preLoadedItem);
        }
    }

    private void openMenuItems(MenuSectionItem item) {
        if (item != null && StringUtils.isNotEmpty(item.getId())) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.MENU_ITEM_DETAIL_ID, item.getId());
            intent.putExtra(AppConstants.SECTION_ID, this.menuItemId);
            intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.MENU_VIEW_CONTROLLER);
            intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
            startActivity(intent);
        }
    }
}
