package com.biznessapps.fragments.menuitems;

import android.app.Activity;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.infoitems.InfoDetailFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.AnalyticItem;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.JsonParserUtils;

public class MenuItemDetailFragment extends InfoDetailFragment {
    private String menuItemDetailId;

    /* access modifiers changed from: protected */
    public void preDataLoading(Activity holdActivity) {
        super.preDataLoading(holdActivity);
        this.menuItemDetailId = holdActivity.getIntent().getStringExtra(AppConstants.MENU_ITEM_DETAIL_ID);
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.info_detail_layout;
    }

    /* access modifiers changed from: protected */
    public AnalyticItem getAnalyticData() {
        AnalyticItem data = super.getAnalyticData();
        data.setCatId(getIntent().getStringExtra(AppConstants.SECTION_ID));
        data.setItemId(getIntent().getStringExtra(AppConstants.MENU_ITEM_DETAIL_ID));
        return data;
    }

    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MENU_ITEM_DETAIL_FORMAT, this.menuItemDetailId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.infoItem = JsonParserUtils.parseInfo(dataToParse);
        return cacher().saveData(CachingConstants.MENU_ITEM_DETAIL_PROPERTY + this.menuItemDetailId, this.infoItem);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.infoItem = (CommonListEntity) cacher().getData(CachingConstants.MENU_ITEM_DETAIL_PROPERTY + this.menuItemDetailId);
        return this.infoItem != null;
    }
}
