package com.biznessapps.fragments.menuitems;

import android.app.Activity;
import android.content.Intent;
import android.widget.ListAdapter;
import com.biznessapps.activities.SingleFragmentActivity;
import com.biznessapps.adapters.CommonAdapter;
import com.biznessapps.adapters.SeparatedListAdapter;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.fragments.CommonListFragment;
import com.biznessapps.layout.R;
import com.biznessapps.model.CommonListEntity;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MenuSectionsListFragment extends CommonListFragment<CommonListEntity> {
    /* access modifiers changed from: protected */
    public String getRequestUrl() {
        return String.format(ServerConstants.MENU_SECTIONS_FORMAT, cacher().getAppCode(), this.tabId);
    }

    /* access modifiers changed from: protected */
    public boolean tryParseData(String dataToParse) {
        this.items = JsonParserUtils.parseMenuList(dataToParse);
        return cacher().saveData(CachingConstants.MENU_SECTIONS_PROPERTY + this.tabId, this.items);
    }

    /* access modifiers changed from: protected */
    public void updateControlsWithData(Activity activity) {
        super.updateControlsWithData(activity);
        plugListView(activity);
    }

    /* access modifiers changed from: protected */
    public boolean canUseCachedData() {
        this.items = (List) cacher().getData(CachingConstants.MENU_SECTIONS_PROPERTY + this.tabId);
        return this.items != null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    protected void onListItemClick(android.widget.AdapterView<?> r3, android.view.View r4, int r5, long r6) {
        /*
            r2 = this;
            android.widget.Adapter r1 = r3.getAdapter()
            java.lang.Object r0 = r1.getItem(r5)
            com.biznessapps.model.CommonListEntity r0 = (com.biznessapps.model.CommonListEntity) r0
            r2.openMenuItems(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.biznessapps.fragments.menuitems.MenuSectionsListFragment.onListItemClick(android.widget.AdapterView, android.view.View, int, long):void");
    }

    private void plugListView(Activity activity) {
        if (this.items != null && !this.items.isEmpty()) {
            CommonListEntity preLoadedItem = null;
            this.adapter = new SeparatedListAdapter(activity.getApplicationContext(), R.layout.section_header, getUiSettings().getSectionBarColor(), getUiSettings().getSectionTextColor());
            Map<String, List<CommonListEntity>> sectionsMap = new LinkedHashMap<>();
            if (this.items.size() == 1 && StringUtils.isEmpty(((CommonListEntity) this.items.get(0)).getId())) {
                ((CommonListEntity) this.items.get(0)).setTitle(getString(R.string.no_items));
            }
            for (CommonListEntity item : this.items) {
                if (item.getId().equalsIgnoreCase(this.sectionId)) {
                    preLoadedItem = item;
                }
                String section = item.getSection();
                if (StringUtils.isEmpty(section)) {
                    section = "";
                }
                List<CommonListEntity> itemsList = sectionsMap.get(section);
                if (itemsList == null) {
                    itemsList = new LinkedList<>();
                }
                itemsList.add(getWrappedItem(item, itemsList));
                sectionsMap.put(section, itemsList);
            }
            for (String section2 : sectionsMap.keySet()) {
                List<CommonListEntity> itemsList2 = sectionsMap.get(section2);
                if (itemsList2 != null && itemsList2.size() > 0) {
                    ((SeparatedListAdapter) this.adapter).addSection(section2, new CommonAdapter(activity.getApplicationContext(), itemsList2));
                }
            }
            this.listView.setAdapter((ListAdapter) this.adapter);
            initListViewListener();
            openMenuItems(preLoadedItem);
        }
    }

    private void openMenuItems(CommonListEntity item) {
        if (item != null && StringUtils.isNotEmpty(item.getId())) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), SingleFragmentActivity.class);
            intent.putExtra(AppConstants.MENU_ITEM_ID, item.getId());
            intent.putExtra(AppConstants.ITEM_ID, this.itemId);
            intent.putExtra(AppConstants.BG_URL_EXTRA, this.bgUrl);
            intent.putExtra(AppConstants.TAB_ID, getHoldActivity().getTabId());
            intent.putExtra(AppConstants.TAB_SPECIAL_ID, getIntent().getStringExtra(AppConstants.TAB_SPECIAL_ID));
            intent.putExtra(AppConstants.TAB_FRAGMENT_EXTRA, ServerConstants.MENU_VIEW_CONTROLLER);
            intent.putExtra(AppConstants.TAB_LABEL, getIntent().getStringExtra(AppConstants.TAB_LABEL));
            startActivity(intent);
        }
    }
}
