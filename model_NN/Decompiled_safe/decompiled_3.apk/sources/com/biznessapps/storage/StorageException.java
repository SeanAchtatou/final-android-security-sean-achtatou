package com.biznessapps.storage;

public class StorageException extends Exception {
    private static final long serialVersionUID = 8283636980124377201L;

    public StorageException() {
    }

    public StorageException(String message) {
        super(message);
    }
}
