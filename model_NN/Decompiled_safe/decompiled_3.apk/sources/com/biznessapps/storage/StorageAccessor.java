package com.biznessapps.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.CachingManager;
import com.biznessapps.fragments.notepad.NotepadItem;
import com.biznessapps.model.CouponItem;
import com.biznessapps.model.LoyaltyItem;
import com.biznessapps.utils.StringUtils;
import java.util.ArrayList;
import java.util.List;

public class StorageAccessor {
    private static final String CACHE_COLUMN_ID = "id";
    private static final String CACHE_COLUMN_KEY = "key";
    private static final String CACHE_COLUMN_VALUE = "value";
    private static final String CACHE_TABLE = "cache_table";
    private static final String COLUMN_CHECKIN_TARGET = "checkinTarget";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_LAST_CHECKIN_TIME = "lastCheckinTime";
    private static final String COLUMN_LAST_REDEEMED_TIME = "lastRedeemedTime";
    private static final String COLUMN_LOYALTY_COUPON_APPROVED = "loyaltyCouponApproved";
    private static final String COLUMN_LOYALTY_COUPON_CODE = "loyaltyCouponCode";
    private static final String COLUMN_LOYALTY_COUPON_ID = "loyaltyCouponId";
    private static final String COLUMN_LOYALTY_COUPON_LOCKED = "loyaltyCouponLocked";
    private static final String COLUMN_LOYALTY_IMAGE_URL = "loyaltyImageUrl";
    private static final String COLUMN_LOYALTY_ITEM_ID = "loyaltyItemId";
    private static final String COUPONS_TABLE = "coupons_table";
    private static final String DATABASE_NAME = "bsa_storage6.db";
    private static final int DB_VERSION = 5;
    private static final String LOYALTY_TABLE = "loyalty_table";
    private static final String NOTEPAD_COLUMN_CONTENT = "content";
    private static final String NOTEPAD_COLUMN_DATE = "date";
    private static final String NOTEPAD_COLUMN_ID = "id";
    private static final String NOTEPAD_COLUMN_TITLE = "title";
    private static final String NOTEPAD_TABLE = "notepad_table";
    private static final String PREFERENCES_COLUMN_APP_CODE = "app_code";
    private static final String PREFERENCES_COLUMN_ID = "id";
    private static final String PREFERENCES_COLUMN_KEY = "key";
    private static final String PREFERENCES_COLUMN_VALUE = "value";
    private static final String PREFERENCES_TABLE = "preference_table";
    private static final String REQUEST_CREATE_CACHE_TABLE = "CREATE TABLE \"cache_table\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT,\"key\" TEXT,\"value\" BLOB)";
    private static final String REQUEST_CREATE_COUPONS_CUT_TABLE = "CREATE TABLE \"coupons_table\" (\"id\" INTEGER PRIMARY KEY,\"checkinTarget\" INTEGER,\"lastCheckinTime\" LONG,\"lastRedeemedTime\" LONG)";
    private static final String REQUEST_CREATE_LOYALTY_TABLE = "CREATE TABLE \"loyalty_table\" (\"id\" INTEGER PRIMARY KEY,\"loyaltyItemId\" TEXT,\"loyaltyCouponId\" TEXT,\"loyaltyImageUrl\" TEXT,\"loyaltyCouponCode\" TEXT,\"loyaltyCouponLocked\" INTEGER,\"loyaltyCouponApproved\" INTEGER)";
    private static final String REQUEST_CREATE_NOTEPAD_TABLE = "CREATE TABLE \"notepad_table\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT,\"title\" TEXT,\"content\" TEXT,\"date\" LONG)";
    private static final String REQUEST_CREATE_PREFERENCES_TABLE = "CREATE TABLE IF NOT EXISTS \"preference_table\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT,\"key\" VARCHAR(64),\"value\" TEXT,\"app_code\" TEXT)";
    private static final String REQUEST_DROP_COUPONS_TABLE = "DROP TABLE IF EXISTS coupons_table";
    private static final String REQUEST_DROP_LOYALTY_TABLE = "DROP TABLE IF EXISTS loyalty_table";
    private static final String REQUEST_DROP_NOTEPAD_TABLE = "DROP TABLE IF EXISTS notepad_table";
    private static final String REQUEST_DROP_PREFERENCE_TABLE = "DROP TABLE IF EXISTS preference_table";
    private static Object lockdb = new Object();
    private DbHelper base;

    private static final String REQUEST_GET_ALL_NOTES() {
        return "SELECT id, title, content, date FROM notepad_table";
    }

    private static final String REQUEST_GET_NOTE(String noteId) {
        return "SELECT id, title, content, date FROM notepad_table WHERE id = " + noteId;
    }

    private static final String REQUEST_GET_COUPON(String couponId) {
        return "SELECT id, checkinTarget, lastCheckinTime, lastRedeemedTime FROM coupons_table WHERE id = " + couponId;
    }

    private static final String REQUEST_GET_LOYALTY_ITEM(String itemId) {
        return "SELECT loyaltyItemId, loyaltyCouponId, loyaltyImageUrl, loyaltyCouponCode, loyaltyCouponLocked, loyaltyCouponApproved FROM loyalty_table WHERE loyaltyItemId = " + itemId;
    }

    private static final String REQUEST_GET_CACHED_ITEM(String key) {
        return "SELECT value FROM cache_table WHERE key = \"" + key + "\"";
    }

    public static final String REQUEST_DELETE_NOTE(String noteId) {
        return "DELETE FROM notepad_table WHERE id = " + noteId;
    }

    private static final String REQUEST_DELETE_COUPON(String couponId) {
        return "DELETE FROM coupons_table WHERE id = " + couponId;
    }

    private static final String REQUEST_DELETE_CACHE_ITEM(String key) {
        return "DELETE FROM cache_table WHERE key = \"" + key + "\"";
    }

    private static final String REQUEST_DELETE_LOYALTY_ITEM(String loyaltyId) {
        return "DELETE FROM loyalty_table WHERE loyaltyItemId = " + loyaltyId;
    }

    private static final String REQUEST_DELETE_PREFERENCE(String key, String appCode) {
        return "DELETE FROM preference_table WHERE key = \"" + key + "\"" + " AND " + "app_code" + " = \"" + appCode + "\"";
    }

    private static final String REQUEST_GET_PREFERENCE(String key, String appCode) {
        return "SELECT key, value FROM preference_table WHERE key = \"" + key + "\"" + " AND " + "app_code" + " = \"" + appCode + "\"";
    }

    public StorageAccessor(Context context) {
        createDB(context);
    }

    public List<NotepadItem> requestAllNotes() {
        List<NotepadItem> entries = null;
        SQLiteDatabase db = this.base.getReadableDatabase();
        db.beginTransaction();
        Cursor cursor = db.rawQuery(REQUEST_GET_ALL_NOTES(), null);
        if (cursor == null) {
            Log.d("DbHelper", String.format("cursor = null", new Object[0]));
        } else {
            boolean working = cursor.moveToFirst();
            int size = cursor.getCount();
            entries = new ArrayList<>(size);
            if (working && size > 0) {
                while (working) {
                    NotepadItem item = new NotepadItem();
                    item.setId(cursor.getString(0));
                    item.setTitle(cursor.getString(1));
                    item.setContent(cursor.getString(2));
                    item.setDate(cursor.getLong(3));
                    entries.add(item);
                    working = cursor.moveToNext();
                }
            }
            cursor.close();
            try {
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
                closeDB(db);
            }
        }
        return entries;
    }

    public NotepadItem getNote(String noteId) {
        NotepadItem item = null;
        SQLiteDatabase db = this.base.getReadableDatabase();
        try {
            db.beginTransaction();
            Cursor cursor = db.rawQuery(REQUEST_GET_NOTE(noteId), null);
            if (cursor == null) {
                Log.d("DbHelper", String.format("cursor = null", new Object[0]));
                db.endTransaction();
                closeDB(db);
                return null;
            }
            if (cursor.moveToFirst()) {
                NotepadItem item2 = new NotepadItem();
                try {
                    item2.setId(cursor.getString(0));
                    item2.setTitle(cursor.getString(1));
                    item2.setContent(cursor.getString(2));
                    item2.setDate(cursor.getLong(3));
                    item = item2;
                } catch (Throwable th) {
                    th = th;
                    db.endTransaction();
                    closeDB(db);
                    throw th;
                }
            }
            cursor.close();
            db.setTransactionSuccessful();
            db.endTransaction();
            closeDB(db);
            return item;
        } catch (Throwable th2) {
            th = th2;
            db.endTransaction();
            closeDB(db);
            throw th;
        }
    }

    public LoyaltyItem getLoyaltyItem(String itemId) {
        boolean z;
        LoyaltyItem item = null;
        SQLiteDatabase db = this.base.getReadableDatabase();
        try {
            db.beginTransaction();
            Cursor cursor = db.rawQuery(REQUEST_GET_LOYALTY_ITEM(itemId), null);
            if (cursor == null) {
                Log.d("DbHelper", String.format("cursor = null", new Object[0]));
                db.endTransaction();
                closeDB(db);
                return null;
            }
            boolean working = cursor.moveToFirst();
            int size = cursor.getCount();
            if (working && size > 0) {
                List<LoyaltyItem.LoyaltyCardItem> loyaltyCards = new ArrayList<>();
                LoyaltyItem item2 = new LoyaltyItem();
                while (working) {
                    try {
                        LoyaltyItem.LoyaltyCardItem cardItem = new LoyaltyItem.LoyaltyCardItem();
                        item2.setId(cursor.getString(0));
                        item2.setImage(cursor.getString(2));
                        cardItem.setCouponId(cursor.getString(1));
                        cardItem.setCouponCode(cursor.getString(3));
                        cardItem.setLocked(cursor.getInt(4) == 1);
                        if (cursor.getInt(5) == 1) {
                            z = true;
                        } else {
                            z = false;
                        }
                        cardItem.setApproved(z);
                        loyaltyCards.add(cardItem);
                        working = cursor.moveToNext();
                    } catch (Throwable th) {
                        th = th;
                        db.endTransaction();
                        closeDB(db);
                        throw th;
                    }
                }
                item2.setCoupons(loyaltyCards);
                item = item2;
            }
            cursor.close();
            db.setTransactionSuccessful();
            db.endTransaction();
            closeDB(db);
            return item;
        } catch (Throwable th2) {
            th = th2;
            db.endTransaction();
            closeDB(db);
            throw th;
        }
    }

    public void saveLoayltyItem(LoyaltyItem loyaltyItem) throws StorageException {
        int i;
        int i2;
        if (loyaltyItem != null && loyaltyItem.getCoupons() != null && !loyaltyItem.getCoupons().isEmpty()) {
            SQLiteDatabase db = this.base.getWritableDatabase();
            try {
                db.beginTransaction();
                db.execSQL(REQUEST_DELETE_LOYALTY_ITEM(loyaltyItem.getId()));
                ContentValues values = new ContentValues();
                for (LoyaltyItem.LoyaltyCardItem card : loyaltyItem.getCoupons()) {
                    values.put(COLUMN_LOYALTY_ITEM_ID, loyaltyItem.getId());
                    values.put(COLUMN_LOYALTY_COUPON_ID, card.getCouponId());
                    values.put(COLUMN_LOYALTY_IMAGE_URL, loyaltyItem.getImage());
                    values.put(COLUMN_LOYALTY_COUPON_CODE, card.getCouponCode());
                    if (card.isLocked()) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    values.put(COLUMN_LOYALTY_COUPON_LOCKED, Integer.valueOf(i));
                    if (card.isApproved()) {
                        i2 = 1;
                    } else {
                        i2 = 0;
                    }
                    values.put(COLUMN_LOYALTY_COUPON_APPROVED, Integer.valueOf(i2));
                    db.insert(LOYALTY_TABLE, null, values);
                    values.clear();
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                closeDB(db);
            } catch (Exception e) {
                throw new StorageException("error with adding coupon's data to the storage");
            } catch (Throwable th) {
                db.endTransaction();
                closeDB(db);
                throw th;
            }
        }
    }

    public String getCachedItem(String key) {
        String value = null;
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        synchronized (lockdb) {
            SQLiteDatabase db = this.base.getReadableDatabase();
            try {
                db.beginTransaction();
                Cursor cursor = db.rawQuery(REQUEST_GET_CACHED_ITEM(key), null);
                if (cursor == null) {
                    Log.d("DbHelper", String.format("cursor = null", new Object[0]));
                    db.endTransaction();
                    closeDB(db);
                    return null;
                }
                if (cursor.moveToFirst()) {
                    value = new String(cursor.getBlob(0));
                }
                cursor.close();
                db.setTransactionSuccessful();
                db.endTransaction();
                closeDB(db);
                return value;
            } catch (Throwable th) {
                db.endTransaction();
                closeDB(db);
                throw th;
            }
        }
    }

    public void saveCacheData(String key, String value) throws StorageException {
        if (StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(value)) {
            synchronized (lockdb) {
                SQLiteDatabase db = this.base.getWritableDatabase();
                try {
                    db.beginTransaction();
                    ContentValues values = new ContentValues();
                    values.put("key", key);
                    values.put("value", value.getBytes());
                    db.insert(CACHE_TABLE, null, values);
                    values.clear();
                    db.setTransactionSuccessful();
                    db.endTransaction();
                    closeDB(db);
                } catch (Exception e) {
                    throw new StorageException("error with saving caching data to the storage");
                } catch (Throwable th) {
                    db.endTransaction();
                    closeDB(db);
                    throw th;
                }
            }
        }
    }

    public CouponItem getCoupon(String couponId) {
        CouponItem item = null;
        if (StringUtils.isEmpty(couponId)) {
            return null;
        }
        SQLiteDatabase db = this.base.getReadableDatabase();
        try {
            db.beginTransaction();
            Cursor cursor = db.rawQuery(REQUEST_GET_COUPON(couponId), null);
            if (cursor == null) {
                Log.d("DbHelper", String.format("cursor = null", new Object[0]));
                db.endTransaction();
                closeDB(db);
                return null;
            }
            if (cursor.moveToFirst()) {
                CouponItem item2 = new CouponItem();
                try {
                    item2.setId(cursor.getString(0));
                    item2.setCheckinTarget(cursor.getInt(1));
                    item2.setLastCheckinTime(cursor.getLong(2));
                    item2.setLastRedeemedTime(cursor.getLong(3));
                    item = item2;
                } catch (Throwable th) {
                    th = th;
                    db.endTransaction();
                    closeDB(db);
                    throw th;
                }
            }
            cursor.close();
            db.setTransactionSuccessful();
            db.endTransaction();
            closeDB(db);
            return item;
        } catch (Throwable th2) {
            th = th2;
            db.endTransaction();
            closeDB(db);
            throw th;
        }
    }

    public void delNote(String noteId) throws StorageException {
        SQLiteDatabase db = this.base.getWritableDatabase();
        if (noteId != null) {
            try {
                db.beginTransaction();
                db.execSQL(REQUEST_DELETE_NOTE(noteId));
                db.setTransactionSuccessful();
                db.endTransaction();
                closeDB(db);
            } catch (Exception e) {
                throw new StorageException("error with deleting Note's data from the storage");
            } catch (Throwable th) {
                db.endTransaction();
                closeDB(db);
                throw th;
            }
        }
    }

    public void addNote(List<NotepadItem> notesToAdd) throws StorageException {
        SQLiteDatabase db = this.base.getWritableDatabase();
        if (notesToAdd != null && notesToAdd.size() != 0) {
            try {
                db.beginTransaction();
                ContentValues values = new ContentValues();
                for (NotepadItem item : notesToAdd) {
                    db.execSQL(REQUEST_DELETE_NOTE(item.getId()));
                    values.put("id", item.getId());
                    values.put("title", item.getTitle());
                    values.put(NOTEPAD_COLUMN_CONTENT, item.getContent());
                    values.put(NOTEPAD_COLUMN_DATE, Long.valueOf(item.getDate()));
                    db.insert(NOTEPAD_TABLE, null, values);
                    values.clear();
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                closeDB(db);
            } catch (Exception e) {
                throw new StorageException("error with adding Note's data to the storage");
            } catch (Throwable th) {
                db.endTransaction();
                closeDB(db);
                throw th;
            }
        }
    }

    public <T> void putConfig(String key, T dataToSave) throws StorageException {
        SQLiteDatabase db = this.base.getWritableDatabase();
        if (dataToSave == null) {
            db.endTransaction();
            closeDB(db);
            return;
        }
        try {
            db.beginTransaction();
            ContentValues values = new ContentValues();
            CachingManager cache = AppCore.getInstance().getCachingManager();
            db.execSQL(REQUEST_DELETE_PREFERENCE(key, cache.getAppCode()));
            values.put("key", key);
            values.put("value", dataToSave.toString());
            values.put("app_code", cache.getAppCode());
            db.insert(PREFERENCES_TABLE, null, values);
            values.clear();
            db.setTransactionSuccessful();
            db.endTransaction();
            closeDB(db);
        } catch (Exception e) {
            throw new StorageException("error with adding preference value to the storage");
        } catch (Throwable th) {
            db.endTransaction();
            closeDB(db);
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public String getConfig(String key) throws StorageException {
        String item = "";
        SQLiteDatabase db = this.base.getReadableDatabase();
        try {
            db.beginTransaction();
            Cursor cursor = db.rawQuery(REQUEST_GET_PREFERENCE(key, AppCore.getInstance().getCachingManager().getAppCode()), null);
            if (cursor == null) {
                Log.d("DbHelper", String.format("cursor = null", new Object[0]));
                db.endTransaction();
                closeDB(db);
                return item;
            }
            if (cursor.moveToFirst()) {
                item = cursor.getString(1);
            }
            cursor.close();
            db.setTransactionSuccessful();
            db.endTransaction();
            closeDB(db);
            return item;
        } catch (Throwable th) {
            db.endTransaction();
            closeDB(db);
            throw th;
        }
    }

    public void addCoupons(List<CouponItem> couponsToAdd) throws StorageException {
        SQLiteDatabase db = this.base.getWritableDatabase();
        if (couponsToAdd != null) {
            try {
                if (couponsToAdd.size() != 0) {
                    db.beginTransaction();
                    ContentValues values = new ContentValues();
                    for (CouponItem item : couponsToAdd) {
                        db.execSQL(REQUEST_DELETE_COUPON(item.getId()));
                        values.put("id", item.getId());
                        values.put(COLUMN_CHECKIN_TARGET, Integer.valueOf(item.getCheckinTarget()));
                        values.put(COLUMN_LAST_CHECKIN_TIME, Long.valueOf(item.getLastCheckinTime()));
                        values.put(COLUMN_LAST_REDEEMED_TIME, Long.valueOf(item.getLastRedeemedTime()));
                        db.insert(COUPONS_TABLE, null, values);
                        values.clear();
                    }
                    db.setTransactionSuccessful();
                    db.endTransaction();
                    closeDB(db);
                    return;
                }
            } catch (Exception e) {
                throw new StorageException("error with adding coupon's data to the storage");
            } catch (Throwable th) {
                db.endTransaction();
                closeDB(db);
                throw th;
            }
        }
        db.endTransaction();
        closeDB(db);
    }

    private void createDB(Context context) {
        try {
            this.base = new DbHelper(context, DATABASE_NAME);
        } catch (SQLiteException e) {
            Log.e("DB", "Failed to open/create database!");
        }
    }

    private synchronized void closeDB(SQLiteDatabase db) {
        if (db != null) {
            db.close();
        }
    }

    private static class DbHelper extends SQLiteOpenHelper {
        DbHelper(Context context, String dbName) {
            super(context, dbName, (SQLiteDatabase.CursorFactory) null, 5);
        }

        /* JADX INFO: finally extract failed */
        public void onCreate(SQLiteDatabase db) {
            Log.d("DbHelper", String.format("creating base", new Object[0]));
            db.beginTransaction();
            db.execSQL(StorageAccessor.REQUEST_CREATE_CACHE_TABLE);
            db.execSQL(StorageAccessor.REQUEST_CREATE_COUPONS_CUT_TABLE);
            db.execSQL(StorageAccessor.REQUEST_CREATE_NOTEPAD_TABLE);
            db.execSQL(StorageAccessor.REQUEST_CREATE_LOYALTY_TABLE);
            db.execSQL(StorageAccessor.REQUEST_CREATE_PREFERENCES_TABLE);
            try {
                db.setTransactionSuccessful();
                db.endTransaction();
                Log.d("DbHelper", String.format("initialized", new Object[0]));
            } catch (Throwable th) {
                db.endTransaction();
                throw th;
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(StorageAccessor.REQUEST_DROP_LOYALTY_TABLE);
            db.execSQL(StorageAccessor.REQUEST_DROP_COUPONS_TABLE);
            db.execSQL(StorageAccessor.REQUEST_DROP_NOTEPAD_TABLE);
            db.execSQL(StorageAccessor.REQUEST_DROP_PREFERENCE_TABLE);
            onCreate(db);
        }
    }
}
