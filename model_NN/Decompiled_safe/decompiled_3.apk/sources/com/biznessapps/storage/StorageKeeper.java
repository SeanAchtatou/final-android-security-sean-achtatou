package com.biznessapps.storage;

import android.content.Context;
import com.biznessapps.fragments.notepad.NotepadItem;
import com.biznessapps.model.CouponItem;
import com.biznessapps.model.LoyaltyItem;
import java.util.List;

public class StorageKeeper {
    private static StorageKeeper instance;
    private StorageAccessor dbAccessor;

    public static StorageKeeper instance() {
        return instance;
    }

    public static void init(Context context) {
        if (instance == null) {
            instance = new StorageKeeper(context);
        }
    }

    private StorageKeeper(Context context) {
        this.dbAccessor = new StorageAccessor(context);
    }

    public List<NotepadItem> getNotes() {
        return this.dbAccessor.requestAllNotes();
    }

    public CouponItem getCouponData(String couponId) {
        return this.dbAccessor.getCoupon(couponId);
    }

    public void addCoupons(List<CouponItem> couponsList) {
        try {
            this.dbAccessor.addCoupons(couponsList);
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    public void addNotes(List<NotepadItem> noteList) {
        try {
            this.dbAccessor.addNote(noteList);
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    public void delNote(NotepadItem currentNote) {
        try {
            this.dbAccessor.delNote(currentNote.getId());
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    public void saveLoyaltyItem(LoyaltyItem loyaltyItem) {
        try {
            this.dbAccessor.saveLoayltyItem(loyaltyItem);
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    public String getCachedItem(String key) {
        return this.dbAccessor.getCachedItem(key);
    }

    public void saveCacheItem(String key, String value) {
        try {
            this.dbAccessor.saveCacheData(key, value);
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    public LoyaltyItem getLoyaltyItem(String itemId) {
        return this.dbAccessor.getLoyaltyItem(itemId);
    }

    public <T> void put(String key, T dataToSave) {
        try {
            this.dbAccessor.putConfig(key, dataToSave);
        } catch (StorageException e) {
            e.printStackTrace();
        }
    }

    public String get(String key) {
        try {
            return this.dbAccessor.getConfig(key);
        } catch (StorageException e) {
            e.printStackTrace();
            return null;
        }
    }
}
