package com.biznessapps.pushnotifications;

import java.io.Serializable;

public class RichPushNotification implements Serializable {
    public static final int WITH_TAB_CONTENT_TYPE = 2;
    public static final int WITH_URL_TYPE = 1;
    private static final long serialVersionUID = 8073233805310868167L;
    private String categoryId;
    private String detailId;
    private String id;
    private String latitude;
    private String longitude;
    private String radius;
    private String tabId;
    private int type;
    private String url;

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public String getTabId() {
        return this.tabId;
    }

    public void setTabId(String tabId2) {
        this.tabId = tabId2;
    }

    public String getCategoryId() {
        return this.categoryId;
    }

    public void setCatId(String categoryId2) {
        this.categoryId = categoryId2;
    }

    public String getDetailId() {
        return this.detailId;
    }

    public void setDetailId(String detailId2) {
        this.detailId = detailId2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getRadius() {
        return this.radius;
    }

    public void setRadius(String radius2) {
        this.radius = radius2;
    }
}
