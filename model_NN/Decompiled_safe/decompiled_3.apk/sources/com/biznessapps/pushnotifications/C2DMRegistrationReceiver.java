package com.biznessapps.pushnotifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class C2DMRegistrationReceiver extends BroadcastReceiver {
    private static final String GCM_EXTRA_KEY = "GCM_ID";
    private static final String PARAM_ACC_ID = "deviceid";
    private static final String PARAM_APPNAME = "package_name";
    private static final String PARAM_REG_ID = "registrationid";
    private static final String PUSH_NOTIFICATIONS_SEND_PART_URL = "http://%s:8080/notification/TokenReceiver";
    private static final String PUSH_NOTIFICATIONS_SEND_URL = "http://198.57.176.205:8080/notification/TokenReceiver";
    private static final String REGISTRACTION_ACTION = "com.google.android.c2dm.intent.REGISTRATION";
    private static final String REGISTRATION_ID = "registration_id";

    public void onReceive(final Context context, Intent intent) {
        if (REGISTRACTION_ACTION.equals(intent.getAction())) {
            final String registrationId = intent.getStringExtra(REGISTRATION_ID);
            final String deviceId = Settings.Secure.getString(context.getContentResolver(), "android_id");
            new Thread(new Runnable() {
                public void run() {
                    C2DMRegistrationReceiver.this.sendDataToServer(context, deviceId, registrationId);
                }
            }).start();
        }
    }

    public void sendDataToServer(Context context, String deviceId, String registrationId) {
        String pushingIp = AppCore.getInstance().getAppSettings().getPushingIp();
        if (StringUtils.isEmpty(pushingIp)) {
            String appCode = AppCore.getInstance().getCachingManager().getAppCode();
            JsonParserUtils.updateInitStateWithData(DataSource.getInstance().getData(String.format(ServerConstants.INIT_URL_FORMAT, appCode)));
            pushingIp = AppCore.getInstance().getAppSettings().getPushingIp();
        }
        String pushingUrl = String.format(PUSH_NOTIFICATIONS_SEND_PART_URL, pushingIp);
        try {
            new DefaultHttpClient().execute(new HttpPost(pushingUrl));
        } catch (Exception e) {
            pushingUrl = null;
        }
        if (pushingUrl == null) {
            pushingUrl = PUSH_NOTIFICATIONS_SEND_URL;
        }
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(pushingUrl);
        try {
            List<NameValuePair> nameValuePairs = new ArrayList<>(1);
            nameValuePairs.add(new BasicNameValuePair(PARAM_APPNAME, context.getPackageName()));
            nameValuePairs.add(new BasicNameValuePair(PARAM_ACC_ID, deviceId));
            nameValuePairs.add(new BasicNameValuePair(PARAM_REG_ID, registrationId + GCM_EXTRA_KEY));
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            client.execute(post);
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
