package com.biznessapps.pushnotifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import com.biznessapps.api.AppCore;
import com.biznessapps.api.DataSource;
import com.biznessapps.constants.AppConstants;
import com.biznessapps.constants.CachingConstants;
import com.biznessapps.constants.ServerConstants;
import com.biznessapps.layout.MainController;
import com.biznessapps.layout.R;
import com.biznessapps.utils.JsonParserUtils;
import com.biznessapps.utils.StringUtils;

public class C2DMMessagesReceiver extends BroadcastReceiver implements AppConstants {
    private static final String PAYLOAD = "payload";
    private static final String RECEIVE_ACTION = "com.google.android.c2dm.intent.RECEIVE";

    public void onReceive(Context context, Intent intent) {
        if (RECEIVE_ACTION.equals(intent.getAction())) {
            if (!AppCore.getInstance().isInitialized()) {
                AppCore.getInstance().init(context);
            }
            checkAndSendNotification(context, intent.getStringExtra(PAYLOAD));
        }
    }

    /* access modifiers changed from: private */
    public void sendNotification(Context context, String bodyMessage, RichPushNotification richNotification) {
        Context context2 = context;
        ((NotificationManager) context.getSystemService("notification")).notify(1, getNotification(context2, R.drawable.icon_icon, context.getString(R.string.push_notification_coming), bodyMessage, richNotification));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private Notification getNotification(Context context, int notificationIcon, String titleMessage, String bodyMessage, RichPushNotification richNotification) {
        Notification notification = new Notification(notificationIcon, titleMessage, System.currentTimeMillis());
        notification.flags |= 16;
        Intent notificationIntent = new Intent(context, MainController.class);
        notificationIntent.putExtra(CachingConstants.OPEN_MESSAGE_TAB_PROPERTY, true);
        if (richNotification != null && (StringUtils.isNotEmpty(richNotification.getTabId()) || StringUtils.isNotEmpty(richNotification.getUrl()))) {
            notificationIntent.putExtra(CachingConstants.RICH_PUSH_PROPERTY, richNotification);
        }
        notification.setLatestEventInfo(context, titleMessage, bodyMessage, PendingIntent.getActivity(context, 0, notificationIntent, 134217728));
        notification.sound = RingtoneManager.getDefaultUri(2);
        return notification;
    }

    private void checkAndSendNotification(Context context, String message) {
        final int detailsIndex = message.indexOf(AppConstants.DETAILS_SEPARATOR);
        if (detailsIndex != -1) {
            final String detailsText = message.substring(detailsIndex + 3);
            int idStartIndex = detailsText.indexOf("id");
            if (idStartIndex != -1) {
                final String id = detailsText.substring(idStartIndex + 3);
                final Context context2 = context;
                final String str = message;
                new AsyncTask<Void, Void, RichPushNotification>() {
                    /* access modifiers changed from: protected */
                    public RichPushNotification doInBackground(Void... params) {
                        return JsonParserUtils.parseRichNotification(DataSource.getInstance().getData(ServerConstants.PUSH_MESSAGE_DETAIL_URL + id));
                    }

                    /* access modifiers changed from: protected */
                    public void onPostExecute(RichPushNotification result) {
                        super.onPostExecute((Object) result);
                        if (detailsText.indexOf(AppConstants.LATITUDE_PARAM) != -1) {
                            C2DMMessagesReceiver.this.sendForSpecificArea(context2, str, str.substring(0, detailsIndex), result);
                        } else {
                            C2DMMessagesReceiver.this.sendNotification(context2, str.substring(0, detailsIndex), result);
                        }
                    }
                }.execute(new Void[0]);
            } else if (detailsText.indexOf(AppConstants.LATITUDE_PARAM) != -1) {
                sendForSpecificArea(context, message, message.substring(0, detailsIndex), null);
            }
        } else {
            sendNotification(context, message, null);
        }
    }

    /* access modifiers changed from: private */
    public void sendForSpecificArea(Context context, String detailsText, String messageToSend, RichPushNotification richNotification) {
        String longitude = richNotification.getLongitude();
        String latitude = richNotification.getLatitude();
        String radiusValue = richNotification.getRadius();
        System.out.println("!!!!!!!!!! radiusValue = " + radiusValue);
        System.out.println("!!!!!!!!!! longitude = " + longitude);
        System.out.println("!!!!!!!!!! latitude = " + latitude);
        if (!StringUtils.checkTextFieldsOnEmpty(longitude, latitude, radiusValue)) {
            double pointLat = Double.parseDouble(latitude);
            double pointLong = Double.parseDouble(longitude);
            double radius = Double.parseDouble(radiusValue);
            Location currentLocation = AppCore.getInstance().getLocationFinder().getCurrentLocation();
            if (currentLocation != null) {
                double currentLat = currentLocation.getLatitude();
                double currentLong = currentLocation.getLongitude();
                System.out.println("!!!!!!!!!! currentLat = " + currentLat);
                System.out.println("!!!!!!!!!! currentLong = " + currentLong);
                if (isInArea(pointLong, pointLat, currentLong, currentLat, radius)) {
                    sendNotification(context, messageToSend, richNotification);
                }
            }
        }
    }

    private static boolean isInArea(double userLong, double userLat, double pointLong, double pointLat, double r) {
        double userLong2 = (3.141592653589793d * userLong) / 180.0d;
        double pointLong2 = (3.141592653589793d * pointLong) / 180.0d;
        double d1 = (pointLong2 - userLong2) / 2.0d;
        double d2 = (((3.141592653589793d * pointLat) / 180.0d) - ((3.141592653589793d * userLat) / 180.0d)) / 2.0d;
        return (2.0d * Math.asin(Math.sqrt((Math.sin(d1) * Math.sin(d1)) + (((Math.cos(userLong2) * Math.cos(pointLong2)) * Math.sin(d2)) * Math.sin(d2))))) * 6367.4446571225d <= 2.0d + r;
    }
}
