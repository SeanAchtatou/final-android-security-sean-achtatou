package org.apache.http.client.protocol;

import com.biznessapps.constants.AppConstants;
import java.io.IOException;
import java.util.Locale;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.annotation.Immutable;
import org.apache.http.client.entity.DeflateDecompressingEntity;
import org.apache.http.client.entity.GzipDecompressingEntity;
import org.apache.http.protocol.HttpContext;

@Immutable
public class ResponseContentEncoding implements HttpResponseInterceptor {
    public void process(HttpResponse response, HttpContext context) throws HttpException, IOException {
        Header ceheader;
        HttpEntity entity = response.getEntity();
        if (entity != null && (ceheader = entity.getContentEncoding()) != null) {
            HeaderElement[] arr$ = ceheader.getElements();
            if (0 < arr$.length) {
                HeaderElement codec = arr$[0];
                String codecname = codec.getName().toLowerCase(Locale.US);
                if (AppConstants.GZIP.equals(codecname) || "x-gzip".equals(codecname)) {
                    response.setEntity(new GzipDecompressingEntity(response.getEntity()));
                } else if ("deflate".equals(codecname)) {
                    response.setEntity(new DeflateDecompressingEntity(response.getEntity()));
                } else if (!"identity".equals(codecname)) {
                    throw new HttpException("Unsupported Content-Coding: " + codec.getName());
                }
            }
        }
    }
}
