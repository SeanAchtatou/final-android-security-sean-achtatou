package org.apache.http.impl.cookie;

import org.apache.http.annotation.Immutable;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.CookieRestrictionViolationException;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.cookie.SetCookie;

@Immutable
public class RFC2109VersionHandler extends AbstractCookieAttributeHandler {
    /* JADX WARN: Type inference failed for: r1v2, types: [java.lang.Throwable, org.apache.http.cookie.MalformedCookieException] */
    /* JADX WARN: Type inference failed for: r1v4, types: [java.lang.Throwable, org.apache.http.cookie.MalformedCookieException] */
    /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Throwable, org.apache.http.cookie.MalformedCookieException] */
    public void parse(SetCookie cookie, String value) throws MalformedCookieException {
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (value == null) {
            throw new MalformedCookieException("Missing value for version attribute");
        } else if (value.trim().length() == 0) {
            throw new MalformedCookieException("Blank value for version attribute");
        } else {
            try {
                cookie.setVersion(Integer.parseInt(value));
            } catch (NumberFormatException e) {
                throw new MalformedCookieException("Invalid version: " + e.getMessage());
            }
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Throwable, org.apache.http.cookie.CookieRestrictionViolationException] */
    public void validate(Cookie cookie, CookieOrigin origin) throws MalformedCookieException {
        if (cookie == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (cookie.getVersion() < 0) {
            throw new CookieRestrictionViolationException("Cookie version may not be negative");
        }
    }
}
