package org.apache.http.impl.conn;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.annotation.GuardedBy;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ClientConnectionOperator;
import org.apache.http.conn.ClientConnectionRequest;
import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.routing.RouteTracker;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.params.HttpParams;

@ThreadSafe
public class SingleClientConnManager implements ClientConnectionManager {
    public static final String MISUSE_MESSAGE = "Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.";
    protected final boolean alwaysShutDown;
    protected final ClientConnectionOperator connOperator;
    @GuardedBy("this")
    protected long connectionExpiresTime;
    protected volatile boolean isShutDown;
    @GuardedBy("this")
    protected long lastReleaseTime;
    private final Log log;
    @GuardedBy("this")
    protected ConnAdapter managedConn;
    protected final SchemeRegistry schemeRegistry;
    @GuardedBy("this")
    protected PoolEntry uniquePoolEntry;

    @Deprecated
    public SingleClientConnManager(HttpParams params, SchemeRegistry schreg) {
        this(schreg);
    }

    public SingleClientConnManager(SchemeRegistry schreg) {
        this.log = LogFactory.getLog(getClass());
        if (schreg == null) {
            throw new IllegalArgumentException("Scheme registry must not be null.");
        }
        this.schemeRegistry = schreg;
        this.connOperator = createConnectionOperator(schreg);
        this.uniquePoolEntry = new PoolEntry();
        this.managedConn = null;
        this.lastReleaseTime = -1;
        this.alwaysShutDown = false;
        this.isShutDown = false;
    }

    public SingleClientConnManager() {
        this(SchemeRegistryFactory.createDefault());
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            shutdown();
        } finally {
            super.finalize();
        }
    }

    public SchemeRegistry getSchemeRegistry() {
        return this.schemeRegistry;
    }

    /* access modifiers changed from: protected */
    public ClientConnectionOperator createConnectionOperator(SchemeRegistry schreg) {
        return new DefaultClientConnectionOperator(schreg);
    }

    /* access modifiers changed from: protected */
    public final void assertStillUp() throws IllegalStateException {
        if (this.isShutDown) {
            throw new IllegalStateException("Manager is shut down.");
        }
    }

    public final ClientConnectionRequest requestConnection(final HttpRoute route, final Object state) {
        return new ClientConnectionRequest() {
            public void abortRequest() {
            }

            public ManagedClientConnection getConnection(long timeout, TimeUnit tunit) {
                return SingleClientConnManager.this.getConnection(route, state);
            }
        };
    }

    public synchronized ManagedClientConnection getConnection(HttpRoute route, Object state) {
        if (route == null) {
            throw new IllegalArgumentException("Route may not be null.");
        }
        assertStillUp();
        if (this.log.isDebugEnabled()) {
            this.log.debug("Get connection for route " + route);
        }
        if (this.managedConn != null) {
            throw new IllegalStateException(MISUSE_MESSAGE);
        }
        boolean recreate = false;
        boolean shutdown = false;
        closeExpiredConnections();
        if (this.uniquePoolEntry.connection.isOpen()) {
            RouteTracker tracker = this.uniquePoolEntry.tracker;
            if (tracker == null || !tracker.toRoute().equals(route)) {
                shutdown = true;
            } else {
                shutdown = false;
            }
        } else {
            recreate = true;
        }
        if (shutdown) {
            recreate = true;
            try {
                this.uniquePoolEntry.shutdown();
            } catch (IOException iox) {
                this.log.debug("Problem shutting down connection.", iox);
            }
        }
        if (recreate) {
            this.uniquePoolEntry = new PoolEntry();
        }
        this.managedConn = new ConnAdapter(this.uniquePoolEntry, route);
        return this.managedConn;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:35:0x0071=Splitter:B:35:0x0071, B:45:0x00a3=Splitter:B:45:0x00a3} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void releaseConnection(org.apache.http.conn.ManagedClientConnection r10, long r11, java.util.concurrent.TimeUnit r13) {
        /*
            r9 = this;
            r7 = 0
            monitor-enter(r9)
            r9.assertStillUp()     // Catch:{ all -> 0x0012 }
            boolean r4 = r10 instanceof org.apache.http.impl.conn.SingleClientConnManager.ConnAdapter     // Catch:{ all -> 0x0012 }
            if (r4 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r5 = "Connection class mismatch, connection not obtained from this manager."
            r4.<init>(r5)     // Catch:{ all -> 0x0012 }
            throw r4     // Catch:{ all -> 0x0012 }
        L_0x0012:
            r4 = move-exception
            monitor-exit(r9)
            throw r4
        L_0x0015:
            org.apache.commons.logging.Log r4 = r9.log     // Catch:{ all -> 0x0012 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ all -> 0x0012 }
            if (r4 == 0) goto L_0x0035
            org.apache.commons.logging.Log r4 = r9.log     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0012 }
            r5.<init>()     // Catch:{ all -> 0x0012 }
            java.lang.String r6 = "Releasing connection "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ all -> 0x0012 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0012 }
            r4.debug(r5)     // Catch:{ all -> 0x0012 }
        L_0x0035:
            r0 = r10
            org.apache.http.impl.conn.SingleClientConnManager$ConnAdapter r0 = (org.apache.http.impl.conn.SingleClientConnManager.ConnAdapter) r0     // Catch:{ all -> 0x0012 }
            r3 = r0
            org.apache.http.impl.conn.AbstractPoolEntry r4 = r3.poolEntry     // Catch:{ all -> 0x0012 }
            if (r4 != 0) goto L_0x003f
        L_0x003d:
            monitor-exit(r9)
            return
        L_0x003f:
            org.apache.http.conn.ClientConnectionManager r2 = r3.getManager()     // Catch:{ all -> 0x0012 }
            if (r2 == 0) goto L_0x004f
            if (r2 == r9) goto L_0x004f
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r5 = "Connection not obtained from this manager."
            r4.<init>(r5)     // Catch:{ all -> 0x0012 }
            throw r4     // Catch:{ all -> 0x0012 }
        L_0x004f:
            boolean r4 = r3.isOpen()     // Catch:{ IOException -> 0x0093 }
            if (r4 == 0) goto L_0x0071
            boolean r4 = r9.alwaysShutDown     // Catch:{ IOException -> 0x0093 }
            if (r4 != 0) goto L_0x005f
            boolean r4 = r3.isMarkedReusable()     // Catch:{ IOException -> 0x0093 }
            if (r4 != 0) goto L_0x0071
        L_0x005f:
            org.apache.commons.logging.Log r4 = r9.log     // Catch:{ IOException -> 0x0093 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ IOException -> 0x0093 }
            if (r4 == 0) goto L_0x006e
            org.apache.commons.logging.Log r4 = r9.log     // Catch:{ IOException -> 0x0093 }
            java.lang.String r5 = "Released connection open but not reusable."
            r4.debug(r5)     // Catch:{ IOException -> 0x0093 }
        L_0x006e:
            r3.shutdown()     // Catch:{ IOException -> 0x0093 }
        L_0x0071:
            r3.detach()     // Catch:{ all -> 0x0012 }
            r4 = 0
            r9.managedConn = r4     // Catch:{ all -> 0x0012 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r9.lastReleaseTime = r4     // Catch:{ all -> 0x0012 }
            int r4 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r4 <= 0) goto L_0x008b
            long r4 = r13.toMillis(r11)     // Catch:{ all -> 0x0012 }
            long r6 = r9.lastReleaseTime     // Catch:{ all -> 0x0012 }
            long r4 = r4 + r6
            r9.connectionExpiresTime = r4     // Catch:{ all -> 0x0012 }
            goto L_0x003d
        L_0x008b:
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r9.connectionExpiresTime = r4     // Catch:{ all -> 0x0012 }
            goto L_0x003d
        L_0x0093:
            r1 = move-exception
            org.apache.commons.logging.Log r4 = r9.log     // Catch:{ all -> 0x00c6 }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ all -> 0x00c6 }
            if (r4 == 0) goto L_0x00a3
            org.apache.commons.logging.Log r4 = r9.log     // Catch:{ all -> 0x00c6 }
            java.lang.String r5 = "Exception shutting down released connection."
            r4.debug(r5, r1)     // Catch:{ all -> 0x00c6 }
        L_0x00a3:
            r3.detach()     // Catch:{ all -> 0x0012 }
            r4 = 0
            r9.managedConn = r4     // Catch:{ all -> 0x0012 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r9.lastReleaseTime = r4     // Catch:{ all -> 0x0012 }
            int r4 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r4 <= 0) goto L_0x00bd
            long r4 = r13.toMillis(r11)     // Catch:{ all -> 0x0012 }
            long r6 = r9.lastReleaseTime     // Catch:{ all -> 0x0012 }
            long r4 = r4 + r6
            r9.connectionExpiresTime = r4     // Catch:{ all -> 0x0012 }
            goto L_0x003d
        L_0x00bd:
            r4 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r9.connectionExpiresTime = r4     // Catch:{ all -> 0x0012 }
            goto L_0x003d
        L_0x00c6:
            r4 = move-exception
            r3.detach()     // Catch:{ all -> 0x0012 }
            r5 = 0
            r9.managedConn = r5     // Catch:{ all -> 0x0012 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r9.lastReleaseTime = r5     // Catch:{ all -> 0x0012 }
            int r5 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r5 <= 0) goto L_0x00e1
            long r5 = r13.toMillis(r11)     // Catch:{ all -> 0x0012 }
            long r7 = r9.lastReleaseTime     // Catch:{ all -> 0x0012 }
            long r5 = r5 + r7
            r9.connectionExpiresTime = r5     // Catch:{ all -> 0x0012 }
        L_0x00e0:
            throw r4     // Catch:{ all -> 0x0012 }
        L_0x00e1:
            r5 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r9.connectionExpiresTime = r5     // Catch:{ all -> 0x0012 }
            goto L_0x00e0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.conn.SingleClientConnManager.releaseConnection(org.apache.http.conn.ManagedClientConnection, long, java.util.concurrent.TimeUnit):void");
    }

    public synchronized void closeExpiredConnections() {
        if (System.currentTimeMillis() >= this.connectionExpiresTime) {
            closeIdleConnections(0, TimeUnit.MILLISECONDS);
        }
    }

    public synchronized void closeIdleConnections(long idletime, TimeUnit tunit) {
        assertStillUp();
        if (tunit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        } else if (this.managedConn == null && this.uniquePoolEntry.connection.isOpen()) {
            if (this.lastReleaseTime <= System.currentTimeMillis() - tunit.toMillis(idletime)) {
                try {
                    this.uniquePoolEntry.close();
                } catch (IOException iox) {
                    this.log.debug("Problem closing idle connection.", iox);
                }
            }
        }
        return;
    }

    public synchronized void shutdown() {
        this.isShutDown = true;
        if (this.managedConn != null) {
            this.managedConn.detach();
        }
        try {
            if (this.uniquePoolEntry != null) {
                this.uniquePoolEntry.shutdown();
            }
            this.uniquePoolEntry = null;
        } catch (IOException iox) {
            this.log.debug("Problem while shutting down manager.", iox);
            this.uniquePoolEntry = null;
        } catch (Throwable th) {
            this.uniquePoolEntry = null;
            throw th;
        }
        return;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public synchronized void revokeConnection() {
        if (this.managedConn != null) {
            this.managedConn.detach();
            try {
                this.uniquePoolEntry.shutdown();
            } catch (IOException iox) {
                this.log.debug("Problem while shutting down connection.", iox);
            }
        }
        return;
    }

    protected class PoolEntry extends AbstractPoolEntry {
        protected PoolEntry() {
            super(SingleClientConnManager.this.connOperator, null);
        }

        /* access modifiers changed from: protected */
        public void close() throws IOException {
            shutdownEntry();
            if (this.connection.isOpen()) {
                this.connection.close();
            }
        }

        /* access modifiers changed from: protected */
        public void shutdown() throws IOException {
            shutdownEntry();
            if (this.connection.isOpen()) {
                this.connection.shutdown();
            }
        }
    }

    protected class ConnAdapter extends AbstractPooledConnAdapter {
        protected ConnAdapter(PoolEntry entry, HttpRoute route) {
            super(SingleClientConnManager.this, entry);
            markReusable();
            entry.route = route;
        }
    }
}
