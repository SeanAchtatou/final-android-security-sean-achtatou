package org.apache.http.impl.conn;

import java.io.IOException;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpHost;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.conn.ClientConnectionOperator;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.conn.OperatedClientConnection;
import org.apache.http.conn.scheme.LayeredSchemeSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

@ThreadSafe
public class DefaultClientConnectionOperator implements ClientConnectionOperator {
    private final Log log = LogFactory.getLog(getClass());
    protected final SchemeRegistry schemeRegistry;

    public DefaultClientConnectionOperator(SchemeRegistry schemes) {
        if (schemes == null) {
            throw new IllegalArgumentException("Scheme registry amy not be null");
        }
        this.schemeRegistry = schemes;
    }

    public OperatedClientConnection createConnection() {
        return new DefaultClientConnection();
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0107 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void openConnection(org.apache.http.conn.OperatedClientConnection r19, org.apache.http.HttpHost r20, java.net.InetAddress r21, org.apache.http.protocol.HttpContext r22, org.apache.http.params.HttpParams r23) throws java.io.IOException {
        /*
            r18 = this;
            if (r19 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r15 = new java.lang.IllegalArgumentException
            java.lang.String r16 = "Connection may not be null"
            r15.<init>(r16)
            throw r15
        L_0x000a:
            if (r20 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r15 = new java.lang.IllegalArgumentException
            java.lang.String r16 = "Target host may not be null"
            r15.<init>(r16)
            throw r15
        L_0x0014:
            if (r23 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r15 = new java.lang.IllegalArgumentException
            java.lang.String r16 = "Parameters may not be null"
            r15.<init>(r16)
            throw r15
        L_0x001e:
            boolean r15 = r19.isOpen()
            if (r15 == 0) goto L_0x002c
            java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
            java.lang.String r16 = "Connection must not be open"
            r15.<init>(r16)
            throw r15
        L_0x002c:
            r0 = r18
            org.apache.http.conn.scheme.SchemeRegistry r15 = r0.schemeRegistry
            java.lang.String r16 = r20.getSchemeName()
            org.apache.http.conn.scheme.Scheme r12 = r15.getScheme(r16)
            org.apache.http.conn.scheme.SchemeSocketFactory r13 = r12.getSchemeSocketFactory()
            java.lang.String r15 = r20.getHostName()
            r0 = r18
            java.net.InetAddress[] r4 = r0.resolveHostname(r15)
            int r15 = r20.getPort()
            int r10 = r12.resolvePort(r15)
            r7 = 0
        L_0x004f:
            int r15 = r4.length
            if (r7 >= r15) goto L_0x00c3
            r3 = r4[r7]
            int r15 = r4.length
            int r15 = r15 + -1
            if (r7 != r15) goto L_0x00c4
            r8 = 1
        L_0x005a:
            r0 = r23
            java.net.Socket r14 = r13.createSocket(r0)
            r0 = r19
            r1 = r20
            r0.opening(r14, r1)
            org.apache.http.impl.conn.HttpInetSocketAddress r11 = new org.apache.http.impl.conn.HttpInetSocketAddress
            r0 = r20
            r11.<init>(r0, r3, r10)
            r9 = 0
            if (r21 == 0) goto L_0x0079
            java.net.InetSocketAddress r9 = new java.net.InetSocketAddress
            r15 = 0
            r0 = r21
            r9.<init>(r0, r15)
        L_0x0079:
            r0 = r18
            org.apache.commons.logging.Log r15 = r0.log
            boolean r15 = r15.isDebugEnabled()
            if (r15 == 0) goto L_0x009f
            r0 = r18
            org.apache.commons.logging.Log r15 = r0.log
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            r16.<init>()
            java.lang.String r17 = "Connecting to "
            java.lang.StringBuilder r16 = r16.append(r17)
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r11)
            java.lang.String r16 = r16.toString()
            r15.debug(r16)
        L_0x009f:
            r0 = r23
            java.net.Socket r5 = r13.connectSocket(r14, r11, r9, r0)     // Catch:{ ConnectException -> 0x00c6, ConnectTimeoutException -> 0x00d1 }
            if (r14 == r5) goto L_0x00af
            r14 = r5
            r0 = r19
            r1 = r20
            r0.opening(r14, r1)     // Catch:{ ConnectException -> 0x00c6, ConnectTimeoutException -> 0x00d1 }
        L_0x00af:
            r0 = r18
            r1 = r22
            r2 = r23
            r0.prepareSocket(r14, r1, r2)     // Catch:{ ConnectException -> 0x00c6, ConnectTimeoutException -> 0x00d1 }
            boolean r15 = r13.isSecure(r14)     // Catch:{ ConnectException -> 0x00c6, ConnectTimeoutException -> 0x00d1 }
            r0 = r19
            r1 = r23
            r0.openCompleted(r15, r1)     // Catch:{ ConnectException -> 0x00c6, ConnectTimeoutException -> 0x00d1 }
        L_0x00c3:
            return
        L_0x00c4:
            r8 = 0
            goto L_0x005a
        L_0x00c6:
            r6 = move-exception
            if (r8 == 0) goto L_0x00d5
            org.apache.http.conn.HttpHostConnectException r15 = new org.apache.http.conn.HttpHostConnectException
            r0 = r20
            r15.<init>(r0, r6)
            throw r15
        L_0x00d1:
            r6 = move-exception
            if (r8 == 0) goto L_0x00d5
            throw r6
        L_0x00d5:
            r0 = r18
            org.apache.commons.logging.Log r15 = r0.log
            boolean r15 = r15.isDebugEnabled()
            if (r15 == 0) goto L_0x0107
            r0 = r18
            org.apache.commons.logging.Log r15 = r0.log
            java.lang.StringBuilder r16 = new java.lang.StringBuilder
            r16.<init>()
            java.lang.String r17 = "Connect to "
            java.lang.StringBuilder r16 = r16.append(r17)
            r0 = r16
            java.lang.StringBuilder r16 = r0.append(r11)
            java.lang.String r17 = " timed out. "
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r17 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r16 = r16.append(r17)
            java.lang.String r16 = r16.toString()
            r15.debug(r16)
        L_0x0107:
            int r7 = r7 + 1
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.conn.DefaultClientConnectionOperator.openConnection(org.apache.http.conn.OperatedClientConnection, org.apache.http.HttpHost, java.net.InetAddress, org.apache.http.protocol.HttpContext, org.apache.http.params.HttpParams):void");
    }

    public void updateSecureConnection(OperatedClientConnection conn, HttpHost target, HttpContext context, HttpParams params) throws IOException {
        if (conn == null) {
            throw new IllegalArgumentException("Connection may not be null");
        } else if (target == null) {
            throw new IllegalArgumentException("Target host may not be null");
        } else if (params == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        } else if (!conn.isOpen()) {
            throw new IllegalStateException("Connection must be open");
        } else {
            Scheme schm = this.schemeRegistry.getScheme(target.getSchemeName());
            if (!(schm.getSchemeSocketFactory() instanceof LayeredSchemeSocketFactory)) {
                throw new IllegalArgumentException("Target scheme (" + schm.getName() + ") must have layered socket factory.");
            }
            LayeredSchemeSocketFactory lsf = (LayeredSchemeSocketFactory) schm.getSchemeSocketFactory();
            try {
                Socket sock = lsf.createLayeredSocket(conn.getSocket(), target.getHostName(), target.getPort(), true);
                prepareSocket(sock, context, params);
                conn.update(sock, target, lsf.isSecure(sock), params);
            } catch (ConnectException ex) {
                throw new HttpHostConnectException(target, ex);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void prepareSocket(Socket sock, HttpContext context, HttpParams params) throws IOException {
        sock.setTcpNoDelay(HttpConnectionParams.getTcpNoDelay(params));
        sock.setSoTimeout(HttpConnectionParams.getSoTimeout(params));
        int linger = HttpConnectionParams.getLinger(params);
        if (linger >= 0) {
            sock.setSoLinger(linger > 0, linger);
        }
    }

    /* access modifiers changed from: protected */
    public InetAddress[] resolveHostname(String host) throws UnknownHostException {
        return InetAddress.getAllByName(host);
    }
}
