package org.apache.http.impl.conn.tsccm;

import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.annotation.ThreadSafe;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ClientConnectionOperator;
import org.apache.http.conn.ClientConnectionRequest;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.conn.ManagedClientConnection;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.conn.DefaultClientConnectionOperator;
import org.apache.http.impl.conn.SchemeRegistryFactory;
import org.apache.http.params.HttpParams;

@ThreadSafe
public class ThreadSafeClientConnManager implements ClientConnectionManager {
    protected final ClientConnectionOperator connOperator;
    protected final ConnPerRouteBean connPerRoute;
    @Deprecated
    protected final AbstractConnPool connectionPool;
    /* access modifiers changed from: private */
    public final Log log;
    protected final ConnPoolByRoute pool;
    protected final SchemeRegistry schemeRegistry;

    public ThreadSafeClientConnManager(SchemeRegistry schreg) {
        this(schreg, -1, TimeUnit.MILLISECONDS);
    }

    public ThreadSafeClientConnManager() {
        this(SchemeRegistryFactory.createDefault());
    }

    public ThreadSafeClientConnManager(SchemeRegistry schreg, long connTTL, TimeUnit connTTLTimeUnit) {
        if (schreg == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        }
        this.log = LogFactory.getLog(getClass());
        this.schemeRegistry = schreg;
        this.connPerRoute = new ConnPerRouteBean();
        this.connOperator = createConnectionOperator(schreg);
        this.pool = createConnectionPool(connTTL, connTTLTimeUnit);
        this.connectionPool = this.pool;
    }

    @Deprecated
    public ThreadSafeClientConnManager(HttpParams params, SchemeRegistry schreg) {
        if (schreg == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        }
        this.log = LogFactory.getLog(getClass());
        this.schemeRegistry = schreg;
        this.connPerRoute = new ConnPerRouteBean();
        this.connOperator = createConnectionOperator(schreg);
        this.pool = (ConnPoolByRoute) createConnectionPool(params);
        this.connectionPool = this.pool;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        try {
            shutdown();
        } finally {
            super.finalize();
        }
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public AbstractConnPool createConnectionPool(HttpParams params) {
        return new ConnPoolByRoute(this.connOperator, params);
    }

    /* access modifiers changed from: protected */
    public ConnPoolByRoute createConnectionPool(long connTTL, TimeUnit connTTLTimeUnit) {
        return new ConnPoolByRoute(this.connOperator, this.connPerRoute, 20, connTTL, connTTLTimeUnit);
    }

    /* access modifiers changed from: protected */
    public ClientConnectionOperator createConnectionOperator(SchemeRegistry schreg) {
        return new DefaultClientConnectionOperator(schreg);
    }

    public SchemeRegistry getSchemeRegistry() {
        return this.schemeRegistry;
    }

    public ClientConnectionRequest requestConnection(final HttpRoute route, Object state) {
        final PoolEntryRequest poolRequest = this.pool.requestPoolEntry(route, state);
        return new ClientConnectionRequest() {
            public void abortRequest() {
                poolRequest.abortRequest();
            }

            public ManagedClientConnection getConnection(long timeout, TimeUnit tunit) throws InterruptedException, ConnectionPoolTimeoutException {
                if (route == null) {
                    throw new IllegalArgumentException("Route may not be null.");
                }
                if (ThreadSafeClientConnManager.this.log.isDebugEnabled()) {
                    ThreadSafeClientConnManager.this.log.debug("Get connection: " + route + ", timeout = " + timeout);
                }
                return new BasicPooledConnAdapter(ThreadSafeClientConnManager.this, poolRequest.getPoolEntry(timeout, tunit));
            }
        };
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x0079=Splitter:B:39:0x0079, B:21:0x003d=Splitter:B:21:0x003d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void releaseConnection(org.apache.http.conn.ManagedClientConnection r10, long r11, java.util.concurrent.TimeUnit r13) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof org.apache.http.impl.conn.tsccm.BasicPooledConnAdapter
            if (r0 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r3)
            throw r0
        L_0x000c:
            r6 = r10
            org.apache.http.impl.conn.tsccm.BasicPooledConnAdapter r6 = (org.apache.http.impl.conn.tsccm.BasicPooledConnAdapter) r6
            org.apache.http.impl.conn.AbstractPoolEntry r0 = r6.getPoolEntry()
            if (r0 == 0) goto L_0x0023
            org.apache.http.conn.ClientConnectionManager r0 = r6.getManager()
            if (r0 == r9) goto L_0x0023
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Connection not obtained from this manager."
            r0.<init>(r3)
            throw r0
        L_0x0023:
            monitor-enter(r6)
            org.apache.http.impl.conn.AbstractPoolEntry r1 = r6.getPoolEntry()     // Catch:{ all -> 0x005e }
            org.apache.http.impl.conn.tsccm.BasicPoolEntry r1 = (org.apache.http.impl.conn.tsccm.BasicPoolEntry) r1     // Catch:{ all -> 0x005e }
            if (r1 != 0) goto L_0x002e
            monitor-exit(r6)     // Catch:{ all -> 0x005e }
        L_0x002d:
            return
        L_0x002e:
            boolean r0 = r6.isOpen()     // Catch:{ IOException -> 0x0069 }
            if (r0 == 0) goto L_0x003d
            boolean r0 = r6.isMarkedReusable()     // Catch:{ IOException -> 0x0069 }
            if (r0 != 0) goto L_0x003d
            r6.shutdown()     // Catch:{ IOException -> 0x0069 }
        L_0x003d:
            boolean r2 = r6.isMarkedReusable()     // Catch:{ all -> 0x005e }
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x0052
            if (r2 == 0) goto L_0x0061
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005e }
        L_0x0052:
            r6.detach()     // Catch:{ all -> 0x005e }
            org.apache.http.impl.conn.tsccm.ConnPoolByRoute r0 = r9.pool     // Catch:{ all -> 0x005e }
            r3 = r11
            r5 = r13
            r0.freeEntry(r1, r2, r3, r5)     // Catch:{ all -> 0x005e }
        L_0x005c:
            monitor-exit(r6)     // Catch:{ all -> 0x005e }
            goto L_0x002d
        L_0x005e:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x005e }
            throw r0
        L_0x0061:
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005e }
            goto L_0x0052
        L_0x0069:
            r7 = move-exception
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x00a1 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x00a1 }
            if (r0 == 0) goto L_0x0079
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x00a1 }
            java.lang.String r3 = "Exception shutting down released connection."
            r0.debug(r3, r7)     // Catch:{ all -> 0x00a1 }
        L_0x0079:
            boolean r2 = r6.isMarkedReusable()     // Catch:{ all -> 0x005e }
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x008e
            if (r2 == 0) goto L_0x0099
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005e }
        L_0x008e:
            r6.detach()     // Catch:{ all -> 0x005e }
            org.apache.http.impl.conn.tsccm.ConnPoolByRoute r0 = r9.pool     // Catch:{ all -> 0x005e }
            r3 = r11
            r5 = r13
            r0.freeEntry(r1, r2, r3, r5)     // Catch:{ all -> 0x005e }
            goto L_0x005c
        L_0x0099:
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005e }
            goto L_0x008e
        L_0x00a1:
            r0 = move-exception
            r8 = r0
            boolean r2 = r6.isMarkedReusable()     // Catch:{ all -> 0x005e }
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005e }
            if (r0 == 0) goto L_0x00b8
            if (r2 == 0) goto L_0x00c3
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005e }
        L_0x00b8:
            r6.detach()     // Catch:{ all -> 0x005e }
            org.apache.http.impl.conn.tsccm.ConnPoolByRoute r0 = r9.pool     // Catch:{ all -> 0x005e }
            r3 = r11
            r5 = r13
            r0.freeEntry(r1, r2, r3, r5)     // Catch:{ all -> 0x005e }
            throw r8     // Catch:{ all -> 0x005e }
        L_0x00c3:
            org.apache.commons.logging.Log r0 = r9.log     // Catch:{ all -> 0x005e }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005e }
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager.releaseConnection(org.apache.http.conn.ManagedClientConnection, long, java.util.concurrent.TimeUnit):void");
    }

    public void shutdown() {
        this.log.debug("Shutting down");
        this.pool.shutdown();
    }

    public int getConnectionsInPool(HttpRoute route) {
        return this.pool.getConnectionsInPool(route);
    }

    public int getConnectionsInPool() {
        return this.pool.getConnectionsInPool();
    }

    public void closeIdleConnections(long idleTimeout, TimeUnit tunit) {
        if (this.log.isDebugEnabled()) {
            this.log.debug("Closing connections idle longer than " + idleTimeout + " " + tunit);
        }
        this.pool.closeIdleConnections(idleTimeout, tunit);
    }

    public void closeExpiredConnections() {
        this.log.debug("Closing expired connections");
        this.pool.closeExpiredConnections();
    }

    public int getMaxTotal() {
        return this.pool.getMaxTotalConnections();
    }

    public void setMaxTotal(int max) {
        this.pool.setMaxTotalConnections(max);
    }

    public int getDefaultMaxPerRoute() {
        return this.connPerRoute.getDefaultMaxPerRoute();
    }

    public void setDefaultMaxPerRoute(int max) {
        this.connPerRoute.setDefaultMaxPerRoute(max);
    }

    public int getMaxForRoute(HttpRoute route) {
        return this.connPerRoute.getMaxForRoute(route);
    }

    public void setMaxForRoute(HttpRoute route, int max) {
        this.connPerRoute.setMaxForRoute(route, max);
    }
}
