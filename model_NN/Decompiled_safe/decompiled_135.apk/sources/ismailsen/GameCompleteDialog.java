package ismailsen;

import android.app.Dialog;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import org.example.isudoku.Game;
import org.example.isudoku.R;

public class GameCompleteDialog extends Dialog {
    private Button finished = ((Button) findViewById(R.id.finish_button));
    private EditText name = ((EditText) findViewById(R.id.name_input));

    public GameCompleteDialog(Context context) {
        super(context);
        setContentView((int) R.layout.alertdialog);
        setTitle((int) R.string.game_correct_title);
        this.finished.setOnClickListener((Game) context);
    }

    public String getName() {
        return this.name.getText().toString();
    }
}
