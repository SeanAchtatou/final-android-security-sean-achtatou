package ismailsen;

import java.util.Random;
import org.example.isudoku.Game;

public class Generator {
    private int howManyNumbers;
    private String puzzle;

    public Generator(int diff) {
        switch (diff) {
            case Game.DIFFICULTY_EASY /*0*/:
                this.howManyNumbers = 34;
                return;
            case Game.DIFFICULTY_MEDIUM /*1*/:
                this.howManyNumbers = 24;
                return;
            case Game.DIFFICULTY_HARD /*2*/:
                this.howManyNumbers = 14;
                return;
            default:
                this.howManyNumbers = 10;
                return;
        }
    }

    public String generatePuzzleString() {
        createFirstRow();
        try {
            this.puzzle = new Solver(this.puzzle).calculate();
            createBlankEntries();
        } catch (InvalidSudokuPuzzleException e) {
            generatePuzzleString();
        }
        return this.puzzle;
    }

    private void createBlankEntries() {
        int index;
        Random rand = new Random();
        char[] grid = this.puzzle.toCharArray();
        for (int i = 0; i < 81 - this.howManyNumbers; i++) {
            do {
                index = rand.nextInt(81);
            } while (grid[index] == '0');
            grid[index] = '0';
        }
        this.puzzle = new String(grid);
    }

    private void createFirstRow() {
        int val;
        StringBuilder puz = new StringBuilder();
        Random rand = new Random();
        boolean[] b = new boolean[9];
        for (int i = 0; i < 9; i++) {
            do {
                val = rand.nextInt(9);
            } while (b[val]);
            puz.append(val + 1);
            b[val] = true;
        }
        for (int i2 = 0; i2 < 73; i2++) {
            puz.append(0);
        }
        this.puzzle = puz.toString();
    }
}
