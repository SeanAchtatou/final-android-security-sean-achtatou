package ismailsen;

import android.app.Activity;
import android.os.Bundle;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.example.isudoku.Keypad;
import org.example.isudoku.PuzzleView;

public class GameBase extends Activity {
    protected ArrayList<int[]> puzzle;
    protected int puzzleIndex = 0;
    protected PuzzleView puzzleView;
    protected int[][] result;
    protected final int[][][] used = ((int[][][]) Array.newInstance(int[].class, 9, 9));

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.puzzle = new ArrayList<>();
        this.puzzle.add(new int[81]);
        for (int i = 0; i < 81; i++) {
            this.puzzle.get(this.puzzleIndex)[i] = 0;
        }
        initResult();
    }

    public String getTileString(int x, int y, int puzzleID) {
        setPuzzleIndex(puzzleID);
        int v = getTile(x, y);
        if (v == 0) {
            return "";
        }
        return String.valueOf(v);
    }

    public void showKeypad() {
        new Keypad(this, this.puzzleView).show();
    }

    public void showKeypad(int x, int y) {
        new Keypad(this, getUsedTiles(x, y), this.puzzleView).show();
    }

    public void setTileIfValid(int x, int y, int value) {
        boolean set = true;
        int[] tiles = getUsedTiles(x, y);
        if (!(value == 0 || tiles == null)) {
            for (int tile : tiles) {
                if (tile == value) {
                    set = false;
                }
                this.result[x][y] = 0;
            }
        }
        setTile(x, y, value);
        calculateUsedTiles();
        if (set) {
            this.result[x][y] = 1;
        }
    }

    /* access modifiers changed from: protected */
    public int getTile(int x, int y) {
        return this.puzzle.get(this.puzzleIndex)[(y * 9) + x];
    }

    public int[] getUsedTiles(int x, int y) {
        return this.used[x][y];
    }

    /* access modifiers changed from: protected */
    public void setTile(int x, int y, int value) {
        this.puzzle.get(this.puzzleIndex)[(y * 9) + x] = value;
    }

    /* access modifiers changed from: protected */
    public void calculateUsedTiles() {
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                this.used[x][y] = calculateUsedTiles(x, y);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int[] calculateUsedTiles(int x, int y) {
        int nused;
        int t;
        int t2;
        int t3;
        int[] c = new int[9];
        for (int i = 0; i < 9; i++) {
            if (!(i == x || (t3 = getTile(i, y)) == 0)) {
                c[t3 - 1] = t3;
            }
        }
        for (int i2 = 0; i2 < 9; i2++) {
            if (!(i2 == y || (t2 = getTile(x, i2)) == 0)) {
                c[t2 - 1] = t2;
            }
        }
        int startx = (x / 3) * 3;
        int starty = (y / 3) * 3;
        for (int i3 = startx; i3 < startx + 3; i3++) {
            for (int j = starty; j < starty + 3; j++) {
                if (!((i3 == x && j == y) || (t = getTile(i3, j)) == 0)) {
                    c[t - 1] = t;
                }
            }
        }
        int nused2 = 0;
        for (int t4 : c) {
            if (t4 != 0) {
                nused2++;
            }
        }
        int[] c1 = new int[nused2];
        int length = c.length;
        int i4 = 0;
        int nused3 = 0;
        while (i4 < length) {
            int t5 = c[i4];
            if (t5 != 0) {
                nused = nused3 + 1;
                c1[nused3] = t5;
            } else {
                nused = nused3;
            }
            i4++;
            nused3 = nused;
        }
        return c1;
    }

    private void initResult() {
        this.result = (int[][]) Array.newInstance(Integer.TYPE, 9, 9);
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                this.result[x][y] = 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void setPuzzleIndex(int index) {
        this.puzzleIndex = index;
    }

    public int addPuzzle(String nPuzzle) {
        int[] puz = fromPuzzleString(nPuzzle);
        if (this.puzzle.add(puz)) {
            return this.puzzle.indexOf(puz);
        }
        return -1;
    }

    public int[] fromPuzzleString(String string) {
        int[] puz = new int[string.length()];
        for (int i = 0; i < puz.length; i++) {
            puz[i] = string.charAt(i) - '0';
        }
        return puz;
    }

    public String puzzleToString() {
        StringBuilder str = new StringBuilder();
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                str.append(String.valueOf(this.puzzle.get(this.puzzleIndex)[(x * 9) + y]));
            }
        }
        return str.toString();
    }
}
