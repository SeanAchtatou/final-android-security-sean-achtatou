package ismailsen.SQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class GameManagerDataHelper {
    private static final String DATABASE_NAME = "manager.db";
    private static final int DATABASE_VERSION = 1;
    private static final String INSERT = "insert into tGameManager(name,puzzle) values (?1,?2)";
    private static final String TABLE_NAME = "tGameManager";
    private Context context;
    private SQLiteDatabase db = new OpenHelper(this.context).getWritableDatabase();
    private SQLiteStatement insertStmt = this.db.compileStatement(INSERT);

    public GameManagerDataHelper(Context context2) {
        this.context = context2;
    }

    public long insert(String name, String puzzle) {
        this.insertStmt.bindString(1, name);
        this.insertStmt.bindString(2, puzzle);
        return this.insertStmt.executeInsert();
    }

    public void delete(int id) {
        this.db.delete(TABLE_NAME, "gameid=" + id, null);
    }

    public void deleteAll() {
        this.db.delete(TABLE_NAME, null, null);
    }

    private static class OpenHelper extends SQLiteOpenHelper {
        OpenHelper(Context context) {
            super(context, GameManagerDataHelper.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE  TABLE tGameManager (gameid INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , name TEXT, puzzle TEXT)");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("Example", "Upgrading database, this will drop tables and recreate.");
            db.execSQL("DROP TABLE IF EXISTS tGameManager");
            onCreate(db);
        }
    }

    public Cursor getCursor() {
        return this.db.query(TABLE_NAME, new String[]{"gameid", "name", "puzzle"}, null, null, null, null, "name");
    }
}
