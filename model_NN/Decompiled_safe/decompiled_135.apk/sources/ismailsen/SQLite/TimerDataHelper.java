package ismailsen.SQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class TimerDataHelper {
    private static final String DATABASE_NAME = "app.db";
    private static final int DATABASE_VERSION = 1;
    private static final String INSERT = "insert into tTimer(name,mode,time) values (?1,?2,?3)";
    private static final String TABLE_NAME = "tTimer";
    private Context context;
    private SQLiteDatabase db = new OpenHelper(this.context).getWritableDatabase();
    private SQLiteStatement insertStmt = this.db.compileStatement(INSERT);

    public TimerDataHelper(Context context2) {
        this.context = context2;
    }

    public long insert(String name, String mode, String time) {
        this.insertStmt.bindString(1, name);
        this.insertStmt.bindString(2, mode);
        this.insertStmt.bindString(3, time);
        return this.insertStmt.executeInsert();
    }

    public void deleteAll() {
        this.db.delete(TABLE_NAME, null, null);
    }

    private static class OpenHelper extends SQLiteOpenHelper {
        OpenHelper(Context context) {
            super(context, TimerDataHelper.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 1);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE  TABLE tTimer (timerid INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , name TEXT, mode TEXT, time TEXT)");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w("Example", "Upgrading database, this will drop tables and recreate.");
            db.execSQL("DROP TABLE IF EXISTS tTimer");
            onCreate(db);
        }
    }

    public Cursor getCursor() {
        return this.db.query(TABLE_NAME, new String[]{"name", "mode", "time"}, null, null, null, null, "time");
    }
}
