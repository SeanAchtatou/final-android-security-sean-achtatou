package ismailsen;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import org.example.isudoku.Game;
import org.example.isudoku.PuzzleView;

public class History implements View.OnClickListener {
    private final Game context;
    private final LinearLayout hint_history;
    private Queue<HistoryEntry> history = new ArrayBlockingQueue(10);
    private final PuzzleView puzzle;

    private class HistoryEntry {
        public Button butt;
        public int x;
        public int y;

        private HistoryEntry() {
        }

        /* synthetic */ HistoryEntry(History history, HistoryEntry historyEntry) {
            this();
        }
    }

    public History(View linearLayout, Game context2, PuzzleView puzzle2) {
        this.hint_history = (LinearLayout) linearLayout;
        this.context = context2;
        this.puzzle = puzzle2;
    }

    public void newMove(int x, int y, int val) {
        HistoryEntry arg = new HistoryEntry(this, null);
        arg.x = x;
        arg.y = y;
        Button butt = new Button(this.context);
        butt.setText(Integer.toString(val));
        butt.setOnClickListener(this);
        arg.butt = butt;
        if (!this.history.offer(arg)) {
            this.history.poll();
            this.history.offer(arg);
        }
        updateView();
    }

    private void updateView() {
        this.hint_history.removeAllViews();
        for (HistoryEntry val : this.history) {
            this.hint_history.addView(val.butt);
        }
    }

    public void toggleHintBar() {
        if (this.hint_history.getVisibility() == 0) {
            this.hint_history.setVisibility(8);
        } else {
            this.hint_history.setVisibility(0);
        }
    }

    public void onClick(View v) {
        for (HistoryEntry val : this.history) {
            if (v == val.butt) {
                this.puzzle.select(val.x, val.y);
            }
        }
    }
}
