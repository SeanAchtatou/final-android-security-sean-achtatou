package ismailsen;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreBoardTableRow extends LinearLayout {
    private TextView mode;
    private TextView name;
    private TextView time;

    public ScoreBoardTableRow(Context context, String name2, String mode2, String time2) {
        super(context);
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        setGravity(16);
        this.name = new TextView(context);
        this.name.setText(name2);
        this.mode = new TextView(context);
        this.mode.setText(mode2);
        this.time = new TextView(context);
        this.time.setText(time2);
        addView(this.name);
        addView(this.mode);
        addView(this.time);
    }
}
