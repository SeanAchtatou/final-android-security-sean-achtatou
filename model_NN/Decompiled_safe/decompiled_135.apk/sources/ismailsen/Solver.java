package ismailsen;

import java.lang.reflect.Array;

public class Solver {
    private int[][] graph;

    public Solver(String puzzle) {
        initGraph(puzzle);
    }

    private void initGraph(String puzzle) {
        this.graph = (int[][]) Array.newInstance(Integer.TYPE, 9, 9);
        int y = 0;
        for (int i = 0; i < 81; i++) {
            int x = i % 9;
            if (i != 0 && x == 0) {
                y++;
            }
            this.graph[y][x] = Integer.valueOf(String.valueOf(puzzle.charAt(i))).intValue();
        }
    }

    private void solve(int row, int col) throws NullPointerException, SolvedSudokuPuzzleException {
        if (row > 8) {
            throw new SolvedSudokuPuzzleException();
        } else if (this.graph[row][col] != 0) {
            next(row, col);
        } else {
            for (int num = 1; num < 10; num++) {
                if (checkRow(row, num) && checkCol(col, num) && checkBox(row, col, num)) {
                    this.graph[row][col] = num;
                    next(row, col);
                }
            }
            this.graph[row][col] = 0;
        }
    }

    private boolean checkRow(int row, int num) {
        for (int col = 0; col < 9; col++) {
            if (this.graph[row][col] == num) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol(int col, int num) {
        for (int row = 0; row < 9; row++) {
            if (this.graph[row][col] == num) {
                return false;
            }
        }
        return true;
    }

    private boolean checkBox(int row, int col, int num) {
        int row2 = (row / 3) * 3;
        int col2 = (col / 3) * 3;
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                if (this.graph[row2 + r][col2 + c] == num) {
                    return false;
                }
            }
        }
        return true;
    }

    private void next(int row, int col) throws NullPointerException, SolvedSudokuPuzzleException {
        if (col < 8) {
            solve(row, col + 1);
        } else {
            solve(row + 1, 0);
        }
    }

    public String calculate() throws InvalidSudokuPuzzleException {
        try {
            solve(0, 0);
            throw new InvalidSudokuPuzzleException();
        } catch (NullPointerException e) {
            throw new InvalidSudokuPuzzleException();
        } catch (SolvedSudokuPuzzleException e2) {
            return gridToString();
        }
    }

    private String gridToString() {
        StringBuilder str = new StringBuilder();
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                str.append(String.valueOf(this.graph[y][x]));
            }
        }
        return str.toString();
    }
}
