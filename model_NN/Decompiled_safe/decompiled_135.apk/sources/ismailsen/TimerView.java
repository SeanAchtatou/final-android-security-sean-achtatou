package ismailsen;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.format.Time;
import android.util.AttributeSet;
import android.widget.TextView;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TimerView extends TextView {
    private Time gameStart = new Time();
    /* access modifiers changed from: private */
    public UpdateGuiHandler guiHandler;
    private ScheduledExecutorService taskScheduler;

    public TimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.gameStart.setToNow();
        this.guiHandler = new UpdateGuiHandler(this, null);
        this.taskScheduler = Executors.newSingleThreadScheduledExecutor();
        this.taskScheduler.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                TimerView.this.guiHandler.sendEmptyMessage(0);
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    public void stopTimer() {
        this.taskScheduler.shutdown();
    }

    private class UpdateGuiHandler extends Handler {
        private UpdateGuiHandler() {
        }

        /* synthetic */ UpdateGuiHandler(TimerView timerView, UpdateGuiHandler updateGuiHandler) {
            this();
        }

        public void handleMessage(Message msg) {
            TimerView.this.updateGui();
        }
    }

    /* access modifiers changed from: private */
    public void updateGui() {
        Time gameEnd = new Time();
        gameEnd.setToNow();
        int secs = (int) ((gameEnd.toMillis(true) - this.gameStart.toMillis(true)) / 1000);
        int mins = secs / 60;
        if (mins > 0) {
            secs -= mins * 60;
        }
        setText(String.valueOf(mins < 10 ? "0" + Integer.toString(mins) : Integer.toString(mins)) + ":" + (secs < 10 ? "0" + Integer.toString(secs) : Integer.toString(secs)));
    }
}
