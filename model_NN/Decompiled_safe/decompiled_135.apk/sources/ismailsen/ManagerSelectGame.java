package ismailsen;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import ismailsen.SQLite.GameManagerDataHelper;
import org.example.isudoku.Game;
import org.example.isudoku.GameManager;
import org.example.isudoku.PuzzleView;
import org.example.isudoku.R;

public class ManagerSelectGame implements View.OnClickListener, DialogInterface.OnClickListener {
    public static final String LOAD_GAME = "Custom_Game";
    private int basePuzzleID;
    private Context context;
    private int dbPuzzleID;

    public ManagerSelectGame(Context context2, ViewGroup group, String name, int id, int puzzleID2) {
        this.dbPuzzleID = puzzleID2;
        this.basePuzzleID = id;
        this.context = context2;
        View selection = View.inflate(context2, R.layout.managerselectgame, null);
        ((TextView) selection.findViewById(R.id.manager_select_name)).setText(name);
        PuzzleView puzz = (PuzzleView) selection.findViewById(R.id.manager_select_game);
        puzz.setPuzzleID(id);
        puzz.setOnClickListener(this);
        group.addView(selection);
    }

    public void onClick(View v) {
        Context c = v.getContext();
        AlertDialog settings = new AlertDialog.Builder(c).create();
        settings.setTitle((int) R.string.manager_play_title);
        settings.setMessage(c.getString(R.string.manager_play_message));
        settings.setButton(c.getString(R.string.manager_play_game), this);
        settings.setButton2(c.getString(R.string.manager_delete_game), this);
        settings.show();
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -2:
                new GameManagerDataHelper(this.context).delete(this.dbPuzzleID);
                this.context.startActivity(new Intent(this.context, GameManager.class));
                dialog.dismiss();
                return;
            case -1:
                Intent j = new Intent(this.context, Game.class);
                ((GameBase) this.context).setPuzzleIndex(this.basePuzzleID);
                j.putExtra(LOAD_GAME, ((GameBase) this.context).puzzleToString());
                this.context.startActivity(j);
                return;
            default:
                return;
        }
    }
}
