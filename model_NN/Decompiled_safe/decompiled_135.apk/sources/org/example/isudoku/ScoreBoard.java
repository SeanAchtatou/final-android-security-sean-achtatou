package org.example.isudoku;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.LinearLayout;
import ismailsen.SQLite.TimerDataHelper;
import ismailsen.ScoreBoardTableRow;

public class ScoreBoard extends Activity {
    private LinearLayout scoreBoard;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.scoreboard);
        this.scoreBoard = (LinearLayout) findViewById(R.id.scoreboard);
        displayScores();
    }

    private void displayScores() {
        Cursor cursor = new TimerDataHelper(this).getCursor();
        if (cursor.moveToFirst()) {
            do {
                this.scoreBoard.addView(new ScoreBoardTableRow(this, cursor.getString(0), cursor.getString(1), cursor.getString(2)));
            } while (cursor.moveToNext());
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }
}
