package org.example.isudoku;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import ismailsen.GameBase;
import ismailsen.GameCompleteDialog;
import ismailsen.Generator;
import ismailsen.History;
import ismailsen.ManagerSelectGame;
import ismailsen.SQLite.TimerDataHelper;
import ismailsen.TimerView;
import ismailsen.latlng;
import java.util.ArrayList;

public class Game extends GameBase implements DialogInterface.OnClickListener, View.OnClickListener {
    public static final int DIFFICULTY_EASY = 0;
    public static final int DIFFICULTY_HARD = 2;
    public static final int DIFFICULTY_MEDIUM = 1;
    public static final String KEY_DIFFICULTY = "org.example.sudoku.difficulty";
    private Dialog completeAlert;
    private int diff;
    private Generator gen;
    private History moveHistory;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int puzID;
        super.onCreate(savedInstanceState);
        this.diff = getIntent().getIntExtra(KEY_DIFFICULTY, 0);
        String puzzle = getIntent().getStringExtra(ManagerSelectGame.LOAD_GAME);
        if (puzzle == null) {
            puzID = getPuzzle(this.diff);
        } else {
            puzID = addPuzzle(puzzle);
        }
        calculateUsedTiles();
        setContentView((int) R.layout.hintbar);
        this.puzzleView = (PuzzleView) findViewById(R.id.sudokupuzzle);
        this.puzzleView.setPuzzleID(puzID);
        findViewById(R.id.hints).setOnClickListener(this);
        this.moveHistory = new History(findViewById(R.id.hint_history), this, this.puzzleView);
        findViewById(R.id.hint_history_bar).setOnClickListener(this);
        this.puzzleView.setMoveHistory(this.moveHistory);
        this.puzzleView.requestFocus();
    }

    private int getPuzzle(int diff2) {
        this.gen = new Generator(diff2);
        return addPuzzle(this.gen.generatePuzzleString());
    }

    /* access modifiers changed from: protected */
    public boolean checkGameComplete() {
        for (int i : (int[]) this.puzzle.get(this.puzzleIndex)) {
            if (i == 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void checkGameResult() {
        if (searchForErrors(false).length == 0) {
            showGameValidDialog();
        } else {
            showGameInvalidDialog();
        }
        this.completeAlert.show();
    }

    /* access modifiers changed from: protected */
    public latlng[] searchForErrors(boolean firstMatch) {
        ArrayList<latlng> res = new ArrayList<>();
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                if (this.result[x][y] == 0) {
                    latlng thisLatLng = new latlng();
                    thisLatLng.x = x;
                    thisLatLng.y = y;
                    res.add(thisLatLng);
                    if (firstMatch) {
                        break;
                    }
                }
            }
        }
        return (latlng[]) res.toArray(new latlng[0]);
    }

    public latlng[] getTilesOfValue(int value) {
        ArrayList<latlng> res = new ArrayList<>();
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                if (((int[]) this.puzzle.get(this.puzzleIndex))[(y * 9) + x] == value) {
                    latlng thisLatLng = new latlng();
                    thisLatLng.x = x;
                    thisLatLng.y = y;
                    res.add(thisLatLng);
                }
            }
        }
        return (latlng[]) res.toArray(new latlng[0]);
    }

    private void showHint() {
        latlng[] res = searchForErrors(true);
        if (res.length > 0) {
            setStatus(R.string.hint_highlighted);
            this.puzzleView.showHint(res[0].x, res[0].y);
            return;
        }
        setStatus(R.string.hint_nohint);
    }

    private void setStatus(int arg) {
        ((TextView) findViewById(R.id.hintstatus)).setText(arg);
    }

    public void setMovesStatus(int arg) {
        ((TextView) findViewById(R.id.hintstatus)).setText(arg > 0 ? String.valueOf(Integer.toString(arg)) + " Moves Available" : "No Moves");
    }

    public void setTileIfValid(int x, int y, int value) {
        super.setTileIfValid(x, y, value);
        if (checkGameComplete()) {
            checkGameResult();
        }
    }

    /* access modifiers changed from: protected */
    public void showGameValidDialog() {
        ((TimerView) findViewById(R.id.timer_status)).stopTimer();
        this.completeAlert = new GameCompleteDialog(this);
    }

    /* access modifiers changed from: protected */
    public void showGameInvalidDialog() {
        AlertDialog failAlert = new AlertDialog.Builder(this).create();
        failAlert.setTitle((int) R.string.game_incorrect_title);
        failAlert.setMessage(getString(R.string.game_incorrect));
        failAlert.setButton("Finish", this);
        failAlert.setButton2("Hint", this);
        this.completeAlert = failAlert;
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case -2:
                showHint();
                break;
            case -1:
                startActivity(new Intent(this, Sudoku.class));
                break;
        }
        this.completeAlert.dismiss();
    }

    public void onClick(View v) {
        if (v == findViewById(R.id.hint_history_bar)) {
            this.moveHistory.toggleHintBar();
        } else if (v == findViewById(R.id.hints)) {
            showHint();
        } else {
            this.completeAlert.dismiss();
            new TimerDataHelper(this).insert(((GameCompleteDialog) this.completeAlert).getName(), this.diff == 0 ? "Easy" : this.diff == 1 ? "Medium" : "Hard", ((TextView) findViewById(R.id.timer_status)).getText().toString());
            finish();
        }
    }
}
