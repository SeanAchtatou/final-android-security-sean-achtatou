package org.example.isudoku;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import ismailsen.GameBase;
import ismailsen.ManagerSelectGame;
import ismailsen.SQLite.GameManagerDataHelper;

public class GameManager extends GameBase implements DialogInterface.OnClickListener, View.OnClickListener {
    private Button createGame;
    private GameManagerDataHelper customGames;
    private Cursor gamesCursor;
    private LinearLayout gamesDisplay;
    private AlertDialog noGames;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.gamemanager);
        this.customGames = new GameManagerDataHelper(this);
        this.gamesCursor = this.customGames.getCursor();
        if (this.gamesCursor.getCount() == 0) {
            showNoGameDialog();
            this.gamesCursor.close();
            return;
        }
        init();
        this.createGame = (Button) findViewById(R.id.manager_create_game_button);
        this.createGame.setOnClickListener(this);
        ((TextView) findViewById(R.id.manager_game_view_status)).setText(String.valueOf(this.gamesCursor.getCount()) + " Saved Games");
    }

    public void showNoGameDialog() {
        AlertDialog failAlert = new AlertDialog.Builder(this).create();
        failAlert.setTitle((int) R.string.game_manager_no_games_title);
        failAlert.setMessage(getString(R.string.game_manager_no_games_message));
        failAlert.setButton("Back", this);
        failAlert.setButton2("Create Game", this);
        this.noGames = failAlert;
        this.noGames.show();
    }

    public void init() {
        this.gamesDisplay = (LinearLayout) findViewById(R.id.manager_select);
        if (this.gamesCursor.moveToFirst()) {
            do {
                int puzzleID = this.gamesCursor.getInt(0);
                new ManagerSelectGame(this, this.gamesDisplay, this.gamesCursor.getString(1), addPuzzle(this.gamesCursor.getString(2)), puzzleID);
            } while (this.gamesCursor.moveToNext());
        }
        if (this.gamesCursor != null && !this.gamesCursor.isClosed()) {
            this.gamesCursor.close();
        }
    }

    private void createNewGame() {
        setContentView((int) R.layout.managercreategame);
        this.puzzleView = (PuzzleView) findViewById(R.id.manager_create_puzzleview);
        ((Button) findViewById(R.id.manager_create_finish_button)).setOnClickListener(this);
    }

    public void onClick(DialogInterface arg0, int which) {
        switch (which) {
            case -2:
                createNewGame();
                break;
            case -1:
                startActivity(new Intent(this, Sudoku.class));
                break;
        }
        this.noGames.dismiss();
    }

    public void onClick(View v) {
        if (v == ((Button) findViewById(R.id.manager_create_finish_button))) {
            this.customGames.insert(((EditText) findViewById(R.id.manager_create_gamename)).getText().toString(), puzzleToString());
            startActivity(new Intent(this, GameManager.class));
        }
        if (v == this.createGame) {
            createNewGame();
        }
    }
}
