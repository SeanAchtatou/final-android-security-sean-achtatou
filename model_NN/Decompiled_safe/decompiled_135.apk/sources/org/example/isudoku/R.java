package org.example.isudoku;

public final class R {

    public static final class anim {
        public static final int cycle_7 = 2130968576;
        public static final int shake = 2130968577;
    }

    public static final class array {
        public static final int difficulty = 2131099648;
    }

    public static final class attr {
        public static final int mode = 2130771968;
    }

    public static final class color {
        public static final int background = 2131230720;
        public static final int puzzle_background = 2131230721;
        public static final int puzzle_dark = 2131230724;
        public static final int puzzle_foreground = 2131230725;
        public static final int puzzle_hilite = 2131230722;
        public static final int puzzle_hint_0 = 2131230726;
        public static final int puzzle_hint_1 = 2131230727;
        public static final int puzzle_hint_2 = 2131230728;
        public static final int puzzle_light = 2131230723;
        public static final int puzzle_selected = 2131230729;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int sudoku_icon = 2130837505;
    }

    public static final class id {
        public static final int about_button = 2131165218;
        public static final int about_content = 2131165187;
        public static final int buttonbar = 2131165199;
        public static final int complete_message = 2131165188;
        public static final int createMode = 2131165185;
        public static final int exit_button = 2131165219;
        public static final int finish_button = 2131165190;
        public static final int gameMode = 2131165184;
        public static final int game_manager_button = 2131165216;
        public static final int hint_history = 2131165196;
        public static final int hint_history_bar = 2131165197;
        public static final int hints = 2131165200;
        public static final int hintstatus = 2131165201;
        public static final int history_holder = 2131165195;
        public static final int keypad = 2131165203;
        public static final int keypad_1 = 2131165204;
        public static final int keypad_2 = 2131165205;
        public static final int keypad_3 = 2131165206;
        public static final int keypad_4 = 2131165207;
        public static final int keypad_5 = 2131165208;
        public static final int keypad_6 = 2131165209;
        public static final int keypad_7 = 2131165210;
        public static final int keypad_8 = 2131165211;
        public static final int keypad_9 = 2131165212;
        public static final int linearLayout1 = 2131165213;
        public static final int manager_create_finish_button = 2131165220;
        public static final int manager_create_game_button = 2131165191;
        public static final int manager_create_gamename = 2131165222;
        public static final int manager_create_puzzleview = 2131165221;
        public static final int manager_game_view_status = 2131165194;
        public static final int manager_scroll = 2131165192;
        public static final int manager_select = 2131165193;
        public static final int manager_select_game = 2131165224;
        public static final int manager_select_name = 2131165223;
        public static final int name_input = 2131165189;
        public static final int new_button = 2131165215;
        public static final int scoreboard = 2131165225;
        public static final int scores_button = 2131165217;
        public static final int selectionMode = 2131165186;
        public static final int settings = 2131165226;
        public static final int sudokupuzzle = 2131165198;
        public static final int textView1 = 2131165214;
        public static final int timer_status = 2131165202;
    }

    public static final class layout {
        public static final int about = 2130903040;
        public static final int alertdialog = 2130903041;
        public static final int gamemanager = 2130903042;
        public static final int hintbar = 2130903043;
        public static final int keypad = 2130903044;
        public static final int main = 2130903045;
        public static final int managercreategame = 2130903046;
        public static final int managerselectgame = 2130903047;
        public static final int scoreboard = 2130903048;
    }

    public static final class menu {
        public static final int menu = 2131361792;
    }

    public static final class string {
        public static final int about_label = 2131296260;
        public static final int about_text = 2131296278;
        public static final int about_title = 2131296277;
        public static final int app_name = 2131296256;
        public static final int easy_label = 2131296271;
        public static final int exit_label = 2131296261;
        public static final int game_correct = 2131296280;
        public static final int game_correct_title = 2131296279;
        public static final int game_incorrect = 2131296282;
        public static final int game_incorrect_title = 2131296281;
        public static final int game_manager_create_finish_button = 2131296289;
        public static final int game_manager_label = 2131296286;
        public static final int game_manager_no_games_message = 2131296288;
        public static final int game_manager_no_games_title = 2131296287;
        public static final int game_title = 2131296274;
        public static final int hard_label = 2131296273;
        public static final int hint_button = 2131296283;
        public static final int hint_highlighted = 2131296284;
        public static final int hint_nohint = 2131296285;
        public static final int hints_summary = 2131296269;
        public static final int hints_title = 2131296268;
        public static final int keypad_title = 2131296276;
        public static final int main_title = 2131296257;
        public static final int manager_create_button = 2131296290;
        public static final int manager_delete_game = 2131296292;
        public static final int manager_play_game = 2131296291;
        public static final int manager_play_message = 2131296294;
        public static final int manager_play_title = 2131296293;
        public static final int medium_label = 2131296272;
        public static final int music_summary = 2131296267;
        public static final int music_title = 2131296266;
        public static final int new_game_label = 2131296258;
        public static final int new_game_title = 2131296270;
        public static final int no_moves_label = 2131296275;
        public static final int score_title = 2131296262;
        public static final int scores_label = 2131296259;
        public static final int settings_label = 2131296263;
        public static final int settings_shortcut = 2131296265;
        public static final int settings_title = 2131296264;
    }

    public static final class styleable {
        public static final int[] PuzzleView = {R.attr.mode};
        public static final int PuzzleView_mode = 0;
    }

    public static final class xml {
        public static final int settings = 2131034112;
    }
}
