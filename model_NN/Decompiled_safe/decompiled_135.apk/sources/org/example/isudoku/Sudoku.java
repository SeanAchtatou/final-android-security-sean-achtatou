package org.example.isudoku;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class Sudoku extends Activity implements View.OnClickListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        findViewById(R.id.new_button).setOnClickListener(this);
        findViewById(R.id.about_button).setOnClickListener(this);
        findViewById(R.id.exit_button).setOnClickListener(this);
        findViewById(R.id.scores_button).setOnClickListener(this);
        findViewById(R.id.game_manager_button).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_button:
                openNewGameDialog();
                return;
            case R.id.game_manager_button:
                showGameManager();
                return;
            case R.id.scores_button:
                showScores();
                return;
            case R.id.about_button:
                startActivity(new Intent(this, About.class));
                return;
            case R.id.exit_button:
                finish();
                return;
            default:
                return;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            default:
                return false;
        }
    }

    private void openNewGameDialog() {
        new AlertDialog.Builder(this).setTitle((int) R.string.new_game_title).setItems((int) R.array.difficulty, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                Sudoku.this.startGame(i);
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void startGame(int i) {
        Intent intent = new Intent(this, Game.class);
        intent.putExtra(Game.KEY_DIFFICULTY, i);
        startActivity(intent);
    }

    private void showScores() {
        startActivity(new Intent(this, ScoreBoard.class));
    }

    private void showGameManager() {
        startActivity(new Intent(this, GameManager.class));
    }
}
