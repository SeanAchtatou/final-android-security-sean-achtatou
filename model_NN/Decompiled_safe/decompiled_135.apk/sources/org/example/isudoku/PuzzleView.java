package org.example.isudoku;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import ismailsen.GameBase;
import ismailsen.History;
import ismailsen.latlng;

public class PuzzleView extends View {
    private static final String TAG = "Sudoku";
    private int displayMode;
    private GameBase game;
    private float height;
    private latlng hint = new latlng();
    private History moveHistory;
    private int puzzleID;
    private final Rect selRect = new Rect();
    private int selX;
    private int selY;
    private int tile;
    private float width;

    public PuzzleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.displayMode = getContext().obtainStyledAttributes(attrs, R.styleable.PuzzleView).getInt(0, 0);
        if (this.displayMode == 0) {
            this.game = (Game) context;
        } else {
            this.game = (GameManager) context;
        }
        setFocusable(true);
        setClickable(true);
        setFocusableInTouchMode(true);
        this.hint.x = -1;
    }

    public void setMoveHistory(History moveHistory2) {
        this.moveHistory = moveHistory2;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int _height = View.MeasureSpec.getSize(heightMeasureSpec);
        int _width = View.MeasureSpec.getSize(widthMeasureSpec);
        if (this.displayMode == 2) {
            _width = (int) (((double) this.game.getWindowManager().getDefaultDisplay().getWidth()) * 0.9d);
            _height = (int) (((double) _height) * 0.9d);
        }
        setMeasuredDimension(_width, _height);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        this.width = ((float) w) / 9.0f;
        this.height = ((float) h) / 9.0f;
        getRect(this.selX, this.selY, this.selRect);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Paint background = new Paint();
        background.setColor(getResources().getColor(R.color.puzzle_background));
        canvas.drawRect(0.0f, 0.0f, (float) getWidth(), (float) getHeight(), background);
        Paint dark = new Paint();
        dark.setColor(getResources().getColor(R.color.puzzle_dark));
        Paint hilite = new Paint();
        hilite.setColor(getResources().getColor(R.color.puzzle_hilite));
        Paint light = new Paint();
        light.setColor(getResources().getColor(R.color.puzzle_light));
        for (int i = 0; i < 9; i++) {
            canvas.drawLine(0.0f, ((float) i) * this.height, (float) getWidth(), ((float) i) * this.height, light);
            canvas.drawLine(0.0f, (((float) i) * this.height) + 1.0f, (float) getWidth(), (((float) i) * this.height) + 1.0f, hilite);
            canvas.drawLine(((float) i) * this.width, 0.0f, ((float) i) * this.width, (float) getHeight(), light);
            canvas.drawLine((((float) i) * this.width) + 1.0f, 0.0f, (((float) i) * this.width) + 1.0f, (float) getHeight(), hilite);
        }
        for (int i2 = 0; i2 < 9; i2++) {
            if (i2 % 3 == 0) {
                canvas.drawLine(0.0f, ((float) i2) * this.height, (float) getWidth(), ((float) i2) * this.height, dark);
                canvas.drawLine(0.0f, (((float) i2) * this.height) + 1.0f, (float) getWidth(), (((float) i2) * this.height) + 1.0f, hilite);
                canvas.drawLine(((float) i2) * this.width, 0.0f, ((float) i2) * this.width, (float) getHeight(), dark);
                canvas.drawLine((((float) i2) * this.width) + 1.0f, 0.0f, (((float) i2) * this.width) + 1.0f, (float) getHeight(), hilite);
            }
        }
        if (this.tile != 0 && this.displayMode == 0) {
            for (latlng val : ((Game) this.game).getTilesOfValue(this.tile)) {
                Rect colRect = new Rect();
                Paint colSel = new Paint();
                colSel.setColor(getResources().getColor(R.color.puzzle_hint_1));
                getRect(val.x, val.y, colRect);
                canvas.drawRect(colRect, colSel);
            }
            this.tile = 0;
        }
        if (this.hint.x == -1 && this.displayMode == 0) {
            Paint selected = new Paint();
            selected.setColor(getResources().getColor(R.color.puzzle_selected));
            canvas.drawRect(this.selRect, selected);
            ((Game) this.game).setMovesStatus(9 - this.game.getUsedTiles(this.selX, this.selY).length);
        }
        if (this.hint.x != -1) {
            Rect colRect2 = new Rect();
            Paint colSel2 = new Paint();
            colSel2.setColor(getResources().getColor(R.color.puzzle_hint_0));
            getRect(this.hint.x, this.hint.y, colRect2);
            canvas.drawRect(colRect2, colSel2);
            this.hint.x = -1;
        }
        Paint paint = new Paint(1);
        paint.setColor(getResources().getColor(R.color.puzzle_foreground));
        paint.setStyle(Paint.Style.FILL);
        paint.setTextSize(this.height * 0.75f);
        paint.setTextScaleX(this.width / this.height);
        paint.setTextAlign(Paint.Align.CENTER);
        Paint.FontMetrics fm = paint.getFontMetrics();
        float x = this.width / 2.0f;
        float y = (this.height / 2.0f) - ((fm.ascent + fm.descent) / 2.0f);
        for (int i3 = 0; i3 < 9; i3++) {
            for (int j = 0; j < 9; j++) {
                canvas.drawText(this.game.getTileString(i3, j, this.puzzleID), (((float) i3) * this.width) + x, (((float) j) * this.height) + y, paint);
            }
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.displayMode == 2) {
            return super.onTouchEvent(event);
        }
        if (event.getAction() != 0) {
            return super.onTouchEvent(event);
        }
        select((int) (event.getX() / this.width), (int) (event.getY() / this.height));
        showKeypadBasedOnMode();
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.displayMode != 2) {
            switch (keyCode) {
                case 7:
                case 62:
                    setSelectedTile(0);
                    break;
                case 8:
                    setSelectedTile(1);
                    break;
                case 9:
                    setSelectedTile(2);
                    break;
                case 10:
                    setSelectedTile(3);
                    break;
                case 11:
                    setSelectedTile(4);
                    break;
                case 12:
                    setSelectedTile(5);
                    break;
                case 13:
                    setSelectedTile(6);
                    break;
                case 14:
                    setSelectedTile(7);
                    break;
                case 15:
                    setSelectedTile(8);
                    break;
                case 16:
                    setSelectedTile(9);
                    break;
                case 19:
                    select(this.selX, this.selY - 1);
                    break;
                case 20:
                    select(this.selX, this.selY + 1);
                    break;
                case 21:
                    select(this.selX - 1, this.selY);
                    break;
                case 22:
                    select(this.selX + 1, this.selY);
                    break;
                case 23:
                case 66:
                    showKeypadBasedOnMode();
                    break;
                default:
                    return super.onKeyDown(keyCode, event);
            }
        }
        Log.d(TAG, "onKeyDown: keycode=" + keyCode + ", event=" + event);
        switch (keyCode) {
            case 7:
            case 62:
                setSelectedTile(0);
                break;
            case 8:
                setSelectedTile(1);
                break;
            case 9:
                setSelectedTile(2);
                break;
            case 10:
                setSelectedTile(3);
                break;
            case 11:
                setSelectedTile(4);
                break;
            case 12:
                setSelectedTile(5);
                break;
            case 13:
                setSelectedTile(6);
                break;
            case 14:
                setSelectedTile(7);
                break;
            case 15:
                setSelectedTile(8);
                break;
            case 16:
                setSelectedTile(9);
                break;
            case 19:
                select(this.selX, this.selY - 1);
                break;
            case 20:
                select(this.selX, this.selY + 1);
                break;
            case 21:
                select(this.selX - 1, this.selY);
                break;
            case 22:
                select(this.selX + 1, this.selY);
                break;
            case 23:
            case 66:
                showKeypadBasedOnMode();
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    public void select(int x, int y) {
        invalidate(this.selRect);
        this.selX = Math.min(Math.max(x, 0), 8);
        this.selY = Math.min(Math.max(y, 0), 8);
        getRect(this.selX, this.selY, this.selRect);
        invalidate(this.selRect);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((int) (((float) x) * this.width), (int) (((float) y) * this.height), (int) ((((float) x) * this.width) + this.width), (int) ((((float) y) * this.height) + this.height));
    }

    public void setSelectedTile(int tile2) {
        this.tile = tile2;
        this.game.setTileIfValid(this.selX, this.selY, tile2);
        if (this.displayMode == 0) {
            this.moveHistory.newMove(this.selX, this.selY, tile2);
        }
        invalidate();
    }

    public void showHint(int x, int y) {
        this.hint.x = x;
        this.hint.y = y;
        getRect(x, y, new Rect());
        invalidate();
    }

    public void showKeypadBasedOnMode() {
        switch (this.displayMode) {
            case Game.DIFFICULTY_EASY:
                this.game.showKeypad();
                return;
            case Game.DIFFICULTY_MEDIUM:
                this.game.showKeypad(this.selX, this.selY);
                return;
            default:
                return;
        }
    }

    public void setPuzzleID(int id) {
        this.puzzleID = id;
    }
}
