package org.example.isudoku;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class Keypad extends Dialog {
    protected static final String TAG = "Sudoku";
    private View keypad;
    private final View[] keys = new View[9];
    private final PuzzleView puzzleView;
    private int[] useds;

    public Keypad(Context context, PuzzleView puzzleView2) {
        super(context);
        this.puzzleView = puzzleView2;
    }

    public Keypad(Context context, int[] useds2, PuzzleView puzzleView2) {
        super(context);
        if (useds2 == null) {
            this.useds = new int[1];
        } else {
            this.useds = useds2;
        }
        this.puzzleView = puzzleView2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle((int) R.string.keypad_title);
        setContentView((int) R.layout.keypad);
        findViews();
        if (this.useds != null) {
            for (int element : this.useds) {
                if (element != 0) {
                    this.keys[element - 1].setVisibility(4);
                }
            }
        }
        setListeners();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        int tile;
        switch (keyCode) {
            case 7:
            case 62:
                tile = 0;
                break;
            case 8:
                tile = 1;
                break;
            case 9:
                tile = 2;
                break;
            case 10:
                tile = 3;
                break;
            case 11:
                tile = 4;
                break;
            case 12:
                tile = 5;
                break;
            case 13:
                tile = 6;
                break;
            case 14:
                tile = 7;
                break;
            case 15:
                tile = 8;
                break;
            case 16:
                tile = 9;
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        if (isValid(tile)) {
            returnResult(tile);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void returnResult(int tile) {
        this.puzzleView.setSelectedTile(tile);
        dismiss();
    }

    private boolean isValid(int tile) {
        for (int t : this.useds) {
            if (tile == t) {
                return false;
            }
        }
        return true;
    }

    private void findViews() {
        this.keypad = findViewById(R.id.keypad);
        this.keys[0] = findViewById(R.id.keypad_1);
        this.keys[1] = findViewById(R.id.keypad_2);
        this.keys[2] = findViewById(R.id.keypad_3);
        this.keys[3] = findViewById(R.id.keypad_4);
        this.keys[4] = findViewById(R.id.keypad_5);
        this.keys[5] = findViewById(R.id.keypad_6);
        this.keys[6] = findViewById(R.id.keypad_7);
        this.keys[7] = findViewById(R.id.keypad_8);
        this.keys[8] = findViewById(R.id.keypad_9);
    }

    private void setListeners() {
        for (int i = 0; i < this.keys.length; i++) {
            final int t = i + 1;
            this.keys[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Keypad.this.returnResult(t);
                }
            });
        }
        this.keypad.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Keypad.this.returnResult(0);
            }
        });
    }
}
