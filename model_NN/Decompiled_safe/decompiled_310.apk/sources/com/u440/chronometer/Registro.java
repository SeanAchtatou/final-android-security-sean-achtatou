package com.u440.chronometer;

public class Registro {
    private String extra;
    private int imagen;
    private int linea;
    private String tiempo;

    public int getLinea() {
        return this.linea;
    }

    public void setLinea(int i) {
        this.linea = i;
    }

    public String getTiempo() {
        return this.tiempo;
    }

    public void setTiempo(String tiempo2) {
        this.tiempo = tiempo2;
    }

    public String getExtra() {
        return this.extra;
    }

    public void setExtra(String extra2) {
        this.extra = extra2;
    }

    public int getImagen() {
        return this.imagen;
    }

    public void setImagen(int i) {
        this.imagen = i;
    }
}
