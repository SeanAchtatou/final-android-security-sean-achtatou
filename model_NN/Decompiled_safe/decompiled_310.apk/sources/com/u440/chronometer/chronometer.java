package com.u440.chronometer;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.os.Vibrator;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class chronometer extends ListActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String PREFS_NAME = "crono";
    private Sensor acc;
    boolean ads = true;
    AlertDialog.Builder alert;
    /* access modifiers changed from: private */
    public boolean bTrg = false;
    int bpm = 30;
    /* access modifiers changed from: private */
    public Button button1;
    /* access modifiers changed from: private */
    public Button button2;
    private Button button3;
    int chess = 0;
    long chesstime = 0;
    String color = "#DB3F7D";
    Double cost = Double.valueOf(5.0d);
    String csvsep;
    Date dAct = new Date();
    long delay = 0;
    boolean displayon = false;
    Double distance = Double.valueOf(1.0d);
    private LinearLayout fondo;
    /* access modifiers changed from: private */
    public TextView info;
    EditText input;
    String isymbol;
    /* access modifiers changed from: private */
    public float[] mAcc;
    private String[] mChess;
    /* access modifiers changed from: private */
    public IconListViewAdapter mChronometer;
    private String[] mDists;
    /* access modifiers changed from: private */
    public float[] mMag;
    /* access modifiers changed from: private */
    public String[] mMet;
    /* access modifiers changed from: private */
    public String[] mMetv;
    /* access modifiers changed from: private */
    public ArrayList<Registro> mRegistros = new ArrayList<>();
    CountDownTimer mReloj;
    View.OnClickListener mResetListener = new View.OnClickListener() {
        public void onClick(View v) {
            chronometer.this.resetCrono();
            chronometer.this.showCrono();
            if (chronometer.this.soundon) {
                chronometer.this.sounds.play(chronometer.this.mplID, 1.0f, 1.0f, 0, 0, 1.0f);
            }
            if (chronometer.this.vibration) {
                chronometer.this.vibrator.vibrate(200);
            }
        }
    };
    SeekBar.OnSeekBarChangeListener mSeek = new SeekBar.OnSeekBarChangeListener() {
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            chronometer.this.bpm = progress + 30;
            chronometer.this.info.setText(String.valueOf(chronometer.this.seekInt(chronometer.this.bpm, chronometer.this.mMetv, chronometer.this.mMet)) + " (" + chronometer.this.bpm + ")");
        }

        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        public void onStopTrackingTouch(SeekBar seekBar) {
            SharedPreferences.Editor editor = chronometer.this.getSharedPreferences(chronometer.PREFS_NAME, 0).edit();
            editor.putInt("bpm", chronometer.this.bpm);
            editor.commit();
        }
    };
    SensorEventListener mSensorListener = new SensorEventListener() {
        public void onSensorChanged(SensorEvent se) {
            if (!chronometer.this.stopped && chronometer.this.mode != 5 && chronometer.this.shake) {
                switch (se.sensor.getType()) {
                    case R.styleable.RangePreference_minimumValue:
                        chronometer.this.mAcc = (float[]) se.values.clone();
                        break;
                    case R.styleable.RangePreference_stepSize:
                        chronometer.this.mMag = (float[]) se.values.clone();
                        break;
                }
                if (chronometer.this.mMag != null && chronometer.this.mAcc != null) {
                    float[] R = new float[9];
                    float[] or = new float[3];
                    if (SensorManager.getRotationMatrix(R, new float[9], chronometer.this.mAcc, chronometer.this.mMag)) {
                        SensorManager.getOrientation(R, or);
                        float round = (float) Math.round(Math.toDegrees((double) or[0]));
                        float mPitch = (float) Math.round(Math.toDegrees((double) or[1]));
                        float round2 = (float) Math.round(Math.toDegrees((double) or[2]));
                        if (Math.abs(mPitch) > 45.0f) {
                            chronometer.this.bTrg = true;
                        }
                        if (Math.abs(mPitch) < 35.0f && chronometer.this.bTrg) {
                            chronometer.this.bTrg = false;
                            chronometer.this.splitCrono();
                        }
                    }
                }
            }
        }

        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };
    private SensorManager mSensorManager;
    View.OnClickListener mStartListener = new View.OnClickListener() {
        public void onClick(View v) {
            boolean z;
            if (chronometer.this.button1.getText().equals(chronometer.this.getResources().getString(R.string.chronometer_done)) || chronometer.this.button1.getText().equals(chronometer.this.getResources().getString(R.string.chronometer_end))) {
                chronometer.this.resetCrono();
                chronometer.this.showCrono();
                if (chronometer.this.soundon) {
                    chronometer.this.sounds.play(chronometer.this.mplID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
                if (chronometer.this.vibration) {
                    chronometer.this.vibrator.vibrate(200);
                    return;
                }
                return;
            }
            if (chronometer.this.stopped) {
                Log.v("START", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
                chronometer.this.button2.setText((int) R.string.chronometer_stop);
                chronometer.this.stopped = false;
                chronometer.this.setCrono();
                if (chronometer.this.mode == 4 && chronometer.this.chess == 2) {
                    chronometer.this.addChess();
                }
                chronometer.this.mReloj.start();
                chronometer chronometer = chronometer.this;
                if (!chronometer.this.displayon || chronometer.this.stopped) {
                    z = false;
                } else {
                    z = true;
                }
                chronometer.setScreen(z);
            } else {
                chronometer.this.button2.setText((int) R.string.chronometer_start);
                chronometer.this.mReloj.cancel();
                chronometer.this.setScreen(false);
                chronometer.this.tickCrono();
                chronometer.this.stopped = true;
            }
            if (chronometer.this.mode != 5) {
                chronometer.this.showCrono();
                if (chronometer.this.soundon) {
                    chronometer.this.sounds.play(chronometer.this.mplID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
                if (chronometer.this.vibration) {
                    chronometer.this.vibrator.vibrate(100);
                }
            }
        }
    };
    View.OnClickListener mTimerListener = new View.OnClickListener() {
        public void onClick(View v) {
            chronometer.this.mWebView.setVisibility(8);
            chronometer.this.splitCrono();
        }
    };
    private String[] mUnits;
    /* access modifiers changed from: private */
    public WebView mWebView;
    private Sensor mag;
    int metID;
    int mode = 0;
    private String[] modes;
    int mpcID;
    int mplID;
    int mpmID;
    int mprID;
    DecimalFormat n2 = new DecimalFormat("00");
    public SharedPreferences pf;
    int player = 1;
    String player1;
    String player2;
    int pld;
    private PowerManager pm;
    boolean resetbutton = false;
    SimpleDateFormat rtf = new SimpleDateFormat("HH:mm:ss.SSS");
    String sLast = "";
    private SeekBar seek;
    boolean shake = false;
    boolean soundon = false;
    SoundPool sounds;
    boolean stopped = true;
    String symbol;
    Long tDly;
    Long tIni = 0L;
    Long tLast = 0L;
    Long tLast2 = 0L;
    Long tLimit = 999999999L;
    int tMin = 1;
    int tNum = 0;
    int tSeg = 0;
    Long tTot = 0L;
    Long tTot2 = 0L;
    String units = "mph";
    boolean vibration = true;
    Vibrator vibrator;
    private PowerManager.WakeLock wl;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.fondo = (LinearLayout) findViewById(R.id.android_fondo);
        this.mChronometer = new IconListViewAdapter(this, R.layout.list_row, this.mRegistros);
        setListAdapter(this.mChronometer);
        this.modes = getResources().getStringArray(R.array.mode_array);
        this.mDists = getResources().getStringArray(R.array.dist_array);
        this.mUnits = getResources().getStringArray(R.array.units_array);
        this.mChess = getResources().getStringArray(R.array.chess_array);
        this.mMet = getResources().getStringArray(R.array.met_array);
        this.mMetv = getResources().getStringArray(R.array.met_arrayv);
        this.button1 = (Button) findViewById(R.id.timer);
        this.button1.setOnClickListener(this.mTimerListener);
        this.button1.setTypeface(Typeface.createFromAsset(getAssets(), "digital-7.ttf"));
        this.button2 = (Button) findViewById(R.id.start);
        this.button2.setOnClickListener(this.mStartListener);
        this.button3 = (Button) findViewById(R.id.reset);
        this.button3.setOnClickListener(this.mResetListener);
        this.info = (TextView) findViewById(R.id.info);
        this.seek = (SeekBar) findViewById(R.id.seek);
        this.seek.setOnSeekBarChangeListener(this.mSeek);
        this.mWebView = (WebView) findViewById(R.id.ads);
        this.mWebView.setBackgroundColor(0);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.vibrator = (Vibrator) getSystemService("vibrator");
        this.pm = (PowerManager) getSystemService("power");
        this.wl = this.pm.newWakeLock(805306394, "fl");
        this.mSensorManager = (SensorManager) getSystemService("sensor");
        this.acc = this.mSensorManager.getDefaultSensor(1);
        this.mag = this.mSensorManager.getDefaultSensor(2);
        if (this.acc == null || this.mag == null) {
            this.mSensorListener = null;
        } else {
            this.mSensorManager.registerListener(this.mSensorListener, this.acc, 1);
            this.mSensorManager.registerListener(this.mSensorListener, this.mag, 1);
        }
        this.sounds = new SoundPool(40, 3, 0);
        this.metID = this.sounds.load(getApplicationContext(), R.raw.metronome, 1);
        this.mplID = this.sounds.load(getApplicationContext(), R.raw.beeplargo, 1);
        this.mpcID = this.sounds.load(getApplicationContext(), R.raw.beepcorto, 1);
        this.mpmID = this.sounds.load(getApplicationContext(), R.raw.dinero, 1);
        this.mprID = this.sounds.load(getApplicationContext(), R.raw.ring, 1);
        this.pf = getApplicationContext().getSharedPreferences(PREFS_NAME, 0);
        this.pf.registerOnSharedPreferenceChangeListener(this);
        onSharedPreferenceChanged(this.pf, null);
    }

    /* access modifiers changed from: package-private */
    public void splitCrono() {
        if (!this.button1.getText().equals(getResources().getString(R.string.chronometer_end))) {
            if (this.button1.getText().equals(getResources().getString(R.string.chronometer_done))) {
                resetCrono();
                showCrono();
                if (this.soundon) {
                    this.sounds.play(this.mplID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
                if (this.vibration) {
                    this.vibrator.vibrate(200);
                    return;
                }
                return;
            }
            this.tNum++;
            tickCrono();
            this.tDly = 0L;
            showCrono();
            String sRes = "";
            if (this.mode == 2) {
                sRes = getUnits();
            }
            if (this.mode == 3) {
                sRes = getCash();
            }
            if (this.mode == 4) {
                if (this.chess == 3 || this.chess == 4) {
                    addChess();
                    showCrono();
                    sRes = getPlayer();
                } else {
                    sRes = getPlayer();
                    addChess();
                }
            }
            String itm = TextUtils.split((String) this.button1.getText(), " ")[0];
            int img = R.drawable.ic_crono_s;
            if (this.mode == 3) {
                img = R.drawable.ic_money;
            }
            if (this.mode == 4) {
                if (this.player == 2) {
                    img = R.drawable.ic_peon_blanco;
                } else {
                    img = R.drawable.ic_peon_negro;
                }
            }
            addItem(this.tNum, itm, sRes, img);
            tickCrono();
            showCrono();
            if (this.soundon) {
                if (this.mode == 3) {
                    this.sounds.play(this.mpmID, 1.0f, 1.0f, 0, 0, 1.0f);
                } else {
                    this.sounds.play(this.mpcID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
            }
            if (this.vibration) {
                this.vibrator.vibrate(50);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void resetCrono() {
        if (this.mReloj != null) {
            this.mReloj.cancel();
        }
        setScreen(false);
        this.button2.setText((int) R.string.chronometer_start);
        this.mRegistros.clear();
        this.mChronometer.notifyDataSetChanged();
        this.tDly = 0L;
        this.tLast = 0L;
        if (this.mode == 1) {
            this.tTot = Long.valueOf(((((long) this.tMin) * 60) + ((long) this.tSeg)) * 1000);
        } else if (this.mode == 4) {
            this.tTot = Long.valueOf(this.chesstime * 60 * 1000);
            this.tLast = this.tTot;
            this.tDly = Long.valueOf(this.delay * 1000);
        } else {
            this.tTot = 0L;
        }
        if (this.mode == 6) {
            this.tLast = Long.valueOf(((((long) this.tMin) * 60) + ((long) this.tSeg)) * 1000);
        }
        this.tNum = 0;
        this.tTot2 = this.tTot;
        this.tLast2 = this.tLast;
        this.player = 1;
        if (this.mode == 4) {
            this.info.setText(this.player1);
        }
        if (this.mode == 5) {
            this.info.setText(String.valueOf(seekInt(this.bpm, this.mMetv, this.mMet)) + " (" + this.bpm + ")");
        }
        this.sLast = "";
        this.stopped = true;
    }

    /* access modifiers changed from: package-private */
    public void tickCrono() {
        this.dAct = new Date();
        long tTmp = System.currentTimeMillis();
        if (!this.stopped) {
            if (this.mode == 1) {
                this.tTot = Long.valueOf(this.tTot.longValue() - (tTmp - this.tIni.longValue()));
            } else if (this.mode != 4) {
                this.tTot = Long.valueOf(this.tTot.longValue() + (tTmp - this.tIni.longValue()));
            } else if (this.chess == 1 && this.tDly.longValue() > 0) {
                this.tDly = Long.valueOf(this.tDly.longValue() - (tTmp - this.tIni.longValue()));
                if (this.tDly.longValue() < 0) {
                    this.tDly = 0L;
                }
            } else if (this.player == 1) {
                this.tTot = Long.valueOf(this.tTot.longValue() - (tTmp - this.tIni.longValue()));
            } else {
                this.tTot2 = Long.valueOf(this.tTot2.longValue() - (tTmp - this.tIni.longValue()));
            }
            if (this.tTot.longValue() < 0) {
                this.tTot = 0L;
            }
            this.tIni = Long.valueOf(tTmp);
        }
    }

    /* access modifiers changed from: package-private */
    public void setCrono() {
        long tTick;
        if (this.mode == 5) {
            tTick = (long) (60000 / this.bpm);
        } else {
            tTick = 97;
        }
        this.mReloj = new CountDownTimer(this.tLimit.longValue(), tTick) {
            public void onTick(long millisUntilFinished) {
                if (!chronometer.this.stopped) {
                    if (chronometer.this.mode == 5) {
                        chronometer.this.sounds.stop(chronometer.this.pld);
                        chronometer.this.pld = chronometer.this.sounds.play(chronometer.this.metID, 1.0f, 1.0f, 0, 0, 1.0f);
                        return;
                    }
                    chronometer.this.tickCrono();
                    chronometer.this.showCrono();
                }
            }

            public void onFinish() {
            }
        };
        this.tIni = Long.valueOf(System.currentTimeMillis());
    }

    /* access modifiers changed from: package-private */
    public void showCrono() {
        boolean z;
        Long tShow;
        String sTime;
        String sDly = "";
        boolean z2 = (this.tTot.longValue() <= 0 && (this.mode == 1 || this.mode == 4)) || (this.tTot2.longValue() <= 0 && this.mode == 4) || (this.tTot.longValue() >= this.tLast.longValue() && this.mode == 6);
        if (this.stopped) {
            z = false;
        } else {
            z = true;
        }
        if (z2 && z) {
            this.mReloj.cancel();
            Log.v("FINISH", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
            setScreen(false);
            this.stopped = true;
            if (this.mode == 4) {
                this.button1.setText((int) R.string.chronometer_end);
                if (this.soundon) {
                    this.sounds.play(this.mprID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
            } else {
                this.button1.setText((int) R.string.chronometer_done);
                if (this.soundon) {
                    this.sounds.play(this.mplID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
            }
            if (this.vibration) {
                this.vibrator.vibrate(200);
                return;
            }
            return;
        }
        if (this.player == 1) {
            tShow = this.tTot;
        } else {
            tShow = this.tTot2;
        }
        Long tSeg2 = Long.valueOf(tShow.longValue() / 1000);
        Long tMin2 = Long.valueOf(tSeg2.longValue() / 60);
        Long tSeg3 = Long.valueOf(tSeg2.longValue() % 60);
        Long tMseg = Long.valueOf((tShow.longValue() % 1000) / 10);
        if (this.mode == 7) {
            sTime = this.rtf.format(this.dAct);
        } else {
            sTime = String.valueOf(this.n2.format(tMin2)) + ":" + this.n2.format(tSeg3) + "." + this.n2.format(tMseg);
        }
        if (this.mode == 4 && this.chess == 1) {
            sDly = " +" + this.n2.format(Long.valueOf(this.tDly.longValue() / 1000)) + "." + this.n2.format(Long.valueOf((this.tDly.longValue() % 1000) / 10));
        }
        this.button1.setText(String.valueOf(sTime) + sDly);
    }

    /* access modifiers changed from: package-private */
    public String getUnits() {
        long tUnits = 0L;
        if (!this.stopped) {
            Long tSys = this.tTot;
            Double tSeg2 = Double.valueOf(((double) (tSys.longValue() - this.tLast.longValue())) / 1000.0d);
            if (tSeg2.doubleValue() > 0.0d) {
                tUnits = Long.valueOf(Math.round((3600.0d * this.distance.doubleValue()) / tSeg2.doubleValue()));
            }
            this.tLast = tSys;
            this.sLast = tUnits + " " + this.units;
        }
        return this.sLast;
    }

    /* access modifiers changed from: package-private */
    public String getCash() {
        return Double.valueOf(((double) Math.round(((((double) this.tTot.longValue()) / 10.0d) * this.cost.doubleValue()) / 3600.0d)) / 100.0d) + " " + this.symbol;
    }

    /* access modifiers changed from: package-private */
    public String getPlayer() {
        if (this.player == 1) {
            this.info.setText(this.player2);
            this.player = 2;
        } else {
            this.info.setText(this.player1);
            this.player = 1;
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public void addChess() {
        if (!this.stopped) {
            Long tDelay = Long.valueOf(this.delay * 1000);
            if (this.chess == 1) {
                this.tDly = tDelay;
            } else if (this.chess == 2 || this.chess == 3) {
                if (this.player == 1) {
                    this.tTot = Long.valueOf(this.tTot.longValue() + tDelay.longValue());
                } else {
                    this.tTot2 = Long.valueOf(this.tTot2.longValue() + tDelay.longValue());
                }
            } else if (this.chess != 4) {
            } else {
                if (this.player == 2) {
                    if (this.tLast2.longValue() - this.tTot2.longValue() < tDelay.longValue()) {
                        this.tTot2 = this.tLast2;
                    } else {
                        this.tTot2 = Long.valueOf(this.tTot2.longValue() + tDelay.longValue());
                    }
                    this.tLast2 = this.tTot2;
                    return;
                }
                if (this.tLast.longValue() - this.tTot.longValue() < tDelay.longValue()) {
                    this.tTot = this.tLast;
                } else {
                    this.tTot = Long.valueOf(this.tTot.longValue() + tDelay.longValue());
                }
                this.tLast = this.tTot;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void showSetTimer() {
        final CronoDialog myDialog = new CronoDialog(this);
        myDialog.setCrono(this.tMin, this.tSeg);
        myDialog.setButton(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    Integer seg = Integer.valueOf(myDialog.getSeg());
                    Integer min = Integer.valueOf(myDialog.getMin());
                    SharedPreferences.Editor editor = chronometer.this.getSharedPreferences(chronometer.PREFS_NAME, 0).edit();
                    editor.putInt("free", min.intValue());
                    editor.putInt("segs", seg.intValue());
                    editor.putString("mode", myDialog.getMode());
                    editor.commit();
                    myDialog.dismiss();
                } catch (Exception e) {
                }
            }
        });
        myDialog.show();
    }

    /* access modifiers changed from: package-private */
    public void showExtra(final int position) {
        final KeyDialog cd;
        if (this.mode != 2 && this.mode != 3 && this.mode != 5) {
            if (this.mode == 4) {
                cd = new KeyDialog(this, 1, R.string.move);
            } else {
                cd = new KeyDialog(this, 2, R.string.bib);
            }
            cd.setKey(((Registro) getListView().getItemAtPosition(position)).getExtra());
            cd.setButton(new View.OnClickListener() {
                public void onClick(View view) {
                    Registro s = (Registro) chronometer.this.getListView().getItemAtPosition(position);
                    s.setExtra(cd.getKey());
                    chronometer.this.mRegistros.set(position, s);
                    chronometer.this.mChronometer.notifyDataSetChanged();
                    cd.dismiss();
                }
            });
            cd.show();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.help:
                startActivity(new Intent(this, Help.class));
                break;
            case R.id.reset:
                resetCrono();
                showCrono();
                if (this.soundon) {
                    this.sounds.play(this.mplID, 1.0f, 1.0f, 0, 0, 1.0f);
                }
                if (this.vibration) {
                    this.vibrator.vibrate(200);
                    break;
                }
                break;
            case R.id.settime:
                showSetTimer();
                break;
            case R.id.save:
                saveDialog();
                break;
            case R.id.preferences:
                startActivity(new Intent(this, prefs.class));
                break;
            case R.id.copy:
                ((ClipboardManager) getSystemService("clipboard")).setText(clip(0, false));
                Toast.makeText(getBaseContext(), (int) R.string.copied, 0).show();
                break;
            case R.id.send:
                Intent s = new Intent("android.intent.action.SEND");
                s.setType("text/plain");
                s.putExtra("android.intent.extra.SUBJECT", getResources().getString(R.string.app_name));
                s.putExtra("android.intent.extra.TEXT", clip(0, false));
                startActivity(Intent.createChooser(s, getResources().getString(R.string.select)));
                break;
            case R.id.eggtimer:
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.u440.eggtimer")));
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        this.mode = Integer.parseInt(sp.getString("mode", "0"));
        this.color = sp.getString("color", "#020202");
        this.tMin = sp.getInt("free", 1);
        this.tSeg = sp.getInt("segs", 0);
        this.soundon = sp.getBoolean("soundon", false);
        this.vibration = sp.getBoolean("vibration", true);
        this.displayon = sp.getBoolean("displayon", false);
        this.resetbutton = sp.getBoolean("resetbutton", false);
        this.shake = sp.getBoolean("shake", false);
        this.units = sp.getString("units", "mph");
        try {
            this.distance = Double.valueOf(Double.valueOf(sp.getString("distance", "1")).doubleValue());
        } catch (Exception e) {
            this.distance = Double.valueOf(1.0d);
        }
        try {
            this.cost = Double.valueOf(Double.valueOf(sp.getString("cost", "5")).doubleValue());
        } catch (Exception e2) {
            this.cost = Double.valueOf(5.0d);
        }
        this.chess = Integer.parseInt(sp.getString("chess", "0"));
        this.player1 = sp.getString("player1", "Player 1");
        this.player2 = sp.getString("player2", "Player 2");
        this.chesstime = (long) sp.getInt("chess_time", 20);
        this.delay = (long) sp.getInt("delay", 3);
        this.bpm = sp.getInt("bpm", 60);
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getCurrencyInstance();
        this.symbol = formatter.getCurrency().getSymbol();
        this.isymbol = formatter.getCurrency().getCurrencyCode();
        if (formatter.getDecimalFormatSymbols().getDecimalSeparator() == ',') {
            this.csvsep = ";";
        } else {
            this.csvsep = ",";
        }
        String tit = String.valueOf(getResources().getString(R.string.mode_title)) + ": " + this.modes[this.mode];
        if (this.mode == 1 || this.mode == 6) {
            tit = String.valueOf(tit) + " (" + this.n2.format((long) this.tMin) + ":" + this.n2.format((long) this.tSeg) + ")";
        }
        if (this.mode == 2) {
            tit = String.valueOf(tit) + " (" + this.units;
        }
        if (this.mode == 2 && this.distance.doubleValue() != 1.0d) {
            tit = String.valueOf(tit) + ", " + this.distance + " " + seek(this.units, this.mUnits, this.mDists);
        }
        if (this.mode == 2) {
            tit = String.valueOf(tit) + ")";
        }
        if (this.mode == 3) {
            tit = String.valueOf(tit) + " (" + this.cost + " " + this.symbol + "/h)";
        }
        if (this.mode == 4) {
            String tit2 = String.valueOf(tit) + " (" + this.mChess[this.chess];
            if (this.chess > 0) {
                tit2 = String.valueOf(tit2) + " " + this.delay + "s";
            }
            tit = String.valueOf(tit2) + ")";
            this.info.setVisibility(0);
        }
        if (this.mode == 5) {
            tit = String.valueOf(tit) + " (" + this.bpm + " bpm)";
            this.info.setVisibility(0);
            this.seek.setVisibility(0);
            this.seek.setProgress(this.bpm - 30);
        } else {
            this.seek.setVisibility(8);
        }
        if (!(this.mode == 4 || this.mode == 5)) {
            this.info.setVisibility(8);
        }
        if (this.mode == 5) {
            this.button1.setVisibility(8);
        } else {
            this.button1.setVisibility(0);
        }
        setTitle(tit);
        if (this.resetbutton) {
            this.button3.setVisibility(0);
        } else {
            this.button3.setVisibility(8);
        }
        this.fondo.setBackgroundColor(Color.parseColor(this.color));
        resetCrono();
        setCrono();
        showCrono();
        if (this.ads) {
            this.mWebView.loadUrl("http://info.u440.com/ads.php?mode=" + this.mode);
            this.ads = false;
        }
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
        showExtra(position);
        super.onListItemClick(l, v, position, id);
    }

    public void addItem(int a, String b, String c, int d) {
        Registro o1 = new Registro();
        o1.setLinea(a);
        o1.setTiempo(b);
        o1.setExtra(c);
        o1.setImagen(d);
        this.mRegistros.add(o1);
        this.mChronometer.notifyDataSetChanged();
    }

    public String seek(String b, String[] a, String[] c) {
        int y = 0;
        for (int x = 0; x < a.length; x++) {
            if (b.equals(a[x])) {
                y = x;
            }
        }
        return c[y];
    }

    public String seekInt(int b, String[] a, String[] c) {
        int y = -1;
        for (int x = 0; x < a.length; x++) {
            if (b <= Integer.parseInt(a[x]) && y == -1) {
                y = x;
            }
        }
        if (y < 0) {
            y = 0;
        }
        if (y > 8) {
            y = 8;
        }
        return c[y];
    }

    /* access modifiers changed from: package-private */
    public String clip(int md, boolean bcr) {
        String clip;
        String sep = "\t";
        String clip2 = "";
        String extra = "bib";
        String cr = "\n";
        if (md == 1) {
            sep = String.valueOf(this.csvsep) + " ";
        }
        if (md == 2) {
            sep = " ";
        }
        if (bcr) {
            cr = "\r\n";
        }
        if (this.mode == 2) {
            extra = this.units;
        }
        if (this.mode == 3) {
            extra = this.symbol;
        }
        if (this.mode == 4) {
            extra = getResources().getString(R.string.player);
        }
        String split = getResources().getString(R.string.split);
        if (md != 2) {
            clip2 = "#" + sep + split + sep + extra + cr;
        }
        Iterator<Registro> it = this.mRegistros.iterator();
        while (it.hasNext()) {
            Registro s = it.next();
            if (md == 2) {
                String bib = s.getExtra();
                if (bib == "") {
                    bib = "0";
                }
                String[] tm = TextUtils.split(s.getTiempo(), ":");
                clip = String.valueOf(clip) + bib + sep + this.n2.format((long) (Integer.parseInt(tm[0]) / 60)) + ":" + this.n2.format((long) (Integer.parseInt(tm[0]) % 60)) + ":" + tm[1] + cr;
            } else {
                clip = String.valueOf(clip) + s.getLinea() + sep + s.getTiempo() + sep + s.getExtra() + cr;
            }
        }
        if (md == 1) {
            return clip.replace(this.symbol, this.isymbol);
        }
        return clip;
    }

    public void saveDialog() {
        final SaveDialog saveas = new SaveDialog(this);
        saveas.setName("Chrono-" + ((String) DateFormat.format("yyyy-MM-dd-kk-mm-ss", new Date())));
        saveas.setButton(new View.OnClickListener() {
            public void onClick(View view) {
                chronometer.this.save(saveas.getName(), saveas.getMode());
                saveas.dismiss();
            }
        });
        saveas.show();
    }

    public void save(String filename, int md) {
        String filename2;
        if (!"mounted".equals(Environment.getExternalStorageState())) {
            Toast.makeText(this, getResources().getText(R.string.sderr), 1).show();
            return;
        }
        new File(getResources().getString(R.string.sddir)).mkdirs();
        if (md == 1) {
            filename2 = String.valueOf(getResources().getString(R.string.sddir)) + filename + ".csv";
        } else {
            filename2 = String.valueOf(getResources().getString(R.string.sddir)) + filename + ".txt";
        }
        try {
            PrintWriter fos = new PrintWriter(new FileWriter(filename2));
            fos.write(clip(md, true));
            fos.flush();
            fos.close();
            Toast.makeText(this, getResources().getText(R.string.sdok), 0).show();
        } catch (Exception e) {
            Toast.makeText(this, getResources().getText(R.string.sderr), 1).show();
        }
    }

    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            try {
                setScreen(this.displayon && !this.stopped);
                if (this.mSensorListener != null) {
                    this.mSensorManager.registerListener(this.mSensorListener, this.mSensorManager.getDefaultSensor(1), 1);
                    this.mSensorManager.registerListener(this.mSensorListener, this.mSensorManager.getDefaultSensor(2), 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            setScreen(false);
            if (this.mSensorListener != null) {
                this.mSensorManager.unregisterListener(this.mSensorListener);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.mReloj != null) {
            this.mReloj.cancel();
        }
        if (this.sounds != null) {
            this.sounds.release();
        }
        this.sounds = null;
        setScreen(false);
        if (this.mSensorListener != null) {
            this.mSensorManager.unregisterListener(this.mSensorListener);
        }
        this.mSensorListener = null;
        this.pf.unregisterOnSharedPreferenceChangeListener(this);
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    public void setScreen(boolean status) {
        if (this.wl != null) {
            if (status) {
                try {
                    if (!this.wl.isHeld()) {
                        this.wl.acquire();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (this.wl.isHeld()) {
                this.wl.release();
            }
        }
    }

    public class IconListViewAdapter extends ArrayAdapter<Registro> {
        private ArrayList<Registro> items;

        public IconListViewAdapter(Context context, int textViewResourceId, ArrayList<Registro> items2) {
            super(context, textViewResourceId, items2);
            this.items = items2;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = ((LayoutInflater) chronometer.this.getSystemService("layout_inflater")).inflate((int) R.layout.list_row, (ViewGroup) null);
            }
            Registro o = this.items.get(position);
            if (o != null) {
                TextView tt1 = (TextView) v.findViewById(R.id.text1);
                TextView tt2 = (TextView) v.findViewById(R.id.text2);
                TextView tt3 = (TextView) v.findViewById(R.id.text3);
                ImageView im = (ImageView) v.findViewById(R.id.image);
                if (im != null) {
                    im.setImageResource(o.getImagen());
                }
                if (tt1 != null) {
                    tt1.setText(new StringBuilder().append(o.getLinea()).toString());
                }
                if (tt2 != null) {
                    tt2.setText(o.getTiempo());
                }
                if (tt3 != null) {
                    tt3.setText(o.getExtra());
                }
            }
            return v;
        }
    }
}
