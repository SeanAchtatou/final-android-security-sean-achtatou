package com.u440.chronometer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RangeDialog extends Dialog implements DialogInterface.OnClickListener {
    Button buttonC;
    Button buttonOK;
    Button masmin;
    Button menmin;
    TextView min;
    int step = 1;
    int value = 50;
    int vmax = 0;
    int vmin = 100;

    public RangeDialog(Context context, String tit, int vmx, int vmn, int stp, int val) {
        super(context);
        setContentView((int) R.layout.range_dialog);
        this.vmax = vmx;
        this.vmin = vmn;
        this.value = val;
        this.step = stp;
        setTitle(tit);
        this.buttonOK = (Button) findViewById(R.id.ok);
        this.buttonC = (Button) findViewById(R.id.cancel);
        this.buttonC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RangeDialog.this.dismiss();
            }
        });
        this.masmin = (Button) findViewById(R.id.masmin);
        this.masmin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RangeDialog.this.value += RangeDialog.this.step;
                if (RangeDialog.this.value > RangeDialog.this.vmax) {
                    RangeDialog.this.value = RangeDialog.this.vmax;
                }
                RangeDialog.this.showValue();
            }
        });
        this.menmin = (Button) findViewById(R.id.menmin);
        this.menmin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RangeDialog.this.value -= RangeDialog.this.step;
                if (RangeDialog.this.value < RangeDialog.this.vmin) {
                    RangeDialog.this.value = RangeDialog.this.vmin;
                }
                RangeDialog.this.showValue();
            }
        });
        this.min = (TextView) findViewById(R.id.min);
        showValue();
    }

    /* access modifiers changed from: package-private */
    public void setButton(View.OnClickListener ck) {
        this.buttonOK.setOnClickListener(ck);
    }

    /* access modifiers changed from: package-private */
    public void showValue() {
        this.min.setText(Integer.toString(this.value));
    }

    /* access modifiers changed from: package-private */
    public int getValue() {
        return this.value;
    }

    public void onClick(DialogInterface dialog, int which) {
        dismiss();
    }
}
