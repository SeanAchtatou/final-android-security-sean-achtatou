package com.u440.chronometer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

public class SaveDialog extends Dialog implements DialogInterface.OnClickListener {
    Button buttonC = ((Button) findViewById(R.id.cancel));
    Button buttonOK = ((Button) findViewById(R.id.ok));
    TextView filename;
    RadioGroup radio;

    public SaveDialog(Context context) {
        super(context);
        setContentView((int) R.layout.save_dialog);
        setTitle((int) R.string.sdsave);
        this.buttonC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SaveDialog.this.dismiss();
            }
        });
        this.filename = (TextView) findViewById(R.id.filename);
        this.radio = (RadioGroup) findViewById(R.id.radio);
    }

    /* access modifiers changed from: package-private */
    public void setButton(View.OnClickListener ck) {
        this.buttonOK.setOnClickListener(ck);
    }

    /* access modifiers changed from: package-private */
    public void setName(String s) {
        this.filename.setText(s);
    }

    /* access modifiers changed from: package-private */
    public String getName() {
        return this.filename.getText().toString();
    }

    /* access modifiers changed from: package-private */
    public int getMode() {
        if (this.radio.getCheckedRadioButtonId() == R.id.radio_txt) {
            return 0;
        }
        if (this.radio.getCheckedRadioButtonId() == R.id.radio_csv) {
            return 1;
        }
        return 2;
    }

    public void onClick(DialogInterface dialog, int which) {
        dismiss();
    }
}
