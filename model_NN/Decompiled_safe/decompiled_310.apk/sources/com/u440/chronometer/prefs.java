package com.u440.chronometer;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class prefs extends PreferenceActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPreferenceManager().setSharedPreferencesName(chronometer.PREFS_NAME);
        addPreferencesFromResource(R.xml.preferences);
    }
}
