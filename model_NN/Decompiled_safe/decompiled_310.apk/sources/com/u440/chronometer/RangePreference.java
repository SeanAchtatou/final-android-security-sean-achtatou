package com.u440.chronometer;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;

public class RangePreference extends Preference {
    int interval = 1;
    int maximum = 100;
    int minimum = 0;
    /* access modifiers changed from: private */
    public int oldValue = 50;
    String units = "";

    public RangePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RangePreference);
        this.maximum = typedArray.getInteger(0, 100);
        this.minimum = typedArray.getInteger(1, 0);
        this.interval = typedArray.getInteger(2, 1);
        this.units = typedArray.getString(3);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void onClick() {
        final RangeDialog myDialog = new RangeDialog(getContext(), this.units, this.maximum, this.minimum, this.interval, this.oldValue);
        myDialog.setButton(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    RangePreference.this.oldValue = myDialog.getValue();
                    RangePreference.this.updatePreference(RangePreference.this.oldValue);
                    myDialog.dismiss();
                } catch (Exception e) {
                }
            }
        });
        myDialog.show();
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray ta, int index) {
        return Integer.valueOf(ta.getInt(index, 50));
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        int temp = restoreValue ? getPersistedInt(50) : ((Integer) defaultValue).intValue();
        if (!restoreValue) {
            persistInt(temp);
        }
        this.oldValue = temp;
    }

    /* access modifiers changed from: private */
    public void updatePreference(int newValue) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(getKey(), newValue);
        editor.commit();
    }
}
