package com.u440.chronometer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Help extends Activity {
    final Activity activity = this;
    WebView mWebView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        this.mWebView = (WebView) findViewById(R.id.help);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.setWebViewClient(new HelloWebViewClient(this, null));
        this.mWebView.loadUrl("file:///android_asset/crono.tpl");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.mWebView.canGoBack()) {
            return super.onKeyDown(keyCode, event);
        }
        this.mWebView.goBack();
        return true;
    }

    private class HelloWebViewClient extends WebViewClient {
        private HelloWebViewClient() {
        }

        /* synthetic */ HelloWebViewClient(Help help, HelloWebViewClient helloWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith("mailto:")) {
                String[] blah_email = url.split(":");
                Intent emailIntent = new Intent("android.intent.action.SEND");
                emailIntent.setType("text/plain");
                emailIntent.putExtra("android.intent.extra.EMAIL", new String[]{blah_email[1]});
                emailIntent.putExtra("android.intent.extra.SUBJECT", "");
                Help.this.startActivity(emailIntent);
            } else {
                view.loadUrl(url);
            }
            return true;
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Toast.makeText(Help.this.activity, "Oh no! " + description, 0).show();
        }
    }
}
