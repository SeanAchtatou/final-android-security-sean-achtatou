package com.u440.chronometer;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

public class SeekBarPreference extends Preference implements SeekBar.OnSeekBarChangeListener {
    public static int interval = 5;
    public static int maximum = 100;
    public static int minimum = 0;
    private TextView monitorBox;
    private float oldValue = 50.0f;
    String units = "";

    public SeekBarPreference(Context context) {
        super(context);
    }

    public SeekBarPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RangePreference);
        maximum = typedArray.getInteger(0, 0);
        minimum = typedArray.getInteger(1, 100);
        interval = typedArray.getInteger(2, 1);
        this.units = typedArray.getString(3);
        typedArray.recycle();
    }

    public SeekBarPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /* access modifiers changed from: protected */
    public View onCreateView(ViewGroup parent) {
        LinearLayout layout = new LinearLayout(getContext());
        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(-2, -2);
        params1.gravity = 3;
        params1.weight = 1.0f;
        LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(80, -2);
        params2.gravity = 5;
        LinearLayout.LayoutParams params3 = new LinearLayout.LayoutParams(30, -2);
        params3.gravity = 17;
        layout.setPadding(15, 5, 10, 5);
        layout.setOrientation(0);
        TextView view = new TextView(getContext());
        view.setText(getTitle());
        view.setTextSize(18.0f);
        view.setTypeface(Typeface.SANS_SERIF, 1);
        view.setGravity(3);
        view.setLayoutParams(params1);
        SeekBar bar = new SeekBar(getContext());
        bar.setMax(maximum);
        bar.setProgress((int) this.oldValue);
        bar.setLayoutParams(params2);
        bar.setOnSeekBarChangeListener(this);
        this.monitorBox = new TextView(getContext());
        this.monitorBox.setTextSize(12.0f);
        this.monitorBox.setTypeface(Typeface.MONOSPACE, 2);
        this.monitorBox.setLayoutParams(params3);
        this.monitorBox.setPadding(2, 5, 0, 0);
        this.monitorBox.setText(new StringBuilder(String.valueOf(bar.getProgress())).toString());
        layout.addView(view);
        layout.addView(bar);
        layout.addView(this.monitorBox);
        layout.setId(16908312);
        return layout;
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int progress2 = Math.round(((float) progress) / ((float) interval)) * interval;
        if (!callChangeListener(Integer.valueOf(progress2))) {
            seekBar.setProgress((int) this.oldValue);
            return;
        }
        seekBar.setProgress(progress2);
        this.oldValue = (float) progress2;
        this.monitorBox.setText(new StringBuilder(String.valueOf(progress2)).toString());
        updatePreference(progress2);
        notifyChanged();
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    /* access modifiers changed from: protected */
    public Object onGetDefaultValue(TypedArray ta, int index) {
        return Integer.valueOf(validateValue(ta.getInt(index, 50)));
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        int temp = restoreValue ? getPersistedInt(50) : ((Integer) defaultValue).intValue();
        if (!restoreValue) {
            persistInt(temp);
        }
        this.oldValue = (float) temp;
    }

    private int validateValue(int value) {
        if (value > maximum) {
            return maximum;
        }
        if (value < 0) {
            return 0;
        }
        if (value % interval != 0) {
            return Math.round(((float) value) / ((float) interval)) * interval;
        }
        return value;
    }

    private void updatePreference(int newValue) {
        SharedPreferences.Editor editor = getEditor();
        editor.putInt(getKey(), newValue);
        editor.commit();
    }
}
