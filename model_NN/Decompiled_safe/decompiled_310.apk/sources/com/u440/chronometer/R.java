package com.u440.chronometer;

public final class R {

    public static final class array {
        public static final int chess_array = 2131230726;
        public static final int chess_arrayv = 2131230727;
        public static final int color_array = 2131230722;
        public static final int color_arrayv = 2131230723;
        public static final int dist_array = 2131230725;
        public static final int met_array = 2131230728;
        public static final int met_arrayv = 2131230729;
        public static final int mode_array = 2131230720;
        public static final int mode_arrayv = 2131230721;
        public static final int skin_array = 2131230730;
        public static final int skin_arrayv = 2131230731;
        public static final int units_array = 2131230724;
    }

    public static final class attr {
        public static final int maximumValue = 2130771968;
        public static final int minimumValue = 2130771969;
        public static final int stepSize = 2130771970;
        public static final int units = 2130771971;
    }

    public static final class drawable {
        public static final int btn_black = 2130837504;
        public static final int ic_crono = 2130837505;
        public static final int ic_crono_s = 2130837506;
        public static final int ic_menu_apps = 2130837507;
        public static final int ic_menu_delete = 2130837508;
        public static final int ic_menu_edit = 2130837509;
        public static final int ic_menu_help = 2130837510;
        public static final int ic_menu_more = 2130837511;
        public static final int ic_menu_preferences = 2130837512;
        public static final int ic_menu_save = 2130837513;
        public static final int ic_menu_send = 2130837514;
        public static final int ic_menu_upload = 2130837515;
        public static final int ic_money = 2130837516;
        public static final int ic_peon_blanco = 2130837517;
        public static final int ic_peon_negro = 2130837518;
        public static final int keyboard_delete = 2130837519;
    }

    public static final class id {
        public static final int ads = 2131361852;
        public static final int android_fondo = 2131361851;
        public static final int button0 = 2131361813;
        public static final int button1 = 2131361815;
        public static final int button2 = 2131361816;
        public static final int button3 = 2131361817;
        public static final int button4 = 2131361818;
        public static final int button5 = 2131361821;
        public static final int button6 = 2131361822;
        public static final int button7 = 2131361823;
        public static final int button8 = 2131361824;
        public static final int button9 = 2131361858;
        public static final int buttonAdm = 2131361830;
        public static final int buttonB = 2131361800;
        public static final int buttonDel = 2131361795;
        public static final int buttonEqual = 2131361828;
        public static final int buttonK = 2131361797;
        public static final int buttonMin = 2131361819;
        public static final int buttonN = 2131361801;
        public static final int buttonPls = 2131361825;
        public static final int buttonQ = 2131361798;
        public static final int buttonQm = 2131361829;
        public static final int buttonR = 2131361799;
        public static final int buttonSpc = 2131361807;
        public static final int buttonSrp = 2131361831;
        public static final int buttona = 2131361803;
        public static final int buttonb = 2131361804;
        public static final int buttonc = 2131361805;
        public static final int buttond = 2131361806;
        public static final int buttone = 2131361809;
        public static final int buttonf = 2131361810;
        public static final int buttong = 2131361811;
        public static final int buttonh = 2131361812;
        public static final int buttonx = 2131361827;
        public static final int cancel = 2131361835;
        public static final int clear = 2131361834;
        public static final int copy = 2131361867;
        public static final int dir = 2131361860;
        public static final int eggtimer = 2131361869;
        public static final int filename = 2131361859;
        public static final int help = 2131361846;
        public static final int image = 2131361850;
        public static final int info = 2131361854;
        public static final int masmin = 2131361836;
        public static final int masseg = 2131361839;
        public static final int menmin = 2131361838;
        public static final int menseg = 2131361841;
        public static final int min = 2131361837;
        public static final int ok = 2131361833;
        public static final int preferences = 2131361866;
        public static final int radio = 2131361842;
        public static final int radio_csv = 2131361862;
        public static final int radio_down = 2131361843;
        public static final int radio_stop = 2131361845;
        public static final int radio_txt = 2131361861;
        public static final int radio_txt2 = 2131361863;
        public static final int radio_up = 2131361844;
        public static final int reset = 2131361857;
        public static final int save = 2131361865;
        public static final int seek = 2131361853;
        public static final int seg = 2131361840;
        public static final int send = 2131361868;
        public static final int settime = 2131361864;
        public static final int start = 2131361856;
        public static final int tableLayout1 = 2131361792;
        public static final int tableRow0 = 2131361796;
        public static final int tableRow1 = 2131361802;
        public static final int tableRow2 = 2131361808;
        public static final int tableRow3 = 2131361814;
        public static final int tableRow4 = 2131361820;
        public static final int tableRow5 = 2131361826;
        public static final int tableRow6 = 2131361832;
        public static final int text = 2131361794;
        public static final int text1 = 2131361847;
        public static final int text2 = 2131361848;
        public static final int text3 = 2131361849;
        public static final int texto = 2131361793;
        public static final int timer = 2131361855;
    }

    public static final class layout {
        public static final int chess_key = 2130903040;
        public static final int crono_dialog = 2130903041;
        public static final int help = 2130903042;
        public static final int list_row = 2130903043;
        public static final int main = 2130903044;
        public static final int num_key = 2130903045;
        public static final int range_dialog = 2130903046;
        public static final int save_dialog = 2130903047;
    }

    public static final class menu {
        public static final int menu = 2131296256;
    }

    public static final class raw {
        public static final int beepcorto = 2131034112;
        public static final int beeplargo = 2131034113;
        public static final int dinero = 2131034114;
        public static final int metronome = 2131034115;
        public static final int ring = 2131034116;
    }

    public static final class string {
        public static final int Chess_b = 2131165268;
        public static final int Chess_k = 2131165265;
        public static final int Chess_n = 2131165269;
        public static final int Chess_q = 2131165266;
        public static final int Chess_r = 2131165267;
        public static final int app_name = 2131165185;
        public static final int bib = 2131165233;
        public static final int bpm_prompt = 2131165274;
        public static final int bpm_summary = 2131165275;
        public static final int bpm_title = 2131165273;
        public static final int cancel = 2131165230;
        public static final int cashclock_cat = 2131165244;
        public static final int chess_prompt = 2131165250;
        public static final int chess_summary = 2131165251;
        public static final int chess_time_prompt = 2131165256;
        public static final int chess_time_summary = 2131165257;
        public static final int chess_time_title = 2131165255;
        public static final int chess_title = 2131165249;
        public static final int chessclock_cat = 2131165248;
        public static final int chronometer_done = 2131165190;
        public static final int chronometer_end = 2131165264;
        public static final int chronometer_reset = 2131165189;
        public static final int chronometer_start = 2131165187;
        public static final int chronometer_stop = 2131165188;
        public static final int chronometer_timer = 2131165186;
        public static final int clear = 2131165270;
        public static final int color_prompt = 2131165201;
        public static final int color_summary = 2131165202;
        public static final int color_title = 2131165200;
        public static final int copied = 2131165221;
        public static final int copy = 2131165220;
        public static final int cost_prompt = 2131165246;
        public static final int cost_summary = 2131165247;
        public static final int cost_title = 2131165245;
        public static final int countdown = 2131165234;
        public static final int countup = 2131165235;
        public static final int delay_prompt = 2131165253;
        public static final int delay_summary = 2131165254;
        public static final int delay_title = 2131165252;
        public static final int device_cat = 2131165199;
        public static final int display_prompt = 2131165207;
        public static final int display_summary = 2131165208;
        public static final int distance_prompt = 2131165242;
        public static final int distance_summary = 2131165243;
        public static final int distance_title = 2131165241;
        public static final int eggtimer = 2131165213;
        public static final int hello = 2131165184;
        public static final int help = 2131165223;
        public static final int help_name = 2131165216;
        public static final int help_url = 2131165212;
        public static final int metronome_cat = 2131165272;
        public static final int minutes_prompt = 2131165218;
        public static final int mode_cat = 2131165191;
        public static final int mode_prompt = 2131165193;
        public static final int mode_summary = 2131165194;
        public static final int mode_title = 2131165192;
        public static final int move = 2131165271;
        public static final int ok = 2131165229;
        public static final int player = 2131165231;
        public static final int player1_prompt = 2131165259;
        public static final int player1_summary = 2131165260;
        public static final int player1_title = 2131165258;
        public static final int player2_prompt = 2131165262;
        public static final int player2_summary = 2131165263;
        public static final int player2_title = 2131165261;
        public static final int preferences = 2131165214;
        public static final int prefs_name = 2131165215;
        public static final int resetbutton_prompt = 2131165197;
        public static final int resetbutton_summary = 2131165198;
        public static final int save = 2131165219;
        public static final int sddir = 2131165227;
        public static final int sderr = 2131165224;
        public static final int sdok = 2131165225;
        public static final int sdsave = 2131165226;
        public static final int select = 2131165228;
        public static final int send = 2131165222;
        public static final int settime_prompt = 2131165217;
        public static final int shake_prompt = 2131165195;
        public static final int shake_summary = 2131165196;
        public static final int skin_prompt = 2131165277;
        public static final int skin_summary = 2131165278;
        public static final int skin_title = 2131165276;
        public static final int soundon_prompt = 2131165203;
        public static final int soundon_summary = 2131165204;
        public static final int split = 2131165232;
        public static final int stopwatch = 2131165236;
        public static final int tachymeter_cat = 2131165237;
        public static final int units_prompt = 2131165239;
        public static final int units_summary = 2131165240;
        public static final int units_title = 2131165238;
        public static final int vibration_prompt = 2131165205;
        public static final int vibration_summary = 2131165206;
        public static final int web_prompt = 2131165210;
        public static final int web_summary = 2131165211;
        public static final int web_title = 2131165209;
    }

    public static final class style {
        public static final int ButtonText = 2131099648;
    }

    public static final class styleable {
        public static final int[] RangePreference = {R.attr.maximumValue, R.attr.minimumValue, R.attr.stepSize, R.attr.units};
        public static final int RangePreference_maximumValue = 0;
        public static final int RangePreference_minimumValue = 1;
        public static final int RangePreference_stepSize = 2;
        public static final int RangePreference_units = 3;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
