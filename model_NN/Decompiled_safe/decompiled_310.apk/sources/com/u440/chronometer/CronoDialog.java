package com.u440.chronometer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;

public class CronoDialog extends Dialog implements DialogInterface.OnClickListener {
    Button buttonC = ((Button) findViewById(R.id.cancel));
    Button buttonOK = ((Button) findViewById(R.id.ok));
    Button masmin;
    Button masseg;
    Button menmin;
    Button menseg;
    TextView min;
    RadioGroup radio;
    TextView seg;

    public CronoDialog(Context context) {
        super(context);
        setContentView((int) R.layout.crono_dialog);
        setTitle((int) R.string.minutes_prompt);
        this.buttonC.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CronoDialog.this.dismiss();
            }
        });
        this.masmin = (Button) findViewById(R.id.masmin);
        this.masmin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CronoDialog.this.min.setText(Integer.toString(Integer.parseInt(CronoDialog.this.min.getText().toString()) + 1));
            }
        });
        this.menmin = (Button) findViewById(R.id.menmin);
        this.menmin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int m = Integer.parseInt(CronoDialog.this.min.getText().toString()) - 1;
                if (m < 0) {
                    m = 0;
                }
                CronoDialog.this.min.setText(Integer.toString(m));
            }
        });
        this.masseg = (Button) findViewById(R.id.masseg);
        this.masseg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int m = Integer.parseInt(CronoDialog.this.seg.getText().toString()) + 1;
                if (m > 59) {
                    m = 0;
                }
                CronoDialog.this.seg.setText(Integer.toString(m));
            }
        });
        this.menseg = (Button) findViewById(R.id.menseg);
        this.menseg.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int m = Integer.parseInt(CronoDialog.this.seg.getText().toString()) - 1;
                if (m < 0) {
                    m = 59;
                }
                CronoDialog.this.seg.setText(Integer.toString(m));
            }
        });
        this.min = (TextView) findViewById(R.id.min);
        this.seg = (TextView) findViewById(R.id.seg);
        this.radio = (RadioGroup) findViewById(R.id.radio);
    }

    /* access modifiers changed from: package-private */
    public void setButton(View.OnClickListener ck) {
        this.buttonOK.setOnClickListener(ck);
    }

    /* access modifiers changed from: package-private */
    public void setCrono(int m, int s) {
        this.min.setText(Integer.toString(m));
        this.seg.setText(Integer.toString(s));
    }

    /* access modifiers changed from: package-private */
    public int getSeg() {
        return Integer.parseInt(this.seg.getText().toString());
    }

    /* access modifiers changed from: package-private */
    public int getMin() {
        return Integer.parseInt(this.min.getText().toString());
    }

    /* access modifiers changed from: package-private */
    public String getMode() {
        if (getSeg() <= 0 && getMin() <= 0) {
            return "0";
        }
        if (this.radio.getCheckedRadioButtonId() == R.id.radio_down) {
            return "1";
        }
        if (this.radio.getCheckedRadioButtonId() == R.id.radio_up) {
            return "6";
        }
        return "0";
    }

    public void onClick(DialogInterface dialog, int which) {
        dismiss();
    }
}
