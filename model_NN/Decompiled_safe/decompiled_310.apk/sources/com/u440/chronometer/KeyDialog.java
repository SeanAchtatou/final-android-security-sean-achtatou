package com.u440.chronometer;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class KeyDialog extends Dialog implements View.OnClickListener {
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    Button b5;
    Button b6;
    Button b7;
    Button b8;
    Button b9;
    Button bB;
    Button bK;
    Button bN;
    Button bQ;
    Button bR;
    Button ba;
    Button bb;
    Button bc;
    Button bd;
    Button be;
    Button bf;
    Button bg;
    Button bh;
    Button buttonAdm;
    Button buttonC;
    Button buttonClr;
    ImageButton buttonDel;
    Button buttonEqual;
    Button buttonOK;
    Button buttonPls;
    Button buttonQm;
    Button buttonR1;
    Button buttonR2;
    Button buttonSpc;
    Button buttonSrp;
    Button bx;
    EditText text;

    public KeyDialog(Context context, int mode, int tit) {
        super(context);
        if (mode == 1) {
            setContentView((int) R.layout.chess_key);
        } else {
            setContentView((int) R.layout.num_key);
        }
        getWindow().setLayout(-1, -1);
        setTitle(tit);
        this.text = (EditText) findViewById(R.id.text);
        this.text.setCursorVisible(false);
        this.buttonOK = (Button) findViewById(R.id.ok);
        this.buttonC = (Button) findViewById(R.id.cancel);
        this.buttonC.setOnClickListener(this);
        this.buttonDel = (ImageButton) findViewById(R.id.buttonDel);
        this.buttonDel.setOnClickListener(this);
        this.buttonClr = (Button) findViewById(R.id.clear);
        this.buttonClr.setOnClickListener(this);
        this.buttonSpc = (Button) findViewById(R.id.buttonSpc);
        this.buttonSpc.setOnClickListener(this);
        this.b1 = (Button) findViewById(R.id.button1);
        this.b1.setOnClickListener(this);
        this.b2 = (Button) findViewById(R.id.button2);
        this.b2.setOnClickListener(this);
        this.b3 = (Button) findViewById(R.id.button3);
        this.b3.setOnClickListener(this);
        this.b4 = (Button) findViewById(R.id.button4);
        this.b4.setOnClickListener(this);
        this.b5 = (Button) findViewById(R.id.button5);
        this.b5.setOnClickListener(this);
        this.b6 = (Button) findViewById(R.id.button6);
        this.b6.setOnClickListener(this);
        this.b7 = (Button) findViewById(R.id.button7);
        this.b7.setOnClickListener(this);
        this.b8 = (Button) findViewById(R.id.button8);
        this.b8.setOnClickListener(this);
        this.buttonR1 = (Button) findViewById(R.id.button0);
        this.buttonR1.setOnClickListener(this);
        this.buttonR2 = (Button) findViewById(R.id.buttonMin);
        this.buttonR2.setOnClickListener(this);
        if (mode == 2) {
            this.b9 = (Button) findViewById(R.id.button9);
            this.b9.setOnClickListener(this);
        }
        if (mode == 1) {
            this.buttonEqual = (Button) findViewById(R.id.buttonEqual);
            this.buttonEqual.setOnClickListener(this);
            this.buttonSrp = (Button) findViewById(R.id.buttonSrp);
            this.buttonSrp.setOnClickListener(this);
            this.buttonQm = (Button) findViewById(R.id.buttonQm);
            this.buttonQm.setOnClickListener(this);
            this.buttonAdm = (Button) findViewById(R.id.buttonAdm);
            this.buttonAdm.setOnClickListener(this);
            this.buttonPls = (Button) findViewById(R.id.buttonPls);
            this.buttonPls.setOnClickListener(this);
            this.bx = (Button) findViewById(R.id.buttonx);
            this.bx.setOnClickListener(this);
            this.ba = (Button) findViewById(R.id.buttona);
            this.ba.setOnClickListener(this);
            this.bb = (Button) findViewById(R.id.buttonb);
            this.bb.setOnClickListener(this);
            this.bc = (Button) findViewById(R.id.buttonc);
            this.bc.setOnClickListener(this);
            this.bd = (Button) findViewById(R.id.buttond);
            this.bd.setOnClickListener(this);
            this.be = (Button) findViewById(R.id.buttone);
            this.be.setOnClickListener(this);
            this.bf = (Button) findViewById(R.id.buttonf);
            this.bf.setOnClickListener(this);
            this.bg = (Button) findViewById(R.id.buttong);
            this.bg.setOnClickListener(this);
            this.bh = (Button) findViewById(R.id.buttonh);
            this.bh.setOnClickListener(this);
            this.bK = (Button) findViewById(R.id.buttonK);
            this.bK.setOnClickListener(this);
            this.bQ = (Button) findViewById(R.id.buttonQ);
            this.bQ.setOnClickListener(this);
            this.bR = (Button) findViewById(R.id.buttonR);
            this.bR.setOnClickListener(this);
            this.bB = (Button) findViewById(R.id.buttonB);
            this.bB.setOnClickListener(this);
            this.bN = (Button) findViewById(R.id.buttonN);
            this.bN.setOnClickListener(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void setButton(View.OnClickListener ck) {
        this.buttonOK.setOnClickListener(ck);
    }

    /* access modifiers changed from: package-private */
    public void setKey(String m) {
        this.text.setText(m);
    }

    /* access modifiers changed from: package-private */
    public String getKey() {
        return this.text.getText().toString();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonDel:
                String s = this.text.getText().toString();
                if (s.length() > 0) {
                    this.text.setText(s.substring(0, s.length() - 1));
                    return;
                }
                return;
            case R.id.buttonSpc:
                this.text.setText(String.valueOf(this.text.getText().toString()) + " ");
                return;
            case R.id.clear:
                this.text.setText("");
                return;
            case R.id.cancel:
                dismiss();
                return;
            default:
                this.text.setText(String.valueOf(this.text.getText().toString()) + ((String) ((Button) v).getText()));
                return;
        }
    }
}
