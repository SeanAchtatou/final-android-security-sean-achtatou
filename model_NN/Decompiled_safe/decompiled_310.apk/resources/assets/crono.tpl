<h2>Help</h2>
<h3>Stopwatch & More 1.14 (Timer & Chronometer)</h3>
<p>
Report bugs & suggestions to: <a href="mailto:info@u440.com">info@u440.com</a>
</p>
<p>
<b>New in this version: Clock mode (Real time). New look for buttons.</b>
</p>
<p>
8 Modes timer: 
<a href="#stopwatch">Stopwatch</a>, 
<a href="#countdown">Countdown</a>, 
<a href="#tachymeter">Tachymeter</a>, 
<a href="#cashclock">Cash clock</a>, 
<a href="#chessclock">Chess clock</a>, 
<a href="#metronome">Metronome</a>, 
<a href="#countup">Count up</a>,
<a href="#clock">Clock</a>
</p>
Click on MENU KEY for:
<ul>
<li>Reset timer
<li>Set time for countdown/up mode (minutes/seconds)
<li>Save split list as file in SD Card (Export .txt, .csv & .txt runscore compatible)
<li>Set preferences
<li>Help
<li>Copy split list to clipboard
<li>Send split list by mail
</ul>
<p>
General Preferences:
<ul>
<li>Select mode: Stopwatch, Countdown, Tachymeter, Cash clock, Chess clock, Metronome, Count up, Clock
<li>Tilt device to split timer (on/off). Put your device upright and then tilt it to split timer, each time.
<li>Select background color
<li>Show reset button (on/off)
<li>Sound (on/off)
<li>Vibration on click (on/off)
<li>Prevent display off (on/off)
</ul>
</p>
<a name="stopwatch"></a>
<h3>Stopwatch</h3>
<ul>
<li>Select this mode in "MENU KEY>Preferences>Mode"
<li>Click on "Start" to run timer
<li>Click on timer to spilt time
<li>Add bib number by clicking on list item.
</ul>

<a name="countdown"></a>
<h3>Countdown</h3>
<ul>
<li>Click on "MENU KEY>Countdown/up" to set time & mode
<li>Click on "Start" to run timer
<li>Click on timer to spilt time
<li>Add bib number by clicking on list item.
</ul>

<a name="tachymeter"></a>
<h3>Tachymeter</h3>
<ul>
<li>Select this mode in "MENU KEY>Preferences>Mode"
<li>Select distance units & distance in "MENU KEY>Preferences>Tachymeter preferences"
<li>Click on "Start" to run timer
<li>Click on timer to spilt speed
</ul>

<a name="cashclock"></a>
<h3>Cash Clock</h3>
<ul>
<li>Select this mode in "MENU KEY>Preferences>Mode"
<li>Set rate in "MENU KEY>Preferences>Cash clock preferences" ($/hour)
<li>Click on "Start" to run timer
<li>Click on timer to spilt cash
</ul>

<a name="chessclock"></a>
<h3>Chess Clock</h3>
<ul>
<li>Select this mode in "MENU KEY>Preferences>Mode"
<li>Set options in "MENU KEY>Preferences>Chess clock preferences"
<li>4 modes: no delay, symple delay, Fisher, Fisher after, Bornstein
<li>Set chess time in minutes (game time).
<li>Set delay time in seconds
<li>You can change player1 and player2 names.
<li>Click on "Start" to run timer
<li>Click on timer to split PLAYER
<li>Add <a href="http://en.wikipedia.org/wiki/Algebraic_chess_notation">algebraic chess notation</a> by clicking on list item.
<li> More info <a href="http://en.wikipedia.org/wiki/Chess_clock">Wikipedia: Chess clock</a>
</ul>

<a name="metronome"></a>
<h3>Metronome</h3>
<ul>
<li>Select this mode in "MENU KEY>Preferences>Mode"
<li>Drag seekbar to set bpm (beats per minute)
<li>Click on "Start" to run beats
<li> More info <a href="http://en.wikipedia.org/wiki/Metronome">Wikipedia: Metronome</a>
</ul>

<a name="countup"></a>
<h3>Count up</h3>
<ul>
<li>Click on "MENU KEY>Countdown/up" to set time & mode
<li>Click on "Start" to run timer
<li>Click on timer to spilt time
<li>Add bib number by clicking on list item.
</ul>

<a name="clock"></a>
<h3>Clock</h3>
<ul>
<li>Click on "MENU KEY>Clock" to set mode
<li>Click on "Start" to run clock
<li>Click on timer to spilt time
<li>Add bib number by clicking on list item.
</ul>
