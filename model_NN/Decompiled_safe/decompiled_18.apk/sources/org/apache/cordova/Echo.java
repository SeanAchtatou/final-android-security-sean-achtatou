package org.apache.cordova;

import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class Echo extends Plugin {
    public PluginResult execute(String action, JSONArray args, String callbackId) {
        try {
            String result = args.getString(0);
            if ("echo".equals(action) || "echoAsync".equals(action)) {
                return new PluginResult(PluginResult.Status.OK, result);
            }
            return new PluginResult(PluginResult.Status.INVALID_ACTION);
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        return "echo".equals(action);
    }
}
