package org.apache.cordova;

import android.net.Proxy;
import com.adsdk.sdk.Const;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.UUID;

public class CallbackServer implements Runnable {
    private static final String LOG_TAG = "CallbackServer";
    static final String digits = "0123456789ABCDEF";
    private boolean active = false;
    private NativeToJsMessageQueue jsMessageQueue;
    private int port = 0;
    private Thread serverThread;
    private String token;
    private boolean usePolling = true;
    private ServerSocket waitSocket;

    public void init(String url) {
        stopServer();
        this.port = 0;
        if (url != null && !url.startsWith("file://")) {
            this.usePolling = true;
            stopServer();
        } else if (Proxy.getDefaultHost() != null) {
            this.usePolling = true;
            stopServer();
        } else {
            this.usePolling = false;
            startServer();
        }
    }

    public boolean usePolling() {
        return this.usePolling;
    }

    public int getPort() {
        return this.port;
    }

    public String getToken() {
        return this.token;
    }

    public void startServer() {
        this.active = false;
        this.serverThread = new Thread(this);
        this.serverThread.start();
    }

    public void restartServer() {
        stopServer();
        startServer();
    }

    public void run() {
        String response;
        try {
            this.active = true;
            this.waitSocket = new ServerSocket(0);
            this.port = this.waitSocket.getLocalPort();
            this.token = UUID.randomUUID().toString();
            while (this.active) {
                Socket connection = this.waitSocket.accept();
                BufferedReader xhrReader = new BufferedReader(new InputStreamReader(connection.getInputStream()), 40);
                DataOutputStream output = new DataOutputStream(connection.getOutputStream());
                String request = xhrReader.readLine();
                if (this.active && request != null) {
                    if (request.contains("GET")) {
                        String[] requestParts = request.split(" ");
                        if (requestParts.length != 3 || !requestParts[1].substring(1).equals(this.token)) {
                            response = "HTTP/1.1 403 Forbidden\r\n\r\n ";
                        } else {
                            String js = null;
                            synchronized (this) {
                                while (this.active && (this.jsMessageQueue == null || (js = this.jsMessageQueue.pop()) == null)) {
                                    try {
                                        wait(10000);
                                        break;
                                    } catch (Exception e) {
                                    }
                                }
                            }
                            if (!this.active) {
                                response = "HTTP/1.1 503 Service Unavailable\r\n\r\n ";
                            } else if (js == null) {
                                response = "HTTP/1.1 404 NO DATA\r\n\r\n ";
                            } else {
                                response = "HTTP/1.1 200 OK\r\n\r\n" + encode(js, Const.ENCODING);
                            }
                        }
                    } else {
                        response = "HTTP/1.1 400 Bad Request\r\n\r\n ";
                    }
                    output.writeBytes(response);
                    output.flush();
                }
                output.close();
                xhrReader.close();
            }
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        this.active = false;
    }

    public void stopServer() {
        if (this.active) {
            this.active = false;
            try {
                this.waitSocket.close();
            } catch (IOException e) {
            }
            synchronized (this) {
                notify();
            }
        }
    }

    public void destroy() {
        stopServer();
    }

    public void onNativeToJsMessageAvailable(NativeToJsMessageQueue queue) {
        synchronized (this) {
            this.jsMessageQueue = queue;
            notify();
        }
    }

    public static String encode(String s, String enc) throws UnsupportedEncodingException {
        if (s == null || enc == null) {
            throw new NullPointerException();
        }
        "".getBytes(enc);
        StringBuilder buf = new StringBuilder(s.length() + 16);
        int start = -1;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if ((ch >= 'a' && ch <= 'z') || ((ch >= 'A' && ch <= 'Z') || ((ch >= '0' && ch <= '9') || " .-*_'(),<>=?@[]{}:~\"\\/;!".indexOf(ch) > -1))) {
                if (start >= 0) {
                    convert(s.substring(start, i), buf, enc);
                    start = -1;
                }
                if (ch != ' ') {
                    buf.append(ch);
                } else {
                    buf.append(' ');
                }
            } else if (start < 0) {
                start = i;
            }
        }
        if (start >= 0) {
            convert(s.substring(start, s.length()), buf, enc);
        }
        return buf.toString();
    }

    private static void convert(String s, StringBuilder buf, String enc) throws UnsupportedEncodingException {
        byte[] bytes = s.getBytes(enc);
        for (int j = 0; j < bytes.length; j++) {
            buf.append('%');
            buf.append(digits.charAt((bytes[j] & 240) >> 4));
            buf.append(digits.charAt(bytes[j] & 15));
        }
    }
}
