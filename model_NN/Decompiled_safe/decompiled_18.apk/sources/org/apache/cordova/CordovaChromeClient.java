package org.apache.cordova;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.LOG;

public class CordovaChromeClient extends WebChromeClient {
    private long MAX_QUOTA = 104857600;
    private String TAG = "CordovaLog";
    private CordovaWebView appView;
    private CordovaInterface cordova;

    public CordovaChromeClient(CordovaInterface cordova2) {
        this.cordova = cordova2;
    }

    public CordovaChromeClient(CordovaInterface ctx, CordovaWebView app) {
        this.cordova = ctx;
        this.appView = app;
    }

    public void setWebView(CordovaWebView view) {
        this.appView = view;
    }

    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this.cordova.getActivity());
        dlg.setMessage(message);
        dlg.setTitle("Alert");
        dlg.setCancelable(true);
        dlg.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                result.confirm();
            }
        });
        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                result.cancel();
            }
        });
        dlg.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return true;
                }
                result.confirm();
                return false;
            }
        });
        dlg.create();
        dlg.show();
        return true;
    }

    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
        AlertDialog.Builder dlg = new AlertDialog.Builder(this.cordova.getActivity());
        dlg.setMessage(message);
        dlg.setTitle("Confirm");
        dlg.setCancelable(true);
        dlg.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                result.confirm();
            }
        });
        dlg.setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                result.cancel();
            }
        });
        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                result.cancel();
            }
        });
        dlg.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode != 4) {
                    return true;
                }
                result.cancel();
                return false;
            }
        });
        dlg.create();
        dlg.show();
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001f, code lost:
        if (r14.appView.isUrlWhiteListed(r16) == false) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onJsPrompt(android.webkit.WebView r15, java.lang.String r16, java.lang.String r17, java.lang.String r18, android.webkit.JsPromptResult r19) {
        /*
            r14 = this;
            r12 = 0
            java.lang.String r1 = "file://"
            r0 = r16
            boolean r1 = r0.startsWith(r1)
            if (r1 != 0) goto L_0x0021
            org.apache.cordova.CordovaWebView r1 = r14.appView
            java.lang.String r1 = r1.baseUrl
            r0 = r16
            int r1 = r0.indexOf(r1)
            if (r1 == 0) goto L_0x0021
            org.apache.cordova.CordovaWebView r1 = r14.appView
            r0 = r16
            boolean r1 = r1.isUrlWhiteListed(r0)
            if (r1 == 0) goto L_0x0022
        L_0x0021:
            r12 = 1
        L_0x0022:
            if (r12 == 0) goto L_0x007c
            if (r18 == 0) goto L_0x007c
            int r1 = r18.length()
            r5 = 3
            if (r1 <= r5) goto L_0x007c
            r1 = 0
            r5 = 4
            r0 = r18
            java.lang.String r1 = r0.substring(r1, r5)
            java.lang.String r5 = "gap:"
            boolean r1 = r1.equals(r5)
            if (r1 == 0) goto L_0x007c
            org.json.JSONArray r7 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0077 }
            r1 = 4
            r0 = r18
            java.lang.String r1 = r0.substring(r1)     // Catch:{ JSONException -> 0x0077 }
            r7.<init>(r1)     // Catch:{ JSONException -> 0x0077 }
            r1 = 0
            java.lang.String r2 = r7.getString(r1)     // Catch:{ JSONException -> 0x0077 }
            r1 = 1
            java.lang.String r3 = r7.getString(r1)     // Catch:{ JSONException -> 0x0077 }
            r1 = 2
            java.lang.String r4 = r7.getString(r1)     // Catch:{ JSONException -> 0x0077 }
            r1 = 3
            boolean r6 = r7.getBoolean(r1)     // Catch:{ JSONException -> 0x0077 }
            org.apache.cordova.CordovaWebView r1 = r14.appView     // Catch:{ JSONException -> 0x0077 }
            org.apache.cordova.api.PluginManager r1 = r1.pluginManager     // Catch:{ JSONException -> 0x0077 }
            r5 = r17
            org.apache.cordova.api.PluginResult r11 = r1.exec(r2, r3, r4, r5, r6)     // Catch:{ JSONException -> 0x0077 }
            if (r11 != 0) goto L_0x0072
            java.lang.String r1 = ""
        L_0x006b:
            r0 = r19
            r0.confirm(r1)     // Catch:{ JSONException -> 0x0077 }
        L_0x0070:
            r1 = 1
            return r1
        L_0x0072:
            java.lang.String r1 = r11.getJSONString()     // Catch:{ JSONException -> 0x0077 }
            goto L_0x006b
        L_0x0077:
            r9 = move-exception
            r9.printStackTrace()
            goto L_0x0070
        L_0x007c:
            if (r12 == 0) goto L_0x009d
            if (r18 == 0) goto L_0x009d
            java.lang.String r1 = "gap_bridge_mode:"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x009d
            org.apache.cordova.CordovaWebView r1 = r14.appView
            org.apache.cordova.NativeToJsMessageQueue r1 = r1.jsMessageQueue
            int r5 = java.lang.Integer.parseInt(r17)
            r1.setBridgeMode(r5)
            java.lang.String r1 = ""
            r0 = r19
            r0.confirm(r1)
            goto L_0x0070
        L_0x009d:
            if (r12 == 0) goto L_0x00b9
            if (r18 == 0) goto L_0x00b9
            java.lang.String r1 = "gap_poll:"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00b9
            org.apache.cordova.CordovaWebView r1 = r14.appView
            org.apache.cordova.NativeToJsMessageQueue r1 = r1.jsMessageQueue
            java.lang.String r11 = r1.pop()
            r0 = r19
            r0.confirm(r11)
            goto L_0x0070
        L_0x00b9:
            if (r18 == 0) goto L_0x00cd
            java.lang.String r1 = "gap_init:"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x00cd
            java.lang.String r1 = "OK"
            r0 = r19
            r0.confirm(r1)
            goto L_0x0070
        L_0x00cd:
            if (r12 == 0) goto L_0x0145
            if (r18 == 0) goto L_0x0145
            java.lang.String r1 = "gap_callbackServer:"
            r0 = r18
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0145
            java.lang.String r11 = ""
            java.lang.String r1 = "usePolling"
            r0 = r17
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0109
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r5 = ""
            java.lang.StringBuilder r1 = r1.append(r5)
            org.apache.cordova.CordovaWebView r5 = r14.appView
            org.apache.cordova.CallbackServer r5 = r5.callbackServer
            boolean r5 = r5.usePolling()
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r11 = r1.toString()
        L_0x0102:
            r0 = r19
            r0.confirm(r11)
            goto L_0x0070
        L_0x0109:
            java.lang.String r1 = "restartServer"
            r0 = r17
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x011b
            org.apache.cordova.CordovaWebView r1 = r14.appView
            org.apache.cordova.CallbackServer r1 = r1.callbackServer
            r1.restartServer()
            goto L_0x0102
        L_0x011b:
            java.lang.String r1 = "getPort"
            r0 = r17
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0132
            org.apache.cordova.CordovaWebView r1 = r14.appView
            org.apache.cordova.CallbackServer r1 = r1.callbackServer
            int r1 = r1.getPort()
            java.lang.String r11 = java.lang.Integer.toString(r1)
            goto L_0x0102
        L_0x0132:
            java.lang.String r1 = "getToken"
            r0 = r17
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0102
            org.apache.cordova.CordovaWebView r1 = r14.appView
            org.apache.cordova.CallbackServer r1 = r1.callbackServer
            java.lang.String r11 = r1.getToken()
            goto L_0x0102
        L_0x0145:
            r13 = r19
            android.app.AlertDialog$Builder r8 = new android.app.AlertDialog$Builder
            org.apache.cordova.api.CordovaInterface r1 = r14.cordova
            android.app.Activity r1 = r1.getActivity()
            r8.<init>(r1)
            r0 = r17
            r8.setMessage(r0)
            android.widget.EditText r10 = new android.widget.EditText
            org.apache.cordova.api.CordovaInterface r1 = r14.cordova
            android.app.Activity r1 = r1.getActivity()
            r10.<init>(r1)
            if (r18 == 0) goto L_0x0169
            r0 = r18
            r10.setText(r0)
        L_0x0169:
            r8.setView(r10)
            r1 = 0
            r8.setCancelable(r1)
            r1 = 17039370(0x104000a, float:2.42446E-38)
            org.apache.cordova.CordovaChromeClient$8 r5 = new org.apache.cordova.CordovaChromeClient$8
            r5.<init>(r10, r13)
            r8.setPositiveButton(r1, r5)
            r1 = 17039360(0x1040000, float:2.424457E-38)
            org.apache.cordova.CordovaChromeClient$9 r5 = new org.apache.cordova.CordovaChromeClient$9
            r5.<init>(r13)
            r8.setNegativeButton(r1, r5)
            r8.create()
            r8.show()
            goto L_0x0070
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.cordova.CordovaChromeClient.onJsPrompt(android.webkit.WebView, java.lang.String, java.lang.String, java.lang.String, android.webkit.JsPromptResult):boolean");
    }

    public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
        LOG.d(this.TAG, "DroidGap:  onExceededDatabaseQuota estimatedSize: %d  currentQuota: %d  totalUsedQuota: %d", Long.valueOf(estimatedSize), Long.valueOf(currentQuota), Long.valueOf(totalUsedQuota));
        if (estimatedSize < this.MAX_QUOTA) {
            long newQuota = estimatedSize;
            LOG.d(this.TAG, "calling quotaUpdater.updateQuota newQuota: %d", Long.valueOf(newQuota));
            quotaUpdater.updateQuota(newQuota);
            return;
        }
        quotaUpdater.updateQuota(currentQuota);
    }

    public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        LOG.d(this.TAG, "%s: Line %d : %s", sourceID, Integer.valueOf(lineNumber), message);
        super.onConsoleMessage(message, lineNumber, sourceID);
    }

    @TargetApi(8)
    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        if (consoleMessage.message() != null) {
            LOG.d(this.TAG, consoleMessage.message());
        }
        return super.onConsoleMessage(consoleMessage);
    }

    public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(origin, callback);
        callback.invoke(origin, true, false);
    }
}
