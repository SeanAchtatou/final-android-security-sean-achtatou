package org.apache.cordova;

import android.os.Message;
import android.util.Log;
import android.webkit.WebView;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedList;
import org.apache.cordova.api.CordovaInterface;

public class NativeToJsMessageQueue {
    private static final int DEFAULT_BRIDGE_MODE = 1;
    private static final String LOG_TAG = "JsMessageQueue";
    private int activeListenerIndex;
    /* access modifiers changed from: private */
    public final CordovaInterface cordova;
    /* access modifiers changed from: private */
    public final LinkedList<String> queue = new LinkedList<>();
    private final BridgeMode[] registeredListeners;
    /* access modifiers changed from: private */
    public final CordovaWebView webView;

    private interface BridgeMode {
        void onNativeToJsMessageAvailable();
    }

    public NativeToJsMessageQueue(CordovaWebView webView2, CordovaInterface cordova2) {
        this.cordova = cordova2;
        this.webView = webView2;
        this.registeredListeners = new BridgeMode[5];
        this.registeredListeners[0] = null;
        this.registeredListeners[1] = new CallbackBridgeMode();
        this.registeredListeners[2] = new LoadUrlBridgeMode();
        this.registeredListeners[3] = new OnlineEventsBridgeMode();
        this.registeredListeners[4] = new PrivateApiBridgeMode();
        reset();
    }

    public void setBridgeMode(int value) {
        if (value < 0 || value >= this.registeredListeners.length) {
            Log.d(LOG_TAG, "Invalid NativeToJsBridgeMode: " + value);
        } else if (value != this.activeListenerIndex) {
            Log.d(LOG_TAG, "Set native->JS mode to " + value);
            synchronized (this) {
                this.activeListenerIndex = value;
                BridgeMode activeListener = this.registeredListeners[value];
                if (!this.queue.isEmpty() && activeListener != null) {
                    activeListener.onNativeToJsMessageAvailable();
                }
            }
        }
    }

    public void reset() {
        synchronized (this) {
            this.queue.clear();
            setBridgeMode(1);
        }
    }

    public String pop() {
        String remove;
        synchronized (this) {
            if (this.queue.isEmpty()) {
                remove = null;
            } else {
                remove = this.queue.remove(0);
            }
        }
        return remove;
    }

    public String popAll() {
        String stringBuffer;
        synchronized (this) {
            int length = this.queue.size();
            if (length == 0) {
                stringBuffer = null;
            } else {
                StringBuffer sb = new StringBuffer();
                int i = 0;
                Iterator i$ = this.queue.iterator();
                while (i$.hasNext()) {
                    String message = i$.next();
                    i++;
                    if (i == length) {
                        sb.append(message);
                    } else {
                        sb.append("try{").append(message).append("}finally{");
                    }
                }
                for (int i2 = 1; i2 < length; i2++) {
                    sb.append('}');
                }
                this.queue.clear();
                stringBuffer = sb.toString();
            }
        }
        return stringBuffer;
    }

    public void add(String statement) {
        synchronized (this) {
            this.queue.add(statement);
            if (this.registeredListeners[this.activeListenerIndex] != null) {
                this.registeredListeners[this.activeListenerIndex].onNativeToJsMessageAvailable();
            }
        }
    }

    private class CallbackBridgeMode implements BridgeMode {
        private CallbackBridgeMode() {
        }

        public void onNativeToJsMessageAvailable() {
            if (NativeToJsMessageQueue.this.webView.callbackServer != null) {
                NativeToJsMessageQueue.this.webView.callbackServer.onNativeToJsMessageAvailable(NativeToJsMessageQueue.this);
            }
        }
    }

    private class LoadUrlBridgeMode implements BridgeMode {
        private LoadUrlBridgeMode() {
        }

        public void onNativeToJsMessageAvailable() {
            NativeToJsMessageQueue.this.webView.loadUrlNow("javascript:" + NativeToJsMessageQueue.this.popAll());
        }
    }

    private class OnlineEventsBridgeMode implements BridgeMode {
        boolean online;
        final Runnable runnable;

        private OnlineEventsBridgeMode() {
            this.online = true;
            this.runnable = new Runnable() {
                public void run() {
                    if (!NativeToJsMessageQueue.this.queue.isEmpty()) {
                        OnlineEventsBridgeMode.this.online = !OnlineEventsBridgeMode.this.online;
                        NativeToJsMessageQueue.this.webView.setNetworkAvailable(OnlineEventsBridgeMode.this.online);
                    }
                }
            };
        }

        public void onNativeToJsMessageAvailable() {
            NativeToJsMessageQueue.this.cordova.getActivity().runOnUiThread(this.runnable);
        }
    }

    private class PrivateApiBridgeMode implements BridgeMode {
        private static final int EXECUTE_JS = 194;
        boolean initFailed;
        Method sendMessageMethod;
        Object webViewCore;

        private PrivateApiBridgeMode() {
        }

        /* JADX INFO: Multiple debug info for r3v3 'webViewObject'  java.lang.Object: [D('webViewObject' org.apache.cordova.CordovaWebView), D('webViewObject' java.lang.Object)] */
        private void initReflection() {
            Object webViewObject = NativeToJsMessageQueue.this.webView;
            Class webViewClass = WebView.class;
            try {
                Field f = webViewClass.getDeclaredField("mProvider");
                f.setAccessible(true);
                webViewObject = f.get(NativeToJsMessageQueue.this.webView);
                webViewClass = webViewObject.getClass();
            } catch (Throwable th) {
            }
            try {
                Field f2 = webViewClass.getDeclaredField("mWebViewCore");
                f2.setAccessible(true);
                this.webViewCore = f2.get(webViewObject);
                if (this.webViewCore != null) {
                    this.sendMessageMethod = this.webViewCore.getClass().getDeclaredMethod("sendMessage", Message.class);
                    this.sendMessageMethod.setAccessible(true);
                }
            } catch (Throwable e) {
                this.initFailed = true;
                Log.e(NativeToJsMessageQueue.LOG_TAG, "PrivateApiBridgeMode failed to find the expected APIs.", e);
            }
        }

        public void onNativeToJsMessageAvailable() {
            if (this.sendMessageMethod == null && !this.initFailed) {
                initReflection();
            }
            if (this.sendMessageMethod != null) {
                Message execJsMessage = Message.obtain(null, EXECUTE_JS, NativeToJsMessageQueue.this.popAll());
                try {
                    this.sendMessageMethod.invoke(this.webViewCore, execJsMessage);
                } catch (Throwable e) {
                    Log.e(NativeToJsMessageQueue.LOG_TAG, "Reflection message bridge failed.", e);
                }
            }
        }
    }
}
