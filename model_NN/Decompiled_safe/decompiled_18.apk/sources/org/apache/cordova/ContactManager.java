package org.apache.cordova;

import android.os.Build;
import android.util.Log;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ContactManager extends Plugin {
    public static final int INVALID_ARGUMENT_ERROR = 1;
    public static final int IO_ERROR = 4;
    private static final String LOG_TAG = "Contact Query";
    public static final int NOT_SUPPORTED_ERROR = 5;
    public static final int PENDING_OPERATION_ERROR = 3;
    public static final int PERMISSION_DENIED_ERROR = 20;
    public static final int TIMEOUT_ERROR = 2;
    public static final int UNKNOWN_ERROR = 0;
    private ContactAccessor contactAccessor;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        JSONObject res;
        PluginResult.Status status = PluginResult.Status.OK;
        if (Build.VERSION.RELEASE.startsWith("1.")) {
            return new PluginResult(PluginResult.Status.ERROR, 5);
        }
        if (this.contactAccessor == null) {
            this.contactAccessor = new ContactAccessorSdk5(this.webView, this.cordova);
        }
        try {
            if (action.equals("search")) {
                return new PluginResult(status, this.contactAccessor.search(args.getJSONArray(0), args.optJSONObject(1)));
            }
            if (action.equals("save")) {
                String id = this.contactAccessor.save(args.getJSONObject(0));
                if (!(id == null || (res = this.contactAccessor.getContactById(id)) == null)) {
                    return new PluginResult(status, res);
                }
            } else if (action.equals("remove") && this.contactAccessor.remove(args.getString(0))) {
                return new PluginResult(status, "");
            }
            return new PluginResult(PluginResult.Status.ERROR, 0);
        } catch (JSONException e) {
            Log.e(LOG_TAG, e.getMessage(), e);
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }
}
