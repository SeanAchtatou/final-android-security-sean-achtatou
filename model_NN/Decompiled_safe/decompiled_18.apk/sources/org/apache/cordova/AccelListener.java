package org.apache.cordova;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import java.util.List;
import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AccelListener extends Plugin implements SensorEventListener {
    public static int ERROR_FAILED_TO_START = 3;
    public static int RUNNING = 2;
    public static int STARTING = 1;
    public static int STOPPED = 0;
    private int accuracy = 0;
    private String callbackId;
    private Sensor mSensor;
    private SensorManager sensorManager;
    private int status;
    private long timestamp = 0;
    private float x = 0.0f;
    private float y = 0.0f;
    private float z = 0.0f;

    public AccelListener() {
        setStatus(STOPPED);
    }

    public void setContext(CordovaInterface cordova) {
        super.setContext(cordova);
        this.sensorManager = (SensorManager) cordova.getActivity().getSystemService("sensor");
    }

    public PluginResult execute(String action, JSONArray args, String callbackId2) {
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT, "");
        result.setKeepCallback(true);
        if (action.equals("start")) {
            this.callbackId = callbackId2;
            if (this.status == RUNNING) {
                return result;
            }
            start();
            return result;
        } else if (!action.equals("stop")) {
            return new PluginResult(PluginResult.Status.INVALID_ACTION);
        } else {
            if (this.status != RUNNING) {
                return result;
            }
            stop();
            return result;
        }
    }

    public void onDestroy() {
        stop();
    }

    private int start() {
        if (this.status == RUNNING || this.status == STARTING) {
            return this.status;
        }
        setStatus(STARTING);
        List<Sensor> list = this.sensorManager.getSensorList(1);
        if (list == null || list.size() <= 0) {
            setStatus(ERROR_FAILED_TO_START);
            fail(ERROR_FAILED_TO_START, "No sensors found to register accelerometer listening to.");
            return this.status;
        }
        this.mSensor = list.get(0);
        this.sensorManager.registerListener(this, this.mSensor, 2);
        setStatus(STARTING);
        long timeout = 2000;
        while (this.status == STARTING && timeout > 0) {
            timeout -= 100;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (timeout == 0) {
            setStatus(ERROR_FAILED_TO_START);
            fail(ERROR_FAILED_TO_START, "Accelerometer could not be started.");
        }
        return this.status;
    }

    private void stop() {
        if (this.status != STOPPED) {
            this.sensorManager.unregisterListener(this);
        }
        setStatus(STOPPED);
        this.accuracy = 0;
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy2) {
        if (sensor.getType() == 1 && this.status != STOPPED) {
            this.accuracy = accuracy2;
        }
    }

    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == 1 && this.status != STOPPED) {
            setStatus(RUNNING);
            if (this.accuracy >= 2) {
                this.timestamp = System.currentTimeMillis();
                this.x = event.values[0];
                this.y = event.values[1];
                this.z = event.values[2];
                win();
            }
        }
    }

    private void fail(int code, String message) {
        JSONObject errorObj = new JSONObject();
        try {
            errorObj.put("code", code);
            errorObj.put("message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PluginResult err = new PluginResult(PluginResult.Status.ERROR, errorObj);
        err.setKeepCallback(true);
        error(err, this.callbackId);
    }

    private void win() {
        PluginResult result = new PluginResult(PluginResult.Status.OK, getAccelerationJSON());
        result.setKeepCallback(true);
        success(result, this.callbackId);
    }

    private void setStatus(int status2) {
        this.status = status2;
    }

    private JSONObject getAccelerationJSON() {
        JSONObject r = new JSONObject();
        try {
            r.put("x", (double) this.x);
            r.put("y", (double) this.y);
            r.put("z", (double) this.z);
            r.put("timestamp", this.timestamp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return r;
    }
}
