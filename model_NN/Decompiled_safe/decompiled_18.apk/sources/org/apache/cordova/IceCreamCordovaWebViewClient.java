package org.apache.cordova;

import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import com.adfonic.android.utils.HtmlFormatter;
import com.adsdk.sdk.Const;
import java.io.IOException;
import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.LOG;

public class IceCreamCordovaWebViewClient extends CordovaWebViewClient {
    public IceCreamCordovaWebViewClient(CordovaInterface cordova) {
        super(cordova);
    }

    public IceCreamCordovaWebViewClient(CordovaInterface cordova, CordovaWebView view) {
        super(cordova, view);
    }

    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if (url.contains("?") || url.contains("#")) {
            return generateWebResourceResponse(url);
        }
        return super.shouldInterceptRequest(view, url);
    }

    private WebResourceResponse generateWebResourceResponse(String url) {
        if (url.startsWith("file:///android_asset/")) {
            String niceUrl = url.replaceFirst("file:///android_asset/", "");
            if (niceUrl.contains("?")) {
                niceUrl = niceUrl.split("\\?")[0];
            } else if (niceUrl.contains("#")) {
                niceUrl = niceUrl.split("#")[0];
            }
            String mimetype = null;
            if (niceUrl.endsWith(".html")) {
                mimetype = HtmlFormatter.TEXT_HTML;
            }
            try {
                return new WebResourceResponse(mimetype, Const.ENCODING, this.cordova.getActivity().getAssets().open(Uri.parse(niceUrl).getPath(), 2));
            } catch (IOException e) {
                LOG.e("generateWebResourceResponse", e.getMessage(), e);
            }
        }
        return null;
    }
}
