package org.apache.cordova;

import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;

public class SplashScreen extends Plugin {
    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        if (action.equals("hide")) {
            this.webView.postMessage("splashscreen", "hide");
        } else if (action.equals("show")) {
            this.webView.postMessage("splashscreen", "show");
        } else {
            status = PluginResult.Status.INVALID_ACTION;
        }
        return new PluginResult(status, "");
    }
}
