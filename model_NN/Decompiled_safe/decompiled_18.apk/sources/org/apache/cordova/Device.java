package org.apache.cordova;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.util.TimeZone;
import org.apache.cordova.api.CordovaInterface;
import org.apache.cordova.api.LOG;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Device extends Plugin {
    public static final String TAG = "Device";
    public static String cordovaVersion = "2.1.0";
    public static String platform = "Android";
    public static String uuid;
    BroadcastReceiver telephonyReceiver = null;

    public void setContext(CordovaInterface cordova) {
        super.setContext(cordova);
        uuid = getUuid();
        initTelephonyReceiver();
    }

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (!action.equals("getDeviceInfo")) {
                return new PluginResult(status, "");
            }
            JSONObject r = new JSONObject();
            r.put("uuid", uuid);
            r.put("version", getOSVersion());
            r.put("platform", platform);
            r.put("name", getProductName());
            r.put("cordova", cordovaVersion);
            return new PluginResult(status, r);
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    public boolean isSynch(String action) {
        if (action.equals("getDeviceInfo")) {
            return true;
        }
        return false;
    }

    public void onDestroy() {
        this.cordova.getActivity().unregisterReceiver(this.telephonyReceiver);
    }

    private void initTelephonyReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PHONE_STATE");
        this.telephonyReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent != null && intent.getAction().equals("android.intent.action.PHONE_STATE") && intent.hasExtra("state")) {
                    String extraData = intent.getStringExtra("state");
                    if (extraData.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                        LOG.i(Device.TAG, "Telephone RINGING");
                        Device.this.webView.postMessage("telephone", "ringing");
                    } else if (extraData.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                        LOG.i(Device.TAG, "Telephone OFFHOOK");
                        Device.this.webView.postMessage("telephone", "offhook");
                    } else if (extraData.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                        LOG.i(Device.TAG, "Telephone IDLE");
                        Device.this.webView.postMessage("telephone", "idle");
                    }
                }
            }
        };
        this.cordova.getActivity().registerReceiver(this.telephonyReceiver, intentFilter);
    }

    public String getPlatform() {
        return platform;
    }

    public String getUuid() {
        return Settings.Secure.getString(this.cordova.getActivity().getContentResolver(), "android_id");
    }

    public String getCordovaVersion() {
        return cordovaVersion;
    }

    public String getModel() {
        return Build.MODEL;
    }

    public String getProductName() {
        return Build.PRODUCT;
    }

    public String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public String getTimeZoneID() {
        return TimeZone.getDefault().getID();
    }
}
