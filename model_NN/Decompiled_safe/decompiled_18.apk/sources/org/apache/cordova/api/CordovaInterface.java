package org.apache.cordova.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public interface CordovaInterface {
    @Deprecated
    void cancelLoadUrl();

    Activity getActivity();

    @Deprecated
    Context getContext();

    Object onMessage(String str, Object obj);

    void setActivityResultCallback(IPlugin iPlugin);

    void startActivityForResult(IPlugin iPlugin, Intent intent, int i);
}
