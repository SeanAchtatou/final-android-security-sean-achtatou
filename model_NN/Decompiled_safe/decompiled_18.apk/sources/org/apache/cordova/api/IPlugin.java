package org.apache.cordova.api;

import android.content.Intent;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;

public interface IPlugin {
    PluginResult execute(String str, JSONArray jSONArray, String str2);

    boolean isSynch(String str);

    void onActivityResult(int i, int i2, Intent intent);

    void onDestroy();

    Object onMessage(String str, Object obj);

    void onNewIntent(Intent intent);

    boolean onOverrideUrlLoading(String str);

    void onPause(boolean z);

    void onResume(boolean z);

    void setContext(CordovaInterface cordovaInterface);

    void setView(CordovaWebView cordovaWebView);
}
