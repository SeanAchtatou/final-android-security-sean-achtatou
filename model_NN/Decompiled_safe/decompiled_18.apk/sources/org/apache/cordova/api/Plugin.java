package org.apache.cordova.api;

import android.content.Intent;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class Plugin implements IPlugin {
    public CordovaInterface cordova;
    public LegacyContext ctx;
    public String id;
    public CordovaWebView webView;

    public abstract PluginResult execute(String str, JSONArray jSONArray, String str2);

    public boolean isSynch(String action) {
        return false;
    }

    public void setContext(CordovaInterface ctx2) {
        this.cordova = ctx2;
        this.ctx = new LegacyContext(this.cordova);
    }

    public void setView(CordovaWebView webView2) {
        this.webView = webView2;
    }

    public void onPause(boolean multitasking) {
    }

    public void onResume(boolean multitasking) {
    }

    public void onNewIntent(Intent intent) {
    }

    public void onDestroy() {
    }

    public Object onMessage(String id2, Object data) {
        return null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    }

    public boolean onOverrideUrlLoading(String url) {
        return false;
    }

    public void sendJavascript(String statement) {
        this.webView.sendJavascript(statement);
    }

    public void success(PluginResult pluginResult, String callbackId) {
        this.webView.sendJavascript(pluginResult.toSuccessCallbackString(callbackId));
    }

    public void success(JSONObject message, String callbackId) {
        this.webView.sendJavascript(new PluginResult(PluginResult.Status.OK, message).toSuccessCallbackString(callbackId));
    }

    public void success(String message, String callbackId) {
        this.webView.sendJavascript(new PluginResult(PluginResult.Status.OK, message).toSuccessCallbackString(callbackId));
    }

    public void error(PluginResult pluginResult, String callbackId) {
        this.webView.sendJavascript(pluginResult.toErrorCallbackString(callbackId));
    }

    public void error(JSONObject message, String callbackId) {
        this.webView.sendJavascript(new PluginResult(PluginResult.Status.ERROR, message).toErrorCallbackString(callbackId));
    }

    public void error(String message, String callbackId) {
        this.webView.sendJavascript(new PluginResult(PluginResult.Status.ERROR, message).toErrorCallbackString(callbackId));
    }
}
