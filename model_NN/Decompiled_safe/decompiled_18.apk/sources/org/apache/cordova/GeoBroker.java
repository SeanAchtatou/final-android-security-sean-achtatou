package org.apache.cordova;

import android.location.Location;
import android.location.LocationManager;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GeoBroker extends Plugin {
    private GPSListener gpsListener;
    private LocationManager locationManager;
    private NetworkListener networkListener;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        if (this.locationManager == null) {
            this.locationManager = (LocationManager) this.cordova.getActivity().getSystemService("location");
            this.networkListener = new NetworkListener(this.locationManager, this);
            this.gpsListener = new GPSListener(this.locationManager, this);
        }
        PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT, "Location API is not available for this device.");
        if (!this.locationManager.isProviderEnabled("gps") && !this.locationManager.isProviderEnabled("network")) {
            return result;
        }
        result.setKeepCallback(true);
        try {
            if (action.equals("getLocation")) {
                boolean enableHighAccuracy = args.getBoolean(0);
                int maximumAge = args.getInt(1);
                Location last = this.locationManager.getLastKnownLocation(enableHighAccuracy ? "gps" : "network");
                if (last != null && System.currentTimeMillis() - last.getTime() <= ((long) maximumAge)) {
                    return new PluginResult(PluginResult.Status.OK, returnLocationJSON(last));
                }
                getCurrentLocation(callbackId, enableHighAccuracy);
                return result;
            } else if (action.equals("addWatch")) {
                addWatch(args.getString(0), callbackId, args.getBoolean(1));
                return result;
            } else if (!action.equals("clearWatch")) {
                return result;
            } else {
                clearWatch(args.getString(0));
                return result;
            }
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION, e.getMessage());
        }
    }

    private void clearWatch(String id) {
        this.gpsListener.clearWatch(id);
        this.networkListener.clearWatch(id);
    }

    private void getCurrentLocation(String callbackId, boolean enableHighAccuracy) {
        if (enableHighAccuracy) {
            this.gpsListener.addCallback(callbackId);
        } else {
            this.networkListener.addCallback(callbackId);
        }
    }

    private void addWatch(String timerId, String callbackId, boolean enableHighAccuracy) {
        if (enableHighAccuracy) {
            this.gpsListener.addWatch(timerId, callbackId);
        } else {
            this.networkListener.addWatch(timerId, callbackId);
        }
    }

    public boolean isSynch(String action) {
        return true;
    }

    public void onDestroy() {
        this.networkListener.destroy();
        this.gpsListener.destroy();
        this.networkListener = null;
        this.gpsListener = null;
    }

    public JSONObject returnLocationJSON(Location loc) {
        Float f = null;
        JSONObject o = new JSONObject();
        try {
            o.put("latitude", loc.getLatitude());
            o.put("longitude", loc.getLongitude());
            o.put("altitude", loc.hasAltitude() ? Double.valueOf(loc.getAltitude()) : null);
            o.put("accuracy", (double) loc.getAccuracy());
            if (loc.hasBearing() && loc.hasSpeed()) {
                f = Float.valueOf(loc.getBearing());
            }
            o.put("heading", f);
            o.put("speed", (double) loc.getSpeed());
            o.put("timestamp", loc.getTime());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return o;
    }

    public void win(Location loc, String callbackId) {
        success(new PluginResult(PluginResult.Status.OK, returnLocationJSON(loc)), callbackId);
    }

    public void fail(int code, String msg, String callbackId) {
        PluginResult result;
        JSONObject obj = new JSONObject();
        String backup = null;
        try {
            obj.put("code", code);
            obj.put("message", msg);
        } catch (JSONException e) {
            obj = null;
            backup = "{'code':" + code + ",'message':'" + msg.replaceAll("'", "'") + "'}";
        }
        if (obj != null) {
            result = new PluginResult(PluginResult.Status.ERROR, obj);
        } else {
            result = new PluginResult(PluginResult.Status.ERROR, backup);
        }
        error(result, callbackId);
    }
}
