package org.apache.cordova;

import android.net.Uri;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FileTransfer extends Plugin {
    private static final String BOUNDARY = "*****";
    public static int CONNECTION_ERR = 3;
    static final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }
    };
    public static int FILE_NOT_FOUND_ERR = 1;
    public static int INVALID_URL_ERR = 2;
    private static final String LINE_END = "\r\n";
    private static final String LINE_START = "--";
    private static final String LOG_TAG = "FileTransfer";
    private HostnameVerifier defaultHostnameVerifier = null;
    private SSLSocketFactory defaultSSLSocketFactory = null;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        try {
            String source = args.getString(0);
            String target = args.getString(1);
            if (action.equals("upload")) {
                return upload(URLDecoder.decode(source), target, args);
            }
            if (action.equals("download")) {
                return download(source, target);
            }
            return new PluginResult(PluginResult.Status.INVALID_ACTION);
        } catch (JSONException e) {
            Log.d(LOG_TAG, "Missing source or target");
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION, "Missing source or target");
        }
    }

    /* JADX WARN: Type inference failed for: r46v34, types: [java.net.URLConnection] */
    /* JADX WARN: Type inference failed for: r46v124, types: [java.net.URLConnection] */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x05f0, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r12.getMessage(), r12);
        r46 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.JSON_EXCEPTION);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0605, code lost:
        if (r9 != null) goto L_0x0607;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0607, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x060c, code lost:
        r39 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:?, code lost:
        r13 = createFileTransferError(org.apache.cordova.FileTransfer.CONNECTION_ERR, r50, r51, r9);
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r13.toString(), r39);
        r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0635, code lost:
        if (r9 != null) goto L_0x0637;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0637, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x0311, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:?, code lost:
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r12.getMessage(), r12);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x04b3, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:?, code lost:
        r13 = createFileTransferError(org.apache.cordova.FileTransfer.FILE_NOT_FOUND_ERR, r50, r51, r9);
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r13.toString(), r12);
        r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x04da, code lost:
        if (r9 != null) goto L_0x04dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x04dc, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0514, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        r13 = createFileTransferError(org.apache.cordova.FileTransfer.INVALID_URL_ERR, r50, r51, r9);
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r13.toString(), r12);
        r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x053b, code lost:
        if (r9 != null) goto L_0x053d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x053d, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0557, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
        r13 = createFileTransferError(org.apache.cordova.FileTransfer.CONNECTION_ERR, r50, r51, r9);
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r13.toString(), r12);
        r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x057e, code lost:
        if (r9 != null) goto L_0x0580;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0580, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x05f0 A[ExcHandler: JSONException (r12v0 'e' org.json.JSONException A[CUSTOM_DECLARE]), PHI: r9 
      PHI: (r9v3 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:1:0x0037, B:74:0x04e2, B:75:?, B:76:0x04f9, B:89:0x0544, B:77:?, B:78:0x0506, B:98:0x0589, B:49:0x0314, B:22:0x01e4, B:35:0x0246] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x060c A[ExcHandler: Throwable (r39v0 't' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r9 
      PHI: (r9v2 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:1:0x0037, B:40:0x0274, B:74:0x04e2, B:75:?, B:76:0x04f9, B:89:0x0544, B:77:?, B:78:0x0506, B:98:0x0589, B:49:0x0314, B:22:0x01e4, B:35:0x0246] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x04b3 A[ExcHandler: FileNotFoundException (r12v3 'e' java.io.FileNotFoundException A[CUSTOM_DECLARE]), PHI: r9 
      PHI: (r9v6 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:1:0x0037, B:40:0x0274, B:74:0x04e2, B:75:?, B:89:0x0544, B:78:0x0506, B:98:0x0589, B:49:0x0314, B:22:0x01e4, B:35:0x0246] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0514 A[ExcHandler: MalformedURLException (r12v2 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), PHI: r9 
      PHI: (r9v5 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:1:0x0037, B:40:0x0274, B:74:0x04e2, B:75:?, B:76:0x04f9, B:89:0x0544, B:77:?, B:78:0x0506, B:98:0x0589, B:49:0x0314, B:22:0x01e4, B:35:0x0246] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0037] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0557 A[ExcHandler: IOException (r12v1 'e' java.io.IOException A[CUSTOM_DECLARE]), PHI: r9 
      PHI: (r9v4 'conn' java.net.HttpURLConnection) = (r9v0 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v8 'conn' java.net.HttpURLConnection), (r9v0 'conn' java.net.HttpURLConnection) binds: [B:1:0x0037, B:40:0x0274, B:74:0x04e2, B:75:?, B:76:0x04f9, B:89:0x0544, B:77:?, B:78:0x0506, B:98:0x0589, B:49:0x0314, B:22:0x01e4, B:35:0x0246] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x0037] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.cordova.api.PluginResult upload(java.lang.String r50, java.lang.String r51, org.json.JSONArray r52) {
        /*
            r49 = this;
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder
            r47.<init>()
            java.lang.String r48 = "upload "
            java.lang.StringBuilder r47 = r47.append(r48)
            r0 = r47
            r1 = r50
            java.lang.StringBuilder r47 = r0.append(r1)
            java.lang.String r48 = " to "
            java.lang.StringBuilder r47 = r47.append(r48)
            r0 = r47
            r1 = r51
            java.lang.StringBuilder r47 = r0.append(r1)
            java.lang.String r47 = r47.toString()
            android.util.Log.d(r46, r47)
            r9 = 0
            r46 = 2
            java.lang.String r47 = "file"
            r0 = r49
            r1 = r52
            r2 = r46
            r3 = r47
            java.lang.String r17 = r0.getArgument(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 3
            java.lang.String r47 = "image.jpg"
            r0 = r49
            r1 = r52
            r2 = r46
            r3 = r47
            java.lang.String r18 = r0.getArgument(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 4
            java.lang.String r47 = "image/jpeg"
            r0 = r49
            r1 = r52
            r2 = r46
            r3 = r47
            java.lang.String r32 = r0.getArgument(r1, r2, r3)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 5
            r0 = r52
            r1 = r46
            org.json.JSONObject r33 = r0.optJSONObject(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r33 != 0) goto L_0x006c
            org.json.JSONObject r33 = new org.json.JSONObject     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r33.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x006c:
            r46 = 6
            r0 = r52
            r1 = r46
            boolean r43 = r0.optBoolean(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 7
            r0 = r52
            r1 = r46
            boolean r46 = r0.optBoolean(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r46 != 0) goto L_0x008e
            r46 = 7
            r0 = r52
            r1 = r46
            boolean r46 = r0.isNull(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r46 == 0) goto L_0x0243
        L_0x008e:
            r8 = 1
        L_0x008f:
            r46 = 8
            r0 = r52
            r1 = r46
            org.json.JSONObject r23 = r0.optJSONObject(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r23 != 0) goto L_0x00a7
            if (r33 == 0) goto L_0x00a7
            java.lang.String r46 = "headers"
            r0 = r33
            r1 = r46
            org.json.JSONObject r23 = r0.optJSONObject(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x00a7:
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "fileKey: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r17
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "fileName: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r18
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "mimeType: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r32
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "params: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r33
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "trustEveryone: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r43
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "chunkedMode: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            java.lang.StringBuilder r47 = r0.append(r8)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "headers: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r23
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            org.apache.cordova.FileUploadResult r37 = new org.apache.cordova.FileUploadResult     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r37.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.io.InputStream r16 = r49.getPathFromUri(r50)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.io.FileInputStream r16 = (java.io.FileInputStream) r16     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r11 = 0
            r30 = 8096(0x1fa0, float:1.1345E-41)
            java.net.URL r44 = new java.net.URL     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r44
            r1 = r51
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = r44.getProtocol()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = r46.toLowerCase()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "https"
            boolean r45 = r46.equals(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r45 == 0) goto L_0x0266
            if (r43 != 0) goto L_0x0246
            java.net.URLConnection r46 = r44.openConnection()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r9 = r0
        L_0x019b:
            r46 = 1
            r0 = r46
            r9.setDoInput(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 1
            r0 = r46
            r9.setDoOutput(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 0
            r0 = r46
            r9.setUseCaches(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "POST"
            r0 = r46
            r9.setRequestMethod(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "Connection"
            java.lang.String r47 = "Keep-Alive"
            r0 = r46
            r1 = r47
            r9.setRequestProperty(r0, r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "Content-Type"
            java.lang.String r47 = "multipart/form-data;boundary=*****"
            r0 = r46
            r1 = r47
            r9.setRequestProperty(r0, r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.webkit.CookieManager r46 = android.webkit.CookieManager.getInstance()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r1 = r51
            java.lang.String r10 = r0.getCookie(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r10 == 0) goto L_0x01e2
            java.lang.String r46 = "Cookie"
            r0 = r46
            r9.setRequestProperty(r0, r10)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x01e2:
            if (r23 == 0) goto L_0x0272
            java.util.Iterator r27 = r23.keys()     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
        L_0x01e8:
            boolean r46 = r27.hasNext()     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            if (r46 == 0) goto L_0x0272
            java.lang.Object r46 = r27.next()     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r21 = r46.toString()     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r23
            r1 = r21
            org.json.JSONArray r22 = r0.optJSONArray(r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            if (r22 != 0) goto L_0x0214
            org.json.JSONArray r22 = new org.json.JSONArray     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r22.<init>()     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r23
            r1 = r21
            java.lang.String r46 = r0.getString(r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r22
            r1 = r46
            r0.put(r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
        L_0x0214:
            r46 = 0
            r0 = r22
            r1 = r46
            java.lang.String r46 = r0.getString(r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r21
            r1 = r46
            r9.setRequestProperty(r0, r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r25 = 1
        L_0x0227:
            int r46 = r22.length()     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r25
            r1 = r46
            if (r0 >= r1) goto L_0x01e8
            r0 = r22
            r1 = r25
            java.lang.String r46 = r0.getString(r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r21
            r1 = r46
            r9.addRequestProperty(r0, r1)     // Catch:{ JSONException -> 0x0271, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            int r25 = r25 + 1
            goto L_0x0227
        L_0x0243:
            r8 = 0
            goto L_0x008f
        L_0x0246:
            r49.trustAllHosts()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.net.URLConnection r24 = r44.openConnection()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            javax.net.ssl.HttpsURLConnection r24 = (javax.net.ssl.HttpsURLConnection) r24     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            javax.net.ssl.HostnameVerifier r46 = r24.getHostnameVerifier()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r1 = r49
            r1.defaultHostnameVerifier = r0     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            javax.net.ssl.HostnameVerifier r46 = org.apache.cordova.FileTransfer.DO_NOT_VERIFY     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r24
            r1 = r46
            r0.setHostnameVerifier(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r9 = r24
            goto L_0x019b
        L_0x0266:
            java.net.URLConnection r46 = r44.openConnection()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r9 = r0
            goto L_0x019b
        L_0x0271:
            r46 = move-exception
        L_0x0272:
            java.lang.String r15 = ""
            java.util.Iterator r27 = r33.keys()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
        L_0x0278:
            boolean r46 = r27.hasNext()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            if (r46 == 0) goto L_0x031f
            java.lang.Object r28 = r27.next()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r46 = java.lang.String.valueOf(r28)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = "headers"
            boolean r46 = r46.equals(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            if (r46 != 0) goto L_0x0278
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = "--*****\r\n"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = "Content-Disposition: form-data; name=\""
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = r28.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = "\";"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = "\r\n\r\n"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = r28.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r33
            r1 = r47
            java.lang.String r47 = r0.getString(r1)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r47 = "\r\n"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ JSONException -> 0x0311, FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, Throwable -> 0x060c }
            goto L_0x0278
        L_0x0311:
            r12 = move-exception
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r12.getMessage()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r1 = r47
            android.util.Log.e(r0, r1, r12)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x031f:
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "--*****\r\n"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            java.lang.StringBuilder r46 = r0.append(r15)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "Content-Disposition: form-data; name=\""
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r1 = r17
            java.lang.StringBuilder r46 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "\";"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = " filename=\""
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r15 = r46.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "UTF-8"
            r0 = r46
            byte[] r14 = r15.getBytes(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.StringBuilder r46 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "\"\r\nContent-Type: "
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r1 = r32
            java.lang.StringBuilder r46 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "\r\n"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "\r\n"
            java.lang.StringBuilder r46 = r46.append(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r31 = r46.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r40 = "\r\n--*****--\r\n"
            java.lang.String r46 = "UTF-8"
            r0 = r18
            r1 = r46
            byte[] r19 = r0.getBytes(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            int r0 = r14.length     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = r0
            int r47 = r31.length()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            int r46 = r46 + r47
            int r47 = r40.length()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            int r46 = r46 + r47
            r0 = r19
            int r0 = r0.length     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47 = r0
            int r38 = r46 + r47
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "String Length: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r38
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.nio.channels.FileChannel r46 = r16.getChannel()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            long r46 = r46.size()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            int r0 = (int) r0     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = r0
            int r20 = r46 + r38
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "Content Length: "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r20
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r8 == 0) goto L_0x04a9
            int r46 = android.os.Build.VERSION.SDK_INT     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47 = 8
            r0 = r46
            r1 = r47
            if (r0 < r1) goto L_0x03ff
            if (r45 == 0) goto L_0x04a9
        L_0x03ff:
            r8 = 1
        L_0x0400:
            if (r8 == 0) goto L_0x04ac
            r0 = r30
            r9.setChunkedStreamingMode(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "Transfer-Encoding"
            java.lang.String r47 = "chunked"
            r0 = r46
            r1 = r47
            r9.setRequestProperty(r0, r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x0412:
            java.io.DataOutputStream r11 = new java.io.DataOutputStream     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.io.OutputStream r46 = r9.getOutputStream()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r11.<init>(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r11.write(r14)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r19
            r11.write(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r31
            r11.writeBytes(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            int r6 = r16.available()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r30
            int r5 = java.lang.Math.min(r6, r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            byte[] r4 = new byte[r5]     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 0
            r0 = r16
            r1 = r46
            int r7 = r0.read(r4, r1, r5)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r41 = 0
            r34 = 0
        L_0x0444:
            if (r7 <= 0) goto L_0x04e0
            long r0 = (long) r7     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = r0
            long r41 = r41 + r46
            r0 = r37
            r1 = r41
            r0.setBytesSent(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 0
            r0 = r46
            r11.write(r4, r0, r5)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 102400(0x19000, double:5.05923E-319)
            long r46 = r46 + r34
            int r46 = (r41 > r46 ? 1 : (r41 == r46 ? 0 : -1))
            if (r46 <= 0) goto L_0x0494
            r34 = r41
            java.lang.String r46 = "FileTransfer"
            java.lang.StringBuilder r47 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47.<init>()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = "Uploaded "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r41
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = " of "
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r47
            r1 = r20
            java.lang.StringBuilder r47 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r48 = " bytes"
            java.lang.StringBuilder r47 = r47.append(r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = r47.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x0494:
            int r6 = r16.available()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r30
            int r5 = java.lang.Math.min(r6, r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = 0
            r0 = r16
            r1 = r46
            int r7 = r0.read(r4, r1, r5)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            goto L_0x0444
        L_0x04a9:
            r8 = 0
            goto L_0x0400
        L_0x04ac:
            r0 = r20
            r9.setFixedLengthStreamingMode(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            goto L_0x0412
        L_0x04b3:
            r12 = move-exception
            int r46 = org.apache.cordova.FileTransfer.FILE_NOT_FOUND_ERR     // Catch:{ all -> 0x063c }
            r0 = r49
            r1 = r46
            r2 = r50
            r3 = r51
            org.json.JSONObject r13 = r0.createFileTransferError(r1, r2, r3, r9)     // Catch:{ all -> 0x063c }
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r13.toString()     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            android.util.Log.e(r0, r1, r12)     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult r46 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult$Status r47 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            r0.<init>(r1, r13)     // Catch:{ all -> 0x063c }
            if (r9 == 0) goto L_0x04df
            r9.disconnect()
        L_0x04df:
            return r46
        L_0x04e0:
            r0 = r40
            r11.writeBytes(r0)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r16.close()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r11.flush()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r11.close()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.StringBuffer r36 = new java.lang.StringBuffer     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = ""
            r0 = r36
            r1 = r46
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.io.DataInputStream r26 = new java.io.DataInputStream     // Catch:{ FileNotFoundException -> 0x0541, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.io.InputStream r46 = r9.getInputStream()     // Catch:{ FileNotFoundException -> 0x0541, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r26
            r1 = r46
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0541, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x0506:
            java.lang.String r29 = r26.readLine()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r29 == 0) goto L_0x0585
            r0 = r36
            r1 = r29
            r0.append(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            goto L_0x0506
        L_0x0514:
            r12 = move-exception
            int r46 = org.apache.cordova.FileTransfer.INVALID_URL_ERR     // Catch:{ all -> 0x063c }
            r0 = r49
            r1 = r46
            r2 = r50
            r3 = r51
            org.json.JSONObject r13 = r0.createFileTransferError(r1, r2, r3, r9)     // Catch:{ all -> 0x063c }
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r13.toString()     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            android.util.Log.e(r0, r1, r12)     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult r46 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult$Status r47 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            r0.<init>(r1, r13)     // Catch:{ all -> 0x063c }
            if (r9 == 0) goto L_0x04df
            r9.disconnect()
            goto L_0x04df
        L_0x0541:
            r12 = move-exception
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r12.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r46
            r1 = r47
            android.util.Log.e(r0, r1, r12)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.io.IOException r46 = new java.io.IOException     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "Received error from server"
            r46.<init>(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            throw r46     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x0557:
            r12 = move-exception
            int r46 = org.apache.cordova.FileTransfer.CONNECTION_ERR     // Catch:{ all -> 0x063c }
            r0 = r49
            r1 = r46
            r2 = r50
            r3 = r51
            org.json.JSONObject r13 = r0.createFileTransferError(r1, r2, r3, r9)     // Catch:{ all -> 0x063c }
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r13.toString()     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            android.util.Log.e(r0, r1, r12)     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult r46 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult$Status r47 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            r0.<init>(r1, r13)     // Catch:{ all -> 0x063c }
            if (r9 == 0) goto L_0x04df
            r9.disconnect()
            goto L_0x04df
        L_0x0585:
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = "got response from server"
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r36.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            int r46 = r9.getResponseCode()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r37
            r1 = r46
            r0.setResponseCode(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = r36.toString()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r37
            r1 = r46
            r0.setResponse(r1)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r26.close()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r43 == 0) goto L_0x05d7
            java.lang.String r46 = r44.getProtocol()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r46 = r46.toLowerCase()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            java.lang.String r47 = "https"
            boolean r46 = r46.equals(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r46 == 0) goto L_0x05d7
            r0 = r9
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = r0
            r0 = r49
            javax.net.ssl.HostnameVerifier r0 = r0.defaultHostnameVerifier     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r47 = r0
            r46.setHostnameVerifier(r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r0 = r49
            javax.net.ssl.SSLSocketFactory r0 = r0.defaultSSLSocketFactory     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46 = r0
            javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(r46)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
        L_0x05d7:
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = "****** About to return a result from upload"
            android.util.Log.d(r46, r47)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            org.apache.cordova.api.PluginResult r46 = new org.apache.cordova.api.PluginResult     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            org.apache.cordova.api.PluginResult$Status r47 = org.apache.cordova.api.PluginResult.Status.OK     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            org.json.JSONObject r48 = r37.toJSONObject()     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            r46.<init>(r47, r48)     // Catch:{ FileNotFoundException -> 0x04b3, MalformedURLException -> 0x0514, IOException -> 0x0557, JSONException -> 0x05f0, Throwable -> 0x060c }
            if (r9 == 0) goto L_0x04df
            r9.disconnect()
            goto L_0x04df
        L_0x05f0:
            r12 = move-exception
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r12.getMessage()     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            android.util.Log.e(r0, r1, r12)     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult r46 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult$Status r47 = org.apache.cordova.api.PluginResult.Status.JSON_EXCEPTION     // Catch:{ all -> 0x063c }
            r46.<init>(r47)     // Catch:{ all -> 0x063c }
            if (r9 == 0) goto L_0x04df
            r9.disconnect()
            goto L_0x04df
        L_0x060c:
            r39 = move-exception
            int r46 = org.apache.cordova.FileTransfer.CONNECTION_ERR     // Catch:{ all -> 0x063c }
            r0 = r49
            r1 = r46
            r2 = r50
            r3 = r51
            org.json.JSONObject r13 = r0.createFileTransferError(r1, r2, r3, r9)     // Catch:{ all -> 0x063c }
            java.lang.String r46 = "FileTransfer"
            java.lang.String r47 = r13.toString()     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            r2 = r39
            android.util.Log.e(r0, r1, r2)     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult r46 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x063c }
            org.apache.cordova.api.PluginResult$Status r47 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x063c }
            r0 = r46
            r1 = r47
            r0.<init>(r1, r13)     // Catch:{ all -> 0x063c }
            if (r9 == 0) goto L_0x04df
            r9.disconnect()
            goto L_0x04df
        L_0x063c:
            r46 = move-exception
            if (r9 == 0) goto L_0x0642
            r9.disconnect()
        L_0x0642:
            throw r46
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.cordova.FileTransfer.upload(java.lang.String, java.lang.String, org.json.JSONArray):org.apache.cordova.api.PluginResult");
    }

    private void trustAllHosts() {
        TrustManager[] trustAllCerts = {new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
            }
        }};
        try {
            this.defaultSSLSocketFactory = HttpsURLConnection.getDefaultSSLSocketFactory();
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage(), e);
        }
    }

    private JSONObject createFileTransferError(int errorCode, String source, String target, HttpURLConnection connection) {
        Integer httpStatus = null;
        if (connection != null) {
            try {
                httpStatus = Integer.valueOf(connection.getResponseCode());
            } catch (IOException e) {
                Log.w(LOG_TAG, "Error getting HTTP status code from connection.", e);
            }
        }
        return createFileTransferError(errorCode, source, target, httpStatus);
    }

    private JSONObject createFileTransferError(int errorCode, String source, String target, Integer httpStatus) {
        JSONObject error = null;
        try {
            JSONObject error2 = new JSONObject();
            try {
                error2.put("code", errorCode);
                error2.put("source", source);
                error2.put("target", target);
                if (httpStatus != null) {
                    error2.put("http_status", httpStatus);
                }
                return error2;
            } catch (JSONException e) {
                e = e;
                error = error2;
                Log.e(LOG_TAG, e.getMessage(), e);
                return error;
            }
        } catch (JSONException e2) {
            e = e2;
            Log.e(LOG_TAG, e.getMessage(), e);
            return error;
        }
    }

    private String getArgument(JSONArray args, int position, String defaultString) {
        String arg = defaultString;
        if (args.length() < position) {
            return arg;
        }
        String arg2 = args.optString(position);
        if (arg2 == null || "null".equals(arg2)) {
            return defaultString;
        }
        return arg2;
    }

    /* JADX WARN: Type inference failed for: r17v19, types: [java.net.URLConnection] */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x011e, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r10 = createFileTransferError(org.apache.cordova.FileTransfer.INVALID_URL_ERR, r21, r22, r7);
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r10.toString(), r9);
        r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0145, code lost:
        if (r7 != null) goto L_0x0147;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0147, code lost:
        r7.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x01ce, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r10 = createFileTransferError(org.apache.cordova.FileTransfer.CONNECTION_ERR, r21, r22, r7);
        android.util.Log.e(org.apache.cordova.FileTransfer.LOG_TAG, r10.toString(), r9);
        r0 = new org.apache.cordova.api.PluginResult(org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01f5, code lost:
        if (r7 != null) goto L_0x01f7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x01f7, code lost:
        r7.disconnect();
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x011e A[ExcHandler: MalformedURLException (r9v1 'e' java.net.MalformedURLException A[CUSTOM_DECLARE]), PHI: r7 
      PHI: (r7v3 'connection' java.net.HttpURLConnection) = (r7v0 'connection' java.net.HttpURLConnection), (r7v0 'connection' java.net.HttpURLConnection), (r7v0 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection) binds: [B:1:0x002f, B:37:0x0186, B:38:?, B:8:0x00b6, B:24:0x010b, B:9:?, B:11:0x00be, B:32:0x014b, B:33:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x002f] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01ce A[ExcHandler: Exception (r9v0 'e' java.lang.Exception A[CUSTOM_DECLARE]), PHI: r7 
      PHI: (r7v2 'connection' java.net.HttpURLConnection) = (r7v0 'connection' java.net.HttpURLConnection), (r7v0 'connection' java.net.HttpURLConnection), (r7v0 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection), (r7v5 'connection' java.net.HttpURLConnection) binds: [B:1:0x002f, B:37:0x0186, B:38:?, B:8:0x00b6, B:24:0x010b, B:9:?, B:11:0x00be, B:32:0x014b, B:33:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:1:0x002f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.apache.cordova.api.PluginResult download(java.lang.String r21, java.lang.String r22) {
        /*
            r20 = this;
            java.lang.String r17 = "FileTransfer"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "download "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r21
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r19 = " to "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r22
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            android.util.Log.d(r17, r18)
            r7 = 0
            r0 = r20
            r1 = r22
            java.io.File r11 = r0.getFileFromPath(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.io.File r17 = r11.getParentFile()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r17.mkdirs()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r20
            org.apache.cordova.CordovaWebView r0 = r0.webView     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r17 = r0
            r0 = r17
            r1 = r21
            boolean r17 = r0.isUrlWhiteListed(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            if (r17 == 0) goto L_0x0184
            java.net.URL r16 = new java.net.URL     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r16
            r1 = r21
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.net.URLConnection r17 = r16.openConnection()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r17
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r7 = r0
            java.lang.String r17 = "GET"
            r0 = r17
            r7.setRequestMethod(r0)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            android.webkit.CookieManager r17 = android.webkit.CookieManager.getInstance()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r17
            r1 = r21
            java.lang.String r8 = r0.getCookie(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            if (r8 == 0) goto L_0x0078
            java.lang.String r17 = "cookie"
            r0 = r17
            r7.setRequestProperty(r0, r8)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
        L_0x0078:
            r7.connect()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r17 = "FileTransfer"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r18.<init>()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r19 = "Download file: "
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r18
            r1 = r16
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r18 = r18.toString()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            android.util.Log.d(r17, r18)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r7.connect()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r17 = "FileTransfer"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r18.<init>()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r19 = "Download file:"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r18
            r1 = r16
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r18 = r18.toString()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            android.util.Log.d(r17, r18)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.io.InputStream r14 = r7.getInputStream()     // Catch:{ FileNotFoundException -> 0x0108, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r17 = 1024(0x400, float:1.435E-42)
            r0 = r17
            byte[] r5 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r6 = 0
            java.io.FileOutputStream r15 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r15.<init>(r11)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
        L_0x00c6:
            int r6 = r14.read(r5)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            if (r6 <= 0) goto L_0x014b
            r17 = 0
            r0 = r17
            r15.write(r5, r0, r6)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            goto L_0x00c6
        L_0x00d4:
            r9 = move-exception
            int r17 = org.apache.cordova.FileTransfer.FILE_NOT_FOUND_ERR     // Catch:{ all -> 0x01fc }
            r0 = r20
            r1 = r17
            r2 = r21
            r3 = r22
            org.json.JSONObject r10 = r0.createFileTransferError(r1, r2, r3, r7)     // Catch:{ all -> 0x01fc }
            java.lang.String r17 = "FileTransfer"
            java.lang.String r18 = "I got a file not found exception"
            android.util.Log.d(r17, r18)     // Catch:{ all -> 0x01fc }
            java.lang.String r17 = "FileTransfer"
            java.lang.String r18 = r10.toString()     // Catch:{ all -> 0x01fc }
            r0 = r17
            r1 = r18
            android.util.Log.e(r0, r1, r9)     // Catch:{ all -> 0x01fc }
            org.apache.cordova.api.PluginResult r17 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x01fc }
            org.apache.cordova.api.PluginResult$Status r18 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x01fc }
            r0 = r17
            r1 = r18
            r0.<init>(r1, r10)     // Catch:{ all -> 0x01fc }
            if (r7 == 0) goto L_0x0107
            r7.disconnect()
        L_0x0107:
            return r17
        L_0x0108:
            r9 = move-exception
            java.lang.String r17 = "FileTransfer"
            java.lang.String r18 = r9.toString()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r17
            r1 = r18
            android.util.Log.e(r0, r1, r9)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.io.IOException r17 = new java.io.IOException     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r18 = "Received error from server"
            r17.<init>(r18)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            throw r17     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
        L_0x011e:
            r9 = move-exception
            int r17 = org.apache.cordova.FileTransfer.INVALID_URL_ERR     // Catch:{ all -> 0x01fc }
            r0 = r20
            r1 = r17
            r2 = r21
            r3 = r22
            org.json.JSONObject r10 = r0.createFileTransferError(r1, r2, r3, r7)     // Catch:{ all -> 0x01fc }
            java.lang.String r17 = "FileTransfer"
            java.lang.String r18 = r10.toString()     // Catch:{ all -> 0x01fc }
            r0 = r17
            r1 = r18
            android.util.Log.e(r0, r1, r9)     // Catch:{ all -> 0x01fc }
            org.apache.cordova.api.PluginResult r17 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x01fc }
            org.apache.cordova.api.PluginResult$Status r18 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x01fc }
            r0 = r17
            r1 = r18
            r0.<init>(r1, r10)     // Catch:{ all -> 0x01fc }
            if (r7 == 0) goto L_0x0107
            r7.disconnect()
            goto L_0x0107
        L_0x014b:
            r15.close()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r17 = "FileTransfer"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r18.<init>()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r19 = "Saved file: "
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r18
            r1 = r22
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r18 = r18.toString()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            android.util.Log.d(r17, r18)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            org.apache.cordova.FileUtils r13 = new org.apache.cordova.FileUtils     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r13.<init>()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            org.json.JSONObject r12 = r13.getEntry(r11)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            org.apache.cordova.api.PluginResult r17 = new org.apache.cordova.api.PluginResult     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            org.apache.cordova.api.PluginResult$Status r18 = org.apache.cordova.api.PluginResult.Status.OK     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r17
            r1 = r18
            r0.<init>(r1, r12)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            if (r7 == 0) goto L_0x0107
            r7.disconnect()
            goto L_0x0107
        L_0x0184:
            java.lang.String r17 = "FileTransfer"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r18.<init>()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r19 = "Source URL is not in white list: '"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r18
            r1 = r21
            java.lang.StringBuilder r18 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r19 = "'"
            java.lang.StringBuilder r18 = r18.append(r19)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            java.lang.String r18 = r18.toString()     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            android.util.Log.w(r17, r18)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            int r17 = org.apache.cordova.FileTransfer.CONNECTION_ERR     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r18 = 401(0x191, float:5.62E-43)
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r20
            r1 = r17
            r2 = r21
            r3 = r22
            r4 = r18
            org.json.JSONObject r10 = r0.createFileTransferError(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            org.apache.cordova.api.PluginResult r17 = new org.apache.cordova.api.PluginResult     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            org.apache.cordova.api.PluginResult$Status r18 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            r0 = r17
            r1 = r18
            r0.<init>(r1, r10)     // Catch:{ FileNotFoundException -> 0x00d4, MalformedURLException -> 0x011e, Exception -> 0x01ce }
            if (r7 == 0) goto L_0x0107
            r7.disconnect()
            goto L_0x0107
        L_0x01ce:
            r9 = move-exception
            int r17 = org.apache.cordova.FileTransfer.CONNECTION_ERR     // Catch:{ all -> 0x01fc }
            r0 = r20
            r1 = r17
            r2 = r21
            r3 = r22
            org.json.JSONObject r10 = r0.createFileTransferError(r1, r2, r3, r7)     // Catch:{ all -> 0x01fc }
            java.lang.String r17 = "FileTransfer"
            java.lang.String r18 = r10.toString()     // Catch:{ all -> 0x01fc }
            r0 = r17
            r1 = r18
            android.util.Log.e(r0, r1, r9)     // Catch:{ all -> 0x01fc }
            org.apache.cordova.api.PluginResult r17 = new org.apache.cordova.api.PluginResult     // Catch:{ all -> 0x01fc }
            org.apache.cordova.api.PluginResult$Status r18 = org.apache.cordova.api.PluginResult.Status.IO_EXCEPTION     // Catch:{ all -> 0x01fc }
            r0 = r17
            r1 = r18
            r0.<init>(r1, r10)     // Catch:{ all -> 0x01fc }
            if (r7 == 0) goto L_0x0107
            r7.disconnect()
            goto L_0x0107
        L_0x01fc:
            r17 = move-exception
            if (r7 == 0) goto L_0x0202
            r7.disconnect()
        L_0x0202:
            throw r17
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.cordova.FileTransfer.download(java.lang.String, java.lang.String):org.apache.cordova.api.PluginResult");
    }

    private InputStream getPathFromUri(String path) throws FileNotFoundException {
        if (path.startsWith("content:")) {
            return this.cordova.getActivity().getContentResolver().openInputStream(Uri.parse(path));
        } else if (!path.startsWith("file://")) {
            return new FileInputStream(path);
        } else {
            int question = path.indexOf("?");
            if (question == -1) {
                return new FileInputStream(path.substring(7));
            }
            return new FileInputStream(path.substring(7, question));
        }
    }

    private File getFileFromPath(String path) throws FileNotFoundException {
        File file;
        if (path.startsWith("file://")) {
            file = new File(path.substring("file://".length()));
        } else {
            file = new File(path);
        }
        if (file.getParent() != null) {
            return file;
        }
        throw new FileNotFoundException();
    }
}
