package com.mobfox.adapter;

import com.google.ads.mediation.MediationServerParameters;

public class ServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "pubid")
    public String pubIdNumber;
}
