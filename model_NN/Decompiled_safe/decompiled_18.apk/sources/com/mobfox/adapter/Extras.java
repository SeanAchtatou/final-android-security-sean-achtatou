package com.mobfox.adapter;

import com.google.ads.mediation.NetworkExtras;

public class Extras implements NetworkExtras {
    private boolean animation = false;
    private boolean location = false;

    public boolean getAnimation() {
        return this.animation;
    }

    public void setAnimation(boolean animation2) {
        this.animation = animation2;
    }

    public boolean getLocation() {
        return this.location;
    }

    public void setLocation(boolean location2) {
        this.location = location2;
    }
}
