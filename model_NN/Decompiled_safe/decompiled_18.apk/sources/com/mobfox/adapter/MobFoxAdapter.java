package com.mobfox.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import com.adsdk.sdk.Ad;
import com.adsdk.sdk.AdListener;
import com.adsdk.sdk.AdManager;
import com.adsdk.sdk.banner.AdView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.inmobi.androidsdk.impl.IMAdException;

public final class MobFoxAdapter implements MediationBannerAdapter<Extras, ServerParameters>, MediationInterstitialAdapter<Extras, ServerParameters> {
    private static final String REQUEST_URL = "http://my.mobfox.com/request.php";
    private static final String VREQUEST_URL = "http://my.mobfox.com/vrequest.php";
    private AdView adView;
    /* access modifiers changed from: private */
    public MediationBannerListener bannerListener;
    /* access modifiers changed from: private */
    public MediationInterstitialListener interstitialListener;
    private AdManager mAdManager;

    public Class<Extras> getAdditionalParametersType() {
        return Extras.class;
    }

    public Class<ServerParameters> getServerParametersType() {
        return ServerParameters.class;
    }

    public void requestBannerAd(MediationBannerListener listener, Activity activity, ServerParameters serverParameters, AdSize adSize, MediationAdRequest mediationAdRequest, Extras extras) {
        this.bannerListener = listener;
        AdSize BANNER_SIZE = new AdSize(320, 50);
        if (adSize.findBestSize(BANNER_SIZE) != BANNER_SIZE) {
            this.bannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.NO_FILL);
            return;
        }
        if (extras != null) {
            this.adView = new AdView(activity, REQUEST_URL, serverParameters.pubIdNumber, extras.getLocation(), extras.getAnimation());
        } else {
            this.adView = new AdView(activity, REQUEST_URL, serverParameters.pubIdNumber, true, true);
        }
        int height = (int) dip2pixel(50, activity);
        this.adView.setLayoutParams(new RelativeLayout.LayoutParams((int) dip2pixel(IMAdException.INVALID_REQUEST, activity), height));
        this.adView.setAdListener(new AdListener() {
            public void noAdFound() {
                if (MobFoxAdapter.this.bannerListener != null) {
                    MobFoxAdapter.this.bannerListener.onFailedToReceiveAd(MobFoxAdapter.this, AdRequest.ErrorCode.NO_FILL);
                }
            }

            public void adLoadSucceeded(Ad ad) {
                if (MobFoxAdapter.this.bannerListener != null) {
                    MobFoxAdapter.this.bannerListener.onReceivedAd(MobFoxAdapter.this);
                }
            }

            public void adClicked() {
                if (MobFoxAdapter.this.bannerListener != null) {
                    MobFoxAdapter.this.bannerListener.onClick(MobFoxAdapter.this);
                }
            }

            public void adClosed(Ad ad, boolean completed) {
            }

            public void adShown(Ad ad, boolean succeeded) {
            }
        });
        this.adView.loadNextAd();
        this.adView.pause();
    }

    public void requestInterstitialAd(MediationInterstitialListener listener, Activity activity, ServerParameters serverParameters, MediationAdRequest mediationAdRequest, Extras extras) {
        this.interstitialListener = listener;
        if (extras != null) {
            this.mAdManager = new AdManager(activity, VREQUEST_URL, serverParameters.pubIdNumber, extras.getLocation());
        } else {
            this.mAdManager = new AdManager(activity, VREQUEST_URL, serverParameters.pubIdNumber, true);
        }
        this.mAdManager.setListener(new AdListener() {
            public void noAdFound() {
                if (MobFoxAdapter.this.interstitialListener != null) {
                    MobFoxAdapter.this.interstitialListener.onFailedToReceiveAd(MobFoxAdapter.this, AdRequest.ErrorCode.NO_FILL);
                }
            }

            public void adShown(Ad ad, boolean arg1) {
                if (MobFoxAdapter.this.interstitialListener != null) {
                    MobFoxAdapter.this.interstitialListener.onPresentScreen(MobFoxAdapter.this);
                }
            }

            public void adLoadSucceeded(Ad arg0) {
                if (MobFoxAdapter.this.interstitialListener != null) {
                    MobFoxAdapter.this.interstitialListener.onReceivedAd(MobFoxAdapter.this);
                }
            }

            public void adClosed(Ad arg0, boolean arg1) {
                if (MobFoxAdapter.this.interstitialListener != null) {
                    MobFoxAdapter.this.interstitialListener.onDismissScreen(MobFoxAdapter.this);
                }
            }

            public void adClicked() {
            }
        });
        this.mAdManager.requestAd();
    }

    public void showInterstitial() {
        if (this.mAdManager != null) {
            this.mAdManager.showAd();
        }
    }

    public void destroy() {
        this.bannerListener = null;
        this.interstitialListener = null;
    }

    public View getBannerView() {
        return this.adView;
    }

    private float dip2pixel(int dip, Context context) {
        return TypedValue.applyDimension(1, (float) dip, context.getResources().getDisplayMetrics());
    }
}
