package com.adfonic.android.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class Network {
    public static String requestDestinationUrl(String urlString) {
        try {
            HttpURLConnection urlConnection = getUrlConnection(urlString);
            urlConnection.getInputStream();
            return urlConnection.getHeaderField("location");
        } catch (Exception e) {
            Log.e("Error getting ad destination URL", e);
            return "";
        }
    }

    private static HttpURLConnection getUrlConnection(String urlString) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) new URL(urlString).openConnection();
        urlConnection.setInstanceFollowRedirects(false);
        return urlConnection;
    }
}
