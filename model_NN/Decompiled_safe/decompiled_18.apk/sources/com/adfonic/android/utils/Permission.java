package com.adfonic.android.utils;

import android.content.Context;

public class Permission {
    public static boolean hasFineGrainLocationAccess(Context context) {
        return context.getPackageManager().checkPermission("android.permission.ACCESS_FINE_LOCATION", context.getPackageName()) == 0;
    }

    public static boolean hasCoarseLocationAccess(Context context) {
        return context.getPackageManager().checkPermission("android.permission.ACCESS_COARSE_LOCATION", context.getPackageName()) == 0;
    }

    public static boolean hasNetworkStateAccess(Context context) {
        return context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) == 0;
    }
}
