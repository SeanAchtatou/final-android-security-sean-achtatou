package com.adfonic.android.utils;

public class Log {
    private static boolean AD_REQUEST_LOGGING_ENABLED = true;
    private static final int ERROR = 6;
    private static final int INFO = 2;
    private static final boolean PRODUCTION_MODE = false;
    public static final String TAG = "Adfonic";
    private static final int VERBOSE = 2;

    private static String getDetailedLog(String msg) {
        return getDetailedLog(msg, 5);
    }

    private static String getDetailedLog(String msg, int depth) {
        Thread current = Thread.currentThread();
        StackTraceElement trace = current.getStackTrace()[depth];
        String filename = trace.getFileName();
        return "[" + current.getName() + "][" + filename.substring(0, filename.length() - 5) + "." + trace.getMethodName() + ":" + trace.getLineNumber() + "] " + msg;
    }

    public static boolean verboseLoggingEnabled() {
        try {
            return android.util.Log.isLoggable(TAG, 2);
        } catch (Throwable th) {
            return true;
        }
    }

    public static boolean infoLoggingEnabled() {
        try {
            return android.util.Log.isLoggable(TAG, 2);
        } catch (Throwable th) {
            return true;
        }
    }

    public static boolean errorLoggingEnabled() {
        try {
            return android.util.Log.isLoggable(TAG, 6);
        } catch (Throwable th) {
            return true;
        }
    }

    public static void e(String msg) {
        try {
            android.util.Log.e(TAG, getDetailedLog(msg));
        } catch (Throwable th) {
        }
    }

    public static void e(String msg, Throwable t) {
        try {
            android.util.Log.e(TAG, getDetailedLog(msg), t);
        } catch (Throwable th) {
        }
    }

    public static void w(String msg, Throwable t) {
        try {
            android.util.Log.w(TAG, getDetailedLog(msg), t);
        } catch (RuntimeException e) {
        }
    }

    public static void v(String msg) {
        try {
            android.util.Log.v(TAG, getDetailedLog(msg));
        } catch (Throwable th) {
        }
    }

    public static void i(String msg) {
        try {
            android.util.Log.i(TAG, getDetailedLog(msg));
        } catch (Throwable th) {
        }
    }

    public static void w(String msg) {
        try {
            android.util.Log.w(TAG, getDetailedLog(msg));
        } catch (Throwable th) {
        }
    }

    public static void setAdLoggingEnabled(boolean enabled) {
        AD_REQUEST_LOGGING_ENABLED = enabled;
    }

    public static void adRequestDetails(String msg) {
        try {
            if (AD_REQUEST_LOGGING_ENABLED && verboseLoggingEnabled()) {
                android.util.Log.i(TAG, msg);
            }
        } catch (Throwable th) {
        }
    }

    public static void adRequestSummary(String msg) {
        try {
            if (AD_REQUEST_LOGGING_ENABLED) {
                android.util.Log.i(TAG, msg);
            }
        } catch (Throwable th) {
        }
    }
}
