package com.adfonic.android.ormma.js;

public class StateMachine {
    private State state;
    private StateChangeListener stateChangeListener;

    public interface StateChangeListener {
        void onStateChanged(State state, State state2);
    }

    public enum State {
        DEFAULT,
        RESIZED,
        HIDDEN,
        EXPANDED,
        LOADING;

        public boolean isLoading() {
            return compareTo(LOADING) == 0;
        }

        public boolean isDefault() {
            return compareTo(DEFAULT) == 0;
        }

        public boolean isResized() {
            return compareTo(RESIZED) == 0;
        }

        public boolean isHidden() {
            return compareTo(HIDDEN) == 0;
        }

        public boolean isExpanded() {
            return compareTo(EXPANDED) == 0;
        }

        public String getJavascriptName() {
            return name().toLowerCase();
        }
    }

    public StateMachine() {
        this(State.LOADING);
    }

    public StateMachine(State state2) {
        changeState(state2);
    }

    public void ready() {
        if (this.state.isLoading()) {
            changeState(State.DEFAULT);
        }
    }

    public void expand() {
        if (this.state.isDefault() || this.state.isResized()) {
            changeState(State.EXPANDED);
        }
    }

    public void resize() {
        if (this.state.isDefault() || this.state.isExpanded()) {
            changeState(State.RESIZED);
        }
    }

    public void close() {
        if (this.state.isDefault()) {
            changeState(State.HIDDEN);
        } else if (this.state.isExpanded() || this.state.isResized()) {
            changeState(State.DEFAULT);
        }
    }

    public void show() {
        if (this.state.isHidden()) {
            changeState(State.DEFAULT);
        }
    }

    public State getState() {
        return this.state;
    }

    private void changeState(State newState) {
        if (this.stateChangeListener != null) {
            this.stateChangeListener.onStateChanged(this.state, newState);
        }
        this.state = newState;
    }

    public void setStateChangeListener(StateChangeListener stateChangeListener2) {
        this.stateChangeListener = stateChangeListener2;
    }
}
