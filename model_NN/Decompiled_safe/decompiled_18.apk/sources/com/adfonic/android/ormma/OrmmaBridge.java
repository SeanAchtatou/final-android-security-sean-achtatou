package com.adfonic.android.ormma;

public interface OrmmaBridge {
    void error(String str, String str2);

    void onKeyboardChange(boolean z);

    void onSizeChange(int i, int i2);

    void ready();

    void reset();

    void viewableChange(boolean z);
}
