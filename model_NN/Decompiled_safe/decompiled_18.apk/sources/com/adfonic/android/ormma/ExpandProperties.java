package com.adfonic.android.ormma;

import com.jumptap.adtag.media.VideoCacheItem;

public class ExpandProperties {
    private static final String HEIGHT = "height";
    private static final String IS_MODAL = "isModal";
    private static final String LOCK_ORIENTATION = "lockOrientation";
    private static final String USE_CUSTOM_CLOSE = "useCustomClose";
    private static final String WIDTH = "width";
    private int height;
    private boolean isModal;
    private boolean lockOrientation;
    private boolean useCustomClose;
    private int width;

    public ExpandProperties() {
    }

    public ExpandProperties(String json) {
        if (json != null) {
            for (String pair : json.replace("{", "").replace("}", "").split(VideoCacheItem.URL_DELIMITER)) {
                String[] keyValue = pair.split(":");
                if (keyValue.length == 2) {
                    setProperty(keyValue, keyValue[0].replace("\"", "").trim());
                }
            }
        }
    }

    public String toJson() {
        return "{\"width\":" + this.width + ",\"height\":" + this.height + ",\"useCustomClose\":" + this.useCustomClose + ",\"isModal\":" + this.isModal + ",\"lockOrientation\":" + this.lockOrientation + "}";
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public boolean isUseCustomClose() {
        return this.useCustomClose;
    }

    public void setUseCustomClose(boolean useCustomClose2) {
        this.useCustomClose = useCustomClose2;
    }

    public boolean isModal() {
        return this.isModal;
    }

    public void setModal(boolean isModal2) {
        this.isModal = isModal2;
    }

    public boolean isLockOrientation() {
        return this.lockOrientation;
    }

    public void setLockOrientation(boolean lockOrientation2) {
        this.lockOrientation = lockOrientation2;
    }

    private boolean parseBoolean(String[] keyValue) {
        return Boolean.parseBoolean(keyValue[1].replace("\"", "").trim());
    }

    private int parseInt(String[] keyValue) {
        return Integer.parseInt(keyValue[1].replace("\"", "").trim());
    }

    private void setProperty(String[] keyValue, String key) {
        if ("width".equalsIgnoreCase(key)) {
            setWidth(parseInt(keyValue));
        } else if ("height".equalsIgnoreCase(key)) {
            setHeight(parseInt(keyValue));
        } else if (USE_CUSTOM_CLOSE.equalsIgnoreCase(key)) {
            setUseCustomClose(parseBoolean(keyValue));
        } else if (IS_MODAL.equalsIgnoreCase(key)) {
            setModal(parseBoolean(keyValue));
        } else if (LOCK_ORIENTATION.equalsIgnoreCase(key)) {
            setLockOrientation(parseBoolean(keyValue));
        }
    }
}
