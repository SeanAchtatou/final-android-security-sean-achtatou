package com.adfonic.android.ormma;

public interface OrmmaView {
    void expand(String str);

    int getDefaultHeight();

    int getDefaultWidth();

    int getDefaultX();

    int getDefaultY();

    ExpandProperties getExpandProperties();

    int getHeight();

    int getMaxHeight();

    int getMaxWidth();

    String getPlacementType();

    int getWidth();

    void hide();

    void injectJavaScript(String str);

    boolean isViewable();

    void open(String str);

    void resize(int i, int i2);

    void setExpandProperties(ExpandProperties expandProperties);

    void showDefaultSize();
}
