package com.adfonic.android.ormma.js;

import com.adfonic.android.ormma.ExpandProperties;
import com.adfonic.android.ormma.OrmmaBridge;
import com.adfonic.android.ormma.OrmmaView;
import com.adfonic.android.ormma.js.StateMachine;
import com.jumptap.adtag.media.VideoCacheItem;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdViewSDK;
import java.util.HashMap;
import java.util.Map;

public class JsOrmmaBridge implements OrmmaBridge {
    private static final String CURRENT_CERTIFIED_VERSION = "1.1.0";
    private static Map<String, Boolean> SUPPORTED_METHODS = new HashMap();
    private StateMachine stateMachine = new StateMachine();
    /* access modifiers changed from: private */
    public OrmmaView view;

    static {
        SUPPORTED_METHODS.put("screen", true);
        SUPPORTED_METHODS.put(MMAdView.KEY_ORIENTATION, false);
        SUPPORTED_METHODS.put("heading", false);
        SUPPORTED_METHODS.put("location", false);
        SUPPORTED_METHODS.put("shake", false);
        SUPPORTED_METHODS.put("tilt", false);
        SUPPORTED_METHODS.put("network", false);
        SUPPORTED_METHODS.put(MMAdViewSDK.Event.INTENT_TXT_MESSAGE, false);
        SUPPORTED_METHODS.put("phone", false);
        SUPPORTED_METHODS.put(MMAdViewSDK.Event.INTENT_EMAIL, false);
        SUPPORTED_METHODS.put("calendar", false);
        SUPPORTED_METHODS.put("camera", false);
        SUPPORTED_METHODS.put("level-1", true);
        SUPPORTED_METHODS.put("level-2", false);
    }

    public JsOrmmaBridge(OrmmaView adView) {
        this.view = adView;
        injectJsFiles();
        this.stateMachine.setStateChangeListener(new StateMachine.StateChangeListener() {
            public void onStateChanged(StateMachine.State oldState, StateMachine.State newState) {
                JsOrmmaBridge.this.fireStateChangeEvent(newState);
                if (oldState.isExpanded() && newState.isDefault()) {
                    JsOrmmaBridge.this.view.showDefaultSize();
                }
            }
        });
    }

    public void ready() {
        this.stateMachine.ready();
        this.view.injectJavaScript("window.ormma.fireReadyEvent()");
    }

    public void error(String message, String action) {
        this.view.injectJavaScript("window.ormma.fireErrorEvent('" + message + "', '" + action + "')");
    }

    public void onKeyboardChange(boolean open) {
        this.view.injectJavaScript("window.ormma.fireKeyboardChangeEvent('" + open + "')");
    }

    public void viewableChange(boolean onScreen) {
        this.view.injectJavaScript("window.ormma.fireViewableChangeEvent('" + onScreen + "')");
    }

    public void onSizeChange(int width, int height) {
        fireSizeChangeEvent();
    }

    public void close() {
        this.stateMachine.close();
        fireSizeChangeEvent();
    }

    public void resize(int width, int height) {
        this.stateMachine.resize();
        fireSizeChangeEvent();
    }

    public void expand(String url) {
        this.view.expand(url);
        this.stateMachine.expand();
        fireSizeChangeEvent();
    }

    public void hide() {
        close();
    }

    public void show() {
        this.stateMachine.show();
        fireSizeChangeEvent();
    }

    public void open(String url) {
        this.view.open(url);
    }

    public String getSize() {
        return "{" + this.view.getWidth() + VideoCacheItem.URL_DELIMITER + this.view.getHeight() + "}";
    }

    public String getMaxSize() {
        return "{" + this.view.getMaxWidth() + VideoCacheItem.URL_DELIMITER + this.view.getMaxHeight() + "}";
    }

    public String getState() {
        return this.stateMachine.getState().getJavascriptName();
    }

    public String getVersion() {
        return CURRENT_CERTIFIED_VERSION;
    }

    public String getDefaultPosition() {
        return "{" + this.view.getDefaultX() + VideoCacheItem.URL_DELIMITER + this.view.getDefaultY() + VideoCacheItem.URL_DELIMITER + this.view.getDefaultWidth() + VideoCacheItem.URL_DELIMITER + this.view.getDefaultHeight() + "}";
    }

    public void setExpandProperties(String properties) {
        this.view.setExpandProperties(new ExpandProperties(properties));
    }

    public String getExpandProperties() {
        return this.view.getExpandProperties().toJson();
    }

    public boolean isViewable() {
        return this.view.isViewable();
    }

    public void useCustomClose(boolean useCustomClose) {
        ExpandProperties ep = this.view.getExpandProperties();
        ep.setUseCustomClose(useCustomClose);
        this.view.setExpandProperties(ep);
    }

    public boolean supports(String feature) {
        if (!SUPPORTED_METHODS.containsKey(feature)) {
            return false;
        }
        return SUPPORTED_METHODS.get(feature).booleanValue();
    }

    public String getPlacementType() {
        return this.view.getPlacementType();
    }

    public void reset() {
        if (!this.stateMachine.getState().isDefault() && !this.stateMachine.getState().isLoading()) {
            close();
        }
    }

    private void fireSizeChangeEvent() {
        fireSizeChangeEvent(this.view.getWidth(), this.view.getHeight());
    }

    private void fireSizeChangeEvent(int width, int height) {
        this.view.injectJavaScript("window.ormma.fireSizeChangeEvent('" + width + "','" + height + "')");
    }

    /* access modifiers changed from: private */
    public void fireStateChangeEvent(StateMachine.State state) {
        this.view.injectJavaScript("window.ormma.fireStateChangeEvent('" + state.getJavascriptName() + "')");
    }

    private void injectJsFiles() {
        this.view.injectJavaScript(OrmmaJs.ORMMA_JS);
        this.view.injectJavaScript(MraidJs.MRAID_JS);
    }
}
