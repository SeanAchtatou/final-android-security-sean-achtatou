package com.adfonic.android.ormma.js;

public class MraidJs {
    public static final String MRAID_JS = "(function() {var mraid = window.mraid = {};mraid.addEventListener = function(event, listener) {\tormma.addEventListener(event, listener);};mraid.removeEventListener = function(event, listener) {\tormma.removeEventListener(event, listener);};mraid.close = function() {\tormma.close();};mraid.getState = function() {\treturn ormma.getState();};mraid.getVersion = function() {\treturn ormma.getVersion();};mraid.isViewable = function() {\treturn ormma.isViewable();};mraid.open = function(url) {\tormma.open(url);};mraid.expand = function(url) {\tormma.expand(url);};mraid.getPlacementType = function() {\treturn ormma.getPlacementType();};mraid.useCustomClose = function() {\tormma.useCustomClose();};mraid.getExpandProperties = function() {\treturn ormma.getExpandProperties();};mraid.setExpandProperties = function(json) {\treturn ormma.getExpandProperties(json);};})();";
}
