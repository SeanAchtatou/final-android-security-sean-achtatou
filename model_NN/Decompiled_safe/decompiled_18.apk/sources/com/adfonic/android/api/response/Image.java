package com.adfonic.android.api.response;

import org.json.JSONException;
import org.json.JSONObject;

public class Image {
    private static final String HEIGHT = "height";
    private static final String URL = "url";
    private static final String WIDTH = "width";
    public int height;
    public String url;
    public int width;

    public static Image fromJson(JSONObject json) throws JSONException {
        Image image = new Image();
        image.url = json.getString(URL);
        image.width = Integer.parseInt(json.getString("width"));
        image.height = Integer.parseInt(json.getString("height"));
        return image;
    }
}
