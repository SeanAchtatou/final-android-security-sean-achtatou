package com.adfonic.android.api;

import android.content.Context;
import android.os.Handler;
import com.adfonic.android.api.request.AndroidRequest;
import com.adfonic.android.api.request.AndroidSystemRequestAdapter;
import com.adfonic.android.utils.Log;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RequestExecutor {
    private ExecutorService executor = Executors.newSingleThreadExecutor();

    public RequestExecutor(Context context) {
    }

    public void close() {
        try {
            this.executor.shutdownNow();
        } catch (Exception e) {
            Log.e("Issue shutting down the executor", e);
        }
    }

    public void execute(Context context, Request request, ExecutorCallback callback, Handler handler) {
        if (request == null) {
            Log.w("Can't execute a null request");
            return;
        }
        if (this.executor == null || this.executor.isShutdown()) {
            this.executor = Executors.newSingleThreadExecutor();
        }
        AndroidRequest androidRequest = new AndroidRequest(request);
        getSystemRequestAdapter().prepareStandardRequest(androidRequest, context);
        this.executor.execute(new RequestRunnable(androidRequest, callback, handler));
    }

    /* access modifiers changed from: protected */
    public AndroidSystemRequestAdapter getSystemRequestAdapter() {
        return new AndroidSystemRequestAdapter();
    }

    public void start() {
        if (this.executor == null || this.executor.isShutdown()) {
            this.executor = Executors.newSingleThreadExecutor();
        }
    }

    public void shutdown() {
        if (this.executor != null) {
            this.executor.shutdownNow();
        }
    }
}
