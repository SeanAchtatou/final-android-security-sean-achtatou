package com.adfonic.android.api.request.utils;

public class UserAgentBuilder {
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getUserAgentString(android.content.Context r7) {
        /*
            java.lang.Class<android.webkit.WebSettings> r3 = android.webkit.WebSettings.class
            r4 = 2
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0036 }
            r5 = 0
            java.lang.Class<android.content.Context> r6 = android.content.Context.class
            r4[r5] = r6     // Catch:{ Exception -> 0x0036 }
            r5 = 1
            java.lang.Class<android.webkit.WebView> r6 = android.webkit.WebView.class
            r4[r5] = r6     // Catch:{ Exception -> 0x0036 }
            java.lang.reflect.Constructor r0 = r3.getDeclaredConstructor(r4)     // Catch:{ Exception -> 0x0036 }
            r3 = 1
            r0.setAccessible(r3)     // Catch:{ Exception -> 0x0036 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0030 }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x0030 }
            r4 = 1
            r5 = 0
            r3[r4] = r5     // Catch:{ all -> 0x0030 }
            java.lang.Object r2 = r0.newInstance(r3)     // Catch:{ all -> 0x0030 }
            android.webkit.WebSettings r2 = (android.webkit.WebSettings) r2     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = r2.getUserAgentString()     // Catch:{ all -> 0x0030 }
            r4 = 0
            r0.setAccessible(r4)     // Catch:{ Exception -> 0x0036 }
        L_0x002f:
            return r3
        L_0x0030:
            r3 = move-exception
            r4 = 0
            r0.setAccessible(r4)     // Catch:{ Exception -> 0x0036 }
            throw r3     // Catch:{ Exception -> 0x0036 }
        L_0x0036:
            r1 = move-exception
            android.webkit.WebView r3 = new android.webkit.WebView
            r3.<init>(r7)
            android.webkit.WebSettings r3 = r3.getSettings()
            java.lang.String r3 = r3.getUserAgentString()
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adfonic.android.api.request.utils.UserAgentBuilder.getUserAgentString(android.content.Context):java.lang.String");
    }
}
