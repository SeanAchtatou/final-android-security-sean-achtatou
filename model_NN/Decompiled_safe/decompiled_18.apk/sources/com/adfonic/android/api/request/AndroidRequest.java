package com.adfonic.android.api.request;

import com.adfonic.android.api.Request;

public class AndroidRequest extends Request {
    private String adfonicSdkVersion;
    private String androidDeviceId;
    private int androidSdkVersion;
    private String deviceName;
    private String hardwareVersion;
    private String latitude;
    private String locale;
    private String longitude;
    private String networkCode;
    private String networkName;
    private String networkType;
    private String operator;
    private String osName;
    private String osVersion;
    private String roaming;
    private String screenSize;
    private String simName;
    private String timeZone;
    private String userAgent;

    public AndroidRequest() {
    }

    public AndroidRequest(Request request) {
        setAge(request.getAge());
        setAgeHigh(request.getAgeHigh());
        setAgeLow(request.getAgeLow());
        setDateOfBirth(request.getDateOfBirth());
        setLanguage(request.getLanguage());
        setLocation(request.getLocation());
        setRefreshTime(request.getRefreshTime());
        setSlotId(request.getSlotId());
        setMale(request.isMale());
        setHasGender(request.hasGender());
        setAllowLocation(request.isAllowLocation());
    }

    public int getAndroidSdkVersion() {
        return this.androidSdkVersion;
    }

    public void setAndroidSdkVersion(int androidSdkVersion2) {
        this.androidSdkVersion = androidSdkVersion2;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public void setUserAgent(String userAgent2) {
        this.userAgent = userAgent2;
    }

    public String getAndroidDeviceId() {
        return this.androidDeviceId;
    }

    public void setAndroidDeviceId(String androidDeviceId2) {
        this.androidDeviceId = androidDeviceId2;
    }

    public String getHardwareVersion() {
        return this.hardwareVersion;
    }

    public void setHardwareVersion(String hardwareVersion2) {
        this.hardwareVersion = hardwareVersion2;
    }

    public String getAdfonicSdkVersion() {
        return this.adfonicSdkVersion;
    }

    public void setAdfonicSdkVersion(String adfonicSdkVersion2) {
        this.adfonicSdkVersion = adfonicSdkVersion2;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String latitude2) {
        this.latitude = latitude2;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String longitude2) {
        this.longitude = longitude2;
    }

    public String getLocale() {
        return this.locale;
    }

    public void setLocale(String locale2) {
        this.locale = locale2;
    }

    public String getTimeZone() {
        return this.timeZone;
    }

    public void setTimeZone(String timeZone2) {
        this.timeZone = timeZone2;
    }

    public String getScreenSize() {
        return this.screenSize;
    }

    public void setScreenSize(String screenSize2) {
        this.screenSize = screenSize2;
    }

    public String getOperator() {
        return this.operator;
    }

    public void setOperator(String operator2) {
        this.operator = operator2;
    }

    public String getNetworkType() {
        return this.networkType;
    }

    public void setNetworkType(String networkType2) {
        this.networkType = networkType2;
    }

    public String getNetworkCode() {
        return this.networkCode;
    }

    public void setNetworkCode(String networkCode2) {
        this.networkCode = networkCode2;
    }

    public String getNetworkName() {
        return this.networkName;
    }

    public void setNetworkName(String networkName2) {
        this.networkName = networkName2;
    }

    public String getSimName() {
        return this.simName;
    }

    public void setSimName(String simName2) {
        this.simName = simName2;
    }

    public String getRoaming() {
        return this.roaming;
    }

    public void setRoaming(String roaming2) {
        this.roaming = roaming2;
    }

    public String getOsName() {
        return this.osName;
    }

    public void setOsName(String osName2) {
        this.osName = osName2;
    }

    public String getOsVersion() {
        return this.osVersion;
    }

    public void setOsVersion(String osVersion2) {
        this.osVersion = osVersion2;
    }

    public String getDeviceName() {
        return this.deviceName;
    }

    public void setDeviceName(String deviceName2) {
        this.deviceName = deviceName2;
    }
}
