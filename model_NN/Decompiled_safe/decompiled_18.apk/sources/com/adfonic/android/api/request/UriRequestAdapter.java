package com.adfonic.android.api.request;

import com.adfonic.android.utils.Log;
import com.google.ads.AdActivity;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;

public class UriRequestAdapter {
    private static final String DATE_OF_BIRTH_FORMAT = "yyyyMMdd";
    private static final String DEFAULT_BEACON_TYPE = "metadata";
    private static final String DEFAULT_T_FORMAT = "json";
    private static final String DEFAULT_T_MARKUP = "1";
    private static final String D_DPID = "d.dpid";
    private static final String D_NAME = "d.name";
    private static final String D_ODIN1 = "d.odin-1";
    private static final String D_OSNAME = "d.osname";
    private static final String D_OSVER = "d.osver";
    private static final String D_SCREENSIZE = "d.screensize";
    private static final String ENCODING = "UTF-8";
    private static final String H_USER_AGENT = "h.user-agent";
    private static final String IS_TEST = "s.test";
    private static final String PROD_URL = "http://adfonic.net/ad/";
    private static final String R_CLIENT = "r.client";
    private static final String R_HW = "r.hw";
    private static final String R_MCCMNC = "r.mccmnc";
    private static final String R_NETCODE = "r.netcode";
    private static final String R_NETNAME = "r.netname";
    private static final String R_NETTYPE = "r.nettype";
    public static final String R_NETTYPE_MOBILE = "mobile";
    public static final String R_NETTYPE_WIFI = "wifi";
    private static final String R_ROAMING = "r.roaming";
    private static final String R_SIMNAME = "r.simname";
    private static final String T_BEACONS = "t.beacons";
    private static final String T_FORMAT = "t.format";
    private static final String T_MARKUP = "t.markup";
    private static final String U_AGE = "u.age";
    private static final String U_AGE_HIGH = "u.ageHigh";
    private static final String U_AGE_LOW = "u.ageLow";
    private static final String U_DOB = "u.dob";
    private static final String U_GENDER = "u.gender";
    private static final String U_LANG = "u.lang";
    private static final String U_LATITUDE = "u.latitude";
    private static final String U_LOCALE = "u.locale";
    private static final String U_LONGITUDE = "u.longitude";
    private static final String U_TZ = "u.tz";
    private StringBuilder builder;
    private AndroidRequest request;

    public UriRequestAdapter(AndroidRequest request2) {
        if (request2.getSlotId() == null) {
            throw new IllegalStateException("Request is invalid, slot id must be defined");
        }
        this.request = request2;
        this.builder = new StringBuilder(PROD_URL);
    }

    public String toUrl() {
        appendSlotId();
        this.builder.append("?");
        append(T_MARKUP, DEFAULT_T_MARKUP);
        append(T_FORMAT, DEFAULT_T_FORMAT);
        append(T_BEACONS, DEFAULT_BEACON_TYPE);
        append(R_CLIENT, this.request.getAdfonicSdkVersion());
        append(D_DPID, this.request.getAndroidDeviceId());
        append(D_ODIN1, this.request.getAndroidDeviceId());
        append(R_HW, this.request.getHardwareVersion());
        append(U_LONGITUDE, this.request.getLongitude());
        append(U_LATITUDE, this.request.getLatitude());
        append(U_LANG, this.request.getLanguage());
        append(U_AGE_HIGH, convertToString(this.request.getAgeHigh()));
        append(U_AGE_LOW, convertToString(this.request.getAgeLow()));
        append(U_AGE, convertToString(this.request.getAge()));
        append(U_DOB, getDateOfBirth());
        append(U_TZ, this.request.getTimeZone());
        append(U_LOCALE, this.request.getLocale());
        append(U_GENDER, getGender());
        append(IS_TEST, getTestValue());
        append(D_SCREENSIZE, this.request.getScreenSize());
        append(D_NAME, this.request.getDeviceName());
        append(D_OSNAME, this.request.getOsName());
        append(D_OSVER, this.request.getOsVersion());
        append(R_MCCMNC, this.request.getOperator());
        append(R_NETTYPE, this.request.getNetworkType());
        append(R_NETCODE, this.request.getNetworkCode());
        append(R_NETNAME, this.request.getNetworkName());
        append(R_SIMNAME, this.request.getSimName());
        append(R_ROAMING, this.request.getRoaming());
        append(H_USER_AGENT, this.request.getUserAgent());
        return this.builder.toString();
    }

    private String getTestValue() {
        if (!this.request.isTest()) {
            return null;
        }
        return DEFAULT_T_MARKUP;
    }

    private String convertToString(int number) {
        if (number <= 0) {
            return null;
        }
        return "" + number;
    }

    private void append(String key, String value) {
        if (value != null) {
            Log.adRequestDetails("Adfonic Request Parameter: " + key + " = " + value);
            this.builder.append(key).append("=").append(encodeParameter(value)).append("&");
        }
    }

    private String encodeParameter(String parameter) {
        try {
            return URLEncoder.encode(parameter, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "";
        }
    }

    private void appendSlotId() {
        this.builder.append(this.request.getSlotId());
    }

    private String getGender() {
        if (!this.request.hasGender()) {
            return null;
        }
        if (this.request.isMale()) {
            return AdActivity.TYPE_PARAM;
        }
        return "f";
    }

    private String getDateOfBirth() {
        if (this.request.getDateOfBirth() == null) {
            return null;
        }
        try {
            new SimpleDateFormat(DATE_OF_BIRTH_FORMAT).parse(this.request.getDateOfBirth());
            return this.request.getDateOfBirth();
        } catch (Exception e) {
            return null;
        }
    }
}
