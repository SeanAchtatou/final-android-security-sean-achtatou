package com.adfonic.android.api.response;

import com.adfonic.android.api.response.ApiResponse;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class BannerComponent {
    private static final String BEACON = "beacon";
    private static final String BEACONS = "beacons";
    private static final String CONTENT = "content";
    private static final String IMAGE = "image";
    private static final String NUM_BEACONS = "numBeacons";
    private static final String TAG_LINE = "tagline";
    private List<Beacon> beacons = new ArrayList();
    private Image image;
    private String tagLine;

    public ApiResponse.Format getFormat() {
        return ApiResponse.Format.BANNER;
    }

    public Image getImage() {
        return this.image;
    }

    public String getTagLine() {
        return this.tagLine;
    }

    public static BannerComponent fromJson(JSONObject component) throws JSONException {
        BannerComponent banner = new BannerComponent();
        if (component.has(IMAGE)) {
            banner.image = Image.fromJson(component.getJSONObject(IMAGE));
            JSONObject tagLineJson = component.optJSONObject(TAG_LINE);
            if (tagLineJson != null) {
                banner.tagLine = tagLineJson.optString(CONTENT);
            }
        }
        if (component.has(BEACONS)) {
            JSONObject jsonBeacon = component.getJSONObject(BEACONS);
            int count = jsonBeacon.getInt(NUM_BEACONS);
            for (int i = 1; i < count + 1; i++) {
                String beaconName = banner.getBeaconKey(i);
                String beaconUrl = jsonBeacon.getString(beaconName);
                Beacon beacon = new Beacon();
                beacon.name = beaconName;
                beacon.url = beaconUrl;
                banner.addBeacon(beacon);
            }
        }
        return banner;
    }

    public List<Beacon> getBeacons() {
        return this.beacons;
    }

    public void addBeacon(Beacon beacon) {
        this.beacons.add(beacon);
    }

    private String getBeaconKey(int index) {
        return BEACON + index;
    }
}
