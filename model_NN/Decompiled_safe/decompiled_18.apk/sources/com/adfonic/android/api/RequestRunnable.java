package com.adfonic.android.api;

import android.os.Handler;
import com.adfonic.android.api.request.AndroidRequest;
import com.adfonic.android.api.request.UriRequestAdapter;
import com.adfonic.android.api.response.ApiResponse;
import com.adfonic.android.utils.Log;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONException;

class RequestRunnable implements Runnable {
    private static final int CONNECTION_TIMEOUT = 20000;
    private static final int READ_TIMEOUT = 20000;
    private static final String USER_AGENT = "User-Agent";
    /* access modifiers changed from: private */
    public final ExecutorCallback callback;
    private final Handler handler;
    private final AndroidRequest request;

    public RequestRunnable(AndroidRequest request2, ExecutorCallback callback2, Handler handler2) {
        this.request = request2;
        this.callback = callback2;
        if (handler2 == null) {
            this.handler = new Handler();
        } else {
            this.handler = handler2;
        }
    }

    public void run() {
        try {
            final ApiResponse response = execute();
            if (this.handler != null && this.callback != null) {
                this.handler.sendEmptyMessage(123);
                this.handler.post(new Runnable() {
                    public void run() {
                        RequestRunnable.this.callback.onResponse(response);
                    }
                });
            }
        } catch (IOException | JSONException e) {
        } catch (Exception e2) {
            if (this.callback != null) {
                this.callback.onThrowable(e2);
            }
        }
    }

    private ApiResponse execute() throws IOException, JSONException {
        InputStream is = null;
        fixLeakProblemForOldVersionsOfAndroid(this.request.getAndroidSdkVersion());
        try {
            String url = new UriRequestAdapter(this.request).toUrl();
            logRequest(url);
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setConnectTimeout(20000);
            conn.setReadTimeout(20000);
            conn.setRequestProperty(USER_AGENT, this.request.getUserAgent());
            is = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    return new ApiResponse().handleResponse(sb.toString());
                }
                sb.append(line);
            }
        } finally {
            closeSilently(is);
            closeSilently(null);
        }
    }

    private void closeSilently(Closeable c) {
        if (c != null) {
            try {
                c.close();
            } catch (Exception e) {
            }
        }
    }

    private void logRequest(String url) {
        if (Log.verboseLoggingEnabled()) {
            Log.v("Executing: request to adfonic servers");
            Log.v("> " + url);
        }
        Log.adRequestSummary("Adfonic Request: " + url);
    }

    private void fixLeakProblemForOldVersionsOfAndroid(int sdkVersion) {
        if (sdkVersion < 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }
}
