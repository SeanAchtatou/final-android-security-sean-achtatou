package com.adfonic.android.api.request;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import com.adfonic.android.api.Request;
import com.adfonic.android.utils.Log;

public class XmlAttributeRequestReader {
    private static final String MAP_KEY_AD_HEIGHT = "ad_height";
    private static final String MAP_KEY_AD_WIDTH = "ad_width";
    private static final String MAP_KEY_ALLOW_LOCATION = "allow_location";
    private static final String MAP_KEY_LANGUAGE = "language";
    private static final String MAP_KEY_REFRESH_AD = "refresh_ad";
    private static final String MAP_KEY_REFRESH_TIME = "refresh_time";
    private static final String MAP_KEY_SLOT_ID = "adfonic_adslot_id";
    private static final String MAP_KEY_TEST_MODE = "test_mode";
    private static final String NS = "http://schemas.android.com/apk/lib/com.adfonic.android";

    public Request convertToRequest(AttributeSet attrs, Context context) {
        Resources res = context.getResources();
        Request request = new Request();
        setSlotId(attrs, res, request);
        setTest(attrs, request);
        setAllowLocation(attrs, request);
        setRefreshAd(attrs, request);
        setRefreshTime(attrs, request);
        setLanguage(attrs, res, request);
        setAdWidth(attrs, res, request);
        setAdHeight(attrs, res, request);
        return request;
    }

    private void setAdHeight(AttributeSet attrs, Resources res, Request request) {
        try {
            request.setAdHeight(Float.valueOf(getString(res, attrs, NS, MAP_KEY_AD_HEIGHT)).floatValue());
        } catch (Exception e) {
            Log.w("Please provide a values for attribute : ad_height");
        }
    }

    private void setAdWidth(AttributeSet attrs, Resources res, Request request) {
        try {
            request.setAdWidth(Float.valueOf(getString(res, attrs, NS, MAP_KEY_AD_WIDTH)).floatValue());
        } catch (Exception e) {
            Log.w("Please provide a values for attribute : ad_width");
        }
    }

    private void setLanguage(AttributeSet attrs, Resources res, Request r) {
        try {
            r.setLanguage(getString(res, attrs, NS, MAP_KEY_LANGUAGE));
        } catch (Exception e) {
            Log.w("Problem reading refresh_time value");
        }
    }

    private void setRefreshTime(AttributeSet attrs, Request r) {
        try {
            r.setRefreshTime(attrs.getAttributeIntValue(NS, MAP_KEY_REFRESH_TIME, 20));
        } catch (Exception e) {
            Log.w("Problem reading refresh_time value");
        }
    }

    private void setRefreshAd(AttributeSet attrs, Request r) {
        try {
            r.setRefreshAd(attrs.getAttributeBooleanValue(NS, MAP_KEY_REFRESH_AD, true));
        } catch (Exception e) {
            Log.w("Problem reading refresh_ad value");
        }
    }

    private void setAllowLocation(AttributeSet attrs, Request r) {
        try {
            r.setAllowLocation(attrs.getAttributeBooleanValue(NS, MAP_KEY_ALLOW_LOCATION, true));
        } catch (Exception e) {
            Log.w("Problem reading allow_location value");
        }
    }

    private void setTest(AttributeSet attrs, Request r) {
        try {
            r.setTest(attrs.getAttributeBooleanValue(NS, MAP_KEY_TEST_MODE, false));
        } catch (Exception e) {
            Log.w("Problem reading test_mode value");
        }
    }

    private void setSlotId(AttributeSet attrs, Resources res, Request r) {
        try {
            r.setSlotId(getString(res, attrs, NS, MAP_KEY_SLOT_ID));
        } catch (Exception e) {
            Log.w("Problem reading adfonic_adslot_id value");
        }
    }

    private String getString(Resources res, AttributeSet attrs, String namespace, String attributeName) {
        int resId = attrs.getAttributeResourceValue(namespace, attributeName, -1);
        if (resId != -1) {
            return res.getString(resId);
        }
        return attrs.getAttributeValue(namespace, attributeName);
    }
}
