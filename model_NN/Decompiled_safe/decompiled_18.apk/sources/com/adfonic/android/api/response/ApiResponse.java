package com.adfonic.android.api.response;

import com.adfonic.android.utils.Log;
import java.io.IOException;
import java.lang.reflect.Field;
import org.json.JSONException;
import org.json.JSONObject;

public class ApiResponse {
    private static final String AD_CONTENT = "adContent";
    private static final String AD_ID = "adId";
    private static final String BANNER_FORMAT = "banner";
    private static final String COMPONENTS = "components";
    private static final String DESTINATION = "destination";
    private static final String DESTINATION_TYPE = "type";
    private static final String DESTINATION_URL = "url";
    private static final String FORMAT = "format";
    private static final String IMAGE320X480 = "image320x480";
    private static final String JSON_STATUS = "status";
    private static final String JSON_STATUS_ERROR = "error";
    private static final String JSON_STATUS_SUCCESS = "success";
    private static final String TEXT_FORMAT = "text";
    private static final String TRACKING_ID = "trackingId";
    private String adContent;
    private String adId;
    private BannerComponent component;
    private Type destinationType;
    private String destinationUrl;
    private String errorMessage = "Unknown error";
    private Format format;
    private boolean isError = true;
    private String trackingId;

    public enum Format {
        BANNER,
        TEXT,
        NA,
        IMAGE320X480
    }

    public enum Type {
        URL,
        CALL,
        AUDIO,
        VIDEO,
        ITUNES_STORE,
        IPHONE_APP_STORE,
        ANDROID,
        AMAZON
    }

    public ApiResponse handleResponse(String json) throws IOException, JSONException {
        JSONObject obj = new JSONObject(json);
        Log.adRequestSummary("Adfonic response: " + obj.toString());
        String status = obj.getString(JSON_STATUS);
        if (status.equalsIgnoreCase(JSON_STATUS_ERROR)) {
            this.isError = true;
            this.errorMessage = obj.optString(JSON_STATUS_ERROR);
        } else if (status.equalsIgnoreCase(JSON_STATUS_SUCCESS)) {
            this.isError = false;
            this.adId = obj.optString(AD_ID);
            this.trackingId = obj.optString(TRACKING_ID);
            this.adContent = obj.optString(AD_CONTENT);
            this.format = extractFormat(obj);
            JSONObject destination = obj.optJSONObject(DESTINATION);
            if (destination != null && destination.has(DESTINATION_TYPE)) {
                try {
                    this.destinationType = Type.valueOf(destination.getString(DESTINATION_TYPE).toUpperCase());
                } catch (IllegalArgumentException e) {
                    this.destinationType = Type.URL;
                }
                this.destinationUrl = destination.optString(DESTINATION_URL);
            }
            JSONObject componentJson = obj.optJSONObject(COMPONENTS);
            if (componentJson != null) {
                this.component = BannerComponent.fromJson(componentJson);
            }
        } else {
            this.isError = true;
            this.errorMessage = "Unknown error";
        }
        return this;
    }

    public boolean isError() {
        return this.isError;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public String getAdId() {
        return this.adId;
    }

    public String getTrackingId() {
        return this.trackingId;
    }

    public String getDestinationUrl() {
        return this.destinationUrl;
    }

    public Type getDestinationType() {
        return this.destinationType;
    }

    public Format getFormat() {
        return this.format;
    }

    public BannerComponent getComponents() {
        return this.component;
    }

    public String getAdContent() {
        return this.adContent;
    }

    public String toString() {
        return getPrivateField(this);
    }

    public boolean isAudioResponse() {
        return getDestinationType() == Type.AUDIO;
    }

    public boolean isVideoResponse() {
        return getDestinationType() == Type.VIDEO;
    }

    public boolean isCallResponse() {
        return getDestinationType() == Type.CALL;
    }

    public boolean isAndroidDestinationType() {
        if (getDestinationType() == Type.ANDROID) {
            return true;
        }
        return false;
    }

    public boolean isAmazonDestinationType() {
        if (getDestinationType() == Type.AMAZON) {
            return true;
        }
        return false;
    }

    private String getPrivateField(Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        StringBuilder b = new StringBuilder();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                b.append(field.getName()).append('=').append(field.get(o)).append(10);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
            }
        }
        return b.toString();
    }

    private Format extractFormat(JSONObject obj) {
        String fas = obj.optString(FORMAT);
        if (TEXT_FORMAT.equalsIgnoreCase(fas)) {
            return Format.TEXT;
        }
        if (BANNER_FORMAT.equalsIgnoreCase(fas)) {
            return Format.BANNER;
        }
        if (IMAGE320X480.equalsIgnoreCase(fas)) {
            return Format.IMAGE320X480;
        }
        return Format.NA;
    }

    public boolean isInterstitial() {
        if (this.format == Format.IMAGE320X480) {
            return true;
        }
        return false;
    }
}
