package com.adfonic.android.api.request;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.adfonic.android.api.request.utils.AndroidDeviceId;
import com.adfonic.android.api.request.utils.UserAgentBuilder;
import com.adfonic.android.utils.Permission;
import java.util.Locale;
import java.util.TimeZone;

public class AndroidSystemRequestAdapter {
    private static String NETWORK_NAME;
    private static String OPERATOR;
    private static String ROAMING;
    private static String SIM_NAME;
    private static String USER_AGENT;

    public void prepareStandardRequest(AndroidRequest request, Context context) {
        setUserAgent(request, context);
        setHardwareVersion(request);
        setAdfonicVersion(request);
        setAndroidDeviceId(request, context);
        setConnectionType(request, context);
        setOperatorInfo(request, context);
        setLocation(request, context);
        setLocale(request);
        setLanguage(request);
        setTimeZone(request);
        setScreenSize(request, context);
    }

    public void setOperatorInfo(AndroidRequest request, Context context) {
        try {
            if (OPERATOR == null) {
                TelephonyManager telMgr = (TelephonyManager) context.getSystemService("phone");
                if (telMgr != null) {
                    OPERATOR = telMgr.getNetworkOperator();
                    NETWORK_NAME = telMgr.getNetworkOperatorName();
                    SIM_NAME = telMgr.getSimOperatorName();
                    if (telMgr.isNetworkRoaming()) {
                        ROAMING = "1";
                    }
                } else {
                    return;
                }
            }
            request.setOperator(OPERATOR);
            request.setNetworkName(NETWORK_NAME);
            request.setSimName(SIM_NAME);
            request.setRoaming(ROAMING);
        } catch (Exception e) {
        }
    }

    public void setScreenSize(AndroidRequest request, Context context) {
        try {
            Display d = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            d.getMetrics(new DisplayMetrics());
            int width = d.getWidth();
            int height = d.getHeight();
            if (width > 0 && height > 0) {
                request.setScreenSize(width + "x" + height);
            }
        } catch (Exception e) {
        }
    }

    public void setTimeZone(AndroidRequest request) {
        try {
            request.setTimeZone(TimeZone.getDefault().getID());
        } catch (Exception e) {
        }
    }

    public void setLocale(AndroidRequest request) {
        try {
            request.setLocale(Locale.getDefault().toString());
        } catch (Exception e) {
        }
    }

    public void setLanguage(AndroidRequest request) {
        try {
            if (request.getLanguage() == null) {
                request.setLanguage(Locale.getDefault().toString());
            }
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void setConnectionType(AndroidRequest request, Context context) {
        ConnectivityManager connManager;
        NetworkInfo mActive;
        try {
            if (Permission.hasNetworkStateAccess(context) && (connManager = (ConnectivityManager) context.getSystemService("connectivity")) != null && (mActive = connManager.getActiveNetworkInfo()) != null) {
                switch (mActive.getType()) {
                    case 0:
                        request.setNetworkType("mobile");
                        break;
                    case 1:
                        break;
                    default:
                        return;
                }
                request.setNetworkType("wifi");
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void setAndroidDeviceId(AndroidRequest request, Context context) {
        request.setAndroidDeviceId(AndroidDeviceId.getDpId(context));
    }

    /* access modifiers changed from: protected */
    public void setAdfonicVersion(AndroidRequest request) {
        request.setAdfonicSdkVersion("Adfonic/Android/1.1.5");
    }

    /* access modifiers changed from: protected */
    public void setHardwareVersion(AndroidRequest request) {
        request.setHardwareVersion("Android/" + Build.VERSION.RELEASE + "/" + Build.MODEL);
        request.setOsName(Build.VERSION.SDK);
        request.setOsVersion(Build.VERSION.RELEASE);
        request.setAndroidSdkVersion(Build.VERSION.SDK_INT);
        request.setDeviceName(Build.DEVICE);
    }

    /* access modifiers changed from: protected */
    public void setUserAgent(AndroidRequest request, Context context) {
        if (USER_AGENT == null) {
            USER_AGENT = UserAgentBuilder.getUserAgentString(context);
        }
        request.setUserAgent(USER_AGENT);
    }

    /* access modifiers changed from: protected */
    public void setLocation(AndroidRequest request, Context context) {
        try {
            if (request.getLocation() != null) {
                request.setLongitude("" + request.getLocation().getLongitude());
                request.setLatitude("" + request.getLocation().getLatitude());
            } else if (request.isAllowLocation()) {
                Location l = getLocation(context);
                request.setLocation(l);
                request.setLongitude("" + l.getLongitude());
                request.setLatitude("" + l.getLatitude());
            }
        } catch (Exception e) {
        }
    }

    private Location getLocation(Context context) {
        LocationManager locManager = (LocationManager) context.getSystemService("location");
        Location locNetwork = null;
        Location locGPS = null;
        if (Permission.hasFineGrainLocationAccess(context)) {
            locNetwork = locManager.getLastKnownLocation("network");
        }
        if (Permission.hasFineGrainLocationAccess(context)) {
            locGPS = locManager.getLastKnownLocation("gps");
        }
        if (locNetwork == null && locGPS != null) {
            return locGPS;
        }
        if (locGPS == null && locNetwork != null) {
            return locNetwork;
        }
        if (locGPS == null) {
            return null;
        }
        if (locGPS.getTime() <= locNetwork.getTime()) {
            return locNetwork;
        }
        return locGPS;
    }
}
