package com.adfonic.android.api;

import android.location.Location;
import java.util.List;

public class Request {
    public static final boolean ALLOW_LOCATION_DEFAULT_VALUE = true;
    public static final boolean REFRESH_DEFAULT_VALUE = true;
    public static final int REFRESH_TIME_DEFAULT_VALUE = 20;
    public static final boolean TEST_MODE_DEFAULT_VALUE = false;
    private float adHeight;
    private float adWidth;
    private int age;
    /* access modifiers changed from: private */
    public int ageHigh;
    /* access modifiers changed from: private */
    public int ageLow;
    private String dateOfBirth;
    private boolean hasGender;
    private boolean isAllowLocation = true;
    private boolean isMale;
    private boolean isRefreshAd = true;
    private boolean isTest;
    private String language;
    private Location location;
    private int refreshTime = 20;
    private String slotId;
    private List<String> tags;

    public String getSlotId() {
        return this.slotId;
    }

    public void setSlotId(String slotId2) {
        this.slotId = slotId2;
    }

    public boolean isTest() {
        return this.isTest;
    }

    public void setTest(boolean isTest2) {
        this.isTest = isTest2;
    }

    public List<String> getTags() {
        return this.tags;
    }

    public void setTags(List<String> tags2) {
        this.tags = tags2;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language2) {
        this.language = language2;
    }

    public int getAge() {
        return this.age;
    }

    public void setAge(int age2) {
        this.age = age2;
    }

    public String getDateOfBirth() {
        return this.dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth2) {
        this.dateOfBirth = dateOfBirth2;
    }

    public boolean isMale() {
        return this.isMale;
    }

    public void setMale(boolean isMale2) {
        this.hasGender = true;
        this.isMale = isMale2;
    }

    public int getAgeLow() {
        return this.ageLow;
    }

    public void setAgeLow(int ageLow2) {
        this.ageLow = ageLow2;
    }

    public int getAgeHigh() {
        return this.ageHigh;
    }

    public void setAgeHigh(int ageHigh2) {
        this.ageHigh = ageHigh2;
    }

    public void setLocation(Location location2) {
        this.location = location2;
    }

    public Location getLocation() {
        return this.location;
    }

    public boolean isAllowLocation() {
        return this.isAllowLocation;
    }

    public void setAllowLocation(boolean isAllowLocation2) {
        this.isAllowLocation = isAllowLocation2;
    }

    public boolean isRefreshAd() {
        return this.isRefreshAd;
    }

    public void setRefreshAd(boolean isRefreshAd2) {
        this.isRefreshAd = isRefreshAd2;
    }

    public int getRefreshTime() {
        return this.refreshTime;
    }

    public void setRefreshTime(int refreshTime2) {
        if (refreshTime2 < 20) {
            this.refreshTime = 20;
        } else if (refreshTime2 > 120) {
            this.refreshTime = 120;
        } else {
            this.refreshTime = refreshTime2;
        }
    }

    public boolean hasGender() {
        return this.hasGender;
    }

    public void setHasGender(boolean hasGender2) {
        this.hasGender = hasGender2;
    }

    public float getAdHeight() {
        return this.adHeight;
    }

    public void setAdHeight(float adHeight2) {
        this.adHeight = adHeight2;
    }

    public float getAdWidth() {
        return this.adWidth;
    }

    public void setAdWidth(float adWidth2) {
        this.adWidth = adWidth2;
    }

    public static class RequestBuilder {
        private Request request;

        public RequestBuilder() {
            this(new Request());
        }

        public RequestBuilder(Request request2) {
            this.request = request2;
        }

        public RequestBuilder withSlotId(String slotId) {
            this.request.setSlotId(slotId);
            return this;
        }

        public RequestBuilder withAge(int age) {
            this.request.setAge(age);
            return this;
        }

        public RequestBuilder withAgeHigh(int ageHigh) {
            this.request.setAgeHigh(ageHigh);
            return this;
        }

        public RequestBuilder withAgeLow(int ageLow) {
            this.request.setAgeLow(ageLow);
            return this;
        }

        public RequestBuilder withAgeRange(int low, int high) {
            int unused = this.request.ageLow = low;
            int unused2 = this.request.ageHigh = high;
            return this;
        }

        public RequestBuilder withDateOfBirth(String dateOfBirth) {
            this.request.setDateOfBirth(dateOfBirth);
            return this;
        }

        public RequestBuilder withIsMale(boolean isMale) {
            this.request.setMale(isMale);
            return this;
        }

        public RequestBuilder withIsTest(boolean isTest) {
            this.request.setMale(isTest);
            return this;
        }

        public RequestBuilder withLanguage(String language) {
            this.request.setLanguage(language);
            return this;
        }

        public RequestBuilder withLocation(Location location) {
            this.request.setLocation(location);
            return this;
        }

        public RequestBuilder withTags(List<String> tags) {
            this.request.setTags(tags);
            return this;
        }

        public RequestBuilder withAllowLocation(boolean allowLocation) {
            this.request.setAllowLocation(allowLocation);
            return this;
        }

        public RequestBuilder withRefreshTime(int refreshTime) {
            this.request.setRefreshTime(refreshTime);
            return this;
        }

        public RequestBuilder withRefreshAd(boolean isRefreshAd) {
            this.request.setRefreshAd(isRefreshAd);
            return this;
        }

        public Request build() {
            return this.request;
        }
    }
}
