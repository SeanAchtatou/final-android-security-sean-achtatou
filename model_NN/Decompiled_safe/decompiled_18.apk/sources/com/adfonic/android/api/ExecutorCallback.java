package com.adfonic.android.api;

import com.adfonic.android.api.response.ApiResponse;

public interface ExecutorCallback {
    void onResponse(ApiResponse apiResponse);

    void onThrowable(Throwable th);
}
