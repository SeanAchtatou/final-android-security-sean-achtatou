package com.adfonic.android.api.request.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.text.TextUtils;
import com.adfonic.android.utils.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class AndroidDeviceId {
    private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
    private static final String PREFERENCE_DEVICE_ID = "UUID";

    public static String getDpId(Context context) {
        String androidId = getAndroidDeviceId(context);
        if (TextUtils.isEmpty(androidId)) {
            return "";
        }
        String SHA1 = SHA1(androidId);
        if (androidId.equals("9774d56d682e549c")) {
            return getTestRandomDeviceIdFromPreferences(context);
        }
        return SHA1;
    }

    private static String getTestRandomDeviceIdFromPreferences(Context context) {
        SharedPreferences settings = context.getSharedPreferences(Log.TAG, 0);
        if (settings.getString(PREFERENCE_DEVICE_ID, null) != null) {
            return settings.getString(PREFERENCE_DEVICE_ID, "");
        }
        String randomUUID = SHA1(UUID.randomUUID().toString());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFERENCE_DEVICE_ID, randomUUID);
        editor.commit();
        return randomUUID;
    }

    private static String getAndroidDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    private static String SHA1(String text) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(text.getBytes());
            return asHex(digest.digest());
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    private static String asHex(byte[] buf) {
        char[] chars = new char[(buf.length * 2)];
        for (int i = 0; i < buf.length; i++) {
            chars[i * 2] = HEX_CHARS[(buf[i] & 240) >>> 4];
            chars[(i * 2) + 1] = HEX_CHARS[buf[i] & 15];
        }
        return new String(chars);
    }
}
