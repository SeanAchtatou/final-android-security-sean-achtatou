package com.adfonic.android;

public interface AdListener {
    void onClick();

    void onDismissScreen();

    void onInternalError();

    void onInvalidRequest();

    void onLeaveApplication();

    void onNetworkError();

    void onNoFill();

    void onPresentScreen();

    void onReceivedAd();
}
