package com.adfonic.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.adfonic.android.utils.Base64;
import com.adfonic.android.utils.HtmlFormatter;
import com.adfonic.android.utils.Log;
import com.adfonic.android.view.task.UrlOpenerTask;

public class AdfonicActivity extends Activity {
    private static final String AUDIO_TYPE = "audio";
    private static final String ENCODED_IMAGE = "iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RUMxNUFDNkIzMDg2MTFFMTk2MDBBQUZDMDU2MEMzRDciIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RUMxNUFDNkMzMDg2MTFFMTk2MDBBQUZDMDU2MEMzRDciPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpFQzE1QUM2OTMwODYxMUUxOTYwMEFBRkMwNTYwQzNENyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpFQzE1QUM2QTMwODYxMUUxOTYwMEFBRkMwNTYwQzNENyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pk0EGf8AAAaKSURBVHjavFhpSFVbFF7nXgMrqDTDoro3M7N6TfgQHEKaywbCwh9WviSwgaBeEvQjJQiiiczmaNRfRdBAOTTYsyxtwoIywiwkS0mpbJ7Nt771zjqce7q3IuIt+Fjbe/bZ69t7r+lofPr0iVwuFxmGIVqlra1N9NevX2XMOrK1tTWRdQLraIaHEcogxnNG/ZcvX2pYVzIqePzAfCZrqLYDYnz48EEMK0DEToIRx5jDLyQzvLpYILBh6IeMEh4XsL4SiATWN969e0dut9uHgEkinJHFkzIZIXihuLiYCgsLqaamhh49ekQtLS1CtEuXLtSzZ0/q27cvjRo1ipKSkpRQC5PYwzqX32+yn4hF4M2bN0JASZhX8Sevm8cYgUmHDh2i1atX0507d+hnpH///rRw4UJKTk7WU7nEBv/mcZXzFIxXr175EGAkMfKZSMTt27dpwYIFVFlZSb8iMTExtHLlSoqMjASROkYGo9xOwsAxBgUFKYkYNn6M4Tl9+jRNmjSJfofs3r2b4uPjQaKekcK4oT7hsjlHd8YWhqeoqMgyHhwcTGvXrqXt27dT586df2gMc3bs2EHr1q2TdyHz5s2jixcv4nQ9jC2M7hp17mXLlqnT5bBOq66uptGjR8uLOJXs7GxKSUmhiIgIio6OloU+fvwY0PjGjRvFEQcNGkRdu3alCxcuiLNhU+PGjcNvHkQ3oxS/u7OyskAglpHHCE5LS6PHjx9T+/btKScnh1JTUy3n7NOnDw0YMIDKy8u/IYFI2LRpE40cOdLyp6FDh1J4eDhVVFRIeN67d4+mT5+O6X8w/mE0upcuXWrw5Gx4/JEjR+T4IGvWrKEZM2b45AgsjJMACexMSWDneXl5NGbMGGuekhg2bBh16NCBzp8/T0+ePJFQjYqKwt20MopdfP9exlT4Ao7PviMsAgd1YuzYsbR582bq1q2bYOvWrXK8eNauXTtrno579Ojh45DmhqYyvEZdXd1fPCmfYSCZaIrE/eXm5sp92pKTz7i2tlbGiHt/z6FLS0slJ2jSgnH4GW+6DWGJExjBMPLz8y3jkGfPntGSJUvE6Zw7UsDRAH+nBJSVlfkY19py+PBhEMHVjwCBKIThiRMnvvHqFy9e0KJFi8TpAhnRa7JrAMYzMzN9jKucOnVKryHKZVY1amho8BtaIDF//nw6d+5cQGOOTEpnzpyhuXPnyrv+pLGxUed6XFpSX758GTC56EkgjJxFy149IXfv3pX0Hcg4BLbMdUKDYByLaP3/vwQnBpsus5n4bppFSO7atYsGDhz4w4WRI3bu3CnvfG89M188d5kFwidWnZP3799PEyZM8Pnd7JKsJuTz58+E7gpANgSJQJtCuJtXUA8CtVgEicQpISEhtHfvXqkNaggGkAEV6KiA9+/fC9DgvH37luLi4iQ1+yMxbdo0JVBrcJ62ElFsbKyVC8LCwmjbtm1iXHfr1OiMMEZ61d+cQB5BTVGnhGGkZCQi3pAkIjQIjdghFlLZsGGD1HDdEYDuCXj9+rVUt5kzZ9KsWbOopKSE0Nj4w5AhQwgVVwUV1dx9I/tAOfoBNJAnQSAjI8Oa2NzcbBlzAs3K8uXL6enTp4IVK1bQ2bNnA5JAVlUBGZPAScZDd3p6OsKwmZ+l9uvXL/j69evU1NQkJRRVzOv1WvcMICuuWrVKiKjAL65evSqOHBoa6uMjyHr79u0TH4JfoFFBakHDy2gQAkhOTCKUkTh48GBCWYbDXb58mTp27Ei9evUS4yC1fv16ORmngERVVZXUf0QO/kYhKigokLUgR48elecsWxgFkg9mz56t2ayadTw6FpDAMcOxbt68SZ06dZI2HE7pz7gKQvHWrVviwPfv36eDBw/KziHHjx+nxMREDCvM3ctCBhxIczlHQgzrY6w9165do8WLF/+WrIfvifHjxyMq6hm+Tak9fMwH6Yw63BfK5vDhw3/ZcEJCgpzIxIkTccp1jHTGDXuX5TI/v+wkEJapjEtovw4cOCAhyW3UTxvGFeL44TMIQ5ZLcHI2Xq7dsKXxqWW1yLbSytcQzshiZHIjEoLyiwXRC3IXJSVVkwsyZu/evSXGp0yZQpMnT1YjaAb2MHJ5k01+NvsfAc1QdhK2uh/HBOawTmZ4tSuyd0fO/gDxzShhAgWMK46PXZ9MGWRvlXASqrVM89/4ur3C40iEKS+cwDoazYQZupj3HA7GuoZRyahgQw+0L7SL9hD6r4B/BRgAsfqsx9kuDxQAAAAASUVORK5CYII=";
    private static final String INTENT_EXTRA_ACTIVITY_START_TYPE = "activityStartType";
    private static final String INTENT_EXTRA_AFTER_MEDIA_URL = "afterMediaUrl";
    private static final String INTENT_EXTRA_MEDIA_TYPE = "mediaType";
    private static final String INTENT_EXTRA_MEDIA_URL = "mediaUrl";
    private static final String INTENT_EXTRA_URL = "url";
    private static final String INTENT_INTERSTITIAL_AD = "interstitialAd";
    private static final int INTERSTITIAL_TIMEOUT = 3000;
    private static final String MEDIA_PLAYER = "MEDIA_PLAYER";
    private static final String VIDEO_TYPE = "video";
    private String activityStartType = "";
    private String afterMediaUrl;
    /* access modifiers changed from: private */
    public Handler mainHandler;
    private MediaController mediaController;
    /* access modifiers changed from: private */
    public VideoView mediaPlayerView;
    private String mediaUrl;
    private Uri uri;

    public static Intent getPlayVideoIntent(String mediaUrl2, Context context) {
        return getPlayMediaIntent(mediaUrl2, VIDEO_TYPE, context);
    }

    public static Intent getPlayAudioIntent(String mediaUrl2, Context context) {
        return getPlayMediaIntent(mediaUrl2, AUDIO_TYPE, context);
    }

    public static Intent getOpenUrlIntent(String url, Context context) {
        Intent i = new Intent(context, AdfonicActivity.class);
        i.setFlags(524288);
        i.setFlags(536870912);
        i.setFlags(8388608);
        i.putExtra(INTENT_EXTRA_URL, url);
        return i;
    }

    private static Intent getPlayMediaIntent(String mediaUrl2, String type, Context context) {
        Intent i = new Intent(context, AdfonicActivity.class);
        i.putExtra(INTENT_EXTRA_MEDIA_URL, mediaUrl2);
        i.putExtra(INTENT_EXTRA_MEDIA_TYPE, VIDEO_TYPE);
        i.putExtra(INTENT_EXTRA_ACTIVITY_START_TYPE, MEDIA_PLAYER);
        return i;
    }

    public static Intent getStartInterstitialIntent(String content, Context context) {
        Intent i = new Intent(context, AdfonicActivity.class);
        i.putExtra(INTENT_INTERSTITIAL_AD, content);
        return i;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        disableWindowTitle();
        this.mainHandler = new Handler();
        getIntentParameters();
        setNoTitleAndFullScreen();
        if (isMediaPlayer()) {
            setupMediaPlayer();
            startMedia();
        } else if (isInterstitial()) {
            showInterstitialContent();
            setInterstitialTimeout();
        } else if (this.uri == null) {
            finish();
        } else {
            LinearLayout container = buildLayoutContainer();
            createLayout(this.uri, null, container);
            setContentView(container);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public void onResume() {
        super.onResume();
        if (isMediaPlayer()) {
            restartVideo();
        }
    }

    public void onPause() {
        super.onPause();
        if (isMediaPlayer() && !hasVideoFinishedPlaying()) {
            this.mediaPlayerView.pause();
        }
    }

    public void onDestroy() {
        if (isMediaPlayer()) {
            this.mainHandler.post(new Runnable() {
                public void run() {
                    try {
                        if (!AdfonicActivity.this.hasVideoFinishedPlaying()) {
                            AdfonicActivity.this.mediaPlayerView.stopPlayback();
                        }
                    } catch (Exception e) {
                        if (Log.errorLoggingEnabled()) {
                            Log.e("Stop playback error " + e);
                        }
                    }
                }
            });
        }
        super.onDestroy();
    }

    private boolean isInterstitial() {
        Intent i = getIntent();
        if (i != null && i.hasExtra(INTENT_INTERSTITIAL_AD)) {
            return true;
        }
        return false;
    }

    private void setInterstitialTimeout() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                try {
                    AdfonicActivity.this.finish();
                } catch (Exception e) {
                }
            }
        }, 3000);
    }

    private void setNoTitleAndFullScreen() {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
    }

    private boolean isMediaPlayer() {
        return MEDIA_PLAYER.equals(this.activityStartType);
    }

    private void startMedia() {
        this.mediaPlayerView.setVideoURI(Uri.parse(this.mediaUrl));
    }

    private void getIntentParameters() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mediaUrl = extras.getString(INTENT_EXTRA_MEDIA_URL);
            this.afterMediaUrl = extras.getString(INTENT_EXTRA_AFTER_MEDIA_URL);
            this.activityStartType = extras.getString(INTENT_EXTRA_ACTIVITY_START_TYPE);
        }
        this.uri = getUri(getIntent());
    }

    private void disableWindowTitle() {
        requestWindowFeature(1);
    }

    private void restartVideo() {
        if (!this.mediaPlayerView.isPlaying()) {
            this.mediaPlayerView.start();
        }
    }

    private void startBrowser() {
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.afterMediaUrl)));
        } catch (Exception e) {
            if (Log.errorLoggingEnabled()) {
                Log.e("IntentBrowser Activity not found " + e.getMessage());
            }
        }
        finish();
    }

    private void setupMediaPlayer() {
        LinearLayout mainLayout = new LinearLayout(this);
        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mainLayout.setKeepScreenOn(true);
        mainLayout.setOrientation(1);
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        frameLayout.setForegroundGravity(17);
        this.mediaPlayerView = new VideoView(this);
        this.mediaPlayerView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.mediaController = new MediaController(this);
        this.mediaController.setAnchorView(frameLayout);
        this.mediaController.setMediaPlayer(this.mediaPlayerView);
        this.mediaPlayerView.setMediaController(this.mediaController);
        mainLayout.addView(frameLayout);
        frameLayout.addView(this.mediaPlayerView);
        setContentView(mainLayout);
        prepareVideo(this.mediaController);
        setErrorListener();
        setCompletionListener();
    }

    private void setCompletionListener() {
        this.mediaPlayerView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer arg0) {
                AdfonicActivity.this.startBrowserIfNecessary();
            }
        });
    }

    private void setErrorListener() {
        this.mediaPlayerView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, final int what, final int extra) {
                AdfonicActivity.this.mainHandler.post(new Runnable() {
                    public void run() {
                        if (Log.errorLoggingEnabled()) {
                            Log.e("Error playing ad media. What=" + what + " extra=" + extra);
                        }
                    }
                });
                return true;
            }
        });
    }

    private void prepareVideo(final MediaController mediaController2) {
        this.mediaPlayerView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                AdfonicActivity.this.mainHandler.post(new Runnable() {
                    public void run() {
                        try {
                            AdfonicActivity.this.mediaPlayerView.start();
                            mediaController2.show(0);
                            AdfonicActivity.this.mediaPlayerView.invalidate();
                        } catch (Exception e) {
                            if (Log.errorLoggingEnabled()) {
                                Log.e("Handler Exception " + e.getMessage());
                            }
                        }
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public void startBrowserIfNecessary() {
        if (this.afterMediaUrl != null) {
            startBrowser();
        }
    }

    private Uri getUri(Intent i) {
        String extra = i.getStringExtra(INTENT_EXTRA_URL);
        if (TextUtils.isEmpty(extra)) {
            return null;
        }
        try {
            return Uri.parse(extra);
        } catch (Exception e) {
            return null;
        }
    }

    private void showInterstitialContent() {
        String content = getIntent().getStringExtra(INTENT_INTERSTITIAL_AD);
        RelativeLayout rLayout = new RelativeLayout(this);
        rLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        LinearLayout ll = buildLayoutContainer();
        createLayout(null, content, ll);
        ImageView image = new ImageView(this);
        setCloseButtonImageBitmap(image);
        image.setPadding(15, 15, 15, 15);
        image.setOnClickListener(new View.OnClickListener() {
            public void onClick(View paramView) {
                AdfonicActivity.this.finish();
            }
        });
        RelativeLayout.LayoutParams tParams = new RelativeLayout.LayoutParams(-2, -2);
        tParams.addRule(11, -1);
        tParams.addRule(10, -1);
        image.setLayoutParams(tParams);
        rLayout.addView(ll);
        rLayout.addView(image);
        setContentView(rLayout);
    }

    private void setCloseButtonImageBitmap(ImageView view) {
        try {
            byte[] imageAsBytes = Base64.decode(ENCODED_IMAGE.getBytes(), 0);
            view.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        } catch (Exception e) {
        }
    }

    private void createLayout(Uri uri2, String content, ViewGroup container) {
        WebView webView = new WebView(this);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        if (uri2 != null) {
            webView.loadUrl(uri2.toString());
        } else {
            webView.loadDataWithBaseURL(null, new HtmlFormatter().applyHtmlFormatting(content), HtmlFormatter.TEXT_HTML, HtmlFormatter.UTF_8, null);
        }
        keepAllWebContentInsideWebView(webView);
        container.addView(webView);
    }

    private void keepAllWebContentInsideWebView(WebView webView) {
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.startsWith(UrlOpenerTask.MARKET_SEARCH)) {
                    try {
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.setData(Uri.parse(url));
                        AdfonicActivity.this.startActivity(intent);
                        AdfonicActivity.this.finish();
                        return false;
                    } catch (Exception e) {
                        return false;
                    }
                } else {
                    view.loadUrl(url);
                    return false;
                }
            }
        });
    }

    private LinearLayout buildLayoutContainer() {
        LinearLayout ll = new LinearLayout(this);
        ll.setOrientation(1);
        ll.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        ll.setGravity(17);
        return ll;
    }

    /* access modifiers changed from: private */
    public boolean hasVideoFinishedPlaying() {
        return this.mediaPlayerView == null || !this.mediaPlayerView.isPlaying();
    }
}
