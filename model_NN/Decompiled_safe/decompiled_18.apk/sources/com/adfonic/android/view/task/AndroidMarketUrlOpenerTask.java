package com.adfonic.android.view.task;

public abstract class AndroidMarketUrlOpenerTask extends UrlOpenerTask {
    /* access modifiers changed from: protected */
    public void onUrlReceived(String destinationUrl) {
        openAndroidMarket(destinationUrl);
    }

    /* access modifiers changed from: protected */
    public void openUrl(String destinationUrl) {
    }
}
