package com.adfonic.android.view.task;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import com.adfonic.android.utils.Log;
import com.adfonic.android.utils.Network;

public abstract class UrlOpenerTask extends AsyncTask<String, Void, Void> {
    public static final String MARKET_SEARCH = "market://";

    /* access modifiers changed from: protected */
    public abstract Context getContext();

    /* access modifiers changed from: protected */
    public abstract void openUrl(String str);

    /* access modifiers changed from: protected */
    public Void doInBackground(String... urls) {
        onUrlReceived(Network.requestDestinationUrl(urls[0]));
        return null;
    }

    /* access modifiers changed from: protected */
    public void onUrlReceived(String destinationUrl) {
        if (isAndroidMarketUrl(destinationUrl)) {
            openAndroidMarket(destinationUrl);
        } else {
            openUrl(destinationUrl);
        }
    }

    /* access modifiers changed from: protected */
    public void openAndroidMarket(String url) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            intent.setData(Uri.parse(url));
            getContext().startActivity(intent);
        } catch (Exception e) {
            Log.e("Error opening Android Market from ad (" + url + ")", e);
        }
    }

    public static boolean isAndroidMarketUrl(String url) {
        if (url != null && url.contains(MARKET_SEARCH)) {
            return true;
        }
        return false;
    }
}
