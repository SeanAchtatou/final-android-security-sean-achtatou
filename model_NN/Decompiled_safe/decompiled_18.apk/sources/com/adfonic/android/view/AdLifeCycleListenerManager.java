package com.adfonic.android.view;

import android.app.Activity;
import android.content.Context;
import com.adfonic.android.AdListener;

public class AdLifeCycleListenerManager {
    private AdListener adListener;

    public AdListener getAdListener() {
        return this.adListener;
    }

    public void setAdListener(AdListener adListener2) {
        this.adListener = adListener2;
    }

    public void onThrowable(Context c) {
        runOnUiThread(c, new Runnable() {
            public void run() {
                try {
                    AdLifeCycleListenerManager.this.getAdListener().onInternalError();
                } catch (Throwable th) {
                }
            }
        });
    }

    public void onAdReceived(Context c) {
        runOnUiThread(c, new Runnable() {
            public void run() {
                try {
                    AdLifeCycleListenerManager.this.getAdListener().onReceivedAd();
                } catch (Throwable th) {
                }
            }
        });
    }

    public void onAdClick(Context c) {
        runOnUiThread(c, new Runnable() {
            public void run() {
                try {
                    AdLifeCycleListenerManager.this.getAdListener().onClick();
                } catch (Throwable th) {
                }
            }
        });
    }

    public void onPresentScreen(Context c) {
        runOnUiThread(c, new Runnable() {
            public void run() {
                try {
                    AdLifeCycleListenerManager.this.getAdListener().onPresentScreen();
                } catch (Throwable th) {
                }
            }
        });
    }

    public void onDismissScreen(Context c) {
        runOnUiThread(c, new Runnable() {
            public void run() {
                try {
                    AdLifeCycleListenerManager.this.getAdListener().onDismissScreen();
                } catch (Throwable th) {
                }
            }
        });
    }

    private void runOnUiThread(Context c, Runnable r) {
        Activity a = getActivityForCallback(c);
        if (a != null) {
            try {
                a.runOnUiThread(r);
            } catch (Throwable th) {
            }
        }
    }

    private Activity getActivityForCallback(Context c) {
        if (getAdListener() == null) {
            return null;
        }
        if (c == null) {
            return null;
        }
        try {
            return (Activity) c;
        } catch (Throwable th) {
            return null;
        }
    }
}
