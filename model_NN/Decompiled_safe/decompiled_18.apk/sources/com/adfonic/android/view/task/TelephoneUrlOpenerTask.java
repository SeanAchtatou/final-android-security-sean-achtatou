package com.adfonic.android.view.task;

import android.content.Intent;
import android.net.Uri;
import com.adfonic.android.utils.Log;

public abstract class TelephoneUrlOpenerTask extends UrlOpenerTask {
    /* access modifiers changed from: protected */
    public void onUrlReceived(String destinationUrl) {
        dialTelephone(destinationUrl);
    }

    private void dialTelephone(String telephoneUrl) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.DIAL");
            intent.setData(Uri.parse(telephoneUrl));
            getContext().startActivity(intent);
        } catch (Exception e) {
            Log.e("Error dialling ad telephone number");
        }
    }

    /* access modifiers changed from: protected */
    public void openUrl(String destinationUrl) {
    }
}
