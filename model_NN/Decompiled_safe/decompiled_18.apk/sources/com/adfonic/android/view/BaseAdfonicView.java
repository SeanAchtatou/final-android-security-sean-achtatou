package com.adfonic.android.view;

import android.content.Context;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.adfonic.android.AdListener;
import com.adfonic.android.AdfonicActivity;
import com.adfonic.android.api.ExecutorCallback;
import com.adfonic.android.api.Request;
import com.adfonic.android.api.RequestExecutor;
import com.adfonic.android.api.request.XmlAttributeRequestReader;
import com.adfonic.android.api.response.ApiResponse;
import com.adfonic.android.utils.Log;
import com.adfonic.android.view.task.AmazonMarketUrlOpenerTask;
import com.adfonic.android.view.task.AndroidMarketUrlOpenerTask;
import com.adfonic.android.view.task.TelephoneUrlOpenerTask;
import com.adfonic.android.view.task.UrlOpenerTask;

public abstract class BaseAdfonicView extends WebView implements View.OnClickListener {
    public static final String VERSION = "1.1.5";
    /* access modifiers changed from: private */
    public AdLifeCycleListenerManager adListenerManager;
    private ExecutorCallback callback;
    private boolean directLoading;
    /* access modifiers changed from: private */
    public Runnable query;
    /* access modifiers changed from: private */
    public Request request;
    private RequestExecutor requestExecutor;
    private ApiResponse response;

    /* access modifiers changed from: protected */
    public abstract void expand();

    /* access modifiers changed from: protected */
    public abstract void loadAdContent(String str);

    /* access modifiers changed from: protected */
    public abstract void open(String str);

    public BaseAdfonicView(Context context) {
        this(context, null);
    }

    public BaseAdfonicView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public BaseAdfonicView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.query = new Runnable() {
            public void run() {
                Handler h = BaseAdfonicView.this.getHandler();
                if (!BaseAdfonicView.this.isRequestInvalid() && !BaseAdfonicView.this.isInterstitial()) {
                    BaseAdfonicView.this.executeCall(h);
                    if (BaseAdfonicView.this.request.isRefreshAd() && BaseAdfonicView.this.request.getRefreshTime() > 0) {
                        h.postDelayed(BaseAdfonicView.this.query, (long) (BaseAdfonicView.this.request.getRefreshTime() * 1000));
                    }
                }
            }
        };
        this.callback = new ExecutorCallback() {
            public void onResponse(ApiResponse response) {
                if (Log.infoLoggingEnabled()) {
                    Log.i("receive an ad from server: \n" + response);
                }
                if (response.isError()) {
                    if (Log.errorLoggingEnabled()) {
                        Log.e("Issue while fetching ad: " + response.getErrorMessage());
                    }
                    BaseAdfonicView.this.setVisibility(8);
                    BaseAdfonicView.this.adListenerManager.onThrowable(BaseAdfonicView.this.getContext());
                    return;
                }
                BaseAdfonicView.this.setResponse(response);
                BaseAdfonicView.this.adListenerManager.onAdReceived(BaseAdfonicView.this.getContext());
                if (!BaseAdfonicView.this.isInterstitial()) {
                    BaseAdfonicView.this.loadResponse(response);
                }
            }

            public void onThrowable(Throwable t) {
                if (Log.errorLoggingEnabled()) {
                    Log.e("Can not fetch add", t);
                }
                BaseAdfonicView.this.adListenerManager.onThrowable(BaseAdfonicView.this.getContext());
            }
        };
        if (attrs != null) {
            this.request = new XmlAttributeRequestReader().convertToRequest(attrs, getContext());
        }
        if (isNotOnline()) {
            setVisibility(8);
        } else {
            init();
        }
    }

    public void showInterstitial() {
        if (!isInterstitial()) {
            Log.w("Response received is not an interstitial ad, please verify the settings!");
        } else {
            startInterstitial(this.response.getAdContent());
        }
    }

    public void loadAd(Request request2) {
        if (request2 == null) {
            Log.w("Can't load a null request!");
            return;
        }
        this.directLoading = true;
        setRequest(request2);
        executeCall(new Handler());
    }

    public void setRequest(Request request2) {
        this.request = request2;
    }

    public Request getRequest() {
        return this.request;
    }

    public void setAdListener(AdListener adListener) {
        this.adListenerManager.setAdListener(adListener);
        setOnClickListener(this);
    }

    public void onClick(View view) {
        this.adListenerManager.onAdClick(getContext());
    }

    /* access modifiers changed from: protected */
    public void setBackgroundToTransparent() {
        setBackgroundColor(0);
        setBackgroundDrawable(null);
        tryToSetBackgroundTransparentFromApiLevel11On();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isNotOnline() && !this.directLoading) {
            post(this.query);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getHandler().removeCallbacks(this.query);
        if (this.requestExecutor != null) {
            this.requestExecutor.shutdown();
        }
        this.requestExecutor = null;
        if (this.adListenerManager != null) {
            this.adListenerManager.onDismissScreen(getContext());
            this.adListenerManager.setAdListener(null);
        }
    }

    /* access modifiers changed from: protected */
    public boolean isAudioResponse() {
        if (this.response == null) {
            return false;
        }
        return this.response.isAudioResponse();
    }

    /* access modifiers changed from: protected */
    public boolean isVideoResponse() {
        if (this.response == null) {
            return false;
        }
        return this.response.isVideoResponse();
    }

    /* access modifiers changed from: protected */
    public boolean isCallResponse() {
        if (this.response == null) {
            return false;
        }
        return this.response.isCallResponse();
    }

    /* access modifiers changed from: protected */
    public boolean isInterstitial() {
        if (this.response == null) {
            return false;
        }
        return this.response.isInterstitial();
    }

    /* access modifiers changed from: protected */
    public boolean isAndroidMarketResponse() {
        if (this.response == null) {
            return false;
        }
        return this.response.isAndroidDestinationType();
    }

    /* access modifiers changed from: protected */
    public boolean isAmazonMarketResponse() {
        if (this.response == null) {
            return false;
        }
        return this.response.isAmazonDestinationType();
    }

    /* access modifiers changed from: private */
    public void loadResponse(ApiResponse response2) {
        setVisibility(0);
        loadAdContent(response2.getAdContent());
        this.adListenerManager.onPresentScreen(getContext());
    }

    /* access modifiers changed from: private */
    public void setResponse(ApiResponse response2) {
        this.response = response2;
    }

    /* access modifiers changed from: private */
    public void executeCall(Handler handler) {
        if (this.requestExecutor == null) {
            Log.w("Api executor is null");
        } else if (isRequestInvalid()) {
            Log.w("Request is not valid, please be sure to provide the right advert slotId");
        } else {
            this.requestExecutor.execute(getContext().getApplicationContext(), this.request, this.callback, handler);
        }
    }

    /* access modifiers changed from: private */
    public boolean isRequestInvalid() {
        String slotId;
        if (this.request == null || (slotId = this.request.getSlotId()) == null || "null".equals(slotId)) {
            return true;
        }
        return false;
    }

    private boolean isNotOnline() {
        NetworkInfo netInfo = ((ConnectivityManager) getContext().getSystemService("connectivity")).getActiveNetworkInfo();
        return netInfo == null || !netInfo.isConnectedOrConnecting();
    }

    private void tryToSetBackgroundTransparentFromApiLevel11On() {
        if (Build.VERSION.SDK_INT >= 11) {
            Class<View> cls = View.class;
            try {
                cls.getMethod("setLayerType", Integer.TYPE, Paint.class).invoke(this, 1, new Paint());
            } catch (Exception e) {
            }
        }
    }

    private void init() {
        this.adListenerManager = new AdLifeCycleListenerManager();
        this.requestExecutor = new RequestExecutor(getContext());
        this.requestExecutor.start();
        setBackgroundToTransparent();
        setWebViewClient();
    }

    private void setWebViewClient() {
        setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                BaseAdfonicView.this.startAdClickThread(url);
                return true;
            }
        });
    }

    /* access modifiers changed from: private */
    public void startAdClickThread(final String url) {
        post(new Runnable() {
            public void run() {
                BaseAdfonicView.this.adListenerManager.onAdClick(BaseAdfonicView.this.getContext());
                if (BaseAdfonicView.this.isAudioResponse()) {
                    BaseAdfonicView.this.playAudio(url);
                } else if (BaseAdfonicView.this.isVideoResponse()) {
                    BaseAdfonicView.this.playVideo(url);
                } else if (BaseAdfonicView.this.isCallResponse()) {
                    BaseAdfonicView.this.executeTelephoneCall(url);
                } else if (BaseAdfonicView.this.isAndroidMarketResponse()) {
                    BaseAdfonicView.this.openAndroidMarket(url);
                } else if (BaseAdfonicView.this.isAmazonMarketResponse()) {
                    BaseAdfonicView.this.openAmazonMarket(url);
                } else {
                    BaseAdfonicView.this.executeOpenUrl(url);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void executeOpenUrl(String url) {
        new CustomUrlOpenerTask().execute(new String[]{url});
    }

    private class CustomUrlOpenerTask extends UrlOpenerTask {
        private CustomUrlOpenerTask() {
        }

        /* access modifiers changed from: protected */
        public Context getContext() {
            return BaseAdfonicView.this.getContext();
        }

        /* access modifiers changed from: protected */
        public void openUrl(String destinationUrl) {
            BaseAdfonicView.this.open(destinationUrl);
        }
    }

    /* access modifiers changed from: private */
    public void executeTelephoneCall(String url) {
        new TelephoneUrlOpenerTask() {
            /* access modifiers changed from: protected */
            public Context getContext() {
                return BaseAdfonicView.this.getContext();
            }
        }.execute(new String[]{url});
    }

    /* access modifiers changed from: private */
    public void openAndroidMarket(String url) {
        new AndroidMarketUrlOpenerTask() {
            /* access modifiers changed from: protected */
            public Context getContext() {
                return BaseAdfonicView.this.getContext();
            }
        }.execute(new String[]{url});
    }

    /* access modifiers changed from: private */
    public void openAmazonMarket(String url) {
        new AmazonMarketUrlOpenerTask() {
            /* access modifiers changed from: protected */
            public Context getContext() {
                return BaseAdfonicView.this.getContext();
            }

            /* access modifiers changed from: protected */
            public void openUrl(String destinationUrl) {
                BaseAdfonicView.this.open(destinationUrl);
            }
        }.execute(new String[]{url});
    }

    private void startInterstitial(String content) {
        Context c = getContext();
        c.startActivity(AdfonicActivity.getStartInterstitialIntent(content, c));
    }

    /* access modifiers changed from: private */
    public void playVideo(String mediaUrl) {
        Context c = getContext();
        c.startActivity(AdfonicActivity.getPlayVideoIntent(mediaUrl, c));
    }

    /* access modifiers changed from: private */
    public void playAudio(String mediaUrl) {
        Context c = getContext();
        c.startActivity(AdfonicActivity.getPlayAudioIntent(mediaUrl, c));
    }
}
