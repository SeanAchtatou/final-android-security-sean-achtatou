package com.adfonic.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.LinearLayout;
import com.adfonic.android.ormma.ExpandProperties;
import com.adfonic.android.ormma.OrmmaBridge;
import com.adfonic.android.ormma.OrmmaView;
import com.adfonic.android.ormma.js.JsOrmmaBridge;
import com.adfonic.android.utils.HtmlFormatter;
import com.adfonic.android.utils.Log;
import com.adfonic.android.view.BaseAdfonicView;

public class AdfonicView extends BaseAdfonicView implements ViewTreeObserver.OnGlobalLayoutListener, OrmmaView {
    private static final String ADFONIC_ORMMA_BRIDGE = "AdfonicOrmmaBridge";
    private OrmmaBridge bridge;
    private int currentOrientation;
    private int defaultHeight;
    private ViewGroup.LayoutParams defaultLayoutParams;
    private ViewGroup defaultParent;
    private int defaultWidth;
    private int defaultX;
    private int defaultY;
    private ExpandProperties expandedProperties;
    private boolean manageExpandState;
    private int maxHeight;
    private int maxWidth;

    public AdfonicView(Context context) {
        this(context, null);
    }

    public AdfonicView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public AdfonicView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.defaultHeight = -1;
        this.defaultWidth = -1;
        this.currentOrientation = -1;
        this.manageExpandState = false;
        init();
    }

    public void injectJavaScript(String str) {
        if (str != null) {
            try {
                super.loadUrl("javascript:" + str);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void onGlobalLayout() {
        int i = getResources().getConfiguration().keyboard;
        switch (getResources().getConfiguration().keyboardHidden) {
            case 1:
                this.bridge.onKeyboardChange(true);
                break;
        }
        if (this.currentOrientation < 0) {
            saveDefaultParams();
        }
        int orientation = getDisplay(getContext()).getOrientation();
        if (this.currentOrientation >= 0 && orientation != this.currentOrientation) {
            resetContentAreaToDefault();
            this.bridge.reset();
            saveDefaultParams();
        }
    }

    public void onSoftKeyboardShown(boolean isShowing) {
    }

    public int getMaxWidth() {
        return this.maxWidth;
    }

    public int getMaxHeight() {
        return this.maxHeight;
    }

    public int getDefaultX() {
        return this.defaultX;
    }

    public int getDefaultY() {
        return this.defaultY;
    }

    public int getDefaultWidth() {
        return this.defaultWidth;
    }

    public int getDefaultHeight() {
        return this.defaultHeight;
    }

    public ExpandProperties getExpandProperties() {
        if (this.expandedProperties == null) {
            this.expandedProperties = new ExpandProperties();
        }
        return this.expandedProperties;
    }

    public void setExpandProperties(ExpandProperties expandProperties) {
        if (expandProperties.isUseCustomClose()) {
            hideCloseButton();
        } else {
            showCloseButton();
        }
        this.expandedProperties = expandProperties;
    }

    public boolean isViewable() {
        return getVisibility() == 0;
    }

    public void open(String url) {
        Context c = getContext();
        c.startActivity(AdfonicActivity.getOpenUrlIntent(url, c));
    }

    public String getPlacementType() {
        return "inline";
    }

    public void resize(int width, int height) {
        changeContentArea(width, height);
    }

    public void hide() {
        setVisibility(8);
    }

    public void showDefaultSize() {
        setVisibility(0);
        resetContentAreaToDefault();
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        int[] position = new int[2];
        getLocationOnScreen(position);
        this.defaultX = position[0];
        this.defaultY = position[1];
        super.onWindowFocusChanged(hasWindowFocus);
    }

    public void expand(String url) {
        if (!TextUtils.isEmpty(url) && !"undefined".equals(url)) {
            loadUrl(url);
        }
        expand();
    }

    public void expand() {
        this.manageExpandState = true;
        ExpandProperties ep = getExpandProperties();
        int height = ep.getHeight();
        int width = ep.getWidth();
        if (height > this.maxHeight) {
            height = this.maxHeight;
        }
        if (width > this.maxWidth) {
            width = this.maxWidth;
        }
        changeContentArea(width, height);
    }

    public void setOrmmaBridge(OrmmaBridge ormmaBridge) {
        this.bridge = ormmaBridge;
    }

    public void enableAdLogging(boolean enabled) {
        Log.setAdLoggingEnabled(enabled);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        isKeyboardVisible(heightMeasureSpec);
        int requestWidth = 0;
        int requestHeight = 0;
        if (getRequest() != null) {
            requestWidth = (int) getRequest().getAdWidth();
            requestHeight = (int) getRequest().getAdHeight();
        }
        super.onMeasure(getMeasureSpec(widthMeasureSpec, getLayoutParams().width, requestWidth), getMeasureSpec(heightMeasureSpec, getLayoutParams().height, requestHeight));
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        this.bridge.onSizeChange(w, h);
    }

    /* access modifiers changed from: protected */
    public void loadAdContent(String adContent) {
        loadDataWithBaseURL("/", new HtmlFormatter().applyHtmlFormatting(adContent), HtmlFormatter.TEXT_HTML, HtmlFormatter.UTF_8, null);
        this.bridge.ready();
        resetExpandProperties();
    }

    private int getMeasureSpec(int measureSpec, int viewSize, int adSize) {
        int mode = View.MeasureSpec.getMode(measureSpec);
        if (mode == 0) {
            return getSizeToBeUsed(measureSpec, adSize);
        }
        if (mode != Integer.MIN_VALUE) {
            if (mode == 1073741824) {
            }
            return measureSpec;
        } else if (viewSize == -1 || viewSize != -2) {
            return measureSpec;
        } else {
            return getSizeToBeUsed(measureSpec, adSize);
        }
    }

    private int getSizeToBeUsed(int measureSpecSize, int adSizePixels) {
        if (adSizePixels <= 0) {
            return measureSpecSize;
        }
        return View.MeasureSpec.makeMeasureSpec((int) (getResolutionFactor((Activity) getContext()) * ((float) adSizePixels)), 1073741824);
    }

    private void isKeyboardVisible(int heightMeasureSpec) {
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        Rect rect = new Rect();
        ((Activity) getContext()).getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int statusBarHeight = rect.top;
        int screenHeight = ((Activity) getContext()).getWindowManager().getDefaultDisplay().getHeight();
        onSoftKeyboardShown((screenHeight - statusBarHeight) - height > screenHeight / 3);
    }

    private float getResolutionFactor(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        return (float) (metrics.densityDpi / 160);
    }

    private void init() {
        setVewViewCustomProperties();
        enableJavascript();
        addGlobalListener();
        createButton();
        showCloseButton();
        createOrmmaBridge();
    }

    private void createOrmmaBridge() {
        this.bridge = new JsOrmmaBridge(this);
        addJavascriptInterface(this.bridge, ADFONIC_ORMMA_BRIDGE);
        this.bridge.ready();
    }

    private void setVewViewCustomProperties() {
        setScrollContainer(false);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        setBackgroundColor(0);
    }

    private void enableJavascript() {
        getSettings().setJavaScriptEnabled(true);
    }

    private void addGlobalListener() {
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    private void createButton() {
    }

    private void hideCloseButton() {
    }

    private void showCloseButton() {
    }

    private void changeContentArea(final int width, final int height) {
        if (!this.manageExpandState) {
            Log.v("returning as do not have to manage state");
            return;
        }
        Log.v("changeContentArea");
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            public void run() {
                AdfonicView.this.changeContentAreaOnUiThread(width, height);
            }
        });
    }

    private void resetContentAreaToDefault() {
        if (!this.manageExpandState) {
            Log.v("returning as do not have to manage state");
        } else {
            ((Activity) getContext()).runOnUiThread(new Runnable() {
                public void run() {
                    AdfonicView.this.resetContentAreaToDefaultOnUiThread();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void resetContentAreaToDefaultOnUiThread() {
        ViewGroup parent = (ViewGroup) getParent();
        parent.removeView(this);
        ((ViewGroup) this.defaultParent.getRootView()).removeView(parent);
        ((Activity) getContext()).getWindow().clearFlags(1024);
        this.defaultParent.addView(this, 1, this.defaultLayoutParams);
    }

    /* access modifiers changed from: private */
    public void changeContentAreaOnUiThread(int width, int height) {
        this.defaultParent.removeView(this);
        LinearLayout.LayoutParams fl = createLinearLayoutParams(width, height);
        LinearLayout background = createBackground(fl);
        ViewGroup vg = (ViewGroup) this.defaultParent.getRootView();
        if (height == this.maxHeight) {
            ((Activity) getContext()).getWindow().setFlags(1024, 1024);
        }
        background.setPadding((this.maxWidth - width) / 2, (this.maxHeight - height) / 2, 0, 0);
        vg.addView(background, fl);
    }

    private LinearLayout.LayoutParams createLinearLayoutParams(int width, int height) {
        return new LinearLayout.LayoutParams(width, height);
    }

    private LinearLayout createBackground(LinearLayout.LayoutParams fl) {
        LinearLayout background = new LinearLayout(getContext());
        background.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View arg0, MotionEvent arg1) {
                return false;
            }
        });
        background.addView(this, fl);
        return background;
    }

    private void saveDefaultParams() {
        Display display = getDisplay(getContext());
        this.currentOrientation = display.getOrientation();
        DisplayMetrics dm = new DisplayMetrics();
        display.getMetrics(dm);
        this.maxWidth = dm.widthPixels;
        this.maxHeight = dm.heightPixels;
        this.defaultWidth = getWidth();
        this.defaultHeight = getHeight();
        this.defaultLayoutParams = getLayoutParams();
        int[] position = new int[2];
        getLocationOnScreen(position);
        this.defaultX = position[0];
        this.defaultY = position[1];
        resetExpandProperties();
        this.defaultParent = (ViewGroup) getParent();
    }

    private void resetExpandProperties() {
        ExpandProperties ep = getExpandProperties();
        ep.setHeight(getMaxHeight());
        ep.setWidth(getMaxWidth());
        setExpandProperties(ep);
    }

    private Display getDisplay(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
    }
}
