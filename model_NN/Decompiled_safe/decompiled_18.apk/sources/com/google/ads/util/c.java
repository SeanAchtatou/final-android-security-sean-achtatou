package com.google.ads.util;

import java.io.UnsupportedEncodingException;

public class c {
    static final /* synthetic */ boolean a = (!c.class.desiredAssertionStatus());

    public static abstract class a {
        public byte[] a;
        public int b;
    }

    public static byte[] a(String str) {
        return a(str.getBytes(), 0);
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        b bVar = new b(i3, new byte[((i2 * 3) / 4)]);
        if (!bVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (bVar.b == bVar.a.length) {
            return bVar.a;
        } else {
            byte[] bArr2 = new byte[bVar.b];
            System.arraycopy(bVar.a, 0, bArr2, 0, bVar.b);
            return bArr2;
        }
    }

    public static class b extends a {
        private static final int[] c = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private static final int[] d = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private int e;
        private int f;
        private final int[] g;

        public b(int i, byte[] bArr) {
            this.a = bArr;
            this.g = (i & 8) == 0 ? c : d;
            this.e = 0;
            this.f = 0;
        }

        public boolean a(byte[] bArr, int i, int i2, boolean z) {
            int i3;
            int i4;
            if (this.e == 6) {
                return false;
            }
            int i5 = i2 + i;
            int i6 = this.e;
            int i7 = this.f;
            int i8 = 0;
            byte[] bArr2 = this.a;
            int[] iArr = this.g;
            int i9 = i;
            while (true) {
                if (i9 < i5) {
                    if (i6 == 0) {
                        while (i9 + 4 <= i5 && (i7 = (iArr[bArr[i9] & 255] << 18) | (iArr[bArr[i9 + 1] & 255] << 12) | (iArr[bArr[i9 + 2] & 255] << 6) | iArr[bArr[i9 + 3] & 255]) >= 0) {
                            bArr2[i8 + 2] = (byte) i7;
                            bArr2[i8 + 1] = (byte) (i7 >> 8);
                            bArr2[i8] = (byte) (i7 >> 16);
                            i8 += 3;
                            i9 += 4;
                        }
                        if (i9 >= i5) {
                            i3 = i7;
                        }
                    }
                    int i10 = i9 + 1;
                    int i11 = iArr[bArr[i9] & 255];
                    switch (i6) {
                        case 0:
                            if (i11 >= 0) {
                                int i12 = i11;
                                i4 = i6 + 1;
                                i7 = i12;
                                continue;
                                i6 = i4;
                                i9 = i10;
                            } else if (i11 != -1) {
                                this.e = 6;
                                return false;
                            }
                            break;
                        case 1:
                            if (i11 >= 0) {
                                i7 = (i7 << 6) | i11;
                                i4 = i6 + 1;
                                continue;
                                i6 = i4;
                                i9 = i10;
                            } else if (i11 != -1) {
                                this.e = 6;
                                return false;
                            }
                            break;
                        case 2:
                            if (i11 >= 0) {
                                i7 = (i7 << 6) | i11;
                                i4 = i6 + 1;
                                continue;
                            } else if (i11 == -2) {
                                bArr2[i8] = (byte) (i7 >> 4);
                                i4 = 4;
                                i8++;
                            } else if (i11 != -1) {
                                this.e = 6;
                                return false;
                            }
                            i6 = i4;
                            i9 = i10;
                            break;
                        case 3:
                            if (i11 >= 0) {
                                i7 = (i7 << 6) | i11;
                                bArr2[i8 + 2] = (byte) i7;
                                bArr2[i8 + 1] = (byte) (i7 >> 8);
                                bArr2[i8] = (byte) (i7 >> 16);
                                i8 += 3;
                                i4 = 0;
                                continue;
                            } else if (i11 == -2) {
                                bArr2[i8 + 1] = (byte) (i7 >> 2);
                                bArr2[i8] = (byte) (i7 >> 10);
                                i8 += 2;
                                i4 = 5;
                            } else if (i11 != -1) {
                                this.e = 6;
                                return false;
                            }
                            i6 = i4;
                            i9 = i10;
                            break;
                        case 4:
                            if (i11 == -2) {
                                i4 = i6 + 1;
                                continue;
                                i6 = i4;
                                i9 = i10;
                            } else if (i11 != -1) {
                                this.e = 6;
                                return false;
                            }
                            break;
                        case 5:
                            if (i11 != -1) {
                                this.e = 6;
                                return false;
                            }
                            break;
                    }
                    i4 = i6;
                    i6 = i4;
                    i9 = i10;
                } else {
                    i3 = i7;
                }
            }
            if (!z) {
                this.e = i6;
                this.f = i3;
                this.b = i8;
                return true;
            }
            switch (i6) {
                case 1:
                    this.e = 6;
                    return false;
                case 2:
                    bArr2[i8] = (byte) (i3 >> 4);
                    i8++;
                    break;
                case 3:
                    int i13 = i8 + 1;
                    bArr2[i8] = (byte) (i3 >> 10);
                    i8 = i13 + 1;
                    bArr2[i13] = (byte) (i3 >> 2);
                    break;
                case 4:
                    this.e = 6;
                    return false;
            }
            this.e = i6;
            this.b = i8;
            return true;
        }
    }

    public static String b(byte[] bArr, int i) {
        try {
            return new String(c(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] c(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        int i4;
        C0003c cVar = new C0003c(i3, null);
        int i5 = (i2 / 3) * 4;
        if (!cVar.d) {
            switch (i2 % 3) {
                case 1:
                    i5 += 2;
                    break;
                case 2:
                    i5 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i5 += 4;
        }
        if (cVar.e && i2 > 0) {
            int i6 = ((i2 - 1) / 57) + 1;
            if (cVar.f) {
                i4 = 2;
            } else {
                i4 = 1;
            }
            i5 += i4 * i6;
        }
        cVar.a = new byte[i5];
        cVar.a(bArr, i, i2, true);
        if (a || cVar.b == i5) {
            return cVar.a;
        }
        throw new AssertionError();
    }

    /* renamed from: com.google.ads.util.c$c  reason: collision with other inner class name */
    public static class C0003c extends a {
        static final /* synthetic */ boolean g;
        private static final byte[] h = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        public int c;
        public final boolean d;
        public final boolean e;
        public final boolean f;
        private final byte[] j;
        private int k;
        private final byte[] l;

        static {
            boolean z;
            if (!c.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            g = z;
        }

        public C0003c(int i2, byte[] bArr) {
            boolean z;
            boolean z2 = true;
            this.a = bArr;
            this.d = (i2 & 1) == 0;
            if ((i2 & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.e = z;
            this.f = (i2 & 4) == 0 ? false : z2;
            this.l = (i2 & 8) == 0 ? h : i;
            this.j = new byte[2];
            this.c = 0;
            this.k = this.e ? 19 : -1;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public boolean a(byte[] bArr, int i2, int i3, boolean z) {
            int i4;
            int i5;
            int i6;
            int i7;
            byte b;
            int i8;
            byte b2;
            int i9;
            byte b3;
            int i10;
            int i11;
            int i12;
            int i13;
            byte[] bArr2 = this.l;
            byte[] bArr3 = this.a;
            int i14 = 0;
            int i15 = this.k;
            int i16 = i3 + i2;
            byte b4 = -1;
            switch (this.c) {
                case 0:
                    i4 = i2;
                    break;
                case 1:
                    if (i2 + 2 <= i16) {
                        int i17 = i2 + 1;
                        b4 = ((this.j[0] & 255) << 16) | ((bArr[i2] & 255) << 8) | (bArr[i17] & 255);
                        this.c = 0;
                        i4 = i17 + 1;
                        break;
                    }
                    i4 = i2;
                    break;
                case 2:
                    if (i2 + 1 <= i16) {
                        i4 = i2 + 1;
                        b4 = ((this.j[0] & 255) << 16) | ((this.j[1] & 255) << 8) | (bArr[i2] & 255);
                        this.c = 0;
                        break;
                    }
                    i4 = i2;
                    break;
                default:
                    i4 = i2;
                    break;
            }
            if (b4 != -1) {
                bArr3[0] = bArr2[(b4 >> 18) & 63];
                bArr3[1] = bArr2[(b4 >> 12) & 63];
                bArr3[2] = bArr2[(b4 >> 6) & 63];
                i14 = 4;
                bArr3[3] = bArr2[b4 & 63];
                i15--;
                if (i15 == 0) {
                    if (this.f) {
                        i13 = 5;
                        bArr3[4] = 13;
                    } else {
                        i13 = 4;
                    }
                    i14 = i13 + 1;
                    bArr3[i13] = 10;
                    i15 = 19;
                }
            }
            while (true) {
                int i18 = i6;
                int i19 = i5;
                if (i4 + 3 <= i16) {
                    byte b5 = ((bArr[i4] & 255) << 16) | ((bArr[i4 + 1] & 255) << 8) | (bArr[i4 + 2] & 255);
                    bArr3[i19] = bArr2[(b5 >> 18) & 63];
                    bArr3[i19 + 1] = bArr2[(b5 >> 12) & 63];
                    bArr3[i19 + 2] = bArr2[(b5 >> 6) & 63];
                    bArr3[i19 + 3] = bArr2[b5 & 63];
                    i4 += 3;
                    i5 = i19 + 4;
                    i6 = i18 - 1;
                    if (i6 == 0) {
                        if (this.f) {
                            i12 = i5 + 1;
                            bArr3[i5] = 13;
                        } else {
                            i12 = i5;
                        }
                        i5 = i12 + 1;
                        bArr3[i12] = 10;
                        i6 = 19;
                    }
                } else {
                    if (z) {
                        if (i4 - this.c == i16 - 1) {
                            if (this.c > 0) {
                                i11 = 1;
                                b3 = this.j[0];
                                i10 = i4;
                            } else {
                                b3 = bArr[i4];
                                i10 = i4 + 1;
                                i11 = 0;
                            }
                            int i20 = (b3 & 255) << 4;
                            this.c -= i11;
                            int i21 = i19 + 1;
                            bArr3[i19] = bArr2[(i20 >> 6) & 63];
                            int i22 = i21 + 1;
                            bArr3[i21] = bArr2[i20 & 63];
                            if (this.d) {
                                int i23 = i22 + 1;
                                bArr3[i22] = 61;
                                i22 = i23 + 1;
                                bArr3[i23] = 61;
                            }
                            if (this.e) {
                                if (this.f) {
                                    bArr3[i22] = 13;
                                    i22++;
                                }
                                bArr3[i22] = 10;
                                i22++;
                            }
                            i4 = i10;
                            i19 = i22;
                        } else if (i4 - this.c == i16 - 2) {
                            if (this.c > 1) {
                                i8 = 1;
                                b = this.j[0];
                            } else {
                                b = bArr[i4];
                                i4++;
                                i8 = 0;
                            }
                            int i24 = (b & 255) << 10;
                            if (this.c > 0) {
                                b2 = this.j[i8];
                                i8++;
                            } else {
                                b2 = bArr[i4];
                                i4++;
                            }
                            int i25 = ((b2 & 255) << 2) | i24;
                            this.c -= i8;
                            int i26 = i19 + 1;
                            bArr3[i19] = bArr2[(i25 >> 12) & 63];
                            int i27 = i26 + 1;
                            bArr3[i26] = bArr2[(i25 >> 6) & 63];
                            int i28 = i27 + 1;
                            bArr3[i27] = bArr2[i25 & 63];
                            if (this.d) {
                                i9 = i28 + 1;
                                bArr3[i28] = 61;
                            } else {
                                i9 = i28;
                            }
                            if (this.e) {
                                if (this.f) {
                                    bArr3[i9] = 13;
                                    i9++;
                                }
                                bArr3[i9] = 10;
                                i9++;
                            }
                            i19 = i9;
                        } else if (this.e && i19 > 0 && i18 != 19) {
                            if (this.f) {
                                i7 = i19 + 1;
                                bArr3[i19] = 13;
                            } else {
                                i7 = i19;
                            }
                            i19 = i7 + 1;
                            bArr3[i7] = 10;
                        }
                        if (!g && this.c != 0) {
                            throw new AssertionError();
                        } else if (!g && i4 != i16) {
                            throw new AssertionError();
                        }
                    } else if (i4 == i16 - 1) {
                        byte[] bArr4 = this.j;
                        int i29 = this.c;
                        this.c = i29 + 1;
                        bArr4[i29] = bArr[i4];
                    } else if (i4 == i16 - 2) {
                        byte[] bArr5 = this.j;
                        int i30 = this.c;
                        this.c = i30 + 1;
                        bArr5[i30] = bArr[i4];
                        byte[] bArr6 = this.j;
                        int i31 = this.c;
                        this.c = i31 + 1;
                        bArr6[i31] = bArr[i4 + 1];
                    }
                    this.b = i19;
                    this.k = i18;
                    return true;
                }
            }
        }
    }

    private c() {
    }
}
