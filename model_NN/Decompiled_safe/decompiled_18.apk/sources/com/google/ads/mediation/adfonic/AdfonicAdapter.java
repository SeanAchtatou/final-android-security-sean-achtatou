package com.google.ads.mediation.adfonic;

import android.app.Activity;
import android.view.View;
import android.widget.FrameLayout;
import com.adfonic.android.AdfonicView;
import com.adfonic.android.api.Request;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.google.ads.mediation.adfonic.listener.MediationBannerAdListener;
import com.google.ads.mediation.adfonic.listener.MediationInterstitialAdListener;
import com.google.ads.mediation.adfonic.util.LayoutFactory;
import com.google.ads.mediation.adfonic.util.RequestHelper;

public class AdfonicAdapter implements MediationBannerAdapter<AdfonicNetworkExtras, AdfonicServerParameters>, MediationInterstitialAdapter<AdfonicNetworkExtras, AdfonicServerParameters> {
    private FrameLayout adContainer;
    private AdfonicView adView;
    private MediationBannerListener bannerListener;
    private MediationInterstitialListener interstitialListener;
    private LayoutFactory layoutFactory = new LayoutFactory();

    public Class<AdfonicNetworkExtras> getAdditionalParametersType() {
        return AdfonicNetworkExtras.class;
    }

    public Class<AdfonicServerParameters> getServerParametersType() {
        return AdfonicServerParameters.class;
    }

    public final void requestBannerAd(MediationBannerListener listener, Activity activity, AdfonicServerParameters serverParameters, AdSize adSize, MediationAdRequest mediationAdRequest, AdfonicNetworkExtras extras) {
        this.bannerListener = listener;
        this.adContainer = new FrameLayout(activity);
        FrameLayout.LayoutParams lf = this.layoutFactory.buildLayoutForAd(activity, adSize);
        this.adContainer.setLayoutParams(lf);
        this.adView = new AdfonicView(activity);
        this.adView.setAdListener(new MediationBannerAdListener(this.bannerListener, this));
        this.adContainer.addView(this.adView, lf);
        this.adView.loadAd(prepareRequest(mediationAdRequest, serverParameters, extras));
    }

    public void requestInterstitialAd(MediationInterstitialListener listener, Activity activity, AdfonicServerParameters serverParameters, MediationAdRequest mediationAdRequest, AdfonicNetworkExtras extras) {
        this.interstitialListener = listener;
        this.adView = new AdfonicView(activity);
        this.adView.setAdListener(new MediationInterstitialAdListener(this.interstitialListener, this));
        this.adView.loadAd(prepareRequest(mediationAdRequest, serverParameters, extras));
    }

    public View getBannerView() {
        return this.adContainer;
    }

    public void showInterstitial() {
        this.adView.showInterstitial();
    }

    public void destroy() {
        this.bannerListener = null;
        this.interstitialListener = null;
        if (this.adContainer != null) {
            this.adContainer.removeAllViews();
            this.adContainer = null;
        }
    }

    private Request prepareRequest(MediationAdRequest mediationAdRequest, AdfonicServerParameters serverParameters, AdfonicNetworkExtras extras) {
        return new RequestHelper().createRequest(serverParameters.adSlotID, mediationAdRequest);
    }
}
