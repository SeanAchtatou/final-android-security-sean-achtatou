package com.google.ads.mediation.jumptap;

import com.google.ads.mediation.MediationServerParameters;

public class JumpTapAdapterServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "pubid")
    public String publisherId;
    @MediationServerParameters.Parameter(name = "siteId", required = false)
    public String siteId;
    @MediationServerParameters.Parameter(name = "adSpotId", required = false)
    public String spotId;
}
