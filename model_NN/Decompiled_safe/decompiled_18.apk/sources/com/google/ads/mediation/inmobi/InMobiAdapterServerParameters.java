package com.google.ads.mediation.inmobi;

import com.google.ads.mediation.MediationServerParameters;

public class InMobiAdapterServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "pubid")
    public String appId;
    @MediationServerParameters.Parameter(name = "isUDIDHashAllowed", required = false)
    public String isUDIDHashAllowed = "true";
}
