package com.google.ads.mediation.adfonic.listener;

import com.adfonic.android.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.adfonic.AdfonicAdapter;

public class MediationBannerAdListener implements AdListener {
    private AdfonicAdapter adapter;
    private MediationBannerListener listener;

    public MediationBannerAdListener(MediationBannerListener listener2, AdfonicAdapter adapter2) {
        this.listener = listener2;
        this.adapter = adapter2;
    }

    public void onReceivedAd() {
        this.listener.onReceivedAd(this.adapter);
    }

    public void onPresentScreen() {
        this.listener.onPresentScreen(this.adapter);
    }

    public void onLeaveApplication() {
        this.listener.onLeaveApplication(this.adapter);
    }

    public void onDismissScreen() {
        this.listener.onDismissScreen(this.adapter);
    }

    public void onClick() {
        this.listener.onClick(this.adapter);
    }

    public void onInvalidRequest() {
        this.listener.onFailedToReceiveAd(this.adapter, AdRequest.ErrorCode.INVALID_REQUEST);
    }

    public void onNetworkError() {
        this.listener.onFailedToReceiveAd(this.adapter, AdRequest.ErrorCode.NETWORK_ERROR);
    }

    public void onNoFill() {
        this.listener.onFailedToReceiveAd(this.adapter, AdRequest.ErrorCode.NO_FILL);
    }

    public void onInternalError() {
        this.listener.onFailedToReceiveAd(this.adapter, AdRequest.ErrorCode.INTERNAL_ERROR);
    }
}
