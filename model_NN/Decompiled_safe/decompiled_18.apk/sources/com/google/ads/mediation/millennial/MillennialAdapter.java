package com.google.ads.mediation.millennial;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.jumptap.adtag.media.VideoCacheItem;
import com.millennialmedia.android.MMAdView;
import com.millennialmedia.android.MMAdViewSDK;
import java.util.Hashtable;

public final class MillennialAdapter implements MediationBannerAdapter<MillennialAdapterExtras, MillennialAdapterServerParameters>, MediationInterstitialAdapter<MillennialAdapterExtras, MillennialAdapterServerParameters> {
    private MMAdView adView;
    /* access modifiers changed from: private */
    public MediationBannerListener bannerListener;
    /* access modifiers changed from: private */
    public MediationInterstitialListener interstitialListener;
    private FrameLayout wrappedAdView;

    private void populateAdViewParameters(MMAdView view, Hashtable<String, String> metaMap, MediationAdRequest mediationAdRequest, MillennialAdapterExtras extras) {
        if (extras == null) {
            extras = new MillennialAdapterExtras();
        }
        if (mediationAdRequest.getKeywords() != null) {
            metaMap.put("keywords", TextUtils.join(VideoCacheItem.URL_DELIMITER, mediationAdRequest.getKeywords()));
        }
        if (extras.getChildren() != null) {
            metaMap.put("children", extras.getChildren().booleanValue() ? "true" : "false");
        }
        this.adView.setId(MMAdViewSDK.DEFAULT_VIEWID);
        if (extras.getAdLocation() != null) {
            switch (extras.getAdLocation()) {
                case BOTTOM:
                    this.adView.setAdType(MMAdView.BANNER_AD_BOTTOM);
                    break;
                case TOP:
                    this.adView.setAdType(MMAdView.BANNER_AD_TOP);
                    break;
            }
        }
        if (extras.getInterstitialTime() != null) {
            switch (extras.getInterstitialTime()) {
                case APP_LAUNCH:
                    this.adView.setAdType(MMAdView.FULLSCREEN_AD_LAUNCH);
                    break;
                case TRANSITION:
                    this.adView.setAdType(MMAdView.FULLSCREEN_AD_TRANSITION);
                    break;
            }
        }
        if (mediationAdRequest.getAgeInYears() != null) {
            this.adView.setAge(mediationAdRequest.getAgeInYears().toString());
        }
        if (mediationAdRequest.getGender() != null) {
            switch (mediationAdRequest.getGender()) {
                case MALE:
                    this.adView.setGender(MMDemographic.GENDER_MALE);
                    break;
                case FEMALE:
                    this.adView.setGender(MMDemographic.GENDER_FEMALE);
                    break;
            }
        }
        if (extras.getIncomeInUsDollars() != null) {
            this.adView.setIncome(extras.getIncomeInUsDollars().toString());
        }
        if (mediationAdRequest.getLocation() != null) {
            this.adView.updateUserLocation(mediationAdRequest.getLocation());
        }
        if (extras.getPostalCode() != null) {
            this.adView.setZip(extras.getPostalCode());
        }
        if (extras.getMaritalStatus() != null) {
            this.adView.setMarital(extras.getMaritalStatus().getDescription());
        }
        if (extras.getEthnicity() != null) {
            this.adView.setEthnicity(extras.getEthnicity().getDescription());
        }
        if (extras.getOrientation() != null) {
            this.adView.setOrientation(extras.getOrientation().getDescription());
        }
        if (extras.getPolitics() != null) {
            this.adView.setPolitics(extras.getPolitics().getDescription());
        }
        if (extras.getEducation() != null) {
            this.adView.setEducation(extras.getEducation().getDescription());
        }
    }

    private static int dip(int pixels, Context context) {
        return (int) TypedValue.applyDimension(1, (float) pixels, context.getResources().getDisplayMetrics());
    }

    public Class<MillennialAdapterExtras> getAdditionalParametersType() {
        return MillennialAdapterExtras.class;
    }

    public Class<MillennialAdapterServerParameters> getServerParametersType() {
        return MillennialAdapterServerParameters.class;
    }

    public void requestBannerAd(MediationBannerListener listener, Activity activity, MillennialAdapterServerParameters serverParameters, AdSize adSize, MediationAdRequest mediationAdRequest, MillennialAdapterExtras extras) {
        FrameLayout.LayoutParams wrappedLayoutParams;
        this.bannerListener = listener;
        Hashtable<String, String> metaMap = new Hashtable<>();
        if (adSize.isSizeAppropriate(320, 53)) {
            metaMap.put("width", "320");
            metaMap.put("height", "53");
            wrappedLayoutParams = new FrameLayout.LayoutParams(dip(320, activity), dip(53, activity));
        } else {
            metaMap.put("width", Integer.toString(adSize.getWidth()));
            metaMap.put("height", Integer.toString(adSize.getHeight()));
            wrappedLayoutParams = new FrameLayout.LayoutParams(dip(adSize.getWidth(), activity), dip(adSize.getHeight(), activity));
        }
        this.adView = new MMAdView(activity, serverParameters.apid, MMAdView.BANNER_AD_TOP, -1, metaMap, mediationAdRequest.isTesting());
        populateAdViewParameters(this.adView, metaMap, mediationAdRequest, extras);
        this.adView.setListener(new BannerListener());
        this.wrappedAdView = new FrameLayout(activity);
        this.wrappedAdView.setLayoutParams(wrappedLayoutParams);
        this.wrappedAdView.addView(this.adView);
        this.adView.callForAd();
    }

    public void requestInterstitialAd(MediationInterstitialListener listener, Activity activity, MillennialAdapterServerParameters serverParameters, MediationAdRequest mediationAdRequest, MillennialAdapterExtras extras) {
        this.interstitialListener = listener;
        Hashtable<String, String> metaMap = new Hashtable<>();
        this.adView = new MMAdView(activity, serverParameters.apid, MMAdView.FULLSCREEN_AD_TRANSITION, -1, metaMap, mediationAdRequest.isTesting());
        populateAdViewParameters(this.adView, metaMap, mediationAdRequest, extras);
        this.adView.setListener(new InterstitialListener());
        this.adView.fetch();
    }

    public void showInterstitial() {
        this.adView.display();
    }

    public void destroy() {
    }

    public View getBannerView() {
        return this.wrappedAdView;
    }

    private class BannerListener implements MMAdView.MMAdListener {
        private BannerListener() {
        }

        public void MMAdClickedToOverlay(MMAdView arg0) {
            MillennialAdapter.this.bannerListener.onClick(MillennialAdapter.this);
            MillennialAdapter.this.bannerListener.onPresentScreen(MillennialAdapter.this);
        }

        public void MMAdFailed(MMAdView arg0) {
            MillennialAdapter.this.bannerListener.onFailedToReceiveAd(MillennialAdapter.this, AdRequest.ErrorCode.NO_FILL);
        }

        public void MMAdOverlayLaunched(MMAdView arg0) {
            MillennialAdapter.this.bannerListener.onPresentScreen(MillennialAdapter.this);
        }

        public void MMAdRequestIsCaching(MMAdView arg0) {
        }

        public void MMAdCachingCompleted(MMAdView arg0, boolean arg1) {
        }

        public void MMAdReturned(MMAdView arg0) {
            MillennialAdapter.this.bannerListener.onReceivedAd(MillennialAdapter.this);
        }
    }

    private class InterstitialListener implements MMAdView.MMAdListener {
        private InterstitialListener() {
        }

        public void MMAdClickedToOverlay(MMAdView arg0) {
            MillennialAdapter.this.interstitialListener.onPresentScreen(MillennialAdapter.this);
        }

        public void MMAdFailed(MMAdView arg0) {
            MillennialAdapter.this.interstitialListener.onFailedToReceiveAd(MillennialAdapter.this, AdRequest.ErrorCode.NO_FILL);
        }

        public void MMAdOverlayLaunched(MMAdView arg0) {
            MillennialAdapter.this.interstitialListener.onPresentScreen(MillennialAdapter.this);
        }

        public void MMAdRequestIsCaching(MMAdView arg0) {
        }

        public void MMAdCachingCompleted(MMAdView arg0, boolean success) {
            if (!success) {
                MillennialAdapter.this.interstitialListener.onFailedToReceiveAd(MillennialAdapter.this, AdRequest.ErrorCode.NETWORK_ERROR);
            } else {
                MillennialAdapter.this.interstitialListener.onReceivedAd(MillennialAdapter.this);
            }
        }

        public void MMAdReturned(MMAdView arg0) {
        }
    }
}
