package com.google.ads.mediation.jumptap;

import com.google.ads.mediation.NetworkExtras;
import com.jumptap.adtag.JtAdWidgetSettings;

public final class JumpTapAdapterExtras implements NetworkExtras {
    private AdultContent adultContent = null;
    private String applicationId = null;
    private String applicationVersion = null;
    private String country = null;
    private String dismissButtonLabel = null;
    private Income income = null;
    private String language = null;
    private String postalCode = null;
    private boolean shouldSendLocation = false;

    public enum AdultContent {
        NOT_ALLOWED(JtAdWidgetSettings.AdultContent.NOT_ALLOWED),
        ALLOWED(JtAdWidgetSettings.AdultContent.ALLOWED),
        ONLY(JtAdWidgetSettings.AdultContent.ONLY);
        
        private final String description;

        private AdultContent(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public JumpTapAdapterExtras setAdultContent(AdultContent adultContent2) {
        this.adultContent = adultContent2;
        return this;
    }

    public JumpTapAdapterExtras clearAdultContent() {
        return setAdultContent(null);
    }

    public AdultContent getAdultContent() {
        return this.adultContent;
    }

    public JumpTapAdapterExtras setApplicationId(String applicationId2) {
        this.applicationId = applicationId2;
        return this;
    }

    public JumpTapAdapterExtras clearApplicationId() {
        return setApplicationId(null);
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public JumpTapAdapterExtras setApplicationVersion(String applicationVersion2) {
        this.applicationVersion = applicationVersion2;
        return this;
    }

    public JumpTapAdapterExtras clearApplicationVersion() {
        return setApplicationVersion(null);
    }

    public String getApplicationVersion() {
        return this.applicationVersion;
    }

    public JumpTapAdapterExtras setCountry(String country2) {
        this.country = country2;
        return this;
    }

    public JumpTapAdapterExtras clearCountry() {
        return setCountry(null);
    }

    public String getCountry() {
        return this.country;
    }

    public JumpTapAdapterExtras setDismissButtonLabel(String dismissButtonLabel2) {
        this.dismissButtonLabel = dismissButtonLabel2;
        return this;
    }

    public JumpTapAdapterExtras clearDismissButtonLabel() {
        return setDismissButtonLabel(null);
    }

    public String getDismissButtonLabel() {
        return this.dismissButtonLabel;
    }

    public enum Income {
        USD_0_TO_15000("000_015"),
        USD_15000_TO_20000("015_020"),
        USD_20000_TO_30000("020_030"),
        USD_30000_TO_40000("030_040"),
        USD_40000_TO_50000("040_050"),
        USD_50000_TO_75000("050_075"),
        USD_75000_TO_100000("075_100"),
        USD_100000_TO_125000("100_125"),
        USD_125000_TO_150000("125_105"),
        USD_150000_AND_UP("150_OVER");
        
        private final String description;

        private Income(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public JumpTapAdapterExtras setIncome(Income income2) {
        this.income = income2;
        return this;
    }

    public JumpTapAdapterExtras clearIncome() {
        return setIncome(null);
    }

    public Income getIncome() {
        return this.income;
    }

    public JumpTapAdapterExtras setLanguage(String language2) {
        this.language = language2;
        return this;
    }

    public JumpTapAdapterExtras clearLanguage() {
        return setLanguage(null);
    }

    public String getLanguage() {
        return this.language;
    }

    public JumpTapAdapterExtras setPostalCode(String postalCode2) {
        this.postalCode = postalCode2;
        return this;
    }

    public JumpTapAdapterExtras clearPostalCode() {
        return setPostalCode(null);
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public JumpTapAdapterExtras setShouldSendLocation(boolean shouldSendLocation2) {
        this.shouldSendLocation = shouldSendLocation2;
        return this;
    }

    public boolean getShouldSendLocation() {
        return this.shouldSendLocation;
    }
}
