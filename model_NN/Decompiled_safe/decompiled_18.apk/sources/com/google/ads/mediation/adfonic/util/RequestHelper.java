package com.google.ads.mediation.adfonic.util;

import com.adfonic.android.api.Request;
import com.google.ads.AdRequest;
import com.google.ads.mediation.MediationAdRequest;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestHelper {
    public Request createRequest(String slotId, MediationAdRequest mediationRequest) {
        Request r = new Request.RequestBuilder().withSlotId(slotId).withRefreshAd(false).build();
        if (mediationRequest != null) {
            setAge(r, mediationRequest);
            setBirthday(r, mediationRequest);
            setLocation(r, mediationRequest);
            setGender(r, mediationRequest);
            setIsTesting(r, mediationRequest);
        }
        return r;
    }

    private void setIsTesting(Request request, MediationAdRequest mediationAdRequest) {
        request.setTest(mediationAdRequest.isTesting());
    }

    private void setGender(Request request, MediationAdRequest mediationAdRequest) {
        if (AdRequest.Gender.MALE == mediationAdRequest.getGender()) {
            request.setMale(true);
        } else {
            request.setMale(false);
        }
    }

    private void setLocation(Request request, MediationAdRequest mediationAdRequest) {
        if (mediationAdRequest.getLocation() != null) {
            request.setLocation(mediationAdRequest.getLocation());
        }
    }

    private void setBirthday(Request request, MediationAdRequest mediationAdRequest) {
        Date date = mediationAdRequest.getBirthday();
        if (date != null) {
            request.setDateOfBirth(new SimpleDateFormat("yyyyMMdd").format(date));
        }
    }

    private void setAge(Request request, MediationAdRequest mediationAdRequest) {
        try {
            int age = mediationAdRequest.getAgeInYears().intValue();
            if (age > 0) {
                request.setAge(age);
            }
        } catch (Exception e) {
        }
    }
}
