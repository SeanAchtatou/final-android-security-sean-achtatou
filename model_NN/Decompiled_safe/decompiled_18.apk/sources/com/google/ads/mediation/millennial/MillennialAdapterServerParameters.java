package com.google.ads.mediation.millennial;

import com.google.ads.mediation.MediationServerParameters;

public class MillennialAdapterServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "pubid")
    public String apid;
}
