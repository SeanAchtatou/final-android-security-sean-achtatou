package com.google.ads.mediation.millennial;

import com.google.ads.mediation.NetworkExtras;
import org.apache.cordova.NetworkManager;

public final class MillennialAdapterExtras implements NetworkExtras {
    private Boolean children = null;
    private Education education = null;
    private Ethnicity ethnicity = null;
    private Integer income = null;
    private InterstitialTime interstitialTime = InterstitialTime.UNKNOWN;
    private AdLocation location = AdLocation.UNKNOWN;
    private MaritalStatus maritalStatus = null;
    private Orientation orientation = null;
    private Politics politics = null;
    private String postalCode = null;

    public enum AdLocation {
        UNKNOWN,
        BOTTOM,
        TOP
    }

    public enum InterstitialTime {
        UNKNOWN,
        APP_LAUNCH,
        TRANSITION
    }

    public MillennialAdapterExtras setAdLocation(AdLocation location2) {
        this.location = location2;
        return this;
    }

    public MillennialAdapterExtras clearAdLocation() {
        return setAdLocation(null);
    }

    public AdLocation getAdLocation() {
        return this.location;
    }

    public MillennialAdapterExtras setInterstitialTime(InterstitialTime interstitialTime2) {
        this.interstitialTime = interstitialTime2;
        return this;
    }

    public MillennialAdapterExtras clearInterstitialTime() {
        return setInterstitialTime(null);
    }

    public InterstitialTime getInterstitialTime() {
        return this.interstitialTime;
    }

    public MillennialAdapterExtras setIncomeInUsDollars(Integer income2) {
        this.income = income2;
        return this;
    }

    public MillennialAdapterExtras clearIncomeInUsDollars() {
        return setIncomeInUsDollars(null);
    }

    public Integer getIncomeInUsDollars() {
        return this.income;
    }

    public enum MaritalStatus {
        SINGLE(MMDemographic.MARITAL_SINGLE),
        DIVORCED(MMDemographic.MARITAL_DIVORCED),
        ENGAGED(MMDemographic.MARITAL_ENGAGED),
        RELATIONSHIP(MMDemographic.MARITAL_RELATIONSHIP),
        SWINGER("swinger");
        
        private final String description;

        private MaritalStatus(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public MillennialAdapterExtras setMaritalStatus(MaritalStatus maritalStatus2) {
        this.maritalStatus = maritalStatus2;
        return this;
    }

    public MillennialAdapterExtras clearMaritalStatus() {
        return setMaritalStatus(null);
    }

    public MaritalStatus getMaritalStatus() {
        return this.maritalStatus;
    }

    public enum Ethnicity {
        HISPANIC(MMDemographic.ETHNICITY_HISPANIC),
        AFRICAN_AMERICAN("africanamerican"),
        ASIAN(MMDemographic.ETHNICITY_ASIAN),
        INDIAN(MMDemographic.ETHNICITY_INDIAN),
        MIDDLE_EASTERN(MMDemographic.ETHNICITY_MIDDLE_EASTERN),
        NATIVE_AMERICAN(MMDemographic.ETHNICITY_NATIVE_AMERICAN),
        PACIFIC_ISLANDER(MMDemographic.ETHNICITY_PACIFIC_ISLANDER),
        WHITE(MMDemographic.ETHNICITY_WHITE),
        OTHER("other");
        
        private final String description;

        private Ethnicity(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public MillennialAdapterExtras setEthnicity(Ethnicity ethnicity2) {
        this.ethnicity = ethnicity2;
        return this;
    }

    public MillennialAdapterExtras clearEthnicity() {
        return setEthnicity(null);
    }

    public Ethnicity getEthnicity() {
        return this.ethnicity;
    }

    public enum Orientation {
        STRAIGHT(MMDemographic.ORIENTATION_STRAIGHT),
        GAY(MMDemographic.ORIENTATION_GAY),
        BISEXUAL(MMDemographic.ORIENTATION_BISEXUAL),
        NOT_SURE("notsure");
        
        private final String description;

        private Orientation(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public MillennialAdapterExtras setOrientation(Orientation orientation2) {
        this.orientation = orientation2;
        return this;
    }

    public MillennialAdapterExtras clearOrientation() {
        return setOrientation(null);
    }

    public Orientation getOrientation() {
        return this.orientation;
    }

    public enum Politics {
        REPUBLICAN("republican"),
        DEMOCRAT("democrat"),
        CONSERVATIVE("conservative"),
        MODERATE("moderate"),
        LIBERAL("liberal"),
        INDEPENDENT("independent"),
        OTHER("other"),
        UNKNOWN(NetworkManager.TYPE_UNKNOWN);
        
        private final String description;

        private Politics(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public MillennialAdapterExtras setPolitics(Politics politics2) {
        this.politics = politics2;
        return this;
    }

    public MillennialAdapterExtras clearPolitics() {
        return setPolitics(null);
    }

    public Politics getPolitics() {
        return this.politics;
    }

    public enum Education {
        HIGH_SCHOOL(MMDemographic.EDUCATION_HIGH_SCHOOL),
        IN_COLLEGE(MMDemographic.EDUCATION_IN_COLLEGE),
        SOME_COLLEGE(MMDemographic.EDUCATION_SOME_COLLEGE),
        ASSOCIATE(MMDemographic.EDUCATION_ASSOCIATE),
        BACHELORS(MMDemographic.EDUCATION_BACHELORS),
        MASTERS(MMDemographic.EDUCATION_MASTERS),
        PHD(MMDemographic.EDUCATION_PHD),
        PROFESSIONAL(MMDemographic.EDUCATION_PROFESSIONAL);
        
        private final String description;

        private Education(String description2) {
            this.description = description2;
        }

        public String getDescription() {
            return this.description;
        }
    }

    public MillennialAdapterExtras setEducation(Education education2) {
        this.education = education2;
        return this;
    }

    public MillennialAdapterExtras clearEducation() {
        return setEducation(null);
    }

    public Education getEducation() {
        return this.education;
    }

    public MillennialAdapterExtras setChildren(Boolean children2) {
        this.children = children2;
        return this;
    }

    public MillennialAdapterExtras clearChildren() {
        return setChildren(null);
    }

    public Boolean getChildren() {
        return this.children;
    }

    public MillennialAdapterExtras setPostalCode(String postalCode2) {
        this.postalCode = postalCode2;
        return this;
    }

    public MillennialAdapterExtras clearPostalCode() {
        return setPostalCode(null);
    }

    public String getPostalCode() {
        return this.postalCode;
    }
}
