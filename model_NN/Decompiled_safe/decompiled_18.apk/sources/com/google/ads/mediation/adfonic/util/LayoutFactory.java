package com.google.ads.mediation.adfonic.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.widget.FrameLayout;
import com.google.ads.AdSize;
import com.inmobi.androidsdk.impl.IMAdException;

public class LayoutFactory {
    public FrameLayout.LayoutParams buildLayoutForAd(Context context, AdSize adSize) {
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        if (AdSize.BANNER == adSize) {
            return new FrameLayout.LayoutParams(convert(320, dm), convert(48, dm), 17);
        }
        if (AdSize.IAB_BANNER == adSize) {
            return new FrameLayout.LayoutParams(convert(468, dm), convert(60, dm), 17);
        }
        if (AdSize.IAB_LEADERBOARD == adSize) {
            return new FrameLayout.LayoutParams(convert(728, dm), convert(90, dm), 17);
        }
        if (AdSize.IAB_MRECT == adSize) {
            return new FrameLayout.LayoutParams(convert(IMAdException.INVALID_REQUEST, dm), convert(250, dm), 17);
        }
        if (AdSize.IAB_WIDE_SKYSCRAPER == adSize) {
            return new FrameLayout.LayoutParams(convert(120, dm), convert(IMAdException.SANDBOX_UAND, dm), 17);
        }
        return null;
    }

    private int convert(int pixels, DisplayMetrics dm) {
        return (int) TypedValue.applyDimension(1, (float) pixels, dm);
    }

    public FrameLayout.LayoutParams buildLayoutForInterstitial(Activity activity) {
        return new FrameLayout.LayoutParams(-1, -1);
    }
}
