package com.google.ads.mediation.jumptap;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdActivity;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.jumptap.adtag.JtAdInterstitial;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.JtAdViewListener;
import com.jumptap.adtag.JtAdWidgetSettings;
import com.jumptap.adtag.utils.JtException;

public final class JumpTapAdapter implements MediationInterstitialAdapter<JumpTapAdapterExtras, JumpTapAdapterServerParameters>, MediationBannerAdapter<JumpTapAdapterExtras, JumpTapAdapterServerParameters> {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$google$ads$AdRequest$Gender;
    private JtAdView adView;
    /* access modifiers changed from: private */
    public MediationBannerListener bannerListener;
    private JtAdInterstitial interstitial;
    /* access modifiers changed from: private */
    public MediationInterstitialListener interstitialListener;

    static /* synthetic */ int[] $SWITCH_TABLE$com$google$ads$AdRequest$Gender() {
        int[] iArr = $SWITCH_TABLE$com$google$ads$AdRequest$Gender;
        if (iArr == null) {
            iArr = new int[AdRequest.Gender.values().length];
            try {
                iArr[AdRequest.Gender.FEMALE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[AdRequest.Gender.MALE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[AdRequest.Gender.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$google$ads$AdRequest$Gender = iArr;
        }
        return iArr;
    }

    private static class CleanJtAdWidgetSettings extends JtAdWidgetSettings {
        private CleanJtAdWidgetSettings() {
        }

        /* synthetic */ CleanJtAdWidgetSettings(CleanJtAdWidgetSettings cleanJtAdWidgetSettings) {
            this();
        }
    }

    private JtAdWidgetSettings buildSettings(JumpTapAdapterServerParameters serverParameters, MediationAdRequest mediationAdRequest, JumpTapAdapterExtras extras) {
        if (extras == null) {
            extras = new JumpTapAdapterExtras();
        }
        JtAdWidgetSettings settings = new CleanJtAdWidgetSettings(null);
        settings.setRefreshPeriod(0);
        settings.setPublisherId(serverParameters.publisherId);
        settings.setSpotId(serverParameters.spotId);
        settings.setSiteId(serverParameters.siteId);
        settings.setApplicationId("GWhirl Adapter");
        settings.setApplicationVersion("1.2.3");
        if (extras.getAdultContent() != null) {
            settings.setAdultContentType(extras.getAdultContent().getDescription());
        }
        if (mediationAdRequest.getAgeInYears() != null) {
            settings.setAge(mediationAdRequest.getAgeInYears().toString());
        }
        if (extras.getApplicationId() != null) {
            settings.setApplicationId(extras.getApplicationId());
        }
        if (extras.getApplicationVersion() != null) {
            settings.setApplicationVersion(extras.getApplicationVersion());
        }
        if (extras.getCountry() != null) {
            settings.setCountry(extras.getCountry());
        }
        if (extras.getDismissButtonLabel() != null) {
            settings.setDismissButtonLabel(extras.getDismissButtonLabel());
        }
        if (mediationAdRequest.getGender() != null) {
            switch ($SWITCH_TABLE$com$google$ads$AdRequest$Gender()[mediationAdRequest.getGender().ordinal()]) {
                case 2:
                    settings.setGender(AdActivity.TYPE_PARAM);
                    break;
                case 3:
                    settings.setGender("f");
                    break;
            }
        }
        if (extras.getIncome() != null) {
            settings.setHHI(extras.getIncome().getDescription());
        }
        if (extras.getLanguage() != null) {
            settings.setLanguage(extras.getLanguage());
        }
        if (extras.getPostalCode() != null) {
            settings.setPostalCode(extras.getPostalCode());
        }
        settings.setShouldSendLocation(extras.getShouldSendLocation());
        return settings;
    }

    public Class<JumpTapAdapterExtras> getAdditionalParametersType() {
        return JumpTapAdapterExtras.class;
    }

    public Class<JumpTapAdapterServerParameters> getServerParametersType() {
        return JumpTapAdapterServerParameters.class;
    }

    public void requestBannerAd(MediationBannerListener listener, Activity activity, JumpTapAdapterServerParameters serverParameters, AdSize adSize, MediationAdRequest mediationAdRequest, JumpTapAdapterExtras extras) {
        this.bannerListener = listener;
        try {
            this.adView = new JtAdView(activity, buildSettings(serverParameters, mediationAdRequest, extras));
            this.adView.setAdViewListener(new BannerListener(this, null));
            this.adView.refreshAd();
        } catch (JtException e) {
            listener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }
    }

    public void requestInterstitialAd(MediationInterstitialListener listener, Activity activity, JumpTapAdapterServerParameters serverParameters, MediationAdRequest mediationAdRequest, JumpTapAdapterExtras extras) {
        this.interstitialListener = listener;
        try {
            this.interstitial = new JtAdInterstitial(activity, buildSettings(serverParameters, mediationAdRequest, extras));
            this.interstitial.setAdViewListener(new InterstitialListener(this, null));
            this.interstitial.refreshAd();
        } catch (JtException e) {
            this.interstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }
    }

    public void showInterstitial() {
        this.interstitial.showAsPopup();
    }

    public void destroy() {
    }

    public View getBannerView() {
        return this.adView;
    }

    private class BannerListener implements JtAdViewListener {
        private BannerListener() {
        }

        /* synthetic */ BannerListener(JumpTapAdapter jumpTapAdapter, BannerListener bannerListener) {
            this();
        }

        public void onAdError(JtAdView arg0, int arg1, int arg2) {
            JumpTapAdapter.this.bannerListener.onFailedToReceiveAd(JumpTapAdapter.this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }

        public void onFocusChange(JtAdView arg0, int arg1, boolean arg2) {
        }

        public void onInterstitialDismissed(JtAdView arg0, int arg1) {
        }

        public void onNewAd(JtAdView arg0, int arg1, String arg2) {
            JumpTapAdapter.this.bannerListener.onReceivedAd(JumpTapAdapter.this);
        }

        public void onNoAdFound(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.bannerListener.onFailedToReceiveAd(JumpTapAdapter.this, AdRequest.ErrorCode.NO_FILL);
        }

        public void onBeginAdInteraction(JtAdView arg0, int arg1) {
        }

        public void onEndAdInteraction(JtAdView arg0, int arg1) {
        }

        public void onHide(JtAdView arg0, int arg1) {
        }

        public void onBannerClicked(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.bannerListener.onClick(JumpTapAdapter.this);
        }

        public void onContract(JtAdView arg0, int arg1) {
        }

        public void onExpand(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.bannerListener.onPresentScreen(JumpTapAdapter.this);
        }

        public void onLaunchActivity(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.bannerListener.onLeaveApplication(JumpTapAdapter.this);
        }

        public void onReturnFromActivity(JtAdView arg0, int arg1) {
        }
    }

    private class InterstitialListener implements JtAdViewListener {
        private InterstitialListener() {
        }

        /* synthetic */ InterstitialListener(JumpTapAdapter jumpTapAdapter, InterstitialListener interstitialListener) {
            this();
        }

        public void onAdError(JtAdView arg0, int arg1, int arg2) {
            JumpTapAdapter.this.interstitialListener.onFailedToReceiveAd(JumpTapAdapter.this, AdRequest.ErrorCode.INTERNAL_ERROR);
        }

        public void onFocusChange(JtAdView arg0, int arg1, boolean arg2) {
        }

        public void onInterstitialDismissed(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.interstitialListener.onDismissScreen(JumpTapAdapter.this);
        }

        public void onNewAd(JtAdView arg0, int arg1, String arg2) {
            JumpTapAdapter.this.interstitialListener.onReceivedAd(JumpTapAdapter.this);
        }

        public void onNoAdFound(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.interstitialListener.onFailedToReceiveAd(JumpTapAdapter.this, AdRequest.ErrorCode.NO_FILL);
        }

        public void onBeginAdInteraction(JtAdView arg0, int arg1) {
        }

        public void onEndAdInteraction(JtAdView arg0, int arg1) {
        }

        public void onHide(JtAdView arg0, int arg1) {
        }

        public void onBannerClicked(JtAdView arg0, int arg1) {
        }

        public void onContract(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.interstitialListener.onDismissScreen(JumpTapAdapter.this);
        }

        public void onExpand(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.interstitialListener.onPresentScreen(JumpTapAdapter.this);
        }

        public void onLaunchActivity(JtAdView arg0, int arg1) {
            JumpTapAdapter.this.interstitialListener.onLeaveApplication(JumpTapAdapter.this);
        }

        public void onReturnFromActivity(JtAdView arg0, int arg1) {
        }
    }
}
