package com.google.ads.mediation.adfonic;

import com.google.ads.mediation.MediationServerParameters;

public final class AdfonicServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "pubid")
    public String adSlotID;
}
