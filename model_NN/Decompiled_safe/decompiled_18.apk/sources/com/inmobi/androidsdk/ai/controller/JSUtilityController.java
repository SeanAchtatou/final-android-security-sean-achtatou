package com.inmobi.androidsdk.ai.controller;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.JSController;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.internal.IMLog;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class JSUtilityController extends JSController {
    private JSAssetController a;
    private JSDisplayController b;

    public JSUtilityController(IMWebView iMWebView, Context context) {
        super(iMWebView, context);
        this.a = new JSAssetController(iMWebView, context);
        this.b = new JSDisplayController(iMWebView, context);
        iMWebView.addJavascriptInterface(this.b, "displayController");
    }

    public void sendSMS(String str, String str2) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> sendSMS: recipient: " + str + " body: " + str2);
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra("address", str);
            intent.putExtra("sms_body", str2);
            intent.setType("vnd.android-dir/mms-sms");
            intent.addFlags(268435456);
            this.imWebView.getExpandedActivity().startActivity(intent);
            this.imWebView.fireOnLeaveApplication();
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception in sending SMS ", e);
            this.imWebView.raiseError("Exception in sending SMS", "sendSMS");
        }
    }

    public void sendMail(String str, String str2, String str3) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> sendMail: recipient: " + str + " subject: " + str2 + " body: " + str3);
        try {
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("plain/text");
            intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
            intent.putExtra("android.intent.extra.SUBJECT", str2);
            intent.putExtra("android.intent.extra.TEXT", str3);
            intent.addFlags(268435456);
            this.imWebView.getExpandedActivity().startActivity(Intent.createChooser(intent, "Choose the Email Client."));
            this.imWebView.fireOnLeaveApplication();
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception in sending mail ", e);
            this.imWebView.raiseError("Exception in sending mail", "sendMail");
        }
    }

    private String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.startsWith("tel:")) {
            return str;
        }
        return "tel:" + str;
    }

    public void makeCall(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> makeCall: number: " + str);
        try {
            if (this.mContext.checkCallingOrSelfPermission("android.permission.CALL_PHONE") == -1) {
                IMLog.debug(IMConstants.LOGGING_TAG, "No permission to make call");
                this.imWebView.raiseError("No Permisson to make call", "makeCall");
                return;
            }
            String a2 = a(str);
            if (a2 == null) {
                this.imWebView.raiseError("Bad Phone Number", "makeCall");
                return;
            }
            Intent intent = new Intent("android.intent.action.CALL", Uri.parse(a2.toString()));
            intent.addFlags(268435456);
            this.imWebView.getExpandedActivity().startActivity(intent);
            this.imWebView.fireOnLeaveApplication();
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception in making call ", e);
            this.imWebView.raiseError("Exception in making call", "makeCall");
        }
    }

    public void createEvent(String str, String str2, String str3) {
        Cursor query;
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> createEvent: date: " + str + " title: " + str2 + " body: " + str3);
        ContentResolver contentResolver = this.mContext.getContentResolver();
        String[] strArr = {"_id", "displayName", "_sync_account"};
        if (Build.VERSION.SDK_INT == 8) {
            query = contentResolver.query(Uri.parse("content://com.android.calendar/calendars"), strArr, null, null, null);
        } else {
            query = contentResolver.query(Uri.parse("content://calendar/calendars"), strArr, null, null, null);
        }
        if (query == null || (query != null && !query.moveToFirst())) {
            Toast.makeText(this.mContext, "No calendar account found", 1).show();
            if (query != null) {
                query.close();
                return;
            }
            return;
        }
        if (query.getCount() == 1) {
            a(query.getInt(0), str, str2, str3);
        } else {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < query.getCount(); i++) {
                HashMap hashMap = new HashMap();
                hashMap.put("ID", query.getString(0));
                hashMap.put("NAME", query.getString(1));
                hashMap.put("EMAILID", query.getString(2));
                arrayList.add(hashMap);
                query.moveToNext();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
            builder.setTitle("Choose Calendar to save event to");
            final ArrayList arrayList2 = arrayList;
            final String str4 = str;
            final String str5 = str2;
            final String str6 = str3;
            builder.setSingleChoiceItems(new SimpleAdapter(this.mContext, arrayList, 17367053, new String[]{"NAME", "EMAILID"}, new int[]{16908308, 16908309}), -1, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    JSUtilityController.this.a(Integer.parseInt((String) ((Map) arrayList2.get(i)).get("ID")), str4, str5, str6);
                    dialogInterface.cancel();
                }
            });
            builder.create().show();
        }
        query.close();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void a(int i, String str, String str2, String str3) {
        Uri insert;
        ContentResolver contentResolver = this.mContext.getContentResolver();
        long parseLong = Long.parseLong(str);
        ContentValues contentValues = new ContentValues();
        contentValues.put("calendar_id", Integer.valueOf(i));
        contentValues.put("title", str2);
        contentValues.put("description", str3);
        contentValues.put("dtstart", Long.valueOf(parseLong));
        contentValues.put("hasAlarm", (Integer) 1);
        contentValues.put("dtend", Long.valueOf(3600000 + parseLong));
        if (Build.VERSION.SDK_INT == 8) {
            insert = contentResolver.insert(Uri.parse("content://com.android.calendar/events"), contentValues);
        } else {
            insert = contentResolver.insert(Uri.parse("content://calendar/events"), contentValues);
        }
        if (insert != null) {
            long parseLong2 = Long.parseLong(insert.getLastPathSegment());
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("event_id", Long.valueOf(parseLong2));
            contentValues2.put("method", (Integer) 1);
            contentValues2.put("minutes", (Integer) 15);
            if (Build.VERSION.SDK_INT == 8) {
                contentResolver.insert(Uri.parse("content://com.android.calendar/reminders"), contentValues2);
            } else {
                contentResolver.insert(Uri.parse("content://calendar/reminders"), contentValues2);
            }
        }
        Toast.makeText(this.mContext, "Event added to calendar", 0).show();
    }

    public String copyTextFromJarIntoAssetDir(String str, String str2) {
        return this.a.copyTextFromJarIntoAssetDir(str, str2);
    }

    public String writeToDiskWrap(InputStream inputStream, String str, boolean z, String str2, String str3, String str4) throws IllegalStateException, IOException {
        return this.a.writeToDiskWrap(inputStream, str, z, str2, str3, str4);
    }

    public void activate(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> activate: " + str);
    }

    public void deactivate(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> deactivate: " + str);
    }

    public void deleteOldAds() {
        this.a.deleteOldAds();
    }

    public void stopAllListeners() {
        try {
            this.a.stopAllListeners();
            this.b.stopAllListeners();
        } catch (Exception e) {
        }
    }

    public void showAlert(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, str);
    }

    public void log(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "Ad Log Message: " + str);
    }

    public void asyncPing(String str) {
        try {
            IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> asyncPing: url: " + str);
            if (!URLUtil.isValidUrl(str)) {
                this.imWebView.raiseError("Invalid url", "asyncPing");
            } else {
                b(str);
            }
        } catch (Exception e) {
        }
    }

    private void b(final String str) {
        new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0071  */
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0078  */
            /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    r1 = 0
                    java.lang.String r0 = r2     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r2 = "%25"
                    java.lang.String r3 = "%"
                    java.lang.String r0 = r0.replaceAll(r2, r3)     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r2 = "InMobiAndroidSDK_3.6.0"
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0067 }
                    r3.<init>()     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r4 = "Pinging URL: "
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0067 }
                    java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0067 }
                    com.inmobi.commons.internal.IMLog.debug(r2, r3)     // Catch:{ Exception -> 0x0067 }
                    java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0067 }
                    r2.<init>(r0)     // Catch:{ Exception -> 0x0067 }
                    java.net.URLConnection r0 = r2.openConnection()     // Catch:{ Exception -> 0x0067 }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0067 }
                    r1 = 20000(0x4e20, float:2.8026E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    java.lang.String r1 = "GET"
                    r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    com.inmobi.androidsdk.ai.controller.JSUtilityController r1 = com.inmobi.androidsdk.ai.controller.JSUtilityController.this     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    com.inmobi.androidsdk.ai.container.IMWebView r1 = r1.imWebView     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    java.lang.String r1 = r1.webviewUserAgent     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    if (r1 == 0) goto L_0x0045
                    java.lang.String r2 = "user-agent"
                    r0.setRequestProperty(r2, r1)     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                L_0x0045:
                    java.lang.String r1 = "InMobiAndroidSDK_3.6.0"
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    r2.<init>()     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    java.lang.String r3 = "Async Ping Connection Response Code: "
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    int r3 = r0.getResponseCode()     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    com.inmobi.commons.internal.IMLog.debug(r1, r2)     // Catch:{ Exception -> 0x0081, all -> 0x007c }
                    if (r0 == 0) goto L_0x0066
                    r0.disconnect()
                L_0x0066:
                    return
                L_0x0067:
                    r0 = move-exception
                L_0x0068:
                    java.lang.String r2 = "InMobiAndroidSDK_3.6.0"
                    java.lang.String r3 = "Error doing async Ping. "
                    com.inmobi.commons.internal.IMLog.debug(r2, r3, r0)     // Catch:{ all -> 0x0075 }
                    if (r1 == 0) goto L_0x0066
                    r1.disconnect()
                    goto L_0x0066
                L_0x0075:
                    r0 = move-exception
                L_0x0076:
                    if (r1 == 0) goto L_0x007b
                    r1.disconnect()
                L_0x007b:
                    throw r0
                L_0x007c:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                    goto L_0x0076
                L_0x0081:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                    goto L_0x0068
                */
                throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.ai.controller.JSUtilityController.AnonymousClass2.run():void");
            }
        }.start();
    }

    public void playAudio(String str, boolean z, boolean z2, boolean z3, String str2, String str3, String str4) {
        IMLog.debug(IMConstants.LOGGING_TAG, "playAudio: url: " + str + " autoPlay: " + z + " controls: " + z2 + " loop: " + z3 + " startStyle: " + str2 + " stopStyle: " + str3 + " id:" + str4);
        this.imWebView.playAudio(str, z, z2, z3, str2, str3, str4);
    }

    public void muteAudio(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> muteAudio: ");
        this.imWebView.muteAudio(str);
    }

    public void unMuteAudio(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> unMuteAudio: ");
        this.imWebView.unMuteAudio(str);
    }

    public boolean isAudioMuted(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> isAudioMuted: ");
        return this.imWebView.isAudioMuted(str);
    }

    public void setAudioVolume(String str, int i) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> setAudioVolume: ");
        this.imWebView.setAudioVolume(str, i);
    }

    public int getAudioVolume(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> getAudioVolume: ");
        return this.imWebView.getAudioVolume(str);
    }

    public void seekAudio(String str, int i) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> seekAudio: ");
        this.imWebView.seekAudio(str, i);
    }

    public void playVideo(String str, boolean z, boolean z2, boolean z3, boolean z4, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> playVideo: url: " + str + " audioMuted: " + z + " autoPlay: " + z2 + " controls: " + z3 + " loop: " + z4 + " x: " + str2 + " y: " + str3 + " width: " + str4 + " height: " + str5 + " startStyle: " + str6 + " stopStyle: " + str7 + " id:" + str8);
        JSController.Dimensions dimensions = new JSController.Dimensions();
        dimensions.x = Integer.parseInt(str2);
        dimensions.y = Integer.parseInt(str3);
        dimensions.width = Integer.parseInt(str4);
        dimensions.height = Integer.parseInt(str5);
        this.imWebView.playVideo(str, z, z2, z3, z4, dimensions, str6, str7, str8);
    }

    public void pauseAudio(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> pauseAudio: id :" + str);
        this.imWebView.pauseAudio(str);
    }

    public void pauseVideo(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> pauseVideo: id :" + str);
        this.imWebView.pauseVideo(str);
    }

    public void closeVideo(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> closeVideo: id :" + str);
        this.imWebView.closeVideo(str);
    }

    public void hideVideo(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> hideVideo: id :" + str);
        this.imWebView.hideVideo(str);
    }

    public void showVideo(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> showVideo: id :" + str);
        this.imWebView.showVideo(str);
    }

    public void muteVideo(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> muteVideo: ");
        this.imWebView.muteVideo(str);
    }

    public void unMuteVideo(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> unMuteVideo: ");
        this.imWebView.unMuteVideo(str);
    }

    public void seekVideo(String str, int i) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> seekVideo: ");
        this.imWebView.seekVideo(str, i);
    }

    public boolean isVideoMuted(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> isVideoMuted: ");
        return this.imWebView.isVideoMuted(str);
    }

    public void setVideoVolume(String str, int i) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> setVideoVolume: ");
        this.imWebView.setVideoVolume(str, i);
    }

    public int getVideoVolume(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> getVideoVolume: ");
        return this.imWebView.getVideoVolume(str);
    }

    public void openExternal(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> openExternal: url: " + str);
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(str));
        intent.addFlags(268435456);
        try {
            this.imWebView.getContext().startActivity(intent);
        } catch (Exception e) {
            this.imWebView.raiseError("Request must specify a valid URL", "openExternal");
        }
    }

    public String getScreenSize() {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> getScreenSize");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        int i = (int) (((float) displayMetrics.widthPixels) / displayMetrics.density);
        int i2 = (int) (((float) displayMetrics.heightPixels) / displayMetrics.density);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("width", i);
            jSONObject.put("height", i2);
        } catch (JSONException e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Failed to get screen size");
        }
        return jSONObject.toString();
    }

    public String getCurrentPosition() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        IMLog.debug(IMConstants.LOGGING_TAG, "JSUtilityController-> getCurrentPosition");
        JSONObject jSONObject = new JSONObject();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) this.mContext.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        try {
            if (this.imWebView.isViewable()) {
                int[] iArr = new int[2];
                this.imWebView.getLocationOnScreen(iArr);
                i = iArr[0];
                try {
                    i2 = iArr[1];
                    try {
                        i4 = (int) (((float) this.imWebView.getWidth()) / displayMetrics.density);
                    } catch (Exception e) {
                        i4 = 0;
                        i5 = i2;
                        i3 = i;
                        try {
                            IMLog.debug(IMConstants.LOGGING_TAG, "Failed to get current position");
                            try {
                                jSONObject.put("x", i3);
                                jSONObject.put("y", i5);
                                jSONObject.put("width", i4);
                                jSONObject.put("height", 0);
                            } catch (JSONException e2) {
                            }
                            return jSONObject.toString();
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            i = i3;
                            i2 = i5;
                            i5 = i4;
                            th = th2;
                            try {
                                jSONObject.put("x", i);
                                jSONObject.put("y", i2);
                                jSONObject.put("width", i5);
                                jSONObject.put("height", 0);
                            } catch (JSONException e3) {
                            }
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        jSONObject.put("x", i);
                        jSONObject.put("y", i2);
                        jSONObject.put("width", i5);
                        jSONObject.put("height", 0);
                        throw th;
                    }
                } catch (Exception e4) {
                    i4 = 0;
                    i3 = i;
                    IMLog.debug(IMConstants.LOGGING_TAG, "Failed to get current position");
                    jSONObject.put("x", i3);
                    jSONObject.put("y", i5);
                    jSONObject.put("width", i4);
                    jSONObject.put("height", 0);
                    return jSONObject.toString();
                } catch (Throwable th4) {
                    th = th4;
                    i2 = 0;
                    jSONObject.put("x", i);
                    jSONObject.put("y", i2);
                    jSONObject.put("width", i5);
                    jSONObject.put("height", 0);
                    throw th;
                }
                try {
                    i5 = (int) (((float) this.imWebView.getHeight()) / displayMetrics.density);
                } catch (Exception e5) {
                    i5 = i2;
                    i3 = i;
                    IMLog.debug(IMConstants.LOGGING_TAG, "Failed to get current position");
                    jSONObject.put("x", i3);
                    jSONObject.put("y", i5);
                    jSONObject.put("width", i4);
                    jSONObject.put("height", 0);
                    return jSONObject.toString();
                } catch (Throwable th5) {
                    Throwable th6 = th5;
                    i5 = i4;
                    th = th6;
                    jSONObject.put("x", i);
                    jSONObject.put("y", i2);
                    jSONObject.put("width", i5);
                    jSONObject.put("height", 0);
                    throw th;
                }
            } else {
                i4 = 0;
                i2 = 0;
                i = 0;
            }
            try {
                jSONObject.put("x", i);
                jSONObject.put("y", i2);
                jSONObject.put("width", i4);
                jSONObject.put("height", i5);
            } catch (JSONException e6) {
            }
        } catch (Exception e7) {
            i4 = 0;
            i3 = 0;
            IMLog.debug(IMConstants.LOGGING_TAG, "Failed to get current position");
            jSONObject.put("x", i3);
            jSONObject.put("y", i5);
            jSONObject.put("width", i4);
            jSONObject.put("height", 0);
            return jSONObject.toString();
        } catch (Throwable th7) {
            th = th7;
            i2 = 0;
            i = 0;
            jSONObject.put("x", i);
            jSONObject.put("y", i2);
            jSONObject.put("width", i5);
            jSONObject.put("height", 0);
            throw th;
        }
        return jSONObject.toString();
    }
}
