package com.inmobi.androidsdk.ai.controller.util;

import org.apache.cordova.NetworkManager;

public enum IMNavigationStringEnum {
    NONE(NetworkManager.TYPE_NONE),
    CLOSE("close"),
    BACK("back"),
    FORWARD("forward"),
    REFRESH("refresh");
    
    private String a;

    private IMNavigationStringEnum(String str) {
        this.a = str;
    }

    public String getText() {
        return this.a;
    }

    public static IMNavigationStringEnum fromString(String str) {
        if (str != null) {
            for (IMNavigationStringEnum iMNavigationStringEnum : values()) {
                if (str.equalsIgnoreCase(iMNavigationStringEnum.a)) {
                    return iMNavigationStringEnum;
                }
            }
        }
        return null;
    }
}
