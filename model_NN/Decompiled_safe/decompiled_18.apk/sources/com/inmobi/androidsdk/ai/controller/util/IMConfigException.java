package com.inmobi.androidsdk.ai.controller.util;

import com.inmobi.androidsdk.impl.IMConfigConstants;

public final class IMConfigException extends Exception {
    public static final int MISSING_ACTIVITY_DECLARATION = -2;
    public static final int MISSING_CONFIG_CHANGES = -3;
    public static final int MISSING_CONFIG_KEYBOARD = -4;
    public static final int MISSING_CONFIG_KEYBOARDHIDDEN = -5;
    public static final int MISSING_CONFIG_ORIENTATION = -6;
    public static final int MISSING_CONFIG_SCREENSIZE = -7;
    public static final int MISSING_CONFIG_SMALLEST_SCREENSIZE = -8;
    public static final int MISSING_PERMISSION = -1;
    private static final long serialVersionUID = 1;
    private int a;

    IMConfigException(int i) {
        this.a = i;
    }

    public int getExceptionCode() {
        return this.a;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer("IMConfigException : ");
        switch (this.a) {
            case MISSING_CONFIG_SMALLEST_SCREENSIZE /*-8*/:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_CONFIG_SMALLEST_SCREENSIZE);
                break;
            case MISSING_CONFIG_SCREENSIZE /*-7*/:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_CONFIG_SCREENSIZE);
                break;
            case MISSING_CONFIG_ORIENTATION /*-6*/:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_CONFIG_ORIENTATION);
                break;
            case MISSING_CONFIG_KEYBOARDHIDDEN /*-5*/:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_CONFIG_KEYBOARDHIDDEN);
                break;
            case MISSING_CONFIG_KEYBOARD /*-4*/:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_CONFIG_KEYBOARD);
                break;
            case MISSING_CONFIG_CHANGES /*-3*/:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_CONFIG_CHANGES);
                break;
            case -2:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_ACTIVITY_DECLARATION);
                break;
            case -1:
                stringBuffer.append(IMConfigConstants.MSG_MISSING_PERMISSION);
                break;
        }
        return stringBuffer.toString();
    }
}
