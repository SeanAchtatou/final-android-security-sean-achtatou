package com.inmobi.androidsdk.ai.controller.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.util.Log;
import com.inmobi.androidsdk.IMBrowserActivity;
import com.inmobi.androidsdk.impl.IMConfigConstants;
import com.inmobi.commons.IMCommonUtil;
import java.util.Iterator;

public class IMSDKUtil {
    public static final String BROWSER_ACTIVITY = "com.inmobi.androidsdk.IMBrowserActivity";
    public static final long DEFAULT_SLOTID = -1;

    public static void validateAdConfiguration(Context context) throws IMConfigException {
        ResolveInfo resolveInfo;
        if (!"3.6.0".equalsIgnoreCase(IMCommonUtil.getReleaseVersion())) {
            Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_RELEASE_VERSION_MISMATCH);
            throw new RuntimeException(IMConfigConstants.MSG_RELEASE_VERSION_MISMATCH);
        } else if (context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_PERMISSION);
            throw new IMConfigException(-1);
        } else {
            Iterator<ResolveInfo> it = context.getPackageManager().queryIntentActivities(new Intent(context, IMBrowserActivity.class), 65536).iterator();
            while (true) {
                if (!it.hasNext()) {
                    resolveInfo = null;
                    break;
                }
                resolveInfo = it.next();
                if (resolveInfo.activityInfo.name.contentEquals(BROWSER_ACTIVITY)) {
                    break;
                }
            }
            if (resolveInfo == null) {
                Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_ACTIVITY_DECLARATION);
                throw new IMConfigException(-2);
            }
            int i = resolveInfo.activityInfo.configChanges;
            if (i == 0) {
                Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_CONFIG_CHANGES);
                throw new IMConfigException(-3);
            } else if ((i & 16) == 0) {
                Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_CONFIG_KEYBOARD);
                throw new IMConfigException(-4);
            } else if ((i & 32) == 0) {
                Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_CONFIG_KEYBOARDHIDDEN);
                throw new IMConfigException(-5);
            } else if ((i & 128) == 0) {
                Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_CONFIG_ORIENTATION);
                throw new IMConfigException(-6);
            } else if (Build.VERSION.SDK_INT < 13) {
            } else {
                if ((i & 1024) == 0) {
                    Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_CONFIG_SCREENSIZE);
                    throw new IMConfigException(-7);
                } else if ((i & 2048) == 0) {
                    Log.e(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_MISSING_CONFIG_SMALLEST_SCREENSIZE);
                    throw new IMConfigException(-8);
                }
            }
        }
    }

    public static void validateAppID(String str) {
        if (str == null) {
            throw new NullPointerException(IMConfigConstants.MSG_NIL_APP_ID);
        } else if (str.trim().equalsIgnoreCase("")) {
            throw new IllegalArgumentException(IMConfigConstants.MSG_INVALID_APP_ID_PARAM);
        }
    }

    public static Activity getRootActivity(Activity activity) {
        while (activity.getParent() != null) {
            activity = activity.getParent();
        }
        return activity;
    }
}
