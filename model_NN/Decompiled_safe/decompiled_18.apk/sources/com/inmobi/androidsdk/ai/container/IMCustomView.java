package com.inmobi.androidsdk.ai.container;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.view.View;

public class IMCustomView extends View {
    private float a;
    private float b;
    private float c;
    private float d;
    private float e;
    private SwitchIconType f;
    private int g;
    private Paint h;
    private Path i;
    private RectF j;

    public enum SwitchIconType {
        CLOSE_BUTTON,
        CLOSE_TRANSPARENT,
        CLOSE_ICON,
        REFRESH,
        BACK,
        FORWARD_ACTIVE,
        FORWARD_INACTIVE
    }

    private IMCustomView(Context context) {
        super(context);
    }

    public IMCustomView(Context context, float f2, SwitchIconType switchIconType) {
        this(context);
        this.f = switchIconType;
        this.a = f2;
        this.g = 15;
        this.b = (50.0f * this.a) / 2.0f;
        this.c = (30.0f * this.a) / 2.0f;
        this.d = this.b - (this.c / 3.0f);
        this.e = this.b + (this.c / 3.0f);
        this.h = new Paint(1);
        this.j = new RectF();
        this.i = new Path();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.h.reset();
        switch (this.f) {
            case CLOSE_BUTTON:
                this.h.setAntiAlias(true);
                this.h.setColor(-16777216);
                this.h.setStrokeWidth(3.0f);
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawCircle(this.b, this.b, this.c, this.h);
                this.h.setColor(-1);
                this.h.setStyle(Paint.Style.STROKE);
                canvas.drawLine(this.d, this.d, this.e, this.e, this.h);
                canvas.drawLine(this.d, this.e, this.e, this.d, this.h);
                canvas.drawCircle(this.b, this.b, this.c, this.h);
                return;
            case CLOSE_TRANSPARENT:
                this.h.setAntiAlias(true);
                this.h.setColor(0);
                this.h.setStrokeWidth(3.0f);
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawCircle(this.b, this.b, this.b, this.h);
                return;
            case FORWARD_ACTIVE:
                this.i.reset();
                this.i.setFillType(Path.FillType.EVEN_ODD);
                this.i.moveTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f));
                this.i.lineTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), (float) (getHeight() / 2));
                this.i.lineTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) + ((((float) this.g) * this.a) / 2.0f));
                this.i.lineTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f));
                this.i.close();
                this.h.setAntiAlias(true);
                this.h.setColor(-16777216);
                this.h.setStrokeWidth(3.0f);
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawPath(this.i, this.h);
                return;
            case FORWARD_INACTIVE:
                this.i.reset();
                this.i.setFillType(Path.FillType.EVEN_ODD);
                this.i.moveTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f));
                this.i.lineTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), (float) (getHeight() / 2));
                this.i.lineTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) + ((((float) this.g) * this.a) / 2.0f));
                this.i.lineTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f));
                this.i.close();
                this.h.setAntiAlias(true);
                this.h.setColor(-12303292);
                this.h.setStrokeWidth(3.0f);
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawPath(this.i, this.h);
                return;
            case BACK:
                this.i.reset();
                this.i.setFillType(Path.FillType.EVEN_ODD);
                this.i.moveTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), (float) (getHeight() / 2));
                this.i.lineTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f));
                this.i.lineTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) + ((((float) this.g) * this.a) / 2.0f));
                this.i.lineTo(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), (float) (getHeight() / 2));
                this.i.close();
                this.h.setAntiAlias(true);
                this.h.setColor(-16777216);
                this.h.setStrokeWidth(3.0f);
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawPath(this.i, this.h);
                return;
            case REFRESH:
                this.i.reset();
                this.h.setAntiAlias(true);
                this.h.setColor(-16777216);
                this.h.setStrokeWidth(5.0f);
                this.h.setStyle(Paint.Style.STROKE);
                this.j.set(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) + ((((float) this.g) * this.a) / 2.0f));
                canvas.drawArc(this.j, 0.0f, 270.0f, false, this.h);
                this.i.setFillType(Path.FillType.EVEN_ODD);
                this.i.moveTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - (this.a * 2.0f));
                this.i.lineTo((((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f)) - (this.a * 2.0f), (float) (getHeight() / 2));
                this.i.lineTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f) + (this.a * 2.0f), (float) (getHeight() / 2));
                this.i.lineTo(((float) (getWidth() / 2)) + ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - (this.a * 2.0f));
                this.i.close();
                this.h.setStyle(Paint.Style.FILL_AND_STROKE);
                canvas.drawPath(this.i, this.h);
                return;
            case CLOSE_ICON:
                this.h.setAntiAlias(true);
                this.h.setColor(-16777216);
                this.h.setStrokeWidth(5.0f);
                this.h.setStyle(Paint.Style.STROKE);
                Canvas canvas2 = canvas;
                canvas2.drawLine(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((((float) this.g) * this.a) / 2.0f) + ((float) (getWidth() / 2)), ((((float) this.g) * this.a) / 2.0f) + ((float) (getHeight() / 2)), this.h);
                Canvas canvas3 = canvas;
                canvas3.drawLine(((float) (getWidth() / 2)) - ((((float) this.g) * this.a) / 2.0f), ((((float) this.g) * this.a) / 2.0f) + ((float) (getHeight() / 2)), ((((float) this.g) * this.a) / 2.0f) + ((float) (getWidth() / 2)), ((float) (getHeight() / 2)) - ((((float) this.g) * this.a) / 2.0f), this.h);
                return;
            default:
                return;
        }
    }

    public void setSwitchInt(SwitchIconType switchIconType) {
        this.f = switchIconType;
    }
}
