package com.inmobi.androidsdk.ai.container;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JsResult;
import android.webkit.URLUtil;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.inmobi.androidsdk.IMBrowserActivity;
import com.inmobi.androidsdk.ai.container.IMCustomView;
import com.inmobi.androidsdk.ai.controller.JSController;
import com.inmobi.androidsdk.ai.controller.JSUtilityController;
import com.inmobi.androidsdk.ai.controller.util.IMAVPlayer;
import com.inmobi.androidsdk.ai.controller.util.IMAVPlayerListener;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.internal.IMLog;
import com.inmobi.commons.internal.InternalSDKUtil;
import com.tapit.adview.AdViewCore;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;

public class IMWebView extends WebView implements Serializable {
    protected static final int IMWEBVIEW_INTERSTITIAL_ID = 117;
    protected static final int INT_BACKGROUND_ID = 224;
    protected static final int INT_CLOSE_BUTTON = 225;
    protected static final int PLACEHOLDER_ID = 437;
    protected static final int RELATIVELAYOUT_ID = 438;
    private static int[] a = {16843039, 16843040};
    public static AtomicBoolean isInterstitialDisplayed = new AtomicBoolean();
    private static final long serialVersionUID = 7098506283154473782L;
    public static boolean userInitiatedClose = false;
    /* access modifiers changed from: private */
    public Hashtable<String, IMAVPlayer> A = new Hashtable<>();
    /* access modifiers changed from: private */
    public Hashtable<String, IMAVPlayer> B = new Hashtable<>();
    private int C;
    private int D;
    /* access modifiers changed from: private */
    public ArrayList<String> E = new ArrayList<>();
    /* access modifiers changed from: private */
    public AtomicBoolean F = new AtomicBoolean();
    private a G = new a(this);
    private Display H;
    private ViewGroup I;
    private WebViewClient J = new WebViewClient() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean
         arg types: [com.inmobi.androidsdk.ai.container.IMWebView, int]
         candidates:
          com.inmobi.androidsdk.ai.container.IMWebView.b(android.os.Bundle, android.app.Activity):void
          com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, java.lang.String):void
          com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean */
        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onPageStarted url: " + str);
            if (IMWebView.this.aa != null) {
                IMWebView.this.aa.onPageStarted(webView, str, bitmap);
            }
            boolean unused = IMWebView.this.j = false;
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> error: " + str);
            if (IMWebView.this.aa != null) {
                IMWebView.this.aa.onReceivedError(webView, i, str, str2);
            }
            try {
                if (IMWebView.this.h == ViewState.LOADING && IMWebView.this.i != null && !IMWebView.this.F.get()) {
                    IMWebView.this.i.onError();
                }
                Message unused = IMWebView.this.S = (Message) null;
            } catch (Exception e) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Exception in webview loading ", e);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean
         arg types: [com.inmobi.androidsdk.ai.container.IMWebView, int]
         candidates:
          com.inmobi.androidsdk.ai.container.IMWebView.b(android.os.Bundle, android.app.Activity):void
          com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, java.lang.String):void
          com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.ai.container.IMWebView.c(com.inmobi.androidsdk.ai.container.IMWebView, boolean):void
         arg types: [com.inmobi.androidsdk.ai.container.IMWebView, int]
         candidates:
          com.inmobi.androidsdk.ai.container.IMWebView.c(com.inmobi.androidsdk.ai.container.IMWebView, java.lang.String):java.lang.String
          com.inmobi.androidsdk.ai.container.IMWebView.c(com.inmobi.androidsdk.ai.container.IMWebView, boolean):void */
        public void onPageFinished(WebView webView, String str) {
            IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onPageFinished, url: " + str);
            if (IMWebView.this.aa != null) {
                IMWebView.this.aa.onPageFinished(webView, str);
            }
            try {
                if (!IMWebView.this.j && IMWebView.this.E.contains(str)) {
                    boolean unused = IMWebView.this.j = true;
                    IMWebView.this.injectJavaScript("(function(){var c=window.mraidview={},f={},g=[],l=!1;c.fireReadyEvent=function(){var b=f.ready;if(null!=b)for(var a=0;a<b.length;a++)b[a]();return\"OK\"};c.fireStateChangeEvent=function(b){var a=f.stateChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireViewableChangeEvent=function(b){var a=f.viewableChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireErrorEvent=function(b,a){var c=f.error;if(null!=c)for(var e=0;e<c.length;e++)c[e](b,a);return\"OK\"};c.fireOrientationChangeEvent=function(b){var a=f.orientationChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireMediaTrackingEvent=function(b,a){var c={};c.name=b;var e=\"inmobi_media_\"+b;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(e=e+\"_\"+a);e=f[e];if(null!=e)for(var h=0;h<e.length;h++)e[h](c);return\"OK\"};c.fireMediaErrorEvent=function(b,a){var c={name:\"error\"};c.code=a;var e=\"inmobi_media_\"+c.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(e=e+\"_\"+b);e=f[e];if(null!=e)for(var h=0;h<e.length;h++)e[h](c);return\"OK\"};c.fireMediaTimeUpdateEvent=function(b,a,c){var e={name:\"timeupdate\",target:{}};e.target.currentTime=a;e.target.duration=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.fireMediaCloseEvent=function(b,a,c){var e={name:\"close\"};e.viaUserInteraction=a;e.target={};e.target.currentTime=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.fireMediaVolumeChangeEvent=function(b,a,c){var e={name:\"volumechange\",target:{}};e.target.volume=a;e.target.muted=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.showAlert=function(b){utilityController.showAlert(b)};c.zeroPad=function(b){var a=\"\";10>b&&(a+=\"0\");return a+b};c.addEventListener=function(b,a){var c=f[b];null==c&&(f[b]=[],c=f[b]);for(var e in c)if(a==e)return;c.push(a)};c.removeEventListener=function(b){try{var a=f[b];null!=a&&delete a}catch(d){c.log(d)}};c.useCustomClose=function(b){try{displayController.useCustomClose(b)}catch(a){c.showAlert(\"use CustomClose: \"+a)}};c.close=function(){try{displayController.close()}catch(b){c.showAlert(\"close: \"+b)}};c.stackCommands=function(b,a){l?g.push(b):(eval(b),a&&(l=!0))};c.executeStack=function(){for(l=!1;0<g.length;){var b=g.shift();eval(b)}};c.emptyStack=function(){for(;0<g.length;)g.shift()};c.expand=function(b){try{displayController.expand(b)}catch(a){c.showAlert(\"executeNativeExpand: \"+a+\", URL = \"+b)}};c.setExpandProperties=function(b){try{b?this.props=b:b=null,displayController.setExpandProperties(c.stringify(b))}catch(a){c.showAlert(\"executeNativesetExpandProperties: \"+a+\", props = \"+b)}};c.acceptAction=function(b){try{displayController.acceptAction(c.stringify(b))}catch(a){c.showAlert(\"acceptAction: \"+a+\", params = \"+b)}};c.rejectAction=function(b){try{displayController.rejectAction(c.stringify(b))}catch(a){c.showAlert(\"rejectAction: \"+a+\", params = \"+b)}};c.open=function(b){try{displayController.open(b)}catch(a){c.showAlert(\"open: \"+a)}};c.openExternal=function(b){try{utilityController.openExternal(b)}catch(a){c.showAlert(\"openExternal: \"+a)}};c.getScreenSize=function(){try{return eval(\"(\"+utilityController.getScreenSize()+\")\")}catch(b){c.showAlert(\"getScreenSize: \"+b)}};c.getCurrentPosition=function(){try{return eval(\"(\"+utilityController.getCurrentPosition()+\")\")}catch(b){c.showAlert(\"getCurrentPosition: \"+b)}};c.resize=function(b,a){try{displayController.resize(b,a)}catch(d){c.showAlert(\"resize: \"+d)}};c.getState=function(){try{return String(displayController.getState())}catch(b){c.showAlert(\"getState: \"+b)}};c.getOrientation=function(){try{return String(displayController.getOrientation())}catch(b){c.showAlert(\"getOrientation: \"+b)}};c.isViewable=function(){try{return displayController.isViewable()}catch(b){c.showAlert(\"isViewable: \"+b)}};c.log=function(b){try{utilityController.log(b)}catch(a){c.showAlert(\"log: \"+a)}};c.getPlacementType=function(){return displayController.getPlacementType()};c.asyncPing=function(b){try{utilityController.asyncPing(b)}catch(a){c.showAlert(\"asyncPing: \"+a)}};c.close=function(){try{displayController.close()}catch(b){c.showAlert(\"close: \"+b)}};c.makeCall=function(b){try{utilityController.makeCall(b)}catch(a){c.showAlert(\"makeCall: \"+a)}};c.sendMail=function(b,a,d){try{utilityController.sendMail(b,a,d)}catch(e){c.showAlert(\"sendMail: \"+e)}};c.sendSMS=function(b,a){try{utilityController.sendSMS(b,a)}catch(d){c.showAlert(\"sendSMS: \"+d)}};c.pauseAudio=function(b){try{var a=getPID(b);utilityController.pauseAudio(a)}catch(d){c.showAlert(\"pauseAudio: \"+d)}};c.muteAudio=function(b){try{var a=getPID(b);utilityController.muteAudio(a)}catch(d){c.showAlert(\"muteAudio: \"+d)}};c.unMuteAudio=function(b){try{var a=getPID(b);utilityController.unMuteAudio(a)}catch(d){c.showAlert(\"unMuteAudio: \"+d)}};c.isAudioMuted=function(b){try{var a=getPID(b);return utilityController.isAudioMuted(a)}catch(d){c.showAlert(\"isAudioMuted: \"+d)}};c.setAudioVolume=function(b,a){try{var d=getPID(b);utilityController.setAudioVolume(d,a)}catch(e){c.showAlert(\"setAudioVolume: \"+e)}};c.getAudioVolume=function(b){try{var a=getPID(b);return utilityController.getAudioVolume(a)}catch(d){c.showAlert(\"getAudioVolume: \"+d)}};c.seekAudio=function(b,a){try{var d=getPID(b);utilityController.seekAudio(d,a)}catch(e){c.showAlert(\"seekAudio: \"+e)}};c.playAudio=function(b,a){var d=!0,e=!1,h=\"normal\",f=\"normal\",g=!0,j=\"\",n=getPID(a);null!=b&&(j=b);null!=a&&(\"undefined\"!=typeof a.autoplay&&!1===a.autoplay&&(d=!1),\"undefined\"!=typeof a.loop&&!0===a.loop&&(e=!0),\"undefined\"!=typeof a.startStyle&&null!=a.startStyle&&(h=a.startStyle),\"undefined\"!=typeof a.stopStyle&&null!=a.stopStyle&&(f=a.stopStyle),\"fullscreen\"==h&&(g=!0));try{utilityController.playAudio(j,d,g,e,h,f,n)}catch(o){c.showAlert(\"playAudio: \"+o)}};c.pauseVideo=function(b){try{var a=getPID(b);utilityController.pauseVideo(a)}catch(d){c.showAlert(\"pauseVideo: \"+d)}};c.closeVideo=function(b){try{var a=getPID(b);utilityController.closeVideo(a)}catch(d){c.showAlert(\"closeVideo: \"+d)}};c.hideVideo=function(b){try{var a=getPID(b);utilityController.hideVideo(a)}catch(d){c.showAlert(\"hideVideo: \"+d)}};c.showVideo=function(b){try{var a=getPID(b);utilityController.showVideo(a)}catch(d){c.showAlert(\"showVideo: \"+d)}};c.muteVideo=function(b){try{var a=getPID(b);utilityController.muteVideo(a)}catch(d){c.showAlert(\"muteVideo: \"+d)}};c.unMuteVideo=function(b){try{var a=getPID(b);utilityController.unMuteVideo(a)}catch(d){c.showAlert(\"unMuteVideo: \"+d)}};c.seekVideo=function(b,a){try{var d=getPID(b);utilityController.seekVideo(d,a)}catch(e){c.showAlert(\"seekVideo: \"+e)}};c.isVideoMuted=function(b){try{var a=getPID(b);return utilityController.isVideoMuted(a)}catch(d){c.showAlert(\"isVideoMuted: \"+d)}};c.setVideoVolume=function(b,a){try{var d=getPID(b);utilityController.setVideoVolume(d,a)}catch(e){c.showAlert(\"setVideoVolume: \"+e)}};c.getVideoVolume=function(b){try{var a=getPID(b);return utilityController.getVideoVolume(a)}catch(d){c.showAlert(\"getVideoVolume: \"+d)}};c.playVideo=function(b,a){var d=!1,e=!0,f=!0,g=!1,j=-99999,l=-99999,n=-99999,o=-99999,k=\"normal\",m=\"exit\",p=\"\",q=getPID(a);null!=b&&(p=b);if(null!=a){\"undefined\"!=typeof a.audio&&\"muted\"==a.audio&&(d=!0);\"undefined\"!=typeof a.autoplay&&!1===a.autoplay&&(e=!1);\"undefined\"!=typeof a.controls&&!1===a.controls&&(f=!1);\"undefined\"!=typeof a.loop&&!0===a.loop&&(g=!0);if(\"undefined\"!=typeof a.inline&&null!=a.inline&&(j=a.inline.left,l=a.inline.top,\"undefined\"!=typeof a.width&&null!=a.width&&(n=a.width),\"undefined\"!=typeof a.height&&null!=a.height))o=a.height;\"undefined\"!=typeof a.startStyle&&null!=a.startStyle&&(k=a.startStyle);\"undefined\"!=typeof a.stopStyle&&null!=a.stopStyle&&(m=a.stopStyle);\"fullscreen\"==k&&(f=!0)}try{utilityController.playVideo(p,d,e,f,g,j,l,n,o,k,m,q)}catch(r){c.showAlert(\"playVideo: \"+r)}};c.updateToPassbook=function(){c.fireErrorEvent(\"Method not supported\",\"updateToPassbook\");c.log(\"Method not supported\")};c.stringify=function(b){if(\"undefined\"===typeof JSON){var a=\"\",d;if(\"undefined\"==typeof b.length)return c.stringifyArg(b);for(d=0;d<b.length;d++)0<d&&(a+=\",\"),a+=c.stringifyArg(b[d]);return a+\"]\"}return JSON.stringify(b)};c.stringifyArg=function(b){var a,d,e;d=typeof b;a=\"\";if(\"number\"===d||\"boolean\"===d)a+=args;else if(b instanceof Array)a=a+\"[\"+b+\"]\";else if(b instanceof Object){d=!0;a+=\"{\";for(e in b)null!==b[e]&&(d||(a+=\",\"),a=a+'\"'+e+'\":',d=typeof b[e],a=\"number\"===d||\"boolean\"===d?a+b[e]:\"function\"===typeof b[e]?a+'\"\"':b[e]instanceof Object?a+this.stringify(args[i][e]):a+'\"'+b[e]+'\"',d=!1);a+=\"}\"}else b=b.replace(/\\\\/g,\"\\\\\\\\\"),b=b.replace(/\"/g,'\\\\\"'),a=a+'\"'+b+'\"';c.showAlert(\"json:\"+a);return a};getPID=function(b){var a=\"\";null!=b&&(\"undefined\"!=typeof b.id&&null!=b.id)&&(a=b.id);return a};var k,j=function(){window.orientation!==k&&(k=window.orientation,displayController.onOrientationChange())};c.registerOrientationListener=function(){k=window.orientation;window.addEventListener(\"resize\",j,!1);window.addEventListener(\"orientationchange\",j,!1)};c.unRegisterOrientationListener=function(){window.removeEventListener(\"resize\",j,!1);window.removeEventListener(\"orientationchange\",j,!1)}})();");
                    IMWebView.this.injectJavaScript("(function(){var c=window.mraid={};c.STATES={LOADING:\"loading\",DEFAULT:\"default\",RESIZED:\"resized\",EXPANDED:\"expanded\",HIDDEN:\"hidden\"};var d=c.EVENTS={READY:\"ready\",ERROR:\"error\",STATECHANGE:\"stateChange\",VIEWABLECHANGE:\"viewableChange\",ORIENTATIONCHANGE:\"orientationChange\",PASSBOOKCHANGE:\"passbookChange\"},i={width:0,height:0},g={width:0,height:0},f={},h={width:0,height:0,useCustomClose:!1,isModal:!0,lockOrientation:!1,orientation:\"\"},l=function(a){this.event=a;this.count=0;var b={};this.add=function(a){var c=String(a);b[c]||(b[c]=a,this.count++)};this.remove=function(a){a=String(a);return b[a]?(b[a]=null,delete b[a],this.count--,!0):!1};this.removeAll=function(){for(var a in b)this.remove(b[a])};this.broadcast=function(a){for(var c in b)b[c].apply({},a)};this.toString=function(){var c=[a,\":\"],d;for(d in b)c.push(\"|\",d,\"|\");return c.join(\"\")}};mraidview.addEventListener(d.READY,function(){e(d.READY)});mraidview.addEventListener(d.STATECHANGE,function(a){e(d.STATECHANGE,a)});mraidview.addEventListener(d.VIEWABLECHANGE,function(a){e(d.VIEWABLECHANGE,a)});mraidview.addEventListener(\"error\",function(a,b){e(d.ERROR,a,b)});mraidview.addEventListener(d.ORIENTATIONCHANGE,function(a){e(d.ORIENTATIONCHANGE,a)});var k=function(a){var b=function(){};b.prototype=a;return new b},e=function(){for(var a=Array(arguments.length),b=0;b<arguments.length;b++)a[b]=arguments[b];b=a.shift();try{f[b]&&f[b].broadcast(a)}catch(c){}},j=function(a){for(var b=0,c=a.length-1;b<a.length&&\" \"==a[b];)b++;for(;c>b&&\" \"==a[c];)c-=1;return a.substring(b,c+1)};c.addEventListener=function(a,b){try{!a||!b?e(d.ERROR,\"Both event and listener are required.\",\"addEventListener\"):d.ERROR==a||d.READY==a||d.STATECHANGE==a||d.VIEWABLECHANGE==a||d.ORIENTATIONCHANGE==a?(f[a]||(f[a]=new l(a)),f[a].add(b)):mraidview.addEventListener(a,b)}catch(c){mraidview.log(c)}};c.useCustomClose=function(a){h.useCustomClose=a;mraidview.useCustomClose(a)};c.close=function(){mraidview.close()};c.getExpandProperties=function(){return h};c.setExpandProperties=function(a){h=a;h.isModal=!0;mraidview.setExpandProperties(h)};c.expand=function(a){mraidview.expand(a)};c.getMaxSize=function(){return k(g)};c.getSize=function(){return k(i)};c.getState=function(){return mraidview.getState()};c.getOrientation=function(){return mraidview.getOrientation()};c.isViewable=function(){return mraidview.isViewable()};c.open=function(a){a?mraidview.open(a):e(d.ERROR,\"URL is required.\",\"open\")};c.removeEventListener=function(a,b){try{if(a){if(b)if(f[a])f[a].remove(b);else{mraidview.removeEventListener(a,b);return}else f[a]&&f[a].removeAll();f[a]&&0==f[a].count&&(f[a]=null,delete f[a])}else e(d.ERROR,\"Must specify an event.\",\"removeEventListener\")}catch(c){mraidview.log(\"removeEventListener\"+c)}};c.resize=function(a,b){null==a||null==b||isNaN(a)||isNaN(b)||0>a||0>b?e(d.ERROR,\"Requested size must be numeric values between 0 and maxSize.\",\"resize\"):a>g.width||b>g.height?e(d.ERROR,\"Request (\"+a+\" x \"+b+\") exceeds maximum allowable size of (\"+g.width+\" x \"+g.height+\")\",\"resize\"):a==i.width&&b==i.height?e(d.ERROR,\"Requested size equals current size.\",\"resize\"):mraidview.resize(a,b)};c.log=function(a){null == a || \"undefined\" == a?e(d.ERROR,\"message is required.\",\"log\"):mraidview.log(a)};c.getVersion=function(){return\"1.0\"};c.getInMobiAIVersion=function(){return 1.2};c.getPlacementType=function(){return mraidview.getPlacementType()};c.asyncPing=function(a){a?mraidview.asyncPing(a):e(d.ERROR,\"URL is required.\",\"asyncPing\")};c.makeCall=function(a){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must provide a number to call.\",\"makeCall\"):mraidview.makeCall(a)};c.sendMail=function(a,b,c){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must specify a recipient.\",\"sendMail\"):mraidview.sendMail(a,b,c)};c.sendSMS=function(a,b){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must specify a recipient.\",\"sendSMS\"):mraidview.sendSMS(a,b)};c.playAudio=function(a,b){\"undefined\"==typeof b||null==b?\"string\"==typeof a?mraidview.playAudio(a,null):\"object\"==typeof a?mraidview.playAudio(null,a):mraidview.playAudio(null,null):mraidview.playAudio(a,b)};c.playVideo=function(a,b){\"undefined\"==typeof b||null==b?\"string\"==typeof a?mraidview.playVideo(a,null):\"object\"==typeof a?mraidview.playVideo(null,a):mraidview.playVideo(null,null):mraidview.playVideo(a,b)};c.pauseAudio=function(a){mraidview.pauseAudio(a)};c.muteAudio=function(a){mraidview.muteAudio(a)};c.unMuteAudio=function(a){mraidview.unMuteAudio(a)};c.isAudioMuted=function(a){return mraidview.isAudioMuted(a)};c.setAudioVolume=function(a){var b=a.volume;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid volume\",\"setAudioVolume\"):0>b||100<b?e(d.ERROR,\"Request must specify a valid volume value in the range 0..100\",\"setAudioVolume\"):mraidview.setAudioVolume(a,b)};c.getAudioVolume=function(a){return mraidview.getAudioVolume(a)};c.pauseVideo=function(a){mraidview.pauseVideo(a)};c.closeVideo=function(a){mraidview.closeVideo(a)};c.hideVideo=function(a){mraidview.hideVideo(a)};c.showVideo=function(a){mraidview.showVideo(a)};c.muteVideo=function(a){mraidview.muteVideo(a)};c.unMuteVideo=function(a){mraidview.unMuteVideo(a)};c.isVideoMuted=function(a){return mraidview.isVideoMuted(a)};c.setVideoVolume=function(a){var b=a.volume;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid volume\",\"setVideoVolume\"):0>b||100<b?e(d.ERROR,\"Request must specify a valid volume value in the range 0..100\",\"setVideoVolume\"):mraidview.setVideoVolume(a,b)};c.getVideoVolume=function(a){return mraidview.getVideoVolume(a)};c.seekAudio=function(a){var b=a.time;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid time\",\"seekAudio\"):0!=b?e(d.ERROR,\"Cannot seek audio other than 0\",\"seekAudio\"):mraidview.seekAudio(a,b)};c.seekVideo=function(a){var b=a.time;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid time\",\"seekVideo\"):0!=b?e(d.ERROR,\"Cannot seek video other than 0\",\"seekVideo\"):mraidview.seekVideo(a,b)};c.openExternal=function(a){mraidview.openExternal(a)};c.updateToPassbook=function(a){mraidview.updateToPassbook(a)};c.getScreenSize=function(){return mraidview.getScreenSize()};c.getCurrentPosition=function(){return mraidview.getCurrentPosition()};c.acceptAction=function(a){mraidview.acceptAction(a)};c.rejectAction=function(a){mraidview.rejectAction(a)}})();");
                }
                IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> Current State:" + IMWebView.this.h);
                if (IMWebView.this.h == ViewState.LOADING) {
                    if (IMWebView.this.o) {
                        IMWebView.this.a(ViewState.EXPANDED);
                    } else {
                        IMWebView.this.a(ViewState.DEFAULT);
                    }
                    IMWebView.this.injectJavaScript("window.mraidview.fireReadyEvent();");
                    if (!IMWebView.this.mIsInterstitialAd || IMWebView.this.q) {
                        IMWebView.this.a(true);
                        if (IMWebView.this.getVisibility() == 4) {
                            IMWebView.this.setVisibility(0);
                        }
                    }
                    if (IMWebView.this.S != null && !IMWebView.this.F.get()) {
                        IMWebView.this.S.sendToTarget();
                    }
                    if (IMWebView.this.T != null) {
                        IMWebView.this.T.sendToTarget();
                    }
                }
            } catch (Exception e) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Exception in onPageFinished ", e);
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> shouldOverrideUrlLoading, url:" + str + "webview id" + webView);
            if (IMWebView.this.l) {
                IMWebView.this.a(webView, str);
                return true;
            }
            Uri parse = Uri.parse(str);
            try {
                if (str.startsWith("tel:")) {
                    Intent intent = new Intent("android.intent.action.DIAL", Uri.parse(str));
                    intent.addFlags(268435456);
                    IMWebView.this.W.startActivity(intent);
                    IMWebView.this.fireOnLeaveApplication();
                    return true;
                } else if (str.startsWith("mailto:")) {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    intent2.addFlags(268435456);
                    IMWebView.this.W.startActivity(intent2);
                    IMWebView.this.fireOnLeaveApplication();
                    return true;
                } else if (str.startsWith("about:blank")) {
                    return false;
                } else {
                    if (!str.startsWith("http://") || str.contains("play.google.com") || str.contains("market.android.com") || str.contains("market%3A%2F%2F")) {
                        Intent intent3 = new Intent();
                        intent3.setAction("android.intent.action.VIEW");
                        intent3.setData(parse);
                        intent3.addFlags(268435456);
                        IMWebView.this.W.startActivity(intent3);
                        IMWebView.this.fireOnLeaveApplication();
                        return true;
                    }
                    IMWebView.this.doHidePlayers();
                    if (IMWebView.this.q) {
                        return false;
                    }
                    Intent intent4 = new Intent(IMWebView.this.W, IMBrowserActivity.class);
                    IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> shouldoverride:" + str);
                    intent4.putExtra(IMBrowserActivity.EXTRA_URL, str);
                    IMBrowserActivity.setWebViewListener(IMWebView.this.i);
                    IMWebView.this.W.startActivity(intent4);
                    return true;
                }
            } catch (Exception e) {
                try {
                    if (!str.startsWith("http://") || str.contains("play.google.com") || str.contains("market.android.com") || str.contains("market%3A%2F%2F")) {
                        Intent intent5 = new Intent();
                        intent5.setAction("android.intent.action.VIEW");
                        intent5.setData(parse);
                        intent5.addFlags(268435456);
                        IMWebView.this.W.startActivity(intent5);
                        IMWebView.this.fireOnLeaveApplication();
                        return true;
                    }
                    IMWebView.this.doHidePlayers();
                    if (IMWebView.this.q) {
                        return false;
                    }
                    Intent intent6 = new Intent(IMWebView.this.W, IMBrowserActivity.class);
                    IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> open:" + str);
                    intent6.putExtra(IMBrowserActivity.EXTRA_URL, str);
                    IMBrowserActivity.setWebViewListener(IMWebView.this.i);
                    IMWebView.this.W.startActivity(intent6);
                    return true;
                } catch (Exception e2) {
                    return false;
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean
         arg types: [com.inmobi.androidsdk.ai.container.IMWebView, int]
         candidates:
          com.inmobi.androidsdk.ai.container.IMWebView.b(android.os.Bundle, android.app.Activity):void
          com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, java.lang.String):void
          com.inmobi.androidsdk.ai.container.IMWebView.b(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean */
        public void onLoadResource(WebView webView, String str) {
            IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onLoadResource:" + str);
            if (IMWebView.this.aa != null) {
                IMWebView.this.aa.onLoadResource(webView, str);
            }
            if (!IMWebView.this.j && str.contains("mraid.js")) {
                IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onLoadResource:Hippy, Mraid ad alert!...injecting mraid and mraidview object");
                boolean unused = IMWebView.this.j = true;
                String url = IMWebView.this.getUrl();
                if (!IMWebView.this.E.contains(url)) {
                    IMWebView.this.E.add(url);
                }
                IMWebView.this.injectJavaScript("(function(){var c=window.mraidview={},f={},g=[],l=!1;c.fireReadyEvent=function(){var b=f.ready;if(null!=b)for(var a=0;a<b.length;a++)b[a]();return\"OK\"};c.fireStateChangeEvent=function(b){var a=f.stateChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireViewableChangeEvent=function(b){var a=f.viewableChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireErrorEvent=function(b,a){var c=f.error;if(null!=c)for(var e=0;e<c.length;e++)c[e](b,a);return\"OK\"};c.fireOrientationChangeEvent=function(b){var a=f.orientationChange;if(null!=a)for(var c=0;c<a.length;c++)a[c](b);return\"OK\"};c.fireMediaTrackingEvent=function(b,a){var c={};c.name=b;var e=\"inmobi_media_\"+b;\"undefined\"!=typeof a&&(null!=a&&\"\"!=a)&&(e=e+\"_\"+a);e=f[e];if(null!=e)for(var h=0;h<e.length;h++)e[h](c);return\"OK\"};c.fireMediaErrorEvent=function(b,a){var c={name:\"error\"};c.code=a;var e=\"inmobi_media_\"+c.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(e=e+\"_\"+b);e=f[e];if(null!=e)for(var h=0;h<e.length;h++)e[h](c);return\"OK\"};c.fireMediaTimeUpdateEvent=function(b,a,c){var e={name:\"timeupdate\",target:{}};e.target.currentTime=a;e.target.duration=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.fireMediaCloseEvent=function(b,a,c){var e={name:\"close\"};e.viaUserInteraction=a;e.target={};e.target.currentTime=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.fireMediaVolumeChangeEvent=function(b,a,c){var e={name:\"volumechange\",target:{}};e.target.volume=a;e.target.muted=c;a=\"inmobi_media_\"+e.name;\"undefined\"!=typeof b&&(null!=b&&\"\"!=b)&&(a=a+\"_\"+b);b=f[a];if(null!=b)for(a=0;a<b.length;a++)b[a](e);return\"OK\"};c.showAlert=function(b){utilityController.showAlert(b)};c.zeroPad=function(b){var a=\"\";10>b&&(a+=\"0\");return a+b};c.addEventListener=function(b,a){var c=f[b];null==c&&(f[b]=[],c=f[b]);for(var e in c)if(a==e)return;c.push(a)};c.removeEventListener=function(b){try{var a=f[b];null!=a&&delete a}catch(d){c.log(d)}};c.useCustomClose=function(b){try{displayController.useCustomClose(b)}catch(a){c.showAlert(\"use CustomClose: \"+a)}};c.close=function(){try{displayController.close()}catch(b){c.showAlert(\"close: \"+b)}};c.stackCommands=function(b,a){l?g.push(b):(eval(b),a&&(l=!0))};c.executeStack=function(){for(l=!1;0<g.length;){var b=g.shift();eval(b)}};c.emptyStack=function(){for(;0<g.length;)g.shift()};c.expand=function(b){try{displayController.expand(b)}catch(a){c.showAlert(\"executeNativeExpand: \"+a+\", URL = \"+b)}};c.setExpandProperties=function(b){try{b?this.props=b:b=null,displayController.setExpandProperties(c.stringify(b))}catch(a){c.showAlert(\"executeNativesetExpandProperties: \"+a+\", props = \"+b)}};c.acceptAction=function(b){try{displayController.acceptAction(c.stringify(b))}catch(a){c.showAlert(\"acceptAction: \"+a+\", params = \"+b)}};c.rejectAction=function(b){try{displayController.rejectAction(c.stringify(b))}catch(a){c.showAlert(\"rejectAction: \"+a+\", params = \"+b)}};c.open=function(b){try{displayController.open(b)}catch(a){c.showAlert(\"open: \"+a)}};c.openExternal=function(b){try{utilityController.openExternal(b)}catch(a){c.showAlert(\"openExternal: \"+a)}};c.getScreenSize=function(){try{return eval(\"(\"+utilityController.getScreenSize()+\")\")}catch(b){c.showAlert(\"getScreenSize: \"+b)}};c.getCurrentPosition=function(){try{return eval(\"(\"+utilityController.getCurrentPosition()+\")\")}catch(b){c.showAlert(\"getCurrentPosition: \"+b)}};c.resize=function(b,a){try{displayController.resize(b,a)}catch(d){c.showAlert(\"resize: \"+d)}};c.getState=function(){try{return String(displayController.getState())}catch(b){c.showAlert(\"getState: \"+b)}};c.getOrientation=function(){try{return String(displayController.getOrientation())}catch(b){c.showAlert(\"getOrientation: \"+b)}};c.isViewable=function(){try{return displayController.isViewable()}catch(b){c.showAlert(\"isViewable: \"+b)}};c.log=function(b){try{utilityController.log(b)}catch(a){c.showAlert(\"log: \"+a)}};c.getPlacementType=function(){return displayController.getPlacementType()};c.asyncPing=function(b){try{utilityController.asyncPing(b)}catch(a){c.showAlert(\"asyncPing: \"+a)}};c.close=function(){try{displayController.close()}catch(b){c.showAlert(\"close: \"+b)}};c.makeCall=function(b){try{utilityController.makeCall(b)}catch(a){c.showAlert(\"makeCall: \"+a)}};c.sendMail=function(b,a,d){try{utilityController.sendMail(b,a,d)}catch(e){c.showAlert(\"sendMail: \"+e)}};c.sendSMS=function(b,a){try{utilityController.sendSMS(b,a)}catch(d){c.showAlert(\"sendSMS: \"+d)}};c.pauseAudio=function(b){try{var a=getPID(b);utilityController.pauseAudio(a)}catch(d){c.showAlert(\"pauseAudio: \"+d)}};c.muteAudio=function(b){try{var a=getPID(b);utilityController.muteAudio(a)}catch(d){c.showAlert(\"muteAudio: \"+d)}};c.unMuteAudio=function(b){try{var a=getPID(b);utilityController.unMuteAudio(a)}catch(d){c.showAlert(\"unMuteAudio: \"+d)}};c.isAudioMuted=function(b){try{var a=getPID(b);return utilityController.isAudioMuted(a)}catch(d){c.showAlert(\"isAudioMuted: \"+d)}};c.setAudioVolume=function(b,a){try{var d=getPID(b);utilityController.setAudioVolume(d,a)}catch(e){c.showAlert(\"setAudioVolume: \"+e)}};c.getAudioVolume=function(b){try{var a=getPID(b);return utilityController.getAudioVolume(a)}catch(d){c.showAlert(\"getAudioVolume: \"+d)}};c.seekAudio=function(b,a){try{var d=getPID(b);utilityController.seekAudio(d,a)}catch(e){c.showAlert(\"seekAudio: \"+e)}};c.playAudio=function(b,a){var d=!0,e=!1,h=\"normal\",f=\"normal\",g=!0,j=\"\",n=getPID(a);null!=b&&(j=b);null!=a&&(\"undefined\"!=typeof a.autoplay&&!1===a.autoplay&&(d=!1),\"undefined\"!=typeof a.loop&&!0===a.loop&&(e=!0),\"undefined\"!=typeof a.startStyle&&null!=a.startStyle&&(h=a.startStyle),\"undefined\"!=typeof a.stopStyle&&null!=a.stopStyle&&(f=a.stopStyle),\"fullscreen\"==h&&(g=!0));try{utilityController.playAudio(j,d,g,e,h,f,n)}catch(o){c.showAlert(\"playAudio: \"+o)}};c.pauseVideo=function(b){try{var a=getPID(b);utilityController.pauseVideo(a)}catch(d){c.showAlert(\"pauseVideo: \"+d)}};c.closeVideo=function(b){try{var a=getPID(b);utilityController.closeVideo(a)}catch(d){c.showAlert(\"closeVideo: \"+d)}};c.hideVideo=function(b){try{var a=getPID(b);utilityController.hideVideo(a)}catch(d){c.showAlert(\"hideVideo: \"+d)}};c.showVideo=function(b){try{var a=getPID(b);utilityController.showVideo(a)}catch(d){c.showAlert(\"showVideo: \"+d)}};c.muteVideo=function(b){try{var a=getPID(b);utilityController.muteVideo(a)}catch(d){c.showAlert(\"muteVideo: \"+d)}};c.unMuteVideo=function(b){try{var a=getPID(b);utilityController.unMuteVideo(a)}catch(d){c.showAlert(\"unMuteVideo: \"+d)}};c.seekVideo=function(b,a){try{var d=getPID(b);utilityController.seekVideo(d,a)}catch(e){c.showAlert(\"seekVideo: \"+e)}};c.isVideoMuted=function(b){try{var a=getPID(b);return utilityController.isVideoMuted(a)}catch(d){c.showAlert(\"isVideoMuted: \"+d)}};c.setVideoVolume=function(b,a){try{var d=getPID(b);utilityController.setVideoVolume(d,a)}catch(e){c.showAlert(\"setVideoVolume: \"+e)}};c.getVideoVolume=function(b){try{var a=getPID(b);return utilityController.getVideoVolume(a)}catch(d){c.showAlert(\"getVideoVolume: \"+d)}};c.playVideo=function(b,a){var d=!1,e=!0,f=!0,g=!1,j=-99999,l=-99999,n=-99999,o=-99999,k=\"normal\",m=\"exit\",p=\"\",q=getPID(a);null!=b&&(p=b);if(null!=a){\"undefined\"!=typeof a.audio&&\"muted\"==a.audio&&(d=!0);\"undefined\"!=typeof a.autoplay&&!1===a.autoplay&&(e=!1);\"undefined\"!=typeof a.controls&&!1===a.controls&&(f=!1);\"undefined\"!=typeof a.loop&&!0===a.loop&&(g=!0);if(\"undefined\"!=typeof a.inline&&null!=a.inline&&(j=a.inline.left,l=a.inline.top,\"undefined\"!=typeof a.width&&null!=a.width&&(n=a.width),\"undefined\"!=typeof a.height&&null!=a.height))o=a.height;\"undefined\"!=typeof a.startStyle&&null!=a.startStyle&&(k=a.startStyle);\"undefined\"!=typeof a.stopStyle&&null!=a.stopStyle&&(m=a.stopStyle);\"fullscreen\"==k&&(f=!0)}try{utilityController.playVideo(p,d,e,f,g,j,l,n,o,k,m,q)}catch(r){c.showAlert(\"playVideo: \"+r)}};c.updateToPassbook=function(){c.fireErrorEvent(\"Method not supported\",\"updateToPassbook\");c.log(\"Method not supported\")};c.stringify=function(b){if(\"undefined\"===typeof JSON){var a=\"\",d;if(\"undefined\"==typeof b.length)return c.stringifyArg(b);for(d=0;d<b.length;d++)0<d&&(a+=\",\"),a+=c.stringifyArg(b[d]);return a+\"]\"}return JSON.stringify(b)};c.stringifyArg=function(b){var a,d,e;d=typeof b;a=\"\";if(\"number\"===d||\"boolean\"===d)a+=args;else if(b instanceof Array)a=a+\"[\"+b+\"]\";else if(b instanceof Object){d=!0;a+=\"{\";for(e in b)null!==b[e]&&(d||(a+=\",\"),a=a+'\"'+e+'\":',d=typeof b[e],a=\"number\"===d||\"boolean\"===d?a+b[e]:\"function\"===typeof b[e]?a+'\"\"':b[e]instanceof Object?a+this.stringify(args[i][e]):a+'\"'+b[e]+'\"',d=!1);a+=\"}\"}else b=b.replace(/\\\\/g,\"\\\\\\\\\"),b=b.replace(/\"/g,'\\\\\"'),a=a+'\"'+b+'\"';c.showAlert(\"json:\"+a);return a};getPID=function(b){var a=\"\";null!=b&&(\"undefined\"!=typeof b.id&&null!=b.id)&&(a=b.id);return a};var k,j=function(){window.orientation!==k&&(k=window.orientation,displayController.onOrientationChange())};c.registerOrientationListener=function(){k=window.orientation;window.addEventListener(\"resize\",j,!1);window.addEventListener(\"orientationchange\",j,!1)};c.unRegisterOrientationListener=function(){window.removeEventListener(\"resize\",j,!1);window.removeEventListener(\"orientationchange\",j,!1)}})();");
                IMWebView.this.injectJavaScript("(function(){var c=window.mraid={};c.STATES={LOADING:\"loading\",DEFAULT:\"default\",RESIZED:\"resized\",EXPANDED:\"expanded\",HIDDEN:\"hidden\"};var d=c.EVENTS={READY:\"ready\",ERROR:\"error\",STATECHANGE:\"stateChange\",VIEWABLECHANGE:\"viewableChange\",ORIENTATIONCHANGE:\"orientationChange\",PASSBOOKCHANGE:\"passbookChange\"},i={width:0,height:0},g={width:0,height:0},f={},h={width:0,height:0,useCustomClose:!1,isModal:!0,lockOrientation:!1,orientation:\"\"},l=function(a){this.event=a;this.count=0;var b={};this.add=function(a){var c=String(a);b[c]||(b[c]=a,this.count++)};this.remove=function(a){a=String(a);return b[a]?(b[a]=null,delete b[a],this.count--,!0):!1};this.removeAll=function(){for(var a in b)this.remove(b[a])};this.broadcast=function(a){for(var c in b)b[c].apply({},a)};this.toString=function(){var c=[a,\":\"],d;for(d in b)c.push(\"|\",d,\"|\");return c.join(\"\")}};mraidview.addEventListener(d.READY,function(){e(d.READY)});mraidview.addEventListener(d.STATECHANGE,function(a){e(d.STATECHANGE,a)});mraidview.addEventListener(d.VIEWABLECHANGE,function(a){e(d.VIEWABLECHANGE,a)});mraidview.addEventListener(\"error\",function(a,b){e(d.ERROR,a,b)});mraidview.addEventListener(d.ORIENTATIONCHANGE,function(a){e(d.ORIENTATIONCHANGE,a)});var k=function(a){var b=function(){};b.prototype=a;return new b},e=function(){for(var a=Array(arguments.length),b=0;b<arguments.length;b++)a[b]=arguments[b];b=a.shift();try{f[b]&&f[b].broadcast(a)}catch(c){}},j=function(a){for(var b=0,c=a.length-1;b<a.length&&\" \"==a[b];)b++;for(;c>b&&\" \"==a[c];)c-=1;return a.substring(b,c+1)};c.addEventListener=function(a,b){try{!a||!b?e(d.ERROR,\"Both event and listener are required.\",\"addEventListener\"):d.ERROR==a||d.READY==a||d.STATECHANGE==a||d.VIEWABLECHANGE==a||d.ORIENTATIONCHANGE==a?(f[a]||(f[a]=new l(a)),f[a].add(b)):mraidview.addEventListener(a,b)}catch(c){mraidview.log(c)}};c.useCustomClose=function(a){h.useCustomClose=a;mraidview.useCustomClose(a)};c.close=function(){mraidview.close()};c.getExpandProperties=function(){return h};c.setExpandProperties=function(a){h=a;h.isModal=!0;mraidview.setExpandProperties(h)};c.expand=function(a){mraidview.expand(a)};c.getMaxSize=function(){return k(g)};c.getSize=function(){return k(i)};c.getState=function(){return mraidview.getState()};c.getOrientation=function(){return mraidview.getOrientation()};c.isViewable=function(){return mraidview.isViewable()};c.open=function(a){a?mraidview.open(a):e(d.ERROR,\"URL is required.\",\"open\")};c.removeEventListener=function(a,b){try{if(a){if(b)if(f[a])f[a].remove(b);else{mraidview.removeEventListener(a,b);return}else f[a]&&f[a].removeAll();f[a]&&0==f[a].count&&(f[a]=null,delete f[a])}else e(d.ERROR,\"Must specify an event.\",\"removeEventListener\")}catch(c){mraidview.log(\"removeEventListener\"+c)}};c.resize=function(a,b){null==a||null==b||isNaN(a)||isNaN(b)||0>a||0>b?e(d.ERROR,\"Requested size must be numeric values between 0 and maxSize.\",\"resize\"):a>g.width||b>g.height?e(d.ERROR,\"Request (\"+a+\" x \"+b+\") exceeds maximum allowable size of (\"+g.width+\" x \"+g.height+\")\",\"resize\"):a==i.width&&b==i.height?e(d.ERROR,\"Requested size equals current size.\",\"resize\"):mraidview.resize(a,b)};c.log=function(a){null == a || \"undefined\" == a?e(d.ERROR,\"message is required.\",\"log\"):mraidview.log(a)};c.getVersion=function(){return\"1.0\"};c.getInMobiAIVersion=function(){return 1.2};c.getPlacementType=function(){return mraidview.getPlacementType()};c.asyncPing=function(a){a?mraidview.asyncPing(a):e(d.ERROR,\"URL is required.\",\"asyncPing\")};c.makeCall=function(a){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must provide a number to call.\",\"makeCall\"):mraidview.makeCall(a)};c.sendMail=function(a,b,c){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must specify a recipient.\",\"sendMail\"):mraidview.sendMail(a,b,c)};c.sendSMS=function(a,b){!a||\"string\"!=typeof a||0==j(a).length?e(d.ERROR,\"Request must specify a recipient.\",\"sendSMS\"):mraidview.sendSMS(a,b)};c.playAudio=function(a,b){\"undefined\"==typeof b||null==b?\"string\"==typeof a?mraidview.playAudio(a,null):\"object\"==typeof a?mraidview.playAudio(null,a):mraidview.playAudio(null,null):mraidview.playAudio(a,b)};c.playVideo=function(a,b){\"undefined\"==typeof b||null==b?\"string\"==typeof a?mraidview.playVideo(a,null):\"object\"==typeof a?mraidview.playVideo(null,a):mraidview.playVideo(null,null):mraidview.playVideo(a,b)};c.pauseAudio=function(a){mraidview.pauseAudio(a)};c.muteAudio=function(a){mraidview.muteAudio(a)};c.unMuteAudio=function(a){mraidview.unMuteAudio(a)};c.isAudioMuted=function(a){return mraidview.isAudioMuted(a)};c.setAudioVolume=function(a){var b=a.volume;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid volume\",\"setAudioVolume\"):0>b||100<b?e(d.ERROR,\"Request must specify a valid volume value in the range 0..100\",\"setAudioVolume\"):mraidview.setAudioVolume(a,b)};c.getAudioVolume=function(a){return mraidview.getAudioVolume(a)};c.pauseVideo=function(a){mraidview.pauseVideo(a)};c.closeVideo=function(a){mraidview.closeVideo(a)};c.hideVideo=function(a){mraidview.hideVideo(a)};c.showVideo=function(a){mraidview.showVideo(a)};c.muteVideo=function(a){mraidview.muteVideo(a)};c.unMuteVideo=function(a){mraidview.unMuteVideo(a)};c.isVideoMuted=function(a){return mraidview.isVideoMuted(a)};c.setVideoVolume=function(a){var b=a.volume;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid volume\",\"setVideoVolume\"):0>b||100<b?e(d.ERROR,\"Request must specify a valid volume value in the range 0..100\",\"setVideoVolume\"):mraidview.setVideoVolume(a,b)};c.getVideoVolume=function(a){return mraidview.getVideoVolume(a)};c.seekAudio=function(a){var b=a.time;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid time\",\"seekAudio\"):0!=b?e(d.ERROR,\"Cannot seek audio other than 0\",\"seekAudio\"):mraidview.seekAudio(a,b)};c.seekVideo=function(a){var b=a.time;\"undefined\"==typeof b||null==b?e(d.ERROR,\"Request must specify a valid time\",\"seekVideo\"):0!=b?e(d.ERROR,\"Cannot seek video other than 0\",\"seekVideo\"):mraidview.seekVideo(a,b)};c.openExternal=function(a){mraidview.openExternal(a)};c.updateToPassbook=function(a){mraidview.updateToPassbook(a)};c.getScreenSize=function(){return mraidview.getScreenSize()};c.getCurrentPosition=function(){return mraidview.getCurrentPosition()};c.acceptAction=function(a){mraidview.acceptAction(a)};c.rejectAction=function(a){mraidview.rejectAction(a)}})();");
            }
        }
    };
    /* access modifiers changed from: private */
    public VideoView K;
    /* access modifiers changed from: private */
    public ViewGroup L;
    /* access modifiers changed from: private */
    public FrameLayout M;
    /* access modifiers changed from: private */
    public WebChromeClient.CustomViewCallback N;
    private WebChromeClient O = new WebChromeClient() {
        public boolean onJsAlert(WebView webView, String str, String str2, final JsResult jsResult) {
            IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onJsAlert, " + str2);
            try {
                new AlertDialog.Builder(webView.getContext()).setTitle(str).setMessage(str2).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        jsResult.confirm();
                    }
                }).setCancelable(false).create().show();
                return true;
            } catch (Exception e) {
                return true;
            }
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            IMLog.debug(IMConstants.LOGGING_TAG, "onShowCustomView ******************************");
            try {
                if (view instanceof FrameLayout) {
                    FrameLayout unused = IMWebView.this.M = (FrameLayout) view;
                    WebChromeClient.CustomViewCallback unused2 = IMWebView.this.N = customViewCallback;
                    ViewGroup unused3 = IMWebView.this.L = (ViewGroup) ((FrameLayout) IMWebView.this.W.findViewById(16908290)).getChildAt(0);
                    if (IMWebView.this.M.getFocusedChild() instanceof VideoView) {
                        VideoView unused4 = IMWebView.this.K = (VideoView) IMWebView.this.M.getFocusedChild();
                        IMWebView.this.L.setVisibility(8);
                        IMWebView.this.M.setVisibility(0);
                        IMWebView.this.W.setContentView(IMWebView.this.M);
                        IMWebView.this.K.setOnCompletionListener(IMWebView.this.P);
                        IMWebView.this.K.setOnKeyListener(new View.OnKeyListener() {
                            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                                if (4 != keyEvent.getKeyCode() || keyEvent.getAction() != 0) {
                                    return false;
                                }
                                IMLog.debug(IMConstants.LOGGING_TAG, "Back Button pressed when html5 video is playing");
                                IMWebView.this.K.stopPlayback();
                                IMWebView.this.M.setVisibility(8);
                                IMWebView.this.d();
                                IMWebView.this.W.setContentView(IMWebView.this.L);
                                return true;
                            }
                        });
                        IMWebView.this.K.start();
                    }
                }
            } catch (Exception e) {
            }
        }
    };
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener P = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            mediaPlayer.stop();
            IMWebView.this.M.setVisibility(8);
            IMWebView.this.d();
            IMWebView.this.W.setContentView(IMWebView.this.L);
        }
    };
    private boolean Q;
    private boolean R = true;
    /* access modifiers changed from: private */
    public Message S;
    /* access modifiers changed from: private */
    public Message T;
    private Message U;
    private Activity V;
    /* access modifiers changed from: private */
    public Activity W;
    private Message X;
    private int Y = -5;
    private int Z = -5;
    /* access modifiers changed from: private */
    public WebViewClient aa;
    private boolean b;
    private JSUtilityController c;
    private float d;
    private int e;
    private int f;
    private int g;
    /* access modifiers changed from: private */
    public ViewState h = ViewState.LOADING;
    /* access modifiers changed from: private */
    public IMWebViewListener i;
    public boolean isTablet = false;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l;
    private String m;
    public boolean mIsInterstitialAd = false;
    public Message mMsgOnInterstitialShown;
    public IMWebView mOriginalWebviewForExpandUrl = null;
    private int n;
    /* access modifiers changed from: private */
    public boolean o = false;
    private IMWebView p;
    public int publisherOrientation;
    /* access modifiers changed from: private */
    public boolean q;
    private boolean r = false;
    private boolean s = true;
    private JSController.ExpandProperties t;
    private boolean u = false;
    public boolean useLockOrient;
    private String v;
    /* access modifiers changed from: private */
    public IMAVPlayer w;
    public String webviewUserAgent;
    /* access modifiers changed from: private */
    public IMAVPlayer x;
    private Object y = new Object();
    private AtomicBoolean z = new AtomicBoolean(false);

    public interface IMWebViewListener {
        void onError();

        void onExpand();

        void onExpandClose();

        void onLeaveApplication();
    }

    public enum ViewState {
        LOADING,
        DEFAULT,
        RESIZED,
        EXPANDED,
        EXPANDING,
        HIDDEN
    }

    public IMWebView(Context context, IMWebViewListener iMWebViewListener) {
        super(context);
        this.i = iMWebViewListener;
        e();
    }

    public IMWebView(Context context, IMWebViewListener iMWebViewListener, boolean z2, boolean z3) {
        super(context);
        this.V = (Activity) context;
        this.mIsInterstitialAd = z2;
        this.q = z3;
        if (this.mIsInterstitialAd) {
            setId(IMWEBVIEW_INTERSTITIAL_ID);
        }
        this.i = iMWebViewListener;
        e();
    }

    public void postInjectJavaScript(String str) {
        if (str != null && this.j) {
            if (str.length() < 400) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Injecting JavaScript: " + str);
            }
            Message obtainMessage = this.G.obtainMessage(1025);
            Bundle bundle = new Bundle();
            bundle.putString("injectMessage", str);
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
        }
    }

    public void injectJavaScript(String str) {
        if (str != null && this.j) {
            if (str.length() < 400) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Injecting JavaScript: " + str);
            }
            super.loadUrl("javascript:" + str);
        }
    }

    public void loadUrl(String str) {
        f();
        super.loadUrl(str);
    }

    public void loadData(String str, String str2, String str3) {
        super.loadData(str, str2, str3);
    }

    public void loadDataWithBaseURL(String str, String str2, String str3, String str4, String str5) {
        f();
        super.loadDataWithBaseURL(str, str2, str3, str4, str5);
    }

    public void clearView() {
        a();
        super.clearView();
    }

    private void a() {
        if (this.h == ViewState.EXPANDED) {
            g();
        }
        invalidate();
        this.c.deleteOldAds();
        this.c.stopAllListeners();
        k();
    }

    public IMWebView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        e();
        getContext().obtainStyledAttributes(attributeSet, a).recycle();
    }

    static class a extends Handler {
        private final WeakReference<IMWebView> a;

        public a(IMWebView iMWebView) {
            this.a = new WeakReference<>(iMWebView);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean
         arg types: [com.inmobi.androidsdk.ai.container.IMWebView, int]
         candidates:
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, android.os.Message):android.os.Message
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, android.view.ViewGroup):android.view.ViewGroup
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, android.webkit.WebChromeClient$CustomViewCallback):android.webkit.WebChromeClient$CustomViewCallback
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, android.widget.FrameLayout):android.widget.FrameLayout
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, android.widget.VideoView):android.widget.VideoView
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, com.inmobi.androidsdk.ai.controller.util.IMAVPlayer):com.inmobi.androidsdk.ai.controller.util.IMAVPlayer
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, java.lang.String):com.inmobi.androidsdk.ai.controller.util.IMAVPlayer
          com.inmobi.androidsdk.ai.container.IMWebView.a(android.os.Bundle, android.app.Activity):void
          com.inmobi.androidsdk.ai.container.IMWebView.a(android.view.ViewGroup, boolean):void
          com.inmobi.androidsdk.ai.container.IMWebView.a(android.webkit.WebView, java.lang.String):void
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, android.os.Bundle):void
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, com.inmobi.androidsdk.ai.container.IMWebView$ViewState):void
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.controller.util.IMAVPlayer, com.inmobi.androidsdk.ai.controller.JSController$Dimensions):void
          com.inmobi.androidsdk.ai.container.IMWebView.a(com.inmobi.androidsdk.ai.container.IMWebView, boolean):boolean */
        public void handleMessage(Message message) {
            String str;
            String str2;
            String str3;
            IMWebView iMWebView = this.a.get();
            if (iMWebView != null) {
                IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView->handleMessage: msg: " + message);
                Bundle data = message.getData();
                switch (message.what) {
                    case 1001:
                        switch (iMWebView.h) {
                            case EXPANDING:
                            case EXPANDED:
                                iMWebView.g();
                                boolean unused = iMWebView.o = false;
                                break;
                            case HIDDEN:
                                iMWebView.injectJavaScript("window.mraidview.fireErrorEvent(\"Current state is not expanded or default\", \"close\")");
                                break;
                            case DEFAULT:
                                if (!iMWebView.mIsInterstitialAd) {
                                    iMWebView.hide();
                                    break;
                                } else {
                                    iMWebView.o();
                                    break;
                                }
                        }
                    case 1002:
                        iMWebView.setVisibility(4);
                        iMWebView.a(ViewState.HIDDEN);
                        break;
                    case 1003:
                        iMWebView.injectJavaScript("window.mraidview.fireChangeEvent({ state: 'default' });");
                        iMWebView.setVisibility(0);
                        break;
                    case 1004:
                        if (iMWebView.h == ViewState.EXPANDING) {
                            iMWebView.a(data);
                            break;
                        }
                        break;
                    case 1005:
                        if (iMWebView.i != null) {
                            iMWebView.i.onExpandClose();
                            break;
                        }
                        break;
                    case 1006:
                        try {
                            iMWebView.b(data, iMWebView.W);
                            break;
                        } catch (Exception e) {
                            IMLog.debug(IMConstants.LOGGING_TAG, "Play video failed ", e);
                            break;
                        }
                    case 1007:
                        try {
                            iMWebView.a(data, iMWebView.W);
                            break;
                        } catch (Exception e2) {
                            IMLog.debug(IMConstants.LOGGING_TAG, "Play audio failed ", e2);
                            break;
                        }
                    case 1008:
                        String string = data.getString("message");
                        iMWebView.injectJavaScript("window.mraidview.fireErrorEvent(\"" + string + "\", \"" + data.getString(AdViewCore.ACTION_KEY) + "\")");
                        break;
                    case 1009:
                        iMWebView.h();
                        break;
                    case 1010:
                        IMAVPlayer iMAVPlayer = (IMAVPlayer) iMWebView.B.get(data.getString("aplayerref"));
                        if (iMAVPlayer != null) {
                            iMAVPlayer.pause();
                            break;
                        }
                        break;
                    case 1011:
                        IMAVPlayer a2 = iMWebView.e(data.getString("pid"));
                        if (a2 == null) {
                            str3 = "window.mraidview.fireErrorEvent(\"Invalid property ID\", \"pauseVideo\")";
                        } else if (a2.getState() != IMAVPlayer.playerState.PLAYING) {
                            str3 = "window.mraidview.fireErrorEvent(\"Invalid player state\", \"pauseVideo\")";
                        } else {
                            a2.pause();
                            return;
                        }
                        iMWebView.injectJavaScript(str3);
                        break;
                    case 1012:
                        ((IMAVPlayer) message.obj).releasePlayer(false);
                        break;
                    case 1013:
                        String string2 = data.getString("pid");
                        IMAVPlayer a3 = iMWebView.e(string2);
                        if (a3 == null) {
                            str2 = "window.mraidview.fireErrorEvent(\"Invalid property ID\", \"hideVideo\")";
                        } else if (a3.getState() == IMAVPlayer.playerState.RELEASED) {
                            str2 = "window.mraidview.fireErrorEvent(\"Invalid player state\", \"hideVideo\")";
                        } else {
                            iMWebView.A.put(string2, a3);
                            a3.hide();
                            a3.releasePlayer(false);
                            return;
                        }
                        iMWebView.injectJavaScript(str2);
                        break;
                    case 1014:
                        String string3 = data.getString("pid");
                        IMAVPlayer a4 = iMWebView.e(string3);
                        if (a4 == null) {
                            str = "window.mraidview.fireErrorEvent(\"Invalid property ID\", \"showVideo\")";
                        } else if (a4.getState() != IMAVPlayer.playerState.RELEASED) {
                            str = "window.mraidview.fireErrorEvent(\"Invalid player state\", \"showVideo\")";
                        } else if (iMWebView.w == null || iMWebView.w.getPropertyID().equalsIgnoreCase(string3)) {
                            iMWebView.A.remove(string3);
                            Bundle bundle = new Bundle();
                            bundle.putString(AdViewCore.EXPAND_URL, a4.getMediaURL());
                            bundle.putParcelable(AdViewCore.DIMENSIONS, a4.getPlayDimensions());
                            bundle.putParcelable(AdViewCore.PLAYER_PROPERTIES, a4.getProperties());
                            iMWebView.b(bundle, iMWebView.W);
                            return;
                        } else {
                            str = "window.mraidview.fireErrorEvent(\"Show failed. There is already a video playing\", \"showVideo\")";
                        }
                        iMWebView.injectJavaScript(str);
                        break;
                    case 1015:
                        ((IMAVPlayer) message.obj).mute();
                        break;
                    case 1016:
                        ((IMAVPlayer) message.obj).unMute();
                        break;
                    case 1017:
                        ((IMAVPlayer) message.obj).setVolume(data.getInt("volume"));
                        break;
                    case 1018:
                        ((IMAVPlayer) message.obj).seekPlayer(data.getInt("seek") * 1000);
                        break;
                    case 1019:
                        IMAVPlayer iMAVPlayer2 = (IMAVPlayer) iMWebView.B.get(data.getString("aplayerref"));
                        if (iMAVPlayer2 != null) {
                            iMAVPlayer2.mute();
                            break;
                        }
                        break;
                    case 1020:
                        IMAVPlayer iMAVPlayer3 = (IMAVPlayer) iMWebView.B.get(data.getString("aplayerref"));
                        if (iMAVPlayer3 != null) {
                            iMAVPlayer3.unMute();
                            break;
                        }
                        break;
                    case 1021:
                        IMAVPlayer iMAVPlayer4 = (IMAVPlayer) iMWebView.B.get(data.getString("aplayerref"));
                        if (iMAVPlayer4 != null) {
                            iMAVPlayer4.setVolume(data.getInt("vol"));
                            break;
                        }
                        break;
                    case 1022:
                        ((IMAVPlayer) message.obj).seekPlayer(data.getInt("seekaudio") * 1000);
                        break;
                    case 1023:
                        iMWebView.p();
                        break;
                    case 1024:
                        iMWebView.b(data.getString(AdViewCore.EXPAND_URL));
                        break;
                    case 1025:
                        String string4 = data.getString("injectMessage");
                        if (string4 != null) {
                            iMWebView.loadUrl("javascript:" + string4);
                            break;
                        }
                        break;
                    case 1026:
                        iMWebView.m();
                        break;
                    case 1027:
                        iMWebView.c();
                        break;
                }
            }
            super.handleMessage(message);
        }
    }

    private FrameLayout a(JSController.ExpandProperties expandProperties) {
        FrameLayout frameLayout = (FrameLayout) getRootView().findViewById(16908290);
        b();
        FrameLayout frameLayout2 = new FrameLayout(getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
        frameLayout2.setId(435);
        frameLayout2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        frameLayout2.setPadding(expandProperties.x, expandProperties.y, 0, 0);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(expandProperties.width, expandProperties.height);
        RelativeLayout relativeLayout = new RelativeLayout(getContext());
        relativeLayout.setId(RELATIVELAYOUT_ID);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(expandProperties.width, expandProperties.height);
        if (this.o) {
            relativeLayout.addView(this.p, layoutParams3);
        } else {
            relativeLayout.addView(this, layoutParams3);
        }
        a(relativeLayout, expandProperties.useCustomClose);
        frameLayout2.addView(relativeLayout, layoutParams2);
        frameLayout.addView(frameLayout2, layoutParams);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        return frameLayout2;
    }

    private void b() {
        try {
            ViewGroup viewGroup = (ViewGroup) getParent();
            int childCount = viewGroup.getChildCount();
            int i2 = 0;
            while (i2 < childCount && viewGroup.getChildAt(i2) != this) {
                i2++;
            }
            this.g = i2;
            FrameLayout frameLayout = new FrameLayout(getContext());
            frameLayout.setId(PLACEHOLDER_ID);
            viewGroup.addView(frameLayout, i2, new ViewGroup.LayoutParams(getWidth(), getHeight()));
            viewGroup.removeView(this);
            this.I = viewGroup;
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception in replaceByPlaceHolder ", e2);
        }
    }

    private void a(ViewGroup viewGroup, boolean z2) {
        IMCustomView iMCustomView;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (this.d * 50.0f), (int) (this.d * 50.0f));
        layoutParams.addRule(11);
        if (z2) {
            iMCustomView = new IMCustomView(getContext(), this.d, IMCustomView.SwitchIconType.CLOSE_TRANSPARENT);
        } else {
            iMCustomView = new IMCustomView(getContext(), this.d, IMCustomView.SwitchIconType.CLOSE_BUTTON);
        }
        viewGroup.addView(iMCustomView, layoutParams);
        iMCustomView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                IMWebView.userInitiatedClose = true;
                IMWebView.this.close();
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(android.os.Bundle r7) {
        /*
            r6 = this;
            java.lang.String r0 = "expand_url"
            java.lang.String r0 = r7.getString(r0)     // Catch:{ Exception -> 0x00c7 }
            boolean r1 = android.webkit.URLUtil.isValidUrl(r0)     // Catch:{ Exception -> 0x00c7 }
            if (r1 == 0) goto L_0x00c3
            r1 = 1
            r6.o = r1     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = new com.inmobi.androidsdk.ai.container.IMWebView     // Catch:{ Exception -> 0x00c7 }
            android.content.Context r2 = r6.getContext()     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView$IMWebViewListener r3 = r6.i     // Catch:{ Exception -> 0x00c7 }
            r4 = 0
            r5 = 0
            r1.<init>(r2, r3, r4, r5)     // Catch:{ Exception -> 0x00c7 }
            r6.p = r1     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            r2 = 1
            r1.o = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            int r2 = r6.publisherOrientation     // Catch:{ Exception -> 0x00c7 }
            r1.publisherOrientation = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            boolean r2 = r6.s     // Catch:{ Exception -> 0x00c7 }
            r1.s = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r2 = new com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties     // Catch:{ Exception -> 0x00c7 }
            r2.<init>()     // Catch:{ Exception -> 0x00c7 }
            r1.t = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r1.t     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r2 = r6.t     // Catch:{ Exception -> 0x00c7 }
            int r2 = r2.x     // Catch:{ Exception -> 0x00c7 }
            r1.x = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r1.t     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r2 = r6.t     // Catch:{ Exception -> 0x00c7 }
            int r2 = r2.y     // Catch:{ Exception -> 0x00c7 }
            r1.y = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r1.t     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r2 = r6.t     // Catch:{ Exception -> 0x00c7 }
            int r2 = r2.currentX     // Catch:{ Exception -> 0x00c7 }
            r1.currentX = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r1.t     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r2 = r6.t     // Catch:{ Exception -> 0x00c7 }
            int r2 = r2.currentY     // Catch:{ Exception -> 0x00c7 }
            r1.currentY = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            boolean r2 = r6.useLockOrient     // Catch:{ Exception -> 0x00c7 }
            r1.useLockOrient = r2     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            r1.mOriginalWebviewForExpandUrl = r6     // Catch:{ Exception -> 0x00c7 }
        L_0x006a:
            android.app.Activity r1 = r6.V     // Catch:{ Exception -> 0x00c7 }
            r6.setExpandedActivity(r1)     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r6.t     // Catch:{ Exception -> 0x00c7 }
            boolean r1 = r1.lockOrientation     // Catch:{ Exception -> 0x00c7 }
            if (r1 != 0) goto L_0x0080
            r1 = -5
            r6.Y = r1     // Catch:{ Exception -> 0x00c7 }
            r1 = -5
            r6.Z = r1     // Catch:{ Exception -> 0x00c7 }
            java.lang.String r1 = "window.mraidview.registerOrientationListener()"
            r6.injectJavaScript(r1)     // Catch:{ Exception -> 0x00c7 }
        L_0x0080:
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r6.t     // Catch:{ Exception -> 0x00c7 }
            android.widget.FrameLayout r1 = r6.a(r1)     // Catch:{ Exception -> 0x00c7 }
            r2 = 0
            r1.setBackgroundColor(r2)     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r1 = r6.t     // Catch:{ Exception -> 0x00c7 }
            int r1 = r1.width     // Catch:{ Exception -> 0x00c7 }
            r6.n = r1     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            if (r1 == 0) goto L_0x009c
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            com.inmobi.androidsdk.ai.controller.JSController$ExpandProperties r2 = r6.t     // Catch:{ Exception -> 0x00c7 }
            int r2 = r2.width     // Catch:{ Exception -> 0x00c7 }
            r1.n = r2     // Catch:{ Exception -> 0x00c7 }
        L_0x009c:
            com.inmobi.androidsdk.ai.container.IMWebView$ViewState r1 = com.inmobi.androidsdk.ai.container.IMWebView.ViewState.EXPANDED     // Catch:{ Exception -> 0x00c7 }
            r6.a(r1)     // Catch:{ Exception -> 0x00c7 }
            java.lang.Object r1 = r6.y     // Catch:{ Exception -> 0x00c7 }
            monitor-enter(r1)     // Catch:{ Exception -> 0x00c7 }
            java.util.concurrent.atomic.AtomicBoolean r2 = r6.z     // Catch:{ all -> 0x00e6 }
            r3 = 0
            r2.set(r3)     // Catch:{ all -> 0x00e6 }
            java.lang.Object r2 = r6.y     // Catch:{ all -> 0x00e6 }
            r2.notifyAll()     // Catch:{ all -> 0x00e6 }
            monitor-exit(r1)     // Catch:{ all -> 0x00e6 }
            boolean r1 = r6.o     // Catch:{ Exception -> 0x00c7 }
            if (r1 == 0) goto L_0x00b9
            com.inmobi.androidsdk.ai.container.IMWebView r1 = r6.p     // Catch:{ Exception -> 0x00c7 }
            r1.loadUrl(r0)     // Catch:{ Exception -> 0x00c7 }
        L_0x00b9:
            com.inmobi.androidsdk.ai.container.IMWebView$IMWebViewListener r0 = r6.i     // Catch:{ Exception -> 0x00c7 }
            if (r0 == 0) goto L_0x00c2
            com.inmobi.androidsdk.ai.container.IMWebView$IMWebViewListener r0 = r6.i     // Catch:{ Exception -> 0x00c7 }
            r0.onExpand()     // Catch:{ Exception -> 0x00c7 }
        L_0x00c2:
            return
        L_0x00c3:
            r1 = 0
            r6.o = r1     // Catch:{ Exception -> 0x00c7 }
            goto L_0x006a
        L_0x00c7:
            r0 = move-exception
            java.lang.String r1 = "InMobiAndroidSDK_3.6.0"
            java.lang.String r2 = "Exception in doexpand "
            com.inmobi.commons.internal.IMLog.debug(r1, r2, r0)
            com.inmobi.androidsdk.ai.container.IMWebView$ViewState r0 = com.inmobi.androidsdk.ai.container.IMWebView.ViewState.DEFAULT
            r6.h = r0
            java.lang.Object r1 = r6.y
            monitor-enter(r1)
            java.util.concurrent.atomic.AtomicBoolean r0 = r6.z     // Catch:{ all -> 0x00e3 }
            r2 = 0
            r0.set(r2)     // Catch:{ all -> 0x00e3 }
            java.lang.Object r0 = r6.y     // Catch:{ all -> 0x00e3 }
            r0.notifyAll()     // Catch:{ all -> 0x00e3 }
            monitor-exit(r1)     // Catch:{ all -> 0x00e3 }
            goto L_0x00c2
        L_0x00e3:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00e3 }
            throw r0
        L_0x00e6:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00e6 }
            throw r0     // Catch:{ Exception -> 0x00c7 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.ai.container.IMWebView.a(android.os.Bundle):void");
    }

    public void setExpandedActivity(Activity activity) {
        this.W = activity;
    }

    public Activity getExpandedActivity() {
        return this.W;
    }

    public void lockExpandOrientation(JSController.ExpandProperties expandProperties) {
        try {
            this.D = this.V.getRequestedOrientation();
            if (expandProperties.lockOrientation) {
                this.useLockOrient = true;
                int integerCurrentRotation = getIntegerCurrentRotation();
                if (expandProperties.orientation.equalsIgnoreCase("portrait")) {
                    if (b(this.V) || b(integerCurrentRotation)) {
                        this.V.setRequestedOrientation(IMWrapperFunctions.getParamPortraitOrientation(integerCurrentRotation));
                    }
                } else if (b(this.V) || c(integerCurrentRotation)) {
                    this.V.setRequestedOrientation(IMWrapperFunctions.getParamLandscapeOrientation(integerCurrentRotation));
                }
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception handling the orientation ", e2);
        }
    }

    public void onOrientationEventChange() {
        this.G.sendEmptyMessage(1027);
    }

    /* access modifiers changed from: private */
    public void c() {
        int i2;
        FrameLayout frameLayout;
        RelativeLayout relativeLayout;
        FrameLayout frameLayout2;
        JSController.Dimensions playDimensions;
        int i3;
        JSController.Dimensions playDimensions2;
        int i4 = 0;
        this.Y = InternalSDKUtil.getDisplayRotation(this.H);
        if (this.Y != -5 && this.Y != this.Z) {
            try {
                IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> SensorEventListener, It came inside the listener" + this.Y);
                this.Z = this.Y;
                int i5 = this.V.getResources().getDisplayMetrics().widthPixels;
                int i6 = this.V.getResources().getDisplayMetrics().heightPixels;
                if (this.isTablet) {
                    this.Y++;
                    if (this.Y > 3) {
                        this.Y = 0;
                    }
                    IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> SensorEventListener, It is a tablet" + this.Y);
                }
                if (!this.mIsInterstitialAd) {
                    if (this.Y == 0 || this.Y == 2) {
                        this.t.actualWidthRequested = this.t.portraitWidthRequested;
                        this.t.actualHeightRequested = this.t.portraitHeightRequested;
                        if (i5 > i6) {
                            i2 = i6;
                        }
                        i2 = i5;
                        i5 = i6;
                    } else {
                        this.t.actualWidthRequested = this.t.portraitHeightRequested;
                        this.t.actualHeightRequested = this.t.portraitWidthRequested;
                        if (i5 < i6) {
                            i2 = i6;
                        }
                        i2 = i5;
                        i5 = i6;
                    }
                    if (this.t.zeroWidthHeight) {
                        this.t.actualWidthRequested = i2;
                        this.t.actualHeightRequested = i5;
                    }
                    int i7 = i5 - this.t.topStuff;
                    if (this.o) {
                        frameLayout = (FrameLayout) this.p.getRootView().findViewById(16908290);
                    } else {
                        frameLayout = (FrameLayout) getRootView().findViewById(16908290);
                    }
                    if (frameLayout != null) {
                        FrameLayout frameLayout3 = (FrameLayout) frameLayout.findViewById(435);
                        relativeLayout = (RelativeLayout) frameLayout3.findViewById(RELATIVELAYOUT_ID);
                        frameLayout2 = frameLayout3;
                    } else {
                        relativeLayout = null;
                        frameLayout2 = null;
                    }
                    if (this.Y == 0 || this.Y == 2) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> SensorEventListener, It is the case from landscape to portrait");
                        this.t.width = Math.min(i2, this.t.actualWidthRequested);
                        this.t.height = Math.min(this.t.actualHeightRequested, i7 - this.t.y);
                        int i8 = i2 - (this.t.x + this.t.width);
                        if (i8 < 0) {
                            int i9 = i8 + this.t.x;
                            if (i9 < 0) {
                                this.t.width = i9 + this.t.width;
                            } else {
                                i4 = i9;
                            }
                        } else {
                            i4 = this.t.x;
                        }
                        if (frameLayout2 != null) {
                            frameLayout2.setPadding(i4, this.t.y, 0, 0);
                            this.t.currentX = i4;
                            this.t.currentY = this.t.y;
                            IMAVPlayer iMAVPlayer = this.w;
                            if (this.o) {
                                this.p.t.currentX = this.t.currentX;
                                this.p.t.currentY = this.t.currentY;
                                iMAVPlayer = this.p.w;
                            }
                            if (iMAVPlayer != null && iMAVPlayer.isInlineVideo() && (playDimensions = iMAVPlayer.getPlayDimensions()) != null && playDimensions.x >= 0 && playDimensions.y >= 0) {
                                ((FrameLayout) iMAVPlayer.getBackGroundLayout()).setPadding(playDimensions.x + i4, playDimensions.y + this.t.y, 0, 0);
                            }
                            relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(this.t.width, this.t.height));
                            if (this.o) {
                                this.p.setLayoutParams(new RelativeLayout.LayoutParams(this.t.width, this.t.height));
                                this.p.n = this.t.width;
                            } else {
                                setLayoutParams(new RelativeLayout.LayoutParams(this.t.width, this.t.height));
                                this.n = this.t.width;
                            }
                            IMLog.debug(IMConstants.LOGGING_TAG, "Dimensions: {" + i4 + " ," + this.t.y + " ," + this.t.width + " ," + this.t.height + "}");
                        }
                    } else if (this.Y == 1 || this.Y == 3) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> SensorEventListener, It is the case from portrait to landscape");
                        this.t.height = Math.min(i7, this.t.actualHeightRequested);
                        this.t.width = Math.min(this.t.actualWidthRequested, i2 - this.t.x);
                        int i10 = i7 - (this.t.y + this.t.height);
                        if (i10 < 0) {
                            int i11 = i10 + this.t.y;
                            if (i11 < 0) {
                                this.t.height = i11 + this.t.height;
                                i3 = 0;
                            } else {
                                i3 = i11;
                            }
                        } else {
                            i3 = this.t.y;
                        }
                        if (frameLayout2 != null) {
                            frameLayout2.setPadding(this.t.x, i3, 0, 0);
                            this.t.currentX = this.t.x;
                            this.t.currentY = i3;
                            IMAVPlayer iMAVPlayer2 = this.w;
                            if (this.o) {
                                this.p.t.currentX = this.t.currentX;
                                this.p.t.currentY = this.t.currentY;
                                iMAVPlayer2 = this.p.w;
                            }
                            if (iMAVPlayer2 != null && iMAVPlayer2.isInlineVideo() && (playDimensions2 = iMAVPlayer2.getPlayDimensions()) != null && playDimensions2.x >= 0 && playDimensions2.y >= 0) {
                                ((FrameLayout) iMAVPlayer2.getBackGroundLayout()).setPadding(this.t.x + playDimensions2.x, playDimensions2.y + i3, 0, 0);
                            }
                            relativeLayout.setLayoutParams(new FrameLayout.LayoutParams(this.t.width, this.t.height));
                            if (this.o) {
                                this.p.setLayoutParams(new RelativeLayout.LayoutParams(this.t.width, this.t.height));
                                this.p.n = this.t.width;
                            } else {
                                setLayoutParams(new RelativeLayout.LayoutParams(this.t.width, this.t.height));
                                this.n = this.t.width;
                            }
                            IMLog.debug(IMConstants.LOGGING_TAG, "Dimensions: {" + this.t.x + " ," + i3 + " ," + this.t.width + " ," + this.t.height + "}");
                        }
                    }
                }
                a(this.Y);
            } catch (Exception e2) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Exception while changing the container coordinates or width while orientation change ", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(WebView webView, String str) {
        String substring;
        IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> Search query requested:" + str);
        try {
            webView.stopLoading();
            int indexOf = str.indexOf("?");
            if (indexOf > 0 && (substring = str.substring(indexOf)) != null && this.m != null) {
                String str2 = this.m;
                System.out.println(str2 + substring);
                Bundle bundle = new Bundle();
                bundle.putString("finaltargeturl", str2 + substring);
                this.X.setData(bundle);
                this.X.sendToTarget();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        try {
            if (this.K != null) {
                this.K.setVisibility(8);
                this.M.removeView(this.K);
                this.K = null;
                this.M.setVisibility(8);
                this.N.onCustomViewHidden();
                this.L.setVisibility(0);
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error removing html5 video");
        }
    }

    private void a(int i2) {
        String str = "window.mraidview.fireOrientationChangeEvent(" + getCurrentRotation(i2) + ");";
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> " + str);
        injectJavaScript(str);
        if (this.o) {
            this.p.injectJavaScript(str);
        }
    }

    public int getIntegerCurrentRotation() {
        int displayRotation = InternalSDKUtil.getDisplayRotation(((WindowManager) this.W.getSystemService("window")).getDefaultDisplay());
        if (InternalSDKUtil.getWhetherTablet(displayRotation, this.W.getResources().getDisplayMetrics().widthPixels, this.W.getResources().getDisplayMetrics().heightPixels)) {
            displayRotation++;
            if (displayRotation > 3) {
                displayRotation = 0;
            }
            this.isTablet = true;
        }
        return displayRotation;
    }

    public String getCurrentRotation(int i2) {
        switch (i2) {
            case 0:
                return "0";
            case 1:
                return "90";
            case 2:
                return "180";
            case 3:
                return "270";
            default:
                return "-1";
        }
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    private void e() {
        this.W = this.V;
        userInitiatedClose = false;
        setScrollContainer(false);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        this.webviewUserAgent = getSettings().getUserAgentString();
        setBackgroundColor(0);
        ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(new DisplayMetrics());
        this.d = this.V.getResources().getDisplayMetrics().density;
        this.b = false;
        getSettings().setJavaScriptEnabled(true);
        this.c = new JSUtilityController(this, getContext());
        addJavascriptInterface(this.c, "utilityController");
        setWebViewClient(this.J);
        setWebChromeClient(this.O);
        this.H = ((WindowManager) this.V.getSystemService("window")).getDefaultDisplay();
        this.n = this.V.getResources().getDisplayMetrics().widthPixels;
    }

    private void f() {
        IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> initStates");
        this.h = ViewState.LOADING;
        this.F.set(false);
        this.R = false;
    }

    public void acceptAction(String str) {
    }

    public void rejectAction(String str) {
    }

    public void sendasyncPing(String str) {
        this.c.asyncPing(str);
    }

    public void addJavascriptObject(Object obj, String str) {
        addJavascriptInterface(obj, str);
    }

    public void reinitializeExpandProperties() {
        this.c.reinitializeExpandProperties();
    }

    public void deinit() {
        close();
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.h.compareTo((Enum) ViewState.DEFAULT) != 0) {
            synchronized (this.y) {
                this.z.set(false);
                this.y.notifyAll();
            }
            if (!this.s && this.publisherOrientation == -1) {
                this.s = true;
            }
            j();
            releaseAllPlayers();
            this.E.clear();
            this.G.sendEmptyMessage(1005);
            setVisibility(0);
            this.o = false;
            if (this.useLockOrient) {
                this.V.setRequestedOrientation(this.D);
            }
            injectJavaScript("window.mraidview.unRegisterOrientationListener()");
            a(ViewState.DEFAULT);
        }
    }

    /* access modifiers changed from: protected */
    public void closeOpened(View view) {
        ((ViewGroup) ((Activity) getContext()).getWindow().getDecorView()).removeView(view);
        requestLayout();
    }

    public String getState() {
        return this.h.toString().toLowerCase();
    }

    public ViewState getStateVariable() {
        return this.h;
    }

    /* access modifiers changed from: private */
    public void a(ViewState viewState) {
        this.h = viewState;
        if (viewState != ViewState.EXPANDING) {
            injectJavaScript("window.mraidview.fireStateChangeEvent('" + getState() + "');");
        }
    }

    public boolean isBusy() {
        return this.k;
    }

    public void close() {
        if (!this.G.hasMessages(1001)) {
            this.G.sendEmptyMessage(1001);
        }
    }

    public void hide() {
        this.G.sendEmptyMessage(1002);
    }

    public void show() {
        this.G.sendEmptyMessage(1003);
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        this.R = z2;
        injectJavaScript("window.mraidview.fireViewableChangeEvent(" + isViewable() + ");");
    }

    public boolean isViewable() {
        return this.R;
    }

    public String getPlacementType() {
        if (this.mIsInterstitialAd) {
            return "interstitial";
        }
        return "inline";
    }

    public void setCustomClose(boolean z2) {
        this.r = z2;
        if (this.mIsInterstitialAd) {
            this.G.sendMessage(this.G.obtainMessage(1009));
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        IMCustomView iMCustomView = (IMCustomView) this.W.findViewById(INT_CLOSE_BUTTON);
        if (iMCustomView == null) {
            return;
        }
        if (this.r) {
            iMCustomView.setSwitchInt(IMCustomView.SwitchIconType.CLOSE_TRANSPARENT);
            iMCustomView.invalidate();
            return;
        }
        iMCustomView.setSwitchInt(IMCustomView.SwitchIconType.CLOSE_BUTTON);
        iMCustomView.invalidate();
    }

    private boolean i() {
        return this.r;
    }

    public void expand(String str, JSController.ExpandProperties expandProperties) {
        a(ViewState.EXPANDING);
        this.o = false;
        this.z.set(true);
        Message obtainMessage = this.G.obtainMessage(1004);
        Bundle bundle = new Bundle();
        bundle.putString(AdViewCore.EXPAND_URL, str);
        obtainMessage.setData(bundle);
        this.t = expandProperties;
        IMLog.debug(IMConstants.LOGGING_TAG, "Dimensions: {" + this.t.x + " ," + this.t.y + " ," + this.t.width + " ," + this.t.height + "}");
        this.s = this.t.lockOrientation;
        this.G.sendMessage(obtainMessage);
    }

    public void openURL(String str) {
        if (!isViewable()) {
            raiseError("Cannot open URL.Ad is not viewable yet", "openURL");
            return;
        }
        doHidePlayers();
        Message obtainMessage = this.G.obtainMessage(1024);
        Bundle bundle = new Bundle();
        bundle.putString(AdViewCore.EXPAND_URL, str);
        obtainMessage.setData(bundle);
        this.G.sendMessage(obtainMessage);
    }

    /* access modifiers changed from: private */
    public String a(String str) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            BasicHttpContext basicHttpContext = new BasicHttpContext();
            if (defaultHttpClient.execute(new HttpGet(str), basicHttpContext).getStatusLine().getStatusCode() == 200) {
                HttpUriRequest httpUriRequest = (HttpUriRequest) basicHttpContext.getAttribute("http.request");
                return httpUriRequest.getURI().isAbsolute() ? httpUriRequest.getURI().toString() : ((HttpHost) basicHttpContext.getAttribute("http.target_host")).toURI() + httpUriRequest.getURI();
            }
        } catch (Exception e2) {
            IMLog.internal(IMConstants.LOGGING_TAG, "Exception getting final redirect url", e2);
        }
        return str;
    }

    /* access modifiers changed from: private */
    public void b(final String str) {
        try {
            if (!str.startsWith("http://") || str.contains("play.google.com") || str.contains("market.android.com") || str.contains("market%3A%2F%2F")) {
                new Thread(new Runnable() {
                    public void run() {
                        String c = IMWebView.this.a(str);
                        if (c != null) {
                            Intent intent = new Intent();
                            intent.setAction("android.intent.action.VIEW");
                            intent.setData(Uri.parse(c));
                            intent.addFlags(268435456);
                            IMWebView.this.W.startActivity(intent);
                            if (IMWebView.this.i != null) {
                                IMWebView.this.i.onLeaveApplication();
                            }
                        }
                    }
                }).start();
            } else if (!URLUtil.isValidUrl(str)) {
                raiseError("Invalid url", "open");
            } else {
                Intent intent = new Intent(this.W, IMBrowserActivity.class);
                IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> open:" + str);
                intent.putExtra(IMBrowserActivity.EXTRA_URL, str);
                IMBrowserActivity.setWebViewListener(this.i);
                this.W.startActivity(intent);
            }
        } catch (ActivityNotFoundException e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Failed to perform mraid Open");
        } catch (Exception e3) {
            raiseError("Invalid url", "open");
        }
    }

    private void j() {
        FrameLayout frameLayout;
        try {
            if (this.o) {
                frameLayout = (FrameLayout) this.p.getRootView().findViewById(16908290);
            } else {
                frameLayout = (FrameLayout) getRootView().findViewById(16908290);
            }
            FrameLayout frameLayout2 = (FrameLayout) this.V.findViewById(PLACEHOLDER_ID);
            FrameLayout frameLayout3 = (FrameLayout) frameLayout.findViewById(435);
            IMLog.debug(IMConstants.LOGGING_TAG, "PlaceHolder ID: " + frameLayout2 + " Bg ID: " + frameLayout3);
            if (this.o) {
                this.p.releaseAllPlayers();
            }
            if (frameLayout3 != null) {
                if (this.o) {
                    ((ViewGroup) frameLayout3.getChildAt(0)).removeView(this.p);
                    this.p = null;
                } else {
                    ((ViewGroup) frameLayout3.getChildAt(0)).removeView(this);
                }
                frameLayout.removeView(frameLayout3);
            }
            k();
            if (frameLayout2 != null) {
                this.I.removeView(frameLayout2);
                this.I.addView(this, this.g);
            }
            this.I.invalidate();
        } catch (Exception e2) {
            Exception exc = e2;
            try {
                ViewGroup viewGroup = (ViewGroup) getParent();
                viewGroup.removeAllViews();
                ((ViewGroup) viewGroup.getParent()).removeAllViews();
            } catch (Exception e3) {
            }
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception while closing the expanded Ad", exc);
        }
    }

    private void k() {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (this.Q) {
            layoutParams.height = this.e;
            layoutParams.width = this.f;
        }
        setVisibility(0);
        requestLayout();
    }

    public boolean isPageFinished() {
        return this.b;
    }

    public String getSize() {
        return "{ width: " + ((int) (((float) getWidth()) / this.d)) + ", " + "height: " + ((int) (((float) getHeight()) / this.d)) + "}";
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onAttachedToWindow");
        if (!this.Q) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            this.e = layoutParams.height;
            this.f = layoutParams.width;
            this.Q = true;
        }
        super.onAttachedToWindow();
    }

    public void raiseError(String str, String str2) {
        Message obtainMessage = this.G.obtainMessage(1008);
        Bundle bundle = new Bundle();
        bundle.putString("message", str);
        bundle.putString(AdViewCore.ACTION_KEY, str2);
        obtainMessage.setData(bundle);
        this.G.sendMessage(obtainMessage);
    }

    public boolean isExpanded() {
        return this.h == ViewState.EXPANDED;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        IMLog.debug(IMConstants.LOGGING_TAG, "IMWebView-> onDetatchedFromWindow");
        this.c.stopAllListeners();
        this.E.clear();
        if (this.mIsInterstitialAd && !this.q) {
            l();
        }
        super.onDetachedFromWindow();
    }

    /* access modifiers changed from: private */
    public void l() {
        userInitiatedClose = true;
        isInterstitialDisplayed.set(false);
        deinit();
    }

    private boolean a(String str, String str2, Activity activity) {
        if (!this.B.isEmpty()) {
            this.x = this.B.get(str);
            if (this.x == null) {
                if (this.B.size() > 4) {
                    raiseError("Too many audio players", "playAudio");
                    return false;
                }
                this.x = new IMAVPlayer(activity, this);
            } else if (!this.x.getMediaURL().equals(str2) && str2.length() != 0) {
                this.x.releasePlayer(false);
                this.B.remove(str);
                this.x = new IMAVPlayer(activity, this);
            } else if (this.x.getState() == IMAVPlayer.playerState.PLAYING) {
                return false;
            } else {
                if (this.x.getState() == IMAVPlayer.playerState.PAUSED) {
                    this.x.start();
                    return false;
                }
                JSController.PlayerProperties properties = this.x.getProperties();
                String mediaURL = this.x.getMediaURL();
                this.x.releasePlayer(false);
                this.B.remove(str);
                this.x = new IMAVPlayer(activity, this);
                this.x.setPlayData(properties, mediaURL);
            }
        } else {
            this.x = new IMAVPlayer(activity, this);
        }
        return true;
    }

    private synchronized IMAVPlayer c(String str) {
        IMAVPlayer iMAVPlayer;
        iMAVPlayer = null;
        if (this.x != null && this.x.getPropertyID().equalsIgnoreCase(str)) {
            iMAVPlayer = this.x;
        } else if (!this.B.isEmpty() && this.B.containsKey(str)) {
            iMAVPlayer = this.B.get(str);
        }
        return iMAVPlayer;
    }

    public void releaseAllPlayers() {
        if (this.w != null) {
            this.A.put(this.w.getPropertyID(), this.w);
        }
        try {
            for (Map.Entry<String, IMAVPlayer> value : this.A.entrySet()) {
                ((IMAVPlayer) value.getValue()).releasePlayer(userInitiatedClose);
            }
        } catch (Exception e2) {
        }
        this.A.clear();
        this.w = null;
        try {
            for (Map.Entry<String, IMAVPlayer> value2 : this.B.entrySet()) {
                ((IMAVPlayer) value2.getValue()).releasePlayer(userInitiatedClose);
            }
        } catch (Exception e3) {
        }
        userInitiatedClose = false;
        this.B.clear();
        this.x = null;
    }

    private IMAVPlayer d(String str) {
        if (!this.A.isEmpty()) {
            return this.A.get(str);
        }
        return null;
    }

    private boolean b(String str, String str2, Activity activity) {
        if ((str2.length() == 0 || URLUtil.isValidUrl(str2)) && (str2.length() != 0 || this.A.containsKey(str))) {
            if (this.w != null) {
                this.w.releasePlayer(false);
            }
            IMAVPlayer d2 = d(str);
            if (d2 == null) {
                this.w = new IMAVPlayer(activity, this);
            } else {
                this.w = d2;
            }
            if (str2.length() == 0) {
                this.w.setPlayData(d2.getProperties(), d2.getMediaURL());
                this.w.setPlayDimensions(d2.getPlayDimensions());
            }
            this.A.remove(str);
            return true;
        }
        raiseError("Request must specify a valid URL", "playVideo");
        return false;
    }

    private boolean a(String str, String str2, Activity activity, JSController.Dimensions dimensions) {
        if (this.w == null || !str.equalsIgnoreCase(this.w.getPropertyID())) {
            return b(str, str2, activity);
        }
        IMAVPlayer.playerState state = this.w.getState();
        if (str.equalsIgnoreCase(this.w.getPropertyID())) {
            String mediaURL = this.w.getMediaURL();
            if (str2.length() == 0 || str2.equalsIgnoreCase(mediaURL)) {
                switch (state) {
                    case PAUSED:
                        this.w.start();
                        a(this.w, dimensions);
                        return false;
                    case PLAYING:
                        a(this.w, dimensions);
                        return false;
                    case COMPLETED:
                        if (this.w.getProperties().doLoop()) {
                            return false;
                        }
                        this.w.start();
                        return false;
                    default:
                        return false;
                }
            } else if (!URLUtil.isValidUrl(str2)) {
                raiseError("Request must specify a valid URL", "playVideo");
                return false;
            } else {
                this.w.releasePlayer(false);
                this.w = new IMAVPlayer(activity, this);
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public synchronized IMAVPlayer e(String str) {
        IMAVPlayer iMAVPlayer;
        iMAVPlayer = null;
        if (this.w != null && this.w.getPropertyID().equalsIgnoreCase(str)) {
            iMAVPlayer = this.w;
        } else if (!this.A.isEmpty() && this.A.containsKey(str)) {
            iMAVPlayer = this.A.get(str);
        }
        return iMAVPlayer;
    }

    public void setAdUnitData(boolean z2, String str) {
        this.l = z2;
        this.m = str;
    }

    public void requestOnPageFinishedCallback(Message message) {
        this.S = message;
    }

    public void pageFinishedCallbackForAdCreativeTesting(Message message) {
        this.T = message;
    }

    public void requestOnInterstitialShown(Message message) {
        this.mMsgOnInterstitialShown = message;
    }

    public void requestOnInterstitialClosed(Message message) {
        this.U = message;
    }

    public void requestOnSearchAdClicked(Message message) {
        this.X = message;
    }

    public void setActivity(Activity activity) {
        this.V = activity;
    }

    /* access modifiers changed from: private */
    public void a(Bundle bundle, Activity activity) {
        final JSController.PlayerProperties playerProperties = (JSController.PlayerProperties) bundle.getParcelable(AdViewCore.PLAYER_PROPERTIES);
        String string = bundle.getString(AdViewCore.EXPAND_URL);
        if (string == null) {
            string = "";
        }
        if (a(playerProperties.id, string, activity)) {
            if ((string.length() != 0 && !URLUtil.isValidUrl(string)) || (string.length() == 0 && !this.B.containsKey(playerProperties.id))) {
                raiseError("Request must specify a valid URL", "playAudio");
            } else if (this.x != null) {
                if (string.length() != 0) {
                    this.x.setPlayData(playerProperties, string);
                }
                this.B.put(playerProperties.id, this.x);
                FrameLayout frameLayout = (FrameLayout) activity.findViewById(16908290);
                if (playerProperties.isFullScreen()) {
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
                    layoutParams.addRule(13);
                    this.x.setLayoutParams(layoutParams);
                    RelativeLayout relativeLayout = new RelativeLayout(activity);
                    relativeLayout.setOnTouchListener(new View.OnTouchListener() {
                        public boolean onTouch(View view, MotionEvent motionEvent) {
                            return true;
                        }
                    });
                    relativeLayout.setBackgroundColor(-16777216);
                    frameLayout.addView(relativeLayout, new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent()));
                    relativeLayout.addView(this.x);
                    this.x.setBackGroundLayout(relativeLayout);
                    this.x.requestFocus();
                    this.x.setOnKeyListener(new View.OnKeyListener() {
                        public boolean onKey(View view, int i, KeyEvent keyEvent) {
                            if (4 != keyEvent.getKeyCode() || keyEvent.getAction() != 0) {
                                return false;
                            }
                            IMLog.debug(IMConstants.LOGGING_TAG, "Back button pressed while fullscreen audio was playing");
                            IMWebView.this.x.releasePlayer(true);
                            return true;
                        }
                    });
                } else {
                    this.x.setLayoutParams(new ViewGroup.LayoutParams(1, 1));
                    frameLayout.addView(this.x);
                }
                this.x.setListener(new IMAVPlayerListener() {
                    public void onPrepared(IMAVPlayer iMAVPlayer) {
                    }

                    public void onError(IMAVPlayer iMAVPlayer) {
                        onComplete(iMAVPlayer);
                    }

                    public void onComplete(IMAVPlayer iMAVPlayer) {
                        try {
                            if (playerProperties.isFullScreen()) {
                                ViewGroup viewGroup = (ViewGroup) iMAVPlayer.getBackGroundLayout().getParent();
                                if (viewGroup != null) {
                                    viewGroup.removeView(iMAVPlayer.getBackGroundLayout());
                                    return;
                                }
                                return;
                            }
                            ViewGroup viewGroup2 = (ViewGroup) iMAVPlayer.getParent();
                            if (viewGroup2 != null) {
                                viewGroup2.removeView(iMAVPlayer);
                            }
                        } catch (Exception e) {
                            IMLog.debug(IMConstants.LOGGING_TAG, "Problem removing the audio relativelayout", e);
                        }
                    }
                });
                this.x.play();
            }
        }
    }

    public void playAudio(String str, boolean z2, boolean z3, boolean z4, String str2, String str3, String str4) {
        synchronized (this.y) {
            if (this.z.get()) {
                try {
                    this.y.wait();
                } catch (InterruptedException e2) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "mutex failed ", e2);
                }
            }
        }
        if (!this.mIsInterstitialAd && this.h != ViewState.EXPANDED) {
            raiseError("Cannot play audio.Ad is not in an expanded state", "playAudio");
        } else if (!isViewable()) {
            raiseError("Cannot play audio.Ad is not viewable yet", "playAudio");
        } else {
            JSController.PlayerProperties playerProperties = new JSController.PlayerProperties();
            playerProperties.setProperties(false, z2, z3, z4, str2, str3, str4);
            Bundle bundle = new Bundle();
            bundle.putString(AdViewCore.EXPAND_URL, str);
            bundle.putParcelable(AdViewCore.PLAYER_PROPERTIES, playerProperties);
            Message obtainMessage = this.G.obtainMessage(1007);
            obtainMessage.setData(bundle);
            this.G.sendMessage(obtainMessage);
        }
    }

    public void pauseAudio(String str) {
        IMAVPlayer c2 = c(str);
        if (c2 == null) {
            raiseError("Invalid property ID", "pauseAudio");
        } else if (c2.getState() != IMAVPlayer.playerState.PLAYING) {
            raiseError("Invalid player state", "pauseAudio");
        } else if (c2.isPlaying()) {
            Message obtainMessage = this.G.obtainMessage(1010);
            Bundle bundle = new Bundle();
            bundle.putString("aplayerref", str);
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
        }
    }

    public void muteAudio(String str) {
        IMAVPlayer c2 = c(str);
        if (c2 == null) {
            raiseError("Invalid property ID", "muteAudio");
        } else if (c2.getState() == IMAVPlayer.playerState.RELEASED) {
            raiseError("Invalid player state", "muteAudio");
        } else {
            Message obtainMessage = this.G.obtainMessage(1019);
            Bundle bundle = new Bundle();
            bundle.putString("aplayerref", str);
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
        }
    }

    public void unMuteAudio(String str) {
        IMAVPlayer c2 = c(str);
        if (c2 == null) {
            raiseError("Invalid property ID", "unmuteAudio");
        } else if (c2.getState() == IMAVPlayer.playerState.RELEASED) {
            raiseError("Invalid player state", "unmuteAudio");
        } else {
            Message obtainMessage = this.G.obtainMessage(1020);
            Bundle bundle = new Bundle();
            bundle.putString("aplayerref", str);
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
        }
    }

    public boolean isAudioMuted(String str) {
        IMAVPlayer c2 = c(str);
        if (c2 != null) {
            return c2.isMediaMuted();
        }
        raiseError("Invalid property ID", "isAudioMuted");
        return false;
    }

    public void setAudioVolume(String str, int i2) {
        if (c(str) == null) {
            raiseError("Invalid property ID", "setAudioVolume");
            return;
        }
        Message obtainMessage = this.G.obtainMessage(1021);
        Bundle bundle = new Bundle();
        bundle.putInt("vol", i2);
        bundle.putString("aplayerref", str);
        obtainMessage.setData(bundle);
        obtainMessage.sendToTarget();
    }

    public int getAudioVolume(String str) {
        IMAVPlayer c2 = c(str);
        if (c2 != null) {
            return c2.getVolume();
        }
        raiseError("Invalid property ID", "getAudioVolume");
        return -1;
    }

    public void seekAudio(String str, int i2) {
        IMAVPlayer c2 = c(str);
        if (c2 == null) {
            raiseError("Invalid property ID", "seekAudio");
        } else if (c2.getState() == IMAVPlayer.playerState.RELEASED) {
            raiseError("Invalid player state", "seekAudio");
        } else {
            Message obtainMessage = this.G.obtainMessage(1022);
            Bundle bundle = new Bundle();
            bundle.putInt("seekaudio", i2);
            obtainMessage.setData(bundle);
            obtainMessage.obj = c2;
            obtainMessage.sendToTarget();
        }
    }

    /* access modifiers changed from: private */
    public void b(Bundle bundle, Activity activity) {
        JSController.PlayerProperties playerProperties;
        JSController.PlayerProperties playerProperties2 = (JSController.PlayerProperties) bundle.getParcelable(AdViewCore.PLAYER_PROPERTIES);
        JSController.Dimensions dimensions = (JSController.Dimensions) bundle.getParcelable(AdViewCore.DIMENSIONS);
        IMLog.debug(IMConstants.LOGGING_TAG, "Final dimensions: " + dimensions);
        String string = bundle.getString(AdViewCore.EXPAND_URL);
        if (a(playerProperties2.id, string, activity, dimensions)) {
            this.k = true;
            if (string.length() == 0) {
                JSController.PlayerProperties properties = this.w.getProperties();
                dimensions = this.w.getPlayDimensions();
                this.w.getMediaURL();
                playerProperties = properties;
            } else {
                this.w.setPlayData(playerProperties2, string);
                this.w.setPlayDimensions(dimensions);
                playerProperties = playerProperties2;
            }
            FrameLayout frameLayout = (FrameLayout) activity.findViewById(16908290);
            if (playerProperties.isFullScreen()) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
                layoutParams.addRule(13);
                this.w.setLayoutParams(layoutParams);
                RelativeLayout relativeLayout = new RelativeLayout(activity);
                relativeLayout.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        return true;
                    }
                });
                relativeLayout.setBackgroundColor(-16777216);
                frameLayout.addView(relativeLayout, new FrameLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent()));
                relativeLayout.addView(this.w);
                this.w.setBackGroundLayout(relativeLayout);
                this.w.requestFocus();
                this.w.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View view, int i, KeyEvent keyEvent) {
                        if (4 != keyEvent.getKeyCode() || keyEvent.getAction() != 0) {
                            return false;
                        }
                        IMLog.debug(IMConstants.LOGGING_TAG, "Back pressed while fullscreen video is playing");
                        IMWebView.this.w.releasePlayer(true);
                        return true;
                    }
                });
            } else {
                this.w.setLayoutParams(new FrameLayout.LayoutParams(dimensions.width, dimensions.height));
                FrameLayout frameLayout2 = new FrameLayout(activity);
                if (this.t == null) {
                    frameLayout2.setPadding(dimensions.x, dimensions.y, 0, 0);
                } else {
                    frameLayout2.setPadding(dimensions.x + this.t.currentX, dimensions.y + this.t.currentY, 0, 0);
                }
                frameLayout.addView(frameLayout2, new FrameLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent()));
                this.w.setBackGroundLayout(frameLayout2);
                frameLayout2.addView(this.w);
            }
            this.w.setListener(new IMAVPlayerListener() {
                public void onPrepared(IMAVPlayer iMAVPlayer) {
                }

                public void onError(IMAVPlayer iMAVPlayer) {
                    onComplete(iMAVPlayer);
                }

                public void onComplete(IMAVPlayer iMAVPlayer) {
                    boolean unused = IMWebView.this.k = false;
                    try {
                        ViewGroup viewGroup = (ViewGroup) iMAVPlayer.getBackGroundLayout().getParent();
                        if (viewGroup != null) {
                            viewGroup.removeView(iMAVPlayer.getBackGroundLayout());
                        }
                        iMAVPlayer.setBackGroundLayout(null);
                    } catch (Exception e) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "Problem removing the video framelayout or relativelayout depending on video startstyle", e);
                    }
                    synchronized (this) {
                        if (IMWebView.this.w != null && iMAVPlayer.getPropertyID().equalsIgnoreCase(IMWebView.this.w.getPropertyID())) {
                            IMAVPlayer unused2 = IMWebView.this.w = (IMAVPlayer) null;
                        }
                    }
                }
            });
            this.w.play();
        }
    }

    public void playVideo(String str, boolean z2, boolean z3, boolean z4, boolean z5, JSController.Dimensions dimensions, String str2, String str3, String str4) {
        synchronized (this.y) {
            if (this.z.get()) {
                try {
                    this.y.wait();
                } catch (InterruptedException e2) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "mutex failed ", e2);
                }
            }
        }
        if (!this.mIsInterstitialAd && this.h != ViewState.EXPANDED) {
            raiseError("Cannot play video.Ad is not in an expanded state", "playVideo");
        } else if (!isViewable()) {
            raiseError("Cannot play video.Ad is not viewable yet", "playVideo");
        } else if (this.A.isEmpty() || this.A.size() != 5 || this.A.containsKey(str4)) {
            Message obtainMessage = this.G.obtainMessage(1006);
            JSController.PlayerProperties playerProperties = new JSController.PlayerProperties();
            playerProperties.setProperties(z2, z3, z4, z5, str2, str3, str4);
            Bundle bundle = new Bundle();
            bundle.putString(AdViewCore.EXPAND_URL, str);
            bundle.putParcelable(AdViewCore.PLAYER_PROPERTIES, playerProperties);
            IMLog.debug(IMConstants.LOGGING_TAG, "Before validation dimension: (" + dimensions.x + ", " + dimensions.y + ", " + dimensions.width + ", " + dimensions.height + ")");
            a(dimensions);
            IMLog.debug(IMConstants.LOGGING_TAG, "After validation dimension: (" + dimensions.x + ", " + dimensions.y + ", " + dimensions.width + ", " + dimensions.height + ")");
            bundle.putParcelable(AdViewCore.DIMENSIONS, dimensions);
            obtainMessage.setData(bundle);
            this.G.sendMessage(obtainMessage);
        } else {
            raiseError("Player Error. Exceeding permissible limit for saved play instances", "playVideo");
        }
    }

    public void pauseVideo(String str) {
        Message obtainMessage = this.G.obtainMessage(1011);
        Bundle bundle = new Bundle();
        bundle.putString("pid", str);
        obtainMessage.setData(bundle);
        this.G.sendMessage(obtainMessage);
    }

    public void closeVideo(String str) {
        IMAVPlayer e2 = e(str);
        if (e2 == null) {
            raiseError("Invalid property ID", "closeVideo");
        } else if (e2.getState() == IMAVPlayer.playerState.RELEASED) {
            raiseError("Invalid player state", "closeVideo");
        } else {
            this.A.remove(str);
            Message obtainMessage = this.G.obtainMessage(1012);
            obtainMessage.obj = e2;
            this.G.sendMessage(obtainMessage);
        }
    }

    public void hideVideo(String str) {
        Message obtainMessage = this.G.obtainMessage(1013);
        Bundle bundle = new Bundle();
        bundle.putString("pid", str);
        obtainMessage.setData(bundle);
        this.G.sendMessage(obtainMessage);
    }

    public void showVideo(String str) {
        Message obtainMessage = this.G.obtainMessage(1014);
        Bundle bundle = new Bundle();
        bundle.putString("pid", str);
        obtainMessage.setData(bundle);
        this.G.sendMessage(obtainMessage);
    }

    public void muteVideo(String str) {
        IMAVPlayer e2 = e(str);
        if (e2 == null) {
            raiseError("Invalid property ID", "muteVideo");
        } else if (e2.getState() == IMAVPlayer.playerState.RELEASED || e2.getState() == IMAVPlayer.playerState.INIT) {
            raiseError("Invalid player state", "muteVideo");
        } else {
            Message obtainMessage = this.G.obtainMessage(1015);
            obtainMessage.obj = e2;
            this.G.sendMessage(obtainMessage);
        }
    }

    public void unMuteVideo(String str) {
        IMAVPlayer e2 = e(str);
        if (e2 == null) {
            raiseError("Invalid property ID", "unMuteVideo");
        } else if (e2.getState() == IMAVPlayer.playerState.RELEASED || e2.getState() == IMAVPlayer.playerState.INIT) {
            raiseError("Invalid player state", "unMuteVideo");
        } else {
            Message obtainMessage = this.G.obtainMessage(1016);
            obtainMessage.obj = e2;
            this.G.sendMessage(obtainMessage);
        }
    }

    public void seekVideo(String str, int i2) {
        IMAVPlayer e2 = e(str);
        if (e2 == null) {
            raiseError("Invalid property ID", "seekVideo");
        } else if (e2.getState() == IMAVPlayer.playerState.RELEASED || e2.getState() == IMAVPlayer.playerState.INIT) {
            raiseError("Invalid player state", "seekVideo");
        } else {
            Message obtainMessage = this.G.obtainMessage(1018);
            Bundle bundle = new Bundle();
            bundle.putInt("seek", i2);
            obtainMessage.setData(bundle);
            obtainMessage.obj = e2;
            this.G.sendMessage(obtainMessage);
        }
    }

    public boolean isVideoMuted(String str) {
        IMAVPlayer e2 = e(str);
        if (e2 != null) {
            return e2.isMediaMuted();
        }
        raiseError("Invalid property ID", "isVideoMuted");
        return false;
    }

    public void setVideoVolume(String str, int i2) {
        IMAVPlayer e2 = e(str);
        if (e2 == null) {
            raiseError("Invalid property ID", "setVideoVolume");
        } else if (e2.getState() == IMAVPlayer.playerState.RELEASED) {
            raiseError("Invalid player state", "setVideoVolume");
        } else {
            Message obtainMessage = this.G.obtainMessage(1017);
            Bundle bundle = new Bundle();
            bundle.putInt("volume", i2);
            obtainMessage.setData(bundle);
            obtainMessage.obj = e2;
            this.G.sendMessage(obtainMessage);
        }
    }

    public int getVideoVolume(String str) {
        IMAVPlayer e2 = e(str);
        if (e2 != null) {
            return e2.getVolume();
        }
        raiseError("Invalid property ID", "getVideoVolume");
        return -1;
    }

    private void a(JSController.Dimensions dimensions) {
        dimensions.width = (int) (((float) dimensions.width) * this.d);
        dimensions.height = (int) (((float) dimensions.height) * this.d);
        dimensions.x = (int) (((float) dimensions.x) * this.d);
        dimensions.y = (int) (((float) dimensions.y) * this.d);
        int i2 = (int) (this.d * 50.0f);
        int i3 = this.n - ((int) (this.d * 50.0f));
        boolean z2 = false;
        if (dimensions.width <= 0 || dimensions.height <= 0) {
            dimensions.width = 1;
            dimensions.height = 1;
            z2 = true;
        }
        int i4 = getContext().getResources().getDisplayMetrics().widthPixels;
        int i5 = getContext().getResources().getDisplayMetrics().heightPixels;
        if (dimensions.width > i4) {
            dimensions.width = IMWrapperFunctions.getParamFillParent();
        }
        if (dimensions.height > i5) {
            dimensions.height = IMWrapperFunctions.getParamFillParent();
        }
        if (!z2 && dimensions.x + dimensions.width > i3 && dimensions.x < i3 && dimensions.y < i2 && dimensions.y + dimensions.height > i2) {
            dimensions.y = i2;
        }
    }

    public void changeContentAreaForInterstitials(boolean z2) {
        IMCustomView iMCustomView;
        int i2;
        try {
            this.C = this.V.getRequestedOrientation();
            m();
            FrameLayout frameLayout = (FrameLayout) this.V.findViewById(16908290);
            RelativeLayout relativeLayout = new RelativeLayout(getContext());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
            layoutParams.addRule(10);
            setFocusable(true);
            setFocusableInTouchMode(true);
            relativeLayout.addView(this, layoutParams);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) (this.d * 50.0f), (int) (this.d * 50.0f));
            if (!i()) {
                iMCustomView = new IMCustomView(getContext(), this.d, IMCustomView.SwitchIconType.CLOSE_BUTTON);
            } else {
                iMCustomView = new IMCustomView(getContext(), this.d, IMCustomView.SwitchIconType.CLOSE_TRANSPARENT);
            }
            layoutParams2.addRule(11);
            iMCustomView.setId(INT_CLOSE_BUTTON);
            relativeLayout.addView(iMCustomView, layoutParams2);
            iMCustomView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    IMWebView.this.l();
                }
            });
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
            relativeLayout.setId(INT_BACKGROUND_ID);
            relativeLayout.setBackgroundColor(z2 ? 0 : -16777216);
            frameLayout.addView(relativeLayout, layoutParams3);
            if (z2) {
                i2 = 0;
            } else {
                i2 = -16777216;
            }
            setBackgroundColor(i2);
            requestFocus();
            setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int i, KeyEvent keyEvent) {
                    if (4 != keyEvent.getKeyCode() || keyEvent.getAction() != 0) {
                        return false;
                    }
                    IMLog.debug(IMConstants.LOGGING_TAG, "Back Button pressed while Interstitial ad is in active state ");
                    IMWebView.this.l();
                    return true;
                }
            });
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case 0:
                            view.requestFocus();
                            return false;
                        case 1:
                            view.requestFocus();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            isInterstitialDisplayed.set(true);
            if (this.mMsgOnInterstitialShown != null) {
                this.mMsgOnInterstitialShown.sendToTarget();
                this.mMsgOnInterstitialShown = null;
            }
            a(true);
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Failed showing interstitial ad", e2);
        }
    }

    public void setExpandPropertiesForInterstitial(boolean z2, boolean z3, String str) {
        setCustomClose(z2);
        this.v = str;
        this.u = z3;
        if (this.q) {
            n();
        }
        if (isViewable() && this.mIsInterstitialAd) {
            this.G.sendEmptyMessage(1026);
        }
    }

    private boolean b(int i2) {
        return i2 == 0 || i2 == 2;
    }

    private boolean c(int i2) {
        return i2 == 1 || i2 == 3;
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.u) {
            int integerCurrentRotation = getIntegerCurrentRotation();
            if (this.v.equalsIgnoreCase("portrait")) {
                if (b(integerCurrentRotation)) {
                    this.V.setRequestedOrientation(IMWrapperFunctions.getParamPortraitOrientation(integerCurrentRotation));
                } else if (b(this.V)) {
                    if (this.w != null && this.w.isInlineVideo() && !this.w.getState().equals(IMAVPlayer.playerState.RELEASED)) {
                        this.w.releasePlayer(false);
                    }
                    this.V.setRequestedOrientation(IMWrapperFunctions.getParamPortraitOrientation(integerCurrentRotation));
                }
            } else if (c(integerCurrentRotation)) {
                this.V.setRequestedOrientation(IMWrapperFunctions.getParamLandscapeOrientation(integerCurrentRotation));
            } else if (b(this.V)) {
                if (this.w != null && this.w.isInlineVideo() && !this.w.getState().equals(IMAVPlayer.playerState.RELEASED)) {
                    this.w.releasePlayer(false);
                }
                this.V.setRequestedOrientation(IMWrapperFunctions.getParamLandscapeOrientation(integerCurrentRotation));
            }
        } else {
            this.Y = -5;
            this.Z = -5;
            injectJavaScript("window.mraidview.registerOrientationListener()");
        }
    }

    private void n() {
        try {
            int integerCurrentRotation = getIntegerCurrentRotation();
            if (!this.u) {
                return;
            }
            if (this.v.equals("portrait")) {
                this.V.setRequestedOrientation(IMWrapperFunctions.getParamPortraitOrientation(integerCurrentRotation));
            } else if (this.v.equals("landscape")) {
                this.V.setRequestedOrientation(IMWrapperFunctions.getParamLandscapeOrientation(integerCurrentRotation));
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void o() {
        try {
            this.mIsInterstitialAd = false;
            this.V.setRequestedOrientation(this.C);
            releaseAllPlayers();
            FrameLayout frameLayout = (FrameLayout) this.V.findViewById(16908290);
            RelativeLayout relativeLayout = (RelativeLayout) frameLayout.findViewById(INT_BACKGROUND_ID);
            if (relativeLayout != null) {
                relativeLayout.removeView(this);
                frameLayout.removeView(relativeLayout);
            }
            if (this.U != null) {
                this.U.sendToTarget();
                this.U = null;
            }
            injectJavaScript("window.mraidview.unRegisterOrientationListener()");
            a(ViewState.HIDDEN);
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Failed to close the interstitial ad", e2);
        }
    }

    public void setExternalWebViewClient(WebViewClient webViewClient) {
        this.aa = webViewClient;
    }

    private void a(IMAVPlayer iMAVPlayer, JSController.Dimensions dimensions) {
        int i2 = (int) (-99999.0f * this.d);
        if (iMAVPlayer.isInlineVideo() && dimensions.x != i2 && dimensions.y != i2) {
            iMAVPlayer.setLayoutParams(new FrameLayout.LayoutParams(dimensions.width, dimensions.height));
            FrameLayout frameLayout = (FrameLayout) iMAVPlayer.getBackGroundLayout();
            if (this.t == null) {
                frameLayout.setPadding(dimensions.x, dimensions.y, 0, 0);
            } else {
                frameLayout.setPadding(dimensions.x + this.t.currentX, dimensions.y + this.t.currentY, 0, 0);
            }
        }
    }

    public void doHidePlayers() {
        this.G.sendEmptyMessage(1023);
    }

    public void fireOnLeaveApplication() {
        doHidePlayers();
        if (this.i != null) {
            this.i.onLeaveApplication();
        }
    }

    /* access modifiers changed from: private */
    public void p() {
        if (!(this.w == null || this.w.getState() == IMAVPlayer.playerState.RELEASED)) {
            this.A.put(this.w.getPropertyID(), this.w);
            this.w.hide();
            this.w.releasePlayer(false);
        }
        for (Map.Entry next : this.B.entrySet()) {
            IMAVPlayer iMAVPlayer = (IMAVPlayer) next.getValue();
            switch (iMAVPlayer.getState()) {
                case PLAYING:
                    iMAVPlayer.pause();
                    break;
                case INIT:
                    iMAVPlayer.releasePlayer(false);
                    this.B.remove(next.getKey());
                    break;
            }
        }
    }

    private int a(Activity activity) {
        ResolveInfo resolveInfo;
        Iterator<ResolveInfo> it = activity.getPackageManager().queryIntentActivities(new Intent(activity, activity.getClass()), 65536).iterator();
        while (true) {
            if (!it.hasNext()) {
                resolveInfo = null;
                break;
            }
            resolveInfo = it.next();
            if (resolveInfo.activityInfo.name.contentEquals(activity.getClass().getName())) {
                break;
            }
        }
        return resolveInfo.activityInfo.configChanges;
    }

    private boolean b(Activity activity) {
        boolean z2;
        boolean z3;
        int i2 = Build.VERSION.SDK_INT;
        int a2 = a(activity);
        if ((a2 & 16) == 0 || (a2 & 32) == 0 || (a2 & 128) == 0) {
            z2 = false;
        } else {
            z2 = true;
        }
        if (i2 < 13 || !((a2 & 1024) == 0 || (a2 & 2048) == 0)) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (!z2 || !z3) {
            return false;
        }
        return true;
    }

    public void cancelLoad() {
        this.F.set(true);
    }
}
