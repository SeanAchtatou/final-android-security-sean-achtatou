package com.inmobi.androidsdk.ai.controller.util;

public final class IMConstants {
    public static final String APP_TRACKER_URL = "http://ma.inmobi.com/downloads/trackerV1";
    public static final String BASE_URL = "http://www.inmobi.com";
    public static final String BUILD_VERSION = "3.6.0";
    public static final int HTTP_SUCCESS_CODE = 200;
    public static final String INMOBI_SDK_RELEASE_VERSION = "3.6.0";
    public static final String LOGGING_TAG = "InMobiAndroidSDK_3.6.0";
    public static final boolean QA_MODE = false;
}
