package com.inmobi.androidsdk.ai.controller.util;

public interface IMAVPlayerListener {
    void onComplete(IMAVPlayer iMAVPlayer);

    void onError(IMAVPlayer iMAVPlayer);

    void onPrepared(IMAVPlayer iMAVPlayer);
}
