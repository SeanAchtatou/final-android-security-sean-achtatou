package com.inmobi.androidsdk.ai.controller;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.JSController;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.internal.IMLog;
import com.inmobi.commons.internal.InternalSDKUtil;
import org.json.JSONObject;

public class JSDisplayController extends JSController {
    private WindowManager a;
    private float b = ((Activity) this.mContext).getResources().getDisplayMetrics().density;

    public JSDisplayController(IMWebView iMWebView, Context context) {
        super(iMWebView, context);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.a = (WindowManager) context.getSystemService("window");
        this.a.getDefaultDisplay().getMetrics(displayMetrics);
    }

    public void open(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> open: url: " + str);
        this.imWebView.openURL(str);
    }

    public String getState() {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> getState");
        return this.imWebView.getState();
    }

    private JSController.ExpandProperties a(JSController.ExpandProperties expandProperties) {
        Display defaultDisplay = this.a.getDefaultDisplay();
        int i = ((Activity) this.mContext).getResources().getDisplayMetrics().widthPixels;
        int i2 = ((Activity) this.mContext).getResources().getDisplayMetrics().heightPixels;
        View findViewById = ((Activity) this.mContext).getWindow().findViewById(16908290);
        expandProperties.topStuff = findViewById.getTop();
        expandProperties.bottomStuff = i2 - findViewById.getBottom();
        int displayRotation = InternalSDKUtil.getDisplayRotation(defaultDisplay);
        if (InternalSDKUtil.getWhetherTablet(displayRotation, i, i2)) {
            displayRotation++;
            if (displayRotation > 3) {
                displayRotation = 0;
            }
            this.imWebView.isTablet = true;
        }
        int i3 = displayRotation;
        IMLog.debug(IMConstants.LOGGING_TAG, "Device current rotation: " + i3);
        IMLog.debug(IMConstants.LOGGING_TAG, "Density of device: " + this.b);
        expandProperties.width = (int) (((float) expandProperties.width) * this.b);
        expandProperties.height = (int) (((float) expandProperties.height) * this.b);
        expandProperties.x = (int) (((float) expandProperties.x) * this.b);
        expandProperties.y = (int) (((float) expandProperties.y) * this.b);
        expandProperties.currentX = 0;
        expandProperties.currentY = 0;
        this.imWebView.publisherOrientation = ((Activity) this.imWebView.getContext()).getRequestedOrientation();
        if (i3 == 0 || i3 == 2) {
            expandProperties.rotationAtExpand = "portrait";
        } else {
            expandProperties.rotationAtExpand = "landscape";
        }
        if (expandProperties.height <= 0 || expandProperties.width <= 0) {
            expandProperties.height = i2;
            expandProperties.width = i;
            expandProperties.zeroWidthHeight = true;
        }
        if (i3 == 0 || i3 == 2) {
            expandProperties.portraitWidthRequested = expandProperties.width;
            expandProperties.portraitHeightRequested = expandProperties.height;
        } else {
            expandProperties.portraitWidthRequested = expandProperties.height;
            expandProperties.portraitHeightRequested = expandProperties.width;
        }
        IMLog.debug(IMConstants.LOGGING_TAG, "Device Width: " + i + " Device height: " + i2);
        int i4 = i2 - expandProperties.topStuff;
        if (expandProperties.width > i) {
            expandProperties.width = i;
        }
        if (expandProperties.height > i4) {
            expandProperties.height = i4;
        }
        int[] iArr = new int[2];
        this.imWebView.getLocationOnScreen(iArr);
        if (expandProperties.x < 0) {
            expandProperties.x = iArr[0];
        }
        if (expandProperties.y < 0) {
            expandProperties.y = iArr[1] - expandProperties.topStuff;
            IMLog.debug(IMConstants.LOGGING_TAG, "topStuff: " + expandProperties.topStuff + " ,bottomStuff: " + expandProperties.bottomStuff);
        }
        IMLog.debug(IMConstants.LOGGING_TAG, "loc 0: " + iArr[0] + " loc 1: " + iArr[1]);
        int i5 = i - (expandProperties.x + expandProperties.width);
        if (i5 < 0) {
            expandProperties.x = i5 + expandProperties.x;
            if (expandProperties.x < 0) {
                expandProperties.width += expandProperties.x;
                expandProperties.x = 0;
            }
        }
        int i6 = i4 - (expandProperties.y + expandProperties.height);
        if (i6 < 0) {
            expandProperties.y = i6 + expandProperties.y;
            if (expandProperties.y < 0) {
                expandProperties.height += expandProperties.y;
                expandProperties.y = 0;
            }
        }
        expandProperties.currentX = expandProperties.x;
        expandProperties.currentY = expandProperties.y;
        IMLog.debug(IMConstants.LOGGING_TAG, "final expanded width after density : " + expandProperties.width + "final expanded height after density " + expandProperties.height + "portrait width requested :" + expandProperties.portraitWidthRequested + "portrait height requested :" + expandProperties.portraitHeightRequested);
        return expandProperties;
    }

    public void setExpandProperties(String str) {
        try {
            this.expProps = (JSController.ExpandProperties) getFromJSON(new JSONObject(str), JSController.ExpandProperties.class);
            IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> ExpandProperties is set: Expandable Width: " + this.expProps.width + " Expandable height: " + this.expProps.height + " Expandable orientation: " + this.expProps.orientation + " Expandable lock orientation: " + this.expProps.lockOrientation + " Expandable Modality: " + this.expProps.isModal + " Expandable Use Custom close: " + this.expProps.useCustomClose);
            this.imWebView.setExpandPropertiesForInterstitial(this.expProps.useCustomClose, this.expProps.lockOrientation, this.expProps.orientation);
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception while setting the expand properties " + e);
        }
    }

    public void acceptAction(String str) {
        this.imWebView.acceptAction(str);
    }

    public void rejectAction(String str) {
        this.imWebView.rejectAction(str);
    }

    public void expand(String str) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> expand: url: " + str);
        try {
            if (this.imWebView.getStateVariable() == IMWebView.ViewState.EXPANDED || this.imWebView.getStateVariable() == IMWebView.ViewState.EXPANDING) {
                IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> Already expanded state");
                return;
            }
            this.imWebView.useLockOrient = false;
            if (this.imWebView.getStateVariable() != IMWebView.ViewState.DEFAULT) {
                this.imWebView.postInjectJavaScript("window.mraidview.fireErrorEvent(\"Current state is not default\", \"expand\")");
            } else if (this.imWebView.getStateVariable() != IMWebView.ViewState.DEFAULT || !this.imWebView.mIsInterstitialAd) {
                a(this.temporaryexpProps, this.expProps);
                IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> At the time of expand the properties are: Expandable width: " + this.temporaryexpProps.width + " Expandable height: " + this.temporaryexpProps.height + " Expandable orientation: " + this.temporaryexpProps.orientation + " Expandable lock orientation: " + this.temporaryexpProps.lockOrientation + " Expandable Modality: " + this.temporaryexpProps.isModal + " Expandable Use custom close " + this.temporaryexpProps.useCustomClose);
                this.imWebView.lockExpandOrientation(this.temporaryexpProps);
                this.imWebView.expand(str, a(this.temporaryexpProps));
            } else {
                this.imWebView.postInjectJavaScript("window.mraidview.fireErrorEvent(\"Expand cannot be called on interstitial ad\", \"expand\")");
            }
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception while expanding the ad. ", e);
        }
    }

    public void onOrientationChange() {
        this.imWebView.onOrientationEventChange();
    }

    private void a(JSController.ExpandProperties expandProperties, JSController.ExpandProperties expandProperties2) {
        expandProperties.width = expandProperties2.width;
        expandProperties.height = expandProperties2.height;
        expandProperties.x = expandProperties2.x;
        expandProperties.y = expandProperties2.y;
        expandProperties.actualWidthRequested = expandProperties2.actualWidthRequested;
        expandProperties.actualHeightRequested = expandProperties2.actualHeightRequested;
        expandProperties.lockOrientation = expandProperties2.lockOrientation;
        expandProperties.isModal = expandProperties2.isModal;
        expandProperties.useCustomClose = expandProperties2.useCustomClose;
        expandProperties.orientation = expandProperties2.orientation;
        expandProperties.topStuff = expandProperties2.topStuff;
        expandProperties.bottomStuff = expandProperties2.bottomStuff;
        expandProperties.portraitWidthRequested = expandProperties2.portraitWidthRequested;
        expandProperties.portraitHeightRequested = expandProperties2.portraitHeightRequested;
        expandProperties.zeroWidthHeight = expandProperties2.zeroWidthHeight;
        expandProperties.rotationAtExpand = expandProperties2.rotationAtExpand;
        expandProperties.currentX = expandProperties2.currentX;
        expandProperties.currentY = expandProperties2.currentY;
    }

    public void useCustomClose(boolean z) {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> useCustomClose" + z);
        this.imWebView.setCustomClose(z);
    }

    public void close() {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> close");
        if (this.imWebView.mOriginalWebviewForExpandUrl != null) {
            this.imWebView.mOriginalWebviewForExpandUrl.close();
        } else {
            this.imWebView.close();
        }
    }

    public boolean isViewable() {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> isViewable");
        return this.imWebView.isViewable();
    }

    public String getPlacementType() {
        IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> getPlacementType");
        return this.imWebView.getPlacementType();
    }

    public String getOrientation() {
        try {
            String currentRotation = this.imWebView.getCurrentRotation(this.imWebView.getIntegerCurrentRotation());
            IMLog.debug(IMConstants.LOGGING_TAG, "JSDisplayController-> getOrientation: " + currentRotation);
            return currentRotation;
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error getOrientation: " + "-1", e);
            return "-1";
        }
    }

    public void stopAllListeners() {
    }
}
