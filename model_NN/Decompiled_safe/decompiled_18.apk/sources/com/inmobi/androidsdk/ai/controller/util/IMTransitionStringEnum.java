package com.inmobi.androidsdk.ai.controller.util;

import org.apache.cordova.NetworkManager;

public enum IMTransitionStringEnum {
    DEFAULT("default"),
    DISSOLVE("dissolve"),
    FADE("fade"),
    ROLL("roll"),
    SLIDE("slide"),
    ZOOM("zoom"),
    NONE(NetworkManager.TYPE_NONE);
    
    private String a;

    private IMTransitionStringEnum(String str) {
        this.a = str;
    }

    public String getText() {
        return this.a;
    }

    public static IMTransitionStringEnum fromString(String str) {
        if (str != null) {
            for (IMTransitionStringEnum iMTransitionStringEnum : values()) {
                if (str.equalsIgnoreCase(iMTransitionStringEnum.a)) {
                    return iMTransitionStringEnum;
                }
            }
        }
        return null;
    }
}
