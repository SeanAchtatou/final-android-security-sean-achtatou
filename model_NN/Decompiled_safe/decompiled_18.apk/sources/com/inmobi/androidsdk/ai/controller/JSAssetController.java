package com.inmobi.androidsdk.ai.controller;

import android.content.Context;
import android.os.StatFs;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.internal.IMLog;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class JSAssetController extends JSController {
    private static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public JSAssetController(IMWebView iMWebView, Context context) {
        super(iMWebView, context);
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0053 A[SYNTHETIC, Splitter:B:27:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String copyTextFromJarIntoAssetDir(java.lang.String r5, java.lang.String r6) {
        /*
            r4 = this;
            r0 = 0
            java.lang.Class<com.inmobi.androidsdk.ai.controller.JSAssetController> r1 = com.inmobi.androidsdk.ai.controller.JSAssetController.class
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            java.net.URL r1 = r1.getResource(r6)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            java.lang.String r1 = r1.getFile()     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            java.lang.String r2 = "file:"
            boolean r2 = r1.startsWith(r2)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            if (r2 == 0) goto L_0x001c
            r2 = 5
            java.lang.String r1 = r1.substring(r2)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
        L_0x001c:
            java.lang.String r2 = "!"
            int r2 = r1.indexOf(r2)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            if (r2 <= 0) goto L_0x0029
            r3 = 0
            java.lang.String r1 = r1.substring(r3, r2)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
        L_0x0029:
            java.util.jar.JarFile r2 = new java.util.jar.JarFile     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            java.util.jar.JarEntry r1 = r2.getJarEntry(r6)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            java.io.InputStream r2 = r2.getInputStream(r1)     // Catch:{ Exception -> 0x0041, all -> 0x004e }
            r1 = 0
            java.lang.String r0 = r4.writeToDisk(r2, r5, r1)     // Catch:{ Exception -> 0x005d }
            if (r2 == 0) goto L_0x0040
            r2.close()     // Catch:{ Exception -> 0x0057 }
        L_0x0040:
            return r0
        L_0x0041:
            r1 = move-exception
            r2 = r0
        L_0x0043:
            r1.printStackTrace()     // Catch:{ all -> 0x005b }
            if (r2 == 0) goto L_0x0040
            r2.close()     // Catch:{ Exception -> 0x004c }
            goto L_0x0040
        L_0x004c:
            r1 = move-exception
            goto L_0x0040
        L_0x004e:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0051:
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            goto L_0x0040
        L_0x0059:
            r1 = move-exception
            goto L_0x0056
        L_0x005b:
            r0 = move-exception
            goto L_0x0051
        L_0x005d:
            r1 = move-exception
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.ai.controller.JSAssetController.copyTextFromJarIntoAssetDir(java.lang.String, java.lang.String):java.lang.String");
    }

    public void addAsset(String str, String str2) {
        HttpEntity a2 = a(str2);
        InputStream inputStream = null;
        try {
            inputStream = a2.getContent();
            writeToDisk(inputStream, str, false);
            this.imWebView.injectJavaScript("mraidAdController.addedAsset('" + str + "' )");
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        try {
            a2.consumeContent();
        } catch (Exception e5) {
            e5.printStackTrace();
        }
    }

    private HttpEntity a(String str) {
        try {
            return new DefaultHttpClient().execute(new HttpGet(str)).getEntity();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int cacheRemaining() {
        StatFs statFs = new StatFs(this.mContext.getFilesDir().getPath());
        return statFs.getFreeBlocks() * statFs.getBlockSize();
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.security.MessageDigest} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.security.MessageDigest} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v14, resolved type: java.security.MessageDigest} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001c A[SYNTHETIC, Splitter:B:11:0x001c] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0047 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0017 A[EDGE_INSN: B:35:0x0017->B:9:0x0017 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String writeToDisk(java.io.InputStream r6, java.lang.String r7, boolean r8) throws java.lang.IllegalStateException, java.io.IOException {
        /*
            r5 = this;
            r1 = 0
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r2 = new byte[r0]
            if (r8 == 0) goto L_0x0045
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x0041 }
        L_0x000d:
            java.io.FileOutputStream r1 = r5.b(r7)     // Catch:{ all -> 0x0053 }
        L_0x0011:
            int r3 = r6.read(r2)     // Catch:{ all -> 0x0053 }
            if (r3 > 0) goto L_0x0047
            r1.flush()     // Catch:{ all -> 0x0053 }
            if (r1 == 0) goto L_0x001f
            r1.close()     // Catch:{ Exception -> 0x005a }
        L_0x001f:
            java.lang.String r1 = r5.a()
            if (r8 == 0) goto L_0x005e
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = r5.a(r0)
            java.lang.String r0 = r5.a(r7, r1, r0)
        L_0x002f:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            return r0
        L_0x0041:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0045:
            r0 = r1
            goto L_0x000d
        L_0x0047:
            if (r8 == 0) goto L_0x004e
            if (r0 == 0) goto L_0x004e
            r0.update(r2)     // Catch:{ all -> 0x0053 }
        L_0x004e:
            r4 = 0
            r1.write(r2, r4, r3)     // Catch:{ all -> 0x0053 }
            goto L_0x0011
        L_0x0053:
            r0 = move-exception
            if (r1 == 0) goto L_0x0059
            r1.close()     // Catch:{ Exception -> 0x005c }
        L_0x0059:
            throw r0
        L_0x005a:
            r1 = move-exception
            goto L_0x001f
        L_0x005c:
            r1 = move-exception
            goto L_0x0059
        L_0x005e:
            r0 = r1
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.ai.controller.JSAssetController.writeToDisk(java.io.InputStream, java.lang.String, boolean):java.lang.String");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.security.MessageDigest} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.security.MessageDigest} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v1, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.StringBuffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.lang.StringBuffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v15, resolved type: java.lang.StringBuffer} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v10, resolved type: java.security.MessageDigest} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0025 A[Catch:{ all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0029 A[Catch:{ all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e A[Catch:{ all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011a A[Catch:{ all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0123 A[Catch:{ all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0143 A[SYNTHETIC, Splitter:B:30:0x0143] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0148 A[SYNTHETIC, Splitter:B:33:0x0148] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0163 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x017f A[SYNTHETIC, Splitter:B:56:0x017f] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0019 A[EDGE_INSN: B:64:0x0019->B:9:0x0019 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String writeToDiskWrap(java.io.InputStream r10, java.lang.String r11, boolean r12, java.lang.String r13, java.lang.String r14, java.lang.String r15) throws java.lang.IllegalStateException, java.io.IOException {
        /*
            r9 = this;
            r2 = 0
            r1 = 0
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r0]
            if (r12 == 0) goto L_0x0160
            java.lang.String r0 = "MD5"
            java.security.MessageDigest r0 = java.security.MessageDigest.getInstance(r0)     // Catch:{ NoSuchAlgorithmException -> 0x015c }
        L_0x000e:
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>()
        L_0x0013:
            int r5 = r10.read(r3)     // Catch:{ all -> 0x0170 }
            if (r5 > 0) goto L_0x0163
            java.lang.String r5 = r4.toString()     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "<html"
            int r3 = r5.indexOf(r3)     // Catch:{ all -> 0x0170 }
            if (r3 < 0) goto L_0x017c
            r2 = 1
            r3 = r2
        L_0x0027:
            if (r3 == 0) goto L_0x0195
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x0170 }
            r2.<init>(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "/mraid_bridge.js"
            int r5 = r2.indexOf(r5)     // Catch:{ all -> 0x0170 }
            if (r5 > 0) goto L_0x0036
        L_0x0036:
            java.lang.String r6 = "/mraid_bridge.js"
            int r6 = r6.length()     // Catch:{ all -> 0x0170 }
            int r6 = r6 + r5
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0170 }
            r7.<init>()     // Catch:{ all -> 0x0170 }
            java.lang.String r8 = "file:/"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r7 = r7.append(r14)     // Catch:{ all -> 0x0170 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0170 }
            r2.replace(r5, r6, r7)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "/mraid.js"
            int r5 = r2.indexOf(r5)     // Catch:{ all -> 0x0170 }
            if (r5 > 0) goto L_0x005b
        L_0x005b:
            java.lang.String r6 = "/mraid.js"
            int r6 = r6.length()     // Catch:{ all -> 0x0170 }
            int r6 = r6 + r5
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0170 }
            r7.<init>()     // Catch:{ all -> 0x0170 }
            java.lang.String r8 = "file:/"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r7 = r7.append(r15)     // Catch:{ all -> 0x0170 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0170 }
            r2.replace(r5, r6, r7)     // Catch:{ all -> 0x0170 }
        L_0x0078:
            java.io.FileOutputStream r1 = r9.b(r11)     // Catch:{ all -> 0x0170 }
            if (r3 != 0) goto L_0x0118
            java.lang.String r5 = "<html>"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "<head>"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "<meta name='viewport' content='user-scalable=no initial-scale=1.0' />"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "<title>Advertisement</title> "
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0170 }
            r5.<init>()     // Catch:{ all -> 0x0170 }
            java.lang.String r6 = "<script src=\"file:/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r5 = r5.append(r14)     // Catch:{ all -> 0x0170 }
            java.lang.String r6 = "\" type=\"text/javascript\"></script>"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0170 }
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0170 }
            r5.<init>()     // Catch:{ all -> 0x0170 }
            java.lang.String r6 = "<script src=\"file:/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r5 = r5.append(r15)     // Catch:{ all -> 0x0170 }
            java.lang.String r6 = "\" type=\"text/javascript\"></script>"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0170 }
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            if (r13 == 0) goto L_0x00fd
            java.lang.String r5 = "<script type=\"text/javascript\">"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            byte[] r5 = r13.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "</script>"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
        L_0x00fd:
            java.lang.String r5 = "</head>"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "<body style=\"margin:0; padding:0; overflow:hidden; background-color:transparent;\">"
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
            java.lang.String r5 = "<div align=\"center\"> "
            byte[] r5 = r5.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r5)     // Catch:{ all -> 0x0170 }
        L_0x0118:
            if (r3 != 0) goto L_0x017f
            byte[] r2 = r4.toByteArray()     // Catch:{ all -> 0x0170 }
            r1.write(r2)     // Catch:{ all -> 0x0170 }
        L_0x0121:
            if (r3 != 0) goto L_0x013e
            java.lang.String r2 = "</div> "
            byte[] r2 = r2.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r2)     // Catch:{ all -> 0x0170 }
            java.lang.String r2 = "</body> "
            byte[] r2 = r2.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r2)     // Catch:{ all -> 0x0170 }
            java.lang.String r2 = "</html> "
            byte[] r2 = r2.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r2)     // Catch:{ all -> 0x0170 }
        L_0x013e:
            r1.flush()     // Catch:{ all -> 0x0170 }
            if (r4 == 0) goto L_0x0146
            r4.close()     // Catch:{ Exception -> 0x018b }
        L_0x0146:
            if (r1 == 0) goto L_0x014b
            r1.close()     // Catch:{ Exception -> 0x018d }
        L_0x014b:
            java.lang.String r1 = r9.a()
            if (r12 == 0) goto L_0x0193
            if (r0 == 0) goto L_0x0193
            java.lang.String r0 = r9.a(r0)
            java.lang.String r0 = r9.a(r11, r1, r0)
        L_0x015b:
            return r0
        L_0x015c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0160:
            r0 = r1
            goto L_0x000e
        L_0x0163:
            if (r12 == 0) goto L_0x016a
            if (r0 == 0) goto L_0x016a
            r0.update(r3)     // Catch:{ all -> 0x0170 }
        L_0x016a:
            r6 = 0
            r4.write(r3, r6, r5)     // Catch:{ all -> 0x0170 }
            goto L_0x0013
        L_0x0170:
            r0 = move-exception
            if (r4 == 0) goto L_0x0176
            r4.close()     // Catch:{ Exception -> 0x018f }
        L_0x0176:
            if (r1 == 0) goto L_0x017b
            r1.close()     // Catch:{ Exception -> 0x0191 }
        L_0x017b:
            throw r0
        L_0x017c:
            r3 = r2
            goto L_0x0027
        L_0x017f:
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0170 }
            byte[] r2 = r2.getBytes()     // Catch:{ all -> 0x0170 }
            r1.write(r2)     // Catch:{ all -> 0x0170 }
            goto L_0x0121
        L_0x018b:
            r2 = move-exception
            goto L_0x0146
        L_0x018d:
            r1 = move-exception
            goto L_0x014b
        L_0x018f:
            r2 = move-exception
            goto L_0x0176
        L_0x0191:
            r1 = move-exception
            goto L_0x017b
        L_0x0193:
            r0 = r1
            goto L_0x015b
        L_0x0195:
            r2 = r1
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.ai.controller.JSAssetController.writeToDiskWrap(java.io.InputStream, java.lang.String, boolean, java.lang.String, java.lang.String, java.lang.String):java.lang.String");
    }

    private String a(String str, String str2, String str3) {
        File file = new File(str2 + File.separator + str);
        new File(str2 + File.separator + "ad").mkdir();
        File file2 = new File(str2 + File.separator + "ad" + File.separator + str3);
        file2.mkdir();
        file.renameTo(new File(file2, file.getName()));
        return file2.getPath() + File.separator;
    }

    private String a(MessageDigest messageDigest) {
        int i = 0;
        byte[] digest = messageDigest.digest();
        char[] cArr = new char[(digest.length * 2)];
        for (int i2 = 0; i2 < digest.length; i2++) {
            int i3 = i + 1;
            cArr[i] = a[(digest[i2] >>> 4) & 15];
            i = i3 + 1;
            cArr[i3] = a[digest[i2] & 15];
        }
        return new String(cArr);
    }

    private String a() {
        return this.mContext.getFilesDir().getPath();
    }

    private FileOutputStream b(String str) throws FileNotFoundException {
        File c = c(d(str));
        c.mkdirs();
        return new FileOutputStream(new File(c, e(str)));
    }

    public void removeAsset(String str) {
        File c = c(d(str));
        c.mkdirs();
        new File(c, e(str)).delete();
        this.imWebView.injectJavaScript("mraidAdController.assetRemoved('" + str + "' )");
    }

    private File c(String str) {
        File filesDir = this.mContext.getFilesDir();
        IMLog.debug(IMConstants.LOGGING_TAG, "Tmp File dir: " + filesDir);
        return new File(filesDir.getPath() + File.separator + str);
    }

    private String d(String str) {
        if (str.lastIndexOf(File.separatorChar) >= 0) {
            return str.substring(0, str.lastIndexOf(File.separatorChar));
        }
        return "/";
    }

    private String e(String str) {
        if (str.lastIndexOf(File.separatorChar) >= 0) {
            return str.substring(str.lastIndexOf(File.separatorChar) + 1);
        }
        return str;
    }

    public String getAssetPath() {
        return "file://" + this.mContext.getFilesDir() + "/";
    }

    public static boolean deleteDirectory(String str) {
        if (str != null) {
            return deleteDirectory(new File(str));
        }
        return false;
    }

    public static boolean deleteDirectory(File file) {
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    deleteDirectory(listFiles[i]);
                } else {
                    listFiles[i].delete();
                }
            }
        }
        return file.delete();
    }

    public void deleteOldAds() {
        deleteDirectory(new File(a() + File.separator + "ad"));
    }

    public void stopAllListeners() {
    }
}
