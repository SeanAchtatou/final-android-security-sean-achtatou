package com.inmobi.androidsdk.ai.controller;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.util.IMNavigationStringEnum;
import com.inmobi.androidsdk.ai.controller.util.IMTransitionStringEnum;
import java.lang.reflect.Field;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class JSController {
    public static final String EXIT = "exit";
    public static final String FULL_SCREEN = "fullscreen";
    public static final String STYLE_NORMAL = "normal";
    protected ExpandProperties expProps = new ExpandProperties();
    protected IMWebView imWebView;
    protected Context mContext;
    protected ExpandProperties temporaryexpProps = new ExpandProperties();

    public abstract void stopAllListeners();

    public static class PlayerProperties extends ReflectedParcelable {
        public static final Parcelable.Creator<PlayerProperties> CREATOR = new Parcelable.Creator<PlayerProperties>() {
            /* renamed from: a */
            public PlayerProperties createFromParcel(Parcel parcel) {
                return new PlayerProperties(parcel);
            }

            /* renamed from: a */
            public PlayerProperties[] newArray(int i) {
                return new PlayerProperties[i];
            }
        };
        public boolean audioMuted;
        public boolean autoPlay;
        public boolean doLoop;
        public String id;
        public boolean showControl;
        public String startStyle;
        public String stopStyle;

        public PlayerProperties() {
            this.showControl = true;
            this.autoPlay = true;
            this.audioMuted = false;
            this.doLoop = false;
            this.stopStyle = JSController.STYLE_NORMAL;
            this.startStyle = JSController.STYLE_NORMAL;
            this.id = "";
        }

        public PlayerProperties(Parcel parcel) {
            super(parcel);
        }

        public void setStopStyle(String str) {
            this.stopStyle = str;
        }

        public void setProperties(boolean z, boolean z2, boolean z3, boolean z4, String str, String str2, String str3) {
            this.autoPlay = z2;
            this.showControl = z3;
            this.doLoop = z4;
            this.audioMuted = z;
            this.startStyle = str;
            this.stopStyle = str2;
            this.id = str3;
        }

        public boolean isAutoPlay() {
            return this.autoPlay;
        }

        public boolean showControl() {
            return this.showControl;
        }

        public boolean doLoop() {
            return this.doLoop;
        }

        public boolean doMute() {
            return this.audioMuted;
        }

        public boolean exitOnComplete() {
            return this.stopStyle.equalsIgnoreCase(JSController.EXIT);
        }

        public boolean isFullScreen() {
            return this.startStyle.equalsIgnoreCase(JSController.FULL_SCREEN);
        }

        public void setFullScreen() {
            this.startStyle = JSController.FULL_SCREEN;
        }
    }

    public void reinitializeExpandProperties() {
        this.expProps.reinitializeExpandProperties();
    }

    public static class ExpandProperties extends ReflectedParcelable {
        public static final Parcelable.Creator<ExpandProperties> CREATOR = new Parcelable.Creator<ExpandProperties>() {
            /* renamed from: a */
            public ExpandProperties createFromParcel(Parcel parcel) {
                return new ExpandProperties(parcel);
            }

            /* renamed from: a */
            public ExpandProperties[] newArray(int i) {
                return new ExpandProperties[i];
            }
        };
        public int actualHeightRequested;
        public int actualWidthRequested;
        public int bottomStuff;
        public int currentX;
        public int currentY;
        public int height;
        public boolean isModal;
        public boolean lockOrientation;
        public String orientation;
        public int portraitHeightRequested;
        public int portraitWidthRequested;
        public String rotationAtExpand;
        public int topStuff;
        public boolean useCustomClose;
        public int width;
        public int x;
        public int y;
        public boolean zeroWidthHeight;

        public ExpandProperties() {
            this.width = 0;
            this.height = 0;
            this.x = -1;
            this.y = -1;
            this.useCustomClose = false;
            this.isModal = true;
            this.lockOrientation = false;
            this.orientation = "";
            this.actualWidthRequested = 0;
            this.actualHeightRequested = 0;
            this.topStuff = 0;
            this.bottomStuff = 0;
            this.portraitWidthRequested = 0;
            this.portraitHeightRequested = 0;
            this.zeroWidthHeight = false;
            this.rotationAtExpand = "";
            this.currentX = 0;
            this.currentY = 0;
        }

        public void reinitializeExpandProperties() {
            this.width = 0;
            this.height = 0;
            this.x = -1;
            this.y = -1;
            this.useCustomClose = false;
            this.isModal = true;
            this.lockOrientation = false;
            this.orientation = "";
            this.actualWidthRequested = 0;
            this.actualHeightRequested = 0;
            this.topStuff = 0;
            this.bottomStuff = 0;
            this.portraitWidthRequested = 0;
            this.portraitHeightRequested = 0;
            this.zeroWidthHeight = false;
            this.rotationAtExpand = "";
            this.currentX = 0;
            this.currentY = 0;
        }

        protected ExpandProperties(Parcel parcel) {
            super(parcel);
        }
    }

    public static class Dimensions extends ReflectedParcelable {
        public static final Parcelable.Creator<Dimensions> CREATOR = new Parcelable.Creator<Dimensions>() {
            /* renamed from: a */
            public Dimensions createFromParcel(Parcel parcel) {
                return new Dimensions(parcel);
            }

            /* renamed from: a */
            public Dimensions[] newArray(int i) {
                return new Dimensions[i];
            }
        };
        public int height;
        public int width;
        public int x;
        public int y;

        public Dimensions() {
            this.x = -1;
            this.y = -1;
            this.width = -1;
            this.height = -1;
        }

        protected Dimensions(Parcel parcel) {
            super(parcel);
        }

        public String toString() {
            return "x: " + this.x + ", y: " + this.y + ", width: " + this.width + ", height: " + this.height;
        }
    }

    public static class Properties extends ReflectedParcelable {
        public static final Parcelable.Creator<Properties> CREATOR = new Parcelable.Creator<Properties>() {
            /* renamed from: a */
            public Properties createFromParcel(Parcel parcel) {
                return new Properties(parcel);
            }

            /* renamed from: a */
            public Properties[] newArray(int i) {
                return new Properties[i];
            }
        };
        public int backgroundColor;
        public float backgroundOpacity;
        public boolean useBackground;

        protected Properties(Parcel parcel) {
            super(parcel);
        }

        public Properties() {
            this.useBackground = false;
            this.backgroundColor = 0;
            this.backgroundOpacity = 0.0f;
        }
    }

    public JSController(IMWebView iMWebView, Context context) {
        this.imWebView = iMWebView;
        this.mContext = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    protected static Object getFromJSON(JSONObject jSONObject, Class<?> cls) throws IllegalAccessException, InstantiationException, NumberFormatException, NullPointerException {
        int parseInt;
        Field[] declaredFields = cls.getDeclaredFields();
        Object newInstance = cls.newInstance();
        for (Field field : declaredFields) {
            String replace = field.getName().replace('_', '-');
            String obj = field.getType().toString();
            try {
                if (obj.equals("int")) {
                    String lowerCase = jSONObject.getString(replace).toLowerCase();
                    if (lowerCase.startsWith("#")) {
                        parseInt = -1;
                        try {
                            if (lowerCase.startsWith("#0x")) {
                                parseInt = Integer.decode(lowerCase.substring(1)).intValue();
                            } else {
                                parseInt = Integer.parseInt(lowerCase.substring(1), 16);
                            }
                        } catch (NumberFormatException e) {
                        }
                    } else {
                        parseInt = Integer.parseInt(lowerCase);
                    }
                    field.set(newInstance, Integer.valueOf(parseInt));
                } else if (obj.equals("class java.lang.String")) {
                    field.set(newInstance, jSONObject.getString(replace));
                } else if (obj.equals("boolean")) {
                    field.set(newInstance, Boolean.valueOf(jSONObject.getBoolean(replace)));
                } else if (obj.equals("float")) {
                    field.set(newInstance, Float.valueOf(Float.parseFloat(jSONObject.getString(replace))));
                } else if (obj.equals("class com.mraid.NavigationStringEnum")) {
                    field.set(newInstance, IMNavigationStringEnum.fromString(jSONObject.getString(replace)));
                } else if (obj.equals("class com.mraid.TransitionStringEnum")) {
                    field.set(newInstance, IMTransitionStringEnum.fromString(jSONObject.getString(replace)));
                }
            } catch (JSONException e2) {
            }
        }
        return newInstance;
    }

    public static class ReflectedParcelable implements Parcelable {
        public ReflectedParcelable() {
        }

        public int describeContents() {
            return 0;
        }

        protected ReflectedParcelable(Parcel parcel) {
            Field[] declaredFields = getClass().getDeclaredFields();
            int i = 0;
            while (i < declaredFields.length) {
                try {
                    Field field = declaredFields[i];
                    Class<?> type = field.getType();
                    if (type.isEnum()) {
                        String cls = type.toString();
                        if (cls.equals("class com.mraid.NavigationStringEnum")) {
                            field.set(this, IMNavigationStringEnum.fromString(parcel.readString()));
                        } else if (cls.equals("class com.mraid.TransitionStringEnum")) {
                            field.set(this, IMTransitionStringEnum.fromString(parcel.readString()));
                        }
                    } else if (!(field.get(this) instanceof Parcelable.Creator)) {
                        field.set(this, parcel.readValue(null));
                    }
                    i++;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return;
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                    return;
                }
            }
        }

        public void writeToParcel(Parcel parcel, int i) {
            Field[] declaredFields = getClass().getDeclaredFields();
            int i2 = 0;
            while (i2 < declaredFields.length) {
                try {
                    Field field = declaredFields[i2];
                    Class<?> type = field.getType();
                    if (type.isEnum()) {
                        String cls = type.toString();
                        if (cls.equals("class com.mraid.NavigationStringEnum")) {
                            parcel.writeString(((IMNavigationStringEnum) field.get(this)).getText());
                        } else if (cls.equals("class com.mraid.TransitionStringEnum")) {
                            parcel.writeString(((IMTransitionStringEnum) field.get(this)).getText());
                        }
                    } else {
                        Object obj = field.get(this);
                        if (!(obj instanceof Parcelable.Creator)) {
                            parcel.writeValue(obj);
                        }
                    }
                    i2++;
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    return;
                } catch (IllegalAccessException e2) {
                    e2.printStackTrace();
                    return;
                }
            }
        }
    }
}
