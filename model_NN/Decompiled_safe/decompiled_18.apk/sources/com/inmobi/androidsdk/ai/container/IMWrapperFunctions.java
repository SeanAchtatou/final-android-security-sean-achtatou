package com.inmobi.androidsdk.ai.container;

import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.view.Display;
import android.view.MotionEvent;
import android.view.ViewGroup;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class IMWrapperFunctions {
    private static final String FIELD_AXIS_SIZE = "AXIS_SIZE";
    private static final String FIELD_AXIS_X = "AXIS_X";
    private static final String FIELD_AXIS_Y = "AXIS_Y";
    private static final String FIELD_CONFIG_SCREEN_SIZE = "CONFIG_SCREEN_SIZE";
    private static final String FIELD_CONFIG_SMALLEST_SCREEN_SIZE = "CONFIG_SMALLEST_SCREEN_SIZE";
    private static final String FIELD_FILL_PARENT = "FILL_PARENT";
    private static final String FIELD_MATCH_PARENT = "MATCH_PARENT";
    private static final String FIELD_REVERSE_LANDSCAPE = "SCREEN_ORIENTATION_REVERSE_LANDSCAPE";
    private static final String FIELD_REVERSE_PORTRAIT = "SCREEN_ORIENTATION_REVERSE_PORTRAIT";
    private static final String METHOD_GETAXISVALUE = "getAxisValue";
    private static final String METHOD_GETHEIGHT = "getHeight";
    private static final String METHOD_GETSIZE = "getSize";
    private static final String METHOD_GETWIDTH = "getWidth";
    private static int a;

    public static int getParamConfigScreenSize() {
        ActivityInfo activityInfo = new ActivityInfo();
        try {
            Field field = ActivityInfo.class.getField(FIELD_CONFIG_SCREEN_SIZE);
            if (field != null) {
                return field.getInt(activityInfo);
            }
            return 0;
        } catch (Exception | NoSuchFieldException e) {
            return 0;
        }
    }

    public static int getParamConfigSmallestScreenSize() {
        ActivityInfo activityInfo = new ActivityInfo();
        try {
            Field field = ActivityInfo.class.getField(FIELD_CONFIG_SMALLEST_SCREEN_SIZE);
            if (field != null) {
                return field.getInt(activityInfo);
            }
            return 0;
        } catch (Exception | NoSuchFieldException e) {
            return 0;
        }
    }

    public static int getParamPortraitOrientation(int i) {
        if (i != 2) {
            return 1;
        }
        ActivityInfo activityInfo = new ActivityInfo();
        try {
            Field field = ActivityInfo.class.getField(FIELD_REVERSE_PORTRAIT);
            if (field != null) {
                return field.getInt(activityInfo);
            }
            return 1;
        } catch (Exception | NoSuchFieldException e) {
            return 1;
        }
    }

    public static int getParamLandscapeOrientation(int i) {
        if (i != 3) {
            return 0;
        }
        ActivityInfo activityInfo = new ActivityInfo();
        try {
            Field field = ActivityInfo.class.getField(FIELD_REVERSE_LANDSCAPE);
            if (field != null) {
                return field.getInt(activityInfo);
            }
            return 0;
        } catch (Exception | NoSuchFieldException e) {
            return 0;
        }
    }

    public static int getDisplayWidth(Display display) {
        boolean z;
        Method method = null;
        try {
            method = Display.class.getMethod(METHOD_GETSIZE, Point.class);
            z = true;
        } catch (NoSuchMethodException e) {
            try {
                method = Display.class.getMethod(METHOD_GETWIDTH, null);
                z = false;
            } catch (NoSuchMethodException e2) {
                z = false;
            }
        }
        if (!z) {
            return ((Integer) method.invoke(display, null)).intValue();
        }
        try {
            Point point = new Point();
            method.invoke(display, point);
            return point.x;
        } catch (IllegalArgumentException e3) {
            return 0;
        } catch (IllegalAccessException e4) {
            return 0;
        } catch (InvocationTargetException e5) {
            return 0;
        }
    }

    public static int getDisplayHeight(Display display) {
        boolean z;
        Method method = null;
        try {
            method = Display.class.getMethod(METHOD_GETSIZE, Point.class);
            z = true;
        } catch (NoSuchMethodException e) {
            try {
                method = Display.class.getMethod(METHOD_GETHEIGHT, null);
                z = false;
            } catch (NoSuchMethodException e2) {
                z = false;
            }
        }
        if (!z) {
            return ((Integer) method.invoke(display, null)).intValue();
        }
        try {
            Point point = new Point();
            method.invoke(display, point);
            return point.y;
        } catch (IllegalArgumentException e3) {
            return 0;
        } catch (IllegalAccessException e4) {
            return 0;
        } catch (InvocationTargetException e5) {
            return 0;
        }
    }

    public static int getParamFillParent() {
        if (a == 0) {
            synchronized (IMWrapperFunctions.class) {
                if (a == 0) {
                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(1, 1);
                    Class<ViewGroup.LayoutParams> cls = ViewGroup.LayoutParams.class;
                    Field field = null;
                    try {
                        field = cls.getField(FIELD_MATCH_PARENT);
                    } catch (NoSuchFieldException e) {
                        try {
                            field = cls.getField(FIELD_FILL_PARENT);
                        } catch (NoSuchFieldException e2) {
                        }
                    }
                    if (field != null) {
                        try {
                            a = field.getInt(layoutParams);
                        } catch (Exception e3) {
                        }
                    }
                }
            }
        }
        return a;
    }

    public static String getTapSize(MotionEvent motionEvent) {
        try {
            return MotionEvent.class.getMethod(METHOD_GETAXISVALUE, Integer.TYPE).invoke(motionEvent, Integer.valueOf(MotionEvent.class.getField(FIELD_AXIS_SIZE).getInt(null))).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTapLocationX(MotionEvent motionEvent) {
        try {
            return MotionEvent.class.getMethod(METHOD_GETAXISVALUE, Integer.TYPE).invoke(motionEvent, Integer.valueOf(MotionEvent.class.getField(FIELD_AXIS_X).getInt(null))).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getTapLocationY(MotionEvent motionEvent) {
        try {
            return MotionEvent.class.getMethod(METHOD_GETAXISVALUE, Integer.TYPE).invoke(motionEvent, Integer.valueOf(MotionEvent.class.getField(FIELD_AXIS_Y).getInt(null))).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
