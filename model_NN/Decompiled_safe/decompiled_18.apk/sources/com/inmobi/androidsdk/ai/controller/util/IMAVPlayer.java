package com.inmobi.androidsdk.ai.controller.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.adsdk.sdk.Const;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.JSController;
import com.inmobi.commons.internal.IMLog;
import com.inmobi.commons.internal.InternalSDKUtil;
import com.jumptap.adtag.media.VideoCacheItem;
import java.lang.ref.WeakReference;

public class IMAVPlayer extends VideoView implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    public static final int MINIMAL_LAYOUT_PARAM = 1;
    private static String f = "play";
    private static String g = "pause";
    private static String h = "ended";
    private static int i = -1;
    private static int j = 2;
    private static String k = "Loading. Please Wait..";
    private JSController.PlayerProperties a;
    /* access modifiers changed from: private */
    public AudioManager b;
    private IMAVPlayerListener c;
    private String d;
    private RelativeLayout e;
    private boolean l;
    private boolean m;
    private IMWebView n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public int q;
    private playerState r;
    private MediaPlayer s;
    /* access modifiers changed from: private */
    public boolean t;
    private ViewGroup u;
    private JSController.Dimensions v;
    private a w;

    public enum playerState {
        INIT,
        PLAYING,
        PAUSED,
        HIDDEN,
        SHOWING,
        COMPLETED,
        RELEASED
    }

    private IMAVPlayer(Context context) {
        super(context);
        this.l = false;
        this.m = false;
        this.q = -1;
        this.w = new a(this);
    }

    public IMAVPlayer(Context context, IMWebView iMWebView) {
        this(context);
        setZOrderOnTop(true);
        this.b = (AudioManager) getContext().getSystemService("audio");
        this.n = iMWebView;
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.o = this.b.getStreamVolume(3);
        this.p = this.o;
        getHolder().addCallback(new SurfaceHolder.Callback() {
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                IMAVPlayer.this.post(new Runnable() {
                    public void run() {
                        IMAVPlayer.this.releasePlayer(false);
                    }
                });
            }

            public void surfaceCreated(SurfaceHolder surfaceHolder) {
            }

            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
            }
        });
    }

    public void setPlayData(JSController.PlayerProperties playerProperties, String str) {
        this.a = playerProperties;
        this.d = str;
    }

    private void a() {
        if (this.a.showControl()) {
            MediaController mediaController = new MediaController(getContext());
            setMediaController(mediaController);
            mediaController.setAnchorView(this);
        }
    }

    private void b() {
        this.d = this.d.trim();
        this.d = a(this.d);
        this.r = playerState.INIT;
        e();
        setVideoPath(this.d);
        a();
        setOnCompletionListener(this);
        setOnErrorListener(this);
        setOnPreparedListener(this);
    }

    private static String a(String str) {
        try {
            byte[] bytes = str.getBytes();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i2 = 0; i2 < bytes.length; i2++) {
                if ((bytes[i2] & 128) > 0) {
                    stringBuffer.append("%" + InternalSDKUtil.byteToHex(bytes[i2]));
                } else {
                    stringBuffer.append((char) bytes[i2]);
                }
            }
            return new String(stringBuffer.toString().getBytes(), Const.RESPONSE_ENCODING);
        } catch (Exception e2) {
            return null;
        }
    }

    private void c() {
        if (this.r == playerState.SHOWING) {
            this.r = this.m ? playerState.COMPLETED : playerState.PAUSED;
        } else if (this.a.isAutoPlay() && this.r == playerState.INIT) {
            if (this.a.doMute()) {
                mute();
            }
            start();
        }
    }

    public void play() {
        b();
    }

    public void setListener(IMAVPlayerListener iMAVPlayerListener) {
        this.c = iMAVPlayerListener;
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        IMLog.debug(IMConstants.LOGGING_TAG, "AVPlayer-> onCompletion");
        this.r = playerState.COMPLETED;
        this.m = true;
        b(h);
        i();
        if (this.a.doLoop()) {
            synchronized (this) {
                if (!j()) {
                    start();
                }
            }
        } else if (this.a.exitOnComplete()) {
            releasePlayer(false);
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        IMLog.debug(IMConstants.LOGGING_TAG, "AVPlayer-> Player error : " + i2);
        f();
        releasePlayer(false);
        if (this.c != null) {
            this.c.onError(this);
        }
        int i4 = i;
        if (i2 == 100) {
            i4 = j;
        }
        a(i4);
        return false;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.s = mediaPlayer;
        if (this.t) {
            try {
                this.s.setVolume(0.0f, 0.0f);
            } catch (Exception e2) {
            }
        }
        IMLog.debug(IMConstants.LOGGING_TAG, "AVPlayer-> onPrepared");
        f();
        if (this.c != null) {
            this.c.onPrepared(this);
        }
        this.l = true;
        c();
    }

    private void d() {
        try {
            ViewGroup viewGroup = (ViewGroup) getParent();
            if (viewGroup != null) {
                viewGroup.removeView(this);
            }
        } catch (Exception e2) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        if (r3.q == -1) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0021, code lost:
        r0 = r3.q;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        a(r4, r0);
        r3.q = -1;
        i();
        unMute();
        m();
        stopPlayback();
        d();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0039, code lost:
        if (r3.c == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003b, code lost:
        r3.c.onComplete(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0044, code lost:
        r0 = java.lang.Math.round((float) (getCurrentPosition() / 1000));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
        r3.r = com.inmobi.androidsdk.ai.controller.util.IMAVPlayer.playerState.RELEASED;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void releasePlayer(boolean r4) {
        /*
            r3 = this;
            r2 = -1
            com.inmobi.androidsdk.ai.container.IMWebView r0 = r3.n
            if (r0 == 0) goto L_0x000f
            com.inmobi.androidsdk.ai.container.IMWebView r0 = r3.n
            com.inmobi.androidsdk.ai.controller.util.IMAVPlayer$2 r1 = new com.inmobi.androidsdk.ai.controller.util.IMAVPlayer$2
            r1.<init>()
            r0.setOnTouchListener(r1)
        L_0x000f:
            monitor-enter(r3)
            boolean r0 = r3.k()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0018
            monitor-exit(r3)     // Catch:{ all -> 0x0041 }
        L_0x0017:
            return
        L_0x0018:
            monitor-exit(r3)     // Catch:{ all -> 0x0041 }
            com.inmobi.androidsdk.ai.controller.util.IMAVPlayer$playerState r0 = com.inmobi.androidsdk.ai.controller.util.IMAVPlayer.playerState.RELEASED
            r3.r = r0
            int r0 = r3.q
            if (r0 == r2) goto L_0x0044
            int r0 = r3.q
        L_0x0023:
            r3.a(r4, r0)
            r3.q = r2
            r3.i()
            r3.unMute()
            r3.m()
            r3.stopPlayback()
            r3.d()
            com.inmobi.androidsdk.ai.controller.util.IMAVPlayerListener r0 = r3.c
            if (r0 == 0) goto L_0x0017
            com.inmobi.androidsdk.ai.controller.util.IMAVPlayerListener r0 = r3.c
            r0.onComplete(r3)
            goto L_0x0017
        L_0x0041:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0041 }
            throw r0
        L_0x0044:
            int r0 = r3.getCurrentPosition()
            int r0 = r0 / 1000
            float r0 = (float) r0
            int r0 = java.lang.Math.round(r0)
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.ai.controller.util.IMAVPlayer.releasePlayer(boolean):void");
    }

    private void e() {
        this.e = new RelativeLayout(getContext());
        this.e.setLayoutParams(getLayoutParams());
        this.e.setBackgroundColor(-16777216);
        TextView textView = new TextView(getContext());
        textView.setText(k);
        textView.setTextColor(-1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.e.addView(textView, layoutParams);
        ((ViewGroup) getParent()).addView(this.e);
    }

    private void f() {
        if (this.e != null) {
            ((ViewGroup) getParent()).removeView(this.e);
        }
    }

    public synchronized void start() {
        if (this.n != null && !n()) {
            this.n.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return motionEvent.getAction() == 2;
                }
            });
        }
        if (this.r == null || this.r != playerState.PLAYING) {
            super.start();
            this.r = playerState.PLAYING;
            this.m = false;
            h();
            IMLog.debug(IMConstants.LOGGING_TAG, "AVPlayer-> start playing");
            if (this.l) {
                b(f);
            }
        }
    }

    public synchronized void pause() {
        if (this.r == null || this.r != playerState.PAUSED) {
            super.pause();
            this.r = playerState.PAUSED;
            i();
            IMLog.debug(IMConstants.LOGGING_TAG, "AVPlayer-> pause");
            b(g);
        }
    }

    private void b(String str) {
        if (this.n != null) {
            this.n.injectJavaScript("window.mraidview.fireMediaTrackingEvent('" + str + "','" + this.a.id + "');");
        }
    }

    private void a(int i2) {
        if (this.n != null) {
            this.n.injectJavaScript("window.mraidview.fireMediaErrorEvent('" + this.a.id + "'," + i2 + ");");
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3) {
        if (this.n != null) {
            this.n.injectJavaScript("window.mraidview.fireMediaTimeUpdateEvent('" + this.a.id + "'," + i2 + VideoCacheItem.URL_DELIMITER + i3 + ");");
        }
    }

    private void a(boolean z, int i2) {
        if (this.n != null) {
            this.n.injectJavaScript("window.mraidview.fireMediaCloseEvent('" + this.a.id + "'," + z + VideoCacheItem.URL_DELIMITER + i2 + ");");
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (this.n != null) {
            this.n.injectJavaScript("window.mraidview.fireMediaVolumeChangeEvent('" + this.a.id + "'," + getVolume() + VideoCacheItem.URL_DELIMITER + isMediaMuted() + ");");
        }
    }

    private void h() {
        this.w.sendEmptyMessage(1001);
    }

    private void i() {
        this.w.removeMessages(1001);
    }

    static class a extends Handler {
        private final WeakReference<IMAVPlayer> a;

        public a(IMAVPlayer iMAVPlayer) {
            this.a = new WeakReference<>(iMAVPlayer);
        }

        public void handleMessage(Message message) {
            IMAVPlayer iMAVPlayer = this.a.get();
            if (iMAVPlayer != null) {
                switch (message.what) {
                    case 1001:
                        if (iMAVPlayer.l()) {
                            synchronized (IMAVPlayer.class) {
                                int streamVolume = iMAVPlayer.b.getStreamVolume(3);
                                if (streamVolume != iMAVPlayer.o) {
                                    int unused = iMAVPlayer.o = streamVolume;
                                    int unused2 = iMAVPlayer.p = iMAVPlayer.o;
                                    if (iMAVPlayer.t) {
                                        iMAVPlayer.unMute();
                                    } else {
                                        iMAVPlayer.g();
                                    }
                                }
                            }
                            int round = Math.round((float) (iMAVPlayer.getCurrentPosition() / 1000));
                            int round2 = Math.round((float) (iMAVPlayer.getDuration() / 1000));
                            if (iMAVPlayer.q != round) {
                                iMAVPlayer.a(round, round2);
                                int unused3 = iMAVPlayer.q = round;
                            }
                            sendEmptyMessageDelayed(1001, 1000);
                            break;
                        } else {
                            return;
                        }
                }
            }
            super.handleMessage(message);
        }
    }

    public String getPropertyID() {
        return this.a.id;
    }

    public boolean isInlineVideo() {
        return !this.a.isFullScreen();
    }

    private boolean j() {
        return this.r == playerState.PAUSED || this.r == playerState.HIDDEN;
    }

    private boolean k() {
        return this.r == playerState.RELEASED;
    }

    /* access modifiers changed from: private */
    public boolean l() {
        return this.r == playerState.PLAYING;
    }

    public String getMediaURL() {
        return this.d;
    }

    public playerState getState() {
        return this.r;
    }

    private void m() {
        this.b.setStreamVolume(3, this.p, 4);
    }

    public void mute() {
        if (this.s != null && !this.t) {
            this.t = true;
            try {
                this.s.setVolume(0.0f, 0.0f);
            } catch (Exception e2) {
            }
            g();
        }
    }

    public void unMute() {
        if (this.s != null) {
            this.t = false;
            try {
                this.s.setVolume(1.0f, 1.0f);
            } catch (Exception e2) {
            }
            g();
        }
    }

    public boolean isMediaMuted() {
        return this.o == 0 || this.t;
    }

    public void setVolume(int i2) {
        synchronized (IMAVPlayer.class) {
            this.o = b(i2);
        }
        this.b.setStreamVolume(3, this.o, 4);
        if (this.t) {
            unMute();
        } else {
            g();
        }
    }

    public int getVolume() {
        synchronized (IMAVPlayer.class) {
            if (!isPlaying()) {
                this.o = this.b.getStreamVolume(3);
            }
        }
        return (this.o * 100) / this.b.getStreamMaxVolume(3);
    }

    private int b(int i2) {
        return (this.b.getStreamMaxVolume(3) * i2) / 100;
    }

    public void hide() {
        try {
            if (isPlaying()) {
                pause();
            }
            this.u.setVisibility(8);
            this.r = playerState.HIDDEN;
        } catch (Exception e2) {
        }
    }

    public void show() {
        this.r = playerState.SHOWING;
        this.u.setVisibility(0);
        setVisibility(0);
    }

    public void seekPlayer(int i2) {
        if (i2 <= getDuration()) {
            seekTo(i2);
        }
    }

    public void setBackGroundLayout(ViewGroup viewGroup) {
        this.u = viewGroup;
    }

    public ViewGroup getBackGroundLayout() {
        return this.u;
    }

    public JSController.PlayerProperties getProperties() {
        return this.a;
    }

    public JSController.Dimensions getPlayDimensions() {
        return this.v;
    }

    public void setPlayDimensions(JSController.Dimensions dimensions) {
        this.v = dimensions;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        try {
            super.onWindowVisibilityChanged(i2);
        } catch (Exception e2) {
        }
    }

    private boolean n() {
        return getLayoutParams().width == 1 && getLayoutParams().height == 1;
    }
}
