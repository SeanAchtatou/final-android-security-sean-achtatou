package com.inmobi.androidsdk;

import android.location.Location;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class IMAdRequest {
    private boolean a = true;
    private Location b;
    private String c;
    private boolean d = false;
    private String e;
    private String f;
    private Calendar g;
    private GenderType h;
    private String i;
    private String j;
    private int k;
    private EducationType l;
    private EthnicityType m;
    private int n;
    private String o;
    private Map<String, String> p;
    private Map<IMIDType, String> q = new HashMap();

    public enum EducationType {
        Edu_None,
        Edu_HighSchool,
        Edu_SomeCollege,
        Edu_InCollege,
        Edu_BachelorsDegree,
        Edu_MastersDegree,
        Edu_DoctoralDegree,
        Edu_Other
    }

    public enum ErrorCode {
        INVALID_REQUEST,
        AD_DOWNLOAD_IN_PROGRESS,
        AD_CLICK_IN_PROGRESS,
        AD_FETCH_TIMEOUT,
        AD_RENDERING_TIMEOUT,
        NETWORK_ERROR,
        INTERNAL_ERROR,
        NO_FILL,
        INVALID_APP_ID
    }

    public enum EthnicityType {
        Eth_None,
        Eth_Mixed,
        Eth_Asian,
        Eth_Black,
        Eth_Hispanic,
        Eth_NativeAmerican,
        Eth_White,
        Eth_Other
    }

    public enum GenderType {
        NONE,
        MALE,
        FEMALE
    }

    public enum IMIDType {
        ID_LOGIN,
        ID_SESSION
    }

    public boolean isLocationInquiryAllowed() {
        return this.a;
    }

    public void setLocationInquiryAllowed(boolean z) {
        this.a = z;
    }

    public Location getCurrentLocation() {
        return this.b;
    }

    public void setCurrentLocation(Location location) {
        this.b = location;
    }

    public void setLocationWithCityStateCountry(String str, String str2, String str3) {
        this.c = str + "-" + str2 + "-" + str3;
    }

    public String getLocationWithCityStateCountry() {
        return this.c;
    }

    public boolean isTestMode() {
        return this.d;
    }

    public void setTestMode(boolean z) {
        this.d = z;
    }

    public String getPostalCode() {
        return this.e;
    }

    public void setPostalCode(String str) {
        this.e = str;
    }

    public String getAreaCode() {
        return this.f;
    }

    public void setAreaCode(String str) {
        this.f = str;
    }

    public void setDateOfBirth(Calendar calendar) {
        this.g = calendar;
    }

    public Calendar getDateOfBirth() {
        return this.g;
    }

    public void setDateOfBirth(Date date) {
        if (this.g == null) {
            this.g = Calendar.getInstance();
        }
        this.g.setTime(date);
    }

    public GenderType getGender() {
        return this.h;
    }

    public void setGender(GenderType genderType) {
        this.h = genderType;
    }

    public String getKeywords() {
        return this.i;
    }

    public void setKeywords(String str) {
        this.i = str;
    }

    public String getSearchString() {
        return this.j;
    }

    public void setSearchString(String str) {
        this.j = str;
    }

    public int getIncome() {
        return this.k;
    }

    public void setIncome(int i2) {
        this.k = i2;
    }

    public EducationType getEducation() {
        return this.l;
    }

    public void setEducation(EducationType educationType) {
        this.l = educationType;
    }

    public EthnicityType getEthnicity() {
        return this.m;
    }

    public void setEthnicity(EthnicityType ethnicityType) {
        this.m = ethnicityType;
    }

    public int getAge() {
        return this.n;
    }

    public void setAge(int i2) {
        this.n = i2;
    }

    public String getInterests() {
        return this.o;
    }

    public void setInterests(String str) {
        this.o = str;
    }

    public Map<String, String> getRequestParams() {
        return this.p;
    }

    public void setRequestParams(Map<String, String> map) {
        this.p = map;
    }

    public void addIDType(IMIDType iMIDType, String str) {
        if (this.q != null) {
            this.q.put(iMIDType, str);
        }
    }

    public String getIDType(IMIDType iMIDType) {
        if (this.q != null) {
            return this.q.get(iMIDType);
        }
        return null;
    }

    public void removeIDType(IMIDType iMIDType) {
        if (this.q != null) {
            this.q.remove(iMIDType);
        }
    }
}
