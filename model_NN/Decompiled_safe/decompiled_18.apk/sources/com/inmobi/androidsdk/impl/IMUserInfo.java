package com.inmobi.androidsdk.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.IMCommonUtil;
import com.inmobi.commons.internal.IMLog;
import com.inmobi.commons.internal.InternalSDKUtil;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

public final class IMUserInfo {
    private IMAdRequest A;
    private Random B;
    private String C;
    private int D = -1;
    private int E;
    boolean a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g = "1";
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private LocationManager m;
    private String n;
    private double o;
    private double p;
    private double q;
    private boolean r;
    private Context s;
    private String t;
    private String u = null;
    private String v = null;
    private String w = null;
    private String x = null;
    private String y = null;
    private String z = null;

    public IMUserInfo(Context context) {
        this.s = context;
        this.B = new Random();
    }

    /* access modifiers changed from: protected */
    public Context getApplicationContext() {
        return this.s;
    }

    public String getAppBId() {
        return this.b;
    }

    private void a(String str) {
        this.b = str;
    }

    public String getAppDisplayName() {
        return this.c;
    }

    private void b(String str) {
        this.c = str;
    }

    private void a(IMAdRequest iMAdRequest) {
        this.A = iMAdRequest;
    }

    public String getAppVer() {
        return this.d;
    }

    private void c(String str) {
        this.d = str;
    }

    public String getNetworkType() {
        return this.e;
    }

    private void d(String str) {
        this.e = str;
    }

    private String a() {
        return this.f;
    }

    public String getRandomKey() {
        return this.h;
    }

    public String getUIDMapEncrypted() {
        return this.i;
    }

    public String getRsakeyVersion() {
        return this.g;
    }

    private void e(String str) {
        this.f = str;
    }

    private void a(int i2) {
        this.h = Integer.toString(i2);
    }

    private void f(String str) {
        this.i = str;
    }

    private void a(Context context) {
        try {
            if (this.C == null) {
                this.C = context.getSharedPreferences("inmobisdkaid", 0).getString("A_ID", null);
            }
            if (this.C == null) {
                this.C = UUID.randomUUID().toString();
                SharedPreferences.Editor edit = context.getSharedPreferences("inmobisdkaid", 0).edit();
                edit.putString("A_ID", this.C);
                edit.commit();
            }
        } catch (Exception e2) {
        }
    }

    public String getAid() {
        return this.C;
    }

    public String getLocalization() {
        return this.j;
    }

    private void g(String str) {
        this.j = str;
    }

    private String b() {
        return this.k;
    }

    private void h(String str) {
        this.k = str;
    }

    public Map<String, String> getRequestParams() {
        if (this.A != null) {
            return this.A.getRequestParams();
        }
        return null;
    }

    public String getSiteId() {
        return this.l;
    }

    private void i(String str) {
        this.l = str;
    }

    public String getPostalCode() {
        if (this.A != null) {
            return this.A.getPostalCode();
        }
        return null;
    }

    public String getAreaCode() {
        if (this.A != null) {
            return this.A.getAreaCode();
        }
        return null;
    }

    public String getDateOfBirth() {
        if (this.A == null || this.A.getDateOfBirth() == null) {
            return null;
        }
        Calendar dateOfBirth = this.A.getDateOfBirth();
        return dateOfBirth.get(1) + "-" + (dateOfBirth.get(2) + 1) + "-" + dateOfBirth.get(5);
    }

    public IMAdRequest.GenderType getGender() {
        if (this.A != null) {
            return this.A.getGender();
        }
        return null;
    }

    public String getKeywords() {
        if (this.A != null) {
            return this.A.getKeywords();
        }
        return null;
    }

    public String getSearchString() {
        if (this.A != null) {
            return this.A.getSearchString();
        }
        return null;
    }

    public int getIncome() {
        if (this.A != null) {
            return this.A.getIncome();
        }
        return 0;
    }

    public IMAdRequest.EducationType getEducation() {
        if (this.A != null) {
            return this.A.getEducation();
        }
        return null;
    }

    public IMAdRequest.EthnicityType getEthnicity() {
        if (this.A != null) {
            return this.A.getEthnicity();
        }
        return null;
    }

    public String getLocationWithCityStateCountry() {
        if (this.A != null) {
            return this.A.getLocationWithCityStateCountry();
        }
        return null;
    }

    public int getAge() {
        if (this.A != null) {
            return this.A.getAge();
        }
        return 0;
    }

    public String getInterests() {
        if (this.A != null) {
            return this.A.getInterests();
        }
        return null;
    }

    private synchronized LocationManager c() {
        return this.m;
    }

    private synchronized void a(LocationManager locationManager) {
        this.m = locationManager;
    }

    private boolean d() {
        if (this.A != null) {
            return this.A.isLocationInquiryAllowed();
        }
        return true;
    }

    private boolean e() {
        return this.a;
    }

    private void a(boolean z2) {
        this.a = z2;
    }

    public double getLat() {
        return this.o;
    }

    private void a(double d2) {
        this.o = d2;
    }

    public double getLon() {
        return this.p;
    }

    private void b(double d2) {
        this.p = d2;
    }

    public double getLocAccuracy() {
        return this.q;
    }

    private void c(double d2) {
        this.q = d2;
    }

    public boolean isValidGeoInfo() {
        return this.r;
    }

    private void b(boolean z2) {
        this.r = z2;
    }

    public boolean isTestMode() {
        if (this.A != null) {
            return this.A.isTestMode();
        }
        return false;
    }

    public String getTestModeAdActionType() {
        return this.n;
    }

    public String getPhoneDefaultUserAgent() {
        if (this.t == null) {
            return "";
        }
        return this.t;
    }

    public synchronized void updateInfo(String str, IMAdRequest iMAdRequest) {
        a(iMAdRequest);
        i();
        i(str);
        if (iMAdRequest != null) {
            b(false);
            if (!d()) {
                a(true);
            } else if (iMAdRequest.getCurrentLocation() != null) {
                a(iMAdRequest.getCurrentLocation());
                b(true);
            } else {
                h();
                if (!e()) {
                    f();
                }
            }
        }
    }

    private void f() {
        try {
            if (c() == null) {
                a((LocationManager) getApplicationContext().getSystemService("location"));
            }
            if (c() != null) {
                LocationManager c2 = c();
                Criteria criteria = new Criteria();
                if (getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                    criteria.setAccuracy(1);
                } else if (getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                    criteria.setAccuracy(2);
                }
                criteria.setCostAllowed(false);
                String bestProvider = c2.getBestProvider(criteria, true);
                if (!isValidGeoInfo() && bestProvider != null) {
                    Location lastKnownLocation = c2.getLastKnownLocation(bestProvider);
                    IMLog.debug(IMConstants.LOGGING_TAG, "lastBestKnownLocation: " + lastKnownLocation);
                    if (lastKnownLocation == null) {
                        lastKnownLocation = g();
                        IMLog.debug(IMConstants.LOGGING_TAG, "lastKnownLocation: " + lastKnownLocation);
                    }
                    a(lastKnownLocation);
                }
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error getting the Location Info ", e2);
        }
    }

    private Location g() {
        Location lastKnownLocation;
        if (c() == null) {
            a((LocationManager) getApplicationContext().getSystemService("location"));
        }
        if (c() != null) {
            LocationManager c2 = c();
            List<String> providers = c2.getProviders(true);
            for (int size = providers.size() - 1; size >= 0; size--) {
                String str = providers.get(size);
                if (c2.isProviderEnabled(str) && (lastKnownLocation = c2.getLastKnownLocation(str)) != null) {
                    return lastKnownLocation;
                }
            }
        }
        return null;
    }

    private void a(Location location) {
        if (location != null) {
            b(true);
            a(location.getLatitude());
            b(location.getLongitude());
            c((double) location.getAccuracy());
        }
    }

    private void h() {
        int checkCallingOrSelfPermission = getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
        int checkCallingOrSelfPermission2 = getApplicationContext().checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        if (checkCallingOrSelfPermission == 0 || checkCallingOrSelfPermission2 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    private void i() {
        String str;
        String str2;
        String str3;
        String str4;
        if (b() == null) {
            h(Build.BRAND);
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                str3 = language.toLowerCase();
                String country = locale.getCountry();
                if (country != null) {
                    str3 = str3 + "_" + country.toLowerCase();
                }
            } else {
                String str5 = (String) System.getProperties().get("user.language");
                String str6 = (String) System.getProperties().get("user.region");
                if (str5 == null || str6 == null) {
                    str3 = language;
                } else {
                    str3 = str5 + "_" + str6;
                }
                if (str3 == null) {
                    str3 = "en";
                }
            }
            g(str3);
            try {
                Context applicationContext = getApplicationContext();
                PackageManager packageManager = applicationContext.getPackageManager();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(applicationContext.getPackageName(), 128);
                if (applicationInfo != null) {
                    a(applicationInfo.packageName);
                    b(applicationInfo.loadLabel(packageManager).toString());
                }
                PackageInfo packageInfo = packageManager.getPackageInfo(applicationContext.getPackageName(), 128);
                if (packageInfo != null) {
                    str4 = packageInfo.versionName;
                    if (str4 == null || str4.equals("")) {
                        str4 = packageInfo.versionCode + "";
                    }
                } else {
                    str4 = null;
                }
                if (str4 != null && !str4.equals("")) {
                    c(str4);
                }
            } catch (Exception e2) {
            }
        }
        e(InternalSDKUtil.getODIN1(InternalSDKUtil.getAndroidId(getApplicationContext())));
        a(this.B.nextInt());
        int i2 = 0;
        if (this.A != null) {
            str2 = this.A.getIDType(IMAdRequest.IMIDType.ID_LOGIN);
            str = this.A.getIDType(IMAdRequest.IMIDType.ID_SESSION);
            i2 = IMCommonUtil.getDeviceIdMask();
        } else {
            str = null;
            str2 = null;
        }
        f(InternalSDKUtil.getUIDMap(str2, str, a(), null, i2, getRandomKey()));
        if (this.s != null) {
            a(this.s.getApplicationContext());
        }
        d(InternalSDKUtil.getConnectivityType(getApplicationContext()));
        try {
            int i3 = getApplicationContext().getResources().getConfiguration().orientation;
            if (i3 == 2) {
                b(3);
            } else if (i3 == 1) {
                b(1);
            }
        } catch (Exception e3) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error getting the orientation info ", e3);
        }
    }

    public String getRefTagKey() {
        return this.u;
    }

    public void setRefTagKey(String str) {
        this.u = str;
    }

    public String getRefTagValue() {
        return this.v;
    }

    public void setRefTagValue(String str) {
        this.v = str;
    }

    public void setPhoneDefaultUserAgent(String str) {
        this.t = str;
    }

    public String getAdUnitSlot() {
        return this.w;
    }

    public void setAdUnitSlot(String str) {
        this.w = str;
    }

    public String getSlotId() {
        return this.x;
    }

    public void setSlotId(String str) {
        this.x = str;
    }

    public String getScreenSize() {
        return this.y;
    }

    public void setScreenSize(String str) {
        this.y = str;
    }

    public String getScreenDensity() {
        return this.z;
    }

    public void setScreenDensity(String str) {
        this.z = str;
    }

    public int getOrientation() {
        return this.E;
    }

    private void b(int i2) {
        this.E = i2;
    }

    public int getRefreshType() {
        return this.D;
    }

    public void setRefreshType(int i2) {
        if (!(i2 == 1 && i2 == 0)) {
            this.D = -1;
        }
        this.D = i2;
    }
}
