package com.inmobi.androidsdk.impl;

import com.adsdk.sdk.BannerAd;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.Serializable;

public class IMAdUnit implements Serializable {
    private static final long serialVersionUID = 7987544297386338802L;
    private AdActionNames a = AdActionNames.AdActionName_Web;
    private AdTypes b = AdTypes.NONE;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private int h;
    private int i;

    public enum AdTypes {
        NONE,
        TEXT,
        BANNER,
        SEARCH,
        RICH_MEDIA;

        public String toString() {
            return super.toString().replaceFirst("AdType_", "").toLowerCase();
        }
    }

    public enum AdActionNames {
        AdActionName_None,
        AdActionName_Web,
        AdActionName_SMS,
        AdActionName_Search,
        AdActionName_Call,
        AdActionName_Android,
        AdActionName_Map,
        AdActionName_Audio,
        AdActionName_Video;

        public String toString() {
            return super.toString().replaceFirst("AdActionName_", "").toLowerCase();
        }
    }

    public static AdActionNames adActionNamefromString(String str) {
        AdActionNames adActionNames = AdActionNames.AdActionName_None;
        if (str == null) {
            return adActionNames;
        }
        if (str.equalsIgnoreCase("call")) {
            return AdActionNames.AdActionName_Call;
        }
        if (str.equalsIgnoreCase(MMAdViewSDK.Event.INTENT_TXT_MESSAGE)) {
            return AdActionNames.AdActionName_SMS;
        }
        if (str.equalsIgnoreCase("search")) {
            return AdActionNames.AdActionName_Search;
        }
        if (str.equalsIgnoreCase("android")) {
            return AdActionNames.AdActionName_Android;
        }
        if (str.equalsIgnoreCase(BannerAd.WEB)) {
            return AdActionNames.AdActionName_Web;
        }
        if (str.equalsIgnoreCase("map")) {
            return AdActionNames.AdActionName_Map;
        }
        if (str.equalsIgnoreCase("audio")) {
            return AdActionNames.AdActionName_Audio;
        }
        if (str.equalsIgnoreCase("video")) {
            return AdActionNames.AdActionName_Video;
        }
        return AdActionNames.AdActionName_None;
    }

    public static AdTypes adTypefromString(String str) {
        AdTypes adTypes = AdTypes.NONE;
        if (str == null) {
            return adTypes;
        }
        if (str.equalsIgnoreCase("banner")) {
            return AdTypes.BANNER;
        }
        if (str.equalsIgnoreCase("text")) {
            return AdTypes.TEXT;
        }
        if (str.equalsIgnoreCase("search")) {
            return AdTypes.SEARCH;
        }
        if (str.equalsIgnoreCase("rm")) {
            return AdTypes.RICH_MEDIA;
        }
        return adTypes;
    }

    public AdActionNames getAdActionName() {
        return this.a;
    }

    public void setAdActionName(AdActionNames adActionNames) {
        this.a = adActionNames;
    }

    public AdTypes getAdType() {
        return this.b;
    }

    public void setAdType(AdTypes adTypes) {
        this.b = adTypes;
    }

    public String getTargetUrl() {
        return this.c;
    }

    public void setTargetUrl(String str) {
        this.c = str;
    }

    public boolean isSendDeviceInfo() {
        return this.e;
    }

    public void setSendDeviceInfo(boolean z) {
        this.e = z;
    }

    public String getDeviceInfoUploadUrl() {
        return this.f;
    }

    public void setDeviceInfoUploadUrl(String str) {
        this.f = str;
    }

    public String getCDATABlock() {
        return this.g;
    }

    public void setCDATABlock(String str) {
        this.g = str;
    }

    public int getWidth() {
        return this.h;
    }

    public void setWidth(int i2) {
        this.h = i2;
    }

    public int getHeight() {
        return this.i;
    }

    public void setHeight(int i2) {
        this.i = i2;
    }

    public String getDefaultTargetUrl() {
        return this.d;
    }

    public void setDefaultTargetUrl(String str) {
        this.d = str;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("AdUnit: ");
        stringBuffer.append(" adActionName: " + this.a);
        stringBuffer.append(" adType: " + this.b);
        stringBuffer.append(" targetUrl: " + this.c);
        stringBuffer.append(" width: " + this.h);
        stringBuffer.append(" height: " + this.i);
        return stringBuffer.toString();
    }
}
