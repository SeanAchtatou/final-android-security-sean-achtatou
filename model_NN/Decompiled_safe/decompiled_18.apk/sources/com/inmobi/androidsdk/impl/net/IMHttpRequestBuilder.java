package com.inmobi.androidsdk.impl.net;

import com.adsdk.sdk.Const;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.androidsdk.impl.IMUserInfo;
import com.inmobi.androidsdk.impl.net.IMRequestResponseManager;
import com.inmobi.commons.internal.IMLog;
import com.jumptap.adtag.media.VideoCacheItem;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Map;

public final class IMHttpRequestBuilder {
    static String a(IMUserInfo iMUserInfo, IMRequestResponseManager.ActionType actionType) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            if (IMRequestResponseManager.ActionType.AdRequest == actionType) {
                String a = a(iMUserInfo);
                stringBuffer.append("requestactivity=AdRequest");
                if (a != null && !a.equalsIgnoreCase("")) {
                    stringBuffer.append("&" + a);
                }
            } else if (IMRequestResponseManager.ActionType.AdRequest_Interstitial == actionType) {
                String a2 = a(iMUserInfo);
                stringBuffer.append("adtype=int");
                if (a2 != null && !a2.equalsIgnoreCase("")) {
                    stringBuffer.append("&" + a2);
                }
            } else if (IMRequestResponseManager.ActionType.DeviceInfoUpload == actionType) {
                stringBuffer.append("requestactivity=DeviceInfo");
            } else {
                stringBuffer.append("requestactivity=AdClicked");
            }
            stringBuffer.append(b(iMUserInfo));
            stringBuffer.append("&" + c(iMUserInfo));
        } catch (Exception e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception occured in an ad request" + e);
        }
        return stringBuffer.toString();
    }

    private static String a(IMUserInfo iMUserInfo) {
        StringBuilder sb = new StringBuilder();
        if (iMUserInfo.getPostalCode() != null) {
            sb.append("u-postalCode=");
            sb.append(getURLEncoded(iMUserInfo.getPostalCode()));
        }
        if (iMUserInfo.getRequestParams() != null) {
            for (Map.Entry next : iMUserInfo.getRequestParams().entrySet()) {
                sb.append("&").append(getURLEncoded(((String) next.getKey()).toString())).append("=").append(getURLEncoded(((String) next.getValue()).toString()));
            }
        }
        if (iMUserInfo.getAreaCode() != null) {
            sb.append("&u-areaCode=");
            sb.append(getURLEncoded(iMUserInfo.getAreaCode()));
        }
        if (iMUserInfo.getDateOfBirth() != null) {
            sb.append("&u-dateOfBirth=");
            sb.append(getURLEncoded(iMUserInfo.getDateOfBirth()));
        }
        if (!(iMUserInfo.getGender() == IMAdRequest.GenderType.NONE || iMUserInfo.getGender() == null)) {
            sb.append("&u-gender=");
            sb.append(iMUserInfo.getGender() == IMAdRequest.GenderType.MALE ? "M" : "F");
        }
        if (iMUserInfo.getKeywords() != null) {
            sb.append("&p-keywords=");
            sb.append(getURLEncoded(iMUserInfo.getKeywords()));
        }
        if (iMUserInfo.getSearchString() != null) {
            sb.append("&p-type=");
            sb.append(getURLEncoded(iMUserInfo.getSearchString()));
        }
        if (iMUserInfo.getIncome() > 0) {
            sb.append("&u-income=");
            sb.append(iMUserInfo.getIncome());
        }
        if (!(iMUserInfo.getEducation() == IMAdRequest.EducationType.Edu_None || iMUserInfo.getEducation() == null)) {
            sb.append("&u-education=");
            sb.append(iMUserInfo.getEducation());
        }
        if (!(iMUserInfo.getEthnicity() == IMAdRequest.EthnicityType.Eth_None || iMUserInfo.getEthnicity() == null)) {
            sb.append("&u-ethnicity=");
            sb.append(iMUserInfo.getEthnicity());
        }
        if (iMUserInfo.getAge() > 0) {
            sb.append("&u-age=");
            sb.append(iMUserInfo.getAge());
        }
        if (iMUserInfo.getInterests() != null) {
            sb.append("&u-interests=");
            sb.append(getURLEncoded(iMUserInfo.getInterests()));
        }
        if (iMUserInfo.getLocationWithCityStateCountry() != null) {
            sb.append("&u-location=");
            sb.append(getURLEncoded(iMUserInfo.getLocationWithCityStateCountry()));
        }
        if (iMUserInfo.getRefreshType() != -1) {
            sb.append("&u-rt=");
            sb.append(getURLEncoded(String.valueOf(iMUserInfo.getRefreshType())));
        }
        String sb2 = sb.toString();
        try {
            if (sb2.charAt(0) == '&') {
                return sb2.substring(1);
            }
            return sb2;
        } catch (Exception e) {
            return sb2;
        }
    }

    private static String b(IMUserInfo iMUserInfo) {
        StringBuilder sb = new StringBuilder();
        if (iMUserInfo.getScreenDensity() != null) {
            sb.append("&d-device-screen-density=").append(getURLEncoded(iMUserInfo.getScreenDensity()));
        }
        if (iMUserInfo.getScreenSize() != null) {
            sb.append("&d-device-screen-size=").append(getURLEncoded(iMUserInfo.getScreenSize()));
        }
        return sb.toString();
    }

    private static String c(IMUserInfo iMUserInfo) {
        StringBuilder sb = new StringBuilder();
        if (iMUserInfo.getSiteId() != null) {
            sb.append("mk-siteid=");
            sb.append(getURLEncoded(iMUserInfo.getSiteId()));
        }
        if (iMUserInfo.getUIDMapEncrypted() != null) {
            sb.append("&u-id-map=");
            sb.append(getURLEncoded(iMUserInfo.getUIDMapEncrypted()));
            sb.append("&u-id-key=");
            sb.append(iMUserInfo.getRandomKey());
            sb.append("&u-key-ver=");
            sb.append(iMUserInfo.getRsakeyVersion());
        }
        if (iMUserInfo.getAid() != null) {
            sb.append("&aid=");
            sb.append(getURLEncoded(iMUserInfo.getAid()));
        }
        sb.append("&mk-version=");
        sb.append(getURLEncoded("pr-SAND-DTGTA-20120915"));
        sb.append("&mk-rel-version=");
        sb.append(getURLEncoded("pr-SAND-DTGTA-20120915"));
        sb.append("&format=xhtml");
        sb.append("&mk-ads=1");
        sb.append("&h-user-agent=");
        sb.append(getURLEncoded(iMUserInfo.getPhoneDefaultUserAgent()));
        sb.append("&u-appBId=");
        sb.append(getURLEncoded(iMUserInfo.getAppBId()));
        sb.append("&u-appDNM=");
        sb.append(getURLEncoded(iMUserInfo.getAppDisplayName()));
        sb.append("&u-appVer=");
        sb.append(getURLEncoded(iMUserInfo.getAppVer()));
        sb.append("&d-localization=");
        sb.append(getURLEncoded(iMUserInfo.getLocalization()));
        if (iMUserInfo.getNetworkType() != null) {
            sb.append("&d-netType=");
            sb.append(getURLEncoded(iMUserInfo.getNetworkType()));
        }
        if (iMUserInfo.getOrientation() != 0) {
            sb.append("&d-orientation=");
            sb.append(iMUserInfo.getOrientation());
        }
        sb.append("&mk-ad-slot=");
        sb.append(getURLEncoded(iMUserInfo.getAdUnitSlot()));
        if (iMUserInfo.getSlotId() != null) {
            sb.append("&mk-site-slotid=");
            sb.append(getURLEncoded(iMUserInfo.getSlotId()));
        }
        if (iMUserInfo.isValidGeoInfo()) {
            sb.append("&u-latlong-accu=");
            sb.append(getURLEncoded(currentLocationStr(iMUserInfo)));
        }
        if (!(iMUserInfo.getRefTagKey() == null || iMUserInfo.getRefTagValue() == null)) {
            sb.append("&").append(getURLEncoded(iMUserInfo.getRefTagKey())).append("=").append(getURLEncoded(iMUserInfo.getRefTagValue()));
        }
        return sb.toString();
    }

    public static String currentLocationStr(IMUserInfo iMUserInfo) {
        StringBuilder sb = new StringBuilder();
        if (sb == null || !iMUserInfo.isValidGeoInfo()) {
            return "";
        }
        sb.append(iMUserInfo.getLat());
        sb.append(VideoCacheItem.URL_DELIMITER);
        sb.append(iMUserInfo.getLon());
        sb.append(VideoCacheItem.URL_DELIMITER);
        sb.append((int) iMUserInfo.getLocAccuracy());
        return sb.toString();
    }

    public static String getURLEncoded(String str) {
        try {
            return URLEncoder.encode(str, Const.ENCODING);
        } catch (Exception e) {
            return "";
        }
    }

    public static String getURLDecoded(String str, String str2) {
        try {
            return URLDecoder.decode(str, str2);
        } catch (Exception e) {
            return "";
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v9, resolved type: java.lang.Float} */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v15 */
    /* JADX WARN: Type inference failed for: r0v16 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String appendClickParams(java.lang.String r5, android.view.MotionEvent r6, java.lang.String r7) {
        /*
            r0 = 0
            java.lang.String r1 = "1.0"
            float r1 = java.lang.Float.parseFloat(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.Float r1 = java.lang.Float.valueOf(r1)     // Catch:{ Exception -> 0x0168 }
            if (r6 == 0) goto L_0x00d0
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0168 }
            r3 = 12
            if (r2 < r3) goto L_0x0174
            if (r7 == 0) goto L_0x0171
            float r1 = java.lang.Float.parseFloat(r7)     // Catch:{ Exception -> 0x0168 }
            java.lang.Float r1 = java.lang.Float.valueOf(r1)     // Catch:{ Exception -> 0x0168 }
            r4 = r1
        L_0x001e:
            java.lang.String r2 = com.inmobi.androidsdk.ai.container.IMWrapperFunctions.getTapLocationX(r6)     // Catch:{ Exception -> 0x0168 }
            if (r2 == 0) goto L_0x016e
            float r1 = java.lang.Float.parseFloat(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.Float r1 = java.lang.Float.valueOf(r1)     // Catch:{ Exception -> 0x0168 }
            r3 = r1
        L_0x002d:
            java.lang.String r1 = com.inmobi.androidsdk.ai.container.IMWrapperFunctions.getTapLocationY(r6)     // Catch:{ Exception -> 0x0168 }
            if (r1 == 0) goto L_0x003b
            float r0 = java.lang.Float.parseFloat(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.Float r0 = java.lang.Float.valueOf(r0)     // Catch:{ Exception -> 0x0168 }
        L_0x003b:
            if (r3 == 0) goto L_0x004a
            float r2 = r3.floatValue()     // Catch:{ Exception -> 0x0168 }
            float r3 = r4.floatValue()     // Catch:{ Exception -> 0x0168 }
            float r2 = r2 / r3
            java.lang.String r2 = java.lang.Float.toString(r2)     // Catch:{ Exception -> 0x0168 }
        L_0x004a:
            if (r0 == 0) goto L_0x0059
            float r0 = r0.floatValue()     // Catch:{ Exception -> 0x0168 }
            float r1 = r4.floatValue()     // Catch:{ Exception -> 0x0168 }
            float r0 = r0 / r1
            java.lang.String r1 = java.lang.Float.toString(r0)     // Catch:{ Exception -> 0x0168 }
        L_0x0059:
            java.lang.String r0 = com.inmobi.androidsdk.ai.container.IMWrapperFunctions.getTapSize(r6)     // Catch:{ Exception -> 0x0168 }
        L_0x005d:
            java.lang.String r3 = "?"
            boolean r3 = r5.contains(r3)     // Catch:{ Exception -> 0x0168 }
            if (r3 == 0) goto L_0x00d1
            if (r2 == 0) goto L_0x00a7
            if (r1 == 0) goto L_0x00a7
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r3.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = "&"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = "u-tap-o"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = "="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r4.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0168 }
            java.lang.String r1 = getURLEncoded(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r5 = r1.toString()     // Catch:{ Exception -> 0x0168 }
        L_0x00a7:
            if (r0 == 0) goto L_0x00d0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r1.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "u-tap-size"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r0 = getURLEncoded(r0)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0168 }
        L_0x00d0:
            return r5
        L_0x00d1:
            if (r2 == 0) goto L_0x013d
            if (r1 == 0) goto L_0x013d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r3.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = "?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = "u-tap-o"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = "="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r4.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r4 = ","
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0168 }
            java.lang.String r1 = getURLEncoded(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r5 = r1.toString()     // Catch:{ Exception -> 0x0168 }
            if (r0 == 0) goto L_0x00d0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r1.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "u-tap-size"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r0 = getURLEncoded(r0)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0168 }
            goto L_0x00d0
        L_0x013d:
            if (r0 == 0) goto L_0x00d0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0168 }
            r1.<init>()     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "u-tap-size"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r2 = "="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r0 = getURLEncoded(r0)     // Catch:{ Exception -> 0x0168 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0168 }
            java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x0168 }
            goto L_0x00d0
        L_0x0168:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x00d0
        L_0x016e:
            r3 = r0
            goto L_0x002d
        L_0x0171:
            r4 = r1
            goto L_0x001e
        L_0x0174:
            r1 = r0
            r2 = r0
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.androidsdk.impl.net.IMHttpRequestBuilder.appendClickParams(java.lang.String, android.view.MotionEvent, java.lang.String):java.lang.String");
    }
}
