package com.inmobi.androidsdk.impl.net;

import android.util.Log;
import android.view.MotionEvent;
import com.adsdk.sdk.BannerAd;
import com.adsdk.sdk.Const;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.androidsdk.impl.IMAdException;
import com.inmobi.androidsdk.impl.IMAdUnit;
import com.inmobi.androidsdk.impl.IMNiceInfo;
import com.inmobi.androidsdk.impl.IMUserInfo;
import com.inmobi.androidsdk.impl.IMXMLParser;
import com.inmobi.commons.internal.IMLog;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public final class IMRequestResponseManager {
    private String a = null;
    private AtomicBoolean b = new AtomicBoolean();
    /* access modifiers changed from: private */
    public HttpURLConnection c;

    public enum ActionType {
        AdRequest,
        AdRequest_Interstitial,
        DeviceInfoUpload,
        AdClick
    }

    public void doCancel() {
        this.b.set(true);
        if (this.c != null) {
            this.c.disconnect();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, Object obj, IMHttpRequestCallback iMHttpRequestCallback) {
        if (!this.b.get() && iMHttpRequestCallback != null) {
            iMHttpRequestCallback.notifyResult(i, obj);
        }
    }

    public void asyncRequestAd(IMUserInfo iMUserInfo, IMNiceInfo iMNiceInfo, ActionType actionType, String str, String str2, IMHttpRequestCallback iMHttpRequestCallback) {
        final IMUserInfo iMUserInfo2 = iMUserInfo;
        final String str3 = str;
        final String str4 = str2;
        final ActionType actionType2 = actionType;
        final IMHttpRequestCallback iMHttpRequestCallback2 = iMHttpRequestCallback;
        final IMNiceInfo iMNiceInfo2 = iMNiceInfo;
        new Thread() {
            public void run() {
                try {
                    String a2 = IMRequestResponseManager.this.a(iMUserInfo2, str3, str4);
                    IMLog.debug(IMConstants.LOGGING_TAG, "Ad Serving URL: " + a2);
                    String a3 = IMHttpRequestBuilder.a(iMUserInfo2, actionType2);
                    Log.d(IMConstants.LOGGING_TAG, a3);
                    HttpURLConnection unused = IMRequestResponseManager.this.c = IMRequestResponseManager.this.a(a2, iMUserInfo2, actionType2);
                    IMRequestResponseManager.this.a(a3);
                    IMRequestResponseManager.this.a(0, IMRequestResponseManager.this.a(iMUserInfo2), iMHttpRequestCallback2);
                } catch (IMAdException e2) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "Exception retrieving ad ", e2);
                    switch (e2.getCode()) {
                        case 100:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.NO_FILL, iMHttpRequestCallback2);
                            break;
                        case 200:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INTERNAL_ERROR, iMHttpRequestCallback2);
                            break;
                        case IMAdException.INVALID_REQUEST:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INVALID_REQUEST, iMHttpRequestCallback2);
                            break;
                        case IMAdException.SANDBOX_OOF:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INVALID_REQUEST, iMHttpRequestCallback2);
                            break;
                        case IMAdException.SANDBOX_BADIP:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INVALID_REQUEST, iMHttpRequestCallback2);
                            break;
                        case IMAdException.SANDBOX_UAND:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INVALID_REQUEST, iMHttpRequestCallback2);
                            break;
                        case IMAdException.SANDBOX_UA:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INVALID_REQUEST, iMHttpRequestCallback2);
                            break;
                        case IMAdException.INVALID_APP_ID:
                            IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INVALID_APP_ID, iMHttpRequestCallback2);
                            break;
                    }
                } catch (IOException e3) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "Exception retrieving ad ", e3);
                    if (e3 instanceof SocketTimeoutException) {
                        IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.AD_FETCH_TIMEOUT, iMHttpRequestCallback2);
                    } else {
                        IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.NETWORK_ERROR, iMHttpRequestCallback2);
                    }
                } catch (Exception e4) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "Exception retrieving ad ", e4);
                    IMRequestResponseManager.this.a(1, IMAdRequest.ErrorCode.INTERNAL_ERROR, iMHttpRequestCallback2);
                } finally {
                    iMNiceInfo2.processNiceParams(IMRequestResponseManager.this.c);
                }
            }
        }.start();
    }

    /* access modifiers changed from: private */
    public String a(IMUserInfo iMUserInfo, String str, String str2) {
        return iMUserInfo.isTestMode() ? str2 : str;
    }

    public String initiateClick(String str, IMUserInfo iMUserInfo, MotionEvent motionEvent, List<?> list) throws IOException {
        String appendClickParams = IMHttpRequestBuilder.appendClickParams(str, motionEvent, iMUserInfo.getScreenDensity());
        IMLog.debug(IMConstants.LOGGING_TAG, ">>> Enter initiateClick, clickURL : " + appendClickParams);
        String str2 = null;
        if (list != null && !list.isEmpty() && "x-mkhoj-adactiontype".equals(list.get(0))) {
            str2 = (String) list.get(1);
        }
        HttpURLConnection a2 = a(appendClickParams, iMUserInfo, ActionType.AdClick);
        if (str2 != null && !iMUserInfo.isTestMode()) {
            a2.setRequestProperty("x-mkhoj-adactionType", str2);
        }
        return a(a2, appendClickParams);
    }

    /* access modifiers changed from: private */
    public HttpURLConnection a(String str, IMUserInfo iMUserInfo, ActionType actionType) throws IOException {
        this.c = (HttpURLConnection) new URL(str).openConnection();
        a(this.c, iMUserInfo, actionType);
        return this.c;
    }

    /* access modifiers changed from: private */
    public void a(String str) throws IOException {
        BufferedWriter bufferedWriter;
        this.c.setRequestProperty("Content-Length", Integer.toString(str.length()));
        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.c.getOutputStream()));
            try {
                bufferedWriter.write(str);
                a(bufferedWriter);
            } catch (Throwable th) {
                th = th;
                a(bufferedWriter);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            bufferedWriter = null;
            a(bufferedWriter);
            throw th;
        }
    }

    private void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Exception closing resource: " + closeable, e);
            }
        }
    }

    private static void a(HttpURLConnection httpURLConnection, IMUserInfo iMUserInfo, ActionType actionType) throws ProtocolException {
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setConnectTimeout(60000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setRequestProperty("user-agent", iMUserInfo.getPhoneDefaultUserAgent());
        httpURLConnection.setRequestProperty("x-mkhoj-testmode", iMUserInfo.isTestMode() ? "YES" : "NO");
        httpURLConnection.setUseCaches(false);
        if (actionType == ActionType.AdClick) {
            httpURLConnection.setRequestMethod("GET");
        } else {
            httpURLConnection.setRequestMethod("POST");
        }
        httpURLConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
        if (iMUserInfo.isTestMode()) {
            httpURLConnection.setRequestProperty("x-mkhoj-adactiontype", iMUserInfo.getTestModeAdActionType() != null ? iMUserInfo.getTestModeAdActionType() : BannerAd.WEB);
        }
    }

    /* access modifiers changed from: private */
    public IMAdUnit a(IMUserInfo iMUserInfo) throws IMAdException, IOException {
        BufferedReader bufferedReader;
        List list;
        String str = null;
        IMLog.debug(IMConstants.LOGGING_TAG, "Http Status Code: " + this.c.getResponseCode());
        if (this.c.getResponseCode() == 200) {
            try {
                bufferedReader = new BufferedReader(new InputStreamReader(this.c.getInputStream(), Const.ENCODING));
                try {
                    StringBuilder sb = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        sb.append(readLine).append("\n");
                    }
                    String sb2 = sb.toString();
                    Log.d(IMConstants.LOGGING_TAG, "Ad Response: " + sb2);
                    Map<String, List<String>> headerFields = this.c.getHeaderFields();
                    if (headerFields != null && headerFields.containsKey("x-mkhoj-ph") && (list = headerFields.get("x-mkhoj-ph")) != null && list.size() == 1) {
                        str = ((String) list.get(0)).trim();
                    }
                    IMAdUnit buildAdUnitFromResponseData = IMXMLParser.buildAdUnitFromResponseData(new BufferedReader(new InputStreamReader(new ByteArrayInputStream(sb2.getBytes()), Const.ENCODING)));
                    IMLog.debug(IMConstants.LOGGING_TAG, "Retrieved AdUnit: " + buildAdUnitFromResponseData);
                    a(buildAdUnitFromResponseData, iMUserInfo, str);
                    this.c.disconnect();
                    a(bufferedReader);
                    return buildAdUnitFromResponseData;
                } catch (Throwable th) {
                    th = th;
                    this.c.disconnect();
                    a(bufferedReader);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedReader = null;
                this.c.disconnect();
                a(bufferedReader);
                throw th;
            }
        } else {
            IMLog.debug(IMConstants.LOGGING_TAG, "Invalid Request. This may be because of invalid appId or appId might not be in 'active' state.");
            throw new IMAdException("Server did not return 200.", IMAdException.INVALID_REQUEST);
        }
    }

    private void a(IMAdUnit iMAdUnit, IMUserInfo iMUserInfo, String str) {
        if (iMAdUnit != null) {
            iMAdUnit.setSendDeviceInfo(true);
            iMAdUnit.setDeviceInfoUploadUrl(str);
        }
    }

    private String a(HttpURLConnection httpURLConnection, String str) {
        String str2;
        List list;
        String str3 = null;
        HttpURLConnection.setFollowRedirects(false);
        try {
            IMLog.debug(IMConstants.LOGGING_TAG, "HTTP Response Code: " + httpURLConnection.getResponseCode());
            if (0 == 0 || str.equalsIgnoreCase(null)) {
                str3 = httpURLConnection.getURL().toString();
            }
        } catch (IOException e) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception getting response code for redirection URL", e);
        }
        if (str3 == null || str.equalsIgnoreCase(str3)) {
            str2 = httpURLConnection.getHeaderField("location");
        } else {
            str2 = str3;
        }
        Map<String, List<String>> headerFields = httpURLConnection.getHeaderFields();
        if (headerFields != null && headerFields.containsKey("action-name") && (list = headerFields.get("action-name")) != null && list.size() == 1) {
            setNewAdActionType(((String) list.get(0)).trim());
        }
        IMLog.debug(IMConstants.LOGGING_TAG, "Redirection URL: " + str2);
        return str2;
    }

    public String getNewAdActionType() {
        return this.a;
    }

    public void setNewAdActionType(String str) {
        this.a = str;
    }
}
