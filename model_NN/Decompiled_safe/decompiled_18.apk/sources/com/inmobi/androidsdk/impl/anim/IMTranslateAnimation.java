package com.inmobi.androidsdk.impl.anim;

import android.view.animation.Animation;
import android.view.animation.Transformation;

public class IMTranslateAnimation extends Animation {
    private int a = 0;
    private int b = 0;
    private int c = 0;
    private int d = 0;
    private float e = 0.0f;
    private float f = 0.0f;
    private float g = 0.0f;
    private float h = 0.0f;
    private float i;
    private float j;
    private float k;
    private float l;

    public IMTranslateAnimation(float f2, float f3, float f4, float f5) {
        this.e = f2;
        this.f = f3;
        this.g = f4;
        this.h = f5;
        this.a = 0;
        this.b = 0;
        this.c = 0;
        this.d = 0;
    }

    public IMTranslateAnimation(int i2, float f2, int i3, float f3, int i4, float f4, int i5, float f5) {
        this.e = f2;
        this.f = f3;
        this.g = f4;
        this.h = f5;
        this.a = i2;
        this.b = i3;
        this.c = i4;
        this.d = i5;
    }

    public void initialize(int i2, int i3, int i4, int i5) {
        super.initialize(i2, i3, i4, i5);
        this.i = resolveSize(this.a, this.e, i2, i4);
        this.j = resolveSize(this.b, this.f, i2, i4);
        this.k = resolveSize(this.c, this.g, i3, i5);
        this.l = resolveSize(this.d, this.h, i3, i5);
    }

    /* access modifiers changed from: protected */
    public void applyTransformation(float f2, Transformation transformation) {
        float f3 = this.i;
        float f4 = this.k;
        if (this.i != this.j) {
            f3 = this.i + ((this.j - this.i) * f2);
        }
        if (this.k != this.l) {
            f4 = this.k + ((this.l - this.k) * f2);
        }
        transformation.getMatrix().setTranslate(f3, f4);
    }
}
