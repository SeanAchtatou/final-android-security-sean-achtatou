package com.inmobi.androidsdk.impl;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.view.MotionEvent;
import com.adsdk.sdk.Const;
import com.inmobi.androidsdk.IMBrowserActivity;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.androidsdk.impl.IMAdUnit;
import com.inmobi.androidsdk.impl.net.IMHttpRequestBuilder;
import com.inmobi.androidsdk.impl.net.IMRequestResponseManager;
import com.inmobi.commons.internal.IMLog;
import java.util.ArrayList;

public class IMClickProcessingTask extends AsyncTask<Void, Void, String> {
    private final IMAdUnit a;
    private final IMUserInfo b;
    private final Context c;
    private Message d;
    private Message e;
    private Message f;
    private IMWebView.IMWebViewListener g;
    private MotionEvent h;

    public IMClickProcessingTask(IMAdUnit iMAdUnit, IMUserInfo iMUserInfo, Context context, MotionEvent motionEvent, Message message, Message message2, Message message3, IMWebView.IMWebViewListener iMWebViewListener) {
        this.a = iMAdUnit;
        this.b = iMUserInfo;
        this.c = context;
        this.d = message;
        this.e = message2;
        this.f = message3;
        this.g = iMWebViewListener;
        this.h = motionEvent;
    }

    /* access modifiers changed from: protected */
    public String doInBackground(Void... voidArr) {
        Exception e2;
        String str;
        try {
            IMRequestResponseManager iMRequestResponseManager = new IMRequestResponseManager();
            ArrayList arrayList = new ArrayList();
            arrayList.add("x-mkhoj-adactiontype");
            arrayList.add(this.a.getAdActionName().toString());
            if (this.a.getAdType().equals(IMAdUnit.AdTypes.SEARCH)) {
                this.h = null;
            }
            str = iMRequestResponseManager.initiateClick(this.a.getTargetUrl(), this.b, this.h, arrayList);
            try {
                String newAdActionType = iMRequestResponseManager.getNewAdActionType();
                if (newAdActionType != null) {
                    this.a.setAdActionName(IMAdUnit.adActionNamefromString(newAdActionType));
                }
            } catch (Exception e3) {
                e2 = e3;
                IMLog.debug(IMConstants.LOGGING_TAG, "Encountered generic exception initiating click", e2);
                return str;
            }
        } catch (Exception e4) {
            Exception exc = e4;
            str = null;
            e2 = exc;
            IMLog.debug(IMConstants.LOGGING_TAG, "Encountered generic exception initiating click", e2);
            return str;
        }
        return str;
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String str) {
        a(str);
        try {
            if (this.d != null) {
                this.d.sendToTarget();
            }
        } catch (Exception e2) {
        }
    }

    private void a(String str) {
        if (str != null) {
            try {
                IMLog.debug(IMConstants.LOGGING_TAG, "Click target URL: " + str);
                IMLog.debug(IMConstants.LOGGING_TAG, "AdActionName: " + this.a.getAdActionName());
                if (this.a.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_SMS) {
                    b(str);
                    if (this.f != null) {
                        this.f.sendToTarget();
                    }
                } else if (this.a.getAdActionName() != IMAdUnit.AdActionNames.AdActionName_Web && this.a.getAdActionName() != IMAdUnit.AdActionNames.AdActionName_Search) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    intent.addFlags(268435456);
                    this.c.startActivity(intent);
                    if (this.f != null) {
                        this.f.sendToTarget();
                    }
                } else if (!str.startsWith("http://") || str.contains("play.google.com") || str.contains("market.android.com")) {
                    Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    intent2.addFlags(268435456);
                    this.c.startActivity(intent2);
                    if (this.f != null) {
                        this.f.sendToTarget();
                    }
                } else {
                    Intent intent3 = new Intent(this.c, IMBrowserActivity.class);
                    intent3.putExtra(IMBrowserActivity.EXTRA_URL, str);
                    intent3.putExtra(IMBrowserActivity.EXTRA_BROWSER_ACTIVITY_TYPE, (int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR);
                    intent3.putExtra("FIRST_INSTANCE", "yes");
                    IMBrowserActivity.setWebViewListener(this.g);
                    this.c.startActivity(intent3);
                    if (this.e != null) {
                        this.e.sendToTarget();
                    }
                }
            } catch (ActivityNotFoundException e2) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Operation could not be performed : " + str, e2);
            } catch (Exception e3) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Error executing post click actions on URL : " + str, e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void b(String str) {
        String str2 = null;
        int indexOf = str.indexOf("&Body=", 0);
        if (indexOf > 0) {
            str2 = str.substring("&Body=".length() + indexOf);
            str = str.substring(0, indexOf);
        }
        Intent intent = new Intent("android.intent.action.SENDTO", Uri.parse(str));
        intent.addFlags(268435456);
        intent.putExtra("compose_mode", true);
        if (str2 != null) {
            intent.putExtra("sms_body", IMHttpRequestBuilder.getURLDecoded(str2, Const.ENCODING));
        }
        this.c.startActivity(intent);
    }
}
