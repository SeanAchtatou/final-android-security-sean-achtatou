package com.inmobi.androidsdk.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.inmobi.commons.internal.IMLog;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

public class IMNiceInfo {
    /* access modifiers changed from: private */
    public List<ScanResult> a = null;
    private Context b = null;
    private String c = null;
    /* access modifiers changed from: private */
    public Long d = 0L;
    /* access modifiers changed from: private */
    public Long e = 0L;
    /* access modifiers changed from: private */
    public AtomicLong f;
    private IMUserInfo g = null;
    /* access modifiers changed from: private */
    public a h = null;

    public IMNiceInfo(Context context, IMUserInfo iMUserInfo) {
        this.b = context;
        this.g = iMUserInfo;
        scanWifiAP();
        this.f = new AtomicLong(0);
    }

    public String getCellInfo() {
        String str;
        if (!a()) {
            IMLog.internal("3.6.0", "Access coarse permission not granted.Cant collect Cell Info");
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService("phone");
            String networkOperator = telephonyManager.getNetworkOperator();
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (!(cellLocation == null || networkOperator == null || networkOperator.equals(""))) {
                String substring = networkOperator.substring(0, 3);
                String substring2 = networkOperator.substring(3);
                if (cellLocation instanceof CdmaCellLocation) {
                    jSONObject.put("a1", 2);
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                    int networkId = cdmaCellLocation.getNetworkId();
                    int baseStationId = cdmaCellLocation.getBaseStationId();
                    int systemId = cdmaCellLocation.getSystemId();
                    if (!(networkId == -1 || baseStationId == -1 || systemId == -1)) {
                        jSONObject.put("a2", substring + "-" + substring2 + "-" + Integer.toHexString(networkId) + "-" + Integer.toHexString(baseStationId) + "-" + Integer.toHexString(systemId));
                    }
                } else {
                    jSONObject.put("a1", 1);
                    GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                    int cid = gsmCellLocation.getCid();
                    int lac = gsmCellLocation.getLac();
                    if (!(cid == -1 || lac == -1)) {
                        jSONObject.put("a2", substring + "-" + substring2 + "-" + Integer.toHexString(lac) + "-" + Integer.toHexString(cid));
                    }
                }
                if (jSONObject.length() != 0) {
                    str = new JSONObject().put("a", jSONObject).toString();
                    return str;
                }
            }
            str = null;
            return str;
        } catch (Exception e2) {
            IMLog.internal("3.6.0", "Error Getting NICE param Cell Info", e2);
            return null;
        }
    }

    public String getSimInfo() {
        String str;
        if (!a()) {
            IMLog.internal("3.6.0", "Access coarse permission not granted.Cant collect Sim Info");
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            String simOperator = ((TelephonyManager) this.b.getSystemService("phone")).getSimOperator();
            if (simOperator != null && !simOperator.equals("")) {
                jSONObject.put("b1", simOperator.substring(0, 3) + "-" + simOperator.substring(3));
            }
            if (jSONObject.length() != 0) {
                str = new JSONObject().put("b", jSONObject).toString();
            } else {
                str = null;
            }
            return str;
        } catch (Exception e2) {
            IMLog.internal("3.6.0", "Error Getting NICE Param Sim Info", e2);
            return null;
        }
    }

    public void scanWifiAP() {
        if (!b()) {
            IMLog.internal("3.6.0", "Access wifi permission not granted.Cant collect scan wifi Info");
            return;
        }
        try {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.net.wifi.SCAN_RESULTS");
            this.b.registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    List unused = IMNiceInfo.this.a = ((WifiManager) context.getSystemService("wifi")).getScanResults();
                }
            }, intentFilter);
            WifiManager wifiManager = (WifiManager) this.b.getSystemService("wifi");
            this.a = wifiManager.getScanResults();
            if (this.a == null) {
                wifiManager.startScan();
            }
        } catch (Exception e2) {
            IMLog.internal("3.6.0", "Error Setting Wifi Apian", e2);
        }
    }

    public String getWifiInfo() {
        String str;
        if (!b()) {
            IMLog.internal("3.6.0", "Access wifi permission not granted.Cant collect get wifi access point Info");
            return null;
        }
        try {
            JSONArray jSONArray = new JSONArray();
            if (this.a != null) {
                for (int i = 0; i < this.a.size(); i++) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("c1a", this.a.get(i).SSID);
                    jSONObject.put("c1b", this.a.get(i).BSSID);
                    jSONObject.put("c1c", this.a.get(i).level);
                    jSONArray.put(i, jSONObject);
                }
            }
            if (jSONArray.length() != 0) {
                str = new JSONObject().put("c", jSONArray).toString();
            } else {
                str = null;
            }
            return str;
        } catch (Exception e2) {
            IMLog.internal("3.6.0", "Error Getting NICE Param Wifi Apian", e2);
            return null;
        }
    }

    public String getTimeStamp() {
        try {
            JSONObject jSONObject = new JSONObject();
            Calendar instance = Calendar.getInstance();
            long timeInMillis = instance.getTimeInMillis();
            int i = instance.get(15);
            jSONObject.put("d1", timeInMillis);
            jSONObject.put("d2", i);
            if (jSONObject.length() != 0) {
                return new JSONObject().put("d", jSONObject).toString();
            }
            return null;
        } catch (Exception e2) {
            IMLog.internal("3.6.0", "Error Getting NICE param Timestamp", e2);
            return null;
        }
    }

    private boolean a() {
        if (this.b.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            return true;
        }
        return false;
    }

    private boolean b() {
        if (this.b.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            return true;
        }
        return false;
    }

    static class a extends Handler {
        private final WeakReference<IMNiceInfo> a;

        public a(IMNiceInfo iMNiceInfo) {
            this.a = new WeakReference<>(iMNiceInfo);
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (this.a.get().f.get() > 0) {
                        this.a.get().c();
                        this.a.get().f.set(this.a.get().f.get() - 1);
                        sendEmptyMessageDelayed(1, this.a.get().d.longValue() * 1000);
                        break;
                    }
                    break;
            }
            super.handleMessage(message);
        }
    }

    public void processNiceParams(HttpURLConnection httpURLConnection) {
        if (httpURLConnection == null) {
            try {
                IMLog.internal("3.6.0", "HTTP Connection lost.Cannot retreive nice url");
            } catch (Exception e2) {
                IMLog.internal("3.6.0", "Failed to process NICE params", e2);
            }
        } else if (true == Boolean.valueOf(Boolean.parseBoolean(httpURLConnection.getHeaderField("x-inmobi-ph-enable"))).booleanValue()) {
            String headerField = httpURLConnection.getHeaderField("x-inmobi-ph-url");
            Long valueOf = Long.valueOf(Long.parseLong(httpURLConnection.getHeaderField("x-inmobi-ph-intvl-sec")));
            Long valueOf2 = Long.valueOf(Long.parseLong(httpURLConnection.getHeaderField("x-inmobi-ph-lse-sec")));
            if (valueOf.longValue() < IMConfigConstants.MIN_NICE_RETRY_INERVAL.longValue()) {
                valueOf = IMConfigConstants.MIN_NICE_RETRY_INERVAL;
            }
            if (!headerField.equals(this.c) || !valueOf2.toString().equals(this.e.toString()) || !valueOf.toString().equals(this.d.toString()) || this.f.get() == 0) {
                this.c = headerField;
                this.d = valueOf;
                this.e = valueOf2;
                IMLog.internal("3.6.0", "NICE URL: " + this.c);
                new Thread(new Runnable() {
                    public void run() {
                        Looper.prepare();
                        if (IMNiceInfo.this.h != null) {
                            IMNiceInfo.this.h.removeMessages(1);
                        } else {
                            a unused = IMNiceInfo.this.h = new a(IMNiceInfo.this);
                        }
                        if (IMNiceInfo.this.d.longValue() != 0 && IMNiceInfo.this.e.longValue() != 0) {
                            IMNiceInfo.this.f.set(IMNiceInfo.this.e.longValue() / IMNiceInfo.this.d.longValue());
                            IMNiceInfo.this.h.sendEmptyMessage(1);
                            Looper.loop();
                        }
                    }
                }).start();
            }
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(this.c);
            List<NameValuePair> d2 = d();
            if (d2 != null) {
                IMLog.internal("3.6.0", "NICE Param: " + d2);
                httpPost.setEntity(new UrlEncodedFormEntity(d2));
                if (defaultHttpClient.execute(httpPost).getStatusLine().getStatusCode() == 200) {
                    IMLog.internal("3.6.0", "NICE params posted Successfully");
                    return;
                }
                return;
            }
            IMLog.internal("3.6.0", "NICE Params not present");
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private List<NameValuePair> d() {
        if (this.g == null) {
            IMLog.internal("3.6.0", "User Info Not initialised");
            return null;
        }
        ArrayList arrayList = new ArrayList(4);
        String cellInfo = getCellInfo();
        String simInfo = getSimInfo();
        String wifiInfo = getWifiInfo();
        String timeStamp = getTimeStamp();
        if (cellInfo != null) {
            arrayList.add(new BasicNameValuePair("d-n-cell", cellInfo));
        }
        if (simInfo != null) {
            arrayList.add(new BasicNameValuePair("d-n-sim", simInfo));
        }
        if (wifiInfo != null) {
            arrayList.add(new BasicNameValuePair("d-n-wifi", wifiInfo));
        }
        if (!((simInfo == null && cellInfo == null && wifiInfo == null) || timeStamp == null)) {
            arrayList.add(new BasicNameValuePair("d-n-time", timeStamp));
            String uIDMapEncrypted = this.g.getUIDMapEncrypted();
            String randomKey = this.g.getRandomKey();
            String rsakeyVersion = this.g.getRsakeyVersion();
            if (!(uIDMapEncrypted == null || randomKey == null || rsakeyVersion == null)) {
                arrayList.add(new BasicNameValuePair("u-id-map", uIDMapEncrypted));
                arrayList.add(new BasicNameValuePair("u-id-key", randomKey));
                arrayList.add(new BasicNameValuePair("u-key-ver", rsakeyVersion));
            }
        }
        if (!arrayList.isEmpty()) {
            return arrayList;
        }
        return null;
    }
}
