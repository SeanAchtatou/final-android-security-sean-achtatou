package com.inmobi.androidsdk.impl;

import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.internal.IMLog;
import java.io.IOException;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public final class IMXMLParser {
    private static String a = "AdResponse";
    private static String b = "Ad";

    public static IMAdUnit buildAdUnitFromResponseData(Reader reader) throws IOException, IMAdException {
        boolean z;
        String str;
        String str2;
        IMAdUnit iMAdUnit = new IMAdUnit();
        try {
            XmlPullParserFactory newInstance = XmlPullParserFactory.newInstance();
            newInstance.setNamespaceAware(true);
            XmlPullParser newPullParser = newInstance.newPullParser();
            newPullParser.setInput(reader);
            String str3 = null;
            int eventType = newPullParser.getEventType();
            boolean z2 = false;
            boolean z3 = false;
            String str4 = null;
            while (eventType != 1) {
                if (eventType == 0) {
                    z = z3;
                    str = str4;
                    str2 = str3;
                } else {
                    if (eventType == 2) {
                        str3 = newPullParser.getName();
                        if (str3 != null) {
                            z3 = true;
                        }
                        if (str3 != null && str3.equalsIgnoreCase(b)) {
                            iMAdUnit.setWidth(Integer.parseInt(newPullParser.getAttributeValue(null, "width")));
                            iMAdUnit.setHeight(Integer.parseInt(newPullParser.getAttributeValue(null, "height")));
                            iMAdUnit.setAdActionName(IMAdUnit.adActionNamefromString(newPullParser.getAttributeValue(null, "actionName")));
                            iMAdUnit.setAdType(IMAdUnit.adTypefromString(newPullParser.getAttributeValue(null, "type")));
                            z = z3;
                            z2 = true;
                            str = newPullParser.getAttributeValue(null, "errorcode");
                            str2 = str3;
                        }
                    } else if (eventType == 3) {
                        z = z3;
                        str = str4;
                        str2 = null;
                    } else if (eventType == 5) {
                        iMAdUnit.setCDATABlock(newPullParser.getText());
                        z = z3;
                        str = str4;
                        str2 = str3;
                    } else if (eventType == 4 && str3 != null && str3.equalsIgnoreCase("AdURL")) {
                        iMAdUnit.setTargetUrl(newPullParser.getText());
                        iMAdUnit.setDefaultTargetUrl(newPullParser.getText());
                    }
                    z = z3;
                    str = str4;
                    str2 = str3;
                }
                String str5 = str;
                z3 = z;
                eventType = newPullParser.nextToken();
                str3 = str2;
                str4 = str5;
            }
            if (!z3) {
                throw new IMAdException("App Id may be Invalid or Inactive", IMAdException.INVALID_APP_ID);
            } else if (!z2) {
                throw new IMAdException("No Ads present", 100);
            } else {
                if (str4 != null) {
                    if (str4.equals("OOF")) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "IP Address not found in CCID File");
                        throw new IMAdException("IP Address not found in CCID File", IMAdException.SANDBOX_OOF);
                    } else if (str4.equals("BADIP")) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "Invalid IP Address");
                        throw new IMAdException("Invalid IP Address", IMAdException.SANDBOX_BADIP);
                    } else if (str4.equals("UAND")) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "User Agent not detected through using wurfl");
                        throw new IMAdException("User Agent not detected through using wurfl", IMAdException.SANDBOX_UAND);
                    } else if (str4.equals("-UA")) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "No User Agent found");
                        throw new IMAdException("No User Agent found", IMAdException.SANDBOX_UA);
                    }
                }
                return iMAdUnit;
            }
        } catch (XmlPullParserException e) {
            throw new IMAdException("Exception constructing Ad", e, 200);
        }
    }
}
