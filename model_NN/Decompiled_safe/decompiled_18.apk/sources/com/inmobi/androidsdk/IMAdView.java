package com.inmobi.androidsdk;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adfonic.android.utils.HtmlFormatter;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWrapperFunctions;
import com.inmobi.androidsdk.ai.controller.util.IMConfigException;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.androidsdk.ai.controller.util.IMSDKUtil;
import com.inmobi.androidsdk.impl.IMAdUnit;
import com.inmobi.androidsdk.impl.IMClickProcessingTask;
import com.inmobi.androidsdk.impl.IMConfigConstants;
import com.inmobi.androidsdk.impl.IMNiceInfo;
import com.inmobi.androidsdk.impl.IMUserInfo;
import com.inmobi.androidsdk.impl.net.IMHttpRequestCallback;
import com.inmobi.androidsdk.impl.net.IMRequestResponseManager;
import com.inmobi.commons.internal.IMLog;
import com.inmobi.commons.internal.InternalSDKUtil;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public final class IMAdView extends RelativeLayout {
    public static final int INMOBI_AD_UNIT_120X600 = 13;
    public static final int INMOBI_AD_UNIT_300X250 = 10;
    public static final int INMOBI_AD_UNIT_320X48 = 9;
    public static final int INMOBI_AD_UNIT_320X50 = 15;
    public static final int INMOBI_AD_UNIT_468X60 = 12;
    public static final int INMOBI_AD_UNIT_728X90 = 11;
    public static final String INMOBI_INTERNAL_TAG = "ref-__in__rt";
    public static final String INMOBI_REF_TAG = "ref-tag";
    public static final int REFRESH_INTERVAL_DEFAULT = 60;
    public static final int REFRESH_INTERVAL_OFF = -1;
    /* access modifiers changed from: private */
    public a A;
    private String B;
    private String C;
    private String D;
    /* access modifiers changed from: private */
    public IMRequestResponseManager E;
    /* access modifiers changed from: private */
    public a F;
    private View.OnTouchListener G;
    private Animation.AnimationListener H;
    private IMHttpRequestCallback I;
    private IMWebView.IMWebViewListener J;
    /* access modifiers changed from: private */
    public int a;
    /* access modifiers changed from: private */
    public IMWebView b;
    /* access modifiers changed from: private */
    public IMWebView c;
    /* access modifiers changed from: private */
    public LinearLayout d;
    /* access modifiers changed from: private */
    public Activity e;
    private boolean f;
    /* access modifiers changed from: private */
    public IMUserInfo g;
    private IMNiceInfo h;
    private AtomicBoolean i;
    private AtomicBoolean j;
    private Animation k;
    private Animation l;
    /* access modifiers changed from: private */
    public IMAdUnit m;
    private String n;
    private String o;
    private String p;
    /* access modifiers changed from: private */
    public IMAdListener q;
    private IMAdRequest r;
    private String s;
    private int t;
    private long u;
    private boolean v;
    /* access modifiers changed from: private */
    public long w;
    /* access modifiers changed from: private */
    public boolean x;
    /* access modifiers changed from: private */
    public AnimationType y;
    private boolean z;

    public enum AnimationType {
        ANIMATION_OFF,
        ROTATE_HORIZONTAL_AXIS,
        ANIMATION_ALPHA,
        ROTATE_VERTICAL_AXIS
    }

    private IMAdView(Context context) {
        super(context);
        this.a = 60;
        this.f = true;
        this.i = new AtomicBoolean();
        this.j = new AtomicBoolean();
        this.m = null;
        this.u = -1;
        this.w = 0;
        this.x = true;
        this.y = AnimationType.ROTATE_HORIZONTAL_AXIS;
        this.z = true;
        this.B = "http://i.w.inmobi.com/showad.asm";
        this.C = "http://i.w.sandbox.inmobi.com/showad.asm";
        this.F = new a(this);
        this.G = new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                IMLog.debug(IMConstants.LOGGING_TAG, "onTouch: view: " + view + ", event: " + motionEvent);
                if (IMAdView.this.b != null && view.equals(IMAdView.this.b)) {
                    view.requestFocusFromTouch();
                } else if (IMAdView.this.c != null && view.equals(IMAdView.this.c)) {
                    view.requestFocusFromTouch();
                } else if (motionEvent.getAction() == 1) {
                    IMAdView.this.k();
                    IMAdView.this.a(motionEvent);
                } else if (motionEvent.getAction() == 0) {
                    if (IMAdView.this.h() || IMAdView.this.g() || IMAdView.this.m == null) {
                        IMAdView.this.k();
                    } else {
                        IMAdView.this.j();
                    }
                } else if (motionEvent.getAction() == 3) {
                    IMAdView.this.k();
                } else if (motionEvent.getAction() == 4) {
                    IMAdView.this.k();
                }
                return true;
            }
        };
        this.H = new Animation.AnimationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, boolean):void
             arg types: [com.inmobi.androidsdk.IMAdView, int]
             candidates:
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, long):long
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.ai.container.IMWebView):com.inmobi.androidsdk.ai.container.IMWebView
              com.inmobi.androidsdk.IMAdView.a(int, com.inmobi.androidsdk.IMAdRequest$ErrorCode):void
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, android.view.MotionEvent):void
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.impl.IMAdUnit):void
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, boolean):void */
            public void onAnimationEnd(Animation animation) {
                boolean z = false;
                try {
                    if (animation.equals(IMAdView.this.a())) {
                        IMAdView.this.removeAllViews();
                        if (IMAdView.this.i()) {
                            IMAdView.this.addView(IMAdView.this.b);
                            if (IMAdView.this.m != null && IMAdView.this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
                                IMAdView.this.b.requestFocusFromTouch();
                            }
                        } else {
                            IMAdView.this.addView(IMAdView.this.c);
                            if (IMAdView.this.m != null && IMAdView.this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
                                IMAdView.this.c.requestFocusFromTouch();
                            }
                        }
                        if (!(IMAdView.this.m.getAdType() == IMAdUnit.AdTypes.RICH_MEDIA || IMAdView.this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search)) {
                            IMAdView.this.addView(IMAdView.this.d);
                        }
                        IMAdView.this.startAnimation(IMAdView.this.b());
                        return;
                    }
                    IMAdView iMAdView = IMAdView.this;
                    if (!IMAdView.this.i()) {
                        z = true;
                    }
                    iMAdView.c(z);
                    IMAdView.this.a(false);
                    IMAdView.this.k();
                } catch (Exception e) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "Error animating banner ads", e);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        };
        this.I = new IMHttpRequestCallback() {
            public void notifyResult(int i, Object obj) {
                IMLog.debug(IMConstants.LOGGING_TAG, ">>> Got HTTP REQUEST callback. Status: " + i + " ,data=" + obj);
                if (i == 0) {
                    IMAdUnit unused = IMAdView.this.m = (IMAdUnit) obj;
                    IMAdView.this.F.sendEmptyMessage(109);
                } else if (i == 1) {
                    Message obtainMessage = IMAdView.this.F.obtainMessage(110);
                    obtainMessage.obj = obj;
                    obtainMessage.sendToTarget();
                }
            }
        };
        this.J = new IMWebView.IMWebViewListener() {
            public void onExpandClose() {
                IMAdView.this.a(103, (IMAdRequest.ErrorCode) null);
            }

            public void onExpand() {
                IMAdView.this.a(102, (IMAdRequest.ErrorCode) null);
            }

            public void onLeaveApplication() {
                IMAdView.this.a(104, (IMAdRequest.ErrorCode) null);
            }

            public void onError() {
                IMLog.debug(IMConstants.LOGGING_TAG, "Error loading ad ");
                IMAdView.this.F.sendEmptyMessage(111);
            }
        };
    }

    public IMAdView(Activity activity, int i2, String str) {
        this(activity);
        this.D = "http://localhost/" + Integer.toString(InternalSDKUtil.incrementBaseUrl()) + "/";
        a(activity, i2, str);
    }

    public IMAdView(Activity activity, int i2, String str, long j2) {
        this(activity);
        this.D = "http://localhost/" + Integer.toString(InternalSDKUtil.incrementBaseUrl()) + "/";
        this.u = j2;
        a(activity, i2, str);
    }

    public IMAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.a = 60;
        this.f = true;
        this.i = new AtomicBoolean();
        this.j = new AtomicBoolean();
        this.m = null;
        this.u = -1;
        this.w = 0;
        this.x = true;
        this.y = AnimationType.ROTATE_HORIZONTAL_AXIS;
        this.z = true;
        this.B = "http://i.w.inmobi.com/showad.asm";
        this.C = "http://i.w.sandbox.inmobi.com/showad.asm";
        this.F = new a(this);
        this.G = new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                IMLog.debug(IMConstants.LOGGING_TAG, "onTouch: view: " + view + ", event: " + motionEvent);
                if (IMAdView.this.b != null && view.equals(IMAdView.this.b)) {
                    view.requestFocusFromTouch();
                } else if (IMAdView.this.c != null && view.equals(IMAdView.this.c)) {
                    view.requestFocusFromTouch();
                } else if (motionEvent.getAction() == 1) {
                    IMAdView.this.k();
                    IMAdView.this.a(motionEvent);
                } else if (motionEvent.getAction() == 0) {
                    if (IMAdView.this.h() || IMAdView.this.g() || IMAdView.this.m == null) {
                        IMAdView.this.k();
                    } else {
                        IMAdView.this.j();
                    }
                } else if (motionEvent.getAction() == 3) {
                    IMAdView.this.k();
                } else if (motionEvent.getAction() == 4) {
                    IMAdView.this.k();
                }
                return true;
            }
        };
        this.H = new Animation.AnimationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, boolean):void
             arg types: [com.inmobi.androidsdk.IMAdView, int]
             candidates:
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, long):long
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.ai.container.IMWebView):com.inmobi.androidsdk.ai.container.IMWebView
              com.inmobi.androidsdk.IMAdView.a(int, com.inmobi.androidsdk.IMAdRequest$ErrorCode):void
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, android.view.MotionEvent):void
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.impl.IMAdUnit):void
              com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, boolean):void */
            public void onAnimationEnd(Animation animation) {
                boolean z = false;
                try {
                    if (animation.equals(IMAdView.this.a())) {
                        IMAdView.this.removeAllViews();
                        if (IMAdView.this.i()) {
                            IMAdView.this.addView(IMAdView.this.b);
                            if (IMAdView.this.m != null && IMAdView.this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
                                IMAdView.this.b.requestFocusFromTouch();
                            }
                        } else {
                            IMAdView.this.addView(IMAdView.this.c);
                            if (IMAdView.this.m != null && IMAdView.this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
                                IMAdView.this.c.requestFocusFromTouch();
                            }
                        }
                        if (!(IMAdView.this.m.getAdType() == IMAdUnit.AdTypes.RICH_MEDIA || IMAdView.this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search)) {
                            IMAdView.this.addView(IMAdView.this.d);
                        }
                        IMAdView.this.startAnimation(IMAdView.this.b());
                        return;
                    }
                    IMAdView iMAdView = IMAdView.this;
                    if (!IMAdView.this.i()) {
                        z = true;
                    }
                    iMAdView.c(z);
                    IMAdView.this.a(false);
                    IMAdView.this.k();
                } catch (Exception e) {
                    IMLog.debug(IMConstants.LOGGING_TAG, "Error animating banner ads", e);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        };
        this.I = new IMHttpRequestCallback() {
            public void notifyResult(int i, Object obj) {
                IMLog.debug(IMConstants.LOGGING_TAG, ">>> Got HTTP REQUEST callback. Status: " + i + " ,data=" + obj);
                if (i == 0) {
                    IMAdUnit unused = IMAdView.this.m = (IMAdUnit) obj;
                    IMAdView.this.F.sendEmptyMessage(109);
                } else if (i == 1) {
                    Message obtainMessage = IMAdView.this.F.obtainMessage(110);
                    obtainMessage.obj = obj;
                    obtainMessage.sendToTarget();
                }
            }
        };
        this.J = new IMWebView.IMWebViewListener() {
            public void onExpandClose() {
                IMAdView.this.a(103, (IMAdRequest.ErrorCode) null);
            }

            public void onExpand() {
                IMAdView.this.a(102, (IMAdRequest.ErrorCode) null);
            }

            public void onLeaveApplication() {
                IMAdView.this.a(104, (IMAdRequest.ErrorCode) null);
            }

            public void onError() {
                IMLog.debug(IMConstants.LOGGING_TAG, "Error loading ad ");
                IMAdView.this.F.sendEmptyMessage(111);
            }
        };
        this.D = "http://localhost/" + Integer.toString(InternalSDKUtil.incrementBaseUrl()) + "/";
        IMLog.debug(IMConstants.LOGGING_TAG, "IMAdView Constructor context: " + context);
        String attributeValue = attributeSet.getAttributeValue(null, "appId");
        int attributeIntValue = attributeSet.getAttributeIntValue(null, "adSize", -1);
        String attributeValue2 = attributeSet.getAttributeValue(null, "slotId");
        if (attributeValue2 != null) {
            this.u = Long.parseLong(attributeValue2);
        }
        a((Activity) context, attributeIntValue, attributeValue);
    }

    public void setRefTagParam(String str, String str2) {
        if (str == null || str2 == null) {
            throw new NullPointerException(IMConfigConstants.MSG_NIL_KEY_VALUE);
        } else if (str.trim().equals("") || str2.trim().equals("")) {
            throw new IllegalArgumentException(IMConfigConstants.MSG_EMPTY_KEY_VALUE);
        } else if (this.g != null) {
            this.g.setRefTagKey(str.toLowerCase());
            this.g.setRefTagValue(str2.toLowerCase());
        }
    }

    private void a(int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException(IMConfigConstants.MSG_INVALID_AD_SIZE);
        }
    }

    private void a(Activity activity, int i2, String str) {
        if (activity == null) {
            throw new NullPointerException(IMConfigConstants.MSG_NIL_ACTIVITY);
        }
        try {
            IMSDKUtil.validateAdConfiguration(activity);
        } catch (IMConfigException e2) {
            e2.printStackTrace();
        }
        a(i2);
        IMSDKUtil.validateAppID(str);
        this.s = str;
        this.t = i2;
        this.e = IMSDKUtil.getRootActivity(activity);
        if (this.b == null) {
            this.b = new IMWebView(this.e, this.J, false, false);
        }
        if (this.c == null) {
            this.c = new IMWebView(this.e, this.J, false, false);
            addView(this.c);
        }
        if (this.d == null) {
            this.d = new LinearLayout(this.e);
            this.d.setLayoutParams(new LinearLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent()));
            this.d.setOnTouchListener(this.G);
            this.d.setBackgroundColor(0);
            addView(this.d);
        }
        d();
        c();
        this.A = new a(this, this.H);
    }

    /* access modifiers changed from: private */
    public void a(IMAdUnit iMAdUnit) {
        IMWebView iMWebView;
        if (iMAdUnit == null || IMAdUnit.AdTypes.NONE == iMAdUnit.getAdType() || iMAdUnit.getCDATABlock() == null) {
            a(false);
            IMLog.debug(IMConstants.LOGGING_TAG, "Cannot load Ad. Invalid Ad Response");
            a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        StringBuffer stringBuffer = new StringBuffer(iMAdUnit.getCDATABlock());
        if (iMAdUnit.getAdType() == IMAdUnit.AdTypes.TEXT) {
            a(stringBuffer);
        }
        String stringBuffer2 = stringBuffer.toString();
        if (i()) {
            if (this.b == null) {
                this.b = new IMWebView(this.e, this.J, false, false);
            }
            iMWebView = this.b;
        } else {
            if (this.c == null) {
                this.c = new IMWebView(this.e, this.J, false, false);
            }
            iMWebView = this.c;
        }
        if (iMAdUnit.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
            IMLog.debug(IMConstants.LOGGING_TAG, "It came to AdActionType_Search method of displayad");
            iMWebView.requestOnSearchAdClicked(this.F.obtainMessage(103));
            iMWebView.setAdUnitData(true, iMAdUnit.getDefaultTargetUrl());
        } else {
            iMWebView.setAdUnitData(false, null);
        }
        iMWebView.requestOnPageFinishedCallback(this.F.obtainMessage(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR));
        this.F.sendEmptyMessageDelayed(108, 60000);
        iMWebView.reinitializeExpandProperties();
        iMWebView.loadDataWithBaseURL(this.D, "<html><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"><meta http-equiv=\"Content-Type\" content=\"text/html charset=utf-16le\"></head><body style=\"margin:0;padding:0\">" + stringBuffer2 + "</body></html>", HtmlFormatter.TEXT_HTML, null, this.D);
    }

    private void a(StringBuffer stringBuffer) {
        if (this.n != null) {
            if (this.o != null) {
                stringBuffer.append("<style>#im_c { background: -webkit-gradient(linear, left top, left bottom, from(#BGCOLOR1), to(#BGCOLOR2)) !important;\tbackground: -moz-linear-gradient(top,  #BGCOLOR1,  #BGCOLOR2) !important;} </style>".replaceAll("#BGCOLOR1", this.n).replaceAll("#BGCOLOR2", this.o));
            } else {
                stringBuffer.append("<style>#im_c { \tbackground:#BGCOLOR1 !important;} </style>".replaceAll("#BGCOLOR1", this.n));
            }
        }
        if (this.p != null) {
            stringBuffer.append("<style>#im_text {\tcolor:#TEXTCOLOR !important;} </style>".replaceAll("#TEXTCOLOR", this.p));
        }
    }

    private void c() {
        if (this.h == null) {
            this.h = new IMNiceInfo(this.e.getApplicationContext(), this.g);
        }
    }

    private void d() {
        if (this.g == null) {
            this.g = new IMUserInfo(this.e.getApplicationContext());
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.e.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            float f2 = displayMetrics.density;
            Display defaultDisplay = ((WindowManager) this.e.getSystemService("window")).getDefaultDisplay();
            int displayWidth = IMWrapperFunctions.getDisplayWidth(defaultDisplay);
            int displayHeight = IMWrapperFunctions.getDisplayHeight(defaultDisplay);
            this.g.setScreenDensity(String.valueOf(f2));
            this.g.setScreenSize("" + displayWidth + "X" + displayHeight);
            try {
                if (this.b != null && this.g.getPhoneDefaultUserAgent().equals("")) {
                    this.g.setPhoneDefaultUserAgent(InternalSDKUtil.getUserAgent(this.e));
                }
            } catch (Exception e2) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Exception occured while setting user agent" + e2);
            }
        }
        this.g.setAdUnitSlot(String.valueOf(this.t));
        if (this.u != -1) {
            this.g.setSlotId(Long.toString(this.u));
        }
        this.g.updateInfo(this.s, this.r);
    }

    /* access modifiers changed from: private */
    public void e() {
        boolean z2 = false;
        try {
            removeAllViews();
            if (i()) {
                addView(this.b);
                if (this.m != null && this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
                    this.b.requestFocusFromTouch();
                }
            } else {
                addView(this.c);
                if (this.m != null && this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search) {
                    this.c.requestFocusFromTouch();
                }
            }
            if (!(this.m.getAdType() == IMAdUnit.AdTypes.RICH_MEDIA || this.m.getAdActionName() == IMAdUnit.AdActionNames.AdActionName_Search)) {
                addView(this.d);
            }
            if (!i()) {
                z2 = true;
            }
            c(z2);
            a(false);
            k();
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error swapping banner ads", e2);
        }
    }

    public synchronized void loadNewAd() {
        IMLog.debug(IMConstants.LOGGING_TAG, " >>>> Start loading new Ad <<<<");
        try {
            if (g()) {
                a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_DOWNLOAD_IN_PROGRESS);
            } else if (h()) {
                a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_CLICK_IN_PROGRESS);
            } else if (!f()) {
                a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INVALID_REQUEST);
            } else {
                a(true);
                d();
                if (this.g != null) {
                    this.g.setRefreshType(0);
                }
                this.F.sendEmptyMessageDelayed(107, 60000);
                this.F.removeMessages(100);
                this.E = new IMRequestResponseManager();
                this.E.asyncRequestAd(this.g, this.h, IMRequestResponseManager.ActionType.AdRequest, this.B, this.C, this.I);
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error in loading ad ", e2);
        }
        return;
    }

    private void setAdServerUrl(String str) {
        this.B = str;
    }

    private void setAdServerTestUrl(String str) {
        this.C = str;
    }

    public void loadNewAd(IMAdRequest iMAdRequest) {
        if (iMAdRequest != null) {
            this.v = iMAdRequest.isTestMode();
            setIMAdRequest(iMAdRequest);
        }
        loadNewAd();
    }

    private boolean f() {
        if (!this.v && !InternalSDKUtil.validateAppId(this.s)) {
            return false;
        }
        if (this.t >= 0) {
            return true;
        }
        IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_SIZE);
        return false;
    }

    /* access modifiers changed from: private */
    public boolean g() {
        return this.i.get();
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        this.i.set(z2);
    }

    /* access modifiers changed from: private */
    public boolean h() {
        IMWebView iMWebView;
        if (this.j.get()) {
            return true;
        }
        if (i()) {
            iMWebView = this.c;
        } else {
            iMWebView = this.b;
        }
        String state = iMWebView.getState();
        IMLog.debug(IMConstants.LOGGING_TAG, "Current Ad State: " + state);
        if (IMWebView.ViewState.EXPANDED.toString().equalsIgnoreCase(state) || IMWebView.ViewState.RESIZED.toString().equalsIgnoreCase(state) || IMWebView.ViewState.EXPANDING.toString().equalsIgnoreCase(state)) {
            IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_STATE);
            return true;
        } else if (!iMWebView.isBusy()) {
            return false;
        } else {
            IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_BUSY);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        this.j.set(z2);
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return this.f;
    }

    /* access modifiers changed from: private */
    public void c(boolean z2) {
        this.f = z2;
        if (z2) {
            this.b.deinit();
            this.b = null;
            return;
        }
        this.c.deinit();
        this.c = null;
    }

    /* access modifiers changed from: package-private */
    public Animation a() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public void a(Animation animation) {
        this.k = animation;
    }

    /* access modifiers changed from: package-private */
    public Animation b() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void b(Animation animation) {
        this.l = animation;
    }

    /* access modifiers changed from: private */
    public void a(final int i2, final IMAdRequest.ErrorCode errorCode) {
        if (!this.z) {
            IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_CALL_BACK);
        } else if (this.q != null) {
            this.e.runOnUiThread(new Runnable() {
                /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
                public void run() {
                    try {
                        switch (i2) {
                            case 100:
                                IMAdView.this.q.onAdRequestCompleted(IMAdView.this);
                                IMAdView.this.l();
                                return;
                            case IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR /*101*/:
                                IMAdView.this.l();
                                switch (AnonymousClass6.a[errorCode.ordinal()]) {
                                    case 1:
                                        IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_CLICK);
                                        break;
                                    case 2:
                                        IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_DOWNLOAD);
                                        break;
                                    case 3:
                                        IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_INVENTORY);
                                        break;
                                    case 4:
                                        IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_INVALID_APP_ID);
                                        break;
                                }
                                IMAdView.this.q.onAdRequestFailed(IMAdView.this, errorCode);
                                return;
                            case 102:
                                IMAdView.this.q.onShowAdScreen(IMAdView.this);
                                return;
                            case 103:
                                IMAdView.this.q.onDismissAdScreen(IMAdView.this);
                                return;
                            case 104:
                                IMAdView.this.q.onLeaveApplication(IMAdView.this);
                                return;
                            default:
                                return;
                        }
                    } catch (Exception e) {
                        IMLog.debug(IMConstants.LOGGING_TAG, "Exception giving callback to the publisher ", e);
                    }
                    IMLog.debug(IMConstants.LOGGING_TAG, "Exception giving callback to the publisher ", e);
                }
            });
        }
    }

    /* renamed from: com.inmobi.androidsdk.IMAdView$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] a = new int[IMAdRequest.ErrorCode.values().length];

        static {
            try {
                a[IMAdRequest.ErrorCode.AD_CLICK_IN_PROGRESS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[IMAdRequest.ErrorCode.AD_DOWNLOAD_IN_PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[IMAdRequest.ErrorCode.NO_FILL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[IMAdRequest.ErrorCode.INVALID_APP_ID.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(MotionEvent motionEvent) {
        try {
            if (this.m != null && !h()) {
                b(true);
                if (this.m.getTargetUrl() != null) {
                    IMBrowserActivity.requestOnPoorAdClosed(this.F.obtainMessage(104));
                    new IMClickProcessingTask(this.m, this.g, this.e, motionEvent, this.F.obtainMessage(102), this.F.obtainMessage(105), this.F.obtainMessage(106), this.J).execute(null);
                }
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception processing ad click ", e2);
            b(false);
        }
        k();
    }

    /* access modifiers changed from: private */
    public void j() {
        try {
            int argb = Color.argb(100, 0, 0, 0);
            if (this.c != null) {
                this.c.setBackgroundColor(argb);
            }
            if (this.b != null) {
                this.b.setBackgroundColor(argb);
            }
            this.d.setBackgroundColor(argb);
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error setHighlightedBGColor", e2);
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        try {
            if (this.c != null) {
                this.c.setBackgroundColor(0);
            }
            if (this.b != null) {
                this.b.setBackgroundColor(0);
            }
            this.d.setBackgroundColor(0);
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error setNormalBGColor", e2);
        }
    }

    public void setAdBackgroundColor(String str) {
        if (a(str)) {
            this.n = str;
            a(this.m);
        }
    }

    public void setAdBackgroundGradientColor(String str, String str2) {
        if (a(str) || a(str2)) {
            this.n = str;
            this.o = str2;
            a(this.m);
        }
    }

    public void setAdTextColor(String str) {
        if (a(str)) {
            this.p = str;
            a(this.m);
        }
    }

    private boolean a(String str) {
        if (str == null) {
            throw new NullPointerException(IMConfigConstants.MSG_NIL_COLOR);
        }
        int length = str.length();
        if (str.startsWith("#") && (length == 4 || length == 7)) {
            return true;
        }
        throw new IllegalArgumentException(IMConfigConstants.MSG_INVALID_COLOR_FORMAT);
    }

    public void setRefreshInterval(int i2) {
        if (i2 == -1) {
            this.a = -1;
            this.F.removeMessages(100);
        } else if (i2 < 20) {
            throw new IllegalArgumentException("Refresh Interval cannot be less than 20 seconds.");
        } else {
            this.a = i2;
            this.F.removeMessages(100);
            this.F.sendEmptyMessageDelayed(100, (long) (i2 * 1000));
        }
    }

    public void setAnimationType(AnimationType animationType) {
        this.y = animationType;
    }

    static class a extends Handler {
        private final WeakReference<IMAdView> a;

        public a(IMAdView iMAdView) {
            this.a = new WeakReference<>(iMAdView);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, boolean):void
         arg types: [com.inmobi.androidsdk.IMAdView, int]
         candidates:
          com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, long):long
          com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.ai.container.IMWebView):com.inmobi.androidsdk.ai.container.IMWebView
          com.inmobi.androidsdk.IMAdView.a(int, com.inmobi.androidsdk.IMAdRequest$ErrorCode):void
          com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, android.view.MotionEvent):void
          com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.impl.IMAdUnit):void
          com.inmobi.androidsdk.IMAdView.a(com.inmobi.androidsdk.IMAdView, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.inmobi.androidsdk.IMAdView.b(com.inmobi.androidsdk.IMAdView, boolean):boolean
         arg types: [com.inmobi.androidsdk.IMAdView, int]
         candidates:
          com.inmobi.androidsdk.IMAdView.b(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.ai.container.IMWebView):com.inmobi.androidsdk.ai.container.IMWebView
          com.inmobi.androidsdk.IMAdView.b(com.inmobi.androidsdk.IMAdView, com.inmobi.androidsdk.impl.IMAdUnit):com.inmobi.androidsdk.impl.IMAdUnit
          com.inmobi.androidsdk.IMAdView.b(com.inmobi.androidsdk.IMAdView, boolean):boolean */
        public void handleMessage(Message message) {
            IMWebView f;
            IMAdView iMAdView = this.a.get();
            if (iMAdView != null) {
                switch (message.what) {
                    case 100:
                        if (iMAdView.a != -1) {
                            if (!iMAdView.e.hasWindowFocus()) {
                                IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_FOCUS);
                            } else if (IMWebView.isInterstitialDisplayed.get()) {
                                IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_INTERSTITIAL_AD_DISPLAYED);
                            } else {
                                iMAdView.loadNewAd();
                            }
                            if (iMAdView.g != null) {
                                iMAdView.g.setRefreshType(1);
                                break;
                            }
                        } else {
                            return;
                        }
                        break;
                    case IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR /*101*/:
                        removeMessages(108);
                        if (iMAdView.x) {
                            iMAdView.e();
                            boolean unused = iMAdView.x = false;
                        } else if (iMAdView.y == AnimationType.ANIMATION_OFF) {
                            iMAdView.e();
                        } else {
                            iMAdView.A.a(iMAdView.y);
                        }
                        long unused2 = iMAdView.w = System.currentTimeMillis();
                        iMAdView.a(100, (IMAdRequest.ErrorCode) null);
                        break;
                    case 102:
                        iMAdView.b(false);
                        break;
                    case 103:
                        Bundle data = message.getData();
                        if (!(data == null || iMAdView.m == null || data.getString("finaltargeturl") == null)) {
                            iMAdView.m.setTargetUrl(data.getString("finaltargeturl"));
                        }
                        iMAdView.a((MotionEvent) null);
                        break;
                    case 104:
                        iMAdView.a(103, (IMAdRequest.ErrorCode) null);
                        break;
                    case 105:
                        iMAdView.a(102, (IMAdRequest.ErrorCode) null);
                        break;
                    case 106:
                        iMAdView.a(104, (IMAdRequest.ErrorCode) null);
                        break;
                    case 107:
                        removeMessages(109);
                        removeMessages(110);
                        iMAdView.E.doCancel();
                        iMAdView.a(false);
                        iMAdView.a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_FETCH_TIMEOUT);
                        break;
                    case 108:
                        removeMessages(111);
                        removeMessages(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR);
                        iMAdView.a(false);
                        if (iMAdView.i()) {
                            f = iMAdView.b;
                            IMWebView unused3 = iMAdView.b = (IMWebView) null;
                        } else {
                            f = iMAdView.c;
                            IMWebView unused4 = iMAdView.c = (IMWebView) null;
                        }
                        f.cancelLoad();
                        f.stopLoading();
                        f.deinit();
                        iMAdView.a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_RENDERING_TIMEOUT);
                        break;
                    case 109:
                        removeMessages(107);
                        iMAdView.a(iMAdView.m);
                        break;
                    case 110:
                        removeMessages(107);
                        iMAdView.a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, (IMAdRequest.ErrorCode) message.obj);
                        iMAdView.a(false);
                        break;
                    case 111:
                        removeMessages(108);
                        removeMessages(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR);
                        iMAdView.a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INTERNAL_ERROR);
                        break;
                }
            }
            super.handleMessage(message);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        IMLog.debug(IMConstants.LOGGING_TAG, "onAttachedToWindow");
        this.z = true;
        setRefreshInterval(this.a);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        IMWebView iMWebView;
        IMLog.debug(IMConstants.LOGGING_TAG, "onDetatchedFromWindow");
        this.z = false;
        setRefreshInterval(-1);
        if (i()) {
            iMWebView = this.c;
        } else {
            iMWebView = this.b;
        }
        if (iMWebView != null) {
            iMWebView.deinit();
        }
    }

    public IMAdListener getIMAdListener() {
        return this.q;
    }

    public void setIMAdListener(IMAdListener iMAdListener) {
        this.q = iMAdListener;
    }

    public IMAdRequest getIMAdRequest() {
        return this.r;
    }

    public void setIMAdRequest(IMAdRequest iMAdRequest) {
        this.r = iMAdRequest;
    }

    public String getAppId() {
        return this.s;
    }

    public void setAppId(String str) {
        IMSDKUtil.validateAppID(str);
        this.s = str;
    }

    public int getAdSize() {
        return this.t;
    }

    public void setAdSize(int i2) {
        a(i2);
        this.t = i2;
    }

    public long getSlotId() {
        return this.u;
    }

    public void setSlotId(long j2) {
        this.u = j2;
    }

    public void stopLoading() {
        if (this.F.hasMessages(107)) {
            this.F.removeMessages(107);
            this.F.sendEmptyMessage(107);
        } else if (this.F.hasMessages(108)) {
            this.F.removeMessages(108);
            this.F.sendEmptyMessage(108);
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (this.F.hasMessages(100)) {
            this.F.removeMessages(100);
        }
        this.F.sendEmptyMessageDelayed(100, (long) (this.a * 1000));
    }
}
