package com.inmobi.androidsdk;

import com.inmobi.androidsdk.IMAdRequest;

public interface IMAdInterstitialListener {
    void onAdRequestFailed(IMAdInterstitial iMAdInterstitial, IMAdRequest.ErrorCode errorCode);

    void onAdRequestLoaded(IMAdInterstitial iMAdInterstitial);

    void onDismissAdScreen(IMAdInterstitial iMAdInterstitial);

    void onLeaveApplication(IMAdInterstitial iMAdInterstitial);

    void onShowAdScreen(IMAdInterstitial iMAdInterstitial);
}
