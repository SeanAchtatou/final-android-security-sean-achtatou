package com.inmobi.androidsdk;

import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.adfonic.android.utils.HtmlFormatter;
import com.inmobi.androidsdk.IMAdRequest;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWrapperFunctions;
import com.inmobi.androidsdk.ai.controller.util.IMConfigException;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.androidsdk.ai.controller.util.IMSDKUtil;
import com.inmobi.androidsdk.impl.IMAdUnit;
import com.inmobi.androidsdk.impl.IMConfigConstants;
import com.inmobi.androidsdk.impl.IMNiceInfo;
import com.inmobi.androidsdk.impl.IMUserInfo;
import com.inmobi.androidsdk.impl.net.IMHttpRequestCallback;
import com.inmobi.androidsdk.impl.net.IMRequestResponseManager;
import com.inmobi.commons.internal.IMLog;
import com.inmobi.commons.internal.InternalSDKUtil;
import java.lang.ref.WeakReference;

public class IMAdInterstitial {
    /* access modifiers changed from: private */
    public State a = State.INIT;
    private IMAdRequest b;
    /* access modifiers changed from: private */
    public Activity c;
    private String d;
    private long e = -1;
    /* access modifiers changed from: private */
    public IMAdInterstitialListener f;
    /* access modifiers changed from: private */
    public IMAdUnit g;
    private IMUserInfo h;
    private IMNiceInfo i;
    /* access modifiers changed from: private */
    public IMWebView j;
    private String k = "http://i.w.inmobi.com/showad.asm";
    private String l = "http://i.w.sandbox.inmobi.com/showad.asm";
    /* access modifiers changed from: private */
    public long m = 0;
    private String n = ("http://localhost/" + Integer.toString(InternalSDKUtil.incrementBaseUrl()) + "/");
    /* access modifiers changed from: private */
    public IMRequestResponseManager o;
    private IMHttpRequestCallback p = new IMHttpRequestCallback() {
        public void notifyResult(int i, Object obj) {
            IMLog.debug(IMConstants.LOGGING_TAG, ">>> Got HTTP REQUEST callback. Status: " + i + " ,data=" + obj);
            if (i == 0) {
                IMAdUnit unused = IMAdInterstitial.this.g = (IMAdUnit) obj;
                IMAdInterstitial.this.q.sendEmptyMessage(308);
            } else if (i == 1) {
                Message obtainMessage = IMAdInterstitial.this.q.obtainMessage(309);
                obtainMessage.obj = obj;
                obtainMessage.sendToTarget();
            }
        }
    };
    /* access modifiers changed from: private */
    public a q = new a(this);
    /* access modifiers changed from: private */
    public IMWebView.IMWebViewListener r = new IMWebView.IMWebViewListener() {
        public void onExpand() {
        }

        public void onExpandClose() {
        }

        public void onLeaveApplication() {
            IMAdInterstitial.this.a(104, null);
        }

        public void onError() {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error loading the interstitial ad ");
            IMAdInterstitial.this.q.sendEmptyMessage(310);
        }
    };

    public enum State {
        INIT,
        READY,
        LOADING,
        ACTIVE
    }

    public IMAdInterstitial(Activity activity, String str) {
        a(activity, str);
    }

    public IMAdInterstitial(Activity activity, String str, long j2) {
        this.e = j2;
        a(activity, str);
    }

    private void a(Activity activity, String str) {
        if (activity == null) {
            throw new NullPointerException(IMConfigConstants.MSG_NIL_ACTIVITY);
        }
        try {
            IMSDKUtil.validateAdConfiguration(activity);
        } catch (IMConfigException e2) {
            e2.printStackTrace();
        }
        IMSDKUtil.validateAppID(str);
        this.c = IMSDKUtil.getRootActivity(activity);
        this.d = str;
        c();
    }

    public State getState() {
        return this.a;
    }

    public void loadNewAd() {
        IMLog.debug(IMConstants.LOGGING_TAG, " >>>> Start loading new Interstitial Ad <<<<");
        if (!a()) {
            a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INVALID_REQUEST);
        } else if (this.a == State.LOADING) {
            a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_DOWNLOAD_IN_PROGRESS);
        } else if (this.a == State.ACTIVE) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Interstitial ad is in ACTIVE state. Try again after sometime.");
            a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INVALID_REQUEST);
        } else {
            this.a = State.LOADING;
            c();
            b();
            this.q.sendEmptyMessageDelayed(306, 60000);
            this.o = new IMRequestResponseManager();
            this.o.asyncRequestAd(this.h, this.i, IMRequestResponseManager.ActionType.AdRequest_Interstitial, this.k, this.l, this.p);
        }
    }

    public void loadNewAd(IMAdRequest iMAdRequest) {
        this.b = iMAdRequest;
        loadNewAd();
    }

    private void setAdServerUrl(String str) {
        this.k = str;
    }

    private void setAdServerTestUrl(String str) {
        this.l = str;
    }

    private boolean a() {
        boolean isTestMode;
        if (this.b == null) {
            isTestMode = false;
        } else {
            isTestMode = this.b.isTestMode();
        }
        if (isTestMode || InternalSDKUtil.validateAppId(this.d)) {
            return true;
        }
        return false;
    }

    private void b() {
        if (this.i == null) {
            this.i = new IMNiceInfo(this.c.getApplicationContext(), this.h);
        }
    }

    private void c() {
        if (this.h == null) {
            this.h = new IMUserInfo(this.c);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            this.c.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            float f2 = displayMetrics.density;
            Display defaultDisplay = ((WindowManager) this.c.getSystemService("window")).getDefaultDisplay();
            int displayWidth = IMWrapperFunctions.getDisplayWidth(defaultDisplay);
            int displayHeight = IMWrapperFunctions.getDisplayHeight(defaultDisplay);
            this.h.setScreenDensity(String.valueOf(f2));
            this.h.setScreenSize("" + displayWidth + "X" + displayHeight);
            try {
                if (this.h.getPhoneDefaultUserAgent().equals("")) {
                    this.h.setPhoneDefaultUserAgent(InternalSDKUtil.getUserAgent(this.c));
                }
            } catch (Exception e2) {
                IMLog.debug(IMConstants.LOGGING_TAG, "Exception occured while setting user agent" + e2);
            }
        }
        this.h.updateInfo(this.d, this.b);
        int i2 = 14;
        if (Build.VERSION.SDK_INT == 11 || Build.VERSION.SDK_INT == 12 || Build.VERSION.SDK_INT == 13) {
            i2 = 17;
        }
        this.h.setAdUnitSlot(String.valueOf(i2));
        if (this.e != -1) {
            this.h.setSlotId(Long.toString(this.e));
        }
    }

    public void show() {
        try {
            IMLog.debug(IMConstants.LOGGING_TAG, "Showing the Interstitial Ad. ");
            if (this.a != State.READY) {
                throw new IllegalStateException("Interstitial ad is not in the 'READY' state. Current state: " + this.a);
            } else if (this.g != null) {
                this.j.setAdUnitData(false, null);
                this.j.requestOnInterstitialClosed(this.q.obtainMessage(304));
                this.j.requestOnInterstitialShown(this.q.obtainMessage(305));
                this.j.changeContentAreaForInterstitials(false);
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Error showing ad ", e2);
        }
    }

    public IMAdInterstitialListener getIMAdInterstitialListener() {
        return this.f;
    }

    public void setIMAdInterstitialListener(IMAdInterstitialListener iMAdInterstitialListener) {
        this.f = iMAdInterstitialListener;
    }

    public IMAdRequest getIMAdRequest() {
        return this.b;
    }

    public void setIMAdRequest(IMAdRequest iMAdRequest) {
        this.b = iMAdRequest;
    }

    public String getAppId() {
        return this.d;
    }

    public void setAppId(String str) {
        IMSDKUtil.validateAppID(str);
        this.d = str;
    }

    public long getSlotId() {
        return this.e;
    }

    public void setSlotId(long j2) {
        this.e = j2;
    }

    public void stopLoading() {
        if (this.q.hasMessages(306)) {
            this.q.removeMessages(306);
            this.q.sendEmptyMessage(306);
        } else if (this.q.hasMessages(307)) {
            this.q.removeMessages(307);
            this.q.sendEmptyMessage(307);
        }
    }

    /* access modifiers changed from: private */
    public void a(final int i2, final IMAdRequest.ErrorCode errorCode) {
        if (this.f != null) {
            this.c.runOnUiThread(new Runnable() {
                public void run() {
                    switch (i2) {
                        case 100:
                            long unused = IMAdInterstitial.this.m = System.currentTimeMillis();
                            IMAdInterstitial.this.f.onAdRequestLoaded(IMAdInterstitial.this);
                            return;
                        case IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR /*101*/:
                            switch (AnonymousClass4.a[errorCode.ordinal()]) {
                                case 1:
                                    IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_CLICK);
                                    break;
                                case 2:
                                    IMLog.debug(IMConstants.LOGGING_TAG, IMConfigConstants.MSG_AD_DOWNLOAD);
                                    break;
                            }
                            IMAdInterstitial.this.f.onAdRequestFailed(IMAdInterstitial.this, errorCode);
                            return;
                        case 102:
                            IMAdInterstitial.this.f.onShowAdScreen(IMAdInterstitial.this);
                            return;
                        case 103:
                            IMAdInterstitial.this.f.onDismissAdScreen(IMAdInterstitial.this);
                            return;
                        case 104:
                            IMAdInterstitial.this.f.onLeaveApplication(IMAdInterstitial.this);
                            return;
                        default:
                            return;
                    }
                }
            });
        }
    }

    /* renamed from: com.inmobi.androidsdk.IMAdInterstitial$4  reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] a = new int[IMAdRequest.ErrorCode.values().length];

        static {
            try {
                a[IMAdRequest.ErrorCode.AD_CLICK_IN_PROGRESS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[IMAdRequest.ErrorCode.AD_DOWNLOAD_IN_PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(IMAdUnit iMAdUnit) {
        if (iMAdUnit == null || IMAdUnit.AdTypes.NONE == iMAdUnit.getAdType() || iMAdUnit.getCDATABlock() == null) {
            this.a = State.INIT;
            IMLog.debug(IMConstants.LOGGING_TAG, "Cannot load Ad. Invalid Ad Response");
            a((int) IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INTERNAL_ERROR);
            return;
        }
        String replaceAll = new StringBuffer(iMAdUnit.getCDATABlock()).toString().replaceAll("%", "%25");
        IMLog.debug(IMConstants.LOGGING_TAG, "Final HTML String: " + replaceAll);
        this.j.requestOnPageFinishedCallback(this.q.obtainMessage(303));
        this.q.sendEmptyMessageDelayed(307, 60000);
        this.j.loadDataWithBaseURL(this.n, "<html><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1\"><meta http-equiv=\"Content-Type\" content=\"text/html charset=utf-16le\"></head><body style=\"margin:0;padding:0\">" + replaceAll + "</body></html>", HtmlFormatter.TEXT_HTML, null, this.n);
    }

    static class a extends Handler {
        private final WeakReference<IMAdInterstitial> a;

        public a(IMAdInterstitial iMAdInterstitial) {
            this.a = new WeakReference<>(iMAdInterstitial);
        }

        public void handleMessage(Message message) {
            IMAdInterstitial iMAdInterstitial = this.a.get();
            if (iMAdInterstitial != null) {
                switch (message.what) {
                    case 303:
                        removeMessages(307);
                        State unused = iMAdInterstitial.a = State.READY;
                        iMAdInterstitial.a(100, null);
                        return;
                    case 304:
                        State unused2 = iMAdInterstitial.a = State.INIT;
                        iMAdInterstitial.a(103, null);
                        IMWebView unused3 = iMAdInterstitial.j = (IMWebView) null;
                        return;
                    case 305:
                        State unused4 = iMAdInterstitial.a = State.ACTIVE;
                        iMAdInterstitial.a(102, null);
                        return;
                    case 306:
                        removeMessages(308);
                        removeMessages(309);
                        State unused5 = iMAdInterstitial.a = State.INIT;
                        iMAdInterstitial.o.doCancel();
                        iMAdInterstitial.a(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_FETCH_TIMEOUT);
                        return;
                    case 307:
                        removeMessages(310);
                        removeMessages(303);
                        State unused6 = iMAdInterstitial.a = State.INIT;
                        iMAdInterstitial.j.cancelLoad();
                        iMAdInterstitial.j.stopLoading();
                        iMAdInterstitial.j.deinit();
                        IMWebView unused7 = iMAdInterstitial.j = (IMWebView) null;
                        iMAdInterstitial.a(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.AD_RENDERING_TIMEOUT);
                        return;
                    case 308:
                        removeMessages(306);
                        try {
                            if (iMAdInterstitial.j == null) {
                                IMWebView unused8 = iMAdInterstitial.j = new IMWebView(iMAdInterstitial.c, iMAdInterstitial.r, true, false);
                            }
                            iMAdInterstitial.a(iMAdInterstitial.g);
                            return;
                        } catch (Exception e) {
                            IMLog.debug(IMConstants.LOGGING_TAG, "Error retrieving ad ", e);
                            State unused9 = iMAdInterstitial.a = State.INIT;
                            iMAdInterstitial.a(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INTERNAL_ERROR);
                            return;
                        }
                    case 309:
                        removeMessages(306);
                        State unused10 = iMAdInterstitial.a = State.INIT;
                        iMAdInterstitial.a(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, (IMAdRequest.ErrorCode) message.obj);
                        return;
                    case 310:
                        removeMessages(307);
                        removeMessages(303);
                        State unused11 = iMAdInterstitial.a = State.INIT;
                        IMWebView unused12 = iMAdInterstitial.j = (IMWebView) null;
                        iMAdInterstitial.a(IMBrowserActivity.EXTRA_BROWSER_STATUS_BAR, IMAdRequest.ErrorCode.INTERNAL_ERROR);
                        return;
                    default:
                        return;
                }
            }
        }
    }
}
