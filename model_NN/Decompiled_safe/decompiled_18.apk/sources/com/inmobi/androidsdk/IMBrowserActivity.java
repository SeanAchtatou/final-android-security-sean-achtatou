package com.inmobi.androidsdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.inmobi.androidsdk.ai.container.IMCustomView;
import com.inmobi.androidsdk.ai.container.IMWebView;
import com.inmobi.androidsdk.ai.container.IMWrapperFunctions;
import com.inmobi.androidsdk.ai.controller.util.IMConstants;
import com.inmobi.commons.internal.IMLog;

public class IMBrowserActivity extends Activity {
    public static final String EXTRA_BROWSER_ACTIVITY_TYPE = "extra_browser_type";
    public static final int EXTRA_BROWSER_STATUS_BAR = 101;
    public static final String EXTRA_URL = "extra_url";
    private static IMWebView.IMWebViewListener b;
    private static Message c;
    /* access modifiers changed from: private */
    public IMWebView a;
    private RelativeLayout d;
    private float e;
    private String f;
    /* access modifiers changed from: private */
    public IMCustomView g;
    private WebViewClient h = new WebViewClient() {
        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (IMBrowserActivity.this.g != null) {
                IMBrowserActivity.this.g.setSwitchInt(IMCustomView.SwitchIconType.FORWARD_INACTIVE);
                IMBrowserActivity.this.g.invalidate();
            }
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (IMBrowserActivity.this.g != null) {
                if (webView.canGoForward()) {
                    IMBrowserActivity.this.g.setSwitchInt(IMCustomView.SwitchIconType.FORWARD_ACTIVE);
                    IMBrowserActivity.this.g.invalidate();
                } else {
                    IMBrowserActivity.this.g.setSwitchInt(IMCustomView.SwitchIconType.FORWARD_INACTIVE);
                    IMBrowserActivity.this.g.invalidate();
                }
            }
            CookieSyncManager.getInstance().sync();
        }
    };

    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        if (Build.VERSION.SDK_INT < 9 || Build.VERSION.SDK_INT >= 11) {
            getWindow().setFlags(1024, 1024);
        }
        ((WindowManager) getSystemService("window")).getDefaultDisplay().getMetrics(new DisplayMetrics());
        this.e = getResources().getDisplayMetrics().density;
        Intent intent = getIntent();
        String stringExtra = intent.getStringExtra(EXTRA_URL);
        this.f = intent.getStringExtra("FIRST_INSTANCE");
        IMLog.debug(IMConstants.LOGGING_TAG, "IMBrowserActivity-> onCreate");
        if (stringExtra != null) {
            this.d = new RelativeLayout(this);
            this.a = new IMWebView(this, b, true, true);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
            layoutParams.addRule(10);
            layoutParams.addRule(2, 100);
            this.d.setBackgroundColor(-1);
            this.d.addView(this.a, layoutParams);
            a(this.d);
            this.a.getSettings().setJavaScriptEnabled(true);
            this.a.setExternalWebViewClient(this.h);
            this.a.getSettings().setLoadWithOverviewMode(true);
            this.a.getSettings().setUseWideViewPort(true);
            this.a.loadUrl(stringExtra);
            CookieSyncManager.createInstance(this);
            CookieSyncManager.getInstance().startSync();
            setContentView(this.d);
        }
    }

    private void a(ViewGroup viewGroup) {
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(0);
        linearLayout.setId(100);
        linearLayout.setWeightSum(100.0f);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        linearLayout.setBackgroundResource(17301658);
        linearLayout.setBackgroundColor(-7829368);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), (int) (44.0f * this.e));
        layoutParams.addRule(12);
        viewGroup.addView(linearLayout, layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(IMWrapperFunctions.getParamFillParent(), IMWrapperFunctions.getParamFillParent());
        layoutParams2.weight = 25.0f;
        IMCustomView iMCustomView = new IMCustomView(this, this.e, IMCustomView.SwitchIconType.CLOSE_ICON);
        linearLayout.addView(iMCustomView, layoutParams2);
        iMCustomView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1) {
                    view.setBackgroundColor(-7829368);
                    IMBrowserActivity.this.finish();
                } else if (motionEvent.getAction() == 0) {
                    view.setBackgroundColor(-16711681);
                }
                return true;
            }
        });
        IMCustomView iMCustomView2 = new IMCustomView(this, this.e, IMCustomView.SwitchIconType.REFRESH);
        linearLayout.addView(iMCustomView2, layoutParams2);
        iMCustomView2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1) {
                    view.setBackgroundColor(-7829368);
                    IMBrowserActivity.this.a.doHidePlayers();
                    IMBrowserActivity.this.a.reload();
                } else if (motionEvent.getAction() == 0) {
                    view.setBackgroundColor(-16711681);
                }
                return true;
            }
        });
        IMCustomView iMCustomView3 = new IMCustomView(this, this.e, IMCustomView.SwitchIconType.BACK);
        linearLayout.addView(iMCustomView3, layoutParams2);
        iMCustomView3.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1) {
                    view.setBackgroundColor(-7829368);
                    if (IMBrowserActivity.this.a.canGoBack()) {
                        IMBrowserActivity.this.a.goBack();
                    } else {
                        IMBrowserActivity.this.finish();
                    }
                } else if (motionEvent.getAction() == 0) {
                    view.setBackgroundColor(-16711681);
                }
                return true;
            }
        });
        this.g = new IMCustomView(this, this.e, IMCustomView.SwitchIconType.FORWARD_INACTIVE);
        linearLayout.addView(this.g, layoutParams2);
        this.g.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 1) {
                    view.setBackgroundColor(-7829368);
                    if (IMBrowserActivity.this.a.canGoForward()) {
                        IMBrowserActivity.this.a.goForward();
                    }
                } else if (motionEvent.getAction() == 0) {
                    view.setBackgroundColor(-16711681);
                }
                return true;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        CookieSyncManager.getInstance().stopSync();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        CookieSyncManager.getInstance().startSync();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        try {
            if (this.a != null) {
                this.a.releaseAllPlayers();
            }
            if (c != null && this.f != null) {
                c.sendToTarget();
            }
        } catch (Exception e2) {
            IMLog.debug(IMConstants.LOGGING_TAG, "Exception in onDestroy ", e2);
        }
    }

    public static void requestOnPoorAdClosed(Message message) {
        c = message;
    }

    public static void setWebViewListener(IMWebView.IMWebViewListener iMWebViewListener) {
        b = iMWebViewListener;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (this.a != null) {
            this.a.onOrientationEventChange();
        }
        super.onConfigurationChanged(configuration);
    }
}
