package com.inmobi.androidsdk;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import com.inmobi.androidsdk.IMAdView;
import com.inmobi.androidsdk.impl.anim.IMRotate3dAnimation;
import com.inmobi.androidsdk.impl.anim.IMRotate3dAnimationVert;

/* compiled from: IMAnimationController */
class a {
    private IMAdView a;
    private Animation.AnimationListener b;

    public a(IMAdView iMAdView, Animation.AnimationListener animationListener) {
        this.a = iMAdView;
        this.b = animationListener;
    }

    public void a(IMAdView.AnimationType animationType) {
        if (animationType == IMAdView.AnimationType.ANIMATION_ALPHA) {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 0.5f);
            AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.5f, 1.0f);
            alphaAnimation.setDuration(1000);
            alphaAnimation.setFillAfter(false);
            alphaAnimation.setAnimationListener(this.b);
            alphaAnimation.setInterpolator(new DecelerateInterpolator());
            alphaAnimation2.setDuration(500);
            alphaAnimation2.setFillAfter(false);
            alphaAnimation2.setAnimationListener(this.b);
            alphaAnimation2.setInterpolator(new DecelerateInterpolator());
            this.a.a(alphaAnimation);
            this.a.b(alphaAnimation2);
        } else if (animationType == IMAdView.AnimationType.ROTATE_HORIZONTAL_AXIS) {
            IMRotate3dAnimation iMRotate3dAnimation = new IMRotate3dAnimation(0.0f, 90.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, true);
            IMRotate3dAnimation iMRotate3dAnimation2 = new IMRotate3dAnimation(270.0f, 360.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, true);
            iMRotate3dAnimation.setDuration(500);
            iMRotate3dAnimation.setFillAfter(false);
            iMRotate3dAnimation.setAnimationListener(this.b);
            iMRotate3dAnimation.setInterpolator(new AccelerateInterpolator());
            iMRotate3dAnimation2.setDuration(500);
            iMRotate3dAnimation2.setFillAfter(false);
            iMRotate3dAnimation2.setAnimationListener(this.b);
            iMRotate3dAnimation2.setInterpolator(new DecelerateInterpolator());
            this.a.a(iMRotate3dAnimation);
            this.a.b(iMRotate3dAnimation2);
        } else if (animationType == IMAdView.AnimationType.ROTATE_VERTICAL_AXIS) {
            IMRotate3dAnimationVert iMRotate3dAnimationVert = new IMRotate3dAnimationVert(0.0f, 90.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, true);
            IMRotate3dAnimationVert iMRotate3dAnimationVert2 = new IMRotate3dAnimationVert(270.0f, 360.0f, ((float) this.a.getWidth()) / 2.0f, ((float) this.a.getHeight()) / 2.0f, 0.0f, true);
            iMRotate3dAnimationVert.setDuration(500);
            iMRotate3dAnimationVert.setFillAfter(false);
            iMRotate3dAnimationVert.setAnimationListener(this.b);
            iMRotate3dAnimationVert.setInterpolator(new AccelerateInterpolator());
            iMRotate3dAnimationVert2.setDuration(500);
            iMRotate3dAnimationVert2.setFillAfter(false);
            iMRotate3dAnimationVert2.setAnimationListener(this.b);
            iMRotate3dAnimationVert2.setInterpolator(new DecelerateInterpolator());
            this.a.a(iMRotate3dAnimationVert);
            this.a.b(iMRotate3dAnimationVert2);
        }
        this.a.startAnimation(this.a.a());
    }
}
