package com.inmobi.commons;

import com.inmobi.commons.internal.IMLog;

public class IMCommonUtil {
    public static final int DEVICE_ID_EXCLUDE_ODIN1 = 2;
    public static final int DEVICE_ID_INCLUDE_DEFAULT = 1;
    private static int a = 1;

    public enum LOG_LEVEL {
        NONE(0),
        DEBUG(1);
        
        private final int a;

        private LOG_LEVEL(int i) {
            this.a = i;
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.a;
        }
    }

    public static void setLogLevel(LOG_LEVEL log_level) {
        if (log_level == LOG_LEVEL.NONE) {
            IMLog.setInternalLogLevel(IMLog.INTERNAL_LOG_LEVEL.NONE);
        } else {
            IMLog.setInternalLogLevel(IMLog.INTERNAL_LOG_LEVEL.DEBUG);
        }
    }

    public static LOG_LEVEL getLogLevel() {
        if (IMLog.getLogLevel().equals(IMLog.INTERNAL_LOG_LEVEL.NONE)) {
            return LOG_LEVEL.NONE;
        }
        return LOG_LEVEL.DEBUG;
    }

    public static String getReleaseVersion() {
        return "3.6.0";
    }

    public static void setDeviceIDMask(int i) {
        a = i;
    }

    public static int getDeviceIdMask() {
        return a;
    }
}
