package com.inmobi.commons.internal;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class IMLocationMonitor implements LocationListener {
    private static IMLocationMonitor a = null;
    private static LocationManager b = null;
    private static final int c = 0;
    private static final int d = 0;
    private static String e = null;
    private static Location f = null;
    private static boolean g;

    public static IMLocationMonitor getInstance() {
        if (a == null) {
            synchronized (IMLocationMonitor.class) {
                if (a == null) {
                    a = new IMLocationMonitor();
                }
            }
        }
        return a;
    }

    public synchronized void startListening(Context context) {
        if (!g) {
            b = (LocationManager) context.getSystemService("location");
            if (b != null) {
                e = b.getBestProvider(new Criteria(), true);
                if (e != null) {
                    g = true;
                    b.requestLocationUpdates(e, 0, 0.0f, this);
                }
            }
        }
    }

    public synchronized void stopListening() {
        if (b != null && g) {
            b.removeUpdates(this);
            g = false;
        }
    }

    private IMLocationMonitor() {
    }

    public Location getCurrentLocation() {
        return f;
    }

    public synchronized void onLocationChanged(Location location) {
        f = location;
    }

    public void onProviderDisabled(String str) {
    }

    public void onProviderEnabled(String str) {
    }

    public void onStatusChanged(String str, int i, Bundle bundle) {
    }
}
