package com.inmobi.commons.internal;

import java.io.UnsupportedEncodingException;

public class Base64 {
    public static final int CRLF = 4;
    public static final int DEFAULT = 0;
    public static final int NO_CLOSE = 16;
    public static final int NO_PADDING = 1;
    public static final int NO_WRAP = 2;
    public static final int URL_SAFE = 8;
    static final /* synthetic */ boolean a = (!Base64.class.desiredAssertionStatus());

    static abstract class a {
        public byte[] a;
        public int b;

        public abstract int a(int i);

        public abstract boolean a(byte[] bArr, int i, int i2, boolean z);

        a() {
        }
    }

    public static byte[] decode(String str, int i) {
        return decode(str.getBytes(), i);
    }

    public static byte[] decode(byte[] bArr, int i) {
        return decode(bArr, 0, bArr.length, i);
    }

    public static byte[] decode(byte[] bArr, int i, int i2, int i3) {
        b bVar = new b(i3, new byte[((i2 * 3) / 4)]);
        if (!bVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (bVar.b == bVar.a.length) {
            return bVar.a;
        } else {
            byte[] bArr2 = new byte[bVar.b];
            System.arraycopy(bVar.a, 0, bArr2, 0, bVar.b);
            return bArr2;
        }
    }

    static class b extends a {
        private static final int[] c = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private static final int[] d = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -2, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
        private static final int e = -1;
        private static final int f = -2;
        private int g;
        private int h;
        private final int[] i;

        public b(int i2, byte[] bArr) {
            this.a = bArr;
            this.i = (i2 & 8) == 0 ? c : d;
            this.g = 0;
            this.h = 0;
        }

        public int a(int i2) {
            return ((i2 * 3) / 4) + 10;
        }

        public boolean a(byte[] bArr, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (this.g == 6) {
                return false;
            }
            int i6 = i3 + i2;
            int i7 = this.g;
            int i8 = this.h;
            int i9 = 0;
            byte[] bArr2 = this.a;
            int[] iArr = this.i;
            int i10 = i2;
            while (true) {
                if (i10 < i6) {
                    if (i7 == 0) {
                        while (i10 + 4 <= i6 && (i8 = (iArr[bArr[i10] & 255] << 18) | (iArr[bArr[i10 + 1] & 255] << 12) | (iArr[bArr[i10 + 2] & 255] << 6) | iArr[bArr[i10 + 3] & 255]) >= 0) {
                            bArr2[i9 + 2] = (byte) i8;
                            bArr2[i9 + 1] = (byte) (i8 >> 8);
                            bArr2[i9] = (byte) (i8 >> 16);
                            i9 += 3;
                            i10 += 4;
                        }
                        if (i10 >= i6) {
                            i4 = i8;
                        }
                    }
                    int i11 = i10 + 1;
                    int i12 = iArr[bArr[i10] & 255];
                    switch (i7) {
                        case 0:
                            if (i12 >= 0) {
                                int i13 = i12;
                                i5 = i7 + 1;
                                i8 = i13;
                                continue;
                                i7 = i5;
                                i10 = i11;
                            } else if (i12 != -1) {
                                this.g = 6;
                                return false;
                            }
                            break;
                        case 1:
                            if (i12 >= 0) {
                                i8 = (i8 << 6) | i12;
                                i5 = i7 + 1;
                                continue;
                                i7 = i5;
                                i10 = i11;
                            } else if (i12 != -1) {
                                this.g = 6;
                                return false;
                            }
                            break;
                        case 2:
                            if (i12 >= 0) {
                                i8 = (i8 << 6) | i12;
                                i5 = i7 + 1;
                                continue;
                            } else if (i12 == -2) {
                                bArr2[i9] = (byte) (i8 >> 4);
                                i5 = 4;
                                i9++;
                            } else if (i12 != -1) {
                                this.g = 6;
                                return false;
                            }
                            i7 = i5;
                            i10 = i11;
                            break;
                        case 3:
                            if (i12 >= 0) {
                                i8 = (i8 << 6) | i12;
                                bArr2[i9 + 2] = (byte) i8;
                                bArr2[i9 + 1] = (byte) (i8 >> 8);
                                bArr2[i9] = (byte) (i8 >> 16);
                                i9 += 3;
                                i5 = 0;
                                continue;
                            } else if (i12 == -2) {
                                bArr2[i9 + 1] = (byte) (i8 >> 2);
                                bArr2[i9] = (byte) (i8 >> 10);
                                i9 += 2;
                                i5 = 5;
                            } else if (i12 != -1) {
                                this.g = 6;
                                return false;
                            }
                            i7 = i5;
                            i10 = i11;
                            break;
                        case 4:
                            if (i12 == -2) {
                                i5 = i7 + 1;
                                continue;
                                i7 = i5;
                                i10 = i11;
                            } else if (i12 != -1) {
                                this.g = 6;
                                return false;
                            }
                            break;
                        case 5:
                            if (i12 != -1) {
                                this.g = 6;
                                return false;
                            }
                            break;
                    }
                    i5 = i7;
                    i7 = i5;
                    i10 = i11;
                } else {
                    i4 = i8;
                }
            }
            if (!z) {
                this.g = i7;
                this.h = i4;
                this.b = i9;
                return true;
            }
            switch (i7) {
                case 1:
                    this.g = 6;
                    return false;
                case 2:
                    bArr2[i9] = (byte) (i4 >> 4);
                    i9++;
                    break;
                case 3:
                    int i14 = i9 + 1;
                    bArr2[i9] = (byte) (i4 >> 10);
                    i9 = i14 + 1;
                    bArr2[i14] = (byte) (i4 >> 2);
                    break;
                case 4:
                    this.g = 6;
                    return false;
            }
            this.g = i7;
            this.b = i9;
            return true;
        }
    }

    public static String encodeToString(byte[] bArr, int i) {
        try {
            return new String(encode(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static String encodeToString(byte[] bArr, int i, int i2, int i3) {
        try {
            return new String(encode(bArr, i, i2, i3), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] encode(byte[] bArr, int i) {
        return encode(bArr, 0, bArr.length, i);
    }

    public static byte[] encode(byte[] bArr, int i, int i2, int i3) {
        int i4;
        c cVar = new c(i3, null);
        int i5 = (i2 / 3) * 4;
        if (!cVar.e) {
            switch (i2 % 3) {
                case 1:
                    i5 += 2;
                    break;
                case 2:
                    i5 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i5 += 4;
        }
        if (cVar.f && i2 > 0) {
            int i6 = ((i2 - 1) / 57) + 1;
            if (cVar.g) {
                i4 = 2;
            } else {
                i4 = 1;
            }
            i5 += i4 * i6;
        }
        cVar.a = new byte[i5];
        cVar.a(bArr, i, i2, true);
        if (a || cVar.b == i5) {
            return cVar.a;
        }
        throw new AssertionError();
    }

    static class c extends a {
        public static final int c = 19;
        static final /* synthetic */ boolean h;
        private static final byte[] i = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] j = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        int d;
        public final boolean e;
        public final boolean f;
        public final boolean g;
        private final byte[] k;
        private int l;
        private final byte[] m;

        static {
            boolean z;
            if (!Base64.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            h = z;
        }

        public c(int i2, byte[] bArr) {
            boolean z;
            boolean z2 = true;
            this.a = bArr;
            this.e = (i2 & 1) == 0;
            if ((i2 & 2) == 0) {
                z = true;
            } else {
                z = false;
            }
            this.f = z;
            this.g = (i2 & 4) == 0 ? false : z2;
            this.m = (i2 & 8) == 0 ? i : j;
            this.k = new byte[2];
            this.d = 0;
            this.l = this.f ? 19 : -1;
        }

        public int a(int i2) {
            return ((i2 * 8) / 5) + 10;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public boolean a(byte[] bArr, int i2, int i3, boolean z) {
            int i4;
            int i5;
            int i6;
            int i7;
            byte b;
            int i8;
            byte b2;
            int i9;
            byte b3;
            int i10;
            int i11;
            int i12;
            int i13;
            byte[] bArr2 = this.m;
            byte[] bArr3 = this.a;
            int i14 = 0;
            int i15 = this.l;
            int i16 = i3 + i2;
            byte b4 = -1;
            switch (this.d) {
                case 0:
                    i4 = i2;
                    break;
                case 1:
                    if (i2 + 2 <= i16) {
                        int i17 = i2 + 1;
                        b4 = ((this.k[0] & 255) << 16) | ((bArr[i2] & 255) << 8) | (bArr[i17] & 255);
                        this.d = 0;
                        i4 = i17 + 1;
                        break;
                    }
                    i4 = i2;
                    break;
                case 2:
                    if (i2 + 1 <= i16) {
                        i4 = i2 + 1;
                        b4 = ((this.k[0] & 255) << 16) | ((this.k[1] & 255) << 8) | (bArr[i2] & 255);
                        this.d = 0;
                        break;
                    }
                    i4 = i2;
                    break;
                default:
                    i4 = i2;
                    break;
            }
            if (b4 != -1) {
                bArr3[0] = bArr2[(b4 >> 18) & 63];
                bArr3[1] = bArr2[(b4 >> 12) & 63];
                bArr3[2] = bArr2[(b4 >> 6) & 63];
                i14 = 4;
                bArr3[3] = bArr2[b4 & 63];
                i15--;
                if (i15 == 0) {
                    if (this.g) {
                        i13 = 5;
                        bArr3[4] = 13;
                    } else {
                        i13 = 4;
                    }
                    i14 = i13 + 1;
                    bArr3[i13] = 10;
                    i15 = 19;
                }
            }
            while (true) {
                int i18 = i6;
                int i19 = i5;
                if (i4 + 3 <= i16) {
                    byte b5 = ((bArr[i4] & 255) << 16) | ((bArr[i4 + 1] & 255) << 8) | (bArr[i4 + 2] & 255);
                    bArr3[i19] = bArr2[(b5 >> 18) & 63];
                    bArr3[i19 + 1] = bArr2[(b5 >> 12) & 63];
                    bArr3[i19 + 2] = bArr2[(b5 >> 6) & 63];
                    bArr3[i19 + 3] = bArr2[b5 & 63];
                    i4 += 3;
                    i5 = i19 + 4;
                    i6 = i18 - 1;
                    if (i6 == 0) {
                        if (this.g) {
                            i12 = i5 + 1;
                            bArr3[i5] = 13;
                        } else {
                            i12 = i5;
                        }
                        i5 = i12 + 1;
                        bArr3[i12] = 10;
                        i6 = 19;
                    }
                } else {
                    if (z) {
                        if (i4 - this.d == i16 - 1) {
                            if (this.d > 0) {
                                i11 = 1;
                                b3 = this.k[0];
                                i10 = i4;
                            } else {
                                b3 = bArr[i4];
                                i10 = i4 + 1;
                                i11 = 0;
                            }
                            int i20 = (b3 & 255) << 4;
                            this.d -= i11;
                            int i21 = i19 + 1;
                            bArr3[i19] = bArr2[(i20 >> 6) & 63];
                            int i22 = i21 + 1;
                            bArr3[i21] = bArr2[i20 & 63];
                            if (this.e) {
                                int i23 = i22 + 1;
                                bArr3[i22] = 61;
                                i22 = i23 + 1;
                                bArr3[i23] = 61;
                            }
                            if (this.f) {
                                if (this.g) {
                                    bArr3[i22] = 13;
                                    i22++;
                                }
                                bArr3[i22] = 10;
                                i22++;
                            }
                            i4 = i10;
                            i19 = i22;
                        } else if (i4 - this.d == i16 - 2) {
                            if (this.d > 1) {
                                i8 = 1;
                                b = this.k[0];
                            } else {
                                b = bArr[i4];
                                i4++;
                                i8 = 0;
                            }
                            int i24 = (b & 255) << 10;
                            if (this.d > 0) {
                                b2 = this.k[i8];
                                i8++;
                            } else {
                                b2 = bArr[i4];
                                i4++;
                            }
                            int i25 = ((b2 & 255) << 2) | i24;
                            this.d -= i8;
                            int i26 = i19 + 1;
                            bArr3[i19] = bArr2[(i25 >> 12) & 63];
                            int i27 = i26 + 1;
                            bArr3[i26] = bArr2[(i25 >> 6) & 63];
                            int i28 = i27 + 1;
                            bArr3[i27] = bArr2[i25 & 63];
                            if (this.e) {
                                i9 = i28 + 1;
                                bArr3[i28] = 61;
                            } else {
                                i9 = i28;
                            }
                            if (this.f) {
                                if (this.g) {
                                    bArr3[i9] = 13;
                                    i9++;
                                }
                                bArr3[i9] = 10;
                                i9++;
                            }
                            i19 = i9;
                        } else if (this.f && i19 > 0 && i18 != 19) {
                            if (this.g) {
                                i7 = i19 + 1;
                                bArr3[i19] = 13;
                            } else {
                                i7 = i19;
                            }
                            i19 = i7 + 1;
                            bArr3[i7] = 10;
                        }
                        if (!h && this.d != 0) {
                            throw new AssertionError();
                        } else if (!h && i4 != i16) {
                            throw new AssertionError();
                        }
                    } else if (i4 == i16 - 1) {
                        byte[] bArr4 = this.k;
                        int i29 = this.d;
                        this.d = i29 + 1;
                        bArr4[i29] = bArr[i4];
                    } else if (i4 == i16 - 2) {
                        byte[] bArr5 = this.k;
                        int i30 = this.d;
                        this.d = i30 + 1;
                        bArr5[i30] = bArr[i4];
                        byte[] bArr6 = this.k;
                        int i31 = this.d;
                        this.d = i31 + 1;
                        bArr6[i31] = bArr[i4 + 1];
                    }
                    this.b = i19;
                    this.l = i18;
                    return true;
                }
            }
        }
    }

    private Base64() {
    }
}
