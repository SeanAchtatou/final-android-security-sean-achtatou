package com.inmobi.commons.internal;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

public class IMFileOperations {
    public static boolean setPreferences(Context context, String str, String str2, String str3) {
        if (context == null || str == null || str2 == null || "".equals(str.trim()) || "".equals(str2.trim())) {
            IMLog.internal("IMCOMMONS_V_1_0_0", "Failed to set preferences..App context NULL");
            return false;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putString(str2, str3);
        edit.commit();
        return true;
    }

    public static void setPreferences(Context context, String str, String str2, boolean z) {
        if (context == null || str == null || str2 == null || "".equals(str.trim()) || "".equals(str2.trim())) {
            IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to set preferences..App context NULL");
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putBoolean(str2, z);
        edit.commit();
    }

    public static void setPreferences(Context context, String str, String str2, int i) {
        if (context == null || str == null || str2 == null || "".equals(str.trim()) || "".equals(str2.trim())) {
            IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to set preferences..App context NULL");
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putInt(str2, i);
        edit.commit();
    }

    public static void setPreferences(Context context, String str, String str2, long j) {
        if (context == null || str == null || str2 == null || "".equals(str.trim()) || "".equals(str2.trim())) {
            IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to set preferences..App context NULL");
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putLong(str2, j);
        edit.commit();
    }

    public static void setPreferences(Context context, String str, String str2, float f) {
        if (context == null || str == null || str2 == null || "".equals(str.trim()) || "".equals(str2.trim())) {
            IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to set preferences..App context NULL");
            return;
        }
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putFloat(str2, f);
        edit.commit();
    }

    public static String getPreferences(Context context, String str, String str2) {
        if (context != null && str != null && str2 != null && !"".equals(str.trim()) && !"".equals(str2.trim())) {
            return context.getSharedPreferences(str, 0).getString(str2, null);
        }
        IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to get preferences..App context NULL");
        return null;
    }

    public static boolean getBooleanPreferences(Context context, String str, String str2) {
        if (context != null && str != null && str2 != null && !"".equals(str.trim()) && !"".equals(str2.trim())) {
            return context.getSharedPreferences(str, 0).getBoolean(str2, false);
        }
        IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to get preferences..App context NULL");
        return false;
    }

    public static int getIntPreferences(Context context, String str, String str2) {
        if (context != null && str != null && str2 != null && !"".equals(str.trim()) && !"".equals(str2.trim())) {
            return context.getSharedPreferences(str, 0).getInt(str2, 0);
        }
        IMLog.debug("IMCOMMONS_V_1_0_0", "Failed to get preferences..App context NULL");
        return 0;
    }

    public static Object readFromFile(Context context, String str) {
        if (context == null || str == null || "".equals(str.trim())) {
            IMLog.internal("IMCOMMONS_V_1_0_0", "Cannot read map application context or Filename NULL");
            return null;
        }
        try {
            try {
                ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(new File(context.getDir("data", 0), str)));
                try {
                    Object readObject = objectInputStream.readObject();
                    try {
                        objectInputStream.close();
                        return readObject;
                    } catch (IOException e) {
                        IMLog.internal("IMCOMMONS_V_1_0_0", "Log File Close Exception");
                        return false;
                    }
                } catch (EOFException e2) {
                    IMLog.internal("IMCOMMONS_V_1_0_0", "End of file", e2);
                    return null;
                } catch (Exception e3) {
                    IMLog.internal("IMCOMMONS_V_1_0_0", "Error reading Event log file", e3);
                    return null;
                }
            } catch (StreamCorruptedException e4) {
                IMLog.internal("IMCOMMONS_V_1_0_0", "Event log File corrupted", e4);
                return null;
            } catch (IOException e5) {
                IMLog.internal("IMCOMMONS_V_1_0_0", "Event log File IO Exception", e5);
                return null;
            }
        } catch (FileNotFoundException e6) {
            IMLog.internal("IMCOMMONS_V_1_0_0", "Event log File doesnot exist", e6);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static boolean saveToFile(Context context, String str, Object obj) {
        if (context == null || str == null || "".equals(str.trim()) || obj == null) {
            IMLog.internal("IMCOMMONS_V_1_0_0", "Cannot read map application context of Filename NULL");
            return false;
        }
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(new File(context.getDir("data", 0), str), false));
            try {
                objectOutputStream.writeObject(obj);
                try {
                    objectOutputStream.flush();
                    try {
                        objectOutputStream.close();
                        return true;
                    } catch (IOException e) {
                        IMLog.internal("IMCOMMONS_V_1_0_0", "Log File Close Exception");
                        return false;
                    }
                } catch (IOException e2) {
                    IMLog.internal("IMCOMMONS_V_1_0_0", "Log File IO Exception", e2);
                    return false;
                }
            } catch (IOException e3) {
                IMLog.internal("IMCOMMONS_V_1_0_0", "Log File IO Exception write ERROR", e3);
                return false;
            }
        } catch (FileNotFoundException e4) {
            IMLog.internal("IMCOMMONS_V_1_0_0", "Log File Not found", e4);
            return false;
        } catch (IOException e5) {
            IMLog.internal("IMCOMMONS_V_1_0_0", "Log File IO Exception", e5);
            return false;
        }
    }
}
