package com.inmobi.commons.internal;

import android.util.Log;

public class IMLog {
    protected static INTERNAL_LOG_LEVEL debugLevel = INTERNAL_LOG_LEVEL.NONE;

    public enum INTERNAL_LOG_LEVEL {
        NONE(0),
        DEBUG(1),
        INTERNAL(2);
        
        private final int a;

        private INTERNAL_LOG_LEVEL(int i) {
            this.a = i;
        }

        public int getValue() {
            return this.a;
        }
    }

    public static INTERNAL_LOG_LEVEL getLogLevel() {
        return debugLevel;
    }

    public static void setInternalLogLevel(INTERNAL_LOG_LEVEL internal_log_level) {
        debugLevel = internal_log_level;
    }

    public static void debug(String str, String str2) {
        if (debugLevel.getValue() >= INTERNAL_LOG_LEVEL.DEBUG.getValue()) {
            Log.d(str, str2);
        }
    }

    public static void debug(String str, String str2, Throwable th) {
        switch (debugLevel) {
            case DEBUG:
                debug(str, str2);
                return;
            case INTERNAL:
                internal(str, str2, th);
                return;
            default:
                return;
        }
    }

    public static void internal(String str, String str2) {
        if (debugLevel.getValue() >= INTERNAL_LOG_LEVEL.INTERNAL.getValue()) {
            StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
            Log.v(str, stackTraceElement.getFileName() + ": " + stackTraceElement.getMethodName() + " " + str2);
        }
    }

    public static void internal(String str, String str2, Throwable th) {
        if (debugLevel.getValue() >= INTERNAL_LOG_LEVEL.INTERNAL.getValue()) {
            StackTraceElement stackTraceElement = new Throwable().getStackTrace()[1];
            Log.v(str, stackTraceElement.getFileName() + ": " + stackTraceElement.getMethodName() + " " + str2, th);
        }
    }
}
