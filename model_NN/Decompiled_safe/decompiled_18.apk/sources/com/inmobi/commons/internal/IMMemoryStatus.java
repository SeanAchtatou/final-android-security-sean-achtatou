package com.inmobi.commons.internal;

import android.os.Environment;

public class IMMemoryStatus {
    private static final int a = -1;

    public static synchronized boolean externalMemoryAvailable() {
        boolean equals;
        synchronized (IMMemoryStatus.class) {
            equals = Environment.getExternalStorageState().equals("mounted");
        }
        return equals;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        r0 = -1;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized long getTotalInternalMemorySize() {
        /*
            java.lang.Class<com.inmobi.commons.internal.IMMemoryStatus> r2 = com.inmobi.commons.internal.IMMemoryStatus.class
            monitor-enter(r2)
            java.io.File r0 = android.os.Environment.getDataDirectory()     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            android.os.StatFs r1 = new android.os.StatFs     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            java.lang.String r0 = r0.getPath()     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            int r0 = r1.getBlockSize()     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            long r3 = (long) r0     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            int r0 = r1.getBlockCount()     // Catch:{ Exception -> 0x001d, all -> 0x0021 }
            long r0 = (long) r0
            long r0 = r0 * r3
        L_0x001b:
            monitor-exit(r2)
            return r0
        L_0x001d:
            r0 = move-exception
            r0 = -1
            goto L_0x001b
        L_0x0021:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.commons.internal.IMMemoryStatus.getTotalInternalMemorySize():long");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized long getTotalExternalMemorySize() {
        /*
            r0 = -1
            java.lang.Class<com.inmobi.commons.internal.IMMemoryStatus> r2 = com.inmobi.commons.internal.IMMemoryStatus.class
            monitor-enter(r2)
            boolean r3 = externalMemoryAvailable()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            if (r3 == 0) goto L_0x0023
            java.io.File r3 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            android.os.StatFs r4 = new android.os.StatFs     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            java.lang.String r3 = r3.getPath()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            int r3 = r4.getBlockSize()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            long r5 = (long) r3     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            int r0 = r4.getBlockCount()     // Catch:{ Exception -> 0x0028, all -> 0x0025 }
            long r0 = (long) r0
            long r0 = r0 * r5
        L_0x0023:
            monitor-exit(r2)
            return r0
        L_0x0025:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        L_0x0028:
            r3 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.inmobi.commons.internal.IMMemoryStatus.getTotalExternalMemorySize():long");
    }

    public static synchronized String formatSize(long j) {
        long j2;
        String str = null;
        synchronized (IMMemoryStatus.class) {
            if (j != 0) {
                if (j >= 1024) {
                    str = " KB";
                    j2 = j / 1024;
                } else {
                    j2 = j;
                }
                if (j2 >= 1024) {
                    str = " MB";
                    j2 /= 1024;
                }
                if (j2 >= 1024) {
                    str = " GB";
                    j2 /= 1024;
                }
                str = j2 + str;
            }
        }
        return str;
    }
}
