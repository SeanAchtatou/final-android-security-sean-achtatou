package com.inmobi.commons.internal;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.view.Display;
import android.webkit.WebView;
import com.adsdk.sdk.Const;
import com.jumptap.adtag.media.VideoCacheItem;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import org.apache.cordova.NetworkManager;

public class InternalSDKUtil {
    public static final String INMOBI_SDK_RELEASE_VERSION = "3.6.0";
    static final String a = "IMCOMMONS_V_1_0_0";
    private static final String b = "C10F7968CFE2C76AC6F0650C877806D4514DE58FC239592D2385BCE5609A84B2A0FBDAF29B05505EAD1FDFEF3D7209ACBF34B5D0A806DF18147EA9C0337D6B5B";
    private static final String c = "010001";
    private static int d = 0;
    private static String e = null;
    private static final String f = "getRotation";
    private static final String g = "getOrientation";

    public static String byteToHex(byte b2) {
        try {
            char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
            return new String(new char[]{cArr[(b2 >> 4) & 15], cArr[b2 & 15]});
        } catch (Exception e2) {
            return null;
        }
    }

    public static String getODIN1(String str) {
        if (str == null) {
            return null;
        }
        try {
            if ("".equals(str.trim())) {
                return null;
            }
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toString((b2 & 255) + 256, 16).substring(1));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            IMLog.debug(a, "Exception in getting ODIN-1", e2);
            return null;
        }
    }

    public static String getUIDMap(String str, String str2, String str3, String str4, int i, String str5) {
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        sb.append("{");
        if (str != null) {
            String a2 = a(str, str5);
            sb.append("LID:'");
            sb.append(a2);
            sb.append("'");
            z = true;
        }
        if (str2 != null) {
            if (z) {
                sb.append(VideoCacheItem.URL_DELIMITER);
            }
            String a3 = a(str2, str5);
            sb.append("SID:'");
            sb.append(a3);
            sb.append("'");
            z = true;
        }
        if (str4 != null) {
            if (z) {
                sb.append(VideoCacheItem.URL_DELIMITER);
            }
            String a4 = a(str4, str5);
            sb.append("UDID:'");
            sb.append(a4);
            sb.append("'");
            z = true;
        }
        if (i <= 0) {
            if (str3 != null) {
                if (z) {
                    sb.append(VideoCacheItem.URL_DELIMITER);
                }
                String a5 = a(str3, str5);
                sb.append("O1:'");
                sb.append(a5);
                sb.append("'");
                z = true;
            }
        } else if (!(str3 == null || (i & 2) == 2 || (i & 1) != 1)) {
            if (z) {
                sb.append(VideoCacheItem.URL_DELIMITER);
            }
            String a6 = a(str3, str5);
            sb.append("O1:'");
            sb.append(a6);
            sb.append("'");
            z = true;
        }
        sb.append("}");
        if (z) {
            return encryptRSA(sb.toString());
        }
        return null;
    }

    private static String a(String str, String str2) {
        try {
            byte[] bytes = str.getBytes(Const.ENCODING);
            byte[] bArr = new byte[bytes.length];
            byte[] bytes2 = str2.getBytes(Const.ENCODING);
            for (int i = 0; i < bytes.length; i++) {
                bArr[i] = (byte) (bytes[i] ^ bytes2[i % bytes2.length]);
            }
            return new String(Base64.encode(bArr, 2), Const.ENCODING);
        } catch (Exception e2) {
            IMLog.debug(a, "Exception in xor with random integer", e2);
            return "";
        }
    }

    public static String encryptRSA(String str) {
        try {
            BigInteger bigInteger = new BigInteger(b, 16);
            BigInteger bigInteger2 = new BigInteger(c, 16);
            Cipher instance = Cipher.getInstance("RSA/ECB/nopadding");
            instance.init(1, (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(bigInteger, bigInteger2)));
            return new String(Base64.encode(a(str.getBytes(Const.ENCODING), 1, instance), 0));
        } catch (Exception e2) {
            IMLog.debug(a, "Exception in encryptRSA", e2);
            return null;
        }
    }

    private static byte[] a(byte[] bArr, int i, Cipher cipher) throws IllegalBlockSizeException, BadPaddingException {
        int i2;
        byte[] bArr2 = new byte[0];
        byte[] bArr3 = new byte[0];
        if (i == 1) {
        }
        int length = bArr.length;
        byte[] bArr4 = new byte[64];
        byte[] bArr5 = bArr3;
        for (int i3 = 0; i3 < length; i3++) {
            if (i3 > 0 && i3 % 64 == 0) {
                bArr5 = a(bArr5, cipher.doFinal(bArr4));
                if (i3 + 64 > length) {
                    i2 = length - i3;
                } else {
                    i2 = 64;
                }
                bArr4 = new byte[i2];
            }
            bArr4[i3 % 64] = bArr[i3];
        }
        return a(bArr5, cipher.doFinal(bArr4));
    }

    private static byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[(bArr.length + bArr2.length)];
        for (int i = 0; i < bArr.length; i++) {
            bArr3[i] = bArr[i];
        }
        for (int i2 = 0; i2 < bArr2.length; i2++) {
            bArr3[bArr.length + i2] = bArr2[i2];
        }
        return bArr3;
    }

    public static synchronized int incrementBaseUrl() {
        int i;
        synchronized (InternalSDKUtil.class) {
            d++;
            i = d;
        }
        return i;
    }

    public static boolean validateAppId(String str) {
        if (str == null) {
            IMLog.debug(a, "appId is null");
            return false;
        } else if (str.matches("(x)+")) {
            IMLog.debug(a, "appId is all xxxxxxx");
            return false;
        } else if (!"".equals(str.trim())) {
            return true;
        } else {
            IMLog.debug(a, "appId is all blank");
            return false;
        }
    }

    public static String getAndroidId(Context context) {
        String str = null;
        try {
            str = Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e2) {
        }
        if (str != null) {
            return str;
        }
        try {
            return Settings.System.getString(context.getContentResolver(), "android_id");
        } catch (Exception e3) {
            return str;
        }
    }

    public static String getConnectivityType(Context context) {
        ConnectivityManager connectivityManager;
        try {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0 && (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) != null) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                int type = activeNetworkInfo.getType();
                int subtype = activeNetworkInfo.getSubtype();
                if (type == 1) {
                    return "wifi";
                }
                if (type == 0) {
                    if (subtype == 1) {
                        return NetworkManager.GPRS;
                    }
                    if (subtype == 2) {
                        return NetworkManager.EDGE;
                    }
                    if (subtype == 3) {
                        return NetworkManager.UMTS;
                    }
                    if (subtype == 0) {
                        return "carrier";
                    }
                    return "carrier";
                }
            }
            return null;
        } catch (Exception e2) {
            IMLog.internal(a, "Error getting the network info", e2);
            return null;
        }
    }

    public static String getUserAgent(Context context) {
        if (e == null) {
            synchronized (InternalSDKUtil.class) {
                if (e == null) {
                    try {
                        e = new WebView(context).getSettings().getUserAgentString();
                    } catch (Exception e2) {
                    }
                }
            }
        }
        return e;
    }

    public static int getDisplayRotation(Display display) {
        Method method = null;
        try {
            method = Display.class.getMethod(f, null);
        } catch (NoSuchMethodException e2) {
            try {
                method = Display.class.getMethod(g, null);
            } catch (NoSuchMethodException e3) {
            }
        }
        if (method == null) {
            return -999;
        }
        try {
            return ((Integer) method.invoke(display, null)).intValue();
        } catch (Exception e4) {
            return -999;
        }
    }

    public static boolean getWhetherTablet(int i, int i2, int i3) {
        if (i2 > i3 && (i == 0 || i == 2)) {
            return true;
        }
        if (i2 >= i3 || (i != 1 && i != 3)) {
            return false;
        }
        return true;
    }

    public static String getODIN1MD5(String str) {
        String odin1;
        if (str == null) {
            return null;
        }
        try {
            if ("".equals(str.trim()) || (odin1 = getODIN1(str)) == null) {
                return null;
            }
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(odin1.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toString((b2 & 255) + 256, 16).substring(1));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            IMLog.debug(a, "Error generating Odin1");
            return null;
        }
    }

    public static String getAndroidIdMD5(Context context) {
        try {
            String androidId = getAndroidId(context);
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(androidId.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                stringBuffer.append(Integer.toString((b2 & 255) + 256, 16).substring(1));
            }
            return stringBuffer.toString();
        } catch (Exception e2) {
            IMLog.internal(a, "Exception in getting MD5 Android Id", e2);
            return null;
        }
    }

    public static boolean checkNetworkAvailibility(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager.getActiveNetworkInfo() == null) {
            return false;
        }
        return connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
