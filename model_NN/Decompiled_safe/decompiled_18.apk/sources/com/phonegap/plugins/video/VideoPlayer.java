package com.phonegap.plugins.video;

import android.content.Intent;
import android.net.Uri;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

public class VideoPlayer extends Plugin {
    private static final String ASSETS = "file:///android_asset/";
    private static final String YOU_TUBE = "youtube.com";

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        try {
            if (action.equals("playVideo")) {
                playVideo(args.getString(0));
            } else {
                status = PluginResult.Status.INVALID_ACTION;
            }
            return new PluginResult(status, "");
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        } catch (IOException e2) {
            return new PluginResult(PluginResult.Status.IO_EXCEPTION);
        }
    }

    private void playVideo(String url) throws IOException {
        Intent intent;
        Uri uri = Uri.parse(url);
        if (url.contains(YOU_TUBE)) {
            intent = new Intent("android.intent.action.VIEW", Uri.parse("vnd.youtube:" + uri.getQueryParameter("v")));
        } else if (url.contains(ASSETS)) {
            String filepath = url.replace(ASSETS, "");
            String filename = filepath.substring(filepath.lastIndexOf("/") + 1, filepath.length());
            if (!new File(this.cordova.getActivity().getFilesDir() + "/" + filename).exists()) {
                copy(filepath, filename);
            }
            Uri uri2 = Uri.parse("file://" + this.cordova.getActivity().getFilesDir() + "/" + filename);
            intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(uri2, "video/*");
        } else {
            intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(uri, "video/*");
        }
        this.cordova.getActivity().startActivity(intent);
    }

    private void copy(String fileFrom, String fileTo) throws IOException {
        InputStream in = this.cordova.getActivity().getAssets().open(fileFrom);
        FileOutputStream out = this.cordova.getActivity().openFileOutput(fileTo, 1);
        byte[] buf = new byte[1024];
        while (true) {
            int len = in.read(buf);
            if (len > 0) {
                out.write(buf, 0, len);
            } else {
                in.close();
                out.close();
                return;
            }
        }
    }
}
