package com.phonegap.plugins.childBrowser;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import java.io.IOException;
import org.apache.cordova.DroidGap;
import org.apache.cordova.api.Plugin;
import org.apache.cordova.api.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChildBrowser extends Plugin {
    /* access modifiers changed from: private */
    public static int CLOSE_EVENT = 0;
    /* access modifiers changed from: private */
    public static int LOCATION_CHANGED_EVENT = 1;
    protected static final String LOG_TAG = "ChildBrowser";
    private String browserCallbackId = null;
    /* access modifiers changed from: private */
    public Dialog dialog;
    /* access modifiers changed from: private */
    public EditText edittext;
    private boolean showLocationBar = true;
    /* access modifiers changed from: private */
    public WebView webview;

    public PluginResult execute(String action, JSONArray args, String callbackId) {
        PluginResult.Status status = PluginResult.Status.OK;
        String result = "";
        try {
            if (action.equals("showWebPage")) {
                this.browserCallbackId = callbackId;
                if (this.dialog != null && this.dialog.isShowing()) {
                    return new PluginResult(PluginResult.Status.ERROR, "ChildBrowser is already open");
                }
                String result2 = showWebPage(args.getString(0), args.optJSONObject(1));
                if (result2.length() > 0) {
                    return new PluginResult(PluginResult.Status.ERROR, result2);
                }
                PluginResult pluginResult = new PluginResult(status, result2);
                pluginResult.setKeepCallback(true);
                return pluginResult;
            } else if (action.equals("close")) {
                closeDialog();
                JSONObject obj = new JSONObject();
                obj.put("type", CLOSE_EVENT);
                PluginResult pluginResult2 = new PluginResult(status, obj);
                pluginResult2.setKeepCallback(false);
                return pluginResult2;
            } else {
                if (action.equals("openExternal")) {
                    result = openExternal(args.getString(0), args.optBoolean(1));
                    if (result.length() > 0) {
                        status = PluginResult.Status.ERROR;
                    }
                } else {
                    status = PluginResult.Status.INVALID_ACTION;
                }
                return new PluginResult(status, result);
            }
        } catch (JSONException e) {
            return new PluginResult(PluginResult.Status.JSON_EXCEPTION);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public String openExternal(String url, boolean usePhoneGap) {
        Intent intent;
        if (usePhoneGap) {
            try {
                intent = new Intent().setClass(this.cordova.getActivity(), DroidGap.class);
                intent.setData(Uri.parse(url));
                intent.putExtra("url", url);
                intent.putExtra("loadUrlTimeoutValue", 60000);
                intent.putExtra("loadingDialog", "Wait,Loading web page...");
                intent.putExtra("hideLoadingDialogOnPageLoad", true);
            } catch (ActivityNotFoundException e) {
                e = e;
                Log.d(LOG_TAG, "ChildBrowser: Error loading url " + url + ":" + e.toString());
                return e.toString();
            }
        } else {
            Intent intent2 = new Intent("android.intent.action.VIEW");
            try {
                intent2.setData(Uri.parse(url));
                intent = intent2;
            } catch (ActivityNotFoundException e2) {
                e = e2;
                Log.d(LOG_TAG, "ChildBrowser: Error loading url " + url + ":" + e.toString());
                return e.toString();
            }
        }
        this.cordova.getActivity().startActivity(intent);
        return "";
    }

    /* access modifiers changed from: private */
    public void closeDialog() {
        if (this.dialog != null) {
            this.dialog.dismiss();
        }
    }

    /* access modifiers changed from: private */
    public void goBack() {
        if (this.webview.canGoBack()) {
            this.webview.goBack();
        }
    }

    /* access modifiers changed from: private */
    public void goForward() {
        if (this.webview.canGoForward()) {
            this.webview.goForward();
        }
    }

    /* access modifiers changed from: private */
    public void navigate(String url) {
        ((InputMethodManager) this.cordova.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.edittext.getWindowToken(), 0);
        if (url.startsWith("http") || url.startsWith("file:")) {
            this.webview.loadUrl(url);
        } else {
            this.webview.loadUrl("http://" + url);
        }
        this.webview.requestFocus();
    }

    /* access modifiers changed from: private */
    public boolean getShowLocationBar() {
        return this.showLocationBar;
    }

    public String showWebPage(final String url, JSONObject options) {
        if (options != null) {
            this.showLocationBar = options.optBoolean("showLocationBar", true);
        }
        this.cordova.getActivity().runOnUiThread(new Runnable() {
            private int dpToPixels(int dipValue) {
                return (int) TypedValue.applyDimension(1, (float) dipValue, ChildBrowser.this.cordova.getActivity().getResources().getDisplayMetrics());
            }

            public void run() {
                Dialog unused = ChildBrowser.this.dialog = new Dialog(ChildBrowser.this.cordova.getActivity(), 16973830);
                ChildBrowser.this.dialog.getWindow().getAttributes().windowAnimations = 16973826;
                ChildBrowser.this.dialog.requestWindowFeature(1);
                ChildBrowser.this.dialog.setCancelable(true);
                ChildBrowser.this.dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog) {
                        try {
                            JSONObject obj = new JSONObject();
                            obj.put("type", ChildBrowser.CLOSE_EVENT);
                            ChildBrowser.this.sendUpdate(obj, false);
                        } catch (JSONException e) {
                            Log.d(ChildBrowser.LOG_TAG, "Should never happen");
                        }
                    }
                });
                LinearLayout main = new LinearLayout(ChildBrowser.this.cordova.getActivity());
                main.setOrientation(1);
                RelativeLayout toolbar = new RelativeLayout(ChildBrowser.this.cordova.getActivity());
                toolbar.setLayoutParams(new RelativeLayout.LayoutParams(-1, dpToPixels(44)));
                toolbar.setPadding(dpToPixels(2), dpToPixels(2), dpToPixels(2), dpToPixels(2));
                toolbar.setHorizontalGravity(3);
                toolbar.setVerticalGravity(48);
                RelativeLayout actionButtonContainer = new RelativeLayout(ChildBrowser.this.cordova.getActivity());
                actionButtonContainer.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
                actionButtonContainer.setHorizontalGravity(3);
                actionButtonContainer.setVerticalGravity(16);
                actionButtonContainer.setId(1);
                ImageButton back = new ImageButton(ChildBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams backLayoutParams = new RelativeLayout.LayoutParams(-2, -1);
                backLayoutParams.addRule(5);
                back.setLayoutParams(backLayoutParams);
                back.setContentDescription("Back Button");
                back.setId(2);
                try {
                    back.setImageBitmap(loadDrawable("www/childbrowser/icon_arrow_left.png"));
                } catch (IOException e) {
                    Log.e(ChildBrowser.LOG_TAG, e.getMessage(), e);
                }
                back.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ChildBrowser.this.goBack();
                    }
                });
                ImageButton forward = new ImageButton(ChildBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams forwardLayoutParams = new RelativeLayout.LayoutParams(-2, -1);
                forwardLayoutParams.addRule(1, 2);
                forward.setLayoutParams(forwardLayoutParams);
                forward.setContentDescription("Forward Button");
                forward.setId(3);
                try {
                    forward.setImageBitmap(loadDrawable("www/childbrowser/icon_arrow_right.png"));
                } catch (IOException e2) {
                    Log.e(ChildBrowser.LOG_TAG, e2.getMessage(), e2);
                }
                forward.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ChildBrowser.this.goForward();
                    }
                });
                EditText unused2 = ChildBrowser.this.edittext = new EditText(ChildBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams textLayoutParams = new RelativeLayout.LayoutParams(-1, -1);
                textLayoutParams.addRule(1, 1);
                textLayoutParams.addRule(0, 5);
                ChildBrowser.this.edittext.setLayoutParams(textLayoutParams);
                ChildBrowser.this.edittext.setId(4);
                ChildBrowser.this.edittext.setSingleLine(true);
                ChildBrowser.this.edittext.setText(url);
                ChildBrowser.this.edittext.setInputType(16);
                ChildBrowser.this.edittext.setImeOptions(2);
                ChildBrowser.this.edittext.setInputType(0);
                ChildBrowser.this.edittext.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() != 0 || keyCode != 66) {
                            return false;
                        }
                        ChildBrowser.this.navigate(ChildBrowser.this.edittext.getText().toString());
                        return true;
                    }
                });
                ImageButton close = new ImageButton(ChildBrowser.this.cordova.getActivity());
                RelativeLayout.LayoutParams closeLayoutParams = new RelativeLayout.LayoutParams(-2, -1);
                closeLayoutParams.addRule(11);
                close.setLayoutParams(closeLayoutParams);
                forward.setContentDescription("Close Button");
                close.setId(5);
                try {
                    close.setImageBitmap(loadDrawable("www/childbrowser/icon_close.png"));
                } catch (IOException e3) {
                    Log.e(ChildBrowser.LOG_TAG, e3.getMessage(), e3);
                }
                close.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        ChildBrowser.this.closeDialog();
                    }
                });
                WebView unused3 = ChildBrowser.this.webview = new WebView(ChildBrowser.this.cordova.getActivity());
                ChildBrowser.this.webview.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
                ChildBrowser.this.webview.setWebChromeClient(new WebChromeClient());
                ChildBrowser.this.webview.setWebViewClient(new ChildBrowserClient(ChildBrowser.this.edittext));
                WebSettings settings = ChildBrowser.this.webview.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setJavaScriptCanOpenWindowsAutomatically(true);
                settings.setBuiltInZoomControls(true);
                settings.setPluginsEnabled(true);
                settings.setDomStorageEnabled(true);
                ChildBrowser.this.webview.loadUrl(url);
                ChildBrowser.this.webview.setId(6);
                ChildBrowser.this.webview.getSettings().setLoadWithOverviewMode(true);
                ChildBrowser.this.webview.getSettings().setUseWideViewPort(true);
                ChildBrowser.this.webview.requestFocus();
                ChildBrowser.this.webview.requestFocusFromTouch();
                actionButtonContainer.addView(back);
                actionButtonContainer.addView(forward);
                toolbar.addView(actionButtonContainer);
                toolbar.addView(ChildBrowser.this.edittext);
                toolbar.addView(close);
                if (ChildBrowser.this.getShowLocationBar()) {
                    main.addView(toolbar);
                }
                main.addView(ChildBrowser.this.webview);
                WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                lp.copyFrom(ChildBrowser.this.dialog.getWindow().getAttributes());
                lp.width = -1;
                lp.height = -1;
                ChildBrowser.this.dialog.setContentView(main);
                ChildBrowser.this.dialog.show();
                ChildBrowser.this.dialog.getWindow().setAttributes(lp);
            }

            private Bitmap loadDrawable(String filename) throws IOException {
                return BitmapFactory.decodeStream(ChildBrowser.this.cordova.getActivity().getAssets().open(filename));
            }
        });
        return "";
    }

    /* access modifiers changed from: private */
    public void sendUpdate(JSONObject obj, boolean keepCallback) {
        if (this.browserCallbackId != null) {
            PluginResult result = new PluginResult(PluginResult.Status.OK, obj);
            result.setKeepCallback(keepCallback);
            success(result, this.browserCallbackId);
        }
    }

    public class ChildBrowserClient extends WebViewClient {
        EditText edittext;

        public ChildBrowserClient(EditText mEditText) {
            this.edittext = mEditText;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            String newloc;
            super.onPageStarted(view, url, favicon);
            if (url.startsWith("http:") || url.startsWith("https:") || url.startsWith("file:")) {
                newloc = url;
            } else {
                newloc = "http://" + url;
            }
            if (!newloc.equals(this.edittext.getText().toString())) {
                this.edittext.setText(newloc);
            }
            try {
                JSONObject obj = new JSONObject();
                obj.put("type", ChildBrowser.LOCATION_CHANGED_EVENT);
                obj.put("location", url);
                ChildBrowser.this.sendUpdate(obj, true);
            } catch (JSONException e) {
                Log.d(ChildBrowser.LOG_TAG, "This should never happen");
            }
        }
    }
}
