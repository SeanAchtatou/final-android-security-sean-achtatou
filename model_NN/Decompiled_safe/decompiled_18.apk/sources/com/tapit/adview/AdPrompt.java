package com.tapit.adview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import com.google.ads.AdActivity;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class AdPrompt {
    private static final String AD_TYPE_DIALOG = "10";
    protected AdRequest adRequest;
    /* access modifiers changed from: private */
    public String callToAction;
    /* access modifiers changed from: private */
    public String clickUrl;
    private LoadContentTask contentTask;
    protected Context context;
    /* access modifiers changed from: private */
    public String declineStr;
    /* access modifiers changed from: private */
    public String html;
    /* access modifiers changed from: private */
    public AdPromptCallbackListener listener;
    /* access modifiers changed from: private */
    public boolean loaded = false;
    /* access modifiers changed from: private */
    public boolean showAfterLoad;
    /* access modifiers changed from: private */
    public String title;

    public interface AdPromptCallbackListener {
        void adPromptClosed(AdPrompt adPrompt, boolean z);

        void adPromptDisplayed(AdPrompt adPrompt);

        void adPromptError(AdPrompt adPrompt, String str);

        void adPromptLoaded(AdPrompt adPrompt);
    }

    public AdPrompt(Context context2, String str) {
        this.context = context2;
        this.adRequest = new AdRequest(str);
        this.adRequest.setAdtype(AD_TYPE_DIALOG);
        this.adRequest.initDefaultParameters(context2);
    }

    public void setLongitude(String str) {
        if (this.adRequest != null && str != null) {
            this.adRequest.setLongitude(str);
        }
    }

    public String getLongitude() {
        if (this.adRequest == null) {
            return null;
        }
        String longitude = this.adRequest.getLongitude();
        if (longitude != null) {
            return longitude;
        }
        return null;
    }

    public void setCustomParameters(Hashtable<String, String> hashtable) {
        if (this.adRequest != null) {
            this.adRequest.setCustomParameters(hashtable);
        }
    }

    public void setCustomParameters(Map<String, String> map) {
        if (this.adRequest != null) {
            this.adRequest.setCustomParameters(map);
        }
    }

    public void setListener(AdPromptCallbackListener adPromptCallbackListener) {
        this.listener = adPromptCallbackListener;
    }

    public void load() {
        this.contentTask = new LoadContentTask(this);
        this.contentTask.execute(0);
    }

    public boolean isLoaded() {
        return this.loaded;
    }

    public void showAdPrompt() {
        if (!this.loaded) {
            this.showAfterLoad = true;
            load();
            return;
        }
        displayAdPrompt(this.title, this.html, this.callToAction, this.declineStr, this.clickUrl);
    }

    private void displayAdPrompt(String str, String str2, String str3, String str4, String str5) {
        final Activity activity = (Activity) this.context;
        final AdPromptCallbackListener adPromptCallbackListener = this.listener;
        try {
            final String str6 = str5;
            AlertDialog create = new AlertDialog.Builder(this.context).setPositiveButton(str3, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    new Thread(new Runnable() {
                        public void run() {
                            activity.startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(str6)), 2);
                            if (adPromptCallbackListener != null) {
                                adPromptCallbackListener.adPromptClosed(this, true);
                            }
                        }
                    }).start();
                }
            }).setNegativeButton(str4, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (adPromptCallbackListener != null) {
                        adPromptCallbackListener.adPromptClosed(this, false);
                    }
                }
            }).create();
            create.setTitle(str);
            create.show();
            if (this.listener != null) {
                this.listener.adPromptDisplayed(this);
            }
        } catch (Exception e) {
            if (this.listener != null) {
                this.listener.adPromptError(this, e.getMessage());
            }
            Log.e("TapIt", "An error occured while attempting to display AdPrompt", e);
        }
    }

    /* access modifiers changed from: private */
    public String requestGet(String str) throws IOException {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        Log.d("TapIt", str);
        InputStream content = defaultHttpClient.execute(new HttpGet(str)).getEntity().getContent();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(content, 8192);
        String readInputStream = readInputStream(bufferedInputStream);
        Log.d("TapIt", readInputStream);
        bufferedInputStream.close();
        content.close();
        return readInputStream;
    }

    private static String readInputStream(BufferedInputStream bufferedInputStream) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
        }
    }

    private class LoadContentTask extends AsyncTask<Integer, Integer, String> {
        private AdPrompt theAdPrompt;

        public LoadContentTask(AdPrompt adPrompt) {
            this.theAdPrompt = adPrompt;
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Integer... numArr) {
            try {
                return AdPrompt.this.requestGet(AdPrompt.this.adRequest.createURL());
            } catch (IOException e) {
                return "{\"error\": \"" + e.getMessage() + "\"}";
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            String message;
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has("error")) {
                    if (AdPrompt.this.listener != null) {
                        AdPrompt.this.listener.adPromptError(this.theAdPrompt, jSONObject.getString("error"));
                    }
                } else if (jSONObject.has("type") && "alert".equals(jSONObject.getString("type"))) {
                    String unused = AdPrompt.this.title = jSONObject.getString("adtitle");
                    if (jSONObject.has(AdActivity.HTML_PARAM)) {
                        String unused2 = AdPrompt.this.html = jSONObject.getString(AdActivity.HTML_PARAM);
                    }
                    String unused3 = AdPrompt.this.callToAction = jSONObject.getString("calltoaction");
                    String unused4 = AdPrompt.this.declineStr = jSONObject.getString("declinestring");
                    String unused5 = AdPrompt.this.clickUrl = jSONObject.getString("clickurl");
                    boolean unused6 = AdPrompt.this.loaded = true;
                    if (AdPrompt.this.listener != null) {
                        AdPrompt.this.listener.adPromptLoaded(this.theAdPrompt);
                    }
                    if (AdPrompt.this.showAfterLoad) {
                        AdPrompt.this.showAdPrompt();
                    }
                } else if (AdPrompt.this.listener != null) {
                    AdPrompt.this.listener.adPromptError(this.theAdPrompt, "Server returned an incompatible ad");
                }
            } catch (JSONException e) {
                if ("".equals(str)) {
                    message = "server returned an empty response";
                } else {
                    message = e.getMessage();
                }
                if (AdPrompt.this.listener != null) {
                    AdPrompt.this.listener.adPromptError(this.theAdPrompt, message);
                }
            }
        }
    }
}
