package com.tapit.adview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import com.tapit.adview.AdViewCore;

public class AdActivity extends Activity {
    public static AdInterstitialBaseView adView;
    public static AdViewCore callingAdView;
    protected WebView webView;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().getStringExtra("com.tapit.adview.ClickURL") != null) {
            setupWebView(bundle);
        } else if (adView != null) {
            setContentView(adView.getInterstitialView(this));
            adView.interstitialShowing();
        } else {
            finish();
        }
    }

    private void setupWebView(Bundle bundle) {
        getWindow().requestFeature(2);
        getWindow().setFeatureInt(2, -1);
        WebView webView2 = new WebView(this);
        webView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        webView2.loadUrl(getIntent().getDataString());
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.getSettings().setSupportZoom(true);
        webView2.getSettings().setBuiltInZoomControls(true);
        webView2.setScrollBarStyle(0);
        webView2.getSettings().setUseWideViewPort(true);
        setContentView(webView2);
        webView2.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                AdViewCore.OnAdDownload onAdDownload;
                if (str.toLowerCase().startsWith("http://") || str.toLowerCase().startsWith("https://")) {
                    webView.loadUrl(str);
                    AdActivity.this.setTitle(str);
                    return false;
                }
                try {
                    if (AdActivity.adView != null) {
                        AdViewCore.OnInterstitialAdDownload onInterstitialAdDownload = AdActivity.adView.getOnInterstitialAdDownload();
                        if (onInterstitialAdDownload != null) {
                            onInterstitialAdDownload.willLeaveApplication(AdActivity.adView);
                        }
                    } else if (!(AdActivity.callingAdView == null || (onAdDownload = AdActivity.callingAdView.getOnAdDownload()) == null)) {
                        onAdDownload.willLeaveApplication(AdActivity.callingAdView);
                    }
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
                    intent.addFlags(335544320);
                    AdActivity.this.startActivityForResult(intent, 3);
                } catch (Exception e) {
                    Log.e("TapIt", "An error occured", e);
                }
                return true;
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                super.onPageStarted(webView, str, bitmap);
            }
        });
        webView2.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView webView, int i) {
                AdActivity.this.setProgress(i * 100);
            }
        });
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        finish();
        return true;
    }

    public void finish() {
        if (adView != null) {
            adView.interstitialClosing();
            adView.removeViews();
            adView = null;
        }
        if (callingAdView != null) {
            callingAdView.willDismissFullScreen();
            callingAdView = null;
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (adView != null) {
            adView.closeInterstitial();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }
}
