package com.tapit.adview.track;

import android.content.Context;
import com.adsdk.sdk.Const;
import com.tapit.adview.AdLog;
import com.tapit.adview.Utils;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class EventTracker {
    /* access modifiers changed from: private */
    public static String TRACK_HANDLER = "/trackevent.php";
    /* access modifiers changed from: private */
    public static String TRACK_HOST = "a.tapit.com";
    private static EventTracker mInstance = null;
    /* access modifiers changed from: private */
    public AdLog adLog = new AdLog(this);
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public String mEvent;
    /* access modifiers changed from: private */
    public String mPackageName;
    private Runnable mTrackEvent = new Runnable() {
        public void run() {
            StringBuilder sb = new StringBuilder("http://" + EventTracker.TRACK_HOST + EventTracker.TRACK_HANDLER);
            sb.append("?pkg=" + EventTracker.this.mPackageName);
            if (EventTracker.this.mEvent == null || EventTracker.this.mEvent == "") {
                EventTracker.this.adLog.log(1, 1, "EventTracker", "Event track failed: No Event Tag Defined");
                return;
            }
            try {
                sb.append("&event=" + URLEncoder.encode(EventTracker.this.mEvent, Const.ENCODING));
            } catch (Exception e) {
            }
            sb.append("&udid=" + Utils.getDeviceIdMD5(EventTracker.this.mContext));
            if (EventTracker.this.ua != null) {
                try {
                    sb.append("&ua=" + URLEncoder.encode(EventTracker.this.ua, Const.ENCODING));
                } catch (Exception e2) {
                }
            }
            String sb2 = sb.toString();
            EventTracker.this.adLog.log(3, 3, "EventTracker", "Event track: " + sb2);
            try {
                if (new DefaultHttpClient().execute(new HttpGet(sb2)).getStatusLine().getStatusCode() != 200) {
                    EventTracker.this.adLog.log(1, 1, "EventTracker", "Event track failed: Status code != 200");
                } else {
                    EventTracker.this.adLog.log(2, 3, "EventTracker", "Event track successful");
                }
            } catch (ClientProtocolException e3) {
                EventTracker.this.adLog.log(1, 1, "EventTracker", "Event track failed: ClientProtocolException (no signal?)");
            } catch (IOException e4) {
                EventTracker.this.adLog.log(1, 1, "EventTracker", "Event track failed: IOException (no signal?)");
            }
        }
    };
    /* access modifiers changed from: private */
    public String ua = null;

    private EventTracker() {
    }

    public static EventTracker getInstance() {
        if (mInstance == null) {
            mInstance = new EventTracker();
        }
        return mInstance;
    }

    public void reportEvent(Context context, String str) {
        if (context != null) {
            this.mContext = context;
            this.mEvent = str;
            this.mPackageName = this.mContext.getPackageName();
            if (this.ua == null) {
                this.ua = Utils.getUserAgentString(this.mContext);
            }
            new Thread(this.mTrackEvent).start();
        }
    }

    public void setLogLevel(int i) {
        this.adLog.setLogLevel(i);
    }
}
