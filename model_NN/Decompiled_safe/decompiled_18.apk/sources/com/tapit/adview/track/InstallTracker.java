package com.tapit.adview.track;

import android.content.Context;
import com.adsdk.sdk.Const;
import com.tapit.adview.AdLog;
import com.tapit.adview.Utils;
import java.io.IOException;
import java.net.URLEncoder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class InstallTracker {
    /* access modifiers changed from: private */
    public static String TRACK_HANDLER = "/adconvert.php";
    /* access modifiers changed from: private */
    public static String TRACK_HOST = "a.tapit.com";
    private static InstallTracker mInstance = null;
    /* access modifiers changed from: private */
    public AdLog adLog = new AdLog(this);
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public String mOfferId;
    /* access modifiers changed from: private */
    public String mPackageName;
    private Runnable mTrackInstall = new Runnable() {
        public void run() {
            StringBuilder sb = new StringBuilder("http://" + InstallTracker.TRACK_HOST + InstallTracker.TRACK_HANDLER);
            sb.append("?pkg=" + InstallTracker.this.mPackageName);
            if (InstallTracker.this.mOfferId != null) {
                try {
                    sb.append("&offer=" + URLEncoder.encode(InstallTracker.this.mOfferId, Const.ENCODING));
                } catch (Exception e) {
                }
            }
            sb.append("&udid=" + Utils.getDeviceIdMD5(InstallTracker.this.mContext));
            if (InstallTracker.this.ua != null) {
                try {
                    sb.append("&ua=" + URLEncoder.encode(InstallTracker.this.ua, Const.ENCODING));
                } catch (Exception e2) {
                }
            }
            String sb2 = sb.toString();
            InstallTracker.this.adLog.log(3, 3, "InstallTracker", "Install track: " + sb2);
            try {
                HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(sb2));
                if (execute.getStatusLine().getStatusCode() != 200) {
                    InstallTracker.this.adLog.log(1, 1, "InstallTracker", "Install track failed: Status code != 200");
                    return;
                }
                HttpEntity entity = execute.getEntity();
                if (entity == null || entity.getContentLength() == 0) {
                    InstallTracker.this.adLog.log(1, 1, "InstallTracker", "Install track failed: Response was empty");
                    return;
                }
                InstallTracker.this.adLog.log(3, 3, "InstallTracker", "Install track successful");
                InstallTracker.this.mContext.getSharedPreferences("tapitSettings", 0).edit().putBoolean(InstallTracker.this.mPackageName + " installed", true).commit();
            } catch (ClientProtocolException e3) {
                InstallTracker.this.adLog.log(1, 1, "InstallTracker", "Install track failed: ClientProtocolException (no signal?)");
            } catch (IOException e4) {
                InstallTracker.this.adLog.log(1, 1, "InstallTracker", "Install track failed: IOException (no signal?)");
            }
        }
    };
    /* access modifiers changed from: private */
    public String ua = null;

    private InstallTracker() {
    }

    public static InstallTracker getInstance() {
        if (mInstance == null) {
            mInstance = new InstallTracker();
        }
        return mInstance;
    }

    public void reportInstall(Context context) {
        reportInstall(context, null);
    }

    public void reportInstall(Context context, String str) {
        if (context != null) {
            this.mContext = context;
            this.mOfferId = str;
            this.mPackageName = this.mContext.getPackageName();
            if (!this.mContext.getSharedPreferences("tapitSettings", 0).getBoolean(this.mPackageName + " installed", false)) {
                this.ua = Utils.getUserAgentString(this.mContext);
                new Thread(this.mTrackInstall).start();
                return;
            }
            this.adLog.log(3, 3, "InstallTracker", "Install already tracked");
        }
    }

    public void setLogLevel(int i) {
        this.adLog.setLogLevel(i);
    }
}
