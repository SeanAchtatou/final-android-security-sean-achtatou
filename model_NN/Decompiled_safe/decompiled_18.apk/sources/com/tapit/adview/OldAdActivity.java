package com.tapit.adview;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

public class OldAdActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().requestFeature(2);
        getWindow().setFeatureInt(2, -1);
        WebView webView = new WebView(this);
        webView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        webView.loadUrl(getIntent().getDataString());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.setScrollBarStyle(0);
        webView.getSettings().setUseWideViewPort(true);
        setContentView(webView);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                if (str.toLowerCase().startsWith("http://") || str.toLowerCase().startsWith("https://")) {
                    webView.loadUrl(str);
                    OldAdActivity.this.setTitle(str);
                    return false;
                }
                OldAdActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                OldAdActivity.this.finish();
                return true;
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                super.onPageStarted(webView, str, bitmap);
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView webView, int i) {
                OldAdActivity.this.setProgress(i * 100);
            }
        });
    }
}
