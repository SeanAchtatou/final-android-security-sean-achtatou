package com.tapit.adview;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdOfferWallView extends AdInterstitialBaseView {
    private static final String bannerItem = "\t\t\t<li>\t\t\t\t<a href=\"%s\">\t\t\t\t\t<div class=\"offer\">\t\t\t\t\t<div class=\"icon\"><img src=\"%s\" width=\"57\" height=\"57\"/></div>\t\t\t\t\t\t<div class=\"desc\">\t\t\t\t\t\t\t<h3>%s</h3>\t\t\t\t\t\t\t<h4>%s</h4>\t\t\t\t\t\t</div>\t\t\t\t\t<div class=\"button\"><img src=\"http://d2bgg7rjywcwsy.cloudfront.net/offerwall/img/arrow.png\" width=\"57\" height=\"57\"/></div>\t\t\t\t\t</div>\t\t\t\t</a>\t\t\t</li>";
    private static final String blank = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><meta name=\"HandheldFriendly\" content=\"true\" /><meta name=\"MobileOptimized\" content=\"320\" /><meta name=\"viewport\" content=\"width=device-width\" /><meta name=\"viewport\" content=\"initial-scale=1.0\" /><meta name=\"viewport\" content=\"user-scalable=no\" /><title>TapIt!</title><link type=\"text/css\" rel=\"stylesheet\" href=\"http://d2bgg7rjywcwsy.cloudfront.net/offerwall/css/style.css\"/></head><body><div id=\"page\"><div id=\"action-wrapper\">\t<div id=\"action\">\t\t<h2>%s</h2>\t</div></div> <!-- end action-wrapper --><div id=\"list-wrapper\">\t<div id=\"list\">\t\t<ul>%s\t\t</ul>\t</div></div> <!-- end list-wrapper --></div> <!-- end page --></body></html>";
    private Button closeButton;

    public AdOfferWallView(Context context, String str) {
        super(context, str);
        setAdtype("7");
    }

    /* access modifiers changed from: protected */
    public String wrapToHTML(String str, String str2, String str3) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has("type") || !jSONObject.getString("type").equals("offerwall")) {
                return blank;
            }
            String string = jSONObject.getString("title");
            String str4 = "";
            JSONArray jSONArray = jSONObject.getJSONArray("offers");
            for (int i = 0; i < jSONArray.length(); i++) {
                JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                str4 = str4 + String.format(bannerItem, jSONObject2.getString("clickurl"), jSONObject2.getString("imageurl"), jSONObject2.getString("adtitle"), jSONObject2.getString("adtext"));
            }
            return String.format(blank, string, str4);
        } catch (JSONException e) {
            Log.e("TapIt", "An error occured while processing JSON", e);
            return blank;
        }
    }

    public View getInterstitialView(Context context) {
        this.callingActivityContext = context;
        this.interstitialLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        this.interstitialLayout.addView(this, layoutParams);
        initCloseButton();
        return this.interstitialLayout;
    }

    private void initCloseButton() {
        this.closeButton = new Button(this.context);
        this.closeButton.setText("Close");
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12, -1);
        layoutParams.addRule(14, -1);
        this.closeButton.setLayoutParams(layoutParams);
        this.interstitialLayout.addView(this.closeButton);
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdOfferWallView.this.closeInterstitial();
            }
        });
    }

    public void loadUrl(String str) {
        super.loadUrl(str);
        this.closeButton.setVisibility(8);
    }
}
