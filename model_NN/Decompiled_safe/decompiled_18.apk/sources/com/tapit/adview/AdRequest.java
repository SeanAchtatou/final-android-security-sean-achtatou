package com.tapit.adview;

import android.content.Context;
import android.util.Log;
import com.adsdk.sdk.Const;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Map;

public class AdRequest {
    private static final String PARAMETER_ADTYPE = "adtype";
    private static final String PARAMETER_BACKGROUND = "paramBG";
    private static final String PARAMETER_CARRIER = "carrier";
    private static final String PARAMETER_CONNECTION_SPEED = "connection_speed";
    public static final String PARAMETER_DEVICE_ID = "udid";
    private static final String PARAMETER_HEIGHT = "h";
    private static final String PARAMETER_LANGUAGES = "languages";
    private static final String PARAMETER_LATITUDE = "lat";
    private static final String PARAMETER_LINK = "paramLINK";
    private static final String PARAMETER_LONGITUDE = "long";
    private static final String PARAMETER_MIN_SIZE_X = "min_size_x";
    private static final String PARAMETER_MIN_SIZE_Y = "min_size_y";
    private static final String PARAMETER_SIZE_X = "size_x";
    private static final String PARAMETER_SIZE_Y = "size_y";
    private static final String PARAMETER_USER_AGENT = "ua";
    private static final String PARAMETER_WIDTH = "w";
    private static final String PARAMETER_ZONE = "zone";
    public static final String TAG = "AdRequest";
    private AdLog adLog;
    private String adserverURL = "http://r.tapit.com/adrequest.php";
    private Map<String, String> customParameters = Collections.synchronizedMap(new HashMap());
    private Map<String, String> parameters = Collections.synchronizedMap(new HashMap());

    public AdRequest(AdLog adLog2) {
        this.adLog = adLog2;
    }

    public AdRequest(String str) {
        setZone(str);
        this.adLog = new AdLog(this);
    }

    public void initDefaultParameters(Context context) {
        String deviceIdMD5 = Utils.getDeviceIdMD5(context);
        String carrier = Utils.getCarrier(context);
        String userAgentString = Utils.getUserAgentString(context);
        this.adLog.log(2, 3, "deviceIdMD5", deviceIdMD5);
        if (deviceIdMD5 != null && deviceIdMD5.length() > 0) {
            this.parameters.put(PARAMETER_DEVICE_ID, deviceIdMD5);
        }
        this.parameters.put("format", "json");
        this.parameters.put("sdk", "android-v1.7.5");
        this.parameters.put(PARAMETER_CARRIER, carrier);
        this.parameters.put(PARAMETER_LANGUAGES, Locale.getDefault().getLanguage());
        this.parameters.put(PARAMETER_USER_AGENT, userAgentString);
    }

    public synchronized String getAdserverURL() {
        return this.adserverURL;
    }

    public synchronized void setAdserverURL(String str) {
        if (str != null) {
            if (str.length() > 0) {
                this.adserverURL = str;
            }
        }
    }

    public AdRequest setUa(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_USER_AGENT, str);
            }
        }
        return this;
    }

    public AdRequest setZone(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_ZONE, str);
            }
        }
        return this;
    }

    public AdRequest setAdtype(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_ADTYPE, str);
            }
        }
        return this;
    }

    public AdRequest setLatitude(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_LATITUDE, str);
            }
        }
        return this;
    }

    public AdRequest setLongitude(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_LONGITUDE, str);
            }
        }
        return this;
    }

    public AdRequest setParamBG(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_BACKGROUND, str);
            }
        }
        return this;
    }

    public AdRequest setParamLINK(String str) {
        if (str != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_LINK, str);
            }
        }
        return this;
    }

    public AdRequest setMinSizeX(Integer num) {
        if (num != null && num.intValue() > 0) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_MIN_SIZE_X, String.valueOf(num));
            }
        }
        return this;
    }

    public AdRequest setMinSizeY(Integer num) {
        if (num != null && num.intValue() > 0) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_MIN_SIZE_Y, String.valueOf(num));
            }
        }
        return this;
    }

    public AdRequest setSizeX(Integer num) {
        if (num != null && num.intValue() > 0) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_SIZE_X, String.valueOf(num));
            }
        }
        return this;
    }

    public AdRequest setSizeY(Integer num) {
        if (num != null && num.intValue() > 0) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_SIZE_Y, String.valueOf(num));
            }
        }
        return this;
    }

    public AdRequest setHeight(Integer num) {
        if (num != null && num.intValue() > 0) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_HEIGHT, String.valueOf(num));
            }
        }
        return this;
    }

    public Integer getHeight() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_HEIGHT));
        }
        return intParameter;
    }

    public AdRequest setWidth(Integer num) {
        if (num != null && num.intValue() > 0) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_WIDTH, String.valueOf(num));
            }
        }
        return this;
    }

    public Integer getWidth() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_WIDTH));
        }
        return intParameter;
    }

    public AdRequest setConnectionSpeed(Integer num) {
        if (num != null) {
            synchronized (this.parameters) {
                this.parameters.put(PARAMETER_CONNECTION_SPEED, String.valueOf(num));
            }
        }
        return this;
    }

    public String getAdtype() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_ADTYPE);
        }
        return str;
    }

    public String getUa() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_USER_AGENT);
        }
        return str;
    }

    public String getZone() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_ZONE);
        }
        return str;
    }

    public String getLatitude() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_LATITUDE);
        }
        return str;
    }

    public String getLongitude() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_LONGITUDE);
        }
        return str;
    }

    public String getParamBG() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_BACKGROUND);
        }
        return str;
    }

    public String getParamLINK() {
        String str;
        synchronized (this.parameters) {
            str = this.parameters.get(PARAMETER_LINK);
        }
        return str;
    }

    public Integer getMinSizeX() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_MIN_SIZE_X));
        }
        return intParameter;
    }

    public Integer getMinSizeY() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_MIN_SIZE_Y));
        }
        return intParameter;
    }

    public Integer getSizeX() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_SIZE_X));
        }
        return intParameter;
    }

    public Integer getSizeY() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_SIZE_Y));
        }
        return intParameter;
    }

    public Integer getConnectionSpeed() {
        Integer intParameter;
        synchronized (this.parameters) {
            intParameter = getIntParameter(this.parameters.get(PARAMETER_CONNECTION_SPEED));
        }
        return intParameter;
    }

    public void setCustomParameters(Hashtable<String, String> hashtable) {
        for (String next : hashtable.keySet()) {
            this.customParameters.put(next, hashtable.get(next));
        }
    }

    public void setCustomParameters(Map<String, String> map) {
        if (map != null) {
            this.customParameters.putAll(map);
        } else {
            this.customParameters.clear();
        }
    }

    public Map<String, String> getCustomParameters() {
        return this.customParameters;
    }

    private Integer getIntParameter(String str) {
        if (str != null) {
            return Integer.valueOf(Integer.parseInt(str));
        }
        return null;
    }

    public synchronized String createURL() throws IllegalStateException {
        return toString();
    }

    public synchronized String toString() {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append(this.adserverURL + "?");
        appendParameters(sb, this.parameters);
        appendParameters(sb, this.customParameters);
        return sb.toString();
    }

    private void appendParameters(StringBuilder sb, Map<String, String> map) {
        if (map != null) {
            synchronized (map) {
                for (String next : map.keySet()) {
                    String str = map.get(next);
                    if (str != null) {
                        try {
                            sb.append("&" + URLEncoder.encode(next, Const.ENCODING) + "=" + URLEncoder.encode(str, Const.ENCODING));
                        } catch (UnsupportedEncodingException e) {
                            Log.e("TapIt", "An error occured", e);
                        }
                    }
                }
            }
        }
    }
}
