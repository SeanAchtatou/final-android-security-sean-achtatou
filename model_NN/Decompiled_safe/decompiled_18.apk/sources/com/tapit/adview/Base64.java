package com.tapit.adview;

public class Base64 {
    private static char[] map1 = new char[64];
    private static byte[] map2 = new byte[128];

    static {
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            map1[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            map1[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            map1[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i2 = i + 1;
        map1[i] = '+';
        int i3 = i2 + 1;
        map1[i2] = '/';
        for (int i4 = 0; i4 < map2.length; i4++) {
            map2[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            map2[map1[i5]] = (byte) i5;
        }
    }

    public static String encodeString(String str) {
        return new String(encode(str.getBytes()));
    }

    public static char[] encode(byte[] bArr) {
        return encode(bArr, bArr.length);
    }

    public static char[] encode(byte[] bArr, int i) {
        byte b;
        byte b2;
        char c;
        char c2;
        int i2 = ((i * 4) + 2) / 3;
        char[] cArr = new char[(((i + 2) / 3) * 4)];
        int i3 = 0;
        int i4 = 0;
        while (i4 < i) {
            int i5 = i4 + 1;
            byte b3 = bArr[i4] & 255;
            if (i5 < i) {
                b = bArr[i5] & 255;
                i5++;
            } else {
                b = 0;
            }
            if (i5 < i) {
                i4 = i5 + 1;
                b2 = bArr[i5] & 255;
            } else {
                i4 = i5;
                b2 = 0;
            }
            int i6 = b3 >>> 2;
            int i7 = ((b3 & 3) << 4) | (b >>> 4);
            int i8 = ((b & 15) << 2) | (b2 >>> 6);
            byte b4 = b2 & 63;
            int i9 = i3 + 1;
            cArr[i3] = map1[i6];
            int i10 = i9 + 1;
            cArr[i9] = map1[i7];
            if (i10 < i2) {
                c = map1[i8];
            } else {
                c = '=';
            }
            cArr[i10] = c;
            int i11 = i10 + 1;
            if (i11 < i2) {
                c2 = map1[b4];
            } else {
                c2 = '=';
            }
            cArr[i11] = c2;
            i3 = i11 + 1;
        }
        return cArr;
    }

    public static String decodeString(String str) {
        return new String(decode(str));
    }

    public static byte[] decode(String str) {
        return decode(str.toCharArray());
    }

    public static byte[] decode(char[] cArr) {
        char c;
        int i;
        char c2;
        int i2;
        int i3;
        int i4 = 0;
        int length = cArr.length;
        if (length % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        int i5 = length;
        while (i5 > 0 && cArr[i5 - 1] == '=') {
            i5--;
        }
        int i6 = (i5 * 3) / 4;
        byte[] bArr = new byte[i6];
        int i7 = 0;
        while (i4 < i5) {
            int i8 = i4 + 1;
            char c3 = cArr[i4];
            int i9 = i8 + 1;
            char c4 = cArr[i8];
            if (i9 < i5) {
                c = cArr[i9];
                i9++;
            } else {
                c = 'A';
            }
            if (i9 < i5) {
                int i10 = i9 + 1;
                c2 = cArr[i9];
                i = i10;
            } else {
                i = i9;
                c2 = 'A';
            }
            if (c3 > 127 || c4 > 127 || c > 127 || c2 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b = map2[c3];
            byte b2 = map2[c4];
            byte b3 = map2[c];
            byte b4 = map2[c2];
            if (b < 0 || b2 < 0 || b3 < 0 || b4 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data. " + ((int) c3));
            }
            int i11 = ((b2 & 15) << 4) | (b3 >>> 2);
            byte b5 = ((b3 & 3) << 6) | b4;
            int i12 = i7 + 1;
            bArr[i7] = (byte) ((b << 2) | (b2 >>> 4));
            if (i12 < i6) {
                i2 = i12 + 1;
                bArr[i12] = (byte) i11;
            } else {
                i2 = i12;
            }
            if (i2 < i6) {
                i3 = i2 + 1;
                bArr[i2] = (byte) b5;
            } else {
                i3 = i2;
            }
            i7 = i3;
            i4 = i;
        }
        return bArr;
    }

    private Base64() {
    }
}
