package com.tapit.adview;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.tapit.adview.AdViewCore;
import java.io.IOException;

public class AdVideoUnitView extends AdInterstitialBaseView {
    /* access modifiers changed from: private */
    public String clickUrl;
    /* access modifiers changed from: private */
    public Button closeButton;
    private String closeButtonText;
    private String goToSiteButtonText;
    /* access modifiers changed from: private */
    public Button goToWebButton;
    /* access modifiers changed from: private */
    public Handler handler = new Handler();
    /* access modifiers changed from: private */
    public MediaPlayer player;
    private SurfaceHolder playerSurfaceHolder;
    private SurfaceView playerSurfaceView;
    /* access modifiers changed from: private */
    public ProgressBar progressBar;
    /* access modifiers changed from: private */
    public Runnable progressRunnable = new Runnable() {
        public void run() {
            try {
                if (AdVideoUnitView.this.player.isPlaying()) {
                    int currentPosition = AdVideoUnitView.this.player.getCurrentPosition();
                    int duration = AdVideoUnitView.this.player.getDuration();
                    if (AdVideoUnitView.this.progressBar.getMax() != duration) {
                        AdVideoUnitView.this.progressBar.setMax(duration);
                    }
                    AdVideoUnitView.this.progressBar.setProgress(currentPosition);
                    if (currentPosition < duration) {
                        AdVideoUnitView.this.handler.postDelayed(AdVideoUnitView.this.progressRunnable, 50);
                    }
                }
            } catch (Exception e) {
                Log.e("TapIt", "An error occured", e);
            }
        }
    };
    private Runnable showButtonsRunnable = new Runnable() {
        public void run() {
            AdVideoUnitView.this.closeButton.setVisibility(0);
            AdVideoUnitView.this.goToWebButton.setVisibility(0);
        }
    };

    public AdVideoUnitView(Context context, String str) {
        super(context, str);
        setAdtype("6");
        this.playerSurfaceView = new SurfaceView(context);
        this.playerSurfaceHolder = this.playerSurfaceView.getHolder();
        this.playerSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
                AdVideoUnitView.this.player.start();
            }

            public void surfaceCreated(SurfaceHolder surfaceHolder) {
            }

            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
            }
        });
        Display defaultDisplay = ((Activity) context).getWindowManager().getDefaultDisplay();
        int height = defaultDisplay.getHeight();
        this.playerSurfaceHolder.setFixedSize(defaultDisplay.getWidth(), height);
        this.player = new MediaPlayer();
        this.player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mediaPlayer) {
                AdVideoUnitView.this.isLoaded = true;
                if (AdVideoUnitView.this.interstitialListener != null) {
                    AdVideoUnitView.this.interstitialListener.ready(this);
                }
            }
        });
        this.player.setOnInfoListener(new MediaPlayer.OnInfoListener() {
            public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                return false;
            }
        });
        this.player.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                return false;
            }
        });
    }

    public void end(AdViewCore adViewCore) {
    }

    public void showInterstitial() {
        super.showInterstitial();
    }

    public void playVideo(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, AdViewCore.Dimensions dimensions, String str3, String str4) {
        this.clickUrl = str2;
        try {
            this.player.setAudioStreamType(3);
            this.player.setDataSource(str);
            this.player.prepareAsync();
        } catch (IllegalArgumentException e) {
            Log.e("TapIt", "An error occured", e);
        } catch (IllegalStateException e2) {
            Log.e("TapIt", "An error occured", e2);
        } catch (IOException e3) {
            Log.e("TapIt", "An error occured", e3);
        } catch (Exception e4) {
            Log.e("TapIt", "An error occured", e4);
        }
    }

    public void interstitialShowing() {
        this.player.start();
        this.handler.post(this.progressRunnable);
    }

    public void interstitialClosing() {
        this.player.stop();
        this.player.release();
    }

    /* access modifiers changed from: protected */
    public void interstitialClose() {
        this.handler.removeCallbacks(this.progressRunnable);
    }

    /* access modifiers changed from: private */
    public void showButtons() {
        this.handler.post(this.showButtonsRunnable);
    }

    public void setCloseButtonText(String str) {
        this.closeButtonText = str;
    }

    public void setGoToSiteButtonText(String str) {
        this.goToSiteButtonText = str;
    }

    public View getInterstitialView(Context context) {
        removeViews();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.playerSurfaceView.setLayoutParams(layoutParams);
        this.player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mediaPlayer) {
                AdVideoUnitView.this.showButtons();
            }
        });
        this.progressBar = new ProgressBar(this.context, null, 16842872);
        this.progressBar.setId(10505);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(12);
        this.progressBar.setLayoutParams(layoutParams2);
        LinearLayout linearLayout = new LinearLayout(this.context);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(2, 10505);
        linearLayout.setLayoutParams(layoutParams3);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2, 1.0f);
        this.closeButton = new Button(this.context);
        this.closeButton.setLayoutParams(layoutParams4);
        this.closeButton.setText(this.closeButtonText != null ? this.closeButtonText : "Close");
        linearLayout.addView(this.closeButton);
        this.closeButton.setVisibility(8);
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdVideoUnitView.this.closeInterstitial();
            }
        });
        this.goToWebButton = new Button(this.context);
        this.goToWebButton.setLayoutParams(layoutParams4);
        this.goToWebButton.setText(this.goToSiteButtonText != null ? this.goToSiteButtonText : "Go To Site");
        linearLayout.addView(this.goToWebButton);
        this.goToWebButton.setVisibility(8);
        this.goToWebButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdVideoUnitView.this.open(AdVideoUnitView.this.clickUrl, false, false, false);
                AdVideoUnitView.this.interstitialClose();
            }
        });
        this.interstitialLayout = new RelativeLayout(context);
        this.interstitialLayout.addView(this.playerSurfaceView);
        this.interstitialLayout.addView(this.progressBar);
        this.interstitialLayout.addView(linearLayout);
        return this.interstitialLayout;
    }

    public void destroy() {
        this.player.release();
    }
}
