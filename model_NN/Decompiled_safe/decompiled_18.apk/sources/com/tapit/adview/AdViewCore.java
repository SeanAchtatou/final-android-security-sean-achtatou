package com.tapit.adview;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.adfonic.android.utils.HtmlFormatter;
import com.adsdk.sdk.Const;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public abstract class AdViewCore extends WebView {
    public static final String ACTION_KEY = "action";
    private static final long AD_DEFAULT_RELOAD_PERIOD = 120000;
    protected static final int BACKGROUND_ID = 101;
    public static final String DIMENSIONS = "expand_dimensions";
    public static final String EXPAND_URL = "expand_url";
    protected static final int PLACEHOLDER_ID = 100;
    public static final String PLAYER_PROPERTIES = "player_properties";
    public static final String TAG = "AdViewCore";
    private static final String TYPE_BANNER = "banner";
    private static final String TYPE_OFFERWALL = "offerwall";
    private static final String TYPE_ORMMA = "ormma";
    private static final String TYPE_VIDEO = "video";
    public static final String VERSION = "1.7.5";
    /* access modifiers changed from: private */
    public static String mBridgeScriptPath = null;
    /* access modifiers changed from: private */
    public static String mScriptPath = null;
    private static int requestCounter = 0;
    /* access modifiers changed from: private */
    public OnAdClickListener adClickListener;
    /* access modifiers changed from: private */
    public OnAdDownload adDownload;
    /* access modifiers changed from: private */
    public int adHeight;
    protected AdLog adLog = new AdLog(this);
    private long adReloadPeriod = AD_DEFAULT_RELOAD_PERIOD;
    protected AdRequest adRequest;
    /* access modifiers changed from: private */
    public int adWidth;
    /* access modifiers changed from: private */
    public boolean animateBack = false;
    private boolean bGotLayoutParams;
    private Integer backgroundColor = 0;
    /* access modifiers changed from: private */
    public LoadContentTask contentTask;
    protected Context context;
    /* access modifiers changed from: private */
    public Integer defaultImageResource;
    Handler handler = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public boolean isBannerAnimationEnabled = true;
    /* access modifiers changed from: private */
    public boolean isFirstTime = true;
    private TimerTask lastTimerTask = null;
    public Object lock = new Object();
    /* access modifiers changed from: private */
    public String mClickURL;
    /* access modifiers changed from: private */
    public String mContent;
    public String mDataToInject = null;
    private float mDensity;
    private int mIndex;
    private int mInitLayoutHeight;
    private int mInitLayoutWidth;
    /* access modifiers changed from: private */
    public ViewState mViewState = ViewState.DEFAULT;
    WebChromeClient mWebChromeClient = new WebChromeClient() {
        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            jsResult.confirm();
            return true;
        }
    };
    private boolean openInInternalBrowser = true;
    private Timer reloadTimer;
    /* access modifiers changed from: private */
    public String typeOfBanner = TYPE_BANNER;

    public enum ACTION {
        PLAY_AUDIO,
        PLAY_VIDEO
    }

    public interface OnAdClickListener {
        void click(String str);
    }

    public interface OnAdDownload {
        void begin(AdViewCore adViewCore);

        void clicked(AdViewCore adViewCore);

        void didPresentFullScreen(AdViewCore adViewCore);

        void end(AdViewCore adViewCore);

        void error(AdViewCore adViewCore, String str);

        void willDismissFullScreen(AdViewCore adViewCore);

        void willLeaveApplication(AdViewCore adViewCore);

        void willPresentFullScreen(AdViewCore adViewCore);
    }

    public interface OnInterstitialAdDownload {
        void clicked(AdViewCore adViewCore);

        void didClose(AdViewCore adViewCore);

        void error(AdViewCore adViewCore, String str);

        void ready(AdViewCore adViewCore);

        void willLeaveApplication(AdViewCore adViewCore);

        void willLoad(AdViewCore adViewCore);

        void willOpen(AdViewCore adViewCore);
    }

    private enum ViewState {
        DEFAULT,
        RESIZED,
        EXPANDED,
        HIDDEN
    }

    public AdViewCore(Context context2, String str) {
        super(context2);
        loadContent(context2, null, null, null, null, null, str, null, null, null, null, null, null, null);
    }

    public AdViewCore(Context context2, AttributeSet attributeSet, int i) {
        super(context2, attributeSet, i);
        initialize(context2, attributeSet);
    }

    public AdViewCore(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        initialize(context2, attributeSet);
    }

    public AdViewCore(Context context2) {
        super(context2);
        initialize(context2, null);
    }

    public OnAdClickListener getOnAdClickListener() {
        return this.adClickListener;
    }

    public void setOnAdClickListener(OnAdClickListener onAdClickListener) {
        this.adClickListener = onAdClickListener;
    }

    public OnAdDownload getOnAdDownload() {
        return this.adDownload;
    }

    public void setOnAdDownload(OnAdDownload onAdDownload) {
        this.adDownload = onAdDownload;
    }

    public Map<String, String> getCustomParameters() {
        if (this.adRequest != null) {
            return this.adRequest.getCustomParameters();
        }
        return null;
    }

    public void setCustomParameters(Hashtable<String, String> hashtable) {
        if (this.adRequest != null) {
            this.adRequest.setCustomParameters(hashtable);
        }
    }

    public void setCustomParameters(Map<String, String> map) {
        if (this.adRequest != null) {
            this.adRequest.setCustomParameters(map);
        }
    }

    public Integer getDefaultImage() {
        return this.defaultImageResource;
    }

    public void setDefaultImage(Integer num) {
        this.defaultImageResource = num;
    }

    public int getUpdateTime() {
        return (int) (this.adReloadPeriod / 1000);
    }

    public void setUpdateTime(int i) {
        boolean z = this.adReloadPeriod == 0;
        this.adReloadPeriod = (long) (i * 1000);
        if (z) {
            scheduleUpdate();
        }
    }

    private void initialize(Context context2, AttributeSet attributeSet) {
        if (attributeSet != null) {
            String attributeValue = attributeSet.getAttributeValue(null, "zone");
            String attributeValue2 = attributeSet.getAttributeValue(null, "keywords");
            String attributeValue3 = attributeSet.getAttributeValue(null, "latitude");
            String attributeValue4 = attributeSet.getAttributeValue(null, "longitude");
            String attributeValue5 = attributeSet.getAttributeValue(null, "ua");
            String attributeValue6 = attributeSet.getAttributeValue(null, "paramBG");
            String attributeValue7 = attributeSet.getAttributeValue(null, "paramLINK");
            Integer valueOf = Integer.valueOf(attributeSet.getAttributeResourceValue(null, "defaultImage", -1));
            Long longParameter = getLongParameter(attributeSet.getAttributeValue(null, "adReloadPeriod"));
            if (longParameter != null) {
                this.adReloadPeriod = longParameter.longValue();
            }
            loadContent(context2, getIntParameter(attributeSet.getAttributeValue(null, "minSizeX")), getIntParameter(attributeSet.getAttributeValue(null, "minSizeY")), getIntParameter(attributeSet.getAttributeValue(null, "sizeX")), getIntParameter(attributeSet.getAttributeValue(null, "sizeY")), valueOf, attributeValue, attributeValue2, attributeValue3, attributeValue4, attributeValue5, attributeValue6, attributeValue7, null);
        }
    }

    private void loadContent(Context context2, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, String str, String str2, String str3, String str4, String str5, String str6, String str7, Map<String, String> map) {
        this.context = context2;
        this.adRequest = new AdRequest(this.adLog);
        this.adRequest.initDefaultParameters(context2);
        this.adRequest.setUa(str5).setZone(str).setLatitude(str3).setLongitude(str4).setParamBG(str6).setParamLINK(str7).setCustomParameters(map);
        this.defaultImageResource = num5;
        WebSettings settings = getSettings();
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(false);
        settings.setCacheMode(2);
        setScrollContainer(false);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        this.mDensity = displayMetrics.density;
        setScriptPath();
        setWebViewClient(new AdWebViewClient(context2));
        setWebChromeClient(this.mWebChromeClient);
    }

    public String getSize() {
        return "{ width: " + ((int) (((float) getWidth()) / this.mDensity)) + ", " + "height: " + ((int) (((float) getHeight()) / this.mDensity)) + "}";
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (!this.bGotLayoutParams && layoutParams != null) {
            this.mInitLayoutHeight = layoutParams.height;
            this.mInitLayoutWidth = layoutParams.width;
            this.bGotLayoutParams = true;
        }
        if (this.isFirstTime) {
            this.contentTask = new LoadContentTask(this, false);
            this.adLog.log(3, 3, "onAttachedToWindow", "isFirstTime is true. Called LoadContentTask");
            this.contentTask.execute(0);
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    public void destroy() {
        cancelUpdating();
        super.destroy();
    }

    /* access modifiers changed from: protected */
    public void cancelUpdating() {
        this.adLog.log(3, 3, "cancelUpdating", "cancelUpdating called");
        loadDataWithBaseURL("");
        removeAllViews();
        if (this.reloadTimer != null) {
            try {
                this.reloadTimer.cancel();
                this.reloadTimer = null;
            } catch (Exception e) {
                this.adLog.log(1, 1, "cancelUpdating", e.getMessage());
            }
        }
        if (this.contentTask != null) {
            try {
                this.contentTask.cancel(true);
            } catch (Exception e2) {
                this.adLog.log(1, 1, "cancelUpdating", e2.getMessage());
            }
        }
        setUpdateTime(0);
    }

    public void update(boolean z) {
        this.contentTask = new LoadContentTask(this, z);
        this.contentTask.execute(0);
    }

    private class LoadContentTask extends AsyncTask<Integer, Integer, String> {
        private static final int BEGIN_STATE = 0;
        private static final int END_STATE = 1;
        private static final int ERROR_STATE = 2;
        private String error;
        private boolean forced;
        /* access modifiers changed from: private */
        public WebView view;

        public LoadContentTask(WebView webView, boolean z) {
            this.forced = z;
            this.view = webView;
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:123:0x02a9 A[SYNTHETIC, Splitter:B:123:0x02a9] */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00a6 A[Catch:{ Exception -> 0x02cf }] */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x00bc A[SYNTHETIC, Splitter:B:40:0x00bc] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.Integer... r14) {
            /*
                r13 = this;
                r10 = 0
                r0 = 0
                r1 = 1
                com.tapit.adview.AdViewCore r2 = com.tapit.adview.AdViewCore.this
                r2.setScrollBarStyle(r0)
                com.tapit.adview.AdViewCore r2 = com.tapit.adview.AdViewCore.this
                java.lang.Object r11 = r2.lock
                monitor-enter(r11)
                com.tapit.adview.AdViewCore r2 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdLog r2 = r2.adLog     // Catch:{ all -> 0x015b }
                r3 = 3
                r4 = 3
                java.lang.String r5 = "AdViewCore"
                java.lang.String r6 = "LoadContentTask started"
                r2.log(r3, r4, r5, r6)     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdViewCore r2 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                boolean r4 = r2.isFirstTime     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdViewCore r2 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                boolean r2 = r2.isShown()     // Catch:{ all -> 0x015b }
                if (r2 != 0) goto L_0x002c
                boolean r2 = r13.forced     // Catch:{ all -> 0x015b }
                if (r2 == 0) goto L_0x002d
            L_0x002c:
                r0 = r1
            L_0x002d:
                if (r0 == 0) goto L_0x003f
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdViewCore$ViewState r0 = r0.mViewState     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdViewCore$ViewState r1 = com.tapit.adview.AdViewCore.ViewState.DEFAULT     // Catch:{ all -> 0x015b }
                if (r0 != r1) goto L_0x003f
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdRequest r0 = r0.adRequest     // Catch:{ all -> 0x015b }
                if (r0 != 0) goto L_0x0046
            L_0x003f:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                r0.scheduleUpdate()     // Catch:{ all -> 0x015b }
                monitor-exit(r11)     // Catch:{ all -> 0x015b }
            L_0x0045:
                return r10
            L_0x0046:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                r1 = 0
                boolean unused = r0.isFirstTime = r1     // Catch:{ all -> 0x015b }
                if (r4 == 0) goto L_0x005a
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                android.os.Handler r0 = r0.handler     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdViewCore$LoadContentTask$1 r1 = new com.tapit.adview.AdViewCore$LoadContentTask$1     // Catch:{ all -> 0x015b }
                r1.<init>()     // Catch:{ all -> 0x015b }
                r0.post(r1)     // Catch:{ all -> 0x015b }
            L_0x005a:
                r0 = 1
                java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ Exception -> 0x0339 }
                r1 = 0
                r2 = 0
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0339 }
                r0[r1] = r2     // Catch:{ Exception -> 0x0339 }
                r13.publishProgress(r0)     // Catch:{ Exception -> 0x0339 }
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0339 }
                com.tapit.adview.AdRequest r0 = r0.adRequest     // Catch:{ Exception -> 0x0339 }
                java.lang.String r0 = r0.createURL()     // Catch:{ Exception -> 0x0339 }
                java.lang.String r1 = "TapIt"
                android.util.Log.d(r1, r0)     // Catch:{ Exception -> 0x0339 }
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0339 }
                java.lang.String r2 = r1.requestGet(r0)     // Catch:{ Exception -> 0x0339 }
                java.lang.String r0 = "TapIt"
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x033f }
                org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0350 }
                r5.<init>(r2)     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r0 = "error"
                boolean r0 = r5.has(r0)     // Catch:{ JSONException -> 0x0350 }
                if (r0 == 0) goto L_0x015e
                java.lang.Exception r0 = new java.lang.Exception     // Catch:{ JSONException -> 0x0099 }
                java.lang.String r1 = "error"
                java.lang.String r1 = r5.getString(r1)     // Catch:{ JSONException -> 0x0099 }
                r0.<init>(r1)     // Catch:{ JSONException -> 0x0099 }
                throw r0     // Catch:{ JSONException -> 0x0099 }
            L_0x0099:
                r0 = move-exception
                r3 = r0
                r1 = r10
                r2 = r10
                r0 = r10
            L_0x009e:
                java.lang.String r5 = ""
                boolean r5 = r5.equals(r2)     // Catch:{ Exception -> 0x02cf }
                if (r5 == 0) goto L_0x02a9
                java.lang.String r3 = "server returned an empty response"
                r13.error = r3     // Catch:{ Exception -> 0x02cf }
                r3 = 1
                java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ Exception -> 0x02cf }
                r5 = 0
                r6 = 2
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x02cf }
                r3[r5] = r6     // Catch:{ Exception -> 0x02cf }
                r13.publishProgress(r3)     // Catch:{ Exception -> 0x02cf }
            L_0x00b8:
                r3 = r2
                r2 = r0
            L_0x00ba:
                if (r3 == 0) goto L_0x0331
                int r0 = r3.length()     // Catch:{ Exception -> 0x0318 }
                if (r0 <= 0) goto L_0x0331
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                android.os.Handler r0 = r0.handler     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore$RemoveAllChildViews r5 = new com.tapit.adview.AdViewCore$RemoveAllChildViews     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore r6 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                android.webkit.WebView r7 = r13.view     // Catch:{ Exception -> 0x0318 }
                r5.<init>(r7)     // Catch:{ Exception -> 0x0318 }
                r0.post(r5)     // Catch:{ Exception -> 0x0318 }
                java.lang.String r0 = "banner"
                com.tapit.adview.AdViewCore r5 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String r5 = r5.typeOfBanner     // Catch:{ Exception -> 0x0318 }
                boolean r0 = r0.equals(r5)     // Catch:{ Exception -> 0x0318 }
                if (r0 != 0) goto L_0x00ee
                java.lang.String r0 = "ormma"
                com.tapit.adview.AdViewCore r5 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String r5 = r5.typeOfBanner     // Catch:{ Exception -> 0x0318 }
                boolean r0 = r0.equals(r5)     // Catch:{ Exception -> 0x0318 }
                if (r0 == 0) goto L_0x02d6
            L_0x00ee:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String r1 = com.tapit.adview.AdViewCore.mBridgeScriptPath     // Catch:{ Exception -> 0x0318 }
                java.lang.String r2 = com.tapit.adview.AdViewCore.mScriptPath     // Catch:{ Exception -> 0x0318 }
                java.lang.String r0 = r0.wrapToHTML(r3, r1, r2)     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String unused = r1.mContent = r0     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                boolean r1 = r1.isBannerAnimationEnabled     // Catch:{ Exception -> 0x0318 }
                if (r1 == 0) goto L_0x0152
                if (r4 != 0) goto L_0x0152
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                int r0 = r0.getWidth()     // Catch:{ Exception -> 0x0318 }
                float r0 = (float) r0     // Catch:{ Exception -> 0x0318 }
                r1 = 1073741824(0x40000000, float:2.0)
                float r3 = r0 / r1
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                int r0 = r0.getHeight()     // Catch:{ Exception -> 0x0318 }
                float r0 = (float) r0     // Catch:{ Exception -> 0x0318 }
                r1 = 1073741824(0x40000000, float:2.0)
                float r4 = r0 / r1
                com.tapit.adview.Rotate3dAnimation r0 = new com.tapit.adview.Rotate3dAnimation     // Catch:{ Exception -> 0x0318 }
                r1 = 0
                r2 = 1119092736(0x42b40000, float:90.0)
                r5 = 1134231552(0x439b0000, float:310.0)
                r6 = 1
                r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0318 }
                r1 = 500(0x1f4, double:2.47E-321)
                r0.setDuration(r1)     // Catch:{ Exception -> 0x0318 }
                r1 = 1
                r0.setFillAfter(r1)     // Catch:{ Exception -> 0x0318 }
                android.view.animation.AccelerateInterpolator r1 = new android.view.animation.AccelerateInterpolator     // Catch:{ Exception -> 0x0318 }
                r1.<init>()     // Catch:{ Exception -> 0x0318 }
                r0.setInterpolator(r1)     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore$LoadContentTask$2 r1 = new com.tapit.adview.AdViewCore$LoadContentTask$2     // Catch:{ Exception -> 0x0318 }
                r1.<init>()     // Catch:{ Exception -> 0x0318 }
                r0.setAnimationListener(r1)     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                android.os.Handler r1 = r1.handler     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore$LoadContentTask$3 r2 = new com.tapit.adview.AdViewCore$LoadContentTask$3     // Catch:{ Exception -> 0x0318 }
                r2.<init>(r0)     // Catch:{ Exception -> 0x0318 }
                r1.post(r2)     // Catch:{ Exception -> 0x0318 }
                r0 = r10
            L_0x0152:
                r10 = r0
            L_0x0153:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                r0.scheduleUpdate()     // Catch:{ all -> 0x015b }
                monitor-exit(r11)     // Catch:{ all -> 0x015b }
                goto L_0x0045
            L_0x015b:
                r0 = move-exception
                monitor-exit(r11)     // Catch:{ all -> 0x015b }
                throw r0
            L_0x015e:
                java.lang.String r0 = "type"
                boolean r0 = r5.has(r0)     // Catch:{ JSONException -> 0x0350 }
                if (r0 == 0) goto L_0x0367
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = "type"
                java.lang.String r1 = r5.getString(r1)     // Catch:{ JSONException -> 0x0350 }
                java.lang.String unused = r0.typeOfBanner = r1     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r0 = "banner"
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = r1.typeOfBanner     // Catch:{ JSONException -> 0x0350 }
                boolean r0 = r0.equals(r1)     // Catch:{ JSONException -> 0x0350 }
                if (r0 == 0) goto L_0x01e0
                java.lang.String r0 = "html"
                java.lang.Object r0 = r5.get(r0)     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x0350 }
                r2 = r10
                r3 = r0
            L_0x0189:
                java.lang.String r0 = "clickurl"
                boolean r0 = r5.has(r0)     // Catch:{ JSONException -> 0x035d, Exception -> 0x034c }
                if (r0 == 0) goto L_0x0364
                java.lang.String r0 = "clickurl"
                java.lang.String r1 = r5.getString(r0)     // Catch:{ JSONException -> 0x035d, Exception -> 0x034c }
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                java.lang.String unused = r0.mClickURL = r1     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
            L_0x019c:
                java.lang.String r0 = "adHeight"
                boolean r0 = r5.has(r0)     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                if (r0 == 0) goto L_0x0261
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ NumberFormatException -> 0x0250 }
                java.lang.String r6 = "adHeight"
                java.lang.String r6 = r5.getString(r6)     // Catch:{ NumberFormatException -> 0x0250 }
                int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ NumberFormatException -> 0x0250 }
                int unused = r0.adHeight = r6     // Catch:{ NumberFormatException -> 0x0250 }
            L_0x01b3:
                java.lang.String r0 = "adWidth"
                boolean r0 = r5.has(r0)     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                if (r0 == 0) goto L_0x029e
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ NumberFormatException -> 0x0292 }
                java.lang.String r6 = "adWidth"
                java.lang.String r5 = r5.getString(r6)     // Catch:{ NumberFormatException -> 0x0292 }
                int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x0292 }
                int unused = r0.adWidth = r5     // Catch:{ NumberFormatException -> 0x0292 }
                r0 = r1
                r1 = r2
                r2 = r3
            L_0x01cd:
                r3 = 1
                java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ JSONException -> 0x01dd }
                r5 = 0
                r6 = 1
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ JSONException -> 0x01dd }
                r3[r5] = r6     // Catch:{ JSONException -> 0x01dd }
                r13.publishProgress(r3)     // Catch:{ JSONException -> 0x01dd }
                goto L_0x00b8
            L_0x01dd:
                r3 = move-exception
                goto L_0x009e
            L_0x01e0:
                java.lang.String r0 = "ormma"
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = r1.typeOfBanner     // Catch:{ JSONException -> 0x0350 }
                boolean r0 = r0.equals(r1)     // Catch:{ JSONException -> 0x0350 }
                if (r0 == 0) goto L_0x01f9
                java.lang.String r0 = "html"
                java.lang.Object r0 = r5.get(r0)     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x0350 }
                r2 = r10
                r3 = r0
                goto L_0x0189
            L_0x01f9:
                java.lang.String r0 = "video"
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = r1.typeOfBanner     // Catch:{ JSONException -> 0x0350 }
                boolean r0 = r0.equals(r1)     // Catch:{ JSONException -> 0x0350 }
                if (r0 == 0) goto L_0x0211
                java.lang.String r0 = "videourl"
                java.lang.String r0 = r5.getString(r0)     // Catch:{ JSONException -> 0x0350 }
                r3 = r2
                r2 = r0
                goto L_0x0189
            L_0x0211:
                java.lang.String r0 = "offerwall"
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = r1.typeOfBanner     // Catch:{ JSONException -> 0x0350 }
                boolean r0 = r0.equals(r1)     // Catch:{ JSONException -> 0x0350 }
                if (r0 == 0) goto L_0x0223
                r3 = r2
                r2 = r10
                goto L_0x0189
            L_0x0223:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = "banner"
                java.lang.String unused = r0.typeOfBanner = r1     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r0 = "html"
                java.lang.Object r0 = r5.get(r0)     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ JSONException -> 0x0350 }
                java.lang.String r1 = ""
                boolean r1 = r1.equals(r0)     // Catch:{ JSONException -> 0x0356, Exception -> 0x0345 }
                if (r1 == 0) goto L_0x024c
                java.lang.String r1 = "server returned a blank ad"
                r13.error = r1     // Catch:{ JSONException -> 0x0356, Exception -> 0x0345 }
                r1 = 1
                java.lang.Integer[] r1 = new java.lang.Integer[r1]     // Catch:{ JSONException -> 0x0356, Exception -> 0x0345 }
                r2 = 0
                r3 = 2
                java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ JSONException -> 0x0356, Exception -> 0x0345 }
                r1[r2] = r3     // Catch:{ JSONException -> 0x0356, Exception -> 0x0345 }
                r13.publishProgress(r1)     // Catch:{ JSONException -> 0x0356, Exception -> 0x0345 }
            L_0x024c:
                r2 = r10
                r3 = r0
                goto L_0x0189
            L_0x0250:
                r0 = move-exception
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                r6 = -1
                int unused = r0.adHeight = r6     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                goto L_0x01b3
            L_0x0259:
                r0 = move-exception
                r12 = r0
                r0 = r1
                r1 = r2
                r2 = r3
                r3 = r12
                goto L_0x009e
            L_0x0261:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                r6 = -1
                int unused = r0.adHeight = r6     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                goto L_0x01b3
            L_0x0269:
                r0 = move-exception
            L_0x026a:
                com.tapit.adview.AdViewCore r5 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdLog r5 = r5.adLog     // Catch:{ all -> 0x015b }
                r6 = 1
                r7 = 1
                java.lang.String r8 = "contentThreadAction.requestGet"
                java.lang.String r9 = r0.getMessage()     // Catch:{ all -> 0x015b }
                r5.log(r6, r7, r8, r9)     // Catch:{ all -> 0x015b }
                java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x015b }
                r13.error = r0     // Catch:{ all -> 0x015b }
                r0 = 1
                java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x015b }
                r5 = 0
                r6 = 2
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x015b }
                r0[r5] = r6     // Catch:{ all -> 0x015b }
                r13.publishProgress(r0)     // Catch:{ all -> 0x015b }
                r12 = r1
                r1 = r2
                r2 = r12
                goto L_0x00ba
            L_0x0292:
                r0 = move-exception
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                r5 = -1
                int unused = r0.adWidth = r5     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                r0 = r1
                r1 = r2
                r2 = r3
                goto L_0x01cd
            L_0x029e:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                r5 = -1
                int unused = r0.adWidth = r5     // Catch:{ JSONException -> 0x0259, Exception -> 0x0269 }
                r0 = r1
                r1 = r2
                r2 = r3
                goto L_0x01cd
            L_0x02a9:
                com.tapit.adview.AdViewCore r5 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x02cf }
                com.tapit.adview.AdLog r5 = r5.adLog     // Catch:{ Exception -> 0x02cf }
                r6 = 1
                r7 = 1
                java.lang.String r8 = "Load JSON"
                java.lang.String r3 = r3.getMessage()     // Catch:{ Exception -> 0x02cf }
                r5.log(r6, r7, r8, r3)     // Catch:{ Exception -> 0x02cf }
                com.tapit.adview.AdViewCore r3 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x02cf }
                java.lang.String r5 = "banner"
                java.lang.String unused = r3.typeOfBanner = r5     // Catch:{ Exception -> 0x02cf }
                r3 = 1
                java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ Exception -> 0x02cf }
                r5 = 0
                r6 = 1
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x02cf }
                r3[r5] = r6     // Catch:{ Exception -> 0x02cf }
                r13.publishProgress(r3)     // Catch:{ Exception -> 0x02cf }
                goto L_0x00b8
            L_0x02cf:
                r3 = move-exception
                r12 = r3
                r3 = r2
                r2 = r1
                r1 = r0
                r0 = r12
                goto L_0x026a
            L_0x02d6:
                java.lang.String r0 = "video"
                com.tapit.adview.AdViewCore r4 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String r4 = r4.typeOfBanner     // Catch:{ Exception -> 0x0318 }
                boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x0318 }
                if (r0 == 0) goto L_0x02f5
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                r7 = 0
                java.lang.String r8 = "fullscreen"
                java.lang.String r9 = "exit"
                r0.playVideo(r1, r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0318 }
                r0 = r10
                goto L_0x0152
            L_0x02f5:
                java.lang.String r0 = "offerwall"
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String r1 = r1.typeOfBanner     // Catch:{ Exception -> 0x0318 }
                boolean r0 = r0.equals(r1)     // Catch:{ Exception -> 0x0318 }
                if (r0 == 0) goto L_0x0336
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String r1 = com.tapit.adview.AdViewCore.mBridgeScriptPath     // Catch:{ Exception -> 0x0318 }
                java.lang.String r2 = com.tapit.adview.AdViewCore.mScriptPath     // Catch:{ Exception -> 0x0318 }
                java.lang.String r0 = r0.wrapToHTML(r3, r1, r2)     // Catch:{ Exception -> 0x0318 }
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                java.lang.String unused = r1.mContent = r0     // Catch:{ Exception -> 0x0318 }
                goto L_0x0152
            L_0x0318:
                r0 = move-exception
                com.tapit.adview.AdViewCore r1 = com.tapit.adview.AdViewCore.this     // Catch:{ all -> 0x015b }
                com.tapit.adview.AdLog r1 = r1.adLog     // Catch:{ all -> 0x015b }
                r2 = 1
                r3 = 1
                java.lang.String r4 = "LoadContentTask"
                java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x015b }
                r1.log(r2, r3, r4, r5)     // Catch:{ all -> 0x015b }
                java.lang.String r1 = "TapIt"
                java.lang.String r2 = "An error occured"
                android.util.Log.e(r1, r2, r0)     // Catch:{ all -> 0x015b }
                goto L_0x0153
            L_0x0331:
                com.tapit.adview.AdViewCore r0 = com.tapit.adview.AdViewCore.this     // Catch:{ Exception -> 0x0318 }
                r0.interstitialClose()     // Catch:{ Exception -> 0x0318 }
            L_0x0336:
                r0 = r10
                goto L_0x0152
            L_0x0339:
                r0 = move-exception
                r1 = r10
                r2 = r10
                r3 = r10
                goto L_0x026a
            L_0x033f:
                r0 = move-exception
                r1 = r10
                r3 = r2
                r2 = r10
                goto L_0x026a
            L_0x0345:
                r1 = move-exception
                r2 = r10
                r3 = r0
                r0 = r1
                r1 = r10
                goto L_0x026a
            L_0x034c:
                r0 = move-exception
                r1 = r10
                goto L_0x026a
            L_0x0350:
                r0 = move-exception
                r3 = r0
                r1 = r10
                r0 = r10
                goto L_0x009e
            L_0x0356:
                r1 = move-exception
                r3 = r1
                r2 = r0
                r1 = r10
                r0 = r10
                goto L_0x009e
            L_0x035d:
                r0 = move-exception
                r1 = r2
                r2 = r3
                r3 = r0
                r0 = r10
                goto L_0x009e
            L_0x0364:
                r1 = r10
                goto L_0x019c
            L_0x0367:
                r0 = r10
                r1 = r10
                goto L_0x01cd
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tapit.adview.AdViewCore.LoadContentTask.doInBackground(java.lang.Integer[]):java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            int intValue = numArr[0].intValue();
            if (AdViewCore.this.adDownload != null) {
                switch (intValue) {
                    case 0:
                        AdViewCore.this.adDownload.begin((AdViewCore) this.view);
                        return;
                    case 1:
                        AdViewCore.this.adDownload.end((AdViewCore) this.view);
                        return;
                    case 2:
                        AdViewCore.this.adDownload.error((AdViewCore) this.view, this.error);
                        return;
                    default:
                        return;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            if (str != null) {
                AdViewCore.this.loadDataWithBaseURL(str);
            }
        }
    }

    /* access modifiers changed from: protected */
    public String wrapToHTML(String str, String str2, String str3) {
        String str4;
        if (this.adWidth > 0) {
            str4 = "style=\"width:" + this.adWidth + "px; margin:0 auto; text-align:center;\"";
        } else {
            str4 = "align=\"left\"";
        }
        return "<html><head><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' /><title>Advertisement</title> </head><body style=\"margin:0; padding:0; overflow:hidden; background-color:transparent;\"><div " + str4 + ">" + str + "</div> " + "</body> " + "</html> ";
    }

    /* access modifiers changed from: protected */
    public void interstitialClose() {
    }

    /* access modifiers changed from: private */
    public void scheduleUpdate() {
        this.adLog.log(3, 3, TAG, "scheduleUpdate " + this.adReloadPeriod);
        if (this.lastTimerTask != null) {
            this.lastTimerTask.cancel();
        }
        this.lastTimerTask = new TimerTask() {
            public void run() {
                LoadContentTask unused = AdViewCore.this.contentTask = new LoadContentTask(AdViewCore.this, false);
                AdViewCore.this.contentTask.execute(0);
            }
        };
        if (this.adReloadPeriod > 0) {
            if (this.reloadTimer == null) {
                this.reloadTimer = new Timer(true);
            }
            this.reloadTimer.schedule(this.lastTimerTask, this.adReloadPeriod);
            return;
        }
        if (this.reloadTimer != null) {
            this.reloadTimer.cancel();
        }
        this.reloadTimer = null;
    }

    private class RemoveAllChildViews implements Runnable {
        private ViewGroup view;

        public RemoveAllChildViews(ViewGroup viewGroup) {
            this.view = viewGroup;
        }

        public void run() {
            try {
                this.view.removeAllViews();
            } catch (Exception e) {
                AdViewCore.this.adLog.log(1, 1, "RemoveAllChildViews", e.getMessage());
            }
        }
    }

    private class AdWebViewClient extends WebViewClient {
        private Context context;

        public AdWebViewClient(Context context2) {
            this.context = context2;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            try {
                AdViewCore.this.adLog.log(2, 3, "OverrideUrlLoading", str);
                if (AdViewCore.this.adClickListener != null) {
                    AdViewCore.this.adLog.log(2, 3, "OverrideUrlLoading - click", str);
                    AdViewCore.this.adClickListener.click(str);
                } else {
                    int checkCallingOrSelfPermission = this.context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE");
                    if (AdViewCore.this.adDownload != null) {
                        AdViewCore.this.adDownload.clicked((AdViewCore) webView);
                    }
                    if (checkCallingOrSelfPermission == 0) {
                        if (AdViewCore.this.isInternetAvailable(this.context)) {
                            AdViewCore.this.adLog.log(2, 3, "OverrideUrlLoading - openUrlInExternalBrowser", str);
                            if (AdViewCore.this.adDownload != null) {
                                AdViewCore.this.adDownload.willPresentFullScreen((AdViewCore) webView);
                            }
                            AdViewCore.this.openUrlInExternalBrowser(this.context, str);
                            if (AdViewCore.this.adDownload != null) {
                                AdViewCore.this.adDownload.didPresentFullScreen((AdViewCore) webView);
                            }
                        } else {
                            Toast.makeText(this.context, "Internet is not available", 1).show();
                        }
                    } else if (checkCallingOrSelfPermission == -1) {
                        AdViewCore.this.adLog.log(2, 3, "OverrideUrlLoading - openUrlInExternalBrowser", str);
                        if (AdViewCore.this.adDownload != null) {
                            AdViewCore.this.adDownload.willPresentFullScreen((AdViewCore) webView);
                            AdViewCore.this.adDownload.willLeaveApplication((AdViewCore) webView);
                        }
                        AdViewCore.this.openUrlInExternalBrowser(this.context, str);
                    }
                }
            } catch (Exception e) {
                AdViewCore.this.adLog.log(1, 1, "shouldOverrideUrlLoading", e.getMessage());
            }
            return true;
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            AdViewCore.this.adLog.log(2, 3, "onPageStarted", str);
        }

        public void onPageFinished(WebView webView, String str) {
            ((AdViewCore) webView).onPageFinished();
            AdViewCore.this.adLog.log(2, 3, "onPageFinished", str);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            try {
                if (AdViewCore.this.adDownload != null) {
                    AdViewCore.this.adDownload.error((AdViewCore) webView, str);
                }
            } catch (Exception e) {
                Log.e("TapIt", "An error occured", e);
            }
        }
    }

    /* access modifiers changed from: private */
    public void openUrlInExternalBrowser(Context context2, String str) {
        if (this.openInInternalBrowser) {
            try {
                AdActivity.callingAdView = this;
                Intent intent = new Intent(context2, AdActivity.class);
                intent.setData(Uri.parse(str));
                intent.putExtra("com.tapit.adview.ClickURL", str);
                context2.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                this.adLog.log(1, 1, "openUrlInExternalBrowser", e.getMessage() + " - Page will open in system browser.");
                try {
                    if (this.adDownload != null) {
                        this.adDownload.willLeaveApplication(this);
                    }
                    context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                } catch (Exception e2) {
                    this.adLog.log(1, 1, "openUrlInExternalBrowser", e2.getMessage());
                }
            } catch (Exception e3) {
                this.adLog.log(1, 1, "openUrlInExternalBrowser", e3.getMessage());
            }
        } else {
            try {
                if (this.adDownload != null) {
                    this.adDownload.willLeaveApplication(this);
                }
                context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            } catch (Exception e4) {
                this.adLog.log(1, 1, "openUrlInExternalBrowser", e4.getMessage());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void willDismissFullScreen() {
        if (this.adDownload != null) {
            this.adDownload.willDismissFullScreen(this);
        }
    }

    /* access modifiers changed from: private */
    public boolean isInternetAvailable(Context context2) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context2.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isAvailable()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public String requestGet(String str) throws IOException {
        requestCounter++;
        int i = requestCounter;
        this.adLog.log(3, 3, "requestGet[" + String.valueOf(i) + "]", str);
        InputStream content = new DefaultHttpClient().execute(new HttpGet(str)).getEntity().getContent();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(content, 8192);
        String readInputStream = readInputStream(bufferedInputStream);
        bufferedInputStream.close();
        content.close();
        this.adLog.log(3, 3, "requestGet result[" + String.valueOf(i) + "]", readInputStream);
        return readInputStream;
    }

    private static String readInputStream(BufferedInputStream bufferedInputStream) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = bufferedInputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
        }
    }

    private static Integer getIntParameter(String str) {
        if (str != null) {
            return Integer.valueOf(Integer.parseInt(str));
        }
        return null;
    }

    private static Long getLongParameter(String str) {
        if (str != null) {
            return Long.valueOf(Long.parseLong(str));
        }
        return null;
    }

    public void injectJavaScript(String str) {
        super.loadUrl("javascript:" + str);
    }

    public String getState() {
        return this.mViewState.toString().toLowerCase();
    }

    public void resize(int i, int i2) {
        throw new UnsupportedOperationException("Can't resize: " + i + ", " + i2);
    }

    public void resetContents() {
        FrameLayout frameLayout = (FrameLayout) getRootView().findViewById(100);
        FrameLayout frameLayout2 = (FrameLayout) getRootView().findViewById(101);
        ViewGroup viewGroup = (ViewGroup) frameLayout.getParent();
        frameLayout2.removeView(this);
        ((FrameLayout) getRootView().findViewById(16908290)).removeView(frameLayout2);
        resetLayout();
        viewGroup.addView(this, this.mIndex);
        viewGroup.removeView(frameLayout);
        viewGroup.invalidate();
    }

    public void close() {
        throw new UnsupportedOperationException("Can't close...");
    }

    public void hide() {
        throw new UnsupportedOperationException("Can't hide...");
    }

    public void show() {
        throw new UnsupportedOperationException("Can't hide...");
    }

    public void expand(Dimensions dimensions, String str, Properties properties) {
        throw new UnsupportedOperationException("Can't expand: " + dimensions + "; " + str + "; " + properties);
    }

    public void open(String str, boolean z, boolean z2, boolean z3) {
        openUrlInExternalBrowser(getContext(), str);
    }

    public void playVideo(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4, Dimensions dimensions, String str3, String str4) {
        throw new UnsupportedOperationException("Can't play: " + str + "; " + str2);
    }

    /* access modifiers changed from: protected */
    public void closeExpanded(View view) {
        ((ViewGroup) ((Activity) getContext()).getWindow().getDecorView()).removeView(view);
        requestLayout();
    }

    public void loadUrl(String str, boolean z, String str2) {
        this.mDataToInject = str2;
        if (!z) {
            if (str != null) {
                try {
                    if (str.length() > 0) {
                        super.loadUrl(str);
                    }
                } catch (Exception e) {
                    Log.e("TapIt", "An error occured", e);
                    this.adLog.log(1, 1, "loadUrl", e.getMessage());
                }
            }
        } else if (this.mContent != null && this.mContent.length() > 0) {
            loadDataWithBaseURL(this.mContent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPageFinished() {
        if (this.mDataToInject != null) {
            injectJavaScript(this.mDataToInject);
        }
        if (this.animateBack) {
            final Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(-90.0f, 0.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, 310.0f, false);
            rotate3dAnimation.setDuration(500);
            rotate3dAnimation.setFillAfter(true);
            rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
            this.handler.post(new Runnable() {
                public void run() {
                    AdViewCore.this.startAnimation(rotate3dAnimation);
                    boolean unused = AdViewCore.this.animateBack = false;
                }
            });
        }
    }

    private synchronized void setScriptPath() {
        mScriptPath = "//android_asset/ormma.js";
        mBridgeScriptPath = "//android_asset/ormma_bridge.js";
    }

    public void setContent(String str) {
        this.mContent = str;
    }

    public void setZone(String str) {
        if (this.adRequest != null) {
            this.adRequest.setZone(str);
        }
    }

    public String getZone() {
        if (this.adRequest != null) {
            return this.adRequest.getZone();
        }
        return null;
    }

    public void setAdtype(String str) {
        if (this.adRequest != null) {
            this.adRequest.setAdtype(str);
        }
    }

    public String getAdtype() {
        if (this.adRequest != null) {
            return this.adRequest.getAdtype();
        }
        return null;
    }

    public void setMinSizeX(Integer num) {
        if (this.adRequest != null) {
            this.adRequest.setMinSizeX(num);
        }
    }

    public Integer getMinSizeX() {
        if (this.adRequest != null) {
            return this.adRequest.getMinSizeX();
        }
        return null;
    }

    public void setMinSizeY(Integer num) {
        if (this.adRequest != null) {
            this.adRequest.setMinSizeY(num);
        }
    }

    public Integer getMinSizeY() {
        if (this.adRequest != null) {
            return this.adRequest.getMinSizeY();
        }
        return null;
    }

    public void setMaxSizeX(Integer num) {
        if (this.adRequest != null) {
            this.adRequest.setSizeX(num);
        }
    }

    public Integer getMaxSizeX() {
        if (this.adRequest != null) {
            return this.adRequest.getSizeX();
        }
        return null;
    }

    public void setMaxSizeY(Integer num) {
        if (this.adRequest != null) {
            this.adRequest.setSizeY(num);
        }
    }

    public Integer getMaxSizeY() {
        if (this.adRequest != null) {
            return this.adRequest.getSizeY();
        }
        return null;
    }

    public void setBackgroundColor(String str) {
        try {
            setBackgroundColor(Integer.decode("#" + str).intValue());
        } catch (NumberFormatException e) {
            this.adLog.log(1, 1, "AdViewCore.setBackgroundColor", e.getMessage());
        }
        if (this.adRequest != null) {
            try {
                this.adLog.log(3, 3, "setBackgroundColor", "#" + str);
                this.adRequest.setParamBG(str);
            } catch (Exception e2) {
                this.adLog.log(1, 1, "AdViewCore.setBackgroundColor", e2.getMessage());
            }
        }
    }

    public void setBackgroundColor(int i) {
        this.backgroundColor = Integer.valueOf(i);
        super.setBackgroundColor(i);
    }

    public String getBackgroundColor() {
        if (this.adRequest != null) {
            return this.adRequest.getParamBG();
        }
        return "FFFFFF";
    }

    public void setTextColor(String str) {
        if (this.adRequest != null) {
            this.adRequest.setParamLINK(str);
        }
    }

    public String getTextColor() {
        if (this.adRequest != null) {
            return this.adRequest.getParamLINK();
        }
        return null;
    }

    public void setAdserverURL(String str) {
        if (this.adRequest != null) {
            this.adRequest.setAdserverURL(str);
        }
    }

    public String getAdserverURL() {
        if (this.adRequest != null) {
            return this.adRequest.getAdserverURL();
        }
        return null;
    }

    public void setLatitude(String str) {
        if (this.adRequest != null && str != null) {
            this.adRequest.setLatitude(str);
        }
    }

    public String getLatitude() {
        if (this.adRequest == null) {
            return null;
        }
        String latitude = this.adRequest.getLatitude();
        if (latitude != null) {
            return latitude;
        }
        return null;
    }

    public void setLongitude(String str) {
        if (this.adRequest != null && str != null) {
            this.adRequest.setLongitude(str);
        }
    }

    public String getLongitude() {
        if (this.adRequest == null) {
            return null;
        }
        String longitude = this.adRequest.getLongitude();
        if (longitude != null) {
            return longitude;
        }
        return null;
    }

    public String getClickURL() {
        return this.mClickURL;
    }

    public void setLogLevel(int i) {
        this.adLog.setLogLevel(i);
    }

    public void setOpenInInternalBrowser(boolean z) {
        this.openInInternalBrowser = z;
    }

    public void raiseError(String str, String str2) {
        throw new UnsupportedOperationException("Can't raise error: " + str + "; " + str2);
    }

    private void resetLayout() {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (this.bGotLayoutParams) {
            layoutParams.height = this.mInitLayoutHeight;
            layoutParams.width = this.mInitLayoutWidth;
        }
        setVisibility(0);
        requestLayout();
    }

    public void setBannerAnimationEnabled(boolean z) {
        this.isBannerAnimationEnabled = z;
    }

    public boolean isBannerAnimationEnabled() {
        return this.isBannerAnimationEnabled;
    }

    /* access modifiers changed from: protected */
    public void loadDataWithBaseURL(String str) {
        super.loadDataWithBaseURL("about:blank", str, HtmlFormatter.TEXT_HTML, Const.ENCODING, "about:blank");
        if (this.backgroundColor != null) {
            setBackgroundColor(this.backgroundColor.intValue());
        }
    }

    protected class Dimensions {
        protected Dimensions() {
        }
    }

    protected class Properties {
        protected Properties() {
        }
    }
}
