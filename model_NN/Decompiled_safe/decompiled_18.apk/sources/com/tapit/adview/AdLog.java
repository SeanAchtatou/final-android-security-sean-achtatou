package com.tapit.adview;

import android.util.Log;
import java.io.File;
import java.io.IOException;

public class AdLog {
    public static final int LOG_LEVEL_1 = 1;
    public static final int LOG_LEVEL_2 = 2;
    public static final int LOG_LEVEL_3 = 3;
    public static final int LOG_LEVEL_NONE = 0;
    public static final int LOG_TYPE_ERROR = 1;
    public static final int LOG_TYPE_INFO = 3;
    public static final int LOG_TYPE_WARNING = 2;
    private static int defaultLevel = 0;
    private int currentLogLevel = 0;
    private Object object;

    public static void setDefaultLogLevel(int i) {
        defaultLevel = i;
    }

    public static void setFileLog(String str) {
        try {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            Runtime.getRuntime().exec("logcat -v time -f " + file.getAbsolutePath());
        } catch (IOException e) {
            Log.e("TapIt", "An error occured", e);
        }
    }

    public AdLog(Object obj) {
        this.object = obj;
        setLogLevel(defaultLevel);
    }

    public void log(int i, int i2, String str, String str2) {
        String str3 = "[" + Integer.toHexString(this.object.hashCode()) + "]" + str;
        if (i <= this.currentLogLevel) {
            switch (i2) {
                case 1:
                    Log.e(str3, str2 + ' ');
                    return;
                case 2:
                    Log.w(str3, str2 + ' ');
                    return;
                default:
                    Log.i(str3, str2 + ' ');
                    return;
            }
        }
    }

    public void setLogLevel(int i) {
        this.currentLogLevel = i;
        switch (i) {
            case 1:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_1");
                return;
            case 2:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_2");
                return;
            case 3:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_3");
                return;
            default:
                log(1, 3, "SetLogLevel", "LOG_LEVEL_NONE");
                return;
        }
    }
}
