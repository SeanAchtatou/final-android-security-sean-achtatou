package com.tapit.adview;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class AdFullscreenView extends AdInterstitialBaseView {
    private ImageButton transparentButton;

    public AdFullscreenView(Context context, String str) {
        super(context, str);
        setAdtype("2");
    }

    public View getInterstitialView(Context context) {
        removeViews();
        this.callingActivityContext = context;
        this.interstitialLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        this.interstitialLayout.addView(this, layoutParams);
        initNavigation();
        return this.interstitialLayout;
    }

    /* access modifiers changed from: protected */
    public void initNavigation() {
        ImageButton imageButton = new ImageButton(this.context);
        imageButton.setBackgroundDrawable(null);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdFullscreenView.this.showNavigation();
            }
        });
        this.interstitialLayout.addView(imageButton, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void showNavigation() {
        this.interstitialLayout.removeView(this.transparentButton);
        Button button = new Button(this.context);
        button.setText("See More");
        button.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdFullscreenView.this.loadAdAction();
            }
        });
        Button button2 = new Button(this.context);
        button2.setText("Skip");
        button2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdFullscreenView.this.closeInterstitial();
            }
        });
        LinearLayout linearLayout = new LinearLayout(this.context);
        linearLayout.setOrientation(1);
        linearLayout.addView(button);
        linearLayout.addView(button2);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        RelativeLayout relativeLayout = new RelativeLayout(this.context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(12);
        relativeLayout.addView(linearLayout, layoutParams2);
        this.interstitialLayout.addView(relativeLayout, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void loadAdAction() {
        loadUrl(getClickURL());
    }
}
