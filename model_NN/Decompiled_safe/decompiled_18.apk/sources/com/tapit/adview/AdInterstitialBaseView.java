package com.tapit.adview;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.inmobi.androidsdk.impl.IMAdException;
import com.tapit.adview.AdViewCore;

public abstract class AdInterstitialBaseView extends AdView implements AdViewCore.OnAdDownload, AdViewCore.OnAdClickListener {
    protected Context callingActivityContext;
    protected final Context context;
    protected RelativeLayout interstitialLayout;
    protected AdViewCore.OnInterstitialAdDownload interstitialListener;
    protected boolean isLoaded = false;

    public abstract View getInterstitialView(Context context2);

    public enum FullscreenAdSize {
        AUTOSIZE_AD(-1, -1),
        MEDIUM_RECTANGLE(IMAdException.INVALID_REQUEST, 250);
        
        public final int height;
        public final int width;

        private FullscreenAdSize(int i, int i2) {
            this.width = i;
            this.height = i2;
        }
    }

    public AdInterstitialBaseView(Context context2, String str) {
        super(context2, str);
        this.context = context2;
        setAdSize(FullscreenAdSize.AUTOSIZE_AD);
        setOnAdDownload(this);
        setOnAdClickListener(this);
        setUpdateTime(0);
    }

    public void setAdSize(FullscreenAdSize fullscreenAdSize) {
        int i;
        int i2;
        int i3 = fullscreenAdSize.width;
        int i4 = fullscreenAdSize.height;
        if (i3 <= 0) {
            Display defaultDisplay = ((Activity) this.context).getWindowManager().getDefaultDisplay();
            int height = defaultDisplay.getHeight();
            int width = defaultDisplay.getWidth();
            int width2 = getWidth();
            if (width2 > 0) {
                width = width2;
            }
            int height2 = getHeight();
            if (height2 <= 0) {
                height2 = height;
            }
            i2 = i4;
            i = i3;
            for (FullscreenAdSize fullscreenAdSize2 : FullscreenAdSize.values()) {
                if (fullscreenAdSize2.width <= width && fullscreenAdSize2.height <= height2 && (fullscreenAdSize2.width > i || fullscreenAdSize2.height > i2)) {
                    i = fullscreenAdSize2.width;
                    i2 = fullscreenAdSize2.height;
                }
            }
        } else {
            i2 = i4;
            i = i3;
        }
        if (this.adRequest != null) {
            this.adRequest.setHeight(Integer.valueOf(i2));
            this.adRequest.setWidth(Integer.valueOf(i));
        }
    }

    /* access modifiers changed from: protected */
    public void removeViews() {
        RelativeLayout relativeLayout = (RelativeLayout) getParent();
        if (relativeLayout != null) {
            relativeLayout.removeAllViews();
        }
    }

    public void closeInterstitial() {
        this.handler.post(new Runnable() {
            public void run() {
                if (AdInterstitialBaseView.this.callingActivityContext != null) {
                    ((Activity) AdInterstitialBaseView.this.callingActivityContext).finish();
                    if (AdInterstitialBaseView.this.interstitialListener != null) {
                        AdInterstitialBaseView.this.interstitialListener.didClose(this);
                    }
                    AdInterstitialBaseView.this.removeViews();
                }
            }
        });
    }

    public boolean isLoaded() {
        return this.isLoaded;
    }

    public void load() {
        update(true);
    }

    public void showInterstitial() {
        if (this.interstitialListener != null) {
            this.interstitialListener.willOpen(this);
        }
        AdActivity.adView = this;
        ((Activity) this.context).startActivityForResult(new Intent(this.context, AdActivity.class), 1);
    }

    public void begin(AdViewCore adViewCore) {
        this.isLoaded = false;
        if (this.interstitialListener != null) {
            this.interstitialListener.willLoad(adViewCore);
        }
    }

    public void end(AdViewCore adViewCore) {
        this.isLoaded = true;
        if (this.interstitialListener != null) {
            this.interstitialListener.ready(adViewCore);
        }
    }

    public void clicked(AdViewCore adViewCore) {
        if (this.interstitialListener != null) {
            this.interstitialListener.clicked(adViewCore);
        }
    }

    public void willLeaveApplication(AdViewCore adViewCore) {
        if (this.interstitialListener != null) {
            this.interstitialListener.willLeaveApplication(adViewCore);
        }
    }

    public void error(AdViewCore adViewCore, String str) {
        if (this.interstitialListener != null) {
            this.interstitialListener.error(adViewCore, str);
        }
    }

    public void click(String str) {
        if (str.toLowerCase().startsWith("http://") || str.toLowerCase().startsWith("https://")) {
            loadUrl(str);
            return;
        }
        if (this.interstitialListener != null) {
            this.interstitialListener.willLeaveApplication(this);
        }
        ((Activity) this.callingActivityContext).startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse(str)), 2);
    }

    public void willPresentFullScreen(AdViewCore adViewCore) {
    }

    public void didPresentFullScreen(AdViewCore adViewCore) {
    }

    public void willDismissFullScreen(AdViewCore adViewCore) {
    }

    public AdViewCore.OnInterstitialAdDownload getOnInterstitialAdDownload() {
        return this.interstitialListener;
    }

    public void setOnInterstitialAdDownload(AdViewCore.OnInterstitialAdDownload onInterstitialAdDownload) {
        this.interstitialListener = onInterstitialAdDownload;
    }

    public final void setUpdateTime(int i) {
    }

    public void interstitialShowing() {
    }

    public void interstitialClosing() {
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || keyEvent.getRepeatCount() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        closeInterstitial();
        return true;
    }

    public static int getResourceIdByName(String str, String str2, String str3) {
        try {
            Class<?>[] classes = Class.forName(str + ".R").getClasses();
            Class<?> cls = null;
            int i = 0;
            while (true) {
                if (i >= classes.length) {
                    break;
                } else if (classes[i].getName().split("\\$")[1].equals(str2)) {
                    cls = classes[i];
                    break;
                } else {
                    i++;
                }
            }
            if (cls != null) {
                return cls.getField(str3).getInt(cls);
            }
            return 0;
        } catch (ClassNotFoundException e) {
            Log.e("TapIt", "An error occured", e);
            return 0;
        } catch (IllegalArgumentException e2) {
            Log.e("TapIt", "An error occured", e2);
            return 0;
        } catch (SecurityException e3) {
            Log.e("TapIt", "An error occured", e3);
            return 0;
        } catch (IllegalAccessException e4) {
            Log.e("TapIt", "An error occured", e4);
            return 0;
        } catch (NoSuchFieldException e5) {
            Log.e("TapIt", "An error occured", e5);
            return 0;
        }
    }
}
