package com.tapit.adview;

public class AutoDetectedParametersSet {
    private static AutoDetectedParametersSet instance;
    private Integer connectionSpeed;
    private String latitude;
    private String longitude;
    private String ua;

    private AutoDetectedParametersSet() {
    }

    public static synchronized AutoDetectedParametersSet getInstance() {
        AutoDetectedParametersSet autoDetectedParametersSet;
        synchronized (AutoDetectedParametersSet.class) {
            if (instance == null) {
                instance = new AutoDetectedParametersSet();
            }
            autoDetectedParametersSet = instance;
        }
        return autoDetectedParametersSet;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String str) {
        this.latitude = str;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String str) {
        this.longitude = str;
    }

    public String getUa() {
        return this.ua;
    }

    public void setUa(String str) {
        this.ua = str;
    }

    public Integer getConnectionSpeed() {
        return this.connectionSpeed;
    }

    public void setConnectionSpeed(Integer num) {
        this.connectionSpeed = num;
    }
}
