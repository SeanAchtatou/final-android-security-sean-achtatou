package com.tapit.adview;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Display;
import com.google.ads.AdActivity;
import com.inmobi.androidsdk.impl.IMAdException;
import java.lang.Thread;

public class AdView extends AdViewCore {
    private BannerAdSize adSize;
    private AutoDetectParametersThread autoDetectParametersThread;
    /* access modifiers changed from: private */
    public WhereamiLocationListener listener;
    /* access modifiers changed from: private */
    public LocationManager locationManager;

    public enum BannerAdSize {
        AUTOSIZE_AD(-1, -1),
        SMALL_BANNER(120, 20),
        MEDIUM_BANNER(168, 28),
        LARGE_BANNER(216, 36),
        XL_BANNER(IMAdException.INVALID_REQUEST, 50),
        IPHONE_BANNER(320, 50),
        MEDIUM_RECTANGLE(IMAdException.INVALID_REQUEST, 250);
        
        public final int height;
        public final int width;

        private BannerAdSize(int i, int i2) {
            this.width = i;
            this.height = i2;
        }
    }

    public AdView(Context context, String str) {
        super(context, str);
        initialize(context);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(context);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(context);
    }

    public AdView(Context context) {
        super(context);
        initialize(context);
    }

    private void initialize(Context context) {
        setAdSize(BannerAdSize.AUTOSIZE_AD);
        if (this.adRequest != null) {
            AutoDetectedParametersSet instance = AutoDetectedParametersSet.getInstance();
            if (this.adRequest.getUa() == null) {
                if (instance.getUa() == null) {
                    String userAgentString = getSettings().getUserAgentString();
                    if (userAgentString != null && userAgentString.length() > 0) {
                        this.adRequest.setUa(userAgentString);
                        instance.setUa(userAgentString);
                    }
                } else {
                    this.adRequest.setUa(instance.getUa());
                }
            }
        }
        this.autoDetectParametersThread = new AutoDetectParametersThread(context, this, this.adRequest);
    }

    public void setAdSize(BannerAdSize bannerAdSize) {
        int i;
        int i2;
        String str;
        int i3 = bannerAdSize.width;
        int i4 = bannerAdSize.height;
        if (i3 <= 0) {
            Display defaultDisplay = ((Activity) this.context).getWindowManager().getDefaultDisplay();
            int height = defaultDisplay.getHeight();
            int width = defaultDisplay.getWidth();
            int i5 = ((Activity) this.context).getResources().getConfiguration().orientation;
            if (i5 == 2) {
                str = "l";
            } else if (i5 == 0) {
                str = "x";
            } else {
                str = "p";
            }
            this.adRequest.getCustomParameters().put(AdActivity.ORIENTATION_PARAM, str);
            int width2 = getWidth();
            if (width2 <= 0) {
                width2 = width;
            }
            int height2 = getHeight();
            if (height2 <= 0) {
                height2 = height;
            }
            i2 = i4;
            i = i3;
            for (BannerAdSize bannerAdSize2 : BannerAdSize.values()) {
                if (!(bannerAdSize == BannerAdSize.AUTOSIZE_AD && bannerAdSize2 == BannerAdSize.MEDIUM_RECTANGLE) && bannerAdSize2.width <= width2 && bannerAdSize2.height <= height2 && (bannerAdSize2.width > i || bannerAdSize2.height > i2)) {
                    i = bannerAdSize2.width;
                    i2 = bannerAdSize2.height;
                }
            }
        } else {
            i2 = i4;
            i = i3;
        }
        if (this.adRequest != null) {
            this.adRequest.setHeight(Integer.valueOf(i2));
            this.adRequest.setWidth(Integer.valueOf(i));
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        this.adLog.log(2, 3, "AttachedToWindow", "");
        if (this.autoDetectParametersThread != null && this.autoDetectParametersThread.getState().equals(Thread.State.NEW)) {
            this.autoDetectParametersThread.start();
        }
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.adLog.log(2, 3, "DetachedFromWindow", "");
        super.onDetachedFromWindow();
    }

    public void startUpdating() {
    }

    public void cancelUpdating() {
        if (!(this.locationManager == null || this.listener == null)) {
            this.locationManager.removeUpdates(this.listener);
        }
        if (this.autoDetectParametersThread != null) {
            try {
                this.autoDetectParametersThread.interrupt();
            } catch (Exception e) {
            }
        }
        super.cancelUpdating();
    }

    private class AutoDetectParametersThread extends Thread {
        private AdRequest adRequest;
        private AdViewCore adserverView;
        private Context context;

        public AutoDetectParametersThread(Context context2, AdViewCore adViewCore, AdRequest adRequest2) {
            this.context = context2;
            this.adserverView = adViewCore;
            this.adRequest = adRequest2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:42:0x0112 A[Catch:{ Exception -> 0x01c9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:66:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r11 = this;
                r2 = 0
                r4 = 0
                r7 = 0
                r8 = 1
                r10 = 2
                com.tapit.adview.AdRequest r0 = r11.adRequest
                if (r0 == 0) goto L_0x011a
                com.tapit.adview.AutoDetectedParametersSet r9 = com.tapit.adview.AutoDetectedParametersSet.getInstance()
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.String r0 = r0.getUa()
                if (r0 != 0) goto L_0x0036
                java.lang.String r0 = r9.getUa()
                if (r0 != 0) goto L_0x011b
                com.tapit.adview.AdViewCore r0 = r11.adserverView
                android.webkit.WebSettings r0 = r0.getSettings()
                java.lang.String r0 = r0.getUserAgentString()
                if (r0 == 0) goto L_0x0036
                int r1 = r0.length()
                if (r1 <= 0) goto L_0x0036
                com.tapit.adview.AdRequest r1 = r11.adRequest
                r1.setUa(r0)
                r9.setUa(r0)
            L_0x0036:
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.String r0 = r0.getLatitude()
                if (r0 == 0) goto L_0x0046
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.String r0 = r0.getLongitude()
                if (r0 != 0) goto L_0x00e2
            L_0x0046:
                java.lang.String r0 = r9.getLatitude()
                if (r0 == 0) goto L_0x0052
                java.lang.String r0 = r9.getLongitude()
                if (r0 != 0) goto L_0x0158
            L_0x0052:
                android.content.Context r0 = r11.context
                java.lang.String r1 = "android.permission.ACCESS_FINE_LOCATION"
                int r1 = r0.checkCallingOrSelfPermission(r1)
                com.tapit.adview.AdView r5 = com.tapit.adview.AdView.this
                android.content.Context r0 = r11.context
                java.lang.String r6 = "location"
                java.lang.Object r0 = r0.getSystemService(r6)
                android.location.LocationManager r0 = (android.location.LocationManager) r0
                android.location.LocationManager unused = r5.locationManager = r0
                if (r1 != 0) goto L_0x0134
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                android.location.LocationManager r0 = r0.locationManager
                java.lang.String r1 = "gps"
                boolean r0 = r0.isProviderEnabled(r1)
                if (r0 == 0) goto L_0x0126
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdView$WhereamiLocationListener r1 = new com.tapit.adview.AdView$WhereamiLocationListener
                com.tapit.adview.AdView r5 = com.tapit.adview.AdView.this
                com.tapit.adview.AdView r6 = com.tapit.adview.AdView.this
                android.location.LocationManager r6 = r6.locationManager
                r1.<init>(r6, r9)
                com.tapit.adview.AdView.WhereamiLocationListener unused = r0.listener = r1
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                android.location.LocationManager r0 = r0.locationManager
                java.lang.String r1 = "gps"
                com.tapit.adview.AdView r5 = com.tapit.adview.AdView.this
                com.tapit.adview.AdView$WhereamiLocationListener r5 = r5.listener
                android.os.Looper r6 = android.os.Looper.getMainLooper()
                r0.requestLocationUpdates(r1, r2, r4, r5, r6)
                r0 = r7
            L_0x00a1:
                if (r0 == 0) goto L_0x00e2
                android.content.Context r0 = r11.context
                java.lang.String r1 = "android.permission.ACCESS_COARSE_LOCATION"
                int r0 = r0.checkCallingOrSelfPermission(r1)
                if (r0 != 0) goto L_0x014c
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                android.location.LocationManager r0 = r0.locationManager
                java.lang.String r1 = "network"
                boolean r0 = r0.isProviderEnabled(r1)
                if (r0 == 0) goto L_0x0140
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdView$WhereamiLocationListener r1 = new com.tapit.adview.AdView$WhereamiLocationListener
                com.tapit.adview.AdView r5 = com.tapit.adview.AdView.this
                com.tapit.adview.AdView r6 = com.tapit.adview.AdView.this
                android.location.LocationManager r6 = r6.locationManager
                r1.<init>(r6, r9)
                com.tapit.adview.AdView.WhereamiLocationListener unused = r0.listener = r1
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                android.location.LocationManager r0 = r0.locationManager
                java.lang.String r1 = "network"
                com.tapit.adview.AdView r5 = com.tapit.adview.AdView.this
                com.tapit.adview.AdView$WhereamiLocationListener r5 = r5.listener
                android.os.Looper r6 = android.os.Looper.getMainLooper()
                r0.requestLocationUpdates(r1, r2, r4, r5, r6)
            L_0x00e2:
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.Integer r0 = r0.getConnectionSpeed()
                if (r0 != 0) goto L_0x011a
                java.lang.Integer r0 = r9.getConnectionSpeed()
                if (r0 != 0) goto L_0x01be
                r1 = 0
                android.content.Context r0 = r11.context     // Catch:{ Exception -> 0x01c9 }
                java.lang.String r2 = "connectivity"
                java.lang.Object r0 = r0.getSystemService(r2)     // Catch:{ Exception -> 0x01c9 }
                android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x01c9 }
                android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x01c9 }
                if (r0 == 0) goto L_0x01cc
                int r2 = r0.getType()     // Catch:{ Exception -> 0x01c9 }
                int r0 = r0.getSubtype()     // Catch:{ Exception -> 0x01c9 }
                if (r2 != r8) goto L_0x01a0
                r0 = 1
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x01c9 }
            L_0x0110:
                if (r0 == 0) goto L_0x011a
                com.tapit.adview.AdRequest r1 = r11.adRequest     // Catch:{ Exception -> 0x01c9 }
                r1.setConnectionSpeed(r0)     // Catch:{ Exception -> 0x01c9 }
                r9.setConnectionSpeed(r0)     // Catch:{ Exception -> 0x01c9 }
            L_0x011a:
                return
            L_0x011b:
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.String r1 = r9.getUa()
                r0.setUa(r1)
                goto L_0x0036
            L_0x0126:
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdLog r0 = r0.adLog
                java.lang.String r1 = "AutoDetectedParametersSet.Gps"
                java.lang.String r5 = "not avalable"
                r0.log(r10, r10, r1, r5)
            L_0x0131:
                r0 = r8
                goto L_0x00a1
            L_0x0134:
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdLog r0 = r0.adLog
                java.lang.String r1 = "AutoDetectedParametersSet.Gps"
                java.lang.String r5 = "no permission ACCESS_FINE_LOCATION"
                r0.log(r10, r10, r1, r5)
                goto L_0x0131
            L_0x0140:
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdLog r0 = r0.adLog
                java.lang.String r1 = "AutoDetectedParametersSet.Network"
                java.lang.String r2 = "not avalable"
                r0.log(r10, r10, r1, r2)
                goto L_0x00e2
            L_0x014c:
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdLog r0 = r0.adLog
                java.lang.String r1 = "AutoDetectedParametersSet.Network"
                java.lang.String r2 = "no permission ACCESS_COARSE_LOCATION"
                r0.log(r10, r10, r1, r2)
                goto L_0x00e2
            L_0x0158:
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.String r1 = r9.getLatitude()
                r0.setLatitude(r1)
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.String r1 = r9.getLongitude()
                r0.setLongitude(r1)
                com.tapit.adview.AdView r0 = com.tapit.adview.AdView.this
                com.tapit.adview.AdLog r0 = r0.adLog
                java.lang.String r1 = "AutoDetectedParametersSet.Gps/Network="
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "("
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r3 = r9.getLatitude()
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r3 = ";"
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r3 = r9.getLongitude()
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r3 = ")"
                java.lang.StringBuilder r2 = r2.append(r3)
                java.lang.String r2 = r2.toString()
                r0.log(r10, r10, r1, r2)
                goto L_0x00e2
            L_0x01a0:
                if (r2 != 0) goto L_0x01cc
                if (r0 != r10) goto L_0x01ab
                r0 = 0
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x01c9 }
                goto L_0x0110
            L_0x01ab:
                if (r0 != r8) goto L_0x01b4
                r0 = 0
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x01c9 }
                goto L_0x0110
            L_0x01b4:
                r2 = 3
                if (r0 != r2) goto L_0x01cc
                r0 = 1
                java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x01c9 }
                goto L_0x0110
            L_0x01be:
                com.tapit.adview.AdRequest r0 = r11.adRequest
                java.lang.Integer r1 = r9.getConnectionSpeed()
                r0.setConnectionSpeed(r1)
                goto L_0x011a
            L_0x01c9:
                r0 = move-exception
                goto L_0x011a
            L_0x01cc:
                r0 = r1
                goto L_0x0110
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tapit.adview.AdView.AutoDetectParametersThread.run():void");
        }
    }

    private class WhereamiLocationListener implements LocationListener {
        private AutoDetectedParametersSet autoDetectedParametersSet;
        private LocationManager locationManager;

        public WhereamiLocationListener(LocationManager locationManager2, AutoDetectedParametersSet autoDetectedParametersSet2) {
            this.locationManager = locationManager2;
            this.autoDetectedParametersSet = autoDetectedParametersSet2;
        }

        public void onLocationChanged(Location location) {
            this.locationManager.removeUpdates(this);
            try {
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                AdView.this.adRequest.setLatitude(Double.toString(latitude));
                AdView.this.adRequest.setLongitude(Double.toString(longitude));
                this.autoDetectedParametersSet.setLatitude(Double.toString(latitude));
                this.autoDetectedParametersSet.setLongitude(Double.toString(longitude));
                AdView.this.adLog.log(3, 3, "LocationChanged=", "(" + this.autoDetectedParametersSet.getLatitude() + ";" + this.autoDetectedParametersSet.getLongitude() + ")");
            } catch (Exception e) {
                AdView.this.adLog.log(2, 1, "LocationChanged", e.getMessage());
            }
        }

        public void onProviderDisabled(String str) {
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
        }
    }
}
