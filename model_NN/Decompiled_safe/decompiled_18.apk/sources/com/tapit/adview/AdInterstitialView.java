package com.tapit.adview;

import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public class AdInterstitialView extends AdInterstitialBaseView {
    protected static final float CLOSE_BUTTON_PADDING_DP = 8.0f;
    protected static final float CLOSE_BUTTON_SIZE_DP = 50.0f;
    protected ImageButton closeButton;

    public AdInterstitialView(Context context, String str) {
        super(context, str);
        setAdtype("2");
    }

    public View getInterstitialView(Context context) {
        this.callingActivityContext = context;
        this.interstitialLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(13);
        this.interstitialLayout.addView(this, layoutParams);
        showInterstitialCloseButton();
        return this.interstitialLayout;
    }

    /* access modifiers changed from: protected */
    public void showInterstitialCloseButton() {
        StateListDrawable stateListDrawable = new StateListDrawable();
        try {
            stateListDrawable.addState(new int[]{-16842919}, getResources().getDrawable(17301594));
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        this.closeButton = new ImageButton(this.context);
        this.closeButton.setImageDrawable(stateListDrawable);
        this.closeButton.setBackgroundDrawable(null);
        this.closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AdInterstitialView.this.closeInterstitial();
            }
        });
        float f = getResources().getDisplayMetrics().density;
        int i = (int) ((CLOSE_BUTTON_SIZE_DP * f) + 0.5f);
        int i2 = (int) ((f * CLOSE_BUTTON_PADDING_DP) + 0.5f);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i);
        layoutParams.addRule(11);
        layoutParams.setMargins(i2, 0, i2, 0);
        this.interstitialLayout.removeView(this.closeButton);
        this.interstitialLayout.addView(this.closeButton, layoutParams);
    }

    public void click(String str) {
        this.closeButton.setVisibility(8);
        super.click(str);
    }

    public void end(AdViewCore adViewCore) {
        super.end(adViewCore);
    }
}
