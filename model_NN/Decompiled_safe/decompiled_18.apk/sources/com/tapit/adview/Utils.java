package com.tapit.adview;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.reflect.Constructor;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import org.apache.cordova.NetworkManager;

public class Utils {
    private static final String INSTALLATION = "INSTALLATION";
    private static String sID = null;
    private static String userAgent = null;

    public static String scrape(String str, String str2, String str3) {
        int indexOf;
        int indexOf2 = str.indexOf(str2);
        if (indexOf2 >= 0 && (indexOf = str.indexOf(str3, str2.length() + indexOf2)) >= 0) {
            return str.substring(indexOf2 + str2.length(), indexOf);
        }
        return "";
    }

    public static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            return byteArrayToHexString(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String byteArrayToHexString(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            byte b2 = b & 255;
            if (b2 < 16) {
                stringBuffer.append("0");
            }
            stringBuffer.append(Integer.toHexString(b2));
        }
        return stringBuffer.toString();
    }

    public static String getUserAgentString(Context context) {
        Constructor<WebSettings> declaredConstructor;
        if (userAgent == null) {
            try {
                declaredConstructor = WebSettings.class.getDeclaredConstructor(Context.class, WebView.class);
                declaredConstructor.setAccessible(true);
                userAgent = declaredConstructor.newInstance(context, null).getUserAgentString();
                declaredConstructor.setAccessible(false);
            } catch (Exception e) {
                userAgent = new WebView(context).getSettings().getUserAgentString();
            } catch (Throwable th) {
                declaredConstructor.setAccessible(false);
                throw th;
            }
        }
        return userAgent;
    }

    public static synchronized String id(Context context) {
        String str;
        synchronized (Utils.class) {
            if (sID == null) {
                File file = new File(context.getFilesDir(), INSTALLATION);
                try {
                    if (!file.exists()) {
                        writeInstallationFile(file);
                    }
                    sID = readInstallationFile(file);
                } catch (Exception e) {
                    sID = "1234567890";
                }
            }
            str = sID;
        }
        return str;
    }

    private static String readInstallationFile(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        byte[] bArr = new byte[((int) randomAccessFile.length())];
        randomAccessFile.readFully(bArr);
        randomAccessFile.close();
        return new String(bArr);
    }

    private static void writeInstallationFile(File file) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(UUID.randomUUID().toString().getBytes());
        fileOutputStream.close();
    }

    public static String getDeviceId(Context context) {
        try {
            String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (deviceId == null) {
                return id(context);
            }
            return deviceId;
        } catch (SecurityException e) {
            return NetworkManager.TYPE_UNKNOWN;
        }
    }

    public static String getDeviceIdMD5(Context context) {
        String deviceId = getDeviceId(context);
        if (deviceId != NetworkManager.TYPE_UNKNOWN) {
            return md5(deviceId);
        }
        return deviceId;
    }

    public static String getCarrier(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
    }
}
