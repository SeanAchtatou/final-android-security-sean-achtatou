package com.tapit.adview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class LockedOrientationDialog extends Dialog {
    /* access modifiers changed from: private */
    public DialogInterface.OnDismissListener unlockListener = null;

    public LockedOrientationDialog(Context context) {
        super(context);
        lockOrientation(context);
    }

    public LockedOrientationDialog(Context context, int i) {
        super(context, i);
        lockOrientation(context);
    }

    protected LockedOrientationDialog(Context context, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z, onCancelListener);
        lockOrientation(context);
    }

    public void setOnDismissListener(final DialogInterface.OnDismissListener onDismissListener) {
        if (this.unlockListener != null) {
            super.setOnDismissListener(new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    onDismissListener.onDismiss(dialogInterface);
                    LockedOrientationDialog.this.unlockListener.onDismiss(dialogInterface);
                }
            });
        } else {
            super.setOnDismissListener(onDismissListener);
        }
    }

    private void lockOrientation(Context context) {
        if (context instanceof Activity) {
            final Activity activity = (Activity) context;
            if (activity.getRequestedOrientation() == -1) {
                switch (context.getResources().getConfiguration().orientation) {
                    case 1:
                        activity.setRequestedOrientation(1);
                        break;
                    case 2:
                        activity.setRequestedOrientation(0);
                        break;
                }
                this.unlockListener = new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialogInterface) {
                        activity.setRequestedOrientation(-1);
                    }
                };
                setOnDismissListener(this.unlockListener);
            }
        }
    }
}
