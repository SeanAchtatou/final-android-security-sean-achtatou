package com.tapit.mediation.admob;

import android.app.Activity;
import android.view.View;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.inmobi.androidsdk.impl.IMAdException;
import com.tapit.adview.AdInterstitialBaseView;
import com.tapit.adview.AdInterstitialView;
import com.tapit.adview.AdView;
import com.tapit.adview.AdViewCore;
import java.util.HashMap;

public final class AdMobAdapter implements MediationBannerAdapter<AdMobAdapterExtras, AdMobAdapterServerParameters>, MediationInterstitialAdapter<AdMobAdapterExtras, AdMobAdapterServerParameters>, AdViewCore.OnAdDownload, AdViewCore.OnInterstitialAdDownload {
    private static final String CLIENT_STRING = "admob-mediation-1.0.0";
    private AdView bannerAd;
    private MediationBannerListener bannerListener;
    private AdInterstitialBaseView interstitialAd;
    private MediationInterstitialListener interstitialListener;

    public Class<AdMobAdapterExtras> getAdditionalParametersType() {
        return AdMobAdapterExtras.class;
    }

    public Class<AdMobAdapterServerParameters> getServerParametersType() {
        return AdMobAdapterServerParameters.class;
    }

    public void requestBannerAd(MediationBannerListener mediationBannerListener, Activity activity, AdMobAdapterServerParameters adMobAdapterServerParameters, AdSize adSize, MediationAdRequest mediationAdRequest, AdMobAdapterExtras adMobAdapterExtras) {
        this.bannerListener = mediationBannerListener;
        AdSize findBestSize = adSize.findBestSize(new AdSize(320, 50), new AdSize(IMAdException.INVALID_REQUEST, 50), new AdSize(216, 36), new AdSize(168, 28), new AdSize(120, 20), new AdSize(IMAdException.INVALID_REQUEST, 250), new AdSize(728, 90));
        if (findBestSize == null) {
            mediationBannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INVALID_REQUEST);
            return;
        }
        this.bannerAd = new AdView(activity, adMobAdapterServerParameters.zoneId);
        HashMap hashMap = new HashMap();
        hashMap.put("client", CLIENT_STRING);
        hashMap.put("h", Integer.toString(findBestSize.getHeight()));
        hashMap.put("w", Integer.toString(findBestSize.getWidth()));
        this.bannerAd.setCustomParameters(hashMap);
        this.bannerAd.setOnAdDownload(this);
        this.bannerAd.setUpdateTime(Integer.MAX_VALUE);
        this.bannerAd.update(true);
    }

    public void requestInterstitialAd(MediationInterstitialListener mediationInterstitialListener, Activity activity, AdMobAdapterServerParameters adMobAdapterServerParameters, MediationAdRequest mediationAdRequest, AdMobAdapterExtras adMobAdapterExtras) {
        this.interstitialListener = mediationInterstitialListener;
        this.interstitialAd = new AdInterstitialView(activity, adMobAdapterServerParameters.zoneId);
        HashMap hashMap = new HashMap();
        hashMap.put("client", CLIENT_STRING);
        this.interstitialAd.setCustomParameters(hashMap);
        this.interstitialAd.setOnInterstitialAdDownload(this);
        this.interstitialAd.load();
    }

    public void showInterstitial() {
        this.interstitialAd.showInterstitial();
    }

    public void destroy() {
        this.bannerListener = null;
        this.interstitialListener = null;
        this.bannerAd = null;
        this.interstitialAd = null;
    }

    public View getBannerView() {
        return this.bannerAd;
    }

    public void begin(AdViewCore adViewCore) {
    }

    public void end(AdViewCore adViewCore) {
        if (this.bannerListener != null && this.bannerAd.getParent() == null) {
            this.bannerListener.onReceivedAd(this);
        }
    }

    public void error(AdViewCore adViewCore, String str) {
        AdRequest.ErrorCode errorCode;
        if ("No available creatives".equals(str)) {
            errorCode = AdRequest.ErrorCode.NO_FILL;
        } else {
            errorCode = AdRequest.ErrorCode.INTERNAL_ERROR;
        }
        if (adViewCore == this.bannerAd) {
            if (this.bannerListener != null) {
                this.bannerListener.onFailedToReceiveAd(this, errorCode);
            }
        } else if (this.interstitialListener != null) {
            this.interstitialListener.onFailedToReceiveAd(this, errorCode);
        }
    }

    public void clicked(AdViewCore adViewCore) {
        if (adViewCore == this.bannerAd && this.bannerListener != null) {
            this.bannerListener.onClick(this);
        }
    }

    public void willPresentFullScreen(AdViewCore adViewCore) {
        if (this.bannerListener != null) {
            this.bannerListener.onPresentScreen(this);
        }
    }

    public void didPresentFullScreen(AdViewCore adViewCore) {
    }

    public void willDismissFullScreen(AdViewCore adViewCore) {
        if (this.bannerListener != null) {
            this.bannerListener.onDismissScreen(this);
        }
    }

    public void willLeaveApplication(AdViewCore adViewCore) {
        if (adViewCore == this.bannerAd && this.bannerListener != null) {
            this.bannerListener.onLeaveApplication(this);
        } else if (adViewCore == this.interstitialAd && this.interstitialListener != null) {
            this.interstitialListener.onLeaveApplication(this);
        }
    }

    public void willLoad(AdViewCore adViewCore) {
    }

    public void ready(AdViewCore adViewCore) {
        if (this.interstitialListener != null) {
            this.interstitialListener.onReceivedAd(this);
        }
    }

    public void willOpen(AdViewCore adViewCore) {
        if (this.interstitialListener != null) {
            this.interstitialListener.onPresentScreen(this);
        }
    }

    public void didClose(AdViewCore adViewCore) {
        if (this.interstitialListener != null) {
            this.interstitialListener.onDismissScreen(this);
        }
    }
}
