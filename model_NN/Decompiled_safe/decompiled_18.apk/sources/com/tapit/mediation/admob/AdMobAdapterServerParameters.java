package com.tapit.mediation.admob;

import com.google.ads.mediation.MediationServerParameters;

public class AdMobAdapterServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "zoneid")
    public String zoneId;
}
