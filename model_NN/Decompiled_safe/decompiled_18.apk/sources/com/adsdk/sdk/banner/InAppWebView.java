package com.adsdk.sdk.banner;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.adsdk.sdk.Const;
import com.adsdk.sdk.Log;

public class InAppWebView extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        String url = (String) getIntent().getExtras().get(Const.REDIRECT_URI);
        if (url == null) {
            Log.d(Const.TAG, "url is null so do not load anything");
            return;
        }
        WebView webView = new WebView(this);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }
        });
        webView.loadUrl(url);
        setContentView(webView);
    }
}
