package com.adsdk.sdk.banner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.ViewFlipper;
import com.adfonic.android.utils.HtmlFormatter;
import com.adsdk.sdk.AdListener;
import com.adsdk.sdk.AdRequest;
import com.adsdk.sdk.BannerAd;
import com.adsdk.sdk.Const;
import com.adsdk.sdk.Log;
import com.adsdk.sdk.RequestBannerAd;
import com.adsdk.sdk.Util;
import com.adsdk.sdk.data.ClickType;
import java.io.InputStream;
import java.lang.Thread;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Timer;

public class AdView extends RelativeLayout {
    public static final int LIVE = 0;
    public static final int TEST = 1;
    private static Field mWebView_LAYER_TYPE_SOFTWARE;
    private static Method mWebView_SetLayerType;
    /* access modifiers changed from: private */
    public AdListener adListener;
    private boolean animation;
    private Animation fadeInAnimation;
    private Animation fadeOutAnimation;
    private WebView firstWebView;
    private boolean includeLocation;
    private int isAccessCoarseLocation;
    private int isAccessFineLocation;
    private boolean isInternalBrowser;
    /* access modifiers changed from: private */
    public Thread loadContentThread;
    private LocationManager locationManager;
    private Context mContext;
    protected boolean mIsInForeground;
    private BroadcastReceiver mScreenStateReceiver;
    private final View.OnTouchListener onTouchListener;
    private String publisherId;
    private Timer reloadTimer;
    private AdRequest request;
    private String requestURL;
    /* access modifiers changed from: private */
    public BannerAd response;
    private WebView secondWebView;
    /* access modifiers changed from: private */
    public final Runnable showContent;
    private int telephonyPermission;
    /* access modifiers changed from: private */
    public boolean touchMove;
    /* access modifiers changed from: private */
    public final Handler updateHandler;
    private ViewFlipper viewFlipper;
    private WebSettings webSettings;
    /* access modifiers changed from: private */
    public InputStream xml;

    public void setWidth(int width) {
    }

    public void setHeight(int width) {
    }

    public AdView(Context context, AttributeSet attributes) {
        super(context, attributes);
        this.includeLocation = false;
        this.isInternalBrowser = false;
        this.fadeInAnimation = null;
        this.fadeOutAnimation = null;
        this.requestURL = null;
        this.mContext = null;
        this.updateHandler = new Handler();
        this.showContent = new Runnable() {
            public void run() {
                AdView.this.showContent();
            }
        };
        this.onTouchListener = new View.OnTouchListener() {
            private float distanceX;
            private float distanceY;

            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (event.getAction() == 0) {
                        AdView.this.touchMove = false;
                        this.distanceX = event.getX();
                        this.distanceY = event.getY();
                    }
                    if (event.getAction() == 2) {
                        if (Math.abs(this.distanceX - event.getX()) > 30.0f) {
                            AdView.this.touchMove = true;
                        }
                        if (Math.abs(this.distanceY - event.getY()) > 30.0f) {
                            AdView.this.touchMove = true;
                        }
                        Log.d(Const.TAG, "touchMove: " + AdView.this.touchMove);
                        return true;
                    }
                    if (event.getAction() == 1) {
                        Log.d(Const.TAG, "size x: " + event.getX());
                        Log.d(Const.TAG, "getHistorySize: " + event.getHistorySize());
                        if (AdView.this.response != null && !AdView.this.touchMove) {
                            AdView.this.openLink();
                            AdView.this.notifyAdClicked();
                        }
                    }
                    return AdView.this.onTouchEvent(event);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        };
        this.mContext = context;
        if (attributes != null) {
            int count = attributes.getAttributeCount();
            for (int i = 0; i < count; i++) {
                String name = attributes.getAttributeName(i);
                if (name.equals("publisherId")) {
                    this.publisherId = attributes.getAttributeValue(i);
                } else if (name.equals("request_url")) {
                    this.requestURL = attributes.getAttributeValue(i);
                } else if (name.equals("animation")) {
                    this.animation = attributes.getAttributeBooleanValue(i, false);
                } else if (name.equals("includeLocation")) {
                    this.includeLocation = attributes.getAttributeBooleanValue(i, false);
                }
            }
        }
        initialize(context);
    }

    public AdView(Context context, String requestURL2, String publisherId2) {
        this(context, requestURL2, publisherId2, false, false);
    }

    public AdView(Context context, String requestURL2, InputStream xml2, String publisherId2, boolean includeLocation2, boolean animation2) {
        this(context, xml2, requestURL2, publisherId2, includeLocation2, animation2);
    }

    public AdView(Context context, InputStream xml2, String requestURL2, String publisherId2, boolean includeLocation2, boolean animation2) {
        super(context);
        this.includeLocation = false;
        this.isInternalBrowser = false;
        this.fadeInAnimation = null;
        this.fadeOutAnimation = null;
        this.requestURL = null;
        this.mContext = null;
        this.updateHandler = new Handler();
        this.showContent = new Runnable() {
            public void run() {
                AdView.this.showContent();
            }
        };
        this.onTouchListener = new View.OnTouchListener() {
            private float distanceX;
            private float distanceY;

            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (event.getAction() == 0) {
                        AdView.this.touchMove = false;
                        this.distanceX = event.getX();
                        this.distanceY = event.getY();
                    }
                    if (event.getAction() == 2) {
                        if (Math.abs(this.distanceX - event.getX()) > 30.0f) {
                            AdView.this.touchMove = true;
                        }
                        if (Math.abs(this.distanceY - event.getY()) > 30.0f) {
                            AdView.this.touchMove = true;
                        }
                        Log.d(Const.TAG, "touchMove: " + AdView.this.touchMove);
                        return true;
                    }
                    if (event.getAction() == 1) {
                        Log.d(Const.TAG, "size x: " + event.getX());
                        Log.d(Const.TAG, "getHistorySize: " + event.getHistorySize());
                        if (AdView.this.response != null && !AdView.this.touchMove) {
                            AdView.this.openLink();
                            AdView.this.notifyAdClicked();
                        }
                    }
                    return AdView.this.onTouchEvent(event);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        };
        this.xml = xml2;
        this.requestURL = requestURL2;
        this.mContext = context;
        this.publisherId = publisherId2;
        this.includeLocation = includeLocation2;
        this.animation = animation2;
        initialize(context);
    }

    public AdView(Context context, String requestURL2, String publisherId2, boolean includeLocation2, boolean animation2) {
        super(context);
        this.includeLocation = false;
        this.isInternalBrowser = false;
        this.fadeInAnimation = null;
        this.fadeOutAnimation = null;
        this.requestURL = null;
        this.mContext = null;
        this.updateHandler = new Handler();
        this.showContent = new Runnable() {
            public void run() {
                AdView.this.showContent();
            }
        };
        this.onTouchListener = new View.OnTouchListener() {
            private float distanceX;
            private float distanceY;

            public boolean onTouch(View v, MotionEvent event) {
                try {
                    if (event.getAction() == 0) {
                        AdView.this.touchMove = false;
                        this.distanceX = event.getX();
                        this.distanceY = event.getY();
                    }
                    if (event.getAction() == 2) {
                        if (Math.abs(this.distanceX - event.getX()) > 30.0f) {
                            AdView.this.touchMove = true;
                        }
                        if (Math.abs(this.distanceY - event.getY()) > 30.0f) {
                            AdView.this.touchMove = true;
                        }
                        Log.d(Const.TAG, "touchMove: " + AdView.this.touchMove);
                        return true;
                    }
                    if (event.getAction() == 1) {
                        Log.d(Const.TAG, "size x: " + event.getX());
                        Log.d(Const.TAG, "getHistorySize: " + event.getHistorySize());
                        if (AdView.this.response != null && !AdView.this.touchMove) {
                            AdView.this.openLink();
                            AdView.this.notifyAdClicked();
                        }
                    }
                    return AdView.this.onTouchEvent(event);
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        };
        this.requestURL = requestURL2;
        this.mContext = context;
        this.publisherId = publisherId2;
        this.includeLocation = includeLocation2;
        this.animation = animation2;
        initialize(context);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        IntentFilter filter = new IntentFilter("android.intent.action.SCREEN_OFF");
        filter.addAction("android.intent.action.USER_PRESENT");
        this.mContext.registerReceiver(this.mScreenStateReceiver, filter);
        Log.v(Const.TAG, "onAttachedToWindow");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mContext.unregisterReceiver(this.mScreenStateReceiver);
        Log.v(Const.TAG, "onDetachedFromWindow");
    }

    private WebView createWebView(Context context) {
        WebView webView = new WebView(getContext()) {
            public void draw(Canvas canvas) {
                if (getWidth() > 0 && getHeight() > 0) {
                    super.draw(canvas);
                }
            }
        };
        this.webSettings = webView.getSettings();
        this.webSettings.setJavaScriptEnabled(true);
        webView.setBackgroundColor(0);
        setLayer(webView);
        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        return webView;
    }

    private void doOpenUrl(String url) {
        if (this.response.getClickType() == null || !this.response.getClickType().equals(ClickType.INAPP)) {
            getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
            return;
        }
        Intent intent = new Intent(getContext(), InAppWebView.class);
        intent.putExtra(Const.REDIRECT_URI, this.response.getClickUrl());
        getContext().startActivity(intent);
    }

    private Location getLocation() {
        if (this.locationManager != null) {
            if (this.isAccessFineLocation == 0 && this.locationManager.isProviderEnabled("gps")) {
                return this.locationManager.getLastKnownLocation("gps");
            }
            if (this.isAccessCoarseLocation == 0 && this.locationManager.isProviderEnabled("network")) {
                return this.locationManager.getLastKnownLocation("network");
            }
        }
        return null;
    }

    public int getRefreshRate() {
        if (this.response != null) {
            return this.response.getRefresh();
        }
        return -1;
    }

    static {
        initCompatibility();
    }

    private static void initCompatibility() {
        try {
            Method[] methods = WebView.class.getMethods();
            int length = methods.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Method m = methods[i];
                if (m.getName().equals("setLayerType")) {
                    mWebView_SetLayerType = m;
                    break;
                }
                i++;
            }
            Log.v("set layer " + mWebView_SetLayerType);
            mWebView_LAYER_TYPE_SOFTWARE = WebView.class.getField("LAYER_TYPE_SOFTWARE");
            Log.v("set1 layer " + mWebView_LAYER_TYPE_SOFTWARE);
        } catch (SecurityException e) {
            Log.v("SecurityException");
        } catch (NoSuchFieldException e2) {
            Log.v("NoSuchFieldException");
        }
    }

    private static void setLayer(WebView webView) {
        if (mWebView_SetLayerType == null || mWebView_LAYER_TYPE_SOFTWARE == null) {
            Log.v("Set Layer is not supported");
            return;
        }
        try {
            Log.v("Set Layer is supported");
            mWebView_SetLayerType.invoke(webView, Integer.valueOf(mWebView_LAYER_TYPE_SOFTWARE.getInt(WebView.class)), null);
        } catch (InvocationTargetException e) {
            Log.v("Set InvocationTargetException");
        } catch (IllegalArgumentException e2) {
            Log.v("Set IllegalArgumentException");
        } catch (IllegalAccessException e3) {
            Log.v("Set IllegalAccessException");
        }
    }

    /* access modifiers changed from: private */
    public AdRequest getRequest() {
        if (this.request == null) {
            this.request = new AdRequest();
            if (this.telephonyPermission == 0) {
                this.request.setDeviceId(((TelephonyManager) getContext().getSystemService("phone")).getDeviceId());
                this.request.setProtocolVersion(Const.PROTOCOL_VERSION);
            } else {
                this.request.setDeviceId(Util.getDeviceId(this.mContext));
                this.request.setProtocolVersion("N1.0");
            }
            this.request.setPublisherId(this.publisherId);
            this.request.setUserAgent(this.webSettings.getUserAgentString());
            this.request.setUserAgent2(Util.buildUserAgent());
            Log.d(Const.TAG, "WebKit UserAgent:" + this.request.getUserAgent());
            Log.d(Const.TAG, "SDK built UserAgent:" + this.request.getUserAgent2());
        }
        Location location = null;
        if (this.includeLocation) {
            location = getLocation();
        }
        if (location != null) {
            Log.d(Const.TAG, "location is longitude: " + location.getLongitude() + ", latitude: " + location.getLatitude());
            this.request.setLatitude(location.getLatitude());
            this.request.setLongitude(location.getLongitude());
        } else {
            this.request.setLatitude(0.0d);
            this.request.setLongitude(0.0d);
        }
        this.request.setType(0);
        this.request.setRequestURL(this.requestURL);
        return this.request;
    }

    private void initialize(Context context) {
        Log.LOGGING_ENABLED = Log.isLoggingEnabled(this.mContext);
        initCompatibility();
        Log.d(Const.TAG, "SDK Version:4.0");
        registerScreenStateBroadcastReceiver();
        this.locationManager = null;
        this.telephonyPermission = context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE");
        this.isAccessFineLocation = context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        this.isAccessCoarseLocation = context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
        if (this.isAccessFineLocation == 0 || this.isAccessCoarseLocation == 0) {
            this.locationManager = (LocationManager) getContext().getSystemService("location");
        }
        this.firstWebView = createWebView(context);
        this.secondWebView = createWebView(context);
        Log.d(Const.TAG, "Create view flipper");
        this.viewFlipper = new ViewFlipper(getContext()) {
            /* access modifiers changed from: protected */
            public void onDetachedFromWindow() {
                try {
                    super.onDetachedFromWindow();
                } catch (IllegalArgumentException e) {
                    stopFlipping();
                }
            }
        };
        float scale = context.getResources().getDisplayMetrics().density;
        setLayoutParams(new RelativeLayout.LayoutParams((int) ((300.0f * scale) + 0.5f), (int) ((50.0f * scale) + 0.5f)));
        FrameLayout.LayoutParams webViewParams = new FrameLayout.LayoutParams(-1, -1);
        this.viewFlipper.addView(this.firstWebView, webViewParams);
        this.viewFlipper.addView(this.secondWebView, webViewParams);
        addView(this.viewFlipper, new RelativeLayout.LayoutParams(-1, -1));
        this.firstWebView.setOnTouchListener(this.onTouchListener);
        this.secondWebView.setOnTouchListener(this.onTouchListener);
        Log.d(Const.TAG, "animation: " + this.animation);
        if (this.animation) {
            this.fadeInAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
            this.fadeInAnimation.setDuration(1000);
            this.fadeOutAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
            this.fadeOutAnimation.setDuration(1000);
            this.viewFlipper.setInAnimation(this.fadeInAnimation);
            this.viewFlipper.setOutAnimation(this.fadeOutAnimation);
        }
    }

    public boolean isInternalBrowser() {
        return this.isInternalBrowser;
    }

    private void registerScreenStateBroadcastReceiver() {
        this.mScreenStateReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.intent.action.SCREEN_OFF")) {
                    if (AdView.this.mIsInForeground) {
                        Log.d(Const.TAG, "Screen sleep with ad in foreground, disable refresh");
                        AdView.this.pause();
                        return;
                    }
                    Log.d(Const.TAG, "Screen sleep but ad in background; refresh should already be disabled");
                } else if (!intent.getAction().equals("android.intent.action.USER_PRESENT")) {
                } else {
                    if (AdView.this.mIsInForeground) {
                        AdView.this.resume();
                        Log.d(Const.TAG, "Screen wake / ad in foreground, reset refresh");
                        return;
                    }
                    Log.d(Const.TAG, "Screen wake but ad in background; don't enable refresh");
                }
            }
        };
        IntentFilter filter = new IntentFilter("android.intent.action.SCREEN_OFF");
        filter.addAction("android.intent.action.USER_PRESENT");
        this.mContext.registerReceiver(this.mScreenStateReceiver, filter);
    }

    private void loadContent() {
        Log.d(Const.TAG, "load content");
        if (this.loadContentThread == null) {
            this.loadContentThread = new Thread(new Runnable() {
                public void run() {
                    RequestBannerAd requestAd;
                    Log.d(Const.TAG, "starting request thread");
                    if (AdView.this.xml == null) {
                        requestAd = new RequestBannerAd();
                    } else {
                        requestAd = new RequestBannerAd(AdView.this.xml);
                    }
                    try {
                        AdView.this.response = (BannerAd) requestAd.sendRequest(AdView.this.getRequest());
                        if (AdView.this.response != null) {
                            Log.d(Const.TAG, "response received");
                            Log.d(Const.TAG, "getVisibility: " + AdView.this.getVisibility());
                            AdView.this.updateHandler.post(AdView.this.showContent);
                        }
                    } catch (Throwable e) {
                        AdView.this.notifyLoadAdFailed(e);
                    }
                    AdView.this.loadContentThread = null;
                    Log.d(Const.TAG, "finishing request thread");
                }
            });
            this.loadContentThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable ex) {
                    Log.e(Const.TAG, "Exception in request thread", ex);
                    AdView.this.loadContentThread = null;
                }
            });
            this.loadContentThread.start();
        }
    }

    public void loadNextAd() {
        Log.d(Const.TAG, "load next ad");
        loadContent();
    }

    /* access modifiers changed from: private */
    public void notifyAdClicked() {
        this.updateHandler.post(new Runnable() {
            public void run() {
                if (AdView.this.adListener != null) {
                    Log.d(Const.TAG, "notify bannerListener of ad clicked: " + AdView.this.adListener.getClass().getName());
                    AdView.this.adListener.adClicked();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void notifyLoadAdFailed(final Throwable e) {
        this.updateHandler.post(new Runnable() {
            public void run() {
                Log.e(Const.TAG, "Exception in request thread", e);
                if (AdView.this.adListener != null) {
                    Log.d(Const.TAG, "notify bannerListener: " + AdView.this.adListener.getClass().getName());
                    AdView.this.adListener.noAdFound();
                }
            }
        });
    }

    private void notifyLoadAdSucceeded() {
        this.updateHandler.post(new Runnable() {
            public void run() {
                if (AdView.this.adListener != null) {
                    Log.d(Const.TAG, "notify bannerListener of load succeeded: " + AdView.this.adListener.getClass().getName());
                    AdView.this.adListener.adLoadSucceeded(null);
                }
            }
        });
    }

    private void notifyNoAd() {
        this.updateHandler.post(new Runnable() {
            public void run() {
                Log.d(Const.TAG, "No Ad");
                if (AdView.this.adListener != null) {
                    AdView.this.adListener.noAdFound();
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);
        if (visibility == 0) {
            this.mIsInForeground = true;
            resume();
        } else {
            this.mIsInForeground = false;
            pause();
        }
        Log.d(Const.TAG, "onWindowVisibilityChanged: " + visibility);
    }

    /* access modifiers changed from: private */
    public void openLink() {
        if (this.response != null && this.response.getClickUrl() != null) {
            doOpenUrl(this.response.getClickUrl());
        }
    }

    public void pause() {
        if (this.reloadTimer != null) {
            try {
                Log.d(Const.TAG, "cancel reload timer");
                this.reloadTimer.cancel();
                this.reloadTimer = null;
            } catch (Exception e) {
                Log.e(Const.TAG, "unable to cancel reloadTimer", e);
            }
        }
    }

    public void resume() {
        if (this.reloadTimer != null) {
            this.reloadTimer.cancel();
            this.reloadTimer = null;
        }
        this.reloadTimer = new Timer();
        Log.d(Const.TAG, "response: " + this.response);
        if (this.response == null || this.response.getRefresh() <= 0) {
            loadContent();
        } else {
            startReloadTimer();
        }
    }

    public void setAdListener(AdListener bannerListener) {
        this.adListener = bannerListener;
    }

    public void setInternalBrowser(boolean isInternalBrowser2) {
        this.isInternalBrowser = isInternalBrowser2;
    }

    /* access modifiers changed from: private */
    public void showContent() {
        WebView webView;
        try {
            if (this.viewFlipper.getCurrentView() == this.firstWebView) {
                webView = this.secondWebView;
            } else {
                webView = this.firstWebView;
            }
            if (this.response.getType() == 0) {
                String text = MessageFormat.format(Const.IMAGE_BODY, this.response.getImageUrl(), Integer.valueOf(this.response.getBannerWidth()), Integer.valueOf(this.response.getBannerHeight()));
                Log.d(Const.TAG, "set image: " + text);
                webView.loadData(Uri.encode(Const.HIDE_BORDER + text), HtmlFormatter.TEXT_HTML, Const.ENCODING);
                notifyLoadAdSucceeded();
            } else if (this.response.getType() == 1) {
                String text2 = Uri.encode(Const.HIDE_BORDER + this.response.getText());
                Log.d(Const.TAG, "set text: " + text2);
                webView.loadData(text2, HtmlFormatter.TEXT_HTML, Const.ENCODING);
                notifyLoadAdSucceeded();
            } else {
                notifyNoAd();
                return;
            }
            if (this.viewFlipper.getCurrentView() == this.firstWebView) {
                Log.d(Const.TAG, "show next");
                this.viewFlipper.showNext();
            } else {
                Log.d(Const.TAG, "show previous");
                this.viewFlipper.showPrevious();
            }
            startReloadTimer();
        } catch (Throwable t) {
            Log.e(Const.TAG, "Exception in show content", t);
        }
    }

    private void startReloadTimer() {
        Log.d(Const.TAG, "start reload timer");
        if (this.reloadTimer != null) {
            int refreshTime = this.response.getRefresh() * 1000;
            Log.d(Const.TAG, "set timer: " + refreshTime);
            this.reloadTimer.schedule(new ReloadTask(this), (long) refreshTime);
        }
    }
}
