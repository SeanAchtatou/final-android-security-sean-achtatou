package com.adsdk.sdk.video;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.adsdk.sdk.Const;
import com.adsdk.sdk.Log;
import com.inmobi.androidsdk.ai.controller.util.IMConfigException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.http.client.methods.HttpGet;

public class ResourceManager {
    static final /* synthetic */ boolean $assertionsDisabled = (!ResourceManager.class.desiredAssertionStatus());
    public static final String BACK_ICON = "browser_back.png";
    public static final String BOTTOMBAR_BG = "bar.png";
    public static final int DEFAULT_BACK_IMAGE_RESOURCE_ID = -14;
    public static final int DEFAULT_BOTTOMBAR_BG_RESOURCE_ID = -2;
    public static final int DEFAULT_EXTERNAL_IMAGE_RESOURCE_ID = -17;
    public static final int DEFAULT_FORWARD_IMAGE_RESOURCE_ID = -15;
    public static final int DEFAULT_PAUSE_IMAGE_RESOURCE_ID = -12;
    public static final int DEFAULT_PLAY_IMAGE_RESOURCE_ID = -11;
    public static final int DEFAULT_RELOAD_IMAGE_RESOURCE_ID = -16;
    public static final int DEFAULT_REPLAY_IMAGE_RESOURCE_ID = -13;
    public static final int DEFAULT_SKIP_IMAGE_RESOURCE_ID = -18;
    public static final int DEFAULT_TOPBAR_BG_RESOURCE_ID = -1;
    public static final String EXTERNAL_ICON = "browser_external.png";
    public static final String FORWARD_ICON = "browser_forward.png";
    public static final String PAUSE_ICON = "video_pause.png";
    public static final String PLAY_ICON = "video_play.png";
    public static final String RELOAD_ICON = "video_replay.png";
    public static final String REPLAY_ICON = "video_replay.png";
    public static final int RESOURCE_LOADED_MSG = 100;
    public static final String SKIP_ICON = "skip.png";
    public static final String TOPBAR_BG = "bar.png";
    public static final int TYPE_FILE = 0;
    public static final int TYPE_UNKNOWN = -1;
    public static final int TYPE_ZIP = 1;
    public static final String VERSION = "version.txt";
    public static boolean sCancel = $assertionsDisabled;
    public static HttpGet sDownloadGet;
    public static boolean sDownloading = $assertionsDisabled;
    private static HashMap<Integer, Drawable> sResources = new HashMap<>();
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public HashMap<Integer, Drawable> mResources = new HashMap<>();

    public static Drawable getDefaultResource(int resId) {
        return sResources.get(Integer.valueOf(resId));
    }

    public static Drawable getDefaultSkipButton(Context ctx) {
        return buildDrawable(ctx, SKIP_ICON);
    }

    public static boolean resourcesInstalled(Context ctx) {
        String[] files = ctx.fileList();
        for (String equals : files) {
            if (VERSION.equals(equals)) {
                Log.d("Resources already installed");
                return true;
            }
        }
        return $assertionsDisabled;
    }

    public static long getInstalledVersion(Context ctx) {
        long result = -1;
        FileInputStream in = null;
        try {
            in = ctx.openFileInput(VERSION);
            result = Long.valueOf(new BufferedReader(new InputStreamReader(in, Const.ENCODING)).readLine()).longValue();
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        Log.d("Resources installed version:" + result);
        return result;
    }

    public static void saveInstalledVersion(Context ctx, long version) {
        FileOutputStream out = null;
        try {
            out = ctx.openFileOutput(VERSION, 0);
            OutputStreamWriter osr = new OutputStreamWriter(out, Const.ENCODING);
            osr.write(String.valueOf(version));
            osr.flush();
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
    }

    public void releaseInstance() {
        Iterator<Map.Entry<Integer, Drawable>> it = this.mResources.entrySet().iterator();
        while (it.hasNext()) {
            it.remove();
            BitmapDrawable bitmapDrawable = (BitmapDrawable) it.next().getValue();
        }
        if ($assertionsDisabled || this.mResources.size() == 0) {
            System.gc();
            return;
        }
        throw new AssertionError();
    }

    private static void initDefaultResource(Context ctx, int resource) {
        switch (resource) {
            case DEFAULT_SKIP_IMAGE_RESOURCE_ID /*-18*/:
                registerImageResource(ctx, -18, SKIP_ICON);
                return;
            case DEFAULT_EXTERNAL_IMAGE_RESOURCE_ID /*-17*/:
                registerImageResource(ctx, -17, EXTERNAL_ICON);
                return;
            case DEFAULT_RELOAD_IMAGE_RESOURCE_ID /*-16*/:
                registerImageResource(ctx, -16, "video_replay.png");
                return;
            case DEFAULT_FORWARD_IMAGE_RESOURCE_ID /*-15*/:
                registerImageResource(ctx, -15, FORWARD_ICON);
                return;
            case DEFAULT_BACK_IMAGE_RESOURCE_ID /*-14*/:
                registerImageResource(ctx, -14, BACK_ICON);
                return;
            case DEFAULT_REPLAY_IMAGE_RESOURCE_ID /*-13*/:
                registerImageResource(ctx, -13, "video_replay.png");
                return;
            case DEFAULT_PAUSE_IMAGE_RESOURCE_ID /*-12*/:
                registerImageResource(ctx, -12, PAUSE_ICON);
                return;
            case DEFAULT_PLAY_IMAGE_RESOURCE_ID /*-11*/:
                registerImageResource(ctx, -11, PLAY_ICON);
                return;
            case -10:
            case -9:
            case IMConfigException.MISSING_CONFIG_SMALLEST_SCREENSIZE /*-8*/:
            case IMConfigException.MISSING_CONFIG_SCREENSIZE /*-7*/:
            case IMConfigException.MISSING_CONFIG_ORIENTATION /*-6*/:
            case IMConfigException.MISSING_CONFIG_KEYBOARDHIDDEN /*-5*/:
            case IMConfigException.MISSING_CONFIG_KEYBOARD /*-4*/:
            case IMConfigException.MISSING_CONFIG_CHANGES /*-3*/:
            default:
                return;
            case -2:
                registerImageResource(ctx, -2, "bar.png");
                return;
            case -1:
                registerImageResource(ctx, -1, "bar.png");
                return;
        }
    }

    private static void registerImageResource(Context ctx, int resId, String name) {
        Drawable d = buildDrawable(ctx, name);
        if (d != null) {
            sResources.put(Integer.valueOf(resId), d);
        } else {
            Log.i("registerImageResource", "drawable was null " + name);
        }
    }

    private static Drawable buildDrawable(Context ctx, String name) {
        InputStream in = null;
        try {
            InputStream in2 = ctx.getClass().getClassLoader().getResourceAsStream("defaultresources/" + name);
            Bitmap b = BitmapFactory.decodeStream(in2);
            if (b != null) {
                DisplayMetrics m = ctx.getResources().getDisplayMetrics();
                int w = b.getWidth();
                int h = b.getHeight();
                int imageWidth = (int) TypedValue.applyDimension(1, (float) w, m);
                int imageHeight = (int) TypedValue.applyDimension(1, (float) h, m);
                if (!(imageWidth == w && imageHeight == h)) {
                    b = Bitmap.createScaledBitmap(b, imageWidth, imageHeight, $assertionsDisabled);
                }
                BitmapDrawable bitmapDrawable = new BitmapDrawable(ctx.getResources(), b);
                if (in2 == null) {
                    return bitmapDrawable;
                }
                try {
                    in2.close();
                    return bitmapDrawable;
                } catch (Exception e) {
                    return bitmapDrawable;
                }
            } else {
                if (in2 != null) {
                    try {
                        in2.close();
                    } catch (Exception e2) {
                    }
                }
                return null;
            }
        } catch (Exception e3) {
            Log.i("ResourceManager cannot find resource " + name);
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e5) {
                }
            }
            throw th;
        }
    }

    public static boolean isDownloading() {
        return sDownloading;
    }

    public static void cancel() {
        sCancel = true;
        if (sDownloadGet != null) {
            sDownloadGet.abort();
            sDownloadGet = null;
        }
        sResources.clear();
    }

    public ResourceManager(Context ctx, Handler h) {
        this.mHandler = h;
    }

    public void fetchResource(Context ctx, String url, int resourceId) {
        if (sResources.get(Integer.valueOf(resourceId)) == null) {
            new FetchImageTask(ctx, url, resourceId).execute(new Void[0]);
        }
    }

    public boolean containsResource(int resourceId) {
        if (this.mResources.get(Integer.valueOf(resourceId)) == null && this.mResources.get(Integer.valueOf(resourceId)) == null) {
            return $assertionsDisabled;
        }
        return true;
    }

    public Drawable getResource(Context ctx, int resourceId) {
        BitmapDrawable d = (BitmapDrawable) this.mResources.get(Integer.valueOf(resourceId));
        return d != null ? d : getStaticResource(ctx, resourceId);
    }

    public static Drawable getStaticResource(Context ctx, int resourceId) {
        BitmapDrawable d = (BitmapDrawable) sResources.get(Integer.valueOf(resourceId));
        if (d != null && !d.getBitmap().isRecycled()) {
            return d;
        }
        initDefaultResource(ctx, resourceId);
        return (BitmapDrawable) sResources.get(Integer.valueOf(resourceId));
    }

    private class FetchImageTask extends AsyncTask<Void, Void, Boolean> {
        Context mContext;
        int mResourceId;
        String mUrl;

        public FetchImageTask(Context ctx, String url, int resId) {
            this.mContext = ctx;
            this.mUrl = url;
            this.mResourceId = resId;
            Log.i("Fetching: " + this.mUrl);
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(Boolean result) {
            super.onPostExecute((Object) result);
            Log.i("Fetched: " + this.mUrl);
            ResourceManager.this.mHandler.sendMessage(ResourceManager.this.mHandler.obtainMessage(100, this.mResourceId, 0));
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Void... params) {
            Drawable d = null;
            if (this.mUrl != null && this.mUrl.length() > 0) {
                d = fetchImage(this.mUrl);
            }
            if (d == null) {
                return Boolean.valueOf((boolean) ResourceManager.$assertionsDisabled);
            }
            ResourceManager.this.mResources.put(Integer.valueOf(this.mResourceId), d);
            return true;
        }

        private Drawable fetchImage(String urlString) {
            try {
                Bitmap b = BitmapFactory.decodeStream((InputStream) new URL(urlString).getContent());
                if (b != null) {
                    DisplayMetrics m = this.mContext.getResources().getDisplayMetrics();
                    int w = b.getWidth();
                    int h = b.getHeight();
                    int imageWidth = (int) TypedValue.applyDimension(1, (float) w, m);
                    int imageHeight = (int) TypedValue.applyDimension(1, (float) h, m);
                    if (!(imageWidth == w && imageHeight == h)) {
                        b = Bitmap.createScaledBitmap(b, imageWidth, imageHeight, ResourceManager.$assertionsDisabled);
                    }
                    return new BitmapDrawable(this.mContext.getResources(), b);
                }
            } catch (Exception e) {
                Log.e("Cannot fetch image:" + urlString, e);
            }
            return null;
        }
    }
}
