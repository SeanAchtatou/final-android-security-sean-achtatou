package com.adsdk.sdk.video;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

public class AspectRatioImageView extends ImageView {
    private boolean mFill = false;
    private int mMaxH = -1;
    private int mMinW = -1;

    public AspectRatioImageView(Context context) {
        super(context);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AspectRatioImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void fillParent(boolean fill, int minWidthDip, int maxHeightDip) {
        this.mFill = fill;
        this.mMaxH = maxHeightDip;
        this.mMinW = minWidthDip;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height;
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        int size = View.MeasureSpec.getSize(heightMeasureSpec);
        if (!this.mFill || getDrawable() == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        int drawableH = getDrawable().getIntrinsicHeight();
        int drawableW = getDrawable().getIntrinsicWidth();
        if (drawableW > drawableH) {
            height = (width * drawableH) / drawableW;
        } else {
            height = width;
            width = (height * drawableW) / drawableH;
        }
        ensureConstraintMetAndSet(width, height, drawableW, drawableH);
    }

    /* access modifiers changed from: protected */
    public int getMeasuredHeight(int widthMeasureSpec, int heightMeasureSpec) {
        int height;
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        int size = View.MeasureSpec.getSize(heightMeasureSpec);
        if (!this.mFill || getDrawable() == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return 0;
        }
        int drawableH = getDrawable().getIntrinsicHeight();
        int drawableW = getDrawable().getIntrinsicWidth();
        if (drawableW > drawableH) {
            height = (width * drawableH) / drawableW;
        } else {
            height = width;
            width = (height * drawableW) / drawableH;
        }
        return getConstrainedHeight(width, height, drawableW, drawableH);
    }

    /* access modifiers changed from: package-private */
    public void ensureConstraintMetAndSet(int measuredWidth, int measuredHeight, int drawableW, int drawableH) {
        if (drawableW < drawableH) {
            if (this.mMinW > 0) {
                float minW = dip2pixel(this.mMinW, getContext());
                if (((float) measuredWidth) < minW) {
                    measuredWidth = (int) minW;
                    measuredHeight = (drawableH / drawableW) * measuredWidth;
                }
            }
            if (this.mMaxH > 0) {
                float maxH = dip2pixel(this.mMaxH, getContext());
                if (((float) measuredHeight) > maxH) {
                    measuredHeight = (int) maxH;
                    measuredWidth = (measuredHeight * drawableW) / drawableH;
                }
            }
        } else {
            if (this.mMaxH > 0) {
                float maxH2 = dip2pixel(this.mMaxH, getContext());
                if (((float) measuredHeight) > maxH2) {
                    measuredHeight = (int) maxH2;
                    measuredWidth = (measuredHeight * drawableW) / drawableH;
                }
            }
            if (this.mMinW > 0) {
                float minW2 = dip2pixel(this.mMinW, getContext());
                if (((float) measuredWidth) < minW2) {
                    measuredWidth = (int) minW2;
                    measuredHeight = (drawableH / drawableW) * measuredWidth;
                }
            }
        }
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    /* access modifiers changed from: package-private */
    public int getConstrainedHeight(int measuredWidth, int measuredHeight, int drawableW, int drawableH) {
        if (drawableW < drawableH) {
            if (this.mMinW > 0) {
                float minW = dip2pixel(this.mMinW, getContext());
                if (((float) measuredWidth) < minW) {
                    measuredHeight = (drawableH / drawableW) * ((int) minW);
                }
            }
            if (this.mMaxH <= 0) {
                return measuredHeight;
            }
            float maxH = dip2pixel(this.mMaxH, getContext());
            if (((float) measuredHeight) <= maxH) {
                return measuredHeight;
            }
            int measuredHeight2 = (int) maxH;
            int measuredWidth2 = (measuredHeight2 * drawableW) / drawableH;
            return measuredHeight2;
        }
        if (this.mMaxH > 0) {
            float maxH2 = dip2pixel(this.mMaxH, getContext());
            if (((float) measuredHeight) > maxH2) {
                measuredHeight = (int) maxH2;
                measuredWidth = (measuredHeight * drawableW) / drawableH;
            }
        }
        if (this.mMinW <= 0) {
            return measuredHeight;
        }
        float minW2 = dip2pixel(this.mMinW, getContext());
        if (((float) measuredWidth) >= minW2) {
            return measuredHeight;
        }
        return (drawableH / drawableW) * ((int) minW2);
    }

    public static float dip2pixel(int dip, Context context) {
        return TypedValue.applyDimension(1, (float) dip, context.getResources().getDisplayMetrics());
    }
}
