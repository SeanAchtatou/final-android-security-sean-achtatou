package com.adsdk.sdk.video;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;
import com.adsdk.sdk.AdManager;
import com.adsdk.sdk.Const;
import com.adsdk.sdk.Log;
import com.adsdk.sdk.Util;
import com.adsdk.sdk.video.InterstitialController;
import com.adsdk.sdk.video.MediaController;
import com.adsdk.sdk.video.SDKVideoView;
import com.adsdk.sdk.video.WebViewClient;
import com.jumptap.adtag.media.VideoCacheItem;
import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class RichMediaActivity extends Activity {
    public static final int TYPE_BROWSER = 0;
    public static final int TYPE_INTERSTITIAL = 2;
    public static final int TYPE_UNKNOWN = -1;
    public static final int TYPE_VIDEO = 1;
    /* access modifiers changed from: private */
    public RichMediaAd mAd;
    /* access modifiers changed from: private */
    public boolean mCanClose;
    /* access modifiers changed from: private */
    public Runnable mCheckProgressTask = new Runnable() {
        public void run() {
            Log.w("Video playback is being checked");
            if (RichMediaActivity.this.mVideoView.getCurrentPosition() - RichMediaActivity.this.mTimeTest <= 1) {
                Log.w("Video playback too slow. Ending");
                RichMediaActivity.this.finish();
                return;
            }
            Log.w("Video playback has restarted");
        }
    };
    private VideoView mCustomVideoView;
    private FrameLayout mCustomView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    /* access modifiers changed from: private */
    public int mEnterAnim;
    /* access modifiers changed from: private */
    public int mExitAnim;
    private ResourceHandler mHandler;
    protected boolean mInterstitialAutocloseReset;
    /* access modifiers changed from: private */
    public Timer mInterstitialAutocloseTimer;
    /* access modifiers changed from: private */
    public Timer mInterstitialCanCloseTimer;
    private final View.OnClickListener mInterstitialClickListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            Log.d(Const.TAG, "RichMediaActivity mInterstitialClickListener");
            if (RichMediaActivity.this.mInterstitialController != null) {
                RichMediaActivity.this.mInterstitialController.toggle();
                RichMediaActivity.this.mInterstitialController.resetAutoclose();
            }
        }
    };
    /* access modifiers changed from: private */
    public InterstitialController mInterstitialController;
    /* access modifiers changed from: private */
    public InterstitialData mInterstitialData;
    /* access modifiers changed from: private */
    public Timer mInterstitialLoadingTimer;
    private WebFrame mInterstitialView;
    /* access modifiers changed from: private */
    public FrameLayout mLoadingView;
    /* access modifiers changed from: private */
    public MediaController mMediaController;
    WebViewClient.OnPageLoadedListener mOnInterstitialLoadedListener = new WebViewClient.OnPageLoadedListener() {
        public void onPageLoaded() {
            Log.v("onPageLoaded");
            if (RichMediaActivity.this.mInterstitialData != null && RichMediaActivity.this.mInterstitialData.autoclose > 0 && RichMediaActivity.this.mInterstitialAutocloseTimer == null && !RichMediaActivity.this.mInterstitialAutocloseReset) {
                InterstitialAutocloseTask autocloseTask = new InterstitialAutocloseTask(RichMediaActivity.this);
                RichMediaActivity.this.mInterstitialAutocloseTimer = new Timer();
                RichMediaActivity.this.mInterstitialAutocloseTimer.schedule(autocloseTask, (long) (RichMediaActivity.this.mInterstitialData.autoclose * 1000));
                Log.v("onPageLoaded mInterstitialAutocloseTimer");
            }
            if (RichMediaActivity.this.mInterstitialData == null || RichMediaActivity.this.mInterstitialData.showSkipButtonAfter <= 0) {
                RichMediaActivity.this.mCanClose = true;
            } else if (RichMediaActivity.this.mInterstitialCanCloseTimer == null) {
                CanSkipTask skipTask = new CanSkipTask(RichMediaActivity.this);
                RichMediaActivity.this.mInterstitialCanCloseTimer = new Timer();
                RichMediaActivity.this.mInterstitialCanCloseTimer.schedule(skipTask, (long) (RichMediaActivity.this.mInterstitialData.showSkipButtonAfter * 1000));
                Log.v("onPageLoaded mInterstitialCanCloseTimer");
            }
            if (RichMediaActivity.this.mInterstitialLoadingTimer != null) {
                RichMediaActivity.this.mInterstitialLoadingTimer.cancel();
                RichMediaActivity.this.mInterstitialLoadingTimer = null;
            }
            RichMediaActivity.this.mPageLoaded = true;
            RichMediaActivity.this.mInterstitialController.pageLoaded();
        }
    };
    View.OnClickListener mOnInterstitialSkipListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.v("###########TRACKING SKIP INTERSTITIAL");
            RichMediaActivity.this.mResult = true;
            RichMediaActivity.this.setResult(-1);
            RichMediaActivity.this.finish();
        }
    };
    InterstitialController.OnResetAutocloseListener mOnResetAutocloseListener = new InterstitialController.OnResetAutocloseListener() {
        public void onResetAutoclose() {
            Log.v("###########RESET AUTOCLOSE INTERSTITIAL");
            RichMediaActivity.this.mInterstitialAutocloseReset = true;
            if (RichMediaActivity.this.mInterstitialAutocloseTimer != null) {
                RichMediaActivity.this.mInterstitialAutocloseTimer.cancel();
                RichMediaActivity.this.mInterstitialAutocloseTimer = null;
            }
        }
    };
    SDKVideoView.OnTimeEventListener mOnVideoCanCloseEventListener = new SDKVideoView.OnTimeEventListener() {
        public void onTimeEvent(int time) {
            Log.d("###########CAN CLOSE VIDEO:" + time);
            RichMediaActivity.this.mCanClose = true;
            if (RichMediaActivity.this.mVideoData.showSkipButton && RichMediaActivity.this.mSkipButton != null) {
                RichMediaActivity.this.mSkipButton.setImageDrawable(RichMediaActivity.this.mResourceManager.getResource(RichMediaActivity.this, -18));
                RichMediaActivity.this.mSkipButton.setVisibility(0);
            }
        }
    };
    MediaPlayer.OnCompletionListener mOnVideoCompletionListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            Log.d("###########TRACKING END VIDEO");
            Vector<String> trackers = RichMediaActivity.this.mVideoData.completeEvents;
            for (int i = 0; i < trackers.size(); i++) {
                Log.d("Track url:" + trackers.get(i));
                TrackEvent event = new TrackEvent();
                event.url = trackers.get(i);
                event.timestamp = System.currentTimeMillis();
                TrackerService.requestTrack(event);
            }
            if (RichMediaActivity.this.mType == 1 && RichMediaActivity.this.mAd.getType() == 3) {
                Intent intent = new Intent(RichMediaActivity.this, RichMediaActivity.class);
                intent.putExtra(Const.AD_EXTRA, RichMediaActivity.this.mAd);
                intent.putExtra(Const.AD_TYPE_EXTRA, 2);
                try {
                    RichMediaActivity.this.startActivity(intent);
                    RichMediaActivity.setActivityAnimation(RichMediaActivity.this, RichMediaActivity.this.mEnterAnim, RichMediaActivity.this.mExitAnim);
                } catch (Exception e) {
                    Log.e("Cannot start Rich Ad activity:" + e, e);
                }
            }
            RichMediaActivity.this.mResult = true;
            RichMediaActivity.this.setResult(-1);
            RichMediaActivity.this.finish();
        }
    };
    MediaPlayer.OnErrorListener mOnVideoErrorListener = new MediaPlayer.OnErrorListener() {
        public boolean onError(MediaPlayer mp, int what, int extra) {
            Log.w("Cannot play video/ Error: " + what + " Extra: " + extra);
            RichMediaActivity.this.finish();
            return false;
        }
    };
    MediaPlayer.OnInfoListener mOnVideoInfoListener = new MediaPlayer.OnInfoListener() {
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            Log.i("Info: " + what + " Extra: " + extra);
            if (what != 703) {
                return false;
            }
            RichMediaActivity.this.mTimeTest = RichMediaActivity.this.mVideoView.getCurrentPosition();
            new Handler().postDelayed(RichMediaActivity.this.mCheckProgressTask, 5000);
            return false;
        }
    };
    MediaController.OnPauseListener mOnVideoPauseListener = new MediaController.OnPauseListener() {
        public void onVideoPause() {
            Log.d("###########TRACKING PAUSE VIDEO");
            Vector<String> trackers = RichMediaActivity.this.mVideoData.pauseEvents;
            for (int i = 0; i < trackers.size(); i++) {
                Log.d("Track url:" + trackers.get(i));
                TrackEvent event = new TrackEvent();
                event.url = trackers.get(i);
                event.timestamp = System.currentTimeMillis();
                TrackerService.requestTrack(event);
            }
        }
    };
    MediaPlayer.OnPreparedListener mOnVideoPreparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mp) {
            Log.d(Const.TAG, "RichMediaActivity onPrepared MediaPlayer");
            if (RichMediaActivity.this.mVideoTimeoutTimer != null) {
                RichMediaActivity.this.mVideoTimeoutTimer.cancel();
                RichMediaActivity.this.mVideoTimeoutTimer = null;
            }
            if (RichMediaActivity.this.mLoadingView != null) {
                RichMediaActivity.this.mLoadingView.setVisibility(8);
            }
            if (RichMediaActivity.this.mVideoData.showNavigationBars) {
                RichMediaActivity.this.mMediaController.setVisibility(0);
            }
            RichMediaActivity.this.mVideoView.requestFocus();
        }
    };
    MediaController.OnReplayListener mOnVideoReplayListener = new MediaController.OnReplayListener() {
        public void onVideoReplay() {
            Log.d("###########TRACKING REPLAY VIDEO");
            Vector<String> trackers = RichMediaActivity.this.mVideoData.replayEvents;
            for (int i = 0; i < trackers.size(); i++) {
                Log.d("Track url:" + trackers.get(i));
                TrackEvent event = new TrackEvent();
                event.url = trackers.get(i);
                event.timestamp = System.currentTimeMillis();
                TrackerService.requestTrack(event);
            }
        }
    };
    View.OnClickListener mOnVideoSkipListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.v("###########TRACKING SKIP VIDEO");
            Vector<String> trackers = RichMediaActivity.this.mVideoData.skipEvents;
            for (int i = 0; i < trackers.size(); i++) {
                Log.d("Track url:" + trackers.get(i));
                TrackEvent event = new TrackEvent();
                event.url = trackers.get(i);
                event.timestamp = System.currentTimeMillis();
                TrackerService.requestTrack(event);
            }
            RichMediaActivity.this.mResult = true;
            RichMediaActivity.this.setResult(-1);
            RichMediaActivity.this.finish();
        }
    };
    SDKVideoView.OnStartListener mOnVideoStartListener = new SDKVideoView.OnStartListener() {
        public void onVideoStart() {
            Log.d("###########TRACKING START VIDEO");
            Vector<String> trackers = RichMediaActivity.this.mVideoData.startEvents;
            for (int i = 0; i < trackers.size(); i++) {
                Log.d("Track url:" + trackers.get(i));
                TrackEvent event = new TrackEvent();
                event.url = trackers.get(i);
                event.timestamp = System.currentTimeMillis();
                TrackerService.requestTrack(event);
            }
        }
    };
    SDKVideoView.OnTimeEventListener mOnVideoTimeEventListener = new SDKVideoView.OnTimeEventListener() {
        public void onTimeEvent(int time) {
            Log.d("###########TRACKING TIME VIDEO:" + time);
            Vector<String> trackers = RichMediaActivity.this.mVideoData.timeTrackingEvents.get(Integer.valueOf(time));
            if (trackers != null) {
                for (int i = 0; i < trackers.size(); i++) {
                    Log.d("Track url:" + ((String) trackers.get(i)));
                    TrackEvent event = new TrackEvent();
                    event.url = (String) trackers.get(i);
                    event.timestamp = System.currentTimeMillis();
                    TrackerService.requestTrack(event);
                }
            }
        }
    };
    MediaController.OnUnpauseListener mOnVideoUnpauseListener = new MediaController.OnUnpauseListener() {
        public void onVideoUnpause() {
            Log.d("###########TRACKING UNPAUSE VIDEO");
            Vector<String> trackers = RichMediaActivity.this.mVideoData.unpauseEvents;
            for (int i = 0; i < trackers.size(); i++) {
                Log.d("Track url:" + trackers.get(i));
                TrackEvent event = new TrackEvent();
                event.url = trackers.get(i);
                event.timestamp = System.currentTimeMillis();
                TrackerService.requestTrack(event);
            }
        }
    };
    WebViewClient.OnPageLoadedListener mOnWebBrowserLoadedListener = new WebViewClient.OnPageLoadedListener() {
        public void onPageLoaded() {
            RichMediaActivity.this.mPageLoaded = true;
        }
    };
    private final View.OnClickListener mOverlayClickListener = new View.OnClickListener() {
        public void onClick(View arg0) {
            Log.d(Const.TAG, "RichMediaActivity mOverlayClickListener");
            if (RichMediaActivity.this.mMediaController != null) {
                RichMediaActivity.this.mMediaController.toggle();
            }
        }
    };
    private final SDKVideoView.OnTimeEventListener mOverlayShowListener = new SDKVideoView.OnTimeEventListener() {
        public void onTimeEvent(int time) {
            Log.d(Const.TAG, "RichMediaActivity mOverlayShowListener show after:" + time);
            if (RichMediaActivity.this.mOverlayView != null) {
                RichMediaActivity.this.mOverlayView.setVisibility(0);
                RichMediaActivity.this.mOverlayView.requestLayout();
            }
        }
    };
    /* access modifiers changed from: private */
    public WebFrame mOverlayView;
    /* access modifiers changed from: private */
    public boolean mPageLoaded = false;
    /* access modifiers changed from: private */
    public ResourceManager mResourceManager;
    /* access modifiers changed from: private */
    public boolean mResult;
    private FrameLayout mRootLayout;
    /* access modifiers changed from: private */
    public ImageView mSkipButton;
    protected int mTimeTest;
    /* access modifiers changed from: private */
    public int mType;
    /* access modifiers changed from: private */
    public VideoData mVideoData;
    private int mVideoHeight;
    private int mVideoLastPosition;
    private FrameLayout mVideoLayout;
    /* access modifiers changed from: private */
    public Timer mVideoTimeoutTimer;
    /* access modifiers changed from: private */
    public SDKVideoView mVideoView;
    private int mVideoWidth;
    private WebFrame mWebBrowserView;
    private int mWindowHeight;
    private int mWindowWidth;
    int marginArg = 8;
    DisplayMetrics metrics;
    int paddingArg = 5;
    int skipButtonSizeLand = 50;
    int skipButtonSizePort = 40;
    private Uri uri;

    class CanSkipTask extends TimerTask {
        /* access modifiers changed from: private */
        public final RichMediaActivity mActivity;

        public CanSkipTask(RichMediaActivity activity) {
            this.mActivity = activity;
        }

        public void run() {
            Log.v("###########TRACKING CAN CLOSE INTERSTITIAL");
            this.mActivity.mCanClose = true;
            if (this.mActivity.mSkipButton != null) {
                this.mActivity.runOnUiThread(new Runnable() {
                    public void run() {
                        CanSkipTask.this.mActivity.mSkipButton.setVisibility(0);
                    }
                });
            }
        }
    }

    class InterstitialAutocloseTask extends TimerTask {
        private final Activity mActivity;

        public InterstitialAutocloseTask(Activity activity) {
            this.mActivity = activity;
        }

        public void run() {
            Log.v("###########TRACKING INTERSTITIAL AUTOCLOSE");
            RichMediaActivity.this.mResult = true;
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    RichMediaActivity.this.setResult(-1);
                    RichMediaActivity.this.finish();
                }
            });
        }
    }

    class InterstitialLoadingTimeoutTask extends TimerTask {
        InterstitialLoadingTimeoutTask() {
        }

        public void run() {
            Log.v(Const.TAG, "###########TRACKING INTERSTITIAL LOADING TIMEOUT");
            RichMediaActivity.this.mCanClose = true;
            RichMediaActivity.this.mInterstitialController.pageLoaded();
        }
    }

    class VideoTimeoutTask extends TimerTask {
        /* access modifiers changed from: private */
        public final Activity mActivity;

        public VideoTimeoutTask(Activity activity) {
            this.mActivity = activity;
        }

        public void run() {
            Log.v("###########TRACKING VIDEO TIMEOUT");
            this.mActivity.runOnUiThread(new Runnable() {
                public void run() {
                    VideoTimeoutTask.this.mActivity.finish();
                }
            });
        }
    }

    public static void setActivityAnimation(Activity activity, int in, int out) {
        try {
            activity.overridePendingTransition(in, out);
        } catch (Exception e) {
        }
    }

    static class ResourceHandler extends Handler {
        WeakReference<RichMediaActivity> richMediaActivity;

        public ResourceHandler(RichMediaActivity activity) {
            this.richMediaActivity = new WeakReference<>(activity);
        }

        public void handleMessage(Message msg) {
            RichMediaActivity wRichMediaActivity = this.richMediaActivity.get();
            if (wRichMediaActivity != null) {
                wRichMediaActivity.handleMessage(msg);
            }
        }
    }

    public void handleMessage(Message msg) {
        switch (msg.what) {
            case 100:
                switch (msg.arg1) {
                    case ResourceManager.DEFAULT_SKIP_IMAGE_RESOURCE_ID:
                        if (this.mSkipButton == null) {
                            return;
                        }
                        if (this.mResourceManager.containsResource(-18)) {
                            this.mSkipButton.setImageDrawable(this.mResourceManager.getResource(this, -18));
                            return;
                        } else {
                            this.mSkipButton.setImageDrawable(this.mResourceManager.getResource(this, -18));
                            return;
                        }
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public void finish() {
        if (this.mAd != null) {
            Log.d("Finish Activity type:" + this.mType + " ad Type:" + this.mAd.getType());
            switch (this.mType) {
                case 1:
                    if (this.mAd.getType() != 5) {
                        if (this.mAd.getType() == 3 && !this.mResult) {
                            AdManager.closeRunningAd(this.mAd, this.mResult);
                            break;
                        }
                    } else {
                        AdManager.closeRunningAd(this.mAd, this.mResult);
                        break;
                    }
                case 2:
                    if (this.mAd.getType() == 6 || this.mAd.getType() == 3 || this.mAd.getType() == 4) {
                        AdManager.closeRunningAd(this.mAd, this.mResult);
                        break;
                    }
            }
        }
        super.finish();
        setActivityAnimation(this, this.mEnterAnim, this.mExitAnim);
    }

    public int getDipSize(int argSize) {
        return (int) TypedValue.applyDimension(1, (float) argSize, getResources().getDisplayMetrics());
    }

    public FrameLayout getRootLayout() {
        return this.mRootLayout;
    }

    public void goBack() {
        if (this.mCustomView != null) {
            Log.d("Closing custom view on back key pressed");
            onHideCustomView();
            return;
        }
        switch (this.mType) {
            case 0:
                if (this.mWebBrowserView.canGoBack()) {
                    this.mWebBrowserView.goBack();
                    return;
                } else {
                    finish();
                    return;
                }
            case 1:
                if (this.mCanClose) {
                    finish();
                    return;
                }
                return;
            case 2:
                if (this.mInterstitialView.canGoBack()) {
                    this.mInterstitialView.goBack();
                    return;
                } else if (this.mCanClose) {
                    this.mResult = true;
                    setResult(-1);
                    finish();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void initInterstitialView() {
        this.mInterstitialData = this.mAd.getInterstitial();
        this.mInterstitialAutocloseReset = false;
        setRequestedOrientation(this.mInterstitialData.orientation);
        FrameLayout layout = new FrameLayout(this);
        this.mInterstitialView = new WebFrame(this, true, false, false);
        this.mInterstitialView.setBackgroundColor(0);
        this.mInterstitialView.setOnPageLoadedListener(this.mOnInterstitialLoadedListener);
        this.mInterstitialController = new InterstitialController(this, this.mInterstitialData);
        this.mInterstitialController.setBrowser(this.mInterstitialView);
        this.mInterstitialController.setBrowserView(this.mInterstitialView);
        this.mInterstitialController.setOnResetAutocloseListener(this.mOnResetAutocloseListener);
        layout.addView(this.mInterstitialController, new FrameLayout.LayoutParams(-1, -1, 17));
        if (this.mInterstitialData.showNavigationBars) {
            this.mInterstitialController.show(0);
        }
        if (this.mInterstitialData.showSkipButton) {
            this.mSkipButton = new ImageView(this);
            this.mSkipButton.setAdjustViewBounds(false);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) this.skipButtonSizeLand, getResources().getDisplayMetrics());
            int buttonSize = (int) (((double) Math.min(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels)) * 0.1d);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(buttonSize, buttonSize, 53);
            if (this.mInterstitialData.orientation == 1) {
                int margin = (int) TypedValue.applyDimension(1, 8.0f, getResources().getDisplayMetrics());
                params.topMargin = margin;
                params.rightMargin = margin;
            } else {
                int margin2 = (int) TypedValue.applyDimension(1, 10.0f, getResources().getDisplayMetrics());
                params.topMargin = margin2;
                params.rightMargin = margin2;
            }
            if (this.mInterstitialData.skipButtonImage == null || this.mInterstitialData.skipButtonImage.length() <= 0) {
                this.mSkipButton.setImageDrawable(this.mResourceManager.getResource(this, -18));
            } else {
                this.mSkipButton.setBackgroundDrawable(null);
                this.mResourceManager.fetchResource(this, this.mInterstitialData.skipButtonImage, -18);
            }
            this.mSkipButton.setOnClickListener(this.mOnInterstitialSkipListener);
            if (this.mInterstitialData.showSkipButtonAfter > 0) {
                this.mCanClose = false;
                this.mSkipButton.setVisibility(8);
                if (this.mInterstitialLoadingTimer == null) {
                    InterstitialLoadingTimeoutTask loadingTimeoutTask = new InterstitialLoadingTimeoutTask();
                    this.mInterstitialLoadingTimer = new Timer();
                    this.mInterstitialLoadingTimer.schedule(loadingTimeoutTask, 10000);
                }
            } else {
                this.mCanClose = true;
                this.mSkipButton.setVisibility(0);
            }
            layout.addView(this.mSkipButton, params);
        } else {
            this.mCanClose = false;
        }
        this.mInterstitialView.setOnClickListener(this.mInterstitialClickListener);
        this.mRootLayout.addView(layout);
        switch (this.mInterstitialData.interstitialType) {
            case 0:
                this.mInterstitialView.loadUrl(this.mInterstitialData.interstitialUrl);
                break;
            case 1:
                this.mInterstitialView.setMarkup(this.mInterstitialData.interstitialMarkup);
                break;
        }
        Log.i(this.mInterstitialView.getWebView().getSettings().getUserAgentString());
    }

    private void initRootLayout() {
        this.mRootLayout = new FrameLayout(this);
        this.mRootLayout.setBackgroundColor(-16777216);
    }

    private void initVideoView() {
        this.mVideoData = this.mAd.getVideo();
        setRequestedOrientation(this.mVideoData.orientation);
        if (this.mVideoData.orientation == 0) {
            if (this.mWindowWidth < this.mWindowHeight) {
                int size = this.mWindowWidth;
                this.mWindowWidth = this.mWindowHeight;
                this.mWindowHeight = size;
            }
        } else if (this.mWindowHeight < this.mWindowWidth) {
            int size2 = this.mWindowHeight;
            this.mWindowHeight = this.mWindowWidth;
            this.mWindowWidth = size2;
        }
        this.mVideoWidth = this.mVideoData.width;
        this.mVideoHeight = this.mVideoData.height;
        if (this.mVideoWidth <= 0) {
            this.mVideoWidth = this.mWindowWidth;
            this.mVideoHeight = this.mWindowHeight;
        } else {
            DisplayMetrics m = getResources().getDisplayMetrics();
            this.mVideoWidth = (int) TypedValue.applyDimension(1, (float) this.mVideoWidth, m);
            this.mVideoHeight = (int) TypedValue.applyDimension(1, (float) this.mVideoHeight, m);
            if (this.mVideoWidth > this.mWindowWidth) {
                this.mVideoWidth = this.mWindowWidth;
            }
            if (this.mVideoHeight > this.mWindowHeight) {
                this.mVideoHeight = this.mWindowHeight;
            }
        }
        Log.d("Video size (" + this.mVideoWidth + VideoCacheItem.URL_DELIMITER + this.mVideoHeight + ")");
        this.mVideoLayout = new FrameLayout(this);
        this.mVideoView = new SDKVideoView(this, this.mVideoWidth, this.mVideoHeight, this.mVideoData.display);
        this.mVideoLayout.addView(this.mVideoView, new FrameLayout.LayoutParams(-1, -1, 17));
        if (this.mVideoData.showHtmlOverlay) {
            this.mOverlayView = new WebFrame(this, false, false, false);
            this.mOverlayView.setEnableZoom(false);
            this.mOverlayView.setOnClickListener(this.mOverlayClickListener);
            this.mOverlayView.setBackgroundColor(0);
            if (this.mVideoData.showHtmlOverlayAfter > 0) {
                this.mOverlayView.setVisibility(8);
                this.mVideoView.setOnTimeEventListener(this.mVideoData.showHtmlOverlayAfter, this.mOverlayShowListener);
            }
            if (this.mVideoData.htmlOverlayType == 0) {
                this.mOverlayView.loadUrl(this.mVideoData.htmlOverlayUrl);
            } else {
                this.mOverlayView.setMarkup(this.mVideoData.htmlOverlayMarkup);
            }
            FrameLayout.LayoutParams overlayParams = new FrameLayout.LayoutParams(-1, -1);
            if (this.mVideoData.showBottomNavigationBar && this.mVideoData.showTopNavigationBar) {
                overlayParams.bottomMargin = (int) (((double) this.mWindowWidth) * 0.11875d);
                overlayParams.topMargin = (int) (((double) this.mWindowWidth) * 0.11875d);
                overlayParams.gravity = 17;
            } else if (this.mVideoData.showBottomNavigationBar && !this.mVideoData.showTopNavigationBar) {
                overlayParams.bottomMargin = (int) (((double) this.mWindowWidth) * 0.11875d);
                overlayParams.gravity = 48;
            } else if (this.mVideoData.showTopNavigationBar && !this.mVideoData.showBottomNavigationBar) {
                overlayParams.topMargin = (int) (((double) this.mWindowWidth) * 0.11875d);
                overlayParams.gravity = 80;
            }
            this.mVideoLayout.addView(this.mOverlayView, overlayParams);
        }
        this.mMediaController = new MediaController(this, this.mVideoData);
        this.mVideoView.setMediaController(this.mMediaController);
        if (this.mVideoData.showNavigationBars) {
            this.mMediaController.toggle();
        }
        if (!this.mVideoData.pauseEvents.isEmpty()) {
            this.mMediaController.setOnPauseListener(this.mOnVideoPauseListener);
        }
        if (!this.mVideoData.unpauseEvents.isEmpty()) {
            this.mMediaController.setOnUnpauseListener(this.mOnVideoUnpauseListener);
        }
        if (!this.mVideoData.replayEvents.isEmpty()) {
            this.mMediaController.setOnReplayListener(this.mOnVideoReplayListener);
        }
        this.mVideoLayout.addView(this.mMediaController, new FrameLayout.LayoutParams(-1, -1, 7));
        if (this.mVideoData.showSkipButton) {
            this.mSkipButton = new ImageView(this);
            this.mSkipButton.setAdjustViewBounds(false);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) this.skipButtonSizeLand, getResources().getDisplayMetrics());
            int buttonSize = (int) (((double) Math.min(getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels)) * 0.09d);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(buttonSize, buttonSize, 53);
            if (this.mVideoData.orientation == 1) {
                int margin = (int) TypedValue.applyDimension(1, 8.0f, getResources().getDisplayMetrics());
                params.topMargin = margin;
                params.rightMargin = margin;
            } else {
                int margin2 = (int) TypedValue.applyDimension(1, 10.0f, getResources().getDisplayMetrics());
                params.topMargin = margin2;
                params.rightMargin = margin2;
            }
            if (this.mVideoData.skipButtonImage == null || this.mVideoData.skipButtonImage.length() <= 0) {
                this.mSkipButton.setImageDrawable(this.mResourceManager.getResource(this, -18));
            } else {
                this.mResourceManager.fetchResource(this, this.mVideoData.skipButtonImage, -18);
            }
            this.mSkipButton.setOnClickListener(this.mOnVideoSkipListener);
            if (this.mVideoData.showSkipButtonAfter > 0) {
                this.mCanClose = false;
                this.mSkipButton.setVisibility(8);
            } else {
                this.mCanClose = true;
                this.mSkipButton.setVisibility(0);
            }
            this.mVideoLayout.addView(this.mSkipButton, params);
        } else {
            this.mCanClose = false;
        }
        if (this.mVideoData.showSkipButtonAfter > 0) {
            this.mVideoView.setOnTimeEventListener(this.mVideoData.showSkipButtonAfter, this.mOnVideoCanCloseEventListener);
        }
        FrameLayout.LayoutParams params2 = new FrameLayout.LayoutParams(-2, -2, 17);
        this.mLoadingView = new FrameLayout(this);
        TextView loadingText = new TextView(this);
        loadingText.setText(Const.LOADING);
        this.mLoadingView.addView(loadingText, params2);
        this.mVideoLayout.addView(this.mLoadingView, new FrameLayout.LayoutParams(-1, -1, 17));
        this.mVideoView.setOnPreparedListener(this.mOnVideoPreparedListener);
        this.mVideoView.setOnCompletionListener(this.mOnVideoCompletionListener);
        this.mVideoView.setOnErrorListener(this.mOnVideoErrorListener);
        this.mVideoView.setOnInfoListener(this.mOnVideoInfoListener);
        if (!this.mVideoData.startEvents.isEmpty()) {
            this.mVideoView.setOnStartListener(this.mOnVideoStartListener);
        }
        if (!this.mVideoData.timeTrackingEvents.isEmpty()) {
            for (Integer intValue : this.mVideoData.timeTrackingEvents.keySet()) {
                this.mVideoView.setOnTimeEventListener(intValue.intValue(), this.mOnVideoTimeEventListener);
            }
        }
        this.mVideoLastPosition = 0;
        this.mVideoView.setVideoPath(this.mVideoData.videoUrl);
    }

    private void initWebBrowserView(boolean showExit) {
        this.mWebBrowserView = new WebFrame(this, true, true, showExit);
        this.mWebBrowserView.setOnPageLoadedListener(this.mOnWebBrowserLoadedListener);
        this.mRootLayout.addView(this.mWebBrowserView);
    }

    public void navigate(String clickUrl) {
        switch (this.mType) {
            case 0:
                this.mWebBrowserView.loadUrl(clickUrl);
                return;
            case 1:
            default:
                Intent intent = new Intent(this, RichMediaActivity.class);
                intent.setData(Uri.parse(clickUrl));
                startActivity(intent);
                return;
            case 2:
                this.mInterstitialView.loadUrl(clickUrl);
                return;
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d("RichMediaActivity onConfigurationChanged");
    }

    public void onCreate(Bundle icicle) {
        Log.d("RichMediaActivity onCreate");
        super.onCreate(icicle);
        this.mResult = false;
        this.mPageLoaded = false;
        setResult(0);
        Window win = getWindow();
        win.setFlags(1024, 1024);
        requestWindowFeature(1);
        win.addFlags(512);
        Display display = getWindowManager().getDefaultDisplay();
        this.metrics = new DisplayMetrics();
        ((WindowManager) getSystemService("window")).getDefaultDisplay().getMetrics(this.metrics);
        this.mWindowWidth = display.getWidth();
        this.mWindowHeight = display.getHeight();
        win.clearFlags(512);
        Log.d("RichMediaActivity Window Size:(" + this.mWindowWidth + VideoCacheItem.URL_DELIMITER + this.mWindowHeight + ")");
        setVolumeControlStream(3);
        this.mType = -1;
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras == null || extras.getSerializable(Const.AD_EXTRA) == null) {
            this.uri = intent.getData();
            if (this.uri == null) {
                Log.d("url is null so do not load anything");
                finish();
                return;
            }
            this.mType = 0;
        } else {
            requestWindowFeature(1);
        }
        this.mHandler = new ResourceHandler(this);
        this.mResourceManager = new ResourceManager(this, this.mHandler);
        initRootLayout();
        if (this.mType != 0) {
            this.mAd = (RichMediaAd) extras.getSerializable(Const.AD_EXTRA);
            this.mEnterAnim = Util.getEnterAnimation(this.mAd.getAnimation());
            this.mExitAnim = Util.getExitAnimation(this.mAd.getAnimation());
            this.mCanClose = false;
            this.mType = extras.getInt(Const.AD_TYPE_EXTRA, -1);
            if (this.mType == -1) {
                switch (this.mAd.getType()) {
                    case 3:
                    case 5:
                        this.mType = 1;
                        break;
                    case 4:
                    case 6:
                        this.mType = 2;
                        break;
                }
            }
            switch (this.mType) {
                case 1:
                    Log.v("Type video");
                    initVideoView();
                    break;
                case 2:
                    Log.v("Type interstitial");
                    initInterstitialView();
                    break;
            }
        } else {
            initWebBrowserView(true);
            this.mWebBrowserView.loadUrl(this.uri.toString());
            this.mEnterAnim = Util.getEnterAnimation(1);
            this.mExitAnim = Util.getExitAnimation(1);
        }
        setContentView(this.mRootLayout);
        Log.d("RichMediaActivity onCreate done");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mMediaController = null;
        this.mResourceManager.releaseInstance();
        if (this.mVideoView != null) {
            this.mVideoView.destroy();
        }
        Log.d("RichMediaActivity onDestroy");
        super.onDestroy();
        Log.d("RichMediaActivity onDestroy done");
    }

    public void onHideCustomView() {
        Log.d("onHideCustomView Hidding Custom View");
        if (this.mCustomView != null) {
            this.mCustomView.setVisibility(8);
            this.mCustomView = null;
            if (this.mCustomVideoView != null) {
                try {
                    Log.d("onHideCustomView stop video");
                    this.mCustomVideoView.stopPlayback();
                } catch (Exception e) {
                    Log.d("Couldn't stop custom video view");
                }
                this.mCustomVideoView = null;
            }
        }
        Log.d("onHideCustomView calling callback");
        this.mCustomViewCallback.onCustomViewHidden();
        this.mRootLayout.setVisibility(0);
        setContentView(this.mRootLayout);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        goBack();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.d("RichMediaActivity onPause");
        super.onPause();
        switch (this.mType) {
            case 1:
                this.mVideoLastPosition = this.mVideoView.getCurrentPosition();
                this.mVideoView.stopPlayback();
                this.mRootLayout.removeView(this.mVideoLayout);
                if (this.mVideoTimeoutTimer != null) {
                    this.mVideoTimeoutTimer.cancel();
                    this.mVideoTimeoutTimer = null;
                    break;
                }
                break;
            case 2:
                if (this.mInterstitialLoadingTimer != null) {
                    this.mInterstitialLoadingTimer.cancel();
                    this.mInterstitialLoadingTimer = null;
                }
                if (this.mInterstitialAutocloseTimer != null) {
                    this.mInterstitialAutocloseTimer.cancel();
                    this.mInterstitialAutocloseTimer = null;
                }
                if (this.mInterstitialCanCloseTimer != null) {
                    this.mInterstitialCanCloseTimer.cancel();
                    this.mInterstitialCanCloseTimer = null;
                    break;
                }
                break;
        }
        Log.d("RichMediaActivity onPause done");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.d("RichMediaActivity onResume");
        super.onResume();
        switch (this.mType) {
            case 1:
                this.mRootLayout.addView(this.mVideoLayout);
                this.mVideoView.seekTo(this.mVideoLastPosition);
                this.mVideoView.start();
                if (this.mVideoTimeoutTimer == null) {
                    VideoTimeoutTask autocloseTask = new VideoTimeoutTask(this);
                    this.mVideoTimeoutTimer = new Timer();
                    this.mVideoTimeoutTimer.schedule(autocloseTask, (long) Const.VIDEO_LOAD_TIMEOUT);
                    break;
                }
                break;
            case 2:
                switch (this.mInterstitialData.interstitialType) {
                    case 0:
                        if (!this.mPageLoaded) {
                            this.mInterstitialView.loadUrl(this.mInterstitialData.interstitialUrl);
                            break;
                        }
                        break;
                    case 1:
                        if (!this.mPageLoaded) {
                            this.mInterstitialView.setMarkup(this.mInterstitialData.interstitialMarkup);
                            break;
                        }
                        break;
                }
        }
        Log.d("RichMediaActivity onResume done");
    }

    public void onShowCustomView(View view, WebChromeClient.CustomViewCallback callback) {
        Log.d(" onShowCustomView");
        if (view instanceof FrameLayout) {
            this.mCustomView = (FrameLayout) view;
            this.mCustomViewCallback = callback;
            if (this.mCustomView.getFocusedChild() instanceof VideoView) {
                Log.d(" onShowCustomView Starting Video View");
                this.mCustomVideoView = (VideoView) this.mCustomView.getFocusedChild();
                this.mCustomVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        Log.d(" onCompletion Video");
                        RichMediaActivity.this.onHideCustomView();
                    }
                });
                this.mCustomVideoView.start();
            }
            this.mRootLayout.setVisibility(8);
            this.mCustomView.setVisibility(0);
            setContentView(this.mCustomView);
        }
    }

    public void playVideo() {
        Log.d("RichMediaActivity play video:" + this.mType);
        switch (this.mType) {
            case 1:
                if (this.mMediaController != null) {
                    this.mMediaController.replay();
                    return;
                }
                return;
            case 2:
                if (this.mAd.getType() == 4) {
                    Log.d("RichMediaActivity launch video");
                    Intent intent = new Intent(this, RichMediaActivity.class);
                    intent.putExtra(Const.AD_EXTRA, this.mAd);
                    intent.putExtra(Const.AD_TYPE_EXTRA, 1);
                    try {
                        startActivity(intent);
                        setActivityAnimation(this, this.mEnterAnim, this.mExitAnim);
                        this.mResult = true;
                        setResult(-1);
                        return;
                    } catch (Exception e) {
                        Log.e(Const.TAG, "Cannot start Rich Ad activity:" + e, e);
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void replayVideo() {
        if (this.mMediaController != null) {
            this.mMediaController.replay();
        }
    }
}
