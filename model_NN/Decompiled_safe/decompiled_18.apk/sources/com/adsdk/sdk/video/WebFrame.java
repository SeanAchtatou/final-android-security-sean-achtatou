package com.adsdk.sdk.video;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adfonic.android.utils.HtmlFormatter;
import com.adsdk.sdk.Const;
import com.adsdk.sdk.Log;
import com.adsdk.sdk.video.InterstitialController;
import com.adsdk.sdk.video.WebViewClient;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class WebFrame extends FrameLayout implements InterstitialController.BrowserControl {
    private static Field mWebView_LAYER_TYPE_SOFTWARE;
    private static Method mWebView_SetLayerType;
    private boolean enableZoom = true;
    private Activity mActivity;
    private InterstitialController mController;
    private ImageView mExitButton;
    /* access modifiers changed from: private */
    public WebView mWebView;
    /* access modifiers changed from: private */
    public WebViewClient mWebViewClient;

    static {
        initCompatibility();
    }

    private static void initCompatibility() {
        try {
            Method[] methods = WebView.class.getMethods();
            int length = methods.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                }
                Method m = methods[i];
                if (m.getName().equals("setLayerType")) {
                    mWebView_SetLayerType = m;
                    break;
                }
                i++;
            }
            Log.v("set layer " + mWebView_SetLayerType);
            mWebView_LAYER_TYPE_SOFTWARE = WebView.class.getField("LAYER_TYPE_SOFTWARE");
            Log.v("set1 layer " + mWebView_LAYER_TYPE_SOFTWARE);
        } catch (SecurityException e) {
            Log.v("SecurityException");
        } catch (NoSuchFieldException e2) {
            Log.v("NoSuchFieldException");
        }
    }

    private static void setLayer(WebView webView) {
        if (mWebView_SetLayerType == null || mWebView_LAYER_TYPE_SOFTWARE == null) {
            Log.v("Set Layer is not supported");
            return;
        }
        try {
            Log.v("Set Layer is supported");
            mWebView_SetLayerType.invoke(webView, Integer.valueOf(mWebView_LAYER_TYPE_SOFTWARE.getInt(WebView.class)), null);
        } catch (InvocationTargetException e) {
            Log.v("Set InvocationTargetException");
        } catch (IllegalArgumentException e2) {
            Log.v("Set IllegalArgumentException");
        } catch (IllegalAccessException e3) {
            Log.v("Set IllegalAccessException");
        }
    }

    public WebFrame(Activity context, boolean allowNavigation, boolean scroll, boolean showExit) {
        super(context);
        initCompatibility();
        this.mActivity = context;
        this.mWebView = new WebView(context);
        this.mWebView.setVerticalScrollBarEnabled(scroll);
        this.mWebView.setHorizontalScrollBarEnabled(scroll);
        this.mWebView.setBackgroundColor(0);
        setLayer(this.mWebView);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setPluginsEnabled(true);
        webSettings.setSupportZoom(this.enableZoom);
        webSettings.setBuiltInZoomControls(this.enableZoom);
        this.mWebViewClient = new WebViewClient(this.mActivity, allowNavigation);
        this.mWebView.setWebViewClient(this.mWebViewClient);
        final Activity localContext = context;
        if (showExit) {
            ImageView bg = new ImageView(context);
            bg.setBackgroundColor(0);
            addView(bg, new FrameLayout.LayoutParams(-1, -1, 17));
            addView(this.mWebView, new FrameLayout.LayoutParams(-1, -1, 17));
            this.mExitButton = new ImageView(context);
            this.mExitButton.setAdjustViewBounds(false);
            this.mExitButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    localContext.finish();
                }
            });
            int buttonSize = (int) TypedValue.applyDimension(1, 35.0f, getResources().getDisplayMetrics());
            this.mExitButton.setImageDrawable(ResourceManager.getStaticResource(context, -18));
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(buttonSize, buttonSize, 53);
            int margin = (int) TypedValue.applyDimension(1, 6.0f, getResources().getDisplayMetrics());
            params.topMargin = margin;
            params.rightMargin = margin;
            addView(this.mExitButton, params);
            return;
        }
        addView(this.mWebView, new FrameLayout.LayoutParams(-1, -1, 17));
    }

    public void loadUrl(String url) {
        new LoadUrlTask().execute(url);
    }

    public void setMarkup(String htmlMarkup) {
        String data = Uri.encode(htmlMarkup);
        this.mWebViewClient.setAllowedUrl(null);
        this.mWebView.loadData(data, HtmlFormatter.TEXT_HTML, Const.ENCODING);
    }

    public void setBackgroundColor(int color) {
        super.setBackgroundColor(color);
        this.mWebView.setBackgroundColor(color);
    }

    private class LoadUrlTask extends AsyncTask<String, Void, String> {
        String userAgent;

        public LoadUrlTask() {
            this.userAgent = WebFrame.this.getUserAgentString();
        }

        /* JADX WARN: Type inference failed for: r9v4, types: [java.net.URLConnection] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.String... r12) {
            /*
                r11 = this;
                r9 = 0
                r3 = r12[r9]
                r7 = 0
                java.net.URL r8 = new java.net.URL     // Catch:{ MalformedURLException -> 0x004e }
                r8.<init>(r3)     // Catch:{ MalformedURLException -> 0x004e }
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                java.lang.String r10 = "Checking URL redirect:"
                r9.<init>(r10)
                java.lang.StringBuilder r9 = r9.append(r3)
                java.lang.String r9 = r9.toString()
                com.adsdk.sdk.Log.d(r9)
                r6 = -1
                r1 = 0
                java.lang.String r4 = r8.toString()
                java.util.HashSet r5 = new java.util.HashSet
                r5.<init>()
                r5.add(r4)
            L_0x0029:
                java.net.URLConnection r9 = r8.openConnection()     // Catch:{ IOException -> 0x008a }
                r0 = r9
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x008a }
                r1 = r0
                java.lang.String r9 = "User-Agent"
                java.lang.String r10 = r11.userAgent     // Catch:{ IOException -> 0x008a }
                r1.setRequestProperty(r9, r10)     // Catch:{ IOException -> 0x008a }
                r9 = 0
                r1.setInstanceFollowRedirects(r9)     // Catch:{ IOException -> 0x008a }
                int r6 = r1.getResponseCode()     // Catch:{ IOException -> 0x008a }
                r9 = 200(0xc8, float:2.8E-43)
                if (r6 != r9) goto L_0x0056
                r1.disconnect()     // Catch:{ IOException -> 0x008a }
                r7 = r8
            L_0x0048:
                if (r1 == 0) goto L_0x004d
                r1.disconnect()
            L_0x004d:
                return r4
            L_0x004e:
                r2 = move-exception
                if (r3 == 0) goto L_0x0053
            L_0x0051:
                r4 = r3
                goto L_0x004d
            L_0x0053:
                java.lang.String r3 = ""
                goto L_0x0051
            L_0x0056:
                java.lang.String r9 = "location"
                java.lang.String r4 = r1.getHeaderField(r9)     // Catch:{ IOException -> 0x008a }
                r1.disconnect()     // Catch:{ IOException -> 0x008a }
                boolean r9 = r5.add(r4)     // Catch:{ IOException -> 0x008a }
                if (r9 != 0) goto L_0x0073
                java.lang.String r9 = "URL redirect cycle detected"
                com.adsdk.sdk.Log.d(r9)     // Catch:{ IOException -> 0x008a }
                if (r1 == 0) goto L_0x006f
                r1.disconnect()
            L_0x006f:
                java.lang.String r4 = ""
                r7 = r8
                goto L_0x004d
            L_0x0073:
                java.net.URL r7 = new java.net.URL     // Catch:{ IOException -> 0x008a }
                r7.<init>(r4)     // Catch:{ IOException -> 0x008a }
                r9 = 302(0x12e, float:4.23E-43)
                if (r6 == r9) goto L_0x0088
                r9 = 301(0x12d, float:4.22E-43)
                if (r6 == r9) goto L_0x0088
                r9 = 307(0x133, float:4.3E-43)
                if (r6 == r9) goto L_0x0088
                r9 = 303(0x12f, float:4.25E-43)
                if (r6 != r9) goto L_0x0048
            L_0x0088:
                r8 = r7
                goto L_0x0029
            L_0x008a:
                r2 = move-exception
                if (r4 == 0) goto L_0x0094
            L_0x008d:
                if (r1 == 0) goto L_0x0092
                r1.disconnect()
            L_0x0092:
                r7 = r8
                goto L_0x004d
            L_0x0094:
                java.lang.String r4 = ""
                goto L_0x008d
            L_0x0097:
                r9 = move-exception
                if (r1 == 0) goto L_0x009d
                r1.disconnect()
            L_0x009d:
                throw r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.adsdk.sdk.video.WebFrame.LoadUrlTask.doInBackground(java.lang.String[]):java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String url) {
            if (url == null || url.equals("")) {
                url = "about:blank";
            }
            Log.d("Show URL: " + url);
            WebFrame.this.mWebViewClient.setAllowedUrl(url);
            WebFrame.this.mWebView.loadUrl(url);
            WebFrame.this.requestLayout();
        }
    }

    /* access modifiers changed from: private */
    public String getUserAgentString() {
        return this.mWebView.getSettings().getUserAgentString();
    }

    public WebView getWebView() {
        return this.mWebView;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        onTouchEvent(ev);
        return false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        return true;
    }

    public boolean canGoBack() {
        return this.mWebView.canGoBack();
    }

    public void goBack() {
        this.mWebView.goBack();
    }

    public boolean canGoForward() {
        return this.mWebView.canGoForward();
    }

    public void goForward() {
        this.mWebView.goForward();
    }

    public void reload() {
        this.mWebView.reload();
    }

    public void setBrowserController(InterstitialController controller) {
        if (this.mController != null) {
            this.mController.hide();
        }
        this.mController = controller;
        attachController();
    }

    private void attachController() {
        if (this.mController != null) {
            this.mController.setBrowser(this);
        }
    }

    public int getTime() {
        long finishedLoadedTime = this.mWebViewClient.getFinishedLoadingTime();
        if (finishedLoadedTime > 0) {
            return (int) (System.currentTimeMillis() - finishedLoadedTime);
        }
        return 0;
    }

    public void launchExternalBrowser() {
        String url = this.mWebViewClient.getAllowedUrl();
        if (url == null || url.length() == 0) {
            url = "about:blank";
        }
        this.mActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public String getPageTitle() {
        return this.mWebView.getTitle();
    }

    public void setOnPageLoadedListener(WebViewClient.OnPageLoadedListener l) {
        this.mWebViewClient.setOnPageLoadedListener(l);
    }

    public boolean isEnableZoom() {
        return this.enableZoom;
    }

    public void setEnableZoom(boolean enableZoom2) {
        this.enableZoom = enableZoom2;
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setSupportZoom(enableZoom2);
        webSettings.setBuiltInZoomControls(enableZoom2);
    }
}
