package com.adsdk.sdk.video;

import com.adsdk.sdk.Log;
import java.lang.Thread;
import java.util.LinkedList;
import java.util.Queue;

public class TrackerService {
    /* access modifiers changed from: private */
    public static Object sLock = new Object();
    /* access modifiers changed from: private */
    public static Queue<TrackEvent> sRetryTrackEvents = new LinkedList();
    /* access modifiers changed from: private */
    public static boolean sStopped;
    /* access modifiers changed from: private */
    public static Thread sThread;
    /* access modifiers changed from: private */
    public static boolean sThreadRunning = false;
    /* access modifiers changed from: private */
    public static Queue<TrackEvent> sTrackEvents = new LinkedList();

    public static void requestTrack(TrackEvent[] trackEvents) {
        synchronized (sLock) {
            for (TrackEvent trackEvent : trackEvents) {
                if (!sTrackEvents.contains(trackEvent)) {
                    sTrackEvents.add(trackEvent);
                }
            }
            Log.d("Added track event:" + sTrackEvents.size());
        }
        if (!sThreadRunning) {
            startTracking();
        }
    }

    public static void requestTrack(TrackEvent trackEvent) {
        synchronized (sLock) {
            if (!sTrackEvents.contains(trackEvent)) {
                sTrackEvents.add(trackEvent);
            }
            Log.d("Added track event:" + sTrackEvents.size());
        }
        if (!sThreadRunning) {
            startTracking();
        }
    }

    public static void requestRetry(TrackEvent trackEvent) {
        synchronized (sLock) {
            if (!sRetryTrackEvents.contains(trackEvent)) {
                trackEvent.retries++;
                if (trackEvent.retries <= 5) {
                    sRetryTrackEvents.add(trackEvent);
                }
            }
            Log.d("Added retry track event:" + sRetryTrackEvents.size());
        }
    }

    /* access modifiers changed from: private */
    public static boolean hasMoreUpdates() {
        boolean hasMore;
        synchronized (sLock) {
            hasMore = !sTrackEvents.isEmpty();
            Log.d("More updates:" + hasMore + " size:" + sTrackEvents.size());
        }
        return hasMore;
    }

    /* access modifiers changed from: private */
    public static TrackEvent getNextUpdate() {
        synchronized (sLock) {
            if (sTrackEvents.peek() == null) {
                return null;
            }
            TrackEvent nextTrackEvent = sTrackEvents.poll();
            return nextTrackEvent;
        }
    }

    public static void startTracking() {
        synchronized (sLock) {
            if (!sThreadRunning) {
                sThreadRunning = true;
                sThread = new Thread(new Runnable() {
                    /* JADX WARNING: CFG modification limit reached, blocks count: 149 */
                    /* Code decompiled incorrectly, please refer to instructions dump. */
                    public void run() {
                        /*
                            r12 = this;
                            r11 = 10000(0x2710, float:1.4013E-41)
                            r10 = 0
                            com.adsdk.sdk.video.TrackerService.sStopped = r10
                        L_0x0006:
                            boolean r7 = com.adsdk.sdk.video.TrackerService.sStopped
                            if (r7 == 0) goto L_0x004f
                            com.adsdk.sdk.video.TrackerService.sStopped = r10
                            com.adsdk.sdk.video.TrackerService.sThreadRunning = r10
                            r7 = 0
                            com.adsdk.sdk.video.TrackerService.sThread = r7
                            return
                        L_0x0017:
                            com.adsdk.sdk.video.TrackEvent r2 = com.adsdk.sdk.video.TrackerService.getNextUpdate()
                            java.lang.StringBuilder r7 = new java.lang.StringBuilder
                            java.lang.String r8 = "Sending tracking :"
                            r7.<init>(r8)
                            java.lang.String r8 = r2.url
                            java.lang.StringBuilder r7 = r7.append(r8)
                            java.lang.String r8 = " Time:"
                            java.lang.StringBuilder r7 = r7.append(r8)
                            long r8 = r2.timestamp
                            java.lang.StringBuilder r7 = r7.append(r8)
                            java.lang.String r8 = " Events left:"
                            java.lang.StringBuilder r7 = r7.append(r8)
                            java.util.Queue r8 = com.adsdk.sdk.video.TrackerService.sTrackEvents
                            int r8 = r8.size()
                            java.lang.StringBuilder r7 = r7.append(r8)
                            java.lang.String r7 = r7.toString()
                            com.adsdk.sdk.Log.d(r7)
                            if (r2 != 0) goto L_0x008d
                        L_0x004f:
                            boolean r7 = com.adsdk.sdk.video.TrackerService.hasMoreUpdates()
                            if (r7 == 0) goto L_0x005b
                            boolean r7 = com.adsdk.sdk.video.TrackerService.sStopped
                            if (r7 == 0) goto L_0x0017
                        L_0x005b:
                            boolean r7 = com.adsdk.sdk.video.TrackerService.sStopped
                            if (r7 != 0) goto L_0x0103
                            java.util.Queue r7 = com.adsdk.sdk.video.TrackerService.sRetryTrackEvents
                            boolean r7 = r7.isEmpty()
                            if (r7 != 0) goto L_0x0103
                            r7 = 30000(0x7530, double:1.4822E-319)
                            java.lang.Thread.sleep(r7)     // Catch:{ Exception -> 0x0109 }
                        L_0x0070:
                            java.lang.Object r8 = com.adsdk.sdk.video.TrackerService.sLock
                            monitor-enter(r8)
                            java.util.Queue r7 = com.adsdk.sdk.video.TrackerService.sTrackEvents     // Catch:{ all -> 0x008a }
                            java.util.Queue r9 = com.adsdk.sdk.video.TrackerService.sRetryTrackEvents     // Catch:{ all -> 0x008a }
                            r7.addAll(r9)     // Catch:{ all -> 0x008a }
                            java.util.Queue r7 = com.adsdk.sdk.video.TrackerService.sRetryTrackEvents     // Catch:{ all -> 0x008a }
                            r7.clear()     // Catch:{ all -> 0x008a }
                            monitor-exit(r8)     // Catch:{ all -> 0x008a }
                            goto L_0x0006
                        L_0x008a:
                            r7 = move-exception
                            monitor-exit(r8)     // Catch:{ all -> 0x008a }
                            throw r7
                        L_0x008d:
                            r6 = 0
                            java.net.URL r6 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00e5 }
                            java.lang.String r7 = r2.url     // Catch:{ MalformedURLException -> 0x00e5 }
                            r6.<init>(r7)     // Catch:{ MalformedURLException -> 0x00e5 }
                            java.lang.String r7 = "Sending conversion Request"
                            com.adsdk.sdk.Log.d(r7)
                            java.lang.StringBuilder r7 = new java.lang.StringBuilder
                            java.lang.String r8 = "Perform tracking HTTP Get Url: "
                            r7.<init>(r8)
                            java.lang.String r8 = r2.url
                            java.lang.StringBuilder r7 = r7.append(r8)
                            java.lang.String r7 = r7.toString()
                            com.adsdk.sdk.Log.d(r7)
                            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient
                            r0.<init>()
                            org.apache.http.params.HttpParams r7 = r0.getParams()
                            org.apache.http.params.HttpConnectionParams.setSoTimeout(r7, r11)
                            org.apache.http.params.HttpParams r7 = r0.getParams()
                            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r7, r11)
                            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
                            java.lang.String r7 = r6.toString()
                            r3.<init>(r7)
                            org.apache.http.HttpResponse r4 = r0.execute(r3)     // Catch:{ Throwable -> 0x00df }
                            org.apache.http.StatusLine r7 = r4.getStatusLine()     // Catch:{ Throwable -> 0x00df }
                            int r7 = r7.getStatusCode()     // Catch:{ Throwable -> 0x00df }
                            r8 = 200(0xc8, float:2.8E-43)
                            if (r7 == r8) goto L_0x00fc
                            com.adsdk.sdk.video.TrackerService.requestRetry(r2)     // Catch:{ Throwable -> 0x00df }
                            goto L_0x004f
                        L_0x00df:
                            r5 = move-exception
                            com.adsdk.sdk.video.TrackerService.requestRetry(r2)
                            goto L_0x004f
                        L_0x00e5:
                            r1 = move-exception
                            java.lang.StringBuilder r7 = new java.lang.StringBuilder
                            java.lang.String r8 = "Wrong tracking url:"
                            r7.<init>(r8)
                            java.lang.String r8 = r2.url
                            java.lang.StringBuilder r7 = r7.append(r8)
                            java.lang.String r7 = r7.toString()
                            com.adsdk.sdk.Log.d(r7)
                            goto L_0x004f
                        L_0x00fc:
                            java.lang.String r7 = "Tracking OK"
                            com.adsdk.sdk.Log.d(r7)     // Catch:{ Throwable -> 0x00df }
                            goto L_0x004f
                        L_0x0103:
                            r7 = 1
                            com.adsdk.sdk.video.TrackerService.sStopped = r7
                            goto L_0x0006
                        L_0x0109:
                            r7 = move-exception
                            goto L_0x0070
                        */
                        throw new UnsupportedOperationException("Method not decompiled: com.adsdk.sdk.video.TrackerService.AnonymousClass1.run():void");
                    }
                });
                sThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                    public void uncaughtException(Thread thread, Throwable ex) {
                        TrackerService.sThreadRunning = false;
                        TrackerService.sThread = null;
                        TrackerService.startTracking();
                    }
                });
                sThread.start();
            }
        }
    }

    public static void release() {
        Log.v("release");
        if (sThread != null) {
            Log.v("release stopping Tracking events thread");
            sStopped = true;
        }
    }
}
