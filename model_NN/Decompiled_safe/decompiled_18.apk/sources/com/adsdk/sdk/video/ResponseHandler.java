package com.adsdk.sdk.video;

import com.inmobi.androidsdk.ai.controller.JSController;
import com.millennialmedia.android.MMAdView;
import java.io.CharArrayWriter;
import java.util.HashMap;
import java.util.Vector;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ResponseHandler extends DefaultHandler {
    private CharArrayWriter contents = new CharArrayWriter();
    private long currentExpiration;
    private TrackerData currentTracker = new TrackerData();
    private boolean insideInterstitial = false;
    private boolean insideMarkup = false;
    private boolean insideVideo = false;
    private boolean insideVideoList = false;
    private RichMediaAd richMediaAd = null;
    HashMap<String, Long> videoList = null;

    public void characters(char[] ch, int start, int length) throws SAXException {
        this.contents.write(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (localName.equals("creative")) {
            if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                throw new SAXException("Creative tag found outside video node");
            }
            getRichMediaAd().getVideo().videoUrl = this.contents.toString().trim();
        } else if (localName.equals("duration")) {
            if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                throw new SAXException("Duration tag found outside video node");
            }
            getRichMediaAd().getVideo().duration = getInteger(this.contents.toString().trim());
        } else if (localName.equals("tracker")) {
            if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                throw new SAXException("Tracker tag found outside video node");
            }
            VideoData video = getRichMediaAd().getVideo();
            this.currentTracker.url = this.contents.toString().trim();
            Vector<String> trackers = null;
            switch (this.currentTracker.type) {
                case 0:
                    trackers = video.getStartEvents();
                    break;
                case 1:
                    trackers = video.getCompleteEvents();
                    break;
                case 2:
                case 3:
                case 4:
                case 5:
                    trackers = video.timeTrackingEvents.get(Integer.valueOf(this.currentTracker.time));
                    if (trackers == null) {
                        trackers = new Vector<>();
                        video.timeTrackingEvents.put(Integer.valueOf(this.currentTracker.time), trackers);
                        break;
                    }
                    break;
                case 6:
                    trackers = video.pauseEvents;
                    break;
                case 7:
                    trackers = video.unpauseEvents;
                    break;
                case 8:
                    trackers = video.muteEvents;
                    break;
                case 9:
                    trackers = video.unmuteEvents;
                    break;
                case 10:
                    trackers = video.skipEvents;
                    break;
                case 11:
                    trackers = video.replayEvents;
                    break;
            }
            if (trackers != null) {
                trackers.add(this.currentTracker.url);
            }
        } else if (localName.equals("htmloverlay")) {
            if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                throw new SAXException("htmloverlay tag found outside video node");
            }
            getRichMediaAd().getVideo().htmlOverlayMarkup = this.contents.toString().trim();
            this.insideMarkup = false;
        } else if (localName.equals("video")) {
            if (this.insideVideoList) {
                this.videoList.put(this.contents.toString().trim(), Long.valueOf(this.currentExpiration));
            }
            this.insideVideo = false;
        } else if (localName.equals("interstitial")) {
            this.insideInterstitial = false;
        } else if (localName.equals("markup")) {
            if (getRichMediaAd() == null || getRichMediaAd().getInterstitial() == null) {
                throw new SAXException("markup tag found outside interstitial node");
            }
            this.insideMarkup = false;
            getRichMediaAd().getInterstitial().interstitialMarkup = this.contents.toString().trim();
        } else if (localName.equals("error")) {
            getRichMediaAd().setType(2);
        }
    }

    public void startDocument() throws SAXException {
        setRichMediaAd(new RichMediaAd());
        this.insideVideoList = false;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (!this.insideMarkup) {
            this.contents.reset();
            if (localName.equals("activevideolist")) {
                this.videoList = new HashMap<>();
                this.insideVideoList = true;
            } else if (localName.equals("ad")) {
                String type = attributes.getValue("type");
                if ("video-to-interstitial".equalsIgnoreCase(type)) {
                    getRichMediaAd().setType(3);
                } else if ("interstitial-to-video".equalsIgnoreCase(type)) {
                    getRichMediaAd().setType(4);
                } else if ("video".equalsIgnoreCase(type)) {
                    getRichMediaAd().setType(5);
                } else if ("interstitial".equalsIgnoreCase(type)) {
                    getRichMediaAd().setType(6);
                } else if ("noAd".equalsIgnoreCase(type)) {
                    getRichMediaAd().setType(2);
                } else {
                    throw new SAXException("Unknown response type " + type);
                }
                String animation = attributes.getValue("animation");
                if ("fade-in".equalsIgnoreCase(animation)) {
                    getRichMediaAd().setAnimation(1);
                } else if ("slide-in-top".equalsIgnoreCase(animation)) {
                    getRichMediaAd().setAnimation(2);
                } else if ("slide-in-bottom".equalsIgnoreCase(animation)) {
                    getRichMediaAd().setAnimation(3);
                } else if ("slide-in-left".equalsIgnoreCase(animation)) {
                    getRichMediaAd().setAnimation(4);
                } else if ("slide-in-right".equalsIgnoreCase(animation)) {
                    getRichMediaAd().setAnimation(5);
                } else if ("flip-in".equalsIgnoreCase(animation)) {
                    getRichMediaAd().setAnimation(6);
                } else {
                    getRichMediaAd().setAnimation(0);
                }
            } else if (localName.equals("video")) {
                if (this.insideVideoList) {
                    this.currentExpiration = getLong(attributes.getValue("expiration")) * 1000;
                    return;
                }
                this.insideVideo = true;
                VideoData video = new VideoData();
                String orientation = attributes.getValue(MMAdView.KEY_ORIENTATION);
                if ("landscape".equalsIgnoreCase(orientation)) {
                    video.orientation = 0;
                } else if ("portrait".equalsIgnoreCase(orientation)) {
                    video.orientation = 1;
                } else {
                    video.orientation = 0;
                }
                if (getRichMediaAd() == null) {
                    throw new SAXException("Video tag found outside document root");
                } else if (getRichMediaAd().getType() != 6 || getRichMediaAd().getType() == 4 || getRichMediaAd().getType() == 3) {
                    getRichMediaAd().setVideo(video);
                } else {
                    throw new SAXException("Found Video tag in an interstitial ad:" + getRichMediaAd().getType());
                }
            } else if (localName.equals("interstitial")) {
                this.insideInterstitial = true;
                InterstitialData inter = new InterstitialData();
                inter.autoclose = getInteger(attributes.getValue("autoclose"));
                String type2 = attributes.getValue("type");
                if ("url".equalsIgnoreCase(type2)) {
                    inter.interstitialType = 0;
                    String url = attributes.getValue("url");
                    if (url == null || url.length() == 0) {
                        throw new SAXException("Empty url for interstitial type " + type2);
                    }
                    inter.interstitialUrl = url;
                } else if ("markup".equalsIgnoreCase(type2)) {
                    inter.interstitialType = 1;
                    this.insideMarkup = true;
                } else {
                    inter.interstitialType = 0;
                    String url2 = attributes.getValue("url");
                    if (url2 == null || url2.length() == 0) {
                        throw new SAXException("Empty url for interstitial type " + type2);
                    }
                    inter.interstitialUrl = url2;
                }
                String orientation2 = attributes.getValue(MMAdView.KEY_ORIENTATION);
                if ("landscape".equalsIgnoreCase(orientation2)) {
                    inter.orientation = 0;
                } else if ("portrait".equalsIgnoreCase(orientation2)) {
                    inter.orientation = 1;
                } else {
                    inter.orientation = 0;
                }
                if (getRichMediaAd() == null) {
                    throw new SAXException("Interstitial tag found outside document root");
                } else if (getRichMediaAd().getType() != 5 || getRichMediaAd().getType() == 4 || getRichMediaAd().getType() == 3) {
                    getRichMediaAd().setInterstitial(inter);
                } else {
                    throw new SAXException("Found Interstitial tag in a video ad:" + getRichMediaAd().getType());
                }
            } else if (localName.equals("creative")) {
                if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                    throw new SAXException("Creative tag found outside video node");
                }
                VideoData video2 = getRichMediaAd().getVideo();
                String delivery = attributes.getValue("delivery");
                if ("progressive".equalsIgnoreCase(delivery)) {
                    video2.delivery = 0;
                } else if ("streaming".equalsIgnoreCase(delivery)) {
                    video2.delivery = 1;
                } else {
                    video2.delivery = 1;
                }
                String type3 = attributes.getValue("type");
                if (type3 == null || type3.length() == 0) {
                    type3 = "application/mp4";
                }
                String display = attributes.getValue("display");
                if (JSController.FULL_SCREEN.equalsIgnoreCase(display)) {
                    video2.display = 0;
                } else if (JSController.STYLE_NORMAL.equalsIgnoreCase(display)) {
                    video2.display = 0;
                } else {
                    video2.display = 0;
                }
                video2.type = type3;
                video2.width = getInteger(attributes.getValue("width"));
                video2.height = getInteger(attributes.getValue("height"));
                video2.bitrate = getInteger(attributes.getValue("bitrate"));
            } else if (localName.equals("skipbutton")) {
                if (this.insideVideo) {
                    if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                        throw new SAXException("skipbutton tag found inside wrong video node");
                    }
                    VideoData video3 = getRichMediaAd().getVideo();
                    video3.showSkipButton = getBoolean(attributes.getValue("show"));
                    video3.showSkipButtonAfter = getInteger(attributes.getValue("showafter"));
                    video3.skipButtonImage = attributes.getValue("graphic");
                } else if (!this.insideInterstitial) {
                } else {
                    if (getRichMediaAd() == null || getRichMediaAd().getInterstitial() == null) {
                        throw new SAXException("skipbutton tag found inside wrong interstitial node");
                    }
                    InterstitialData inter2 = getRichMediaAd().getInterstitial();
                    inter2.showSkipButton = getBoolean(attributes.getValue("show"));
                    inter2.showSkipButtonAfter = getInteger(attributes.getValue("showafter"));
                    inter2.skipButtonImage = attributes.getValue("graphic");
                }
            } else if (localName.equals("navigation")) {
                if (this.insideVideo) {
                    if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                        throw new SAXException("navigation tag found inside wrong video node");
                    }
                    VideoData video4 = getRichMediaAd().getVideo();
                    video4.showNavigationBars = getBoolean(attributes.getValue("show"));
                    video4.allowTapNavigationBars = getBoolean(attributes.getValue("allowtap"));
                } else if (!this.insideInterstitial) {
                } else {
                    if (getRichMediaAd() == null || getRichMediaAd().getInterstitial() == null) {
                        throw new SAXException("navigation tag found inside wrong interstitial node");
                    }
                    InterstitialData inter3 = getRichMediaAd().getInterstitial();
                    inter3.showNavigationBars = getBoolean(attributes.getValue("show"));
                    inter3.allowTapNavigationBars = getBoolean(attributes.getValue("allowtap"));
                }
            } else if (localName.equals("topbar")) {
                if (this.insideVideo) {
                    if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                        throw new SAXException("topbar tag found inside wrong video node");
                    }
                    VideoData video5 = getRichMediaAd().getVideo();
                    video5.showTopNavigationBar = getBoolean(attributes.getValue("show"));
                    video5.topNavigationBarBackground = attributes.getValue("custombackgroundurl");
                } else if (!this.insideInterstitial) {
                } else {
                    if (getRichMediaAd() == null || getRichMediaAd().getInterstitial() == null) {
                        throw new SAXException("topbar tag found inside wrong interstitial node");
                    }
                    InterstitialData inter4 = getRichMediaAd().getInterstitial();
                    inter4.showTopNavigationBar = getBoolean(attributes.getValue("show"));
                    inter4.topNavigationBarBackground = attributes.getValue("custombackgroundurl");
                    String titleType = attributes.getValue("title");
                    if ("fixed".equalsIgnoreCase(titleType)) {
                        inter4.topNavigationBarTitleType = 0;
                        inter4.topNavigationBarTitle = attributes.getValue("titlecontent");
                    } else if ("variable".equalsIgnoreCase(titleType)) {
                        inter4.topNavigationBarTitleType = 1;
                    } else {
                        inter4.topNavigationBarTitleType = 2;
                    }
                }
            } else if (localName.equals("bottombar")) {
                if (this.insideVideo) {
                    if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                        throw new SAXException("bottombar tag found inside wrong video node");
                    }
                    VideoData video6 = getRichMediaAd().getVideo();
                    video6.showBottomNavigationBar = getBoolean(attributes.getValue("show"));
                    video6.bottomNavigationBarBackground = attributes.getValue("custombackgroundurl");
                    video6.showPauseButton = getBoolean(attributes.getValue("pausebutton"));
                    video6.showReplayButton = getBoolean(attributes.getValue("replaybutton"));
                    video6.showTimer = getBoolean(attributes.getValue("timer"));
                    video6.pauseButtonImage = attributes.getValue("pausebuttonurl");
                    video6.playButtonImage = attributes.getValue("playbuttonurl");
                    video6.replayButtonImage = attributes.getValue("replaybuttonurl");
                } else if (!this.insideInterstitial) {
                } else {
                    if (getRichMediaAd() == null || getRichMediaAd().getInterstitial() == null) {
                        throw new SAXException("bottombar tag found inside wrong interstitial node");
                    }
                    InterstitialData inter5 = getRichMediaAd().getInterstitial();
                    inter5.showBottomNavigationBar = getBoolean(attributes.getValue("show"));
                    inter5.bottomNavigationBarBackground = attributes.getValue("custombackgroundurl");
                    inter5.showBackButton = getBoolean(attributes.getValue("backbutton"));
                    inter5.showForwardButton = getBoolean(attributes.getValue("forwardbutton"));
                    inter5.showReloadButton = getBoolean(attributes.getValue("reloadbutton"));
                    inter5.showExternalButton = getBoolean(attributes.getValue("externalbutton"));
                    inter5.showTimer = getBoolean(attributes.getValue("timer"));
                    inter5.backButtonImage = attributes.getValue("backbuttonurl");
                    inter5.forwardButtonImage = attributes.getValue("forwardbuttonurl");
                    inter5.reloadButtonImage = attributes.getValue("reloadbuttonurl");
                    inter5.externalButtonImage = attributes.getValue("externalbuttonurl");
                }
            } else if (localName.equals("navicon")) {
                if (this.insideVideo) {
                    if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                        throw new SAXException("navicon tag found inside wrong video node");
                    }
                    VideoData video7 = getRichMediaAd().getVideo();
                    NavIconData icon = new NavIconData();
                    icon.title = attributes.getValue("title");
                    icon.clickUrl = attributes.getValue("clickurl");
                    icon.iconUrl = attributes.getValue("iconurl");
                    if ("inapp".equalsIgnoreCase(attributes.getValue("opentype"))) {
                        icon.openType = 0;
                    } else {
                        icon.openType = 1;
                    }
                    video7.icons.add(icon);
                } else if (!this.insideInterstitial) {
                } else {
                    if (getRichMediaAd() == null || getRichMediaAd().getInterstitial() == null) {
                        throw new SAXException("navicon tag found inside wrong interstitial node");
                    }
                    InterstitialData inter6 = getRichMediaAd().getInterstitial();
                    NavIconData icon2 = new NavIconData();
                    icon2.title = attributes.getValue("title");
                    icon2.clickUrl = attributes.getValue("clickurl");
                    icon2.iconUrl = attributes.getValue("iconurl");
                    if ("inapp".equalsIgnoreCase(attributes.getValue("opentype"))) {
                        icon2.openType = 0;
                    } else {
                        icon2.openType = 1;
                    }
                    inter6.icons.add(icon2);
                }
            } else if (localName.equals("tracker")) {
                if (!this.insideVideo) {
                    return;
                }
                if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                    throw new SAXException("tracker tag found inside wrong video node");
                }
                VideoData video8 = getRichMediaAd().getVideo();
                this.currentTracker.reset();
                String type4 = attributes.getValue("type");
                if ("start".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 0;
                } else if ("complete".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 1;
                } else if ("midpoint".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 2;
                    this.currentTracker.time = video8.duration / 2;
                } else if ("firstquartile".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 3;
                    this.currentTracker.time = video8.duration / 4;
                } else if ("thirdquartile".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 4;
                    this.currentTracker.time = (video8.duration * 3) / 4;
                } else if ("pause".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 6;
                } else if ("unpause".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 7;
                } else if ("mute".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 8;
                } else if ("unmute".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 9;
                } else if ("replay".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 11;
                } else if ("skip".equalsIgnoreCase(type4)) {
                    this.currentTracker.type = 10;
                } else if (type4 != null && type4.startsWith("sec:")) {
                    this.currentTracker.type = 5;
                    this.currentTracker.time = getInteger(type4.substring(4));
                }
            } else if (localName.equals("htmloverlay") && this.insideVideo) {
                if (getRichMediaAd() == null || getRichMediaAd().getVideo() == null) {
                    throw new SAXException("htmloverlay tag found inside wrong video node");
                }
                VideoData video9 = getRichMediaAd().getVideo();
                this.insideMarkup = true;
                String type5 = attributes.getValue("type");
                if ("url".equalsIgnoreCase(type5)) {
                    video9.htmlOverlayType = 0;
                    String url3 = attributes.getValue("url");
                    if (url3 == null || url3.length() == 0) {
                        throw new SAXException("Empty url for overlay type " + type5);
                    }
                    video9.htmlOverlayUrl = url3;
                } else if ("markup".equalsIgnoreCase(type5)) {
                    video9.htmlOverlayType = 1;
                    this.insideMarkup = true;
                } else {
                    video9.htmlOverlayType = 0;
                    String url4 = attributes.getValue("url");
                    if (url4 == null || url4.length() == 0) {
                        throw new SAXException("Empty url for overlay type " + type5);
                    }
                    video9.htmlOverlayUrl = url4;
                }
                video9.showHtmlOverlayAfter = getInteger(attributes.getValue("showafter"));
                video9.showHtmlOverlay = getBoolean(attributes.getValue("show"));
            }
        }
    }

    private int getInteger(String text) {
        if (text == null) {
            return -1;
        }
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private long getLong(String text) {
        if (text == null) {
            return -1;
        }
        try {
            return Long.parseLong(text);
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private boolean getBoolean(String text) {
        if (text == null) {
            return false;
        }
        try {
            if (Integer.parseInt(text) > 0) {
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public RichMediaAd getRichMediaAd() {
        return this.richMediaAd;
    }

    public void setRichMediaAd(RichMediaAd richMediaAd2) {
        this.richMediaAd = richMediaAd2;
    }
}
