package com.adsdk.sdk.video;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.ConditionVariable;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.MediaController;
import com.adsdk.sdk.Const;
import com.adsdk.sdk.Log;
import com.jumptap.adtag.media.VideoCacheItem;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

public class SDKVideoView extends SurfaceView implements MediaController.MediaPlayerControl {
    private static final int STATE_ERROR = -1;
    private static final int STATE_IDLE = 0;
    private static final int STATE_PAUSED = 4;
    private static final int STATE_PLAYBACK_COMPLETED = 5;
    private static final int STATE_PLAYING = 3;
    private static final int STATE_PREPARED = 2;
    private static final int STATE_PREPARING = 1;
    private MediaPlayer.OnBufferingUpdateListener mBufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            SDKVideoView.this.mCurrentBufferPercentage = percent;
        }
    };
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            SDKVideoView.this.mCurrentState = 5;
            SDKVideoView.this.mTargetState = 5;
            if (SDKVideoView.this.mMediaController != null) {
                SDKVideoView.this.mMediaController.show(0);
            }
            if (SDKVideoView.this.mOnCompletionListener != null) {
                SDKVideoView.this.mOnCompletionListener.onCompletion(SDKVideoView.this.mMediaPlayer);
            }
        }
    };
    private Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentBufferPercentage;
    /* access modifiers changed from: private */
    public int mCurrentState = 0;
    private int mDisplayMode;
    private int mDuration;
    private MediaPlayer.OnErrorListener mErrorListener = new MediaPlayer.OnErrorListener() {
        public boolean onError(MediaPlayer mp, int framework_err, int impl_err) {
            Log.d("Error: " + framework_err + VideoCacheItem.URL_DELIMITER + impl_err);
            SDKVideoView.this.mCurrentState = -1;
            SDKVideoView.this.mTargetState = -1;
            if (SDKVideoView.this.mMediaController != null) {
                SDKVideoView.this.mMediaController.hide();
            }
            if (SDKVideoView.this.mOnErrorListener == null || SDKVideoView.this.mOnErrorListener.onError(SDKVideoView.this.mMediaPlayer, framework_err, impl_err)) {
            }
            return true;
        }
    };
    public Handler mHandler;
    private int mHeight;
    private MediaPlayer.OnInfoListener mInfoListener = new MediaPlayer.OnInfoListener() {
        public boolean onInfo(MediaPlayer mp, int what, int extra) {
            Log.d("Info/Warning: " + what + VideoCacheItem.URL_DELIMITER + extra);
            if (SDKVideoView.this.mOnInfoListener == null || SDKVideoView.this.mOnInfoListener.onInfo(SDKVideoView.this.mMediaPlayer, what, extra)) {
            }
            return true;
        }
    };
    /* access modifiers changed from: private */
    public MediaController mMediaController;
    /* access modifiers changed from: private */
    public MediaPlayer mMediaPlayer = null;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener mOnCompletionListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnErrorListener mOnErrorListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnInfoListener mOnInfoListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener mOnPreparedListener;
    private OnStartListener mOnStartListener;
    /* access modifiers changed from: private */
    public boolean mPlayWhenSurfaceReady;
    MediaPlayer.OnPreparedListener mPreparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mp) {
            Log.d("SDKVideoView onPrepared");
            SDKVideoView.this.mCurrentState = 2;
            if (SDKVideoView.this.mOnPreparedListener != null) {
                SDKVideoView.this.mOnPreparedListener.onPrepared(SDKVideoView.this.mMediaPlayer);
            }
            if (SDKVideoView.this.mMediaController != null) {
                SDKVideoView.this.mMediaController.setEnabled(true);
            }
            int seekToPosition = SDKVideoView.this.mSeekWhenPrepared;
            if (seekToPosition != 0) {
                SDKVideoView.this.seekTo(seekToPosition);
            }
            if (!SDKVideoView.this.mSurfaceReady) {
                Log.d("SDKVideoView onPrepared surface not ready yet");
                return;
            }
            SDKVideoView.this.setVideoDisplaySize();
            if (SDKVideoView.this.mTargetState == 3) {
                SDKVideoView.this.start();
            } else if (SDKVideoView.this.isPlaying()) {
            } else {
                if ((seekToPosition != 0 || SDKVideoView.this.getCurrentPosition() > 0) && SDKVideoView.this.mMediaController != null) {
                    SDKVideoView.this.mMediaController.show(0);
                }
            }
        }
    };
    SurfaceHolder.Callback mSHCallback = new SurfaceHolder.Callback() {
        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("SDKVideoView surfaceChanged");
            SDKVideoView.this.mSurfaceWidth = w;
            SDKVideoView.this.mSurfaceHeight = h;
            SDKVideoView.this.setVideoDisplaySize();
        }

        public void surfaceCreated(SurfaceHolder holder) {
            Log.d("Surface created");
            SDKVideoView.this.mSurfaceReady = true;
            if (SDKVideoView.this.mPlayWhenSurfaceReady) {
                SDKVideoView.this.openVideo();
            }
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("Surface destroyed");
            SDKVideoView.this.mSurfaceReady = false;
            if (SDKVideoView.this.mMediaController != null) {
                SDKVideoView.this.mMediaController.hide();
            }
            SDKVideoView.this.release(true);
        }
    };
    /* access modifiers changed from: private */
    public int mSeekWhenPrepared;
    MediaPlayer.OnVideoSizeChangedListener mSizeChangedListener = new MediaPlayer.OnVideoSizeChangedListener() {
        public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
            Log.d("SDKVideoView OnVideoSizeChangedListener");
        }
    };
    /* access modifiers changed from: private */
    public int mSurfaceHeight;
    /* access modifiers changed from: private */
    public boolean mSurfaceReady = false;
    /* access modifiers changed from: private */
    public int mSurfaceWidth;
    /* access modifiers changed from: private */
    public int mTargetState = 0;
    /* access modifiers changed from: private */
    public HashMap<Integer, Vector<OnTimeEventListener>> mTimeEventListeners = new HashMap<>();
    private Runnable mTimeEventRunnable;
    private Thread mTimeEventThread;
    /* access modifiers changed from: private */
    public ConditionVariable mTimeEventThreadDone = new ConditionVariable(false);
    private Uri mUri;
    private int mVideoHeight;
    private int mVideoWidth;
    private int mWidth;

    public interface OnStartListener {
        void onVideoStart();
    }

    public interface OnTimeEventListener {
        void onTimeEvent(int i);
    }

    public SDKVideoView(Context context, int width, int height, int displayMode) {
        super(context);
        this.mContext = context;
        this.mWidth = width;
        this.mHeight = height;
        this.mDisplayMode = displayMode;
        initVideoView();
    }

    public void destroy() {
        this.mTimeEventThreadDone.open();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = getDefaultSize(this.mVideoWidth, widthMeasureSpec);
        int height = getDefaultSize(this.mVideoHeight, heightMeasureSpec);
        if (this.mVideoWidth > 0 && this.mVideoHeight > 0) {
            if (this.mVideoWidth * height > this.mVideoHeight * width) {
                height = (this.mVideoHeight * width) / this.mVideoWidth;
            } else if (this.mVideoWidth * height < this.mVideoHeight * width) {
                width = (this.mVideoWidth * height) / this.mVideoHeight;
            }
        }
        setMeasuredDimension(width, height);
        Log.d("SDKVideoView onMeasure video size (" + this.mVideoWidth + VideoCacheItem.URL_DELIMITER + this.mVideoHeight + ") surface:(" + this.mSurfaceWidth + VideoCacheItem.URL_DELIMITER + this.mSurfaceHeight + ") Setting size:(" + width + VideoCacheItem.URL_DELIMITER + height + ")");
    }

    private void initVideoView() {
        this.mHandler = new Handler();
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        this.mSurfaceWidth = 0;
        this.mSurfaceHeight = 0;
        this.mSurfaceReady = false;
        setVisibility(0);
        getHolder().addCallback(this.mSHCallback);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.mCurrentState = 0;
        this.mTargetState = 0;
    }

    public void setVideoPath(String path) {
        setVideoURI(Uri.parse(path));
    }

    public void setVideoURI(Uri uri) {
        this.mUri = uri;
        this.mSeekWhenPrepared = 0;
        openVideo();
    }

    public void stopPlayback() {
        if (this.mMediaPlayer != null) {
            if (this.mMediaPlayer.isPlaying()) {
                this.mMediaPlayer.stop();
            }
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            this.mCurrentState = 0;
            this.mTargetState = 0;
        }
    }

    /* access modifiers changed from: private */
    public void openVideo() {
        if (this.mUri != null) {
            this.mPlayWhenSurfaceReady = false;
            if (!this.mSurfaceReady) {
                this.mPlayWhenSurfaceReady = true;
                Log.d("Open Video not starting until surface created");
                return;
            }
            release(false);
            try {
                this.mMediaPlayer = new MediaPlayer();
                this.mMediaPlayer.setDisplay(getHolder());
                this.mMediaPlayer.setOnPreparedListener(this.mPreparedListener);
                this.mMediaPlayer.setOnVideoSizeChangedListener(this.mSizeChangedListener);
                this.mDuration = -1;
                this.mMediaPlayer.setOnCompletionListener(this.mCompletionListener);
                this.mMediaPlayer.setOnErrorListener(this.mErrorListener);
                this.mMediaPlayer.setOnBufferingUpdateListener(this.mBufferingUpdateListener);
                this.mMediaPlayer.setOnInfoListener(this.mInfoListener);
                this.mCurrentBufferPercentage = 0;
                this.mMediaPlayer.setDataSource(this.mContext, this.mUri);
                this.mMediaPlayer.setAudioStreamType(3);
                this.mMediaPlayer.setScreenOnWhilePlaying(true);
                this.mMediaPlayer.prepareAsync();
                this.mTimeEventRunnable = new Runnable() {
                    public void run() {
                        Log.d("Time Event Thread started");
                        do {
                            if (SDKVideoView.this.mMediaPlayer != null && SDKVideoView.this.mCurrentState == 3) {
                                try {
                                    final int time = SDKVideoView.this.mMediaPlayer.getCurrentPosition() / 1000;
                                    Vector<OnTimeEventListener> listeners = (Vector) SDKVideoView.this.mTimeEventListeners.get(Integer.valueOf(time));
                                    if (listeners != null) {
                                        for (int i = 0; i < listeners.size(); i++) {
                                            final OnTimeEventListener l = (OnTimeEventListener) listeners.elementAt(i);
                                            SDKVideoView.this.mHandler.post(new Runnable() {
                                                public void run() {
                                                    l.onTimeEvent(time);
                                                }
                                            });
                                        }
                                        listeners.clear();
                                    }
                                } catch (Exception e) {
                                    Log.e("Time Event Thread error" + e, e);
                                }
                            }
                        } while (!SDKVideoView.this.mTimeEventThreadDone.block(1000));
                        Log.v("Time Event Thread stopped");
                    }
                };
                this.mTimeEventThread = new Thread(this.mTimeEventRunnable);
                this.mTimeEventThread.start();
                this.mCurrentState = 1;
                attachMediaController();
            } catch (IOException ex) {
                Log.w(Const.TAG, "Unable to open content: " + this.mUri, ex);
                this.mCurrentState = -1;
                this.mTargetState = -1;
                this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
            } catch (IllegalArgumentException ex2) {
                Log.w(Const.TAG, "Unable to open content: " + this.mUri, ex2);
                this.mCurrentState = -1;
                this.mTargetState = -1;
                this.mErrorListener.onError(this.mMediaPlayer, 1, 0);
            }
        }
    }

    public void setMediaController(MediaController controller) {
        if (this.mMediaController != null) {
            this.mMediaController.hide();
        }
        this.mMediaController = controller;
        attachMediaController();
    }

    private void attachMediaController() {
        if (this.mMediaPlayer != null && this.mMediaController != null) {
            this.mMediaController.setMediaPlayer(this);
            this.mMediaController.setEnabled(isInPlaybackState());
        }
    }

    /* access modifiers changed from: private */
    public void setVideoDisplaySize() {
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        if (this.mMediaPlayer != null) {
            this.mVideoWidth = this.mMediaPlayer.getVideoWidth();
            this.mVideoHeight = this.mMediaPlayer.getVideoHeight();
        }
        Log.d("SDKVideoView setVideoDisplaySize View Size (" + this.mWidth + VideoCacheItem.URL_DELIMITER + this.mHeight + ") Video size (" + this.mVideoWidth + VideoCacheItem.URL_DELIMITER + this.mVideoHeight + ") surface:(" + this.mSurfaceWidth + VideoCacheItem.URL_DELIMITER + this.mSurfaceHeight + ")");
        if (this.mSurfaceReady && this.mVideoWidth > 0 && this.mVideoHeight > 0) {
            if (this.mDisplayMode == 1) {
                if (this.mVideoWidth * this.mHeight > this.mWidth * this.mVideoHeight) {
                    this.mHeight = (this.mWidth * this.mVideoHeight) / this.mVideoWidth;
                } else if (this.mVideoWidth * this.mHeight < this.mWidth * this.mVideoHeight) {
                    this.mWidth = (this.mHeight * this.mVideoWidth) / this.mVideoHeight;
                }
            }
            getHolder().setFixedSize(this.mWidth, this.mHeight);
        }
        getHolder().setFixedSize(this.mVideoWidth, this.mVideoHeight);
    }

    /* access modifiers changed from: private */
    public void release(boolean cleartargetstate) {
        if (this.mMediaPlayer != null) {
            this.mCurrentState = 0;
            if (this.mTimeEventThread != null) {
                this.mTimeEventThreadDone.open();
                this.mTimeEventThread = null;
            }
            this.mMediaPlayer.reset();
            this.mMediaPlayer.release();
            this.mMediaPlayer = null;
            if (cleartargetstate) {
                this.mTargetState = 0;
            }
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!isInPlaybackState() || this.mMediaController == null || ev.getAction() != 0) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onTrackballEvent(MotionEvent ev) {
        if (!isInPlaybackState() || this.mMediaController == null) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean isKeyCodeSupported = (keyCode == 4 || keyCode == 24 || keyCode == 25 || keyCode == 82 || keyCode == 5 || keyCode == 6) ? false : true;
        if (isInPlaybackState() && isKeyCodeSupported && this.mMediaController != null) {
            if (keyCode == 79 || keyCode == 85) {
                if (this.mMediaPlayer.isPlaying()) {
                    pause();
                    return true;
                }
                start();
                return true;
            } else if (keyCode != 86 || !this.mMediaPlayer.isPlaying()) {
                toggleMediaControlsVisiblity();
            } else {
                pause();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void toggleMediaControlsVisiblity() {
        if (this.mMediaController != null) {
            this.mMediaController.toggle();
        }
    }

    public void start() {
        this.mTargetState = 3;
        if (isInPlaybackState()) {
            Intent intent = new Intent("com.android.music.musicservicecommand");
            intent.putExtra("command", "pause");
            this.mContext.sendBroadcast(intent);
            this.mMediaPlayer.start();
            if (this.mMediaController != null) {
                this.mMediaController.onStart();
            }
            if (this.mCurrentState == 2 && this.mOnStartListener != null) {
                this.mOnStartListener.onVideoStart();
            }
            this.mCurrentState = 3;
        } else if (this.mMediaPlayer == null) {
            openVideo();
        }
    }

    public void pause() {
        if (isInPlaybackState() && this.mMediaPlayer.isPlaying()) {
            this.mMediaPlayer.pause();
            this.mCurrentState = 4;
            if (this.mMediaController != null) {
                this.mMediaController.onPause();
            }
        }
        this.mTargetState = 4;
    }

    public int getDuration() {
        if (!isInPlaybackState()) {
            this.mDuration = -1;
            return this.mDuration;
        } else if (this.mDuration > 0) {
            return this.mDuration;
        } else {
            this.mDuration = this.mMediaPlayer.getDuration();
            return this.mDuration;
        }
    }

    public int getCurrentPosition() {
        if (isInPlaybackState()) {
            return this.mMediaPlayer.getCurrentPosition();
        }
        return 0;
    }

    public void seekTo(int msec) {
        if (isInPlaybackState()) {
            this.mMediaPlayer.seekTo(msec);
            this.mSeekWhenPrepared = 0;
            return;
        }
        this.mSeekWhenPrepared = msec;
    }

    public boolean isPlaying() {
        return isInPlaybackState() && this.mMediaPlayer.isPlaying();
    }

    public int getBufferPercentage() {
        if (this.mMediaPlayer != null) {
            return this.mCurrentBufferPercentage;
        }
        return 0;
    }

    private boolean isInPlaybackState() {
        if (this.mMediaPlayer == null || this.mCurrentState == -1 || this.mCurrentState == 0 || this.mCurrentState == 1) {
            return false;
        }
        return true;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener l) {
        this.mOnPreparedListener = l;
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener l) {
        this.mOnCompletionListener = l;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener l) {
        this.mOnErrorListener = l;
    }

    public void setOnInfoListener(MediaPlayer.OnInfoListener l) {
        this.mOnInfoListener = l;
    }

    public void setOnStartListener(OnStartListener l) {
        this.mOnStartListener = l;
    }

    public void setOnTimeEventListener(int time, OnTimeEventListener onTimeEventListener) {
        Vector<OnTimeEventListener> listeners = this.mTimeEventListeners.get(Integer.valueOf(time));
        if (listeners == null) {
            listeners = new Vector<>();
            this.mTimeEventListeners.put(Integer.valueOf(time), listeners);
        }
        listeners.add(onTimeEventListener);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        Log.i("Video view detached from Window");
        super.onDetachedFromWindow();
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return false;
    }

    public boolean canSeekForward() {
        return true;
    }
}
