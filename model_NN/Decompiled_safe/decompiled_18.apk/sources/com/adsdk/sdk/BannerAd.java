package com.adsdk.sdk;

import com.adsdk.sdk.data.ClickType;

public class BannerAd implements Ad {
    public static final String OTHER = "other";
    public static final String WEB = "web";
    private static final long serialVersionUID = 3271938798582141269L;
    private int bannerHeight;
    private int bannerWidth;
    private ClickType clickType;
    private String clickUrl;
    private String imageUrl;
    private int refresh;
    private boolean scale;
    private boolean skipPreflight;
    private String text;
    private int type;
    private String urlType;

    public int getBannerHeight() {
        return this.bannerHeight;
    }

    public int getBannerWidth() {
        return this.bannerWidth;
    }

    public ClickType getClickType() {
        return this.clickType;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public int getRefresh() {
        return this.refresh;
    }

    public String getText() {
        return this.text;
    }

    public int getType() {
        return this.type;
    }

    public String getUrlType() {
        return this.urlType;
    }

    public boolean isScale() {
        return this.scale;
    }

    public boolean isSkipPreflight() {
        return this.skipPreflight;
    }

    public void setBannerHeight(int bannerHeight2) {
        this.bannerHeight = bannerHeight2;
    }

    public void setBannerWidth(int bannerWidth2) {
        this.bannerWidth = bannerWidth2;
    }

    public void setClickType(ClickType clickType2) {
        this.clickType = clickType2;
    }

    public void setClickUrl(String clickUrl2) {
        this.clickUrl = clickUrl2;
    }

    public void setImageUrl(String imageUrl2) {
        this.imageUrl = imageUrl2;
    }

    public void setRefresh(int refresh2) {
        this.refresh = refresh2;
    }

    public void setScale(boolean scale2) {
        this.scale = scale2;
    }

    public void setSkipPreflight(boolean skipPreflight2) {
        this.skipPreflight = skipPreflight2;
    }

    public void setText(String text2) {
        this.text = text2;
    }

    public void setType(int adType) {
        this.type = adType;
    }

    public void setUrlType(String urlType2) {
        this.urlType = urlType2;
    }

    public String toString() {
        return "Response [refresh=" + this.refresh + ", type=" + this.type + ", bannerWidth=" + this.bannerWidth + ", bannerHeight=" + this.bannerHeight + ", text=" + this.text + ", imageUrl=" + this.imageUrl + ", clickType=" + this.clickType + ", clickUrl=" + this.clickUrl + ", urlType=" + this.urlType + ", scale=" + this.scale + ", skipPreflight=" + this.skipPreflight + "]";
    }
}
