package com.adsdk.sdk;

import com.adsdk.sdk.data.ClickType;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class RequestBannerAd extends RequestAd<BannerAd> {
    public RequestBannerAd() {
    }

    public RequestBannerAd(InputStream xmlArg) {
        this.is = xmlArg;
        Log.d("Parse is null" + (this.is == null));
    }

    private int getInteger(String text) {
        if (text == null) {
            return 0;
        }
        try {
            return Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private String getValue(Document document, String name) {
        Element element = (Element) document.getElementsByTagName(name).item(0);
        if (element != null) {
            NodeList nodeList = element.getChildNodes();
            if (nodeList.getLength() > 0) {
                return nodeList.item(0).getNodeValue();
            }
        }
        return null;
    }

    private boolean getValueAsBoolean(Document document, String name) {
        return "yes".equalsIgnoreCase(getValue(document, name));
    }

    private int getValueAsInt(Document document, String name) {
        return getInteger(getValue(document, name));
    }

    private String convertStreamToString(InputStream is) {
        try {
            return new Scanner(is).useDelimiter("\\A").next();
        } catch (NoSuchElementException e) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public BannerAd parse(InputStream inputStream) throws RequestException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        BannerAd response = new BannerAd();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource src = new InputSource(inputStream);
            if (Log.LOGGING_ENABLED) {
                String sResponse = convertStreamToString(inputStream);
                Log.d("Ad RequestPerform HTTP Response: " + sResponse);
                src = new InputSource(new ByteArrayInputStream(sResponse.getBytes(Const.RESPONSE_ENCODING)));
            }
            src.setEncoding(Const.RESPONSE_ENCODING);
            Document doc = db.parse(src);
            Element element = doc.getDocumentElement();
            if (element == null) {
                throw new RequestException("Cannot parse Response, document is not an xml");
            }
            String errorValue = getValue(doc, "error");
            if (errorValue != null) {
                throw new RequestException("Error Response received: " + errorValue);
            }
            String type = element.getAttribute("type");
            element.normalize();
            if ("imageAd".equalsIgnoreCase(type)) {
                response.setType(0);
                response.setBannerWidth(getValueAsInt(doc, "bannerwidth"));
                response.setBannerHeight(getValueAsInt(doc, "bannerheight"));
                response.setClickType(ClickType.getValue(getValue(doc, "clicktype")));
                response.setClickUrl(getValue(doc, "clickurl"));
                response.setImageUrl(getValue(doc, "imageurl"));
                response.setRefresh(getValueAsInt(doc, "refresh"));
                response.setScale(getValueAsBoolean(doc, "scale"));
                response.setSkipPreflight(getValueAsBoolean(doc, "skippreflight"));
            } else if ("textAd".equalsIgnoreCase(type)) {
                response.setType(1);
                response.setText(getValue(doc, "htmlString"));
                response.setClickType(ClickType.getValue(getValue(doc, "clicktype")));
                response.setClickUrl(getValue(doc, "clickurl"));
                response.setRefresh(getValueAsInt(doc, "refresh"));
                response.setScale(getValueAsBoolean(doc, "scale"));
                response.setSkipPreflight(getValueAsBoolean(doc, "skippreflight"));
            } else if ("noAd".equalsIgnoreCase(type)) {
                response.setType(2);
            } else {
                throw new RequestException("Unknown response type " + type);
            }
            return response;
        } catch (ParserConfigurationException e) {
            throw new RequestException("Cannot parse Response", e);
        } catch (SAXException e2) {
            throw new RequestException("Cannot parse Response", e2);
        } catch (IOException e3) {
            throw new RequestException("Cannot read Response", e3);
        } catch (Throwable t) {
            throw new RequestException("Cannot read Response", t);
        }
    }

    /* access modifiers changed from: package-private */
    public BannerAd parseTestString() throws RequestException {
        return parse(this.is);
    }
}
