package com.adsdk.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.Enumeration;
import java.util.Locale;
import java.util.UUID;

public class Util {
    private static int sFadeInAnimationId = 0;
    private static int sFadeOutAnimationId = 0;
    private static int sSlideInBottomAnimationId = 0;
    private static int sSlideInLeftAnimationId = 0;
    private static int sSlideInRightAnimationId = 0;
    private static int sSlideInTopAnimationId = 0;
    private static int sSlideOutBottomAnimationId = 0;
    private static int sSlideOutLeftAnimationId = 0;
    private static int sSlideOutRightAnimationId = 0;
    private static int sSlideOutTopAnimationId = 0;

    public static boolean isNetworkAvailable(Context ctx) {
        if (ctx.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            return true;
        }
        NetworkInfo info = ((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        if (info == null) {
            return false;
        }
        int netType = info.getType();
        if (netType == 1 || netType == 0) {
            return info.isConnected();
        }
        return false;
    }

    public static String getConnectionType(Context context) {
        NetworkInfo info;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || (info = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
            return Const.CONNECTION_TYPE_UNKNOWN;
        }
        int netType = info.getType();
        int netSubtype = info.getSubtype();
        if (netType == 1) {
            return Const.CONNECTION_TYPE_WIFI;
        }
        if (netType == 6) {
            return Const.CONNECTION_TYPE_WIMAX;
        }
        if (netType != 0) {
            return Const.CONNECTION_TYPE_UNKNOWN;
        }
        switch (netSubtype) {
            case 1:
                return Const.CONNECTION_TYPE_MOBILE_GPRS;
            case 2:
                return Const.CONNECTION_TYPE_MOBILE_EDGE;
            case 3:
                return Const.CONNECTION_TYPE_MOBILE_UMTS;
            case 4:
                return Const.CONNECTION_TYPE_MOBILE_CDMA;
            case 5:
                return Const.CONNECTION_TYPE_MOBILE_EVDO_0;
            case 6:
                return Const.CONNECTION_TYPE_MOBILE_EVDO_A;
            case 7:
                return Const.CONNECTION_TYPE_MOBILE_1xRTT;
            case 8:
                return Const.CONNECTION_TYPE_MOBILE_HSDPA;
            case 9:
                return Const.CONNECTION_TYPE_MOBILE_HSUPA;
            case 10:
                return Const.CONNECTION_TYPE_MOBILE_HSPA;
            case 11:
                return Const.CONNECTION_TYPE_MOBILE_IDEN;
            case 12:
                return Const.CONNECTION_TYPE_MOBILE_EVDO_B;
            case 13:
                return Const.CONNECTION_TYPE_MOBILE_LTE;
            case 14:
                return Const.CONNECTION_TYPE_MOBILE_EHRPD;
            case 15:
                return Const.CONNECTION_TYPE_MOBILE_HSPAP;
            default:
                return Const.CONNECTION_TYPE_MOBILE_UNKNOWN;
        }
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(ex.toString());
        }
        return null;
    }

    public static String getTelephonyDeviceId(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return "";
    }

    public static String getDeviceId(Context context) {
        String androidId = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (androidId == null || androidId.equals("9774d56d682e549c") || androidId.equals("0000000000000000")) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            androidId = prefs.getString(Const.PREFS_DEVICE_ID, null);
            if (androidId == null) {
                try {
                    String uuid = UUID.randomUUID().toString();
                    MessageDigest digest = MessageDigest.getInstance("MD5");
                    digest.update(uuid.getBytes(), 0, uuid.length());
                    androidId = String.format("%032X", new BigInteger(1, digest.digest())).substring(0, 16);
                } catch (Exception e) {
                    Log.d("Could not generate pseudo unique id", e);
                    androidId = "9774d56d682e549c";
                }
                prefs.edit().putString(Const.PREFS_DEVICE_ID, androidId).commit();
            }
            Log.d("Unknown Android ID using pseudo unique id:" + androidId);
        }
        return androidId;
    }

    public static Location getLocation(Context context) {
        LocationManager locationManager;
        int isAccessFineLocation = context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION");
        int isAccessCoarseLocation = context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION");
        if ((isAccessFineLocation == 0 || isAccessCoarseLocation == 0) && (locationManager = (LocationManager) context.getSystemService("location")) != null) {
            if (isAccessFineLocation == 0 && locationManager.isProviderEnabled("gps")) {
                return locationManager.getLastKnownLocation("gps");
            }
            if (isAccessCoarseLocation == 0 && locationManager.isProviderEnabled("network")) {
                return locationManager.getLastKnownLocation("network");
            }
        }
        return null;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getDefaultUserAgentString(android.content.Context r7) {
        /*
            java.lang.Class<android.webkit.WebSettings> r3 = android.webkit.WebSettings.class
            r4 = 2
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0036 }
            r5 = 0
            java.lang.Class<android.content.Context> r6 = android.content.Context.class
            r4[r5] = r6     // Catch:{ Exception -> 0x0036 }
            r5 = 1
            java.lang.Class<android.webkit.WebView> r6 = android.webkit.WebView.class
            r4[r5] = r6     // Catch:{ Exception -> 0x0036 }
            java.lang.reflect.Constructor r0 = r3.getDeclaredConstructor(r4)     // Catch:{ Exception -> 0x0036 }
            r3 = 1
            r0.setAccessible(r3)     // Catch:{ Exception -> 0x0036 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0030 }
            r4 = 0
            r3[r4] = r7     // Catch:{ all -> 0x0030 }
            r4 = 1
            r5 = 0
            r3[r4] = r5     // Catch:{ all -> 0x0030 }
            java.lang.Object r2 = r0.newInstance(r3)     // Catch:{ all -> 0x0030 }
            android.webkit.WebSettings r2 = (android.webkit.WebSettings) r2     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = r2.getUserAgentString()     // Catch:{ all -> 0x0030 }
            r4 = 0
            r0.setAccessible(r4)     // Catch:{ Exception -> 0x0036 }
        L_0x002f:
            return r3
        L_0x0030:
            r3 = move-exception
            r4 = 0
            r0.setAccessible(r4)     // Catch:{ Exception -> 0x0036 }
            throw r3     // Catch:{ Exception -> 0x0036 }
        L_0x0036:
            r1 = move-exception
            android.webkit.WebView r3 = new android.webkit.WebView
            r3.<init>(r7)
            android.webkit.WebSettings r3 = r3.getSettings()
            java.lang.String r3 = r3.getUserAgentString()
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adsdk.sdk.Util.getDefaultUserAgentString(android.content.Context):java.lang.String");
    }

    public static String buildUserAgent() {
        String androidVersion = Build.VERSION.RELEASE;
        String model = Build.MODEL;
        String androidBuild = Build.ID;
        Locale l = Locale.getDefault();
        String language = l.getLanguage();
        String locale = "en";
        if (language != null) {
            locale = language.toLowerCase();
            String country = l.getCountry();
            if (country != null) {
                locale = String.valueOf(locale) + "-" + country.toLowerCase();
            }
        }
        return String.format(Const.USER_AGENT_PATTERN, androidVersion, locale, model, androidBuild);
    }

    public static int getMemoryClass(Context context) {
        try {
            return ((Integer) ActivityManager.class.getMethod("getMemoryClass", new Class[0]).invoke((ActivityManager) context.getSystemService("activity"), new Object[0])).intValue();
        } catch (Exception e) {
            return 16;
        }
    }

    public static void initializeAnimations(Context ctx) {
        Resources r = ctx.getResources();
        sFadeInAnimationId = r.getIdentifier("fade_in", "anim", ctx.getPackageName());
        sFadeOutAnimationId = r.getIdentifier("fade_out", "anim", ctx.getPackageName());
        sSlideInBottomAnimationId = r.getIdentifier("slide_bottom_in", "anim", ctx.getPackageName());
        sSlideOutBottomAnimationId = r.getIdentifier("slide_bottom_out", "anim", ctx.getPackageName());
        sSlideInTopAnimationId = r.getIdentifier("slide_top_in", "anim", ctx.getPackageName());
        sSlideOutTopAnimationId = r.getIdentifier("slide_top_out", "anim", ctx.getPackageName());
        sSlideInLeftAnimationId = r.getIdentifier("slide_left_in", "anim", ctx.getPackageName());
        sSlideOutLeftAnimationId = r.getIdentifier("slide_left_out", "anim", ctx.getPackageName());
        sSlideInRightAnimationId = r.getIdentifier("slide_right_in", "anim", ctx.getPackageName());
        sSlideOutRightAnimationId = r.getIdentifier("slide_right_out", "anim", ctx.getPackageName());
    }

    public static AnimationSet getEnterAnimationSet(int animation) {
        AnimationSet set = new AnimationSet(false);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(3000);
        set.addAnimation(alphaAnimation);
        switch (animation) {
            case 1:
            case 6:
                return set;
            case 2:
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                translateAnimation.setDuration(1000);
                set.addAnimation(translateAnimation);
                return set;
            case 3:
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                translateAnimation2.setDuration(1000);
                set.addAnimation(translateAnimation2);
                return set;
            case 4:
                TranslateAnimation translateAnimation3 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation3.setDuration(1000);
                set.addAnimation(translateAnimation3);
                return set;
            case 5:
                TranslateAnimation translateAnimation4 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation4.setDuration(1000);
                set.addAnimation(translateAnimation4);
                return set;
            default:
                return null;
        }
    }

    public static AnimationSet getExitAnimationSet(int animation) {
        AnimationSet set = new AnimationSet(false);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(3000);
        set.addAnimation(alphaAnimation);
        switch (animation) {
            case 1:
            case 6:
                return set;
            case 2:
                TranslateAnimation translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation.setDuration(1000);
                set.addAnimation(translateAnimation);
                return set;
            case 3:
                TranslateAnimation translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                translateAnimation2.setDuration(1000);
                set.addAnimation(translateAnimation2);
                return set;
            case 4:
                TranslateAnimation translateAnimation3 = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation3.setDuration(1000);
                set.addAnimation(translateAnimation3);
                return set;
            case 5:
                TranslateAnimation translateAnimation4 = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation4.setDuration(1000);
                set.addAnimation(translateAnimation4);
                return set;
            default:
                return null;
        }
    }

    public static int getEnterAnimation(int animation) {
        switch (animation) {
            case 1:
                return sFadeInAnimationId;
            case 2:
                return sSlideInTopAnimationId;
            case 3:
                return sSlideInBottomAnimationId;
            case 4:
                return sSlideInLeftAnimationId;
            case 5:
                return sSlideInRightAnimationId;
            case 6:
                return sFadeInAnimationId;
            default:
                return 0;
        }
    }

    public static int getExitAnimation(int animation) {
        switch (animation) {
            case 1:
                return sFadeOutAnimationId;
            case 2:
                return sSlideOutTopAnimationId;
            case 3:
                return sSlideOutBottomAnimationId;
            case 4:
                return sSlideOutLeftAnimationId;
            case 5:
                return sSlideOutRightAnimationId;
            case 6:
                return sFadeOutAnimationId;
            default:
                return 0;
        }
    }
}
