package com.adsdk.sdk;

import android.content.Context;

public final class Log {
    public static boolean LOGGING_ENABLED = false;

    public static boolean isLoggingEnabled(Context context) {
        int debug = context.getResources().getIdentifier("adsdk_debug_enabled", "string", context.getPackageName());
        if (debug == 0 || !context.getResources().getString(debug).equalsIgnoreCase("true")) {
            return false;
        }
        return true;
    }

    private static boolean isLoggingEnabled() {
        return LOGGING_ENABLED;
    }

    public static void d(String msg) {
        d(Const.TAG, msg);
    }

    public static void d(String tag, String msg) {
        if (isLoggingEnabled()) {
            android.util.Log.d(tag, msg, null);
        }
    }

    public static void d(String tag, String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.d(tag, msg, tr);
        }
    }

    public static void d(String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.d(Const.TAG, msg, tr);
        }
    }

    public static void e(String msg) {
        e(Const.TAG, msg);
    }

    public static void e(String tag, String msg) {
        if (isLoggingEnabled()) {
            android.util.Log.w(tag, msg, null);
        }
    }

    public static void e(String tag, String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.w(tag, msg, tr);
        }
    }

    public static void e(String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.w(Const.TAG, msg, tr);
        }
    }

    public static void i(String msg) {
        i(Const.TAG, msg);
    }

    public static void i(String tag, String msg) {
        if (isLoggingEnabled()) {
            android.util.Log.i(tag, msg, null);
        }
    }

    public static void i(String tag, String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.i(tag, msg, tr);
        }
    }

    public static void i(String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.i(Const.TAG, msg, tr);
        }
    }

    public static void v(String msg) {
        v(Const.TAG, msg);
    }

    public static void v(String tag, String msg) {
        if (isLoggingEnabled()) {
            android.util.Log.v(tag, msg, null);
        }
    }

    public static void v(String tag, String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.v(tag, msg, tr);
        }
    }

    public static void v(String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.v(Const.TAG, msg, tr);
        }
    }

    public static void w(String msg) {
        w(Const.TAG, msg);
    }

    public static void w(String tag, String msg) {
        if (isLoggingEnabled()) {
            android.util.Log.w(tag, msg, null);
        }
    }

    public static void w(String tag, String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.w(tag, msg, tr);
        }
    }

    public static void w(String msg, Throwable tr) {
        if (isLoggingEnabled()) {
            android.util.Log.w(Const.TAG, msg, tr);
        }
    }
}
