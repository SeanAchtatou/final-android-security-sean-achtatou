package com.adsdk.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import com.adsdk.sdk.video.ResourceManager;
import com.adsdk.sdk.video.RichMediaActivity;
import com.adsdk.sdk.video.RichMediaAd;
import com.adsdk.sdk.video.TrackerService;
import java.io.InputStream;
import java.lang.Thread;
import java.util.HashMap;

public class AdManager {
    private static Context mContext;
    private static HashMap<Long, AdManager> sRunningAds = new HashMap<>();
    private boolean mEnabled = true;
    /* access modifiers changed from: private */
    public Handler mHandler;
    private boolean mIncludeLocation;
    /* access modifiers changed from: private */
    public AdListener mListener;
    private String mPublisherId;
    private AdRequest mRequest = null;
    /* access modifiers changed from: private */
    public Thread mRequestThread;
    /* access modifiers changed from: private */
    public RichMediaAd mResponse;
    private String mUniqueId1;
    private String mUniqueId2;
    private String mUserAgent;
    private String requestURL;

    public static AdManager getAdManager(RichMediaAd ad) {
        AdManager adManager = sRunningAds.remove(Long.valueOf(ad.getTimestamp()));
        if (adManager == null) {
            Log.d("Cannot find AdManager with running ad:" + ad.getTimestamp());
        }
        return adManager;
    }

    public static void closeRunningAd(RichMediaAd ad, boolean result) {
        AdManager adManager = sRunningAds.remove(Long.valueOf(ad.getTimestamp()));
        if (adManager == null) {
            Log.d("Cannot find AdManager with running ad:" + ad.getTimestamp());
            return;
        }
        Log.d("Notify closing event to AdManager with running ad:" + ad.getTimestamp());
        adManager.notifyAdClose(ad, result);
    }

    public void release() {
        TrackerService.release();
        ResourceManager.cancel();
    }

    public AdManager(Context ctx, String requestURL2, String publisherId, boolean includeLocation) throws IllegalArgumentException {
        setmContext(ctx);
        this.requestURL = requestURL2;
        this.mPublisherId = publisherId;
        this.mIncludeLocation = includeLocation;
        this.mRequestThread = null;
        this.mHandler = new Handler();
        initialize();
    }

    public void setListener(AdListener listener) {
        this.mListener = listener;
    }

    public void requestAd() {
        if (!this.mEnabled) {
            Log.w("Cannot request rich adds on low memory devices");
        } else if (this.mRequestThread == null) {
            Log.d("Requesting Ad (v4.0-1.0)");
            this.mResponse = null;
            this.mRequestThread = new Thread(new Runnable() {
                public void run() {
                    while (ResourceManager.isDownloading()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                        }
                    }
                    Log.d("starting request thread");
                    try {
                        AdManager.this.mResponse = (RichMediaAd) new RequestRichMediaAd().sendRequest(AdManager.this.getRequest());
                        if (AdManager.this.mResponse.getVideo() != null && Build.VERSION.SDK_INT < 8) {
                            Log.d("Not capable of video");
                            AdManager.this.notifyNoAdFound();
                            Log.d("finishing ad request thread");
                            AdManager.this.mRequestThread = null;
                        } else if (AdManager.this.mResponse.getType() == 3 || AdManager.this.mResponse.getType() == 4 || AdManager.this.mResponse.getType() == 5 || AdManager.this.mResponse.getType() == 6) {
                            Log.d("response OK received");
                            if (AdManager.this.mListener != null) {
                                AdManager.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        AdManager.this.mListener.adLoadSucceeded(AdManager.this.mResponse);
                                    }
                                });
                            }
                            Log.d("finishing ad request thread");
                            AdManager.this.mRequestThread = null;
                        } else {
                            if (AdManager.this.mResponse.getType() == 2) {
                                Log.d("response NO AD received");
                                if (AdManager.this.mListener != null) {
                                    AdManager.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            AdManager.this.notifyNoAdFound();
                                        }
                                    });
                                }
                            } else {
                                Log.w("response BANNER received");
                                if (AdManager.this.mListener != null) {
                                    AdManager.this.mHandler.post(new Runnable() {
                                        public void run() {
                                            AdManager.this.notifyNoAdFound();
                                        }
                                    });
                                }
                            }
                            Log.d("finishing ad request thread");
                            AdManager.this.mRequestThread = null;
                        }
                    } catch (Throwable t) {
                        AdManager.this.mResponse = new RichMediaAd();
                        AdManager.this.mResponse.setType(-1);
                        if (AdManager.this.mListener != null) {
                            Log.d("Ad Load failed. Reason:" + t);
                            t.printStackTrace();
                            AdManager.this.mHandler.post(new Runnable() {
                                public void run() {
                                    AdManager.this.notifyNoAdFound();
                                }
                            });
                        }
                    }
                }
            });
            this.mRequestThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable ex) {
                    AdManager.this.mResponse = new RichMediaAd();
                    AdManager.this.mResponse.setType(-1);
                    Log.e("Handling exception in ad request thread", ex);
                    AdManager.this.mRequestThread = null;
                }
            });
            this.mRequestThread.start();
        } else {
            Log.w("Request thread already running");
        }
    }

    public void setRequestURL(String requestURL2) {
        this.requestURL = requestURL2;
    }

    public void requestAd(final InputStream xml) {
        if (!this.mEnabled) {
            Log.w("Cannot request rich adds on low memory devices");
        } else if (this.mRequestThread == null) {
            Log.d("Requesting Ad (v4.0-1.0)");
            this.mResponse = null;
            this.mRequestThread = new Thread(new Runnable() {
                public void run() {
                    while (ResourceManager.isDownloading()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                        }
                    }
                    Log.d("starting request thread");
                    try {
                        AdManager.this.mResponse = (RichMediaAd) new RequestRichMediaAd(xml).sendRequest(AdManager.this.getRequest());
                        if (AdManager.this.mResponse.getType() != 2) {
                            Log.d("response OK received");
                            if (AdManager.this.mListener != null) {
                                AdManager.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        AdManager.this.mListener.adLoadSucceeded(AdManager.this.mResponse);
                                    }
                                });
                            }
                        } else {
                            Log.d("response NO AD received");
                            if (AdManager.this.mListener != null) {
                                AdManager.this.mHandler.post(new Runnable() {
                                    public void run() {
                                        AdManager.this.notifyNoAdFound();
                                    }
                                });
                            }
                        }
                    } catch (Throwable t) {
                        AdManager.this.mResponse = new RichMediaAd();
                        AdManager.this.mResponse.setType(-1);
                        if (AdManager.this.mListener != null) {
                            Log.d("Ad Load failed. Reason:" + t);
                            t.printStackTrace();
                            AdManager.this.mHandler.post(new Runnable() {
                                public void run() {
                                    AdManager.this.notifyNoAdFound();
                                }
                            });
                        }
                    }
                    Log.d("finishing ad request thread");
                    AdManager.this.mRequestThread = null;
                }
            });
            this.mRequestThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable ex) {
                    AdManager.this.mResponse = new RichMediaAd();
                    AdManager.this.mResponse.setType(-1);
                    Log.e("Handling exception in ad request thread", ex);
                    AdManager.this.mRequestThread = null;
                }
            });
            this.mRequestThread.start();
        } else {
            Log.w("Request thread already running");
        }
    }

    public boolean isAdLoaded() {
        return this.mResponse != null;
    }

    public void requestAdAndShow(long timeout) {
        AdListener l = this.mListener;
        this.mListener = null;
        requestAd();
        long now = System.currentTimeMillis();
        long timeoutTime = now + timeout;
        while (!isAdLoaded() && now < timeoutTime) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
            }
            now = System.currentTimeMillis();
        }
        this.mListener = l;
        showAd();
    }

    public void showAd() {
        Activity activity = (Activity) getContext();
        if (this.mResponse == null || this.mResponse.getType() == 2 || this.mResponse.getType() == -1) {
            notifyAdShown(this.mResponse, false);
            return;
        }
        RichMediaAd ad = this.mResponse;
        boolean result = false;
        try {
            if (Util.isNetworkAvailable(getContext())) {
                ad.setTimestamp(System.currentTimeMillis());
                Log.v("Showing Ad:" + ad);
                Intent intent = new Intent(activity, RichMediaActivity.class);
                intent.putExtra(Const.AD_EXTRA, ad);
                activity.startActivityForResult(intent, 0);
                RichMediaActivity.setActivityAnimation(activity, Util.getEnterAnimation(ad.getAnimation()), Util.getExitAnimation(ad.getAnimation()));
                result = true;
                sRunningAds.put(Long.valueOf(ad.getTimestamp()), this);
            } else {
                Log.d("No network available. Cannot show Ad.");
            }
        } catch (Exception e) {
            Log.e("Unknown exception when showing Ad", e);
        } finally {
            notifyAdShown(ad, result);
        }
    }

    private void initialize() throws IllegalArgumentException {
        this.mUserAgent = Util.getDefaultUserAgentString(getContext());
        Log.LOGGING_ENABLED = Log.isLoggingEnabled(getmContext());
        Log.d("Ad SDK Version:4.0");
        this.mUniqueId1 = Util.getTelephonyDeviceId(getContext());
        this.mUniqueId2 = Util.getDeviceId(getContext());
        if (this.mPublisherId == null || this.mPublisherId.length() == 0) {
            Log.e("Publisher Id cannot be null or empty");
            throw new IllegalArgumentException("User Id cannot be null or empty");
        } else if (this.mUniqueId2 == null || this.mUniqueId2.length() == 0) {
            Log.e("Cannot get system device Id");
            throw new IllegalArgumentException("System Device Id cannot be null or empty");
        } else {
            Log.d("AdManager Publisher Id:" + this.mPublisherId + " Device Id:" + this.mUniqueId1 + " DeviceId2:" + this.mUniqueId2);
            this.mEnabled = Util.getMemoryClass(getContext()) > 16;
            Util.initializeAnimations(getContext());
        }
    }

    /* access modifiers changed from: private */
    public void notifyNoAdFound() {
        if (this.mListener != null) {
            Log.d("No ad found.");
            this.mHandler.post(new Runnable() {
                public void run() {
                    AdManager.this.mListener.noAdFound();
                }
            });
        }
        this.mResponse = null;
    }

    private void notifyAdShown(final RichMediaAd ad, final boolean ok) {
        if (this.mListener != null) {
            Log.d("Ad Shown. Result:" + ok);
            this.mHandler.post(new Runnable() {
                public void run() {
                    AdManager.this.mListener.adShown(ad, ok);
                }
            });
        }
        this.mResponse = null;
    }

    private void notifyAdClose(final RichMediaAd ad, final boolean ok) {
        if (this.mListener != null) {
            Log.d("Ad Close. Result:" + ok);
            this.mHandler.post(new Runnable() {
                public void run() {
                    AdManager.this.mListener.adClosed(ad, ok);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public AdRequest getRequest() {
        if (this.mRequest == null) {
            this.mRequest = new AdRequest();
            this.mRequest.setDeviceId(this.mUniqueId1);
            this.mRequest.setDeviceId2(this.mUniqueId2);
            this.mRequest.setPublisherId(this.mPublisherId);
            this.mRequest.setUserAgent(this.mUserAgent);
            this.mRequest.setUserAgent2(Util.buildUserAgent());
        }
        Location location = null;
        if (this.mIncludeLocation) {
            location = Util.getLocation(getContext());
        }
        if (location != null) {
            Log.d("location is longitude: " + location.getLongitude() + ", latitude: " + location.getLatitude());
            this.mRequest.setLatitude(location.getLatitude());
            this.mRequest.setLongitude(location.getLongitude());
        } else {
            this.mRequest.setLatitude(0.0d);
            this.mRequest.setLongitude(0.0d);
        }
        this.mRequest.setConnectionType(Util.getConnectionType(getContext()));
        this.mRequest.setIpAddress(Util.getLocalIpAddress());
        this.mRequest.setTimestamp(System.currentTimeMillis());
        this.mRequest.setType(1);
        this.mRequest.setRequestURL(this.requestURL);
        Log.d("Getting new request:" + this.mRequest.toString());
        return this.mRequest;
    }

    private Context getContext() {
        return getmContext();
    }

    private static Context getmContext() {
        return mContext;
    }

    private static void setmContext(Context mContext2) {
        mContext = mContext2;
    }
}
