package com.adsdk.sdk;

import android.net.Uri;
import android.os.Build;
import com.google.ads.AdActivity;

public class AdRequest {
    public static final int BANNER = 0;
    private static final String REQUEST_TYPE_ANDROID = "android_app";
    public static final int VAD = 1;
    private String connectionType;
    private String deviceId;
    private String deviceId2;
    private String headers;
    private String ipAddress;
    private double latitude = 0.0d;
    private String listAds;
    private double longitude = 0.0d;
    private String protocolVersion;
    private String publisherId;
    private String requestURL;
    private long timestamp;
    private int type = -1;
    private String userAgent;
    private String userAgent2;

    public String getAndroidVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getConnectionType() {
        return this.connectionType;
    }

    public String getDeviceId() {
        if (this.deviceId == null) {
            return "";
        }
        return this.deviceId;
    }

    public String getDeviceId2() {
        return this.deviceId2;
    }

    public String getDeviceMode() {
        return Build.MODEL;
    }

    public String getHeaders() {
        if (this.headers == null) {
            return "";
        }
        return this.headers;
    }

    public String getIpAddress() {
        if (this.ipAddress == null) {
            return "";
        }
        return this.ipAddress;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public String getListAds() {
        if (this.listAds != null) {
            return this.listAds;
        }
        return "";
    }

    public double getLongitude() {
        return this.longitude;
    }

    public String getProtocolVersion() {
        if (this.protocolVersion == null) {
            return Const.PROTOCOL_VERSION;
        }
        return this.protocolVersion;
    }

    public String getPublisherId() {
        if (this.publisherId == null) {
            return "";
        }
        return this.publisherId;
    }

    public String getRequestType() {
        return REQUEST_TYPE_ANDROID;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getUserAgent() {
        if (this.userAgent == null) {
            return "";
        }
        return this.userAgent;
    }

    public String getUserAgent2() {
        if (this.userAgent2 == null) {
            return "";
        }
        return this.userAgent2;
    }

    public void setConnectionType(String connectionType2) {
        this.connectionType = connectionType2;
    }

    public void setDeviceId(String deviceId3) {
        this.deviceId = deviceId3;
    }

    public void setDeviceId2(String deviceId22) {
        this.deviceId2 = deviceId22;
    }

    public void setHeaders(String headers2) {
        this.headers = headers2;
    }

    public void setIpAddress(String ipAddress2) {
        this.ipAddress = ipAddress2;
    }

    public void setLatitude(double latitude2) {
        this.latitude = latitude2;
    }

    public void setListAds(String listAds2) {
        this.listAds = listAds2;
    }

    public void setLongitude(double longitude2) {
        this.longitude = longitude2;
    }

    public void setProtocolVersion(String protocolVersion2) {
        this.protocolVersion = protocolVersion2;
    }

    public void setPublisherId(String publisherId2) {
        this.publisherId = publisherId2;
    }

    public void setTimestamp(long timestamp2) {
        this.timestamp = timestamp2;
    }

    public void setUserAgent(String userAgent3) {
        this.userAgent = userAgent3;
    }

    public void setUserAgent2(String userAgent3) {
        this.userAgent2 = userAgent3;
    }

    public String toString() {
        return toUri().toString();
    }

    public Uri toUri() {
        Uri.Builder b = Uri.parse(getRequestURL()).buildUpon();
        b.appendQueryParameter("rt", getRequestType());
        b.appendQueryParameter("v", getProtocolVersion());
        b.appendQueryParameter(AdActivity.INTENT_ACTION_PARAM, getIpAddress());
        b.appendQueryParameter(AdActivity.URL_PARAM, getUserAgent());
        b.appendQueryParameter("u2", getUserAgent2());
        b.appendQueryParameter("s", getPublisherId());
        b.appendQueryParameter(AdActivity.ORIENTATION_PARAM, getDeviceId());
        b.appendQueryParameter("o2", getDeviceId2());
        b.appendQueryParameter("t", Long.toString(getTimestamp()));
        b.appendQueryParameter("connection_type", getConnectionType());
        b.appendQueryParameter("listads", getListAds());
        b.appendQueryParameter("c.mraid", "0");
        switch (getType()) {
            case 0:
                b.appendQueryParameter("sdk", "banner");
                break;
            case 1:
                b.appendQueryParameter("sdk", "vad");
                break;
        }
        return b.build();
    }

    public String getRequestURL() {
        return this.requestURL;
    }

    public void setRequestURL(String requestURL2) {
        this.requestURL = requestURL2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }
}
