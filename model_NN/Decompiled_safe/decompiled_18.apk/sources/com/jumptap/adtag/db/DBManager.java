package com.jumptap.adtag.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import com.jumptap.adtag.events.EventType;
import com.jumptap.adtag.events.JtEvent;
import com.jumptap.adtag.media.VideoCacheItem;
import com.jumptap.adtag.utils.JtAdManager;
import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private static final String ADID_COLUMN = "adid";
    private static final String CREATE_PENDING_EVENTS_QUERY = "CREATE TABLE IF NOT EXISTS pending_events(id INTEGER PRIMARY KEY, eventType TEXT, date TEXT, url TEXT);";
    private static final String CREATE_VIDEO_CACHE_QUERY = "CREATE TABLE IF NOT EXISTS video_cache(id INTEGER PRIMARY KEY, adid INTEGER,date TEXT);";
    private static final String DATABASE_NAME = "jt_ad_view.db";
    private static final int DATABASE_VERSION = 5;
    private static final String DATE_COLUMN = "date";
    private static final String DROP_QUERY = "DROP TABLE IF EXISTS pending_events; DROP TABLE IF EXISTS video_cache;";
    private static final String EVENT_TYPE_COLUMN = "eventType";
    private static final String ID_COLUMN = "id";
    private static final String INSERT_EVENT_QUERY = "insert into pending_events(eventType,date,url) values (?,?,?)";
    private static final String INSERT_VIDEO_CACHE_QUERY = "insert into video_cache(adid,date) values (?,?)";
    private static final String PENDING_EVENTS_TABLE_NAME = "pending_events";
    private static final String URL_COLUMN = "url";
    private static final String VIDEO_CACHE_TABLE_NAME = "video_cache";
    private static DBManager dbManagerInstance;
    private Context context;
    private SQLiteDatabase db;
    private SQLiteStatement insertStmt;

    private DBManager(Context context2) {
        this.context = context2;
        openDB();
    }

    public static DBManager getInstance(Context context2) {
        if (dbManagerInstance == null) {
            dbManagerInstance = new DBManager(context2);
        }
        return dbManagerInstance;
    }

    public void close() {
        this.db.close();
    }

    public long insertEvent(JtEvent event) {
        long executeInsert;
        synchronized (dbManagerInstance) {
            if (!this.db.isOpen()) {
                openDB();
            }
            this.insertStmt = this.db.compileStatement(INSERT_EVENT_QUERY);
            this.insertStmt.bindString(1, event.getEventType().name());
            this.insertStmt.bindString(2, event.getDate());
            this.insertStmt.bindString(3, event.getUrl());
            executeInsert = this.insertStmt.executeInsert();
        }
        return executeInsert;
    }

    public void deleteAllPendingEvents() {
        synchronized (dbManagerInstance) {
            try {
                if (!this.db.isOpen()) {
                    openDB();
                }
                this.db.delete(PENDING_EVENTS_TABLE_NAME, null, null);
            } catch (IllegalStateException e) {
                Log.d(JtAdManager.JT_AD, "Failed to delete all pending events table", e);
            }
        }
    }

    public void deleteEventById(int id) {
        synchronized (dbManagerInstance) {
            try {
                if (!this.db.isOpen()) {
                    openDB();
                }
                this.db.delete(PENDING_EVENTS_TABLE_NAME, "id=?", new String[]{Integer.toString(id)});
            } catch (IllegalStateException e) {
                Log.d(JtAdManager.JT_AD, "Failed to delete event by id=" + id, e);
            }
        }
    }

    public List<JtEvent> selectAllEvents() {
        ArrayList arrayList;
        synchronized (dbManagerInstance) {
            Cursor cursor = null;
            List<Integer> bads = null;
            arrayList = new ArrayList();
            if (!this.db.isOpen()) {
                openDB();
            }
            cursor = this.db.query(PENDING_EVENTS_TABLE_NAME, new String[]{ID_COLUMN, EVENT_TYPE_COLUMN, "date", URL_COLUMN}, null, null, null, null, "id asc");
            if (cursor.moveToFirst()) {
                do {
                    List<Integer> bads2 = bads;
                    try {
                        int id = cursor.getInt(0);
                        if (cursor.getString(1) != null) {
                            arrayList.add(new JtEvent(id, cursor.getString(3), EventType.valueOf(cursor.getString(1)), cursor.getString(2)));
                            bads = bads2;
                        } else {
                            if (bads2 == null) {
                                bads = new ArrayList<>();
                            } else {
                                bads = bads2;
                            }
                            try {
                                bads.add(new Integer(id));
                            } catch (Throwable th) {
                                th = th;
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        bads = bads2;
                        if (cursor != null) {
                            if (!cursor.isClosed()) {
                                cursor.close();
                            }
                        }
                        if (bads != null && bads.size() > 10) {
                            for (Integer i : bads) {
                                deleteEventById(i.intValue());
                            }
                        }
                        throw th;
                    }
                } while (cursor.moveToNext());
            }
            if (cursor != null) {
                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
            if (bads != null && bads.size() > 10) {
                for (Integer i2 : bads) {
                    deleteEventById(i2.intValue());
                }
            }
        }
        return arrayList;
    }

    public long insertVideoCacheItem(VideoCacheItem item) {
        long executeInsert;
        synchronized (dbManagerInstance) {
            if (!this.db.isOpen()) {
                openDB();
            }
            this.insertStmt = this.db.compileStatement(INSERT_VIDEO_CACHE_QUERY);
            this.insertStmt.bindString(1, item.getAdID());
            this.insertStmt.bindString(2, item.getDate());
            executeInsert = this.insertStmt.executeInsert();
        }
        return executeInsert;
    }

    public List<VideoCacheItem> selectAllVideoCacheItems() {
        List<VideoCacheItem> list;
        synchronized (dbManagerInstance) {
            list = new ArrayList<>();
            if (!this.db.isOpen()) {
                openDB();
            }
            Cursor cursor = this.db.query(VIDEO_CACHE_TABLE_NAME, new String[]{ID_COLUMN, ADID_COLUMN, "date"}, null, null, null, null, "id desc");
            if (cursor.moveToFirst()) {
                do {
                    list.add(new VideoCacheItem(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
                } while (cursor.moveToNext());
            }
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return list;
    }

    public void deleteAllVideoCacheItems() {
        synchronized (dbManagerInstance) {
            try {
                if (!this.db.isOpen()) {
                    openDB();
                }
                this.db.delete(VIDEO_CACHE_TABLE_NAME, null, null);
            } catch (IllegalStateException e) {
                Log.d(JtAdManager.JT_AD, "Failed to delete all video cache items table", e);
            }
        }
    }

    public void deleteVideoCacheItemByAdId(String adid) {
        synchronized (dbManagerInstance) {
            try {
                if (!this.db.isOpen()) {
                    openDB();
                }
                this.db.delete(VIDEO_CACHE_TABLE_NAME, "adid=?", new String[]{adid});
            } catch (IllegalStateException e) {
                Log.d(JtAdManager.JT_AD, "Failed to delete video chache item by adid=" + adid, e);
            }
        }
    }

    public void deleteVideoCacheItemById(int id) {
        synchronized (dbManagerInstance) {
            try {
                if (!this.db.isOpen()) {
                    openDB();
                }
                this.db.delete(VIDEO_CACHE_TABLE_NAME, "id=?", new String[]{Integer.toString(id)});
            } catch (IllegalStateException e) {
                Log.d(JtAdManager.JT_AD, "Failed to delete video chache item by id=" + id, e);
            }
        }
    }

    private void openDB() {
        this.db = new JtSQLiteOpenHelper(this.context).getWritableDatabase();
    }

    private static class JtSQLiteOpenHelper extends SQLiteOpenHelper {
        JtSQLiteOpenHelper(Context context) {
            super(context, DBManager.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, 5);
        }

        public void onCreate(SQLiteDatabase db) {
            Log.w(JtAdManager.JT_AD, "Creating database.");
            db.execSQL(DBManager.CREATE_PENDING_EVENTS_QUERY);
            db.execSQL(DBManager.CREATE_VIDEO_CACHE_QUERY);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(JtAdManager.JT_AD, "Upgrading database, this will drop tables and recreate.");
            db.execSQL(DBManager.DROP_QUERY);
            onCreate(db);
        }
    }
}
