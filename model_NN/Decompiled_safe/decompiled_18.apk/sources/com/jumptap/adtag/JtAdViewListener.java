package com.jumptap.adtag;

public interface JtAdViewListener {
    void onAdError(JtAdView jtAdView, int i, int i2);

    void onBannerClicked(JtAdView jtAdView, int i);

    void onBeginAdInteraction(JtAdView jtAdView, int i);

    void onContract(JtAdView jtAdView, int i);

    void onEndAdInteraction(JtAdView jtAdView, int i);

    void onExpand(JtAdView jtAdView, int i);

    void onFocusChange(JtAdView jtAdView, int i, boolean z);

    void onHide(JtAdView jtAdView, int i);

    void onInterstitialDismissed(JtAdView jtAdView, int i);

    void onLaunchActivity(JtAdView jtAdView, int i);

    void onNewAd(JtAdView jtAdView, int i, String str);

    void onNoAdFound(JtAdView jtAdView, int i);

    void onReturnFromActivity(JtAdView jtAdView, int i);
}
