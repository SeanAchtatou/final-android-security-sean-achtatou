package com.jumptap.adtag;

import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import com.jumptap.adtag.utils.JtAdManager;

public class JtAdWidgetSettings implements Cloneable {
    private static final String ANDROID_OS = "Linux";
    private static final String BASE_JT_URL = "http://a.jumptap.com/a/ads?textOnly=f";
    private static final String DEFAULT_LANG = "en";
    private static final int DEFAULT_REFRESH_PERIOD = 60;
    private static final String DISMISS_BUTTON_DEFAULT_LABEL = "Close";
    private static final int INTERSTITIAL_SHOW_DEFAULT_TIME = 10;
    private static final String JT_BUNDLE = "com.jumptap.adtag-android";
    private static final String JT_LIB_VER = "2.4.0.5";
    private static final String TAPLINK_VER = "v28";
    private static final String XHTML = "xhtml";
    private String adFormat = XHTML;
    private String adultContentType = AdultContent.NOT_ALLOWED;
    private String age = "";
    private Bitmap alternateImage;
    private String appId = "";
    private String appVer = "";
    private int backgroundColor = -1;
    private String bundleVersion = JT_BUNDLE;
    private String country = "";
    private String dismissButtonLabel = DISMISS_BUTTON_DEFAULT_LABEL;
    private String gender = "";
    private int height = 50;
    private String hhi = "";
    private String host = BASE_JT_URL;
    private int interstitialshowTime = 10;
    private String jtLibVer = JT_LIB_VER;
    private String language = DEFAULT_LANG;
    private String os = ANDROID_OS;
    private String postalCode = "";
    private String publisherId = null;
    private int refreshPeriod = 60;
    private boolean shouldDebugNetworkTraffic = false;
    private boolean shouldSendLocation = true;
    private String siteId = null;
    private String spotId = null;
    private String userAgent = null;
    private String version = TAPLINK_VER;
    private int width = 320;

    public interface AdultContent {
        public static final String ALLOWED = "allowed";
        public static final String NOT_ALLOWED = "notallowed";
        public static final String ONLY = "only";
    }

    protected JtAdWidgetSettings() {
    }

    protected JtAdWidgetSettings(String publisherId2) {
        this.publisherId = publisherId2;
    }

    public JtAdWidgetSettings copy() {
        try {
            return (JtAdWidgetSettings) clone();
        } catch (CloneNotSupportedException e) {
            Log.d(JtAdManager.JT_AD, e.getMessage());
            return null;
        }
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public void setPublisherId(String publisherId2) {
        this.publisherId = publisherId2;
    }

    public String getSiteId() {
        return this.siteId;
    }

    public void setSiteId(String siteId2) {
        this.siteId = siteId2;
    }

    public String getSpotId() {
        return this.spotId;
    }

    public void setSpotId(String spotId2) {
        this.spotId = spotId2;
    }

    public String getHostURL() {
        return this.host;
    }

    public void setHostURL(String host2) {
        this.host = host2;
    }

    public String getUserAgent(WebView webView) {
        if (this.userAgent == null) {
            this.userAgent = webView.getSettings().getUserAgentString();
            Log.d(JtAdManager.JT_AD, "userAgent: " + this.userAgent);
        }
        return this.userAgent;
    }

    public String getLanguage() {
        return this.language;
    }

    public void setLanguage(String language2) {
        this.language = language2;
    }

    public String getAdultContentType() {
        return this.adultContentType;
    }

    public void setAdultContentType(String adultContent) {
        this.adultContentType = adultContent;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setPostalCode(String postalCode2) {
        this.postalCode = postalCode2;
    }

    public String getCountry() {
        return this.country;
    }

    public void setCountry(String country2) {
        this.country = country2;
    }

    public String getAge() {
        return this.age;
    }

    public void setAge(String age2) {
        this.age = age2;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender2) {
        this.gender = gender2;
    }

    public String getHHI() {
        return this.hhi;
    }

    public void setHHI(String hhi2) {
        this.hhi = hhi2;
    }

    public int getRefreshPeriod() {
        return this.refreshPeriod;
    }

    public void setRefreshPeriod(int refreshPeriod2) {
        this.refreshPeriod = refreshPeriod2;
    }

    public String getBundleVersion() {
        return this.bundleVersion;
    }

    /* access modifiers changed from: protected */
    public String getAdFormat() {
        return this.adFormat;
    }

    public void setAdFormat(String adFormat2) {
        this.adFormat = adFormat2;
    }

    public String getJtLibVer() {
        return this.jtLibVer;
    }

    public String getVersion() {
        return this.version;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public void setShouldSendLocation(boolean shouldSendLocation2) {
        this.shouldSendLocation = shouldSendLocation2;
    }

    public boolean isShouldSendLocation() {
        return this.shouldSendLocation;
    }

    public Bitmap getAlternateImage() {
        return this.alternateImage;
    }

    public void setAlternateImage(Bitmap alternateImage2) {
        this.alternateImage = alternateImage2;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor2) {
        this.backgroundColor = backgroundColor2;
    }

    public void setDismissButtonLabel(String dismissButtonLabel2) {
        this.dismissButtonLabel = dismissButtonLabel2;
    }

    public String getDismissButtonLabel() {
        return this.dismissButtonLabel;
    }

    public void setInterstitialshowTime(int interstitialshowTime2) {
        this.interstitialshowTime = interstitialshowTime2;
    }

    public int getInterstitialshowTime() {
        return this.interstitialshowTime;
    }

    public String getOs() {
        return this.os;
    }

    public void setApplicationId(String appId2) {
        this.appId = appId2;
    }

    public String getApplicationId() {
        return this.appId;
    }

    public void setApplicationVersion(String appVer2) {
        this.appVer = appVer2;
    }

    public String getApplicationVersion() {
        return this.appVer;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width2) {
        this.width = width2;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height2) {
        this.height = height2;
    }

    public void setShouldDebugNetworkTraffic(boolean should) {
        this.shouldDebugNetworkTraffic = should;
    }

    public boolean getShouldDebugNetworkTraffic() {
        return this.shouldDebugNetworkTraffic;
    }
}
