package com.jumptap.adtag;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.utils.JtAdManager;
import com.jumptap.adtag.utils.JtException;
import java.util.Timer;
import java.util.TimerTask;

public class JtAdInterstitial extends JtAdView {
    private DismissTimer dismissTask;
    private boolean isPopupShown = false;
    private PopupWindow popup;

    public JtAdInterstitial(Context context) throws JtException {
        super(context);
        init();
    }

    public JtAdInterstitial(Context context, JtAdWidgetSettings widgetSettings) throws JtException {
        super(context, widgetSettings);
        init();
    }

    public JtAdInterstitial(Context context, AttributeSet attrs) throws JtException {
        super(context, attrs);
        init();
    }

    public JtAdInterstitial(Context context, AttributeSet attrs, int defStyle) throws JtException {
        super(context, attrs, defStyle);
        init();
    }

    public void show() {
        startTimer();
    }

    public void showAsPopup() {
        this.popup.showAtLocation(this, 48, 0, 0);
        this.isPopupShown = true;
    }

    public void dismiss() {
        dismiss(false);
    }

    /* access modifiers changed from: private */
    public void dismiss(boolean isManuallyClosed) {
        stopTimer();
        if (this.isPopupShown) {
            this.popup.dismiss();
            this.isPopupShown = false;
        }
        onInterstitialDismissed();
    }

    public void onAdError(int errorCode) {
        super.onAdError(errorCode);
        dismiss(false);
    }

    public void onNoAdFound() {
        super.onNoAdFound();
        dismiss(false);
    }

    public void resize(int width, int height, boolean shouldExpand) {
    }

    /* access modifiers changed from: protected */
    public int getHeightSize() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public int getWidthSize() {
        return -1;
    }

    /* access modifiers changed from: protected */
    public void startTimers(boolean shouldRefreshAd) {
        super.startTimers(shouldRefreshAd);
        startTimer();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void}
     arg types: [com.jumptap.adtag.JtAdInterstitial, int, int, int]
     candidates:
      ClspMth{android.widget.PopupWindow.<init>(android.content.Context, android.util.AttributeSet, int, int):void}
      ClspMth{android.widget.PopupWindow.<init>(android.view.View, int, int, boolean):void} */
    private void init() {
        configDismissButton();
        this.popup = new PopupWindow((View) this, getWidthSize(), getHeightSize(), true);
        this.webViewArr[this.ACTIVE_WEBVIEW_INDEX].setOnTouchListener(new InterstitialOnTouchListener(this, getId()));
        this.webViewArr[this.INACTIVE_WEBVIEW_INDEX].setOnTouchListener(new InterstitialOnTouchListener(this, getId()));
    }

    private void configDismissButton() {
        this.dismiss.setVisibility(0);
        this.dismiss.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                JtAdInterstitial.this.dismiss(true);
            }
        });
    }

    private void startTimer() {
        int interShowTime = this.widgetSettings.getInterstitialshowTime();
        Log.i(JtAdManager.JT_AD, "Starting interstitial timer");
        Timer timer = new Timer("DismisTask");
        if (this.dismissTask != null) {
            this.dismissTask.cancel();
        }
        this.dismissTask = new DismissTimer();
        timer.schedule(this.dismissTask, (long) (interShowTime * 1000));
    }

    private void stopTimer() {
        Log.i(JtAdManager.JT_AD, "stopping interstitial timer");
        if (this.dismissTask != null) {
            this.dismissTask.cancel();
        }
    }

    /* access modifiers changed from: private */
    public void restartTimer() {
        stopTimer();
        startTimer();
    }

    private class InterstitialOnTouchListener extends JtAdView.JtAdListener {
        public InterstitialOnTouchListener(JtAdView widget, int widgetId) {
            super(widget, widgetId);
        }

        public boolean onTouch(View v, MotionEvent event) {
            super.onTouch(v, event);
            JtAdInterstitial.this.restartTimer();
            return false;
        }
    }

    class DismissTimer extends TimerTask {
        DismissTimer() {
        }

        public void run() {
            Log.i(JtAdManager.JT_AD, "starting dismissal timer task");
            JtAdInterstitial.this.post(new Runnable() {
                public void run() {
                    JtAdInterstitial.this.dismiss(false);
                }
            });
        }
    }
}
