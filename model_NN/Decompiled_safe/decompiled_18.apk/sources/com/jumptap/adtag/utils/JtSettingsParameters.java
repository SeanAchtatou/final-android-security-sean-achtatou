package com.jumptap.adtag.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import com.jumptap.adtag.JtAdWidgetSettings;

public class JtSettingsParameters {
    private static final String ADULT_CONTENT_TYPE = "adultContentType";
    private static final String AGE = "age";
    private static final String ALTERNATE_IMAGE = "alternateImage";
    private static final String APP_ID = "applicationId";
    private static final String APP_VER = "applicationVersion";
    private static final String BACKGROUND_COLOR = "alternateColor";
    private static final String COUNTRY = "country";
    private static final String DISMIIS_BUTTON_LABEL = "dismissLabel";
    private static final String GENDER = "gender";
    private static final String HHI = "hhi";
    private static final String HOST = "host";
    private static final String INTERDTITIAL_TIME = "interstitialTime";
    private static final String LANGUAGE = "language";
    private static final String POSTAL_CODE = "postalCode";
    private static final String PUBLISHER_ID = "publisherId";
    private static final String REFRESH_PERIOD = "refreshPeriodInSec";
    private static final String SITE_ID = "siteId";
    private static final String SPOT_ID = "spotId";

    public static String getStringValueFromAttributes(AttributeSet attributes, String ns, String attrName) {
        String paramValue;
        if (attributes == null || (paramValue = attributes.getAttributeValue(ns, attrName)) == null || paramValue.equalsIgnoreCase("") || paramValue.length() <= 0) {
            return null;
        }
        return paramValue;
    }

    public static int getUnsignedIntValueFromAttributes(AttributeSet attributes, String ns, String attrName) {
        if (attributes != null) {
            return attributes.getAttributeUnsignedIntValue(ns, attrName, -1);
        }
        return -1;
    }

    public static int getIntValueFromAttributes(AttributeSet attributes, String ns, String attrName) {
        if (attributes != null) {
            return attributes.getAttributeIntValue(ns, attrName, -1);
        }
        return -1;
    }

    public static void populateSettings(JtAdWidgetSettings widgetSettings, AttributeSet attributes, Context context) {
        String paramValue = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, PUBLISHER_ID);
        if (paramValue != null) {
            widgetSettings.setPublisherId(paramValue);
        }
        String paramValue2 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, SITE_ID);
        if (paramValue2 != null) {
            widgetSettings.setSiteId(paramValue2);
        }
        String paramValue3 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, SPOT_ID);
        if (paramValue3 != null) {
            widgetSettings.setSpotId(paramValue3);
        }
        String paramValue4 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, HOST);
        if (paramValue4 != null) {
            widgetSettings.setHostURL(paramValue4);
        }
        String paramValue5 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, LANGUAGE);
        if (paramValue5 != null) {
            widgetSettings.setLanguage(paramValue5);
        }
        String paramValue6 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, ADULT_CONTENT_TYPE);
        if (paramValue6 != null) {
            widgetSettings.setAdultContentType(paramValue6);
        }
        String paramValue7 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, POSTAL_CODE);
        if (paramValue7 != null) {
            widgetSettings.setPostalCode(paramValue7);
        }
        String paramValue8 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, COUNTRY);
        if (paramValue8 != null) {
            widgetSettings.setCountry(paramValue8);
        }
        String paramValue9 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, "age");
        if (paramValue9 != null) {
            widgetSettings.setAge(paramValue9);
        }
        String paramValue10 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, "gender");
        if (paramValue10 != null) {
            widgetSettings.setGender(paramValue10);
        }
        String paramValue11 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, HHI);
        if (paramValue11 != null) {
            widgetSettings.setHHI(paramValue11);
        }
        String paramValue12 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, ALTERNATE_IMAGE);
        if (paramValue12 != null) {
            setAlternativeImage(paramValue12, widgetSettings, context);
        }
        String paramValue13 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, DISMIIS_BUTTON_LABEL);
        if (paramValue13 != null) {
            widgetSettings.setDismissButtonLabel(paramValue13);
        }
        int intValue = getUnsignedIntValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, BACKGROUND_COLOR);
        if (intValue != -1) {
            widgetSettings.setBackgroundColor(intValue);
        }
        int intValue2 = getIntValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, REFRESH_PERIOD);
        if (intValue2 != -1) {
            widgetSettings.setRefreshPeriod(intValue2);
        }
        int intValue3 = getIntValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, INTERDTITIAL_TIME);
        if (intValue3 != -1) {
            widgetSettings.setInterstitialshowTime(intValue3);
        }
        String paramValue14 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, APP_ID);
        if (paramValue14 != null) {
            widgetSettings.setApplicationId(paramValue14);
        }
        String paramValue15 = getStringValueFromAttributes(attributes, JtAdManager.JT_NAMRSPACE, APP_VER);
        if (paramValue15 != null) {
            widgetSettings.setApplicationVersion(paramValue15);
        }
    }

    private static void setAlternativeImage(String paramValue, JtAdWidgetSettings widgetSettings, Context context) {
        Resources res = context.getResources();
        String defaultPackage = context.getPackageName();
        Log.d(JtAdManager.JT_AD, "Here is the parameter: " + paramValue + " defaultPackage: " + defaultPackage);
        int imageID = res.getIdentifier(paramValue, null, defaultPackage);
        if (imageID != 0) {
            try {
                widgetSettings.setAlternateImage(((BitmapDrawable) res.getDrawable(imageID)).getBitmap());
            } catch (Resources.NotFoundException e) {
                Log.e(JtAdManager.JT_AD, "Cannot found Resource:" + paramValue + ". Going to load system alternative image");
                Log.e(JtAdManager.JT_AD, e.getMessage());
            }
        } else {
            Log.e(JtAdManager.JT_AD, "Cannot found Resource:" + paramValue);
        }
    }
}
