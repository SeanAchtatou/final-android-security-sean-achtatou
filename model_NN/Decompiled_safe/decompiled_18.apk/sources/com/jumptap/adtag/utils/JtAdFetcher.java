package com.jumptap.adtag.utils;

import android.content.Context;
import android.util.Log;
import com.jumptap.adtag.listeners.JtAdViewInnerListener;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class JtAdFetcher implements Runnable {
    private static final String ADVIEW_HTML_FILE_NAME = "adview.html";
    private static final String ADVIEW_JS_FILE_NAME = "adview.js";
    private static final String AD_CONTENT_HERE_STR = "%AD_CONTENT_HERE%";
    private static final int BUFFER_SIZE = 1024;
    private static final String JS_CONTENT_HERE_STR = "%JS_CONTENT_HERE%";
    private static final String XJT_RESPONSE_ID = "Xjt-Responseid";
    private String adRequestId;
    private JtAdViewInnerListener contentListener = null;
    private Context context;
    private boolean shouldDebugNetworkTraffic = false;
    private String url;
    private String wrapperContent = null;

    public JtAdFetcher(Context context2, JtAdViewInnerListener contentListener2) throws JtException {
        this.contentListener = contentListener2;
        this.context = context2;
        makeContentWrapper();
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setShouldDebugNetworkTraffic(boolean debug) {
        this.shouldDebugNetworkTraffic = debug;
    }

    public void run() {
        Log.d(JtAdManager.JT_AD, "Started Url Thread");
        String adWrappedContent = modifyHtml(getAdContent());
        if (this.contentListener != null) {
            this.contentListener.setContent(adWrappedContent, this.adRequestId);
        }
    }

    public void kickOffUrlFetch(String url2) {
        Log.d(JtAdManager.JT_AD, "Kicked off fetcher");
        this.url = url2;
        new Thread(this).start();
    }

    public String getAdRequestId() {
        return this.adRequestId;
    }

    public String getAdContent() {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(this.url);
        if (this.shouldDebugNetworkTraffic) {
            Log.d(JtAdManager.JT_AD, "Created html client for: " + this.url);
        }
        try {
            HttpResponse resp = client.execute(get);
            Header firstHeader = resp.getFirstHeader(XJT_RESPONSE_ID);
            if (firstHeader != null) {
                this.adRequestId = firstHeader.getValue();
            }
            int statusCode = resp.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                HttpEntity ent = resp.getEntity();
                int contentLength = (int) ent.getContentLength();
                InputStream is = ent.getContent();
                byte[] bar = new byte[BUFFER_SIZE];
                StringBuilder reqContent = new StringBuilder();
                while (true) {
                    int len = is.read(bar);
                    if (len <= 0) {
                        return reqContent.toString();
                    }
                    reqContent.append(new StringBuffer(new String(bar, 0, len)));
                }
            } else {
                Log.e(JtAdManager.JT_AD, "JtAdFetcher.getAdContent: Recieve error Code=[" + statusCode + "] when sending url=[" + this.url + "]");
                return null;
            }
        } catch (IOException e) {
            Log.e(JtAdManager.JT_AD, "JtAdFetcher.getAdContent:" + e.toString());
            return null;
        }
    }

    private String modifyHtml(String sourceHtml) {
        if (sourceHtml == null) {
            return null;
        }
        if (sourceHtml.length() > 0) {
            return this.wrapperContent.replace(AD_CONTENT_HERE_STR, sourceHtml);
        }
        return "";
    }

    private void makeContentWrapper() throws JtException {
        if (this.wrapperContent == null) {
            StringBuilder htmlFileContent = FileReaderUtil.getFileContent(this.context, ADVIEW_HTML_FILE_NAME);
            this.wrapperContent = htmlFileContent.toString().replace(JS_CONTENT_HERE_STR, FileReaderUtil.getFileContent(this.context, ADVIEW_JS_FILE_NAME));
        }
    }
}
