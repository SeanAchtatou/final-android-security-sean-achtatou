package com.jumptap.adtag.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.webkit.WebView;
import com.jumptap.adtag.media.VideoCacheItem;
import java.util.List;

public class JtAccelerator implements SensorEventListener {
    private Sensor accSensor = null;
    Context context;
    boolean isAccelStarted = false;
    private long lastUpdate = -1;
    private SensorManager sensorManager;
    int updateFrequency = -1;
    WebView webView;

    public JtAccelerator(Context context2, WebView webView2) {
        this.context = context2;
        this.webView = webView2;
        this.sensorManager = (SensorManager) context2.getSystemService("sensor");
    }

    public void start(int time) {
        this.updateFrequency = time;
        if (!this.isAccelStarted) {
            List<Sensor> sensors = this.sensorManager.getSensorList(1);
            if (sensors.size() > 0) {
                this.accSensor = sensors.get(0);
            }
            this.sensorManager.registerListener(this, this.accSensor, 1);
        }
    }

    public void stop() {
        if (this.isAccelStarted) {
            this.sensorManager.unregisterListener(this);
        }
        this.isAccelStarted = false;
    }

    public void onSensorChanged(SensorEvent event) {
        if (event != null && event.sensor.getType() == 1 && event.values.length >= 3) {
            long curTime = System.currentTimeMillis();
            if (this.lastUpdate == -1 || curTime - this.lastUpdate > ((long) this.updateFrequency)) {
                this.lastUpdate = curTime;
                float[] values = event.values;
                float x = values[0];
                float y = values[1];
                this.webView.loadUrl("javascript:gotAccel(" + x + ", " + y + VideoCacheItem.URL_DELIMITER + values[2] + " )");
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }
}
