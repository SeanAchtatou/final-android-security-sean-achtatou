package com.jumptap.adtag.utils;

public class JtException extends Exception {
    private static final long serialVersionUID = 1;

    public JtException(String message) {
        super(message);
    }
}
