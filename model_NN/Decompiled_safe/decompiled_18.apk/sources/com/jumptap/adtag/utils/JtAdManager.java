package com.jumptap.adtag.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.adsdk.sdk.Const;
import com.jumptap.adtag.media.VideoCacheItem;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.util.Enumeration;

public class JtAdManager {
    private static final String CHAR_SET = "iso-8859-1";
    private static final String CONNECTION_TYPE_EDGE = "edge";
    private static final String CONNECTION_TYPE_WIFI = "wifi";
    private static final String CONNECTION_TYPE__3G = "3g";
    public static final String JT_AD = "JtAd";
    public static final String JT_NAMRSPACE = "http://www.jumptap.com/lib/android";
    private static final int NETWORK_TYPE_CDMA = 4;
    private static final int NETWORK_TYPE_EVDO_0 = 5;
    private static final int NETWORK_TYPE_EVDO_A = 6;
    private static final int NETWORK_TYPE_HSDPA = 8;
    private static final int NETWORK_TYPE_HSPA = 10;
    private static final int NETWORK_TYPE_HSUPA = 9;
    private static final int NETWORK_TYPE_IDEN = 11;
    private static final int NETWORK_TYPE_RRT = 7;
    private static final String PREF_NAME = "jtPref";
    private static final String SHA1_ALGORITHM = "SHA-1";
    private static String procVersionStr = null;
    private boolean testMode = false;

    private JtAdManager() {
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    public void setTestMode(boolean testMode2) {
        this.testMode = testMode2;
    }

    public static String getIPAddress() {
        if (0 == 0) {
            return getLocalIpAddress();
        }
        return null;
    }

    public static String getHID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), "android_id");
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 15;
            int two_halfs = 0;
            while (true) {
                if (halfbyte < 0 || halfbyte > 9) {
                    buf.append((char) ((halfbyte - 10) + 97));
                } else {
                    buf.append((char) (halfbyte + 48));
                }
                halfbyte = data[i] & 15;
                int two_halfs2 = two_halfs + 1;
                if (two_halfs >= 1) {
                    break;
                }
                two_halfs = two_halfs2;
            }
        }
        return buf.toString();
    }

    public static String SHA1(String text) {
        if (text == null) {
            return null;
        }
        try {
            MessageDigest md = MessageDigest.getInstance(SHA1_ALGORITHM);
            byte[] bArr = new byte[40];
            md.update(text.getBytes(CHAR_SET), 0, text.length());
            return convertToHex(md.digest());
        } catch (Exception e) {
            Log.i(JT_AD, "Error generating generating SHA-1: ", e);
            return null;
        }
    }

    public static String getHIDSHA1(Context context) {
        return SHA1(getHID(context));
    }

    public static String getLocation(Context context) {
        JtLocation.init(context);
        Location currentLocation = JtLocation.getCurrentLocation();
        if (currentLocation != null) {
            return currentLocation.getLatitude() + VideoCacheItem.URL_DELIMITER + currentLocation.getLongitude();
        }
        return null;
    }

    private static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
            while (en.hasMoreElements()) {
                Enumeration<InetAddress> enumIpAddr = en.nextElement().getInetAddresses();
                while (true) {
                    if (enumIpAddr.hasMoreElements()) {
                        InetAddress inetAddress = enumIpAddr.nextElement();
                        if (!inetAddress.isLoopbackAddress()) {
                            return inetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e(JT_AD, "JtAdManager.getLocalIpAddress:" + ex.toString());
        }
        return null;
    }

    public static String getSDKVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getAndroidModel() {
        return Build.DEVICE;
    }

    public static String getBrand() {
        return Build.BRAND;
    }

    public static String getManufacturer() {
        return "";
    }

    public static String getOperatorName(Context context) {
        TelephonyManager telephonyManager = getTelephonyManager(context);
        if (telephonyManager != null) {
            return telephonyManager.getNetworkOperatorName();
        }
        return "";
    }

    public static String getNetworkType(Context context) {
        TelephonyManager telephonyManager = getTelephonyManager(context);
        if (telephonyManager == null) {
            return "";
        }
        switch (telephonyManager.getNetworkType()) {
            case 1:
                return Const.CONNECTION_TYPE_MOBILE_GPRS;
            case 2:
                return Const.CONNECTION_TYPE_MOBILE_EDGE;
            case 3:
                return Const.CONNECTION_TYPE_MOBILE_UMTS;
            default:
                return "";
        }
    }

    public static String getPhoneType(Context context) {
        TelephonyManager telephonyManager = getTelephonyManager(context);
        if (telephonyManager == null) {
            return "";
        }
        switch (telephonyManager.getPhoneType()) {
            case 1:
                return "GSM";
            default:
                return "";
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getKernelVersion() {
        /*
            java.lang.String r6 = com.jumptap.adtag.utils.JtAdManager.procVersionStr
            if (r6 != 0) goto L_0x0096
            r4 = 0
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ IOException -> 0x004c }
            java.io.FileReader r6 = new java.io.FileReader     // Catch:{ IOException -> 0x004c }
            java.lang.String r7 = "/proc/version"
            r6.<init>(r7)     // Catch:{ IOException -> 0x004c }
            r7 = 256(0x100, float:3.59E-43)
            r5.<init>(r6, r7)     // Catch:{ IOException -> 0x004c }
            java.lang.String r4 = r5.readLine()     // Catch:{ all -> 0x0047 }
            r5.close()     // Catch:{ IOException -> 0x004c }
            java.lang.String r0 = "\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\([^)]+\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)"
            java.lang.String r6 = "\\w+\\s+\\w+\\s+([^\\s]+)\\s+\\(([^\\s@]+(?:@[^\\s.]+)?)[^)]*\\)\\s+\\([^)]+\\)\\s+([^\\s]+)\\s+(?:PREEMPT\\s+)?(.+)"
            java.util.regex.Pattern r3 = java.util.regex.Pattern.compile(r6)     // Catch:{ IOException -> 0x004c }
            java.util.regex.Matcher r2 = r3.matcher(r4)     // Catch:{ IOException -> 0x004c }
            boolean r6 = r2.matches()     // Catch:{ IOException -> 0x004c }
            if (r6 != 0) goto L_0x0057
            java.lang.String r6 = "JtAd"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x004c }
            r7.<init>()     // Catch:{ IOException -> 0x004c }
            java.lang.String r8 = "Regex did not match on /proc/version: "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x004c }
            java.lang.StringBuilder r7 = r7.append(r4)     // Catch:{ IOException -> 0x004c }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x004c }
            android.util.Log.e(r6, r7)     // Catch:{ IOException -> 0x004c }
            java.lang.String r6 = "Unavailable"
        L_0x0046:
            return r6
        L_0x0047:
            r6 = move-exception
            r5.close()     // Catch:{ IOException -> 0x004c }
            throw r6     // Catch:{ IOException -> 0x004c }
        L_0x004c:
            r1 = move-exception
            java.lang.String r6 = "JtAd"
            java.lang.String r7 = "IO Exception when getting kernel version for Device Info screen"
            android.util.Log.e(r6, r7, r1)
            java.lang.String r6 = "Unavailable"
            goto L_0x0046
        L_0x0057:
            int r6 = r2.groupCount()     // Catch:{ IOException -> 0x004c }
            r7 = 4
            if (r6 >= r7) goto L_0x0083
            java.lang.String r6 = "JtAd"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x004c }
            r7.<init>()     // Catch:{ IOException -> 0x004c }
            java.lang.String r8 = "Regex match on /proc/version only returned "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x004c }
            int r8 = r2.groupCount()     // Catch:{ IOException -> 0x004c }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x004c }
            java.lang.String r8 = " groups"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ IOException -> 0x004c }
            java.lang.String r7 = r7.toString()     // Catch:{ IOException -> 0x004c }
            android.util.Log.e(r6, r7)     // Catch:{ IOException -> 0x004c }
            java.lang.String r6 = "Unavailable"
            goto L_0x0046
        L_0x0083:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x004c }
            r7 = 1
            java.lang.String r7 = r2.group(r7)     // Catch:{ IOException -> 0x004c }
            r6.<init>(r7)     // Catch:{ IOException -> 0x004c }
            java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x004c }
            com.jumptap.adtag.utils.JtAdManager.procVersionStr = r6     // Catch:{ IOException -> 0x004c }
            java.lang.String r6 = com.jumptap.adtag.utils.JtAdManager.procVersionStr     // Catch:{ IOException -> 0x004c }
            goto L_0x0046
        L_0x0096:
            java.lang.String r6 = com.jumptap.adtag.utils.JtAdManager.procVersionStr
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jumptap.adtag.utils.JtAdManager.getKernelVersion():java.lang.String");
    }

    private static ConnectivityManager getConnectivityManager(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            try {
                return (ConnectivityManager) context.getSystemService("connectivity");
            } catch (SecurityException secExp) {
                Log.e(JT_AD, "JtAdManager.getConnectivityManager: " + secExp.getMessage());
                return null;
            }
        } else {
            Log.e(JT_AD, "JtAdManager: Requires ACCESS_NETWORK_STATE permission");
            return null;
        }
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                return (TelephonyManager) context.getSystemService("phone");
            } catch (SecurityException secExp) {
                Log.e(JT_AD, "JtAdManager.getTelephonyManager: " + secExp.getMessage());
                return null;
            }
        } else {
            Log.e(JT_AD, "JtAdManager: Requires READ_PHONE_STATE permission");
            return null;
        }
    }

    public static String getPreferences(Context context, String prefName, String defaultValue) {
        return context.getSharedPreferences(PREF_NAME, 0).getString(prefName, defaultValue);
    }

    public static void savePreferences(Context context, String prefName, String prefValue) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, 0).edit();
        editor.putString(prefName, prefValue);
        editor.commit();
    }

    public static void removePreferences(Context context, String prefName) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREF_NAME, 0).edit();
        editor.remove(prefName);
        editor.commit();
    }

    public static String getConnectionType(Context context) {
        NetworkInfo activeNetworkInfo;
        String connectionType = "";
        ConnectivityManager connectivityManager = getConnectivityManager(context);
        if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
            int netType = activeNetworkInfo.getType();
            int netSubtype = activeNetworkInfo.getSubtype();
            if (netType == 1) {
                connectionType = "wifi";
            } else if (netType == 0) {
                connectionType = getConnectionTypeByNetSubtype(netSubtype);
            }
        }
        Log.i(JT_AD, "connectionType=" + connectionType);
        return connectionType;
    }

    private static String getConnectionTypeByNetSubtype(int netSubtype) {
        String connectionType = "";
        if (netSubtype == 3 || netSubtype == 7 || netSubtype == 8 || netSubtype == 10 || netSubtype == 9 || netSubtype == 5 || netSubtype == 6) {
            connectionType = "3g";
        }
        if (netSubtype == 1 || netSubtype == 2 || netSubtype == 4 || netSubtype == 11) {
            return "edge";
        }
        return connectionType;
    }
}
