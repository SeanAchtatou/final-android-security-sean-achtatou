package com.jumptap.adtag.utils;

import android.content.Context;
import android.webkit.WebView;
import com.adsdk.sdk.Const;
import com.jumptap.adtag.JtAdWidgetSettings;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class JtAdUrlBuilder {
    private static final String JT_COUNT = "1";
    private Context context;
    private JtAdWidgetSettings widgetSettings;

    public JtAdUrlBuilder(JtAdWidgetSettings widgetSettings2, Context context2) {
        this.widgetSettings = widgetSettings2;
        this.context = context2;
    }

    public String getAdUrl(WebView view) {
        return getAdUrl(view, null);
    }

    public String getAdUrl(WebView view, String aditionalToUrl) {
        String location;
        StringBuilder jtUrl = new StringBuilder(this.widgetSettings.getHostURL());
        jtUrl.append("&ua=").append(encodeParam(this.widgetSettings.getUserAgent(view)));
        jtUrl.append("&pub=").append(encodeParam(this.widgetSettings.getPublisherId()));
        jtUrl.append("&spot=").append(encodeParam(this.widgetSettings.getSpotId()));
        jtUrl.append("&site=").append(encodeParam(this.widgetSettings.getSiteId()));
        if (this.widgetSettings.isShouldSendLocation() && (location = JtAdManager.getLocation(this.context)) != null) {
            jtUrl.append("&ll=").append(encodeParam(location));
            jtUrl.append("&country=").append(encodeParam(this.widgetSettings.getCountry()));
            jtUrl.append("&pc=").append(encodeParam(this.widgetSettings.getPostalCode()));
        }
        jtUrl.append("&mt-age=").append(encodeParam(this.widgetSettings.getAge()));
        jtUrl.append("&mt-gender=").append(encodeParam(this.widgetSettings.getGender()));
        jtUrl.append("&mt-hhi=").append(encodeParam(this.widgetSettings.getHHI()));
        jtUrl.append("&hid=").append(encodeParam(JtAdManager.getHID(this.context)));
        jtUrl.append("&a=").append(encodeParam(this.widgetSettings.getAdultContentType()));
        jtUrl.append("&l=").append(encodeParam(this.widgetSettings.getLanguage()));
        jtUrl.append("&c=").append(encodeParam(JT_COUNT));
        jtUrl.append("&version=").append(encodeParam(this.widgetSettings.getVersion()));
        jtUrl.append("&mt-speed=").append(encodeParam(JtAdManager.getConnectionType(this.context)));
        jtUrl.append("&mt-jtlib=").append(encodeParam(this.widgetSettings.getJtLibVer()));
        jtUrl.append("&mt-bundle=").append(encodeParam(this.widgetSettings.getBundleVersion()));
        jtUrl.append("&mt-os=").append(encodeParam(this.widgetSettings.getOs()));
        jtUrl.append("&mt-osversion=").append(encodeParam(JtAdManager.getSDKVersion()));
        jtUrl.append("&mt-model=").append(encodeParam(JtAdManager.getAndroidModel()));
        jtUrl.append("&mt-make=").append(encodeParam(JtAdManager.getBrand()));
        jtUrl.append("&mt-fw=").append(encodeParam(JtAdManager.getKernelVersion()));
        jtUrl.append("&mt-operator=").append(encodeParam(JtAdManager.getOperatorName(this.context)));
        jtUrl.append("&mt-nradio=").append(encodeParam(JtAdManager.getNetworkType(this.context)));
        jtUrl.append("&mt-dradio=").append(encodeParam(JtAdManager.getPhoneType(this.context)));
        int width = view.getWidth();
        int height = view.getHeight();
        String widthString = Integer.toString(width);
        String heightString = Integer.toString(height);
        jtUrl.append("&mt-width=").append(encodeParam(widthString));
        jtUrl.append("&mt-height=").append(encodeParam(heightString));
        if (aditionalToUrl != null) {
            jtUrl.append("&" + aditionalToUrl);
        }
        return jtUrl.toString();
    }

    public static String encodeParam(String str) {
        if (str == null || str.equals("")) {
            return "";
        }
        try {
            return URLEncoder.encode(str, Const.ENCODING);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }
}
