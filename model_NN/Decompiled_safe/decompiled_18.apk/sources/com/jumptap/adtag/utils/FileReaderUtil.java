package com.jumptap.adtag.utils;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;

public class FileReaderUtil {
    private static final int BUFFER_SIZE = 1024;

    public static StringBuilder getFileContent(Context context, String fileName) throws JtException {
        StringBuilder fileContentString = null;
        if (0 == 0) {
            fileContentString = new StringBuilder();
            byte[] buffer = new byte[BUFFER_SIZE];
            try {
                InputStream in = context.getResources().getAssets().open(fileName);
                while (true) {
                    int len = in.read(buffer);
                    if (len <= 0) {
                        break;
                    }
                    fileContentString.append(new StringBuffer(new String(buffer, 0, len)));
                }
                in.close();
            } catch (IOException e) {
                throw new JtException("Cannot load " + fileName + " from asset folder" + e.getMessage());
            }
        }
        return fileContentString;
    }
}
