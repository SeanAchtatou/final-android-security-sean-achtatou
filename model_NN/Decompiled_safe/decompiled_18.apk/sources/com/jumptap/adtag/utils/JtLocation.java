package com.jumptap.adtag.utils;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

public class JtLocation {
    private static Location currentLocation = null;
    private static JtLocation jtLocationInstance = null;
    /* access modifiers changed from: private */
    public static LocationManager locationManager = null;
    private LocationListener listenerCoarse = null;

    private JtLocation() {
    }

    protected static JtLocation getInstance() {
        if (jtLocationInstance == null) {
            jtLocationInstance = new JtLocation();
        }
        return jtLocationInstance;
    }

    public static void init(Context context) {
        if (locationManager == null) {
            getInstance().registerLocationListeners(context);
        }
    }

    private void registerLocationListeners(Context context) {
        try {
            if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                locationManager = (LocationManager) context.getSystemService("location");
                setCurrentLocation(locationManager.getLastKnownLocation("network"));
                if (this.listenerCoarse == null) {
                    createLocationListeners();
                }
                locationManager.requestLocationUpdates("network", 500, 1000.0f, this.listenerCoarse);
                return;
            }
            Log.e(JtAdManager.JT_AD, "Requires ACCESS_COARSE_LOCATION permission");
        } catch (SecurityException e) {
            Log.e(JtAdManager.JT_AD, "Requires ACCESS_COARSE_LOCATION permission");
        } catch (IllegalArgumentException illArg) {
            if ("sdk".equals(Build.MODEL)) {
                Log.e(JtAdManager.JT_AD, "Emulator is not sending location updates.");
                return;
            }
            throw illArg;
        }
    }

    private void createLocationListeners() {
        this.listenerCoarse = new LocationListener() {
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }

            public void onLocationChanged(Location location) {
                JtLocation.setCurrentLocation(location);
                if (location.getAccuracy() > 1000.0f && location.hasAccuracy()) {
                    JtLocation.locationManager.removeUpdates(this);
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public static void setCurrentLocation(Location currentLocation2) {
        currentLocation = currentLocation2;
    }

    public static Location getCurrentLocation() {
        return currentLocation;
    }
}
