package com.jumptap.adtag.listeners;

import com.jumptap.adtag.JtAdWidgetSettings;

public interface JtAdViewInnerListener {
    String getAdRequestId();

    JtAdWidgetSettings getWidgetSettings();

    void handleClicks(String str);

    void hide();

    void onAdError(int i);

    void onBeginAdInteraction();

    void onEndAdInteraction();

    void onInterstitialDismissed();

    void onNewAd();

    void onNoAdFound();

    boolean post(Runnable runnable);

    void resize(int i, int i2, boolean z);

    void resizeWithCallback(boolean z, int i, int i2, String str, int i3, String str2);

    void setContent(String str, String str2);
}
