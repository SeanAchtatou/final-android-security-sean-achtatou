package com.jumptap.adtag.actions;

import android.content.Context;
import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.utils.JtAdManager;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;

public abstract class AdAction {
    protected String redirectedUrl = null;
    protected String url = null;
    protected String useragent = null;

    public interface UrlPredicate {
        boolean test(String str);
    }

    public abstract void perform(Context context, JtAdView jtAdView);

    public static String getRedirectedUrl(String url2, String useragent2) {
        HttpClient client = new DefaultHttpClient();
        HttpClientParams.setRedirecting(client.getParams(), false);
        try {
            HttpGet get = new HttpGet(url2);
            get.setHeader("User-Agent", useragent2);
            HttpResponse resp = client.execute(get);
            if (resp != null) {
                Header header = resp.getFirstHeader("Location");
                if (header != null) {
                    return header.getValue();
                }
                Log.e(JtAdManager.JT_AD, "AdAction: cannot find Location header in the respons of :" + url2);
                return null;
            }
            Log.e(JtAdManager.JT_AD, "AdAction: cannot execute:" + url2);
            return null;
        } catch (Exception e) {
            Log.e(JtAdManager.JT_AD, "getRedirectedUrl:" + e.toString());
            return null;
        }
    }

    public static String getRedirectedUrlWithPredicate(String url2, String useragent2, UrlPredicate p) {
        int count = 0;
        while (url2 != null && count < 4 && !p.test(url2)) {
            count++;
            url2 = getRedirectedUrl(url2, useragent2);
        }
        if ("".equals(url2)) {
            return null;
        }
        return url2;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public void setRedirectedUrl(String redirectedUrl2) {
        this.redirectedUrl = redirectedUrl2;
    }

    public void setUserAgent(String useragent2) {
        this.useragent = useragent2;
    }
}
