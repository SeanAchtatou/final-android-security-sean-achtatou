package com.jumptap.adtag.actions;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.utils.JtAdManager;

public class BrowserAdAction extends AdAction {
    public void perform(Context context, JtAdView widget) {
        Log.i(JtAdManager.JT_AD, "Performing BrowserAdAction: " + this.redirectedUrl);
        Intent myIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.redirectedUrl));
        if (widget != null) {
            try {
                widget.setLaunchedActivity(true);
                widget.notifyLaunchActivity();
            } catch (ActivityNotFoundException e) {
                Log.e(JtAdManager.JT_AD, "cannot initiate Browser", e);
                return;
            }
        }
        context.startActivity(myIntent);
    }
}
