package com.jumptap.adtag.actions;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.actions.AdAction;
import com.jumptap.adtag.utils.JtAdManager;
import com.millennialmedia.android.MMAdViewSDK;

public class MapAdAction extends AdAction {
    public void perform(Context context, JtAdView widget) {
        Log.i(JtAdManager.JT_AD, "Performing MapAdAction: " + this.redirectedUrl);
        if (this.redirectedUrl != null && !"".equals(this.redirectedUrl)) {
            this.redirectedUrl = getRedirectedUrlWithPredicate(this.redirectedUrl, this.useragent, new AdAction.UrlPredicate() {
                public boolean test(String s) {
                    return MMAdViewSDK.Event.INTENT_MAPS.equals(Uri.parse(s).getScheme());
                }
            });
            if (this.redirectedUrl != null) {
                Intent myIntent = new Intent("android.intent.action.VIEW", Uri.parse(this.redirectedUrl));
                if (widget != null) {
                    try {
                        widget.setLaunchedActivity(true);
                        widget.notifyLaunchActivity();
                    } catch (ActivityNotFoundException e) {
                        Log.e(JtAdManager.JT_AD, "cannot initiate Map", e);
                        return;
                    }
                }
                context.startActivity(myIntent);
                return;
            }
            Log.i(JtAdManager.JT_AD, "no geo url to navigate to");
        }
    }
}
