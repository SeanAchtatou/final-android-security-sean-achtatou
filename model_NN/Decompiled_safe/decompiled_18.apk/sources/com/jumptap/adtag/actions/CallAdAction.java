package com.jumptap.adtag.actions;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.actions.AdAction;
import com.jumptap.adtag.utils.JtAdManager;
import com.millennialmedia.android.MMAdViewSDK;

public class CallAdAction extends AdAction {
    public void perform(Context context, JtAdView widget) {
        Log.i(JtAdManager.JT_AD, "Performing CallAdAction: " + this.redirectedUrl);
        if (this.redirectedUrl != null) {
            this.redirectedUrl = getRedirectedUrlWithPredicate(this.redirectedUrl, this.useragent, new AdAction.UrlPredicate() {
                public boolean test(String s) {
                    return MMAdViewSDK.Event.INTENT_PHONE_CALL.equals(Uri.parse(s).getScheme());
                }
            });
            if (this.redirectedUrl != null) {
                if (widget != null) {
                    try {
                        widget.setLaunchedActivity(true);
                        widget.notifyLaunchActivity();
                    } catch (ActivityNotFoundException e) {
                        Log.e(JtAdManager.JT_AD, "cannot initiate phone call:url=" + this.redirectedUrl, e);
                        return;
                    }
                }
                context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(this.redirectedUrl)));
                return;
            }
            Log.i(JtAdManager.JT_AD, "no tel url to dial");
        }
    }
}
