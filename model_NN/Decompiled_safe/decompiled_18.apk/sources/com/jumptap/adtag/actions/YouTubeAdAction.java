package com.jumptap.adtag.actions;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.utils.JtAdManager;

public class YouTubeAdAction extends AdAction {
    private static final String CLASS_NAME1 = "com.google.android.youtube.YouTubePlayer";
    private static final String CLASS_NAME2 = "com.google.android.youtube.PlayerActivity";
    private static final String PACKAGE_NAME = "com.google.android.youtube";

    public void perform(Context context, JtAdView widget) {
        Log.i(JtAdManager.JT_AD, "Performing YouTubeAdAction:" + this.redirectedUrl);
        Uri uri = Uri.parse(this.redirectedUrl);
        if (widget != null) {
            widget.setLaunchedActivity(true);
            widget.notifyLaunchActivity();
        }
        try {
            openYoutubePlayer(context, uri, CLASS_NAME2);
        } catch (ActivityNotFoundException e1) {
            Log.e(JtAdManager.JT_AD, "cannot start activity: com.google.android.youtube.PlayerActivity");
            Log.e(JtAdManager.JT_AD, e1.getMessage());
            try {
                openYoutubePlayer(context, uri, CLASS_NAME1);
            } catch (ActivityNotFoundException e2) {
                Log.e(JtAdManager.JT_AD, "cannot start activity: com.google.android.youtube.YouTubePlayer");
                Log.e(JtAdManager.JT_AD, e2.getMessage());
                context.startActivity(new Intent("android.intent.action.VIEW", uri));
            }
        }
    }

    private void openYoutubePlayer(Context context, Uri uri, String className) {
        Intent myIntent = new Intent("android.intent.action.VIEW", uri);
        myIntent.setComponent(new ComponentName(PACKAGE_NAME, className));
        myIntent.setFlags(268435456);
        context.startActivity(myIntent);
    }
}
