package com.jumptap.adtag.actions;

import android.net.Uri;
import android.util.Log;
import com.jumptap.adtag.utils.JtAdManager;
import com.millennialmedia.android.MMAdViewSDK;

public class ActionFactory {
    private static AdAction browserAction = new BrowserAdAction();
    private static AdAction callAction = new CallAdAction();
    private static AdAction mapAdAction = new MapAdAction();
    private static AdAction storeAdAction = new StoreAdAction();
    private static AdAction videoAdAction = new VideoAdAction();
    private static AdAction youTubeAdAction = new YouTubeAdAction();

    public static AdAction createAction(String url, String useragent) {
        AdAction action;
        Log.d(JtAdManager.JT_AD, "createAction from url:" + url);
        String redirectedUrl = AdAction.getRedirectedUrl(url, useragent);
        Log.d(JtAdManager.JT_AD, "redirected Url:" + redirectedUrl);
        if (redirectedUrl == null || "".equals(redirectedUrl)) {
            action = browserAction;
            redirectedUrl = url;
        } else {
            action = createActionByUri(Uri.parse(redirectedUrl));
        }
        if (action == null) {
            String actionType = Uri.parse(url).getQueryParameter("t");
            Log.d(JtAdManager.JT_AD, "actionType= " + actionType);
            action = createActionByType(actionType);
        }
        if (action == null) {
            action = browserAction;
        }
        action.setUrl(url);
        action.setRedirectedUrl(redirectedUrl);
        action.setUserAgent(useragent);
        return action;
    }

    private static AdAction createActionByUri(Uri uri) {
        String scheme = uri.getScheme();
        String host = uri.getHost();
        String path = uri.getPath();
        Log.d(JtAdManager.JT_AD, "Creating action from scheme:" + scheme + "  host: " + host + "   path:" + path);
        if ("about".equals(scheme)) {
            return browserAction;
        }
        if (MMAdViewSDK.Event.INTENT_PHONE_CALL.equals(scheme) || MMAdViewSDK.Event.INTENT_TXT_MESSAGE.equals(scheme)) {
            return callAction;
        }
        if (path != null && !"".equals(path) && isVideoExt(path)) {
            return videoAdAction;
        }
        if ("maps.google.com".equals(host)) {
            return mapAdAction;
        }
        if (!"www.youtube.com".equals(host) || path == null || !"/watch".contains(path)) {
            return null;
        }
        return youTubeAdAction;
    }

    private static boolean isVideoExt(String path) {
        return path.endsWith(".mov") || path.endsWith(".avi") || path.endsWith(".mpg") || path.endsWith(".mpeg") || path.endsWith(".wmv") || path.endsWith(".wma") || path.endsWith(".mp4") || path.endsWith(".3pg");
    }

    private static AdAction createActionByType(String actionType) {
        if (actionType == null || actionType.equals("uri") || actionType.equals("url")) {
            return browserAction;
        }
        if (actionType.equals("call") || actionType.equals(MMAdViewSDK.Event.INTENT_PHONE_CALL)) {
            return callAction;
        }
        if (actionType.equals("video") || actionType.equals("movie")) {
            return videoAdAction;
        }
        if (actionType.equals("youtube")) {
            return youTubeAdAction;
        }
        if (actionType.equals("map") || actionType.equals("gmap")) {
            return mapAdAction;
        }
        if (actionType.equals("itunes") || actionType.equals("store")) {
            return storeAdAction;
        }
        return null;
    }
}
