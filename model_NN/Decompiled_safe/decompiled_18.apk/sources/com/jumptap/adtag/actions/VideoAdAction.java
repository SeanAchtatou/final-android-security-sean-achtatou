package com.jumptap.adtag.actions;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import com.jumptap.adtag.JtAdView;
import com.jumptap.adtag.utils.JtAdManager;

public class VideoAdAction extends AdAction {
    public void perform(Context context, JtAdView widget) {
        Log.i(JtAdManager.JT_AD, "Performing VideoAdAction: " + this.redirectedUrl);
        Intent myIntent = new Intent("android.intent.action.VIEW");
        myIntent.setDataAndType(Uri.parse(this.redirectedUrl), "video/*");
        if (widget != null) {
            try {
                widget.setLaunchedActivity(true);
                widget.notifyLaunchActivity();
            } catch (ActivityNotFoundException e) {
                Log.e(JtAdManager.JT_AD, "cannot initiate video", e);
                return;
            }
        }
        context.startActivity(myIntent);
    }
}
