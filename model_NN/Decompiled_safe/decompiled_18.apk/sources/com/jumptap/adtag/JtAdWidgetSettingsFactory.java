package com.jumptap.adtag;

public class JtAdWidgetSettingsFactory {
    private static JtAdWidgetSettings widgetSettings = null;

    private JtAdWidgetSettingsFactory() {
    }

    public static JtAdWidgetSettings createWidgetSettings() {
        if (widgetSettings != null) {
            return widgetSettings.copy();
        }
        widgetSettings = new JtAdWidgetSettings();
        return widgetSettings;
    }
}
