package com.jumptap.adtag.activity;

import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import com.jumptap.adtag.JtAdWidgetSettings;
import com.jumptap.adtag.db.DBManager;
import com.jumptap.adtag.media.JTMediaPlayer;
import com.jumptap.adtag.media.VideoCacheItem;
import com.jumptap.adtag.utils.JtAdFetcher;
import com.jumptap.adtag.utils.JtAdManager;
import com.jumptap.adtag.utils.JtAdUrlBuilder;
import com.jumptap.adtag.utils.JtException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class JTVideo {
    private static final String ADID_STR = "AdID";
    private static final String CLICK_THROUGH_STR = "ClickThrough";
    private static final String IMPRESSION_STR = "Impression";
    private static final String MEDIA_FILE_STR = "MediaFile";
    private static final String URL_STR = "URL";
    private static final long WEEK_IN_MILLIS = 604800000;
    /* access modifiers changed from: private */
    public static String adId = "";
    private static String adRequestId;
    /* access modifiers changed from: private */
    public static String clickThroughUrl = "";
    private static File downloadingMediaFile;
    /* access modifiers changed from: private */
    public static boolean isDownLoading = false;
    private static int totalKbRead = 0;
    /* access modifiers changed from: private */
    public static List<String> trackingUrlArr = new ArrayList();
    /* access modifiers changed from: private */
    public static String videoUrl = "";

    public static boolean prepare(JtAdWidgetSettings widgetSettings, Context context) {
        return prepare(widgetSettings, context, false);
    }

    protected static boolean prepare(JtAdWidgetSettings widgetSettings, Context context, boolean shouldForceDownload) {
        DBManager dbManager = DBManager.getInstance(context);
        JtAdUrlBuilder adUrlBuilder = new JtAdUrlBuilder(widgetSettings, context);
        boolean isVideoInCache = isVideoInCache(dbManager);
        Log.d(JtAdManager.JT_AD, "in prepare  shouldForceDownload= " + shouldForceDownload + "  isVideoInCache=" + isVideoInCache);
        if (shouldForceDownload) {
            dbManager.deleteAllVideoCacheItems();
        }
        boolean retVal = false;
        if (!isVideoInCache || shouldForceDownload) {
            Log.d(JtAdManager.JT_AD, "video not found in cache . downloading new video");
            getAdContent(context, adUrlBuilder.getAdUrl(new WebView(context), "type=video&specific=false"));
            runSaveVideoThread(dbManager, context);
        } else {
            VideoCacheItem item = getLastItem(dbManager);
            String savedAdID = item.getAdID();
            Log.d(JtAdManager.JT_AD, "video found in cache adid = " + savedAdID);
            getAdContent(context, adUrlBuilder.getAdUrl(new WebView(context), "type=video&specific=true&adid=" + savedAdID));
            Log.d(JtAdManager.JT_AD, "TL return adid = " + adId);
            if (savedAdID.equals(adId)) {
                Log.d(JtAdManager.JT_AD, "prepering video from cache");
                downloadingMediaFile = new File(context.getCacheDir(), adId + ".dat");
                JTMediaPlayer mediaPlayer = JTMediaPlayer.getInstance();
                try {
                    FileInputStream in = new FileInputStream(downloadingMediaFile);
                    mediaPlayer.prepareVideo(in.getFD());
                    in.close();
                    retVal = true;
                } catch (FileNotFoundException e) {
                    Log.e(JtAdManager.JT_AD, "Fail to play video", e);
                } catch (IOException e2) {
                    Log.e(JtAdManager.JT_AD, "Fail to play video", e2);
                }
            } else {
                Log.d(JtAdManager.JT_AD, "video  in cache is not valid downloading new file");
                deleteVideo(item, dbManager, context);
                prepare(widgetSettings, context, true);
            }
        }
        maintainDB(dbManager, context);
        return retVal;
    }

    private static VideoCacheItem getLastItem(DBManager dbManager) {
        return dbManager.selectAllVideoCacheItems().get(0);
    }

    private static void maintainDB(DBManager dbManager, Context context) {
        Log.i(JtAdManager.JT_AD, "maintainDB");
        List<VideoCacheItem> items = dbManager.selectAllVideoCacheItems();
        for (int i = 0; i < items.size(); i++) {
            VideoCacheItem item = items.get(i);
            Log.i(JtAdManager.JT_AD, "found in db: item = " + item.toString());
            if (checkIfExpired(item)) {
                deleteVideo(item, dbManager, context);
            }
        }
    }

    private static boolean checkIfExpired(VideoCacheItem item) {
        return System.currentTimeMillis() - Long.parseLong(item.getDate()) > WEEK_IN_MILLIS;
    }

    private static void deleteVideo(VideoCacheItem item, DBManager dbManager, Context context) {
        Log.i(JtAdManager.JT_AD, "deleting from db item = " + item.toString());
        dbManager.deleteVideoCacheItemById(item.getId());
        File videoFile = new File(context.getCacheDir(), item.getAdID() + ".dat");
        if (videoFile.exists()) {
            videoFile.delete();
        }
    }

    private static void runSaveVideoThread(final DBManager dbManager, final Context context) {
        if (videoUrl == null || videoUrl.equals("")) {
            Log.e(JtAdManager.JT_AD, "cannot save video since url is empty");
        } else {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        if (!JTVideo.isDownLoading) {
                            boolean unused = JTVideo.isDownLoading = true;
                            JTVideo.saveVideoToFile(JTVideo.videoUrl, context);
                            dbManager.insertVideoCacheItem(new VideoCacheItem(JTVideo.adId, "" + System.currentTimeMillis()));
                            boolean unused2 = JTVideo.isDownLoading = false;
                        }
                    } catch (IOException e) {
                        Log.e(JtAdManager.JT_AD, "cannot save video", e);
                    }
                }
            }).start();
        }
    }

    public static void saveVideoToFile(String mediaUrl, Context context) throws IOException {
        URLConnection cn = new URL(mediaUrl).openConnection();
        cn.connect();
        InputStream stream = cn.getInputStream();
        if (stream == null) {
            Log.e(JtAdManager.JT_AD, "Unable to create InputStream for mediaUrl:" + mediaUrl);
            return;
        }
        downloadingMediaFile = new File(context.getCacheDir(), adId + ".dat");
        if (downloadingMediaFile.exists()) {
            downloadingMediaFile.delete();
        }
        FileOutputStream out = new FileOutputStream(downloadingMediaFile);
        byte[] buf = new byte[16384];
        int totalBytesRead = 0;
        while (true) {
            int numread = stream.read(buf);
            if (numread <= 0) {
                stream.close();
                Log.i(JtAdManager.JT_AD, "Done saving file  " + adId + ".dat, total" + totalKbRead + " Kb read");
                return;
            }
            out.write(buf, 0, numread);
            totalBytesRead += numread;
            totalKbRead = totalBytesRead / 1000;
        }
    }

    private static void getAdContent(Context context, String asVideoUrl) {
        try {
            JtAdFetcher fetcher = new JtAdFetcher(context, null);
            try {
                fetcher.setUrl(asVideoUrl);
                String adContent = fetcher.getAdContent();
                adRequestId = fetcher.getAdRequestId();
                parseAdContent(adContent);
            } catch (JtException e) {
                e = e;
                Log.e(JtAdManager.JT_AD, "FAil to get ad content url is =" + asVideoUrl, e);
            }
        } catch (JtException e2) {
            e = e2;
            Log.e(JtAdManager.JT_AD, "FAil to get ad content url is =" + asVideoUrl, e);
        }
    }

    private static boolean isVideoInCache(DBManager dbManager) {
        return dbManager.selectAllVideoCacheItems().size() > 0;
    }

    public static boolean isReady() {
        return JTMediaPlayer.getInstance().isReady();
    }

    protected static String getAdRequestId() {
        return adRequestId;
    }

    protected static List<String> getTrackingUrl() {
        return trackingUrlArr;
    }

    protected static void clearTrackingUrl() {
        trackingUrlArr.clear();
    }

    protected static String getClickThroughUrl() {
        return clickThroughUrl;
    }

    private static void parseAdContent(String adContent) {
        if (adContent == null || adContent.equals("")) {
            Log.e(JtAdManager.JT_AD, "Cannot parse content since adContent is empty");
            return;
        }
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            initParams();
            XMLReader mainReader = spf.newSAXParser().getXMLReader();
            mainReader.setContentHandler(new DefaultHandler() {
                private boolean isAdID = false;
                private boolean isClickThrough = false;
                private boolean isImpression = false;
                private boolean isMediaFile = false;
                private boolean isUrl = false;

                public void startDocument() throws SAXException {
                    super.startDocument();
                }

                public void startElement(String uri, String localName, String name, Attributes atts) throws SAXException {
                    super.startElement(uri, localName, name, atts);
                    if (localName.compareTo(JTVideo.IMPRESSION_STR) == 0) {
                        this.isImpression = true;
                    } else if (localName.compareTo(JTVideo.CLICK_THROUGH_STR) == 0) {
                        this.isClickThrough = true;
                    } else if (localName.compareTo(JTVideo.MEDIA_FILE_STR) == 0) {
                        this.isMediaFile = true;
                    } else if (localName.equals(JTVideo.URL_STR)) {
                        this.isUrl = true;
                    } else if (localName.equals(JTVideo.ADID_STR)) {
                        this.isAdID = true;
                    }
                }

                public void endElement(String uri, String localName, String name) throws SAXException {
                    super.endElement(uri, localName, name);
                    if (localName.equals(JTVideo.IMPRESSION_STR)) {
                        this.isImpression = false;
                    } else if (localName.equals(JTVideo.CLICK_THROUGH_STR)) {
                        this.isClickThrough = false;
                    } else if (localName.equals(JTVideo.MEDIA_FILE_STR)) {
                        this.isMediaFile = false;
                    } else if (localName.equals(JTVideo.URL_STR)) {
                        this.isUrl = false;
                    } else if (localName.equals(JTVideo.ADID_STR)) {
                        this.isAdID = false;
                    }
                }

                public void characters(char[] ch, int start, int length) throws SAXException {
                    super.characters(ch, start, length);
                    String text = new String(ch, start, length).trim();
                    if (!this.isUrl || text == null || text.equals("")) {
                        if (this.isAdID && text != null && !text.equals("")) {
                            String unused = JTVideo.adId = text;
                        }
                    } else if (this.isImpression) {
                        JTVideo.trackingUrlArr.add(text);
                    } else if (this.isClickThrough) {
                        String unused2 = JTVideo.clickThroughUrl = text;
                    } else if (this.isMediaFile) {
                        String unused3 = JTVideo.videoUrl = text;
                    }
                }
            });
            mainReader.parse(new InputSource(new StringReader(adContent)));
        } catch (Exception e) {
            Log.e(JtAdManager.JT_AD, "The following execption was thrown while SAX parsing : " + e.getMessage());
            Log.e(JtAdManager.JT_AD, "SAX fail to parse content:" + adContent);
        }
    }

    private static void initParams() {
        clearTrackingUrl();
        clickThroughUrl = "";
        videoUrl = "";
        adId = "";
    }
}
