package com.jumptap.adtag.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.VideoView;
import com.jumptap.adtag.JtAdWidgetSettings;
import com.jumptap.adtag.JtAdWidgetSettingsFactory;
import com.jumptap.adtag.actions.ActionFactory;
import com.jumptap.adtag.actions.AdAction;
import com.jumptap.adtag.listeners.JtAdViewInnerListener;
import com.jumptap.adtag.media.JTMediaPlayer;
import com.jumptap.adtag.utils.JtAdManager;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class JTVideoActivity extends Activity implements JtAdViewInnerListener, MediaPlayer.OnCompletionListener {
    private static final int LEARN_MORE_BTN_ID = 77777777;
    private static final int SKIP_BTN_ID = 8888888;
    /* access modifiers changed from: private */
    public Button learnMoreBtn;
    /* access modifiers changed from: private */
    public RelativeLayout.LayoutParams learnMoreBtnRlp;
    /* access modifiers changed from: private */
    public Button skipBtn;
    /* access modifiers changed from: private */
    public RelativeLayout.LayoutParams skipBtnRlp;
    private VideoView videoView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initUI();
        initJtMediaPlayer();
        sendTrackingLink();
        configLearnMoreBtnOnClickListener();
    }

    private void configLearnMoreBtnOnClickListener() {
        final String clickThroughUrl = JTVideo.getClickThroughUrl();
        if (clickThroughUrl == null || clickThroughUrl.equals("")) {
            this.learnMoreBtn.setVisibility(8);
        } else {
            this.learnMoreBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    new Thread(new Runnable() {
                        public void run() {
                            AdAction createAction = ActionFactory.createAction(clickThroughUrl, JTVideoActivity.this.getWidgetSettings().getUserAgent(null));
                            createAction.setRedirectedUrl(clickThroughUrl);
                            createAction.perform(JTVideoActivity.this, null);
                            JTVideoActivity.this.closeActivity();
                        }
                    }).start();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        closeActivity();
    }

    private void initJtMediaPlayer() {
        JTMediaPlayer jtMediaPlayer = JTMediaPlayer.getInstance();
        jtMediaPlayer.setOnCompletionListener(this);
        jtMediaPlayer.setVideoView(this.videoView);
    }

    private void sendTrackingLink() {
        List<String> urlArr = JTVideo.getTrackingUrl();
        if (urlArr != null) {
            HttpClient client = new DefaultHttpClient();
            Iterator i$ = urlArr.iterator();
            while (i$.hasNext()) {
                String url = i$.next();
                if ((url != null) && (!url.equals(""))) {
                    try {
                        try {
                            client.execute(new HttpGet(url));
                            Log.i(JtAdManager.JT_AD, "Sending video tracking url succeeded   url=" + url);
                        } catch (ClientProtocolException e) {
                            e = e;
                            Log.e(JtAdManager.JT_AD, "fail to send video tracking url  url=" + url, e);
                        } catch (IOException e2) {
                            e = e2;
                            Log.e(JtAdManager.JT_AD, "fail to send video tracking url  url=" + url, e);
                        }
                    } catch (ClientProtocolException e3) {
                        e = e3;
                        Log.e(JtAdManager.JT_AD, "fail to send video tracking url  url=" + url, e);
                    } catch (IOException e4) {
                        e = e4;
                        Log.e(JtAdManager.JT_AD, "fail to send video tracking url  url=" + url, e);
                    }
                }
            }
        }
    }

    private void initUI() {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        RelativeLayout relLayout = new RelativeLayout(this);
        relLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        setLearnMoreButton();
        this.learnMoreBtnRlp = new RelativeLayout.LayoutParams(width / 2, 35);
        this.learnMoreBtnRlp.addRule(6);
        relLayout.addView(this.learnMoreBtn, this.learnMoreBtnRlp);
        setSkipButton();
        this.skipBtnRlp = new RelativeLayout.LayoutParams(width / 2, 35);
        this.skipBtnRlp.addRule(6);
        this.skipBtnRlp.addRule(1, LEARN_MORE_BTN_ID);
        relLayout.addView(this.skipBtn, this.skipBtnRlp);
        RelativeLayout.LayoutParams videoLp = new RelativeLayout.LayoutParams(-2, -2);
        videoLp.addRule(3, SKIP_BTN_ID);
        this.videoView = new VideoView(this);
        relLayout.addView(this.videoView, videoLp);
        setContentView(relLayout);
    }

    private void setLearnMoreButton() {
        this.learnMoreBtn = new Button(this);
        this.learnMoreBtn.setId(LEARN_MORE_BTN_ID);
        this.learnMoreBtn.setClickable(true);
        this.learnMoreBtn.setText("Learn more");
    }

    private void setSkipButton() {
        this.skipBtn = new Button(this);
        this.skipBtn.setId(SKIP_BTN_ID);
        this.skipBtn.setClickable(true);
        this.skipBtn.setText("Skip");
        this.skipBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new Thread(new Runnable() {
                    public void run() {
                        JTVideoActivity.this.closeActivity();
                    }
                }).start();
            }
        });
    }

    /* access modifiers changed from: private */
    public void closeActivity() {
        runOnUiThread(new Runnable() {
            public void run() {
                JTMediaPlayer.getInstance().release();
                JTVideo.prepare(JtAdWidgetSettingsFactory.createWidgetSettings(), JTVideoActivity.this, true);
                JTVideoActivity.this.finish();
            }
        });
    }

    public void onConfigurationChanged(Configuration newConfig) {
        runOnUiThread(new Runnable() {
            public void run() {
                int width = JTVideoActivity.this.getWindowManager().getDefaultDisplay().getWidth();
                JTVideoActivity.this.skipBtnRlp.width = width / 2;
                JTVideoActivity.this.skipBtn.setLayoutParams(JTVideoActivity.this.skipBtnRlp);
                JTVideoActivity.this.skipBtn.requestLayout();
                JTVideoActivity.this.learnMoreBtnRlp.width = width / 2;
                JTVideoActivity.this.learnMoreBtn.setLayoutParams(JTVideoActivity.this.learnMoreBtnRlp);
                JTVideoActivity.this.learnMoreBtn.requestLayout();
            }
        });
        super.onConfigurationChanged(newConfig);
    }

    public String getAdRequestId() {
        return JTVideo.getAdRequestId();
    }

    public JtAdWidgetSettings getWidgetSettings() {
        return JtAdWidgetSettingsFactory.createWidgetSettings();
    }

    public void onAdError(int errorCode) {
    }

    public void onInterstitialDismissed() {
    }

    public void onNewAd() {
    }

    public void onNoAdFound() {
    }

    public boolean post(Runnable action) {
        return false;
    }

    public void resize(int width, int height, boolean shouldExpand) {
    }

    public void resizeWithCallback(boolean shouldExpand, int width, int height, String callback, int transition, String options) {
    }

    public void onBeginAdInteraction() {
    }

    public void onEndAdInteraction() {
    }

    public void hide() {
    }

    public void setContent(String resContent, String adRequestId) {
    }

    public void onCompletion(MediaPlayer mp) {
        closeActivity();
    }

    public void handleClicks(String URI) {
    }
}
