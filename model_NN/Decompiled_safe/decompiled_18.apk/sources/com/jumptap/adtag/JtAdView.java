package com.jumptap.adtag;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adfonic.android.utils.HtmlFormatter;
import com.jumptap.adtag.actions.ActionFactory;
import com.jumptap.adtag.actions.AdAction;
import com.jumptap.adtag.callbacks.EmptyBodyChecker;
import com.jumptap.adtag.callbacks.JtWebviewCb;
import com.jumptap.adtag.events.EventManager;
import com.jumptap.adtag.events.EventType;
import com.jumptap.adtag.listeners.JtAdViewInnerListener;
import com.jumptap.adtag.utils.JtAccelerator;
import com.jumptap.adtag.utils.JtAdFetcher;
import com.jumptap.adtag.utils.JtAdManager;
import com.jumptap.adtag.utils.JtAdUrlBuilder;
import com.jumptap.adtag.utils.JtException;
import com.jumptap.adtag.utils.JtSettingsParameters;

public class JtAdView extends RelativeLayout implements JtAdViewInnerListener {
    private static final int DISMISS_BTN_HIEGHT = 30;
    private static final int DISMISS_BTN_ID = 999999;
    private static final int DISMISS_BTN_TEXT_SIZE = 20;
    protected static final int MILLIS_IN_SEC = 1000;
    protected int ACTIVE_WEBVIEW_INDEX = 0;
    protected int INACTIVE_WEBVIEW_INDEX = 1;
    private JtAccelerator accel;
    private String adRequestId = "";
    private String adRequestUrl = "";
    private JtAdUrlBuilder adUrlBuilder;
    protected JtAdViewListener adViewListener;
    protected Context context;
    protected TextView dismiss;
    private EmptyBodyChecker emptyBodyChecker;
    protected EventManager eventManager;
    private JtAdFetcher fetcher;
    /* access modifiers changed from: private */
    public ImageView imgView;
    /* access modifiers changed from: private */
    public boolean isOnReceivedErrorCalled = false;
    private boolean isWindowVisible = false;
    private boolean launchedActivity = false;
    /* access modifiers changed from: private */
    public Runnable loadUrlRunnable;
    private ProgressDialog progressDialog;
    private boolean refreshIsImmediate = false;
    private String responseContent = "";
    protected WebView[] webViewArr;
    protected JtAdWidgetSettings widgetSettings = null;

    public JtAdView(Context context2) throws JtException {
        super(context2);
        Log.d(JtAdManager.JT_AD, "JtAdView(Context context)");
        init(context2, null);
    }

    public JtAdView(Context context2, JtAdWidgetSettings widgetSettings2) throws JtException {
        super(context2);
        this.widgetSettings = widgetSettings2;
        Log.d(JtAdManager.JT_AD, "JtAdView(Context context, JtAdWidgetSettings widgetSettings)");
        init(context2, null);
    }

    public JtAdView(Context context2, AttributeSet attrs) throws JtException {
        super(context2, attrs);
        Log.d(JtAdManager.JT_AD, "JtAdView(Context context, AttributeSet attrs)");
        init(context2, attrs);
    }

    public JtAdView(Context context2, AttributeSet attrs, int defStyle) throws JtException {
        super(context2, attrs, defStyle);
        Log.d(JtAdManager.JT_AD, "JtAdView(Context context, AttributeSet attrs, int defStyle)");
        init(context2, attrs);
    }

    public JtAdWidgetSettings getWidgetSettings() {
        return this.widgetSettings;
    }

    public void setWidgetSettings(JtAdWidgetSettings widgetSettings2) {
        this.widgetSettings = widgetSettings2;
    }

    public void setAdViewListener(JtAdViewListener adViewListener2) {
        this.adViewListener = adViewListener2;
    }

    public void refreshAd() {
        refreshAd(0);
    }

    public String getAdRequestUrl() {
        return this.adRequestUrl;
    }

    /* access modifiers changed from: protected */
    public void startTimers(boolean shouldRefreshAd) {
        int refreshPeriod = this.widgetSettings.getRefreshPeriod();
        if (refreshPeriod > 0 && shouldRefreshAd) {
            refreshAd(refreshPeriod);
        }
    }

    /* access modifiers changed from: private */
    public void refreshAd(int refreshPeriod) {
        if (refreshPeriod == 0) {
            this.refreshIsImmediate = true;
        } else {
            this.refreshIsImmediate = false;
        }
        removeCallbacks(this.loadUrlRunnable);
        Handler handler = new Handler(Looper.getMainLooper());
        if (this.refreshIsImmediate) {
            handler.post(this.loadUrlRunnable);
        } else {
            handler.postDelayed(this.loadUrlRunnable, (long) (refreshPeriod * MILLIS_IN_SEC));
        }
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (visibility == 0) {
            this.webViewArr[this.ACTIVE_WEBVIEW_INDEX].loadUrl("javascript:if(typeof ORMMAReady == 'function') { if (!ormma.ready) { ormma.ready = true; ORMMAReady(); } else {console.log(\"not ready\");} } else {console.log(\"no ormmaready\");}");
        }
    }

    public void setContent(final String adContent, String adRequestId2) {
        this.responseContent = adContent;
        this.adRequestId = adRequestId2;
        if (adContent == null) {
            onAdError(-1);
            return;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                JtAdView.this.webViewArr[JtAdView.this.INACTIVE_WEBVIEW_INDEX].loadDataWithBaseURL(null, adContent, HtmlFormatter.TEXT_HTML, HtmlFormatter.UTF_8, null);
            }
        });
        Log.d(JtAdManager.JT_AD, "called load data");
    }

    public void resize(final int width, final int height, final boolean shouldExpand) {
        if (getLayoutParams() != null) {
            post(new Runnable() {
                public void run() {
                    ViewGroup.LayoutParams vlp;
                    int newWidth = shouldExpand ? width : JtAdView.this.getWidthSize();
                    int newHeight = shouldExpand ? height : JtAdView.this.getHeightSize();
                    ViewGroup.LayoutParams lp = JtAdView.this.getLayoutParams();
                    if (lp != null) {
                        int existingWidth = lp.width;
                        int existingHeight = lp.height;
                        if (existingWidth != newWidth || existingHeight != newHeight) {
                            if (shouldExpand) {
                                JtAdView.this.notifyExpand();
                            } else {
                                JtAdView.this.notifyContract();
                            }
                            lp.width = newWidth;
                            lp.height = newHeight;
                            View v = JtAdView.this.webViewArr[JtAdView.this.ACTIVE_WEBVIEW_INDEX];
                            if (v != null && (vlp = v.getLayoutParams()) != null) {
                                vlp.width = newWidth;
                                vlp.height = newHeight;
                                JtAdView.this.requestLayout();
                            }
                        }
                    }
                }
            });
        }
    }

    public void hide() {
        post(new Runnable() {
            public void run() {
                JtAdView.this.setVisibility(4);
            }
        });
        if (this.adViewListener != null) {
            this.adViewListener.onHide(this, getId());
        }
    }

    public void resizeWithCallback(boolean shouldExpand, int width, int height, final String callback, int transition, String options) {
        Log.d(JtAdManager.JT_AD, "JtAdView::resizeWithCallback (" + callback + ")");
        resize(width, height, shouldExpand);
        post(new Runnable() {
            public void run() {
                Log.d(JtAdManager.JT_AD, "JtAdView/Runnable::run (" + callback + ")");
                JtAdView.this.webViewArr[JtAdView.this.ACTIVE_WEBVIEW_INDEX].loadUrl("javascript:executeCallback('" + callback + "');");
            }
        });
    }

    public String getAdRequestId() {
        return this.adRequestId;
    }

    private void init(Context context2, AttributeSet attrs) throws JtException {
        Log.d(JtAdManager.JT_AD, "getting into init()");
        this.context = context2;
        this.loadUrlRunnable = new Runnable() {
            public void run() {
                JtAdView.this.loadUrlIfVisible();
            }
        };
        try {
            initWidgetSettings(attrs);
            setMinimumWidth(getWidthSize());
            setMinimumHeight(getHeightSize());
            this.adUrlBuilder = new JtAdUrlBuilder(this.widgetSettings, context2);
            this.eventManager = new EventManager(context2, this);
            this.emptyBodyChecker = new EmptyBodyChecker(this, this.eventManager);
            setVerticalScrollBarEnabled(false);
            setHorizontalScrollBarEnabled(false);
            initAlternativeImgView();
            initWebViewArray();
            initDismissButton();
            addView(this.dismiss, new RelativeLayout.LayoutParams(getWidthSize(), getDismissBtnHeight()));
            RelativeLayout.LayoutParams viewsRlp = new RelativeLayout.LayoutParams(getWidthSize(), getHeightSize());
            viewsRlp.addRule(3, DISMISS_BTN_ID);
            viewsRlp.addRule(14);
            addView(this.webViewArr[this.INACTIVE_WEBVIEW_INDEX], viewsRlp);
            addView(this.webViewArr[this.ACTIVE_WEBVIEW_INDEX], viewsRlp);
            addView(this.imgView, viewsRlp);
            this.fetcher = new JtAdFetcher(context2, this);
            this.fetcher.setShouldDebugNetworkTraffic(this.widgetSettings.getShouldDebugNetworkTraffic());
        } catch (JtException e) {
            Log.e(JtAdManager.JT_AD, e.getMessage());
            throw e;
        }
    }

    private void initDismissButton() {
        this.dismiss = new TextView(this.context);
        this.dismiss.setId(DISMISS_BTN_ID);
        this.dismiss.setText(this.widgetSettings.getDismissButtonLabel());
        this.dismiss.setVisibility(8);
        this.dismiss.setClickable(true);
        this.dismiss.setTextColor(-16777216);
        this.dismiss.setTextSize(20.0f);
        this.dismiss.setBackgroundColor(-7829368);
    }

    /* access modifiers changed from: protected */
    public int getHeightSize() {
        return (int) (((float) this.widgetSettings.getHeight()) * getDensity());
    }

    /* access modifiers changed from: protected */
    public int getWidthSize() {
        return (int) (((float) this.widgetSettings.getWidth()) * getDensity());
    }

    /* access modifiers changed from: protected */
    public int getDismissBtnHeight() {
        return (int) (30.0f * getDensity());
    }

    private float getDensity() {
        return getContext().getResources().getDisplayMetrics().density;
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int visibility) {
        Log.d(JtAdManager.JT_AD, "visibility=" + visibility);
        super.onWindowVisibilityChanged(visibility);
        switch (visibility) {
            case 0:
                if (this.webViewArr[this.ACTIVE_WEBVIEW_INDEX].getVisibility() == 0) {
                    this.webViewArr[this.ACTIVE_WEBVIEW_INDEX].bringToFront();
                }
                if (this.launchedActivity) {
                    setLaunchedActivity(false);
                    notifyReturnFromActivity();
                }
                if (!this.isWindowVisible) {
                    this.isWindowVisible = true;
                    if (this.imgView != null) {
                        setBgAndImg();
                        this.imgView.setVisibility(0);
                    }
                    if (getWidgetSettings().getRefreshPeriod() > 0) {
                        loadUrlIfVisible();
                        return;
                    }
                    return;
                }
                return;
            case 4:
            case 8:
                dismissProgressDialog();
                this.isWindowVisible = false;
                if (this.eventManager != null) {
                }
                return;
            default:
                this.isWindowVisible = false;
                return;
        }
    }

    /* access modifiers changed from: private */
    public void loadUrlIfVisible() {
        int visibility = getVisibility();
        if (!this.refreshIsImmediate && (!this.isWindowVisible || 0 != 0)) {
            return;
        }
        if (this.context.checkCallingOrSelfPermission("android.permission.INTERNET") != 0) {
            Log.e(JtAdManager.JT_AD, "JtAdView: Requires INTERNET permission");
            onAdError(-1);
        } else if (this.adUrlBuilder != null) {
            this.adRequestUrl = this.adUrlBuilder.getAdUrl(this.webViewArr[this.INACTIVE_WEBVIEW_INDEX]);
            if (this.widgetSettings.getShouldDebugNetworkTraffic()) {
                Log.d(JtAdManager.JT_AD, "Base url : " + this.adRequestUrl);
            }
            getUrlFromNetwork();
        }
    }

    private void getUrlFromNetwork() {
        this.fetcher.kickOffUrlFetch(this.adRequestUrl);
    }

    private void initWebViewArray() {
        this.webViewArr = new WebView[2];
        this.webViewArr[this.ACTIVE_WEBVIEW_INDEX] = new WebView(this.context);
        initWebView(this.webViewArr[this.ACTIVE_WEBVIEW_INDEX]);
        this.webViewArr[this.INACTIVE_WEBVIEW_INDEX] = new WebView(this.context);
        initWebView(this.webViewArr[this.INACTIVE_WEBVIEW_INDEX]);
    }

    private void initWebView(WebView webView) {
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVisibility(4);
        webView.setBackgroundColor(this.widgetSettings.getBackgroundColor());
        binderBrowser(webView);
        webView.setWebViewClient(new JtAdWebViewClient());
        JtAdListener jtAdListener = new JtAdListener(this, getId());
        webView.setOnTouchListener(jtAdListener);
        webView.setOnFocusChangeListener(jtAdListener);
        webView.setOnClickListener(jtAdListener);
        webView.setOnLongClickListener(jtAdListener);
    }

    private void binderBrowser(WebView webView) {
        webView.addJavascriptInterface(new JtWebviewCb(this.context, this), "JtVwCb");
        this.accel = new JtAccelerator(this.context, webView);
        webView.addJavascriptInterface(this.accel, "Accel");
        webView.addJavascriptInterface(this.eventManager, "Tracking");
        webView.addJavascriptInterface(this.emptyBodyChecker, "EmptyBodyChecker");
    }

    private void initAlternativeImgView() {
        this.imgView = new ImageView(this.context);
        setBgAndImg();
    }

    private void setBgAndImg() {
        int backgroundColor = this.widgetSettings.getBackgroundColor();
        if (backgroundColor != -1) {
            this.imgView.setBackgroundColor(backgroundColor);
        }
        Bitmap bm = this.widgetSettings.getAlternateImage();
        if (bm != null) {
            Log.d(JtAdManager.JT_AD, "Loading user's alternate image");
            this.imgView.setImageBitmap(bm);
        }
    }

    private void initWidgetSettings(AttributeSet attributes) throws JtException {
        if (this.widgetSettings == null) {
            this.widgetSettings = JtAdWidgetSettingsFactory.createWidgetSettings();
        }
        JtSettingsParameters.populateSettings(this.widgetSettings, attributes, this.context);
        if (this.widgetSettings.getPublisherId() == null || "".equals(this.widgetSettings.getPublisherId())) {
            throw new JtException("Publisher id was not set. Please set it and try again");
        }
        if (this.widgetSettings.getApplicationId() == null || "".equals(this.widgetSettings.getApplicationId())) {
            Log.e(JtAdManager.JT_AD, "Application id was not set. empty application id will be sent to Jumptap servers");
        }
        if (this.widgetSettings.getApplicationVersion() == null || "".equals(this.widgetSettings.getApplicationVersion())) {
            Log.e(JtAdManager.JT_AD, "Application version was not set. empty application version will be sent to Jumptap servers");
        }
    }

    /* access modifiers changed from: private */
    public void dismissProgressDialog() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
            this.progressDialog = null;
        }
    }

    public void onAdError(int errorCode) {
        resize(0, 0, true);
        if (this.adViewListener != null) {
            this.adViewListener.onAdError(this, getId(), errorCode);
        }
        startTimers(false);
    }

    public void onNoAdFound() {
        resize(0, 0, true);
        if (this.adViewListener != null) {
            this.adViewListener.onNoAdFound(this, getId());
        }
    }

    public void onInterstitialDismissed() {
        if (this.adViewListener != null) {
            this.adViewListener.onInterstitialDismissed(this, getId());
        }
    }

    public void onNewAd() {
        if (this.adViewListener != null) {
            this.adViewListener.onNewAd(this, getId(), this.responseContent);
        }
    }

    public void onBeginAdInteraction() {
        if (this.adViewListener != null) {
            this.adViewListener.onBeginAdInteraction(this, getId());
        }
    }

    public void onEndAdInteraction() {
        if (this.adViewListener != null) {
            this.adViewListener.onEndAdInteraction(this, getId());
        }
    }

    public void notifyExpand() {
        if (this.adViewListener != null) {
            this.adViewListener.onExpand(this, getId());
        }
    }

    public void notifyContract() {
        if (this.adViewListener != null) {
            this.adViewListener.onContract(this, getId());
        }
    }

    public void notifyAdClicked() {
        if (this.adViewListener != null) {
            this.adViewListener.onBannerClicked(this, getId());
        }
    }

    public void notifyLaunchActivity() {
        if (this.adViewListener != null) {
            this.adViewListener.onLaunchActivity(this, getId());
        }
    }

    public void notifyReturnFromActivity() {
        if (this.adViewListener != null) {
            this.adViewListener.onReturnFromActivity(this, getId());
        }
    }

    public void handleClicks(String url) {
        this.progressDialog = ProgressDialog.show(this.context, "", "", true, true);
        new Thread(new PerformActionHandler(url, this)).start();
    }

    public void setLaunchedActivity(boolean launchedActivity2) {
        this.launchedActivity = launchedActivity2;
    }

    protected class JtAdListener implements View.OnTouchListener, View.OnFocusChangeListener, View.OnClickListener, View.OnLongClickListener {
        private JtAdView widget = null;
        private int widgetId = -1;

        public JtAdListener(JtAdView widget2, int widgetId2) {
            this.widget = widget2;
            this.widgetId = widgetId2;
        }

        public boolean onTouch(View v, MotionEvent event) {
            if (this.widget.getWidgetSettings().getRefreshPeriod() <= 0) {
                return false;
            }
            JtAdView.this.refreshAd(60);
            return false;
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (JtAdView.this.adViewListener != null) {
                JtAdView.this.adViewListener.onFocusChange(this.widget, this.widgetId, hasFocus);
            }
            if (hasFocus) {
                JtAdView.this.removeCallbacks(JtAdView.this.loadUrlRunnable);
            } else if (this.widget.getWidgetSettings().getRefreshPeriod() > 0) {
                JtAdView.this.refreshAd(60);
            }
        }

        public void onClick(View v) {
        }

        public boolean onLongClick(View v) {
            return false;
        }
    }

    private class JtAdWebViewClient extends WebViewClient {
        private JtAdWebViewClient() {
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.d(JtAdManager.JT_AD, "onPageFinished=" + url);
            JtAdView.this.resize(JtAdView.this.getWidthSize(), JtAdView.this.getHeightSize(), false);
            view.loadUrl("javascript:EmptyBodyChecker.checkBody(document.getElementsByTagName(\"body\")[0].innerHTML)");
            if (!JtAdView.this.isOnReceivedErrorCalled) {
                JtAdView.this.imgView.setVisibility(4);
                switchActiveAndInactiveIndex();
                JtAdView.this.webViewArr[JtAdView.this.INACTIVE_WEBVIEW_INDEX].setVisibility(4);
                JtAdView.this.webViewArr[JtAdView.this.ACTIVE_WEBVIEW_INDEX].setVisibility(0);
                if (JtAdView.this.adViewListener == null) {
                    JtAdView.this.eventManager.forceSendingInteractEvent();
                    JtAdView.this.eventManager.sendReport(EventType.impression);
                }
            }
            boolean unused = JtAdView.this.isOnReceivedErrorCalled = false;
            JtAdView.this.startTimers(true);
            if (JtAdView.this.getVisibility() == 0) {
                view.loadUrl("javascript:if(typeof ORMMAReady == 'function') { if (!ormma.ready) { ormma.ready = true; ORMMAReady(); } else {console.log(\"not ready\");} } else {console.log(\"no ormmaready\");}");
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Log.e(JtAdManager.JT_AD, "errorcode=" + errorCode + " desc=" + description);
            boolean unused = JtAdView.this.isOnReceivedErrorCalled = true;
            JtAdView.this.onAdError(errorCode);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            boolean shouldOverrideUrlLoading = false;
            if (view.equals(JtAdView.this.webViewArr[JtAdView.this.ACTIVE_WEBVIEW_INDEX])) {
                JtAdView.this.handleClicks(url);
                shouldOverrideUrlLoading = true;
            } else {
                Log.e(JtAdManager.JT_AD, "shouldOverrideUrlLoading cannot override url: " + url);
            }
            Log.d(JtAdManager.JT_AD, "shouldOverrideUrlLoading=" + shouldOverrideUrlLoading);
            return shouldOverrideUrlLoading;
        }

        private void switchActiveAndInactiveIndex() {
            JtAdView.this.INACTIVE_WEBVIEW_INDEX = (JtAdView.this.INACTIVE_WEBVIEW_INDEX + 1) % 2;
            JtAdView.this.ACTIVE_WEBVIEW_INDEX = (JtAdView.this.ACTIVE_WEBVIEW_INDEX + 1) % 2;
        }
    }

    private class PerformActionHandler implements Runnable {
        String url;
        JtAdView widget;

        PerformActionHandler(String url2, JtAdView adView) {
            this.url = url2;
            this.widget = adView;
        }

        public void run() {
            AdAction action = ActionFactory.createAction(this.url, JtAdView.this.widgetSettings.getUserAgent(null));
            if (action != null) {
                JtAdView.this.notifyAdClicked();
                action.perform(JtAdView.this.context, this.widget);
            } else {
                Log.e(JtAdManager.JT_AD, "Cannot perform action or find AdAction for url: " + this.url);
            }
            JtAdView.this.dismissProgressDialog();
        }
    }
}
