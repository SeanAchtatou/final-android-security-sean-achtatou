package com.jumptap.adtag.callbacks;

import android.util.Log;
import com.jumptap.adtag.events.EventManager;
import com.jumptap.adtag.listeners.JtAdViewInnerListener;
import com.jumptap.adtag.utils.JtAdManager;

public class EmptyBodyChecker {
    private EventManager eventManager = null;
    private JtAdViewInnerListener innerListener = null;

    public EmptyBodyChecker(JtAdViewInnerListener innerListener2, EventManager eventManager2) {
        this.eventManager = eventManager2;
        this.innerListener = innerListener2;
    }

    public void checkBody(String bodyHtml) {
        if (bodyHtml.length() == 0) {
            Log.d(JtAdManager.JT_AD, "Ad was not found.");
            if (this.innerListener != null) {
                this.innerListener.onNoAdFound();
            }
        } else if (this.innerListener != null) {
            this.innerListener.onNewAd();
        }
    }
}
