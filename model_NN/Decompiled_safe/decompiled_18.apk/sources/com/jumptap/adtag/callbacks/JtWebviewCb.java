package com.jumptap.adtag.callbacks;

import android.content.Context;
import android.util.Log;
import com.jumptap.adtag.listeners.JtAdViewInnerListener;
import com.jumptap.adtag.utils.JtAdManager;

public class JtWebviewCb {
    private JtAdViewInnerListener adView;
    private Context context;

    public JtWebviewCb(Context ctx, JtAdViewInnerListener adView2) {
        this.context = ctx;
        this.adView = adView2;
    }

    public void setSize(int a, int b, int c, int d, boolean shouldExpand) {
        Log.d(JtAdManager.JT_AD, "JtWebViewCB::setSize a:" + a + " b:" + b + " c:" + c + " d:" + d + " shouldExpand:" + shouldExpand);
        float density = this.context.getResources().getDisplayMetrics().density;
        this.adView.resize((int) (((float) c) * density), (int) (((float) d) * density), shouldExpand);
    }

    public void resizeViewer(int left, int top, int right, int bottom, int transition, String options, String callback) {
        Log.d(JtAdManager.JT_AD, "JtWebViewCB::resizeViewer left: " + left + " top:" + top + " right: " + right + " bottom: " + bottom + " transition: " + transition + " options: " + options + " callback: " + callback);
    }

    public String getDeviceId() {
        String andId = JtAdManager.getHID(this.context);
        Log.d(JtAdManager.JT_AD, "JtWebViewCB::getDeviceId andId=" + andId);
        return andId;
    }

    public void expandTo(int width, int height, String callback, int transition, String options) {
        Log.d(JtAdManager.JT_AD, "JtWebViewCB::expandTo (" + callback + ")");
        float density = this.context.getResources().getDisplayMetrics().density;
        int w = (int) (((float) width) * density);
        this.adView.resizeWithCallback(true, w, (int) (((float) height) * density), callback, transition, options);
    }

    public void restoreToBanner(String callback, int transition, String options) {
        this.adView.resizeWithCallback(false, 0, 0, callback, transition, options);
    }

    public String getScreenSize() {
        float density = this.context.getResources().getDisplayMetrics().density;
        int heightPixels = this.context.getResources().getDisplayMetrics().heightPixels;
        return "{\"width\": " + ((int) (((float) this.context.getResources().getDisplayMetrics().widthPixels) / density)) + ",\"height\":" + ((int) (((float) heightPixels) / density)) + "}";
    }

    public void openURI(String URI, String contentType, String options) {
        Log.d(JtAdManager.JT_AD, "JtWebViewCB::openURI URI: " + URI + " contentType:" + contentType + " options: " + options);
        this.adView.handleClicks(URI);
    }

    public String getLocation(String callback, String options) {
        Log.d(JtAdManager.JT_AD, "JtWebViewCB::getLocation options: " + options + " callback: " + callback + "  location=" + "Kuku");
        return "Kuku";
    }

    public void hideWidget() {
        this.adView.hide();
    }
}
