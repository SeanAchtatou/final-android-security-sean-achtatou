package com.jumptap.adtag;

import android.app.Activity;
import android.util.Log;
import com.jumptap.adtag.events.EventManager;
import com.jumptap.adtag.events.EventType;
import com.jumptap.adtag.utils.JtAdManager;
import java.util.HashMap;
import java.util.Map;

public class JTAppReport {
    public static void sendConversionTrackingReport(Activity activity, String appId, String appVer, Map<String, String> paramMap) {
        Log.d(JtAdManager.JT_AD, "Application request to send conversion tracking url");
        String hid = JtAdManager.getHID(activity);
        EventType eventType = getEventType(activity);
        String date = EventManager.getDateByEventType(eventType, activity);
        if (paramMap == null) {
            paramMap = new HashMap<>();
        }
        paramMap.put(EventManager.HID_STRING, hid);
        paramMap.put(EventManager.APP_ID_STRING, appId);
        paramMap.put(EventManager.APP_VER_STRING, appVer);
        paramMap.put(EventManager.EVENT_STRING, eventType.name());
        paramMap.put(EventManager.DATE_STRING, date);
        EventManager.sendReport(activity, EventManager.buildEventTrackingUrl(paramMap), eventType, date);
    }

    private static EventType getEventType(Activity activity) {
        String firstLaunchStr = EventManager.getFirstLaunchStr();
        String preferences = JtAdManager.getPreferences(activity, EventManager.getFirstPrefName(), firstLaunchStr);
        if (preferences == null || "".equals(preferences) || firstLaunchStr.equals(preferences)) {
            return EventType.install;
        }
        return EventType.run;
    }
}
