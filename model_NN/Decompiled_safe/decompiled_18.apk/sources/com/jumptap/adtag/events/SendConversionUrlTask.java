package com.jumptap.adtag.events;

import android.content.Context;
import android.util.Log;
import com.jumptap.adtag.db.DBManager;
import com.jumptap.adtag.utils.JtAdManager;
import java.io.IOException;
import java.util.TimerTask;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class SendConversionUrlTask extends TimerTask {
    private Context context;

    public SendConversionUrlTask(Context context2) {
        this.context = context2;
    }

    public void run() {
        Log.d("JtAd-Tracking", "SendConversionUrlTask woke up");
        DBManager dbManager = DBManager.getInstance(this.context);
        for (JtEvent event : dbManager.selectAllEvents()) {
            if (sendReportToTL(this.context, event)) {
                dbManager.deleteEventById(event.getId());
            }
        }
    }

    private static boolean sendReportToTL(Context context2, JtEvent event) {
        String url = event.getUrl();
        EventType eventType = event.getEventType();
        String date = event.getDate();
        Log.i("JtAd-Tracking", "sending url to Jumptap servers:" + url);
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        get.setHeader("User-Agent", System.getProperty("http.agent"));
        try {
            int statusCode = client.execute(get).getStatusLine().getStatusCode();
            Log.i("JtAd-Tracking", "conversion/event tracking response status code:" + statusCode);
            if (statusCode == 200) {
                if (EventType.install.equals(eventType) && context2 != null) {
                    JtAdManager.savePreferences(context2, "isFirstLaunch", "0");
                    JtAdManager.removePreferences(context2, "installDate");
                }
                return true;
            } else if (!EventType.install.equals(eventType) || context2 == null) {
                return false;
            } else {
                JtAdManager.savePreferences(context2, "isFirstLaunch", date);
                return false;
            }
        } catch (IOException e) {
            Log.e("JtAd-Tracking", "JTAppReport.sendReportToTL:" + e.toString());
            return false;
        }
    }
}
