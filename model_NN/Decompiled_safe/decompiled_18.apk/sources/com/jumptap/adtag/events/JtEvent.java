package com.jumptap.adtag.events;

public class JtEvent {
    private String date;
    private EventType eventType;
    private int id;
    private String url;

    public JtEvent() {
        initEvent(-1, null, null, null);
    }

    public JtEvent(String url2, EventType eventType2, String date2) {
        initEvent(-1, url2, eventType2, date2);
    }

    public JtEvent(int id2, String url2, EventType eventType2, String date2) {
        initEvent(id2, url2, eventType2, date2);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url2) {
        this.url = url2;
    }

    public EventType getEventType() {
        return this.eventType;
    }

    public void setEventType(EventType eventType2) {
        this.eventType = eventType2;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String toString() {
        return "id=" + this.id + " ,eventType=" + this.eventType + " ,date=" + this.date + " ,url=" + this.url;
    }

    private void initEvent(int id2, String url2, EventType eventType2, String date2) {
        this.id = id2;
        this.url = url2;
        this.eventType = eventType2;
        this.date = date2;
    }
}
