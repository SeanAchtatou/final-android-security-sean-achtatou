package com.jumptap.adtag.events;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import com.jumptap.adtag.db.DBManager;
import com.jumptap.adtag.listeners.JtAdViewInnerListener;
import com.jumptap.adtag.utils.JtAdManager;
import com.jumptap.adtag.utils.JtAdUrlBuilder;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class EventManager {
    private static final String AD_REQUEST_ID_STRING = "jtreqid";
    private static final String AMP_STRING = "&";
    public static final String APP_ID_STRING = "app";
    public static final String APP_VER_STRING = "appVer";
    private static final String CONVERSION_TRACKING_URL = "http://a.jumptap.com/a/conversion?";
    private static final String DATE_FORMAT = "yyyyMMddhhmmss";
    public static final String DATE_STRING = "date";
    public static final int DELAY_AFTER_INTERACTION = 60;
    private static final String DURATION_STRING = "duration";
    private static final String EQUAL_STRING = "=";
    public static final String EVENT_STRING = "event";
    private static final String FIRST_LAUNCH = "1";
    public static final String HID_STRING = "hid";
    protected static final String INSTALL_DATE_PREF_NAME = "installDate";
    protected static final String IS_FIRST_PREF_NAME = "isFirstLaunch";
    protected static final String JT_AD_TRACKING = "JtAd-Tracking";
    protected static final String NOT_FIRST_LAUNCH = "0";
    private static final String PUB_STRING = "pub";
    private static final String SPOT_STRING = "spot";
    private static final String UNITS_STRING = "units";
    private static Timer eventTimer;
    /* access modifiers changed from: private */
    public JtAdViewInnerListener adViewListener;
    private Context context;
    /* access modifiers changed from: private */
    public EventInteractionInfo eventInteractionInfo = new EventInteractionInfo();
    private InteractEventTask interEventTask;

    public EventManager(Context context2, JtAdViewInnerListener adViewListener2) {
        this.context = context2;
        this.adViewListener = adViewListener2;
        initTimer();
        scheduleConversionTask(context2);
    }

    public void sendReport(EventType eventType) {
        sendReport(eventType, null);
    }

    public void close() {
        DBManager.getInstance(this.context).close();
    }

    /* access modifiers changed from: private */
    public void sendReport(EventType eventType, Map<String, String> paramMap) {
        Log.i(JT_AD_TRACKING, "sendReport: eventType=" + eventType.name());
        String date = getDateByEventType(eventType, this.context);
        if (paramMap == null) {
            paramMap = new HashMap<>();
        }
        populateParamsMap(this.context, this.adViewListener, eventType, date, paramMap);
        sendReport(this.context, buildEventTrackingUrl(paramMap), eventType, date);
    }

    public void startInteraction() {
        Log.d(JT_AD_TRACKING, "startInteraction");
        this.eventInteractionInfo.interactionStarted();
        this.adViewListener.onBeginAdInteraction();
        stopTimer();
    }

    public void stopInteraction() {
        Log.d(JT_AD_TRACKING, "stopInteraction");
        this.eventInteractionInfo.interactionEnded();
        this.adViewListener.onEndAdInteraction();
        startTimer();
    }

    public void forceSendingInteractEvent() {
        stopInteraction();
        Log.d(JT_AD_TRACKING, "forceSendingInteractEvent");
        new Thread(this.interEventTask).start();
    }

    public static String getFirstPrefName() {
        return IS_FIRST_PREF_NAME;
    }

    public static String getFirstLaunchStr() {
        return FIRST_LAUNCH;
    }

    public static String buildEventTrackingUrl(Map<String, String> paramMap) {
        StringBuilder url = new StringBuilder(CONVERSION_TRACKING_URL);
        if (paramMap != null && paramMap.size() > 0) {
            for (Map.Entry<String, String> entry : paramMap.entrySet()) {
                url.append(AMP_STRING);
                url.append((String) entry.getKey());
                url.append(EQUAL_STRING);
                url.append(JtAdUrlBuilder.encodeParam((String) entry.getValue()));
            }
        }
        return url.toString();
    }

    public static String getDateByEventType(EventType eventType, Context context2) {
        if (!EventType.install.equals(eventType) || context2 == null) {
            return (String) DateFormat.format(DATE_FORMAT, System.currentTimeMillis());
        }
        return getDateForInstallEvent(context2);
    }

    private static String getDateForInstallEvent(Context context2) {
        String dateFromPref = JtAdManager.getPreferences(context2, INSTALL_DATE_PREF_NAME, null);
        if (dateFromPref == null) {
            return (String) DateFormat.format(DATE_FORMAT, System.currentTimeMillis());
        }
        return dateFromPref;
    }

    private void populateParamsMap(Context con, JtAdViewInnerListener adViewLst, EventType eventType, String date, Map<String, String> paramMap) {
        String hid = JtAdManager.getHID(con);
        String appId = adViewLst.getWidgetSettings().getApplicationId();
        String appVer = adViewLst.getWidgetSettings().getApplicationVersion();
        paramMap.put(EVENT_STRING, eventType.value());
        paramMap.put(HID_STRING, hid);
        paramMap.put(APP_ID_STRING, appId);
        paramMap.put(APP_VER_STRING, appVer);
        paramMap.put(DATE_STRING, date);
        paramMap.put(AD_REQUEST_ID_STRING, adViewLst.getAdRequestId());
        paramMap.put(PUB_STRING, adViewLst.getWidgetSettings().getPublisherId());
        paramMap.put(SPOT_STRING, adViewLst.getWidgetSettings().getSpotId());
    }

    public static void sendReport(Context context2, String url, EventType eventType, String date) {
        initTimer();
        new Thread(new LogEvent(context2, new JtEvent(url, eventType, date))).start();
    }

    private static void initTimer() {
        if (eventTimer == null) {
            eventTimer = new Timer("EventManagerTimer");
        }
    }

    /* access modifiers changed from: private */
    public static void scheduleConversionTask(Context context2) {
        eventTimer.schedule(new SendConversionUrlTask(context2), 0);
    }

    private void startTimer() {
        Log.i(JT_AD_TRACKING, "Starting Interaction Event Task timer");
        if (this.interEventTask != null) {
            this.interEventTask.cancel();
        }
        this.interEventTask = new InteractEventTask();
        eventTimer.schedule(this.interEventTask, 60000);
    }

    private void stopTimer() {
        Log.i(JT_AD_TRACKING, "stopping Interaction Event Task timer");
        if (this.interEventTask != null) {
            this.interEventTask.cancel();
        }
    }

    private static class LogEvent implements Runnable {
        private Context context;
        private JtEvent event;

        public LogEvent(Context context2, JtEvent event2) {
            this.context = context2;
            this.event = event2;
        }

        public void run() {
            DBManager.getInstance(this.context).insertEvent(this.event);
            EventManager.scheduleConversionTask(this.context);
        }
    }

    private class InteractEventTask extends TimerTask {
        private InteractEventTask() {
        }

        public void run() {
            EventManager.this.adViewListener.post(new Runnable() {
                public void run() {
                    long duration = EventManager.this.eventInteractionInfo.getDuration();
                    if (duration > 0) {
                        Log.i(EventManager.JT_AD_TRACKING, "performing Interaction Event Task timer");
                        Map<String, String> paramMap = new HashMap<>();
                        paramMap.put(EventManager.DURATION_STRING, "" + duration);
                        paramMap.put(EventManager.UNITS_STRING, "" + EventManager.this.eventInteractionInfo.getNumOfInteractions());
                        Log.i(EventManager.JT_AD_TRACKING, "reset");
                        EventManager.this.eventInteractionInfo.reset();
                        EventManager.this.sendReport(EventType.interact, paramMap);
                    }
                }
            });
        }
    }

    private class EventInteractionInfo {
        private long endInteractionTime;
        private int numOfInteractions;
        private long startInteractionTime;

        EventInteractionInfo() {
            reset();
        }

        public void interactionStarted() {
            if (this.startInteractionTime == -1) {
                this.startInteractionTime = System.currentTimeMillis();
            }
            Log.d(EventManager.JT_AD_TRACKING, "interactionStarted: startInteractionTime=" + this.startInteractionTime + " endInteractionTime=" + this.endInteractionTime);
            this.numOfInteractions++;
        }

        public void interactionEnded() {
            if (this.startInteractionTime != -1) {
                this.endInteractionTime = System.currentTimeMillis();
            }
            Log.d(EventManager.JT_AD_TRACKING, "interactionEnded: startInteractionTime=" + this.startInteractionTime + " endInteractionTime=" + this.endInteractionTime);
        }

        public void reset() {
            this.startInteractionTime = -1;
            this.endInteractionTime = -1;
            this.numOfInteractions = 0;
        }

        public long getDuration() {
            return this.endInteractionTime - this.startInteractionTime;
        }

        public int getNumOfInteractions() {
            return this.numOfInteractions;
        }
    }
}
