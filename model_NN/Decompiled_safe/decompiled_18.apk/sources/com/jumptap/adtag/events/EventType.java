package com.jumptap.adtag.events;

public enum EventType {
    run("run"),
    install("install"),
    impression("impression"),
    click("click"),
    interact("interact"),
    adVideo25("25view"),
    adVideo50("50view"),
    adVideo75("75view"),
    adVideo100("100view"),
    dismiss("dismiss");
    
    private final String value;

    private EventType(String value2) {
        this.value = value2;
    }

    public final String value() {
        return this.value;
    }
}
