package com.jumptap.adtag.media;

import android.media.MediaPlayer;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.VideoView;
import com.jumptap.adtag.utils.JtAdManager;
import java.io.FileDescriptor;

public class JTMediaPlayer implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, SurfaceHolder.Callback, View.OnTouchListener, MediaPlayer.OnErrorListener {
    private static JTMediaPlayer jtMediaPlayer = null;
    private SurfaceHolder holder;
    private boolean isPrepared = false;
    private int mVideoHeight;
    private int mVideoWidth;
    private MediaPlayer mediaPlayer;
    private MediaPlayer.OnCompletionListener onCompletionListener;

    private JTMediaPlayer() {
    }

    public static JTMediaPlayer getInstance() {
        if (jtMediaPlayer == null) {
            jtMediaPlayer = new JTMediaPlayer();
        }
        return jtMediaPlayer;
    }

    public boolean isReady() {
        return this.isPrepared;
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener completionListener) {
        this.onCompletionListener = completionListener;
    }

    public void prepareVideo(FileDescriptor fileDescriptor) {
        try {
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.setDataSource(fileDescriptor);
            this.mediaPlayer.setScreenOnWhilePlaying(true);
            this.mediaPlayer.prepare();
            this.mediaPlayer.setOnBufferingUpdateListener(this);
            this.mediaPlayer.setOnCompletionListener(this);
            this.mediaPlayer.setOnPreparedListener(this);
            this.mediaPlayer.setAudioStreamType(3);
        } catch (Exception e) {
            Log.e(JtAdManager.JT_AD, "error: " + e.getMessage(), e);
        }
    }

    public int getCurrentPosition() {
        if (this.mediaPlayer == null || !this.isPrepared) {
            return 0;
        }
        return this.mediaPlayer.getCurrentPosition();
    }

    public int getDuration() {
        if (this.mediaPlayer == null || !this.isPrepared) {
            return -1;
        }
        try {
            return this.mediaPlayer.getDuration();
        } catch (Exception e) {
            Log.e(JtAdManager.JT_AD, "Problem in getDuration", e);
            return -1;
        }
    }

    public void pause() {
        if (this.mediaPlayer != null && this.isPrepared && this.mediaPlayer.isPlaying()) {
            this.mediaPlayer.pause();
        }
    }

    public void play() {
        if (this.mediaPlayer != null && this.isPrepared) {
            this.mediaPlayer.start();
        }
    }

    public void stop() {
        this.mediaPlayer.stop();
        this.isPrepared = false;
    }

    public void setVideoView(VideoView videoView) {
        this.holder = videoView.getHolder();
        this.holder.addCallback(this);
        this.holder.setType(3);
        videoView.setOnTouchListener(this);
        videoView.setFocusable(true);
        videoView.setFocusableInTouchMode(true);
    }

    public void release() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.reset();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
        this.isPrepared = false;
        this.holder = null;
    }

    public void onBufferingUpdate(MediaPlayer arg0, int percent) {
        Log.d(JtAdManager.JT_AD, "onBufferingUpdate percent:" + percent);
    }

    public void onCompletion(MediaPlayer mp) {
        Log.d(JtAdManager.JT_AD, "onCompletion called");
        if (this.onCompletionListener != null) {
            this.onCompletionListener.onCompletion(this.mediaPlayer);
        }
    }

    public void onPrepared(MediaPlayer mediaplayer) {
        Log.d(JtAdManager.JT_AD, "onPrepared called");
        this.mVideoWidth = this.mediaPlayer.getVideoWidth();
        this.mVideoHeight = this.mediaPlayer.getVideoHeight();
        this.isPrepared = true;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!this.isPrepared || event.getAction() != 0) {
            return true;
        }
        if (this.mediaPlayer.isPlaying()) {
            pause();
            return true;
        }
        play();
        return true;
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e("ZL", "Error in playing video type=" + what + "  extra=" + extra);
        return false;
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
        Log.d(JtAdManager.JT_AD, "surfaceChanged called");
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        Log.d(JtAdManager.JT_AD, "surfaceCreated called");
        if (this.mediaPlayer != null && this.isPrepared) {
            this.mediaPlayer.setDisplay(holder2);
            if (this.mVideoWidth != 0 && this.mVideoHeight != 0) {
                this.mediaPlayer.start();
            }
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        Log.d(JtAdManager.JT_AD, "surfaceDestroyed called");
    }
}
