package com.jumptap.adtag.media;

public class VideoCacheItem {
    public static final String URL_DELIMITER = ",";
    private String adID;
    private String date;
    private int id;

    public VideoCacheItem() {
        init(-1, null, null);
    }

    public VideoCacheItem(String adid, String date2) {
        init(-1, adid, date2);
    }

    public VideoCacheItem(int id2, String adid, String date2) {
        init(id2, adid, date2);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id2) {
        this.id = id2;
    }

    public String getAdID() {
        return this.adID;
    }

    public void setAdID(String adID2) {
        this.adID = adID2;
    }

    public void setDate(String date2) {
        this.date = date2;
    }

    public String getDate() {
        return this.date;
    }

    public String toString() {
        return "id=" + this.id + " ,adID=" + this.adID + "   ,date=" + this.date;
    }

    private void init(int id2, String adid, String date2) {
        this.id = id2;
        this.adID = adid;
        this.date = date2;
    }
}
