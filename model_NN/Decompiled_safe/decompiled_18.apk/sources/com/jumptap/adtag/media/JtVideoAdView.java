package com.jumptap.adtag.media;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.MediaController;
import com.jumptap.adtag.utils.JtAdManager;
import java.io.IOException;

public class JtVideoAdView extends SurfaceView implements MediaController.MediaPlayerControl {
    private static JtVideoAdView videoViewInstance;
    private MediaPlayer.OnBufferingUpdateListener bufferingUpdateListener = new MediaPlayer.OnBufferingUpdateListener() {
        public void onBufferingUpdate(MediaPlayer mp, int percent) {
            int unused = JtVideoAdView.this.currentBufferPercentage = percent;
        }
    };
    private MediaPlayer.OnCompletionListener completionListener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            if (JtVideoAdView.this.mediaController != null) {
                JtVideoAdView.this.mediaController.hide();
            }
            if (JtVideoAdView.this.onCompletionListener != null) {
                JtVideoAdView.this.onCompletionListener.onCompletion(JtVideoAdView.this.mediaPlayer);
            }
            JtVideoAdView.this.setVisibility(8);
        }
    };
    private Context context;
    /* access modifiers changed from: private */
    public int currentBufferPercentage;
    /* access modifiers changed from: private */
    public boolean isPrepared;
    /* access modifiers changed from: private */
    public MediaController mediaController;
    /* access modifiers changed from: private */
    public MediaPlayer mediaPlayer = null;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener onCompletionListener;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener onPreparedListener;
    MediaPlayer.OnPreparedListener preparedListener = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mp) {
            Log.d("ZL", "OnPreparedListenerOnPreparedListener");
            boolean unused = JtVideoAdView.this.isPrepared = true;
            if (JtVideoAdView.this.onPreparedListener != null) {
                JtVideoAdView.this.onPreparedListener.onPrepared(JtVideoAdView.this.mediaPlayer);
            }
            if (JtVideoAdView.this.mediaController != null) {
                JtVideoAdView.this.mediaController.setEnabled(true);
            }
            int unused2 = JtVideoAdView.this.videoWidth = mp.getVideoWidth();
            int unused3 = JtVideoAdView.this.videoHeight = mp.getVideoHeight();
            if (JtVideoAdView.this.videoWidth != 0 && JtVideoAdView.this.videoHeight != 0) {
                JtVideoAdView.this.getHolder().setFixedSize(JtVideoAdView.this.videoWidth, JtVideoAdView.this.videoHeight);
                if (JtVideoAdView.this.seekWhenPrepared != 0) {
                    JtVideoAdView.this.mediaPlayer.seekTo(JtVideoAdView.this.seekWhenPrepared);
                }
                if (JtVideoAdView.this.mediaController != null) {
                    JtVideoAdView.this.mediaController.show();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public int seekWhenPrepared;
    SurfaceHolder.Callback shCallback = new SurfaceHolder.Callback() {
        public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
            Log.d("ZL", "####################surfaceChanged  getHolder()=" + JtVideoAdView.this.isPrepared);
            if (JtVideoAdView.this.isPrepared && JtVideoAdView.this.videoWidth == w && JtVideoAdView.this.videoHeight == h) {
                if (JtVideoAdView.this.seekWhenPrepared != 0) {
                    JtVideoAdView.this.mediaPlayer.seekTo(JtVideoAdView.this.seekWhenPrepared);
                }
                if (JtVideoAdView.this.mediaController != null) {
                    JtVideoAdView.this.mediaController.show();
                }
            }
        }

        public void surfaceCreated(SurfaceHolder holder) {
            SurfaceHolder unused = JtVideoAdView.this.surfaceHolder = holder;
            Log.d("ZL", "####################surfaceCreated");
            JtVideoAdView.this.openVideo();
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            Log.d("ZL", "####################surfaceDestroyed");
            SurfaceHolder unused = JtVideoAdView.this.surfaceHolder = null;
            if (JtVideoAdView.this.mediaController != null) {
                JtVideoAdView.this.mediaController.hide();
            }
            if (JtVideoAdView.this.mediaPlayer != null) {
                JtVideoAdView.this.mediaPlayer.reset();
                JtVideoAdView.this.mediaPlayer.release();
                MediaPlayer unused2 = JtVideoAdView.this.mediaPlayer = null;
            }
        }
    };
    /* access modifiers changed from: private */
    public SurfaceHolder surfaceHolder = null;
    private Uri uri;
    /* access modifiers changed from: private */
    public int videoHeight;
    /* access modifiers changed from: private */
    public int videoWidth;

    private JtVideoAdView(Context context2) {
        super(context2);
        this.context = context2;
        initVideoView();
    }

    public static JtVideoAdView getInstance(Context context2) {
        if (videoViewInstance == null) {
            videoViewInstance = new JtVideoAdView(context2);
        }
        return videoViewInstance;
    }

    private void initVideoView() {
        this.videoWidth = 0;
        this.videoHeight = 0;
        getHolder().addCallback(this.shCallback);
        getHolder().setType(3);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
    }

    public void setMediaController(MediaController controller) {
        if (this.mediaController != null) {
            this.mediaController.hide();
        }
        this.mediaController = controller;
        attachMediaController();
    }

    public void prepare(String path) {
        setVideoPath(path);
        requestFocus();
    }

    private void setVideoPath(String path) {
        setVideoURI(Uri.parse(path));
    }

    private void setVideoURI(Uri uri2) {
        this.uri = uri2;
        this.seekWhenPrepared = 0;
        openVideo();
        requestLayout();
        invalidate();
    }

    /* access modifiers changed from: private */
    public void openVideo() {
        if (this.uri == null || this.surfaceHolder == null) {
            Log.d("ZL", "XXXXXXXXXXXXXXXXXXXXXX");
            return;
        }
        Log.d("ZL", "YYYYYYYYYYYYYYYYY");
        Intent i = new Intent("com.android.music.musicservicecommand");
        i.putExtra("command", "pause");
        this.context.sendBroadcast(i);
        if (this.mediaPlayer != null) {
            this.mediaPlayer.reset();
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
        try {
            this.mediaPlayer = new MediaPlayer();
            this.mediaPlayer.setOnPreparedListener(this.preparedListener);
            this.isPrepared = false;
            this.mediaPlayer.setOnCompletionListener(this.completionListener);
            this.mediaPlayer.setOnBufferingUpdateListener(this.bufferingUpdateListener);
            this.currentBufferPercentage = 0;
            this.mediaPlayer.setDataSource(this.context, this.uri);
            this.mediaPlayer.setDisplay(this.surfaceHolder);
            this.mediaPlayer.setAudioStreamType(3);
            this.mediaPlayer.setScreenOnWhilePlaying(true);
            this.mediaPlayer.prepareAsync();
            attachMediaController();
        } catch (IOException ex) {
            Log.w(JtAdManager.JT_AD, "Unable to open content: " + this.uri, ex);
        } catch (IllegalArgumentException ex2) {
            Log.w(JtAdManager.JT_AD, "Unable to open content: " + this.uri, ex2);
        }
    }

    private void attachMediaController() {
        if (this.mediaPlayer != null && this.mediaController != null) {
            this.mediaController.setAnchorView(this);
            this.mediaController.setMediaPlayer(this);
            this.mediaController.setEnabled(true);
        }
    }

    public boolean onTouchEvent(MotionEvent ev) {
        if (!this.isPrepared) {
            return true;
        }
        if (this.mediaPlayer != null && this.mediaController != null) {
            toggleMediaControlsVisiblity();
            return true;
        } else if (ev.getAction() != 0) {
            return true;
        } else {
            if (this.mediaPlayer.isPlaying()) {
                pause();
                return true;
            }
            start();
            return true;
        }
    }

    public boolean onTrackballEvent(MotionEvent ev) {
        if (!this.isPrepared || this.mediaPlayer == null || this.mediaController == null) {
            return false;
        }
        toggleMediaControlsVisiblity();
        return false;
    }

    private void toggleMediaControlsVisiblity() {
        if (this.mediaController.isShowing()) {
            this.mediaController.hide();
        } else {
            this.mediaController.show();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!(!this.isPrepared || keyCode == 4 || keyCode == 24 || keyCode == 25 || keyCode == 82 || keyCode == 5 || keyCode == 6 || this.mediaPlayer == null || this.mediaController == null)) {
            if (keyCode == 79) {
                if (this.mediaPlayer.isPlaying()) {
                    pause();
                    this.mediaController.show();
                } else {
                    start();
                    this.mediaController.hide();
                }
                return true;
            }
            toggleMediaControlsVisiblity();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void start() {
        Log.d("ZL", "start  mediaPlayer=" + this.mediaPlayer.toString() + "   isPrepared" + this.isPrepared);
        if (this.mediaPlayer != null && this.isPrepared) {
            this.mediaPlayer.start();
        }
    }

    public void pause() {
        if (this.mediaPlayer != null && this.isPrepared && this.mediaPlayer.isPlaying()) {
            this.mediaPlayer.pause();
        }
    }

    public int getDuration() {
        if (this.mediaPlayer == null || !this.isPrepared) {
            return -1;
        }
        try {
            return this.mediaPlayer.getDuration();
        } catch (Exception e) {
            Log.e(JtAdManager.JT_AD, "Problem in getDuration", e);
            return -1;
        }
    }

    public int getCurrentPosition() {
        if (this.mediaPlayer == null || !this.isPrepared) {
            return 0;
        }
        return this.mediaPlayer.getCurrentPosition();
    }

    public void seekTo(int msec) {
        if (this.mediaPlayer == null || !this.isPrepared) {
            this.seekWhenPrepared = msec;
        } else {
            this.mediaPlayer.seekTo(msec);
        }
    }

    public boolean isPlaying() {
        if (this.mediaPlayer == null || !this.isPrepared) {
            return false;
        }
        return this.mediaPlayer.isPlaying();
    }

    public boolean isReady() {
        return this.mediaPlayer != null && this.isPrepared;
    }

    public int getBufferPercentage() {
        if (this.mediaPlayer != null) {
            return this.currentBufferPercentage;
        }
        return 0;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return true;
    }

    public boolean canSeekForward() {
        return true;
    }
}
