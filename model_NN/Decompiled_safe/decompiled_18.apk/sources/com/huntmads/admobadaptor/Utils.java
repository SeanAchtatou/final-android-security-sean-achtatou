package com.huntmads.admobadaptor;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class Utils {
    private static final String INSTALLATION = "INSTALLATION";
    static String sID = null;

    public static boolean checkPermission(Activity activity, String permission) {
        return activity.checkCallingOrSelfPermission(permission) == 0;
    }

    public static String scrape(String resp, String start, String stop) {
        int len;
        int offset = resp.indexOf(start);
        if (offset >= 0 && (len = resp.indexOf(stop, start.length() + offset)) >= 0) {
            return resp.substring(start.length() + offset, len);
        }
        return "";
    }

    public static String scrapeIgnoreCase(String resp, String start, String stop) {
        int len;
        String temp = resp.toLowerCase();
        String start2 = start.toLowerCase();
        String stop2 = stop.toLowerCase();
        int offset = temp.indexOf(start2);
        if (offset >= 0 && (len = temp.indexOf(stop2, start2.length() + offset)) >= 0) {
            return resp.substring(start2.length() + offset, len);
        }
        return "";
    }

    public static String md5(String data) {
        try {
            MessageDigest digester = MessageDigest.getInstance("MD5");
            digester.update(data.getBytes());
            return byteArrayToHexString(digester.digest());
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String byteArrayToHexString(byte[] array) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : array) {
            int intVal = b & 255;
            if (intVal < 16) {
                hexString.append("0");
            }
            hexString.append(Integer.toHexString(intVal));
        }
        return hexString.toString();
    }

    public static int getConnectionSpeed(Context context) {
        Integer connectionSpeed = 0;
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (networkInfo != null) {
                int type = networkInfo.getType();
                int subtype = networkInfo.getSubtype();
                if (type == 1) {
                    connectionSpeed = 1;
                } else if (type == 0) {
                    if (subtype == 2) {
                        connectionSpeed = 0;
                    } else if (subtype == 1) {
                        connectionSpeed = 0;
                    } else if (subtype == 3) {
                        connectionSpeed = 1;
                    }
                }
            }
        } catch (Exception e) {
        }
        return connectionSpeed.intValue();
    }

    public static String getMcc(Context context) {
        String networkOperator;
        TelephonyManager tm = (TelephonyManager) context.getSystemService("phone");
        if (tm == null || (networkOperator = tm.getNetworkOperator()) == null || networkOperator.length() <= 3) {
            return "NULL";
        }
        return networkOperator.substring(0, 3);
    }

    public static String getMnc(Context context) {
        TelephonyManager tm;
        String networkOperator;
        if (!checkPermission((Activity) context, "android.permission.READ_PHONE_STATE") || (tm = (TelephonyManager) context.getSystemService("phone")) == null || (networkOperator = tm.getNetworkOperator()) == null || networkOperator.length() <= 3) {
            return "NULL";
        }
        return networkOperator.substring(3);
    }

    public static String getUserID(Context context) {
        String deviceId = null;
        if (checkPermission((Activity) context, "android.permission.READ_PHONE_STATE")) {
            String temp = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (temp != null) {
                deviceId = temp;
            } else {
                String temp2 = Settings.Secure.getString(context.getContentResolver(), "android_id");
                if (temp2 != null) {
                    deviceId = temp2;
                } else {
                    deviceId = null;
                }
            }
        }
        if (deviceId == null) {
            deviceId = id(context);
        }
        String deviceIdMD5 = md5(deviceId);
        return (deviceIdMD5 == null || deviceIdMD5.length() <= 0) ? "NULL" : deviceIdMD5;
    }

    public static synchronized String id(Context context) {
        String str;
        synchronized (Utils.class) {
            if (sID == null) {
                File installation = new File(context.getFilesDir(), INSTALLATION);
                try {
                    if (!installation.exists()) {
                        writeInstallationFile(installation);
                    }
                    sID = readInstallationFile(installation);
                } catch (Exception e) {
                    sID = "1234567890";
                }
            }
            str = sID;
        }
        return str;
    }

    private static String readInstallationFile(File installation) throws IOException {
        RandomAccessFile f = new RandomAccessFile(installation, "r");
        byte[] bytes = new byte[((int) f.length())];
        f.readFully(bytes);
        f.close();
        return new String(bytes);
    }

    private static void writeInstallationFile(File installation) throws IOException {
        FileOutputStream out = new FileOutputStream(installation);
        out.write(UUID.randomUUID().toString().getBytes());
        out.close();
    }
}
