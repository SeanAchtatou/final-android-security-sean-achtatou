package com.huntmads.admobadaptor;

import android.util.Xml;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;

public class HuntMadsXmlReader {
    private URL rssUrl;
    public String str_Name;

    public void RssParserPull(String url) {
        try {
            this.rssUrl = new URL(url);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public List<HashMap<String, String>> parse() {
        List<HashMap<String, String>> noticias = null;
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(getInputStream(), null);
            int evento = parser.getEventType();
            HashMap<String, String> noticiaActual = null;
            String etiqueta = null;
            while (true) {
                List<HashMap<String, String>> list = noticias;
                if (evento == 1) {
                    return list;
                }
                switch (evento) {
                    case 0:
                        try {
                            noticias = new ArrayList<>();
                            break;
                        } catch (Exception e) {
                            ex = e;
                            throw new RuntimeException(ex);
                        }
                    case 1:
                    default:
                        noticias = list;
                        break;
                    case 2:
                        etiqueta = parser.getName();
                        if (!etiqueta.equals(this.str_Name)) {
                            if (noticiaActual != null) {
                                noticiaActual.put(etiqueta, parser.getText());
                                noticias = list;
                                break;
                            }
                            noticias = list;
                            break;
                        } else {
                            noticiaActual = new HashMap<>();
                            noticias = list;
                            break;
                        }
                    case 3:
                        etiqueta = parser.getName();
                        if (etiqueta.equals(this.str_Name) && noticiaActual != null) {
                            list.add(noticiaActual);
                        }
                        noticias = list;
                        break;
                    case 4:
                        noticiaActual.put(etiqueta, parser.getText());
                        noticias = list;
                        break;
                }
                evento = parser.next();
            }
        } catch (Exception e2) {
            ex = e2;
            throw new RuntimeException(ex);
        }
    }

    private InputStream getInputStream() {
        try {
            return this.rssUrl.openConnection().getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
