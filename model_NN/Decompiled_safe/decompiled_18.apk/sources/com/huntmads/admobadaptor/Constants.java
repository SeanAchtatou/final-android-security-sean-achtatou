package com.huntmads.admobadaptor;

public class Constants {
    public static final int DEFAULT_COLOR = 16777215;
    public static final String SDK_VERSION = "2.9.1";
}
