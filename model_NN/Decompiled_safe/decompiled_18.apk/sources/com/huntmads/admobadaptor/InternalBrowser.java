package com.huntmads.admobadaptor;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import java.io.IOException;

public class InternalBrowser extends Dialog {
    int ID_BOTTOM = 3;
    int ID_MAIN = 1;
    int ID_WEB = 2;
    Context _context;
    Button buttonBack;
    Button buttonForward;
    Button buttonOpen;
    Button buttonRefresh;
    Button buttonStopRefresh;
    Dialog thisDialog = this;
    WebView webView;

    public InternalBrowser(Context context, String url) {
        super(context);
        this._context = context;
        requestWindowFeature(1);
        LinearLayout mailLayout = new LinearLayout(context);
        mailLayout.setId(this.ID_MAIN);
        mailLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mailLayout.setOrientation(1);
        this.webView = new WebView(context);
        this.webView.setId(this.ID_WEB);
        this.webView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.webView.loadUrl(url);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        LinearLayout bottomLayout = new LinearLayout(context);
        mailLayout.setOrientation(1);
        bottomLayout.setId(this.ID_BOTTOM);
        bottomLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 0.0f));
        bottomLayout.setOrientation(0);
        bottomLayout.setBackgroundDrawable(GetDrawable(this._context, "ib_bg_down.png"));
        this.buttonBack = AddButton(bottomLayout, "ib_arrow_left_regular.png", "ib_arrow_left_press.png", "ib_arrow_left_disabled.png");
        this.buttonForward = AddButton(bottomLayout, "ib_arrow_right_regular.png", "ib_arrow_right_press.png", "ib_arrow_right_disabled.png");
        this.buttonRefresh = AddButton(bottomLayout, "ib_apdate_regular.png", "ib_apdate_press.png", null, true);
        this.buttonOpen = AddButton(bottomLayout, "ib_window_regular.png", "ib_window_press.png", null);
        mailLayout.addView(this.webView);
        mailLayout.addView(bottomLayout);
        setContentView(mailLayout);
        getWindow().setLayout(-1, -1);
        getWindow().setBackgroundDrawable(null);
        this.buttonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InternalBrowser.this.webView.goBack();
                InternalBrowser.this.UpdateButtons();
            }
        });
        this.buttonForward.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InternalBrowser.this.webView.goForward();
                InternalBrowser.this.UpdateButtons();
            }
        });
        this.buttonRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InternalBrowser.this.webView.reload();
            }
        });
        this.buttonOpen.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    InternalBrowser.this._context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(InternalBrowser.this.webView.getUrl())));
                } catch (Exception e) {
                }
                InternalBrowser.this.thisDialog.dismiss();
            }
        });
        this.buttonStopRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InternalBrowser.this.webView.stopLoading();
            }
        });
        this.webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                InternalBrowser.this.buttonRefresh.setVisibility(0);
                InternalBrowser.this.buttonStopRefresh.setVisibility(8);
                InternalBrowser.this.UpdateButtons();
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                InternalBrowser.this.buttonRefresh.setVisibility(8);
                InternalBrowser.this.buttonStopRefresh.setVisibility(0);
            }
        });
        UpdateButtons();
    }

    /* access modifiers changed from: package-private */
    public void UpdateButtons() {
        this.buttonBack.setEnabled(this.webView.canGoBack());
        this.buttonForward.setEnabled(this.webView.canGoForward());
    }

    /* access modifiers changed from: package-private */
    public Button AddButton(LinearLayout bottomLayout, String normal, String pressed, String disable) {
        return AddButton(bottomLayout, normal, pressed, disable, false);
    }

    /* access modifiers changed from: package-private */
    public Button AddButton(LinearLayout bottomLayout, String normal, String pressed, String disable, boolean isStop) {
        Button button = new Button(this._context);
        button.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        button.setBackgroundDrawable(GetSelector(this._context, normal, pressed, disable));
        LinearLayout ll = new LinearLayout(this._context);
        ll.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        ll.setGravity(17);
        ll.addView(button);
        if (isStop) {
            this.buttonStopRefresh = new Button(this._context);
            this.buttonStopRefresh.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            this.buttonStopRefresh.setBackgroundDrawable(GetSelector(this._context, "ib_close_regular.png", "ib_close_press.png", null));
            ll.addView(this.buttonStopRefresh);
            this.buttonStopRefresh.setVisibility(8);
        }
        bottomLayout.addView(ll);
        return button;
    }

    public static StateListDrawable GetSelector(Context context, String normal, String pressed, String disable) {
        StateListDrawable result = new StateListDrawable();
        result.addState(new int[]{-16842919, 16842910}, GetDrawable(context, normal));
        if (pressed != null) {
            result.addState(new int[]{16842919, 16842910}, GetDrawable(context, pressed));
        }
        if (disable != null) {
            result.addState(new int[]{-16842910}, GetDrawable(context, disable));
        } else {
            result.addState(new int[]{-16842910}, GetDrawable(context, normal));
        }
        return result;
    }

    public static Drawable GetDrawable(Context context, String fileName) {
        try {
            return Drawable.createFromStream(context.getAssets().open(fileName), null);
        } catch (IOException e) {
            return null;
        }
    }
}
