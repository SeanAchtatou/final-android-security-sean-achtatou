package com.huntmads.admobadaptor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.adfonic.android.utils.HtmlFormatter;
import com.adsdk.sdk.Const;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.mediation.MediationAdRequest;
import com.google.ads.mediation.MediationBannerAdapter;
import com.google.ads.mediation.MediationBannerListener;
import com.google.ads.mediation.MediationInterstitialAdapter;
import com.google.ads.mediation.MediationInterstitialListener;
import com.millennialmedia.android.MMAdViewSDK;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

public final class HuntMadsAdapter implements MediationBannerAdapter<HuntMadsExtras, HuntMadsServerParameters>, MediationInterstitialAdapter<HuntMadsExtras, HuntMadsServerParameters> {
    private String MCC;
    private String MNC;
    private AdSize _AdSize;
    private HuntMadsExtras _Extras;
    private MediationAdRequest _MediationAdRequest;
    private HuntMadsServerParameters _ServerParameters;
    private Location aLocation;
    /* access modifiers changed from: private */
    public MediationBannerListener bannerListener;
    private int conectionSpeed = 0;
    private float factorDPI = 1.0f;
    private Runnable getAdsRunnable;
    private int hasLocation;
    /* access modifiers changed from: private */
    public MediationInterstitialListener interstitialListener;
    private boolean isIntersitial = false;
    private boolean isTestDevice = false;
    private ImageView iv;
    /* access modifiers changed from: private */
    public Activity theAct;
    /* access modifiers changed from: private */
    public String theClick2;
    /* access modifiers changed from: private */
    public Dialog theIntersitial;
    private String thesource;
    private Thread thread;
    private String ua;
    private String udid;
    private HashMap<String, Bitmap> urlToBitmap = new HashMap<>();
    private WebView webView;

    private InputStream OpenHttpConnection(String urlString) throws IOException {
        URLConnection conn = new URL(urlString).openConnection();
        if (!(conn instanceof HttpURLConnection)) {
            throw new IOException("Not an HTTP connection");
        }
        try {
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            if (httpConn.getResponseCode() == 200) {
                return httpConn.getInputStream();
            }
            return null;
        } catch (Exception e) {
            throw new IOException("Error connecting");
        }
    }

    private Bitmap DownloadImage(String URL) {
        try {
            InputStream in = OpenHttpConnection(URL);
            if (in == null) {
                return null;
            }
            Bitmap bitmap = BitmapFactory.decodeStream(in);
            in.close();
            return bitmap;
        } catch (IOException e1) {
            e1.printStackTrace();
            return null;
        }
    }

    private String getUrlToCall() {
        String atype;
        String aformat;
        String IdZone = this._ServerParameters.IdZone;
        String IdSite = this._ServerParameters.IdSite;
        int y = this._AdSize.getHeight();
        int x = this._AdSize.getWidth();
        String user = this.ua;
        if (this.isIntersitial) {
            atype = "4";
            aformat = "FULLSCREEN";
        } else {
            atype = "2";
            if (x == 320 && y == 50) {
                aformat = "auto";
            } else {
                aformat = String.valueOf(x) + "x" + y;
            }
        }
        String request = "http://ads.huntmad.com/ad?mediationpartner=googleadmob&site=" + IdSite + "&zone=" + IdZone + "&count=1&key=3&size_x=" + x + "&size_y=" + y + "&min_size_x=" + x + "&min_size_y=" + y + "&type=" + atype + "&ua=" + user + "&udid=" + this.udid + "&connection_speed=" + this.conectionSpeed + "&version=2.9.0&mcc=" + this.MCC + "&mnc=" + this.MNC + "&format=" + aformat;
        if (this.hasLocation == 1) {
            request = String.valueOf(request) + "&lat=" + this.aLocation.getLatitude() + "long=" + this.aLocation.getLongitude();
        }
        if (this.isTestDevice) {
            return String.valueOf(request) + "&test=1";
        }
        return request;
    }

    /* access modifiers changed from: private */
    public void getAds() {
        try {
            this.isIntersitial = false;
            String strUrl = getUrlToCall();
            HuntMadsXmlReader axmlreader = new HuntMadsXmlReader();
            axmlreader.str_Name = "mojiva";
            axmlreader.RssParserPull(strUrl);
            List<HashMap<String, String>> salida = axmlreader.parse();
            if (salida.size() > 0) {
                int hayError = 0;
                HashMap<String, String> o = salida.get(0);
                if (o.get("error") != null) {
                    hayError = 1;
                }
                if (hayError == 1) {
                    this.bannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.NO_FILL);
                    return;
                }
                String thestrIMAGE = (String) o.get("img");
                this.theClick2 = (String) o.get("url");
                if (this.urlToBitmap.get(thestrIMAGE) == null) {
                    Bitmap abit = DownloadImage(thestrIMAGE);
                    if (abit == null) {
                        this.bannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
                        return;
                    }
                    this.iv.setImageBitmap(abit);
                    this.urlToBitmap.put(thestrIMAGE, abit);
                    String track = (String) o.get("track");
                    if (!(track == null || track == "")) {
                        this.urlToBitmap.get(track);
                    }
                    this.bannerListener.onReceivedAd(this);
                }
            }
        } catch (Exception e) {
            try {
                this.bannerListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.NETWORK_ERROR);
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void getAdsIntersitial() {
        try {
            this.isIntersitial = true;
            String strUrl = getUrlToCall();
            HuntMadsXmlReader axmlreader = new HuntMadsXmlReader();
            axmlreader.str_Name = "mojiva";
            axmlreader.RssParserPull(strUrl);
            List<HashMap<String, String>> salida = axmlreader.parse();
            if (salida.size() > 0) {
                int hayError = 0;
                HashMap<String, String> o = salida.get(0);
                if (o.get("error") != null) {
                    hayError = 1;
                }
                if (hayError == 1) {
                    this.interstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.NO_FILL);
                    return;
                }
                this.thesource = (String) o.get("content");
                this.theClick2 = (String) o.get("url");
                if (this.thesource.equals("")) {
                    this.interstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.INTERNAL_ERROR);
                    return;
                }
                String track = (String) o.get("track");
                if (!(track == null || track == "")) {
                    this.urlToBitmap.get(track);
                }
                this.interstitialListener.onReceivedAd(this);
            }
        } catch (Exception e) {
            try {
                this.interstitialListener.onFailedToReceiveAd(this, AdRequest.ErrorCode.NETWORK_ERROR);
            } catch (Exception e2) {
            }
        }
    }

    public Class<HuntMadsExtras> getAdditionalParametersType() {
        return HuntMadsExtras.class;
    }

    public Class<HuntMadsServerParameters> getServerParametersType() {
        return HuntMadsServerParameters.class;
    }

    /* access modifiers changed from: private */
    public void _openUrlInExternalBrowser(Context context, String url) {
        String lastUrl = null;
        String newUrl = url;
        while (!newUrl.equals(lastUrl)) {
            lastUrl = newUrl;
            try {
                HttpURLConnection conn = (HttpURLConnection) new URL(newUrl).openConnection();
                newUrl = conn.getHeaderField("Location");
                if (newUrl == null) {
                    newUrl = conn.getURL().toString();
                }
            } catch (Exception e) {
                newUrl = lastUrl;
            }
        }
        Uri uri = Uri.parse(newUrl);
        if (uri.getScheme().equals(MMAdViewSDK.Event.INTENT_PHONE_CALL) || uri.getScheme().equals(MMAdViewSDK.Event.INTENT_PHONE_CALL)) {
            try {
                this.bannerListener.onLeaveApplication(this);
                context.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(newUrl)));
            } catch (Exception e2) {
            }
        } else {
            try {
                this.bannerListener.onLeaveApplication(this);
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(newUrl)));
            } catch (Exception e3) {
            }
        }
    }

    public void requestBannerAd(MediationBannerListener listener, Activity activity, HuntMadsServerParameters serverParameters, AdSize adSize, MediationAdRequest mediationAdRequest, HuntMadsExtras extras) {
        this.theAct = activity;
        DisplayMetrics metrics = new DisplayMetrics();
        this.theAct.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        this.factorDPI = ((float) metrics.densityDpi) / 160.0f;
        if (this.factorDPI < 1.0f) {
            this.factorDPI = 1.0f;
        }
        switch (metrics.densityDpi) {
        }
        this.conectionSpeed = Utils.getConnectionSpeed(activity);
        this.MNC = Utils.getMnc(activity);
        this.MCC = Utils.getMcc(activity);
        this.udid = Utils.getUserID(activity);
        this.isTestDevice = mediationAdRequest.isTesting();
        this.aLocation = mediationAdRequest.getLocation();
        if (this.aLocation != null) {
            this.hasLocation = 1;
        }
        this.ua = new WebView(this.theAct).getSettings().getUserAgentString();
        try {
            this.ua = URLEncoder.encode(this.ua, Const.ENCODING);
        } catch (Exception e) {
        }
        this._MediationAdRequest = mediationAdRequest;
        this._AdSize = adSize;
        this._ServerParameters = serverParameters;
        this._Extras = extras;
        this.bannerListener = listener;
        int width = this._AdSize.getWidth();
        int height = this._AdSize.getHeight();
        int width2 = (int) (((float) width) * this.factorDPI);
        int height2 = (int) (((float) height) * this.factorDPI);
        this.iv = new ImageView(activity);
        this.iv.setScaleType(ImageView.ScaleType.FIT_XY);
        this.iv.setLayoutParams(new AbsListView.LayoutParams(width2, height2));
        this.iv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HuntMadsAdapter.this.bannerListener.onClick(HuntMadsAdapter.this);
                HuntMadsAdapter.this._openUrlInExternalBrowser(HuntMadsAdapter.this.theAct, HuntMadsAdapter.this.theClick2);
            }
        });
        this.getAdsRunnable = new Runnable() {
            public void run() {
                HuntMadsAdapter.this.getAds();
            }
        };
        this.thread = new Thread(null, this.getAdsRunnable, "getAdsRunnable");
        this.thread.start();
    }

    public void requestInterstitialAd(MediationInterstitialListener listener, Activity activity, HuntMadsServerParameters serverParameters, MediationAdRequest mediationAdRequest, HuntMadsExtras extras) {
        this.interstitialListener = listener;
        this.theAct = activity;
        this.conectionSpeed = Utils.getConnectionSpeed(activity);
        this.MNC = Utils.getMnc(activity);
        this.MCC = Utils.getMcc(activity);
        this.udid = Utils.getUserID(activity);
        this.isTestDevice = mediationAdRequest.isTesting();
        this.aLocation = mediationAdRequest.getLocation();
        if (this.aLocation != null) {
            this.hasLocation = 1;
        }
        this.ua = new WebView(this.theAct).getSettings().getUserAgentString();
        try {
            this.ua = URLEncoder.encode(this.ua, Const.ENCODING);
        } catch (Exception e) {
        }
        this._MediationAdRequest = mediationAdRequest;
        Display display = activity.getWindowManager().getDefaultDisplay();
        this._AdSize = new AdSize(display.getWidth(), display.getHeight());
        this._ServerParameters = serverParameters;
        this._Extras = extras;
        this.getAdsRunnable = new Runnable() {
            public void run() {
                HuntMadsAdapter.this.getAdsIntersitial();
            }
        };
        this.thread = new Thread(null, this.getAdsRunnable, "getAdsRunnable");
        this.thread.start();
        this.theIntersitial = new Dialog(activity, 16973830);
        this.theAct = activity;
        RelativeLayout mainLayout = new RelativeLayout(this.theAct);
        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.webView = new WebView(activity);
        this.webView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        mainLayout.addView(this.webView);
        this.webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });
        Button closeButton = new Button(this.theAct);
        closeButton.setText("Close");
        RelativeLayout.LayoutParams closeLayoutParams = new RelativeLayout.LayoutParams(-2, -2);
        closeLayoutParams.addRule(12, -1);
        closeLayoutParams.addRule(14, -1);
        closeButton.setLayoutParams(closeLayoutParams);
        closeButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HuntMadsAdapter.this.interstitialListener.onDismissScreen(HuntMadsAdapter.this);
                HuntMadsAdapter.this.theIntersitial.dismiss();
            }
        });
        mainLayout.addView(closeButton);
        this.theIntersitial.setContentView(mainLayout);
    }

    public void showInterstitial() {
        this.interstitialListener.onPresentScreen(this);
        this.thesource = "<html><head> <meta name=\"viewport\" content=\"width=device-width,minimum-scale=1.0, maximum-scale=10.0\" /></head><body style=\"text-align:center;margin:0;padding:0\">" + this.thesource + "</body></html>";
        this.webView.loadData(this.thesource, HtmlFormatter.TEXT_HTML, null);
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setLoadWithOverviewMode(false);
        this.theIntersitial.show();
    }

    public void destroy() {
        this.thread = null;
        this.bannerListener = null;
        this.interstitialListener = null;
    }

    public View getBannerView() {
        return this.iv;
    }
}
