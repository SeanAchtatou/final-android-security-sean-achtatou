package com.huntmads.admobadaptor;

import com.google.ads.mediation.NetworkExtras;

public final class HuntMadsExtras implements NetworkExtras {
    private BoneType boneType = BoneType.UNKNOWN;

    public enum BoneType {
        TIBIA,
        FEMUR,
        PATELLA,
        OTHER,
        UNKNOWN
    }

    public HuntMadsExtras setBoneType(BoneType boneType2) {
        this.boneType = boneType2;
        return this;
    }

    public HuntMadsExtras clearBoneType() {
        return setBoneType(null);
    }

    public BoneType getAdLocation() {
        return this.boneType;
    }
}
