package com.huntmads.admobadaptor;

import com.google.ads.mediation.MediationServerParameters;

public class HuntMadsServerParameters extends MediationServerParameters {
    @MediationServerParameters.Parameter(name = "siteID")
    public String IdSite;
    @MediationServerParameters.Parameter(name = "zoneID")
    public String IdZone;
}
