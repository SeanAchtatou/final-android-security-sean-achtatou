package com.scoreloop.client.android.ui.component.base;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.scoreloop.client.android.ui.framework.BaseListAdapter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1671.R;

public abstract class ComponentListActivity<T extends BaseListItem> extends ComponentActivity implements View.OnClickListener, AdapterView.OnItemClickListener, BaseListAdapter.OnListItemClickListener<T> {
    private BaseListItem _footerItem;
    protected boolean _isVisibleOptionsMenuAccountSettings = true;

    public void setVisibleOptionsMenuAccountSettings(boolean isVisibleOptionsMenuProfileSettings) {
        this._isVisibleOptionsMenuAccountSettings = isVisibleOptionsMenuProfileSettings;
    }

    public BaseListAdapter<T> getBaseListAdapter() {
        return (BaseListAdapter) getListAdapter();
    }

    public ListAdapter getListAdapter() {
        return getListView().getAdapter();
    }

    public ListView getListView() {
        return (ListView) findViewById(R.id.sl_list);
    }

    public void hideFooter() {
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.sl_footer);
        if (viewGroup != null) {
            this._footerItem = null;
            viewGroup.removeAllViews();
        }
    }

    public void onClick(View view) {
        if (this._footerItem != null) {
            onFooterItemClick(this._footerItem);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sl_list_view, true);
        getListView().setFocusable(true);
        getListView().setOnItemClickListener(this);
    }

    /* access modifiers changed from: protected */
    public void onFooterItemClick(BaseListItem footerItem) {
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getBaseListAdapter().onItemClick(parent, view, position, id);
    }

    public void onListItemClick(BaseListItem baseListItem) {
    }

    public void setListAdapter(ListAdapter adapter) {
        getListView().setAdapter(adapter);
        getBaseListAdapter().setOnListItemClickListener(this);
    }

    public void showFooter(BaseListItem footerItem) {
        View footerView;
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.sl_footer);
        if (viewGroup != null && (footerView = footerItem.getView(null, null)) != null) {
            this._footerItem = footerItem;
            viewGroup.addView(footerView);
            if (footerItem.isEnabled()) {
                footerView.setOnClickListener(this);
            }
        }
    }

    public boolean onCreateOptionsMenuForActivityGroup(Menu menu) {
        getMenuInflater().inflate(R.menu.sl_options_menu, menu);
        return super.onCreateOptionsMenuForActivityGroup(menu);
    }

    public boolean onPrepareOptionsMenuForActivityGroup(Menu menu) {
        MenuItem item = menu.findItem(R.id.sl_item_account_settings);
        if (item != null) {
            item.setVisible(this._isVisibleOptionsMenuAccountSettings);
        }
        return super.onPrepareOptionsMenuForActivityGroup(menu);
    }

    public boolean onOptionsItemSelectedForActivityGroup(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sl_item_account_settings /*2131427452*/:
                getTracker().trackEvent(TrackerEvents.CAT_NAVI, TrackerEvents.NAVI_OM_ACCOUNT_SETTINGS, null, 0);
                display(getFactory().createProfileSettingsScreenDescription(getSessionUser()));
                return true;
            default:
                return super.onOptionsItemSelectedForActivityGroup(item);
        }
    }
}
