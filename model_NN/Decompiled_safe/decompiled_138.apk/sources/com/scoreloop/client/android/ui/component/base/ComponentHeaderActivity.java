package com.scoreloop.client.android.ui.component.base;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.skyd.bestpuzzle.n1671.R;

public abstract class ComponentHeaderActivity extends ComponentActivity implements View.OnClickListener {
    private TextView _caption;

    /* access modifiers changed from: protected */
    public ImageView getImageView() {
        return (ImageView) findViewById(R.id.sl_header_image);
    }

    public void onClick(View view) {
    }

    public void onCreate(Bundle savedInstanceState, int layout_id) {
        super.onCreate(savedInstanceState);
        setContentView(layout_id);
    }

    /* access modifiers changed from: protected */
    public void onSpinnerShow(boolean show) {
    }

    /* access modifiers changed from: protected */
    public void setCaption(String captionText) {
        if (this._caption == null) {
            Display display = getWindowManager().getDefaultDisplay();
            int orientation = display.getOrientation();
            DisplayMetrics metrics = new DisplayMetrics();
            display.getMetrics(metrics);
            if (metrics.widthPixels > metrics.heightPixels || orientation == 1 || orientation == 3) {
                this._caption = (TextView) findViewById(R.id.sl_header_caption_land);
            } else {
                this._caption = (TextView) findViewById(R.id.sl_header_caption);
            }
        }
        if (this._caption != null) {
            this._caption.setText(captionText);
        }
    }

    /* access modifiers changed from: protected */
    public void setSubTitle(String subTitle) {
        TextView textView = (TextView) findViewById(R.id.sl_header_subtitle);
        if (textView != null) {
            textView.setText(subTitle);
        }
    }

    /* access modifiers changed from: protected */
    public void setTitle(String title) {
        ((TextView) findViewById(R.id.sl_header_title)).setText(title);
    }
}
