package com.scoreloop.client.android.core.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringsParser {
    public Map<String, String> a(InputStream inputStream) {
        try {
            HashMap hashMap = new HashMap();
            Pattern compile = Pattern.compile("^\\s*\"(.*)\"\\s*=\\s*\"(.*)\"\\s*;\\s*$");
            Matcher matcher = compile.matcher("");
            matcher.usePattern(compile);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            for (String readLine = bufferedReader.readLine(); readLine != null; readLine = bufferedReader.readLine()) {
                matcher.reset(readLine);
                if (matcher.matches()) {
                    hashMap.put(matcher.group(1), matcher.group(2));
                }
            }
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
