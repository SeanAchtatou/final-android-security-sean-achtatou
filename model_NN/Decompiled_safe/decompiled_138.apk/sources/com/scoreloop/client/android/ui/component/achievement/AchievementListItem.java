package com.scoreloop.client.android.ui.component.achievement;

import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.scoreloop.client.android.core.model.Achievement;
import com.scoreloop.client.android.core.model.Award;
import com.scoreloop.client.android.core.model.Money;
import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.Configuration;
import com.scoreloop.client.android.ui.component.base.StringFormatter;
import com.scoreloop.client.android.ui.framework.BaseListItem;
import com.skyd.bestpuzzle.n1671.R;

public class AchievementListItem extends BaseListItem {
    private final Achievement _achievement;
    private final boolean _belongsToSessionUser;
    private final String _description;

    private static String getDescriptionText(Achievement achievement, Configuration configuration) {
        Award award = achievement.getAward();
        String text = award.getLocalizedDescription();
        Money reward = award.getRewardMoney();
        if (reward == null || !reward.hasAmount()) {
            return text;
        }
        StringBuilder builder = new StringBuilder();
        builder.append(StringFormatter.formatMoney(reward, configuration)).append("\n").append(text);
        return builder.toString();
    }

    private static String getTitleText(Achievement achievement) {
        return achievement.getAward().getLocalizedTitle();
    }

    public AchievementListItem(ComponentActivity activity, Achievement achievement, boolean belongsToSessionUser) {
        super(activity, new BitmapDrawable(achievement.getImage()), getTitleText(achievement));
        this._description = getDescriptionText(achievement, activity.getConfiguration());
        this._achievement = achievement;
        this._belongsToSessionUser = belongsToSessionUser;
    }

    public Achievement getAchievement() {
        return this._achievement;
    }

    public int getType() {
        return 1;
    }

    public View getView(View view, ViewGroup parent) {
        if (view == null) {
            view = getLayoutInflater().inflate((int) R.layout.sl_list_item_achievement, (ViewGroup) null);
        }
        ((ImageView) view.findViewById(R.id.sl_list_item_achievement_icon)).setImageDrawable(getDrawable());
        ((TextView) view.findViewById(R.id.sl_list_item_achievement_title)).setText(getTitle());
        ((TextView) view.findViewById(R.id.sl_list_item_achievement_description)).setText(this._description);
        view.findViewById(R.id.sl_list_item_achievement_accessory).setVisibility(isEnabled() ? 0 : 4);
        return view;
    }

    public boolean isEnabled() {
        return this._belongsToSessionUser && this._achievement.isAchieved() && this._achievement.getIdentifier() != null;
    }
}
