package com.scoreloop.client.android.core.spi.myspace;

import com.scoreloop.client.android.core.PublishedFor__1_0_0;
import com.scoreloop.client.android.core.controller.SocialProviderController;
import com.scoreloop.client.android.core.controller.SocialProviderControllerObserver;
import com.scoreloop.client.android.core.model.Session;
import com.scoreloop.client.android.core.spi.AuthViewController;

public class MySpaceSocialProviderController extends SocialProviderController implements AuthViewController.Observer {
    private a b = null;

    @PublishedFor__1_0_0
    public MySpaceSocialProviderController(Session session, SocialProviderControllerObserver socialProviderControllerObserver) {
        super(session, socialProviderControllerObserver);
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.b = new a(d(), this);
        this.b.a(c_());
    }

    public void a(Throwable th) {
        d_().socialProviderControllerDidFail(this, th);
    }

    public void b() {
        d_().socialProviderControllerDidEnterInvalidCredentials(this);
    }

    public void b_() {
        d_().socialProviderControllerDidCancel(this);
    }

    public void c() {
        getSocialProvider().a(d().getUser(), this.b.b(), this.b.c(), this.b.d());
        a(SocialProviderController.UpdateMode.SUBMIT);
    }
}
