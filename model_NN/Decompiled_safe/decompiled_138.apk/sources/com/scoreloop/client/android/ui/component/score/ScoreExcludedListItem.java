package com.scoreloop.client.android.ui.component.score;

import com.scoreloop.client.android.ui.component.base.ComponentActivity;
import com.scoreloop.client.android.ui.component.base.StandardListItem;
import com.skyd.bestpuzzle.n1671.R;

public class ScoreExcludedListItem extends StandardListItem<Void> {
    public ScoreExcludedListItem(ComponentActivity context) {
        super(context, null, context.getSessionUser().getDisplayName(), context.getResources().getString(R.string.sl_not_on_highscore_list), null);
    }

    /* access modifiers changed from: protected */
    public String getImageUrl() {
        return getComponentActivity().getSessionUser().getImageUrl();
    }

    /* access modifiers changed from: protected */
    public int getLayoutId() {
        return R.layout.sl_list_item_score_excluded;
    }

    public int getType() {
        return 20;
    }

    public boolean isEnabled() {
        return false;
    }
}
