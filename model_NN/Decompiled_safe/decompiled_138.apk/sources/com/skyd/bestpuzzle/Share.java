package com.skyd.bestpuzzle;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.skyd.bestpuzzle.n1671.R;
import com.skyd.core.android.Android;

public class Share extends Activity {
    private Button _Button01 = null;
    private Button _Button02 = null;
    private EditText _EditText01 = null;

    public Button getButton01() {
        if (this._Button01 == null) {
            this._Button01 = (Button) findViewById(R.id.Button01);
        }
        return this._Button01;
    }

    public Button getButton02() {
        if (this._Button02 == null) {
            this._Button02 = (Button) findViewById(R.id.Button02);
        }
        return this._Button02;
    }

    public EditText getEditText01() {
        if (this._EditText01 == null) {
            this._EditText01 = (EditText) findViewById(R.id.EditText01);
        }
        return this._EditText01;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.share);
        getButton02().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Share.this.finish();
            }
        });
        getButton01().setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Android.shareText(Share.this, Share.this.getResources().getString(R.string.shareTitle), Share.this.getEditText01().getText().toString());
            }
        });
        getEditText01().setText(String.format(getEditText01().getText().toString(), getText(R.string.app_name), getText(R.string.freeVersionAppUrl), getText(R.string.demoVideoUrl)));
    }
}
