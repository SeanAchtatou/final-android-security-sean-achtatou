package com.skyd.bestpuzzle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.widget.Toast;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.skyd.bestpuzzle.n1671.R;
import com.skyd.core.android.game.GameImageSpirit;
import com.skyd.core.android.game.GameMaster;
import com.skyd.core.android.game.GameObject;
import com.skyd.core.android.game.GameScene;
import com.skyd.core.android.game.GameSpirit;
import com.skyd.core.draw.DrawHelper;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PuzzleScene extends GameScene {
    public Controller Controller;
    public Desktop Desktop;
    boolean IsDrawingPuzzle;
    boolean IsDrawnSelectRect;
    public GameImageSpirit MoveButton;
    public UI MoveButtonSlot;
    public OriginalImage OriginalImage;
    public long PastTime;
    public long StartTime;
    public UI SubmitScoreButton;
    /* access modifiers changed from: private */
    public RectF SubmitScoreButtonRect;
    public GameSpirit Time;
    public UI ViewFullButton;
    /* access modifiers changed from: private */
    public RectF ViewFullButtonRect;
    public UI ZoomInButton;
    public UI ZoomOutButton;
    private boolean _IsFinished = false;
    private ArrayList<OnIsFinishedChangedListener> _IsFinishedChangedListenerList = null;
    long savetime = Long.MIN_VALUE;
    RectF selectRect;

    public interface OnIsFinishedChangedListener {
        void OnIsFinishedChangedEvent(Object obj, boolean z);
    }

    public PuzzleScene() {
        setMaxDrawCacheLayer(25.0f);
        this.StartTime = new Date().getTime();
        loadGameState();
    }

    public void loadGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        this.PastTime = c.getPastTime().longValue();
        this._IsFinished = c.getIsFinished().booleanValue();
    }

    public void saveGameState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        if (getIsFinished()) {
            c.setPastTime(Long.valueOf(this.PastTime));
        } else {
            c.setPastTime(Long.valueOf(this.PastTime + (new Date().getTime() - this.StartTime)));
        }
        c.setIsFinished(Boolean.valueOf(getIsFinished()));
    }

    public void setFinished() {
        if (!getIsFinished()) {
            this.SubmitScoreButton.show();
        }
        setIsFinished(true);
        this.PastTime += new Date().getTime() - this.StartTime;
    }

    public boolean getIsFinished() {
        return this._IsFinished;
    }

    private void setIsFinished(boolean value) {
        onIsFinishedChanged(value);
        this._IsFinished = value;
    }

    private void setIsFinishedToDefault() {
        setIsFinished(false);
    }

    public boolean addOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null) {
            this._IsFinishedChangedListenerList = new ArrayList<>();
        } else if (this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.add(listener);
        return true;
    }

    public boolean removeOnIsFinishedChangedListener(OnIsFinishedChangedListener listener) {
        if (this._IsFinishedChangedListenerList == null || !this._IsFinishedChangedListenerList.contains(listener)) {
            return false;
        }
        this._IsFinishedChangedListenerList.remove(listener);
        return true;
    }

    public void clearOnIsFinishedChangedListeners() {
        if (this._IsFinishedChangedListenerList != null) {
            this._IsFinishedChangedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onIsFinishedChanged(boolean newValue) {
        if (this._IsFinishedChangedListenerList != null) {
            Iterator<OnIsFinishedChangedListener> it = this._IsFinishedChangedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnIsFinishedChangedEvent(this, newValue);
            }
        }
    }

    public int getTargetCacheID() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
        this.IsDrawingPuzzle = false;
        this.IsDrawnSelectRect = false;
        super.drawChilds(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        float lvl = ((GameSpirit) child).getLevel();
        if (!this.IsDrawingPuzzle && lvl <= 10.0f) {
            this.IsDrawingPuzzle = true;
            c.setMatrix(this.Desktop.getMatrix());
        } else if (lvl > 10.0f && this.IsDrawingPuzzle) {
            this.IsDrawingPuzzle = false;
            c.setMatrix(new Matrix());
        }
        if (lvl > 25.0f && this.selectRect != null && !this.IsDrawnSelectRect) {
            this.IsDrawnSelectRect = true;
            Paint p = new Paint();
            p.setARGB(100, 180, 225, 255);
            c.drawRect(this.selectRect, p);
        }
        return super.onDrawingChild(child, c, drawArea);
    }

    public void load(Context c) {
        loadOriginalImage(c);
        loadDesktop(c);
        loadInterface(c);
        loadPuzzle(c);
        loadPuzzleState();
        this.Desktop.updateCurrentObserveAreaRectF();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF
     arg types: [int, int, float, float, int]
     candidates:
      com.skyd.core.draw.DrawHelper.calculateScaleSize(double, double, double, double, boolean):com.skyd.core.vector.Vector2D
      com.skyd.core.draw.DrawHelper.calculateScaleSize(float, float, float, float, boolean):com.skyd.core.vector.Vector2DF */
    public void loadOriginalImage(Context c) {
        this.OriginalImage = new OriginalImage() {
            /* access modifiers changed from: protected */
            public boolean onDrawing(Canvas c, Rect drawArea) {
                c.drawARGB(180, 0, 0, 0);
                return super.onDrawing(c, drawArea);
            }
        };
        this.OriginalImage.setLevel(50.0f);
        this.OriginalImage.setName("OriginalImage");
        this.OriginalImage.getSize().resetWith(DrawHelper.calculateScaleSize(320.0f, 240.0f, (float) (GameMaster.getScreenWidth() - 40), (float) (GameMaster.getScreenHeight() - 40), true));
        this.OriginalImage.setTotalSeparateColumn(8);
        this.OriginalImage.setTotalSeparateRow(6);
        this.OriginalImage.getOriginalSize().reset(800.0f, 600.0f);
        this.OriginalImage.hide();
        this.OriginalImage.setIsUseAbsolutePosition(true);
        this.OriginalImage.setIsUseAbsoluteSize(true);
        this.OriginalImage.getImage().setIsUseAbsolutePosition(true);
        this.OriginalImage.getImage().setIsUseAbsoluteSize(true);
        this.OriginalImage.getPosition().reset(((float) (GameMaster.getScreenWidth() / 2)) - (this.OriginalImage.getSize().getX() / 2.0f), ((float) (GameMaster.getScreenHeight() / 2)) - (this.OriginalImage.getSize().getY() / 2.0f));
        getSpiritList().add(this.OriginalImage);
    }

    public void loadDesktop(Context c) {
        this.Desktop = new Desktop();
        this.Desktop.setLevel(-9999.0f);
        this.Desktop.setName("Desktop");
        this.Desktop.getSize().reset(1600.0f, 1200.0f);
        this.Desktop.getCurrentObservePosition().reset(800.0f, 600.0f);
        this.Desktop.setOriginalImage(this.OriginalImage);
        this.Desktop.setIsUseAbsolutePosition(true);
        this.Desktop.setIsUseAbsoluteSize(true);
        this.Desktop.ColorAPaint = new Paint();
        this.Desktop.ColorAPaint.setColor(Color.argb(255, 32, 30, 43));
        this.Desktop.ColorBPaint = new Paint();
        this.Desktop.ColorBPaint.setColor(Color.argb(255, 26, 24, 37));
        getSpiritList().add(this.Desktop);
    }

    public void loadPuzzle(Context c) {
        this.Controller = new Controller();
        this.Controller.setLevel(10.0f);
        this.Controller.setName("Controller");
        this.Controller.setDesktop(this.Desktop);
        this.Controller.setIsUseAbsolutePosition(true);
        this.Controller.setIsUseAbsoluteSize(true);
        getSpiritList().add(this.Controller);
        Puzzle.getRealitySize().reset(100.0f, 100.0f);
        Puzzle.getMaxRadius().reset(76.66666f, 76.66666f);
        c835843763(c);
        c2117744539(c);
        c554505218(c);
        c540953574(c);
        c347201124(c);
        c549195933(c);
        c1344449134(c);
        c32578822(c);
        c959494666(c);
        c1297346196(c);
        c173612097(c);
        c1735473045(c);
        c1311493721(c);
        c624448770(c);
        c1957967961(c);
        c1995761681(c);
        c1776101008(c);
        c69616936(c);
        c1760440841(c);
        c817111459(c);
        c1897980253(c);
        c9891676(c);
        c1165443759(c);
        c432650368(c);
        c1000996921(c);
        c483679687(c);
        c157144169(c);
        c1134129599(c);
        c1458548836(c);
        c33141792(c);
        c43735389(c);
        c634519917(c);
        c1393730910(c);
        c2032024260(c);
        c768038756(c);
        c1707850582(c);
        c117419844(c);
        c1381295196(c);
        c1190328125(c);
        c702574111(c);
        c635774282(c);
        c418604698(c);
        c709322898(c);
        c732856940(c);
        c408533478(c);
        c437778440(c);
        c436263762(c);
        c487737114(c);
    }

    public void loadInterface(Context c) {
        this.MoveButton = new GameImageSpirit();
        this.MoveButton.setLevel(22.0f);
        this.MoveButton.getImage().loadImageFromResource(c, R.drawable.movebutton);
        this.MoveButton.getSize().reset(155.0f, 155.0f);
        this.MoveButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButton);
        final Vector2DF mvds = this.MoveButton.getDisplaySize();
        mvds.scale(0.5f);
        this.MoveButton.getPosition().reset(5.0f + mvds.getX(), ((float) (GameMaster.getScreenHeight() - 5)) - mvds.getY());
        this.MoveButton.getPositionOffset().resetWith(mvds.negateNew());
        this.MoveButtonSlot = new UI() {
            Vector2DF movevector;
            boolean needmove;

            public void executive(Vector2DF point) {
                this.movevector = point.minusNew(getPosition()).restrainLength(mvds.getX() * 0.4f);
                this.needmove = true;
            }

            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.needmove) {
                    PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition().plusNew(this.movevector));
                    PuzzleScene.this.Desktop.getCurrentObservePosition().plus(this.movevector);
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < mvds.getX() + 5.0f;
            }

            public void reset() {
                this.needmove = false;
                PuzzleScene.this.MoveButton.getPosition().resetWith(getPosition());
                PuzzleScene.this.refreshDrawCacheBitmap();
            }
        };
        this.MoveButtonSlot.setLevel(21.0f);
        this.MoveButtonSlot.getImage().loadImageFromResource(c, R.drawable.movebuttonslot);
        this.MoveButtonSlot.getSize().reset(155.0f, 155.0f);
        this.MoveButtonSlot.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.MoveButtonSlot);
        this.MoveButtonSlot.getPosition().resetWith(this.MoveButton.getPosition());
        this.MoveButtonSlot.getPositionOffset().resetWith(this.MoveButton.getPositionOffset());
        this.ZoomInButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(long, long):long}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.min(1.3f, PuzzleScene.this.Desktop.getZoom() * 1.05f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                }
                super.updateSelf();
            }
        };
        this.ZoomInButton.setLevel(21.0f);
        this.ZoomInButton.getImage().loadImageFromResource(c, R.drawable.zoombutton1);
        this.ZoomInButton.getSize().reset(77.0f, 77.0f);
        this.ZoomInButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomInButton);
        Vector2DF zibds = this.ZoomInButton.getDisplaySize().scale(0.5f);
        this.ZoomInButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 50)) - zibds.getX(), ((float) (GameMaster.getScreenHeight() - 10)) - zibds.getY());
        this.ZoomInButton.getPositionOffset().resetWith(zibds.negateNew());
        this.ZoomOutButton = new UI() {
            boolean zoom;

            public void executive(Vector2DF point) {
                this.zoom = true;
            }

            public boolean isInArea(Vector2DF point) {
                return point.minusNew(getPosition()).getLength() < getDisplaySize().getX() / 2.0f;
            }

            public void reset() {
                this.zoom = false;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.max(float, float):float}
             arg types: [int, float]
             candidates:
              ClspMth{java.lang.Math.max(double, double):double}
              ClspMth{java.lang.Math.max(int, int):int}
              ClspMth{java.lang.Math.max(long, long):long}
              ClspMth{java.lang.Math.max(float, float):float} */
            /* access modifiers changed from: protected */
            public void updateSelf() {
                if (this.zoom) {
                    PuzzleScene.this.Desktop.setZoom(Math.max(0.3f, PuzzleScene.this.Desktop.getZoom() * 0.95f));
                    PuzzleScene.this.refreshDrawCacheBitmap();
                    GameMaster.log(this, Float.valueOf(PuzzleScene.this.Desktop.getCurrentObserveAreaRectF().width()));
                }
                super.updateSelf();
            }
        };
        this.ZoomOutButton.setLevel(21.0f);
        this.ZoomOutButton.getImage().loadImageFromResource(c, R.drawable.zoombutton2);
        this.ZoomOutButton.getSize().reset(77.0f, 77.0f);
        this.ZoomOutButton.setIsUseAbsolutePosition(true);
        getSpiritList().add(this.ZoomOutButton);
        Vector2DF zobds = this.ZoomOutButton.getDisplaySize().scale(0.5f);
        this.ZoomOutButton.getPosition().reset((((float) (GameMaster.getScreenWidth() - 100)) - zobds.getX()) - (zibds.getX() * 2.0f), ((float) (GameMaster.getScreenHeight() - 10)) - zobds.getY());
        this.ZoomOutButton.getPositionOffset().resetWith(zobds.negateNew());
        this.ViewFullButton = new UI() {
            public void executive(Vector2DF point) {
                if (PuzzleScene.this.OriginalImage.getImage().getImage() == null) {
                    PuzzleScene.this.OriginalImage.getImage().loadImageFromResource(GameMaster.getContext(), R.drawable.oim);
                }
                PuzzleScene.this.OriginalImage.show();
            }

            public boolean isInArea(Vector2DF point) {
                return point.isIn(PuzzleScene.this.ViewFullButtonRect);
            }

            public void reset() {
                PuzzleScene.this.OriginalImage.hide();
            }
        };
        this.ViewFullButton.getImage().loadImageFromResource(c, R.drawable.viewfull);
        this.ViewFullButton.setLevel(21.0f);
        this.ViewFullButton.setIsUseAbsolutePosition(true);
        this.ViewFullButton.getSize().reset(83.0f, 38.0f);
        getSpiritList().add(this.ViewFullButton);
        this.ViewFullButton.getPosition().reset(((float) (GameMaster.getScreenWidth() - 15)) - this.ViewFullButton.getDisplaySize().getX(), 15.0f);
        this.ViewFullButtonRect = new VectorRect2DF(this.ViewFullButton.getPosition(), this.ViewFullButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton = new UI() {
            public void executive(Vector2DF point) {
                Score score = new Score(Double.valueOf(Math.ceil((double) (PuzzleScene.this.PastTime / 1000))), null);
                score.setMode(21);
                ScoreloopManagerSingleton.get().onGamePlayEnded(score);
                hide();
                Toast.makeText(GameMaster.getContext(), (int) R.string.Submitting, 1).show();
            }

            public boolean isInArea(Vector2DF point) {
                return getVisibleOriginalValue() && point.isIn(PuzzleScene.this.SubmitScoreButtonRect);
            }

            public void reset() {
            }
        };
        this.SubmitScoreButton.getImage().loadImageFromResource(c, R.drawable.submitscore);
        this.SubmitScoreButton.setLevel(21.0f);
        this.SubmitScoreButton.setIsUseAbsolutePosition(true);
        this.SubmitScoreButton.getSize().reset(114.0f, 40.0f);
        getSpiritList().add(this.SubmitScoreButton);
        this.SubmitScoreButton.getPosition().reset(15.0f, 60.0f);
        this.SubmitScoreButtonRect = new VectorRect2DF(this.SubmitScoreButton.getPosition(), this.SubmitScoreButton.getDisplaySize()).getRectF();
        this.SubmitScoreButton.hide();
        this.Time = new GameSpirit() {
            DecimalFormat FMT = new DecimalFormat("00");
            Paint p1;
            Paint p2;

            public GameObject getDisplayContentChild() {
                return null;
            }

            /* access modifiers changed from: protected */
            public void drawSelf(Canvas c, Rect drawArea) {
                if (this.p1 == null) {
                    this.p1 = new Paint();
                    this.p1.setARGB(160, 255, 255, 255);
                    this.p1.setAntiAlias(true);
                    this.p1.setTextSize(28.0f);
                    this.p1.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                if (this.p2 == null) {
                    this.p2 = new Paint();
                    this.p2.setARGB(100, 0, 0, 0);
                    this.p2.setAntiAlias(true);
                    this.p2.setTextSize(28.0f);
                    this.p2.setTypeface(Typeface.createFromAsset(GameMaster.getContext().getAssets(), "fonts/font.ttf"));
                }
                long t = PuzzleScene.this.getIsFinished() ? PuzzleScene.this.PastTime : PuzzleScene.this.PastTime + (new Date().getTime() - PuzzleScene.this.StartTime);
                long h = t / 3600000;
                long m = (t - (((1000 * h) * 60) * 60)) / 60000;
                String str = String.valueOf(this.FMT.format(h)) + ":" + this.FMT.format(m) + ":" + this.FMT.format(((t - (((1000 * h) * 60) * 60)) - ((1000 * m) * 60)) / 1000) + (PuzzleScene.this.getIsFinished() ? " Finished" : "");
                c.drawText(str, 16.0f, 44.0f, this.p2);
                c.drawText(str, 15.0f, 43.0f, this.p1);
                super.drawSelf(c, drawArea);
            }
        };
        this.Time.setLevel(40.0f);
        getSpiritList().add(this.Time);
    }

    public float getMaxPuzzleLevel() {
        float maxlvl = 0.0f;
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getLevel() <= 10.0f) {
                maxlvl = Math.max(f.getLevel(), maxlvl);
            }
        }
        return maxlvl;
    }

    public Puzzle getTouchPuzzle(Vector2DF v) {
        Vector2DF rv = this.Desktop.reMapPoint(v);
        Puzzle o = null;
        float olen = 99999.0f;
        for (int i = getSpiritList().size() - 1; i >= 0; i--) {
            if (getSpiritList().get(i) instanceof Puzzle) {
                Puzzle p = (Puzzle) getSpiritList().get(i);
                float len = p.getPositionInDesktop().minusNew(rv).getLength();
                if (len <= Puzzle.getRealitySize().getX() * 0.4f) {
                    return p;
                }
                if (len <= Puzzle.getRealitySize().getX() && olen > len) {
                    o = p;
                    olen = len;
                }
            }
        }
        return o;
    }

    public void startDrawSelectRect(Vector2DF startDragPoint, Vector2DF v) {
        this.selectRect = new VectorRect2DF(startDragPoint, v.minusNew(startDragPoint)).getFixedRectF();
    }

    public void stopDrawSelectRect() {
        this.selectRect = null;
    }

    public ArrayList<Puzzle> getSelectPuzzle(Vector2DF startDragPoint, Vector2DF v) {
        Vector2DF rs = this.Desktop.reMapPoint(startDragPoint);
        ArrayList<Puzzle> l = new ArrayList<>();
        RectF rect = new VectorRect2DF(rs, this.Desktop.reMapPoint(v).minusNew(rs)).getFixedRectF();
        float xo = Puzzle.getRealitySize().getX() / 3.0f;
        float yo = Puzzle.getRealitySize().getY() / 3.0f;
        RectF sr = new RectF(rect.left - xo, rect.top - yo, rect.right + xo, rect.bottom + yo);
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            if (f.getPositionInDesktop().isIn(sr)) {
                l.add(f);
            }
        }
        return l;
    }

    public void loadPuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences s = c.getSharedPreferences();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().load(c, s);
        }
    }

    public void savePuzzleState() {
        Center c = (Center) GameMaster.getContext().getApplicationContext();
        SharedPreferences.Editor e = c.getSharedPreferences().edit();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            it.next().save(c, e);
        }
        e.commit();
    }

    public void reset() {
        this.Controller.reset();
        Iterator<Puzzle> it = this.Desktop.getPuzzleList().iterator();
        while (it.hasNext()) {
            Puzzle f = it.next();
            f.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
            this.Desktop.RandomlyPlaced(f);
        }
        refreshDrawCacheBitmap();
        setIsFinishedToDefault();
        this.StartTime = new Date().getTime();
        this.PastTime = 0;
        this.SubmitScoreButton.hide();
        saveGameState();
        savePuzzleState();
    }

    /* access modifiers changed from: package-private */
    public void c835843763(Context c) {
        Puzzle p835843763 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load835843763(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save835843763(editor, this);
            }
        };
        p835843763.setID(835843763);
        p835843763.setName("835843763");
        p835843763.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p835843763);
        this.Desktop.RandomlyPlaced(p835843763);
        p835843763.setTopEdgeType(EdgeType.Flat);
        p835843763.setBottomEdgeType(EdgeType.Convex);
        p835843763.setLeftEdgeType(EdgeType.Flat);
        p835843763.setRightEdgeType(EdgeType.Concave);
        p835843763.setTopExactPuzzleID(-1);
        p835843763.setBottomExactPuzzleID(2117744539);
        p835843763.setLeftExactPuzzleID(-1);
        p835843763.setRightExactPuzzleID(1344449134);
        p835843763.getDisplayImage().loadImageFromResource(c, R.drawable.p835843763h);
        p835843763.setExactRow(0);
        p835843763.setExactColumn(0);
        p835843763.getSize().reset(100.0f, 126.6667f);
        p835843763.getPositionOffset().reset(-50.0f, -50.0f);
        p835843763.setIsUseAbsolutePosition(true);
        p835843763.setIsUseAbsoluteSize(true);
        p835843763.getImage().setIsUseAbsolutePosition(true);
        p835843763.getImage().setIsUseAbsoluteSize(true);
        p835843763.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p835843763.resetPosition();
        getSpiritList().add(p835843763);
    }

    /* access modifiers changed from: package-private */
    public void c2117744539(Context c) {
        Puzzle p2117744539 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2117744539(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2117744539(editor, this);
            }
        };
        p2117744539.setID(2117744539);
        p2117744539.setName("2117744539");
        p2117744539.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2117744539);
        this.Desktop.RandomlyPlaced(p2117744539);
        p2117744539.setTopEdgeType(EdgeType.Concave);
        p2117744539.setBottomEdgeType(EdgeType.Convex);
        p2117744539.setLeftEdgeType(EdgeType.Flat);
        p2117744539.setRightEdgeType(EdgeType.Convex);
        p2117744539.setTopExactPuzzleID(835843763);
        p2117744539.setBottomExactPuzzleID(554505218);
        p2117744539.setLeftExactPuzzleID(-1);
        p2117744539.setRightExactPuzzleID(32578822);
        p2117744539.getDisplayImage().loadImageFromResource(c, R.drawable.p2117744539h);
        p2117744539.setExactRow(1);
        p2117744539.setExactColumn(0);
        p2117744539.getSize().reset(126.6667f, 126.6667f);
        p2117744539.getPositionOffset().reset(-50.0f, -50.0f);
        p2117744539.setIsUseAbsolutePosition(true);
        p2117744539.setIsUseAbsoluteSize(true);
        p2117744539.getImage().setIsUseAbsolutePosition(true);
        p2117744539.getImage().setIsUseAbsoluteSize(true);
        p2117744539.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2117744539.resetPosition();
        getSpiritList().add(p2117744539);
    }

    /* access modifiers changed from: package-private */
    public void c554505218(Context c) {
        Puzzle p554505218 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load554505218(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save554505218(editor, this);
            }
        };
        p554505218.setID(554505218);
        p554505218.setName("554505218");
        p554505218.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p554505218);
        this.Desktop.RandomlyPlaced(p554505218);
        p554505218.setTopEdgeType(EdgeType.Concave);
        p554505218.setBottomEdgeType(EdgeType.Concave);
        p554505218.setLeftEdgeType(EdgeType.Flat);
        p554505218.setRightEdgeType(EdgeType.Convex);
        p554505218.setTopExactPuzzleID(2117744539);
        p554505218.setBottomExactPuzzleID(540953574);
        p554505218.setLeftExactPuzzleID(-1);
        p554505218.setRightExactPuzzleID(959494666);
        p554505218.getDisplayImage().loadImageFromResource(c, R.drawable.p554505218h);
        p554505218.setExactRow(2);
        p554505218.setExactColumn(0);
        p554505218.getSize().reset(126.6667f, 100.0f);
        p554505218.getPositionOffset().reset(-50.0f, -50.0f);
        p554505218.setIsUseAbsolutePosition(true);
        p554505218.setIsUseAbsoluteSize(true);
        p554505218.getImage().setIsUseAbsolutePosition(true);
        p554505218.getImage().setIsUseAbsoluteSize(true);
        p554505218.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p554505218.resetPosition();
        getSpiritList().add(p554505218);
    }

    /* access modifiers changed from: package-private */
    public void c540953574(Context c) {
        Puzzle p540953574 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load540953574(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save540953574(editor, this);
            }
        };
        p540953574.setID(540953574);
        p540953574.setName("540953574");
        p540953574.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p540953574);
        this.Desktop.RandomlyPlaced(p540953574);
        p540953574.setTopEdgeType(EdgeType.Convex);
        p540953574.setBottomEdgeType(EdgeType.Concave);
        p540953574.setLeftEdgeType(EdgeType.Flat);
        p540953574.setRightEdgeType(EdgeType.Convex);
        p540953574.setTopExactPuzzleID(554505218);
        p540953574.setBottomExactPuzzleID(347201124);
        p540953574.setLeftExactPuzzleID(-1);
        p540953574.setRightExactPuzzleID(1297346196);
        p540953574.getDisplayImage().loadImageFromResource(c, R.drawable.p540953574h);
        p540953574.setExactRow(3);
        p540953574.setExactColumn(0);
        p540953574.getSize().reset(126.6667f, 126.6667f);
        p540953574.getPositionOffset().reset(-50.0f, -76.66666f);
        p540953574.setIsUseAbsolutePosition(true);
        p540953574.setIsUseAbsoluteSize(true);
        p540953574.getImage().setIsUseAbsolutePosition(true);
        p540953574.getImage().setIsUseAbsoluteSize(true);
        p540953574.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p540953574.resetPosition();
        getSpiritList().add(p540953574);
    }

    /* access modifiers changed from: package-private */
    public void c347201124(Context c) {
        Puzzle p347201124 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load347201124(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save347201124(editor, this);
            }
        };
        p347201124.setID(347201124);
        p347201124.setName("347201124");
        p347201124.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p347201124);
        this.Desktop.RandomlyPlaced(p347201124);
        p347201124.setTopEdgeType(EdgeType.Convex);
        p347201124.setBottomEdgeType(EdgeType.Concave);
        p347201124.setLeftEdgeType(EdgeType.Flat);
        p347201124.setRightEdgeType(EdgeType.Convex);
        p347201124.setTopExactPuzzleID(540953574);
        p347201124.setBottomExactPuzzleID(549195933);
        p347201124.setLeftExactPuzzleID(-1);
        p347201124.setRightExactPuzzleID(173612097);
        p347201124.getDisplayImage().loadImageFromResource(c, R.drawable.p347201124h);
        p347201124.setExactRow(4);
        p347201124.setExactColumn(0);
        p347201124.getSize().reset(126.6667f, 126.6667f);
        p347201124.getPositionOffset().reset(-50.0f, -76.66666f);
        p347201124.setIsUseAbsolutePosition(true);
        p347201124.setIsUseAbsoluteSize(true);
        p347201124.getImage().setIsUseAbsolutePosition(true);
        p347201124.getImage().setIsUseAbsoluteSize(true);
        p347201124.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p347201124.resetPosition();
        getSpiritList().add(p347201124);
    }

    /* access modifiers changed from: package-private */
    public void c549195933(Context c) {
        Puzzle p549195933 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load549195933(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save549195933(editor, this);
            }
        };
        p549195933.setID(549195933);
        p549195933.setName("549195933");
        p549195933.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p549195933);
        this.Desktop.RandomlyPlaced(p549195933);
        p549195933.setTopEdgeType(EdgeType.Convex);
        p549195933.setBottomEdgeType(EdgeType.Flat);
        p549195933.setLeftEdgeType(EdgeType.Flat);
        p549195933.setRightEdgeType(EdgeType.Convex);
        p549195933.setTopExactPuzzleID(347201124);
        p549195933.setBottomExactPuzzleID(-1);
        p549195933.setLeftExactPuzzleID(-1);
        p549195933.setRightExactPuzzleID(1735473045);
        p549195933.getDisplayImage().loadImageFromResource(c, R.drawable.p549195933h);
        p549195933.setExactRow(5);
        p549195933.setExactColumn(0);
        p549195933.getSize().reset(126.6667f, 126.6667f);
        p549195933.getPositionOffset().reset(-50.0f, -76.66666f);
        p549195933.setIsUseAbsolutePosition(true);
        p549195933.setIsUseAbsoluteSize(true);
        p549195933.getImage().setIsUseAbsolutePosition(true);
        p549195933.getImage().setIsUseAbsoluteSize(true);
        p549195933.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p549195933.resetPosition();
        getSpiritList().add(p549195933);
    }

    /* access modifiers changed from: package-private */
    public void c1344449134(Context c) {
        Puzzle p1344449134 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1344449134(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1344449134(editor, this);
            }
        };
        p1344449134.setID(1344449134);
        p1344449134.setName("1344449134");
        p1344449134.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1344449134);
        this.Desktop.RandomlyPlaced(p1344449134);
        p1344449134.setTopEdgeType(EdgeType.Flat);
        p1344449134.setBottomEdgeType(EdgeType.Concave);
        p1344449134.setLeftEdgeType(EdgeType.Convex);
        p1344449134.setRightEdgeType(EdgeType.Concave);
        p1344449134.setTopExactPuzzleID(-1);
        p1344449134.setBottomExactPuzzleID(32578822);
        p1344449134.setLeftExactPuzzleID(835843763);
        p1344449134.setRightExactPuzzleID(1311493721);
        p1344449134.getDisplayImage().loadImageFromResource(c, R.drawable.p1344449134h);
        p1344449134.setExactRow(0);
        p1344449134.setExactColumn(1);
        p1344449134.getSize().reset(126.6667f, 100.0f);
        p1344449134.getPositionOffset().reset(-76.66666f, -50.0f);
        p1344449134.setIsUseAbsolutePosition(true);
        p1344449134.setIsUseAbsoluteSize(true);
        p1344449134.getImage().setIsUseAbsolutePosition(true);
        p1344449134.getImage().setIsUseAbsoluteSize(true);
        p1344449134.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1344449134.resetPosition();
        getSpiritList().add(p1344449134);
    }

    /* access modifiers changed from: package-private */
    public void c32578822(Context c) {
        Puzzle p32578822 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load32578822(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save32578822(editor, this);
            }
        };
        p32578822.setID(32578822);
        p32578822.setName("32578822");
        p32578822.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p32578822);
        this.Desktop.RandomlyPlaced(p32578822);
        p32578822.setTopEdgeType(EdgeType.Convex);
        p32578822.setBottomEdgeType(EdgeType.Concave);
        p32578822.setLeftEdgeType(EdgeType.Concave);
        p32578822.setRightEdgeType(EdgeType.Convex);
        p32578822.setTopExactPuzzleID(1344449134);
        p32578822.setBottomExactPuzzleID(959494666);
        p32578822.setLeftExactPuzzleID(2117744539);
        p32578822.setRightExactPuzzleID(624448770);
        p32578822.getDisplayImage().loadImageFromResource(c, R.drawable.p32578822h);
        p32578822.setExactRow(1);
        p32578822.setExactColumn(1);
        p32578822.getSize().reset(126.6667f, 126.6667f);
        p32578822.getPositionOffset().reset(-50.0f, -76.66666f);
        p32578822.setIsUseAbsolutePosition(true);
        p32578822.setIsUseAbsoluteSize(true);
        p32578822.getImage().setIsUseAbsolutePosition(true);
        p32578822.getImage().setIsUseAbsoluteSize(true);
        p32578822.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p32578822.resetPosition();
        getSpiritList().add(p32578822);
    }

    /* access modifiers changed from: package-private */
    public void c959494666(Context c) {
        Puzzle p959494666 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load959494666(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save959494666(editor, this);
            }
        };
        p959494666.setID(959494666);
        p959494666.setName("959494666");
        p959494666.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p959494666);
        this.Desktop.RandomlyPlaced(p959494666);
        p959494666.setTopEdgeType(EdgeType.Convex);
        p959494666.setBottomEdgeType(EdgeType.Convex);
        p959494666.setLeftEdgeType(EdgeType.Concave);
        p959494666.setRightEdgeType(EdgeType.Concave);
        p959494666.setTopExactPuzzleID(32578822);
        p959494666.setBottomExactPuzzleID(1297346196);
        p959494666.setLeftExactPuzzleID(554505218);
        p959494666.setRightExactPuzzleID(1957967961);
        p959494666.getDisplayImage().loadImageFromResource(c, R.drawable.p959494666h);
        p959494666.setExactRow(2);
        p959494666.setExactColumn(1);
        p959494666.getSize().reset(100.0f, 153.3333f);
        p959494666.getPositionOffset().reset(-50.0f, -76.66666f);
        p959494666.setIsUseAbsolutePosition(true);
        p959494666.setIsUseAbsoluteSize(true);
        p959494666.getImage().setIsUseAbsolutePosition(true);
        p959494666.getImage().setIsUseAbsoluteSize(true);
        p959494666.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p959494666.resetPosition();
        getSpiritList().add(p959494666);
    }

    /* access modifiers changed from: package-private */
    public void c1297346196(Context c) {
        Puzzle p1297346196 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1297346196(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1297346196(editor, this);
            }
        };
        p1297346196.setID(1297346196);
        p1297346196.setName("1297346196");
        p1297346196.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1297346196);
        this.Desktop.RandomlyPlaced(p1297346196);
        p1297346196.setTopEdgeType(EdgeType.Concave);
        p1297346196.setBottomEdgeType(EdgeType.Concave);
        p1297346196.setLeftEdgeType(EdgeType.Concave);
        p1297346196.setRightEdgeType(EdgeType.Concave);
        p1297346196.setTopExactPuzzleID(959494666);
        p1297346196.setBottomExactPuzzleID(173612097);
        p1297346196.setLeftExactPuzzleID(540953574);
        p1297346196.setRightExactPuzzleID(1995761681);
        p1297346196.getDisplayImage().loadImageFromResource(c, R.drawable.p1297346196h);
        p1297346196.setExactRow(3);
        p1297346196.setExactColumn(1);
        p1297346196.getSize().reset(100.0f, 100.0f);
        p1297346196.getPositionOffset().reset(-50.0f, -50.0f);
        p1297346196.setIsUseAbsolutePosition(true);
        p1297346196.setIsUseAbsoluteSize(true);
        p1297346196.getImage().setIsUseAbsolutePosition(true);
        p1297346196.getImage().setIsUseAbsoluteSize(true);
        p1297346196.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1297346196.resetPosition();
        getSpiritList().add(p1297346196);
    }

    /* access modifiers changed from: package-private */
    public void c173612097(Context c) {
        Puzzle p173612097 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load173612097(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save173612097(editor, this);
            }
        };
        p173612097.setID(173612097);
        p173612097.setName("173612097");
        p173612097.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p173612097);
        this.Desktop.RandomlyPlaced(p173612097);
        p173612097.setTopEdgeType(EdgeType.Convex);
        p173612097.setBottomEdgeType(EdgeType.Concave);
        p173612097.setLeftEdgeType(EdgeType.Concave);
        p173612097.setRightEdgeType(EdgeType.Convex);
        p173612097.setTopExactPuzzleID(1297346196);
        p173612097.setBottomExactPuzzleID(1735473045);
        p173612097.setLeftExactPuzzleID(347201124);
        p173612097.setRightExactPuzzleID(1776101008);
        p173612097.getDisplayImage().loadImageFromResource(c, R.drawable.p173612097h);
        p173612097.setExactRow(4);
        p173612097.setExactColumn(1);
        p173612097.getSize().reset(126.6667f, 126.6667f);
        p173612097.getPositionOffset().reset(-50.0f, -76.66666f);
        p173612097.setIsUseAbsolutePosition(true);
        p173612097.setIsUseAbsoluteSize(true);
        p173612097.getImage().setIsUseAbsolutePosition(true);
        p173612097.getImage().setIsUseAbsoluteSize(true);
        p173612097.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p173612097.resetPosition();
        getSpiritList().add(p173612097);
    }

    /* access modifiers changed from: package-private */
    public void c1735473045(Context c) {
        Puzzle p1735473045 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1735473045(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1735473045(editor, this);
            }
        };
        p1735473045.setID(1735473045);
        p1735473045.setName("1735473045");
        p1735473045.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1735473045);
        this.Desktop.RandomlyPlaced(p1735473045);
        p1735473045.setTopEdgeType(EdgeType.Convex);
        p1735473045.setBottomEdgeType(EdgeType.Flat);
        p1735473045.setLeftEdgeType(EdgeType.Concave);
        p1735473045.setRightEdgeType(EdgeType.Concave);
        p1735473045.setTopExactPuzzleID(173612097);
        p1735473045.setBottomExactPuzzleID(-1);
        p1735473045.setLeftExactPuzzleID(549195933);
        p1735473045.setRightExactPuzzleID(69616936);
        p1735473045.getDisplayImage().loadImageFromResource(c, R.drawable.p1735473045h);
        p1735473045.setExactRow(5);
        p1735473045.setExactColumn(1);
        p1735473045.getSize().reset(100.0f, 126.6667f);
        p1735473045.getPositionOffset().reset(-50.0f, -76.66666f);
        p1735473045.setIsUseAbsolutePosition(true);
        p1735473045.setIsUseAbsoluteSize(true);
        p1735473045.getImage().setIsUseAbsolutePosition(true);
        p1735473045.getImage().setIsUseAbsoluteSize(true);
        p1735473045.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1735473045.resetPosition();
        getSpiritList().add(p1735473045);
    }

    /* access modifiers changed from: package-private */
    public void c1311493721(Context c) {
        Puzzle p1311493721 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1311493721(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1311493721(editor, this);
            }
        };
        p1311493721.setID(1311493721);
        p1311493721.setName("1311493721");
        p1311493721.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1311493721);
        this.Desktop.RandomlyPlaced(p1311493721);
        p1311493721.setTopEdgeType(EdgeType.Flat);
        p1311493721.setBottomEdgeType(EdgeType.Concave);
        p1311493721.setLeftEdgeType(EdgeType.Convex);
        p1311493721.setRightEdgeType(EdgeType.Convex);
        p1311493721.setTopExactPuzzleID(-1);
        p1311493721.setBottomExactPuzzleID(624448770);
        p1311493721.setLeftExactPuzzleID(1344449134);
        p1311493721.setRightExactPuzzleID(1760440841);
        p1311493721.getDisplayImage().loadImageFromResource(c, R.drawable.p1311493721h);
        p1311493721.setExactRow(0);
        p1311493721.setExactColumn(2);
        p1311493721.getSize().reset(153.3333f, 100.0f);
        p1311493721.getPositionOffset().reset(-76.66666f, -50.0f);
        p1311493721.setIsUseAbsolutePosition(true);
        p1311493721.setIsUseAbsoluteSize(true);
        p1311493721.getImage().setIsUseAbsolutePosition(true);
        p1311493721.getImage().setIsUseAbsoluteSize(true);
        p1311493721.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1311493721.resetPosition();
        getSpiritList().add(p1311493721);
    }

    /* access modifiers changed from: package-private */
    public void c624448770(Context c) {
        Puzzle p624448770 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load624448770(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save624448770(editor, this);
            }
        };
        p624448770.setID(624448770);
        p624448770.setName("624448770");
        p624448770.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p624448770);
        this.Desktop.RandomlyPlaced(p624448770);
        p624448770.setTopEdgeType(EdgeType.Convex);
        p624448770.setBottomEdgeType(EdgeType.Concave);
        p624448770.setLeftEdgeType(EdgeType.Concave);
        p624448770.setRightEdgeType(EdgeType.Concave);
        p624448770.setTopExactPuzzleID(1311493721);
        p624448770.setBottomExactPuzzleID(1957967961);
        p624448770.setLeftExactPuzzleID(32578822);
        p624448770.setRightExactPuzzleID(817111459);
        p624448770.getDisplayImage().loadImageFromResource(c, R.drawable.p624448770h);
        p624448770.setExactRow(1);
        p624448770.setExactColumn(2);
        p624448770.getSize().reset(100.0f, 126.6667f);
        p624448770.getPositionOffset().reset(-50.0f, -76.66666f);
        p624448770.setIsUseAbsolutePosition(true);
        p624448770.setIsUseAbsoluteSize(true);
        p624448770.getImage().setIsUseAbsolutePosition(true);
        p624448770.getImage().setIsUseAbsoluteSize(true);
        p624448770.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p624448770.resetPosition();
        getSpiritList().add(p624448770);
    }

    /* access modifiers changed from: package-private */
    public void c1957967961(Context c) {
        Puzzle p1957967961 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1957967961(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1957967961(editor, this);
            }
        };
        p1957967961.setID(1957967961);
        p1957967961.setName("1957967961");
        p1957967961.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1957967961);
        this.Desktop.RandomlyPlaced(p1957967961);
        p1957967961.setTopEdgeType(EdgeType.Convex);
        p1957967961.setBottomEdgeType(EdgeType.Concave);
        p1957967961.setLeftEdgeType(EdgeType.Convex);
        p1957967961.setRightEdgeType(EdgeType.Concave);
        p1957967961.setTopExactPuzzleID(624448770);
        p1957967961.setBottomExactPuzzleID(1995761681);
        p1957967961.setLeftExactPuzzleID(959494666);
        p1957967961.setRightExactPuzzleID(1897980253);
        p1957967961.getDisplayImage().loadImageFromResource(c, R.drawable.p1957967961h);
        p1957967961.setExactRow(2);
        p1957967961.setExactColumn(2);
        p1957967961.getSize().reset(126.6667f, 126.6667f);
        p1957967961.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1957967961.setIsUseAbsolutePosition(true);
        p1957967961.setIsUseAbsoluteSize(true);
        p1957967961.getImage().setIsUseAbsolutePosition(true);
        p1957967961.getImage().setIsUseAbsoluteSize(true);
        p1957967961.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1957967961.resetPosition();
        getSpiritList().add(p1957967961);
    }

    /* access modifiers changed from: package-private */
    public void c1995761681(Context c) {
        Puzzle p1995761681 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1995761681(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1995761681(editor, this);
            }
        };
        p1995761681.setID(1995761681);
        p1995761681.setName("1995761681");
        p1995761681.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1995761681);
        this.Desktop.RandomlyPlaced(p1995761681);
        p1995761681.setTopEdgeType(EdgeType.Convex);
        p1995761681.setBottomEdgeType(EdgeType.Concave);
        p1995761681.setLeftEdgeType(EdgeType.Convex);
        p1995761681.setRightEdgeType(EdgeType.Convex);
        p1995761681.setTopExactPuzzleID(1957967961);
        p1995761681.setBottomExactPuzzleID(1776101008);
        p1995761681.setLeftExactPuzzleID(1297346196);
        p1995761681.setRightExactPuzzleID(9891676);
        p1995761681.getDisplayImage().loadImageFromResource(c, R.drawable.p1995761681h);
        p1995761681.setExactRow(3);
        p1995761681.setExactColumn(2);
        p1995761681.getSize().reset(153.3333f, 126.6667f);
        p1995761681.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1995761681.setIsUseAbsolutePosition(true);
        p1995761681.setIsUseAbsoluteSize(true);
        p1995761681.getImage().setIsUseAbsolutePosition(true);
        p1995761681.getImage().setIsUseAbsoluteSize(true);
        p1995761681.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1995761681.resetPosition();
        getSpiritList().add(p1995761681);
    }

    /* access modifiers changed from: package-private */
    public void c1776101008(Context c) {
        Puzzle p1776101008 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1776101008(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1776101008(editor, this);
            }
        };
        p1776101008.setID(1776101008);
        p1776101008.setName("1776101008");
        p1776101008.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1776101008);
        this.Desktop.RandomlyPlaced(p1776101008);
        p1776101008.setTopEdgeType(EdgeType.Convex);
        p1776101008.setBottomEdgeType(EdgeType.Convex);
        p1776101008.setLeftEdgeType(EdgeType.Concave);
        p1776101008.setRightEdgeType(EdgeType.Concave);
        p1776101008.setTopExactPuzzleID(1995761681);
        p1776101008.setBottomExactPuzzleID(69616936);
        p1776101008.setLeftExactPuzzleID(173612097);
        p1776101008.setRightExactPuzzleID(1165443759);
        p1776101008.getDisplayImage().loadImageFromResource(c, R.drawable.p1776101008h);
        p1776101008.setExactRow(4);
        p1776101008.setExactColumn(2);
        p1776101008.getSize().reset(100.0f, 153.3333f);
        p1776101008.getPositionOffset().reset(-50.0f, -76.66666f);
        p1776101008.setIsUseAbsolutePosition(true);
        p1776101008.setIsUseAbsoluteSize(true);
        p1776101008.getImage().setIsUseAbsolutePosition(true);
        p1776101008.getImage().setIsUseAbsoluteSize(true);
        p1776101008.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1776101008.resetPosition();
        getSpiritList().add(p1776101008);
    }

    /* access modifiers changed from: package-private */
    public void c69616936(Context c) {
        Puzzle p69616936 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load69616936(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save69616936(editor, this);
            }
        };
        p69616936.setID(69616936);
        p69616936.setName("69616936");
        p69616936.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p69616936);
        this.Desktop.RandomlyPlaced(p69616936);
        p69616936.setTopEdgeType(EdgeType.Concave);
        p69616936.setBottomEdgeType(EdgeType.Flat);
        p69616936.setLeftEdgeType(EdgeType.Convex);
        p69616936.setRightEdgeType(EdgeType.Concave);
        p69616936.setTopExactPuzzleID(1776101008);
        p69616936.setBottomExactPuzzleID(-1);
        p69616936.setLeftExactPuzzleID(1735473045);
        p69616936.setRightExactPuzzleID(432650368);
        p69616936.getDisplayImage().loadImageFromResource(c, R.drawable.p69616936h);
        p69616936.setExactRow(5);
        p69616936.setExactColumn(2);
        p69616936.getSize().reset(126.6667f, 100.0f);
        p69616936.getPositionOffset().reset(-76.66666f, -50.0f);
        p69616936.setIsUseAbsolutePosition(true);
        p69616936.setIsUseAbsoluteSize(true);
        p69616936.getImage().setIsUseAbsolutePosition(true);
        p69616936.getImage().setIsUseAbsoluteSize(true);
        p69616936.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p69616936.resetPosition();
        getSpiritList().add(p69616936);
    }

    /* access modifiers changed from: package-private */
    public void c1760440841(Context c) {
        Puzzle p1760440841 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1760440841(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1760440841(editor, this);
            }
        };
        p1760440841.setID(1760440841);
        p1760440841.setName("1760440841");
        p1760440841.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1760440841);
        this.Desktop.RandomlyPlaced(p1760440841);
        p1760440841.setTopEdgeType(EdgeType.Flat);
        p1760440841.setBottomEdgeType(EdgeType.Concave);
        p1760440841.setLeftEdgeType(EdgeType.Concave);
        p1760440841.setRightEdgeType(EdgeType.Concave);
        p1760440841.setTopExactPuzzleID(-1);
        p1760440841.setBottomExactPuzzleID(817111459);
        p1760440841.setLeftExactPuzzleID(1311493721);
        p1760440841.setRightExactPuzzleID(1000996921);
        p1760440841.getDisplayImage().loadImageFromResource(c, R.drawable.p1760440841h);
        p1760440841.setExactRow(0);
        p1760440841.setExactColumn(3);
        p1760440841.getSize().reset(100.0f, 100.0f);
        p1760440841.getPositionOffset().reset(-50.0f, -50.0f);
        p1760440841.setIsUseAbsolutePosition(true);
        p1760440841.setIsUseAbsoluteSize(true);
        p1760440841.getImage().setIsUseAbsolutePosition(true);
        p1760440841.getImage().setIsUseAbsoluteSize(true);
        p1760440841.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1760440841.resetPosition();
        getSpiritList().add(p1760440841);
    }

    /* access modifiers changed from: package-private */
    public void c817111459(Context c) {
        Puzzle p817111459 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load817111459(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save817111459(editor, this);
            }
        };
        p817111459.setID(817111459);
        p817111459.setName("817111459");
        p817111459.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p817111459);
        this.Desktop.RandomlyPlaced(p817111459);
        p817111459.setTopEdgeType(EdgeType.Convex);
        p817111459.setBottomEdgeType(EdgeType.Concave);
        p817111459.setLeftEdgeType(EdgeType.Convex);
        p817111459.setRightEdgeType(EdgeType.Concave);
        p817111459.setTopExactPuzzleID(1760440841);
        p817111459.setBottomExactPuzzleID(1897980253);
        p817111459.setLeftExactPuzzleID(624448770);
        p817111459.setRightExactPuzzleID(483679687);
        p817111459.getDisplayImage().loadImageFromResource(c, R.drawable.p817111459h);
        p817111459.setExactRow(1);
        p817111459.setExactColumn(3);
        p817111459.getSize().reset(126.6667f, 126.6667f);
        p817111459.getPositionOffset().reset(-76.66666f, -76.66666f);
        p817111459.setIsUseAbsolutePosition(true);
        p817111459.setIsUseAbsoluteSize(true);
        p817111459.getImage().setIsUseAbsolutePosition(true);
        p817111459.getImage().setIsUseAbsoluteSize(true);
        p817111459.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p817111459.resetPosition();
        getSpiritList().add(p817111459);
    }

    /* access modifiers changed from: package-private */
    public void c1897980253(Context c) {
        Puzzle p1897980253 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1897980253(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1897980253(editor, this);
            }
        };
        p1897980253.setID(1897980253);
        p1897980253.setName("1897980253");
        p1897980253.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1897980253);
        this.Desktop.RandomlyPlaced(p1897980253);
        p1897980253.setTopEdgeType(EdgeType.Convex);
        p1897980253.setBottomEdgeType(EdgeType.Concave);
        p1897980253.setLeftEdgeType(EdgeType.Convex);
        p1897980253.setRightEdgeType(EdgeType.Convex);
        p1897980253.setTopExactPuzzleID(817111459);
        p1897980253.setBottomExactPuzzleID(9891676);
        p1897980253.setLeftExactPuzzleID(1957967961);
        p1897980253.setRightExactPuzzleID(157144169);
        p1897980253.getDisplayImage().loadImageFromResource(c, R.drawable.p1897980253h);
        p1897980253.setExactRow(2);
        p1897980253.setExactColumn(3);
        p1897980253.getSize().reset(153.3333f, 126.6667f);
        p1897980253.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1897980253.setIsUseAbsolutePosition(true);
        p1897980253.setIsUseAbsoluteSize(true);
        p1897980253.getImage().setIsUseAbsolutePosition(true);
        p1897980253.getImage().setIsUseAbsoluteSize(true);
        p1897980253.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1897980253.resetPosition();
        getSpiritList().add(p1897980253);
    }

    /* access modifiers changed from: package-private */
    public void c9891676(Context c) {
        Puzzle p9891676 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load9891676(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save9891676(editor, this);
            }
        };
        p9891676.setID(9891676);
        p9891676.setName("9891676");
        p9891676.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p9891676);
        this.Desktop.RandomlyPlaced(p9891676);
        p9891676.setTopEdgeType(EdgeType.Convex);
        p9891676.setBottomEdgeType(EdgeType.Concave);
        p9891676.setLeftEdgeType(EdgeType.Concave);
        p9891676.setRightEdgeType(EdgeType.Convex);
        p9891676.setTopExactPuzzleID(1897980253);
        p9891676.setBottomExactPuzzleID(1165443759);
        p9891676.setLeftExactPuzzleID(1995761681);
        p9891676.setRightExactPuzzleID(1134129599);
        p9891676.getDisplayImage().loadImageFromResource(c, R.drawable.p9891676h);
        p9891676.setExactRow(3);
        p9891676.setExactColumn(3);
        p9891676.getSize().reset(126.6667f, 126.6667f);
        p9891676.getPositionOffset().reset(-50.0f, -76.66666f);
        p9891676.setIsUseAbsolutePosition(true);
        p9891676.setIsUseAbsoluteSize(true);
        p9891676.getImage().setIsUseAbsolutePosition(true);
        p9891676.getImage().setIsUseAbsoluteSize(true);
        p9891676.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p9891676.resetPosition();
        getSpiritList().add(p9891676);
    }

    /* access modifiers changed from: package-private */
    public void c1165443759(Context c) {
        Puzzle p1165443759 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1165443759(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1165443759(editor, this);
            }
        };
        p1165443759.setID(1165443759);
        p1165443759.setName("1165443759");
        p1165443759.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1165443759);
        this.Desktop.RandomlyPlaced(p1165443759);
        p1165443759.setTopEdgeType(EdgeType.Convex);
        p1165443759.setBottomEdgeType(EdgeType.Convex);
        p1165443759.setLeftEdgeType(EdgeType.Convex);
        p1165443759.setRightEdgeType(EdgeType.Convex);
        p1165443759.setTopExactPuzzleID(9891676);
        p1165443759.setBottomExactPuzzleID(432650368);
        p1165443759.setLeftExactPuzzleID(1776101008);
        p1165443759.setRightExactPuzzleID(1458548836);
        p1165443759.getDisplayImage().loadImageFromResource(c, R.drawable.p1165443759h);
        p1165443759.setExactRow(4);
        p1165443759.setExactColumn(3);
        p1165443759.getSize().reset(153.3333f, 153.3333f);
        p1165443759.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1165443759.setIsUseAbsolutePosition(true);
        p1165443759.setIsUseAbsoluteSize(true);
        p1165443759.getImage().setIsUseAbsolutePosition(true);
        p1165443759.getImage().setIsUseAbsoluteSize(true);
        p1165443759.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1165443759.resetPosition();
        getSpiritList().add(p1165443759);
    }

    /* access modifiers changed from: package-private */
    public void c432650368(Context c) {
        Puzzle p432650368 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load432650368(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save432650368(editor, this);
            }
        };
        p432650368.setID(432650368);
        p432650368.setName("432650368");
        p432650368.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p432650368);
        this.Desktop.RandomlyPlaced(p432650368);
        p432650368.setTopEdgeType(EdgeType.Concave);
        p432650368.setBottomEdgeType(EdgeType.Flat);
        p432650368.setLeftEdgeType(EdgeType.Convex);
        p432650368.setRightEdgeType(EdgeType.Convex);
        p432650368.setTopExactPuzzleID(1165443759);
        p432650368.setBottomExactPuzzleID(-1);
        p432650368.setLeftExactPuzzleID(69616936);
        p432650368.setRightExactPuzzleID(33141792);
        p432650368.getDisplayImage().loadImageFromResource(c, R.drawable.p432650368h);
        p432650368.setExactRow(5);
        p432650368.setExactColumn(3);
        p432650368.getSize().reset(153.3333f, 100.0f);
        p432650368.getPositionOffset().reset(-76.66666f, -50.0f);
        p432650368.setIsUseAbsolutePosition(true);
        p432650368.setIsUseAbsoluteSize(true);
        p432650368.getImage().setIsUseAbsolutePosition(true);
        p432650368.getImage().setIsUseAbsoluteSize(true);
        p432650368.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p432650368.resetPosition();
        getSpiritList().add(p432650368);
    }

    /* access modifiers changed from: package-private */
    public void c1000996921(Context c) {
        Puzzle p1000996921 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1000996921(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1000996921(editor, this);
            }
        };
        p1000996921.setID(1000996921);
        p1000996921.setName("1000996921");
        p1000996921.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1000996921);
        this.Desktop.RandomlyPlaced(p1000996921);
        p1000996921.setTopEdgeType(EdgeType.Flat);
        p1000996921.setBottomEdgeType(EdgeType.Convex);
        p1000996921.setLeftEdgeType(EdgeType.Convex);
        p1000996921.setRightEdgeType(EdgeType.Convex);
        p1000996921.setTopExactPuzzleID(-1);
        p1000996921.setBottomExactPuzzleID(483679687);
        p1000996921.setLeftExactPuzzleID(1760440841);
        p1000996921.setRightExactPuzzleID(43735389);
        p1000996921.getDisplayImage().loadImageFromResource(c, R.drawable.p1000996921h);
        p1000996921.setExactRow(0);
        p1000996921.setExactColumn(4);
        p1000996921.getSize().reset(153.3333f, 126.6667f);
        p1000996921.getPositionOffset().reset(-76.66666f, -50.0f);
        p1000996921.setIsUseAbsolutePosition(true);
        p1000996921.setIsUseAbsoluteSize(true);
        p1000996921.getImage().setIsUseAbsolutePosition(true);
        p1000996921.getImage().setIsUseAbsoluteSize(true);
        p1000996921.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1000996921.resetPosition();
        getSpiritList().add(p1000996921);
    }

    /* access modifiers changed from: package-private */
    public void c483679687(Context c) {
        Puzzle p483679687 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load483679687(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save483679687(editor, this);
            }
        };
        p483679687.setID(483679687);
        p483679687.setName("483679687");
        p483679687.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p483679687);
        this.Desktop.RandomlyPlaced(p483679687);
        p483679687.setTopEdgeType(EdgeType.Concave);
        p483679687.setBottomEdgeType(EdgeType.Convex);
        p483679687.setLeftEdgeType(EdgeType.Convex);
        p483679687.setRightEdgeType(EdgeType.Convex);
        p483679687.setTopExactPuzzleID(1000996921);
        p483679687.setBottomExactPuzzleID(157144169);
        p483679687.setLeftExactPuzzleID(817111459);
        p483679687.setRightExactPuzzleID(634519917);
        p483679687.getDisplayImage().loadImageFromResource(c, R.drawable.p483679687h);
        p483679687.setExactRow(1);
        p483679687.setExactColumn(4);
        p483679687.getSize().reset(153.3333f, 126.6667f);
        p483679687.getPositionOffset().reset(-76.66666f, -50.0f);
        p483679687.setIsUseAbsolutePosition(true);
        p483679687.setIsUseAbsoluteSize(true);
        p483679687.getImage().setIsUseAbsolutePosition(true);
        p483679687.getImage().setIsUseAbsoluteSize(true);
        p483679687.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p483679687.resetPosition();
        getSpiritList().add(p483679687);
    }

    /* access modifiers changed from: package-private */
    public void c157144169(Context c) {
        Puzzle p157144169 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load157144169(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save157144169(editor, this);
            }
        };
        p157144169.setID(157144169);
        p157144169.setName("157144169");
        p157144169.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p157144169);
        this.Desktop.RandomlyPlaced(p157144169);
        p157144169.setTopEdgeType(EdgeType.Concave);
        p157144169.setBottomEdgeType(EdgeType.Convex);
        p157144169.setLeftEdgeType(EdgeType.Concave);
        p157144169.setRightEdgeType(EdgeType.Concave);
        p157144169.setTopExactPuzzleID(483679687);
        p157144169.setBottomExactPuzzleID(1134129599);
        p157144169.setLeftExactPuzzleID(1897980253);
        p157144169.setRightExactPuzzleID(1393730910);
        p157144169.getDisplayImage().loadImageFromResource(c, R.drawable.p157144169h);
        p157144169.setExactRow(2);
        p157144169.setExactColumn(4);
        p157144169.getSize().reset(100.0f, 126.6667f);
        p157144169.getPositionOffset().reset(-50.0f, -50.0f);
        p157144169.setIsUseAbsolutePosition(true);
        p157144169.setIsUseAbsoluteSize(true);
        p157144169.getImage().setIsUseAbsolutePosition(true);
        p157144169.getImage().setIsUseAbsoluteSize(true);
        p157144169.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p157144169.resetPosition();
        getSpiritList().add(p157144169);
    }

    /* access modifiers changed from: package-private */
    public void c1134129599(Context c) {
        Puzzle p1134129599 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1134129599(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1134129599(editor, this);
            }
        };
        p1134129599.setID(1134129599);
        p1134129599.setName("1134129599");
        p1134129599.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1134129599);
        this.Desktop.RandomlyPlaced(p1134129599);
        p1134129599.setTopEdgeType(EdgeType.Concave);
        p1134129599.setBottomEdgeType(EdgeType.Convex);
        p1134129599.setLeftEdgeType(EdgeType.Concave);
        p1134129599.setRightEdgeType(EdgeType.Concave);
        p1134129599.setTopExactPuzzleID(157144169);
        p1134129599.setBottomExactPuzzleID(1458548836);
        p1134129599.setLeftExactPuzzleID(9891676);
        p1134129599.setRightExactPuzzleID(2032024260);
        p1134129599.getDisplayImage().loadImageFromResource(c, R.drawable.p1134129599h);
        p1134129599.setExactRow(3);
        p1134129599.setExactColumn(4);
        p1134129599.getSize().reset(100.0f, 126.6667f);
        p1134129599.getPositionOffset().reset(-50.0f, -50.0f);
        p1134129599.setIsUseAbsolutePosition(true);
        p1134129599.setIsUseAbsoluteSize(true);
        p1134129599.getImage().setIsUseAbsolutePosition(true);
        p1134129599.getImage().setIsUseAbsoluteSize(true);
        p1134129599.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1134129599.resetPosition();
        getSpiritList().add(p1134129599);
    }

    /* access modifiers changed from: package-private */
    public void c1458548836(Context c) {
        Puzzle p1458548836 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1458548836(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1458548836(editor, this);
            }
        };
        p1458548836.setID(1458548836);
        p1458548836.setName("1458548836");
        p1458548836.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1458548836);
        this.Desktop.RandomlyPlaced(p1458548836);
        p1458548836.setTopEdgeType(EdgeType.Concave);
        p1458548836.setBottomEdgeType(EdgeType.Convex);
        p1458548836.setLeftEdgeType(EdgeType.Concave);
        p1458548836.setRightEdgeType(EdgeType.Convex);
        p1458548836.setTopExactPuzzleID(1134129599);
        p1458548836.setBottomExactPuzzleID(33141792);
        p1458548836.setLeftExactPuzzleID(1165443759);
        p1458548836.setRightExactPuzzleID(768038756);
        p1458548836.getDisplayImage().loadImageFromResource(c, R.drawable.p1458548836h);
        p1458548836.setExactRow(4);
        p1458548836.setExactColumn(4);
        p1458548836.getSize().reset(126.6667f, 126.6667f);
        p1458548836.getPositionOffset().reset(-50.0f, -50.0f);
        p1458548836.setIsUseAbsolutePosition(true);
        p1458548836.setIsUseAbsoluteSize(true);
        p1458548836.getImage().setIsUseAbsolutePosition(true);
        p1458548836.getImage().setIsUseAbsoluteSize(true);
        p1458548836.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1458548836.resetPosition();
        getSpiritList().add(p1458548836);
    }

    /* access modifiers changed from: package-private */
    public void c33141792(Context c) {
        Puzzle p33141792 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load33141792(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save33141792(editor, this);
            }
        };
        p33141792.setID(33141792);
        p33141792.setName("33141792");
        p33141792.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p33141792);
        this.Desktop.RandomlyPlaced(p33141792);
        p33141792.setTopEdgeType(EdgeType.Concave);
        p33141792.setBottomEdgeType(EdgeType.Flat);
        p33141792.setLeftEdgeType(EdgeType.Concave);
        p33141792.setRightEdgeType(EdgeType.Convex);
        p33141792.setTopExactPuzzleID(1458548836);
        p33141792.setBottomExactPuzzleID(-1);
        p33141792.setLeftExactPuzzleID(432650368);
        p33141792.setRightExactPuzzleID(1707850582);
        p33141792.getDisplayImage().loadImageFromResource(c, R.drawable.p33141792h);
        p33141792.setExactRow(5);
        p33141792.setExactColumn(4);
        p33141792.getSize().reset(126.6667f, 100.0f);
        p33141792.getPositionOffset().reset(-50.0f, -50.0f);
        p33141792.setIsUseAbsolutePosition(true);
        p33141792.setIsUseAbsoluteSize(true);
        p33141792.getImage().setIsUseAbsolutePosition(true);
        p33141792.getImage().setIsUseAbsoluteSize(true);
        p33141792.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p33141792.resetPosition();
        getSpiritList().add(p33141792);
    }

    /* access modifiers changed from: package-private */
    public void c43735389(Context c) {
        Puzzle p43735389 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load43735389(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save43735389(editor, this);
            }
        };
        p43735389.setID(43735389);
        p43735389.setName("43735389");
        p43735389.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p43735389);
        this.Desktop.RandomlyPlaced(p43735389);
        p43735389.setTopEdgeType(EdgeType.Flat);
        p43735389.setBottomEdgeType(EdgeType.Convex);
        p43735389.setLeftEdgeType(EdgeType.Concave);
        p43735389.setRightEdgeType(EdgeType.Convex);
        p43735389.setTopExactPuzzleID(-1);
        p43735389.setBottomExactPuzzleID(634519917);
        p43735389.setLeftExactPuzzleID(1000996921);
        p43735389.setRightExactPuzzleID(117419844);
        p43735389.getDisplayImage().loadImageFromResource(c, R.drawable.p43735389h);
        p43735389.setExactRow(0);
        p43735389.setExactColumn(5);
        p43735389.getSize().reset(126.6667f, 126.6667f);
        p43735389.getPositionOffset().reset(-50.0f, -50.0f);
        p43735389.setIsUseAbsolutePosition(true);
        p43735389.setIsUseAbsoluteSize(true);
        p43735389.getImage().setIsUseAbsolutePosition(true);
        p43735389.getImage().setIsUseAbsoluteSize(true);
        p43735389.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p43735389.resetPosition();
        getSpiritList().add(p43735389);
    }

    /* access modifiers changed from: package-private */
    public void c634519917(Context c) {
        Puzzle p634519917 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load634519917(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save634519917(editor, this);
            }
        };
        p634519917.setID(634519917);
        p634519917.setName("634519917");
        p634519917.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p634519917);
        this.Desktop.RandomlyPlaced(p634519917);
        p634519917.setTopEdgeType(EdgeType.Concave);
        p634519917.setBottomEdgeType(EdgeType.Concave);
        p634519917.setLeftEdgeType(EdgeType.Concave);
        p634519917.setRightEdgeType(EdgeType.Concave);
        p634519917.setTopExactPuzzleID(43735389);
        p634519917.setBottomExactPuzzleID(1393730910);
        p634519917.setLeftExactPuzzleID(483679687);
        p634519917.setRightExactPuzzleID(1381295196);
        p634519917.getDisplayImage().loadImageFromResource(c, R.drawable.p634519917h);
        p634519917.setExactRow(1);
        p634519917.setExactColumn(5);
        p634519917.getSize().reset(100.0f, 100.0f);
        p634519917.getPositionOffset().reset(-50.0f, -50.0f);
        p634519917.setIsUseAbsolutePosition(true);
        p634519917.setIsUseAbsoluteSize(true);
        p634519917.getImage().setIsUseAbsolutePosition(true);
        p634519917.getImage().setIsUseAbsoluteSize(true);
        p634519917.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p634519917.resetPosition();
        getSpiritList().add(p634519917);
    }

    /* access modifiers changed from: package-private */
    public void c1393730910(Context c) {
        Puzzle p1393730910 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1393730910(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1393730910(editor, this);
            }
        };
        p1393730910.setID(1393730910);
        p1393730910.setName("1393730910");
        p1393730910.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1393730910);
        this.Desktop.RandomlyPlaced(p1393730910);
        p1393730910.setTopEdgeType(EdgeType.Convex);
        p1393730910.setBottomEdgeType(EdgeType.Convex);
        p1393730910.setLeftEdgeType(EdgeType.Convex);
        p1393730910.setRightEdgeType(EdgeType.Convex);
        p1393730910.setTopExactPuzzleID(634519917);
        p1393730910.setBottomExactPuzzleID(2032024260);
        p1393730910.setLeftExactPuzzleID(157144169);
        p1393730910.setRightExactPuzzleID(1190328125);
        p1393730910.getDisplayImage().loadImageFromResource(c, R.drawable.p1393730910h);
        p1393730910.setExactRow(2);
        p1393730910.setExactColumn(5);
        p1393730910.getSize().reset(153.3333f, 153.3333f);
        p1393730910.getPositionOffset().reset(-76.66666f, -76.66666f);
        p1393730910.setIsUseAbsolutePosition(true);
        p1393730910.setIsUseAbsoluteSize(true);
        p1393730910.getImage().setIsUseAbsolutePosition(true);
        p1393730910.getImage().setIsUseAbsoluteSize(true);
        p1393730910.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1393730910.resetPosition();
        getSpiritList().add(p1393730910);
    }

    /* access modifiers changed from: package-private */
    public void c2032024260(Context c) {
        Puzzle p2032024260 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load2032024260(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save2032024260(editor, this);
            }
        };
        p2032024260.setID(2032024260);
        p2032024260.setName("2032024260");
        p2032024260.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p2032024260);
        this.Desktop.RandomlyPlaced(p2032024260);
        p2032024260.setTopEdgeType(EdgeType.Concave);
        p2032024260.setBottomEdgeType(EdgeType.Concave);
        p2032024260.setLeftEdgeType(EdgeType.Convex);
        p2032024260.setRightEdgeType(EdgeType.Convex);
        p2032024260.setTopExactPuzzleID(1393730910);
        p2032024260.setBottomExactPuzzleID(768038756);
        p2032024260.setLeftExactPuzzleID(1134129599);
        p2032024260.setRightExactPuzzleID(702574111);
        p2032024260.getDisplayImage().loadImageFromResource(c, R.drawable.p2032024260h);
        p2032024260.setExactRow(3);
        p2032024260.setExactColumn(5);
        p2032024260.getSize().reset(153.3333f, 100.0f);
        p2032024260.getPositionOffset().reset(-76.66666f, -50.0f);
        p2032024260.setIsUseAbsolutePosition(true);
        p2032024260.setIsUseAbsoluteSize(true);
        p2032024260.getImage().setIsUseAbsolutePosition(true);
        p2032024260.getImage().setIsUseAbsoluteSize(true);
        p2032024260.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p2032024260.resetPosition();
        getSpiritList().add(p2032024260);
    }

    /* access modifiers changed from: package-private */
    public void c768038756(Context c) {
        Puzzle p768038756 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load768038756(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save768038756(editor, this);
            }
        };
        p768038756.setID(768038756);
        p768038756.setName("768038756");
        p768038756.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p768038756);
        this.Desktop.RandomlyPlaced(p768038756);
        p768038756.setTopEdgeType(EdgeType.Convex);
        p768038756.setBottomEdgeType(EdgeType.Concave);
        p768038756.setLeftEdgeType(EdgeType.Concave);
        p768038756.setRightEdgeType(EdgeType.Concave);
        p768038756.setTopExactPuzzleID(2032024260);
        p768038756.setBottomExactPuzzleID(1707850582);
        p768038756.setLeftExactPuzzleID(1458548836);
        p768038756.setRightExactPuzzleID(635774282);
        p768038756.getDisplayImage().loadImageFromResource(c, R.drawable.p768038756h);
        p768038756.setExactRow(4);
        p768038756.setExactColumn(5);
        p768038756.getSize().reset(100.0f, 126.6667f);
        p768038756.getPositionOffset().reset(-50.0f, -76.66666f);
        p768038756.setIsUseAbsolutePosition(true);
        p768038756.setIsUseAbsoluteSize(true);
        p768038756.getImage().setIsUseAbsolutePosition(true);
        p768038756.getImage().setIsUseAbsoluteSize(true);
        p768038756.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p768038756.resetPosition();
        getSpiritList().add(p768038756);
    }

    /* access modifiers changed from: package-private */
    public void c1707850582(Context c) {
        Puzzle p1707850582 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1707850582(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1707850582(editor, this);
            }
        };
        p1707850582.setID(1707850582);
        p1707850582.setName("1707850582");
        p1707850582.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1707850582);
        this.Desktop.RandomlyPlaced(p1707850582);
        p1707850582.setTopEdgeType(EdgeType.Convex);
        p1707850582.setBottomEdgeType(EdgeType.Flat);
        p1707850582.setLeftEdgeType(EdgeType.Concave);
        p1707850582.setRightEdgeType(EdgeType.Concave);
        p1707850582.setTopExactPuzzleID(768038756);
        p1707850582.setBottomExactPuzzleID(-1);
        p1707850582.setLeftExactPuzzleID(33141792);
        p1707850582.setRightExactPuzzleID(418604698);
        p1707850582.getDisplayImage().loadImageFromResource(c, R.drawable.p1707850582h);
        p1707850582.setExactRow(5);
        p1707850582.setExactColumn(5);
        p1707850582.getSize().reset(100.0f, 126.6667f);
        p1707850582.getPositionOffset().reset(-50.0f, -76.66666f);
        p1707850582.setIsUseAbsolutePosition(true);
        p1707850582.setIsUseAbsoluteSize(true);
        p1707850582.getImage().setIsUseAbsolutePosition(true);
        p1707850582.getImage().setIsUseAbsoluteSize(true);
        p1707850582.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1707850582.resetPosition();
        getSpiritList().add(p1707850582);
    }

    /* access modifiers changed from: package-private */
    public void c117419844(Context c) {
        Puzzle p117419844 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load117419844(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save117419844(editor, this);
            }
        };
        p117419844.setID(117419844);
        p117419844.setName("117419844");
        p117419844.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p117419844);
        this.Desktop.RandomlyPlaced(p117419844);
        p117419844.setTopEdgeType(EdgeType.Flat);
        p117419844.setBottomEdgeType(EdgeType.Convex);
        p117419844.setLeftEdgeType(EdgeType.Concave);
        p117419844.setRightEdgeType(EdgeType.Convex);
        p117419844.setTopExactPuzzleID(-1);
        p117419844.setBottomExactPuzzleID(1381295196);
        p117419844.setLeftExactPuzzleID(43735389);
        p117419844.setRightExactPuzzleID(709322898);
        p117419844.getDisplayImage().loadImageFromResource(c, R.drawable.p117419844h);
        p117419844.setExactRow(0);
        p117419844.setExactColumn(6);
        p117419844.getSize().reset(126.6667f, 126.6667f);
        p117419844.getPositionOffset().reset(-50.0f, -50.0f);
        p117419844.setIsUseAbsolutePosition(true);
        p117419844.setIsUseAbsoluteSize(true);
        p117419844.getImage().setIsUseAbsolutePosition(true);
        p117419844.getImage().setIsUseAbsoluteSize(true);
        p117419844.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p117419844.resetPosition();
        getSpiritList().add(p117419844);
    }

    /* access modifiers changed from: package-private */
    public void c1381295196(Context c) {
        Puzzle p1381295196 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1381295196(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1381295196(editor, this);
            }
        };
        p1381295196.setID(1381295196);
        p1381295196.setName("1381295196");
        p1381295196.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1381295196);
        this.Desktop.RandomlyPlaced(p1381295196);
        p1381295196.setTopEdgeType(EdgeType.Concave);
        p1381295196.setBottomEdgeType(EdgeType.Convex);
        p1381295196.setLeftEdgeType(EdgeType.Convex);
        p1381295196.setRightEdgeType(EdgeType.Concave);
        p1381295196.setTopExactPuzzleID(117419844);
        p1381295196.setBottomExactPuzzleID(1190328125);
        p1381295196.setLeftExactPuzzleID(634519917);
        p1381295196.setRightExactPuzzleID(732856940);
        p1381295196.getDisplayImage().loadImageFromResource(c, R.drawable.p1381295196h);
        p1381295196.setExactRow(1);
        p1381295196.setExactColumn(6);
        p1381295196.getSize().reset(126.6667f, 126.6667f);
        p1381295196.getPositionOffset().reset(-76.66666f, -50.0f);
        p1381295196.setIsUseAbsolutePosition(true);
        p1381295196.setIsUseAbsoluteSize(true);
        p1381295196.getImage().setIsUseAbsolutePosition(true);
        p1381295196.getImage().setIsUseAbsoluteSize(true);
        p1381295196.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1381295196.resetPosition();
        getSpiritList().add(p1381295196);
    }

    /* access modifiers changed from: package-private */
    public void c1190328125(Context c) {
        Puzzle p1190328125 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load1190328125(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save1190328125(editor, this);
            }
        };
        p1190328125.setID(1190328125);
        p1190328125.setName("1190328125");
        p1190328125.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p1190328125);
        this.Desktop.RandomlyPlaced(p1190328125);
        p1190328125.setTopEdgeType(EdgeType.Concave);
        p1190328125.setBottomEdgeType(EdgeType.Concave);
        p1190328125.setLeftEdgeType(EdgeType.Concave);
        p1190328125.setRightEdgeType(EdgeType.Concave);
        p1190328125.setTopExactPuzzleID(1381295196);
        p1190328125.setBottomExactPuzzleID(702574111);
        p1190328125.setLeftExactPuzzleID(1393730910);
        p1190328125.setRightExactPuzzleID(408533478);
        p1190328125.getDisplayImage().loadImageFromResource(c, R.drawable.p1190328125h);
        p1190328125.setExactRow(2);
        p1190328125.setExactColumn(6);
        p1190328125.getSize().reset(100.0f, 100.0f);
        p1190328125.getPositionOffset().reset(-50.0f, -50.0f);
        p1190328125.setIsUseAbsolutePosition(true);
        p1190328125.setIsUseAbsoluteSize(true);
        p1190328125.getImage().setIsUseAbsolutePosition(true);
        p1190328125.getImage().setIsUseAbsoluteSize(true);
        p1190328125.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p1190328125.resetPosition();
        getSpiritList().add(p1190328125);
    }

    /* access modifiers changed from: package-private */
    public void c702574111(Context c) {
        Puzzle p702574111 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load702574111(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save702574111(editor, this);
            }
        };
        p702574111.setID(702574111);
        p702574111.setName("702574111");
        p702574111.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p702574111);
        this.Desktop.RandomlyPlaced(p702574111);
        p702574111.setTopEdgeType(EdgeType.Convex);
        p702574111.setBottomEdgeType(EdgeType.Convex);
        p702574111.setLeftEdgeType(EdgeType.Concave);
        p702574111.setRightEdgeType(EdgeType.Concave);
        p702574111.setTopExactPuzzleID(1190328125);
        p702574111.setBottomExactPuzzleID(635774282);
        p702574111.setLeftExactPuzzleID(2032024260);
        p702574111.setRightExactPuzzleID(437778440);
        p702574111.getDisplayImage().loadImageFromResource(c, R.drawable.p702574111h);
        p702574111.setExactRow(3);
        p702574111.setExactColumn(6);
        p702574111.getSize().reset(100.0f, 153.3333f);
        p702574111.getPositionOffset().reset(-50.0f, -76.66666f);
        p702574111.setIsUseAbsolutePosition(true);
        p702574111.setIsUseAbsoluteSize(true);
        p702574111.getImage().setIsUseAbsolutePosition(true);
        p702574111.getImage().setIsUseAbsoluteSize(true);
        p702574111.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p702574111.resetPosition();
        getSpiritList().add(p702574111);
    }

    /* access modifiers changed from: package-private */
    public void c635774282(Context c) {
        Puzzle p635774282 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load635774282(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save635774282(editor, this);
            }
        };
        p635774282.setID(635774282);
        p635774282.setName("635774282");
        p635774282.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p635774282);
        this.Desktop.RandomlyPlaced(p635774282);
        p635774282.setTopEdgeType(EdgeType.Concave);
        p635774282.setBottomEdgeType(EdgeType.Concave);
        p635774282.setLeftEdgeType(EdgeType.Convex);
        p635774282.setRightEdgeType(EdgeType.Convex);
        p635774282.setTopExactPuzzleID(702574111);
        p635774282.setBottomExactPuzzleID(418604698);
        p635774282.setLeftExactPuzzleID(768038756);
        p635774282.setRightExactPuzzleID(436263762);
        p635774282.getDisplayImage().loadImageFromResource(c, R.drawable.p635774282h);
        p635774282.setExactRow(4);
        p635774282.setExactColumn(6);
        p635774282.getSize().reset(153.3333f, 100.0f);
        p635774282.getPositionOffset().reset(-76.66666f, -50.0f);
        p635774282.setIsUseAbsolutePosition(true);
        p635774282.setIsUseAbsoluteSize(true);
        p635774282.getImage().setIsUseAbsolutePosition(true);
        p635774282.getImage().setIsUseAbsoluteSize(true);
        p635774282.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p635774282.resetPosition();
        getSpiritList().add(p635774282);
    }

    /* access modifiers changed from: package-private */
    public void c418604698(Context c) {
        Puzzle p418604698 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load418604698(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save418604698(editor, this);
            }
        };
        p418604698.setID(418604698);
        p418604698.setName("418604698");
        p418604698.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p418604698);
        this.Desktop.RandomlyPlaced(p418604698);
        p418604698.setTopEdgeType(EdgeType.Convex);
        p418604698.setBottomEdgeType(EdgeType.Flat);
        p418604698.setLeftEdgeType(EdgeType.Convex);
        p418604698.setRightEdgeType(EdgeType.Concave);
        p418604698.setTopExactPuzzleID(635774282);
        p418604698.setBottomExactPuzzleID(-1);
        p418604698.setLeftExactPuzzleID(1707850582);
        p418604698.setRightExactPuzzleID(487737114);
        p418604698.getDisplayImage().loadImageFromResource(c, R.drawable.p418604698h);
        p418604698.setExactRow(5);
        p418604698.setExactColumn(6);
        p418604698.getSize().reset(126.6667f, 126.6667f);
        p418604698.getPositionOffset().reset(-76.66666f, -76.66666f);
        p418604698.setIsUseAbsolutePosition(true);
        p418604698.setIsUseAbsoluteSize(true);
        p418604698.getImage().setIsUseAbsolutePosition(true);
        p418604698.getImage().setIsUseAbsoluteSize(true);
        p418604698.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p418604698.resetPosition();
        getSpiritList().add(p418604698);
    }

    /* access modifiers changed from: package-private */
    public void c709322898(Context c) {
        Puzzle p709322898 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load709322898(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save709322898(editor, this);
            }
        };
        p709322898.setID(709322898);
        p709322898.setName("709322898");
        p709322898.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p709322898);
        this.Desktop.RandomlyPlaced(p709322898);
        p709322898.setTopEdgeType(EdgeType.Flat);
        p709322898.setBottomEdgeType(EdgeType.Convex);
        p709322898.setLeftEdgeType(EdgeType.Concave);
        p709322898.setRightEdgeType(EdgeType.Flat);
        p709322898.setTopExactPuzzleID(-1);
        p709322898.setBottomExactPuzzleID(732856940);
        p709322898.setLeftExactPuzzleID(117419844);
        p709322898.setRightExactPuzzleID(-1);
        p709322898.getDisplayImage().loadImageFromResource(c, R.drawable.p709322898h);
        p709322898.setExactRow(0);
        p709322898.setExactColumn(7);
        p709322898.getSize().reset(100.0f, 126.6667f);
        p709322898.getPositionOffset().reset(-50.0f, -50.0f);
        p709322898.setIsUseAbsolutePosition(true);
        p709322898.setIsUseAbsoluteSize(true);
        p709322898.getImage().setIsUseAbsolutePosition(true);
        p709322898.getImage().setIsUseAbsoluteSize(true);
        p709322898.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p709322898.resetPosition();
        getSpiritList().add(p709322898);
    }

    /* access modifiers changed from: package-private */
    public void c732856940(Context c) {
        Puzzle p732856940 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load732856940(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save732856940(editor, this);
            }
        };
        p732856940.setID(732856940);
        p732856940.setName("732856940");
        p732856940.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p732856940);
        this.Desktop.RandomlyPlaced(p732856940);
        p732856940.setTopEdgeType(EdgeType.Concave);
        p732856940.setBottomEdgeType(EdgeType.Convex);
        p732856940.setLeftEdgeType(EdgeType.Convex);
        p732856940.setRightEdgeType(EdgeType.Flat);
        p732856940.setTopExactPuzzleID(709322898);
        p732856940.setBottomExactPuzzleID(408533478);
        p732856940.setLeftExactPuzzleID(1381295196);
        p732856940.setRightExactPuzzleID(-1);
        p732856940.getDisplayImage().loadImageFromResource(c, R.drawable.p732856940h);
        p732856940.setExactRow(1);
        p732856940.setExactColumn(7);
        p732856940.getSize().reset(126.6667f, 126.6667f);
        p732856940.getPositionOffset().reset(-76.66666f, -50.0f);
        p732856940.setIsUseAbsolutePosition(true);
        p732856940.setIsUseAbsoluteSize(true);
        p732856940.getImage().setIsUseAbsolutePosition(true);
        p732856940.getImage().setIsUseAbsoluteSize(true);
        p732856940.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p732856940.resetPosition();
        getSpiritList().add(p732856940);
    }

    /* access modifiers changed from: package-private */
    public void c408533478(Context c) {
        Puzzle p408533478 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load408533478(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save408533478(editor, this);
            }
        };
        p408533478.setID(408533478);
        p408533478.setName("408533478");
        p408533478.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p408533478);
        this.Desktop.RandomlyPlaced(p408533478);
        p408533478.setTopEdgeType(EdgeType.Concave);
        p408533478.setBottomEdgeType(EdgeType.Concave);
        p408533478.setLeftEdgeType(EdgeType.Convex);
        p408533478.setRightEdgeType(EdgeType.Flat);
        p408533478.setTopExactPuzzleID(732856940);
        p408533478.setBottomExactPuzzleID(437778440);
        p408533478.setLeftExactPuzzleID(1190328125);
        p408533478.setRightExactPuzzleID(-1);
        p408533478.getDisplayImage().loadImageFromResource(c, R.drawable.p408533478h);
        p408533478.setExactRow(2);
        p408533478.setExactColumn(7);
        p408533478.getSize().reset(126.6667f, 100.0f);
        p408533478.getPositionOffset().reset(-76.66666f, -50.0f);
        p408533478.setIsUseAbsolutePosition(true);
        p408533478.setIsUseAbsoluteSize(true);
        p408533478.getImage().setIsUseAbsolutePosition(true);
        p408533478.getImage().setIsUseAbsoluteSize(true);
        p408533478.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p408533478.resetPosition();
        getSpiritList().add(p408533478);
    }

    /* access modifiers changed from: package-private */
    public void c437778440(Context c) {
        Puzzle p437778440 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load437778440(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save437778440(editor, this);
            }
        };
        p437778440.setID(437778440);
        p437778440.setName("437778440");
        p437778440.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p437778440);
        this.Desktop.RandomlyPlaced(p437778440);
        p437778440.setTopEdgeType(EdgeType.Convex);
        p437778440.setBottomEdgeType(EdgeType.Convex);
        p437778440.setLeftEdgeType(EdgeType.Convex);
        p437778440.setRightEdgeType(EdgeType.Flat);
        p437778440.setTopExactPuzzleID(408533478);
        p437778440.setBottomExactPuzzleID(436263762);
        p437778440.setLeftExactPuzzleID(702574111);
        p437778440.setRightExactPuzzleID(-1);
        p437778440.getDisplayImage().loadImageFromResource(c, R.drawable.p437778440h);
        p437778440.setExactRow(3);
        p437778440.setExactColumn(7);
        p437778440.getSize().reset(126.6667f, 153.3333f);
        p437778440.getPositionOffset().reset(-76.66666f, -76.66666f);
        p437778440.setIsUseAbsolutePosition(true);
        p437778440.setIsUseAbsoluteSize(true);
        p437778440.getImage().setIsUseAbsolutePosition(true);
        p437778440.getImage().setIsUseAbsoluteSize(true);
        p437778440.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p437778440.resetPosition();
        getSpiritList().add(p437778440);
    }

    /* access modifiers changed from: package-private */
    public void c436263762(Context c) {
        Puzzle p436263762 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load436263762(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save436263762(editor, this);
            }
        };
        p436263762.setID(436263762);
        p436263762.setName("436263762");
        p436263762.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p436263762);
        this.Desktop.RandomlyPlaced(p436263762);
        p436263762.setTopEdgeType(EdgeType.Concave);
        p436263762.setBottomEdgeType(EdgeType.Concave);
        p436263762.setLeftEdgeType(EdgeType.Concave);
        p436263762.setRightEdgeType(EdgeType.Flat);
        p436263762.setTopExactPuzzleID(437778440);
        p436263762.setBottomExactPuzzleID(487737114);
        p436263762.setLeftExactPuzzleID(635774282);
        p436263762.setRightExactPuzzleID(-1);
        p436263762.getDisplayImage().loadImageFromResource(c, R.drawable.p436263762h);
        p436263762.setExactRow(4);
        p436263762.setExactColumn(7);
        p436263762.getSize().reset(100.0f, 100.0f);
        p436263762.getPositionOffset().reset(-50.0f, -50.0f);
        p436263762.setIsUseAbsolutePosition(true);
        p436263762.setIsUseAbsoluteSize(true);
        p436263762.getImage().setIsUseAbsolutePosition(true);
        p436263762.getImage().setIsUseAbsoluteSize(true);
        p436263762.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p436263762.resetPosition();
        getSpiritList().add(p436263762);
    }

    /* access modifiers changed from: package-private */
    public void c487737114(Context c) {
        Puzzle p487737114 = new Puzzle() {
            public void load(Center c, SharedPreferences sharedPreferences) {
                c.load487737114(sharedPreferences, this);
            }

            public void save(Center c, SharedPreferences.Editor editor) {
                c.save487737114(editor, this);
            }
        };
        p487737114.setID(487737114);
        p487737114.setName("487737114");
        p487737114.setDesktop(this.Desktop);
        this.Desktop.getPuzzleList().add(p487737114);
        this.Desktop.RandomlyPlaced(p487737114);
        p487737114.setTopEdgeType(EdgeType.Convex);
        p487737114.setBottomEdgeType(EdgeType.Flat);
        p487737114.setLeftEdgeType(EdgeType.Convex);
        p487737114.setRightEdgeType(EdgeType.Flat);
        p487737114.setTopExactPuzzleID(436263762);
        p487737114.setBottomExactPuzzleID(-1);
        p487737114.setLeftExactPuzzleID(418604698);
        p487737114.setRightExactPuzzleID(-1);
        p487737114.getDisplayImage().loadImageFromResource(c, R.drawable.p487737114h);
        p487737114.setExactRow(5);
        p487737114.setExactColumn(7);
        p487737114.getSize().reset(126.6667f, 126.6667f);
        p487737114.getPositionOffset().reset(-76.66666f, -76.66666f);
        p487737114.setIsUseAbsolutePosition(true);
        p487737114.setIsUseAbsoluteSize(true);
        p487737114.getImage().setIsUseAbsolutePosition(true);
        p487737114.getImage().setIsUseAbsoluteSize(true);
        p487737114.setRotation(GameMaster.getRandom().nextFloat() * 360.0f);
        p487737114.resetPosition();
        getSpiritList().add(p487737114);
    }
}
