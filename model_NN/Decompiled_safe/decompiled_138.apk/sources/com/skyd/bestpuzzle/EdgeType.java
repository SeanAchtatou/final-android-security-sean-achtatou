package com.skyd.bestpuzzle;

public enum EdgeType {
    Flat,
    Convex,
    Concave;

    public boolean canPiece(EdgeType target) {
        return (this == Flat || target == Flat || this == target) ? false : true;
    }
}
