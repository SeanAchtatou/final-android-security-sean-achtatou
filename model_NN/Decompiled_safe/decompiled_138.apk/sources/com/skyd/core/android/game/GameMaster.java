package com.skyd.core.android.game;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.skyd.core.math.MathEx;
import com.skyd.core.vector.Vector2DF;
import java.util.Random;

public class GameMaster {
    private static Context _Context = null;
    private static Random _Random = new Random();
    private static int _ScreenHeight = 0;
    private static int _ScreenWidth = 0;

    public static Random getRandom() {
        return _Random;
    }

    public static void setRandom(Random value) {
        _Random = value;
    }

    public static void setRandomToDefault() {
        setRandom(new Random());
    }

    public static void init(Context c) {
        Display d = ((WindowManager) c.getSystemService("window")).getDefaultDisplay();
        setScreenHeight(d.getHeight());
        setScreenWidth(d.getWidth());
        setContext(c);
    }

    public static Context getContext() {
        return _Context;
    }

    public static void setContext(Context value) {
        _Context = value;
    }

    public static void setContextToDefault() {
        setContext(null);
    }

    public static int getScreenHeight() {
        return _ScreenHeight;
    }

    public static void setScreenHeight(int value) {
        _ScreenHeight = value;
    }

    public static void setScreenHeightToDefault() {
        setScreenHeight(0);
    }

    public static int getScreenWidth() {
        return _ScreenWidth;
    }

    public static void setScreenWidth(int value) {
        _ScreenWidth = value;
    }

    public static void setScreenWidthToDefault() {
        setScreenWidth(0);
    }

    public static boolean executeVerticalCapsuleHitTest(IGameVerticalCapsuleHitTest obj, Vector2DF point) {
        float t = obj.getVerticalCapsuleHitTestY();
        float b = t + obj.getVerticalCapsuleHitTestHeight();
        float x = obj.getVerticalCapsuleHitTestX();
        float r = obj.getVerticalCapsuleHitTestRadius();
        if (point.getY() >= t) {
            return point.getY() > b ? MathEx.distance(x, b, point.getX(), point.getY()) <= r : Math.abs(point.getX() - x) <= r;
        }
        if (MathEx.distance(x, t, point.getX(), point.getY()) <= r) {
            return true;
        }
        return false;
    }

    public static boolean executeRectHitTest(IGameRectHitTest obj, Vector2DF point) {
        return point.getX() <= obj.getRectHitTestRight() && point.getX() >= obj.getRectHitTestLeft() && point.getY() <= obj.getRectHitTestBottom() && point.getY() >= obj.getRectHitTestTop();
    }

    public static float getVerticalCapsuleLeftBound(IGameVerticalCapsuleHitTest obj, float limitY) {
        float t = obj.getVerticalCapsuleHitTestY();
        float b = t + obj.getVerticalCapsuleHitTestHeight();
        float x = obj.getVerticalCapsuleHitTestX();
        float r = obj.getVerticalCapsuleHitTestRadius();
        if (limitY < t - r || limitY > b + r) {
            return x - r;
        }
        if (limitY < t) {
            return x - ((float) Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (t - limitY), 2.0d)));
        }
        if (limitY <= b || limitY > b + r) {
            return x - r;
        }
        return x - ((float) Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (limitY - b), 2.0d)));
    }

    public static float getVerticalCapsuleRightBound(IGameVerticalCapsuleHitTest obj, float limitY) {
        float t = obj.getVerticalCapsuleHitTestY();
        float b = t + obj.getVerticalCapsuleHitTestHeight();
        float x = obj.getVerticalCapsuleHitTestX();
        float r = obj.getVerticalCapsuleHitTestRadius();
        if (limitY < t - r || limitY > b + r) {
            return x + r;
        }
        if (limitY < t) {
            return ((float) Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (t - limitY), 2.0d))) + x;
        }
        if (limitY <= b || limitY > b + r) {
            return x + r;
        }
        return ((float) Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (limitY - b), 2.0d))) + x;
    }

    public static float getVerticalCapsuleTopBound(IGameVerticalCapsuleHitTest obj, float limitX) {
        float t = obj.getVerticalCapsuleHitTestY();
        float x = obj.getVerticalCapsuleHitTestX();
        float r = obj.getVerticalCapsuleHitTestRadius();
        if (limitX < x - r || limitX > x + r) {
            return t - r;
        }
        if (limitX < x) {
            return (float) (((double) t) - Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (x - limitX), 2.0d)));
        }
        if (limitX > x) {
            return (float) (((double) t) - Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (limitX - x), 2.0d)));
        }
        return t - r;
    }

    public static float getVerticalCapsuleBottomBound(IGameVerticalCapsuleHitTest obj, float limitX) {
        float b = obj.getVerticalCapsuleHitTestY() + obj.getVerticalCapsuleHitTestHeight();
        float x = obj.getVerticalCapsuleHitTestX();
        float r = obj.getVerticalCapsuleHitTestRadius();
        if (limitX < x - r || limitX > x + r) {
            return b + r;
        }
        if (limitX < x) {
            return (float) (((double) b) + Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (x - limitX), 2.0d)));
        }
        if (limitX > x) {
            return (float) (((double) b) + Math.sqrt(Math.pow((double) r, 2.0d) - Math.pow((double) (limitX - x), 2.0d)));
        }
        return b + r;
    }

    public static void log(Object sender, Object msg) {
        Log.d("SkyD_LOG", String.valueOf(sender.getClass().getName()) + " : " + msg);
    }

    public static Resources getResources() {
        return getContext().getResources();
    }
}
