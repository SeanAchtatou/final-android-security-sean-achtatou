package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Rect;

public abstract class GameLeafObject extends GameObject {
    public GameObject getDisplayContentChild() {
        return null;
    }

    public void updateChilds() {
    }

    /* access modifiers changed from: protected */
    public void drawChilds(Canvas c, Rect drawArea) {
    }
}
