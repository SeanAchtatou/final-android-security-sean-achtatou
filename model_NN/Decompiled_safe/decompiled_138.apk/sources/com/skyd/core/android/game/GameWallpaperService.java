package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.skyd.core.vector.Vector2DF;
import java.util.Date;

public abstract class GameWallpaperService extends WallpaperService implements IGameDisplayBase {
    private int _FPS = 60;
    private int _FPSDrawColor = -65536;
    private Vector2DF _FPSDrawPosition = new Vector2DF(10.0f, 10.0f);
    private int _FPSDrawTextSize = 22;
    /* access modifiers changed from: private */
    public Boolean _IsFirstShow = true;
    private Boolean _IsInitialized = false;
    /* access modifiers changed from: private */
    public boolean _IsShowFPS = false;
    /* access modifiers changed from: private */
    public boolean _Runnable = false;
    int fps;
    int fpscount = 0;
    long fpsstart;
    int frameRate;
    /* access modifiers changed from: private */
    public final Handler handler = new Handler();

    /* access modifiers changed from: protected */
    public abstract void PaintInThread(Canvas canvas);

    /* access modifiers changed from: protected */
    public abstract void UpdateInThread();

    public abstract void init(Boolean bool);

    public WallpaperService.Engine onCreateEngine() {
        return new GameEngine();
    }

    public Boolean getIsFirstShow() {
        return this._IsFirstShow;
    }

    public boolean getRunnable() {
        return this._Runnable;
    }

    public void setRunnable(boolean value) {
        this._Runnable = value;
    }

    public void setRunnableToDefault() {
        setRunnable(false);
    }

    public int getFPS() {
        return this._FPS;
    }

    public void setFPS(int value) {
        this._FPS = value;
        updateFrameRate(value);
    }

    public void setFPSToDefault() {
        setFPS(60);
    }

    public Boolean getIsInitialized() {
        return this._IsInitialized;
    }

    public void setIsInitialized(Boolean value) {
        this._IsInitialized = value;
    }

    public void setIsInitializedToDefault() {
        setIsInitialized(false);
    }

    /* access modifiers changed from: protected */
    public void updateFrameRate(int FPS) {
        this.frameRate = 1000 / FPS;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: protected */
    public long calculateSleepTime(Date beginTime) {
        return Math.max(0L, ((long) this.frameRate) - (new Date().getTime() - beginTime.getTime()));
    }

    class GameEngine extends WallpaperService.Engine {
        private final Runnable drawrunnable = new Runnable() {
            public void run() {
                GameEngine.this.draw();
            }
        };

        GameEngine() {
            super(GameWallpaperService.this);
        }

        /* access modifiers changed from: protected */
        public void draw() {
            SurfaceHolder h = getSurfaceHolder();
            Canvas c = null;
            GameWallpaperService.this.handler.removeCallbacks(this.drawrunnable);
            Date beginTime = new Date();
            try {
                GameWallpaperService.this.UpdateInThread();
                c = h.lockCanvas();
                GameWallpaperService.this.PaintInThread(c);
                if (GameWallpaperService.this._IsShowFPS) {
                    GameWallpaperService.this.processingFPS(c, beginTime);
                }
                if (GameWallpaperService.this._Runnable) {
                    GameWallpaperService.this.handler.postDelayed(this.drawrunnable, GameWallpaperService.this.calculateSleepTime(beginTime));
                }
            } finally {
                if (c != null) {
                    h.unlockCanvasAndPost(c);
                }
            }
        }

        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);
            setTouchEventsEnabled(true);
        }

        public void onDestroy() {
            super.onDestroy();
            GameWallpaperService.this.handler.removeCallbacks(this.drawrunnable);
        }

        public void onSurfaceCreated(SurfaceHolder holder) {
            super.onSurfaceCreated(holder);
            GameWallpaperService.this.init(GameWallpaperService.this._IsFirstShow);
            GameWallpaperService.this._IsFirstShow = false;
            GameWallpaperService.this.setIsInitialized(true);
            GameWallpaperService.this.onSurfaceCreated(holder);
        }

        public void onSurfaceDestroyed(SurfaceHolder holder) {
            GameWallpaperService.this.onSurfaceDestroyed(holder);
            super.onSurfaceDestroyed(holder);
        }

        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);
            Log.d(getClass().toString(), "onVisibilityChanged " + visible);
            GameWallpaperService.this.setRunnable(visible);
            if (visible) {
                draw();
            } else {
                GameWallpaperService.this.handler.removeCallbacks(this.drawrunnable);
            }
            GameWallpaperService.this.onVisibilityChanged(visible);
        }

        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);
            GameWallpaperService.this.onSurfaceChanged(holder, format, width, height);
        }

        public Bundle onCommand(String action, int x, int y, int z, Bundle extras, boolean resultRequested) {
            Log.d("onCommand", action);
            return GameWallpaperService.this.onCommand(action, x, y, z, extras, resultRequested);
        }

        public void onDesiredSizeChanged(int desiredWidth, int desiredHeight) {
            GameWallpaperService.this.onDesiredSizeChanged(desiredWidth, desiredHeight);
            Log.d("onDesiredSizeChanged", String.valueOf(desiredWidth) + " " + desiredHeight);
            super.onDesiredSizeChanged(desiredWidth, desiredHeight);
        }

        public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
            Log.d("onOffsetsChanged", String.valueOf(xOffset) + " " + yOffset);
            GameWallpaperService.this.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset);
        }

        public void onTouchEvent(MotionEvent event) {
            Log.d("onTouchEvent", new StringBuilder(String.valueOf(event.getSize())).toString());
            GameWallpaperService.this.onTouchEvent(event);
            super.onTouchEvent(event);
        }
    }

    public Bundle onCommand(String action, int x, int y, int z, Bundle extras, boolean resultRequested) {
        return null;
    }

    public void onDesiredSizeChanged(int desiredWidth, int desiredHeight) {
    }

    public void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
    }

    public void onTouchEvent(MotionEvent event) {
    }

    public void onVisibilityChanged(boolean visible) {
    }

    public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void onSurfaceCreated(SurfaceHolder holder) {
    }

    public void onSurfaceDestroyed(SurfaceHolder holder) {
    }

    public void processingFPS(Canvas c, Date beginTime) {
        if (this.fpscount == 0) {
            this.fpsstart = beginTime.getTime();
        }
        this.fpscount++;
        if (beginTime.getTime() - this.fpsstart > 1000) {
            this.fps = this.fpscount - 1;
            this.fpscount = 0;
        }
        Paint paint = new Paint();
        paint.setColor(getFPSDrawColor());
        paint.setTextSize((float) getFPSDrawTextSize());
        c.drawText("FPS:" + this.fps, getFPSDrawPosition().getX(), getFPSDrawPosition().getY(), paint);
    }

    public boolean getIsShowFPS() {
        return this._IsShowFPS;
    }

    private void setIsShowFPS(boolean value) {
        this._IsShowFPS = value;
    }

    public void showFPS(int x, int y, int size, int color) {
        setFPSDrawTextSize(size);
        setFPSDrawColor(color);
        showFPS(x, y);
    }

    public void showFPS(int x, int y) {
        getFPSDrawPosition().reset((float) x, (float) y);
        showFPS();
    }

    public void showFPS() {
        setIsShowFPS(true);
    }

    public void hideFPS() {
        setIsShowFPS(false);
    }

    public Vector2DF getFPSDrawPosition() {
        return this._FPSDrawPosition;
    }

    public int getFPSDrawTextSize() {
        return this._FPSDrawTextSize;
    }

    public void setFPSDrawTextSize(int value) {
        this._FPSDrawTextSize = value;
    }

    public void setFPSDrawTextSizeToDefault() {
        setFPSDrawTextSize(22);
    }

    public int getFPSDrawColor() {
        return this._FPSDrawColor;
    }

    public void setFPSDrawColor(int value) {
        this._FPSDrawColor = value;
    }

    public void setFPSDrawColorToDefault() {
        setFPSDrawColor(-65536);
    }
}
