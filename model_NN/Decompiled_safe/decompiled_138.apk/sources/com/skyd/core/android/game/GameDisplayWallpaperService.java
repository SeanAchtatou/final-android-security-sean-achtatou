package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class GameDisplayWallpaperService extends GameWallpaperService implements IGameDisplayArea {
    private int _DisplaySceneIndex = 0;
    private ArrayList<OnDrawingBeginListener> _DrawingBeginListenerList = null;
    private ArrayList<OnDrawingEndListener> _DrawingEndListenerList = null;
    private ArrayList<GameScene> _SceneList = new ArrayList<>();

    public interface OnDrawingBeginListener {
        void OnEvent(Object obj, Canvas canvas, Rect rect);
    }

    public interface OnDrawingEndListener {
        void OnEvent(Object obj, Canvas canvas, Rect rect);
    }

    /* access modifiers changed from: protected */
    public ArrayList<GameScene> getSceneList() {
        return this._SceneList;
    }

    /* access modifiers changed from: protected */
    public void setSceneList(ArrayList<GameScene> value) {
        this._SceneList = value;
    }

    /* access modifiers changed from: protected */
    public void setSceneListToDefault() {
        setSceneList(new ArrayList());
    }

    /* access modifiers changed from: protected */
    public void PaintInThread(Canvas c) {
        draw(c);
    }

    /* access modifiers changed from: protected */
    public void UpdateInThread() {
        update();
    }

    public void addScene(GameScene scene) throws Exception {
        if (scene.getParentDisplayArea() == null || scene.getParentDisplayArea() == this) {
            Iterator<GameScene> it = this._SceneList.iterator();
            while (it.hasNext()) {
                if (it.next().getName() == scene.getName()) {
                    throw new Exception("同名场景已存在于此显示区域！");
                }
            }
            scene.setParentDisplayArea(this);
            this._SceneList.add(scene);
            scene.updateDisplayAreaSize();
            scene.updateDisplayAreaScaleReferenceValue();
            return;
        }
        throw new Exception("该场景已存在于另一显示区域中！");
    }

    public void removeScene(GameScene scene) throws Exception {
        if (this._SceneList.contains(scene)) {
            scene.setParentDisplayAreaToDefault();
            this._SceneList.remove(scene);
            return;
        }
        throw new Exception("该场景并未存在于此显示区域中！");
    }

    public int getDisplaySceneIndex() {
        return this._DisplaySceneIndex;
    }

    public void setDisplaySceneIndex(int value) {
        this._DisplaySceneIndex = value;
    }

    public void setDisplaySceneIndexToDefault() {
        setDisplaySceneIndex(0);
    }

    public void setDisplaySceneName(String name) throws Exception {
        for (int i = 0; i < this._SceneList.size(); i++) {
            if (this._SceneList.get(i).getName() == name) {
                this._DisplaySceneIndex = i;
                return;
            }
        }
        throw new Exception("未找到名为" + name + "的场景");
    }

    public String getDisplaySceneName() {
        return getDisplayScene().getName();
    }

    public GameScene getDisplayScene() {
        return this._SceneList.get(this._DisplaySceneIndex);
    }

    public GameScene getSceneByName(String name) {
        Iterator<GameScene> it = this._SceneList.iterator();
        while (it.hasNext()) {
            GameScene f = it.next();
            if (f.getName() == name) {
                return f;
            }
        }
        return null;
    }

    public void update() {
        if (this._SceneList.size() > 0) {
            getDisplayScene().update();
        }
    }

    public void draw(Canvas c) {
        if (c != null) {
            onDrawingBegin(c, null);
            if (this._SceneList.size() > 0) {
                getDisplayScene().draw(c, null);
            }
            onDrawingEnd(c, null);
        }
    }

    public boolean addOnDrawingBeginListener(OnDrawingBeginListener listener) {
        if (this._DrawingBeginListenerList == null) {
            this._DrawingBeginListenerList = new ArrayList<>();
        } else if (this._DrawingBeginListenerList.contains(listener)) {
            return false;
        }
        this._DrawingBeginListenerList.add(listener);
        return true;
    }

    public boolean removeOnDrawingBeginListener(OnDrawingBeginListener listener) {
        if (this._DrawingBeginListenerList == null || !this._DrawingBeginListenerList.contains(listener)) {
            return false;
        }
        this._DrawingBeginListenerList.remove(listener);
        return true;
    }

    public void clearOnDrawingBeginListeners() {
        if (this._DrawingBeginListenerList != null) {
            this._DrawingBeginListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onDrawingBegin(Canvas c, Rect displayRect) {
        if (this._DrawingBeginListenerList != null) {
            Iterator<OnDrawingBeginListener> it = this._DrawingBeginListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnEvent(this, c, displayRect);
            }
        }
    }

    public boolean addOnDrawingEndListener(OnDrawingEndListener listener) {
        if (this._DrawingEndListenerList == null) {
            this._DrawingEndListenerList = new ArrayList<>();
        } else if (this._DrawingEndListenerList.contains(listener)) {
            return false;
        }
        this._DrawingEndListenerList.add(listener);
        return true;
    }

    public boolean removeOnDrawingEndListener(OnDrawingEndListener listener) {
        if (this._DrawingEndListenerList == null || !this._DrawingEndListenerList.contains(listener)) {
            return false;
        }
        this._DrawingEndListenerList.remove(listener);
        return true;
    }

    public void clearOnDrawingEndListeners() {
        if (this._DrawingEndListenerList != null) {
            this._DrawingEndListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onDrawingEnd(Canvas c, Rect displayRect) {
        if (this._DrawingEndListenerList != null) {
            Iterator<OnDrawingEndListener> it = this._DrawingEndListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnEvent(this, c, displayRect);
            }
        }
    }

    public int getHeight() {
        return GameMaster.getScreenHeight();
    }

    public int getWidth() {
        return GameMaster.getScreenWidth();
    }
}
