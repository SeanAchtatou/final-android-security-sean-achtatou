package com.skyd.core.android.game.crosswisewar;

import com.skyd.core.game.crosswisewar.ICooling;
import com.skyd.core.game.crosswisewar.IEntity;
import com.skyd.core.game.crosswisewar.IObj;
import com.skyd.core.game.crosswisewar.IWarrior;
import java.util.Iterator;

public abstract class Warrior extends Entity implements IWarrior {
    private int _Speed = 1;

    public boolean move() {
        if (getEncounterEntity() != null && !getIsCanMoveAttac()) {
            return false;
        }
        float pos = getPositionInScene() + ((float) (getNation().getIsRighteous() ? getSpeed() : 0 - getSpeed()));
        Iterator<IObj> it = getParentScene().getChildrenList().iterator();
        while (it.hasNext()) {
            IObj f = it.next();
            if ((f instanceof IEntity) && !f.equals(this)) {
                IEntity t = (IEntity) f;
                if (hitTest(pos, getOccupyWidth(), t.getPositionInScene(), t.getOccupyWidth())) {
                    return false;
                }
            }
        }
        setPositionInScene(pos);
        return true;
    }

    /* access modifiers changed from: protected */
    public void updateSelf() {
        super.updateSelf();
        move();
    }

    public static boolean hitTest(float position1, int width1, float position2, int width2) {
        return Math.abs(position1 - position2) <= ((float) Math.max(width1, width2));
    }

    public ICooling getCooling() {
        return CoolingMaster.getCooling(getClass());
    }

    public int getSpeed() {
        return this._Speed;
    }

    public void setSpeed(int value) {
        this._Speed = value;
    }

    public void setSpeedToDefault() {
        setSpeed(1);
    }
}
