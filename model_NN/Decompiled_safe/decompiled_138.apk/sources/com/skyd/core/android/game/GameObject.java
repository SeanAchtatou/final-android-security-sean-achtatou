package com.skyd.core.android.game;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import com.skyd.core.math.MathEx;
import com.skyd.core.vector.Vector2DF;
import com.skyd.core.vector.VectorRect2DF;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class GameObject {
    private ArrayList<OnDrawingListener> _DrawingListenerList = null;
    private ArrayList<OnDrawnListener> _DrawnListenerList = null;
    private boolean _IsStretchContent = false;
    private boolean _IsUseAbsolutePosition = false;
    private boolean _IsUseAbsoluteSize = false;
    private GameMotionGroup _MotionGroup = new GameMotionGroup();
    private float _Opacity = 1.0f;
    private GameObject _Parent = null;
    private Vector2DF _Position = new Vector2DF(0.0f, 0.0f);
    private Vector2DF _PositionOffset = new Vector2DF(0.0f, 0.0f);
    private float _Rotation = 0.0f;
    private Vector2DF _Scale = new Vector2DF(1.0f, 1.0f);
    private Vector2DF _Size = new Vector2DF(-1.0f, -1.0f);
    private ArrayList<OnUpdatedListener> _UpdatedListenerList = null;
    private ArrayList<OnUpdatingListener> _UpdatingListenerList = null;
    private boolean _Visible = true;

    public interface OnDrawingListener {
        boolean OnDrawingEvent(Object obj, Canvas canvas, Rect rect);
    }

    public interface OnDrawnListener {
        void OnDrawnEvent(Object obj, Canvas canvas, Rect rect);
    }

    public interface OnUpdatedListener {
        void OnUpdatedEvent(Object obj);
    }

    public interface OnUpdatingListener {
        boolean OnUpdatingEvent(Object obj);
    }

    /* access modifiers changed from: protected */
    public abstract void drawChilds(Canvas canvas, Rect rect);

    /* access modifiers changed from: protected */
    public abstract void drawSelf(Canvas canvas, Rect rect);

    public abstract GameObject getDisplayContentChild();

    /* access modifiers changed from: protected */
    public abstract void updateChilds();

    /* access modifiers changed from: protected */
    public abstract void updateSelf();

    public Vector2DF getPosition() {
        return this._Position;
    }

    public void setPosition(Vector2DF value) {
        this._Position = value;
    }

    public void setPositionToDefault() {
        setPosition(new Vector2DF(0.0f, 0.0f));
    }

    public Vector2DF getPositionOffset() {
        return this._PositionOffset;
    }

    public void setPositionOffset(Vector2DF value) {
        this._PositionOffset = value;
    }

    public void setPositionOffsetToDefault() {
        setPositionOffset(new Vector2DF(0.0f, 0.0f));
    }

    public Vector2DF getSize() {
        return this._Size;
    }

    public void setSize(Vector2DF value) {
        this._Size = value;
    }

    public void setSizeToDefault() {
        setSize(new Vector2DF(-1.0f, -1.0f));
    }

    public boolean getIsStretchContent() {
        return this._IsStretchContent;
    }

    public void setIsStretchContent(boolean value) {
        this._IsStretchContent = value;
    }

    public void setIsStretchContentToDefault() {
        setIsStretchContent(false);
    }

    public boolean getIsUseAbsoluteSize() {
        return this._IsUseAbsoluteSize;
    }

    public void setIsUseAbsoluteSize(boolean value) {
        this._IsUseAbsoluteSize = value;
    }

    public void setIsUseAbsoluteSizeToDefault() {
        setIsUseAbsoluteSize(false);
    }

    public boolean getIsUseAbsolutePosition() {
        return this._IsUseAbsolutePosition;
    }

    public void setIsUseAbsolutePosition(boolean value) {
        this._IsUseAbsolutePosition = value;
    }

    public void setIsUseAbsolutePositionToDefault() {
        setIsUseAbsolutePosition(false);
    }

    public Vector2DF getScale() {
        return this._Scale;
    }

    public void setScale(Vector2DF value) {
        this._Scale = value;
    }

    public void setScaleToDefault() {
        setScale(new Vector2DF(1.0f, 1.0f));
    }

    public float getRotation() {
        return this._Rotation;
    }

    public void setRotation(float value) {
        this._Rotation = value;
    }

    public void setRotationToDefault() {
        setRotation(0.0f);
    }

    public float getOpacity() {
        return this._Opacity;
    }

    public void setOpacity(float value) {
        this._Opacity = value;
        if (this._Opacity > 1.0f) {
            this._Opacity = 1.0f;
        } else if (this._Opacity < 0.0f) {
            this._Opacity = 0.0f;
        }
    }

    public void setOpacityToDefault() {
        setOpacity(1.0f);
    }

    public boolean getDisplayVisible() {
        if (getParent() == null) {
            return this._Visible;
        }
        return this._Visible && getParent().getDisplayVisible();
    }

    public boolean getVisibleOriginalValue() {
        return this._Visible;
    }

    /* access modifiers changed from: protected */
    public void setVisible(boolean value) {
        this._Visible = value;
    }

    public void show() {
        setVisible(true);
        if (getDisplayContentChild() != null) {
            getDisplayContentChild().show();
        }
    }

    public void hide() {
        setVisible(false);
        if (getDisplayContentChild() != null) {
            getDisplayContentChild().hide();
        }
    }

    public GameObject getParent() {
        return this._Parent;
    }

    public void setParent(GameObject value) throws GameException {
        this._Parent = value;
    }

    public void setParentToDefault() throws GameException {
        setParent(null);
    }

    public Vector2DF getDisplayPosition(Matrix m) {
        return getDisplayAngularPointA(getMatrix());
    }

    public Vector2DF getDisplayPosition() {
        return getDisplayPosition(getMatrix());
    }

    public Vector2DF getDisplaySize(Matrix m) {
        Vector2DF a = getDisplayAngularPointA(m);
        return new Vector2DF(Math.abs(getDisplayAngularPointB(m).minus(a).getLength()), Math.abs(getDisplayAngularPointB(m).minus(a).getLength()));
    }

    public Vector2DF getDisplaySize() {
        return getDisplaySize(getMatrix());
    }

    public Rect getDisplayRect(Matrix m) {
        Vector2DF a = getDisplayAngularPointA(m);
        Vector2DF b = getDisplayAngularPointB(m);
        Vector2DF c = getDisplayAngularPointC(m);
        Vector2DF d = getDisplayAngularPointD(m);
        float left = MathEx.min(Float.valueOf(a.getX()), Float.valueOf(b.getX()), Float.valueOf(c.getX()), Float.valueOf(d.getX())).floatValue();
        float right = MathEx.max(Float.valueOf(a.getX()), Float.valueOf(b.getX()), Float.valueOf(c.getX()), Float.valueOf(d.getX())).floatValue();
        return new Rect((int) left, (int) MathEx.min(Float.valueOf(a.getY()), Float.valueOf(b.getY()), Float.valueOf(c.getY()), Float.valueOf(d.getY())).floatValue(), (int) right, (int) MathEx.max(Float.valueOf(a.getY()), Float.valueOf(b.getY()), Float.valueOf(c.getY()), Float.valueOf(d.getY())).floatValue());
    }

    public Rect getDisplayRect() {
        return getDisplayRect(getMatrix());
    }

    public RectF getDisplayRectF(Matrix m) {
        Vector2DF a = getDisplayAngularPointA(m);
        Vector2DF b = getDisplayAngularPointB(m);
        Vector2DF c = getDisplayAngularPointC(m);
        Vector2DF d = getDisplayAngularPointD(m);
        Float left = MathEx.min(Float.valueOf(a.getX()), Float.valueOf(b.getX()), Float.valueOf(c.getX()), Float.valueOf(d.getX()));
        Float right = MathEx.max(Float.valueOf(a.getX()), Float.valueOf(b.getX()), Float.valueOf(c.getX()), Float.valueOf(d.getX()));
        return new RectF(left.floatValue(), MathEx.min(Float.valueOf(a.getY()), Float.valueOf(b.getY()), Float.valueOf(c.getY()), Float.valueOf(d.getY())).floatValue(), right.floatValue(), MathEx.max(Float.valueOf(a.getY()), Float.valueOf(b.getY()), Float.valueOf(c.getY()), Float.valueOf(d.getY())).floatValue());
    }

    public RectF getDisplayRectF() {
        return getDisplayRectF(getMatrix());
    }

    public VectorRect2DF getDisplayVectorRect2DF(Matrix m) {
        Vector2DF a = getDisplayAngularPointA(m);
        Vector2DF b = getDisplayAngularPointB(m);
        Vector2DF c = getDisplayAngularPointC(m);
        Vector2DF d = getDisplayAngularPointD(m);
        Float left = MathEx.min(Float.valueOf(a.getX()), Float.valueOf(b.getX()), Float.valueOf(c.getX()), Float.valueOf(d.getX()));
        Float right = MathEx.max(Float.valueOf(a.getX()), Float.valueOf(b.getX()), Float.valueOf(c.getX()), Float.valueOf(d.getX()));
        return new VectorRect2DF(left.floatValue(), MathEx.min(Float.valueOf(a.getY()), Float.valueOf(b.getY()), Float.valueOf(c.getY()), Float.valueOf(d.getY())).floatValue(), right.floatValue(), MathEx.max(Float.valueOf(a.getY()), Float.valueOf(b.getY()), Float.valueOf(c.getY()), Float.valueOf(d.getY())).floatValue());
    }

    public VectorRect2DF getDisplayVectorRect2DF() {
        return getDisplayVectorRect2DF(getMatrix());
    }

    public float getDisplayRotation(Matrix m) {
        return getDisplayAngularPointB(m).minus(getDisplayAngularPointA(m)).getAngle();
    }

    public float getDisplayRotation() {
        return getDisplayRotation(getMatrix());
    }

    public float getDisplayOpacity() {
        if (getParent() != null) {
            return getParent().getDisplayOpacity() * getOpacity();
        }
        return getOpacity();
    }

    public Vector2DF getDisplayScale(Matrix m) {
        Vector2DF v = getDisplaySize(m);
        return new Vector2DF(v.getX() / getDrawSize().getX(), v.getY() / getDrawSize().getY());
    }

    public Vector2DF getDisplayScale() {
        return getDisplayScale(getMatrix());
    }

    public Vector2DF getDisplayAreaFixScaleForPosition() {
        if (this._IsUseAbsolutePosition) {
            return new Vector2DF(1.0f, 1.0f);
        }
        return getRoot().getDisplayAreaFixScaleReferenceValue();
    }

    public Vector2DF getDisplayAreaFixScaleForSize() {
        if (this._IsUseAbsoluteSize) {
            return new Vector2DF(1.0f, 1.0f);
        }
        return getRoot().getDisplayAreaFixScaleReferenceValue();
    }

    public void draw(Canvas c, Rect drawArea) {
        if (getDisplayVisible()) {
            c.save();
            operateCanvas(c);
            if (onDrawing(c, drawArea)) {
                drawChilds(c, drawArea);
                drawSelf(c, drawArea);
            }
            onDrawn(c, drawArea);
            c.restore();
            return;
        }
        onSkipDraw(c, drawArea);
    }

    /* access modifiers changed from: protected */
    public void onSkipDraw(Canvas c, Rect drawArea) {
    }

    public Vector2DF getDrawSize() {
        Vector2DF sc = getDisplayAreaFixScaleForSize();
        Vector2DF v = new Vector2DF(getDrawWidth(), getDrawHeight());
        v.scale(sc.getX(), sc.getY());
        return v;
    }

    public float getDrawWidth() {
        float v = getSize().getX();
        if (v < 0.0f) {
            return getDisplayContentChild().getDrawWidth();
        }
        return v;
    }

    public float getDrawHeight() {
        float v = getSize().getY();
        if (v < 0.0f) {
            return getDisplayContentChild().getDrawHeight();
        }
        return v;
    }

    public Vector2DF getDrawPosition() {
        Vector2DF sc = getDisplayAreaFixScaleForPosition();
        if (getParent() instanceof GameRootObject) {
            return getRoot().transitionPosition(getPosition()).scale(sc.getX(), sc.getY());
        }
        return getPosition().scaleNew(sc.getX(), sc.getY());
    }

    public Vector2DF getDrawPositionOffset() {
        Vector2DF sc = getDisplayAreaFixScaleForPosition();
        return getPositionOffset().scaleNew(sc.getX(), sc.getY());
    }

    public RectF getDrawRectF() {
        Vector2DF s = getDrawPosition().plusNew(getDrawPositionOffset());
        Vector2DF e = s.plusNew(getDrawSize());
        return new RectF(s.getX(), s.getY(), e.getX(), e.getY());
    }

    public RectF getRectF() {
        Vector2DF s = getPosition().plusNew(getPositionOffset());
        Vector2DF e = s.plusNew(getSize());
        return new RectF(s.getX(), s.getY(), e.getX(), e.getY());
    }

    /* access modifiers changed from: protected */
    public void operateCanvas(Canvas c) {
        Vector2DF p = getDrawPosition();
        Vector2DF o = getDrawPositionOffset();
        c.translate(p.getX(), p.getY());
        clipCanvas(c, o);
        c.rotate(getRotation());
        Vector2DF sc = getScale();
        c.scale(sc.getX(), sc.getY());
        operateCanvasForParentAbsoluteSize(c);
        c.translate(o.getX(), o.getY());
    }

    public Matrix getMatrix() {
        GameObject pa = getParent();
        Matrix m = pa == null ? new Matrix() : pa.getMatrix();
        Vector2DF p = getDrawPosition();
        Vector2DF o = getDrawPositionOffset();
        m.preTranslate(p.getX(), p.getY());
        m.preRotate(getRotation());
        Vector2DF sc = getScale();
        m.preScale(sc.getX(), sc.getY());
        operateMatrixForParentAbsoluteSize(m);
        m.preTranslate(o.getX(), o.getY());
        return m;
    }

    /* access modifiers changed from: protected */
    public void clipCanvas(Canvas c, Vector2DF offset) {
    }

    /* access modifiers changed from: protected */
    public void operateCanvasForParentAbsoluteSize(Canvas c) {
        Vector2DF s = getParentAbsoluteSizeScale();
        c.scale(s.getX(), s.getY());
    }

    /* access modifiers changed from: protected */
    public void operateMatrixForParentAbsoluteSize(Matrix m) {
        Vector2DF s = getParentAbsoluteSizeScale();
        m.preScale(s.getX(), s.getY());
    }

    /* access modifiers changed from: protected */
    public Vector2DF getParentAbsoluteSizeScale() {
        if (getParent() == null || (getParent() instanceof GameSpiritGroup)) {
            return new Vector2DF(1.0f, 1.0f);
        }
        float xs = getParent().getDrawWidth() / getDrawWidth();
        float ys = getParent().getDrawHeight() / getDrawHeight();
        if (getParent().getIsStretchContent()) {
            return new Vector2DF(xs, ys);
        }
        float as = Math.min(xs, ys);
        return new Vector2DF(as, as);
    }

    /* access modifiers changed from: protected */
    public final void drawChild(GameObject child, Canvas c, Rect drawArea) {
        if (onDrawingChild(child, c, drawArea)) {
            child.draw(c, drawArea);
        }
        onDrawingChild(child, c, drawArea);
    }

    /* access modifiers changed from: protected */
    public boolean onDrawingChild(GameObject child, Canvas c, Rect drawArea) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDrawnChild(GameObject child, Canvas c, Rect drawArea) {
    }

    public void update() {
        if (onUpdating()) {
            updateSelf();
            getMotionGroup().update(this);
            updateChilds();
        }
        onUpdated();
    }

    public boolean addOnUpdatingListener(OnUpdatingListener listener) {
        if (this._UpdatingListenerList == null) {
            this._UpdatingListenerList = new ArrayList<>();
        } else if (this._UpdatingListenerList.contains(listener)) {
            return false;
        }
        this._UpdatingListenerList.add(listener);
        return true;
    }

    public boolean removeOnUpdatingListener(OnUpdatingListener listener) {
        if (this._UpdatingListenerList == null || !this._UpdatingListenerList.contains(listener)) {
            return false;
        }
        this._UpdatingListenerList.remove(listener);
        return true;
    }

    public void clearOnUpdatingListeners() {
        if (this._UpdatingListenerList != null) {
            this._UpdatingListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onUpdating() {
        if (this._UpdatingListenerList != null) {
            Iterator<OnUpdatingListener> it = this._UpdatingListenerList.iterator();
            while (it.hasNext()) {
                if (!it.next().OnUpdatingEvent(this)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean addOnUpdatedListener(OnUpdatedListener listener) {
        if (this._UpdatedListenerList == null) {
            this._UpdatedListenerList = new ArrayList<>();
        } else if (this._UpdatedListenerList.contains(listener)) {
            return false;
        }
        this._UpdatedListenerList.add(listener);
        return true;
    }

    public boolean removeOnUpdatedListener(OnUpdatedListener listener) {
        if (this._UpdatedListenerList == null || !this._UpdatedListenerList.contains(listener)) {
            return false;
        }
        this._UpdatedListenerList.remove(listener);
        return true;
    }

    public void clearOnUpdatedListeners() {
        if (this._UpdatedListenerList != null) {
            this._UpdatedListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onUpdated() {
        if (this._UpdatedListenerList != null) {
            Iterator<OnUpdatedListener> it = this._UpdatedListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnUpdatedEvent(this);
            }
        }
    }

    public boolean addOnDrawingListener(OnDrawingListener listener) {
        if (this._DrawingListenerList == null) {
            this._DrawingListenerList = new ArrayList<>();
        } else if (this._DrawingListenerList.contains(listener)) {
            return false;
        }
        this._DrawingListenerList.add(listener);
        return true;
    }

    public boolean removeOnDrawingListener(OnDrawingListener listener) {
        if (this._DrawingListenerList == null || !this._DrawingListenerList.contains(listener)) {
            return false;
        }
        this._DrawingListenerList.remove(listener);
        return true;
    }

    public void clearOnDrawingListeners() {
        if (this._DrawingListenerList != null) {
            this._DrawingListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public boolean onDrawing(Canvas c, Rect drawArea) {
        if (this._DrawingListenerList != null) {
            Iterator<OnDrawingListener> it = this._DrawingListenerList.iterator();
            while (it.hasNext()) {
                if (!it.next().OnDrawingEvent(this, c, drawArea)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean addOnDrawnListener(OnDrawnListener listener) {
        if (this._DrawnListenerList == null) {
            this._DrawnListenerList = new ArrayList<>();
        } else if (this._DrawnListenerList.contains(listener)) {
            return false;
        }
        this._DrawnListenerList.add(listener);
        return true;
    }

    public boolean removeOnDrawnListener(OnDrawnListener listener) {
        if (this._DrawnListenerList == null || !this._DrawnListenerList.contains(listener)) {
            return false;
        }
        this._DrawnListenerList.remove(listener);
        return true;
    }

    public void clearOnDrawnListeners() {
        if (this._DrawnListenerList != null) {
            this._DrawnListenerList.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void onDrawn(Canvas c, Rect drawArea) {
        if (this._DrawnListenerList != null) {
            Iterator<OnDrawnListener> it = this._DrawnListenerList.iterator();
            while (it.hasNext()) {
                it.next().OnDrawnEvent(this, c, drawArea);
            }
        }
    }

    public GameMotionGroup getMotionGroup() {
        return this._MotionGroup;
    }

    /* access modifiers changed from: protected */
    public void setMotionGroup(GameMotionGroup value) {
        this._MotionGroup = value;
    }

    /* access modifiers changed from: protected */
    public void setMotionGroupToDefault() {
        setMotionGroup(new GameMotionGroup());
    }

    public GameRootObject getRoot() {
        return getParent().getRoot();
    }

    public Vector2DF getAngularPointA() {
        return new Vector2DF(0.0f, 0.0f);
    }

    public Vector2DF getDisplayAngularPointA(Matrix m) {
        return getRoot().transitionPosition(getAngularPointA().map(m));
    }

    public Vector2DF getDisplayAngularPointA() {
        return getDisplayAngularPointA(getMatrix());
    }

    public Vector2DF getAngularPointB() {
        return getAngularPointA().plus(getDrawSize().scaleNew(1.0f, 0.0f));
    }

    public Vector2DF getDisplayAngularPointB(Matrix m) {
        return getRoot().transitionPosition(getAngularPointB().map(m));
    }

    public Vector2DF getDisplayAngularPointB() {
        return getDisplayAngularPointB(getMatrix());
    }

    public Vector2DF getAngularPointC() {
        return getAngularPointA().plus(getDrawSize().scaleNew(0.0f, 1.0f));
    }

    public Vector2DF getDisplayAngularPointC(Matrix m) {
        return getRoot().transitionPosition(getAngularPointC().map(m));
    }

    public Vector2DF getDisplayAngularPointC() {
        return getDisplayAngularPointC(getMatrix());
    }

    public Vector2DF getAngularPointD() {
        return getAngularPointA().plus(getDrawSize());
    }

    public Vector2DF getDisplayAngularPointD(Matrix m) {
        return getRoot().transitionPosition(getAngularPointD().map(m));
    }

    public Vector2DF getDisplayAngularPointD() {
        return getDisplayAngularPointD(getMatrix());
    }

    public void setPositionOffsetToHalfSize() {
        getPositionOffset().resetWith(getSize().scaleNew(0.5f).negate());
    }
}
