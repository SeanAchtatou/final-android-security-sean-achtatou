package com.skyd.core.game.crosswisewar;

public interface IMove {
    int getSpeed();

    boolean move();

    void setSpeed(int i);
}
