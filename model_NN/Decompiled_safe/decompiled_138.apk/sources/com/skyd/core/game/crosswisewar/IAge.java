package com.skyd.core.game.crosswisewar;

public interface IAge extends IBase {
    void UpToNextAge();
}
