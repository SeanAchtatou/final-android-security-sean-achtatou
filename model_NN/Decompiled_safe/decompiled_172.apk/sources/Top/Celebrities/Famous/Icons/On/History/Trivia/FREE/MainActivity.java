package Top.Celebrities.Famous.Icons.On.History.Trivia.FREE;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

public class MainActivity extends Activity implements View.OnClickListener, Runnable {
    final int NO = 115;
    final int NUM_OF_OBJECTS = 102;
    final int RIGHT = 103;
    final int WRONG = 102;
    final int YES = 104;
    TextView counter;
    int currBtn = 0;
    Integer currObj = 0;
    long currTimeVal = 30;
    ArrayList<GameObject> gameObjects;
    Handler handler = new Handler();
    ImageButton[] imagebuttons;
    ArrayList<String> names = new ArrayList<>();
    float orgvolume;
    String[] pics = {"AbrahamLincoln.png", "AlbertEinstein.png", "AlCapone.png", "AlexanderGrahamBell.png", "AlfredHitchcock.png", "AmyWinehouse.png", "AndyKaufman.png", "AngelinaJolie.png", "AnneFrank.png", "AshtonKutcher.png", "BetteDavis.png", "BillGates.png", "BobDylan.png", "BobMarley.png", "Bono.png", "BradPitt.png", "BritneySpears.png", "BrookeShields.png", "BruceLee.png", "BruceWillis.png", "BuddyHolly.png", "CameronDiaz.png", "CharlesDeGaulle.png", "CharlieChaplin.png", "CheGuevara.png", "ChristinaAguilera.png", "ChristopherWalken.png", "CindyCrawford.png", "DemiMoore.png", "DennisHaysbert.png", "DrewBarrymore.png", "DrStephenHawking.png", "ElvisPresley.png", "ErnestHemingway.png", "FarrahFawcett.png", "FidelCastro.png", "FrankSinatra.png", "FredAstaire.png", "FreddieMercury.png", "GerardButler.png", "GordonRamsey.png", "GwynethPaltrow.png", "HalleBerry.png", "HaydenPanettiere.png", "HughGrant.png", "JackieChan.png", "JackNicholson.png", "JamesDean.png", "JanisJoplin.png", "JenniferAniston.png", "JenniferConnelly.png", "JenniferLopez.png", "JerryGarcia.png", "JessicaAlba.png", "JimMorrison.png", "JoanRivers.png", "JoeDiMaggio.png", "JohnFKennedy.png", "JohnLennon.png", "JohnLithgow.png", "JuliaRoberts.png", "KateWinslet.png", "KatieHolmes.png", "KurtCobain.png", "LadyGaga.png", "LouisArmstrong.png", "LudwigvanBeethoven.png", "Madonna.png", "MahatmaGandhi.png", "MarilynManson.png", "MarilynMonroe.png", "MartinLutherKing.png", "MenaSuvari.png", "MichaelJackson.png", "MichaelJordan.png", "MickeyRourke.png", "MickJagger.png", "MorganPorterfieldFreeman.png", "MuhammadAli.png", "NaomiWatts.png", "NeilArmstrong.png", "NelsonMandela.png", "NicoleRichie.png", "OprahWinfrey.png", "OzzyOsbourne.png", "PaulNewman.png", "PrincessDiana.png", "Rihanna.png", "RobertDeNiro.png", "RoveMcManus.png", "SalvadorDali.png", "Shakira.png", "SigmundFreud.png", "StanleyKubrick.png", "SteveJobs.png", "TeriHatcher.png", "TheDalaiLama.png", "TomCruise.png", "VictoriaBeckham.png", "WaltDisney.png", "WinstonChurchill.png", "WoodyAllen.png"};
    TextView score;
    int scoreval = 0;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;
    Integer[] soundsNo = {Integer.valueOf((int) R.raw.no1), Integer.valueOf((int) R.raw.no2), Integer.valueOf((int) R.raw.no3), Integer.valueOf((int) R.raw.no4), Integer.valueOf((int) R.raw.no5), Integer.valueOf((int) R.raw.no6), Integer.valueOf((int) R.raw.no7), Integer.valueOf((int) R.raw.no8), Integer.valueOf((int) R.raw.no9), Integer.valueOf((int) R.raw.no10)};
    Integer[] soundsYes = {Integer.valueOf((int) R.raw.yes1), Integer.valueOf((int) R.raw.yes2), Integer.valueOf((int) R.raw.yes3), Integer.valueOf((int) R.raw.yes4), Integer.valueOf((int) R.raw.yes5), Integer.valueOf((int) R.raw.yes6), Integer.valueOf((int) R.raw.yes7), Integer.valueOf((int) R.raw.yes8), Integer.valueOf((int) R.raw.yes9), Integer.valueOf((int) R.raw.yes10)};
    CountDownTimer tc;
    TextView timer;
    TextView title;
    ImageView titleImageView;
    float volume;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.titleImageView = (ImageView) findViewById(R.id.goimage);
        this.title = (TextView) findViewById(R.id.title);
        this.timer = (TextView) findViewById(R.id.timer);
        this.score = (TextView) findViewById(R.id.score);
        this.counter = (TextView) findViewById(R.id.counter);
        this.imagebuttons = new ImageButton[4];
        this.imagebuttons[0] = (ImageButton) findViewById(R.id.image1);
        this.imagebuttons[1] = (ImageButton) findViewById(R.id.image2);
        this.imagebuttons[2] = (ImageButton) findViewById(R.id.image3);
        this.imagebuttons[3] = (ImageButton) findViewById(R.id.image4);
        for (int i = 0; i < 4; i++) {
            this.imagebuttons[i].setOnClickListener(this);
        }
        InitObjects();
        AudioManager mgr = (AudioManager) getSystemService("audio");
        this.volume = ((float) mgr.getStreamVolume(3)) / ((float) mgr.getStreamMaxVolume(3));
        this.orgvolume = this.volume;
        StartGame();
    }

    /* access modifiers changed from: package-private */
    public void StartGame() {
        LoadNextImages();
        ShowTitle();
    }

    public void ShowTitle() {
        try {
            this.titleImageView.setVisibility(0);
            this.titleImageView.bringToFront();
            this.titleImageView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fadeout));
            this.titleImageView.setVisibility(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void InitObjects() {
        this.gameObjects = new ArrayList<>();
        this.soundPool = new SoundPool(4, 3, 100);
        this.soundPoolMap = new HashMap<>();
        for (int i = 0; i < 102; i++) {
            try {
                String name = SpaceWords(this.pics[i].replace(".png", ""));
                this.names.add(name);
                this.gameObjects.add(new GameObject(name, this.pics[i], i));
            } catch (Exception e) {
                Log.e("Load Image", e.toString());
            }
        }
        this.soundPoolMap.put(102, Integer.valueOf(this.soundPool.load(this, R.raw.wrong, 1)));
        this.soundPoolMap.put(103, Integer.valueOf(this.soundPool.load(this, R.raw.right, 1)));
        for (int i2 = 0; i2 < 10; i2++) {
            this.soundPoolMap.put(Integer.valueOf(i2 + 104), Integer.valueOf(this.soundPool.load(this, this.soundsYes[i2].intValue(), 1)));
            this.soundPoolMap.put(Integer.valueOf(i2 + 115), Integer.valueOf(this.soundPool.load(this, this.soundsNo[i2].intValue(), 1)));
        }
    }

    /* access modifiers changed from: package-private */
    public String SpaceWords(String name) {
        String newName = "";
        int index = 0;
        int last = 0;
        for (char c : name.toUpperCase().toCharArray()) {
            if (c == name.charAt(index)) {
                newName = String.valueOf(newName) + name.substring(last, index) + " ";
                last = index;
            }
            index++;
        }
        return (String.valueOf(newName) + name.substring(last, name.length())).trim();
    }

    private void LoadNextImages() {
        boolean similar;
        if (this.tc != null) {
            this.tc.cancel();
        }
        this.currBtn = new Random().nextInt(3);
        this.title.setText(this.names.get(this.currObj.intValue()));
        ArrayList<Integer> ints = new ArrayList<>();
        AssetManager assetManager = getAssets();
        int i = 0;
        while (i < 4) {
            int rand = new Random().nextInt(101);
            while (rand == this.currObj.intValue()) {
                rand = new Random().nextInt(101);
            }
            do {
                similar = false;
                Iterator it = ints.iterator();
                while (it.hasNext()) {
                    if (((Integer) it.next()).intValue() == rand) {
                        rand = new Random().nextInt(101);
                        similar = true;
                    }
                }
            } while (similar);
            ints.add(Integer.valueOf(rand));
            int idToGet = i != this.currBtn ? rand : this.currObj.intValue();
            try {
                this.imagebuttons[i].setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeStream(assetManager.open(this.gameObjects.get(idToGet).getImageName())), 240, 240, true));
                this.imagebuttons[i].setTag(Integer.valueOf(idToGet));
            } catch (Exception e) {
            }
            i++;
        }
        this.tc = null;
        this.tc = new TimeCounter(30000, 1000).start();
    }

    public void onClick(View v) {
        final boolean answeredRight = ((Integer) v.getTag()).intValue() == this.currObj.intValue();
        int rndVoice = new Random().nextInt(12);
        if (rndVoice < 10) {
            playSound(answeredRight ? rndVoice + 104 : rndVoice + 115);
        }
        v.setBackgroundColor(answeredRight ? -16711936 : -65536);
        final View vv = v;
        v.postDelayed(new Runnable() {
            public void run() {
                vv.setBackgroundColor(0);
                MainActivity.this.score.setText("Score: " + MainActivity.this.scoreval);
                if (answeredRight) {
                    MainActivity.this.handler.post(MainActivity.this);
                }
            }
        }, 2000);
        new Thread() {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
                MainActivity.this.playSound(answeredRight ? 103 : 102);
                MainActivity.this.scoreval = (int) (answeredRight ? ((long) MainActivity.this.scoreval) + MainActivity.this.currTimeVal : (long) (MainActivity.this.scoreval - 15));
            }
        }.run();
    }

    public void playSound(int sound) {
        this.soundPool.play(this.soundPoolMap.get(Integer.valueOf(sound)).intValue(), this.volume, this.volume, 1, 0, 1.0f);
    }

    public void run() {
        this.currObj = Integer.valueOf(this.currObj.intValue() + 1);
        this.counter.setText(String.valueOf(this.currObj.intValue() + 1) + "/101");
        if (this.currObj.intValue() >= 102) {
            this.tc.cancel();
            DisplayDialog();
            this.scoreval = 0;
            this.currObj = 0;
            this.counter.setText("1/101");
            this.score.setText("Score: " + this.scoreval);
            return;
        }
        LoadNextImages();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 82) {
            return super.onKeyDown(keyCode, event);
        }
        if (keyCode == 4) {
            Toast.makeText(getApplicationContext(), "Exit trough Menu only.", 0);
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exitbtn:
                finish();
                return true;
            case R.id.aboutbtn:
                this.volume = this.volume == this.orgvolume ? 0.0f : this.orgvolume;
                String title2 = item.getTitle().toString();
                if (item.isVisible()) {
                    item.setTitle(title2 == "Unmute" ? "Mute" : "Unmute");
                }
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void DisplayDialog() {
        View layout = ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.custom_dialog, (ViewGroup) findViewById(R.id.layout_root));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        final AlertDialog alert = builder.create();
        ((Button) layout.findViewById(R.id.btnok)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                alert.dismiss();
                MainActivity.this.StartGame();
            }
        });
        ((TextView) layout.findViewById(R.id.text3)).setText(Integer.toString(this.scoreval));
        alert.setTitle("Master of Celebs - You did it!");
        alert.setIcon((int) R.drawable.info);
        alert.show();
    }

    public class TimeCounter extends CountDownTimer {
        public TimeCounter(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        public void onFinish() {
            MainActivity.this.playSound(102);
            MainActivity.this.handler.post(MainActivity.this);
        }

        public void onTick(long millisUntilFinished) {
            MainActivity.this.currTimeVal = millisUntilFinished / 1000;
            MainActivity.this.timer.setText(Long.toString(MainActivity.this.currTimeVal));
        }
    }
}
