package Top.Celebrities.Famous.Icons.On.History.Trivia.FREE;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.MotionEvent;

public class SplashScreenActivity extends Activity {
    public static MediaPlayer mp;
    protected Handler m_ExitHandler = null;
    protected Runnable m_ExitRunnable = null;
    protected int m_SplashTime = 3000;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(3);
        mp = MediaPlayer.create(getApplicationContext(), (int) R.raw.bgmusic);
        mp.start();
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.splash);
        this.m_ExitRunnable = new Runnable() {
            public void run() {
                SplashScreenActivity.this.exitSplash();
            }
        };
        this.m_ExitHandler = new Handler();
        this.m_ExitHandler.postDelayed(this.m_ExitRunnable, (long) this.m_SplashTime);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.m_ExitHandler.removeCallbacks(this.m_ExitRunnable);
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        this.m_ExitHandler.removeCallbacks(this.m_ExitRunnable);
        if (!isFinishing()) {
            finish();
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != 0) {
            return true;
        }
        this.m_ExitHandler.removeCallbacks(this.m_ExitRunnable);
        exitSplash();
        return true;
    }

    /* access modifiers changed from: private */
    public void exitSplash() {
        finish();
        try {
            startActivity(new Intent(this, MainActivity.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
