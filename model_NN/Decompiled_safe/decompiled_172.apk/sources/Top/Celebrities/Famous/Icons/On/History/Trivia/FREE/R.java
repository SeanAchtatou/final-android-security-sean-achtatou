package Top.Celebrities.Famous.Icons.On.History.Trivia.FREE;

public final class R {

    public static final class anim {
        public static final int fadeout = 2130968576;
    }

    public static final class attr {
    }

    public static final class drawable {
        public static final int goimage = 2130837504;
        public static final int icon = 2130837505;
        public static final int info = 2130837506;
        public static final int mainbg = 2130837507;
        public static final int pic = 2130837508;
        public static final int splash = 2130837509;
    }

    public static final class id {
        public static final int SplashScreenLayout = 2131230735;
        public static final int aboutbtn = 2131230738;
        public static final int adView = 2131230732;
        public static final int btnok = 2131230724;
        public static final int counter = 2131230726;
        public static final int exitbtn = 2131230737;
        public static final int goimage = 2131230734;
        public static final int image1 = 2131230728;
        public static final int image2 = 2131230729;
        public static final int image3 = 2131230730;
        public static final int image4 = 2131230731;
        public static final int layout_root = 2131230720;
        public static final int score = 2131230725;
        public static final int splashscreen = 2131230736;
        public static final int text = 2131230721;
        public static final int text2 = 2131230722;
        public static final int text3 = 2131230723;
        public static final int timer = 2131230733;
        public static final int title = 2131230727;
    }

    public static final class layout {
        public static final int custom_dialog = 2130903040;
        public static final int main = 2130903041;
        public static final int splash = 2130903042;
    }

    public static final class menu {
        public static final int menu = 2131165184;
    }

    public static final class raw {
        public static final int bgmusic = 2131034112;
        public static final int no1 = 2131034113;
        public static final int no10 = 2131034114;
        public static final int no2 = 2131034115;
        public static final int no3 = 2131034116;
        public static final int no4 = 2131034117;
        public static final int no5 = 2131034118;
        public static final int no6 = 2131034119;
        public static final int no7 = 2131034120;
        public static final int no8 = 2131034121;
        public static final int no9 = 2131034122;
        public static final int right = 2131034123;
        public static final int wrong = 2131034124;
        public static final int yes1 = 2131034125;
        public static final int yes10 = 2131034126;
        public static final int yes2 = 2131034127;
        public static final int yes3 = 2131034128;
        public static final int yes4 = 2131034129;
        public static final int yes5 = 2131034130;
        public static final int yes6 = 2131034131;
        public static final int yes7 = 2131034132;
        public static final int yes8 = 2131034133;
        public static final int yes9 = 2131034134;
    }

    public static final class string {
        public static final int about = 2131099650;
        public static final int app_name = 2131099648;
        public static final int btnok = 2131099651;
        public static final int exit = 2131099649;
    }
}
