package Top.Celebrities.Famous.Icons.On.History.Trivia.FREE;

public class GameObject {
    String imageName;
    String name;
    int soundID;

    public GameObject(String name2, String imageName2, int s) {
        this.name = name2;
        this.imageName = imageName2;
        this.soundID = s;
    }

    public String getName() {
        return this.name;
    }

    public String getImageName() {
        return this.imageName;
    }

    public int getSoundID() {
        return this.soundID;
    }
}
