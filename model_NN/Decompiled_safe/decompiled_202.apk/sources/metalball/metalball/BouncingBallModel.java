package metalball.metalball;

import android.os.Vibrator;
import java.util.concurrent.atomic.AtomicReference;

public class BouncingBallModel {
    private static final float STOP_BOUNCING_VELOCITY = 2.0f;
    public final Object LOCK = new Object();
    private float accelX;
    private float accelY;
    public float ballPixelX;
    public float ballPixelY;
    private final int ballRadius;
    private volatile long lastTimeMs = -1;
    private int pixelHeight;
    private int pixelWidth;
    public float pixelsPerMeter = 10.0f;
    public float rebound = 0.8f;
    private float velocityX;
    private float velocityY;
    private AtomicReference<Vibrator> vibratorRef = new AtomicReference<>();
    public Boolean vvv = true;

    public BouncingBallModel(int ballRadius2) {
        this.ballRadius = ballRadius2;
    }

    public void setAccel(float ax, float ay) {
        synchronized (this.LOCK) {
            this.accelX = ax;
            this.accelY = ay;
        }
    }

    public void setSize(int width, int height) {
        synchronized (this.LOCK) {
            this.pixelWidth = width;
            this.pixelHeight = height;
        }
    }

    public int getBallRadius() {
        return this.ballRadius;
    }

    public void moveBall(int ballX, int ballY) {
        synchronized (this.LOCK) {
            this.ballPixelX = (float) ballX;
            this.ballPixelY = (float) ballY;
            this.velocityX = 0.0f;
            this.velocityY = 0.0f;
        }
    }

    public void updatePhysics() {
        float lWidth;
        float lHeight;
        float lBallX;
        float lBallY;
        float lVx;
        float lVy;
        float lAx;
        float lAy;
        Vibrator v;
        synchronized (this.LOCK) {
            lWidth = (float) this.pixelWidth;
            lHeight = (float) this.pixelHeight;
            lBallX = this.ballPixelX;
            lBallY = this.ballPixelY;
            lVx = this.velocityX;
            lVy = this.velocityY;
            lAx = this.accelX;
            lAy = -this.accelY;
        }
        if (lWidth > 0.0f && lHeight > 0.0f) {
            long curTime = System.currentTimeMillis();
            if (this.lastTimeMs < 0) {
                this.lastTimeMs = curTime;
                return;
            }
            long elapsedMs = curTime - this.lastTimeMs;
            this.lastTimeMs = curTime;
            float lVx2 = lVx + (((((float) elapsedMs) * lAx) / 1000.0f) * this.pixelsPerMeter);
            float lVy2 = lVy + (((((float) elapsedMs) * lAy) / 1000.0f) * this.pixelsPerMeter);
            float lBallX2 = lBallX + (((((float) elapsedMs) * lVx2) / 1000.0f) * this.pixelsPerMeter);
            float lBallY2 = lBallY + (((((float) elapsedMs) * lVy2) / 1000.0f) * this.pixelsPerMeter);
            boolean bouncedX = false;
            boolean bouncedY = false;
            if (lBallY2 - ((float) this.ballRadius) < 0.0f) {
                lBallY2 = (float) this.ballRadius;
                lVy2 = (-lVy2) * this.rebound;
                bouncedY = true;
            } else if (((float) this.ballRadius) + lBallY2 > lHeight) {
                lBallY2 = lHeight - ((float) this.ballRadius);
                lVy2 = (-lVy2) * this.rebound;
                bouncedY = true;
            }
            if (bouncedY && Math.abs(lVy2) < STOP_BOUNCING_VELOCITY) {
                lVy2 = 0.0f;
                bouncedY = false;
            }
            if (lBallX2 - ((float) this.ballRadius) < 0.0f) {
                lBallX2 = (float) this.ballRadius;
                lVx2 = (-lVx2) * this.rebound;
                bouncedX = true;
            } else if (((float) this.ballRadius) + lBallX2 > lWidth) {
                lBallX2 = lWidth - ((float) this.ballRadius);
                lVx2 = (-lVx2) * this.rebound;
                bouncedX = true;
            }
            if (bouncedX && Math.abs(lVx2) < STOP_BOUNCING_VELOCITY) {
                lVx2 = 0.0f;
                bouncedX = false;
            }
            synchronized (this.LOCK) {
                this.ballPixelX = lBallX2;
                this.ballPixelY = lBallY2;
                this.velocityX = lVx2;
                this.velocityY = lVy2;
            }
            if ((bouncedX || bouncedY) && (v = this.vibratorRef.get()) != null && this.vvv.booleanValue()) {
                v.vibrate(20);
            }
        }
    }

    public void setVibrator(Vibrator v) {
        this.vibratorRef.set(v);
    }
}
