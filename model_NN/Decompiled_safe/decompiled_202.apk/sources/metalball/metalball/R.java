package metalball.metalball;

public final class R {

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int ball11 = 2130837505;
        public static final int icon = 2130837506;
    }

    public static final class id {
        public static final int BANNER = 2131034112;
        public static final int IAB_BANNER = 2131034114;
        public static final int IAB_LEADERBOARD = 2131034115;
        public static final int IAB_MRECT = 2131034113;
        public static final int adView = 2131034116;
        public static final int adbar4 = 2131034126;
        public static final int bouncing_ball_surface = 2131034117;
        public static final int options_buttom_tablerow = 2131034124;
        public static final int options_okay_button = 2131034125;
        public static final int options_power_controller_title = 2131034120;
        public static final int options_power_controller_title2 = 2131034122;
        public static final int options_vibrate_checkbox = 2131034119;
        public static final int scrollview2 = 2131034118;
        public static final int velocityController = 2131034121;
        public static final int velocityController2 = 2131034123;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int options = 2130903041;
    }

    public static final class raw {
        public static final int d200 = 2130968576;
        public static final int speedpass = 2130968577;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int hello = 2131099648;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }
}
