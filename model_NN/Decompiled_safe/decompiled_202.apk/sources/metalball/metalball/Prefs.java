package metalball.metalball;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import com.google.ads.AdRequest;
import com.google.ads.AdView;

public class Prefs extends Activity implements View.OnClickListener {
    /* access modifiers changed from: private */
    public SharedPreferences mBaseSettings;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        requestWindowFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.options);
        ((AdView) findViewById(R.id.adbar4)).loadAd(new AdRequest());
        this.mBaseSettings = PreferenceManager.getDefaultSharedPreferences(this);
        CheckBox vibrateCheckbox = (CheckBox) findViewById(R.id.options_vibrate_checkbox);
        vibrateCheckbox.setChecked(this.mBaseSettings.getBoolean(ConstantInfo.PREFERENCE_KEY_VIBRATE, true));
        vibrateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Prefs.this.mBaseSettings.edit().putBoolean(ConstantInfo.PREFERENCE_KEY_VIBRATE, true).commit();
                } else {
                    Prefs.this.mBaseSettings.edit().putBoolean(ConstantInfo.PREFERENCE_KEY_VIBRATE, false).commit();
                }
            }
        });
        SeekBar seekBar = (SeekBar) findViewById(R.id.velocityController);
        seekBar.setProgress(this.mBaseSettings.getInt(ConstantInfo.PREFERENCE_KEY_SPEED, 10));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Prefs.this.mBaseSettings.edit().putInt(ConstantInfo.PREFERENCE_KEY_SPEED, seekBar.getProgress()).commit();
            }
        });
        SeekBar seekBar2 = (SeekBar) findViewById(R.id.velocityController2);
        seekBar2.setProgress(this.mBaseSettings.getInt(ConstantInfo.PREFERENCE_KEY_REBOUND, 8));
        seekBar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Prefs.this.mBaseSettings.edit().putInt(ConstantInfo.PREFERENCE_KEY_REBOUND, seekBar.getProgress()).commit();
            }
        });
        ((Button) findViewById(R.id.options_okay_button)).setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.options_okay_button:
                finish();
                return;
            default:
                return;
        }
    }
}
