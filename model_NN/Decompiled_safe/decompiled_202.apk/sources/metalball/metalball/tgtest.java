package metalball.metalball;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import java.util.concurrent.TimeUnit;

public class tgtest extends Activity implements SurfaceHolder.Callback, SensorListener {
    private static final int BALL_RADIUS = 25;
    int aaa = 6;
    AdView adview;
    private Paint backgroundPaint;
    private Paint ballPaint;
    private GameLoop gameLoop;
    private SurfaceHolder holder;
    private long lastSensorUpdate = -1;
    private Bitmap mBackgroundImage;
    private Drawable mBall;
    private int mBallHeight;
    private int mBallWidth;
    private Rect mFullScreenRect;
    /* access modifiers changed from: private */
    public final BouncingBallModel model = new BouncingBallModel(BALL_RADIUS);
    private SensorManager sensorMgr;
    private SurfaceView surface;

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().addFlags(128);
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.adview = (AdView) findViewById(R.id.adView);
        this.adview.loadAd(new AdRequest());
        Resources res = getResources();
        this.mBall = res.getDrawable(R.drawable.ball11);
        this.mBackgroundImage = BitmapFactory.decodeResource(res, R.drawable.background);
        this.mBallWidth = this.mBall.getIntrinsicWidth();
        this.mBallHeight = this.mBall.getIntrinsicHeight();
        this.mFullScreenRect = new Rect(0, 0, 320, 480);
        this.surface = (SurfaceView) findViewById(R.id.bouncing_ball_surface);
        this.holder = this.surface.getHolder();
        this.surface.getHolder().addCallback(this);
        this.backgroundPaint = new Paint();
        this.backgroundPaint.setColor(-1);
        this.ballPaint = new Paint();
        this.ballPaint.setColor(-65536);
        this.ballPaint.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.model.setVibrator(null);
        this.sensorMgr.unregisterListener(this, 2);
        this.sensorMgr = null;
        this.model.setAccel(0.0f, 0.0f);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.sensorMgr = (SensorManager) getSystemService("sensor");
        if (!this.sensorMgr.registerListener(this, 2, 1)) {
            this.sensorMgr.unregisterListener(this, 2);
        }
        this.model.setVibrator((Vibrator) getSystemService("vibrator"));
    }

    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
    }

    public void surfaceChanged(SurfaceHolder holder2, int format, int width, int height) {
        this.mBackgroundImage = Bitmap.createScaledBitmap(this.mBackgroundImage, width, height, true);
        this.model.setSize(width, height);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        this.model.vvv = Boolean.valueOf(preferences.getBoolean(ConstantInfo.PREFERENCE_KEY_VIBRATE, true));
        this.model.pixelsPerMeter = (float) preferences.getInt(ConstantInfo.PREFERENCE_KEY_SPEED, 10);
        this.model.rebound = ((float) preferences.getInt(ConstantInfo.PREFERENCE_KEY_REBOUND, 8)) / 10.0f;
    }

    public void surfaceCreated(SurfaceHolder holder2) {
        this.gameLoop = new GameLoop(this, null);
        this.gameLoop.start();
    }

    /* access modifiers changed from: private */
    public void draw() {
        Canvas c = null;
        try {
            c = this.holder.lockCanvas();
            if (c != null) {
                doDraw(c);
            }
        } finally {
            if (c != null) {
                this.holder.unlockCanvasAndPost(c);
            }
        }
    }

    private void doDraw(Canvas c) {
        float ballX;
        float ballY;
        int width = c.getWidth();
        int height = c.getHeight();
        c.drawBitmap(this.mBackgroundImage, 0.0f, 0.0f, this.backgroundPaint);
        synchronized (this.model.LOCK) {
            ballX = this.model.ballPixelX;
            ballY = this.model.ballPixelY;
        }
        this.mBall.setBounds(((int) ballX) - (this.mBallWidth / 2), ((int) ballY) - (this.mBallHeight / 2), ((int) ballX) + (this.mBallWidth / 2), ((int) ballY) + (this.mBallHeight / 2));
        this.mBall.draw(c);
    }

    public void surfaceDestroyed(SurfaceHolder holder2) {
        try {
            this.model.setSize(0, 0);
            this.gameLoop.safeStop();
        } finally {
            this.gameLoop = null;
        }
    }

    private class GameLoop extends Thread {
        private volatile boolean running;

        private GameLoop() {
            this.running = true;
        }

        /* synthetic */ GameLoop(tgtest tgtest, GameLoop gameLoop) {
            this();
        }

        public void run() {
            while (this.running) {
                try {
                    TimeUnit.MILLISECONDS.sleep(5);
                    tgtest.this.draw();
                    tgtest.this.model.updatePhysics();
                } catch (InterruptedException e) {
                    this.running = false;
                }
            }
        }

        public void safeStop() {
            this.running = false;
            interrupt();
        }
    }

    public void onAccuracyChanged(int sensor, int accuracy) {
    }

    public void onSensorChanged(int sensor, float[] values) {
        if (sensor == 2) {
            long curTime = System.currentTimeMillis();
            if (this.lastSensorUpdate == -1 || curTime - this.lastSensorUpdate > 50) {
                this.lastSensorUpdate = curTime;
                this.model.setAccel(values[0], values[1]);
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        if (event.getAction() == 0) {
            switch (event.getKeyCode()) {
                case 82:
                    startActivity(new Intent(this, Prefs.class));
                    return true;
            }
        }
        return false;
    }
}
