package metalball.metalball;

public interface ConstantInfo {
    public static final String PREFERENCE_KEY_REBOUND = "metalball.power";
    public static final String PREFERENCE_KEY_SPEED = "metalball.speed";
    public static final String PREFERENCE_KEY_VIBRATE = "metalball.vibrate";
}
