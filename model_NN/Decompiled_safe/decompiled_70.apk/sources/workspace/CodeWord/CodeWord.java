package workspace.CodeWord;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.io.File;
import java.util.ArrayList;

public class CodeWord extends Activity {
    private static final int AMEND_ID = 1;
    private static final int CLEAR_SOLUTION_ID = 5;
    private static final int CLEAR_WIP_ID = 4;
    private static final int CLONE_ID = 2;
    private static final int DELETE_ID = 2;
    private static final int EMAILFILE_ID = 7;
    private static final int EXAMPLE_ID = 3;
    private static final int FROMCLIPBOARD_ID = 6;
    private static final int FROMFILE_ID = 4;
    private static final int FROMTEXT_ID = 5;
    private static final int HELP_ID = 9;
    private static final int INFO_ID = 3;
    private static final int NEW_ID = 1;
    private static final int OPTIONS_ID = 8;
    private static final int SAVETOFILE_ID = 6;
    private static final int SENDTEXT_ID = 7;
    private Button bHelp;
    private Button bSmsSelectCancel;
    private CodewordPrefs codewordPrefs;
    private Button createHere;
    private int deviceSize = 1;
    private ListView dirView;
    private DisplayMetrics dm;
    /* access modifiers changed from: private */
    public FileArrayAdapter fileArrayAdapter;
    /* access modifiers changed from: private */
    public int fileOption = 0;
    private Button filePickerCancel;
    private ListView lSmsList;
    private ListView listView;
    /* access modifiers changed from: private */
    public PuzzlesDbAdapter mDbHelper;
    private int orientationLock = -1;
    private Button puzzleOptionsClose;
    /* access modifiers changed from: private */
    public PuzzlesListAdapter puzzlesListAdapter;
    /* access modifiers changed from: private */
    public PuzzlesRow puzzlesRow;
    private SmsListAdapter smsListAdapter;
    /* access modifiers changed from: private */
    public ArrayList<SmsText> smsTexts;
    /* access modifiers changed from: private */
    public EditText tCreator;
    /* access modifiers changed from: private */
    public EditText tRef;
    /* access modifiers changed from: private */
    public ViewFlipper viewFlipper;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        int i;
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        setContentView((int) R.layout.puzzles_list);
        this.codewordPrefs = new CodewordPrefs(getApplicationContext());
        this.dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(this.dm);
        if (this.dm.widthPixels < 300) {
            i = 0;
        } else {
            i = 1;
        }
        this.deviceSize = i;
        if (this.deviceSize == 0) {
            this.codewordPrefs.setMainOrientation(1);
        }
        this.viewFlipper = (ViewFlipper) findViewById(R.id.listFlipper);
        this.dirView = (ListView) findViewById(R.id.filePickerList);
        this.createHere = (Button) findViewById(R.id.bCreateHere);
        this.filePickerCancel = (Button) findViewById(R.id.bFilePickerCancel);
        this.puzzleOptionsClose = (Button) findViewById(R.id.bPuzzleOptionsClose);
        this.puzzleOptionsClose.setOnClickListener(new PuzzleOptionsCloseListener());
        this.bHelp = (Button) findViewById(R.id.bHelp);
        this.bHelp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CodeWord.this.codewordHelp("");
            }
        });
        this.lSmsList = (ListView) findViewById(R.id.lSmsList);
        this.bSmsSelectCancel = (Button) findViewById(R.id.bSmsSelectCancel);
        this.mDbHelper = new PuzzlesDbAdapter(this);
        this.mDbHelper.open();
        this.listView = (ListView) findViewById(R.id.puzzlesList);
        this.puzzlesListAdapter = new PuzzlesListAdapter(this, this.mDbHelper, this.viewFlipper);
        this.listView.setAdapter((ListAdapter) this.puzzlesListAdapter);
        this.listView.setOnItemClickListener(new ChoosePuzzleListener());
        orientationSetup();
        registerForContextMenu(this.listView);
        new LongScreenTimeout(getApplicationContext(), 0);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            updatePuzzleInfo(true, Long.valueOf(extras.getLong("updateInfo")));
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        orientationSetup();
    }

    public void orientationSetup() {
        this.orientationLock = this.codewordPrefs.getMainOrientation();
        if (this.orientationLock > 0) {
            setRequestedOrientation(this.orientationLock);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new LongScreenTimeout(getApplicationContext(), 1);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.puzzlesListAdapter.refreshPuzzlesList();
        new LongScreenTimeout(getApplicationContext(), 0);
        super.onResume();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 0, (int) R.string.menu_insert).setIcon(17301547);
        menu.add(0, 2, 0, (int) R.string.menu_clone).setIcon(17301555);
        menu.add(0, 3, 0, (int) R.string.menu_example).setIcon(17301569);
        menu.add(0, 4, 0, (int) R.string.from_file).setIcon(17301589);
        menu.add(0, 5, 0, (int) R.string.from_text).setIcon(17301584);
        menu.add(0, 6, 0, (int) R.string.from_clipboard).setIcon(17301589);
        menu.add(0, 7, 0, (int) R.string.email_file).setIcon(17301586);
        menu.add(0, (int) OPTIONS_ID, 0, (int) R.string.menu_options).setIcon(17301577);
        menu.add(0, (int) HELP_ID, 0, (int) R.string.menu_help).setIcon(17301568);
        return result;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                newPuzzle();
                return true;
            case 2:
                clonePuzzle();
                return true;
            case 3:
                createExamplePuzzle();
                return true;
            case 4:
                this.fileOption = 0;
                puzzleFromFile();
                return true;
            case 5:
                puzzleFromText();
                return true;
            case 6:
                puzzleFromClipboard();
                return true;
            case 7:
                this.fileOption = 1;
                puzzleFromFile();
                return true;
            case OPTIONS_ID /*8*/:
                puzzleOptions();
                return true;
            case HELP_ID /*9*/:
                codewordHelp("#Puzzle selection|outline");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 1, 0, (int) R.string.menu_amend).setIcon(17301566);
        menu.add(0, 2, 0, (int) R.string.menu_delete).setIcon(17301564);
        menu.add(0, 3, 0, (int) R.string.menu_info).setIcon(17301569);
        menu.add(0, 4, 0, (int) R.string.menu_clear_wip).setIcon(17301570);
        menu.add(0, 5, 0, (int) R.string.menu_clear_solution).setIcon(17301570);
        menu.add(0, 6, 0, (int) R.string.menu_savetofile).setIcon(17301582);
        menu.add(0, 7, 0, (int) R.string.menu_sendtext).setIcon(17301586);
    }

    public boolean onContextItemSelected(MenuItem item) {
        Long rowId = this.puzzlesListAdapter.getPuzzleID(((AdapterView.AdapterContextMenuInfo) item.getMenuInfo()).position);
        switch (item.getItemId()) {
            case 1:
                Intent it = new Intent(this, PuzzleEditor.class);
                it.putExtra("puzzleMode", 1);
                executePuzzleIntent(it, rowId.longValue());
                return true;
            case 2:
                Toast.makeText(getApplicationContext(), "Deleting puzzle ", 0).show();
                this.mDbHelper.deletePuzzle(rowId.longValue());
                this.puzzlesListAdapter.refreshPuzzlesList();
                return true;
            case 3:
                updatePuzzleInfo(false, rowId);
                return true;
            case 4:
                Toast.makeText(getApplicationContext(), "Clearing work so far ", 0).show();
                clearPuzzle(rowId);
                return true;
            case 5:
                Toast.makeText(getApplicationContext(), "Clearing solution ", 0).show();
                this.puzzlesRow = this.mDbHelper.fetchPuzzle(rowId.longValue());
                this.puzzlesRow.answers = "";
                this.mDbHelper.updatePuzzle(this.puzzlesRow);
                return true;
            case 6:
                savePuzzle(rowId.longValue());
                return true;
            case 7:
                sendText(rowId.longValue());
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    public class ChoosePuzzleListener implements AdapterView.OnItemClickListener {
        public ChoosePuzzleListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            CodeWord.this.executePuzzleIntent(new Intent(CodeWord.this.getBaseContext(), PlayPuzzle.class), CodeWord.this.puzzlesListAdapter.getPuzzleID(position).longValue());
        }
    }

    /* access modifiers changed from: private */
    public void puzzleOptions() {
        new CodeWordOptions(getApplicationContext(), (LinearLayout) findViewById(R.id.lPuzzleListLayout), this.deviceSize);
    }

    public class PuzzleOptionsCloseListener implements View.OnClickListener {
        public PuzzleOptionsCloseListener() {
        }

        public void onClick(View view) {
            CodeWord.this.viewFlipper.setDisplayedChild(CodeWord.this.puzzlesListAdapter.getCount() == 0 ? 1 : 0);
        }
    }

    /* access modifiers changed from: private */
    public void codewordHelp(String hyperlink) {
        Intent it = new Intent(this, CodeWordHelp.class);
        it.putExtra("hyperlink", hyperlink);
        startActivityForResult(it, 0);
    }

    private void newPuzzle() {
        PuzzlesRow puzzlesRow2 = new PuzzlesRow();
        puzzlesRow2.ref = "New puzzle";
        puzzlesRow2.creator = "wip";
        puzzlesRow2.setGridFromArray(new short[169]);
        puzzlesRow2.supplied = "";
        puzzlesRow2.guesses = "";
        puzzlesRow2.answers = "";
        puzzlesRow2.createDate = puzzlesRow2.getDateNow();
        puzzlesRow2.modificationDate = 0;
        long rowId = this.mDbHelper.createPuzzle(puzzlesRow2);
        this.puzzlesListAdapter.refreshPuzzlesList();
        Intent it = new Intent(this, PuzzleEditor.class);
        it.putExtra("puzzleMode", 0);
        executePuzzleIntent(it, rowId);
    }

    private void clonePuzzle() {
        PuzzlesRow puzzlesRow2 = new PuzzlesRow();
        puzzlesRow2.ref = "Transcribe puzzle";
        puzzlesRow2.creator = "";
        puzzlesRow2.setGridFromArray(new short[169]);
        puzzlesRow2.supplied = "";
        puzzlesRow2.guesses = "";
        puzzlesRow2.answers = "";
        puzzlesRow2.createDate = puzzlesRow2.getDateNow();
        puzzlesRow2.modificationDate = 0;
        long rowId = this.mDbHelper.createPuzzle(puzzlesRow2);
        this.puzzlesListAdapter.refreshPuzzlesList();
        Intent it = new Intent(this, PuzzleEditor.class);
        it.putExtra("puzzleMode", 1);
        executePuzzleIntent(it, rowId);
    }

    private void puzzleFromClipboard() {
        PuzzlesRow puzzlesRow2 = new PuzzlesRow().getPuzzleFromClipboard(this);
        if (puzzlesRow2 == null) {
            Toast.makeText(getApplicationContext(), "Sorry clipboard content is not a puzzle", 0).show();
        } else if (!puzzlesRow2.workingPuzzle.booleanValue()) {
            Toast.makeText(getApplicationContext(), "Sorry the puzzle in the clipboard is broken", 0).show();
        } else {
            puzzlesRow2.guesses = puzzlesRow2.supplied;
            Long rowId = Long.valueOf(this.mDbHelper.createPuzzle(puzzlesRow2));
            this.puzzlesListAdapter.refreshPuzzlesList();
            executePuzzleIntent(new Intent(this, PlayPuzzle.class), rowId.longValue());
        }
    }

    private void updatePuzzleInfo(boolean amendableCreator, Long rowId) {
        this.viewFlipper.setDisplayedChild(5);
        this.puzzlesRow = this.mDbHelper.fetchPuzzle(rowId.longValue());
        this.puzzlesRow.guesses = this.puzzlesRow.supplied;
        this.tRef = (EditText) findViewById(R.id.tMainRef);
        this.tRef.setText(this.puzzlesRow.ref);
        this.tCreator = (EditText) findViewById(R.id.tMainCreator);
        this.tCreator.setText(this.puzzlesRow.creator);
        if (!amendableCreator && this.puzzlesRow.creator.matches("wip")) {
            this.tCreator.setEnabled(false);
        }
        this.tCreator.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String editable = CodeWord.this.tCreator.getText().toString();
                if (!CodeWord.this.tCreator.getText().toString().matches("wip")) {
                    return false;
                }
                Toast.makeText(CodeWord.this.getApplicationContext(), "Don't make the creator's name 'wip' please", 0).show();
                return true;
            }
        });
        ((Button) findViewById(R.id.bUpdateInfo)).setOnClickListener(new UpdatePuzzleInfoListener());
        ((TextView) findViewById(R.id.tMainDateCreated)).setText("Created on: " + this.puzzlesRow.getCreateDateString());
        ((TextView) findViewById(R.id.tMainDateModified)).setText("Updated last: " + this.puzzlesRow.getModificationDateString());
    }

    public class UpdatePuzzleInfoListener implements View.OnClickListener {
        public UpdatePuzzleInfoListener() {
        }

        public void onClick(View view) {
            CodeWord.this.puzzlesRow.creator = CodeWord.this.tCreator.getText().toString();
            CodeWord.this.puzzlesRow.ref = CodeWord.this.tRef.getText().toString();
            CodeWord.this.mDbHelper.updatePuzzle(CodeWord.this.puzzlesRow);
            CodeWord.this.puzzlesListAdapter.refreshPuzzlesList();
            Toast.makeText(CodeWord.this.getApplicationContext(), "Puzzle info updated", 0).show();
            CodeWord.this.viewFlipper.setDisplayedChild(0);
        }
    }

    private void clearPuzzle(Long rowId) {
        this.puzzlesRow = this.mDbHelper.fetchPuzzle(rowId.longValue());
        this.puzzlesRow.guesses = this.puzzlesRow.supplied;
        this.mDbHelper.updatePuzzle(this.puzzlesRow);
    }

    private void puzzleFromFile() {
        this.viewFlipper.setDisplayedChild(4);
        this.filePickerCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CodeWord.this.viewFlipper.setDisplayedChild(CodeWord.this.puzzlesListAdapter.getCount() == 0 ? 1 : 0);
            }
        });
        this.dirView.setOnItemClickListener(new dirListener());
        String s = this.codewordPrefs.getSDFolder();
        if (s.length() == 0) {
            sdFolderOption();
        } else {
            this.fileArrayAdapter = new FileArrayAdapter(this, this.dirView, s, false, ".cwd", this.createHere, false);
        }
    }

    private void sdFolderOption() {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setMessage("Your SD folder location is not available.  Would you like to update the location of it now?");
        alertbox.setPositiveButton("Yes please", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                CodeWord.this.puzzleOptions();
            }
        });
        alertbox.setNegativeButton("No thanks", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                CodeWord.this.viewFlipper.setDisplayedChild(CodeWord.this.puzzlesListAdapter.getCount() == 0 ? 1 : 0);
            }
        });
        alertbox.show();
    }

    class dirListener implements AdapterView.OnItemClickListener {
        dirListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            int i;
            File f = CodeWord.this.fileArrayAdapter.getFile(position);
            if (f == null) {
                CodeWord.this.fileArrayAdapter.nextList(position);
                return;
            }
            ViewFlipper access$1 = CodeWord.this.viewFlipper;
            if (CodeWord.this.puzzlesListAdapter.getCount() == 0) {
                i = 1;
            } else {
                i = 0;
            }
            access$1.setDisplayedChild(i);
            PuzzlesRow puzzlesRow = new PuzzlesRow().getPuzzleFromFile(f);
            if (puzzlesRow == null) {
                Toast.makeText(CodeWord.this.getApplicationContext(), "Sorry not a puzzle", 0).show();
            } else if (!puzzlesRow.workingPuzzle.booleanValue()) {
                Toast.makeText(CodeWord.this.getApplicationContext(), "Sorry the puzzle is broken", 0).show();
            } else if (CodeWord.this.fileOption == 0) {
                puzzlesRow.guesses = puzzlesRow.supplied;
                long rowId = CodeWord.this.mDbHelper.createPuzzle(puzzlesRow);
                CodeWord.this.puzzlesListAdapter.refreshPuzzlesList();
                CodeWord.this.executePuzzleIntent(new Intent(CodeWord.this.getApplicationContext(), PlayPuzzle.class), rowId);
            } else {
                Intent sendIntent = new Intent("android.intent.action.SEND");
                sendIntent.setType("text/plain");
                sendIntent.putExtra("android.intent.extra.STREAM", Uri.fromFile(f));
                CodeWord.this.startActivity(Intent.createChooser(sendIntent, "What do you want to use"));
            }
        }
    }

    private void createExamplePuzzle() {
        PuzzlesRow puzzlesRow2 = new PuzzlesRow().getPuzzleFromAssets(getApplicationContext(), "puzzle.xml");
        if (puzzlesRow2 == null) {
            Toast.makeText(getApplicationContext(), "Sorry example not available", 0).show();
        } else if (!puzzlesRow2.workingPuzzle.booleanValue()) {
            Toast.makeText(getApplicationContext(), "Sorry the puzzle is broken", 0).show();
        } else {
            puzzlesRow2.ref = "Example puzzle";
            puzzlesRow2.creator = "CodeWord";
            puzzlesRow2.guesses = puzzlesRow2.supplied;
            puzzlesRow2.createDate = puzzlesRow2.getDateNow();
            puzzlesRow2.modificationDate = 0;
            Long rowId = Long.valueOf(this.mDbHelper.createPuzzle(puzzlesRow2));
            this.puzzlesListAdapter.refreshPuzzlesList();
            executePuzzleIntent(new Intent(this, PlayPuzzle.class), rowId.longValue());
        }
    }

    private void savePuzzle(long rowId) {
        this.puzzlesRow = this.mDbHelper.fetchPuzzle(rowId);
        int status = this.puzzlesRow.savePuzzleAsFile(this, false);
        if (status == 0) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setMessage("The puzzle is already saved");
            alertbox.setPositiveButton((int) R.string.file_replace, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                    CodeWord.this.puzzlesRow.savePuzzleAsFile(CodeWord.this.getApplicationContext(), true);
                }
            });
            alertbox.setNegativeButton((int) R.string.file_keep, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {
                }
            });
            alertbox.show();
        } else if (status == 2) {
            sdFolderOption();
        }
    }

    private void sendText(long rowId) {
        this.puzzlesRow = this.mDbHelper.fetchPuzzle(rowId);
        Intent sendIntent = new Intent("android.intent.action.SEND");
        sendIntent.setType("text/plain");
        sendIntent.putExtra("android.intent.extra.TEXT", "Try this puzzle: " + this.puzzlesRow.getPuzzleAsXml());
        startActivity(Intent.createChooser(sendIntent, "What do you want to use"));
    }

    private void puzzleFromText() {
        Cursor cur = getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        this.smsTexts = new ArrayList<>();
        while (cur.moveToNext()) {
            String person = cur.getString(cur.getColumnIndex("address"));
            String date = cur.getString(cur.getColumnIndex("date"));
            String body = cur.getString(cur.getColumnIndex("body"));
            if (body.contains("<puzzle>")) {
                this.puzzlesRow = new PuzzlesRow();
                SmsText smsText = new SmsText();
                smsText.person = person;
                smsText.date = date;
                smsText.puzzleRow = null;
                if (body.contains("<?xml")) {
                    smsText.puzzleRow = this.puzzlesRow.getPuzzleFromString(body.substring(body.indexOf("<?xml")));
                }
                this.smsTexts.add(smsText);
            }
        }
        cur.close();
        if (this.smsTexts.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Sorry, can't find a message containing a puzzle", 0).show();
            return;
        }
        this.viewFlipper.setDisplayedChild(3);
        this.smsListAdapter = new SmsListAdapter(getApplicationContext(), this.smsTexts);
        this.lSmsList.setAdapter((ListAdapter) this.smsListAdapter);
        this.lSmsList.setOnItemClickListener(new ChooseSMSListener());
        this.bSmsSelectCancel.setOnClickListener(new SmsSelectCancelListener());
    }

    public class ChooseSMSListener implements AdapterView.OnItemClickListener {
        public ChooseSMSListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            SmsText smsText = (SmsText) CodeWord.this.smsTexts.get(position);
            if (smsText.puzzleRow != null && smsText.puzzleRow.isPuzzlePlayable()) {
                CodeWord.this.viewFlipper.setDisplayedChild(CodeWord.this.puzzlesListAdapter.getCount() == 0 ? 1 : 0);
                PuzzlesRow p = smsText.puzzleRow;
                p.guesses = p.supplied;
                p.modificationDate = 0;
                long rowId = CodeWord.this.mDbHelper.createPuzzle(p);
                CodeWord.this.puzzlesListAdapter.refreshPuzzlesList();
                CodeWord.this.executePuzzleIntent(new Intent(CodeWord.this.getBaseContext(), PlayPuzzle.class), rowId);
            }
        }
    }

    public class SmsSelectCancelListener implements View.OnClickListener {
        public SmsSelectCancelListener() {
        }

        public void onClick(View view) {
            int i;
            ViewFlipper access$1 = CodeWord.this.viewFlipper;
            if (CodeWord.this.puzzlesListAdapter.getCount() == 0) {
                i = 1;
            } else {
                i = 0;
            }
            access$1.setDisplayedChild(i);
            Toast.makeText(CodeWord.this.getApplicationContext(), "Generation of puzzle from SMS text cancelled", 0).show();
        }
    }

    public void executePuzzleIntent(Intent it, long rowId) {
        it.putExtra("puzzleRow", rowId);
        it.putExtra("deviceSize", this.deviceSize);
        it.putExtra("unusableHeight", getWindow().findViewById(16908290).getTop());
        startActivityForResult(it, 0);
    }
}
