package workspace.CodeWord;

import android.content.ContentResolver;
import android.content.Context;
import android.provider.Settings;

public class LongScreenTimeout {
    public LongScreenTimeout(Context context, int action) {
        CodewordPrefs codewordPrefs = new CodewordPrefs(context.getApplicationContext());
        if (codewordPrefs.getScreenTimeout() != 0) {
            ContentResolver cr = context.getContentResolver();
            switch (action) {
                case 0:
                    codewordPrefs.setScreenTimeout(Settings.System.getInt(context.getContentResolver(), "screen_off_timeout", 1));
                    Settings.System.putInt(cr, "screen_off_timeout", 600000);
                    return;
                case 1:
                    Settings.System.putInt(cr, "screen_off_timeout", codewordPrefs.getScreenTimeout());
                    return;
                default:
                    return;
            }
        }
    }
}
