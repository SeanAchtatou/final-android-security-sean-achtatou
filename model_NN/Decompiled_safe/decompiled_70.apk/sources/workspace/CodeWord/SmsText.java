package workspace.CodeWord;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class SmsText {
    String date;
    String person;
    PuzzlesRow puzzleRow;

    public String getFormattedDate() {
        Locale locale = Locale.getDefault();
        Long longDateTime = Long.decode(this.date);
        if (longDateTime.longValue() == 0) {
            return "n/a";
        }
        DateFormat.getDateInstance(1, locale).format(new Date());
        return String.valueOf(DateFormat.getTimeInstance(3, locale).format(longDateTime)) + " " + DateFormat.getDateInstance(2, locale).format(longDateTime);
    }
}
