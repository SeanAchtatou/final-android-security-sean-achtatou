package workspace.CodeWord;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class PuzzlesDbAdapter {
    public static final String COLUMN_ANSWERS = "answers";
    public static final String COLUMN_CREATE_DATE = "created";
    public static final String COLUMN_GRID = "grid";
    public static final String COLUMN_GUESSES = "guesses";
    public static final String COLUMN_MODIFICATION_DATE = "modified";
    public static final String COLUMN_PUZZLE_CREATOR = "creator";
    public static final String COLUMN_PUZZLE_REF = "reference";
    public static final String COLUMN_ROW_ID = "_id";
    public static final String COLUMN_SUPPLIED = "supplied";
    private static final String DATABASE_CREATE = "create table puzzles (_id integer primary key autoincrement, reference text not null, creator text not null, grid text not null, supplied text not null, guesses text not null, answers text not null, created integer, modified integer);";
    private static final String DATABASE_NAME = "codeword";
    private static final String DATABASE_TABLE = "puzzles";
    private static final int DATABASE_VERSION = 2;
    private static final String TAG = "PuzzlesDbAdapter";
    private final Context mCtx;
    private SQLiteDatabase mDb;
    private DatabaseHelper mDbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, PuzzlesDbAdapter.DATABASE_NAME, (SQLiteDatabase.CursorFactory) null, (int) PuzzlesDbAdapter.DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(PuzzlesDbAdapter.DATABASE_CREATE);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(PuzzlesDbAdapter.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS puzzles");
            onCreate(db);
        }
    }

    public PuzzlesDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public PuzzlesDbAdapter open() throws SQLException {
        this.mDbHelper = new DatabaseHelper(this.mCtx);
        this.mDb = this.mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        this.mDbHelper.close();
    }

    public long createPuzzle(PuzzlesRow puzzlesRow) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(COLUMN_PUZZLE_REF, puzzlesRow.ref);
        initialValues.put(COLUMN_PUZZLE_CREATOR, puzzlesRow.creator);
        initialValues.put(COLUMN_GRID, puzzlesRow.grid);
        initialValues.put(COLUMN_SUPPLIED, puzzlesRow.supplied);
        initialValues.put(COLUMN_GUESSES, puzzlesRow.guesses);
        initialValues.put(COLUMN_ANSWERS, puzzlesRow.answers);
        initialValues.put(COLUMN_CREATE_DATE, Long.valueOf(puzzlesRow.getDateNow()));
        return this.mDb.insert(DATABASE_TABLE, null, initialValues);
    }

    public boolean deletePuzzle(long rowId) {
        return this.mDb.delete(DATABASE_TABLE, new StringBuilder("_id=").append(rowId).toString(), null) > 0;
    }

    public Long CountOfPuzzles() {
        Cursor mCursor = this.mDb.rawQuery("select count(*) from puzzles", null);
        mCursor.moveToFirst();
        mCursor.close();
        long x = mCursor.getLong(0);
        mCursor.close();
        return Long.valueOf(x);
    }

    public List<PuzzlesRow> getAllPuzzles() {
        List<PuzzlesRow> allPuzzles = new ArrayList<>();
        SQLiteDatabase sQLiteDatabase = this.mDb;
        String[] strArr = new String[9];
        strArr[0] = COLUMN_ROW_ID;
        strArr[1] = COLUMN_PUZZLE_REF;
        strArr[DATABASE_VERSION] = COLUMN_PUZZLE_CREATOR;
        strArr[3] = COLUMN_GRID;
        strArr[4] = COLUMN_SUPPLIED;
        strArr[5] = COLUMN_GUESSES;
        strArr[6] = COLUMN_ANSWERS;
        strArr[7] = COLUMN_CREATE_DATE;
        strArr[8] = COLUMN_MODIFICATION_DATE;
        Cursor mCursor = sQLiteDatabase.query(DATABASE_TABLE, strArr, null, null, null, null, null);
        if (mCursor.getCount() != 0) {
            while (mCursor.moveToNext()) {
                PuzzlesRow puzzlesRow = new PuzzlesRow();
                puzzlesRow.rowId = mCursor.getString(0);
                puzzlesRow.ref = mCursor.getString(1);
                puzzlesRow.creator = mCursor.getString(DATABASE_VERSION);
                puzzlesRow.grid = mCursor.getString(3);
                puzzlesRow.supplied = mCursor.getString(4);
                puzzlesRow.guesses = mCursor.getString(5);
                puzzlesRow.answers = mCursor.getString(6);
                puzzlesRow.createDate = mCursor.getLong(7);
                puzzlesRow.modificationDate = mCursor.getLong(8);
                allPuzzles.add(puzzlesRow);
            }
            mCursor.close();
        }
        return allPuzzles;
    }

    public PuzzlesRow fetchPuzzle(long rowId) throws SQLException {
        PuzzlesRow puzzlesRow = new PuzzlesRow();
        SQLiteDatabase sQLiteDatabase = this.mDb;
        String[] strArr = new String[9];
        strArr[0] = COLUMN_ROW_ID;
        strArr[1] = COLUMN_PUZZLE_REF;
        strArr[DATABASE_VERSION] = COLUMN_PUZZLE_CREATOR;
        strArr[3] = COLUMN_GRID;
        strArr[4] = COLUMN_SUPPLIED;
        strArr[5] = COLUMN_GUESSES;
        strArr[6] = COLUMN_ANSWERS;
        strArr[7] = COLUMN_CREATE_DATE;
        strArr[8] = COLUMN_MODIFICATION_DATE;
        Cursor mCursor = sQLiteDatabase.query(true, DATABASE_TABLE, strArr, "_id=" + rowId, null, null, null, null, null);
        if (mCursor == null) {
            return null;
        }
        mCursor.moveToFirst();
        puzzlesRow.rowId = mCursor.getString(0);
        puzzlesRow.ref = mCursor.getString(1);
        puzzlesRow.creator = mCursor.getString(DATABASE_VERSION);
        puzzlesRow.grid = mCursor.getString(3);
        puzzlesRow.supplied = mCursor.getString(4);
        puzzlesRow.guesses = mCursor.getString(5);
        puzzlesRow.answers = mCursor.getString(6);
        puzzlesRow.createDate = mCursor.getLong(7);
        puzzlesRow.modificationDate = mCursor.getLong(8);
        mCursor.close();
        return puzzlesRow;
    }

    public boolean updatePuzzle(PuzzlesRow puzzlesRow) {
        ContentValues update = new ContentValues();
        update.put(COLUMN_PUZZLE_REF, puzzlesRow.ref);
        update.put(COLUMN_PUZZLE_CREATOR, puzzlesRow.creator);
        update.put(COLUMN_GRID, puzzlesRow.grid);
        update.put(COLUMN_SUPPLIED, puzzlesRow.supplied);
        update.put(COLUMN_GUESSES, puzzlesRow.guesses);
        update.put(COLUMN_ANSWERS, puzzlesRow.answers);
        update.put(COLUMN_MODIFICATION_DATE, Long.valueOf(puzzlesRow.getDateNow()));
        return this.mDb.update(DATABASE_TABLE, update, new StringBuilder("_id=").append(puzzlesRow.rowId).toString(), null) > 0;
    }
}
