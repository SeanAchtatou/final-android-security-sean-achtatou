package workspace.CodeWord;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ChooseLetterAdapter extends BaseAdapter {
    public String[] activeLetters = new String[28];
    private String[] alphabet = {" ", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "X"};
    private Drawable buttonBackground;
    private int cellHeight;
    private int cellWidth;
    private Context context;
    private short[] letterUseage = new short[26];

    public ChooseLetterAdapter(Context context2, short[] letterUseage2, int deviceSize) {
        this.context = context2;
        this.letterUseage = letterUseage2;
        this.buttonBackground = context2.getResources().getDrawable(R.drawable.buttonface_blue_1);
        this.cellWidth = deviceSize == 0 ? 60 : 80;
        this.cellHeight = deviceSize == 0 ? 30 : 50;
        for (int i = 0; i < 28; i++) {
            this.activeLetters[i] = this.alphabet[i];
        }
    }

    public void refreshArray(char[] lettersInUse) {
        for (int i = 1; i < 27; i++) {
            this.activeLetters[i] = this.alphabet[i];
        }
        for (int i2 = 0; i2 < 26; i2++) {
            if (lettersInUse[i2] != ' ') {
                this.activeLetters[(lettersInUse[i2] - 'A') + 1] = " ";
            }
        }
        notifyDataSetChanged();
    }

    public void refreshWithDistribution(char[] lettersInUse) {
        for (int i = 0; i < 26; i++) {
            if (lettersInUse[i] != ' ') {
                this.activeLetters[i + 1] = String.valueOf(lettersInUse[i]) + "  " + ((int) this.letterUseage[i]);
            } else {
                this.activeLetters[i + 1] = String.valueOf(i + 1) + "  " + ((int) this.letterUseage[i]);
            }
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        return 28;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv;
        boolean z;
        ImageView iv;
        if (position == 27) {
            if (convertView == null || !convertView.getClass().equals(ImageView.class)) {
                iv = new ImageView(this.context);
                iv.setLayoutParams(new AbsListView.LayoutParams(this.cellWidth, this.cellHeight));
                iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            } else {
                iv = (ImageView) convertView;
            }
            iv.setImageResource(R.drawable.cancel);
            return iv;
        }
        if (convertView == null || !convertView.getClass().equals(TextView.class)) {
            tv = new TextView(this.context);
            tv.setLayoutParams(new AbsListView.LayoutParams(this.cellWidth, this.cellHeight));
            tv.setBackgroundDrawable(this.buttonBackground);
            tv.setGravity(17);
            tv.setTextColor(-16777216);
        } else {
            tv = (TextView) convertView;
        }
        boolean equals = this.activeLetters[position].equals(" ");
        if (position > 0) {
            z = true;
        } else {
            z = false;
        }
        if ((equals & z) && (position < 27)) {
            tv.setVisibility(4);
        } else {
            tv.setVisibility(0);
        }
        tv.setText(this.activeLetters[position]);
        return tv;
    }
}
