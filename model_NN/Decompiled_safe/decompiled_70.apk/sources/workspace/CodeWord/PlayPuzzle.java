package workspace.CodeWord;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.Toast;
import android.widget.ViewFlipper;

public class PlayPuzzle extends Activity {
    private static final int HELP_ID = 2;
    private static final int INFO_ORIENTATION = 1;
    /* access modifiers changed from: private */
    public GridView alphabetGrid;
    /* access modifiers changed from: private */
    public ChooseLetterAdapter chooseLetterAdapter;
    private CodewordPrefs codewordPrefs;
    private int currentOrientation;
    private int deviceSize = INFO_ORIENTATION;
    private Bundle extras;
    /* access modifiers changed from: private */
    public boolean hideClues;
    private LinearLayout lCreateButtons;
    /* access modifiers changed from: private */
    public int letterClicked = 0;
    /* access modifiers changed from: private */
    public ViewFlipper letterFlipper;
    private GridView letterGrid;
    /* access modifiers changed from: private */
    public LetterGridAdapter letterGridAdapter;
    /* access modifiers changed from: private */
    public short[] letterUseage = new short[26];
    private PuzzlesDbAdapter mDbHelper;
    private Long mRowId;
    private LinearLayout mainLayout;
    private int orientationLock = -1;
    /* access modifiers changed from: private */
    public short[] originalArray = new short[163];
    /* access modifiers changed from: private */
    public PuzzlesRow puzzlesRow;
    private boolean solvedByMe = false;
    /* access modifiers changed from: private */
    public char[] suppliedLetters = new char[26];
    private int unusableHeight = 0;
    /* access modifiers changed from: private */
    public ViewFlipper viewFlipper;
    private Button whatsLeft;
    private GridView wordGrid;
    /* access modifiers changed from: private */
    public WordsGridAdapter wordsGridAdapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.codewordPrefs = new CodewordPrefs(getApplicationContext());
        this.hideClues = this.codewordPrefs.getHideClues();
        this.mRowId = null;
        this.extras = getIntent().getExtras();
        if (this.extras != null) {
            this.mRowId = Long.valueOf(this.extras.getLong("puzzleRow"));
            this.deviceSize = this.extras.getInt("deviceSize");
            this.unusableHeight = this.extras.getInt("unusableHeight");
        }
        this.mDbHelper = new PuzzlesDbAdapter(this);
        this.mDbHelper.open();
        this.puzzlesRow = this.mDbHelper.fetchPuzzle(this.mRowId.longValue());
        if (!this.puzzlesRow.isPuzzlePlayable()) {
            Toast.makeText(getApplicationContext(), "Note, not all the letters of the alphabet are used in this puzzle!", (int) INFO_ORIENTATION).show();
        }
        if (!this.puzzlesRow.answers.contentEquals("") && !this.hideClues) {
            Toast.makeText(getApplicationContext(), "Hints are available for this puzzle", (int) INFO_ORIENTATION).show();
        }
        this.originalArray = this.puzzlesRow.getGridArray();
        for (int i = 0; i < 169; i += INFO_ORIENTATION) {
            short c = this.originalArray[i];
            if (c > 0) {
                short[] sArr = this.letterUseage;
                int i2 = c - INFO_ORIENTATION;
                sArr[i2] = (short) (sArr[i2] + 1);
            }
        }
        this.suppliedLetters = this.puzzlesRow.getSuppliedArray();
        this.mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        this.wordGrid = (GridView) findViewById(R.id.wordGrid);
        this.letterGrid = (GridView) findViewById(R.id.letterGrid);
        this.alphabetGrid = (GridView) findViewById(R.id.alphabetGrid);
        this.wordsGridAdapter = new WordsGridAdapter(this);
        this.wordsGridAdapter.setOriginalArray(this.originalArray, this.suppliedLetters, this.puzzlesRow.getGuessesArray(), this.deviceSize);
        this.wordGrid.setAdapter((ListAdapter) this.wordsGridAdapter);
        this.wordGrid.setOnItemClickListener(new WordGridListener());
        this.letterGridAdapter = new LetterGridAdapter(this, this.suppliedLetters, this.puzzlesRow.getGuessesArray(), this.deviceSize);
        this.letterGrid.setAdapter((ListAdapter) this.letterGridAdapter);
        this.letterGrid.setOnItemClickListener(new LetterGridListener());
        this.letterGrid.setOnItemLongClickListener(new LetterGridLongListener());
        this.chooseLetterAdapter = new ChooseLetterAdapter(this, this.letterUseage, this.deviceSize);
        this.whatsLeft = (Button) findViewById(R.id.whats_left);
        this.whatsLeft.setOnClickListener(new WhatsLeftListener());
        this.letterFlipper = (ViewFlipper) findViewById(R.id.vLetterFlipper);
        this.alphabetGrid.setAdapter((ListAdapter) this.chooseLetterAdapter);
        this.lCreateButtons = (LinearLayout) findViewById(R.id.lCreateButtons);
        this.lCreateButtons.setVisibility(8);
        new LongScreenTimeout(getApplicationContext(), 0);
        orientationSetup();
        ((GestureOverlayView) findViewById(R.id.gestures)).setVisibility(8);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        orientationSetup();
    }

    public void orientationSetup() {
        int i;
        this.orientationLock = this.codewordPrefs.getMainOrientation();
        if (this.orientationLock > 0) {
            setRequestedOrientation(this.orientationLock);
        }
        this.currentOrientation = getApplicationContext().getResources().getConfiguration().orientation;
        if (this.currentOrientation == HELP_ID) {
            this.letterGrid.setNumColumns(this.deviceSize == 0 ? 3 : 6);
            this.alphabetGrid.setNumColumns(this.deviceSize == 0 ? HELP_ID : 4);
        } else if (this.currentOrientation == INFO_ORIENTATION) {
            this.letterGrid.setNumColumns(this.deviceSize == 0 ? 7 : 8);
            GridView gridView = this.alphabetGrid;
            if (this.deviceSize == 0) {
                i = 5;
            } else {
                i = 6;
            }
            gridView.setNumColumns(i);
        }
        this.wordsGridAdapter.sizeGrid(this.currentOrientation, this.unusableHeight);
        this.mainLayout.setOrientation(this.currentOrientation);
        updateWhatsLeft();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new LongScreenTimeout(getApplicationContext(), INFO_ORIENTATION);
        savePuzzle();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        new LongScreenTimeout(getApplicationContext(), 0);
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        menu.add(0, (int) INFO_ORIENTATION, 0, (int) R.string.menu_unlock_orientation);
        menu.add(0, (int) HELP_ID, 0, (int) R.string.menu_help).setIcon(17301568);
        return result;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.getItem(0).setTitle(this.orientationLock >= 0 ? R.string.menu_unlock_orientation : R.string.menu_lock_orientation);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case INFO_ORIENTATION /*1*/:
                if (this.orientationLock <= 0) {
                    this.orientationLock = INFO_ORIENTATION;
                } else if (this.deviceSize > 0) {
                    this.orientationLock = -1;
                    setRequestedOrientation(-1);
                } else {
                    Toast.makeText(getApplicationContext(), "Orientation is locked to portrait on smaller screens", 0).show();
                }
                this.codewordPrefs.setMainOrientation(this.orientationLock);
                return INFO_ORIENTATION;
            case HELP_ID /*2*/:
                codewordHelp();
                return INFO_ORIENTATION;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class WordGridListener implements AdapterView.OnItemClickListener {
        public WordGridListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            int count = 0;
            if (PlayPuzzle.this.wordsGridAdapter.highlightLetter >= 0) {
                PlayPuzzle.this.wordsGridAdapter.highlightALetter(-1);
            }
            if (PlayPuzzle.this.originalArray[position] != 0) {
                for (int i = 0; i < 169; i += PlayPuzzle.INFO_ORIENTATION) {
                    if (PlayPuzzle.this.originalArray[i] == PlayPuzzle.this.originalArray[position]) {
                        count += PlayPuzzle.INFO_ORIENTATION;
                    }
                }
                PlayPuzzle.this.letterClicked = PlayPuzzle.this.originalArray[position];
                if (PlayPuzzle.this.suppliedLetters[PlayPuzzle.this.letterClicked - PlayPuzzle.INFO_ORIENTATION] == ' ') {
                    PlayPuzzle.this.chooseLetterAdapter.refreshArray(PlayPuzzle.this.letterGridAdapter.texts);
                    PlayPuzzle.this.alphabetGrid.setOnItemClickListener(new ChooseLetterListener());
                    PlayPuzzle.this.alphabetGrid.setOnItemLongClickListener(null);
                    PlayPuzzle.this.wordsGridAdapter.highlightALetter(PlayPuzzle.this.letterClicked);
                    PlayPuzzle.this.letterFlipper.setDisplayedChild(PlayPuzzle.INFO_ORIENTATION);
                }
            }
        }
    }

    public class LetterGridListener implements AdapterView.OnItemClickListener {
        public LetterGridListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (PlayPuzzle.this.suppliedLetters[position] == ' ') {
                PlayPuzzle.this.letterClicked = position + PlayPuzzle.INFO_ORIENTATION;
                PlayPuzzle.this.chooseLetterAdapter.refreshArray(PlayPuzzle.this.letterGridAdapter.texts);
                PlayPuzzle.this.alphabetGrid.setOnItemClickListener(new ChooseLetterListener());
                PlayPuzzle.this.alphabetGrid.setOnItemLongClickListener(null);
                PlayPuzzle.this.wordsGridAdapter.highlightALetter(position + PlayPuzzle.INFO_ORIENTATION);
                PlayPuzzle.this.letterFlipper.setDisplayedChild(PlayPuzzle.INFO_ORIENTATION);
            }
        }
    }

    public class LetterGridLongListener implements AdapterView.OnItemLongClickListener {
        public LetterGridLongListener() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View v, int position, long id) {
            short s = PlayPuzzle.this.originalArray[position];
            String s2 = "";
            if (PlayPuzzle.this.letterGridAdapter.texts[position] != ' ') {
                s2 = " (" + PlayPuzzle.this.letterGridAdapter.texts[position] + ") ";
            }
            Toast.makeText(PlayPuzzle.this.getApplicationContext(), String.valueOf(position + PlayPuzzle.INFO_ORIENTATION) + s2 + " occurs " + ((int) PlayPuzzle.this.letterUseage[position]) + " times", 0).show();
            return true;
        }
    }

    public class ViewClickListener implements View.OnClickListener {
        public ViewClickListener() {
        }

        public void onClick(View view) {
            PlayPuzzle.this.viewFlipper.setDisplayedChild(0);
        }
    }

    public class ChooseLetterListener implements AdapterView.OnItemClickListener {
        public ChooseLetterListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            PlayPuzzle.this.wordsGridAdapter.highlightALetter(-1);
            if (position > 26 || (position > 0 && PlayPuzzle.this.chooseLetterAdapter.activeLetters[position] == " ")) {
                PlayPuzzle.this.letterFlipper.setDisplayedChild(0);
                return;
            }
            for (int i = 0; i < 169; i += PlayPuzzle.INFO_ORIENTATION) {
                if (PlayPuzzle.this.originalArray[i] == PlayPuzzle.this.letterClicked) {
                    if (position == 0) {
                        PlayPuzzle.this.wordsGridAdapter.texts[i] = String.valueOf(PlayPuzzle.this.letterClicked);
                    } else {
                        PlayPuzzle.this.wordsGridAdapter.texts[i] = PlayPuzzle.this.chooseLetterAdapter.activeLetters[position];
                    }
                }
            }
            PlayPuzzle.this.wordsGridAdapter.notifyDataSetChanged();
            if (position == 0) {
                PlayPuzzle.this.letterGridAdapter.texts[PlayPuzzle.this.letterClicked - PlayPuzzle.INFO_ORIENTATION] = ' ';
            } else {
                PlayPuzzle.this.letterGridAdapter.texts[PlayPuzzle.this.letterClicked - PlayPuzzle.INFO_ORIENTATION] = PlayPuzzle.this.chooseLetterAdapter.activeLetters[position].charAt(0);
            }
            PlayPuzzle.this.letterGridAdapter.notifyDataSetChanged();
            PlayPuzzle.this.updateWhatsLeft();
            PlayPuzzle.this.letterFlipper.setDisplayedChild(0);
        }
    }

    public class WhatsLeftListener implements View.OnClickListener {
        public WhatsLeftListener() {
        }

        public void onClick(View view) {
            if (!PlayPuzzle.this.updateWhatsLeft()) {
                PlayPuzzle.this.alphabetGrid.setOnItemClickListener(new HighlightLetterListener());
                PlayPuzzle.this.alphabetGrid.setOnItemLongClickListener(new HighlightLetterLongListener());
                PlayPuzzle.this.chooseLetterAdapter.refreshWithDistribution(PlayPuzzle.this.letterGridAdapter.texts);
                PlayPuzzle.this.letterFlipper.setDisplayedChild(PlayPuzzle.INFO_ORIENTATION);
            }
        }
    }

    public class HighlightLetterListener implements AdapterView.OnItemClickListener {
        public HighlightLetterListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (position <= 0 || position >= 27) {
                PlayPuzzle.this.wordsGridAdapter.highlightALetter(-1);
                if (position == 27) {
                    PlayPuzzle.this.letterFlipper.setDisplayedChild(0);
                    return;
                }
                return;
            }
            PlayPuzzle.this.wordsGridAdapter.highlightALetter(position);
        }
    }

    public class HighlightLetterLongListener implements AdapterView.OnItemLongClickListener {
        public HighlightLetterLongListener() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (PlayPuzzle.this.hideClues) {
                return PlayPuzzle.INFO_ORIENTATION;
            }
            if (PlayPuzzle.this.puzzlesRow.answers.contentEquals("")) {
                Toast.makeText(PlayPuzzle.this.getApplicationContext(), "No solution provided", 0).show();
            } else {
                Toast.makeText(PlayPuzzle.this.getApplicationContext(), "Hint: try " + PlayPuzzle.this.puzzlesRow.getAnswersArray()[position - PlayPuzzle.INFO_ORIENTATION], 0).show();
            }
            return PlayPuzzle.INFO_ORIENTATION;
        }
    }

    private void codewordHelp() {
        Intent it = new Intent(this, CodeWordHelp.class);
        it.putExtra("hyperlink", "#Playing a puzzle|outline");
        startActivityForResult(it, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean updateWhatsLeft() {
        int unused = 26;
        char[] s = new char[26];
        for (int i = 0; i < 26; i += INFO_ORIENTATION) {
            if (this.letterGridAdapter.texts[i] != ' ') {
                unused--;
            }
        }
        this.whatsLeft.setTextSize((float) (this.deviceSize == 0 ? 13 : 16));
        if (unused == 0) {
            if (this.whatsLeft.getText().toString().contains("Done")) {
                Toast.makeText(getApplicationContext(), "Solution remembered", 0).show();
                this.puzzlesRow.setAnswersFromArray(this.letterGridAdapter.texts);
                this.mDbHelper.updatePuzzle(this.puzzlesRow);
            }
            if (this.puzzlesRow.answers.contentEquals("")) {
                this.whatsLeft.setText("Done! Press to keep as your solution");
                this.solvedByMe = true;
                return false;
            }
            for (int i2 = 0; i2 < 26; i2 += INFO_ORIENTATION) {
                if (this.puzzlesRow.getAnswersArray()[i2] != this.letterGridAdapter.texts[i2]) {
                    this.whatsLeft.setText(" Your solution appears wrong");
                    this.solvedByMe = false;
                    return false;
                }
            }
            this.whatsLeft.setText("Completed" + (this.solvedByMe ? " and saved" : ", the solution is correct!"));
            return INFO_ORIENTATION;
        }
        if (this.deviceSize == 0 && this.currentOrientation == HELP_ID) {
            this.whatsLeft.setTextSize(12.0f);
            int j = 0;
            for (int i3 = 0; i3 < 26; i3 += INFO_ORIENTATION) {
                if (this.letterGridAdapter.texts[i3] == ' ') {
                    s[j] = (char) (i3 + 64);
                    j += INFO_ORIENTATION;
                }
            }
        } else {
            this.whatsLeft.setTextSize(16.0f);
            for (int i4 = 0; i4 < 26; i4 += INFO_ORIENTATION) {
                s[i4] = (char) (i4 + 65);
            }
            for (int i5 = 0; i5 < 26; i5 += INFO_ORIENTATION) {
                if (this.letterGridAdapter.texts[i5] != ' ') {
                    s[this.letterGridAdapter.texts[i5] - 'A'] = ' ';
                }
            }
        }
        this.whatsLeft.setText(String.valueOf(this.deviceSize == 0 ? "" : "Unused: ") + String.copyValueOf(s));
        return false;
    }

    private final void savePuzzle() {
        this.puzzlesRow.setGuessesFromArray(this.letterGridAdapter.texts);
        this.mDbHelper.updatePuzzle(this.puzzlesRow);
    }
}
