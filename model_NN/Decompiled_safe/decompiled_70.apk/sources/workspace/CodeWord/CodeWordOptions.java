package workspace.CodeWord;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.io.File;

public class CodeWordOptions {
    /* access modifiers changed from: private */
    public CodewordPrefs codewordPrefs;
    Context context;
    /* access modifiers changed from: private */
    public Button createHere;
    /* access modifiers changed from: private */
    public ListView dirView;
    /* access modifiers changed from: private */
    public FileArrayAdapter fileArrayAdapter;
    private Button filePickerCancel;
    private CheckBox hideClues;
    private CheckBox longTimeout;
    /* access modifiers changed from: private */
    public TextView sdFolder;
    View view;
    /* access modifiers changed from: private */
    public ViewFlipper viewFlipper;

    public CodeWordOptions(final Context context2, LinearLayout view2, int deviceSize) {
        this.codewordPrefs = new CodewordPrefs(context2.getApplicationContext());
        this.context = context2;
        this.view = view2;
        this.viewFlipper = (ViewFlipper) view2.findViewById(R.id.listFlipper);
        this.dirView = (ListView) view2.findViewById(R.id.filePickerList);
        this.hideClues = (CheckBox) view2.findViewById(R.id.cHideClues);
        this.longTimeout = (CheckBox) view2.findViewById(R.id.cLongTimeout);
        this.sdFolder = (TextView) view2.findViewById(R.id.tSaveFolder);
        this.sdFolder.setOnClickListener(new sdFolderListener());
        this.createHere = (Button) view2.findViewById(R.id.bCreateHere);
        this.createHere.setOnClickListener(new createHereListener());
        this.filePickerCancel = (Button) view2.findViewById(R.id.bFilePickerCancel);
        this.hideClues.setChecked(this.codewordPrefs.getHideClues());
        this.longTimeout.setChecked(this.codewordPrefs.getScreenTimeout() > 0);
        this.sdFolder.setText(this.codewordPrefs.getSDFolder());
        this.viewFlipper.setDisplayedChild(2);
        this.filePickerCancel.setOnClickListener(new FilePickerCancelListener());
        this.hideClues.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                CodeWordOptions.this.codewordPrefs.setHideClues(isChecked);
            }
        });
        this.longTimeout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    CodeWordOptions.this.codewordPrefs.setScreenTimeout(1);
                    new LongScreenTimeout(context2, 0);
                    return;
                }
                new LongScreenTimeout(context2, 1);
                CodeWordOptions.this.codewordPrefs.setScreenTimeout(0);
            }
        });
    }

    public class ScreenSizeListener implements AdapterView.OnItemSelectedListener {
        public ScreenSizeListener() {
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            CodeWordOptions.this.codewordPrefs.setDeviceSize(position - 1);
        }

        public void onNothingSelected(AdapterView parent) {
        }
    }

    public class sdFolderListener implements View.OnClickListener {
        public sdFolderListener() {
        }

        public void onClick(View view) {
            CodeWordOptions.this.viewFlipper.setDisplayedChild(4);
            CodeWordOptions.this.dirView.setOnItemClickListener(new sdDirListener());
            CodeWordOptions.this.fileArrayAdapter = new FileArrayAdapter(CodeWordOptions.this.context, CodeWordOptions.this.dirView, "/sdcard/", true, "", CodeWordOptions.this.createHere, true);
        }
    }

    public class sdDirListener implements AdapterView.OnItemClickListener {
        public sdDirListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int position, long arg3) {
            File f = CodeWordOptions.this.fileArrayAdapter.getFile(position);
            if (f.getName().matches("CodeWord Games")) {
                CodeWordOptions.this.sdFolder.setText(f.getPath());
                CodeWordOptions.this.codewordPrefs.setSDFolder(CodeWordOptions.this.sdFolder.getText().toString());
                Toast.makeText(CodeWordOptions.this.context, "Folder selected as your save folder", 0).show();
                CodeWordOptions.this.viewFlipper.setDisplayedChild(2);
            }
            CodeWordOptions.this.fileArrayAdapter.nextList(position);
        }
    }

    public class createHereListener implements View.OnClickListener {
        public createHereListener() {
        }

        public void onClick(View view) {
            String path = String.valueOf(CodeWordOptions.this.fileArrayAdapter.getActiveFolder().getAbsolutePath()) + "/CodeWord Games";
            if (new File(path).mkdir()) {
                CodeWordOptions.this.sdFolder.setText(path);
                CodeWordOptions.this.codewordPrefs.setSDFolder(path);
                Toast.makeText(CodeWordOptions.this.context, "Folder created here", 0).show();
                CodeWordOptions.this.viewFlipper.setDisplayedChild(2);
                return;
            }
            Toast.makeText(CodeWordOptions.this.context, "Folder Codeword Games aleady exists here", 0).show();
        }
    }

    public class FilePickerCancelListener implements View.OnClickListener {
        public FilePickerCancelListener() {
        }

        public void onClick(View view) {
            CodeWordOptions.this.viewFlipper.setDisplayedChild(2);
        }
    }
}
