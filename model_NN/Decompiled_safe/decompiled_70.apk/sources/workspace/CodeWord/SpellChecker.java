package workspace.CodeWord;

import android.app.Activity;
import android.content.res.AssetManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewFlipper;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class SpellChecker {
    Activity activity;
    Button bDone;
    EditText eWordToSpell;
    GoogleResult googleResult;
    ViewFlipper letterFlipper;
    TextView tResult;
    XMLHandler xmlHandler;

    public SpellChecker(Activity activity2) {
        this.activity = activity2;
        this.letterFlipper = (ViewFlipper) activity2.findViewById(R.id.vLetterFlipper);
        this.eWordToSpell = (EditText) activity2.findViewById(R.id.eWordToSpell);
        this.eWordToSpell.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 6) {
                    return false;
                }
                SpellChecker.this.askGoogle(SpellChecker.this.eWordToSpell.getText().toString());
                return false;
            }
        });
        this.eWordToSpell.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                SpellChecker.this.eWordToSpell.setText("");
            }
        });
        this.tResult = (TextView) activity2.findViewById(R.id.tResult);
        this.bDone = (Button) activity2.findViewById(R.id.bSpellingDone);
        this.bDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SpellChecker.this.letterFlipper.setDisplayedChild(0);
            }
        });
    }

    public boolean isWordCorrect(String word) {
        askGoogle(word);
        return this.googleResult.checkResult().booleanValue();
    }

    public void checkSpelling(String word) {
        this.letterFlipper.setDisplayedChild(2);
        this.tResult.setText("");
        if (word.length() > 2) {
            this.eWordToSpell.setText(word);
            askGoogle(word);
            if (this.googleResult.checkResult().booleanValue()) {
                this.tResult.setText("Word in Google dictionary");
                return;
            }
            String substitutes = "Word not in dictionary, try:\n";
            Iterator<String> it = this.googleResult.getSubstitutes().iterator();
            while (it.hasNext()) {
                substitutes = String.valueOf(substitutes) + it.next() + " ";
            }
            this.tResult.setText(substitutes);
            return;
        }
        this.eWordToSpell.setText("");
    }

    /* access modifiers changed from: private */
    public void askGoogle(String word) {
        StringBuffer requestXML = new StringBuffer();
        requestXML.append("<spellrequest textalreadyclipped=\"0\" ignoredups=\"1\" ignoredigits=\"1\" ignoreallcaps=\"0\"><text>");
        requestXML.append(word);
        requestXML.append("</text></spellrequest>");
        this.xmlHandler = new XMLHandler();
        try {
            URLConnection conn = new URL("https://www.google.com/tbproxy/spell?lang=en&hl=en").openConnection();
            conn.setDoOutput(true);
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            out.write(requestXML.toString());
            out.close();
            this.googleResult = this.xmlHandler.parseSpellcheck(conn.getInputStream());
        } catch (Exception e) {
        }
    }

    private class GoogleResult {
        String charsChecked;
        String clipped;
        String error;
        String length;
        String offset;
        String score;
        String substitutes;

        private GoogleResult() {
        }

        /* synthetic */ GoogleResult(SpellChecker spellChecker, GoogleResult googleResult) {
            this();
        }

        public Boolean checkResult() {
            return this.substitutes == null;
        }

        public ArrayList<String> getSubstitutes() {
            ArrayList<String> list = new ArrayList<>();
            if (this.substitutes == "") {
                return null;
            }
            String[] s = this.substitutes.split("\t");
            for (String add : s) {
                list.add(add);
            }
            return list;
        }
    }

    public class XMLHandler extends DefaultHandler {
        AssetManager assetManager;
        Boolean currentElement = false;
        String currentValue = null;
        GoogleResult result;
        SAXParser sp;
        SAXParserFactory spf;
        XMLReader xr;

        public XMLHandler() {
        }

        /* access modifiers changed from: private */
        public GoogleResult parseSpellcheck(InputStream stream) {
            this.result = new GoogleResult(SpellChecker.this, null);
            try {
                this.spf = SAXParserFactory.newInstance();
                this.sp = this.spf.newSAXParser();
                this.xr = this.sp.getXMLReader();
                this.xr.setContentHandler(this);
                this.xr.parse(new InputSource(stream));
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        return null;
                    }
                }
                return this.result;
            } catch (Exception e2) {
                return null;
            }
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            this.currentElement = true;
            if (localName.equalsIgnoreCase("c")) {
                this.result.offset = attributes.getValue("o");
                this.result.length = attributes.getValue("l");
                this.result.score = attributes.getValue("s");
            } else if (localName.equalsIgnoreCase("spellresult")) {
                this.result.error = attributes.getValue("error");
                this.result.clipped = attributes.getValue("clipped");
                this.result.charsChecked = attributes.getValue("charschecked");
            }
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            if (this.currentElement.booleanValue()) {
                this.currentValue = new String(ch, start, length);
                this.currentElement = false;
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            this.currentElement = false;
            if (localName.equalsIgnoreCase("c")) {
                this.result.substitutes = this.currentValue;
            }
            this.currentValue = "";
        }
    }
}
