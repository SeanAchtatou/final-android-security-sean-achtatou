package workspace.CodeWord;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;
import java.io.File;

public class CodewordPrefs {
    private Context context;
    private SharedPreferences.Editor edit = this.mPrefs.edit();
    private SharedPreferences mPrefs;

    public CodewordPrefs(Context context2) {
        this.context = context2;
        this.mPrefs = context2.getSharedPreferences("codeWord", 0);
    }

    public int getMainOrientation() {
        return this.mPrefs.getInt("mainOrientation", -1);
    }

    public void setMainOrientation(int mainOrientation) {
        this.edit.putInt("mainOrientation", mainOrientation);
        this.edit.commit();
    }

    public int getDeviceSize() {
        return this.mPrefs.getInt("deviceSize", -1);
    }

    public void setDeviceSize(int deviceSize) {
        this.edit.putInt("deviceSize", deviceSize);
        this.edit.commit();
    }

    public void setScreenTimeout(int screenTimeout) {
        this.edit.putInt("screenTimeout", screenTimeout);
        this.edit.commit();
    }

    public int getScreenTimeout() {
        return this.mPrefs.getInt("screenTimeout", 0);
    }

    public void setHideClues(boolean hideClues) {
        this.edit.putBoolean("hideClues", hideClues);
        this.edit.commit();
    }

    public boolean getHideClues() {
        return this.mPrefs.getBoolean("hideClues", false);
    }

    public void setSaveEmailPassword(boolean saveEmailPassword) {
        this.edit.putBoolean("saveEmailPassword", saveEmailPassword);
        this.edit.commit();
    }

    public boolean getSaveEmailPassword() {
        return this.mPrefs.getBoolean("saveEmailPassword", false);
    }

    public void setEmailPassword(String emailPassword) {
        this.edit.putString("emailPassword", emailPassword);
        this.edit.commit();
    }

    public String getEmailPassword() {
        return this.mPrefs.getString("emailPassword", "");
    }

    public void setEmailServer(String emailServer) {
        this.edit.putString("emailServer", emailServer);
        this.edit.commit();
    }

    public String getEmailServer() {
        return this.mPrefs.getString("emailServer", "");
    }

    public void setUsername(String userName) {
        this.edit.putString("userName", userName);
        this.edit.commit();
    }

    public String getUsername() {
        return this.mPrefs.getString("userName", "");
    }

    public void setEmailFrom(String fromEmail) {
        this.edit.putString("emailFrom", fromEmail);
        this.edit.commit();
    }

    public String getEmailFrom() {
        return this.mPrefs.getString("emailFrom", "");
    }

    public void setSDFolder(String sdFolder) {
        this.edit.putString("sdFolder", sdFolder);
        this.edit.commit();
    }

    public String getSDFolder() {
        String s = this.mPrefs.getString("sdFolder", "");
        if (s.length() == 0) {
            Toast.makeText(this.context, "The save folder location is required", 0).show();
            return s;
        } else if (new File(s).isDirectory()) {
            return s;
        } else {
            Toast.makeText(this.context, "Sorry, the save folder is missing", 0).show();
            return "";
        }
    }
}
