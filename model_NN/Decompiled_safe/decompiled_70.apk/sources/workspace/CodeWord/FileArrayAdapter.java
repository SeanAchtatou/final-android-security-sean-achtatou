package workspace.CodeWord;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FileArrayAdapter extends BaseAdapter {
    private File activeFolder;
    private Context context;
    private Button createHereButton;
    private boolean createVisible;
    private List<Option> dir;
    private File[] dirs;
    private boolean dirsOnly;
    private String fileSuffix;
    private List<Option> fls;
    private LayoutInflater inflater;
    private Option o;
    private String startFolder;
    private TextView t1;
    private TextView t2;

    public FileArrayAdapter(Context context2, ListView dirView, String startFolder2, boolean dirsOnly2, String fileSuffix2, Button createHereButton2, Boolean createVisible2) {
        this.context = context2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.dirsOnly = dirsOnly2;
        this.fileSuffix = fileSuffix2;
        this.startFolder = new File(startFolder2).getName();
        this.createHereButton = createHereButton2;
        this.createVisible = createVisible2.booleanValue();
        fill(new File(startFolder2));
        dirView.setAdapter((ListAdapter) this);
    }

    public File getFile(int position) {
        this.o = this.dir.get(position);
        File f = new File(this.o.getPath());
        if (!f.isDirectory() || this.dirsOnly) {
            return f;
        }
        return null;
    }

    public File getActiveFolder() {
        return this.activeFolder;
    }

    /* access modifiers changed from: package-private */
    public boolean fill(File f) {
        this.activeFolder = f;
        this.createHereButton.setVisibility(this.createVisible ? 0 : 8);
        this.dirs = f.listFiles();
        this.dir = new ArrayList();
        this.fls = new ArrayList();
        try {
            for (File ff : this.dirs) {
                if (ff.isDirectory()) {
                    this.dir.add(new Option(ff.getName(), "Folder", ff.getAbsolutePath()));
                }
                if (ff.getName().matches("CodeWord Games") && this.createVisible) {
                    this.createHereButton.setVisibility(8);
                }
                if (!this.dirsOnly && (ff.getName().contains(this.fileSuffix) || this.fileSuffix.length() == 0)) {
                    this.fls.add(new Option(ff.getName(), "File Size: " + ff.length(), ff.getAbsolutePath()));
                }
            }
        } catch (Exception e) {
        }
        Collections.sort(this.dir);
        Collections.sort(this.fls);
        this.dir.addAll(this.fls);
        notifyDataSetChanged();
        if (!f.getName().equalsIgnoreCase(this.startFolder)) {
            this.dir.add(0, new Option("..", "Parent Directory", f.getParent()));
        }
        return false;
    }

    public void nextList(int position) {
        this.o = this.dir.get(position);
        if (this.o.getData().equalsIgnoreCase("folder") || this.o.getData().equalsIgnoreCase("parent directory")) {
            fill(new File(this.o.getPath()));
        }
    }

    public int getCount() {
        return this.dir.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        View rowView;
        if (convertView == null) {
            rowView = this.inflater.inflate((int) R.layout.dir_row, (ViewGroup) null);
        } else {
            rowView = convertView;
        }
        this.o = this.dir.get(position);
        this.t1 = (TextView) rowView.findViewById(R.id.textView1);
        this.t2 = (TextView) rowView.findViewById(R.id.textView2);
        if (this.t1 != null) {
            this.t1.setText(this.o.getName());
            if (this.o.getData().equalsIgnoreCase("folder")) {
                this.t1.setTextColor(-7829368);
            } else {
                this.t1.setTextColor(-1);
            }
            if (this.o.getName().equalsIgnoreCase("..")) {
                this.t1.setTextColor(-65536);
            }
        }
        if (this.t2 != null) {
            this.t2.setText(this.o.getData());
        }
        return rowView;
    }

    class Option implements Comparable<Option> {
        private String data;
        private String name;
        private String path;

        public Option(String n, String d, String p) {
            this.name = n;
            this.data = d;
            this.path = p;
        }

        public String getName() {
            return this.name;
        }

        public String getData() {
            return this.data;
        }

        public String getPath() {
            return this.path;
        }

        public int compareTo(Option o) {
            if (this.name != null) {
                return this.name.toLowerCase().compareTo(o.getName().toLowerCase());
            }
            throw new IllegalArgumentException();
        }
    }
}
