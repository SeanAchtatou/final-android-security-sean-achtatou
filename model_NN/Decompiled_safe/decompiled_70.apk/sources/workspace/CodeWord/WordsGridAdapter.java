package workspace.CodeWord;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WordsGridAdapter extends BaseAdapter {
    private static int cellHeight;
    private static int cellWidth;
    private Activity activity;
    private int currentPosition = -1;
    private int deviceSize;
    public int highlightLetter = -1;
    private Boolean noOrCharFlag = true;
    public short[] originalData;
    public char[] suppliedLetters;
    public String[] texts = new String[169];

    public WordsGridAdapter(Activity activity2) {
        this.activity = activity2;
    }

    public void setOriginalArray(short[] originalData2, char[] suppliedLetters2, char[] savedLetters, int deviceSize2) {
        this.suppliedLetters = suppliedLetters2;
        this.originalData = originalData2;
        this.deviceSize = deviceSize2;
        for (int i = 0; i < 169; i++) {
            if (originalData2[i] == 0) {
                this.texts[i] = " ";
            } else if (savedLetters[originalData2[i] - 1] != ' ') {
                this.texts[i] = String.valueOf(savedLetters[originalData2[i] - 1]);
            } else {
                this.texts[i] = Integer.toString(originalData2[i]);
            }
        }
    }

    public void sizeGrid(int orientation, int unusableHeight) {
        DisplayMetrics dm = new DisplayMetrics();
        GridView wordGrid = (GridView) this.activity.findViewById(R.id.wordGrid);
        FrameLayout wordGridLayout = (FrameLayout) this.activity.findViewById(R.id.wordGridLayout);
        this.activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        if (orientation == 2) {
            cellHeight = ((dm.heightPixels - unusableHeight) - 20) / 13;
            cellWidth = (int) ((float) (((double) cellHeight) * 1.1d));
            wordGrid.setLayoutParams(new FrameLayout.LayoutParams((cellWidth * 13) + 28, dm.heightPixels));
            wordGridLayout.setLayoutParams(new LinearLayout.LayoutParams((cellWidth * 13) + 28, dm.heightPixels));
        } else {
            cellWidth = (dm.widthPixels - 13) / 13;
            cellHeight = (int) ((float) (((double) cellWidth) * 0.9d));
            wordGrid.setLayoutParams(new FrameLayout.LayoutParams(dm.widthPixels, (cellHeight * 13) + 28));
            wordGridLayout.setLayoutParams(new LinearLayout.LayoutParams(dm.widthPixels, (cellHeight * 13) + 28));
        }
        wordGrid.setVerticalSpacing(1);
    }

    public void flipGrid(boolean noOrCharFlag2) {
        this.noOrCharFlag = Boolean.valueOf(noOrCharFlag2);
        notifyDataSetChanged();
    }

    public void highlightPosition(int currentPosition2) {
        this.currentPosition = currentPosition2;
        notifyDataSetChanged();
    }

    public void highlightALetter(int highlightLetter2) {
        this.highlightLetter = highlightLetter2;
        notifyDataSetChanged();
    }

    public int getCount() {
        return 169;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv;
        if (convertView == null) {
            tv = new TextView(this.activity);
            tv.setGravity(17);
        } else {
            tv = (TextView) convertView;
        }
        tv.setLayoutParams(new AbsListView.LayoutParams(cellWidth, cellHeight));
        if (this.texts[position].matches(" ")) {
            tv.setBackgroundColor(-16777216);
            tv.setText(" ");
        } else {
            tv.setBackgroundColor(-1);
            tv.setTypeface(Typeface.DEFAULT);
            if (!isAnInt(this.texts[position])) {
                tv.setTextColor(-16777216);
                tv.setText(this.texts[position]);
            } else if (this.noOrCharFlag.booleanValue()) {
                tv.setText(this.texts[position]);
                tv.setTextColor(-3355444);
            } else {
                tv.setText(String.valueOf((char) (Short.parseShort(this.texts[position]) + 64)));
                tv.setTextColor(-16777216);
            }
            if (this.suppliedLetters[this.originalData[position] - 1] != ' ') {
                tv.setTypeface(Typeface.DEFAULT_BOLD);
            }
        }
        if (position == this.currentPosition) {
            tv.setBackgroundColor(-65536);
        }
        if (this.originalData[position] == this.highlightLetter) {
            tv.setBackgroundColor(-65536);
        }
        return tv;
    }

    public boolean isAnInt(String i) {
        try {
            Integer.parseInt(i);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
