package workspace.CodeWord;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class PuzzleEditor extends Activity {
    private static final int HELP_ID = 3;
    private static final int INFO_ORIENTATION = 2;
    private static final int INFO_WORDS = 1;
    private GridView alphabetGrid;
    /* access modifiers changed from: private */
    public Button bEntryCompleted;
    /* access modifiers changed from: private */
    public Button bHOrV;
    /* access modifiers changed from: private */
    public ChooseLetterAdapter chooseLetterAdapter;
    private CodewordPrefs codewordPrefs;
    private int currentOrientation;
    /* access modifiers changed from: private */
    public Drawable dDownArrow;
    /* access modifiers changed from: private */
    public Drawable dRightArrow;
    private int deviceSize = INFO_WORDS;
    /* access modifiers changed from: private */
    public boolean gesturesFlag = false;
    /* access modifiers changed from: private */
    public GestureOverlayView gesturesView;
    /* access modifiers changed from: private */
    public boolean hOrVFlag = true;
    final Handler handler;
    private final String[] headings;
    /* access modifiers changed from: private */
    public String lastPrediction = "";
    /* access modifiers changed from: private */
    public short letterChosen = 0;
    /* access modifiers changed from: private */
    public short letterClicked = 0;
    /* access modifiers changed from: private */
    public ViewFlipper letterFlipper;
    private GridView letterGrid;
    /* access modifiers changed from: private */
    public LetterGridAdapter letterGridAdapter;
    private PuzzlesDbAdapter mDbHelper;
    /* access modifiers changed from: private */
    public GestureLibrary mLibrary;
    private Long mRowId;
    private LinearLayout mainLayout;
    /* access modifiers changed from: private */
    public boolean notifyAllUsed = true;
    private int orientationLock = -1;
    /* access modifiers changed from: private */
    public short[] originalArray = new short[163];
    /* access modifiers changed from: private */
    public int puzzleMode = INFO_WORDS;
    /* access modifiers changed from: private */
    public PuzzlesRow puzzlesRow;
    private Button spaceBar;
    /* access modifiers changed from: private */
    public SpellChecker spellChecker;
    /* access modifiers changed from: private */
    public ProgressDialog spellingProgressDialog;
    /* access modifiers changed from: private */
    public String spellingString;
    private SpellingThread spellingThread;
    /* access modifiers changed from: private */
    public char[] suppliedLetters = new char[26];
    /* access modifiers changed from: private */
    public ScrollView svWhatsLeft;
    private int unusableHeight;
    /* access modifiers changed from: private */
    public ViewFlipper viewFlipper;
    private GridView wordGrid;
    /* access modifiers changed from: private */
    public WordsGridAdapter wordsGridAdapter;

    public PuzzleEditor() {
        String[] strArr = new String[HELP_ID];
        strArr[0] = "Creating puzzle";
        strArr[INFO_WORDS] = "Transcribing puzzle";
        strArr[INFO_ORIENTATION] = "Amending puzzle";
        this.headings = strArr;
        this.unusableHeight = 0;
        this.handler = new Handler() {
            public void handleMessage(Message msg) {
                if (msg.what == 0) {
                    PuzzleEditor.this.spellingProgressDialog.setMessage(PuzzleEditor.this.spellingString);
                    return;
                }
                if (PuzzleEditor.this.spellingProgressDialog != null) {
                    PuzzleEditor.this.spellingProgressDialog.dismiss();
                }
                AlertDialog.Builder ad = new AlertDialog.Builder(PuzzleEditor.this);
                ad.setMessage(PuzzleEditor.this.spellingString);
                ad.setNeutralButton("Done", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                ad.show();
            }
        };
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        this.codewordPrefs = new CodewordPrefs(getApplicationContext());
        this.mRowId = null;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mRowId = Long.valueOf(extras.getLong("puzzleRow"));
            this.deviceSize = extras.getInt("deviceSize");
            this.unusableHeight = extras.getInt("unusableHeight");
        }
        this.mDbHelper = new PuzzlesDbAdapter(this);
        this.mDbHelper.open();
        this.puzzlesRow = this.mDbHelper.fetchPuzzle(this.mRowId.longValue());
        this.puzzleMode = this.puzzlesRow.creator.matches("wip") ? 0 : INFO_WORDS;
        if (this.puzzleMode == INFO_WORDS && this.puzzlesRow.answers.length() > 0) {
            this.puzzleMode = INFO_ORIENTATION;
        }
        setTitle(this.headings[this.puzzleMode]);
        this.originalArray = this.puzzlesRow.getGridArray();
        this.suppliedLetters = this.puzzlesRow.getSuppliedArray();
        this.svWhatsLeft = (ScrollView) findViewById(R.id.svWhatsLeft);
        this.spaceBar = (Button) findViewById(R.id.whats_left);
        this.spaceBar.setTextSize((float) (this.deviceSize == 0 ? 16 : 18));
        this.spaceBar.setGravity(17);
        this.spaceBar.setText("Blank Square");
        this.spaceBar.setOnClickListener(new spaceBarListener(this, null));
        this.mainLayout = (LinearLayout) findViewById(R.id.mainLayout);
        this.wordGrid = (GridView) findViewById(R.id.wordGrid);
        this.wordsGridAdapter = new WordsGridAdapter(this);
        char[] startingLetters = new char[26];
        for (int i = 0; i < 26; i += INFO_WORDS) {
            startingLetters[i] = this.suppliedLetters[i];
        }
        this.wordsGridAdapter.setOriginalArray(this.originalArray, this.suppliedLetters, startingLetters, this.deviceSize);
        this.wordGrid.setAdapter((ListAdapter) this.wordsGridAdapter);
        this.wordGrid.setOnItemClickListener(new WordGridListener());
        this.wordGrid.setOnItemLongClickListener(new WordGridLongListener());
        this.letterGrid = (GridView) findViewById(R.id.letterGrid);
        this.letterGridAdapter = new LetterGridAdapter(this, this.suppliedLetters, startingLetters, this.deviceSize);
        this.letterGrid.setAdapter((ListAdapter) this.letterGridAdapter);
        this.letterGrid.setOnItemClickListener(new LetterGridListener());
        this.letterGrid.setOnItemLongClickListener(new LetterGridLongListener());
        this.letterFlipper = (ViewFlipper) findViewById(R.id.vLetterFlipper);
        this.chooseLetterAdapter = new ChooseLetterAdapter(this, null, this.deviceSize);
        this.alphabetGrid = (GridView) findViewById(R.id.alphabetGrid);
        this.alphabetGrid.setAdapter((ListAdapter) this.chooseLetterAdapter);
        this.alphabetGrid.setOnItemClickListener(new ChooseLetterListener());
        this.currentOrientation = getApplicationContext().getResources().getConfiguration().orientation;
        this.bHOrV = (Button) findViewById(R.id.bHOrV);
        if (this.puzzleMode == 0) {
            this.letterGridAdapter.flipGrid(false);
            this.wordsGridAdapter.flipGrid(false);
        } else {
            this.bHOrV.setVisibility(8);
        }
        this.bEntryCompleted = (Button) findViewById(R.id.bEntryCompleted);
        this.bEntryCompleted.setOnClickListener(new entryCompletedListener());
        this.bEntryCompleted.setVisibility(8);
        if (this.letterGridAdapter.updateLettersUsed(this.wordsGridAdapter.originalData)) {
            Toast.makeText(getApplicationContext(), "All letters are used", 0).show();
            if (this.puzzleMode == 0) {
                this.bEntryCompleted.setVisibility(0);
            }
        }
        Resources res = getResources();
        this.dRightArrow = res.getDrawable(R.drawable.down_arrow);
        this.dDownArrow = res.getDrawable(R.drawable.right_arrow);
        this.bHOrV.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!PuzzleEditor.this.gesturesFlag) {
                    PuzzleEditor.this.hOrVFlag = !PuzzleEditor.this.hOrVFlag;
                    PuzzleEditor.this.bHOrV.setBackgroundDrawable(PuzzleEditor.this.hOrVFlag ? PuzzleEditor.this.dDownArrow : PuzzleEditor.this.dRightArrow);
                    return;
                }
                PuzzleEditor.this.takeGesturesAction("");
            }
        });
        this.bHOrV.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View v) {
                PuzzleEditor.this.gesturesFlag = !PuzzleEditor.this.gesturesFlag;
                PuzzleEditor.this.lastPrediction = "";
                PuzzleEditor.this.gesturesView.setVisibility(PuzzleEditor.this.gesturesFlag ? 0 : 8);
                Toast.makeText(PuzzleEditor.this.getApplicationContext(), "Gestures " + (PuzzleEditor.this.gesturesFlag ? "on" : "off"), 0).show();
                return false;
            }
        });
        this.letterGridAdapter.updateLettersUsed(this.wordsGridAdapter.originalData);
        this.wordsGridAdapter.highlightPosition(0);
        new LongScreenTimeout(getApplicationContext(), 0);
        orientationSetup();
        this.mLibrary = GestureLibraries.fromRawResource(this, R.raw.gestures);
        if (!this.mLibrary.load()) {
            finish();
        }
        this.gesturesView = (GestureOverlayView) findViewById(R.id.gestures);
        this.gesturesView.addOnGesturePerformedListener(new OnEditGestureListener());
        this.gesturesView.setVisibility(8);
        this.spellChecker = new SpellChecker(this);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        orientationSetup();
    }

    public void orientationSetup() {
        int i;
        this.orientationLock = this.codewordPrefs.getMainOrientation();
        if (this.orientationLock > 0) {
            setRequestedOrientation(this.orientationLock);
        }
        this.currentOrientation = getApplicationContext().getResources().getConfiguration().orientation;
        if (this.currentOrientation == INFO_ORIENTATION) {
            this.letterGrid.setNumColumns(this.deviceSize == 0 ? HELP_ID : 6);
            this.alphabetGrid.setNumColumns(this.deviceSize == 0 ? INFO_ORIENTATION : 4);
        } else if (this.currentOrientation == INFO_WORDS) {
            this.letterGrid.setNumColumns(this.deviceSize == 0 ? 7 : 8);
            GridView gridView = this.alphabetGrid;
            if (this.deviceSize == 0) {
                i = 5;
            } else {
                i = 6;
            }
            gridView.setNumColumns(i);
        }
        this.wordsGridAdapter.sizeGrid(this.currentOrientation, this.unusableHeight);
        this.mainLayout.setOrientation(this.currentOrientation);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        new LongScreenTimeout(getApplicationContext(), INFO_WORDS);
        savePuzzle();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        new LongScreenTimeout(getApplicationContext(), 0);
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        menu.add(0, (int) INFO_WORDS, 0, (int) R.string.menu_list_words);
        menu.add(0, (int) INFO_ORIENTATION, 0, (int) R.string.menu_unlock_orientation);
        menu.add(0, (int) HELP_ID, 0, (int) R.string.menu_help).setIcon(17301568);
        return result;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.getItem(INFO_WORDS).setTitle(this.orientationLock >= 0 ? R.string.menu_unlock_orientation : R.string.menu_lock_orientation);
        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case INFO_WORDS /*1*/:
                AlertDialog.Builder ad = new AlertDialog.Builder(this);
                ad.setMessage("Would you like to spellcheck your words using Google?  Words not found are followed with '?'");
                ad.setPositiveButton("Yes please", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (PuzzleEditor.this.isInternetOn()) {
                            PuzzleEditor.this.listWordsUsed(true);
                        }
                    }
                });
                ad.setNegativeButton("No thanks, too slow", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        PuzzleEditor.this.listWordsUsed(false);
                    }
                });
                ad.show();
                return INFO_WORDS;
            case INFO_ORIENTATION /*2*/:
                if (this.orientationLock <= 0) {
                    this.orientationLock = INFO_WORDS;
                } else if (this.deviceSize > 0) {
                    this.orientationLock = -1;
                    setRequestedOrientation(-1);
                } else {
                    Toast.makeText(getApplicationContext(), "Orientation is locked to portrait on smaller screens", 0).show();
                }
                this.codewordPrefs.setMainOrientation(this.orientationLock);
                return INFO_WORDS;
            case HELP_ID /*3*/:
                codewordHelp();
                return INFO_WORDS;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class OnEditGestureListener implements GestureOverlayView.OnGesturePerformedListener {
        OnEditGestureListener() {
        }

        public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
            ArrayList<Prediction> predictions = PuzzleEditor.this.mLibrary.recognize(gesture);
            if (predictions.size() > 0) {
                Prediction prediction = predictions.get(0);
                if (prediction.score > 1.0d) {
                    PuzzleEditor.this.takeGesturesAction(prediction.name);
                }
            }
        }
    }

    public void takeGesturesAction(String predictionName) {
        if (predictionName.length() == 0) {
            predictionName = this.lastPrediction;
        }
        if (predictionName.matches("move_up")) {
            this.hOrVFlag = false;
            prevLetter();
        }
        if (predictionName.matches("move_down")) {
            this.hOrVFlag = false;
            nextLetter();
        }
        if (predictionName.matches("move_left")) {
            this.hOrVFlag = true;
            prevLetter();
        }
        if (predictionName.matches("move_right")) {
            this.hOrVFlag = true;
            nextLetter();
        }
        this.lastPrediction = predictionName;
        if (predictionName.length() != 0) {
            this.bHOrV.setBackgroundDrawable(this.hOrVFlag ? this.dDownArrow : this.dRightArrow);
            this.wordsGridAdapter.highlightPosition(this.letterClicked);
        }
    }

    public class WordGridListener implements AdapterView.OnItemClickListener {
        public WordGridListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            PuzzleEditor.this.letterClicked = (short) position;
            PuzzleEditor.this.wordsGridAdapter.highlightPosition(position);
        }
    }

    public class WordGridLongListener implements AdapterView.OnItemLongClickListener {
        public WordGridLongListener() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (!PuzzleEditor.this.isInternetOn()) {
                return PuzzleEditor.INFO_WORDS;
            }
            PuzzleEditor.this.spellChecker.checkSpelling(PuzzleEditor.this.puzzlesRow.getWord(PuzzleEditor.this.originalArray, PuzzleEditor.this.puzzleMode == 0 ? PuzzleEditor.INFO_WORDS : false, position, PuzzleEditor.this.hOrVFlag));
            PuzzleEditor.this.svWhatsLeft.scrollTo(PuzzleEditor.INFO_WORDS, PuzzleEditor.INFO_WORDS);
            return PuzzleEditor.INFO_WORDS;
        }
    }

    public class LetterGridListener implements AdapterView.OnItemClickListener {
        public LetterGridListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (PuzzleEditor.this.letterGridAdapter.texts[position] != ' ') {
                PuzzleEditor.this.wordsGridAdapter.texts[PuzzleEditor.this.letterClicked] = String.valueOf(PuzzleEditor.this.letterGridAdapter.texts[position]);
            } else {
                PuzzleEditor.this.wordsGridAdapter.texts[PuzzleEditor.this.letterClicked] = String.valueOf(position + PuzzleEditor.INFO_WORDS);
            }
            PuzzleEditor.this.wordsGridAdapter.originalData[PuzzleEditor.this.letterClicked] = (short) (position + PuzzleEditor.INFO_WORDS);
            if (PuzzleEditor.this.letterGridAdapter.updateLettersUsed(PuzzleEditor.this.wordsGridAdapter.originalData)) {
                if (PuzzleEditor.this.puzzleMode == 0) {
                    if (PuzzleEditor.this.notifyAllUsed) {
                        Toast.makeText(PuzzleEditor.this.getApplicationContext(), "All letters now used", 0).show();
                    }
                    PuzzleEditor.this.bEntryCompleted.setVisibility(0);
                }
                PuzzleEditor.this.notifyAllUsed = false;
            } else {
                PuzzleEditor.this.bEntryCompleted.setVisibility(8);
                PuzzleEditor.this.notifyAllUsed = true;
            }
            PuzzleEditor.this.nextLetter();
            PuzzleEditor.this.wordsGridAdapter.highlightPosition(PuzzleEditor.this.letterClicked);
        }
    }

    public class LetterGridLongListener implements AdapterView.OnItemLongClickListener {
        public LetterGridLongListener() {
        }

        public boolean onItemLongClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (PuzzleEditor.this.puzzleMode == 0) {
                if (PuzzleEditor.this.suppliedLetters[position] == ' ') {
                    PuzzleEditor.this.suppliedLetters[position] = (char) (position + 65);
                } else {
                    PuzzleEditor.this.suppliedLetters[position] = ' ';
                }
                PuzzleEditor.this.letterGridAdapter.notifyDataSetChanged();
                PuzzleEditor.this.wordsGridAdapter.notifyDataSetChanged();
                return true;
            }
            PuzzleEditor.this.letterChosen = (short) (position + PuzzleEditor.INFO_WORDS);
            PuzzleEditor.this.chooseLetterAdapter.refreshArray(PuzzleEditor.this.letterGridAdapter.texts);
            PuzzleEditor.this.letterFlipper.showNext();
            return true;
        }
    }

    public class ChooseLetterListener implements AdapterView.OnItemClickListener {
        public ChooseLetterListener() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int position, long id) {
            if (position > 26) {
                PuzzleEditor.this.letterFlipper.showPrevious();
                return;
            }
            for (int i = 0; i < 169; i += PuzzleEditor.INFO_WORDS) {
                if (PuzzleEditor.this.originalArray[i] == PuzzleEditor.this.letterChosen) {
                    if (position == 0) {
                        PuzzleEditor.this.wordsGridAdapter.texts[i] = String.valueOf((int) PuzzleEditor.this.letterChosen);
                    } else {
                        PuzzleEditor.this.wordsGridAdapter.texts[i] = PuzzleEditor.this.chooseLetterAdapter.activeLetters[position];
                    }
                }
            }
            if (position == 0) {
                PuzzleEditor.this.letterGridAdapter.texts[PuzzleEditor.this.letterChosen - PuzzleEditor.INFO_WORDS] = ' ';
                PuzzleEditor.this.suppliedLetters[PuzzleEditor.this.letterChosen - PuzzleEditor.INFO_WORDS] = ' ';
            } else {
                PuzzleEditor.this.letterGridAdapter.texts[PuzzleEditor.this.letterChosen - PuzzleEditor.INFO_WORDS] = PuzzleEditor.this.chooseLetterAdapter.activeLetters[position].charAt(0);
                PuzzleEditor.this.suppliedLetters[PuzzleEditor.this.letterChosen - PuzzleEditor.INFO_WORDS] = PuzzleEditor.this.chooseLetterAdapter.activeLetters[position].charAt(0);
            }
            PuzzleEditor.this.wordsGridAdapter.notifyDataSetChanged();
            PuzzleEditor.this.letterGridAdapter.notifyDataSetChanged();
            PuzzleEditor.this.letterChosen = (short) (position - PuzzleEditor.INFO_WORDS);
            PuzzleEditor.this.letterFlipper.showPrevious();
        }
    }

    private class spaceBarListener implements View.OnClickListener {
        private spaceBarListener() {
        }

        /* synthetic */ spaceBarListener(PuzzleEditor puzzleEditor, spaceBarListener spacebarlistener) {
            this();
        }

        public void onClick(View view) {
            PuzzleEditor.this.wordsGridAdapter.texts[PuzzleEditor.this.letterClicked] = " ";
            PuzzleEditor.this.wordsGridAdapter.originalData[PuzzleEditor.this.letterClicked] = 0;
            PuzzleEditor.this.letterGridAdapter.updateLettersUsed(PuzzleEditor.this.wordsGridAdapter.originalData);
            PuzzleEditor.this.nextLetter();
            PuzzleEditor.this.wordsGridAdapter.highlightPosition(PuzzleEditor.this.letterClicked);
        }
    }

    public class ViewClickListener implements View.OnClickListener {
        public ViewClickListener() {
        }

        public void onClick(View view) {
            PuzzleEditor.this.viewFlipper.showPrevious();
        }
    }

    public class entryCompletedListener implements View.OnClickListener {
        public entryCompletedListener() {
        }

        public void onClick(View v) {
            String t;
            int count = 0;
            AlertDialog.Builder ad = new AlertDialog.Builder(PuzzleEditor.this);
            for (int i = 0; i < 26; i += PuzzleEditor.INFO_WORDS) {
                if (PuzzleEditor.this.suppliedLetters[i] != ' ') {
                    count += PuzzleEditor.INFO_WORDS;
                }
            }
            if (count < PuzzleEditor.INFO_ORIENTATION) {
                t = "It's usual to have at least two supplied letters!  Do you want to continue?";
            } else {
                t = "You are about to complete your puzzle by scrambling your alphabet.  Continue?";
            }
            ad.setMessage(t);
            ad.setPositiveButton("Yes please", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    PuzzleEditor.this.completePuzzle();
                }
            });
            ad.setNegativeButton("Let me reconsider", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            ad.show();
        }
    }

    /* access modifiers changed from: private */
    public void nextLetter() {
        if (this.letterClicked != 168) {
            if (this.hOrVFlag) {
                if (this.puzzleMode == INFO_WORDS || (this.letterClicked + 1) % 13 != 0) {
                    this.letterClicked = (short) (this.letterClicked + 1);
                }
            } else if (this.letterClicked < 156) {
                this.letterClicked = (short) (this.letterClicked + 13);
            }
        }
    }

    private void prevLetter() {
        if (this.letterClicked != 0) {
            if (this.hOrVFlag) {
                if (this.letterClicked % 13 != 0) {
                    this.letterClicked = (short) (this.letterClicked - INFO_WORDS);
                }
            } else if (this.letterClicked >= 13) {
                this.letterClicked = (short) (this.letterClicked - 13);
            }
        }
    }

    /* access modifiers changed from: private */
    public void completePuzzle() {
        Random r = new Random();
        short[] randomArray = new short[27];
        char[] lettersArray = new char[26];
        for (short i = 0; i < 27; i = (short) (i + 1)) {
            randomArray[i] = i;
        }
        for (short i2 = 1; i2 < 27; i2 = (short) (i2 + 1)) {
            short j = (short) (r.nextInt(25) + INFO_WORDS);
            short x = randomArray[i2];
            randomArray[i2] = randomArray[j];
            randomArray[j] = x;
        }
        for (short i3 = 0; i3 < 169; i3 = (short) (i3 + 1)) {
            if (this.originalArray[i3] != 0) {
                this.originalArray[i3] = randomArray[this.originalArray[i3]];
            }
        }
        this.puzzlesRow.setGridFromArray(this.originalArray);
        for (short i4 = 0; i4 < 26; i4 = (short) (i4 + 1)) {
            lettersArray[randomArray[i4 + 1] - INFO_WORDS] = (char) (i4 + 65);
        }
        this.puzzlesRow.setAnswersFromArray(lettersArray);
        for (short i5 = 0; i5 < 26; i5 = (short) (i5 + 1)) {
            lettersArray[i5] = this.suppliedLetters[i5];
        }
        for (short i6 = 0; i6 < 26; i6 = (short) (i6 + 1)) {
            this.suppliedLetters[randomArray[i6 + 1] - INFO_WORDS] = lettersArray[i6];
        }
        this.puzzlesRow.setSuppliedFromArray(this.suppliedLetters);
        this.puzzlesRow.createDate = this.puzzlesRow.getDateNow();
        this.puzzlesRow.creator = "";
        this.mDbHelper.updatePuzzle(this.puzzlesRow);
        Toast.makeText(getApplicationContext(), "The puzzle is ready to play! Now update your puzzle info", (int) INFO_WORDS).show();
        Intent it = new Intent(this, CodeWord.class);
        it.putExtra("updateInfo", this.mRowId);
        startActivityForResult(it, 0);
    }

    private void codewordHelp() {
        Intent it = new Intent(this, CodeWordHelp.class);
        it.putExtra("hyperlink", "#Amending a puzzle|outline");
        startActivityForResult(it, 0);
    }

    private final void savePuzzle() {
        this.puzzlesRow.setGridFromArray(this.originalArray);
        this.puzzlesRow.setSuppliedFromArray(this.suppliedLetters);
        char[] startingLetters = this.puzzlesRow.getGuessesArray();
        for (int i = 0; i < 26; i += INFO_WORDS) {
            if (!(startingLetters[i] == this.letterGridAdapter.texts[i] || this.letterGridAdapter.texts[i] == ' ')) {
                int j = 0;
                while (true) {
                    if (j >= 26) {
                        break;
                    } else if (startingLetters[j] == this.letterGridAdapter.texts[i]) {
                        startingLetters[j] = ' ';
                        break;
                    } else {
                        j += INFO_WORDS;
                    }
                }
                startingLetters[i] = this.letterGridAdapter.texts[i];
            }
        }
        this.puzzlesRow.setGuessesFromArray(startingLetters);
        this.mDbHelper.updatePuzzle(this.puzzlesRow);
    }

    /* access modifiers changed from: private */
    public void listWordsUsed(boolean checkSpelling) {
        this.spellChecker = new SpellChecker(this);
        if (checkSpelling) {
            this.spellingProgressDialog = ProgressDialog.show(this, "Checking word", "Contacting Google..", true, true);
        } else {
            this.spellingProgressDialog = null;
        }
        this.spellingThread = new SpellingThread(this.handler, Boolean.valueOf(checkSpelling));
        this.spellingThread.start();
    }

    public boolean isInternetOn() {
        if (((ConnectivityManager) getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo() != null) {
            return true;
        }
        Toast.makeText(getApplicationContext(), "An internet connection is needed for using Google Spelling, enable one and try again", 0).show();
        return false;
    }

    private class SpellingThread extends Thread {
        boolean checkSpelling;
        Handler mHandler;

        SpellingThread(Handler h, Boolean checkSpelling2) {
            this.mHandler = h;
            this.checkSpelling = checkSpelling2.booleanValue();
        }

        public void run() {
            ArrayList<String> wordsList = new ArrayList<>();
            Iterator<String> it = PuzzleEditor.this.puzzlesRow.getWordsList(PuzzleEditor.this.originalArray, PuzzleEditor.this.puzzleMode == 0 ? PuzzleEditor.INFO_WORDS : false, true).iterator();
            while (it.hasNext()) {
                wordsList.add(it.next());
            }
            wordsList.add("\n");
            Iterator<String> it2 = PuzzleEditor.this.puzzlesRow.getWordsList(PuzzleEditor.this.originalArray, PuzzleEditor.this.puzzleMode == 0 ? PuzzleEditor.INFO_WORDS : false, false).iterator();
            while (it2.hasNext()) {
                wordsList.add(it2.next());
            }
            String words = "Horizontal\n";
            Iterator it3 = wordsList.iterator();
            while (it3.hasNext()) {
                String s = (String) it3.next();
                if (s == "\n") {
                    words = String.valueOf(words) + "\nVertical\n";
                } else if (this.checkSpelling) {
                    words = String.valueOf(words) + s + (PuzzleEditor.this.spellChecker.isWordCorrect(s) ? "" : "?") + " ";
                    PuzzleEditor.this.spellingString = s;
                    this.mHandler.sendEmptyMessage(0);
                } else {
                    words = String.valueOf(words) + s + " ";
                }
            }
            PuzzleEditor.this.spellingString = words;
            this.mHandler.sendEmptyMessage(PuzzleEditor.INFO_WORDS);
        }
    }
}
