package workspace.CodeWord;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class CodeWordHelp extends Activity {
    final String HELPFILE = "file:///android_asset/help.htm";
    Button bCloseHelp;
    String hyperlink;
    WebView mWebView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.help);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.hyperlink = extras.getString("hyperlink");
        }
        this.mWebView = (WebView) findViewById(R.id.webview);
        this.bCloseHelp = (Button) findViewById(R.id.bCloseHelp);
        this.bCloseHelp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CodeWordHelp.this.mWebView.destroy();
                CodeWordHelp.this.finish();
            }
        });
        this.mWebView.setWebViewClient(new HelpWebViewClient(this, null));
        this.mWebView.loadUrl("file:///android_asset/help.htm" + this.hyperlink);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        if (this.mWebView.canGoBack()) {
            this.mWebView.goBack();
        }
        return true;
    }

    private class HelpWebViewClient extends WebViewClient {
        private HelpWebViewClient() {
        }

        /* synthetic */ HelpWebViewClient(CodeWordHelp codeWordHelp, HelpWebViewClient helpWebViewClient) {
            this();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
