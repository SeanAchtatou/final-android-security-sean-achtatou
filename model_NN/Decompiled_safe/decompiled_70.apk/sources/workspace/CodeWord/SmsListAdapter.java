package workspace.CodeWord;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

public class SmsListAdapter extends BaseAdapter {
    Context context;
    SmsText smsText;
    ArrayList<SmsText> smsTexts;

    public SmsListAdapter(Context context2, ArrayList<SmsText> smsTexts2) {
        this.context = context2;
        this.smsTexts = smsTexts2;
    }

    public int getCount() {
        return this.smsTexts.size();
    }

    public Object getItem(int arg0) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Boolean puzzleWorks = false;
        TextView tv = convertView == null ? new TextView(this.context) : (TextView) convertView;
        this.smsText = this.smsTexts.get(position);
        if (this.smsText.puzzleRow != null) {
            puzzleWorks = Boolean.valueOf(this.smsText.puzzleRow.isPuzzlePlayable());
        }
        tv.setText("From " + this.smsText.person + " sent " + this.smsText.getFormattedDate() + (puzzleWorks.booleanValue() ? "Title " + this.smsText.puzzleRow.ref : "Puzzle not complete"));
        tv.setTextColor(puzzleWorks.booleanValue() ? -16711936 : -65536);
        return tv;
    }
}
