package workspace.CodeWord;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ViewFlipper;
import java.util.List;

public class PuzzlesListAdapter extends BaseAdapter {
    private List<PuzzlesRow> allPuzzles;
    private Context context;
    private boolean inHelpMode = false;
    private LayoutInflater inflater;
    private PuzzlesDbAdapter mDbHelper;
    private PuzzlesRow puzzlesRow;
    private ViewFlipper viewFlipper;

    public PuzzlesListAdapter(Context context2, PuzzlesDbAdapter mDbHelper2, ViewFlipper viewFlipper2) {
        this.context = context2;
        this.mDbHelper = mDbHelper2;
        this.inflater = (LayoutInflater) context2.getSystemService("layout_inflater");
        this.viewFlipper = viewFlipper2;
        refreshPuzzlesList();
    }

    public PuzzlesRow getPuzzlesRow(int ListRow) {
        return this.allPuzzles.get(ListRow);
    }

    public Long getPuzzleID(int ListRow) {
        return Long.valueOf(this.allPuzzles.get(ListRow).rowId);
    }

    public void refreshPuzzlesList() {
        this.allPuzzles = this.mDbHelper.getAllPuzzles();
        if (this.allPuzzles.size() == 0) {
            if (!this.inHelpMode) {
                this.viewFlipper.setDisplayedChild(1);
            }
            this.inHelpMode = true;
        } else {
            if (this.inHelpMode) {
                this.viewFlipper.setDisplayedChild(0);
            }
            this.inHelpMode = false;
        }
        notifyDataSetChanged();
    }

    public int getCount() {
        return this.allPuzzles.size();
    }

    public Object getItem(int arg0) {
        return null;
    }

    public long getItemId(int arg0) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup arg2) {
        View rowView;
        if (convertView == null) {
            rowView = this.inflater.inflate((int) R.layout.puzzles_row, (ViewGroup) null);
        } else {
            rowView = convertView;
        }
        this.puzzlesRow = this.allPuzzles.get(position);
        ((TextView) rowView.findViewById(R.id.tRef)).setText(this.puzzlesRow.ref);
        ((TextView) rowView.findViewById(R.id.tCreator)).setText("Creator: " + this.puzzlesRow.creator);
        ((TextView) rowView.findViewById(R.id.tDateCreated)).setText("Created: " + this.puzzlesRow.getCreateDateString());
        ((TextView) rowView.findViewById(R.id.tDateModified)).setText("Updated: " + this.puzzlesRow.getModificationDateString());
        return rowView;
    }
}
