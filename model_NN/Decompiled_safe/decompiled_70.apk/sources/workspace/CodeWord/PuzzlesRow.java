package workspace.CodeWord;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.ClipboardManager;
import android.util.Xml;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlSerializer;

public class PuzzlesRow {
    String answers;
    CodewordPrefs codewordPrefs;
    long createDate;
    String creator;
    String grid;
    String guesses = "";
    long modificationDate;
    String ref;
    String rowId;
    String supplied;
    Boolean workingPuzzle = true;

    public boolean isPuzzlePlayable() {
        boolean[] letters = new boolean[26];
        for (int i = 0; i < 169; i++) {
            int c = this.grid.charAt(i) - 'A';
            if (c >= 0) {
                letters[c] = true;
            }
        }
        for (int i2 = 0; i2 < 26; i2++) {
            if (!letters[i2]) {
                return false;
            }
        }
        return true;
    }

    public String getCreateDateString() {
        return makeDateString(this.createDate);
    }

    public String getModificationDateString() {
        return makeDateString(this.modificationDate);
    }

    public long getDateNow() {
        return Calendar.getInstance().getTimeInMillis();
    }

    public short[] getGridArray() {
        short[] gridArray = new short[169];
        for (int i = 0; i < 169; i++) {
            if (this.grid.charAt(i) == '0') {
                gridArray[i] = 0;
            } else {
                gridArray[i] = (short) ((this.grid.charAt(i) - 'A') + 1);
            }
        }
        return gridArray;
    }

    public void setGridFromArray(short[] gridArray) {
        this.grid = "";
        for (int i = 0; i < 169; i++) {
            if (gridArray[i] == 0) {
                this.grid = String.valueOf(this.grid) + '0';
            } else {
                this.grid = String.valueOf(this.grid) + ((char) ((gridArray[i] + 65) - 1));
            }
        }
    }

    public boolean setGridFromString(String s) {
        this.workingPuzzle = false;
        if (s.length() != 169) {
            return false;
        }
        if (!checkChars(s, '0', 169)) {
            return false;
        }
        this.grid = s;
        this.workingPuzzle = true;
        return true;
    }

    public char[] getSuppliedArray() {
        char[] suppliedArray = new char[26];
        for (int i = 0; i < 26; i++) {
            suppliedArray[i] = this.supplied.contentEquals("") ? ' ' : this.supplied.charAt(i);
        }
        return suppliedArray;
    }

    public void setSuppliedFromArray(char[] suppliedArray) {
        this.supplied = "";
        if (suppliedArray != null) {
            for (int i = 0; i < 26; i++) {
                this.supplied = String.valueOf(this.supplied) + suppliedArray[i];
            }
        }
    }

    public boolean setSuppliedFromString(String supplied2) {
        if (!checkChars(supplied2, ' ', 26)) {
            return false;
        }
        this.supplied = supplied2;
        return true;
    }

    public char[] getGuessesArray() {
        char[] guessesArray = new char[26];
        for (int i = 0; i < 26; i++) {
            guessesArray[i] = this.guesses.contentEquals("") ? ' ' : this.guesses.charAt(i);
        }
        return guessesArray;
    }

    public void setGuessesFromArray(char[] guessesArray) {
        this.guesses = "";
        if (guessesArray != null) {
            for (int i = 0; i < 26; i++) {
                this.guesses = String.valueOf(this.guesses) + guessesArray[i];
            }
        }
    }

    public boolean setGuessesFromString(String guesses2) {
        if (!checkChars(guesses2, ' ', 26)) {
            return false;
        }
        this.guesses = guesses2;
        return true;
    }

    public char[] getAnswersArray() {
        char[] answersArray = new char[26];
        for (int i = 0; i < 26; i++) {
            answersArray[i] = this.answers.contentEquals("") ? ' ' : this.answers.charAt(i);
        }
        return answersArray;
    }

    public void setAnswersFromArray(char[] answersArray) {
        this.answers = "";
        boolean zap = true;
        if (answersArray != null) {
            for (int i = 0; i < 26; i++) {
                if (answersArray[i] != ' ') {
                    zap = false;
                }
                this.answers = String.valueOf(this.answers) + answersArray[i];
            }
            if (zap) {
                this.answers = "";
            }
        }
    }

    public boolean setAnswersFromString(String answers2) {
        if (!checkChars(answers2, ' ', 26)) {
            this.answers = "";
            return false;
        }
        this.answers = answers2;
        return true;
    }

    public int savePuzzleAsFile(Context context, boolean forceSave) {
        this.codewordPrefs = new CodewordPrefs(context);
        String s = this.codewordPrefs.getSDFolder();
        if (s.length() == 0) {
            Toast.makeText(context, "Please setup your save folder and try again", 0).show();
            return 2;
        } else if (this.ref.contains("/") || this.ref.contains("\\")) {
            Toast.makeText(context, "Sorry, you can't have '\\' or '/' in your reference ", 0).show();
            return -1;
        } else {
            String fileName = String.valueOf(s) + "/" + this.ref + ".cwd";
            try {
                File file = new File(fileName);
                if (!file.createNewFile()) {
                    if (!forceSave) {
                        return 0;
                    }
                    file.delete();
                    new File(fileName);
                }
                try {
                    BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
                    out.write(getPuzzleAsXml());
                    out.close();
                    Toast.makeText(context, "Puzzle " + fileName + " saved", 0).show();
                    return 1;
                } catch (IOException e) {
                    Toast.makeText(context, "Writing to File " + fileName + " failed", 0).show();
                    return -1;
                }
            } catch (IOException e2) {
                Toast.makeText(context, "Sorry, the create failed", 0).show();
                return -1;
            }
        }
    }

    public PuzzlesRow getPuzzleFromString(String text) {
        return new XMLHandler().parsePuzzle(new ByteArrayInputStream(text.getBytes()));
    }

    public PuzzlesRow getPuzzleFromAssets(Context context, String xmlName) {
        try {
            return new XMLHandler().parsePuzzle(context.getAssets().open(xmlName));
        } catch (IOException e) {
            return null;
        }
    }

    public PuzzlesRow getPuzzleFromFile(File file) {
        try {
            return new XMLHandler().parsePuzzle(new FileInputStream(file));
        } catch (IOException e) {
            return null;
        }
    }

    public PuzzlesRow getPuzzleFromClipboard(Context context) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService("clipboard");
        if (!cm.hasText()) {
            return null;
        }
        String s = String.valueOf(cm.getText().toString()) + "\n";
        if (!s.contains("<puzzle>")) {
            return null;
        }
        if (!s.contains("<?xml")) {
            return null;
        }
        return getPuzzleFromString(s.substring(s.indexOf("<?xml")));
    }

    public String getPuzzleAsXml() {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "puzzle");
            serializer.startTag("", "info");
            serializer.attribute("", "ref", this.ref);
            serializer.attribute("", PuzzlesDbAdapter.COLUMN_PUZZLE_CREATOR, this.creator);
            serializer.attribute("", "dateCreated", getCreateDateString());
            serializer.endTag("", "info");
            serializer.startTag("", "gridrows");
            for (int i = 0; i < 13; i++) {
                serializer.attribute("", "row" + (i + 1), this.grid.substring(i * 13, (i * 13) + 13));
            }
            serializer.endTag("", "gridrows");
            serializer.startTag("", PuzzlesDbAdapter.COLUMN_SUPPLIED);
            serializer.text(this.supplied);
            serializer.endTag("", PuzzlesDbAdapter.COLUMN_SUPPLIED);
            serializer.startTag("", "solution");
            serializer.text(this.answers);
            serializer.endTag("", "solution");
            serializer.endTag("", "puzzle");
            serializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            return null;
        }
    }

    public String makeDateString(long longDateTime) {
        Locale locale = Locale.getDefault();
        if (longDateTime == 0) {
            return "n/a";
        }
        DateFormat.getDateInstance(2, locale).format(new Date());
        return String.valueOf(DateFormat.getTimeInstance(3, locale).format(Long.valueOf(longDateTime))) + " " + DateFormat.getDateInstance(3, locale).format(Long.valueOf(longDateTime));
    }

    public ArrayList<String> getWordsList(short[] grid2, boolean answersInGrid, boolean hOrVFlag) {
        short[] thisGrid;
        boolean z;
        char[] thisAnswers = null;
        String word = "";
        int x = 0;
        ArrayList<String> words = new ArrayList<>();
        if (grid2 == null) {
            thisGrid = getGridArray();
            if (this.answers == null) {
                thisAnswers = getAnswersArray();
            }
        } else {
            thisGrid = grid2;
            if (answersInGrid) {
                thisAnswers = null;
            } else {
                thisAnswers = getAnswersArray();
            }
        }
        do {
            if (x < 0) {
                z = true;
            } else {
                z = false;
            }
            Boolean end = Boolean.valueOf(z);
            int x2 = Math.abs(x);
            if (thisGrid[x2] == 0 || end.booleanValue()) {
                if (word.length() > 1) {
                    words.add(String.valueOf(word.substring(0, 1)) + word.substring(1).toLowerCase());
                }
                word = "";
            }
            if (thisGrid[x2] != 0) {
                if (thisAnswers != null) {
                    word = String.valueOf(word) + thisAnswers[thisGrid[x2] - 1];
                } else {
                    word = String.valueOf(word) + ((char) ((thisGrid[x2] + 65) - 1));
                }
            }
            x = nextCell(hOrVFlag, x2);
        } while (x != -255);
        return words;
    }

    public String getWord(short[] grid2, boolean answersInGrid, int position, boolean hOrVFlag) {
        short[] thisGrid;
        char[] thisAnswers = null;
        String word = "";
        Boolean up = false;
        Boolean across = false;
        int x = position;
        if (grid2 == null) {
            thisGrid = getGridArray();
            if (this.answers == null) {
                thisAnswers = getAnswersArray();
            }
        } else {
            thisGrid = grid2;
            if (answersInGrid) {
                thisAnswers = null;
            } else {
                thisAnswers = getAnswersArray();
            }
        }
        if (thisGrid[x] == 0) {
            return "";
        }
        if (x > 13 && thisGrid[x - 13] != 0) {
            up = true;
        }
        if (x < 156 && thisGrid[x + 13] != 0) {
            up = true;
        }
        if (x % 13 > 0 && thisGrid[x - 1] != 0) {
            across = true;
        }
        if (x % 13 < 12 && thisGrid[x + 1] != 0) {
            across = true;
        }
        if (!up.booleanValue() || !across.booleanValue()) {
            if (up.booleanValue()) {
                hOrVFlag = false;
            }
            if (across.booleanValue()) {
                hOrVFlag = true;
            }
        }
        while (x > 0) {
            if (!hOrVFlag) {
                if (x < 13 || thisGrid[x - 13] == 0) {
                    break;
                }
                x -= 13;
            } else if (x % 13 == 0 || thisGrid[x - 1] == 0) {
                break;
            } else {
                x--;
            }
        }
        do {
            Boolean end = Boolean.valueOf(x < 0);
            int x2 = Math.abs(x);
            if (thisGrid[x2] != 0 && !end.booleanValue()) {
                if (thisGrid[x2] != 0) {
                    if (thisAnswers != null) {
                        word = String.valueOf(word) + thisAnswers[thisGrid[x2] - 1];
                    } else {
                        word = String.valueOf(word) + ((char) ((thisGrid[x2] + 65) - 1));
                    }
                }
                x = nextCell(hOrVFlag, x2);
            }
        } while (x != -255);
        if (word.length() > 0) {
            return String.valueOf(word.substring(0, 1)) + word.substring(1).toLowerCase();
        }
        return word;
    }

    private int nextCell(boolean hOrVFlag, int position) {
        int position2;
        if (position == 168) {
            return -255;
        }
        if (hOrVFlag) {
            position2 = position + 1;
            if (position2 % 13 == 0) {
                position2 = -position2;
            }
        } else if (position >= 156) {
            position2 = -((position % 156) + 1);
        } else {
            position2 = position + 13;
        }
        return position2;
    }

    private boolean checkChars(String s, char blank, int length) {
        if (s.length() == 0) {
            return true;
        }
        int i = 0;
        while (i < s.length()) {
            char c = s.charAt(i);
            if (c >= 'A' || c <= 'Z' || c == blank) {
                i++;
            } else {
                this.workingPuzzle = false;
                return false;
            }
        }
        if (s.length() == length) {
            return true;
        }
        return false;
    }

    public class XMLHandler extends DefaultHandler {
        AssetManager assetManager;
        Boolean currentElement = false;
        String currentValue = null;
        PuzzlesRow puzzlesRow;
        SAXParser sp;
        SAXParserFactory spf;
        XMLReader xr;

        public XMLHandler() {
        }

        /* access modifiers changed from: private */
        public PuzzlesRow parsePuzzle(InputStream stream) {
            this.puzzlesRow = new PuzzlesRow();
            try {
                this.spf = SAXParserFactory.newInstance();
                this.sp = this.spf.newSAXParser();
                this.xr = this.sp.getXMLReader();
                this.xr.setContentHandler(this);
                this.xr.parse(new InputSource(stream));
                if (stream != null) {
                    try {
                        stream.close();
                    } catch (IOException e) {
                        return null;
                    }
                }
                return this.puzzlesRow;
            } catch (Exception e2) {
                return null;
            }
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            String s = "";
            this.currentElement = true;
            if (localName.equalsIgnoreCase("gridrows")) {
                for (int i = 0; i < 13; i++) {
                    s = String.valueOf(s) + attributes.getValue("row" + (i + 1));
                }
                this.puzzlesRow.setGridFromString(s);
            } else if (localName.equalsIgnoreCase("info")) {
                this.puzzlesRow.ref = attributes.getValue("ref");
                this.puzzlesRow.creator = attributes.getValue(PuzzlesDbAdapter.COLUMN_PUZZLE_CREATOR);
            }
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            if (this.currentElement.booleanValue()) {
                this.currentValue = new String(ch, start, length);
                this.currentElement = false;
            }
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            this.currentElement = false;
            if (localName.equalsIgnoreCase(PuzzlesDbAdapter.COLUMN_SUPPLIED)) {
                this.puzzlesRow.setSuppliedFromString(this.currentValue);
            } else if (localName.equalsIgnoreCase("solution")) {
                this.puzzlesRow.setAnswersFromString(this.currentValue);
            }
            this.currentValue = "";
        }
    }
}
