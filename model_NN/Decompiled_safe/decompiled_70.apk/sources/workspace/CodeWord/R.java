package workspace.CodeWord;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int blue = 2131099648;
        public static final int green = 2131099650;
        public static final int light_blue = 2131099649;
        public static final int white = 2131099652;
        public static final int yellow = 2131099651;
    }

    public static final class drawable {
        public static final int buttonface_blue_1 = 2130837504;
        public static final int buttonface_green_1 = 2130837505;
        public static final int cancel = 2130837506;
        public static final int codeword = 2130837507;
        public static final int down_arrow = 2130837508;
        public static final int ic_menu_revert = 2130837509;
        public static final int right_arrow = 2130837510;
        public static final int whatsleft = 2130837511;
    }

    public static final class id {
        public static final int alphabetGrid = 2131165203;
        public static final int bCloseHelp = 2131165188;
        public static final int bCreateHere = 2131165223;
        public static final int bEntryCompleted = 2131165200;
        public static final int bFilePickerCancel = 2131165224;
        public static final int bHOrV = 2131165199;
        public static final int bHelp = 2131165213;
        public static final int bPuzzleOptionsClose = 2131165218;
        public static final int bSmsSelectCancel = 2131165220;
        public static final int bSpellingDone = 2131165207;
        public static final int bUpdateInfo = 2131165231;
        public static final int cHideClues = 2131165215;
        public static final int cLongTimeout = 2131165216;
        public static final int eWordToSpell = 2131165205;
        public static final int filePickerList = 2131165225;
        public static final int frameLayout2 = 2131165202;
        public static final int gestures = 2131165193;
        public static final int infoLayout = 2131165226;
        public static final int lCreateButtons = 2131165198;
        public static final int lFilePicker = 2131165222;
        public static final int lHelp = 2131165211;
        public static final int lPuzzleListLayout = 2131165208;
        public static final int lSmsLayout = 2131165219;
        public static final int lSmsList = 2131165221;
        public static final int lSpellChecker = 2131165204;
        public static final int letterGrid = 2131165201;
        public static final int linearLayout1 = 2131165184;
        public static final int linearLayout2 = 2131165196;
        public static final int linearLayout3 = 2131165234;
        public static final int listFlipper = 2131165209;
        public static final int mainLayout = 2131165190;
        public static final int puzzleHelp = 2131165212;
        public static final int puzzleOptions = 2131165214;
        public static final int puzzlesList = 2131165210;
        public static final int svWhatsLeft = 2131165195;
        public static final int tCreator = 2131165233;
        public static final int tDateCreated = 2131165235;
        public static final int tDateModified = 2131165236;
        public static final int tMainCreator = 2131165228;
        public static final int tMainDateCreated = 2131165229;
        public static final int tMainDateModified = 2131165230;
        public static final int tMainRef = 2131165227;
        public static final int tRef = 2131165232;
        public static final int tResult = 2131165206;
        public static final int tSaveFolder = 2131165217;
        public static final int textView1 = 2131165185;
        public static final int textView2 = 2131165186;
        public static final int vLetterFlipper = 2131165194;
        public static final int webLayout = 2131165187;
        public static final int webview = 2131165189;
        public static final int whats_left = 2131165197;
        public static final int wordGrid = 2131165192;
        public static final int wordGridLayout = 2131165191;
    }

    public static final class layout {
        public static final int dir_row = 2130903040;
        public static final int help = 2130903041;
        public static final int main = 2130903042;
        public static final int puzzles_list = 2130903043;
        public static final int puzzles_row = 2130903044;
    }

    public static final class raw {
        public static final int gestures = 2130968576;
    }

    public static final class string {
        public static final int amend_puzzle = 2131034133;
        public static final int app_name = 2131034112;
        public static final int cancel = 2131034134;
        public static final int delete_puzzle = 2131034135;
        public static final int email_file = 2131034136;
        public static final int file_keep = 2131034139;
        public static final int file_replace = 2131034140;
        public static final int from_clipboard = 2131034137;
        public static final int from_file = 2131034138;
        public static final int from_text = 2131034141;
        public static final int menu_add = 2131034113;
        public static final int menu_amend = 2131034114;
        public static final int menu_clear_solution = 2131034116;
        public static final int menu_clear_wip = 2131034117;
        public static final int menu_clone = 2131034118;
        public static final int menu_delete = 2131034119;
        public static final int menu_email = 2131034120;
        public static final int menu_example = 2131034121;
        public static final int menu_help = 2131034122;
        public static final int menu_info = 2131034123;
        public static final int menu_insert = 2131034124;
        public static final int menu_list_words = 2131034125;
        public static final int menu_lock_orientation = 2131034126;
        public static final int menu_open = 2131034127;
        public static final int menu_options = 2131034128;
        public static final int menu_play = 2131034115;
        public static final int menu_save = 2131034129;
        public static final int menu_savetofile = 2131034130;
        public static final int menu_sendtext = 2131034131;
        public static final int menu_unlock_orientation = 2131034132;
        public static final int no_puzzles = 2131034142;
        public static final int orientation_change = 2131034143;
        public static final int save_my_changes = 2131034144;
        public static final int welcome_text = 2131034145;
    }
}
