package workspace.CodeWord;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LetterGridAdapter extends BaseAdapter {
    private Drawable buttonBackgroundBlue;
    private Drawable buttonBackgroundGreen;
    private int cellHeight;
    private int cellWidth;
    private Context context;
    private boolean[] lettersUsed;
    private Boolean noOrCharFlag = true;
    public char[] suppliedLetters;
    public char[] texts = new char[26];

    public LetterGridAdapter(Context context2, char[] suppliedLetters2, char[] savedGuesses, int deviceSize) {
        this.context = context2;
        this.suppliedLetters = suppliedLetters2;
        Resources res = context2.getResources();
        this.buttonBackgroundBlue = res.getDrawable(R.drawable.buttonface_blue_1);
        this.buttonBackgroundGreen = res.getDrawable(R.drawable.buttonface_green_1);
        this.cellWidth = deviceSize == 0 ? 22 : 50;
        this.cellHeight = deviceSize == 0 ? 22 : 45;
        for (int i = 0; i < 26; i++) {
            this.texts[i] = savedGuesses[i];
        }
        this.lettersUsed = null;
    }

    public void flipGrid(boolean noOrCharFlag2) {
        this.noOrCharFlag = Boolean.valueOf(noOrCharFlag2);
        notifyDataSetChanged();
    }

    public boolean updateLettersUsed(short[] originalData) {
        boolean allUsed = true;
        if (this.lettersUsed == null) {
            this.lettersUsed = new boolean[26];
        }
        for (int i = 0; i < 26; i++) {
            this.lettersUsed[i] = false;
        }
        for (int i2 = 0; i2 < 169; i2++) {
            short s = originalData[i2];
            if (s > 0) {
                this.lettersUsed[s - 1] = true;
            }
        }
        for (int i3 = 0; i3 < 26; i3++) {
            if (!this.lettersUsed[i3]) {
                allUsed = false;
            }
        }
        notifyDataSetChanged();
        return allUsed;
    }

    public int getCount() {
        return 26;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv;
        if (convertView == null) {
            tv = new TextView(this.context);
            tv.setLayoutParams(new AbsListView.LayoutParams(this.cellWidth, this.cellHeight));
            tv.setGravity(17);
        } else {
            tv = (TextView) convertView;
        }
        tv.setBackgroundDrawable(this.buttonBackgroundBlue);
        tv.setTypeface(Typeface.DEFAULT);
        if (this.lettersUsed != null) {
            if (this.texts[position] != ' ') {
                tv.setTextColor(-16777216);
                tv.setText(String.valueOf(this.texts[position]));
            } else {
                if (this.noOrCharFlag.booleanValue()) {
                    tv.setText(String.valueOf(position + 1));
                } else {
                    tv.setText(String.valueOf((char) (position + 65)));
                }
                tv.setTextColor(-7829368);
            }
            if (this.suppliedLetters[position] != ' ') {
                tv.setTypeface(Typeface.DEFAULT_BOLD);
            }
            if (this.lettersUsed[position]) {
                tv.setBackgroundDrawable(this.buttonBackgroundGreen);
            }
        } else if (this.texts[position] != ' ') {
            tv.setTextColor(-16777216);
            tv.setText(String.valueOf(this.texts[position]));
        } else {
            tv.setTextColor(-7829368);
            tv.setText(String.valueOf(position + 1));
        }
        return tv;
    }
}
