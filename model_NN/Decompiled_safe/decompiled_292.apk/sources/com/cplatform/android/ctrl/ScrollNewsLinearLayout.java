package com.cplatform.android.ctrl;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.ad.AdItem;
import com.cplatform.android.cmsurfclient.service.entry.Item;
import java.util.ArrayList;

public class ScrollNewsLinearLayout extends LinearLayout {
    private static final int MESSAGE_HIDDEN = 102;
    private static final int MESSAGE_SCROLLNEWS = 101;
    private static final int MESSAGE_SCROLLNEWS_DYNAMIC = 103;
    private static final long SCROLL_TIMES = 16000;
    ArrayList<Item> itemslist = null;
    scrollOpenUrlInterface mCaller = null;
    Context mContext;
    ArrayList<Item> mCurrentItemsList = null;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ScrollNewsLinearLayout.MESSAGE_SCROLLNEWS /*101*/:
                    Log.v("ScrollNewsLinearLayout", "enter handleMessage MESSAGE_SCROLLNEWS");
                    ScrollNewsLinearLayout.this.showScrollFace();
                    Message message = new Message();
                    message.what = ScrollNewsLinearLayout.MESSAGE_SCROLLNEWS_DYNAMIC;
                    ScrollNewsLinearLayout.this.mHandler.sendMessageDelayed(message, ScrollNewsLinearLayout.SCROLL_TIMES);
                    return;
                case ScrollNewsLinearLayout.MESSAGE_HIDDEN /*102*/:
                    Log.v("ScrollNewsLinearLayout", "enter handleMessage MESSAGE_HIDDEN");
                    ScrollNewsLinearLayout.this.hide(msg.arg1);
                    return;
                case ScrollNewsLinearLayout.MESSAGE_SCROLLNEWS_DYNAMIC /*103*/:
                    Log.v("ScrollNewsLinearLayout", "enter handleMessage MESSAGE_SCROLLNEWS_DYNAMIC");
                    ScrollNewsLinearLayout.this.doScroll();
                    Message dynamicmessage = new Message();
                    dynamicmessage.what = ScrollNewsLinearLayout.MESSAGE_SCROLLNEWS_DYNAMIC;
                    ScrollNewsLinearLayout.this.mHandler.sendMessageDelayed(dynamicmessage, ScrollNewsLinearLayout.SCROLL_TIMES);
                    return;
                default:
                    return;
            }
        }
    };
    int mHight;
    private SurfManagerActivity mSurfMgr = null;
    String[] mUrlArry = new String[3];
    private TextView textViewHot1 = null;
    private TextView textViewHot2 = null;
    private TextView textViewHot3 = null;
    TextView[] textViewHotArray = new TextView[3];

    public ScrollNewsLinearLayout(Context context) {
        super(context);
        this.mContext = context;
        this.mHight = getHeight();
        for (int i = 0; i < this.textViewHotArray.length; i++) {
            this.textViewHotArray[i] = new TextView(context);
            addView(this.textViewHotArray[i]);
        }
        initTextView();
    }

    public ScrollNewsLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.v("ScrollNewsLinearLayout", "enter ScrollNewsLinearLayout construct2");
        this.mHight = getHeight();
        this.mContext = context;
        for (int i = 0; i < this.textViewHotArray.length; i++) {
            this.textViewHotArray[i] = new TextView(context);
            addView(this.textViewHotArray[i]);
        }
        initTextView();
    }

    private void initTextView() {
        this.textViewHotArray[0].setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        ScrollNewsLinearLayout.this.textViewHotArray[0].setLinkTextColor(-1);
                        ScrollNewsLinearLayout.this.textViewHotArray[0].setBackgroundColor(AdItem.TEXTCOLOR_PRESSED);
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_DOWN");
                        return true;
                    case 1:
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_DOWN");
                        ScrollNewsLinearLayout.this.textViewHotArray[0].setLinkTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                        ScrollNewsLinearLayout.this.textViewHotArray[0].setBackgroundColor(0);
                        if (ScrollNewsLinearLayout.this.mCaller == null) {
                            return true;
                        }
                        ScrollNewsLinearLayout.this.mCaller.onOpenScrollUrlLink(ScrollNewsLinearLayout.this.mUrlArry[0]);
                        return true;
                    case 2:
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_MOVE");
                        return true;
                    default:
                        return true;
                }
            }
        });
        this.textViewHotArray[1].setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        ScrollNewsLinearLayout.this.textViewHotArray[1].setLinkTextColor(-1);
                        ScrollNewsLinearLayout.this.textViewHotArray[1].setBackgroundColor(AdItem.TEXTCOLOR_PRESSED);
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_DOWN");
                        break;
                    case 1:
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_DOWN");
                        ScrollNewsLinearLayout.this.textViewHotArray[1].setLinkTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                        ScrollNewsLinearLayout.this.textViewHotArray[1].setBackgroundColor(0);
                        if (ScrollNewsLinearLayout.this.mCaller != null) {
                            ScrollNewsLinearLayout.this.mCaller.onOpenScrollUrlLink(ScrollNewsLinearLayout.this.mUrlArry[1]);
                            break;
                        }
                        break;
                    case 2:
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_MOVE");
                        break;
                }
                return true;
            }
        });
        this.textViewHotArray[2].setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case 0:
                        ScrollNewsLinearLayout.this.textViewHotArray[2].setLinkTextColor(-1);
                        ScrollNewsLinearLayout.this.textViewHotArray[2].setBackgroundColor(AdItem.TEXTCOLOR_PRESSED);
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_DOWN");
                        return true;
                    case 1:
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_DOWN");
                        ScrollNewsLinearLayout.this.textViewHotArray[2].setLinkTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                        ScrollNewsLinearLayout.this.textViewHotArray[2].setBackgroundColor(0);
                        if (ScrollNewsLinearLayout.this.mCaller == null) {
                            return true;
                        }
                        ScrollNewsLinearLayout.this.mCaller.onOpenScrollUrlLink(ScrollNewsLinearLayout.this.mUrlArry[2]);
                        return true;
                    case 2:
                        Log.v("ScrollNewsLinearLayout", "enter onTouch ACTION_MOVE");
                        return true;
                    default:
                        return true;
                }
            }
        });
    }

    private void createChildControls() {
        int hotNum = getShowhotNum();
        Log.v("createChildControls", "enter createChildControls hotNum = " + hotNum);
        if (-1 != hotNum) {
            appear(hotNum);
            if (this.mCurrentItemsList == null) {
                this.mCurrentItemsList = new ArrayList<>();
            }
            this.mCurrentItemsList.clear();
            for (int i = 0; i < hotNum; i++) {
                this.mCurrentItemsList.add(this.itemslist.get(0));
                this.itemslist.add(this.itemslist.remove(0));
            }
        }
    }

    /* access modifiers changed from: private */
    public void showScrollFace() {
        int hotNum = getShowhotNum();
        Log.v("createChildControls", "enter showScrollFace hotNum = " + hotNum);
        if (-1 != hotNum) {
            for (int i = 0; i < hotNum; i++) {
                this.textViewHotArray[i].setVisibility(0);
            }
            for (int j = hotNum; j < 3; j++) {
                this.textViewHotArray[j].setVisibility(8);
            }
            if (this.mCurrentItemsList == null) {
                this.mCurrentItemsList = new ArrayList<>();
            }
            this.mCurrentItemsList.clear();
            for (int i2 = 0; i2 < hotNum; i2++) {
                this.mCurrentItemsList.add(this.itemslist.get(0));
                this.itemslist.add(this.itemslist.remove(0));
            }
            Message msg = new Message();
            msg.what = MESSAGE_HIDDEN;
            msg.arg1 = hotNum;
            this.mHandler.removeMessages(MESSAGE_HIDDEN);
            this.mHandler.sendMessageDelayed(msg, 13333);
        }
    }

    /* access modifiers changed from: private */
    public void doScroll() {
        Log.v("ScrollNewsLinearLayout", "enter doScroll");
        createChildControls();
    }

    public void setScrollNews(scrollOpenUrlInterface caller, ArrayList<Item> items) {
        Log.v("ScrollNewsLinearLayout", "enter setScrollNews");
        this.mCaller = caller;
        this.itemslist = items;
        if (this.itemslist == null) {
            Log.v("ScrollNewsLinearLayout", "#\n#\n#\n#\n#\n#\n#\n#\nenter this.itemslist = " + this.itemslist);
        }
        setVisibility(0);
        startScrollNewsMessage();
    }

    public void leaveScrollNewsMessage() {
        this.mHandler.removeMessages(MESSAGE_SCROLLNEWS);
        this.mHandler.removeMessages(MESSAGE_SCROLLNEWS_DYNAMIC);
        for (int i = 0; i < 3; i++) {
            if (this.textViewHotArray[i] != null) {
                this.textViewHotArray[i].clearAnimation();
            }
        }
    }

    public void startScrollNewsMessage() {
        leaveScrollNewsMessage();
        this.mHandler.sendEmptyMessage(MESSAGE_SCROLLNEWS);
    }

    private int getShowhotNum() {
        if (this.itemslist == null || this.itemslist.isEmpty()) {
            return -1;
        }
        int len = this.itemslist.size() >= 3 ? 3 : this.itemslist.size();
        Log.v("ScrollNewsLinearLayout", "enter getShowhotNum itemslist.size() = " + this.itemslist.size());
        int totallen = 0;
        int i = 0;
        while (true) {
            if (i >= len) {
                break;
            }
            LinearLayout.LayoutParams mParamsMid = new LinearLayout.LayoutParams(-2, -2);
            this.textViewHotArray[i].setSingleLine(true);
            this.textViewHotArray[i].setHorizontalScrollBarEnabled(false);
            this.textViewHotArray[i].setEllipsize(TextUtils.TruncateAt.MARQUEE);
            this.textViewHotArray[i].setMarqueeRepeatLimit(-1);
            this.textViewHotArray[i].setLinkTextColor((int) AdItem.TEXTCOLOR_PRESSED);
            this.textViewHotArray[i].setTextAppearance(this.mContext, R.style.ui_hot_text_normal);
            this.textViewHotArray[i].setText(Html.fromHtml("<a href=\"" + this.itemslist.get(i).url + "\">" + this.itemslist.get(i).name + "</a>"));
            this.mUrlArry[i] = this.itemslist.get(i).url;
            mParamsMid.leftMargin = 10;
            this.textViewHotArray[i].setLayoutParams(mParamsMid);
            this.textViewHotArray[i].measure(0, 0);
            totallen += this.textViewHotArray[i].getMeasuredWidth() + 10;
            if (totallen > getWidth()) {
                len = i;
                if (len == 0) {
                    len = 1;
                }
            } else {
                i++;
            }
        }
        Log.v("ScrollNewsLinearLayout", "enter getShowhotNum this.getWidth() = " + getWidth());
        return len;
    }

    /* access modifiers changed from: package-private */
    public void hide(int hotnum) {
        Log.v("ScrollNewsLinearLayout", "enter hide");
        for (int i = 0; i < hotnum; i++) {
            this.textViewHotArray[i].setVisibility(8);
            int toYDelta = ((-getHeight()) * 3) / 4;
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation((float) 0, (float) 0, (float) 0, (float) toYDelta);
            new AlphaAnimation(1.0f, 0.0f);
            animationSet.addAnimation(translateAnimation);
            this.textViewHotArray[i].setAnimation(animationSet);
            animationSet.setDuration(2666);
            animationSet.start();
        }
        for (int j = hotnum; j < 3; j++) {
            this.textViewHotArray[j].setVisibility(8);
        }
    }

    /* access modifiers changed from: package-private */
    public void appear(int hotnum) {
        Log.v("ScrollNewsLinearLayout", "enter appear");
        for (int i = 0; i < hotnum; i++) {
            int fromYDelta = (getHeight() * 2) / 3;
            AnimationSet animationSet = new AnimationSet(true);
            TranslateAnimation translateAnimation = new TranslateAnimation((float) 0, (float) 0, (float) fromYDelta, (float) 0);
            new AlphaAnimation(0.0f, 1.0f);
            animationSet.addAnimation(translateAnimation);
            this.textViewHotArray[i].setAnimation(animationSet);
            animationSet.setDuration(2666);
            animationSet.start();
            this.textViewHotArray[i].setVisibility(0);
        }
        for (int j = hotnum; j < 3; j++) {
            this.textViewHotArray[j].setVisibility(8);
        }
        Message msg = new Message();
        msg.what = MESSAGE_HIDDEN;
        msg.arg1 = hotnum;
        this.mHandler.removeMessages(MESSAGE_HIDDEN);
        this.mHandler.sendMessageDelayed(msg, 13333);
    }

    private static class MyURLSpan extends ClickableSpan {
        scrollOpenUrlInterface mCaller = null;
        private String mUrl;

        MyURLSpan(scrollOpenUrlInterface caller, String url) {
            Log.v("jiangdeming test", "come MyURLSpan construct");
            this.mUrl = url;
            this.mCaller = caller;
        }

        public void onClick(View widget) {
            Log.v("jiangdeming test", "come MyURLSpan  >>  onClick ");
            if (this.mCaller != null) {
                this.mCaller.onOpenScrollUrlLink(this.mUrl);
            }
        }
    }
}
