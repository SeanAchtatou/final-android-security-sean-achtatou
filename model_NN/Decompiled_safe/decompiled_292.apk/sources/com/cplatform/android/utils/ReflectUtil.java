package com.cplatform.android.utils;

import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectUtil {
    private static final String LOG_TAG = "ReflectUtil";

    public static Object getDeclaredField(Class<?> declaredClass, Object classObject, String fieldName) {
        try {
            Field field = declaredClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            return field.get(classObject);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in setField", e);
            return null;
        }
    }

    public static Object invokeDeclaredMethod(Class<?> declaredClass, Object classObject, String methodName, Object[] params, Class<?>[] paramTypes) {
        try {
            Method method = declaredClass.getDeclaredMethod(methodName, paramTypes);
            method.setAccessible(true);
            if (params == null) {
                return method.invoke(classObject, new Object[0]);
            }
            return method.invoke(classObject, params);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in invokeDeclaredMethod", e);
            return null;
        }
    }

    public static Object invokeDeclaredMethodNoParams(Class<?> declaredClass, Object classObject, String methodName) {
        Object ret = null;
        try {
            Method method = declaredClass.getDeclaredMethod(methodName, new Class[0]);
            if (method != null) {
                method.setAccessible(true);
                ret = method.invoke(classObject, new Object[0]);
            }
            return ret;
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in invokeDeclaredMethod", e);
            return null;
        }
    }

    public static Object invokeMethodNoParams(Class<?> declaredClass, Object classObject, String methodName) {
        try {
            return declaredClass.getMethod(methodName, new Class[0]).invoke(classObject, new Object[0]);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in invokeMethodNoParams", e);
            return null;
        }
    }

    public static Object invokeMethodWithParams(Class<?> declaredClass, Object classObject, String methodName, Object[] params, Class<?>[] paramTypes) {
        try {
            return declaredClass.getMethod(methodName, paramTypes).invoke(classObject, params);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in invokeMethod", e);
            return null;
        }
    }

    public static void setDeclaredField(Class<?> declaredClass, Object classObject, String fieldName, Object value) {
        try {
            Field field = declaredClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(classObject, value);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in setDeclaredField", e);
        }
    }

    public static void setField(Class<?> declaredClass, Object classObject, String filedName, Object value) {
        try {
            declaredClass.getField(filedName).set(classObject, value);
        } catch (Exception e) {
            Log.d(LOG_TAG, "Exception in setField", e);
        }
    }
}
