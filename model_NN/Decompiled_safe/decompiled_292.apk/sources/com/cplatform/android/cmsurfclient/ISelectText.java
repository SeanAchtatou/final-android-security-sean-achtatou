package com.cplatform.android.cmsurfclient;

public interface ISelectText {
    void onCopyText(String str);

    void onSearchText(String str);

    void onShareText(String str);
}
