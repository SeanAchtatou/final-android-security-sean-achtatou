package com.cplatform.android.cmsurfclient;

public interface IWebViewManager {
    void onDownloadStart(String str);

    void onPageFinished(String str, String str2, boolean z, boolean z2);

    void onPageStarted(String str);

    void onProgressChanged(int i);

    void onReceivedError(int i, String str, String str2);

    void onReceivedTitle(String str);

    void onReceivedUrl(String str);

    void onReceivedUrlAndTitle(String str, String str2);

    void onUpdateBackAndForward(boolean z, boolean z2);
}
