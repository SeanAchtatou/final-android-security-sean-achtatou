package com.cplatform.android.cmsurfclient.navigator;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.navigator.NavigatorTabHost;

public class NavigatorTabItem extends LinearLayout {
    private ImageView mItemIcon;
    private TextView mItemLabel;
    private int mTabIndex;
    private NavigatorTabHost.NavigatorTabSpec mTabSpec;

    public NavigatorTabItem(Context context) {
        super(context);
    }

    public NavigatorTabItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NavigatorTabItem(Context context, int tabIndex, NavigatorTabHost.NavigatorTabSpec tabSpec) {
        super(context);
        this.mTabIndex = tabIndex;
        this.mTabSpec = tabSpec;
        inflate(context, R.layout.navigator_widget_tabitem, this);
        setBackgroundResource(R.drawable.home_widget_bg);
        this.mItemIcon = (ImageView) findViewById(R.id.NavigatorTabItemIcon);
        this.mItemLabel = (TextView) findViewById(R.id.NavigatorTabItemLabel);
        this.mItemIcon.setImageBitmap(this.mTabSpec.getInactiveIcon());
        this.mItemLabel.setText(this.mTabSpec.getLabel());
    }

    public int getTabIndex() {
        return this.mTabIndex;
    }

    public void selectItem(boolean bSelected) {
        if (bSelected) {
            this.mItemLabel.setVisibility(0);
            this.mItemIcon.setImageBitmap(this.mTabSpec.getActiveIcon());
            setBackgroundResource(R.drawable.home_widget_bg_active);
            return;
        }
        this.mItemLabel.setVisibility(8);
        this.mItemIcon.setImageBitmap(this.mTabSpec.getInactiveIcon());
        setBackgroundResource(R.drawable.home_widget_bg);
    }
}
