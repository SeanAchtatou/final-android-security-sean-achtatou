package com.cplatform.android.cmsurfclient.share;

public class ShareContactItem {
    public boolean mIsSelected;
    public String mPhoneNumber;
    public String mUserName;

    public ShareContactItem(String userName, String phoneNumber, boolean isSelected) {
        this.mUserName = userName;
        this.mPhoneNumber = phoneNumber;
        this.mIsSelected = isSelected;
    }
}
