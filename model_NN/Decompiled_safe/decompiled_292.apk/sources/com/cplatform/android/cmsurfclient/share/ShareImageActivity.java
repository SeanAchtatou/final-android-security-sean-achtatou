package com.cplatform.android.cmsurfclient.share;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public class ShareImageActivity extends Activity {
    private static final String DEBUG_TAG = "ShareImageActivity";
    private static final int MAX_SHARE_USERNUMER = 5;
    private static final int REQUEST_CODE_SELECTUSER = 1;
    public static Bitmap mBmpShare = null;
    private Button mBtnCancel;
    private Button mBtnShare;
    private LinearLayout mBtnShareUser;
    /* access modifiers changed from: private */
    public Handler mHandler;
    /* access modifiers changed from: private */
    public ImageView mImgShare;
    private ImageView mImgShareUser;
    private SharePageAdapter mListAdapter;
    /* access modifiers changed from: private */
    public ArrayList<SharePageItem> mListUser;
    private ShareItemList mListViewTargetUser;
    /* access modifiers changed from: private */
    public LinearLayout mLoading;
    /* access modifiers changed from: private */
    public String mPageTitle;
    /* access modifiers changed from: private */
    public String mPageUrl;
    /* access modifiers changed from: private */
    public ShareHistoryDB mShareDB = null;
    /* access modifiers changed from: private */
    public String mShareSrc;
    private String mShareTitle;
    /* access modifiers changed from: private */
    public String mShareUrl;
    private TextView mTxtContacts;
    /* access modifiers changed from: private */
    public EditText mTxtShareComment;
    /* access modifiers changed from: private */
    public EditText mTxtShareTitle;
    /* access modifiers changed from: private */
    public AutoCompleteTextView mTxtShareUser;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.share_image);
        this.mHandler = new Handler();
        final Runnable mRunnable = new Runnable() {
            public void run() {
                ShareImageActivity.this.mImgShare.setImageBitmap(ShareImageActivity.mBmpShare);
                ShareImageActivity.this.mLoading.setVisibility(8);
                ShareImageActivity.this.mImgShare.setVisibility(0);
            }
        };
        this.mListViewTargetUser = (ShareItemList) findViewById(R.id.share_image_list_targetuser);
        this.mLoading = (LinearLayout) findViewById(R.id.share_image_loading);
        this.mImgShare = (ImageView) findViewById(R.id.share_image_image);
        this.mLoading.setVisibility(0);
        this.mImgShare.setVisibility(8);
        this.mTxtShareTitle = (EditText) findViewById(R.id.share_image_title);
        this.mTxtShareComment = (EditText) findViewById(R.id.share_image_comment);
        this.mTxtShareUser = (AutoCompleteTextView) findViewById(R.id.share_image_username);
        this.mTxtShareUser.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                }
            }
        });
        this.mImgShareUser = (ImageView) findViewById(R.id.share_image_view_username);
        this.mImgShareUser.setImageResource(17301506);
        this.mImgShareUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShareImageActivity.this.mTxtShareUser.showDropDown();
            }
        });
        this.mBtnShareUser = (LinearLayout) findViewById(R.id.share_image_btn_username);
        this.mBtnShareUser.setClickable(true);
        this.mBtnShareUser.setFocusable(true);
        this.mBtnShareUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShareImageActivity.this.mTxtShareUser.showDropDown();
            }
        });
        this.mShareDB = new ShareHistoryDB(this);
        if (this.mShareDB != null) {
            this.mShareDB.load();
            this.mTxtShareUser.setAdapter(new ArrayAdapter<>(this, 17367050, this.mShareDB.mShareBy));
            this.mTxtShareUser.setThreshold(0);
            if (this.mShareDB.mShareBy.size() > 0) {
                this.mTxtShareUser.setText(this.mShareDB.mShareBy.get(0).toString());
            }
        }
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.mShareTitle = b.getString("share_title");
            this.mShareUrl = b.getString("share_url");
            this.mShareSrc = b.getString("share_src");
            this.mPageTitle = b.getString("page_title");
            this.mPageUrl = b.getString("page_url");
        }
        if (this.mShareUrl != null && this.mShareUrl.length() > 0) {
            new Thread() {
                public void run() {
                    try {
                        ShareImageActivity.mBmpShare = ShareImageActivity.this.returnBitmap(ShareImageActivity.this.mShareSrc);
                        if (ShareImageActivity.mBmpShare != null) {
                            ShareImageActivity.this.mHandler.post(mRunnable);
                        }
                    } catch (Exception e) {
                    }
                }
            }.start();
        }
        this.mTxtShareTitle.setText(this.mShareTitle);
        this.mTxtShareTitle.setSingleLine(true);
        this.mTxtContacts = (TextView) findViewById(R.id.share_image_contacts);
        this.mTxtContacts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String users = WindowAdapter2.BLANK_URL;
                for (int i = 0; i < ShareImageActivity.this.mListUser.size(); i++) {
                    SharePageItem userItem = (SharePageItem) ShareImageActivity.this.mListUser.get(i);
                    String userInfo = userItem.mUserInfo;
                    if (userInfo != null && userInfo.length() > 0) {
                        users = users + userInfo;
                        if (i != ShareImageActivity.this.mListUser.size() - 1) {
                            users = users + ";";
                        }
                    } else if (userItem.mTxtInfo != null) {
                        userItem.mUserInfo = userItem.mTxtInfo.getText().toString();
                        users = users + userItem.mUserInfo;
                        if (i != ShareImageActivity.this.mListUser.size() - 1) {
                            users = users + ";";
                        }
                    }
                }
                Bundle b = new Bundle();
                b.putString("share_targetusers", users);
                ShareImageActivity.this.startActivityForResult(new Intent(ShareImageActivity.this, ShareSelectUserActivity.class).putExtras(b), 1);
            }
        });
        this.mBtnShare = (Button) findViewById(R.id.share_image_btn_send);
        this.mBtnShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String userName;
                String phoneNumber;
                String shareBy = ShareImageActivity.this.mTxtShareUser.getText().toString();
                if (ShareImageActivity.this.mShareDB != null && shareBy.length() > 0) {
                    ShareImageActivity.this.mShareDB.addShareBy(shareBy);
                }
                String shareTargets = WindowAdapter2.BLANK_URL;
                for (int i = 0; i < ShareImageActivity.this.mListUser.size(); i++) {
                    SharePageItem targetUser = (SharePageItem) ShareImageActivity.this.mListUser.get(i);
                    if (targetUser != null) {
                        if (targetUser.mUserInfo.length() == 0 && targetUser.mTxtInfo != null) {
                            targetUser.mUserInfo = targetUser.mTxtInfo.getText().toString();
                        }
                        if (targetUser.mUserInfo.length() > 0) {
                            int beginPos = targetUser.mUserInfo.indexOf("(");
                            int endPos = targetUser.mUserInfo.indexOf(")");
                            if (beginPos <= 0 || endPos <= 0) {
                                userName = WindowAdapter2.BLANK_URL;
                                phoneNumber = targetUser.mUserInfo;
                            } else {
                                phoneNumber = targetUser.mUserInfo.substring(0, beginPos);
                                userName = targetUser.mUserInfo.substring(beginPos + 1, endPos);
                            }
                            if (phoneNumber.length() > 0) {
                                shareTargets = shareTargets + phoneNumber;
                                if (i < ShareImageActivity.this.mListUser.size() - 1) {
                                    shareTargets = shareTargets + ",";
                                }
                                if (ShareImageActivity.this.mShareDB != null) {
                                    ShareImageActivity.this.mShareDB.addShareTo(phoneNumber, userName);
                                }
                            }
                        }
                    }
                }
                if (shareTargets.length() > 0) {
                    String params = "uid=";
                    Log.i(ShareImageActivity.DEBUG_TAG, "uid=" + SurfManagerActivity.mMsb.uid);
                    if (!TextUtils.isEmpty(SurfManagerActivity.mMsb.uid)) {
                        params = params + SurfManagerActivity.mMsb.uid;
                    }
                    String params2 = ((params + "&sends=") + shareTargets) + "&title=";
                    if (ShareImageActivity.this.mTxtShareTitle.getText().toString().length() > 0) {
                        try {
                            params2 = params2 + URLEncoder.encode(ShareImageActivity.this.mTxtShareTitle.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    String params3 = params2 + "&url=";
                    try {
                        if (ShareImageActivity.this.mShareSrc.length() > 0) {
                            params3 = params3 + URLEncoder.encode(ShareImageActivity.this.mShareSrc, "UTF-8");
                        } else {
                            params3 = params3 + URLEncoder.encode(ShareImageActivity.this.mShareUrl, "UTF-8");
                        }
                    } catch (UnsupportedEncodingException e2) {
                        e2.printStackTrace();
                    }
                    if (ShareImageActivity.this.mTxtShareUser.getText().toString().length() > 0) {
                        params3 = params3 + "&sharer=";
                        try {
                            params3 = params3 + URLEncoder.encode(ShareImageActivity.this.mTxtShareUser.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (ShareImageActivity.this.mTxtShareComment.getText().toString().length() > 0) {
                        params3 = params3 + "&remark=";
                        try {
                            params3 = params3 + URLEncoder.encode(ShareImageActivity.this.mTxtShareComment.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e4) {
                            e4.printStackTrace();
                        }
                    }
                    String params4 = (params3 + "&type=2") + "&page_title=";
                    if (ShareImageActivity.this.mPageTitle != null && ShareImageActivity.this.mPageTitle.length() > 0) {
                        try {
                            params4 = params4 + URLEncoder.encode(ShareImageActivity.this.mPageTitle, "UTF-8");
                        } catch (UnsupportedEncodingException e5) {
                            e5.printStackTrace();
                        }
                    }
                    String params5 = params4 + "&page_url=";
                    if (ShareImageActivity.this.mPageUrl != null && ShareImageActivity.this.mPageUrl.length() > 0) {
                        try {
                            params5 = params5 + URLEncoder.encode(ShareImageActivity.this.mPageUrl, "UTF-8");
                        } catch (UnsupportedEncodingException e6) {
                            e6.printStackTrace();
                        }
                    }
                    Log.i(ShareImageActivity.DEBUG_TAG, "Share page with params:" + params5);
                    ShareImageActivity.this.setResult(-1, new Intent().setAction(params5));
                    ShareImageActivity.this.finish();
                    return;
                }
                Toast.makeText(ShareImageActivity.this, (int) R.string.ishare_no_friend, 1).show();
            }
        });
        this.mBtnCancel = (Button) findViewById(R.id.share_image_btn_cancel);
        this.mBtnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShareImageActivity.this.finish();
            }
        });
        this.mListUser = new ArrayList<>();
        this.mListUser.add(new SharePageItem(WindowAdapter2.BLANK_URL));
        this.mListAdapter = new SharePageAdapter(this, this.mListUser, this.mShareDB.mShareTo, true);
        this.mListViewTargetUser.setAdapter(this.mListAdapter);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != 0) {
            String act = data.getAction();
            switch (requestCode) {
                case 1:
                    if (act != null) {
                        this.mListUser.clear();
                        String[] userArray = act.split(";");
                        for (String userInfo : userArray) {
                            this.mListUser.add(new SharePageItem(userInfo));
                        }
                        this.mListAdapter.notifyDataSetChanged();
                        this.mListViewTargetUser.setAdapter(this.mListAdapter);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onRemoveTargetUser(SharePageItem item) {
        this.mListAdapter.remove(item);
        this.mListViewTargetUser.setAdapter(this.mListAdapter);
    }

    public void onAddTargetUser() {
        if (this.mListUser.size() < 5) {
            this.mListAdapter.add(new SharePageItem(WindowAdapter2.BLANK_URL));
            this.mListViewTargetUser.setAdapter(this.mListAdapter);
            return;
        }
        Toast.makeText(this, getResources().getString(R.string.ishare_max_friends_left) + 5 + getResources().getString(R.string.ishare_max_friends_right), 1).show();
    }

    public void onModifyTargetUser(SharePageItem item) {
        for (int i = 0; i < this.mListUser.size(); i++) {
            SharePageItem userItem = this.mListUser.get(i);
            if (userItem.mTag.equals(item.mTag)) {
                userItem.mUserInfo = item.mUserInfo;
                this.mListUser.set(i, userItem);
                this.mListAdapter.notifyDataSetChanged();
                this.mListViewTargetUser.setAdapter(this.mListAdapter);
                return;
            }
        }
    }

    public Bitmap returnBitmap(String url) {
        Log.i(DEBUG_TAG, "returnBitmap with url:" + url);
        try {
            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters, 10000);
            HttpConnectionParams.setSoTimeout(httpParameters, 10000);
            HttpClient httpClient = new DefaultHttpClient(httpParameters);
            if (SurfManagerActivity.isCMWap()) {
                httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
            }
            HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
            if (httpResponse.getStatusLine().getStatusCode() != 200) {
                return null;
            }
            InputStream is = httpResponse.getEntity().getContent();
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            is.close();
            return bitmap;
        } catch (Exception e) {
            Log.e(DEBUG_TAG, "returnBitmap", e);
            return null;
        }
    }

    public class RequestThread extends Thread {
        String mImgUrl;

        public RequestThread(String imgUrl) {
            this.mImgUrl = imgUrl;
        }

        public void run() {
            try {
                ShareImageActivity.mBmpShare = ShareImageActivity.this.returnBitmap(this.mImgUrl);
            } catch (Exception e) {
            }
        }
    }
}
