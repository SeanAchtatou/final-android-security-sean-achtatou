package com.cplatform.android.cmsurfclient.bookmark;

import android.app.Activity;
import android.view.View;

public class BookmarkItemEditOnClickListener implements View.OnClickListener {
    public Activity mActivity;
    public BookmarkItem mItem;

    public BookmarkItemEditOnClickListener(Activity activity, BookmarkItem item) {
        this.mActivity = activity;
        this.mItem = item;
    }

    public void onClick(View v) {
        ((BookmarkActivity) this.mActivity).onEdit(this.mItem);
    }
}
