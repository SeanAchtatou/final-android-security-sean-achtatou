package com.cplatform.android.cmsurfclient;

import android.app.ActivityManager;
import android.content.ContentValues;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.URLUtil;
import android.webkit.WebBackForwardList;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.cplatform.android.cmsurfclient.download.provider.DownloadProviderHelper;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.preference.SurfBrowserSettings;
import com.cplatform.android.cmsurfclient.utils.Base64;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import com.cplatform.android.utils.ReflectUtil;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Iterator;

public class WebViewManager implements ISurfWebView {
    /* access modifiers changed from: private */
    public static final String LOG_TAG = WebViewManager.class.getSimpleName();
    static final int MAX_WEBVIEW_NUM = 5;
    static final int MESSAGE_HIDE_CONTROLS = 2;
    static final int MESSAGE_PRELOAD = 2;
    static final int MESSAGE_ZOOM = 1;
    static final String SHAREIMAGE_SRC = "src=";
    static final String SHAREIMAGE_TAG = "shareimage.do";
    static final String SHAREIMAGE_TITLE = "share_title=";
    static final String SHAREIMAGE_URL = "share_url=";
    static final String SHAREPAGE_SRC = "g3url=";
    /* access modifiers changed from: private */
    public BrowserViewNew mBrowser = null;
    private int mCurWebView = -1;
    public String mHitUrl = WindowAdapter2.BLANK_URL;
    private ArrayList<UrlHistory> mListUrlHistory = null;
    private ArrayList<SurfWebView> mListWebView = null;
    private Handler mMessageHandler = null;
    private String mOriginalUserAgent = WindowAdapter2.BLANK_URL;
    private SurfManagerActivity mSurfMgr = null;
    private int mUrlHistoryIdx = -1;

    public WebViewManager(BrowserViewNew browser) {
        this.mBrowser = browser;
        if (this.mBrowser != null) {
            this.mSurfMgr = this.mBrowser.getSurfManager();
        }
        this.mListWebView = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            SurfWebView webView = new SurfWebView(this.mSurfMgr);
            webView.setID(i);
            initWebView(webView);
            this.mListWebView.add(webView);
        }
        this.mCurWebView = -1;
        this.mListUrlHistory = new ArrayList<>();
        this.mUrlHistoryIdx = -1;
        this.mMessageHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        Bundle data = msg.getData();
                        if (data != null) {
                            Float zoomLevel = Float.valueOf(data.getFloat("zoom_level"));
                            if (WebViewManager.this.getCurWebView() != null && zoomLevel.floatValue() > 0.0f) {
                                Log.w(WebViewManager.LOG_TAG, "hangleMessage: zoom_level=" + zoomLevel);
                                WebViewManager.this.getCurWebView().zoomTo(zoomLevel.floatValue());
                                return;
                            }
                            return;
                        }
                        return;
                    case 2:
                        Bundle data2 = msg.getData();
                        if (data2 != null) {
                            String url = data2.getString("preload_url");
                            if (WebViewManager.this.getCurWebView() != null && !TextUtils.isEmpty(url)) {
                                Log.w(WebViewManager.LOG_TAG, "hangleMessage: preload_url=" + url);
                                WebViewManager.this.preloadUrl(url);
                                return;
                            }
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
    }

    public void onNetworkStatusChanged(int networkStatus) {
        SurfWebView view = getCurWebView();
        if (view != null) {
            view.onNetworkStatusChanged(networkStatus);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public SurfWebView getCurWebView() {
        if (this.mCurWebView < 0 || this.mCurWebView >= 5) {
            return null;
        }
        return this.mListWebView.get(this.mCurWebView);
    }

    public double getCurWebViewScale() {
        SurfWebView webView = getCurWebView();
        if (webView != null) {
            return (double) webView.getScale();
        }
        return 1.0d;
    }

    public SurfWebView getCachedWebView() {
        int nextWebView = this.mCurWebView - 1;
        if (nextWebView < 0) {
            nextWebView = 4;
        }
        return this.mListWebView.get(nextWebView);
    }

    public double getCachedWebViewScale() {
        SurfWebView webView = getCachedWebView();
        if (webView != null) {
            return (double) webView.getScale();
        }
        return 1.0d;
    }

    public void destroy() {
        for (int i = 0; i < 5; i++) {
            SurfWebView view = this.mListWebView.get(i);
            if (view != null) {
                view.stopLoading();
                view.destroy();
            }
        }
        this.mListWebView.clear();
        this.mListUrlHistory.clear();
    }

    public void goBack() {
        Log.e(LOG_TAG, "goBack: current position in visit history = " + this.mUrlHistoryIdx);
        if (this.mUrlHistoryIdx > 0) {
            this.mUrlHistoryIdx--;
            UrlHistory urlHistory = this.mListUrlHistory.get(this.mUrlHistoryIdx);
            this.mCurWebView = urlHistory.mWebViewIdx;
            Log.d(LOG_TAG, "set current position in visit history to " + this.mUrlHistoryIdx + ", and webview index to " + this.mCurWebView);
            findAndLoadUrl(urlHistory.mUrl, false, true);
        }
    }

    public void goForward() {
        Log.e(LOG_TAG, "goForward: current position in visit history = " + this.mUrlHistoryIdx);
        if (this.mUrlHistoryIdx < this.mListUrlHistory.size() - 1) {
            this.mUrlHistoryIdx++;
            UrlHistory urlHistory = this.mListUrlHistory.get(this.mUrlHistoryIdx);
            this.mCurWebView = urlHistory.mWebViewIdx;
            Log.d(LOG_TAG, "set current position in visit history to " + this.mUrlHistoryIdx + ", and webview index to " + this.mCurWebView);
            findAndLoadUrl(urlHistory.mUrl, false, true);
        }
    }

    public boolean canGoBack() {
        if (this.mUrlHistoryIdx > 0) {
            return true;
        }
        return false;
    }

    public boolean canGoForward() {
        if (this.mUrlHistoryIdx < this.mListUrlHistory.size() - 1) {
            return true;
        }
        return false;
    }

    public void pageUp(Boolean top) {
        SurfWebView view = getCurWebView();
        if (view != null) {
            view.pageUp(top.booleanValue());
        }
    }

    public void pageDown(Boolean bottom) {
        SurfWebView view = getCurWebView();
        if (view != null) {
            view.pageDown(bottom.booleanValue());
        }
    }

    public void zoomIn() {
        SurfWebView webView = getCurWebView();
        if (webView != null) {
            double scale = (double) webView.getScale();
            Log.e(LOG_TAG, "WebView scale before zoomin button clicked: " + scale);
            if (scale <= 1.25d) {
                webView.zoomIn();
            }
        }
    }

    public void zoomOut() {
        SurfWebView webView = getCurWebView();
        if (webView != null) {
            double scale = (double) webView.getScale();
            Log.e(LOG_TAG, "WebView scale before zoomout button clicked: " + scale);
            if (scale > 1.0d) {
                webView.zoomOut();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x002d A[LOOP:0: B:5:0x002d->B:8:0x003d, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zoomAuto() {
        /*
            r7 = this;
            com.cplatform.android.cmsurfclient.SurfWebView r2 = r7.getCurWebView()
            if (r2 == 0) goto L_0x002c
            float r3 = r2.getScale()
            double r0 = (double) r3
            java.lang.String r3 = com.cplatform.android.cmsurfclient.WebViewManager.LOG_TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "WebView scale before double tap: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r0)
            java.lang.String r4 = r4.toString()
            android.util.Log.e(r3, r4)
            r3 = 4608308318706860032(0x3ff4000000000000, double:1.25)
            int r3 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r3 > 0) goto L_0x002d
            r2.zoomIn()
        L_0x002c:
            return
        L_0x002d:
            float r3 = r2.getScale()
            double r3 = (double) r3
            r5 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r3 <= 0) goto L_0x002c
            boolean r3 = r2.zoomOut()
            r4 = 1
            if (r3 == r4) goto L_0x002d
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.WebViewManager.zoomAuto():void");
    }

    public void refresh() {
        Log.e(LOG_TAG, "refresh");
        SurfWebView webView = getCurWebView();
        if (webView != null && this.mBrowser != null) {
            webView.setVisibility(4);
            webView.loadUrlWithInitialScale(this.mBrowser.mItem.url, true, getCurWebViewScale());
        }
    }

    public void clearFormData() {
        for (int i = 0; i < this.mListWebView.size(); i++) {
            SurfWebView view = this.mListWebView.get(i);
            if (view != null) {
                view.clearFormData();
            }
        }
    }

    public void clearCache(Boolean isIncludeDiskFiles) {
        for (int i = 0; i < this.mListWebView.size(); i++) {
            SurfWebView view = this.mListWebView.get(i);
            if (view != null) {
                view.clearCache(isIncludeDiskFiles.booleanValue());
            }
        }
    }

    public void clearHistory() {
        Log.e(LOG_TAG, "clearHistory()");
        for (int i = 0; i < this.mListWebView.size(); i++) {
            SurfWebView view = this.mListWebView.get(i);
            if (view != null) {
                view.clearHistory();
            }
        }
        this.mListUrlHistory.clear();
        if (this.mCurWebView >= 0 && getCurWebView() != null) {
            UrlHistory history = new UrlHistory(this.mCurWebView, getCurWebView().getUrl());
            this.mUrlHistoryIdx = 0;
            this.mListUrlHistory.add(this.mUrlHistoryIdx, history);
        }
    }

    public void findAndLoadUrl(String url, boolean isSetInitialScale, boolean isGoBackOrForward) {
        Log.e(LOG_TAG, "try to find url in webview's backforward list");
        SurfWebView webView = getCurWebView();
        WebBackForwardList historyList = webView.copyBackForwardList();
        if (historyList.getSize() > 0) {
            int curIdx = historyList.getCurrentIndex();
            int dstIdx = -1;
            int i = 0;
            while (true) {
                if (i >= historyList.getSize()) {
                    break;
                } else if (historyList.getItemAtIndex(i).getUrl().equalsIgnoreCase(url)) {
                    dstIdx = i;
                    break;
                } else {
                    i++;
                }
            }
            if (dstIdx == -1) {
                Log.d(LOG_TAG, "no matched url found, load normally");
                webView.loadUrlWithInitialScale(url, isSetInitialScale, getCachedWebViewScale());
            } else if (dstIdx == curIdx) {
                Log.d(LOG_TAG, "current url is what we want");
                this.mBrowser.onPageFinished(webView.getUrl(), webView.getTitle(), true, canGoForward());
                switchToFront(webView);
                if (SurfBrowserSettings.getInstance().isRememberZoomLevel() && webView.isPreload() && ((double) webView.getScale()) != getCachedWebViewScale() && this.mUrlHistoryIdx == this.mListUrlHistory.size() - 1) {
                    Log.e(LOG_TAG, "Scale changed after page preloaded!");
                    webView.setPreload(false);
                    if (this.mMessageHandler != null) {
                        Log.w(LOG_TAG, "sendMessage: zoom_level=" + getCachedWebViewScale());
                        Message message = new Message();
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putFloat("zoom_level", (float) getCachedWebViewScale());
                        message.setData(bundle);
                        this.mMessageHandler.sendMessageDelayed(message, 200);
                    }
                }
                if ((!isGoBackOrForward || this.mUrlHistoryIdx == this.mListUrlHistory.size() - 1) && !TextUtils.isEmpty(webView.getPreloadUrl()) && this.mMessageHandler != null) {
                    Log.w(LOG_TAG, "sendMessage: preloadUrl=" + webView.getPreloadUrl());
                    Message message2 = new Message();
                    message2.what = 2;
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("preload_url", webView.getPreloadUrl());
                    message2.setData(bundle2);
                    this.mMessageHandler.sendMessageDelayed(message2, 300);
                }
            } else {
                Log.d(LOG_TAG, "matched url found at position " + dstIdx + ", load in cache of " + (dstIdx - curIdx) + " step");
                webView.loadUrlInCache(dstIdx - curIdx);
            }
        } else {
            Log.d(LOG_TAG, "backforward list empty, load normally");
            webView.loadUrlWithInitialScale(url, isSetInitialScale, getCachedWebViewScale());
        }
    }

    public void loadUrl(String url, boolean isSetInitialScale) {
        Log.e(LOG_TAG, "loarUrl()...");
        Log.w(LOG_TAG, "url: " + url);
        Log.w(LOG_TAG, "isSetInitialScale: " + isSetInitialScale);
        this.mCurWebView++;
        if (this.mCurWebView >= 5) {
            this.mCurWebView = 0;
        }
        Log.w(LOG_TAG, "set current webview index to " + this.mCurWebView);
        if (this.mUrlHistoryIdx < this.mListUrlHistory.size() - 1) {
            Log.d(LOG_TAG, "remove from " + (this.mUrlHistoryIdx + 1) + " to end of visit history");
            for (int i = this.mListUrlHistory.size() - 1; i > this.mUrlHistoryIdx; i--) {
                this.mListUrlHistory.remove(i);
            }
        }
        this.mUrlHistoryIdx++;
        this.mListUrlHistory.add(this.mUrlHistoryIdx, new UrlHistory(this.mCurWebView, url));
        Log.w(LOG_TAG, "add visit history at position " + this.mUrlHistoryIdx);
        findAndLoadUrl(url, isSetInitialScale, false);
    }

    private void switchToFront(SurfWebView webView) {
        if (this.mBrowser != null) {
            this.mBrowser.bringWebViewToFront(webView);
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    private void setPageCacheCapacity(WebSettings webSettings) {
        Object[] aObj = new Object[1];
        Class[] aClass = {Integer.TYPE};
        if (Build.VERSION.SDK_INT > 4) {
            if (((Integer) ReflectUtil.invokeMethodNoParams(ActivityManager.class, (ActivityManager) this.mSurfMgr.getSystemService("activity"), "getMemoryClass")).intValue() > 16) {
                aObj[0] = 5;
                ReflectUtil.invokeMethodWithParams(WebSettings.class, webSettings, "setPageCacheCapacity", aObj, aClass);
                return;
            }
            aObj[0] = 1;
            ReflectUtil.invokeMethodWithParams(WebSettings.class, webSettings, "setPageCacheCapacity", aObj, aClass);
            return;
        }
        aObj[0] = 1;
        ReflectUtil.invokeMethodWithParams(WebSettings.class, webSettings, "setPageCacheCapacity", aObj, aClass);
    }

    private void initWebView(SurfWebView webView) {
        webView.setListener(this);
        webView.setFocusable(true);
        webView.setFocusableInTouchMode(true);
        webView.setLongClickable(true);
        webView.setScrollBarStyle(0);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setNeedInitialFocus(false);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webView.hideZoomControls();
        String model = Build.MODEL;
        if (!TextUtils.isEmpty(model) && model.equalsIgnoreCase(SurfManagerActivity.MODEL_DOPOD_A8188)) {
            ReflectUtil.invokeDeclaredMethodNoParams(WebView.class, webView, "enableMultiTouch");
            Object[] aObj = {false};
            ReflectUtil.invokeDeclaredMethod(WebView.class, webView, "setFindDialogHeight", aObj, new Class[]{Boolean.TYPE});
        }
        if (this.mSurfMgr != null) {
            this.mSurfMgr.registerForContextMenu(webView);
            webSettings.setTextSize(SurfBrowserSettings.getInstance().getFontSize());
            StringBuilder userAgent = new StringBuilder();
            userAgent.append(this.mSurfMgr.mUserAgent);
            if (TextUtils.isEmpty(this.mOriginalUserAgent)) {
                this.mOriginalUserAgent = webSettings.getUserAgentString();
            }
            userAgent.append(this.mOriginalUserAgent);
            webSettings.setUserAgentString(userAgent.toString());
            Log.d(LOG_TAG, "Set UserAgent to " + userAgent.toString());
        }
        SurfBrowserSettings s = SurfBrowserSettings.getInstance();
        s.addObserver(webView.getSettings()).update(s, null);
        webView.setDownloadListener(new DownloadListener() {
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {
                Log.e(WebViewManager.LOG_TAG, "onDownloadStart：url=" + url);
                Log.e(WebViewManager.LOG_TAG, "onDownloadStart：userAgent=" + userAgent);
                Log.e(WebViewManager.LOG_TAG, "onDownloadStart：contentDisposition=" + contentDisposition);
                Log.e(WebViewManager.LOG_TAG, "onDownloadStart：mimetype=" + mimeType);
                Log.e(WebViewManager.LOG_TAG, "onDownloadStart：contentLength=" + contentLength);
                if (mimeType.equalsIgnoreCase(SurfBrowser.OMS_MIMETYPE)) {
                    WebViewManager.this.mBrowser.onDownloadOMSFile(url);
                } else {
                    WebViewManager.this.mBrowser.downloadFile(WindowAdapter2.BLANK_URL, url);
                }
            }
        });
        webView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                Log.e(WebViewManager.LOG_TAG, "WebView.onCreateContextMenu");
                WebView.HitTestResult hitTest = ((SurfWebView) v).getHitTestResult();
                if (hitTest != null) {
                    WebViewManager.this.mHitUrl = hitTest.getExtra();
                    Log.e(WebViewManager.LOG_TAG, "hitTestResult type = " + hitTest.getType());
                    Log.e(WebViewManager.LOG_TAG, "hitTestResult url = " + hitTest.getExtra());
                    switch (hitTest.getType()) {
                        case 0:
                            menu.add(0, 37, 0, (int) R.string.page_property);
                            menu.add(0, 39, 0, (int) R.string.menu_item_addbookmark);
                            menu.add(0, 38, 0, (int) R.string.closepage);
                            return;
                        case 1:
                            menu.add(0, 31, 0, (int) R.string.contextmenu_browser_open);
                            menu.add(0, 32, 0, (int) R.string.contextmenu_browser_opennew);
                            menu.add(0, 40, 0, (int) R.string.contextmenu_browser_openbackground);
                            menu.add(0, 33, 0, (int) R.string.contextmenu_browser_bookmark);
                            menu.add(0, 34, 0, (int) R.string.contextmenu_browser_sharelink);
                            return;
                        case 2:
                        case 3:
                        case 4:
                        default:
                            WebViewManager.this.mHitUrl = WindowAdapter2.BLANK_URL;
                            Log.v("setOnCreateContextMenuListener test", "enter onCreateContextMenu>>default");
                            return;
                        case 5:
                            menu.add(0, 36, 0, (int) R.string.contextmenu_browser_downloadpicture).setOnMenuItemClickListener(new Download(WebViewManager.this.mHitUrl));
                            return;
                        case 6:
                            menu.add(0, 31, 0, (int) R.string.contextmenu_browser_open);
                            menu.add(0, 32, 0, (int) R.string.contextmenu_browser_opennew);
                            menu.add(0, 40, 0, (int) R.string.contextmenu_browser_openbackground);
                            menu.add(0, 33, 0, (int) R.string.contextmenu_browser_bookmark);
                            menu.add(0, 34, 0, (int) R.string.contextmenu_browser_sharelink);
                            return;
                        case 7:
                            menu.add(0, 31, 0, (int) R.string.contextmenu_browser_open);
                            menu.add(0, 32, 0, (int) R.string.contextmenu_browser_opennew);
                            menu.add(0, 40, 0, (int) R.string.contextmenu_browser_openbackground);
                            menu.add(0, 33, 0, (int) R.string.contextmenu_browser_bookmark);
                            menu.add(0, 34, 0, (int) R.string.contextmenu_browser_sharelink);
                            return;
                        case 8:
                            menu.add(0, 31, 0, (int) R.string.contextmenu_browser_open);
                            menu.add(0, 32, 0, (int) R.string.contextmenu_browser_opennew);
                            menu.add(0, 40, 0, (int) R.string.contextmenu_browser_openbackground);
                            menu.add(0, 33, 0, (int) R.string.contextmenu_browser_bookmark);
                            menu.add(0, 34, 0, (int) R.string.contextmenu_browser_sharelink);
                            return;
                    }
                }
            }
        });
    }

    private class Download implements MenuItem.OnMenuItemClickListener {
        private String mText;

        public boolean onMenuItemClick(MenuItem item) {
            WebViewManager.this.downloadImage(null, this.mText);
            return true;
        }

        public Download(String toDownload) {
            this.mText = toDownload;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void downloadImage(String title, String url) {
        if (!TextUtils.isEmpty(url)) {
            if (url.startsWith(SurfManagerActivity.mMsb.domain)) {
                url = url.replace(SurfManagerActivity.mMsb.domain, "http://");
            }
            int srcPos = url.toLowerCase().indexOf(SHAREPAGE_SRC);
            if (srcPos > 0) {
                url = URLUtil.guessUrl(url.substring(SHAREPAGE_SRC.length() + srcPos));
                Log.e(LOG_TAG, "downloadImage - g3src:" + url);
            }
            if (title == null) {
                title = WindowAdapter2.BLANK_URL;
            }
            Log.e(LOG_TAG, "downloadImage: " + title);
            Log.e(LOG_TAG, url);
            ContentValues contentValues = new ContentValues();
            contentValues.put(Downloads.COLUMN_STATUS, (Integer) -100);
            contentValues.put(Downloads.COLUMN_APP_DATA, WindowAdapter2.BLANK_URL);
            DownloadProviderHelper.startDownload(this.mSurfMgr, url, WindowAdapter2.BLANK_URL, title, contentValues);
        }
    }

    public void preloadUrl(String nextPage) {
        Log.e(LOG_TAG, "preload next page: " + nextPage);
        int preloadWebView = this.mCurWebView + 1;
        if (preloadWebView >= 5) {
            preloadWebView = 0;
        }
        this.mListUrlHistory.add(this.mUrlHistoryIdx + 1, new UrlHistory(preloadWebView, nextPage));
        Log.d(LOG_TAG, "add visit history at position: " + (this.mUrlHistoryIdx + 1));
        SurfWebView webView = this.mListWebView.get(preloadWebView);
        webView.setPreload(true);
        webView.loadUrlWithInitialScale(nextPage, true, getCurWebViewScale());
    }

    public class UrlHistory {
        String mUrl;
        int mWebViewIdx;

        public UrlHistory(int idx, String url) {
            this.mWebViewIdx = idx;
            this.mUrl = url;
        }
    }

    private void printHistoryList() {
        int len = this.mListUrlHistory.size();
        Log.d(LOG_TAG, "history current: " + this.mUrlHistoryIdx);
        for (int i = 0; i < len; i++) {
            UrlHistory his = this.mListUrlHistory.get(i);
            Log.d(LOG_TAG, "history lst(" + i + "): " + his.mWebViewIdx + ", " + his.mUrl);
        }
        Log.d(LOG_TAG, "webview current: " + this.mCurWebView);
        int webId = 0;
        Iterator i$ = this.mListWebView.iterator();
        while (i$.hasNext()) {
            WebBackForwardList historyList = i$.next().copyBackForwardList();
            if (historyList.getSize() > 0) {
                Log.d(LOG_TAG, "webview" + webId + " current: " + historyList.getCurrentIndex());
                for (int i2 = 0; i2 < historyList.getSize(); i2++) {
                    Log.d(LOG_TAG, "webview" + webId + " list(" + i2 + "): " + historyList.getItemAtIndex(i2).getUrl());
                }
            }
            webId++;
        }
    }

    public boolean isCurrent(SurfWebView view) {
        return view == getCurWebView();
    }

    public void onLoadUrl(String url) {
        loadUrl(url, true);
    }

    public void onPageFinished(SurfWebView view, String url, String title) {
        if (this.mBrowser != null) {
            if (isCurrent(view)) {
                this.mBrowser.onPageFinished(url, title, true, canGoForward());
                switchToFront(view);
            } else {
                this.mBrowser.onUpdateBackAndForward(true, canGoForward());
            }
        }
        printHistoryList();
    }

    public void onPageStarted(SurfWebView view, String url) {
        CookieSyncManager.getInstance().resetSync();
        if (isCurrent(view)) {
            this.mBrowser.onPageStarted(url);
        }
    }

    public void onPreloadUrl(String url) {
        preloadUrl(url);
    }

    public void onProgressChanged(SurfWebView view, int newProgress) {
        CookieSyncManager.getInstance().sync();
        if (isCurrent(view)) {
            this.mBrowser.onProgressChanged(newProgress);
        }
    }

    public void onReceivedError(SurfWebView view, int errorCode, String description, String failingUrl) {
        view.loadUrl("file:///android_asset/html/error.html");
        if (isCurrent(view)) {
            this.mBrowser.onReceivedError(errorCode, description, failingUrl);
        }
    }

    public void onReceivedTitle(SurfWebView view, String title, boolean isUpdateTitleOnly) {
        Log.e(LOG_TAG, "onReceivedTitle(): " + title);
        if (!isCurrent(view)) {
            return;
        }
        if (isUpdateTitleOnly) {
            this.mBrowser.onReceivedTitle(title);
            return;
        }
        this.mBrowser.onReceivedUrlAndTitle(view.getUrl(), title);
        if (!view.isBrowseInCache()) {
            if (!TextUtils.isEmpty(view.getUrl()) && !view.getUrl().equalsIgnoreCase("about:blank") && !view.getUrl().startsWith("file:///android_asset/")) {
                UrlHistory urlHistory = this.mListUrlHistory.get(this.mUrlHistoryIdx);
                if (!urlHistory.mUrl.equalsIgnoreCase(view.getUrl())) {
                    Log.i(LOG_TAG, "urlHistory.mUrl = " + urlHistory.mUrl);
                    Log.i(LOG_TAG, "view.getUrl() = " + view.getUrl());
                    if (view.getUrl().equalsIgnoreCase(view.getOriginalUrl())) {
                        UrlHistory urlHistoryNew = new UrlHistory(this.mCurWebView, view.getUrl());
                        this.mUrlHistoryIdx++;
                        this.mListUrlHistory.add(this.mUrlHistoryIdx, urlHistoryNew);
                        Log.i(LOG_TAG, "add new visit history at pos: " + this.mUrlHistoryIdx);
                    } else {
                        this.mListUrlHistory.set(this.mUrlHistoryIdx, new UrlHistory(this.mCurWebView, view.getUrl()));
                        Log.i(LOG_TAG, "reset visit history at pos: " + this.mUrlHistoryIdx);
                    }
                }
            }
            switchToFront(view);
        }
    }

    public void onShareImage(String url) {
        String srcEncoded;
        String shareSrc;
        String shareUrl = WindowAdapter2.BLANK_URL;
        String shareTitle = WindowAdapter2.BLANK_URL;
        String shareSrc2 = WindowAdapter2.BLANK_URL;
        int urlPos = url.toLowerCase().indexOf(SHAREIMAGE_URL);
        if (urlPos > 0) {
            int endPos = url.indexOf("&", urlPos);
            if (endPos > 0) {
                shareUrl = url.substring(SHAREIMAGE_URL.length() + urlPos, endPos);
            } else {
                shareUrl = url.substring(SHAREIMAGE_URL.length() + urlPos);
            }
        }
        int srcPos = shareUrl.toLowerCase().indexOf(SHAREIMAGE_SRC);
        if (srcPos > 0) {
            int endPos2 = shareUrl.indexOf("&", srcPos);
            if (endPos2 > 0) {
                srcEncoded = shareUrl.substring(SHAREIMAGE_SRC.length() + srcPos, endPos2);
            } else {
                srcEncoded = shareUrl.substring(SHAREIMAGE_SRC.length() + srcPos);
            }
            Log.i(LOG_TAG, "shareImage - srcEncoded:" + srcEncoded);
            if (URLUtil.isValidUrl(srcEncoded)) {
                shareSrc = srcEncoded;
            } else {
                shareSrc = new String(Base64.decode(srcEncoded));
            }
            shareSrc2 = SurfManagerActivity.formatUrl(shareSrc);
            Log.e(LOG_TAG, "shareImage - shareSrc: " + shareSrc2);
        }
        int titlePos = url.toLowerCase().indexOf(SHAREIMAGE_TITLE);
        if (titlePos > 0) {
            int endPos3 = url.indexOf("&", titlePos);
            if (endPos3 > 0) {
                shareTitle = url.substring(SHAREIMAGE_TITLE.length() + titlePos, endPos3);
            } else {
                shareTitle = url.substring(SHAREIMAGE_TITLE.length() + titlePos);
            }
        }
        try {
            shareTitle = URLDecoder.decode(shareTitle, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            shareUrl = URLDecoder.decode(shareUrl, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        Log.i(LOG_TAG, "shareImage - title:" + shareTitle);
        Log.i(LOG_TAG, "shareImage - url:" + shareUrl);
        Log.i(LOG_TAG, "shareImage - src:" + shareSrc2);
        this.mBrowser.shareImage(shareTitle, shareUrl, shareSrc2, getCurWebView().getTitle(), getCurWebView().getUrl());
    }

    public void onUpdateVisitedHistory(SurfWebView view, String url) {
        if (isCurrent(view)) {
            this.mListUrlHistory.set(this.mUrlHistoryIdx, new UrlHistory(this.mCurWebView, url));
            Log.i(LOG_TAG, "reset visit history at pos: " + this.mUrlHistoryIdx);
        }
    }

    public boolean isHistoryTail(String url) {
        if (this.mListUrlHistory.size() <= 0 || !url.equals(this.mListUrlHistory.get(this.mListUrlHistory.size() - 1).mUrl)) {
            return false;
        }
        return true;
    }

    public void onGoBack() {
        if (canGoBack()) {
            goBack();
        } else if (this.mSurfMgr != null) {
            this.mSurfMgr.onBrowserClickBack();
        }
    }

    public void onGoForward() {
        if (canGoForward()) {
            goForward();
        }
    }
}
