package com.cplatform.android.cmsurfclient.windows;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;

public class WindowManagerView extends LinearLayout {
    private final String LOG_TAG = "WindowManagerView";
    private LinearLayout mBtnBack;
    private LinearLayout mBtnNew;
    private final LayoutInflater mFactory;
    private ListView mListView;
    public final SurfManagerActivity mSurfMgr;
    private WindowAdapter2 mWindowAdapter;
    private TextView mWindowTitle;

    public WindowManagerView(SurfManagerActivity context) {
        super(context);
        this.mSurfMgr = context;
        this.mFactory = LayoutInflater.from(context);
        this.mFactory.inflate((int) R.layout.windows_manager, this);
        initTitleBar();
        initWindowList();
        initToolBar();
        setFromWindowTag();
    }

    public void onBackPressed() {
        Log.i("WindowManagerView", "onBackPressed");
        goBack();
    }

    public void onDelete(WindowItemNew item) {
        if (this.mSurfMgr != null) {
            this.mSurfMgr.onCloseWebView(item);
            this.mWindowAdapter.notifyDataSetChanged();
        }
    }

    public void onSwitchTo(WindowItemNew item) {
        if (this.mSurfMgr != null) {
            this.mSurfMgr.mLastWindowItem = item;
            goBack();
        }
    }

    public void setFromWindowTag() {
        if (this.mWindowAdapter != null) {
            this.mWindowAdapter.notifyDataSetChanged();
        }
    }

    private void initTitleBar() {
        this.mWindowTitle = (TextView) findViewById(R.id.titlebar_text);
        this.mWindowTitle.setText((int) R.string.windowsmanager);
    }

    private void initWindowList() {
        if (this.mSurfMgr != null) {
            this.mWindowAdapter = new WindowAdapter2(this, this.mSurfMgr.mListWebView);
            if (this.mWindowAdapter != null) {
                this.mListView = (ListView) findViewById(R.id.windows_manager_listview);
                this.mListView.setAdapter((ListAdapter) this.mWindowAdapter);
            }
        }
    }

    private void initToolBar() {
        this.mBtnNew = (LinearLayout) findViewById(R.id.windows_manager_add);
        this.mBtnBack = (LinearLayout) findViewById(R.id.windows_manager_back);
        this.mBtnNew.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (WindowManagerView.this.mSurfMgr != null && WindowManagerView.this.mSurfMgr.addBrowserItem(false) != null) {
                    WindowManagerView.this.goBack();
                }
            }
        });
        this.mBtnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                WindowManagerView.this.goBack();
            }
        });
    }

    public void goBack() {
        if (this.mSurfMgr != null) {
            this.mSurfMgr.showCurrentWindowItem();
        }
    }
}
