package com.cplatform.android.cmsurfclient.cache;

import java.util.Date;

public class CacheItem {
    public Date date;
    public String file;
    public long id;
    public String url;

    public CacheItem(long id2, String url2, String file2, Date date2) {
        this.id = id2;
        this.url = url2;
        this.file = file2;
        this.date = date2;
    }
}
