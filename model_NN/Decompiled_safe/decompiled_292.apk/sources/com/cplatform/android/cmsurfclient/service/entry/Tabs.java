package com.cplatform.android.cmsurfclient.service.entry;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Tabs {
    public ArrayList<Tab> tabs = null;

    public Tabs() {
    }

    public Tabs(Element entry) {
        NodeList nlTab;
        if (entry != null && (nlTab = entry.getElementsByTagName("tab")) != null && nlTab.getLength() > 0) {
            this.tabs = new ArrayList<>();
            int len = nlTab.getLength();
            for (int i = 0; i < len; i++) {
                this.tabs.add(new Tab((Element) nlTab.item(i)));
            }
        }
    }
}
