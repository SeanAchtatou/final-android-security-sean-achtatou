package com.cplatform.android.cmsurfclient.searchtip;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;

public class SearchTipAdapter extends ArrayAdapter<SearchTipItem> {
    private Activity mActivity;

    public SearchTipAdapter(Activity activity, ArrayList<SearchTipItem> items) {
        super(activity, (int) R.layout.naviedit_searchtip_item, items);
        this.mActivity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        SearchTipItem item = (SearchTipItem) getItem(position);
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate((int) R.layout.naviedit_searchtip_item, (ViewGroup) null);
        } else {
            row = convertView;
        }
        row.setClickable(true);
        row.setOnClickListener(new SearchTipItemOnClickListener(this.mActivity, item));
        String titleTxt = item.title;
        if (titleTxt == null) {
            titleTxt = WindowAdapter2.BLANK_URL;
        }
        ((TextView) row.findViewById(R.id.searchtip_title)).setText(titleTxt.trim());
        return row;
    }
}
