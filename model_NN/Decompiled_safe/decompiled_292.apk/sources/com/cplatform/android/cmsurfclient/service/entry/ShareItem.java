package com.cplatform.android.cmsurfclient.service.entry;

import org.w3c.dom.Element;

public class ShareItem {
    public String mMessage;
    public String mPhoneNumber;
    public String mRetCode;

    public ShareItem(ShareItem item) {
        this.mPhoneNumber = item.mPhoneNumber;
        this.mRetCode = item.mRetCode;
        this.mMessage = item.mMessage;
    }

    public ShareItem(Element entry) {
        if (entry != null) {
            this.mPhoneNumber = entry.getAttribute("mobile");
            this.mRetCode = entry.getAttribute("retcode");
            this.mMessage = entry.getAttribute("retmessage");
        }
    }

    public String toString() {
        return "PhoneNumber:" + this.mPhoneNumber + " ReturnCode:" + this.mRetCode + " Message:" + this.mMessage;
    }
}
