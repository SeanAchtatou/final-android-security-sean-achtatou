package com.cplatform.android.cmsurfclient.bookmark_history;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.bookmark.BookmarkActivity;
import com.cplatform.android.cmsurfclient.history.HistoryActivity;

public class BookmarkHistoryTabActivity extends TabActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView((int) R.layout.bookmarkhistory_manager);
        new IndicatorView(this, new CharSequence[]{getResources().getString(R.string.bookmarkhistory_tab_bookmark), getResources().getString(R.string.bookmarkhistory_tab_history)}, new Intent[]{new Intent().setClass(this, BookmarkActivity.class), new Intent().setClass(this, HistoryActivity.class)}).setTabHostAppearance(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
