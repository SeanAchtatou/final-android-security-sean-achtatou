package com.cplatform.android.cmsurfclient.service.entry;

import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import org.w3c.dom.Element;

public class Item {
    public String act = null;
    public String color = null;
    public String img = null;
    public String name = null;
    public String url = null;

    public Item() {
    }

    public Item(Item item) {
        this.name = item.name;
        this.color = item.color;
        this.act = item.act;
        this.img = item.img;
        this.url = item.url;
    }

    public Item(Element entry) {
        if (entry != null) {
            this.name = entry.getAttribute(QueryApList.Carriers.NAME);
            this.color = entry.getAttribute("color");
            this.img = entry.getAttribute("img");
            this.act = entry.getAttribute("act");
            this.url = entry.getAttribute("url");
        }
    }

    public String toString() {
        return "name:" + this.name + "\tcolor:" + this.color + "\timg:" + this.img + "\tact:" + this.act + "\turl:" + this.url;
    }
}
