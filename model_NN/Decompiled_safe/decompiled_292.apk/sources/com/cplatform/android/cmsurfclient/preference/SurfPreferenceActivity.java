package com.cplatform.android.cmsurfclient.preference;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;

public class SurfPreferenceActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        Preference e = findPreference(SurfBrowserSettings.PREF_FONT_SIZE);
        e.setOnPreferenceChangeListener(this);
        e.setSummary(getVisualTextSizeName(getPreferenceScreen().getSharedPreferences().getString(SurfBrowserSettings.PREF_FONT_SIZE, null)));
        findPreference(SurfBrowserSettings.PREF_CLEAR_HISTORY).setOnPreferenceChangeListener(this);
        findPreference(SurfBrowserSettings.PREF_CLEAR_CACHE).setOnPreferenceChangeListener(this);
        findPreference(SurfBrowserSettings.PREF_CLEAR_COOKIES).setOnPreferenceChangeListener(this);
        findPreference(SurfBrowserSettings.PREF_CLEAR_FORMDATA).setOnPreferenceChangeListener(this);
        findPreference(SurfBrowserSettings.PREF_CLEAR_PASSWORDS).setOnPreferenceChangeListener(this);
        findPreference(SurfBrowserSettings.PREF_CWMAP_PREFERRED).setOnPreferenceChangeListener(this);
        findPreference(SurfBrowserSettings.PREF_RESET_DEFAULTS).setOnPreferenceChangeListener(this);
        Preference e2 = findPreference(SurfBrowserSettings.PREF_ADAPTER_PAGESIZE);
        e2.setOnPreferenceChangeListener(this);
        e2.setSummary(getVisualPageSizeName(getPreferenceScreen().getSharedPreferences().getString(SurfBrowserSettings.PREF_ADAPTER_PAGESIZE, null)));
        Preference e3 = findPreference(SurfBrowserSettings.PREF_ADAPTER_IMAGEQUALITY);
        e3.setOnPreferenceChangeListener(this);
        e3.setSummary(getVisualImageQualityName(getPreferenceScreen().getSharedPreferences().getString(SurfBrowserSettings.PREF_ADAPTER_IMAGEQUALITY, null)));
        Preference e4 = findPreference(SurfBrowserSettings.PREF_ADAPTER_IMAGESIZE);
        e4.setOnPreferenceChangeListener(this);
        e4.setSummary(getVisualImageSizeName(getPreferenceScreen().getSharedPreferences().getString(SurfBrowserSettings.PREF_ADAPTER_IMAGESIZE, null)));
        Preference e5 = findPreference(SurfBrowserSettings.PREF_ADAPTER_FONTSIZE);
        e5.setOnPreferenceChangeListener(this);
        e5.setSummary(getVisualFontSizeName(getPreferenceScreen().getSharedPreferences().getString(SurfBrowserSettings.PREF_ADAPTER_FONTSIZE, null)));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        SurfBrowserSettings.getInstance().syncSharedPreferences(getPreferenceScreen().getSharedPreferences());
    }

    public boolean onPreferenceChange(Preference pref, Object objValue) {
        if (pref.getKey().equals(SurfBrowserSettings.PREF_RESET_DEFAULTS)) {
            if (((Boolean) objValue).booleanValue()) {
                finish();
            }
        } else if (pref.getKey().equals(SurfBrowserSettings.PREF_FONT_SIZE)) {
            pref.setSummary(getVisualTextSizeName((String) objValue));
            return true;
        } else if (pref.getKey().equals(SurfBrowserSettings.PREF_CWMAP_PREFERRED)) {
            setResult(-1, new Intent());
            return true;
        } else if (pref.getKey().equals(SurfBrowserSettings.PREF_ADAPTER_PAGESIZE)) {
            pref.setSummary(getVisualPageSizeName((String) objValue));
            return true;
        } else if (pref.getKey().equals(SurfBrowserSettings.PREF_ADAPTER_IMAGEQUALITY)) {
            pref.setSummary(getVisualImageQualityName((String) objValue));
            return true;
        } else if (pref.getKey().equals(SurfBrowserSettings.PREF_ADAPTER_IMAGESIZE)) {
            pref.setSummary(getVisualImageSizeName((String) objValue));
            return true;
        } else if (pref.getKey().equals(SurfBrowserSettings.PREF_ADAPTER_FONTSIZE)) {
            pref.setSummary(getVisualFontSizeName((String) objValue));
            return true;
        }
        return false;
    }

    private CharSequence getVisualTextSizeName(String enumName) {
        CharSequence[] visualNames = getResources().getTextArray(R.array.pref_text_size_choices);
        CharSequence[] enumNames = getResources().getTextArray(R.array.pref_text_size_values);
        if (visualNames.length != enumNames.length) {
            return WindowAdapter2.BLANK_URL;
        }
        for (int i = 0; i < enumNames.length; i++) {
            if (enumNames[i].equals(enumName)) {
                return visualNames[i];
            }
        }
        return WindowAdapter2.BLANK_URL;
    }

    private CharSequence getVisualPageSizeName(String enumName) {
        CharSequence[] visualNames = getResources().getTextArray(R.array.pref_adapter_pagesize_choices);
        CharSequence[] enumNames = getResources().getTextArray(R.array.pref_adapter_pagesize_values);
        if (visualNames.length != enumNames.length) {
            return WindowAdapter2.BLANK_URL;
        }
        for (int i = 0; i < enumNames.length; i++) {
            if (enumNames[i].equals(enumName)) {
                return visualNames[i];
            }
        }
        return WindowAdapter2.BLANK_URL;
    }

    private CharSequence getVisualImageQualityName(String enumName) {
        CharSequence[] visualNames = getResources().getTextArray(R.array.pref_adapter_imagequality_choices);
        CharSequence[] enumNames = getResources().getTextArray(R.array.pref_adapter_imagequality_values);
        if (visualNames.length != enumNames.length) {
            return WindowAdapter2.BLANK_URL;
        }
        for (int i = 0; i < enumNames.length; i++) {
            if (enumNames[i].equals(enumName)) {
                return visualNames[i];
            }
        }
        return WindowAdapter2.BLANK_URL;
    }

    private CharSequence getVisualImageSizeName(String enumName) {
        CharSequence[] visualNames = getResources().getTextArray(R.array.pref_adapter_imagesize_choices);
        CharSequence[] enumNames = getResources().getTextArray(R.array.pref_adapter_imagesize_values);
        if (visualNames.length != enumNames.length) {
            return WindowAdapter2.BLANK_URL;
        }
        for (int i = 0; i < enumNames.length; i++) {
            if (enumNames[i].equals(enumName)) {
                return visualNames[i];
            }
        }
        return WindowAdapter2.BLANK_URL;
    }

    private CharSequence getVisualFontSizeName(String enumName) {
        CharSequence[] visualNames = getResources().getTextArray(R.array.pref_adapter_fontsize_choices);
        CharSequence[] enumNames = getResources().getTextArray(R.array.pref_adapter_fontsize_values);
        if (visualNames.length != enumNames.length) {
            return WindowAdapter2.BLANK_URL;
        }
        for (int i = 0; i < enumNames.length; i++) {
            if (enumNames[i].equals(enumName)) {
                return visualNames[i];
            }
        }
        return WindowAdapter2.BLANK_URL;
    }
}
