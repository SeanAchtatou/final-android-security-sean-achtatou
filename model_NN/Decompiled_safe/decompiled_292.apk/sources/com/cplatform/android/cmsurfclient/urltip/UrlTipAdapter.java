package com.cplatform.android.cmsurfclient.urltip;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;

public class UrlTipAdapter extends ArrayAdapter<UrlTipItem> {
    private Activity mActivity;

    public UrlTipAdapter(Activity activity, ArrayList<UrlTipItem> items) {
        super(activity, (int) R.layout.naviedit_urltip_item, items);
        this.mActivity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        UrlTipItem item = (UrlTipItem) getItem(position);
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate((int) R.layout.naviedit_urltip_item, (ViewGroup) null);
        } else {
            row = convertView;
        }
        row.setClickable(true);
        row.setOnClickListener(new UrlTipItemOnClickListener(this.mActivity, item));
        ((TextView) row.findViewById(R.id.urltip_url)).setText(item.url);
        String titleTxt = item.title;
        if (titleTxt != null) {
            titleTxt = titleTxt.trim();
        }
        if (WindowAdapter2.BLANK_URL.equals(titleTxt)) {
            titleTxt = null;
        }
        TextView title = (TextView) row.findViewById(R.id.urltip_title);
        if (titleTxt == null) {
            title.setText(WindowAdapter2.BLANK_URL);
            title.setVisibility(8);
        } else {
            title.setText(item.title);
            title.setVisibility(0);
        }
        return row;
    }
}
