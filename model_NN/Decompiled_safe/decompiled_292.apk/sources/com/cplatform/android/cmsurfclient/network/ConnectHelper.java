package com.cplatform.android.cmsurfclient.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebSettings;
import com.cplatform.android.cmsurfclient.network.android.APNUtil;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.lang.reflect.Method;
import java.net.Socket;

public class ConnectHelper {
    public static final int FEATURE_ALREADY_ACTIVE = 0;
    public static final String FEATURE_ENABLE_INTERNET = "internet";
    public static final String FEATURE_ENABLE_WAP = "wap";
    public static final int FEATURE_REQUEST_FAILED = 3;
    public static final int FEATURE_REQUEST_STARTED = 1;
    public static final int FEATURE_TYPE_NOT_AVAILABLE = 2;
    /* access modifiers changed from: private */
    public static final String LOG_TAG = ConnectHelper.class.getSimpleName();
    public static final int MESSAGE_DATA_STATE_CHANGED = 1;
    private static Context mContext = null;
    /* access modifiers changed from: private */
    public String mApType = WindowAdapter2.BLANK_URL;
    /* access modifiers changed from: private */
    public ConnectivityManager mConnMgr = null;
    /* access modifiers changed from: private */
    public Handler mHandler = null;
    /* access modifiers changed from: private */
    public String mInterfaceName = WindowAdapter2.BLANK_URL;
    /* access modifiers changed from: private */
    public boolean mIsOPhone = false;
    /* access modifiers changed from: private */
    public boolean mListening = false;
    /* access modifiers changed from: private */
    public NetworkManager mNetworkMgr = null;
    private ConnectivityBroadcastReceiver mReceiver;

    public static String getApType(NetworkInfo info) {
        if (info == null) {
            return WindowAdapter2.BLANK_URL;
        }
        try {
            return (String) NetworkInfo.class.getMethod("getApType", new Class[0]).invoke(info, new Object[0]);
        } catch (Exception e) {
            return WindowAdapter2.BLANK_URL;
        }
    }

    public static String getInterfaceName(NetworkInfo info) {
        if (info == null) {
            return WindowAdapter2.BLANK_URL;
        }
        try {
            return (String) NetworkInfo.class.getMethod("getInterfaceName", new Class[0]).invoke(info, new Object[0]);
        } catch (Exception e) {
            return WindowAdapter2.BLANK_URL;
        }
    }

    public static void setInterface(String param) {
        Log.e(LOG_TAG, "Call setInterface: " + param);
        try {
            Socket.class.getMethod("setInterface", String.class).invoke(null, param);
        } catch (Exception e) {
        }
    }

    public static void setProxy(String proxyIP, int proxyPort) {
        Log.e(LOG_TAG, "setProxy: " + proxyIP + ", " + proxyPort);
        Class<WebSettings> cls = WebSettings.class;
        try {
            Method setProxy = cls.getMethod("setProxy", Context.class, String.class, Integer.TYPE);
            if (setProxy != null) {
                setProxy.invoke(null, mContext, proxyIP, Integer.valueOf(proxyPort));
                return;
            }
            Log.e(LOG_TAG, "Failed to find method setProxy in class WebSettings");
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error occurred when call setProxy");
        }
    }

    public ConnectHelper(NetworkManager networkManager, boolean isOPhone) {
        this.mNetworkMgr = networkManager;
        mContext = this.mNetworkMgr.mSurfMgr;
        this.mIsOPhone = isOPhone;
        if (this.mIsOPhone) {
            this.mConnMgr = (ConnectivityManager) mContext.getSystemService("connectivity");
        }
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                Log.e(ConnectHelper.LOG_TAG, "handleMessage: " + msg.what);
                switch (msg.what) {
                    case 1:
                        Intent intent = (Intent) msg.obj;
                        boolean noConnectivity = intent.getBooleanExtra("noConnectivity", false);
                        boolean isFailover = intent.getBooleanExtra("isFailover", false);
                        String reason = intent.getStringExtra("reason");
                        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
                        NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra("otherNetwork");
                        boolean isConnected = false;
                        if (!noConnectivity && networkInfo != null) {
                            isConnected = networkInfo.isConnectedOrConnecting();
                        }
                        Log.w(ConnectHelper.LOG_TAG, "handleMessage: EXTRA_NO_CONNECTIVITY = " + noConnectivity);
                        Log.w(ConnectHelper.LOG_TAG, "handleMessage: EXTRA_IS_FAILOVER = " + isFailover);
                        Log.w(ConnectHelper.LOG_TAG, "handleMessage: EXTRA_REASON = " + reason);
                        Log.w(ConnectHelper.LOG_TAG, "handleMessage: EXTRA_NETWORK_INFO = " + networkInfo.toString());
                        Log.w(ConnectHelper.LOG_TAG, "handleMessage: EXTRA_OTHER_NETWORK_INFO = " + (otherNetworkInfo == null ? "[none]" : otherNetworkInfo.toString()));
                        Log.w(ConnectHelper.LOG_TAG, "handleMessage: isConnected = " + isConnected);
                        if (networkInfo != null) {
                            String networkType = "UNKNOWN";
                            if (networkInfo.getType() == 1) {
                                networkType = "WIFI";
                            } else if (!TextUtils.isEmpty(networkInfo.getExtraInfo())) {
                                networkType = networkInfo.getExtraInfo().toUpperCase();
                            }
                            if (isConnected) {
                                if (ConnectHelper.this.mIsOPhone) {
                                    String apType = ConnectHelper.getApType(networkInfo);
                                    Log.w(ConnectHelper.LOG_TAG, "handleMessage: apType = " + apType);
                                    Log.w(ConnectHelper.LOG_TAG, "handleMessage: networkType = " + networkType);
                                    if (!TextUtils.isEmpty(ConnectHelper.this.mApType) && !TextUtils.isEmpty(apType)) {
                                        if (ConnectHelper.this.mApType.equalsIgnoreCase(apType)) {
                                            String proxyIP = ConnectHelper.this.mApType.equalsIgnoreCase(ConnectHelper.FEATURE_ENABLE_WAP) ? "10.0.0.172" : null;
                                            int proxyPort = ConnectHelper.this.mApType.equalsIgnoreCase(ConnectHelper.FEATURE_ENABLE_WAP) ? 80 : 0;
                                            String unused = ConnectHelper.this.mInterfaceName = ConnectHelper.this.mApType.equalsIgnoreCase(ConnectHelper.FEATURE_ENABLE_WAP) ? ConnectHelper.getInterfaceName(networkInfo) : null;
                                            ConnectHelper.setProxy(proxyIP, proxyPort);
                                            ConnectHelper.setInterface(ConnectHelper.this.mInterfaceName);
                                            ConnectHelper.this.mNetworkMgr.onNetworkStateChanged(true, networkType);
                                            return;
                                        } else if (networkType.equalsIgnoreCase("WIFI")) {
                                            ConnectHelper.this.mConnMgr.stopUsingNetworkFeature(0, ConnectHelper.this.mApType);
                                            String unused2 = ConnectHelper.this.mApType = apType;
                                            ConnectHelper.setProxy(null, 0);
                                            ConnectHelper.setInterface(null);
                                            ConnectHelper.this.mNetworkMgr.onNetworkStateChanged(true, networkType);
                                            return;
                                        } else {
                                            ConnectHelper.this.mNetworkMgr.onNetworkActive();
                                            return;
                                        }
                                    } else {
                                        return;
                                    }
                                } else {
                                    ConnectHelper.this.mNetworkMgr.onNetworkStateChanged(true, networkType);
                                    return;
                                }
                            } else if (ConnectHelper.this.mIsOPhone) {
                                String apType2 = ConnectHelper.getApType(networkInfo);
                                Log.w(ConnectHelper.LOG_TAG, "handleMessage: apType = " + apType2);
                                Log.w(ConnectHelper.LOG_TAG, "handleMessage: networkType = " + networkType);
                                if (!TextUtils.isEmpty(ConnectHelper.this.mApType) && !TextUtils.isEmpty(apType2) && ConnectHelper.this.mApType.equalsIgnoreCase(apType2)) {
                                    ConnectHelper.this.mNetworkMgr.onNetworkStateChanged(false, networkType);
                                    return;
                                }
                                return;
                            } else {
                                ConnectHelper.this.mNetworkMgr.onNetworkStateChanged(false, networkType);
                                return;
                            }
                        } else {
                            return;
                        }
                    default:
                        return;
                }
            }
        };
        this.mReceiver = new ConnectivityBroadcastReceiver();
        startListening();
    }

    public boolean connect(boolean isCMWAPPreferred) {
        Log.e(LOG_TAG, "connect: isCMWAPPreferred = " + isCMWAPPreferred);
        if (this.mIsOPhone) {
            this.mApType = isCMWAPPreferred ? FEATURE_ENABLE_WAP : FEATURE_ENABLE_INTERNET;
            int result = this.mConnMgr.startUsingNetworkFeature(0, this.mApType);
            Log.e(LOG_TAG, "Result of startUsingNetworkFeature(" + this.mApType + ") : " + result);
            if (result == 0) {
                NetworkInfo[] arr$ = this.mConnMgr.getAllNetworkInfo();
                int len$ = arr$.length;
                int i$ = 0;
                while (true) {
                    if (i$ >= len$) {
                        break;
                    }
                    NetworkInfo netInfo = arr$[i$];
                    if (!this.mApType.equalsIgnoreCase(getApType(netInfo)) || !netInfo.isConnected()) {
                        i$++;
                    } else {
                        String proxyIP = this.mApType.equalsIgnoreCase(FEATURE_ENABLE_WAP) ? "10.0.0.172" : null;
                        int proxyPort = this.mApType.equalsIgnoreCase(FEATURE_ENABLE_WAP) ? 80 : 0;
                        this.mInterfaceName = this.mApType.equalsIgnoreCase(FEATURE_ENABLE_WAP) ? getInterfaceName(netInfo) : null;
                        setProxy(proxyIP, proxyPort);
                        setInterface(this.mInterfaceName);
                        String networkType = "UNKNOWN";
                        if (netInfo.getType() == 1) {
                            networkType = "WIFI";
                        } else if (!TextUtils.isEmpty(netInfo.getExtraInfo())) {
                            networkType = netInfo.getExtraInfo().toUpperCase();
                        }
                        this.mNetworkMgr.onNetworkStateChanged(true, networkType);
                    }
                }
            } else if (result == 2 || result == 3) {
                this.mNetworkMgr.onNetworkStateChanged(false, isCMWAPPreferred ? "CMWAP" : "CMNET");
                return false;
            }
        } else {
            APNUtil.getInstance(mContext).modifyAPN(isCMWAPPreferred, true);
        }
        return true;
    }

    public void disconnect() {
        Log.e(LOG_TAG, "diconnect()");
        if (this.mIsOPhone) {
            Log.e(LOG_TAG, "stopUsingNetworkFeature: " + this.mApType);
            this.mConnMgr.stopUsingNetworkFeature(0, this.mApType);
            setProxy(null, 0);
            setInterface(null);
        } else {
            APNUtil.getInstance(mContext).restoreAPN();
        }
        stopListening();
    }

    private synchronized void startListening() {
        Log.e(LOG_TAG, "startListening...");
        if (!this.mListening) {
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            mContext.registerReceiver(this.mReceiver, filter);
            this.mListening = true;
        }
    }

    private synchronized void stopListening() {
        Log.e(LOG_TAG, "stopListening...");
        if (this.mListening) {
            mContext.unregisterReceiver(this.mReceiver);
            this.mListening = false;
        }
    }

    private class ConnectivityBroadcastReceiver extends BroadcastReceiver {
        private ConnectivityBroadcastReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            Log.e(ConnectHelper.LOG_TAG, "ConnectivityBroadcastReceiver.onReceive(): intent = " + intent.toString());
            if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE") && ConnectHelper.this.mListening && ConnectHelper.this.mHandler != null) {
                ConnectHelper.this.mHandler.sendMessage(ConnectHelper.this.mHandler.obtainMessage(1, intent));
            }
        }
    }
}
