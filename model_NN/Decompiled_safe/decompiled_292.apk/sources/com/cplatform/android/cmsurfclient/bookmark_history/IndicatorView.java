package com.cplatform.android.cmsurfclient.bookmark_history;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;

public class IndicatorView extends View {
    private static final int selectedTextStyle = 2131361801;
    private static final int unSelectedTextStyle = 2131361800;
    /* access modifiers changed from: private */
    public Context mContext;
    private Intent[] mIntents;
    private CharSequence[] mTabNames;
    private View mView;

    public IndicatorView(Context context, CharSequence[] tabNames, Intent[] intents) {
        super(context);
        this.mContext = context;
        this.mTabNames = tabNames;
        this.mIntents = intents;
    }

    private View getIndicatorView(CharSequence charsequence) {
        this.mView = LayoutInflater.from(this.mContext).inflate((int) R.layout.bookmarkhistory_tabhost_tab, (ViewGroup) null);
        ((TextView) this.mView.findViewById(R.id.tab_name)).setText(charsequence);
        return this.mView;
    }

    public void setSelectAppearance() {
        ((TextView) this.mView.findViewById(R.id.tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_selected);
    }

    public void setTabHostAppearance(TabActivity tabactivity) {
        final TabHost tabHost = tabactivity.getTabHost();
        final TabWidget tabWidget = tabHost.getTabWidget();
        int tabNamesCount = this.mTabNames.length;
        for (int j = 0; j < tabNamesCount; j++) {
            tabHost.addTab(tabHost.newTabSpec(this.mTabNames[j].toString()).setIndicator(getIndicatorView(this.mTabNames[j])).setContent(this.mIntents[j]));
        }
        int tabCount = tabWidget.getChildCount();
        for (int j2 = 0; j2 < tabCount; j2 = j2 + 1 + 1) {
            ((TextView) tabWidget.getChildAt(j2).findViewById(R.id.tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_unselected);
        }
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String s) {
                int childCount = tabWidget.getChildCount();
                int i = 0;
                while (i < childCount) {
                    ((TextView) tabWidget.getChildAt(i).findViewById(R.id.tab_name)).setTextAppearance(IndicatorView.this.mContext, tabHost.getCurrentTab() == i ? R.style.download_tab_text_selected : R.style.download_tab_text_unselected);
                    i++;
                }
            }
        });
        tabHost.setCurrentTab(0);
        ((TextView) tabWidget.getChildAt(0).findViewById(R.id.tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_selected);
    }

    public void setUnSelectAppearance() {
        ((TextView) this.mView.findViewById(R.id.tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_selected);
    }
}
