package com.cplatform.android.cmsurfclient.download.provider;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.download.ui.DownloadHelper;
import com.cplatform.android.cmsurfclient.download.ui.DownloadHomeTabActivity;
import com.cplatform.android.cmsurfclient.download.util.FileNameUtils;

public final class DownloadProviderHelper {
    private DownloadProviderHelper() {
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [java.lang.Throwable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean retryDownload(android.content.Context r11, int r12) throws com.cplatform.android.cmsurfclient.download.util.FileNameUtils.WebAddressParserException {
        /*
            r7 = 0
            if (r11 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "ctx"
            r2.<init>(r3)
            throw r2
        L_0x000b:
            if (r12 >= 0) goto L_0x0015
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "id"
            r2.<init>(r3)
            throw r2
        L_0x0015:
            java.lang.String r2 = "download/provider"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Retrying download: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r12)
            java.lang.String r3 = r3.toString()
            android.util.Log.v(r2, r3)
            android.net.Uri r2 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
            java.lang.String r3 = java.lang.String.valueOf(r12)
            android.net.Uri r1 = android.net.Uri.withAppendedPath(r2, r3)
            android.content.ContentResolver r0 = r11.getContentResolver()
            r2 = 0
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r7 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ WebAddressParserException -> 0x0110 }
            boolean r2 = r7.moveToFirst()     // Catch:{ WebAddressParserException -> 0x0110 }
            if (r2 == 0) goto L_0x011d
            r2 = 0
            r3 = 0
            r0.delete(r1, r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "_data"
            int r2 = r7.getColumnIndex(r2)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r10 = r7.getString(r2)     // Catch:{ WebAddressParserException -> 0x0110 }
            boolean r2 = android.text.TextUtils.isEmpty(r10)     // Catch:{ WebAddressParserException -> 0x0110 }
            if (r2 != 0) goto L_0x0074
            java.io.File r9 = new java.io.File     // Catch:{ WebAddressParserException -> 0x0110 }
            r9.<init>(r10)     // Catch:{ WebAddressParserException -> 0x0110 }
            boolean r2 = r9.exists()     // Catch:{ WebAddressParserException -> 0x0110 }
            if (r2 == 0) goto L_0x0074
            boolean r2 = r9.isDirectory()     // Catch:{ WebAddressParserException -> 0x0110 }
            if (r2 != 0) goto L_0x0074
            r9.delete()     // Catch:{ WebAddressParserException -> 0x0110 }
        L_0x0074:
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.<init>()     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "uri"
            java.lang.String r3 = "uri"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "useragent"
            java.lang.String r3 = "useragent"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "notificationpackage"
            java.lang.String r3 = "notificationpackage"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "notificationclass"
            java.lang.String r3 = "notificationclass"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "visibility"
            java.lang.String r3 = "visibility"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            int r3 = r7.getInt(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "mimetype"
            java.lang.String r3 = "mimetype"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "hint"
            java.lang.String r3 = "hint"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "title"
            java.lang.String r3 = "title"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r2 = "description"
            java.lang.String r3 = "description"
            int r3 = r7.getColumnIndex(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            java.lang.String r3 = r7.getString(r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            r6.put(r2, r3)     // Catch:{ WebAddressParserException -> 0x0110 }
            android.net.Uri r2 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()     // Catch:{ WebAddressParserException -> 0x0110 }
            r0.insert(r2, r6)     // Catch:{ WebAddressParserException -> 0x0110 }
            r7.close()     // Catch:{ WebAddressParserException -> 0x0110 }
            r2 = 1
        L_0x010f:
            return r2
        L_0x0110:
            r2 = move-exception
            r8 = r2
            java.lang.String r2 = "download/provider"
            java.lang.String r3 = "error"
            android.util.Log.e(r2, r3, r8)
            r7.close()     // Catch:{ Exception -> 0x011f }
        L_0x011c:
            throw r8
        L_0x011d:
            r2 = 0
            goto L_0x010f
        L_0x011f:
            r2 = move-exception
            goto L_0x011c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.DownloadProviderHelper.retryDownload(android.content.Context, int):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public static void _startDownload(Context context, String url, String hint, String title, ContentValues contentvalues) throws FileNameUtils.WebAddressParserException {
        if (context == null) {
            throw new IllegalArgumentException("ctx");
        } else if (url == null || url.length() == 0) {
            throw new IllegalArgumentException("url");
        } else {
            Log.v(Constants.TAG, "Starting download: " + url);
            FileNameUtils.UriAndFileName uriandfilename = FileNameUtils.guessFileName(url);
            contentvalues.put(Downloads.COLUMN_URI, uriandfilename.Uri.toString());
            if (!contentvalues.containsKey(Downloads.COLUMN_USER_AGENT)) {
                contentvalues.put(Downloads.COLUMN_USER_AGENT, Constants.UserAgent_Android_1dot5);
            }
            contentvalues.put(Downloads.COLUMN_NOTIFICATION_PACKAGE, context.getPackageName());
            contentvalues.put(Downloads.COLUMN_NOTIFICATION_CLASS, DownloadSettings.getDownloadReceiverClassName());
            contentvalues.put(Downloads.COLUMN_VISIBILITY, (Integer) 1);
            if (!contentvalues.containsKey(Downloads.COLUMN_MIME_TYPE)) {
                contentvalues.put(Downloads.COLUMN_MIME_TYPE, uriandfilename.Mimetype);
            }
            if (!TextUtils.isEmpty(hint)) {
                contentvalues.put(Downloads.COLUMN_FILE_NAME_HINT, hint);
            } else {
                contentvalues.put(Downloads.COLUMN_FILE_NAME_HINT, uriandfilename.Filename);
            }
            if (!TextUtils.isEmpty(title)) {
                contentvalues.put(Downloads.COLUMN_TITLE, title);
            } else {
                contentvalues.put(Downloads.COLUMN_TITLE, uriandfilename.Filename);
            }
            if (!contentvalues.containsKey(Downloads.COLUMN_DESCRIPTION)) {
                contentvalues.put(Downloads.COLUMN_DESCRIPTION, uriandfilename.Uri.getHost());
            }
            context.getContentResolver().insert(Uri.parse("content://" + DownloadSettings.getDownloadProviderAuthorities() + "/download"), contentvalues);
            Toast.makeText(context, (int) R.string.download_started, 0).show();
            if (1 != 0) {
                Intent intent = new Intent(context, DownloadHomeTabActivity.class);
                intent.setFlags(268435456);
                context.startActivity(intent);
            }
        }
    }

    /* JADX INFO: Multiple debug info for r10v3 android.app.AlertDialog$Builder: [D('context' android.content.Context), D('builder' android.app.AlertDialog$Builder)] */
    public static void startDownload(Context context, String url, String hint, String title, ContentValues contentvalues) throws FileNameUtils.WebAddressParserException {
        if (context == null) {
            throw new IllegalArgumentException("ctx");
        } else if (url == null || url.length() == 0) {
            throw new IllegalArgumentException("url");
        } else if (!Environment.getExternalStorageState().equals("mounted")) {
            Toast.makeText(context, (int) R.string.download_writesdcard_failed, 0).show();
        } else {
            Log.v(Constants.TAG, "Starting download: " + url);
            DownloadInfo downloadInfo = getTaskByUrl(context, FileNameUtils.guessFileName(url).Uri.toString());
            if (downloadInfo != null) {
                final Context context1 = context;
                final int id = downloadInfo.mId;
                int status = downloadInfo.mStatus;
                final String localPath = downloadInfo.mFileName;
                final String url1 = url;
                final String hint1 = hint;
                final String title1 = title;
                final ContentValues contentvalues1 = contentvalues;
                if (Downloads.isStatusCompleted(status)) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context1);
                    builder.setTitle((int) R.string.app_name);
                    builder.setMessage((int) R.string.download_prompt_redownload);
                    builder.setCancelable(true).setPositiveButton((int) R.string.download_save, new DialogInterface.OnClickListener(null) {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            dialoginterface.dismiss();
                            DownloadHelper.deleteDownloadTask(context1, Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(id)), null, localPath);
                            DownloadProviderHelper._startDownload(context1, url1, hint1, title1, contentvalues1);
                        }
                    }).setNegativeButton((int) R.string.download_do_not_save, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialoginterface, int i) {
                            dialoginterface.dismiss();
                        }
                    });
                    builder.create().show();
                } else if (Downloads.isStatusError(status)) {
                    DownloadHelper.deleteDownloadTask(context, Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(id)), null, localPath);
                    _startDownload(context, url, hint, title, contentvalues);
                } else {
                    Toast.makeText(context, (int) R.string.download_already_in_queue, 0).show();
                }
            } else {
                _startDownload(context, url, hint, title, contentvalues);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public static void startDownload(Context context, String s, String s1, String s2, Boolean boolean1) throws FileNameUtils.WebAddressParserException {
        ContentValues contentvalues = new ContentValues();
        if (boolean1.booleanValue()) {
            contentvalues.put(Downloads.COLUMN_STATUS, (Integer) -100);
        }
        startDownload(context, s, s1, s2, contentvalues);
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00e7 A[SYNTHETIC, Splitter:B:40:0x00e7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.cplatform.android.cmsurfclient.download.provider.DownloadInfo getTaskByUrl(android.content.Context r34, java.lang.String r35) {
        /*
            r9 = 0
            android.content.ContentResolver r3 = r34.getContentResolver()     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            android.net.Uri r4 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r34 = 5
            r0 = r34
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r5 = r0
            r34 = 0
            java.lang.String r6 = "_id"
            r5[r34] = r6     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r34 = 1
            java.lang.String r6 = "title"
            r5[r34] = r6     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r34 = 2
            java.lang.String r6 = "uri"
            r5[r34] = r6     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r34 = 3
            java.lang.String r6 = "status"
            r5[r34] = r6     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r34 = 4
            java.lang.String r6 = "_data"
            r5[r34] = r6     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            java.lang.String r6 = "uri = ?"
            r34 = 1
            r0 = r34
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            r7 = r0
            r34 = 0
            r7[r34] = r35     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            java.lang.String r8 = "_id"
            android.database.Cursor r34 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x00c6, all -> 0x00e0 }
            if (r34 != 0) goto L_0x004d
            r35 = 0
            if (r34 == 0) goto L_0x004c
            r34.close()     // Catch:{ Exception -> 0x00ed }
        L_0x004a:
            r34 = 0
        L_0x004c:
            return r35
        L_0x004d:
            r34.moveToFirst()     // Catch:{ Exception -> 0x00fc }
            boolean r35 = r34.isAfterLast()     // Catch:{ Exception -> 0x00fc }
            if (r35 != 0) goto L_0x00b5
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r3 = new com.cplatform.android.cmsurfclient.download.provider.DownloadInfo     // Catch:{ Exception -> 0x00fc }
            java.lang.String r35 = "_id"
            int r35 = r34.getColumnIndexOrThrow(r35)     // Catch:{ Exception -> 0x00fc }
            int r4 = r34.getInt(r35)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r35 = "uri"
            int r35 = r34.getColumnIndexOrThrow(r35)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r5 = r34.getString(r35)     // Catch:{ Exception -> 0x00fc }
            r6 = 0
            r7 = 0
            java.lang.String r35 = "_data"
            int r35 = r34.getColumnIndexOrThrow(r35)     // Catch:{ Exception -> 0x00fc }
            java.lang.String r8 = r34.getString(r35)     // Catch:{ Exception -> 0x00fc }
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.String r35 = "status"
            int r35 = r34.getColumnIndexOrThrow(r35)     // Catch:{ Exception -> 0x00fc }
            int r13 = r34.getInt(r35)     // Catch:{ Exception -> 0x00fc }
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 0
            r25 = 0
            r26 = 0
            r27 = 0
            r29 = 0
            r30 = 0
            r31 = 0
            r32 = 0
            r33 = 0
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r19, r20, r21, r22, r23, r24, r25, r26, r27, r29, r30, r31, r32, r33)     // Catch:{ Exception -> 0x00fc }
            if (r34 == 0) goto L_0x00b2
            r34.close()     // Catch:{ Exception -> 0x00f0 }
        L_0x00b0:
            r34 = 0
        L_0x00b2:
            r35 = r3
            goto L_0x004c
        L_0x00b5:
            if (r34 == 0) goto L_0x00bc
            r34.close()     // Catch:{ Exception -> 0x00f2 }
        L_0x00ba:
            r34 = 0
        L_0x00bc:
            if (r34 == 0) goto L_0x00c3
            r34.close()     // Catch:{ Exception -> 0x00f4 }
        L_0x00c1:
            r34 = 0
        L_0x00c3:
            r35 = 0
            goto L_0x004c
        L_0x00c6:
            r34 = move-exception
            r35 = r34
            r34 = r9
        L_0x00cb:
            java.lang.String r3 = "Uri e"
            java.lang.String r4 = "error"
            r0 = r3
            r1 = r4
            r2 = r35
            android.util.Log.e(r0, r1, r2)     // Catch:{ all -> 0x00fa }
            r34 = 0
            if (r34 == 0) goto L_0x00c3
            r34.close()     // Catch:{ Exception -> 0x00f6 }
        L_0x00dd:
            r34 = 0
            goto L_0x00c3
        L_0x00e0:
            r34 = move-exception
            r35 = r34
            r34 = r9
        L_0x00e5:
            if (r34 == 0) goto L_0x00ec
            r34.close()     // Catch:{ Exception -> 0x00f8 }
        L_0x00ea:
            r34 = 0
        L_0x00ec:
            throw r35
        L_0x00ed:
            r34 = move-exception
            goto L_0x004a
        L_0x00f0:
            r34 = move-exception
            goto L_0x00b0
        L_0x00f2:
            r34 = move-exception
            goto L_0x00ba
        L_0x00f4:
            r34 = move-exception
            goto L_0x00c1
        L_0x00f6:
            r34 = move-exception
            goto L_0x00dd
        L_0x00f8:
            r34 = move-exception
            goto L_0x00ea
        L_0x00fa:
            r35 = move-exception
            goto L_0x00e5
        L_0x00fc:
            r35 = move-exception
            goto L_0x00cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.DownloadProviderHelper.getTaskByUrl(android.content.Context, java.lang.String):com.cplatform.android.cmsurfclient.download.provider.DownloadInfo");
    }
}
