package com.cplatform.android.cmsurfclient.service.entry;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SNews {
    public ArrayList<Item> items = null;
    public String url = null;

    public SNews() {
    }

    public SNews(Element entry) {
        if (entry != null) {
            this.url = entry.getAttribute("url");
            NodeList nlItem = entry.getElementsByTagName("item");
            if (nlItem != null && nlItem.getLength() > 0) {
                this.items = new ArrayList<>();
                int len = nlItem.getLength();
                for (int m = 0; m < len; m++) {
                    this.items.add(new Item((Element) nlItem.item(m)));
                }
            }
        }
    }
}
