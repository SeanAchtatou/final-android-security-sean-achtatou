package com.cplatform.android.cmsurfclient.download.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import com.cplatform.android.cmsurfclient.R;

public class DownloadHomeTabActivity extends TabActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.downloadhome);
        new IndicatorView(this, new CharSequence[]{getResources().getString(R.string.downloadhome_tab_running_tasks), getResources().getString(R.string.downloadhome_tab_completed_tasks)}, new Intent[]{new Intent().setClass(this, DownloadRunningTasksActivity.class), new Intent().setClass(this, DownloadCompletedTasksActivity.class)}).setTabHostAppearance(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }
}
