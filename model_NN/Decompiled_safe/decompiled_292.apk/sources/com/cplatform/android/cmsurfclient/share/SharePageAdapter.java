package com.cplatform.android.cmsurfclient.share;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.cplatform.android.cmsurfclient.R;
import java.util.ArrayList;

public class SharePageAdapter extends ArrayAdapter<SharePageItem> {
    private Activity mActivity;
    /* access modifiers changed from: private */
    public boolean mIsShareImage;
    private ArrayList<String> mShareTo;

    public SharePageAdapter(Activity activity, ArrayList<SharePageItem> items, ArrayList<String> shareTo, boolean isShareImage) {
        super(activity, (int) R.layout.share_page_listitem, items);
        this.mActivity = activity;
        this.mShareTo = shareTo;
        this.mIsShareImage = isShareImage;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate(R.layout.share_page_listitem, (ViewGroup) null);
        } else {
            row = convertView;
        }
        AutoCompleteTextView txtUserInfo = (AutoCompleteTextView) row.findViewById(R.id.share_page_listitem_txt_userinfo);
        txtUserInfo.setAdapter(new ArrayAdapter(this.mActivity, 17367050, this.mShareTo));
        txtUserInfo.setThreshold(0);
        SharePageItem item = (SharePageItem) getItem(position);
        txtUserInfo.setText(item.mUserInfo);
        item.mTxtInfo = txtUserInfo;
        txtUserInfo.setOnFocusChangeListener(new UserInfoFocusChangeListener(this.mActivity, item));
        txtUserInfo.addTextChangedListener(new UserInfoTextWatcher(this.mActivity, item));
        ImageView imgDropdown = (ImageView) row.findViewById(R.id.share_page_listitem_view_dropdown);
        imgDropdown.setImageResource(17301506);
        final AutoCompleteTextView autoCompleteTextView = txtUserInfo;
        imgDropdown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                autoCompleteTextView.showDropDown();
            }
        });
        LinearLayout btnDropdown = (LinearLayout) row.findViewById(R.id.share_page_listitem_btn_dropdown);
        btnDropdown.setClickable(true);
        btnDropdown.setFocusable(true);
        final AutoCompleteTextView autoCompleteTextView2 = txtUserInfo;
        btnDropdown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                autoCompleteTextView2.showDropDown();
            }
        });
        ImageButton btnAddUser = (ImageButton) row.findViewById(R.id.share_page_listitem_btn_add);
        btnAddUser.setBackgroundResource(17301547);
        ImageButton btnRemoveUser = (ImageButton) row.findViewById(R.id.share_page_listitem_btn_remove);
        btnRemoveUser.setBackgroundResource(17301533);
        btnAddUser.setOnClickListener(new AddButtonOnClickListener(this.mActivity));
        btnRemoveUser.setOnClickListener(new RemoveButtonOnClickListener(this.mActivity, item));
        if (position == 0) {
            btnAddUser.setVisibility(0);
            btnRemoveUser.setVisibility(4);
        } else {
            btnAddUser.setVisibility(4);
            btnRemoveUser.setVisibility(0);
        }
        return row;
    }

    public class RemoveButtonOnClickListener implements View.OnClickListener {
        private Activity mActivity;
        private SharePageItem mItem;

        public RemoveButtonOnClickListener(Activity activity, SharePageItem item) {
            this.mActivity = activity;
            this.mItem = item;
        }

        public void onClick(View v) {
            if (SharePageAdapter.this.mIsShareImage) {
                ((ShareImageActivity) this.mActivity).onRemoveTargetUser(this.mItem);
            } else {
                ((SharePageActivity) this.mActivity).onRemoveTargetUser(this.mItem);
            }
        }
    }

    public class AddButtonOnClickListener implements View.OnClickListener {
        private Activity mActivity;

        public AddButtonOnClickListener(Activity activity) {
            this.mActivity = activity;
        }

        /* Debug info: failed to restart local var, previous not found, register: 1 */
        public void onClick(View v) {
            if (SharePageAdapter.this.mIsShareImage) {
                ((ShareImageActivity) this.mActivity).onAddTargetUser();
            } else {
                ((SharePageActivity) this.mActivity).onAddTargetUser();
            }
        }
    }

    public class UserInfoTextWatcher implements TextWatcher {
        private Activity mActivity;
        private SharePageItem mItem;

        public UserInfoTextWatcher(Activity activity, SharePageItem item) {
            this.mActivity = activity;
            this.mItem = item;
        }

        public void afterTextChanged(Editable s) {
            this.mItem.mUserInfo = s.toString();
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    }

    public class UserInfoFocusChangeListener implements View.OnFocusChangeListener {
        private Activity mActivity;
        private SharePageItem mItem;

        public UserInfoFocusChangeListener(Activity activity, SharePageItem item) {
            this.mActivity = activity;
            this.mItem = item;
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                this.mItem.mUserInfo = ((EditText) v).getText().toString();
            }
        }
    }
}
