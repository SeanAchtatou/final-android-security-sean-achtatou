package com.cplatform.android.cmsurfclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CacheManager;
import android.webkit.URLUtil;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.ad.AdItem;
import com.cplatform.android.cmsurfclient.ad.AdLinkButton;
import com.cplatform.android.cmsurfclient.bookmark_history.BookmarkHistoryTabActivity;
import com.cplatform.android.cmsurfclient.cache.CacheDB;
import com.cplatform.android.cmsurfclient.menu.BrowserOptionsMenu;
import com.cplatform.android.cmsurfclient.naviedit.NaviEditSearchActivity;
import com.cplatform.android.cmsurfclient.naviedit.NaviEditUrlActivity;
import com.cplatform.android.cmsurfclient.navigator.NavigatorTabHost;
import com.cplatform.android.cmsurfclient.quicklink.QuickLinkAdapter;
import com.cplatform.android.cmsurfclient.quicklink.QuickLinkHelper;
import com.cplatform.android.cmsurfclient.quicklink.QuickLinkItem;
import com.cplatform.android.cmsurfclient.service.entry.Group;
import com.cplatform.android.cmsurfclient.service.entry.Item;
import com.cplatform.android.cmsurfclient.service.entry.Label;
import com.cplatform.android.cmsurfclient.service.entry.Msb;
import com.cplatform.android.cmsurfclient.service.entry.Tab;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import com.cplatform.android.cmsurfclient.wml2html.Wml2Html;
import com.cplatform.android.ctrl.ScrollNewsLinearLayout;
import com.cplatform.android.ctrl.scrollOpenUrlInterface;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class HomeView extends LinearLayout implements scrollOpenUrlInterface {
    private static final String LOG_TAG = "HomeView";
    /* access modifiers changed from: private */
    public static int WIDTH_BETWEEN = 5;
    /* access modifiers changed from: private */
    public static int WIDTH_TOP_BOTTOM = 10;
    public static QuickLinkAdapter mQuickLinkAdapter = null;
    public static QuickLinkHelper mQuickLinkHelper = null;
    private ArrayList<AdItem> adList;
    private View btnNaviSearch;
    private View btnNaviUrl;
    /* access modifiers changed from: private */
    public List<List<List<Item>>> childArray;
    /* access modifiers changed from: private */
    public List<String> groupArray;
    /* access modifiers changed from: private */
    public Handler hander;
    private ImageButton mButtonBack;
    private ImageButton mButtonForward;
    /* access modifiers changed from: private */
    public ImageButton mButtonHome;
    private ImageButton mButtonMenu;
    private ImageButton mButtonWindow;
    /* access modifiers changed from: private */
    public int mCaptureIdx;
    /* access modifiers changed from: private */
    public int mDefaultTab;
    private ExpandableListView mExpandableListView;
    private final LayoutInflater mFactory;
    /* access modifiers changed from: private */
    public String mFailUrl;
    private GridView mGridViewNavi;
    /* access modifiers changed from: private */
    public boolean mIsBrowseInCache;
    /* access modifiers changed from: private */
    public HashMap<String, String> mMapUrl;
    /* access modifiers changed from: private */
    public String mOriginalUserAgent;
    /* access modifiers changed from: private */
    public ProgressBar mPageLoadProgress;
    /* access modifiers changed from: private */
    public RelativeLayout mProgressLayer;
    /* access modifiers changed from: private */
    public QuickLinkItem mQuickLinkItem;
    /* access modifiers changed from: private */
    public Queue<Integer> mSnapshotList;
    /* access modifiers changed from: private */
    public SurfManagerActivity mSurfMgr;
    /* access modifiers changed from: private */
    public NavigatorTabHost mTabHost;
    /* access modifiers changed from: private */
    public SurfWebView mWebView;
    /* access modifiers changed from: private */
    public SurfWebView mWebViewCapture;
    ScrollNewsLinearLayout scrollNewsLayout;
    RelativeLayout scrollnews_hot;

    public HomeView(SurfManagerActivity context) {
        this(context, -1);
    }

    public HomeView(SurfManagerActivity context, int tabID) {
        super(context);
        this.mQuickLinkItem = null;
        this.mCaptureIdx = -1;
        this.mDefaultTab = 0;
        this.mSurfMgr = null;
        this.mIsBrowseInCache = false;
        this.mOriginalUserAgent = WindowAdapter2.BLANK_URL;
        this.scrollNewsLayout = null;
        this.scrollnews_hot = null;
        this.hander = new Handler();
        this.mSurfMgr = context;
        this.mFactory = LayoutInflater.from(context);
        this.mFactory.inflate((int) R.layout.main, this);
        this.mProgressLayer = (RelativeLayout) findViewById(R.id.browser_progress_layer);
        this.mProgressLayer.setVisibility(8);
        initScrollNews();
        initYellowPage();
        initNavigator();
        initMainTabs();
        initToolbar();
        initWebView();
        initNaviBar();
        if (tabID != -1) {
            this.mTabHost.setCurrentTab(tabID);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.mSurfMgr == null) {
                return true;
            }
            this.mSurfMgr.showQuitDialog();
            return true;
        } else if (keyCode == 82) {
            Bundle bundle = new Bundle();
            if (this.mTabHost.getCurrentTab() == this.mDefaultTab) {
                bundle.putString("menu_type", "menu_navi");
            } else {
                bundle.putString("menu_type", "menu_channel");
            }
            bundle.putBoolean("full_screen", this.mSurfMgr.mIsFullScreen);
            this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, BrowserOptionsMenu.class).putExtras(bundle), 4);
            return true;
        } else if (keyCode == 25) {
            if (this.mWebView == null) {
                return true;
            }
            this.mWebView.pageDown(false);
            return true;
        } else if (keyCode != 24 || this.mWebView == null) {
            return true;
        } else {
            this.mWebView.pageUp(false);
            return true;
        }
    }

    public void onResume(int tabID) {
        Log.v("homeview onResume test############", "enter onResume tabID = " + tabID);
        if (tabID != -1) {
            Log.v("homeview onResume test############", "enter onResume mDefaultTab = " + this.mDefaultTab);
            this.mTabHost.setCurrentTab(tabID);
            if (tabID == this.mDefaultTab) {
                restartScrollMessage();
            } else {
                removeScrollMessage();
            }
        } else {
            this.mTabHost.setCurrentTab(this.mDefaultTab);
            restartScrollMessage();
        }
        if (mQuickLinkHelper != null) {
            mQuickLinkHelper.load();
            if (mQuickLinkAdapter != null) {
                mQuickLinkAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onRefresh() {
        String url = this.mMapUrl.get(this.mTabHost.getCurrentTabTag());
        if (url != null && url.length() > 0) {
            this.mPageLoadProgress.setProgress(0);
            this.mPageLoadProgress.setVisibility(0);
            this.mWebView.clearView();
            this.mWebView.loadUrl(url);
        }
    }

    public void onReloadData() {
        Log.e(LOG_TAG, "HomeView:onReloadData...");
        initScrollNews();
        initYellowPage();
    }

    /* access modifiers changed from: package-private */
    public void setFullScreen(boolean isFullScreen) {
        this.mSurfMgr.mIsFullScreen = isFullScreen;
        if (isFullScreen) {
            WindowManager.LayoutParams attrs = this.mSurfMgr.getWindow().getAttributes();
            attrs.flags |= 1024;
            this.mSurfMgr.getWindow().setAttributes(attrs);
            this.mSurfMgr.getWindow().addFlags(512);
            return;
        }
        WindowManager.LayoutParams attrs2 = this.mSurfMgr.getWindow().getAttributes();
        attrs2.flags &= -1025;
        this.mSurfMgr.getWindow().setAttributes(attrs2);
        this.mSurfMgr.getWindow().clearFlags(512);
    }

    public void onOptionMenu(String act) {
        if (act.equalsIgnoreCase("browser_options_refresh")) {
            onRefresh();
        } else if (act.equalsIgnoreCase("browser_options_addbookmark")) {
        } else {
            if (act.equalsIgnoreCase("browser_options_bookmark")) {
                if (this.mSurfMgr != null) {
                    this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, BookmarkHistoryTabActivity.class), 5);
                }
            } else if (act.equalsIgnoreCase("browser_options_share")) {
            } else {
                if (act.equalsIgnoreCase("browser_options_search")) {
                    if (this.mSurfMgr != null) {
                        Bundle b = new Bundle();
                        b.putBoolean("show_url", false);
                        b.putInt("padding_top", 0);
                        this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, NaviEditSearchActivity.class).putExtras(b), 1);
                    }
                } else if (act.equalsIgnoreCase("browser_options_settings")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.settings();
                    }
                } else if (act.equalsIgnoreCase("browser_options_update")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.checkUpgrade();
                    }
                } else if (act.equalsIgnoreCase("browser_options_download")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.download();
                    }
                } else if (act.equalsIgnoreCase("browser_options_quit")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.showQuitDialog();
                    }
                } else if (act.equalsIgnoreCase("browser_options_fullscreen")) {
                    setFullScreen(!this.mSurfMgr.mIsFullScreen);
                } else if (act.equalsIgnoreCase("browser_options_nightmode")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.showBrightnessAdjust();
                    }
                } else if (act.equalsIgnoreCase("browser_options_flow")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.browseInCurrentWindow("http://go.10086.cn/msb_server/tools.do?query=gprs", -1);
                    }
                } else if (act.equalsIgnoreCase("browser_options_lockscreen")) {
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.showLockScreen();
                    }
                } else if (act.equalsIgnoreCase("browser_options_help") && this.mSurfMgr != null) {
                    this.mSurfMgr.browseInCurrentWindow("file:///android_asset/html/help.html", -1);
                }
            }
        }
    }

    public void onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case SurfManagerActivity.CONTEXTMENU_QUICKLINK_EDIT /*11*/:
                if (this.mQuickLinkItem.mFlag > 0) {
                    final View dialogview = LayoutInflater.from(this.mSurfMgr).inflate((int) R.layout.quicklink_add, (ViewGroup) null);
                    EditText txtTitle = (EditText) dialogview.findViewById(R.id.quicklink_add_title);
                    if (txtTitle != null) {
                        txtTitle.setText(this.mQuickLinkItem.mTitle);
                    }
                    EditText txtUrl = (EditText) dialogview.findViewById(R.id.quicklink_add_url);
                    if (txtUrl != null) {
                        txtUrl.setText(this.mQuickLinkItem.mUrl);
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(this.mSurfMgr);
                    builder.setTitle((int) R.string.quicklink_edit_title);
                    builder.setView(dialogview);
                    builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String title = ((EditText) dialogview.findViewById(R.id.quicklink_add_title)).getText().toString();
                            String url = ((EditText) dialogview.findViewById(R.id.quicklink_add_url)).getText().toString();
                            if (TextUtils.isEmpty(title)) {
                                Toast.makeText(HomeView.this.mSurfMgr, (int) R.string.quicklink_prompt_title, 0).show();
                            } else if (TextUtils.isEmpty(url) || !URLUtil.isValidUrl(URLUtil.guessUrl(url))) {
                                Toast.makeText(HomeView.this.mSurfMgr, (int) R.string.quicklink_prompt_url, 0).show();
                            } else {
                                HomeView.mQuickLinkHelper.update(HomeView.this.mQuickLinkItem.mIdx, 2, title, URLUtil.guessUrl(url), WindowAdapter2.BLANK_URL);
                                HomeView.mQuickLinkAdapter.notifyDataSetChanged();
                                if (HomeView.this.mCaptureIdx == -1) {
                                    int unused = HomeView.this.mCaptureIdx = HomeView.this.mQuickLinkItem.mIdx;
                                    HomeView.this.mWebViewCapture.loadUrl(URLUtil.guessUrl(url));
                                    return;
                                }
                                HomeView.this.mSnapshotList.offer(Integer.valueOf(HomeView.this.mQuickLinkItem.mIdx));
                            }
                        }
                    });
                    builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                    return;
                }
                return;
            case SurfManagerActivity.CONTEXTMENU_QUICKLINK_DELETE /*12*/:
                if (this.mQuickLinkItem.mFlag > 0) {
                    mQuickLinkHelper.update(this.mQuickLinkItem.mIdx, 0, WindowAdapter2.BLANK_URL, WindowAdapter2.BLANK_URL, WindowAdapter2.BLANK_URL);
                    mQuickLinkAdapter.notifyDataSetChanged();
                    return;
                }
                return;
            case SurfManagerActivity.CONTEXTMENU_QUICKLINK_OPENNEW /*13*/:
                if (this.mSurfMgr != null && this.mQuickLinkItem != null) {
                    this.mSurfMgr.setCurrentSnapshot(this);
                    this.mSurfMgr.browseInNewWindow(this.mQuickLinkItem.mUrl, this.mTabHost.getCurrentTab());
                    return;
                }
                return;
            default:
                return;
        }
    }

    private boolean initScrollNews() {
        Msb msb;
        Log.e(LOG_TAG, "initScrollNews...");
        if (!(this.mSurfMgr == null || (msb = SurfManagerActivity.mMsb) == null || msb.snews == null || msb.snews.items == null || msb.snews.items.size() <= 0)) {
            ArrayList<Item> newsItems = new ArrayList<>();
            ArrayList<Item> items = msb.snews.items;
            final String hoturl = msb.snews.url;
            Log.v(LOG_TAG, "hoturl = " + hoturl);
            Iterator i$ = items.iterator();
            while (i$.hasNext()) {
                Item item = i$.next();
                if (item != null && !TextUtils.isEmpty(item.name) && !TextUtils.isEmpty(item.url)) {
                    newsItems.add(item);
                }
            }
            this.scrollnews_hot = (RelativeLayout) findViewById(R.id.scrollnewlinearlayout);
            this.scrollNewsLayout = (ScrollNewsLinearLayout) findViewById(R.id.scrollnews_layout);
            ImageView hotimageview = (ImageView) findViewById(R.id.hot);
            if (this.scrollNewsLayout == null) {
                return false;
            }
            this.scrollnews_hot.setVisibility(0);
            this.scrollNewsLayout.setScrollNews(this, newsItems);
            hotimageview.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (HomeView.this.scrollNewsLayout != null && hoturl != null) {
                        String url = hoturl.trim();
                        if (!WindowAdapter2.BLANK_URL.equals(url) && HomeView.this.mSurfMgr != null) {
                            HomeView.this.mSurfMgr.browseInCurrentWindow(URLUtil.guessUrl(url), -1);
                        }
                    }
                }

                private void setImageResource(int iconHotClick) {
                }
            });
        }
        return true;
    }

    private boolean initYellowPage() {
        Msb msb;
        Log.e(LOG_TAG, "initYellowPage...");
        this.groupArray = new ArrayList();
        this.childArray = new ArrayList();
        this.adList = new ArrayList<>();
        if (!(this.mSurfMgr == null || (msb = SurfManagerActivity.mMsb) == null)) {
            for (int i = 0; i < msb.tabs.tabs.size(); i++) {
                Tab tab = msb.tabs.tabs.get(i);
                if (tab.act.equalsIgnoreCase("NAV")) {
                    for (int j = 0; j < tab.labels.size(); j++) {
                        Label label = tab.labels.get(j);
                        if (label.layout.equalsIgnoreCase("LIST")) {
                            if (label.groups != null) {
                                this.groupArray.add(label.name);
                                ArrayList arrayList = new ArrayList();
                                for (int k = 0; k < label.groups.size(); k++) {
                                    Group group = label.groups.get(k);
                                    ArrayList<Item> items = new ArrayList<>();
                                    if (!TextUtils.isEmpty(group.name) && !TextUtils.isEmpty(group.url)) {
                                        Item item = new Item(group);
                                        item.act = "group";
                                        items.add(item);
                                    }
                                    if (group.items != null) {
                                        for (int l = 0; l < group.items.size(); l++) {
                                            items.add(group.items.get(l));
                                        }
                                    }
                                    int idx = 0;
                                    int column = 0;
                                    ArrayList arrayList2 = new ArrayList();
                                    while (idx < items.size()) {
                                        arrayList2.add(items.get(idx));
                                        idx++;
                                        column++;
                                        if (column == 3) {
                                            column = 0;
                                            arrayList.add(arrayList2);
                                            arrayList2 = new ArrayList();
                                        }
                                    }
                                    if (arrayList2.size() > 0 && arrayList2.size() < 3) {
                                        arrayList.add(arrayList2);
                                    }
                                }
                                this.childArray.add(arrayList);
                            }
                        } else if (label.layout.equalsIgnoreCase("AD") && label.groups != null) {
                            for (int k2 = 0; k2 < label.groups.size(); k2++) {
                                Group group2 = label.groups.get(k2);
                                if (group2.items != null) {
                                    for (int l2 = 0; l2 < group2.items.size(); l2++) {
                                        Item item2 = group2.items.get(l2);
                                        if (item2 != null) {
                                            this.adList.add(new AdItem(item2.name, item2.url));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        this.mExpandableListView = (ExpandableListView) findViewById(R.id.listviewYellowPage);
        for (int i2 = 0; i2 < this.adList.size(); i2++) {
            LinearLayout adLink = (LinearLayout) this.mSurfMgr.getLayoutInflater().inflate((int) R.layout.adlink_list_item, (ViewGroup) null);
            AdLinkButton lnkBtn = (AdLinkButton) adLink.findViewById(R.id.adlink_item);
            lnkBtn.setUrl(this.adList.get(i2).url);
            lnkBtn.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
            lnkBtn.setText(this.adList.get(i2).title);
            lnkBtn.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Button adLinkItem = (Button) v;
                    if (event.getAction() == 0) {
                        adLinkItem.setTextColor(-1);
                        return false;
                    } else if (event.getAction() == 1) {
                        adLinkItem.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                        return false;
                    } else {
                        adLinkItem.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                        return false;
                    }
                }
            });
            lnkBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HomeView.this.mSurfMgr.browseInCurrentWindow(((AdLinkButton) v).getUrl(), -1);
                }
            });
            this.mExpandableListView.addFooterView(adLink);
        }
        this.mExpandableListView.setAdapter(new ExpandableAdapter(this.mSurfMgr));
        this.mExpandableListView.setGroupIndicator(null);
        this.mExpandableListView.setChildIndicator(null);
        this.mExpandableListView.setSelector((int) R.color.selector_white);
        this.mExpandableListView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.e(HomeView.LOG_TAG, "ExpandableListView:onKey(" + keyCode + ")");
                if (keyCode == 4 || keyCode == 82) {
                    return false;
                }
                return true;
            }
        });
        return true;
    }

    public class YellowPageItem extends LinearLayout {
        /* access modifiers changed from: private */
        public SurfManagerActivity mActivity;
        /* access modifiers changed from: private */
        public List<Item> mItemArray;
        private int mNavibarIdx;
        /* access modifiers changed from: private */
        public int mToolbarIdx;

        public YellowPageItem(Context context) {
            super(context);
        }

        public YellowPageItem(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public YellowPageItem(Context context, SurfManagerActivity activity, ArrayList<Item> itemArray, int toolbarIdx, int navibarIdx, int groupPosition, int childPosition, int childrenCount) {
            super(context);
            this.mActivity = activity;
            this.mToolbarIdx = toolbarIdx;
            this.mNavibarIdx = navibarIdx;
            this.mItemArray = itemArray;
            inflate(context, R.layout.browser_yellowpage_item, this);
            Button btnSite = null;
            setPadding(0, 0, 0, 0);
            if (childPosition == 0) {
                setPadding(0, HomeView.WIDTH_TOP_BOTTOM, 0, HomeView.WIDTH_BETWEEN);
            } else if (childPosition == childrenCount - 1) {
                setPadding(0, 0, 0, HomeView.WIDTH_TOP_BOTTOM);
            } else {
                setPadding(0, 0, 0, HomeView.WIDTH_BETWEEN);
            }
            int i2 = itemArray.size();
            for (int l1 = 0; l1 < i2; l1++) {
                Item item = itemArray.get(l1);
                switch (l1) {
                    case 0:
                        btnSite = (Button) findViewById(R.id.first_site);
                        if (btnSite != null) {
                            final HomeView homeView = HomeView.this;
                            btnSite.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    view.setPressed(false);
                                    YellowPageItem.this.mActivity.browseInCurrentWindow(((Item) YellowPageItem.this.mItemArray.get(0)).url, YellowPageItem.this.mToolbarIdx);
                                }
                            });
                            break;
                        }
                        break;
                    case 1:
                        btnSite = (Button) findViewById(R.id.second_site);
                        if (btnSite != null) {
                            final HomeView homeView2 = HomeView.this;
                            btnSite.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    view.setPressed(false);
                                    YellowPageItem.this.mActivity.browseInCurrentWindow(((Item) YellowPageItem.this.mItemArray.get(1)).url, YellowPageItem.this.mToolbarIdx);
                                }
                            });
                            break;
                        }
                        break;
                    case 2:
                        btnSite = (Button) findViewById(R.id.third_site);
                        if (btnSite != null) {
                            final HomeView homeView22 = HomeView.this;
                            btnSite.setOnClickListener(new View.OnClickListener() {
                                public void onClick(View view) {
                                    view.setPressed(false);
                                    YellowPageItem.this.mActivity.browseInCurrentWindow(((Item) YellowPageItem.this.mItemArray.get(2)).url, YellowPageItem.this.mToolbarIdx);
                                }
                            });
                            break;
                        }
                        break;
                }
                if (btnSite != null) {
                    btnSite.setVisibility(0);
                    if (item.act.equalsIgnoreCase("group")) {
                        btnSite.setBackgroundResource(R.drawable.navi_group_bg);
                    } else {
                        btnSite.setBackgroundResource(R.drawable.navi_site_bg);
                    }
                    btnSite.setText(item.name);
                    btnSite.setEllipsize(TextUtils.TruncateAt.END);
                    if (!TextUtils.isEmpty(item.color)) {
                        btnSite.setTextColor(-16777216 + Integer.parseInt(item.color, 16));
                    } else {
                        btnSite.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                    }
                }
            }
        }
    }

    public class ExpandableAdapter extends BaseExpandableListAdapter {
        Activity activity;

        public ExpandableAdapter(Activity a) {
            this.activity = a;
        }

        public Object getChild(int groupPosition, int childPosition) {
            return ((List) HomeView.this.childArray.get(groupPosition)).get(childPosition);
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            return ((List) HomeView.this.childArray.get(groupPosition)).size();
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            return new YellowPageItem(HomeView.this.mSurfMgr, HomeView.this.mSurfMgr, (ArrayList) ((List) HomeView.this.childArray.get(groupPosition)).get(childPosition), HomeView.this.mTabHost.getCurrentTab(), groupPosition, groupPosition, childPosition, getChildrenCount(groupPosition));
        }

        public Object getGroup(int groupPosition) {
            return HomeView.this.groupArray.get(groupPosition);
        }

        public int getGroupCount() {
            return HomeView.this.groupArray.size();
        }

        public long getGroupId(int groupPosition) {
            return (long) groupPosition;
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String string = (String) HomeView.this.groupArray.get(groupPosition);
            View convertView2 = LayoutInflater.from(this.activity).inflate((int) R.layout.browser_yellowpage_header, (ViewGroup) null);
            if (isExpanded) {
                ((ImageView) convertView2.findViewById(R.id.expand_icon)).setImageResource(R.drawable.daohang_header_icon_shouqi);
            } else {
                ((ImageView) convertView2.findViewById(R.id.expand_icon)).setImageResource(R.drawable.daohang_header_icon_zhankai);
            }
            ((TextView) convertView2.findViewById(R.id.yellowpage_header)).setText(string);
            return convertView2;
        }

        public boolean hasStableIds() {
            return false;
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    private boolean initNavigator() {
        mQuickLinkHelper = new QuickLinkHelper(this.mSurfMgr);
        mQuickLinkHelper.load();
        mQuickLinkAdapter = new QuickLinkAdapter(this.mSurfMgr, mQuickLinkHelper.mItems);
        this.mGridViewNavi = (GridView) findViewById(R.id.quicklink_grid);
        this.mGridViewNavi.setAdapter((ListAdapter) mQuickLinkAdapter);
        this.mSurfMgr.registerForContextMenu(this.mGridViewNavi);
        this.mGridViewNavi.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                if (HomeView.this.mQuickLinkItem.mFlag > 0) {
                    menu.add(0, 13, 0, "新窗口打开");
                    menu.add(0, 11, 0, "编辑");
                    menu.add(0, 12, 0, "删除");
                }
            }
        });
        this.mGridViewNavi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                QuickLinkItem unused = HomeView.this.mQuickLinkItem = (QuickLinkItem) ((GridView) parent).getItemAtPosition(position);
                return false;
            }
        });
        this.mGridViewNavi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                QuickLinkItem unused = HomeView.this.mQuickLinkItem = (QuickLinkItem) ((GridView) parent).getItemAtPosition(position);
                if (HomeView.this.mQuickLinkItem.mFlag <= 0) {
                    final View dialogview = LayoutInflater.from(HomeView.this.mSurfMgr).inflate((int) R.layout.quicklink_add, (ViewGroup) null);
                    EditText txtUrl = (EditText) dialogview.findViewById(R.id.quicklink_add_url);
                    if (txtUrl != null) {
                        txtUrl.setText("http://");
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(HomeView.this.mSurfMgr);
                    builder.setTitle((int) R.string.quicklink_add_title);
                    builder.setView(dialogview);
                    builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            String title = ((EditText) dialogview.findViewById(R.id.quicklink_add_title)).getText().toString();
                            String url = ((EditText) dialogview.findViewById(R.id.quicklink_add_url)).getText().toString();
                            if (title.length() <= 0) {
                                Toast.makeText(HomeView.this.mSurfMgr, (int) R.string.quicklink_prompt_title, 0).show();
                            } else if (url.length() <= 0 || !URLUtil.isValidUrl(URLUtil.guessUrl(url))) {
                                Toast.makeText(HomeView.this.mSurfMgr, (int) R.string.quicklink_prompt_url, 0).show();
                            } else {
                                HomeView.mQuickLinkHelper.update(HomeView.this.mQuickLinkItem.mIdx, 2, title, URLUtil.guessUrl(url), WindowAdapter2.BLANK_URL);
                                HomeView.mQuickLinkAdapter.notifyDataSetChanged();
                                if (HomeView.this.mCaptureIdx == -1) {
                                    int unused = HomeView.this.mCaptureIdx = HomeView.this.mQuickLinkItem.mIdx;
                                    HomeView.this.mWebViewCapture.loadUrl(URLUtil.guessUrl(url));
                                    return;
                                }
                                HomeView.this.mSnapshotList.offer(Integer.valueOf(HomeView.this.mQuickLinkItem.mIdx));
                            }
                        }
                    });
                    builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                } else if (HomeView.this.mSurfMgr != null) {
                    HomeView.this.mSurfMgr.browseInCurrentWindow(HomeView.this.mQuickLinkItem.mUrl, HomeView.this.mTabHost.getCurrentTab());
                }
            }
        });
        this.mWebViewCapture = (SurfWebView) findViewById(R.id.webview_capture);
        this.mWebViewCapture.getSettings().setJavaScriptEnabled(true);
        this.mWebViewCapture.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(HomeView.LOG_TAG, "mWebViewCapture.shouldOverrideUrlLoading:" + url);
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                int width;
                Log.i(HomeView.LOG_TAG, "mWebViewCapture.onPageFinished:" + url);
                if (HomeView.this.mCaptureIdx != -1) {
                    Picture pic = view.capturePicture();
                    Rect r = new Rect();
                    view.getLocalVisibleRect(r);
                    int w = r.width();
                    int h = r.height();
                    if (w > h) {
                        width = h;
                    } else {
                        width = w;
                    }
                    float density = HomeView.this.mSurfMgr.getResources().getDisplayMetrics().density;
                    float scale = (72.0f * density) / ((float) width);
                    Bitmap bmp = Bitmap.createBitmap((int) (72.0f * density), (int) (72.0f * density), Bitmap.Config.RGB_565);
                    Canvas canvas = new Canvas(bmp);
                    canvas.scale(2.0f * scale, 2.0f * scale);
                    pic.draw(canvas);
                    if (!TextUtils.isEmpty(view.getUrl())) {
                        String filename = HomeView.this.md5(view.getUrl()) + ".jpg";
                        try {
                            OutputStream os = HomeView.this.mSurfMgr.openFileOutput(filename, 0);
                            if (bmp.compress(Bitmap.CompressFormat.PNG, 70, os)) {
                                os.flush();
                                os.close();
                            }
                            os.close();
                        } catch (FileNotFoundException | IOException e) {
                        }
                        HomeView.mQuickLinkHelper.onSnapshotCaptured(HomeView.this.mCaptureIdx, filename);
                        HomeView.mQuickLinkAdapter.notifyDataSetChanged();
                    }
                    if (HomeView.this.mSnapshotList.size() > 0) {
                        int nextIdx = ((Integer) HomeView.this.mSnapshotList.poll()).intValue();
                        int unused = HomeView.this.mCaptureIdx = nextIdx;
                        view.loadUrl(HomeView.mQuickLinkHelper.mItems.get(nextIdx).mUrl);
                    } else {
                        int unused2 = HomeView.this.mCaptureIdx = -1;
                    }
                }
                super.onPageFinished(view, url);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.i(HomeView.LOG_TAG, "mWebViewCapture.onPageStarted:" + url);
                super.onPageStarted(view, url, favicon);
            }

            /* Debug info: failed to restart local var, previous not found, register: 4 */
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.i(HomeView.LOG_TAG, "mWebViewCapture.onReceivedError:" + description);
                if (HomeView.this.mSnapshotList.size() > 0) {
                    int nextIdx = ((Integer) HomeView.this.mSnapshotList.poll()).intValue();
                    int unused = HomeView.this.mCaptureIdx = nextIdx;
                    view.loadUrl(HomeView.mQuickLinkHelper.mItems.get(nextIdx).mUrl);
                    return;
                }
                int unused2 = HomeView.this.mCaptureIdx = -1;
            }
        });
        String model = Build.MODEL;
        if (!TextUtils.isEmpty(model) && model.equalsIgnoreCase(SurfManagerActivity.MODEL_DOPOD_A8188)) {
            Log.e(LOG_TAG, "dopod A8188 found, try load an empty page...");
            this.mWebViewCapture.loadUrl("http://go.10086.cn/msb_server/a8188.do");
        }
        return true;
    }

    private boolean initMainTabs() {
        Bitmap bmpTabActive;
        Bitmap bmpTabInactive;
        this.mTabHost = (NavigatorTabHost) findViewById(R.id.tabhost_main);
        this.mTabHost.setup();
        this.mMapUrl = new HashMap<>();
        Msb msb = null;
        if (this.mSurfMgr != null) {
            SurfManagerActivity surfManagerActivity = this.mSurfMgr;
            msb = SurfManagerActivity.mMsb;
        }
        if (msb != null) {
            for (int i = 0; i < msb.tabs.tabs.size(); i++) {
                Tab tab = msb.tabs.tabs.get(i);
                if (tab.isDefault) {
                    this.mDefaultTab = i;
                }
                String fileTabActive = CacheDB.getInstance(this.mSurfMgr).getFile(tab.imgDown);
                String fileTabInactive = CacheDB.getInstance(this.mSurfMgr).getFile(tab.imgUp);
                if (msb.isAssets) {
                    bmpTabActive = getImageFromAssetFile(this.mSurfMgr, fileTabActive, 240);
                    bmpTabInactive = getImageFromAssetFile(this.mSurfMgr, fileTabInactive, 240);
                } else {
                    bmpTabActive = getImageFromDataFile(this.mSurfMgr, fileTabActive, 240);
                    bmpTabInactive = getImageFromDataFile(this.mSurfMgr, fileTabInactive, 240);
                }
                if (tab.act.equalsIgnoreCase("NAV")) {
                    this.mTabHost.addTab(this.mTabHost.newTabSpec(Integer.toString(tab.hashCode()), tab.name, bmpTabActive, bmpTabInactive).setContent((int) R.id.home_navigator));
                } else if (tab.act.equalsIgnoreCase("WEB")) {
                    this.mTabHost.addTab(this.mTabHost.newTabSpec(Integer.toString(tab.hashCode()), tab.name, bmpTabActive, bmpTabInactive).setContent((int) R.id.home_browser));
                    this.mMapUrl.put(Integer.toString(tab.hashCode()), tab.url);
                } else if (tab.act.equalsIgnoreCase("FAV")) {
                }
            }
        }
        this.mTabHost.setCurrentTab(this.mDefaultTab);
        this.mTabHost.setOnTabChangedListener(new NavigatorTabHost.OnTabChangeListener() {
            public void onTabChanged(String tabId) {
                String userAgent;
                HomeView.this.mProgressLayer.setVisibility(8);
                if (HomeView.this.mSurfMgr != null) {
                    HomeView.this.mSurfMgr.onHomeTabChanged(HomeView.this.mTabHost.getCurrentTab());
                    HomeView.this.mButtonHome.setEnabled((HomeView.this.mSurfMgr.mLastWindowItem == null || HomeView.this.mSurfMgr.mLastWindowItem.tab == -1 || HomeView.this.mSurfMgr.mLastWindowItem.tab == HomeView.this.mDefaultTab) ? false : true);
                }
                String url = (String) HomeView.this.mMapUrl.get(tabId);
                Log.e(HomeView.LOG_TAG, "TabHost.onTabChanged - tabId:" + tabId + ", url:" + url);
                WebSettings webSettings = HomeView.this.mWebView.getSettings();
                String userAgent2 = HomeView.this.mSurfMgr.mUserAgent;
                if (HomeView.this.mOriginalUserAgent.length() > 0) {
                    userAgent = userAgent2 + HomeView.this.mOriginalUserAgent;
                } else {
                    userAgent = userAgent2 + webSettings.getUserAgentString();
                }
                webSettings.setUserAgentString(userAgent);
                Log.e(HomeView.LOG_TAG, "Set UserAgent to " + userAgent);
                if (url == null || url.length() <= 0) {
                    HomeView.this.restartScrollMessage();
                    return;
                }
                HomeView.this.removeScrollMessage();
                WebBackForwardList historyList = HomeView.this.mWebView.copyBackForwardList();
                if (historyList.getSize() > 0) {
                    int curIdx = historyList.getCurrentIndex();
                    int dstIdx = -1;
                    int i = 0;
                    while (true) {
                        if (i >= historyList.getSize()) {
                            break;
                        }
                        String historyUrl = historyList.getItemAtIndex(i).getUrl();
                        Log.d(HomeView.LOG_TAG, "TabHost.onTabChanged - WebView history item url:" + historyUrl);
                        if (historyUrl.equalsIgnoreCase(url)) {
                            dstIdx = i;
                            break;
                        }
                        i++;
                    }
                    if (dstIdx == -1) {
                        boolean unused = HomeView.this.mIsBrowseInCache = false;
                        HomeView.this.mWebView.clearView();
                        HomeView.this.mWebView.loadUrl(url);
                        return;
                    }
                    boolean unused2 = HomeView.this.mIsBrowseInCache = true;
                    HomeView.this.mWebView.goBackOrForward(dstIdx - curIdx);
                    return;
                }
                boolean unused3 = HomeView.this.mIsBrowseInCache = false;
                HomeView.this.mWebView.clearView();
                HomeView.this.mWebView.loadUrl(url);
            }
        });
        return true;
    }

    private boolean initToolbar() {
        this.mButtonHome = (ImageButton) findViewById(R.id.ButtonHome);
        this.mButtonBack = (ImageButton) findViewById(R.id.ButtonBackward);
        this.mButtonForward = (ImageButton) findViewById(R.id.ButtonForward);
        this.mButtonWindow = (ImageButton) findViewById(R.id.ButtonWindows);
        this.mButtonMenu = (ImageButton) findViewById(R.id.ButtonMenu);
        this.mButtonHome.setEnabled(false);
        this.mButtonBack.setEnabled(false);
        this.mButtonForward.setEnabled(false);
        this.mButtonHome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HomeView.this.mSurfMgr != null) {
                    if (HomeView.this.mSurfMgr.mLastWindowItem != null) {
                        HomeView.this.mSurfMgr.mLastWindowItem.tab = HomeView.this.mDefaultTab;
                    }
                    HomeView.this.mSurfMgr.showCurrentWindowItem();
                }
            }
        });
        this.mButtonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HomeView.this.mSurfMgr != null) {
                    HomeView.this.mSurfMgr.onHomeClickBack();
                }
            }
        });
        this.mButtonForward.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HomeView.this.mSurfMgr != null) {
                    HomeView.this.mSurfMgr.onHomeClickForward();
                }
            }
        });
        this.mButtonWindow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HomeView.this.mSurfMgr != null) {
                    HomeView.this.mSurfMgr.setCurrentSnapshot(HomeView.this);
                    HomeView.this.mSurfMgr.showWindowsManager();
                }
            }
        });
        this.mButtonMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (HomeView.this.mSurfMgr != null) {
                    Bundle bundle = new Bundle();
                    if (HomeView.this.mTabHost.getCurrentTab() == HomeView.this.mDefaultTab) {
                        bundle.putString("menu_type", "menu_navi");
                    } else {
                        bundle.putString("menu_type", "menu_channel");
                    }
                    bundle.putBoolean("full_screen", HomeView.this.mSurfMgr.mIsFullScreen);
                    HomeView.this.mSurfMgr.startActivityForResult(new Intent(HomeView.this.mSurfMgr, BrowserOptionsMenu.class).putExtras(bundle), 4);
                }
            }
        });
        return true;
    }

    private boolean initWebView() {
        this.mSnapshotList = new LinkedList();
        this.mPageLoadProgress = (ProgressBar) findViewById(R.id.progress_main);
        this.mPageLoadProgress.setVisibility(8);
        this.mPageLoadProgress.setProgress(0);
        this.mWebView = (SurfWebView) findViewById(R.id.webview_main);
        this.mWebView.clearCache(true);
        this.mWebView.clearHistory();
        this.mWebView.addJavascriptInterface(new runJavaScript(), "erropage");
        this.mSurfMgr.registerForContextMenu(this.mWebView);
        WebSettings webSettings = this.mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setSupportZoom(false);
        webSettings.setNeedInitialFocus(false);
        if (this.mSurfMgr != null) {
            this.mOriginalUserAgent = webSettings.getUserAgentString();
            String userAgent = this.mSurfMgr.mUserAgent + webSettings.getUserAgentString();
            webSettings.setUserAgentString(userAgent);
            Log.d(LOG_TAG, "Set UserAgent to " + userAgent);
        }
        this.mWebView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (HomeView.this.mSurfMgr == null) {
                    return true;
                }
                HomeView.this.mSurfMgr.browseInCurrentWindow(url, HomeView.this.mTabHost.getCurrentTab());
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                String line;
                Log.d(HomeView.LOG_TAG, "onPageFinished - url:" + url);
                view.requestFocus();
                HomeView.this.mPageLoadProgress.setVisibility(8);
                HomeView.this.mProgressLayer.setVisibility(8);
                if (SurfBrowser.useWML2HTML()) {
                    CacheManager.CacheResult cache = CacheManager.getCacheFile(url, (Map) null);
                    if (cache != null) {
                        String mime = cache.getMimeType();
                        Log.i(HomeView.LOG_TAG, "onPageFinished: mimeType = " + mime);
                        if (mime.equalsIgnoreCase("text/plain")) {
                            BufferedReader br = new BufferedReader(new InputStreamReader(cache.getInputStream()));
                            String buffer = WindowAdapter2.BLANK_URL;
                            do {
                                try {
                                    line = br.readLine();
                                    if (line != null) {
                                        buffer = buffer + line;
                                        Log.e(HomeView.LOG_TAG, "WML line: " + line);
                                        continue;
                                    }
                                } catch (Exception e) {
                                }
                            } while (line != null);
                            view.clearView();
                            try {
                                URL url2 = new URL(url);
                                String html = new Wml2Html().parse(buffer, url2.getHost(), url2.getPath(), true);
                                Log.i(HomeView.LOG_TAG, "host=" + url2.getHost() + ", path=" + url2.getPath());
                                Log.e(HomeView.LOG_TAG, html);
                                view.loadDataWithBaseURL(null, html, "text/html", "utf-8", null);
                            } catch (MalformedURLException e2) {
                                e2.printStackTrace();
                            }
                        }
                        view.setVisibility(0);
                    } else if (!url.equalsIgnoreCase("about:blank")) {
                        Log.i(HomeView.LOG_TAG, "onPageFinished: no cache file found, use javascript method...");
                        view.setVisibility(0);
                    } else {
                        view.setVisibility(0);
                    }
                }
                super.onPageFinished(view, url);
            }

            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                Log.d(HomeView.LOG_TAG, "onPageStarted - url:" + url);
                HomeView.this.mPageLoadProgress.setProgress(0);
                if (!HomeView.this.mIsBrowseInCache) {
                    HomeView.this.mPageLoadProgress.setVisibility(0);
                    HomeView.this.mProgressLayer.setVisibility(0);
                }
                if (SurfBrowser.useWML2HTML()) {
                    CacheManager.CacheResult cache = CacheManager.getCacheFile(url, (Map) null);
                    if (cache != null) {
                        String mime = cache.getMimeType();
                        Log.i(HomeView.LOG_TAG, "onPageStarted: mimeType = " + mime);
                        if (mime.equalsIgnoreCase("text/plain")) {
                            view.setVisibility(4);
                        }
                    } else {
                        view.setVisibility(4);
                    }
                }
                super.onPageStarted(view, url, favicon);
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                view.setVisibility(0);
                view.loadUrl("file:///android_asset/html/error.html");
                String unused = HomeView.this.mFailUrl = failingUrl;
                HomeView.this.mPageLoadProgress.setVisibility(8);
                HomeView.this.mProgressLayer.setVisibility(8);
                Toast.makeText(HomeView.this.mSurfMgr, description, 0).show();
            }
        });
        this.mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                Log.d(HomeView.LOG_TAG, "onProgressChanged - newProgress:" + newProgress);
                if (!HomeView.this.mIsBrowseInCache) {
                    HomeView.this.mPageLoadProgress.setProgress(newProgress);
                    if (newProgress > 10) {
                        HomeView.this.mProgressLayer.setVisibility(8);
                    }
                }
                super.onProgressChanged(view, newProgress);
            }

            public void onReceivedTitle(WebView view, String title) {
                view.setVisibility(0);
                super.onReceivedTitle(view, title);
            }
        });
        return true;
    }

    private boolean initNaviBar() {
        this.btnNaviSearch = findViewById(R.id.btn_search);
        this.btnNaviSearch.setVisibility(8);
        if (SurfManagerActivity.mMsb != null) {
            Msb msb = SurfManagerActivity.mMsb;
            if (!(msb.search == null || msb.search.items == null || msb.search.items.size() <= 0)) {
                this.btnNaviSearch.setVisibility(0);
            }
        }
        this.btnNaviUrl = findViewById(R.id.btn_url);
        this.btnNaviSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putBoolean("show_url", false);
                b.putInt("padding_top", 40);
                HomeView.this.mSurfMgr.startActivityForResult(new Intent(HomeView.this.mSurfMgr, NaviEditSearchActivity.class).putExtras(b), 1);
            }
        });
        this.btnNaviUrl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putBoolean("show_url", false);
                b.putInt("padding_top", 40);
                HomeView.this.mSurfMgr.startActivityForResult(new Intent(HomeView.this.mSurfMgr, NaviEditUrlActivity.class).putExtras(b), 2);
            }
        });
        return true;
    }

    public static Bitmap getImageFromAssetFile(Context context, String fileName) {
        Bitmap image = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
            return image;
        } catch (Exception e) {
            return image;
        }
    }

    public static Bitmap getImageFromAssetFile(Context context, String fileName, int inDensity) {
        Bitmap image = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inDensity = inDensity;
            image = BitmapFactory.decodeStream(is, null, opts);
            is.close();
            return image;
        } catch (Exception e) {
            return image;
        }
    }

    public static Bitmap getImageFromDataFile(Context context, String fileName) {
        Bitmap image = null;
        try {
            FileInputStream is = context.openFileInput(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
            return image;
        } catch (IOException e) {
            return image;
        }
    }

    public static Bitmap getImageFromDataFile(Context context, String fileName, int inDensity) {
        Bitmap image = null;
        try {
            FileInputStream is = context.openFileInput(fileName);
            BitmapFactory.Options opts = new BitmapFactory.Options();
            opts.inDensity = inDensity;
            image = BitmapFactory.decodeStream(is, null, opts);
            is.close();
            return image;
        } catch (IOException e) {
            return image;
        }
    }

    public String md5(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return WindowAdapter2.BLANK_URL;
        }
    }

    public void refreshButton(boolean canGoBack, boolean canGoForward, int count) {
        this.mButtonBack.setEnabled(canGoBack);
        this.mButtonForward.setEnabled(canGoForward);
        this.mButtonHome.setEnabled((this.mSurfMgr.mLastWindowItem == null || this.mSurfMgr.mLastWindowItem.tab == -1 || this.mSurfMgr.mLastWindowItem.tab == this.mDefaultTab) ? false : true);
        if (count < 1) {
            count = 1;
        } else if (count > 9) {
            count = 9;
        }
        this.mButtonWindow.setImageResource(this.mSurfMgr.mNewWindowIcon[count - 1]);
    }

    public void removeScrollMessage() {
        Log.v(LOG_TAG, "enter removeScrollMessage");
        if (this.scrollNewsLayout != null) {
            this.scrollNewsLayout.leaveScrollNewsMessage();
        }
    }

    public void restartScrollMessage() {
        Log.v(LOG_TAG, "enter restartScrollMessage");
        if (this.scrollNewsLayout != null) {
            this.scrollNewsLayout.startScrollNewsMessage();
        }
    }

    public void onOpenScrollUrlLink(String url) {
        this.mSurfMgr.browseInCurrentWindow(URLUtil.guessUrl(url), -1);
    }

    final class runJavaScript {
        runJavaScript() {
        }

        public String runOnAndroidJavaScript(final String str) {
            if (str.equalsIgnoreCase("getfailurl")) {
                Log.v("jdm", "getfailurl mFailUrl= " + HomeView.this.mFailUrl);
                return HomeView.this.mFailUrl;
            }
            HomeView.this.hander.post(new Runnable() {
                public void run() {
                    if (str.equalsIgnoreCase("refresh")) {
                        Log.v("jdm", "refresh");
                        if (HomeView.this.mWebView != null) {
                            HomeView.this.mWebView.setVisibility(4);
                            HomeView.this.mWebView.loadUrlWithInitialScale(HomeView.this.mFailUrl, true, (double) HomeView.this.mWebView.getScale());
                        }
                    } else if (str.equalsIgnoreCase("connect")) {
                        Log.v("jdm", "connect");
                        SurfManagerActivity.mNetworkMgr.connect();
                    }
                }
            });
            return null;
        }
    }
}
