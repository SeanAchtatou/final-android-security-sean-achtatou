package com.cplatform.android.cmsurfclient.naviedit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.HomeView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.cache.CacheDB;
import com.cplatform.android.cmsurfclient.searchtip.SearchTipAdapter;
import com.cplatform.android.cmsurfclient.searchtip.SearchTipDB;
import com.cplatform.android.cmsurfclient.searchtip.SearchTipItem;
import com.cplatform.android.cmsurfclient.service.entry.Msb;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngine;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngines;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;

public class NaviEditSearchActivity extends Activity {
    TextView btnNaviSearch;
    View btnSearchEngine = null;
    TextView clearView = null;
    SearchEngineItem defaultSearchEngine = null;
    EditText editNaviSearch;
    /* access modifiers changed from: private */
    public boolean isAssets = true;
    private ListView mSearchEngineListView = null;
    private SearchTipAdapter mSearchHistoryAdapter;
    private ListView mSearchHistoryView;
    private ArrayList<SearchTipItem> mSearchTipItems;
    /* access modifiers changed from: private */
    public View mSearchTipSelector;
    /* access modifiers changed from: private */
    public String searchEncode = "gbk";
    private ImageView searchEngineImg;
    ArrayList<SearchEngineItem> searchEngineItems;
    /* access modifiers changed from: private */
    public String searchURL = null;
    SearchEngineAdapter serachEngineAdapter;
    LinearLayout serachEngineSelector;

    public void onClick(SearchEngineItem item) {
        this.defaultSearchEngine = item;
        this.serachEngineAdapter.notifyDataSetChanged();
        this.serachEngineSelector.setVisibility(8);
        this.searchURL = item.url;
        this.searchEncode = item.encode;
        String bmFile = CacheDB.getInstance(this).getFile(item.icon);
        this.searchEngineImg.setImageBitmap(this.isAssets ? HomeView.getImageFromAssetFile(this, bmFile) : HomeView.getImageFromDataFile(this, bmFile));
        refreshSearchTip();
    }

    public class SearchEngineAdapter extends ArrayAdapter<SearchEngineItem> {
        private Activity mActivity;

        public class SearchEngineItemOnClickListener implements View.OnClickListener {
            public Activity mActivity;
            public SearchEngineItem mItem;

            public SearchEngineItemOnClickListener(Activity activity, SearchEngineItem item) {
                this.mActivity = activity;
                this.mItem = item;
            }

            public void onClick(View v) {
                ((NaviEditSearchActivity) this.mActivity).onClick(this.mItem);
            }
        }

        public SearchEngineAdapter(Activity activity, ArrayList<SearchEngineItem> items) {
            super(activity, (int) R.layout.search_engine_item, items);
            this.mActivity = activity;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            View row;
            SearchEngineItem item = (SearchEngineItem) getItem(position);
            if (convertView == null) {
                row = this.mActivity.getLayoutInflater().inflate((int) R.layout.search_engine_item, (ViewGroup) null);
            } else {
                row = convertView;
            }
            row.setClickable(true);
            row.setOnClickListener(new SearchEngineItemOnClickListener(NaviEditSearchActivity.this, item));
            ((TextView) row.findViewById(R.id.search_engine_name)).setText(item.title);
            ImageView icon = (ImageView) row.findViewById(R.id.search_engine_icon);
            String bmFile = CacheDB.getInstance(NaviEditSearchActivity.this).getFile(NaviEditSearchActivity.this.searchEngineItems.get(position).icon);
            icon.setImageBitmap(NaviEditSearchActivity.this.isAssets ? HomeView.getImageFromAssetFile(NaviEditSearchActivity.this, bmFile) : HomeView.getImageFromDataFile(NaviEditSearchActivity.this, bmFile));
            ((ImageView) row.findViewById(R.id.search_engine_choice)).setImageResource(NaviEditSearchActivity.this.defaultSearchEngine == item ? R.drawable.ico_radio_on : R.drawable.ico_radio_off);
            return row;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int top = 40;
        int background = R.drawable.naviedit_input_bg;
        Bundle b = getIntent().getExtras();
        if (b != null) {
            top = b.getInt("padding_top", 0);
            background = b.getInt("background", R.drawable.naviedit_input_bg);
        }
        requestWindowFeature(1);
        setContentView(R.layout.naviedit_search);
        ((RelativeLayout) findViewById(R.id.naviedit_bar)).setBackgroundResource(background);
        View v = findViewById(R.id.naviedit_top);
        v.setPadding(v.getPaddingLeft(), top, v.getPaddingRight(), (v.getPaddingBottom() + top) - v.getPaddingTop());
        this.btnNaviSearch = (TextView) findViewById(R.id.btn_search);
        this.btnNaviSearch.setText((int) R.string.naviedit_cancel);
        this.editNaviSearch = (EditText) findViewById(R.id.edit_search);
        this.editNaviSearch.setText(WindowAdapter2.BLANK_URL);
        this.searchEngineImg = (ImageView) findViewById(R.id.img_search_engine);
        this.searchEngineItems = new ArrayList<>();
        Msb msb = SurfManagerActivity.mMsb;
        if (!(msb == null || msb.search == null)) {
            SearchEngines search = msb.search;
            if (search.items != null) {
                this.isAssets = msb.isAssets;
                int len = search.items.size();
                for (int i = 0; i < len; i++) {
                    SearchEngine se = search.items.get(i);
                    this.searchEngineItems.add(new SearchEngineItem(se.img, se.name, se.url, se.encode));
                    if (se.isDefault) {
                        this.defaultSearchEngine = this.searchEngineItems.get(i);
                    }
                }
            }
        }
        if (this.defaultSearchEngine == null && this.searchEngineItems.size() > 0) {
            this.defaultSearchEngine = this.searchEngineItems.get(0);
        }
        this.searchURL = this.defaultSearchEngine.url;
        this.searchEncode = this.defaultSearchEngine.encode;
        String bmFile = CacheDB.getInstance(this).getFile(this.defaultSearchEngine.icon);
        this.searchEngineImg.setImageBitmap(this.isAssets ? HomeView.getImageFromAssetFile(this, bmFile) : HomeView.getImageFromDataFile(this, bmFile));
        this.serachEngineAdapter = new SearchEngineAdapter(this, this.searchEngineItems);
        this.mSearchEngineListView = (ListView) findViewById(R.id.listview_search_engine);
        this.mSearchEngineListView.setAdapter((ListAdapter) this.serachEngineAdapter);
        this.serachEngineSelector = (LinearLayout) findViewById(R.id.search_engine_selector);
        this.btnSearchEngine = findViewById(R.id.search_engine_btn);
        this.btnSearchEngine.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NaviEditSearchActivity.this.mSearchTipSelector.setVisibility(8);
                NaviEditSearchActivity.this.serachEngineSelector.setVisibility(0);
            }
        });
        this.mSearchTipSelector = findViewById(R.id.search_tip_selector);
        this.mSearchTipItems = new ArrayList<>();
        this.mSearchHistoryAdapter = new SearchTipAdapter(this, this.mSearchTipItems);
        this.mSearchHistoryView = (ListView) findViewById(R.id.listview_searchtip);
        if (this.clearView == null) {
            this.clearView = new TextView(this);
            this.clearView.setSingleLine(true);
            this.clearView.setText(Html.fromHtml(getResources().getString(R.string.cleansearchrecord)));
            this.clearView.setGravity(5);
            this.clearView.setTextColor(-14540118);
            this.clearView.setPadding(0, 0, 10, 0);
            this.clearView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    SearchTipDB.getInstance(NaviEditSearchActivity.this).clear();
                    NaviEditSearchActivity.this.refreshSearchTip();
                }
            });
            this.mSearchHistoryView.addFooterView(this.clearView);
        }
        this.mSearchHistoryView.setAdapter((ListAdapter) this.mSearchHistoryAdapter);
        this.editNaviSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!NaviEditSearchActivity.this.editNaviSearch.isFocused() || NaviEditSearchActivity.this.mSearchTipSelector.getVisibility() == 0) {
                    NaviEditSearchActivity.this.mSearchTipSelector.setVisibility(8);
                } else {
                    NaviEditSearchActivity.this.refreshSearchTip();
                }
            }
        });
        this.editNaviSearch.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    NaviEditSearchActivity.this.btnNaviSearch.setText((int) R.string.naviedit_search_go);
                } else {
                    NaviEditSearchActivity.this.btnNaviSearch.setText((int) R.string.naviedit_cancel);
                }
                NaviEditSearchActivity.this.refreshSearchTip();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.editNaviSearch.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                String key;
                if (keyCode != 66) {
                    return false;
                }
                String search = NaviEditSearchActivity.this.editNaviSearch.getText().toString();
                try {
                    key = URLEncoder.encode(search, (NaviEditSearchActivity.this.searchEncode == null || WindowAdapter2.BLANK_URL.equals(NaviEditSearchActivity.this.searchEncode)) ? "gbk" : NaviEditSearchActivity.this.searchEncode);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    key = search;
                }
                NaviEditSearchActivity.this.setResult(-1, new Intent().setAction(NaviEditSearchActivity.this.searchURL + key));
                NaviEditSearchActivity.this.finish();
                return true;
            }
        });
        ((InputMethodManager) this.editNaviSearch.getContext().getSystemService("input_method")).toggleSoftInput(0, 2);
        this.btnNaviSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String key;
                String search = NaviEditSearchActivity.this.editNaviSearch.getText().toString().trim();
                if (TextUtils.isEmpty(search)) {
                    NaviEditSearchActivity.this.setResult(0, null);
                } else {
                    SearchTipDB.getInstance(NaviEditSearchActivity.this).updateOrAdd(search, Long.valueOf(new Date().getTime()));
                    try {
                        key = URLEncoder.encode(search, (NaviEditSearchActivity.this.searchEncode == null || WindowAdapter2.BLANK_URL.equals(NaviEditSearchActivity.this.searchEncode)) ? "gbk" : NaviEditSearchActivity.this.searchEncode);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                        key = search;
                    }
                    NaviEditSearchActivity.this.setResult(-1, new Intent().setAction(NaviEditSearchActivity.this.searchURL + key));
                }
                NaviEditSearchActivity.this.finish();
            }
        });
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            View v = findViewById(R.id.naviedit_bar);
            int[] location = {0, 0};
            v.getLocationOnScreen(location);
            float y = event.getY();
            if (y <= ((float) (location[1] + v.getHeight())) && y >= ((float) location[1])) {
                float x = event.getX();
                this.btnSearchEngine.getLocationOnScreen(location);
                if (x <= ((float) (location[0] + this.btnSearchEngine.getWidth()))) {
                    this.btnSearchEngine.performClick();
                } else {
                    this.btnNaviSearch.getLocationOnScreen(location);
                    if (x >= ((float) location[0])) {
                        this.btnNaviSearch.performClick();
                    }
                }
            } else if (this.serachEngineSelector.getVisibility() == 8) {
                setResult(0, null);
                finish();
            } else {
                this.serachEngineSelector.setVisibility(8);
                refreshSearchTip();
            }
        }
        return true;
    }

    public void onClick(SearchTipItem item) {
        String title;
        if (item == null || item.title == null) {
            title = WindowAdapter2.BLANK_URL;
        } else {
            title = item.title;
        }
        this.editNaviSearch.setText(title);
        this.editNaviSearch.setSelection(title.length());
        refreshSearchTip();
    }

    public void refreshSearchTip() {
        int i;
        if (this.serachEngineSelector.getVisibility() == 8) {
            String tmp = this.editNaviSearch.getText().toString();
            ArrayList<SearchTipItem> ret = new ArrayList<>();
            ArrayList<SearchTipItem> historyItems = SearchTipDB.getInstance(this).find(tmp, 20);
            if (historyItems != null && historyItems.size() > 0) {
                ret.addAll(historyItems);
            }
            this.mSearchTipItems.clear();
            HashSet<String> titles = new HashSet<>();
            if (ret != null && ret.size() > 0) {
                Iterator i$ = ret.iterator();
                while (i$.hasNext()) {
                    SearchTipItem tipTitle = (SearchTipItem) i$.next();
                    if (tipTitle != null && tipTitle.title != null && !tipTitle.title.equals(tmp) && !titles.contains(tipTitle.title)) {
                        titles.add(tipTitle.title);
                        this.mSearchTipItems.add(tipTitle);
                        Log.e("found_searchtip", tipTitle.title);
                    }
                }
            }
            View view = this.mSearchTipSelector;
            if (this.mSearchTipItems.size() > 0) {
                i = 0;
            } else {
                i = 8;
            }
            view.setVisibility(i);
            this.mSearchHistoryAdapter.notifyDataSetChanged();
        }
    }
}
