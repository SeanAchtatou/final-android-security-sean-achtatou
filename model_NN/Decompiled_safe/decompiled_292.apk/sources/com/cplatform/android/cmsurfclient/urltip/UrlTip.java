package com.cplatform.android.cmsurfclient.urltip;

import android.util.Log;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UrlTip {
    private static UrlTip instance = null;
    private boolean isSort = true;
    private final ArrayList<String> items = new ArrayList<>();

    public static UrlTip getInstance() {
        if (instance == null) {
            instance = new UrlTip();
            instance.addItem("http://www.");
            instance.addItem("http://www.xiaoyige.com");
            instance.addItem("http://www.google.com");
            instance.addItem("http://www.baidu.com");
        }
        return instance;
    }

    private UrlTip() {
    }

    private void sort() {
        if (!this.isSort) {
            Collections.sort(this.items);
            this.isSort = true;
        }
    }

    public ArrayList<String> getItems() {
        sort();
        return this.items;
    }

    public void addItem(String item) {
        Log.i("history", "add item:" + item);
        if (!this.items.contains(item)) {
            this.isSort = false;
            this.items.add(item);
        }
    }

    public void removeItem(String item) {
        Log.i("history", "remove item:" + item);
        this.items.remove(item);
    }

    public List<String> find(String item) {
        Log.i("urltip", "find item:" + item);
        if (item == null) {
            return null;
        }
        String item2 = item.trim();
        if (WindowAdapter2.BLANK_URL.equals(item2)) {
            return null;
        }
        ArrayList<String> items2 = getItems();
        int startPos = items2.indexOf(item2);
        if (startPos < 0) {
            items2.add(item2);
            Collections.sort(items2);
            startPos = items2.indexOf(item2);
            items2.remove(startPos);
        }
        String tmp = item2 + 65535;
        int endPos = items2.indexOf(tmp);
        if (endPos < 0) {
            items2.add(tmp);
            Collections.sort(items2);
            endPos = items2.indexOf(tmp);
            items2.remove(endPos);
        }
        if (endPos >= startPos) {
            return items2.subList(startPos, endPos);
        }
        return null;
    }
}
