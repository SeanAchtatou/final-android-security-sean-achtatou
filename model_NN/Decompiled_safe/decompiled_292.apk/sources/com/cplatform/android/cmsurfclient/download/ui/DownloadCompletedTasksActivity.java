package com.cplatform.android.cmsurfclient.download.ui;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Formatter;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.download.provider.Constants;
import com.cplatform.android.cmsurfclient.download.provider.DownloadNotification;
import com.cplatform.android.cmsurfclient.download.provider.DownloadProviderHelper;
import com.cplatform.android.cmsurfclient.download.provider.DownloadSettings;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.download.ui.DownloadHelper;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DownloadCompletedTasksActivity extends Activity implements AdapterView.OnItemClickListener {
    private static final int CONTEXT_MENU_ID_DOWNLOAD_DELETE = 1;
    private static final int CONTEXT_MENU_ID_DOWNLOAD_INFO = 3;
    private static final int CONTEXT_MENU_ID_DOWNLOAD_RENAME = 4;
    private static final int CONTEXT_MENU_ID_DOWNLOAD_RETRY = 2;
    private static final String LOG_TAG = "download/ui/DownloadCompletedTasksActivity";
    /* access modifiers changed from: private */
    public TasksAdapter mContentAdapter;
    private ListView mContentView;
    private DownloadProviderContentObserver mObserver = new DownloadProviderContentObserver(new Handler());
    /* access modifiers changed from: private */
    public Cursor mTasksCompleted;

    class DownloadProviderContentObserver extends ContentObserver {
        public void onChange(boolean flag) {
            super.onChange(flag);
            DownloadCompletedTasksActivity.this.mTasksCompleted.requery();
            DownloadCompletedTasksActivity.this.mContentAdapter.notifyDataSetChanged();
            DownloadCompletedTasksActivity.this.updateActionButtonStatus();
        }

        public DownloadProviderContentObserver(Handler handler) {
            super(handler);
        }
    }

    class TasksAdapter extends BaseAdapter {
        public int getCount() {
            return DownloadCompletedTasksActivity.this.mTasksCompleted.getCount();
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return 0;
        }

        /* Debug info: failed to restart local var, previous not found, register: 18 */
        public View getView(int pos, View view, ViewGroup viewgroup) {
            DownloadCompletedTasksActivity.this.mTasksCompleted.moveToPosition(pos);
            LinearLayout linearlayout = (LinearLayout) view;
            if (view == null) {
                linearlayout = (LinearLayout) LayoutInflater.from(DownloadCompletedTasksActivity.this).inflate((int) R.layout.download_task_complete_item, (ViewGroup) null);
            }
            String s = DownloadCompletedTasksActivity.this.mTasksCompleted.getString(DownloadCompletedTasksActivity.this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_MIME_TYPE));
            ImageView imageview = (ImageView) linearlayout.findViewById(R.id.download_task_item_icon);
            if (TextUtils.isEmpty(s)) {
                imageview.setVisibility(4);
            } else {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setDataAndType(Uri.fromParts("file", WindowAdapter2.BLANK_URL, null), s);
                PackageManager packagemanager = DownloadCompletedTasksActivity.this.getPackageManager();
                List<ResolveInfo> list = packagemanager.queryIntentActivities(intent, 65536);
                if (list.size() > 0) {
                    imageview.setImageDrawable(list.get(0).activityInfo.loadIcon(packagemanager));
                    imageview.setVisibility(0);
                } else {
                    imageview.setVisibility(4);
                }
            }
            String title = DownloadCompletedTasksActivity.this.mTasksCompleted.getString(DownloadCompletedTasksActivity.this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_TITLE));
            if (TextUtils.isEmpty(title)) {
                title = "<Unknown>";
            }
            ((TextView) linearlayout.findViewById(R.id.download_task_item_title)).setText(title);
            ((TextView) linearlayout.findViewById(R.id.download_task_item_weburl_domain)).setText(Uri.parse(DownloadCompletedTasksActivity.this.mTasksCompleted.getString(DownloadCompletedTasksActivity.this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_URI))).getAuthority());
            long l3 = DownloadCompletedTasksActivity.this.mTasksCompleted.getLong(DownloadCompletedTasksActivity.this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_TOTAL_BYTES));
            StringBuilder stringbuilder = new StringBuilder();
            if (Downloads.isStatusSuccess(DownloadCompletedTasksActivity.this.mTasksCompleted.getInt(DownloadCompletedTasksActivity.this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_STATUS)))) {
                if (l3 > 0) {
                    stringbuilder.append(Formatter.formatFileSize(DownloadCompletedTasksActivity.this, l3));
                    stringbuilder.append(' ');
                }
                stringbuilder.append(DownloadCompletedTasksActivity.this.getResources().getText(R.string.download_status_success));
                stringbuilder.append(' ');
                long l5 = DownloadCompletedTasksActivity.this.mTasksCompleted.getLong(DownloadCompletedTasksActivity.this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_LAST_MODIFICATION));
                if (l5 > 0) {
                    stringbuilder.append(DateUtils.formatSameDayTime(l5, new Date().getTime(), 2, 3));
                }
                ((TextView) linearlayout.findViewById(R.id.download_task_item_result)).setTextColor(DownloadCompletedTasksActivity.this.getResources().getColor(R.color.download_black));
                linearlayout.findViewById(R.id.download_task_fail_error_indicator).setVisibility(8);
            } else {
                stringbuilder.append(DownloadCompletedTasksActivity.this.getResources().getText(R.string.download_status_failed));
                ((TextView) linearlayout.findViewById(R.id.download_task_item_result)).setTextColor(DownloadCompletedTasksActivity.this.getResources().getColor(R.color.download_red));
                linearlayout.findViewById(R.id.download_task_fail_error_indicator).setVisibility(0);
            }
            ((TextView) linearlayout.findViewById(R.id.download_task_item_result)).setText(stringbuilder.toString());
            return linearlayout;
        }

        private TasksAdapter() {
        }

        TasksAdapter(DownloadCompletedTasksActivity downloadCompletedTasksActivity, View.OnClickListener _pcls1) {
            this();
        }
    }

    public boolean onContextItemSelected(MenuItem menuitem) {
        int pos = ((AdapterView.AdapterContextMenuInfo) menuitem.getMenuInfo()).position;
        if (pos < 0) {
            return false;
        }
        if (pos >= this.mTasksCompleted.getCount()) {
            return false;
        }
        this.mTasksCompleted.moveToPosition(pos);
        int id = this.mTasksCompleted.getInt(this.mTasksCompleted.getColumnIndex(QueryApList.Carriers._ID));
        String entity = this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_APP_DATA));
        String data = this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads._DATA));
        switch (menuitem.getItemId()) {
            case 1:
                DownloadHelper.confirmAndDeleteDownloadTask(this, Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(id)), entity, data);
                break;
            case 2:
                if (id >= 0) {
                    Toast.makeText(this, DownloadProviderHelper.retryDownload(this, id) ? R.string.download_retry_task_success : R.string.download_retry_task_fail, 0).show();
                    break;
                }
                break;
            case 3:
                StringBuilder stringbuilder = new StringBuilder();
                String title = this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_TITLE));
                if (TextUtils.isEmpty(title)) {
                    title = "<Unknown>";
                }
                String uri = this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_URI));
                stringbuilder.append("文件名：").append(title).append("\n");
                if (Downloads.isStatusSuccess(this.mTasksCompleted.getInt(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_STATUS)))) {
                    long total_bytes = this.mTasksCompleted.getLong(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_TOTAL_BYTES));
                    if (total_bytes > 0) {
                        stringbuilder.append("文件大小：").append(Formatter.formatFileSize(this, total_bytes)).append("\n");
                    }
                    stringbuilder.append("保存路径：").append(data).append("\n");
                } else {
                    stringbuilder.append(getResources().getText(R.string.download_status_failed));
                }
                stringbuilder.append("下载地址：").append(uri).append("\n");
                DownloadHelper.viewDownloadTask(this, stringbuilder.toString());
                break;
            case 4:
                DownloadHelper.confirmAndRenameDownloadTask(this, Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(id)), data);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.downloadtasks_listview);
        this.mTasksCompleted = getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, Downloads.COLUMN_STATUS, Downloads.COLUMN_CURRENT_BYTES, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_MIME_TYPE, Downloads.COLUMN_APP_DATA, Downloads.COLUMN_URI, Downloads.COLUMN_LAST_MODIFICATION, Downloads.COLUMN_APP_DATA, Downloads._DATA}, DownloadNotification.WHERE_COMPLETED_ALL, null, "_id DESC");
        this.mContentView = (ListView) findViewById(R.id.download_tasks_listview);
        this.mContentAdapter = new TasksAdapter(this, null);
        this.mContentView.setAdapter((ListAdapter) this.mContentAdapter);
        this.mContentView.setOnCreateContextMenuListener(this);
        this.mContentView.setOnItemClickListener(this);
        findViewById(R.id.downloadtasks_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadCompletedTasksActivity.this.finish();
            }
        });
        updateActionButtonStatus();
        findViewById(R.id.downloadtasks_clear_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadCompletedTasksActivity.this.confirmAndDeleteAllDownloadTask();
            }
        });
    }

    public void onCreateContextMenu(ContextMenu contextmenu, View view, ContextMenu.ContextMenuInfo contextmenuinfo) {
        if (view == this.mContentView) {
            contextmenu.setHeaderTitle((int) R.string.app_name);
            contextmenu.add(0, 3, 0, (int) R.string.download_menu_details);
            contextmenu.add(0, 1, 0, (int) R.string.download_delete_task);
            contextmenu.add(0, 2, 0, (int) R.string.download_retry_task);
            contextmenu.add(0, 4, 0, (int) R.string.download_menu_rename);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mTasksCompleted.close();
        super.onDestroy();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
        if (pos >= 0 && pos < this.mTasksCompleted.getCount()) {
            this.mTasksCompleted.moveToPosition(pos);
            int status = this.mTasksCompleted.getInt(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_STATUS));
            String data = this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads._DATA));
            String mimeType = this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_MIME_TYPE));
            if (Downloads.isStatusSuccess(status) && !TextUtils.isEmpty(data)) {
                File file = new File(data);
                if (file.exists() && !file.isDirectory()) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(Uri.fromFile(file), mimeType);
                    intent.setFlags(268435456);
                    try {
                        startActivity(intent);
                    } catch (ActivityNotFoundException activitynotfoundexception) {
                        Log.d(Constants.TAG, "no activity for " + mimeType, activitynotfoundexception);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        getContentResolver().registerContentObserver(DownloadSettings.getDownloadProviderContentUri(), true, this.mObserver);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        getContentResolver().unregisterContentObserver(this.mObserver);
    }

    /* access modifiers changed from: private */
    public void updateActionButtonStatus() {
        View clearBtn = findViewById(R.id.downloadtasks_clear_button);
        if (clearBtn != null) {
            if (this.mTasksCompleted.getCount() == 0) {
                clearBtn.setVisibility(4);
                clearBtn.setEnabled(false);
                return;
            }
            clearBtn.setVisibility(0);
            clearBtn.setEnabled(true);
        }
    }

    public void confirmAndDeleteAllDownloadTask() {
        final DownloadHelper.DownloadConfirmableDialogBuilder builder = new DownloadHelper.DownloadConfirmableDialogBuilder(this);
        builder.setTitle((int) R.string.app_name);
        builder.SelectedItem = 0;
        builder.setSingleChoiceItems(new CharSequence[]{getResources().getText(R.string.download_delete_task_only), getResources().getText(R.string.download_delete_task_and_file)}, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                builder.SelectedItem = i;
            }
        });
        builder.setCancelable(false).setPositiveButton((int) R.string.download_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                boolean z;
                DownloadCompletedTasksActivity downloadCompletedTasksActivity = DownloadCompletedTasksActivity.this;
                if (builder.SelectedItem != 1) {
                    z = true;
                } else {
                    z = false;
                }
                downloadCompletedTasksActivity.deleteAllDownloadTask(z);
                Toast.makeText(DownloadCompletedTasksActivity.this, (int) R.string.download_delete_task_success, 0).show();
                dialoginterface.dismiss();
            }
        }).setNegativeButton((int) R.string.download_do_not_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }

    public void deleteAllDownloadTask(boolean deleteTaskOnly) {
        int size = this.mTasksCompleted.getCount();
        ArrayList<Uri> uriList = new ArrayList<>();
        ArrayList<String> entityList = new ArrayList<>();
        ArrayList<String> dataList = new ArrayList<>();
        for (int pos = 0; pos < size; pos++) {
            if (pos < this.mTasksCompleted.getCount()) {
                this.mTasksCompleted.moveToPosition(pos);
                Uri uri = Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(this.mTasksCompleted.getInt(this.mTasksCompleted.getColumnIndex(QueryApList.Carriers._ID))));
                String entity = deleteTaskOnly ? null : this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads.COLUMN_APP_DATA));
                String data = deleteTaskOnly ? null : this.mTasksCompleted.getString(this.mTasksCompleted.getColumnIndex(Downloads._DATA));
                uriList.add(uri);
                entityList.add(entity);
                dataList.add(data);
            }
        }
        int size2 = uriList.size();
        for (int pos2 = 0; pos2 < size2; pos2++) {
            DownloadHelper.deleteDownloadTask(this, (Uri) uriList.get(pos2), (String) entityList.get(pos2), (String) dataList.get(pos2));
        }
    }
}
