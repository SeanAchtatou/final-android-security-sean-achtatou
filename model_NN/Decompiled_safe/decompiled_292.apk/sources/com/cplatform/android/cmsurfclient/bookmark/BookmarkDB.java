package com.cplatform.android.cmsurfclient.bookmark;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import java.util.ArrayList;

public class BookmarkDB {
    private static final String DATABASE_NAME = "CMSurfClient";
    private static final String TABLE_BOOKMARK_NAME = "bookmark";
    private static BookmarkDB instance = null;
    private final String DEBUG_TAG = "BookmarkDB";
    private Context context;
    private SQLiteDatabase db = null;
    public ArrayList<BookmarkItem> items;

    public static BookmarkDB getInstance(Context context2) {
        if (instance == null) {
            instance = new BookmarkDB(context2);
            instance.load();
        }
        return instance;
    }

    public BookmarkDB(Context context2) {
        this.context = context2;
        this.items = new ArrayList<>();
    }

    public boolean openDB() {
        if (this.db == null || !this.db.isOpen()) {
            this.db = this.context.openOrCreateDatabase(DATABASE_NAME, 0, null);
            prepareTable();
        }
        return this.db != null && this.db.isOpen();
    }

    public void closeDB() {
        if (this.db != null) {
            if (this.db.isOpen()) {
                this.db.close();
            }
            this.db = null;
        }
    }

    private void prepareTable() {
        StringBuffer sql = new StringBuffer(100);
        sql.append("create table ").append(TABLE_BOOKMARK_NAME).append("(_id integer primary key autoincrement, title text, url text, img text);");
        try {
            this.db.execSQL(sql.toString());
        } catch (Exception e) {
        }
    }

    public void load() {
        this.items.clear();
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_BOOKMARK_NAME, new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, "url", "img"}, null, null, null, null, null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                int id = cur.getInt(0);
                this.items.add(new BookmarkItem((long) id, cur.getString(1), cur.getString(2), cur.getString(3)));
                Log.i("BookmarkDB", "load:" + id);
                cur.moveToNext();
            }
            cur.close();
            closeDB();
        }
    }

    public boolean add(BookmarkItem item) {
        boolean ret = false;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put(Downloads.COLUMN_TITLE, item.title);
                values.put("url", item.url);
                values.put("img", item.img);
                long id = this.db.insert(TABLE_BOOKMARK_NAME, null, values);
                item.id = id;
                Log.i("BookmarkDB", "*add:" + id + "item.size=" + this.items.size());
                this.items.add(item);
                Log.i("BookmarkDB", "#add:" + id + "item.size=" + this.items.size());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean del(BookmarkItem item) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_BOOKMARK_NAME);
                sql.append(" where _id=").append(item.id).append(";");
                Log.i("BookmarkDB", sql.toString());
                this.db.execSQL(sql.toString());
                this.items.remove(item);
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean update(BookmarkItem item) {
        boolean ret = false;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put(Downloads.COLUMN_TITLE, item.title);
                values.put("url", item.url);
                values.put("img", item.img);
                this.db.update(TABLE_BOOKMARK_NAME, values, "_id=" + item.id, null);
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean isBookmarkExist(BookmarkItem item) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("select * from ").append(TABLE_BOOKMARK_NAME);
                sql.append(" where url='").append(item.url).append("'");
                Log.i("BookmarkDB", sql.toString());
                Cursor cur = this.db.rawQuery(sql.toString(), null);
                if (cur != null) {
                    if (cur.getCount() > 0) {
                        ret = true;
                    }
                    cur.close();
                }
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }
}
