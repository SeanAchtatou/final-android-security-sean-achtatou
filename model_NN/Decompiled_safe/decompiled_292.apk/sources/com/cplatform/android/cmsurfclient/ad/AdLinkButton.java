package com.cplatform.android.cmsurfclient.ad;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class AdLinkButton extends Button {
    private String mUrl = null;

    public AdLinkButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    public String getUrl() {
        return this.mUrl;
    }
}
