package com.cplatform.android.cmsurfclient.bookmark;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import java.util.ArrayList;

public class BookmarkAdapter extends ArrayAdapter<BookmarkItem> {
    private Activity mActivity;

    public BookmarkAdapter(Activity activity, ArrayList<BookmarkItem> items) {
        super(activity, (int) R.layout.bookmark_item, items);
        this.mActivity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        BookmarkItem item = (BookmarkItem) getItem(position);
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate((int) R.layout.bookmark_item, (ViewGroup) null);
        } else {
            row = convertView;
        }
        row.setClickable(true);
        row.setOnClickListener(new BookmarkItemOnClickListener(this.mActivity, item));
        String itemTitle = item.title;
        String itemUrl = item.url;
        ((TextView) row.findViewById(R.id.bookmark_title)).setText(itemTitle);
        ((TextView) row.findViewById(R.id.bookmark_url)).setText(itemUrl);
        ((ImageView) row.findViewById(R.id.bookmark_icon)).setImageResource(item.icon);
        ((ImageButton) row.findViewById(R.id.bookmark_edit)).setOnClickListener(new BookmarkItemEditOnClickListener(this.mActivity, item));
        ((ImageButton) row.findViewById(R.id.bookmark_delete)).setOnClickListener(new BookmarkItemDeleteOnClickListenter(this.mActivity, item));
        return row;
    }
}
