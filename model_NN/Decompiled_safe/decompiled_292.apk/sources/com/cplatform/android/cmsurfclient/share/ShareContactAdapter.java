package com.cplatform.android.cmsurfclient.share;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import java.util.ArrayList;

public class ShareContactAdapter extends ArrayAdapter<ShareContactItem> {
    private Activity mActivity;

    public ShareContactAdapter(Activity activity, ArrayList<ShareContactItem> items) {
        super(activity, (int) R.layout.share_contacts_listitem, items);
        this.mActivity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate((int) R.layout.share_contacts_listitem, (ViewGroup) null);
        } else {
            row = convertView;
        }
        ShareContactItem item = (ShareContactItem) getItem(position);
        CheckBox chkSelected = (CheckBox) row.findViewById(R.id.share_contacts_listitem_selected);
        chkSelected.setOnCheckedChangeListener(new ContactItemOnCheckedChangeListener(this.mActivity, item));
        String itemUserName = item.mUserName;
        String itemPhoneNumber = item.mPhoneNumber;
        boolean itemChecked = item.mIsSelected;
        ((TextView) row.findViewById(R.id.share_contacts_listitem_username)).setText(itemUserName);
        ((TextView) row.findViewById(R.id.share_contacts_listitem_phonenumber)).setText(itemPhoneNumber);
        chkSelected.setChecked(itemChecked);
        return row;
    }

    public class ContactItemOnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {
        private Activity mActivity;
        private ShareContactItem mItem;

        public ContactItemOnCheckedChangeListener(Activity activity, ShareContactItem item) {
            this.mActivity = activity;
            this.mItem = item;
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (this.mItem.mIsSelected != isChecked) {
                this.mItem.mIsSelected = isChecked;
                if (!((ShareSelectUserActivity) this.mActivity).onUserSelectionChanged(this.mItem)) {
                    buttonView.setChecked(false);
                }
            }
        }
    }
}
