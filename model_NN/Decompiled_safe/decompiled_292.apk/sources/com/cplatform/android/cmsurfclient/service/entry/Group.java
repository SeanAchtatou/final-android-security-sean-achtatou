package com.cplatform.android.cmsurfclient.service.entry;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Group extends Item {
    public ArrayList<Item> items = null;

    public Group() {
    }

    public Group(Element entry) {
        super(entry);
        NodeList nlGroupItem;
        if (entry != null && (nlGroupItem = entry.getElementsByTagName("item")) != null && nlGroupItem.getLength() > 0) {
            this.items = new ArrayList<>();
            int groupItemLen = nlGroupItem.getLength();
            for (int m = 0; m < groupItemLen; m++) {
                this.items.add(new Item((Element) nlGroupItem.item(m)));
            }
        }
    }

    public String toString() {
        return "name:" + this.name + "\tcolor:" + this.color + "\timg:" + this.img + "\tact:" + this.act + "\turl:" + this.url;
    }
}
