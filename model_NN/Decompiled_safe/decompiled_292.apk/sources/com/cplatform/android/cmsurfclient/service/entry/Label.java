package com.cplatform.android.cmsurfclient.service.entry;

import android.content.Context;
import android.util.Xml;
import com.cplatform.android.cmsurfclient.SurfBrowser;
import com.cplatform.android.cmsurfclient.cache.CacheDB;
import com.cplatform.android.cmsurfclient.cache.CacheItem;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xmlpull.v1.XmlSerializer;

public class Label {
    public static final String CLEAR_CACHE = "clear";
    public static final String LOAD_CACHE = "load";
    public static final String SAVE_CACHE = "save";
    static boolean isFirst = true;
    public String act = null;
    public String color = null;
    public ArrayList<Group> groups = null;
    public String id = null;
    public String imgDown = null;
    public String imgUp = null;
    public boolean isDefault = false;
    public String layout = null;
    public String name = null;
    public String url = null;

    public Label() {
    }

    public Label(Element entry) {
        NodeList nlGroup;
        CacheDB cacheDB;
        if (entry != null) {
            this.name = entry.getAttribute(QueryApList.Carriers.NAME);
            this.color = entry.getAttribute("color");
            this.imgUp = entry.getAttribute("img_up");
            this.imgDown = entry.getAttribute("img_down");
            this.act = entry.getAttribute("act");
            this.url = entry.getAttribute("url");
            this.isDefault = "1".equals(entry.getAttribute("default"));
            this.layout = entry.getAttribute("layout");
            this.id = entry.getAttribute("id");
            String cache = entry.getAttribute("cache");
            if (!"QLINK".equals(this.act) && !"COMMON".equals(this.act) && ((this.act == null || WindowAdapter2.BLANK_URL.equals(this.act)) && (nlGroup = entry.getElementsByTagName("group")) != null && nlGroup.getLength() > 0)) {
                this.groups = new ArrayList<>();
                int groupLen = nlGroup.getLength();
                for (int k = 0; k < groupLen; k++) {
                    this.groups.add(new Group((Element) nlGroup.item(k)));
                }
            }
            if (cache != null && !WindowAdapter2.BLANK_URL.equals(cache) && this.id != null && !WindowAdapter2.BLANK_URL.equals(this.id) && (cacheDB = CacheDB.getInstance(SurfBrowser.getInstance())) != null) {
                String labelID = "msb." + this.id;
                String labelFile = "msb." + this.id + ".dat";
                CacheItem cacheItem = cacheDB.items.get(labelID);
                if (LOAD_CACHE.equalsIgnoreCase(cache)) {
                    if (cacheItem != null) {
                        loadCachedLabel(SurfBrowser.getInstance(), cacheItem.file);
                    }
                } else if (SAVE_CACHE.equalsIgnoreCase(cache)) {
                    if (cacheItem != null) {
                        cacheDB.del(cacheItem);
                    }
                    saveCachedLabel(SurfBrowser.getInstance(), labelFile, entry);
                    cacheDB.add(new CacheItem(-1, labelID, labelFile, new Date()));
                } else if (CLEAR_CACHE.equalsIgnoreCase(cache) && cacheItem != null) {
                    cacheDB.del(cacheItem);
                }
            }
        }
    }

    private void loadCachedLabel(Context context, String f) {
        NodeList nlGroup;
        if (context != null) {
            FileInputStream in = null;
            try {
                FileInputStream in2 = context.openFileInput(f);
                NodeList nl = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in2).getDocumentElement().getElementsByTagName("label");
                if (nl != null && nl.getLength() > 0 && (nlGroup = ((Element) nl.item(0)).getElementsByTagName("group")) != null && nlGroup.getLength() > 0) {
                    this.groups = new ArrayList<>();
                    int groupLen = nlGroup.getLength();
                    for (int k = 0; k < groupLen; k++) {
                        this.groups.add(new Group((Element) nlGroup.item(k)));
                    }
                }
                if (in2 != null) {
                    try {
                        in2.close();
                    } catch (Exception e) {
                    }
                }
            } catch (Exception e2) {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Exception e3) {
                    }
                }
            } catch (Throwable th) {
                if (in != null) {
                    try {
                        in.close();
                    } catch (Exception e4) {
                    }
                }
                throw th;
            }
        }
    }

    private boolean saveCachedLabel(Context context, String f, Element entry) {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            dealNode(entry, serializer);
            serializer.endDocument();
            try {
                OutputStream os = context.openFileOutput(f, 0);
                OutputStreamWriter osw = new OutputStreamWriter(os);
                osw.write(writer.toString());
                osw.close();
                os.close();
                return true;
            } catch (IOException e) {
                return false;
            }
        } catch (Exception e2) {
            return false;
        }
    }

    private void dealNode(Node node, XmlSerializer serializer) {
        try {
            if (node.getNodeType() == 3) {
                serializer.text(node.getNodeValue());
                return;
            }
            serializer.startTag(WindowAdapter2.BLANK_URL, node.getNodeName());
            String text = node.getNodeValue();
            if (text != null) {
                serializer.text(text);
            }
            NamedNodeMap map = node.getAttributes();
            int attrSize = map.getLength();
            for (int i = 0; i < attrSize; i++) {
                Node Attrnode = map.item(i);
                serializer.attribute(WindowAdapter2.BLANK_URL, Attrnode.getNodeName(), Attrnode.getNodeValue());
            }
            NodeList childs = node.getChildNodes();
            int nodeSize = childs.getLength();
            for (int i2 = 0; i2 < nodeSize; i2++) {
                dealNode(childs.item(i2), serializer);
            }
            serializer.endTag(WindowAdapter2.BLANK_URL, node.getNodeName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return "name:" + this.name + "\tcolor:" + this.color + "\timg_up:" + this.imgUp + "\timg_down:" + this.imgDown + "\tdefault:" + this.isDefault + "\tact:" + this.act + "\tlayout:" + this.layout;
    }
}
