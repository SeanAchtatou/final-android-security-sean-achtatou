package com.cplatform.android.cmsurfclient;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Browser;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.bookmark.BookmarkManager;
import com.cplatform.android.cmsurfclient.bookmark.IBookmarkManager;
import com.cplatform.android.cmsurfclient.bookmark_history.BookmarkHistoryTabActivity;
import com.cplatform.android.cmsurfclient.download.provider.DownloadProviderHelper;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.history.HistoryDB;
import com.cplatform.android.cmsurfclient.menu.BrowserOptionsMenu;
import com.cplatform.android.cmsurfclient.naviedit.NaviEditSearchActivity;
import com.cplatform.android.cmsurfclient.naviedit.NaviEditUrlActivity;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.preference.SurfBrowserSettings;
import com.cplatform.android.cmsurfclient.quicklink.QuickLinkHelper;
import com.cplatform.android.cmsurfclient.service.entry.Msb;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngine;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngines;
import com.cplatform.android.cmsurfclient.share.ShareImageActivity;
import com.cplatform.android.cmsurfclient.share.SharePageActivity;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import com.cplatform.android.cmsurfclient.windows.WindowItemNew;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class BrowserViewNew extends LinearLayout implements IWebViewManager, ISelectText, IBookmarkManager {
    static final int MESSAGE_DOWNLOAD_OMS = 3;
    static final int MESSAGE_HIDE_CONTROLS = 2;
    static final int MESSAGE_PARSE_OMS = 4;
    static final int MESSAGE_SHOW = 1;
    static final String SHAREIMAGE_SRC = "src=";
    static final String SHAREIMAGE_TAG = "shareimage.do";
    static final String SHAREIMAGE_TITLE = "share_title=";
    static final String SHAREIMAGE_URL = "share_url=";
    static final String SHAREPAGE_SRC = "g3url=";
    static final String SHAREPAGE_TITLE = "page_title=";
    static final String SHAREPAGE_URL = "page_url=";
    static final String SHARETYPE_IMAGE = "method=preShareImage";
    static final String SHARETYPE_TEXT = "method=preShareText";
    static final String WEBSHAREURL = "http://go.10086.cn/share/shareImage.do?";
    private final String LOG_TAG = BrowserViewNew.class.getSimpleName();
    private BookmarkManager mBookmarkMgr = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonBack = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonCancel = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonForward = null;
    private ImageButton mButtonHome = null;
    private ImageButton mButtonMenu = null;
    /* access modifiers changed from: private */
    public View mButtonRefresh = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonScrollDown = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonScrollUp = null;
    private ImageButton mButtonShare = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonShowFS = null;
    /* access modifiers changed from: private */
    public View mButtonStop = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonStopFS = null;
    private ImageButton mButtonWindow = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonZoomIn = null;
    /* access modifiers changed from: private */
    public ImageButton mButtonZoomOut = null;
    ArrayList<HashMap<String, Object>> mDlgList;
    FindDialog mFindDialog = null;
    private RelativeLayout mFullScreenToolBar = null;
    /* access modifiers changed from: private */
    public Handler mHandler = null;
    public WindowItemNew mItem = null;
    private View mNaviUrl = null;
    SelectText mSelectText = null;
    /* access modifiers changed from: private */
    public int mShowTimeCount = 0;
    /* access modifiers changed from: private */
    public SurfManagerActivity mSurfMgr = null;
    private TimerTask mTask = null;
    private Timer mTimer = null;
    /* access modifiers changed from: private */
    public RelativeLayout mTitleBar = null;
    /* access modifiers changed from: private */
    public LinearLayout mToolBar = null;
    /* access modifiers changed from: private */
    public ProgressBar mWebProgress = null;
    /* access modifiers changed from: private */
    public ProgressBar mWebProgressUrl = null;
    private TextView mWebTitle = null;
    public FrameLayout mWebViewContainer = null;
    public WebViewManager mWebViewMgr = null;

    static /* synthetic */ int access$008(BrowserViewNew x0) {
        int i = x0.mShowTimeCount;
        x0.mShowTimeCount = i + 1;
        return i;
    }

    public BrowserViewNew(SurfManagerActivity context, WindowItemNew item) {
        super(context);
        this.mSurfMgr = context;
        this.mItem = item;
        LayoutInflater.from(context).inflate((int) R.layout.browser_new, this);
        initTitleBar();
        initWebViewMgr();
        initSelectText();
        initLoadProgress();
        initToolBar();
        initScrollBar();
        initZoomBar();
        initTimer();
        this.mBookmarkMgr = new BookmarkManager(this.mSurfMgr, this);
        if (!TextUtils.isEmpty(this.mItem.url)) {
            loadUrl(this.mItem.url, false);
        } else {
            this.mWebTitle.setText((int) R.string.blandpage);
        }
    }

    public SurfManagerActivity getSurfManager() {
        return this.mSurfMgr;
    }

    public void onClose() {
        Log.e(this.LOG_TAG, "onClose");
        this.mWebViewMgr.destroy();
        if (this.mTimer != null) {
            try {
                this.mTimer.cancel();
                this.mTimer.purge();
            } catch (Exception e) {
            }
        }
    }

    public void onResume() {
        Log.e(this.LOG_TAG, "onResume");
        if (this.mSurfMgr != null) {
            if (this.mSurfMgr.mIsFullScreen) {
                this.mTitleBar.setVisibility(8);
                this.mToolBar.setVisibility(8);
            }
            if (SurfBrowserSettings.getInstance().isShowZoomControls()) {
                this.mButtonZoomIn.setVisibility(0);
                this.mButtonZoomOut.setVisibility(0);
            } else {
                this.mButtonZoomIn.setVisibility(8);
                this.mButtonZoomOut.setVisibility(8);
            }
            if (SurfBrowserSettings.getInstance().isShowPageControls()) {
                this.mButtonScrollUp.setVisibility(0);
                this.mButtonScrollDown.setVisibility(0);
                return;
            }
            this.mButtonScrollUp.setVisibility(8);
            this.mButtonScrollDown.setVisibility(8);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.mSelectText != null) {
                this.mSelectText.endSelectText();
            }
            if (this.mWebViewMgr.canGoBack()) {
                this.mWebViewMgr.goBack();
            } else if (this.mSurfMgr != null) {
                this.mSurfMgr.onBrowserClickBack();
            }
        } else if (keyCode == 82) {
            if (this.mSelectText != null) {
                this.mSelectText.endSelectText();
            }
            if (this.mSurfMgr.mIsFullScreen) {
                this.mTitleBar.setVisibility(0);
                this.mToolBar.setVisibility(0);
            }
            Bundle bundle = new Bundle();
            bundle.putString("menu_type", "menu_browser");
            bundle.putBoolean("full_screen", this.mSurfMgr.mIsFullScreen);
            this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, BrowserOptionsMenu.class).putExtras(bundle), 4);
        } else if (keyCode == 25) {
            if (this.mSelectText != null) {
                this.mSelectText.endSelectText();
            }
            if (!SurfBrowserSettings.getInstance().isUseVolumeKeys()) {
                return false;
            }
            this.mWebViewMgr.pageDown(false);
        } else if (keyCode == 24) {
            if (this.mSelectText != null) {
                this.mSelectText.endSelectText();
            }
            if (!SurfBrowserSettings.getInstance().isUseVolumeKeys()) {
                return false;
            }
            this.mWebViewMgr.pageUp(false);
        }
        return true;
    }

    public void onNetworkStatusChanged(int networkStatus) {
        Log.e(this.LOG_TAG, "onNetworkStatusChanged: network status = " + networkStatus);
        if (this.mWebViewMgr != null) {
            this.mWebViewMgr.onNetworkStatusChanged(networkStatus);
        }
    }

    private void setTextInRange(TextView view, String title) {
        view.setText(title);
    }

    public void setTitle(String title) {
        this.mWebTitle.setText(title);
    }

    private void initSelectText() {
        this.mSelectText = (SelectText) findViewById(R.id.selectText);
        this.mSelectText.setInterface(this);
    }

    private void initLoadProgress() {
        this.mWebProgress = (ProgressBar) findViewById(R.id.progress_browser);
        this.mWebProgress.setVisibility(8);
        this.mWebProgressUrl = (ProgressBar) findViewById(R.id.progress_url);
        this.mWebProgressUrl.setVisibility(8);
    }

    private void resetTimer() {
        Log.e(this.LOG_TAG, "resetTimer");
        if (this.mTimer != null && this.mTask != null) {
            try {
                this.mTimer.schedule(this.mTask, 0, 1000);
            } catch (Exception e) {
                Log.e(this.LOG_TAG, "exception: " + e.toString());
            }
        }
    }

    private void initTimer() {
        this.mTimer = new Timer();
        this.mTask = new TimerTask() {
            public void run() {
                BrowserViewNew.access$008(BrowserViewNew.this);
                if (BrowserViewNew.this.mShowTimeCount >= 5) {
                    int unused = BrowserViewNew.this.mShowTimeCount = 0;
                    Message message = new Message();
                    message.what = 2;
                    BrowserViewNew.this.mHandler.sendMessage(message);
                }
            }
        };
        this.mHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 2:
                        if (SurfBrowserSettings.getInstance().isShowZoomControls()) {
                            BrowserViewNew.this.mButtonZoomIn.setVisibility(8);
                            BrowserViewNew.this.mButtonZoomOut.setVisibility(8);
                        }
                        if (SurfBrowserSettings.getInstance().isShowPageControls()) {
                            BrowserViewNew.this.mButtonScrollUp.setVisibility(8);
                            BrowserViewNew.this.mButtonScrollDown.setVisibility(8);
                            return;
                        }
                        return;
                    case 3:
                        BrowserViewNew.this.downloadAndParseOMS(msg.getData().getString("oms_url"));
                        return;
                    case 4:
                        BrowserViewNew.this.parseOMS(msg.getData().getString("oms_file"));
                        return;
                    default:
                        return;
                }
            }
        };
        resetTimer();
    }

    private void initTitleBar() {
        this.mTitleBar = (RelativeLayout) findViewById(R.id.browser_titlebar);
        if (this.mSurfMgr == null || !this.mSurfMgr.mIsFullScreen) {
            this.mTitleBar.setVisibility(0);
        } else {
            this.mTitleBar.setVisibility(8);
        }
        this.mWebTitle = (TextView) findViewById(R.id.txt_url);
        this.mWebTitle.setSingleLine();
        this.mWebTitle.setEllipsize(TextUtils.TruncateAt.END);
        this.mButtonShare = (ImageButton) findViewById(R.id.browser_titlebar_btn_share);
        this.mButtonStop = findViewById(R.id.browser_titlebar_btn_stop);
        this.mButtonRefresh = findViewById(R.id.browser_titlebar_btn_refresh);
        this.mButtonShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                SurfWebView webView = BrowserViewNew.this.mWebViewMgr.getCurWebView();
                if (webView != null && !TextUtils.isEmpty(webView.getUrl())) {
                    BrowserViewNew.this.sharePage(webView.getTitle(), webView.getUrl());
                }
            }
        });
        this.mButtonStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.doCancelLoading();
            }
        });
        this.mButtonRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                SurfWebView webView = BrowserViewNew.this.mWebViewMgr.getCurWebView();
                if (webView != null) {
                    String webViewUrl = webView.getUrl();
                    if (TextUtils.isEmpty(webViewUrl)) {
                        if (BrowserViewNew.this.mItem == null) {
                            webViewUrl = null;
                        } else {
                            webViewUrl = BrowserViewNew.this.mItem.url;
                        }
                    }
                    if (TextUtils.isEmpty(webViewUrl) || webViewUrl.equalsIgnoreCase("about:blank")) {
                        BrowserViewNew.this.mButtonRefresh.setVisibility(8);
                        return;
                    }
                    BrowserViewNew.this.mItem.title = WindowAdapter2.BLANK_URL;
                    BrowserViewNew.this.mButtonRefresh.setVisibility(8);
                    BrowserViewNew.this.mButtonStop.setVisibility(0);
                    BrowserViewNew.this.mButtonStopFS.setVisibility(0);
                    BrowserViewNew.this.mButtonCancel.setVisibility(0);
                    BrowserViewNew.this.mButtonBack.setVisibility(8);
                    if (BrowserViewNew.this.mSurfMgr != null && BrowserViewNew.this.mSurfMgr.mIsFullScreen) {
                        BrowserViewNew.this.mWebProgress.setProgress(0);
                        BrowserViewNew.this.mWebProgress.setVisibility(0);
                    }
                    BrowserViewNew.this.mWebProgressUrl.setProgress(0);
                    BrowserViewNew.this.mWebProgressUrl.setVisibility(0);
                    BrowserViewNew.this.mWebViewMgr.refresh();
                    return;
                }
                BrowserViewNew.this.mButtonRefresh.setVisibility(8);
            }
        });
        this.mNaviUrl = findViewById(R.id.btn_url);
        this.mNaviUrl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                Bundle b = new Bundle();
                b.putBoolean("show_url", true);
                b.putCharSequence("url", BrowserViewNew.this.mItem.url);
                b.putInt("padding_top", 0);
                b.putInt("background", R.drawable.naviedit_input_bg);
                BrowserViewNew.this.mSurfMgr.startActivityForResult(new Intent(BrowserViewNew.this.mSurfMgr, NaviEditUrlActivity.class).putExtras(b), 2);
            }
        });
    }

    /* access modifiers changed from: private */
    public void doCancelLoading() {
        Log.e(this.LOG_TAG, "doCancelLoading()...");
        this.mButtonStopFS.setVisibility(8);
        this.mButtonStop.setVisibility(8);
        this.mButtonCancel.setVisibility(8);
        this.mButtonBack.setVisibility(0);
        SurfWebView webView = this.mWebViewMgr.getCurWebView();
        if (webView != null) {
            webView.stopLoading();
            if (webView.getUrl() == null) {
                this.mWebTitle.setText((int) R.string.blandpage);
                this.mButtonRefresh.setVisibility(8);
                return;
            }
            String title = webView.getTitle();
            setTextInRange(this.mWebTitle, title == null ? webView.getUrl() : title);
        }
    }

    private void initZoomBar() {
        this.mButtonZoomIn = (ImageButton) findViewById(R.id.btn_zoomin);
        this.mButtonZoomOut = (ImageButton) findViewById(R.id.btn_zoomout);
        if (SurfBrowserSettings.getInstance().isShowZoomControls()) {
            this.mButtonZoomIn.setVisibility(0);
        } else {
            this.mButtonZoomOut.setVisibility(8);
        }
        this.mButtonZoomIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.mWebViewMgr.zoomIn();
            }
        });
        this.mButtonZoomOut.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.mWebViewMgr.zoomOut();
            }
        });
    }

    private void initScrollBar() {
        this.mButtonScrollUp = (ImageButton) findViewById(R.id.btn_scrollup);
        this.mButtonScrollDown = (ImageButton) findViewById(R.id.btn_scrolldown);
        if (SurfBrowserSettings.getInstance().isShowPageControls()) {
            this.mButtonScrollUp.setVisibility(0);
            this.mButtonScrollDown.setVisibility(0);
        } else {
            this.mButtonScrollUp.setVisibility(8);
            this.mButtonScrollDown.setVisibility(8);
        }
        this.mButtonScrollUp.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.mWebViewMgr.pageUp(false);
            }
        });
        this.mButtonScrollDown.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.mWebViewMgr.pageDown(false);
            }
        });
    }

    private void initToolBar() {
        this.mFullScreenToolBar = (RelativeLayout) findViewById(R.id.fullscreen_toolbar);
        this.mFullScreenToolBar.setVisibility(this.mSurfMgr.mIsFullScreen ? 0 : 8);
        this.mButtonStopFS = (ImageButton) findViewById(R.id.fullscreen_btn_stop);
        this.mButtonStopFS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.doCancelLoading();
                BrowserViewNew.this.mButtonStopFS.setVisibility(8);
            }
        });
        this.mButtonShowFS = (ImageButton) findViewById(R.id.fullscreen_btn_show);
        this.mButtonShowFS.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.mTitleBar.setVisibility(0);
                BrowserViewNew.this.mToolBar.setVisibility(0);
                BrowserViewNew.this.mButtonShowFS.setVisibility(8);
            }
        });
        this.mToolBar = (LinearLayout) findViewById(R.id.browser_toolbar);
        this.mToolBar.setVisibility(this.mSurfMgr.mIsFullScreen ? 8 : 0);
        this.mButtonHome = (ImageButton) findViewById(R.id.ButtonHome);
        this.mButtonBack = (ImageButton) findViewById(R.id.ButtonBackward);
        this.mButtonCancel = (ImageButton) findViewById(R.id.ButtonCancel);
        this.mButtonForward = (ImageButton) findViewById(R.id.ButtonForward);
        this.mButtonWindow = (ImageButton) findViewById(R.id.ButtonWindows);
        this.mButtonMenu = (ImageButton) findViewById(R.id.ButtonMenu);
        this.mButtonForward.setEnabled(false);
        this.mButtonHome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                if (BrowserViewNew.this.mSurfMgr != null) {
                    BrowserViewNew.this.mSurfMgr.onBrowserClickHome(BrowserViewNew.this.mWebViewMgr.canGoForward());
                }
            }
        });
        this.mButtonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                if (BrowserViewNew.this.mWebViewMgr.canGoBack()) {
                    BrowserViewNew.this.mWebViewMgr.goBack();
                } else if (BrowserViewNew.this.mSurfMgr != null) {
                    BrowserViewNew.this.mSurfMgr.onBrowserClickBack();
                }
            }
        });
        this.mButtonCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.doCancelLoading();
            }
        });
        this.mButtonForward.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BrowserViewNew.this.mButtonForward.clearAnimation();
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                if (BrowserViewNew.this.mWebViewMgr.canGoForward()) {
                    BrowserViewNew.this.mButtonForward.setEnabled(false);
                    BrowserViewNew.this.mWebViewMgr.goForward();
                }
            }
        });
        this.mButtonWindow.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                if (BrowserViewNew.this.mSurfMgr != null) {
                    BrowserViewNew.this.mSurfMgr.setCurrentSnapshot(BrowserViewNew.this);
                    BrowserViewNew.this.mSurfMgr.showWindowsManager();
                }
            }
        });
        this.mButtonMenu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BrowserViewNew.this.mSelectText != null) {
                    BrowserViewNew.this.mSelectText.endSelectText();
                }
                Bundle bundle = new Bundle();
                bundle.putString("menu_type", "menu_browser");
                bundle.putBoolean("full_screen", BrowserViewNew.this.mSurfMgr.mIsFullScreen);
                BrowserViewNew.this.mSurfMgr.startActivityForResult(new Intent(BrowserViewNew.this.mSurfMgr, BrowserOptionsMenu.class).putExtras(bundle), 4);
            }
        });
    }

    public void onTouch(View v, MotionEvent event) {
        if (this.mSurfMgr.mIsFullScreen) {
            this.mTitleBar.setVisibility(8);
            this.mToolBar.setVisibility(8);
            this.mFullScreenToolBar.setVisibility(0);
            this.mButtonShowFS.setVisibility(0);
        }
        if (SurfBrowserSettings.getInstance().isShowZoomControls()) {
            this.mButtonZoomIn.setVisibility(0);
            this.mButtonZoomOut.setVisibility(0);
        }
        if (SurfBrowserSettings.getInstance().isShowPageControls()) {
            this.mButtonScrollUp.setVisibility(0);
            this.mButtonScrollDown.setVisibility(0);
        }
        this.mShowTimeCount = 0;
        switch (event.getAction()) {
            case 0:
            case 1:
                if (!v.hasFocus()) {
                    v.requestFocus();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void initWebViewMgr() {
        this.mWebViewContainer = (FrameLayout) findViewById(R.id.webviewContainer);
        if (this.mWebViewContainer != null) {
            this.mWebViewContainer.setVisibility(0);
        }
        this.mWebViewMgr = new WebViewManager(this);
    }

    public void onDoubleTap(MotionEvent e) {
        this.mWebViewMgr.zoomAuto();
    }

    public void onContextItemSelected(int itemID, String url, String title) {
        if (!TextUtils.isEmpty(url)) {
            switch (itemID) {
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_OPEN /*31*/:
                    this.mWebViewMgr.loadUrl(url, true);
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_OPENNEW /*32*/:
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.setCurrentSnapshot(this);
                        this.mSurfMgr.browseInNewWindow(url, -1);
                        return;
                    }
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_BOOKMARK /*33*/:
                    addBookmark(title, url, false);
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_SHARELINK /*34*/:
                    sharePage(title, url);
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_SHAREPICTURE /*35*/:
                    SurfWebView webView = this.mWebViewMgr.getCurWebView();
                    if (webView != null) {
                        shareImage(title, url, url, webView.getTitle(), webView.getUrl());
                        return;
                    }
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_DOWNLOADPICTURE /*36*/:
                    downloadImage(title, url);
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_PAGEPROPERTY /*37*/:
                    showPagePropertyDlg(title, url);
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_CLOSEPAGE /*38*/:
                    if (this.mSelectText != null) {
                        this.mSelectText.endSelectText();
                    }
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.onBrowserClickHome(this.mWebViewMgr.canGoForward());
                        return;
                    }
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_ADDBOOKMARK /*39*/:
                    addBookmark(title, url, true);
                    return;
                case SurfManagerActivity.CONTEXTMENU_BROWSERVIEW_OPENBACKGROUND /*40*/:
                    if (this.mSurfMgr != null) {
                        this.mSurfMgr.browseInBackground(url);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void loadUrlWithPrefix(String url, boolean isSetInitialScale) {
        if (!TextUtils.isEmpty(url)) {
            HistoryDB.getInstance(SurfBrowser.getInstance()).updateOrAdd(url, WindowAdapter2.BLANK_URL, Long.valueOf(new Date().getTime()));
            this.mWebViewMgr.loadUrl(url, isSetInitialScale);
        }
    }

    private void loadUrl(String url, boolean isSetInitialScale) {
        Log.e(this.LOG_TAG, "loarUrl()...");
        Log.w(this.LOG_TAG, "url: " + url);
        Log.w(this.LOG_TAG, "isSetInitialScale: " + isSetInitialScale);
        loadUrlWithPrefix(url, isSetInitialScale);
    }

    public void loadUrl(String url, int tab, boolean isSetInitialScale) {
        this.mItem.url = url;
        if (tab >= 0) {
            this.mItem.tab = tab;
        }
        loadUrl(this.mItem.url, isSetInitialScale);
    }

    private void addBookmark(String bookmarkTitle, String bookmarkUrl, boolean isShowCheckBox) {
        if (bookmarkUrl != null && bookmarkUrl.length() > 0) {
            if (bookmarkTitle == null) {
                bookmarkTitle = WindowAdapter2.BLANK_URL;
            }
            if (this.mBookmarkMgr != null) {
                this.mBookmarkMgr.addBookmark(bookmarkTitle, bookmarkUrl, isShowCheckBox);
            }
        }
    }

    public String md5(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return WindowAdapter2.BLANK_URL;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void downloadImage(String title, String url) {
        if (!TextUtils.isEmpty(url)) {
            if (url.startsWith(SurfManagerActivity.mMsb.domain)) {
                url = url.replace(SurfManagerActivity.mMsb.domain, "http://");
            }
            int srcPos = url.toLowerCase().indexOf(SHAREPAGE_SRC);
            if (srcPos > 0) {
                url = URLUtil.guessUrl(url.substring(SHAREPAGE_SRC.length() + srcPos));
                Log.e(this.LOG_TAG, "downloadImage - g3src:" + url);
            }
            if (title == null) {
                title = WindowAdapter2.BLANK_URL;
            }
            Log.e(this.LOG_TAG, "downloadImage: " + title);
            Log.e(this.LOG_TAG, url);
            ContentValues contentValues = new ContentValues();
            contentValues.put(Downloads.COLUMN_STATUS, (Integer) -100);
            contentValues.put(Downloads.COLUMN_APP_DATA, WindowAdapter2.BLANK_URL);
            DownloadProviderHelper.startDownload(this.mSurfMgr, url, WindowAdapter2.BLANK_URL, title, contentValues);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void downloadFile(String title, String url) {
        if (!TextUtils.isEmpty(url)) {
            if (title == null) {
                title = WindowAdapter2.BLANK_URL;
            }
            Log.e(this.LOG_TAG, "downloadFile: " + title);
            Log.e(this.LOG_TAG, url);
            ContentValues contentValues = new ContentValues();
            contentValues.put(Downloads.COLUMN_STATUS, (Integer) -100);
            contentValues.put(Downloads.COLUMN_APP_DATA, WindowAdapter2.BLANK_URL);
            DownloadProviderHelper.startDownload(this.mSurfMgr, url, WindowAdapter2.BLANK_URL, title, contentValues);
        }
    }

    public void setFullScreen(boolean isFullScreen) {
        this.mSurfMgr.mIsFullScreen = isFullScreen;
        if (isFullScreen) {
            WindowManager.LayoutParams attrs = this.mSurfMgr.getWindow().getAttributes();
            attrs.flags |= 1024;
            this.mSurfMgr.getWindow().setAttributes(attrs);
            this.mSurfMgr.getWindow().addFlags(512);
            this.mTitleBar.setVisibility(8);
            this.mToolBar.setVisibility(8);
            this.mFullScreenToolBar.setVisibility(0);
            this.mButtonShowFS.setVisibility(0);
            return;
        }
        WindowManager.LayoutParams attrs2 = this.mSurfMgr.getWindow().getAttributes();
        attrs2.flags &= -1025;
        this.mSurfMgr.getWindow().setAttributes(attrs2);
        this.mSurfMgr.getWindow().clearFlags(512);
        this.mTitleBar.setVisibility(0);
        this.mToolBar.setVisibility(0);
        this.mFullScreenToolBar.setVisibility(8);
    }

    public void showFindDialog() {
        if (this.mFindDialog == null) {
            this.mFindDialog = new FindDialog(this.mSurfMgr);
        }
        SurfWebView webView = this.mWebViewMgr.getCurWebView();
        if (webView != null) {
            this.mFindDialog.setWebView(webView);
            Class<WebView> cls = WebView.class;
            try {
                cls.getMethod("setFindIsUp", Boolean.TYPE).invoke(webView, true);
            } catch (Throwable th) {
            }
        }
        this.mFindDialog.show();
    }

    public void onOptionMenu(String act) {
        SurfWebView webView;
        SurfWebView webView2;
        Log.e(this.LOG_TAG, "onOptionMenu: " + act);
        if (act.equalsIgnoreCase("browser_options_refresh")) {
            Log.d(this.LOG_TAG, "RefreshOption.onClick");
            if (this.mButtonRefresh.getVisibility() == 0) {
                SurfWebView webView3 = this.mWebViewMgr.getCurWebView();
                if (webView3 != null) {
                    String webViewUrl = webView3.getUrl();
                    if (TextUtils.isEmpty(webViewUrl)) {
                        if (this.mItem == null) {
                            webViewUrl = null;
                        } else {
                            webViewUrl = this.mItem.url;
                        }
                    }
                    if (TextUtils.isEmpty(webViewUrl) || webViewUrl.equalsIgnoreCase("about:blank")) {
                        this.mButtonRefresh.setVisibility(8);
                        return;
                    }
                    this.mItem.title = WindowAdapter2.BLANK_URL;
                    this.mButtonRefresh.setVisibility(8);
                    this.mButtonStop.setVisibility(0);
                    this.mButtonStopFS.setVisibility(0);
                    this.mButtonCancel.setVisibility(0);
                    this.mButtonBack.setVisibility(8);
                    if (this.mSurfMgr != null && this.mSurfMgr.mIsFullScreen) {
                        this.mWebProgress.setProgress(0);
                        this.mWebProgress.setVisibility(0);
                    }
                    this.mWebProgressUrl.setProgress(0);
                    this.mWebProgressUrl.setVisibility(0);
                    this.mWebViewMgr.refresh();
                    return;
                }
                this.mButtonRefresh.setVisibility(8);
            }
        } else if (act.equalsIgnoreCase("browser_options_addbookmark")) {
            String title = this.mWebTitle.getText().toString();
            SurfWebView webView4 = this.mWebViewMgr.getCurWebView();
            if (webView4 != null) {
                addBookmark(title, webView4.getUrl(), true);
            }
        } else if (act.equalsIgnoreCase("browser_options_bookmark")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, BookmarkHistoryTabActivity.class), 5);
            }
        } else if (act.equalsIgnoreCase("browser_options_share")) {
            String title2 = this.mWebTitle.getText().toString();
            SurfWebView webView5 = this.mWebViewMgr.getCurWebView();
            if (webView5 != null) {
                sharePage(title2, webView5.getUrl());
            }
        } else if (act.equalsIgnoreCase("browser_options_search")) {
            if (this.mSurfMgr != null) {
                Bundle b = new Bundle();
                b.putBoolean("show_url", false);
                b.putInt("padding_top", 0);
                this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, NaviEditSearchActivity.class).putExtras(b), 1);
            }
        } else if (act.equalsIgnoreCase("browser_options_settings")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.settings();
            }
        } else if (act.equalsIgnoreCase("browser_options_update")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.checkUpgrade();
            }
        } else if (act.equalsIgnoreCase("browser_options_download")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.download();
            }
        } else if (act.equalsIgnoreCase("browser_options_quit")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.showQuitDialog();
            }
        } else if (act.equalsIgnoreCase("browser_options_nightmode")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.showBrightnessAdjust();
            }
        } else if (act.equalsIgnoreCase("browser_options_fullscreen")) {
            setFullScreen(!this.mSurfMgr.mIsFullScreen);
        } else if (act.equalsIgnoreCase("browser_options_flow")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.browseInCurrentWindow("http://go.10086.cn/msb_server/tools.do?query=gprs", -1);
            }
        } else if (act.equalsIgnoreCase("browser_options_lockscreen")) {
            if (this.mSurfMgr != null) {
                this.mSurfMgr.showLockScreen();
            }
        } else if (act.equalsIgnoreCase("browser_options_selecttext")) {
            if (this.mSelectText != null && (webView2 = this.mWebViewMgr.getCurWebView()) != null) {
                this.mSelectText.setWebView(webView2);
                this.mSelectText.startSelectText();
            }
        } else if (act.equalsIgnoreCase("browser_options_searchtext")) {
            showFindDialog();
        } else if (act.equalsIgnoreCase("browser_options_searchtext")) {
            showFindDialog();
        } else if (act.equalsIgnoreCase("browser_options_help") && (webView = this.mWebViewMgr.getCurWebView()) != null) {
            webView.loadUrl("file:///android_asset/html/help.html");
        }
    }

    public void refreshButton(int count) {
        if (count < 1) {
            count = 1;
        } else if (count > 9) {
            count = 9;
        }
        this.mButtonWindow.setImageResource(this.mSurfMgr.mNewWindowIcon[count - 1]);
    }

    public void onDownloadStart(String url) {
    }

    public void onReceivedTitle(String title) {
        setTextInRange(this.mWebTitle, title);
    }

    public void onReceivedUrl(String url) {
    }

    public void sharePage(String shareTitle, String shareUrl) {
        if (!TextUtils.isEmpty(shareUrl)) {
            if (shareTitle == null) {
                shareTitle = WindowAdapter2.BLANK_URL;
            }
            Log.e(this.LOG_TAG, "sharePage: Title - " + shareTitle);
            Log.e(this.LOG_TAG, "shareUrl: Url - " + shareUrl);
            showListDlg(shareTitle, shareUrl);
        }
    }

    public void onPageStarted(String url) {
        Log.e(this.LOG_TAG, "onPageStarted: " + url);
        if (this.mSurfMgr != null && this.mSurfMgr.mIsFullScreen) {
            this.mWebProgress.setProgress(0);
            this.mWebProgress.setVisibility(0);
        }
        this.mWebProgressUrl.setProgress(0);
        this.mWebProgressUrl.setVisibility(0);
        this.mButtonStop.setVisibility(0);
        this.mButtonRefresh.setVisibility(8);
        this.mButtonStopFS.setVisibility(0);
        this.mButtonCancel.setVisibility(0);
        this.mButtonBack.setVisibility(8);
        this.mButtonForward.setEnabled(false);
        setTitle("正在打开页面...");
        if (!url.equalsIgnoreCase("about:blank")) {
            this.mItem.url = url;
        }
        this.mItem.title = url;
    }

    public void onPageFinished(String url, String title, boolean isGoBackEnabled, boolean isGoForwardEnabled) {
        Log.e(this.LOG_TAG, "onPageFinished: url = " + url);
        Log.e(this.LOG_TAG, "onPageFinished: title = " + title);
        Log.e(this.LOG_TAG, "onPageFinished: isGoBackEnabled = " + isGoBackEnabled);
        Log.e(this.LOG_TAG, "onPageFInished: isGoForwardEnabled = " + isGoForwardEnabled);
        onReceivedUrlAndTitle(url, title);
        this.mWebProgress.setVisibility(8);
        this.mWebProgressUrl.setVisibility(8);
        this.mButtonStop.setVisibility(8);
        this.mButtonRefresh.setVisibility(0);
        this.mButtonStopFS.setVisibility(8);
        this.mButtonCancel.setVisibility(8);
        this.mButtonBack.setVisibility(0);
        this.mButtonBack.setEnabled(isGoBackEnabled);
        this.mButtonForward.setEnabled(isGoForwardEnabled);
    }

    public void onProgressChanged(int newProgress) {
        if (this.mSurfMgr != null && this.mSurfMgr.mIsFullScreen) {
            this.mWebProgress.setProgress(newProgress);
        }
        this.mWebProgressUrl.setProgress(newProgress);
    }

    public void onReceivedUrlAndTitle(String url, String title) {
        this.mItem.url = url;
        this.mItem.title = title;
        setTextInRange(this.mWebTitle, title);
        HistoryDB.getInstance(SurfBrowser.getInstance()).updateOrAdd(url, title, Long.valueOf(new Date().getTime()));
    }

    public void onReceivedError(int errorCode, String description, String failingUrl) {
        this.mWebProgress.setVisibility(8);
        this.mWebProgressUrl.setVisibility(8);
        this.mButtonStop.setVisibility(8);
        this.mButtonRefresh.setVisibility(0);
        this.mButtonStopFS.setVisibility(8);
        this.mButtonCancel.setVisibility(8);
        this.mButtonBack.setVisibility(0);
        this.mWebTitle.setText(description);
        Toast.makeText(this.mSurfMgr, description, 0).show();
    }

    public void shareImage(String shareTitle, String shareUrl, String shareSrc, String pageTitle, String pageUrl) {
        if (!TextUtils.isEmpty(shareUrl) && !TextUtils.isEmpty(shareSrc) && !TextUtils.isEmpty(pageUrl)) {
            if (shareTitle == null) {
                shareTitle = WindowAdapter2.BLANK_URL;
            }
            if (pageTitle == null) {
                pageTitle = WindowAdapter2.BLANK_URL;
            }
            Log.e(this.LOG_TAG, "shareImage: shareTitle - " + shareTitle);
            Log.e(this.LOG_TAG, "shareImage: shareUrl - " + shareUrl);
            Log.e(this.LOG_TAG, "shareImage: shareSrc - " + shareSrc);
            Log.e(this.LOG_TAG, "shareImage: pageTitle - " + pageTitle);
            Log.e(this.LOG_TAG, "shareImage: pageUrl - " + pageUrl);
            showImageListDlg(shareTitle, shareUrl, shareSrc, pageTitle, pageUrl);
        }
    }

    public void bringWebViewToFront(WebView webView) {
        if (webView == null) {
            Log.e(this.LOG_TAG, "bringWebViewToFront: webView is an null pointer!");
            return;
        }
        Log.e(this.LOG_TAG, "bringWebViewToFront(): " + webView.getUrl());
        webView.setVisibility(0);
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(-1, -1);
        this.mWebViewContainer.removeAllViews();
        this.mWebViewContainer.addView(webView, params);
    }

    public void onCopyText(String text) {
    }

    public void onSearchText(String text) {
        String key;
        String str;
        String searchURL = null;
        String searchEncode = null;
        Msb msb = SurfManagerActivity.mMsb;
        if (!(msb == null || msb.search == null)) {
            SearchEngines search = msb.search;
            if (search.items != null) {
                int len = search.items.size();
                int i = 0;
                while (true) {
                    if (i >= len) {
                        break;
                    }
                    SearchEngine se = search.items.get(i);
                    if (se.isDefault) {
                        searchURL = se.url;
                        searchEncode = se.encode;
                        break;
                    }
                    i++;
                }
                if (TextUtils.isEmpty(searchURL) && search.items.size() > 0) {
                    searchURL = search.items.get(0).url;
                    searchEncode = search.items.get(0).encode;
                }
            }
        }
        try {
            if (TextUtils.isEmpty(searchEncode)) {
                str = "gbk";
            } else {
                str = searchEncode;
            }
            key = URLEncoder.encode(text, str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            key = text;
        }
        this.mSurfMgr.browseInCurrentWindow(searchURL + key, -1);
    }

    public void onShareText(String text) {
        if (!TextUtils.isEmpty(text)) {
            Browser.sendString(this.mSurfMgr, text);
        }
    }

    public void onAddQuickLink(String title, String url) {
        int width;
        SurfWebView webView = this.mWebViewMgr.getCurWebView();
        if (webView != null) {
            Picture pic = webView.capturePicture();
            Rect r = new Rect();
            webView.getLocalVisibleRect(r);
            int w = r.width();
            int h = r.height();
            if (w > h) {
                width = h;
            } else {
                width = w;
            }
            float density = this.mSurfMgr.getResources().getDisplayMetrics().density;
            float scale = (72.0f * density) / ((float) width);
            Log.d(this.LOG_TAG, "WebView.capturePicture - width=" + w + ", height=" + h + ", scale=" + scale);
            Bitmap bmp = Bitmap.createBitmap((int) (72.0f * density), (int) (72.0f * density), Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bmp);
            canvas.scale(2.0f * scale, 2.0f * scale);
            pic.draw(canvas);
            String filename = md5(webView.getUrl()) + ".jpg";
            try {
                FileOutputStream openFileOutput = this.mSurfMgr.openFileOutput(filename, 0);
                if (bmp.compress(Bitmap.CompressFormat.PNG, 70, openFileOutput)) {
                    openFileOutput.flush();
                    openFileOutput.close();
                }
                openFileOutput.close();
            } catch (FileNotFoundException | IOException e) {
            }
            QuickLinkHelper quickLinkHelper = new QuickLinkHelper(this.mSurfMgr);
            quickLinkHelper.load();
            if (!quickLinkHelper.add(title, URLUtil.guessUrl(url), filename)) {
                Toast.makeText(this.mSurfMgr, (int) R.string.quicklink_list_full, 0).show();
            }
            Log.i(this.LOG_TAG, "Add quicklink with title:" + title + ", url:" + URLUtil.guessUrl(url) + ", image:" + filename);
        }
    }

    private String getNodeValue(Element root, String tagName) {
        Node node;
        Node childNode;
        if (root == null || TextUtils.isEmpty(tagName)) {
            return null;
        }
        NodeList nodeList = root.getElementsByTagName(tagName);
        if (nodeList == null || (node = nodeList.item(0)) == null || (childNode = node.getFirstChild()) == null) {
            return null;
        }
        return childNode.getNodeValue();
    }

    public void parseOMS(String file) {
        if (!TextUtils.isEmpty(file)) {
            try {
                FileInputStream is = new FileInputStream(file);
                if (is != null) {
                    downloadApkFromOMS(is);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void downloadApkFromOMS(InputStream in) {
        Element root;
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in);
            if (!(doc == null || (root = doc.getDocumentElement()) == null)) {
                String apkName = getNodeValue(root, QueryApList.Carriers.NAME);
                String apkType = getNodeValue(root, QueryApList.Carriers.TYPE);
                String apkUrl = getNodeValue(root, "objectURI");
                String apkSize = getNodeValue(root, "size");
                String apkVendor = getNodeValue(root, "vendor");
                String apkDesc = getNodeValue(root, Downloads.COLUMN_DESCRIPTION);
                StringBuilder messageBody = new StringBuilder();
                if (!TextUtils.isEmpty(apkName)) {
                    messageBody.append(this.mSurfMgr.getResources().getString(R.string.oms_name));
                    messageBody.append(apkName);
                    messageBody.append("\n");
                }
                if (!TextUtils.isEmpty(apkVendor)) {
                    messageBody.append(this.mSurfMgr.getResources().getString(R.string.oms_vendor));
                    messageBody.append(apkVendor);
                    messageBody.append("\n");
                }
                if (!TextUtils.isEmpty(apkSize)) {
                    String apkSize2 = Integer.toString(Integer.parseInt(apkSize) / 1024);
                    messageBody.append(this.mSurfMgr.getResources().getString(R.string.oms_size));
                    messageBody.append(apkSize2);
                    messageBody.append("K");
                    messageBody.append("\n");
                }
                if (!TextUtils.isEmpty(apkType)) {
                    messageBody.append(this.mSurfMgr.getResources().getString(R.string.oms_type));
                    messageBody.append(apkType);
                    messageBody.append("\n");
                }
                if (!TextUtils.isEmpty(apkDesc)) {
                    messageBody.append(this.mSurfMgr.getResources().getString(R.string.oms_desc));
                    messageBody.append(apkDesc);
                    messageBody.append("\n");
                }
                if (!TextUtils.isEmpty(apkUrl)) {
                    final String str = apkName;
                    final String str2 = apkUrl;
                    new AlertDialog.Builder(this.mSurfMgr).setTitle((int) R.string.oms_download_confirm).setMessage(messageBody).setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            BrowserViewNew.this.downloadFile(str, str2);
                        }
                    }).setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).show();
                }
            }
        } catch (IOException e) {
        } catch (SAXException e2) {
        } catch (ParserConfigurationException e3) {
        } finally {
        }
    }

    public void downloadAndParseOMS(String url) {
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 30000);
        HttpConnectionParams.setSoTimeout(httpParameters, 30000);
        HttpClient httpClient = new DefaultHttpClient(httpParameters);
        if (SurfManagerActivity.isCMWap()) {
            httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
        }
        try {
            HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                downloadApkFromOMS(httpResponse.getEntity().getContent());
            }
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void onDownloadOMSFile(String url) {
        Message message = new Message();
        message.what = 3;
        Bundle bundle = new Bundle();
        bundle.putString("oms_url", url);
        message.setData(bundle);
        this.mHandler.sendMessage(message);
    }

    public void onParseOMSFile(String filename) {
        Message message = new Message();
        message.what = 4;
        Bundle bundle = new Bundle();
        bundle.putString("oms_file", filename);
        message.setData(bundle);
        this.mHandler.sendMessage(message);
    }

    public void showListDlg(String shareTitle, String shareUrl) {
        if (shareUrl.startsWith(SurfManagerActivity.mMsb.domain)) {
            shareUrl = shareUrl.replace(SurfManagerActivity.mMsb.domain, "http://");
            Log.e(this.LOG_TAG, shareUrl);
        }
        int srcPos = shareUrl.toLowerCase().indexOf(SHAREPAGE_SRC);
        if (srcPos > 0) {
            shareUrl = URLUtil.guessUrl(shareUrl.substring(SHAREPAGE_SRC.length() + srcPos));
            Log.e(this.LOG_TAG, "sharePage - g3src:" + shareUrl);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setDataAndType(Uri.fromParts("file", WindowAdapter2.BLANK_URL, null), "text/plain");
        PackageManager packagemanager = getContext().getPackageManager();
        List<ResolveInfo> list = packagemanager.queryIntentActivities(intent, 65536);
        String[] strArray = new String[(list.size() + 1)];
        ResolveInfo[] ResolveInfoArray = new ResolveInfo[(list.size() + 1)];
        this.mDlgList = new ArrayList<>();
        strArray[0] = getResources().getString(R.string.loveshare);
        HashMap<String, Object> map0 = new HashMap<>();
        map0.put("shareicon", getResources().getDrawable(R.drawable.icon));
        map0.put("sharetext", strArray[0]);
        this.mDlgList.add(map0);
        int i = 0 + 1;
        for (ResolveInfo info : list) {
            strArray[i] = info.activityInfo.loadLabel(packagemanager).toString();
            ResolveInfoArray[i] = info;
            HashMap<String, Object> map = new HashMap<>();
            map.put("shareicon", info.activityInfo.loadIcon(packagemanager));
            map.put("sharetext", info.activityInfo.loadLabel(packagemanager));
            this.mDlgList.add(map);
            i++;
        }
        TasksAdapter tasksAdapter = new TasksAdapter();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.shareway);
        final String str = shareTitle;
        final String str2 = shareUrl;
        final ResolveInfo[] resolveInfoArr = ResolveInfoArray;
        builder.setAdapter(tasksAdapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                if (which == 0) {
                    BrowserViewNew.this.loveShare(str, str2);
                    return;
                }
                String shareContent = BrowserViewNew.this.getResources().getString(R.string.share_url_template);
                if (str != null && str.length() > 0) {
                    shareContent = shareContent + BrowserViewNew.this.getResources().getString(R.string.share_url_template_left) + str + BrowserViewNew.this.getResources().getString(R.string.share_url_template_right);
                }
                Intent intent = new Intent();
                intent.setAction("android.intent.action.SEND");
                intent.setPackage(resolveInfoArr[which].activityInfo.packageName);
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TEXT", shareContent + str2);
                BrowserViewNew.this.mSurfMgr.startActivity(intent);
            }
        });
        builder.show();
    }

    public void showImageListDlg(String shareTitle, String shareUrl, String shareSrc, String pageTitle, String pageUrl) {
        if (pageUrl.startsWith(SurfManagerActivity.mMsb.domain)) {
            pageUrl = pageUrl.replace(SurfManagerActivity.mMsb.domain, "http://");
        }
        int srcPos = pageUrl.toLowerCase().indexOf(SHAREPAGE_SRC);
        if (srcPos > 0) {
            pageUrl = URLUtil.guessUrl(pageUrl.substring(SHAREPAGE_SRC.length() + srcPos));
            Log.e(this.LOG_TAG, "shareImage - g3src:" + pageUrl);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setDataAndType(Uri.fromParts("file", WindowAdapter2.BLANK_URL, null), "text/plain");
        PackageManager packagemanager = getContext().getPackageManager();
        List<ResolveInfo> list = packagemanager.queryIntentActivities(intent, 65536);
        String[] strArray = new String[(list.size() + 1)];
        ResolveInfo[] ResolveInfoArray = new ResolveInfo[(list.size() + 1)];
        this.mDlgList = new ArrayList<>();
        strArray[0] = getResources().getString(R.string.loveshare);
        HashMap<String, Object> map0 = new HashMap<>();
        map0.put("shareicon", getResources().getDrawable(R.drawable.icon));
        map0.put("sharetext", strArray[0]);
        this.mDlgList.add(map0);
        int i = 0 + 1;
        for (ResolveInfo info : list) {
            strArray[i] = info.activityInfo.loadLabel(packagemanager).toString();
            ResolveInfoArray[i] = info;
            HashMap<String, Object> map = new HashMap<>();
            map.put("shareicon", info.activityInfo.loadIcon(packagemanager));
            map.put("sharetext", info.activityInfo.loadLabel(packagemanager));
            this.mDlgList.add(map);
            i++;
        }
        TasksAdapter tasksAdapter = new TasksAdapter();
        final String shareTitletmp = shareTitle;
        final String shareUrltmp = shareUrl;
        final String shareSrctmp = shareSrc;
        final String pageTitletmp = pageTitle;
        final String pageUrltmp = pageUrl;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle((int) R.string.shareway);
        builder.setAdapter(tasksAdapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                if (which == 0) {
                    BrowserViewNew.this.loveImageShare(shareTitletmp, shareUrltmp, shareSrctmp, pageTitletmp, pageUrltmp);
                    return;
                }
                String shareContent = BrowserViewNew.this.getResources().getString(R.string.share_image_template);
                if (!TextUtils.isEmpty(shareTitletmp)) {
                    shareContent = shareContent + BrowserViewNew.this.getResources().getString(R.string.share_url_template_left) + shareTitletmp + BrowserViewNew.this.getResources().getString(R.string.share_url_template_right);
                }
                Intent intent = new Intent("android.intent.action.SEND");
                intent.setType("text/plain");
                intent.putExtra("android.intent.extra.TEXT", shareContent + shareUrltmp);
                BrowserViewNew.this.mSurfMgr.startActivity(Intent.createChooser(intent, BrowserViewNew.this.getResources().getString(R.string.share_type)));
            }
        });
        builder.show();
    }

    public void loveShare(String shareTitle, String shareUrl) {
        Log.e(this.LOG_TAG, "sharePage: Title - " + shareTitle);
        Log.e(this.LOG_TAG, "shareUrl: Url - " + shareUrl);
        if (this.mSurfMgr == null) {
            return;
        }
        if (SurfManagerActivity.mNetworkMgr == null || !SurfManagerActivity.mNetworkMgr.getNetworkType().equalsIgnoreCase("CMWAP") || TextUtils.isEmpty(SurfManagerActivity.mMsb.uid)) {
            if (TextUtils.isEmpty(shareTitle)) {
                shareTitle = shareUrl;
            }
            String webUrl = null;
            if (shareUrl.toString().length() > 0) {
                try {
                    webUrl = "http://go.10086.cn/share/shareImage.do?method=preShareText&share_url=" + URLEncoder.encode(shareUrl.toString(), "UTF-8") + "&" + SHAREIMAGE_TITLE + URLEncoder.encode(shareTitle.toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            Log.e(this.LOG_TAG, "webUrl: " + webUrl);
            this.mWebViewMgr.loadUrl(webUrl, true);
            return;
        }
        Bundle b = new Bundle();
        b.putString("share_title", shareTitle);
        b.putString("share_url", shareUrl);
        this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, SharePageActivity.class).putExtras(b), 8);
    }

    public void loveImageShare(String shareTitle, String shareUrl, String shareSrc, String pageTitle, String pageUrl) {
        if (this.mSurfMgr == null) {
            return;
        }
        if (SurfManagerActivity.mNetworkMgr == null || !SurfManagerActivity.mNetworkMgr.getNetworkType().equalsIgnoreCase("CMWAP") || SurfManagerActivity.mMsb.uid == null || SurfManagerActivity.mMsb.uid.length() <= 0) {
            if (TextUtils.isEmpty(shareTitle)) {
                shareTitle = shareUrl;
            }
            String webUrl = null;
            if (shareUrl.toString().length() > 0) {
                try {
                    webUrl = "http://go.10086.cn/share/shareImage.do?method=preShareImage&share_url=" + URLEncoder.encode(shareUrl.toString(), "UTF-8") + "&" + SHAREIMAGE_TITLE + URLEncoder.encode(shareTitle.toString(), "UTF-8") + "&" + SHAREPAGE_URL + URLEncoder.encode(pageUrl.toString(), "UTF-8") + "&" + SHAREPAGE_TITLE + URLEncoder.encode(pageTitle.toString(), "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            this.mSurfMgr.browseInNewWindow(webUrl, -1);
            return;
        }
        Bundle b = new Bundle();
        b.putString("share_title", shareTitle);
        b.putString("share_url", shareUrl);
        b.putString("share_src", shareSrc);
        b.putString("page_title", pageTitle);
        b.putString("page_url", pageUrl);
        this.mSurfMgr.startActivityForResult(new Intent(this.mSurfMgr, ShareImageActivity.class).putExtras(b), 9);
    }

    class TasksAdapter extends BaseAdapter {
        public int getCount() {
            return BrowserViewNew.this.mDlgList.size();
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int pos, View view, ViewGroup viewgroup) {
            LinearLayout linearlayout = (LinearLayout) view;
            if (view == null) {
                linearlayout = (LinearLayout) LayoutInflater.from(BrowserViewNew.this.getContext()).inflate((int) R.layout.share_item, (ViewGroup) null);
            }
            ((ImageView) linearlayout.findViewById(R.id.shareicon)).setImageDrawable((Drawable) BrowserViewNew.this.mDlgList.get(pos).get("shareicon"));
            ((TextView) linearlayout.findViewById(R.id.sharetext)).setText((String) BrowserViewNew.this.mDlgList.get(pos).get("sharetext"));
            return linearlayout;
        }

        private TasksAdapter() {
        }

        TasksAdapter(BrowserViewNew browserViewNew, View.OnClickListener _pcls1) {
            this();
        }
    }

    public void onUpdateBackAndForward(boolean canGoBack, boolean canGoForward) {
        this.mButtonBack.setEnabled(canGoBack);
        this.mButtonForward.setEnabled(canGoForward);
        if (canGoForward) {
            this.mButtonForward.clearAnimation();
            this.mButtonForward.startAnimation(AnimationUtils.loadAnimation(this.mSurfMgr, R.anim.preload_scale));
        }
    }

    private void showPagePropertyDlg(String title, final String url) {
        View dialogview = LayoutInflater.from(getContext()).inflate((int) R.layout.dialog_pageproperty, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle((int) R.string.page_property);
        ((TextView) dialogview.findViewById(R.id.pagetile)).setText(title);
        ((TextView) dialogview.findViewById(R.id.pageaddress)).setText(url);
        builder.setView(dialogview);
        builder.setPositiveButton((int) R.string.copyaddress, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ClipboardManager clipboardManager = (ClipboardManager) BrowserViewNew.this.getContext().getSystemService("clipboard");
                clipboardManager.setText(url);
                Toast.makeText(BrowserViewNew.this.mSurfMgr, (int) R.string.copysuccessfull, 0).show();
                Log.v("showPagePropertyDlg", "clipboardManager.getText() " + ((Object) clipboardManager.getText()));
            }
        });
        builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        builder.show();
    }
}
