package com.cplatform.android.cmsurfclient.bookmark;

import com.cplatform.android.cmsurfclient.R;

public class BookmarkItem {
    public int icon = R.drawable.icon_listitem_bookmark;
    public long id;
    public String img;
    public String title;
    public String url;

    public BookmarkItem(long id2, String title2, String url2, int icon2) {
        this.id = id2;
        this.title = title2;
        this.url = url2;
        this.img = null;
    }

    public BookmarkItem(long id2, String title2, String url2, String img2) {
        this.id = id2;
        this.img = img2;
        this.title = title2;
        this.url = url2;
    }
}
