package com.cplatform.android.cmsurfclient.urltip;

import android.app.Activity;
import android.view.View;
import com.cplatform.android.cmsurfclient.naviedit.NaviEditUrlActivity;

public class UrlTipItemOnClickListener implements View.OnClickListener {
    public Activity mActivity;
    public UrlTipItem mItem;

    public UrlTipItemOnClickListener(Activity activity, UrlTipItem item) {
        this.mActivity = activity;
        this.mItem = item;
    }

    public void onClick(View v) {
        ((NaviEditUrlActivity) this.mActivity).onClick(this.mItem);
    }
}
