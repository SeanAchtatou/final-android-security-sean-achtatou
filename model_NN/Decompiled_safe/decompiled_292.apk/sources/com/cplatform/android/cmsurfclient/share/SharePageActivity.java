package com.cplatform.android.cmsurfclient.share;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SharePageActivity extends Activity {
    private static final String DEBUG_TAG = "SharePageActivity";
    private static final int MAX_SHARE_USERNUMER = 5;
    private static final int REQUEST_CODE_SELECTUSER = 1;
    private Button mBtnCancel;
    private Button mBtnShare;
    private LinearLayout mBtnShareUser;
    private ImageView mImgShareUser;
    private SharePageAdapter mListAdapter;
    /* access modifiers changed from: private */
    public ArrayList<SharePageItem> mListUser;
    private ShareItemList mListViewTargetUser;
    /* access modifiers changed from: private */
    public ShareHistoryDB mShareDB = null;
    private String mShareTitle;
    /* access modifiers changed from: private */
    public String mShareUrl;
    private TextView mTxtContacts;
    /* access modifiers changed from: private */
    public EditText mTxtShareComment;
    /* access modifiers changed from: private */
    public EditText mTxtShareTitle;
    /* access modifiers changed from: private */
    public TextView mTxtShareUrl;
    /* access modifiers changed from: private */
    public AutoCompleteTextView mTxtShareUser;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.share_page);
        this.mListViewTargetUser = (ShareItemList) findViewById(R.id.share_page_list_targetuser);
        this.mTxtShareTitle = (EditText) findViewById(R.id.share_page_title);
        this.mTxtShareUrl = (TextView) findViewById(R.id.share_page_url);
        this.mTxtShareComment = (EditText) findViewById(R.id.share_page_comment);
        this.mTxtShareUser = (AutoCompleteTextView) findViewById(R.id.share_page_username);
        this.mTxtShareUser.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                }
            }
        });
        this.mImgShareUser = (ImageView) findViewById(R.id.share_page_view_username);
        this.mImgShareUser.setImageResource(17301506);
        this.mImgShareUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharePageActivity.this.mTxtShareUser.showDropDown();
            }
        });
        this.mBtnShareUser = (LinearLayout) findViewById(R.id.share_page_btn_username);
        this.mBtnShareUser.setClickable(true);
        this.mBtnShareUser.setFocusable(true);
        this.mBtnShareUser.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharePageActivity.this.mTxtShareUser.showDropDown();
            }
        });
        this.mShareDB = new ShareHistoryDB(this);
        if (this.mShareDB != null) {
            this.mShareDB.load();
            this.mTxtShareUser.setAdapter(new ArrayAdapter<>(this, 17367050, this.mShareDB.mShareBy));
            this.mTxtShareUser.setThreshold(0);
            if (this.mShareDB.mShareBy.size() > 0) {
                this.mTxtShareUser.setText(this.mShareDB.mShareBy.get(0).toString());
            }
        }
        Bundle b = getIntent().getExtras();
        if (b != null) {
            this.mShareTitle = b.getString("share_title");
            this.mShareUrl = b.getString("share_url");
        }
        this.mTxtShareTitle.setText(this.mShareTitle);
        this.mTxtShareTitle.setSingleLine(true);
        this.mTxtShareUrl.setText(this.mShareUrl);
        this.mTxtShareUrl.setSingleLine(true);
        this.mTxtContacts = (TextView) findViewById(R.id.share_page_contacts);
        this.mTxtContacts.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String users = WindowAdapter2.BLANK_URL;
                for (int i = 0; i < SharePageActivity.this.mListUser.size(); i++) {
                    SharePageItem userItem = (SharePageItem) SharePageActivity.this.mListUser.get(i);
                    String userInfo = userItem.mUserInfo;
                    if (userInfo != null && userInfo.length() > 0) {
                        users = users + userInfo;
                        if (i != SharePageActivity.this.mListUser.size() - 1) {
                            users = users + ";";
                        }
                    } else if (userItem.mTxtInfo != null) {
                        userItem.mUserInfo = userItem.mTxtInfo.getText().toString();
                        users = users + userItem.mUserInfo;
                        if (i != SharePageActivity.this.mListUser.size() - 1) {
                            users = users + ";";
                        }
                    }
                }
                Bundle b = new Bundle();
                b.putString("share_targetusers", users);
                SharePageActivity.this.startActivityForResult(new Intent(SharePageActivity.this, ShareSelectUserActivity.class).putExtras(b), 1);
            }
        });
        this.mBtnShare = (Button) findViewById(R.id.share_page_btn_send);
        this.mBtnShare.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String userName;
                String phoneNumber;
                String shareBy = SharePageActivity.this.mTxtShareUser.getText().toString();
                if (SharePageActivity.this.mShareDB != null && shareBy.length() > 0) {
                    SharePageActivity.this.mShareDB.addShareBy(shareBy);
                }
                String shareTargets = WindowAdapter2.BLANK_URL;
                for (int i = 0; i < SharePageActivity.this.mListUser.size(); i++) {
                    SharePageItem targetUser = (SharePageItem) SharePageActivity.this.mListUser.get(i);
                    if (targetUser != null) {
                        if (targetUser.mUserInfo.length() == 0 && targetUser.mTxtInfo != null) {
                            targetUser.mUserInfo = targetUser.mTxtInfo.getText().toString();
                        }
                        if (targetUser.mUserInfo.length() > 0) {
                            int beginPos = targetUser.mUserInfo.indexOf("(");
                            int endPos = targetUser.mUserInfo.indexOf(")");
                            if (beginPos <= 0 || endPos <= 0) {
                                userName = WindowAdapter2.BLANK_URL;
                                phoneNumber = targetUser.mUserInfo;
                            } else {
                                phoneNumber = targetUser.mUserInfo.substring(0, beginPos);
                                userName = targetUser.mUserInfo.substring(beginPos + 1, endPos);
                            }
                            if (phoneNumber.length() > 0) {
                                shareTargets = shareTargets + phoneNumber;
                                if (i < SharePageActivity.this.mListUser.size() - 1) {
                                    shareTargets = shareTargets + ",";
                                }
                                if (SharePageActivity.this.mShareDB != null) {
                                    SharePageActivity.this.mShareDB.addShareTo(phoneNumber, userName);
                                }
                            }
                        }
                    }
                }
                if (shareTargets.length() > 0) {
                    String params = "uid=";
                    Log.i(SharePageActivity.DEBUG_TAG, "uid=" + SurfManagerActivity.mMsb.uid);
                    if (!TextUtils.isEmpty(SurfManagerActivity.mMsb.uid)) {
                        params = params + SurfManagerActivity.mMsb.uid;
                    }
                    String params2 = ((params + "&sends=") + shareTargets) + "&title=";
                    if (SharePageActivity.this.mTxtShareTitle.getText().toString().length() > 0) {
                        try {
                            params2 = params2 + URLEncoder.encode(SharePageActivity.this.mTxtShareTitle.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                    String params3 = params2 + "&url=";
                    if (SharePageActivity.this.mTxtShareUrl.getText().toString().length() > 0) {
                        try {
                            params3 = params3 + URLEncoder.encode(SharePageActivity.this.mTxtShareUrl.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e2) {
                            e2.printStackTrace();
                        }
                    } else {
                        try {
                            params3 = params3 + URLEncoder.encode(SharePageActivity.this.mShareUrl, "UTF-8");
                        } catch (UnsupportedEncodingException e3) {
                            e3.printStackTrace();
                        }
                    }
                    if (SharePageActivity.this.mTxtShareUser.getText().toString().length() > 0) {
                        params3 = params3 + "&sharer=";
                        try {
                            params3 = params3 + URLEncoder.encode(SharePageActivity.this.mTxtShareUser.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e4) {
                            e4.printStackTrace();
                        }
                    }
                    if (SharePageActivity.this.mTxtShareComment.getText().toString().length() > 0) {
                        params3 = params3 + "&remark=";
                        try {
                            params3 = params3 + URLEncoder.encode(SharePageActivity.this.mTxtShareComment.getText().toString(), "UTF-8");
                        } catch (UnsupportedEncodingException e5) {
                            e5.printStackTrace();
                        }
                    }
                    SharePageActivity.this.setResult(-1, new Intent().setAction(params3 + "&type=1"));
                    SharePageActivity.this.finish();
                    return;
                }
                Toast.makeText(SharePageActivity.this, (int) R.string.ishare_no_friend, 1).show();
            }
        });
        this.mBtnCancel = (Button) findViewById(R.id.share_page_btn_cancel);
        this.mBtnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharePageActivity.this.finish();
            }
        });
        this.mListUser = new ArrayList<>();
        this.mListUser.add(new SharePageItem(WindowAdapter2.BLANK_URL));
        this.mListAdapter = new SharePageAdapter(this, this.mListUser, this.mShareDB.mShareTo, false);
        this.mListViewTargetUser.setAdapter(this.mListAdapter);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != 0) {
            String act = data.getAction();
            switch (requestCode) {
                case 1:
                    if (act != null) {
                        this.mListUser.clear();
                        String[] userArray = act.split(";");
                        for (String userInfo : userArray) {
                            this.mListUser.add(new SharePageItem(userInfo));
                        }
                        this.mListAdapter.notifyDataSetChanged();
                        this.mListViewTargetUser.setAdapter(this.mListAdapter);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public void onRemoveTargetUser(SharePageItem item) {
        this.mListAdapter.remove(item);
        this.mListViewTargetUser.setAdapter(this.mListAdapter);
    }

    public void onAddTargetUser() {
        if (this.mListUser.size() < 5) {
            this.mListAdapter.add(new SharePageItem(WindowAdapter2.BLANK_URL));
            this.mListViewTargetUser.setAdapter(this.mListAdapter);
            return;
        }
        Toast.makeText(this, getResources().getString(R.string.ishare_max_friends_left) + 5 + getResources().getString(R.string.ishare_max_friends_right), 1).show();
    }

    public void onModifyTargetUser(SharePageItem item) {
        for (int i = 0; i < this.mListUser.size(); i++) {
            SharePageItem userItem = this.mListUser.get(i);
            if (userItem.mTag.equals(item.mTag)) {
                userItem.mUserInfo = item.mUserInfo;
                this.mListUser.set(i, userItem);
                this.mListAdapter.notifyDataSetChanged();
                this.mListViewTargetUser.setAdapter(this.mListAdapter);
                return;
            }
        }
    }
}
