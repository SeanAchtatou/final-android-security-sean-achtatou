package com.cplatform.android.cmsurfclient.history;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.bookmark.BookmarkManager;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class HistoryActivity extends ExpandableListActivity {
    private static final int MENU_ADD_FAVORITE = 1;
    private static final int MENU_DELETE = 0;
    public static HistoryDB bmDB = null;
    private final String DEBUG_TAG = "HistoryActivity";
    /* access modifiers changed from: private */
    public List<List<HistoryItem>> childArray;
    private TextView emptyNotify = null;
    /* access modifiers changed from: private */
    public List<String> groupArray;
    ExpandableAdapter historyAdapter = null;
    private BookmarkManager mBookmarkMgr = null;
    private LinearLayout mButtonBack;
    private LinearLayout mButtonClear;
    private SurfManagerActivity mSurfMgr = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.history);
        this.emptyNotify = (TextView) findViewById(R.id.history_empty);
        this.mButtonClear = (LinearLayout) findViewById(R.id.history_clear);
        this.mButtonBack = (LinearLayout) findViewById(R.id.history_back);
        bmDB = HistoryDB.getInstance(this);
        this.groupArray = new ArrayList();
        this.childArray = new ArrayList();
        Date d = new Date();
        Date date = new Date(d.getYear(), d.getMonth(), d.getDate());
        long today = date.getTime();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(5, -1);
        long yestoday = c.getTime().getTime();
        ArrayList<HistoryItem> items = bmDB.load();
        if (items == null || items.size() == 0) {
            this.emptyNotify.setVisibility(0);
            this.mButtonClear.setVisibility(8);
        } else {
            ArrayList<HistoryItem> childOfToday = new ArrayList<>();
            ArrayList<HistoryItem> childOfYestoday = new ArrayList<>();
            ArrayList<HistoryItem> childOfMore = new ArrayList<>();
            Iterator i$ = items.iterator();
            while (i$.hasNext()) {
                HistoryItem item = i$.next();
                if (item.date.longValue() > today) {
                    childOfToday.add(item);
                } else if (item.date.longValue() > yestoday) {
                    childOfYestoday.add(item);
                } else {
                    childOfMore.add(item);
                }
            }
            if (childOfToday.size() > 0) {
                this.groupArray.add(getResources().getString(R.string.history_today));
                this.childArray.add(childOfToday);
            }
            if (childOfYestoday.size() > 0) {
                this.groupArray.add(getResources().getString(R.string.history_yesterday));
                this.childArray.add(childOfYestoday);
            }
            if (childOfMore.size() > 0) {
                this.groupArray.add(getResources().getString(R.string.history_early));
                this.childArray.add(childOfMore);
            }
            this.emptyNotify.setVisibility(8);
            this.mButtonClear.setVisibility(0);
        }
        this.historyAdapter = new ExpandableAdapter(this);
        setListAdapter(this.historyAdapter);
        registerForContextMenu(getExpandableListView());
        this.mBookmarkMgr = new BookmarkManager(this, null);
        getExpandableListView().setGroupIndicator(null);
        getExpandableListView().setChildIndicator(null);
        if (getExpandableListView().getCount() > 0) {
            getExpandableListView().expandGroup(0);
        }
        this.mButtonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HistoryActivity.this.getParent().setResult(-1, new Intent().setAction("none"));
                HistoryActivity.this.finish();
            }
        });
        this.mButtonClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                HistoryActivity.this.comfirmAndDeleteAllHistory();
            }
        });
    }

    public void doAndDeleteAllHistory() {
        HistoryDB.getInstance(this).clear();
        for (List<HistoryItem> lst : this.childArray) {
            lst.clear();
        }
        this.childArray.clear();
        this.groupArray.clear();
        this.historyAdapter.notifyDataSetChanged();
        this.emptyNotify.setVisibility(0);
        this.mButtonClear.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void comfirmAndDeleteAllHistory() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.history_clear);
        builder.setMessage((int) R.string.history_prompt_clear);
        builder.setCancelable(false);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
                HistoryActivity.this.doAndDeleteAllHistory();
            }
        });
        builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }

    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        HistoryItem childItem;
        if (groupPosition < 0 || childPosition < 0 || (childItem = (HistoryItem) this.childArray.get(groupPosition).get(childPosition)) == null) {
            return false;
        }
        getParent().setResult(-1, new Intent().setAction(childItem.url));
        finish();
        return true;
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle((int) R.string.menu);
        int type = ExpandableListView.getPackedPositionType(((ExpandableListView.ExpandableListContextMenuInfo) menuInfo).packedPosition);
        if (type == 1) {
            menu.add(0, 1, 0, (int) R.string.menu_item_addbookmark);
            menu.add(0, 0, 0, (int) R.string.delete);
        } else if (type == 0) {
            menu.add(0, 0, 0, (int) R.string.delete);
        }
    }

    public boolean onContextItemSelected(MenuItem item) {
        ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item.getMenuInfo();
        int type = ExpandableListView.getPackedPositionType(info.packedPosition);
        int groupPos = ExpandableListView.getPackedPositionGroup(info.packedPosition);
        if (type == 1) {
            int childPos = ExpandableListView.getPackedPositionChild(info.packedPosition);
            switch (item.getItemId()) {
                case 0:
                    removeClildData(groupPos, childPos);
                    return true;
                case 1:
                    this.mBookmarkMgr.addBookmark(((HistoryItem) this.childArray.get(groupPos).get(childPos)).title, ((HistoryItem) this.childArray.get(groupPos).get(childPos)).url, false);
                    break;
            }
            return super.onContextItemSelected(item);
        } else if (type != 0) {
            return false;
        } else {
            switch (item.getItemId()) {
                case 0:
                    removeGroupdata(groupPos);
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }
        }
    }

    public class ExpandableAdapter extends BaseExpandableListAdapter {
        Activity activity;

        public ExpandableAdapter(Activity a) {
            this.activity = a;
        }

        public Object getChild(int groupPosition, int childPosition) {
            return ((List) HistoryActivity.this.childArray.get(groupPosition)).get(childPosition);
        }

        public long getChildId(int groupPosition, int childPosition) {
            return (long) childPosition;
        }

        public int getChildrenCount(int groupPosition) {
            return ((List) HistoryActivity.this.childArray.get(groupPosition)).size();
        }

        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View convertView2 = LayoutInflater.from(this.activity).inflate((int) R.layout.history_item, (ViewGroup) null);
            HistoryItem item = (HistoryItem) ((List) HistoryActivity.this.childArray.get(groupPosition)).get(childPosition);
            ((TextView) convertView2.findViewById(R.id.history_title)).setText((item.title == null || WindowAdapter2.BLANK_URL.equals(item.title)) ? item.url : item.title);
            ((TextView) convertView2.findViewById(R.id.history_url)).setText(item.url);
            return convertView2;
        }

        public Object getGroup(int groupPosition) {
            return HistoryActivity.this.groupArray.get(groupPosition);
        }

        public int getGroupCount() {
            return HistoryActivity.this.groupArray.size();
        }

        public long getGroupId(int groupPosition) {
            return (long) groupPosition;
        }

        /* Debug info: failed to restart local var, previous not found, register: 5 */
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String string = (String) HistoryActivity.this.groupArray.get(groupPosition);
            View convertView2 = LayoutInflater.from(this.activity).inflate((int) R.layout.browser_yellowpage_header, (ViewGroup) null);
            if (isExpanded) {
                ((ImageView) convertView2.findViewById(R.id.expand_icon)).setImageResource(R.drawable.daohang_header_icon_shouqi);
            } else {
                ((ImageView) convertView2.findViewById(R.id.expand_icon)).setImageResource(R.drawable.daohang_header_icon_zhankai);
            }
            ((TextView) convertView2.findViewById(R.id.yellowpage_header)).setText(string);
            return convertView2;
        }

        public boolean hasStableIds() {
            return false;
        }

        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void removeGroupdata(int groupPos) {
        HistoryDB.getInstance(this).delGroup(((HistoryItem) this.childArray.get(groupPos).get(0)).date.longValue());
        this.childArray.get(groupPos).clear();
        this.childArray.remove(groupPos);
        this.groupArray.remove(groupPos);
        if (this.groupArray.isEmpty()) {
            this.emptyNotify.setVisibility(0);
            this.mButtonClear.setVisibility(8);
        }
        this.historyAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public void removeClildData(int groupPos, int childPos) {
        HistoryDB.getInstance(this).del(((HistoryItem) this.childArray.get(groupPos).get(childPos)).id);
        List<HistoryItem> tmplist = this.childArray.get(groupPos);
        tmplist.remove(childPos);
        if (tmplist.isEmpty()) {
            this.childArray.remove(groupPos);
            this.groupArray.remove(groupPos);
        }
        if (this.groupArray.isEmpty()) {
            this.emptyNotify.setVisibility(0);
            this.mButtonClear.setVisibility(8);
        }
        this.historyAdapter.notifyDataSetChanged();
    }
}
