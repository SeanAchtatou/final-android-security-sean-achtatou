package com.cplatform.android.cmsurfclient.download.ui;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.download.util.FileNameUtils;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.File;

public class DownloadHelper {

    public static class DownloadConfirmableDialogBuilder extends AlertDialog.Builder {
        public int SelectedItem;

        public DownloadConfirmableDialogBuilder(Context context) {
            super(context);
        }
    }

    public static void confirmAndDeleteDownloadTask(Context context1, Uri download, String meta, String path) {
        final DownloadConfirmableDialogBuilder builder = new DownloadConfirmableDialogBuilder(context1);
        builder.setTitle((int) R.string.app_name);
        builder.SelectedItem = 0;
        builder.setSingleChoiceItems(new CharSequence[]{context1.getResources().getText(R.string.download_delete_task_only), context1.getResources().getText(R.string.download_delete_task_and_file)}, 0, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                builder.SelectedItem = i;
            }
        });
        final Context context = context1;
        final Uri downloadUri = download;
        final String metaUri = meta;
        final String localPath = path;
        builder.setCancelable(false).setPositiveButton((int) R.string.download_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                DownloadHelper.deleteDownloadTask(context, downloadUri, builder.SelectedItem == 1 ? metaUri : null, builder.SelectedItem == 1 ? localPath : null);
                Toast.makeText(context, (int) R.string.download_delete_task_success, 0).show();
                dialoginterface.dismiss();
            }
        }).setNegativeButton((int) R.string.download_do_not_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }

    public static boolean deleteDownloadTask(Context context, Uri uri, String s, String s1) {
        if (!TextUtils.isEmpty(s)) {
            context.getContentResolver().delete(Uri.parse(s), null, null);
        }
        context.getContentResolver().delete(uri, null, null);
        if (TextUtils.isEmpty(s1)) {
            return false;
        }
        try {
            File file = new File(s1);
            if (!file.exists() || file.isDirectory()) {
                return false;
            }
            file.delete();
            return false;
        } catch (SecurityException e) {
            Log.e("download/ui/DownloadHelper", "error", e);
            return false;
        }
    }

    public static void viewDownloadTask(Context context, String info) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("任务详细");
        dialog.setMessage(info);
        dialog.setNegativeButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        dialog.show();
    }

    public static void confirmAndRenameDownloadTask(final Context context, final Uri uri, final String path) {
        final View dialogview = LayoutInflater.from(context).inflate((int) R.layout.download_dialog_rename, (ViewGroup) null);
        EditText fileNameEdit = (EditText) dialogview.findViewById(R.id.file_name);
        String file = path;
        if (file == null) {
            file = WindowAdapter2.BLANK_URL;
        }
        int i = file.lastIndexOf(47);
        if (i >= 0) {
            file = file.substring(i + 1);
        }
        fileNameEdit.setText(file);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle((int) R.string.download_menu_rename);
        builder.setView(dialogview);
        builder.setCancelable(false);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                String localPath;
                String file = ((EditText) dialogview.findViewById(R.id.file_name)).getText().toString().trim();
                if (TextUtils.isEmpty(file)) {
                    Toast.makeText(context, (int) R.string.download_filename_empty, 0).show();
                } else if (!file.matches("^[^\\/\\\\<>\\*\\?\\:\"\\|]+$")) {
                    Toast.makeText(context, (int) R.string.download_filename_invalid, 0).show();
                } else {
                    String localPath2 = path;
                    int pos = localPath2.lastIndexOf(47);
                    if (pos >= 0) {
                        localPath = localPath2.substring(0, pos + 1);
                    } else {
                        localPath = localPath2 + "/";
                    }
                    String localPath3 = localPath + file;
                    try {
                        File f = new File(path);
                        if (!f.exists() || f.isDirectory()) {
                            Toast.makeText(context, (int) R.string.download_file_not_exist, 0).show();
                            dialoginterface.dismiss();
                            return;
                        }
                        File dest = new File(localPath3);
                        if (dest.exists()) {
                            Toast.makeText(context, (int) R.string.download_file_already_exist, 0).show();
                        } else if (dest.isDirectory()) {
                            Toast.makeText(context, (int) R.string.download_filename_invalid, 0).show();
                        } else {
                            if (f.renameTo(dest)) {
                                ContentValues values = new ContentValues();
                                values.put(Downloads._DATA, localPath3);
                                values.put(Downloads.COLUMN_FILE_NAME_HINT, file);
                                values.put(Downloads.COLUMN_TITLE, file);
                                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(FileNameUtils.getFileExtension(file));
                                if (mimeType != null) {
                                    values.put(Downloads.COLUMN_MIME_TYPE, mimeType);
                                }
                                context.getContentResolver().update(uri, values, null, null);
                                Toast.makeText(context, (int) R.string.download_rename_completed, 0).show();
                                dialoginterface.dismiss();
                                return;
                            }
                            Toast.makeText(context, (int) R.string.download_rename_failed, 0).show();
                        }
                    } catch (Exception e) {
                        Log.e("download/ui/DownloadHelper", "error", e);
                    }
                }
            }
        }).setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }
}
