package com.cplatform.android.cmsurfclient.preference;

import android.content.Context;
import android.util.AttributeSet;

class BrowserYesNoPreference extends YesNoPreference {
    public BrowserYesNoPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onDialogClosed(boolean positiveResult) {
        super.onDialogClosed(positiveResult);
        if (positiveResult) {
            setEnabled(false);
            Context context = getContext();
            if (SurfBrowserSettings.PREF_CLEAR_CACHE.equals(getKey())) {
                SurfBrowserSettings.getInstance().clearCache(context);
            } else if (SurfBrowserSettings.PREF_CLEAR_COOKIES.equals(getKey())) {
                SurfBrowserSettings.getInstance().clearCookies(context);
            } else if (SurfBrowserSettings.PREF_CLEAR_HISTORY.equals(getKey())) {
                SurfBrowserSettings.getInstance().clearHistory(context);
            } else if (SurfBrowserSettings.PREF_CLEAR_FORMDATA.equals(getKey())) {
                SurfBrowserSettings.getInstance().clearFormData(context);
            } else if (SurfBrowserSettings.PREF_CLEAR_PASSWORDS.equals(getKey())) {
                SurfBrowserSettings.getInstance().clearPasswords(context);
            } else if (SurfBrowserSettings.PREF_RESET_DEFAULTS.equals(getKey())) {
                SurfBrowserSettings.getInstance().resetDefaultPreferences(context);
                setEnabled(true);
            }
        }
    }
}
