package com.cplatform.android.cmsurfclient.urltip;

public class UrlTipItem {
    public String title;
    public String url;

    public UrlTipItem(String title2, String url2) {
        this.title = title2;
        this.url = url2;
    }
}
