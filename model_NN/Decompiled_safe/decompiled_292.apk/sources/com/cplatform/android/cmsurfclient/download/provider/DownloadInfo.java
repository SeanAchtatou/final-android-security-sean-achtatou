package com.cplatform.android.cmsurfclient.download.provider;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import java.net.URI;

public class DownloadInfo {
    public String mAppointName;
    public String mClass;
    public int mControl;
    public String mCookies;
    public int mCurrentBytes;
    public long mCurrentElapsedTime;
    public int mDestination;
    public String mETag;
    public String mExtras;
    public String mFileName;
    public int mFuzz = Helpers.sRandom.nextInt(1001);
    public volatile boolean mHasActiveThread;
    public String mHint;
    public int mId;
    public String mIfRangeId;
    public long mLastMod;
    public boolean mMediaScanned;
    public String mMimeType;
    public boolean mNoIntegrity;
    public int mNumFailed;
    public String mPackage;
    public int mRedirectCount;
    public String mReferer;
    public int mRetryAfter;
    public int mStatus;
    public String mSubDirectory;
    public int mTotalBytes;
    public String mUri;
    public String mUriDomain;
    public String mUserAgent;
    public int mVisibility;

    public DownloadInfo(int id, String uri, boolean noIntegrity, String hint, String fileName, String mimeType, int destination, int visibility, int control, int status, int numFailed, int retryAfter, int redirectCount, long lastMod, String pckg, String clazz, String extras, String cookies, String userAgent, String referer, int totalBytes, int currentBytes, long currentElapsedTime, String eTag, String ifRangeId, boolean mediaScanned, String subDirectory, String appointName) {
        this.mId = id;
        this.mUri = uri;
        this.mNoIntegrity = noIntegrity;
        this.mHint = hint;
        this.mFileName = fileName;
        this.mMimeType = mimeType;
        this.mDestination = destination;
        this.mVisibility = visibility;
        this.mControl = control;
        this.mStatus = status;
        this.mNumFailed = numFailed;
        this.mRetryAfter = retryAfter;
        this.mRedirectCount = redirectCount;
        this.mLastMod = lastMod;
        this.mPackage = pckg;
        this.mClass = clazz;
        this.mExtras = extras;
        this.mCookies = cookies;
        this.mUserAgent = userAgent;
        this.mReferer = referer;
        this.mTotalBytes = totalBytes;
        this.mCurrentBytes = currentBytes;
        this.mCurrentElapsedTime = currentElapsedTime;
        this.mETag = eTag;
        this.mIfRangeId = ifRangeId;
        this.mMediaScanned = mediaScanned;
        this.mSubDirectory = subDirectory;
        this.mAppointName = appointName;
        this.mUriDomain = getUriDomain(this.mUri);
    }

    public static String getUriDomain(String s) {
        if (TextUtils.isEmpty(s)) {
            return null;
        }
        URI uri = URI.create(s);
        return String.format("%s://%s/", uri.getScheme(), uri.getAuthority());
    }

    public boolean canUseNetwork(boolean available, boolean roaming) {
        if (!available) {
            return false;
        }
        if (this.mDestination == 3) {
            return !roaming;
        }
        return true;
    }

    public boolean hasCompletionNotification() {
        if (!Downloads.isStatusCompleted(this.mStatus)) {
            return false;
        }
        if (this.mVisibility == 1) {
            return true;
        }
        return false;
    }

    public boolean isReadyToStart(long now) {
        if (this.mControl == 1) {
            return false;
        }
        if (this.mStatus == 0) {
            return true;
        }
        if (this.mStatus == 190) {
            return true;
        }
        if (this.mStatus == 192) {
            return true;
        }
        if (this.mStatus == 193) {
            if (this.mNumFailed == 0) {
                return true;
            }
            if (restartTime() < now) {
                return true;
            }
        }
        return false;
    }

    public boolean isReadyToRestart(long now) {
        if (this.mControl == 1) {
            return false;
        }
        if (this.mStatus == 0) {
            return true;
        }
        if (this.mStatus == 190) {
            return true;
        }
        if (this.mStatus == 193) {
            if (this.mNumFailed == 0) {
                return true;
            }
            if (restartTime() < now) {
                return true;
            }
        }
        return false;
    }

    public long restartTime() {
        if (this.mRetryAfter > 0) {
            return this.mLastMod + ((long) this.mRetryAfter);
        }
        return this.mLastMod + ((long) ((this.mFuzz + Constants.MAX_DOWNLOADS) * 30 * (1 << (this.mNumFailed - 1))));
    }

    public void sendIntentIfRequested(Uri uri, Context context) {
        if (this.mPackage != null && this.mClass != null) {
            Intent intent = new Intent(Downloads.ACTION_DOWNLOAD_COMPLETED);
            intent.setClassName(this.mPackage, this.mClass);
            if (this.mExtras != null) {
                intent.putExtra(Downloads.COLUMN_NOTIFICATION_EXTRAS, this.mExtras);
            }
            intent.setData(uri);
            context.sendBroadcast(intent);
        }
    }
}
