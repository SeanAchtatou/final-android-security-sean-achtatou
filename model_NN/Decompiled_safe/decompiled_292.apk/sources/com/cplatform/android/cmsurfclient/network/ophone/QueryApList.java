package com.cplatform.android.cmsurfclient.network.ophone;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class QueryApList {
    protected Context mContext = null;
    protected int[] mListApId = null;
    protected String[] mListApn = null;
    protected String[] mListProxy = null;
    protected int[] mListProxyPort = null;
    protected String[] mListTitle = null;
    protected String[] mListType = null;

    public QueryApList(Context context) {
        this.mContext = context;
        loadApListInfo();
    }

    public int getApSize() {
        if (this.mListTitle == null) {
            return 0;
        }
        return this.mListTitle.length;
    }

    public int getApId(int index) {
        if (this.mListApId == null || index >= this.mListApId.length) {
            return -1;
        }
        return this.mListApId[index];
    }

    public String getApTitle(int index) {
        if (this.mListTitle == null || index >= this.mListTitle.length) {
            return null;
        }
        return this.mListTitle[index];
    }

    public String getApValue(int index) {
        if (this.mListApn == null || index >= this.mListApn.length) {
            return null;
        }
        return this.mListApn[index];
    }

    public String getApType(int index) {
        if (this.mListType == null || index >= this.mListType.length) {
            return null;
        }
        return this.mListType[index];
    }

    public String getApProxy(int index) {
        if (this.mListProxy == null || index >= this.mListProxy.length) {
            return null;
        }
        return this.mListProxy[index];
    }

    public int getApProxyPort(int index) {
        if (this.mListProxyPort == null || index >= this.mListProxyPort.length) {
            return -1;
        }
        return this.mListProxyPort[index];
    }

    public int[] getApId() {
        return this.mListApId;
    }

    public String[] getApTitle() {
        return this.mListTitle;
    }

    public String[] getApValue() {
        return this.mListApn;
    }

    public String[] getApType() {
        return this.mListType;
    }

    /* access modifiers changed from: protected */
    public void loadApListInfo() {
        Cursor c = this.mContext.getContentResolver().query(Carriers.CONTENT_URI, new String[]{Carriers._ID, Carriers.NAME, Carriers.APN, Carriers.TYPE, Carriers.PROXY, Carriers.PORT}, null, null, Carriers.TYPE);
        if (c != null) {
            int count = c.getCount();
            this.mListApId = new int[count];
            this.mListTitle = new String[count];
            this.mListApn = new String[count];
            this.mListType = new String[count];
            this.mListProxy = new String[count];
            this.mListProxyPort = new int[count];
            int i = 0;
            while (c.moveToNext()) {
                try {
                    this.mListApId[i] = c.getInt(0);
                    this.mListTitle[i] = c.getString(1);
                    this.mListApn[i] = c.getString(2);
                    this.mListType[i] = c.getString(3);
                    this.mListProxy[i] = c.getString(4);
                    this.mListProxyPort[i] = c.getInt(5);
                    Log.d("TEST", this.mListTitle[i] + "; " + this.mListApn[i] + "; " + this.mListType[i]);
                    i++;
                } finally {
                    c.close();
                }
            }
        }
    }

    protected static final class Carriers {
        public static final String APN = "apn";
        public static final Uri CONTENT_URI = Uri.parse("content://telephony/carriers");
        public static final String CURRENT = "current";
        public static final String DEFAULT_SORT_ORDER = "name ASC";
        public static final String MCC = "mcc";
        public static final String MMSC = "mmsc";
        public static final String MMSPORT = "mmsport";
        public static final String MMSPROXY = "mmsproxy";
        public static final String MNC = "mnc";
        public static final String NAME = "name";
        public static final String NUMERIC = "numeric";
        public static final String PASSWORD = "password";
        public static final String PORT = "port";
        public static final String PRELOADED = "preloaded";
        public static final String PROXY = "proxy";
        public static final String SERVER = "server";
        public static final String TYPE = "type";
        public static final String USER = "user";
        public static final String VISIBLE = "visible";
        public static final String _ID = "_id";

        protected Carriers() {
        }
    }
}
