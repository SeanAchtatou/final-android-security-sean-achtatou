package com.cplatform.android.cmsurfclient.history;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.preference.SurfBrowserSettings;
import com.cplatform.android.cmsurfclient.urltip.UrlTipItem;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class HistoryDB {
    private static final String DATABASE_NAME = "CMSurfClient";
    private static int MAX_ITEMS = 500;
    private static int MOREDAY = 2;
    private static int MORE_ITEMS_LIMIT = 100;
    private static final String TABLE_HISTORY_NAME = "history";
    private static int TODAY = 0;
    private static int YESTODAY = 1;
    private static HistoryDB instance = null;
    private final String DEBUG_TAG = "HistoryDB";
    private Context context;
    private SQLiteDatabase db = null;
    private boolean isTableCreated = false;
    private int itemCount = 0;

    private void clearIfNeed(long date) {
        if (MAX_ITEMS > 0 && this.itemCount >= MAX_ITEMS + MORE_ITEMS_LIMIT) {
            clear(date);
            this.itemCount = getCount();
        }
    }

    private void clearIfNeed() {
        if (MAX_ITEMS > 0 && this.itemCount >= MAX_ITEMS + MORE_ITEMS_LIMIT) {
            clear(getLastDate());
            this.itemCount = getCount();
        }
    }

    public static HistoryDB getInstance(Context context2) {
        if (instance == null) {
            instance = new HistoryDB(context2);
        }
        return instance;
    }

    public HistoryDB(Context context2) {
        this.context = context2;
        this.itemCount = getCount();
    }

    public boolean openDB() {
        if (this.db == null || !this.db.isOpen()) {
            this.db = this.context.openOrCreateDatabase(DATABASE_NAME, 0, null);
            prepareTable();
        }
        return this.db != null && this.db.isOpen();
    }

    public void closeDB() {
        if (this.db != null) {
            if (this.db.isOpen()) {
                this.db.close();
            }
            this.db = null;
        }
    }

    private void prepareTable() {
        if (!this.isTableCreated) {
            StringBuffer sql = new StringBuffer(100);
            sql.append("create table ").append(TABLE_HISTORY_NAME).append("(_id integer primary key autoincrement, title text, url text, visitdate long);");
            try {
                this.db.execSQL(sql.toString());
            } catch (Exception e) {
            }
            this.isTableCreated = true;
        }
    }

    public ArrayList<HistoryItem> load() {
        ArrayList<HistoryItem> items = new ArrayList<>();
        long date = 0;
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_HISTORY_NAME, new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, "url", "visitdate"}, null, null, null, null, "visitdate desc");
            cur.moveToFirst();
            int count = 0;
            while (!cur.isAfterLast()) {
                int id = cur.getInt(0);
                date = cur.getLong(3);
                items.add(new HistoryItem((long) id, cur.getString(1), cur.getString(2), Long.valueOf(date)));
                cur.moveToNext();
                count++;
                if (MAX_ITEMS > 0 && count >= MAX_ITEMS) {
                    break;
                }
            }
            cur.close();
            closeDB();
        }
        clearIfNeed(date);
        return items;
    }

    public long getLastDate() {
        long date = 0;
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_HISTORY_NAME, new String[]{"visitdate"}, null, null, null, null, "visitdate desc");
            cur.moveToFirst();
            int count = 0;
            while (!cur.isAfterLast()) {
                date = cur.getLong(0);
                cur.moveToNext();
                count++;
                if (MAX_ITEMS > 0 && count >= MAX_ITEMS) {
                    break;
                }
            }
            cur.close();
            closeDB();
        }
        return date;
    }

    public ArrayList<UrlTipItem> find(String url, int max) {
        if (url != null) {
            url = url.trim();
        }
        if (WindowAdapter2.BLANK_URL.equals(url)) {
            url = null;
        }
        ArrayList<UrlTipItem> items = new ArrayList<>();
        try {
            if (openDB()) {
                int count = 0;
                if (url != null) {
                    Cursor cur = this.db.query(TABLE_HISTORY_NAME, new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, "url", "visitdate"}, " url like ? ", new String[]{url + "%"}, null, null, "visitdate desc");
                    cur.moveToFirst();
                    count = 0;
                    while (!cur.isAfterLast()) {
                        items.add(new UrlTipItem(cur.getString(1), cur.getString(2)));
                        cur.moveToNext();
                        count++;
                        if (max > 0 && count >= max) {
                            break;
                        }
                    }
                    cur.close();
                }
                if (count < 1) {
                    int count2 = 0;
                    Cursor cur2 = this.db.query(TABLE_HISTORY_NAME, new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, "url", "visitdate"}, null, null, null, null, "visitdate desc");
                    cur2.moveToFirst();
                    while (!cur2.isAfterLast()) {
                        items.add(new UrlTipItem(cur2.getString(1), cur2.getString(2)));
                        cur2.moveToNext();
                        count2++;
                        if (max > 0 && count2 >= max) {
                            break;
                        }
                    }
                    cur2.close();
                }
                closeDB();
            }
        } catch (Exception e) {
        }
        return items;
    }

    public boolean clear(long visitdate) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_HISTORY_NAME);
                sql.append(" where visitdate<").append(visitdate).append(";");
                this.db.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean clear() {
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("DROP TABLE IF EXISTS ").append(TABLE_HISTORY_NAME);
                this.db.execSQL(sql.toString());
                this.itemCount = 0;
            } catch (Exception e) {
            }
            closeDB();
        }
        this.isTableCreated = false;
        return false;
    }

    public long add(String url, String title, Long visitdate) {
        long id = 0;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put(Downloads.COLUMN_TITLE, title);
                values.put("url", url);
                values.put("visitdate", visitdate);
                id = this.db.insert(TABLE_HISTORY_NAME, null, values);
                if (id > 0) {
                    this.itemCount++;
                }
            } catch (Exception e) {
            }
            closeDB();
        }
        clearIfNeed();
        return id;
    }

    public void updateOrAdd(String url, String title, Long visitdate) {
        if (!SurfBrowserSettings.getInstance().isPrivacyBrowsing() && update(url, title, visitdate) <= 0) {
            add(url, title, visitdate);
        }
    }

    public int update(String url, String title, Long visitdate) {
        int rows = 0;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put(Downloads.COLUMN_TITLE, title);
                values.put("url", url);
                values.put("visitdate", visitdate);
                rows = this.db.update(TABLE_HISTORY_NAME, values, " (url='" + url + "') ", null);
            } catch (Exception e) {
            }
            closeDB();
        }
        return rows;
    }

    public int getCount() {
        int count = 0;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("select count(*) from ").append(TABLE_HISTORY_NAME);
                Cursor cur = this.db.rawQuery(sql.toString(), null);
                if (cur != null) {
                    cur.moveToFirst();
                    if (!cur.isAfterLast()) {
                        count = cur.getInt(0);
                    }
                    cur.close();
                }
            } catch (Exception e) {
            }
            closeDB();
        }
        return count;
    }

    public boolean del(long id) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_HISTORY_NAME);
                sql.append(" where _id=").append(id).append(";");
                this.db.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean delGroup(long whichday) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                Date d = new Date();
                Date d2 = new Date(d.getYear(), d.getMonth(), d.getDate());
                long today = d2.getTime();
                Calendar c = Calendar.getInstance();
                c.setTime(d2);
                c.add(5, -1);
                long yestoday = c.getTime().getTime();
                if (whichday > today) {
                    sql.append("delete from ").append(TABLE_HISTORY_NAME);
                    sql.append(" where visitdate>").append(today).append(";");
                } else if (whichday > yestoday) {
                    sql.append("delete from ").append(TABLE_HISTORY_NAME);
                    sql.append(" where visitdate<").append(today).append(" and visitdate >").append(yestoday).append(";");
                } else {
                    sql.append("delete from ").append(TABLE_HISTORY_NAME);
                    sql.append(" where visitdate<").append(yestoday).append(";");
                }
                this.db.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }
}
