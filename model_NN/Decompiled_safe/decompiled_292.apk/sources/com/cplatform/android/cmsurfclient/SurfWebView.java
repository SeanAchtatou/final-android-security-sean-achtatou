package com.cplatform.android.cmsurfclient;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.net.http.SslError;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.webkit.CacheManager;
import android.webkit.HttpAuthHandler;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ZoomButtonsController;
import com.cplatform.android.cmsurfclient.preference.SurfBrowserSettings;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import com.cplatform.android.cmsurfclient.wml2html.PageDataNew;
import com.cplatform.android.cmsurfclient.wml2html.Wml2Html;
import com.cplatform.android.utils.ReflectUtil;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SurfWebView extends WebView {
    static final String ANCHOR_PATTERN = "(?i)<a([^>]+)>([\\s\\S]*?)</a>";
    static final String HREF_PATTERN = "(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))";
    static final String LOG_TAG = SurfWebView.class.getSimpleName();
    static final int MESSAGE_SHOW = 1;
    static final String MIMETYPE_HTML = "text/html";
    static final String MIMETYPE_TEXT = "text/plain";
    static final String MIMETYPE_WML = "text/vnd.wap.wml";
    static final String MIMETYPE_WMLC = "application/vnd.wap.wmlc";
    static final String MIMETYPE_XHTML = "application/xhtml+xml";
    static final String NEXT_PATTERN = "(?i)下\\s*一*\\s*[页章节张]";
    static final String SCHEME_HTTP = "http://";
    static final String SCHEME_HTTPS = "https://";
    static final String SCHEME_RTSP = "rtsp://";
    static final String SCHEME_WTAI = "wtai://wp/";
    static final String SCHEME_WTAI_AP = "wtai://wp/ap;";
    static final String SCHEME_WTAI_MC = "wtai://wp/mc;";
    static final String SCHEME_WTAI_SD = "wtai://wp/sd;";
    static final String SHAREIMAGE_TAG = "shareimage.do";
    public static final Object[] args = new Object[3];
    /* access modifiers changed from: private */
    public static String mPageDataTitle = WindowAdapter2.BLANK_URL;
    /* access modifiers changed from: private */
    public static String mPageDataUrl = WindowAdapter2.BLANK_URL;
    static Pattern mPatternAnchor = Pattern.compile(ANCHOR_PATTERN, 10);
    static Pattern mPatternHref = Pattern.compile(HREF_PATTERN, 10);
    static Pattern mPatternNext = Pattern.compile(NEXT_PATTERN, 10);
    public static final Class[] types = {Integer.TYPE, Integer.TYPE, Boolean.TYPE};
    /* access modifiers changed from: private */
    public Handler hander = new Handler();
    private boolean mBrowseInCache = false;
    /* access modifiers changed from: private */
    public Context mContext = null;
    /* access modifiers changed from: private */
    public String mFailUrl;
    private GestureDetector mGestureDetector;
    /* access modifiers changed from: private */
    public boolean mGoBack = false;
    /* access modifiers changed from: private */
    public int mID = -1;
    private boolean mIsLogClickShow = false;
    private boolean mIsPreload = false;
    /* access modifiers changed from: private */
    public ISurfWebView mListener = null;
    private Handler mMainHandler = null;
    public PageDataNew mPageData = null;
    /* access modifiers changed from: private */
    public String mPreloadUrl = WindowAdapter2.BLANK_URL;
    private String mSelectedText = WindowAdapter2.BLANK_URL;
    /* access modifiers changed from: private */
    public String mStartedUrl = WindowAdapter2.BLANK_URL;
    private String mSuspendedUrl = null;
    private WebChromeClient mWebChromeClient = null;
    private WebViewClient mWebViewClient = null;

    public boolean isLongClickShow() {
        return this.mIsLogClickShow;
    }

    public void setIsLongClickShow(boolean isLogClickShow) {
        this.mIsLogClickShow = isLogClickShow;
    }

    public boolean isPreload() {
        return this.mIsPreload;
    }

    public void setPreload(boolean isPreload) {
        this.mIsPreload = isPreload;
    }

    public SurfWebView(Context context) {
        super(context);
        init(context);
    }

    public SurfWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SurfWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        try {
            super.onWindowFocusChanged(hasWindowFocus);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        try {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
        } catch (Exception e) {
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        requestFocus();
        try {
            return super.onTouchEvent(event) | this.mGestureDetector.onTouchEvent(event);
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void init(Context context) {
        this.mContext = context;
        this.mPageData = new PageDataNew(this);
        addJavascriptInterface(this, "PageData");
        this.mWebViewClient = new SurfWebViewClient();
        addJavascriptInterface(new runJavaScript(), "erropage");
        setWebViewClient(this.mWebViewClient);
        this.mWebChromeClient = new SurfWebChromeClient();
        setWebChromeClient(this.mWebChromeClient);
        this.mGestureDetector = new GestureDetector(new WebViewOnGestureListener());
        this.mMainHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        SurfWebView.this.setVisibility(0);
                        Bundle data = msg.getData();
                        if (data != null) {
                            String url = data.getString("pagedata_url");
                            if (SurfWebView.this.mListener != null && !TextUtils.isEmpty(url)) {
                                SurfWebView.this.mListener.onPageFinished(SurfWebView.this, url, WindowAdapter2.BLANK_URL);
                                return;
                            }
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
    }

    public void setID(int id) {
        this.mID = id;
    }

    public void setListener(ISurfWebView listener) {
        this.mListener = listener;
    }

    public void loadUrlInCache(int steps) {
        Log.e(LOG_TAG + this.mID, "loadUrlInCache: steps = " + steps);
        this.mBrowseInCache = true;
        this.mGoBack = steps < 0;
        stopLoading();
        goBackOrForward(steps);
    }

    public boolean isBrowseInCache() {
        return this.mBrowseInCache;
    }

    public String getPreloadUrl() {
        return this.mPreloadUrl;
    }

    public void cancelHighLightText() {
        ReflectUtil.setDeclaredField(WebView.class, this, "mExtendSelection", false);
        ReflectUtil.setDeclaredField(WebView.class, this, "mShiftIsPressed", false);
    }

    public void highLightText(int x1, int y1, int x2, int y2) {
        ReflectUtil.setDeclaredField(WebView.class, this, "mExtendSelection", false);
        ReflectUtil.setDeclaredField(WebView.class, this, "mShiftIsPressed", true);
        float scale = getScale();
        int scrollX = getScrollX();
        int scrollY = getScrollY();
        moveSelectedText((int) (((float) (x1 + scrollX)) / scale), (int) (((float) (y1 + scrollY)) / scale), false);
        moveSelectedText((int) (((float) (x2 + scrollX)) / scale), (int) (((float) (y2 + scrollY)) / scale), true);
        invalidate();
    }

    private void moveSelectedText(int x, int y, boolean flag) {
        Object region;
        ReflectUtil.setDeclaredField(WebView.class, this, "mSelectX", Integer.valueOf(x));
        ReflectUtil.setDeclaredField(WebView.class, this, "mSelectY", Integer.valueOf(y));
        Object[] aObjs = args;
        aObjs[0] = Integer.valueOf(x);
        aObjs[1] = Integer.valueOf(y);
        aObjs[2] = Boolean.valueOf(flag);
        ReflectUtil.invokeDeclaredMethod(WebView.class, this, "nativeMoveSelection", aObjs, types);
        if (!flag) {
            ReflectUtil.setDeclaredField(WebView.class, this, "mTouchSelection", true);
            ReflectUtil.setDeclaredField(WebView.class, this, "mExtendSelection", true);
            if (Build.VERSION.SDK_INT >= 8 && (region = ReflectUtil.invokeDeclaredMethodNoParams(WebView.class, this, "nativeGetSelection")) != null) {
                this.mSelectedText = region.toString();
                Log.e(LOG_TAG + this.mID, "moveSelectedText: " + this.mSelectedText);
                return;
            }
            return;
        }
        invalidate();
    }

    public void copyHighlightedText() {
        if (Build.VERSION.SDK_INT >= 8) {
            ReflectUtil.setDeclaredField(WebView.class, this, "mShiftIsPressed", false);
            ((ClipboardManager) getContext().getSystemService("clipboard")).setText(this.mSelectedText);
            return;
        }
        ReflectUtil.invokeDeclaredMethodNoParams(WebView.class, this, "commitCopy");
    }

    public void onNetworkStatusChanged(int networkStatus) {
        Log.e(LOG_TAG, "onNetworkStatusChanged: network status = " + networkStatus);
        switch (networkStatus) {
            case 2:
                if (!TextUtils.isEmpty(this.mSuspendedUrl)) {
                    Log.w(LOG_TAG, "loading suspended url: " + this.mSuspendedUrl);
                    loadUrl(this.mSuspendedUrl);
                    this.mSuspendedUrl = null;
                    return;
                }
                return;
            case 3:
                if (!TextUtils.isEmpty(this.mSuspendedUrl)) {
                    this.mFailUrl = this.mSuspendedUrl;
                    Log.w(LOG_TAG, "load error.html with mFailUrl=" + this.mFailUrl);
                    loadUrl("file:///android_asset/html/error.html");
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void loadUrlWithInitialScale(String url, boolean setInitialScale, double scale) {
        Log.e(LOG_TAG + this.mID, "loadUrlWithInitialScale()");
        Log.w(LOG_TAG + this.mID, "url = " + url);
        Log.w(LOG_TAG + this.mID, "setInitialScale = " + setInitialScale);
        Log.w(LOG_TAG + this.mID, "scale = " + scale);
        this.mBrowseInCache = false;
        this.mGoBack = false;
        stopLoading();
        clearView();
        if (SurfBrowserSettings.getInstance().isRememberZoomLevel() && setInitialScale) {
            setInitialScale((int) (scale * 100.0d));
        } else if (scale != 1.0d) {
            setInitialScale((int) (100.0d / scale));
            scrollTo(0, 0);
        }
        switch (((SurfManagerActivity) this.mContext).getNetworkStatus()) {
            case 1:
                Log.w(LOG_TAG, "network is connecting, url saved and wait for network connected...");
                this.mSuspendedUrl = url;
                if (this.mListener != null) {
                    this.mListener.onReceivedTitle(this, getResources().getString(R.string.openingnetwork), true);
                    return;
                }
                return;
            case 2:
                Log.w(LOG_TAG, "network is connected, load url now...");
                this.mSuspendedUrl = null;
                if (this.mListener != null) {
                    this.mListener.onReceivedTitle(this, getResources().getString(R.string.openingpage), true);
                }
                loadUrl(url);
                return;
            case 3:
                Log.w(LOG_TAG, "network is disconnected, url saved and wait for network connected...");
                this.mSuspendedUrl = url;
                if (this.mListener != null) {
                    this.mListener.onReceivedTitle(this, getResources().getString(R.string.openingnetwork), true);
                }
                SurfManagerActivity.mNetworkMgr.connect();
                return;
            default:
                Log.w(LOG_TAG, "network is in unknown state, discard it...");
                return;
        }
    }

    private class ZoomListener implements ZoomButtonsController.OnZoomListener {
        public void onVisibilityChanged(boolean flag) {
            SurfWebView.this.getZoomBtnControls().getZoomControls().setVisibility(8);
        }

        public void onZoom(boolean flag) {
        }

        ZoomListener() {
        }
    }

    /* access modifiers changed from: private */
    public ZoomButtonsController getZoomBtnControls() {
        return (ZoomButtonsController) ReflectUtil.invokeMethodNoParams(WebView.class, this, "getZoomButtonsController");
    }

    public void hideZoomControls() {
        ZoomButtonsController zoomButtonsController = getZoomBtnControls();
        if (zoomButtonsController != null) {
            zoomButtonsController.setOnZoomListener(new ZoomListener());
        }
    }

    public void zoomTo(float zoomLevel) {
        Log.e(LOG_TAG, "zoomTo: " + zoomLevel);
        if (Build.VERSION.SDK_INT >= 8) {
            Class[] aClass = {Float.TYPE, Boolean.TYPE};
            ReflectUtil.invokeDeclaredMethod(WebView.class, this, "zoomWithPreview", new Object[]{Float.valueOf(zoomLevel), true}, aClass);
            return;
        }
        Class[] aClass2 = {Float.TYPE};
        ReflectUtil.invokeDeclaredMethod(WebView.class, this, "zoomWithPreview", new Object[]{Float.valueOf(zoomLevel)}, aClass2);
    }

    public PageDataNew getPageData() {
        return this.mPageData;
    }

    public void onPageData(String text, String html) {
        boolean z;
        boolean z2;
        Log.e(LOG_TAG + this.mID, "onPageData");
        Log.d(LOG_TAG + this.mID, "Try to find WML tag in page data...");
        Matcher match = Pattern.compile("<(card)[^>]*>([\\s\\S]+)<\\/\\1>", 2).matcher(text);
        if (match == null || !match.find()) {
            Log.d(LOG_TAG + this.mID, "No WML tag found");
            findAndLoadNextPageFromData(mPageDataUrl, html);
            if (this.mMainHandler != null) {
                if (!TextUtils.isEmpty(mPageDataTitle)) {
                    this.mMainHandler.sendEmptyMessage(1);
                } else {
                    Message message = new Message();
                    message.what = 1;
                    Bundle bundle = new Bundle();
                    bundle.putString("pagedata_url", mPageDataUrl);
                    message.setData(bundle);
                    this.mMainHandler.sendMessage(message);
                }
                Log.e(LOG_TAG, "Send show message to main handler...");
                return;
            }
            return;
        }
        Log.d(LOG_TAG + this.mID, "WML tag found in page data, convert from WML to HTML format");
        try {
            URL u = new URL(mPageDataUrl);
            Wml2Html wml2Html = new Wml2Html();
            String host = u.getHost();
            String path = u.getPath();
            if (!this.mGoBack) {
                z = true;
            } else {
                z = false;
            }
            String strHtml = wml2Html.parse(text, host, path, z);
            String str = LOG_TAG + this.mID;
            StringBuilder append = new StringBuilder().append("host = ").append(u.getHost()).append(", path = ").append(u.getPath()).append(", forward = ");
            if (!this.mGoBack) {
                z2 = true;
            } else {
                z2 = false;
            }
            Log.i(str, append.append(z2).toString());
            Log.d(LOG_TAG + this.mID, "scale = " + getScale());
            if (((double) getScale()) < 1.25d) {
                setInitialScale(125);
            }
            clearView();
            Log.d(LOG_TAG + this.mID, "Load converted HTML data in current WebView");
            loadDataWithBaseURL(null, strHtml, MIMETYPE_HTML, "utf-8", null);
            findAndLoadNextPageFromData(mPageDataUrl, strHtml);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isNeedPreload(String url) {
        Log.e(LOG_TAG + this.mID, "isNeedPreload: url = " + url);
        if (url.equalsIgnoreCase("about:blank")) {
            return false;
        }
        Log.i(LOG_TAG + this.mID, "isNeedPreload: mBrowseInCache = " + this.mBrowseInCache);
        if (this.mListener == null) {
            return false;
        }
        boolean isHistoryTail = this.mListener.isHistoryTail(url);
        Log.i(LOG_TAG + this.mID, "isNeedPreload: mListener.isHistoryTail = " + isHistoryTail);
        return isHistoryTail && SurfBrowserSettings.getInstance().isPreload();
    }

    /* access modifiers changed from: protected */
    public void findAndLoadNextPageFromTitle(String url, String title) {
        Log.e(LOG_TAG + this.mID, "findAndLoadNextPageFromTitle()");
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(title)) {
            Log.d(LOG_TAG + this.mID, "url or title is null or empty");
        } else if (!isNeedPreload(url)) {
        } else {
            if (url.toLowerCase().startsWith(SurfManagerActivity.mMsb.domain)) {
                Matcher match = Pattern.compile("\\[([0-9]+)\\/([0-9]+)\\]").matcher(title);
                if (match.find()) {
                    Log.d(LOG_TAG + this.mID, "Matched next page pattern found in title");
                    int curPage = Integer.parseInt(match.group(1));
                    int totalPage = Integer.parseInt(match.group(2));
                    Log.d(LOG_TAG + this.mID, "current page = " + curPage + ", total page = " + totalPage);
                    if (curPage > 0 && curPage < totalPage) {
                        String nextPage = WindowAdapter2.BLANK_URL;
                        if (curPage > 1) {
                            if (url.contains("__ca__=%3Frt=me%26pn=" + curPage)) {
                                nextPage = url.replace("__ca__=%3Frt=me%26pn=" + curPage, "__ca__=%3Frt=me%26pn=" + (curPage + 1));
                            }
                        } else if (!url.contains("__ca__=%3Frt=me%26pn=")) {
                            nextPage = !url.contains("?") ? url + "?" + "__ca__=%3Frt=me%26pn=" + (curPage + 1) : url + "&" + "__ca__=%3Frt=me%26pn=" + (curPage + 1);
                        }
                        if (this.mListener == null || !this.mListener.isCurrent(this) || TextUtils.isEmpty(nextPage)) {
                            this.mPreloadUrl = nextPage;
                        } else {
                            this.mListener.onPreloadUrl(nextPage);
                        }
                    }
                }
            } else {
                Log.d(LOG_TAG + this.mID, "Only web page adapted by WEBGW can be preloaded in this method");
            }
        }
    }

    public String md5(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(input.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return WindowAdapter2.BLANK_URL;
        }
    }

    /* access modifiers changed from: protected */
    public void findAndLoadNextPageFromData(String url, String buffer) {
        Log.e(LOG_TAG + this.mID, "findAndLoadNextPageFromData()");
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(buffer)) {
            Log.d(LOG_TAG + this.mID, "url or buffer is null or empty");
        } else if (!isNeedPreload(url)) {
        } else {
            if (url.toLowerCase().contains(SHAREIMAGE_TAG)) {
                Log.d(LOG_TAG + this.mID, "iShare url found, do not preload");
                return;
            }
            new FindPreloadUrlTask().execute(url, buffer);
        }
    }

    /* access modifiers changed from: private */
    public String findPreloadUrl(String url, String buffer) {
        String preloadUrl = WindowAdapter2.BLANK_URL;
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(buffer)) {
            return preloadUrl;
        }
        Matcher matchAnchor = mPatternAnchor.matcher(buffer);
        while (matchAnchor.find()) {
            String anchor = matchAnchor.group(0);
            if (!anchor.toLowerCase().contains("onclick") && findNextKeyword(matchAnchor.group(2))) {
                Log.e(LOG_TAG + this.mID, "Matched next page pattern found: " + anchor);
                preloadUrl = findHref(url, matchAnchor.group(0));
                if (!TextUtils.isEmpty(preloadUrl)) {
                    break;
                }
            }
        }
        if (TextUtils.isEmpty(preloadUrl)) {
            Log.e(LOG_TAG + this.mID, "No matched next page pattern found");
        }
        return preloadUrl;
    }

    private boolean findNextKeyword(String buffer) {
        if (TextUtils.isEmpty(buffer)) {
            return false;
        }
        return mPatternNext.matcher(buffer).find();
    }

    private String findHref(String url, String buffer) {
        String nextUrl = WindowAdapter2.BLANK_URL;
        if (TextUtils.isEmpty(url) || TextUtils.isEmpty(buffer)) {
            return nextUrl;
        }
        Log.d(LOG_TAG + this.mID, "findHref: buffer = " + buffer);
        if (!TextUtils.isEmpty(buffer)) {
            Matcher matchHref = mPatternHref.matcher(buffer);
            if (matchHref.find()) {
                Log.d(LOG_TAG + this.mID, "href = " + matchHref.group(0));
                nextUrl = matchHref.group(1).replaceAll("\"", WindowAdapter2.BLANK_URL).replaceAll("'", WindowAdapter2.BLANK_URL).replaceAll("&amp;", "&");
                Log.d(LOG_TAG + this.mID, "matched next url = " + nextUrl);
                if (!nextUrl.startsWith("http:")) {
                    try {
                        try {
                            nextUrl = new URI(url).resolve(nextUrl).toURL().toString();
                            Log.e(LOG_TAG + this.mID, "append base url to next url: " + nextUrl);
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                            nextUrl = WindowAdapter2.BLANK_URL;
                        }
                    } catch (URISyntaxException e2) {
                        e2.printStackTrace();
                        nextUrl = WindowAdapter2.BLANK_URL;
                    }
                }
            } else {
                Log.d(LOG_TAG + this.mID, "no matched next url found");
            }
        }
        return nextUrl;
    }

    /* access modifiers changed from: private */
    public void tryLoadUrl(String url) {
        if (url.equals(this.mStartedUrl)) {
            Log.i(LOG_TAG + this.mID, "same url with last onPageStarted, load directly in current webview");
            if (this.mListener != null) {
                this.mListener.onUpdateVisitedHistory(this, url);
            }
            clearView();
            loadUrl(url);
        } else if (this.mListener != null) {
            this.mListener.onLoadUrl(url);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0027 A[LOOP:0: B:3:0x0027->B:6:0x0037, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void zoomAuto() {
        /*
            r6 = this;
            float r2 = r6.getScale()
            double r0 = (double) r2
            java.lang.String r2 = com.cplatform.android.cmsurfclient.SurfWebView.LOG_TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "WebView scale before double tap: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            android.util.Log.e(r2, r3)
            r2 = 4608308318706860032(0x3ff4000000000000, double:1.25)
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0027
            r6.zoomIn()
        L_0x0026:
            return
        L_0x0027:
            float r2 = r6.getScale()
            double r2 = (double) r2
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0026
            boolean r2 = r6.zoomOut()
            r3 = 1
            if (r2 == r3) goto L_0x0027
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.SurfWebView.zoomAuto():void");
    }

    public class SurfWebViewClient extends WebViewClient {
        public SurfWebViewClient() {
        }

        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "doUpdateVisitedHistory: url=" + url + ", isReload=" + isReload);
            super.doUpdateVisitedHistory(view, url, isReload);
        }

        public void onLoadResource(WebView view, String url) {
            Log.w(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onLoadResource: " + url);
            super.onLoadResource(view, url);
        }

        public void onScaleChanged(WebView view, float oldScale, float newScale) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onScaleChanged: oldScale=" + oldScale + ", newScale=" + newScale);
            super.onScaleChanged(view, oldScale, newScale);
        }

        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onReceivedHttpAuthRequest: host=" + host + ", realm=" + realm);
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
        }

        public void onUnhandledKeyEvent(WebView view, KeyEvent event) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onUnhandledKeyEvent: event=" + event.toString());
            super.onUnhandledKeyEvent(view, event);
        }

        public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "shouldOverrideKeyEvent: event=" + event.toString());
            return super.shouldOverrideKeyEvent(view, event);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "shouldOverrideUrlLoading: " + url);
            if (url.startsWith(SurfWebView.SCHEME_WTAI)) {
                if (url.startsWith(SurfWebView.SCHEME_WTAI_MC)) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("tel:" + url.substring(SurfWebView.SCHEME_WTAI_MC.length())));
                    if (SurfWebView.this.mContext != null) {
                        SurfWebView.this.mContext.startActivity(intent);
                    }
                    return true;
                } else if (url.startsWith(SurfWebView.SCHEME_WTAI_SD)) {
                    return false;
                } else {
                    if (url.startsWith(SurfWebView.SCHEME_WTAI_AP)) {
                        return false;
                    }
                }
            }
            if (url.startsWith("about:")) {
                return false;
            }
            if (url.startsWith(SurfWebView.SCHEME_HTTP) || url.startsWith(SurfWebView.SCHEME_HTTPS)) {
                SurfWebView.this.tryLoadUrl(url);
                return true;
            }
            try {
                Intent intent2 = Intent.parseUri(url, 1);
                intent2.addCategory("android.intent.category.BROWSABLE");
                intent2.setComponent(null);
                try {
                    if (SurfWebView.this.mContext != null && ((Activity) SurfWebView.this.mContext).startActivityIfNeeded(intent2, -1)) {
                        return true;
                    }
                    SurfWebView.this.tryLoadUrl(url);
                    return true;
                } catch (ActivityNotFoundException e) {
                    e.printStackTrace();
                }
            } catch (URISyntaxException ex) {
                Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "Bad URI " + url + ": " + ex.getMessage());
                return false;
            }
        }

        public void onPageFinished(WebView view, String url) {
            String line;
            String line2;
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onPageFinished - url: " + url);
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getOriginalUrl() - " + view.getOriginalUrl());
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getUrl() - " + view.getUrl());
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getTitle() - " + view.getTitle());
            SurfWebView.this.findAndLoadNextPageFromTitle(url, view.getTitle());
            CacheManager.CacheResult cache = CacheManager.getCacheFile(url, (Map) null);
            if (cache != null) {
                String mime = cache.getMimeType();
                Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "Cache file for url:'" + url + "' found");
                Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "mimeType=" + mime);
                if (mime.equalsIgnoreCase(SurfWebView.MIMETYPE_TEXT)) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(cache.getInputStream()));
                    StringBuilder buffer = new StringBuilder();
                    do {
                        try {
                            line2 = bufferedReader.readLine();
                            if (line2 != null) {
                                buffer.append(line2);
                                continue;
                            }
                        } catch (Exception e) {
                        }
                    } while (line2 != null);
                    try {
                        URL url2 = new URL(url);
                        String html = new Wml2Html().parse(buffer.toString(), url2.getHost(), url2.getPath(), !SurfWebView.this.mGoBack);
                        Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "host=" + url2.getHost() + ", path=" + url2.getPath() + ", forward=" + (!SurfWebView.this.mGoBack));
                        if (((double) view.getScale()) < 1.25d) {
                            view.setInitialScale(125);
                        }
                        view.clearView();
                        view.loadDataWithBaseURL(null, html, SurfWebView.MIMETYPE_HTML, "utf-8", null);
                        SurfWebView.this.findAndLoadNextPageFromData(url, html);
                    } catch (MalformedURLException e2) {
                        e2.printStackTrace();
                    }
                } else if (mime.equalsIgnoreCase(SurfWebView.MIMETYPE_HTML) || mime.equalsIgnoreCase(SurfWebView.MIMETYPE_XHTML) || mime.equalsIgnoreCase(SurfWebView.MIMETYPE_WMLC) || mime.equalsIgnoreCase(SurfWebView.MIMETYPE_WML)) {
                    if (SurfWebView.this.isNeedPreload(url)) {
                        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(cache.getInputStream()));
                        StringBuilder buffer2 = new StringBuilder();
                        do {
                            try {
                                line = bufferedReader2.readLine();
                                if (line != null) {
                                    buffer2.append(line);
                                    continue;
                                }
                            } catch (Exception e3) {
                            }
                        } while (line != null);
                        SurfWebView.this.findAndLoadNextPageFromData(url, buffer2.toString());
                    }
                    if (SurfWebView.this.mListener != null) {
                        SurfWebView.this.mListener.onPageFinished(SurfWebView.this, view.getUrl(), view.getTitle());
                    }
                }
                view.setVisibility(0);
            } else {
                Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "No cache file found for url:'" + url + "'");
                if (TextUtils.isEmpty(url) || url.equalsIgnoreCase("about:blank")) {
                    view.setVisibility(0);
                    if (SurfWebView.this.mListener != null) {
                        SurfWebView.this.mListener.onPageFinished(SurfWebView.this, view.getUrl(), view.getTitle());
                    }
                } else {
                    Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "Insert javascript into current page to get HTML data...");
                    String unused = SurfWebView.mPageDataUrl = url;
                    String unused2 = SurfWebView.mPageDataTitle = view.getTitle();
                    view.loadUrl("javascript:{var __p=window.PageData.getPageData();if(__p)__p.setPage(window.document.body.innerText,window.document.body.innerHTML);};");
                    if (view.getTitle() != null) {
                        if (SurfWebView.this.mListener != null) {
                            SurfWebView.this.mListener.onPageFinished(SurfWebView.this, url, view.getTitle());
                        }
                    } else if (view.getUrl() == null && SurfWebView.this.mListener != null) {
                        SurfWebView.this.mListener.onPageFinished(SurfWebView.this, url, WindowAdapter2.BLANK_URL);
                    }
                }
            }
            if (!SurfBrowser.useWML2HTML()) {
                view.setVisibility(0);
            }
            super.onPageFinished(view, url);
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onPageStarted - url:" + url);
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getOriginalUrl()" + view.getOriginalUrl());
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getUrl()" + view.getUrl());
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getTitle()" + view.getTitle());
            int visibility = 0;
            if (SurfBrowser.useWML2HTML()) {
                CacheManager.CacheResult cache = CacheManager.getCacheFile(url, (Map) null);
                if (cache != null) {
                    String mime = cache.getMimeType();
                    Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "Cache file of url:'" + url + "' found");
                    Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "mimeType = " + mime);
                    if (mime.equalsIgnoreCase(SurfWebView.MIMETYPE_TEXT)) {
                        visibility = 4;
                    }
                } else {
                    Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "No cache file found, set WebView invisible");
                    visibility = 4;
                }
            }
            view.setVisibility(visibility);
            String unused = SurfWebView.this.mStartedUrl = url;
            if (SurfWebView.this.mListener != null) {
                SurfWebView.this.mListener.onPageStarted(SurfWebView.this, url);
            }
            super.onPageStarted(view, url, favicon);
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onReceivedSslError");
            handler.proceed();
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onReceivedError - errorCode:" + errorCode + ", description:" + description + ", failingUrl:" + failingUrl);
            if (failingUrl.toLowerCase().startsWith(SurfManagerActivity.mMsb.domain)) {
                String newUrl = SurfWebView.SCHEME_HTTP + failingUrl.substring(SurfManagerActivity.mMsb.domain.length());
                Log.d(SurfWebView.LOG_TAG + SurfWebView.this.mID, "reload with new url: " + newUrl);
                view.clearView();
                view.loadUrl(newUrl);
                return;
            }
            view.setVisibility(0);
            String unused = SurfWebView.this.mFailUrl = failingUrl;
            view.loadUrl("file:///android_asset/html/error.html");
            if (SurfWebView.this.mListener != null) {
                SurfWebView.this.mListener.onReceivedError(SurfWebView.this, errorCode, description, failingUrl);
            }
        }

        public void onFormResubmission(WebView webview, Message dontResend, Message resend) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onFormResubmission");
            dontResend.sendToTarget();
        }
    }

    public class SurfWebChromeClient extends WebChromeClient {
        public SurfWebChromeClient() {
        }

        public void onProgressChanged(WebView view, int newProgress) {
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onProgressChanged - newProgress:" + newProgress);
            if (SurfWebView.this.mListener != null) {
                SurfWebView.this.mListener.onProgressChanged(SurfWebView.this, newProgress);
            }
            super.onProgressChanged(view, newProgress);
        }

        public void onReceivedTitle(WebView view, String title) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onReceivedTitle - title:" + title);
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getOriginalUrl()" + view.getOriginalUrl());
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getUrl()" + view.getUrl());
            Log.i(SurfWebView.LOG_TAG + SurfWebView.this.mID, "view.getTitle()" + view.getTitle());
            view.setVisibility(0);
            if (SurfWebView.this.mListener != null) {
                SurfWebView.this.mListener.onReceivedTitle(SurfWebView.this, title, false);
            }
            super.onReceivedTitle(view, title);
        }

        public void onReceivedIcon(WebView view, Bitmap icon) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onReceivedIcon");
            super.onReceivedIcon(view, icon);
        }

        public void onRequestFocus(WebView view) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onRequestFocus");
            super.onRequestFocus(view);
        }

        public boolean onCreateWindow(WebView view, boolean dialog, boolean userGesture, Message resultMsg) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onCreateWindow: dialog=" + dialog + ", userGesture=" + userGesture + ", resultMsg: " + resultMsg.toString());
            return super.onCreateWindow(view, dialog, userGesture, resultMsg);
        }

        public void onCloseWindow(WebView window) {
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "onCloseWindow");
            super.onCloseWindow(window);
        }
    }

    class FindPreloadUrlTask extends AsyncTask<String, Integer, String> {
        FindPreloadUrlTask() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(String... params) {
            String url = params[0];
            String buffer = params[1];
            if (!TextUtils.isEmpty(url) && !TextUtils.isEmpty(buffer)) {
                return SurfWebView.this.findPreloadUrl(url, buffer);
            }
            Log.e(SurfWebView.LOG_TAG + SurfWebView.this.mID, "FindPreloadUrlTask.doInBackground: url or buffer is empty");
            return null;
        }

        /* access modifiers changed from: protected */
        public void onCancelled() {
            super.onCancelled();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (TextUtils.isEmpty(result)) {
                String unused = SurfWebView.this.mPreloadUrl = WindowAdapter2.BLANK_URL;
            } else if (SurfWebView.this.mListener == null || !SurfWebView.this.mListener.isCurrent(SurfWebView.this) || TextUtils.isEmpty(result)) {
                String unused2 = SurfWebView.this.mPreloadUrl = result;
            } else {
                SurfWebView.this.mListener.onPreloadUrl(result);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... values) {
        }
    }

    private class WebViewOnGestureListener extends GestureDetector.SimpleOnGestureListener {
        private WebViewOnGestureListener() {
        }

        public boolean onDoubleTap(MotionEvent e) {
            Log.e(SurfWebView.LOG_TAG, "DoubleTapListener.onDoubleTap");
            SurfWebView.this.zoomAuto();
            return true;
        }
    }

    final class runJavaScript {
        runJavaScript() {
        }

        public String runOnAndroidJavaScript(final String str) {
            if (str.equalsIgnoreCase("getfailurl")) {
                Log.v("jdm", "getfailurl mFailUrl= " + SurfWebView.this.mFailUrl);
                return SurfWebView.this.mFailUrl;
            }
            SurfWebView.this.hander.post(new Runnable() {
                public void run() {
                    if (str.equalsIgnoreCase("refresh")) {
                        Log.w(SurfWebView.LOG_TAG, "enter runJavaScript>>refresh");
                        SurfWebView.this.setVisibility(4);
                        SurfWebView.this.loadUrlWithInitialScale(SurfWebView.this.mFailUrl, true, (double) SurfWebView.this.getScale());
                    } else if (str.equalsIgnoreCase("connect")) {
                        Log.w(SurfWebView.LOG_TAG, "enter runJavaScript>>connect");
                        SurfManagerActivity.mNetworkMgr.connect();
                    }
                }
            });
            return null;
        }
    }
}
