package com.cplatform.android.cmsurfclient.download.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.CrossProcessCursor;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.CursorWrapper;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.os.Binder;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.util.Log;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;

public class DownloadProvider extends ContentProvider {
    private static final String DB_NAME = "downloads.db";
    private static final String DB_TABLE = "downloads";
    private static final int DB_VERSION = 3;
    protected static final int DOWNLOADS = 1;
    protected static final int DOWNLOADS_ID = 2;
    private static final String DOWNLOAD_LIST_TYPE = "vnd.android.cursor.dir/download";
    private static final String DOWNLOAD_TYPE = "vnd.android.cursor.item/download";
    private static final String[] sAppReadableColumnsArray = {QueryApList.Carriers._ID, Downloads.COLUMN_APP_DATA, Downloads._DATA, Downloads.COLUMN_MIME_TYPE, Downloads.COLUMN_VISIBILITY, Downloads.COLUMN_DESTINATION, Downloads.COLUMN_CONTROL, Downloads.COLUMN_STATUS, Downloads.COLUMN_LAST_MODIFICATION, Downloads.COLUMN_NOTIFICATION_PACKAGE, Downloads.COLUMN_NOTIFICATION_CLASS, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_CURRENT_BYTES, Downloads.COLUMN_CURRENT_ELAPSED_TIME, Downloads.COLUMN_TITLE, Downloads.COLUMN_DESCRIPTION, Downloads.COLUMN_URI};
    private static HashSet<String> sAppReadableColumnsSet = new HashSet<>();
    protected static final UriMatcher sURIMatcher = new UriMatcher(-1);
    private SQLiteOpenHelper mOpenHelper = null;

    static {
        for (String add : sAppReadableColumnsArray) {
            sAppReadableColumnsSet.add(add);
        }
        DownloadSettings.ensureAppSettingsInitialized();
        sURIMatcher.addURI(DownloadSettings.getDownloadProviderAuthorities(), "download", 1);
        sURIMatcher.addURI(DownloadSettings.getDownloadProviderAuthorities(), "download/#", 2);
    }

    class ReadOnlyCursorWrapper extends CursorWrapper implements CrossProcessCursor {
        private CrossProcessCursor mCursor;

        public void fillWindow(int i, CursorWindow cursorwindow) {
            this.mCursor.fillWindow(i, cursorwindow);
        }

        public CursorWindow getWindow() {
            return this.mCursor.getWindow();
        }

        public boolean onMove(int i, int j) {
            return this.mCursor.onMove(i, j);
        }

        public ReadOnlyCursorWrapper(Cursor cursor) {
            super(cursor);
            this.mCursor = (CrossProcessCursor) cursor;
        }
    }

    final class DatabaseHelper extends SQLiteOpenHelper {
        public void onCreate(SQLiteDatabase sqlitedatabase) {
            DownloadProvider.this.createTable(sqlitedatabase);
        }

        public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j) {
            if (i < j) {
                Log.i(Constants.TAG, "Upgrading downloads database from version " + i + " to " + j + ", which will destroy all old data");
                DownloadProvider.this.dropTable(sqlitedatabase);
                DownloadProvider.this.createTable(sqlitedatabase);
            }
        }

        public DatabaseHelper(Context context) {
            super(context, DownloadProvider.DB_NAME, (SQLiteDatabase.CursorFactory) null, 3);
        }
    }

    private static final void copyBoolean(String s, ContentValues contentvalues, ContentValues contentvalues1) {
        Boolean boolean1 = contentvalues.getAsBoolean(s);
        if (boolean1 != null) {
            contentvalues1.put(s, boolean1);
        }
    }

    private static final void copyInteger(String s, ContentValues contentvalues, ContentValues contentvalues1) {
        Integer integer = contentvalues.getAsInteger(s);
        if (integer != null) {
            contentvalues1.put(s, integer);
        }
    }

    private static final void copyString(String s, ContentValues contentvalues, ContentValues contentvalues1) {
        String s1 = contentvalues.getAsString(s);
        if (s1 != null) {
            contentvalues1.put(s, s1);
        }
    }

    /* access modifiers changed from: private */
    public void createTable(SQLiteDatabase sqlitedatabase) {
        try {
            sqlitedatabase.execSQL("CREATE TABLE downloads(_id INTEGER PRIMARY KEY AUTOINCREMENT,uri TEXT, method INTEGER, entity TEXT, no_integrity BOOLEAN, hint TEXT, _data TEXT, mimetype TEXT, destination INTEGER, visibility INTEGER, control INTEGER, status INTEGER, numfailed INTEGER, lastmod BIGINT, notificationpackage TEXT, notificationclass TEXT, notificationextras TEXT, cookiedata TEXT, useragent TEXT, referer TEXT, total_bytes INTEGER, current_bytes INTEGER, current_elapsed_time INTEGER, etag TEXT, if_range_id TEXT, uid INTEGER, otheruid INTEGER, title TEXT, description TEXT, scanned BOOLEAN, subdirectory TEXT, appointname TEXT );");
        } catch (SQLException e) {
            SQLException sqlexception = e;
            Log.e(Constants.TAG, "couldn't create table in downloads database");
            throw sqlexception;
        }
    }

    /* access modifiers changed from: private */
    public void dropTable(SQLiteDatabase sqlitedatabase) {
        try {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS downloads");
        } catch (SQLException e) {
            SQLException sqlexception = e;
            Log.e(Constants.TAG, "couldn't drop table in downloads database");
            throw sqlexception;
        }
    }

    public int delete(Uri uri, String where, String[] whereArgs) {
        String myWhere;
        Helpers.validateSelection(where, sAppReadableColumnsSet);
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        int match = sURIMatcher.match(uri);
        switch (match) {
            case 1:
            case 2:
                break;
            default:
                Log.d(Constants.TAG, "deleting unknown/invalid URI: " + uri);
                throw new UnsupportedOperationException("Cannot delete URI: " + uri);
        }
        if (where == null) {
            myWhere = WindowAdapter2.BLANK_URL;
        } else if (match == 1) {
            myWhere = "( " + where + " )";
        } else {
            myWhere = "( " + where + " ) AND ";
        }
        if (match == 2) {
            myWhere = myWhere + " ( _id = " + Long.parseLong(uri.getPathSegments().get(1)) + " ) ";
        }
        if (!(Binder.getCallingPid() == Process.myPid() || Binder.getCallingUid() == 0)) {
            myWhere = myWhere + " AND ( uid=" + Binder.getCallingUid() + " OR " + Downloads.COLUMN_OTHER_UID + "=" + Binder.getCallingUid() + " )";
        }
        int count = db.delete(DB_TABLE, myWhere, whereArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case 1:
                return DOWNLOAD_LIST_TYPE;
            case 2:
                return DOWNLOAD_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x013f, code lost:
        if (getContext().getPackageManager().getApplicationInfo(r10, 0).uid == r15) goto L_0x0141;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.net.Uri insert(android.net.Uri r21, android.content.ContentValues r22) {
        /*
            r20 = this;
            r0 = r20
            android.database.sqlite.SQLiteOpenHelper r0 = r0.mOpenHelper
            r17 = r0
            android.database.sqlite.SQLiteDatabase r14 = r17.getWritableDatabase()
            android.content.UriMatcher r17 = com.cplatform.android.cmsurfclient.download.provider.DownloadProvider.sURIMatcher
            r0 = r17
            r1 = r21
            int r17 = r0.match(r1)
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 == r1) goto L_0x0055
            java.lang.String r17 = "download/provider"
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "calling insert on an unknown/invalid URI: "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r21
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            android.util.Log.d(r17, r18)
            java.lang.IllegalArgumentException r17 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r18 = new java.lang.StringBuilder
            r18.<init>()
            java.lang.String r19 = "Unknown/Invalid URI "
            java.lang.StringBuilder r18 = r18.append(r19)
            r0 = r18
            r1 = r21
            java.lang.StringBuilder r18 = r0.append(r1)
            java.lang.String r18 = r18.toString()
            r17.<init>(r18)
            throw r17
        L_0x0055:
            android.content.ContentValues r7 = new android.content.ContentValues
            r7.<init>()
            java.lang.String r17 = "uri"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "entity"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "no_integrity"
            r0 = r17
            r1 = r22
            r2 = r7
            copyBoolean(r0, r1, r2)
            java.lang.String r17 = "hint"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "mimetype"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "destination"
            r0 = r22
            r1 = r17
            java.lang.Integer r6 = r0.getAsInteger(r1)
            if (r6 == 0) goto L_0x00a1
            java.lang.String r17 = "destination"
            r0 = r7
            r1 = r17
            r2 = r6
            r0.put(r1, r2)
        L_0x00a1:
            java.lang.String r17 = "visibility"
            r0 = r22
            r1 = r17
            java.lang.Integer r16 = r0.getAsInteger(r1)
            if (r16 != 0) goto L_0x0252
            int r17 = r6.intValue()
            if (r17 != 0) goto L_0x0240
            java.lang.String r17 = "visibility"
            r18 = 1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)
            r0 = r7
            r1 = r17
            r2 = r18
            r0.put(r1, r2)
        L_0x00c3:
            java.lang.String r17 = "control"
            r0 = r17
            r1 = r22
            r2 = r7
            copyInteger(r0, r1, r2)
            java.lang.String r17 = "status"
            r0 = r22
            r1 = r17
            boolean r17 = r0.containsKey(r1)
            if (r17 == 0) goto L_0x025e
            java.lang.String r17 = "status"
            r0 = r17
            r1 = r22
            r2 = r7
            copyInteger(r0, r1, r2)
        L_0x00e3:
            java.lang.String r17 = "lastmod"
            long r18 = java.lang.System.currentTimeMillis()
            java.lang.Long r18 = java.lang.Long.valueOf(r18)
            r0 = r7
            r1 = r17
            r2 = r18
            r0.put(r1, r2)
            java.lang.String r17 = "current_elapsed_time"
            r18 = 0
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)
            r0 = r7
            r1 = r17
            r2 = r18
            r0.put(r1, r2)
            java.lang.String r17 = "notificationpackage"
            r0 = r22
            r1 = r17
            java.lang.String r10 = r0.getAsString(r1)
            java.lang.String r17 = "notificationclass"
            r0 = r22
            r1 = r17
            java.lang.String r4 = r0.getAsString(r1)
            if (r10 == 0) goto L_0x0153
            if (r4 == 0) goto L_0x0153
            int r15 = android.os.Binder.getCallingUid()
            if (r15 == 0) goto L_0x0141
            android.content.Context r17 = r20.getContext()     // Catch:{ NameNotFoundException -> 0x0278 }
            android.content.pm.PackageManager r17 = r17.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0278 }
            r18 = 0
            r0 = r17
            r1 = r10
            r2 = r18
            android.content.pm.ApplicationInfo r17 = r0.getApplicationInfo(r1, r2)     // Catch:{ NameNotFoundException -> 0x0278 }
            r0 = r17
            int r0 = r0.uid     // Catch:{ NameNotFoundException -> 0x0278 }
            r17 = r0
            r0 = r17
            r1 = r15
            if (r0 != r1) goto L_0x0153
        L_0x0141:
            java.lang.String r17 = "notificationpackage"
            r0 = r7
            r1 = r17
            r2 = r10
            r0.put(r1, r2)     // Catch:{ NameNotFoundException -> 0x0278 }
            java.lang.String r17 = "notificationclass"
            r0 = r7
            r1 = r17
            r2 = r4
            r0.put(r1, r2)     // Catch:{ NameNotFoundException -> 0x0278 }
        L_0x0153:
            java.lang.String r17 = "notificationextras"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "cookiedata"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "useragent"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "referer"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "uid"
            int r18 = android.os.Binder.getCallingUid()
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)
            r0 = r7
            r1 = r17
            r2 = r18
            r0.put(r1, r2)
            int r17 = android.os.Binder.getCallingUid()
            if (r17 != 0) goto L_0x019d
            java.lang.String r17 = "uid"
            r0 = r17
            r1 = r22
            r2 = r7
            copyInteger(r0, r1, r2)
        L_0x019d:
            java.lang.String r17 = "title"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "description"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "subdirectory"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            java.lang.String r17 = "appointname"
            r0 = r17
            r1 = r22
            r2 = r7
            copyString(r0, r1, r2)
            android.content.Context r5 = r20.getContext()
            android.content.Intent r8 = new android.content.Intent
            r8.<init>()
            java.lang.String r17 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getAppPackageName()
            java.lang.String r18 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadServiceClassName()
            r0 = r8
            r1 = r17
            r2 = r18
            r0.setClassName(r1, r2)
            r5.startService(r8)
            java.lang.String r17 = "downloads"
            r18 = 0
            r0 = r14
            r1 = r17
            r2 = r18
            r3 = r7
            long r12 = r0.insert(r1, r2, r3)
            r11 = 0
            r17 = -1
            int r17 = (r12 > r17 ? 1 : (r12 == r17 ? 0 : -1))
            if (r17 == 0) goto L_0x0270
            android.content.Intent r9 = new android.content.Intent
            r9.<init>()
            java.lang.String r17 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getAppPackageName()
            java.lang.String r18 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadServiceClassName()
            r0 = r9
            r1 = r17
            r2 = r18
            r0.setClassName(r1, r2)
            r5.startService(r9)
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            r17.<init>()
            android.net.Uri r18 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r18 = "/"
            java.lang.StringBuilder r17 = r17.append(r18)
            r0 = r17
            r1 = r12
            java.lang.StringBuilder r17 = r0.append(r1)
            java.lang.String r17 = r17.toString()
            android.net.Uri r11 = android.net.Uri.parse(r17)
            android.content.ContentResolver r17 = r5.getContentResolver()
            r18 = 0
            r0 = r17
            r1 = r21
            r2 = r18
            r0.notifyChange(r1, r2)
        L_0x023f:
            return r11
        L_0x0240:
            java.lang.String r17 = "visibility"
            r18 = 2
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)
            r0 = r7
            r1 = r17
            r2 = r18
            r0.put(r1, r2)
            goto L_0x00c3
        L_0x0252:
            java.lang.String r17 = "visibility"
            r0 = r7
            r1 = r17
            r2 = r16
            r0.put(r1, r2)
            goto L_0x00c3
        L_0x025e:
            java.lang.String r17 = "status"
            r18 = 190(0xbe, float:2.66E-43)
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)
            r0 = r7
            r1 = r17
            r2 = r18
            r0.put(r1, r2)
            goto L_0x00e3
        L_0x0270:
            java.lang.String r17 = "download/provider"
            java.lang.String r18 = "couldn't insert into downloads database"
            android.util.Log.d(r17, r18)
            goto L_0x023f
        L_0x0278:
            r17 = move-exception
            goto L_0x0153
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.DownloadProvider.insert(android.net.Uri, android.content.ContentValues):android.net.Uri");
    }

    public boolean onCreate() {
        this.mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    public ParcelFileDescriptor openFile(Uri uri, String s) throws FileNotFoundException {
        Cursor cursor1 = query(uri, new String[]{Downloads._DATA, Downloads.COLUMN_SUB_DIRECTORY}, null, null, null);
        int l = cursor1 != null ? cursor1.getCount() : 0;
        if (l != 1) {
            if (cursor1 != null) {
                cursor1.close();
            }
            if (l == 0) {
                throw new FileNotFoundException("No entry for " + uri);
            }
            throw new FileNotFoundException("Multiple items at " + uri);
        }
        cursor1.moveToFirst();
        String s11 = cursor1.getString(0);
        String s12 = cursor1.getString(1);
        cursor1.close();
        if (s11 == null) {
            throw new FileNotFoundException("No filename found.");
        } else if (!Helpers.isFilenameValid(s11, s12)) {
            throw new FileNotFoundException("Invalid filename.");
        } else if (!"r".equals(s)) {
            throw new FileNotFoundException("Bad mode for " + uri + ": " + s);
        } else {
            ParcelFileDescriptor parcelfiledescriptor = ParcelFileDescriptor.open(new File(s11), 268435456);
            if (parcelfiledescriptor == null) {
                throw new FileNotFoundException("couldn't open file");
            }
            ContentValues contentvalues = new ContentValues();
            contentvalues.put(Downloads.COLUMN_LAST_MODIFICATION, Long.valueOf(System.currentTimeMillis()));
            update(uri, contentvalues, null, null);
            return parcelfiledescriptor;
        }
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sort) {
        if (!(selection == null || selectionArgs == null)) {
        }
        Helpers.validateSelection(selection, sAppReadableColumnsSet);
        SQLiteDatabase db = this.mOpenHelper.getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        boolean emptyWhere = true;
        switch (sURIMatcher.match(uri)) {
            case 1:
                qb.setTables(DB_TABLE);
                break;
            case 2:
                qb.setTables(DB_TABLE);
                qb.appendWhere("_id=");
                qb.appendWhere(uri.getPathSegments().get(1));
                emptyWhere = false;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        if (!(Binder.getCallingPid() == Process.myPid() || Binder.getCallingUid() == 0 || !Process.supportsProcesses())) {
            if (!emptyWhere) {
                qb.appendWhere(" AND ");
            }
            qb.appendWhere("( uid=" + Binder.getCallingUid() + " OR " + Downloads.COLUMN_OTHER_UID + "=" + Binder.getCallingUid() + " )");
            if (projection == null) {
                projection = sAppReadableColumnsArray;
            } else {
                for (int i = 0; i < projection.length; i++) {
                    if (!sAppReadableColumnsSet.contains(projection[i])) {
                        throw new IllegalArgumentException("column " + projection[i] + " is not allowed in queries");
                    }
                }
            }
        }
        Cursor ret = qb.query(db, projection, selection, selectionArgs, null, null, sort);
        if (ret != null) {
            ret = new ReadOnlyCursorWrapper(ret);
        }
        if (ret != null) {
            ret.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return ret;
    }

    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        ContentValues filteredValues;
        String myWhere;
        int count;
        Helpers.validateSelection(where, sAppReadableColumnsSet);
        SQLiteDatabase db = this.mOpenHelper.getWritableDatabase();
        boolean startService = false;
        if (Binder.getCallingPid() != Process.myPid()) {
            filteredValues = new ContentValues();
            copyString(Downloads.COLUMN_APP_DATA, values, filteredValues);
            copyInteger(Downloads.COLUMN_VISIBILITY, values, filteredValues);
            Integer i = values.getAsInteger(Downloads.COLUMN_CONTROL);
            if (i != null) {
                filteredValues.put(Downloads.COLUMN_CONTROL, i);
                startService = true;
            }
            copyInteger(Downloads.COLUMN_CONTROL, values, filteredValues);
            copyString(Downloads.COLUMN_TITLE, values, filteredValues);
            copyString(Downloads.COLUMN_DESCRIPTION, values, filteredValues);
        } else {
            filteredValues = values;
        }
        int match = sURIMatcher.match(uri);
        switch (match) {
            case 1:
            case 2:
                break;
            default:
                Log.d(Constants.TAG, "updating unknown/invalid URI: " + uri);
                throw new UnsupportedOperationException("Cannot update URI: " + uri);
        }
        if (where == null) {
            myWhere = WindowAdapter2.BLANK_URL;
        } else if (match == 1) {
            myWhere = "( " + where + " )";
        } else {
            myWhere = "( " + where + " ) AND ";
        }
        if (match == 2) {
            myWhere = myWhere + " ( _id = " + Long.parseLong(uri.getPathSegments().get(1)) + " ) ";
        }
        if (!(Binder.getCallingPid() == Process.myPid() || Binder.getCallingUid() == 0)) {
            myWhere = myWhere + " AND ( uid=" + Binder.getCallingUid() + " OR " + Downloads.COLUMN_OTHER_UID + "=" + Binder.getCallingUid() + " )";
        }
        if (filteredValues.size() > 0) {
            count = db.update(DB_TABLE, filteredValues, myWhere, whereArgs);
        } else {
            count = 0;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        if (startService) {
            Context context = getContext();
            context.startService(new Intent(context, DownloadService.class));
        }
        return count;
    }
}
