package com.cplatform.android.cmsurfclient.download.provider;

import android.app.Service;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class DownloadService extends Service {
    /* access modifiers changed from: private */
    public ArrayList<DownloadInfo> mDownloads;
    /* access modifiers changed from: private */
    public DownloadMediaScannerConnection mMediaScannerConnection;
    /* access modifiers changed from: private */
    public CharArrayBuffer mNewChars;
    private DownloadNotification mNotifier;
    private DownloadManagerContentObserver mObserver;
    /* access modifiers changed from: private */
    public boolean mPendingUpdate;
    /* access modifiers changed from: private */
    public UpdateThread mUpdateThread;
    private boolean mWifiConnected = false;
    /* access modifiers changed from: private */
    public CharArrayBuffer oldChars;

    static {
        DownloadSettings.ensureAppSettingsInitialized();
    }

    /* access modifiers changed from: protected */
    public DownloadNotification getDownloadNotifier() {
        return this.mNotifier;
    }

    class UpdateThread extends Thread {
        /* JADX WARNING: Code restructure failed: missing block: B:100:0x02b7, code lost:
            r27 = true;
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x02c4, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1100(r0.this$0, r7) == false) goto L_0x02c8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x02c6, code lost:
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x02c8, code lost:
            r31 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1200(r0.this$0, r7, r33);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x02d9, code lost:
            if (r31 != 0) goto L_0x02e8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x02db, code lost:
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x02dd, code lost:
            r7 = r7 + 1;
            r6.moveToNext();
            r25 = r6.isAfterLast();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x02ec, code lost:
            if (r31 <= 0) goto L_0x02dd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x02f0, code lost:
            if (r31 >= r40) goto L_0x02dd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x02f2, code lost:
            r40 = r31;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x02f5, code lost:
            r0.this$0.getDownloadNotifier().updateAll();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x0301, code lost:
            if (r27 == false) goto L_0x0395;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:0x0310, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1400(r0.this$0).isConnected() != false) goto L_0x031e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x0312, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1400(r0.this$0).connect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x031e, code lost:
            r6.close();
            r39 = r0.this$0.getContentResolver().query(com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri(), null, "status = ?", new java.lang.String[]{java.lang.String.valueOf(-100)}, com.cplatform.android.cmsurfclient.network.ophone.QueryApList.Carriers._ID);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x0347, code lost:
            if (r39.moveToFirst() == false) goto L_0x0418;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x0349, code lost:
            r35 = 0;
            r21 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x035d, code lost:
            if (r21 >= com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).size()) goto L_0x03a3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x0377, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusCompleted(((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r21)).mStatus) != false) goto L_0x0392;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x038e, code lost:
            if (((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r21)).mControl == 1) goto L_0x0392;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x0390, code lost:
            r35 = r35 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x0392, code lost:
            r21 = r21 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x0395, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1400(r0.this$0).disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x03aa, code lost:
            if (r35 >= com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxConcurrentDownloadingTasks()) goto L_0x0418;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x03ac, code lost:
            r30 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x03b5, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxDownloadingTasksPerDomain() == Integer.MAX_VALUE) goto L_0x03bd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:135:0x03bb, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxDownloadingTasksPerDomain() > 0) goto L_0x0424;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:136:0x03bd, code lost:
            r16 = new android.content.ContentValues();
            r16.put(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_STATUS, java.lang.Integer.valueOf((int) com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_PENDING));
            r0.this$0.getContentResolver().update(android.content.ContentUris.withAppendedId(com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri(), (long) r39.getInt(r39.getColumnIndexOrThrow(com.cplatform.android.cmsurfclient.network.ophone.QueryApList.Carriers._ID))), r16, null, null);
            r30 = true;
            r35 = r35 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:137:0x0409, code lost:
            if (r35 < com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxConcurrentDownloadingTasks()) goto L_0x041d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:138:0x040b, code lost:
            if (r30 == false) goto L_0x0418;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:139:0x040d, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$102(r0.this$0, true);
            r26 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:142:0x0421, code lost:
            if (r39.moveToNext() != false) goto L_0x03bd;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:143:0x0424, code lost:
            r20 = new java.util.HashMap<>();
            r19 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).size();
            r21 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:145:0x043c, code lost:
            if (r21 >= r19) goto L_0x04f5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:147:0x0456, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusCompleted(((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r21)).mStatus) != false) goto L_0x04b8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:149:0x046d, code lost:
            if (((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r21)).mControl == 1) goto L_0x04b8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:150:0x046f, code lost:
            r38 = ((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r21)).mUriDomain;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:151:0x048a, code lost:
            if (android.text.TextUtils.isEmpty(r38) == false) goto L_0x04bc;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:152:0x048c, code lost:
            android.util.Log.e(com.cplatform.android.cmsurfclient.download.provider.Constants.TAG, "Unknown domain for uri: " + ((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r21)).mUri);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:153:0x04b8, code lost:
            r21 = r21 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:154:0x04bc, code lost:
            r38 = r38.toLowerCase();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:155:0x04c8, code lost:
            if (r20.containsKey(r38) == false) goto L_0x04e7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:156:0x04ca, code lost:
            r20.put(r38, java.lang.Integer.valueOf(((java.lang.Integer) r20.get(r38)).intValue() + 1));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:157:0x04e7, code lost:
            r20.put(r38, 1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:159:0x0506, code lost:
            if (r39.getInt(r39.getColumnIndex(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_CONTROL)) != 1) goto L_0x050f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:0x050c, code lost:
            if (r39.moveToNext() == false) goto L_0x040b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:162:0x050f, code lost:
            r37 = com.cplatform.android.cmsurfclient.download.provider.DownloadInfo.getUriDomain(r39.getString(r39.getColumnIndexOrThrow(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_URI)));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:163:0x0527, code lost:
            if (android.text.TextUtils.isEmpty(r37) == false) goto L_0x0530;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:165:0x052d, code lost:
            if (r39.moveToNext() == false) goto L_0x040b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:166:0x0530, code lost:
            r37 = r37.toLowerCase();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:167:0x053c, code lost:
            if (r20.containsKey(r37) == false) goto L_0x055c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:168:0x053e, code lost:
            r36 = ((java.lang.Integer) r20.get(r37)).intValue();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:170:0x0553, code lost:
            if (r36 < com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxDownloadingTasksPerDomain()) goto L_0x055f;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:172:0x0559, code lost:
            if (r39.moveToNext() == false) goto L_0x040b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:173:0x055c, code lost:
            r36 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:174:0x055f, code lost:
            r17 = new android.content.ContentValues();
            r17.put(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_STATUS, java.lang.Integer.valueOf((int) com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_PENDING));
            r0.this$0.getContentResolver().update(android.content.ContentUris.withAppendedId(com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri(), (long) r39.getInt(r39.getColumnIndexOrThrow(com.cplatform.android.cmsurfclient.network.ophone.QueryApList.Carriers._ID))), r17, null, null);
            r35 = r35 + 1;
            r30 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:175:0x05ab, code lost:
            if (r35 >= com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxConcurrentDownloadingTasks()) goto L_0x040b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:177:0x05b5, code lost:
            if (r20.containsKey(r37) == false) goto L_0x05db;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:178:0x05b7, code lost:
            r20.put(r37, java.lang.Integer.valueOf(((java.lang.Integer) r20.get(r37)).intValue() + 1));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:180:0x05d7, code lost:
            if (r39.moveToNext() == false) goto L_0x040b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:181:0x05db, code lost:
            r20.put(r37, 1);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:221:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bb, code lost:
            r28 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r0.this$0);
            r29 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkRoaming(r0.this$0);
            r33 = java.lang.System.currentTimeMillis();
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$400(r0.this$0, false);
            r6 = r0.this$0.getContentResolver().query(com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri(), null, "status != ?", new java.lang.String[]{java.lang.String.valueOf(-100)}, com.cplatform.android.cmsurfclient.network.ophone.QueryApList.Carriers._ID);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00fc, code lost:
            if (r6 == null) goto L_0x007b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00fe, code lost:
            r6.moveToFirst();
            r7 = 0;
            r27 = false;
            r26 = false;
            r40 = Long.MAX_VALUE;
            r25 = r6.isAfterLast();
            r23 = -1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0111, code lost:
            if (r25 != false) goto L_0x0119;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0113, code lost:
            r23 = r6.getColumnIndexOrThrow(com.cplatform.android.cmsurfclient.network.ophone.QueryApList.Carriers._ID);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0119, code lost:
            if (r25 == false) goto L_0x012a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0128, code lost:
            if (r7 >= com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).size()) goto L_0x02f5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x012a, code lost:
            if (r25 == false) goto L_0x0154;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0135, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$600(r0.this$0, r7) == false) goto L_0x014b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0140, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$700(r0.this$0) == false) goto L_0x014b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0142, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$800(r0.this$0, null, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x014b, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$900(r0.this$0, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0154, code lost:
            r22 = r6.getInt(r23);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0168, code lost:
            if (r7 != com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).size()) goto L_0x01d7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x016a, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1000(r0.this$0, r6, r7, r28, r29, r33);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x0181, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$600(r0.this$0, r7) == false) goto L_0x019d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x018c, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$700(r0.this$0) == false) goto L_0x0199;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x0197, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$800(r0.this$0, r6, r7) != false) goto L_0x019d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0199, code lost:
            r27 = true;
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x01a6, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1100(r0.this$0, r7) == false) goto L_0x01aa;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x01a8, code lost:
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x01aa, code lost:
            r31 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1200(r0.this$0, r7, r33);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x01bb, code lost:
            if (r31 != 0) goto L_0x01ca;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x01bd, code lost:
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x01bf, code lost:
            r7 = r7 + 1;
            r6.moveToNext();
            r25 = r6.isAfterLast();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x01ce, code lost:
            if (r31 <= 0) goto L_0x01bf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x01d2, code lost:
            if (r31 >= r40) goto L_0x01bf;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x01d4, code lost:
            r40 = r31;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x01d7, code lost:
            r15 = ((com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$500(r0.this$0).get(r7)).mId;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x01eb, code lost:
            if (r15 >= r22) goto L_0x0216;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x01f6, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$600(r0.this$0, r7) == false) goto L_0x020c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x0201, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$700(r0.this$0) == false) goto L_0x020c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x0203, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$800(r0.this$0, null, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x020c, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$900(r0.this$0, r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x0219, code lost:
            if (r15 != r22) goto L_0x0288;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x021b, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1300(r0.this$0, r6, r7, r28, r29, r33);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x0232, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$600(r0.this$0, r7) == false) goto L_0x024e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x023d, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$700(r0.this$0) == false) goto L_0x024a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x0248, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$800(r0.this$0, r6, r7) != false) goto L_0x024e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x024a, code lost:
            r27 = true;
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x0257, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1100(r0.this$0, r7) == false) goto L_0x025b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x0259, code lost:
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x025b, code lost:
            r31 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1200(r0.this$0, r7, r33);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x026c, code lost:
            if (r31 != 0) goto L_0x027b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x026e, code lost:
            r26 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x0270, code lost:
            r7 = r7 + 1;
            r6.moveToNext();
            r25 = r6.isAfterLast();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x027f, code lost:
            if (r31 <= 0) goto L_0x0270;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0283, code lost:
            if (r31 >= r40) goto L_0x0270;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x0285, code lost:
            r40 = r31;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0288, code lost:
            com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$1000(r0.this$0, r6, r7, r28, r29, r33);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x029f, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$600(r0.this$0, r7) == false) goto L_0x02bb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x02aa, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$700(r0.this$0) == false) goto L_0x02b7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x02b5, code lost:
            if (com.cplatform.android.cmsurfclient.download.provider.DownloadService.access$800(r0.this$0, r6, r7) != false) goto L_0x02bb;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r42 = this;
                r5 = 10
                android.os.Process.setThreadPriority(r5)
                r26 = 0
                r40 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            L_0x000c:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                monitor-enter(r5)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                com.cplatform.android.cmsurfclient.download.provider.DownloadService$UpdateThread r6 = r6.mUpdateThread     // Catch:{ all -> 0x0028 }
                r0 = r6
                r1 = r42
                if (r0 == r1) goto L_0x002b
                java.lang.IllegalStateException r6 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0028 }
                java.lang.String r7 = "multiple UpdateThreads in DownloadService"
                r6.<init>(r7)     // Catch:{ all -> 0x0028 }
                throw r6     // Catch:{ all -> 0x0028 }
            L_0x0028:
                r6 = move-exception
                monitor-exit(r5)     // Catch:{ all -> 0x0028 }
                throw r6
            L_0x002b:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                boolean r6 = r6.mPendingUpdate     // Catch:{ all -> 0x0028 }
                if (r6 != 0) goto L_0x00b1
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                r7 = 0
                com.cplatform.android.cmsurfclient.download.provider.DownloadService.UpdateThread unused = r6.mUpdateThread = r7     // Catch:{ all -> 0x0028 }
                if (r26 != 0) goto L_0x0049
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                r6.stopSelf()     // Catch:{ all -> 0x0028 }
            L_0x0049:
                r8 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                int r6 = (r40 > r8 ? 1 : (r40 == r8 ? 0 : -1))
                if (r6 == 0) goto L_0x0068
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                java.lang.String r7 = "alarm"
                java.lang.Object r14 = r6.getSystemService(r7)     // Catch:{ all -> 0x0028 }
                android.app.AlarmManager r14 = (android.app.AlarmManager) r14     // Catch:{ all -> 0x0028 }
                if (r14 != 0) goto L_0x007c
                java.lang.String r6 = "download/provider"
                java.lang.String r7 = "couldn't get alarm manager"
                android.util.Log.e(r6, r7)     // Catch:{ all -> 0x0028 }
            L_0x0068:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                r7 = 0
                android.database.CharArrayBuffer unused = r6.oldChars = r7     // Catch:{ all -> 0x0028 }
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                r7 = 0
                android.database.CharArrayBuffer unused = r6.mNewChars = r7     // Catch:{ all -> 0x0028 }
                monitor-exit(r5)     // Catch:{ all -> 0x0028 }
            L_0x007b:
                return
            L_0x007c:
                android.content.Intent r24 = new android.content.Intent     // Catch:{ all -> 0x0028 }
                java.lang.String r6 = "android.intent.action.DOWNLOAD_WAKEUP"
                r0 = r24
                r1 = r6
                r0.<init>(r1)     // Catch:{ all -> 0x0028 }
                java.lang.String r6 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getAppPackageName()     // Catch:{ all -> 0x0028 }
                java.lang.String r7 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadReceiverClassName()     // Catch:{ all -> 0x0028 }
                r0 = r24
                r1 = r6
                r2 = r7
                r0.setClassName(r1, r2)     // Catch:{ all -> 0x0028 }
                r6 = 0
                long r8 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0028 }
                long r8 = r8 + r40
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r7 = r0
                r10 = 0
                r11 = 1073741824(0x40000000, float:2.0)
                r0 = r7
                r1 = r10
                r2 = r24
                r3 = r11
                android.app.PendingIntent r7 = android.app.PendingIntent.getBroadcast(r0, r1, r2, r3)     // Catch:{ all -> 0x0028 }
                r14.set(r6, r8, r7)     // Catch:{ all -> 0x0028 }
                goto L_0x0068
            L_0x00b1:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this     // Catch:{ all -> 0x0028 }
                r6 = r0
                r7 = 0
                boolean unused = r6.mPendingUpdate = r7     // Catch:{ all -> 0x0028 }
                monitor-exit(r5)     // Catch:{ all -> 0x0028 }
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r28 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r5)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r29 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkRoaming(r5)
                long r33 = java.lang.System.currentTimeMillis()
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r6 = 0
                r5.handleNetworkStatusChange(r6)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                android.content.ContentResolver r5 = r5.getContentResolver()
                android.net.Uri r6 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
                r7 = 0
                java.lang.String r8 = "status != ?"
                r9 = 1
                java.lang.String[] r9 = new java.lang.String[r9]
                r10 = 0
                r11 = -100
                java.lang.String r11 = java.lang.String.valueOf(r11)
                r9[r10] = r11
                java.lang.String r10 = "_id"
                android.database.Cursor r6 = r5.query(r6, r7, r8, r9, r10)
                if (r6 == 0) goto L_0x007b
                r6.moveToFirst()
                r7 = 0
                r27 = 0
                r26 = 0
                r40 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
                boolean r25 = r6.isAfterLast()
                r23 = -1
                if (r25 != 0) goto L_0x0119
                java.lang.String r5 = "_id"
                int r23 = r6.getColumnIndexOrThrow(r5)
            L_0x0119:
                if (r25 == 0) goto L_0x012a
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                int r5 = r5.size()
                if (r7 >= r5) goto L_0x02f5
            L_0x012a:
                if (r25 == 0) goto L_0x0154
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.shouldScanFile(r7)
                if (r5 == 0) goto L_0x014b
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.mediaScannerConnected()
                if (r5 == 0) goto L_0x014b
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r8 = 0
                boolean unused = r5.scanFile(r8, r7)
            L_0x014b:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r5.deleteDownload(r7)
                goto L_0x0119
            L_0x0154:
                r0 = r6
                r1 = r23
                int r22 = r0.getInt(r1)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                int r5 = r5.size()
                if (r7 != r5) goto L_0x01d7
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r8 = r28
                r9 = r29
                r10 = r33
                r5.insertDownload(r6, r7, r8, r9, r10)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.shouldScanFile(r7)
                if (r5 == 0) goto L_0x019d
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.mediaScannerConnected()
                if (r5 == 0) goto L_0x0199
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.scanFile(r6, r7)
                if (r5 != 0) goto L_0x019d
            L_0x0199:
                r27 = 1
                r26 = 1
            L_0x019d:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.visibleNotification(r7)
                if (r5 == 0) goto L_0x01aa
                r26 = 1
            L_0x01aa:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r0 = r5
                r1 = r7
                r2 = r33
                long r31 = r0.nextAction(r1, r2)
                r8 = 0
                int r5 = (r31 > r8 ? 1 : (r31 == r8 ? 0 : -1))
                if (r5 != 0) goto L_0x01ca
                r26 = 1
            L_0x01bf:
                int r7 = r7 + 1
                r6.moveToNext()
                boolean r25 = r6.isAfterLast()
                goto L_0x0119
            L_0x01ca:
                r8 = 0
                int r5 = (r31 > r8 ? 1 : (r31 == r8 ? 0 : -1))
                if (r5 <= 0) goto L_0x01bf
                int r5 = (r31 > r40 ? 1 : (r31 == r40 ? 0 : -1))
                if (r5 >= 0) goto L_0x01bf
                r40 = r31
                goto L_0x01bf
            L_0x01d7:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                java.lang.Object r5 = r5.get(r7)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                int r15 = r5.mId
                r0 = r15
                r1 = r22
                if (r0 >= r1) goto L_0x0216
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.shouldScanFile(r7)
                if (r5 == 0) goto L_0x020c
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.mediaScannerConnected()
                if (r5 == 0) goto L_0x020c
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r8 = 0
                boolean unused = r5.scanFile(r8, r7)
            L_0x020c:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r5.deleteDownload(r7)
                goto L_0x0119
            L_0x0216:
                r0 = r15
                r1 = r22
                if (r0 != r1) goto L_0x0288
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r8 = r28
                r9 = r29
                r10 = r33
                r5.updateDownload(r6, r7, r8, r9, r10)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.shouldScanFile(r7)
                if (r5 == 0) goto L_0x024e
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.mediaScannerConnected()
                if (r5 == 0) goto L_0x024a
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.scanFile(r6, r7)
                if (r5 != 0) goto L_0x024e
            L_0x024a:
                r27 = 1
                r26 = 1
            L_0x024e:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.visibleNotification(r7)
                if (r5 == 0) goto L_0x025b
                r26 = 1
            L_0x025b:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r0 = r5
                r1 = r7
                r2 = r33
                long r31 = r0.nextAction(r1, r2)
                r8 = 0
                int r5 = (r31 > r8 ? 1 : (r31 == r8 ? 0 : -1))
                if (r5 != 0) goto L_0x027b
                r26 = 1
            L_0x0270:
                int r7 = r7 + 1
                r6.moveToNext()
                boolean r25 = r6.isAfterLast()
                goto L_0x0119
            L_0x027b:
                r8 = 0
                int r5 = (r31 > r8 ? 1 : (r31 == r8 ? 0 : -1))
                if (r5 <= 0) goto L_0x0270
                int r5 = (r31 > r40 ? 1 : (r31 == r40 ? 0 : -1))
                if (r5 >= 0) goto L_0x0270
                r40 = r31
                goto L_0x0270
            L_0x0288:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r8 = r28
                r9 = r29
                r10 = r33
                r5.insertDownload(r6, r7, r8, r9, r10)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.shouldScanFile(r7)
                if (r5 == 0) goto L_0x02bb
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.mediaScannerConnected()
                if (r5 == 0) goto L_0x02b7
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.scanFile(r6, r7)
                if (r5 != 0) goto L_0x02bb
            L_0x02b7:
                r27 = 1
                r26 = 1
            L_0x02bb:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                boolean r5 = r5.visibleNotification(r7)
                if (r5 == 0) goto L_0x02c8
                r26 = 1
            L_0x02c8:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r0 = r5
                r1 = r7
                r2 = r33
                long r31 = r0.nextAction(r1, r2)
                r8 = 0
                int r5 = (r31 > r8 ? 1 : (r31 == r8 ? 0 : -1))
                if (r5 != 0) goto L_0x02e8
                r26 = 1
            L_0x02dd:
                int r7 = r7 + 1
                r6.moveToNext()
                boolean r25 = r6.isAfterLast()
                goto L_0x0119
            L_0x02e8:
                r8 = 0
                int r5 = (r31 > r8 ? 1 : (r31 == r8 ? 0 : -1))
                if (r5 <= 0) goto L_0x02dd
                int r5 = (r31 > r40 ? 1 : (r31 == r40 ? 0 : -1))
                if (r5 >= 0) goto L_0x02dd
                r40 = r31
                goto L_0x02dd
            L_0x02f5:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                com.cplatform.android.cmsurfclient.download.provider.DownloadNotification r5 = r5.getDownloadNotifier()
                r5.updateAll()
                if (r27 == 0) goto L_0x0395
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                com.cplatform.android.cmsurfclient.download.provider.DownloadService$DownloadMediaScannerConnection r5 = r5.mMediaScannerConnection
                boolean r5 = r5.isConnected()
                if (r5 != 0) goto L_0x031e
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                com.cplatform.android.cmsurfclient.download.provider.DownloadService$DownloadMediaScannerConnection r5 = r5.mMediaScannerConnection
                r5.connect()
            L_0x031e:
                r6.close()
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                android.content.ContentResolver r8 = r5.getContentResolver()
                android.net.Uri r9 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
                r10 = 0
                java.lang.String r11 = "status = ?"
                r5 = 1
                java.lang.String[] r12 = new java.lang.String[r5]
                r5 = 0
                r6 = -100
                java.lang.String r6 = java.lang.String.valueOf(r6)
                r12[r5] = r6
                java.lang.String r13 = "_id"
                android.database.Cursor r39 = r8.query(r9, r10, r11, r12, r13)
                boolean r5 = r39.moveToFirst()
                if (r5 == 0) goto L_0x0418
                r35 = 0
                r21 = 0
            L_0x034d:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                int r5 = r5.size()
                r0 = r21
                r1 = r5
                if (r0 >= r1) goto L_0x03a3
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                r0 = r5
                r1 = r21
                java.lang.Object r5 = r0.get(r1)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                int r5 = r5.mStatus
                boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusCompleted(r5)
                if (r5 != 0) goto L_0x0392
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                r0 = r5
                r1 = r21
                java.lang.Object r5 = r0.get(r1)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                int r5 = r5.mControl
                r6 = 1
                if (r5 == r6) goto L_0x0392
                int r35 = r35 + 1
            L_0x0392:
                int r21 = r21 + 1
                goto L_0x034d
            L_0x0395:
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                com.cplatform.android.cmsurfclient.download.provider.DownloadService$DownloadMediaScannerConnection r5 = r5.mMediaScannerConnection
                r5.disconnect()
                goto L_0x031e
            L_0x03a3:
                int r5 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxConcurrentDownloadingTasks()
                r0 = r35
                r1 = r5
                if (r0 >= r1) goto L_0x0418
                r30 = 0
                int r5 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxDownloadingTasksPerDomain()
                r6 = 2147483647(0x7fffffff, float:NaN)
                if (r5 == r6) goto L_0x03bd
                int r5 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxDownloadingTasksPerDomain()
                if (r5 > 0) goto L_0x0424
            L_0x03bd:
                android.content.ContentValues r16 = new android.content.ContentValues
                r16.<init>()
                java.lang.String r5 = "status"
                r6 = 190(0xbe, float:2.66E-43)
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
                r0 = r16
                r1 = r5
                r2 = r6
                r0.put(r1, r2)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                android.content.ContentResolver r5 = r5.getContentResolver()
                android.net.Uri r6 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
                java.lang.String r7 = "_id"
                r0 = r39
                r1 = r7
                int r7 = r0.getColumnIndexOrThrow(r1)
                r0 = r39
                r1 = r7
                int r7 = r0.getInt(r1)
                long r8 = (long) r7
                android.net.Uri r6 = android.content.ContentUris.withAppendedId(r6, r8)
                r7 = 0
                r8 = 0
                r0 = r5
                r1 = r6
                r2 = r16
                r3 = r7
                r4 = r8
                r0.update(r1, r2, r3, r4)
                r30 = 1
                int r35 = r35 + 1
                int r5 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxConcurrentDownloadingTasks()
                r0 = r35
                r1 = r5
                if (r0 < r1) goto L_0x041d
            L_0x040b:
                if (r30 == 0) goto L_0x0418
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                r6 = 1
                boolean unused = r5.mPendingUpdate = r6
                r26 = 0
            L_0x0418:
                r39.close()
                goto L_0x000c
            L_0x041d:
                boolean r5 = r39.moveToNext()
                if (r5 != 0) goto L_0x03bd
                goto L_0x040b
            L_0x0424:
                java.util.HashMap r20 = new java.util.HashMap
                r20.<init>()
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                int r19 = r5.size()
                r21 = 0
            L_0x0438:
                r0 = r21
                r1 = r19
                if (r0 >= r1) goto L_0x04f5
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                r0 = r5
                r1 = r21
                java.lang.Object r5 = r0.get(r1)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                int r5 = r5.mStatus
                boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusCompleted(r5)
                if (r5 != 0) goto L_0x04b8
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                r0 = r5
                r1 = r21
                java.lang.Object r5 = r0.get(r1)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                int r5 = r5.mControl
                r6 = 1
                if (r5 == r6) goto L_0x04b8
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                r0 = r5
                r1 = r21
                java.lang.Object r5 = r0.get(r1)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                r0 = r5
                java.lang.String r0 = r0.mUriDomain
                r38 = r0
                boolean r5 = android.text.TextUtils.isEmpty(r38)
                if (r5 == 0) goto L_0x04bc
                java.lang.String r6 = "download/provider"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r7 = "Unknown domain for uri: "
                java.lang.StringBuilder r7 = r5.append(r7)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                java.util.ArrayList r5 = r5.mDownloads
                r0 = r5
                r1 = r21
                java.lang.Object r5 = r0.get(r1)
                com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r5 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r5
                java.lang.String r5 = r5.mUri
                java.lang.StringBuilder r5 = r7.append(r5)
                java.lang.String r5 = r5.toString()
                android.util.Log.e(r6, r5)
            L_0x04b8:
                int r21 = r21 + 1
                goto L_0x0438
            L_0x04bc:
                java.lang.String r38 = r38.toLowerCase()
                r0 = r20
                r1 = r38
                boolean r5 = r0.containsKey(r1)
                if (r5 == 0) goto L_0x04e7
                r0 = r20
                r1 = r38
                java.lang.Object r5 = r0.get(r1)
                java.lang.Integer r5 = (java.lang.Integer) r5
                int r5 = r5.intValue()
                int r18 = r5 + 1
                java.lang.Integer r5 = java.lang.Integer.valueOf(r18)
                r0 = r20
                r1 = r38
                r2 = r5
                r0.put(r1, r2)
                goto L_0x04b8
            L_0x04e7:
                r5 = 1
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                r0 = r20
                r1 = r38
                r2 = r5
                r0.put(r1, r2)
                goto L_0x04b8
            L_0x04f5:
                java.lang.String r5 = "control"
                r0 = r39
                r1 = r5
                int r5 = r0.getColumnIndex(r1)
                r0 = r39
                r1 = r5
                int r5 = r0.getInt(r1)
                r6 = 1
                if (r5 != r6) goto L_0x050f
                boolean r5 = r39.moveToNext()
                if (r5 == 0) goto L_0x040b
                goto L_0x04f5
            L_0x050f:
                java.lang.String r5 = "uri"
                r0 = r39
                r1 = r5
                int r5 = r0.getColumnIndexOrThrow(r1)
                r0 = r39
                r1 = r5
                java.lang.String r5 = r0.getString(r1)
                java.lang.String r37 = com.cplatform.android.cmsurfclient.download.provider.DownloadInfo.getUriDomain(r5)
                boolean r5 = android.text.TextUtils.isEmpty(r37)
                if (r5 == 0) goto L_0x0530
                boolean r5 = r39.moveToNext()
                if (r5 == 0) goto L_0x040b
                goto L_0x04f5
            L_0x0530:
                java.lang.String r37 = r37.toLowerCase()
                r0 = r20
                r1 = r37
                boolean r5 = r0.containsKey(r1)
                if (r5 == 0) goto L_0x055c
                r0 = r20
                r1 = r37
                java.lang.Object r5 = r0.get(r1)
                java.lang.Integer r5 = (java.lang.Integer) r5
                int r36 = r5.intValue()
            L_0x054c:
                int r5 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxDownloadingTasksPerDomain()
                r0 = r36
                r1 = r5
                if (r0 < r1) goto L_0x055f
                boolean r5 = r39.moveToNext()
                if (r5 == 0) goto L_0x040b
                goto L_0x04f5
            L_0x055c:
                r36 = 0
                goto L_0x054c
            L_0x055f:
                android.content.ContentValues r17 = new android.content.ContentValues
                r17.<init>()
                java.lang.String r5 = "status"
                r6 = 190(0xbe, float:2.66E-43)
                java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
                r0 = r17
                r1 = r5
                r2 = r6
                r0.put(r1, r2)
                r0 = r42
                com.cplatform.android.cmsurfclient.download.provider.DownloadService r0 = com.cplatform.android.cmsurfclient.download.provider.DownloadService.this
                r5 = r0
                android.content.ContentResolver r5 = r5.getContentResolver()
                android.net.Uri r6 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
                java.lang.String r7 = "_id"
                r0 = r39
                r1 = r7
                int r7 = r0.getColumnIndexOrThrow(r1)
                r0 = r39
                r1 = r7
                int r7 = r0.getInt(r1)
                long r8 = (long) r7
                android.net.Uri r6 = android.content.ContentUris.withAppendedId(r6, r8)
                r7 = 0
                r8 = 0
                r0 = r5
                r1 = r6
                r2 = r17
                r3 = r7
                r4 = r8
                r0.update(r1, r2, r3, r4)
                int r35 = r35 + 1
                r30 = 1
                int r5 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getMaxConcurrentDownloadingTasks()
                r0 = r35
                r1 = r5
                if (r0 >= r1) goto L_0x040b
                r0 = r20
                r1 = r37
                boolean r5 = r0.containsKey(r1)
                if (r5 == 0) goto L_0x05db
                r0 = r20
                r1 = r37
                java.lang.Object r5 = r0.get(r1)
                java.lang.Integer r5 = (java.lang.Integer) r5
                int r5 = r5.intValue()
                int r18 = r5 + 1
                java.lang.Integer r5 = java.lang.Integer.valueOf(r18)
                r0 = r20
                r1 = r37
                r2 = r5
                r0.put(r1, r2)
            L_0x05d3:
                boolean r5 = r39.moveToNext()
                if (r5 == 0) goto L_0x040b
                goto L_0x04f5
            L_0x05db:
                r5 = 1
                java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                r0 = r20
                r1 = r37
                r2 = r5
                r0.put(r1, r2)
                goto L_0x05d3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.DownloadService.UpdateThread.run():void");
        }

        public UpdateThread() {
            super("Download Service");
        }
    }

    public class DownloadMediaScannerConnection extends MediaScannerConnection {
        private DownloadMediaScannerConnectionClient mClient;

        public boolean goScanFile(String s, String s1) {
            synchronized (this.mClient) {
                scanFile(s, s1);
            }
            return true;
        }

        public void onServiceConnected(ComponentName componentname, IBinder ibinder) {
            super.onServiceConnected(componentname, ibinder);
            synchronized (DownloadService.this) {
                DownloadService.this.updateFromProvider();
            }
        }

        public void onServiceDisconnected(ComponentName componentname) {
            super.onServiceDisconnected(componentname);
        }

        public DownloadMediaScannerConnection(Context context, DownloadMediaScannerConnectionClient downloadmediascannerconnectionclient) {
            super(context, downloadmediascannerconnectionclient);
            this.mClient = downloadmediascannerconnectionclient;
        }
    }

    public class DownloadMediaScannerConnectionClient implements MediaScannerConnection.MediaScannerConnectionClient {
        public boolean mScanCompleted;

        public void onMediaScannerConnected() {
        }

        public void onScanCompleted(String s, Uri uri) {
        }

        public DownloadMediaScannerConnectionClient() {
        }
    }

    class DownloadManagerContentObserver extends ContentObserver {
        public void onChange(boolean flag) {
            DownloadService.this.updateFromProvider();
        }

        public DownloadManagerContentObserver() {
            super(new Handler());
        }
    }

    /* access modifiers changed from: private */
    public void deleteDownload(int arrayPos) {
        DownloadInfo info = this.mDownloads.get(arrayPos);
        if (info.mStatus == 192) {
            info.mStatus = Downloads.STATUS_CANCELED;
        } else if (!(info.mDestination == 0 || info.mFileName == null)) {
            new File(info.mFileName).delete();
        }
        getDownloadNotifier().cancel(info.mId);
        this.mDownloads.remove(arrayPos);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    public void handleNetworkStatusChange(boolean flag) {
        synchronized (this) {
            try {
                if (DownloadSettings.getPauseDownloadWhenWifiDisconnected()) {
                    if (!flag) {
                        if (Helpers.isNetworkAvailable(this)) {
                            boolean wifiConnected = this.mWifiConnected;
                            this.mWifiConnected = Helpers.isWifiAvailable(this);
                            if (this.mWifiConnected != wifiConnected) {
                                Log.v(Constants.TAG, "Wifi status is changed: " + this.mWifiConnected);
                            }
                            if (!this.mWifiConnected && wifiConnected) {
                                ContentValues contentvalues = new ContentValues();
                                contentvalues.put(Downloads.COLUMN_CONTROL, (Integer) 1);
                                int i = getContentResolver().update(DownloadSettings.getDownloadProviderContentUri(), contentvalues, String.format("(%s) OR (%s)", DownloadNotification.WHERE_IN_QUEUE, DownloadNotification.WHERE_RUNNING), null);
                                if (i > 0) {
                                    Intent intent = new Intent(Downloads.ACTION_DOWNLOAD_ALL_PAUSED_WIFI_DISCONNECTED);
                                    intent.setClassName(DownloadSettings.getAppPackageName(), DownloadSettings.getDownloadReceiverClassName());
                                    intent.putExtra(Downloads.EXTRA_PAUSED_TASKS_COUNT, i);
                                    sendBroadcast(intent);
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void insertDownload(Cursor cursor, int i, boolean flag, boolean flag1, long l) {
        boolean z;
        boolean z2;
        int retryRedirect = cursor.getInt(cursor.getColumnIndexOrThrow(Constants.RETRY_AFTER_X_REDIRECT_COUNT));
        int i2 = cursor.getInt(cursor.getColumnIndexOrThrow(QueryApList.Carriers._ID));
        String string = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_URI));
        if (cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NO_INTEGRITY)) == 1) {
            z = true;
        } else {
            z = false;
        }
        String string2 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_FILE_NAME_HINT));
        String string3 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads._DATA));
        String string4 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_MIME_TYPE));
        int i3 = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DESTINATION));
        int i4 = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_VISIBILITY));
        int i5 = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CONTROL));
        int i6 = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_STATUS));
        int i7 = cursor.getInt(cursor.getColumnIndexOrThrow(Constants.FAILED_CONNECTIONS));
        int i8 = 268435455 & retryRedirect;
        int i9 = retryRedirect >> 28;
        long j = cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_LAST_MODIFICATION));
        String string5 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NOTIFICATION_PACKAGE));
        String string6 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NOTIFICATION_CLASS));
        String string7 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NOTIFICATION_EXTRAS));
        String string8 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_COOKIE_DATA));
        String string9 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_USER_AGENT));
        String string10 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_REFERER));
        int i10 = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_TOTAL_BYTES));
        int i11 = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CURRENT_BYTES));
        long j2 = cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CURRENT_ELAPSED_TIME));
        String string11 = cursor.getString(cursor.getColumnIndexOrThrow(Constants.ETAG));
        String string12 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_IF_RANGE_ID));
        if (cursor.getInt(cursor.getColumnIndexOrThrow(Constants.MEDIA_SCANNED)) == 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        DownloadInfo downloadinfo = new DownloadInfo(i2, string, z, string2, string3, string4, i3, i4, i5, i6, i7, i8, i9, j, string5, string6, string7, string8, string9, string10, i10, i11, j2, string11, string12, z2, cursor.getString(cursor.getColumnIndex(Downloads.COLUMN_SUB_DIRECTORY)), cursor.getString(cursor.getColumnIndex(Downloads.COLUMN_APPOINT_NAME)));
        this.mDownloads.add(i, downloadinfo);
        if (downloadinfo.mStatus == 0 && ((downloadinfo.mDestination == 0 || downloadinfo.mDestination == 2) && downloadinfo.mMimeType != null)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromParts("file", WindowAdapter2.BLANK_URL, null), downloadinfo.mMimeType);
            if (getPackageManager().resolveActivity(intent, 65536) == null) {
                Log.d(Constants.TAG, "no application to handle MIME type " + downloadinfo.mMimeType);
                downloadinfo.mStatus = Downloads.STATUS_NOT_ACCEPTABLE;
                Uri uri = ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), (long) downloadinfo.mId);
                ContentValues contentvalues = new ContentValues();
                contentvalues.put(Downloads.COLUMN_STATUS, Integer.valueOf((int) Downloads.STATUS_NOT_ACCEPTABLE));
                getContentResolver().update(uri, contentvalues, null, null);
                downloadinfo.sendIntentIfRequested(uri, this);
                return;
            }
        }
        if (downloadinfo.canUseNetwork(flag, flag1)) {
            if (!downloadinfo.isReadyToStart(l)) {
                return;
            }
            if (downloadinfo.mHasActiveThread) {
                throw new IllegalStateException("Multiple threads on same download on insert");
            }
            if (downloadinfo.mStatus != 192) {
                downloadinfo.mStatus = Downloads.STATUS_RUNNING;
                ContentValues contentValues = new ContentValues();
                contentValues.put(Downloads.COLUMN_STATUS, Integer.valueOf(downloadinfo.mStatus));
                getContentResolver().update(ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), (long) downloadinfo.mId), contentValues, null, null);
            }
            DownloadThread downloadThread = new DownloadThread(this, downloadinfo);
            downloadinfo.mHasActiveThread = true;
            downloadThread.start();
        } else if (downloadinfo.mStatus == 0 || downloadinfo.mStatus == 190 || downloadinfo.mStatus == 192) {
            downloadinfo.mStatus = Downloads.STATUS_RUNNING_PAUSED;
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put(Downloads.COLUMN_STATUS, Integer.valueOf((int) Downloads.STATUS_RUNNING_PAUSED));
            getContentResolver().update(ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), (long) downloadinfo.mId), contentValues2, null, null);
        }
    }

    /* access modifiers changed from: private */
    public final boolean mediaScannerConnected() {
        return this.mMediaScannerConnection.isConnected();
    }

    /* access modifiers changed from: private */
    public long nextAction(int arrayPos, long now) {
        DownloadInfo info = this.mDownloads.get(arrayPos);
        if (Downloads.isStatusCompleted(info.mStatus)) {
            return -1;
        }
        if (info.mStatus != 193) {
            return 0;
        }
        if (info.mNumFailed == 0) {
            return 0;
        }
        long when = info.restartTime();
        if (when <= now) {
            return 0;
        }
        return when - now;
    }

    private void removeSpuriousFiles() {
        File[] files = Environment.getDownloadCacheDirectory().listFiles();
        if (files != null) {
            HashSet<String> fileSet = new HashSet<>();
            for (int i = 0; i < files.length; i++) {
                if (!files[i].getName().equals(Constants.KNOWN_SPURIOUS_FILENAME) && !files[i].getName().equalsIgnoreCase(Constants.RECOVERY_DIRECTORY)) {
                    fileSet.add(files[i].getPath());
                }
            }
            Cursor cursor = getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{Downloads._DATA}, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        fileSet.remove(cursor.getString(0));
                    } while (cursor.moveToNext());
                }
                cursor.close();
            }
            Iterator<String> iterator = fileSet.iterator();
            while (iterator.hasNext()) {
                new File((String) iterator.next()).delete();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean scanFile(android.database.Cursor r9, int r10) {
        /*
            r8 = this;
            r7 = 1
            java.util.ArrayList<com.cplatform.android.cmsurfclient.download.provider.DownloadInfo> r3 = r8.mDownloads
            java.lang.Object r2 = r3.get(r10)
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r2 = (com.cplatform.android.cmsurfclient.download.provider.DownloadInfo) r2
            monitor-enter(r8)
            com.cplatform.android.cmsurfclient.download.provider.DownloadService$DownloadMediaScannerConnection r3 = r8.mMediaScannerConnection     // Catch:{ all -> 0x006c }
            boolean r3 = r3.isConnected()     // Catch:{ all -> 0x006c }
            if (r3 == 0) goto L_0x0069
            com.cplatform.android.cmsurfclient.download.provider.DownloadService$DownloadMediaScannerConnection r3 = r8.mMediaScannerConnection     // Catch:{ Exception -> 0x004d }
            java.lang.String r4 = r2.mFileName     // Catch:{ Exception -> 0x004d }
            java.lang.String r5 = r2.mMimeType     // Catch:{ Exception -> 0x004d }
            boolean r3 = r3.goScanFile(r4, r5)     // Catch:{ Exception -> 0x004d }
            if (r3 == 0) goto L_0x0069
            if (r9 == 0) goto L_0x004a
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ Exception -> 0x004d }
            r0.<init>()     // Catch:{ Exception -> 0x004d }
            java.lang.String r3 = "scanned"
            r4 = 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x004d }
            r0.put(r3, r4)     // Catch:{ Exception -> 0x004d }
            android.content.ContentResolver r3 = r8.getContentResolver()     // Catch:{ Exception -> 0x004d }
            android.net.Uri r4 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()     // Catch:{ Exception -> 0x004d }
            java.lang.String r5 = "_id"
            int r5 = r9.getColumnIndexOrThrow(r5)     // Catch:{ Exception -> 0x004d }
            long r5 = r9.getLong(r5)     // Catch:{ Exception -> 0x004d }
            android.net.Uri r4 = android.content.ContentUris.withAppendedId(r4, r5)     // Catch:{ Exception -> 0x004d }
            r5 = 0
            r6 = 0
            r3.update(r4, r0, r5, r6)     // Catch:{ Exception -> 0x004d }
        L_0x004a:
            monitor-exit(r8)     // Catch:{ all -> 0x006c }
            r3 = r7
        L_0x004c:
            return r3
        L_0x004d:
            r3 = move-exception
            r1 = r3
            java.lang.String r3 = "download/provider"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x006c }
            r4.<init>()     // Catch:{ all -> 0x006c }
            java.lang.String r5 = "Failed to scan file "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006c }
            java.lang.String r5 = r2.mFileName     // Catch:{ all -> 0x006c }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x006c }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x006c }
            android.util.Log.d(r3, r4)     // Catch:{ all -> 0x006c }
        L_0x0069:
            monitor-exit(r8)     // Catch:{ all -> 0x006c }
            r3 = 0
            goto L_0x004c
        L_0x006c:
            r3 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x006c }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.DownloadService.scanFile(android.database.Cursor, int):boolean");
    }

    /* access modifiers changed from: private */
    public boolean shouldScanFile(int arrayPos) {
        DownloadInfo info = this.mDownloads.get(arrayPos);
        return !info.mMediaScanned && info.mDestination == 0 && Downloads.isStatusSuccess(info.mStatus);
    }

    private String stringFromCursor(String old, Cursor cursor, String column) {
        int index = cursor.getColumnIndexOrThrow(column);
        if (old == null) {
            return cursor.getString(index);
        }
        if (this.mNewChars == null) {
            this.mNewChars = new CharArrayBuffer(128);
        }
        cursor.copyStringToBuffer(index, this.mNewChars);
        int length = this.mNewChars.sizeCopied;
        if (length != old.length()) {
            return cursor.getString(index);
        }
        if (this.oldChars == null || this.oldChars.sizeCopied < length) {
            this.oldChars = new CharArrayBuffer(length);
        }
        char[] oldArray = this.oldChars.data;
        char[] newArray = this.mNewChars.data;
        old.getChars(0, length, oldArray, 0);
        for (int i = length - 1; i >= 0; i--) {
            if (oldArray[i] != newArray[i]) {
                return new String(newArray, 0, length);
            }
        }
        return old;
    }

    private void trimDatabase() {
        Cursor cursor = getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID}, DownloadNotification.WHERE_COMPLETED_ALL, null, Downloads.COLUMN_LAST_MODIFICATION);
        if (cursor == null) {
            Log.e(Constants.TAG, "null cursor in trimDatabase");
            return;
        }
        if (cursor.moveToFirst()) {
            int columnId = cursor.getColumnIndexOrThrow(QueryApList.Carriers._ID);
            for (int numDelete = cursor.getCount() - Constants.MAX_DOWNLOADS; numDelete > 0; numDelete--) {
                getContentResolver().delete(ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), cursor.getLong(columnId)), null, null);
                if (!cursor.moveToNext()) {
                    break;
                }
            }
        }
        cursor.close();
    }

    /* access modifiers changed from: private */
    public void updateDownload(Cursor cursor, int i, boolean flag, boolean flag1, long l) {
        DownloadInfo downloadinfo = this.mDownloads.get(i);
        int k = cursor.getColumnIndexOrThrow(Downloads.COLUMN_STATUS);
        int failedColumn = cursor.getColumnIndexOrThrow(Constants.FAILED_CONNECTIONS);
        downloadinfo.mId = cursor.getInt(cursor.getColumnIndexOrThrow(QueryApList.Carriers._ID));
        downloadinfo.mUri = stringFromCursor(downloadinfo.mUri, cursor, Downloads.COLUMN_URI);
        downloadinfo.mUriDomain = DownloadInfo.getUriDomain(downloadinfo.mUri);
        downloadinfo.mNoIntegrity = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NO_INTEGRITY)) == 1;
        downloadinfo.mHint = stringFromCursor(downloadinfo.mHint, cursor, Downloads.COLUMN_FILE_NAME_HINT);
        downloadinfo.mFileName = stringFromCursor(downloadinfo.mFileName, cursor, Downloads._DATA);
        downloadinfo.mMimeType = stringFromCursor(downloadinfo.mMimeType, cursor, Downloads.COLUMN_MIME_TYPE);
        downloadinfo.mDestination = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_DESTINATION));
        int newVisibility = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_VISIBILITY));
        if (downloadinfo.mVisibility == 1 && newVisibility != 1 && Downloads.isStatusCompleted(downloadinfo.mStatus)) {
            getDownloadNotifier().cancel(downloadinfo.mId);
        }
        downloadinfo.mVisibility = newVisibility;
        synchronized (downloadinfo) {
            downloadinfo.mControl = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CONTROL));
        }
        int newStatus = cursor.getInt(k);
        if (!Downloads.isStatusCompleted(downloadinfo.mStatus) && Downloads.isStatusCompleted(newStatus)) {
            getDownloadNotifier().cancel(downloadinfo.mId);
        }
        downloadinfo.mStatus = newStatus;
        downloadinfo.mNumFailed = cursor.getInt(failedColumn);
        int retryRedirect = cursor.getInt(cursor.getColumnIndexOrThrow(Constants.RETRY_AFTER_X_REDIRECT_COUNT));
        downloadinfo.mRetryAfter = 268435455 & retryRedirect;
        downloadinfo.mRedirectCount = retryRedirect >> 28;
        downloadinfo.mLastMod = cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_LAST_MODIFICATION));
        downloadinfo.mPackage = stringFromCursor(downloadinfo.mPackage, cursor, Downloads.COLUMN_NOTIFICATION_PACKAGE);
        downloadinfo.mClass = stringFromCursor(downloadinfo.mClass, cursor, Downloads.COLUMN_NOTIFICATION_CLASS);
        downloadinfo.mCookies = stringFromCursor(downloadinfo.mCookies, cursor, Downloads.COLUMN_COOKIE_DATA);
        downloadinfo.mUserAgent = stringFromCursor(downloadinfo.mUserAgent, cursor, Downloads.COLUMN_USER_AGENT);
        downloadinfo.mReferer = stringFromCursor(downloadinfo.mReferer, cursor, Downloads.COLUMN_REFERER);
        downloadinfo.mTotalBytes = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_TOTAL_BYTES));
        downloadinfo.mCurrentBytes = cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CURRENT_BYTES));
        downloadinfo.mCurrentElapsedTime = cursor.getLong(cursor.getColumnIndexOrThrow(Downloads.COLUMN_CURRENT_ELAPSED_TIME));
        downloadinfo.mETag = stringFromCursor(downloadinfo.mETag, cursor, Constants.ETAG);
        downloadinfo.mIfRangeId = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_IF_RANGE_ID));
        downloadinfo.mMediaScanned = cursor.getInt(cursor.getColumnIndexOrThrow(Constants.MEDIA_SCANNED)) == 1;
        downloadinfo.mSubDirectory = cursor.getString(cursor.getColumnIndex(Downloads.COLUMN_SUB_DIRECTORY));
        downloadinfo.mAppointName = cursor.getString(cursor.getColumnIndex(Downloads.COLUMN_APPOINT_NAME));
        if (downloadinfo.canUseNetwork(flag, flag1) && downloadinfo.isReadyToRestart(l)) {
            if (downloadinfo.mHasActiveThread) {
                throw new IllegalStateException("Multiple threads on same download on update");
            }
            downloadinfo.mStatus = Downloads.STATUS_RUNNING;
            ContentValues contentvalues = new ContentValues();
            contentvalues.put(Downloads.COLUMN_STATUS, Integer.valueOf(downloadinfo.mStatus));
            getContentResolver().update(ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), (long) downloadinfo.mId), contentvalues, null, null);
            DownloadThread downloadThread = new DownloadThread(this, downloadinfo);
            downloadinfo.mHasActiveThread = true;
            downloadThread.start();
        }
    }

    /* access modifiers changed from: private */
    public void updateFromProvider() {
        synchronized (this) {
            this.mPendingUpdate = true;
            if (this.mUpdateThread == null) {
                this.mUpdateThread = new UpdateThread();
                this.mUpdateThread.start();
            }
        }
    }

    /* access modifiers changed from: private */
    public boolean visibleNotification(int arrayPos) {
        return this.mDownloads.get(arrayPos).hasCompletionNotification();
    }

    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Cannot bind to Download Manager Service");
    }

    public void onCreate() {
        this.mNotifier = new DownloadNotification(this);
        super.onCreate();
        DownloadSettings.initializeDownloadWhenWifiDisconnected(getApplicationContext());
        this.mDownloads = new ArrayList<>();
        this.mObserver = new DownloadManagerContentObserver();
        getContentResolver().registerContentObserver(DownloadSettings.getDownloadProviderContentUri(), true, this.mObserver);
        this.mMediaScannerConnection = new DownloadMediaScannerConnection(getApplicationContext(), new DownloadMediaScannerConnectionClient());
        getDownloadNotifier().cancelAll();
        getDownloadNotifier().updateAll();
        trimDatabase();
        removeSpuriousFiles();
        updateFromProvider();
    }

    public void onDestroy() {
        getContentResolver().unregisterContentObserver(this.mObserver);
        handleNetworkStatusChange(true);
        super.onDestroy();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        updateFromProvider();
    }
}
