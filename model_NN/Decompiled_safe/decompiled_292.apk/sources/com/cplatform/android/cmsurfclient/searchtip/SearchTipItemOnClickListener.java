package com.cplatform.android.cmsurfclient.searchtip;

import android.app.Activity;
import android.view.View;
import com.cplatform.android.cmsurfclient.naviedit.NaviEditSearchActivity;

public class SearchTipItemOnClickListener implements View.OnClickListener {
    public Activity mActivity;
    public SearchTipItem mItem;

    public SearchTipItemOnClickListener(Activity activity, SearchTipItem item) {
        this.mActivity = activity;
        this.mItem = item;
    }

    public void onClick(View v) {
        ((NaviEditSearchActivity) this.mActivity).onClick(this.mItem);
    }
}
