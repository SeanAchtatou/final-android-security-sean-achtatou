package com.cplatform.android.cmsurfclient.share;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

public class ShareItemList extends LinearLayout {
    SharePageAdapter mAdapter;

    public ShareItemList(Context context) {
        super(context);
    }

    public ShareItemList(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setAdapter(SharePageAdapter adapter) {
        this.mAdapter = adapter;
        removeAllViews();
        for (int i = 0; i < adapter.getCount(); i++) {
            Log.e("ShareItemList", "setAdapter: index=" + i);
            addView(adapter.getView(i, null, null), i);
        }
    }
}
