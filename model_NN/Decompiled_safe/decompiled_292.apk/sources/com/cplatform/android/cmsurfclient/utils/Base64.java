package com.cplatform.android.cmsurfclient.utils;

import com.cplatform.android.cmsurfclient.download.provider.Downloads;

public class Base64 {
    private static byte[] base64DecodeChars = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -1, -1, -1, -1, -1};
    private static char[] base64EncodeChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    private Base64() {
    }

    public static String encode(byte[] data) {
        StringBuffer sb = new StringBuffer();
        int len = data.length;
        int i = 0;
        while (true) {
            if (i >= len) {
                break;
            }
            int i2 = i + 1;
            int b1 = data[i] & 255;
            if (i2 == len) {
                sb.append(base64EncodeChars[b1 >>> 2]);
                sb.append(base64EncodeChars[(b1 & 3) << 4]);
                sb.append("==");
                break;
            }
            int i3 = i2 + 1;
            int b2 = data[i2] & 255;
            if (i3 == len) {
                sb.append(base64EncodeChars[b1 >>> 2]);
                sb.append(base64EncodeChars[((b1 & 3) << 4) | ((b2 & 240) >>> 4)]);
                sb.append(base64EncodeChars[(b2 & 15) << 2]);
                sb.append("=");
                break;
            }
            int b3 = data[i3] & 255;
            sb.append(base64EncodeChars[b1 >>> 2]);
            sb.append(base64EncodeChars[((b1 & 3) << 4) | ((b2 & 240) >>> 4)]);
            sb.append(base64EncodeChars[((b2 & 15) << 2) | ((b3 & Downloads.STATUS_RUNNING) >>> 6)]);
            sb.append(base64EncodeChars[b3 & 63]);
            i = i3 + 1;
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0085 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x005e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0022 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] decode(java.lang.String r13) {
        /*
            r12 = 61
            r11 = -1
            byte[] r5 = r13.getBytes()
            int r8 = r5.length
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>(r8)
            r6 = 0
            r0 = -1
            r1 = -1
            r2 = -1
            r3 = -1
        L_0x0012:
            if (r6 >= r8) goto L_0x0023
        L_0x0014:
            byte[] r9 = com.cplatform.android.cmsurfclient.utils.Base64.base64DecodeChars
            int r7 = r6 + 1
            byte r10 = r5[r6]
            byte r0 = r9[r10]
            if (r7 >= r8) goto L_0x0020
            if (r0 == r11) goto L_0x0095
        L_0x0020:
            if (r0 != r11) goto L_0x0028
            r6 = r7
        L_0x0023:
            byte[] r9 = r4.toByteArray()
        L_0x0027:
            return r9
        L_0x0028:
            if (r7 >= r8) goto L_0x0045
        L_0x002a:
            r6 = r7
            byte[] r9 = com.cplatform.android.cmsurfclient.utils.Base64.base64DecodeChars
            int r7 = r6 + 1
            byte r10 = r5[r6]
            byte r1 = r9[r10]
            if (r7 >= r8) goto L_0x0037
            if (r1 == r11) goto L_0x002a
        L_0x0037:
            if (r1 != r11) goto L_0x003b
            r6 = r7
            goto L_0x0023
        L_0x003b:
            int r9 = r0 << 2
            r10 = r1 & 48
            int r10 = r10 >>> 4
            r9 = r9 | r10
            r4.write(r9)
        L_0x0045:
            r6 = r7
            if (r6 >= r8) goto L_0x006d
        L_0x0048:
            int r7 = r6 + 1
            byte r2 = r5[r6]
            if (r2 != r12) goto L_0x0054
            byte[] r9 = r4.toByteArray()
            r6 = r7
            goto L_0x0027
        L_0x0054:
            byte[] r9 = com.cplatform.android.cmsurfclient.utils.Base64.base64DecodeChars
            byte r2 = r9[r2]
            if (r7 >= r8) goto L_0x005c
            if (r2 == r11) goto L_0x0093
        L_0x005c:
            if (r2 != r11) goto L_0x0060
            r6 = r7
            goto L_0x0023
        L_0x0060:
            r9 = r1 & 15
            int r9 = r9 << 4
            r10 = r2 & 60
            int r10 = r10 >>> 2
            r9 = r9 | r10
            r4.write(r9)
            r6 = r7
        L_0x006d:
            if (r6 >= r8) goto L_0x0012
        L_0x006f:
            int r7 = r6 + 1
            byte r3 = r5[r6]
            if (r3 != r12) goto L_0x007b
            byte[] r9 = r4.toByteArray()
            r6 = r7
            goto L_0x0027
        L_0x007b:
            byte[] r9 = com.cplatform.android.cmsurfclient.utils.Base64.base64DecodeChars
            byte r3 = r9[r3]
            if (r7 >= r8) goto L_0x0083
            if (r3 == r11) goto L_0x0091
        L_0x0083:
            if (r3 != r11) goto L_0x0087
            r6 = r7
            goto L_0x0023
        L_0x0087:
            r9 = r2 & 3
            int r9 = r9 << 6
            r9 = r9 | r3
            r4.write(r9)
            r6 = r7
            goto L_0x0012
        L_0x0091:
            r6 = r7
            goto L_0x006f
        L_0x0093:
            r6 = r7
            goto L_0x0048
        L_0x0095:
            r6 = r7
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.utils.Base64.decode(java.lang.String):byte[]");
    }
}
