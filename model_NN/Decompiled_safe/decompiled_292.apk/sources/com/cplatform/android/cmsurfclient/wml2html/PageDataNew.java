package com.cplatform.android.cmsurfclient.wml2html;

import com.cplatform.android.cmsurfclient.SurfWebView;

public class PageDataNew {
    SurfWebView handler = null;

    public PageDataNew(SurfWebView handler2) {
        this.handler = handler2;
    }

    public void setPage(String text, String html) {
        if (this.handler != null) {
            this.handler.onPageData(text, html);
        }
    }
}
