package com.cplatform.android.cmsurfclient.quicklink;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

public class QuickLinkHelper {
    private final String DEBUG_TAG = "QuickLinkHelper";
    private final String DEFAULT_SNAPSHOT = "quicklink.png";
    private final String QUICKLINK_TAG = "quicklink";
    private Context mContext;
    public ArrayList<QuickLinkItem> mItems;

    public QuickLinkHelper(Context context) {
        this.mContext = context;
        this.mItems = new ArrayList<>();
    }

    public void load() {
        this.mItems.clear();
        if (!loadXML() && loadBaseXML()) {
            saveXML();
        }
    }

    public boolean loadBaseXML() {
        DocumentBuilder docBuilder;
        DocumentBuilderFactory docBuilderFactory;
        DocumentBuilderFactory docBuilderFactory2;
        try {
            NodeList nodeList = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.mContext.getResources().getAssets().open("quicklink.xml")).getDocumentElement().getElementsByTagName("quicklink");
            for (int i = 0; i < nodeList.getLength(); i++) {
                NamedNodeMap attrMap = nodeList.item(i).getAttributes();
                Node attrTitle = attrMap.getNamedItem(Downloads.COLUMN_TITLE);
                Node attrUrl = attrMap.getNamedItem("url");
                Node attrImage = attrMap.getNamedItem("image");
                Node attrFlag = attrMap.getNamedItem("flag");
                this.mItems.add(new QuickLinkItem(i, Integer.parseInt(attrFlag.getNodeValue()), attrTitle.getNodeValue(), attrUrl.getNodeValue(), attrImage.getNodeValue()));
            }
            return true;
        } catch (IOException e) {
            return false;
        } catch (SAXException e2) {
            return false;
        } catch (ParserConfigurationException e3) {
            return false;
        } finally {
        }
    }

    public boolean loadXML() {
        DocumentBuilder docBuilder;
        DocumentBuilderFactory docBuilderFactory;
        DocumentBuilderFactory docBuilderFactory2;
        try {
            NodeList nodeList = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(this.mContext.openFileInput("quicklink.xml")).getDocumentElement().getElementsByTagName("quicklink");
            for (int i = 0; i < nodeList.getLength(); i++) {
                NamedNodeMap attrMap = nodeList.item(i).getAttributes();
                Node attrTitle = attrMap.getNamedItem(Downloads.COLUMN_TITLE);
                Node attrUrl = attrMap.getNamedItem("url");
                Node attrImage = attrMap.getNamedItem("image");
                Node attrFlag = attrMap.getNamedItem("flag");
                String title = attrTitle.getNodeValue();
                String url = attrUrl.getNodeValue();
                String image = attrImage.getNodeValue();
                String flag = attrFlag.getNodeValue();
                if (flag.equalsIgnoreCase("2")) {
                    flag = "1";
                    image = "quicklink.png";
                }
                this.mItems.add(new QuickLinkItem(i, Integer.parseInt(flag), title, url, image));
            }
            return true;
        } catch (IOException e) {
            return false;
        } catch (SAXException e2) {
            return false;
        } catch (ParserConfigurationException e3) {
            return false;
        } finally {
        }
    }

    public boolean saveXML() {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag(WindowAdapter2.BLANK_URL, "quicklinks");
            for (int i = 0; i < this.mItems.size(); i++) {
                QuickLinkItem item = this.mItems.get(i);
                serializer.startTag(WindowAdapter2.BLANK_URL, "quicklink");
                serializer.attribute(WindowAdapter2.BLANK_URL, "flag", String.valueOf(item.mFlag));
                serializer.attribute(WindowAdapter2.BLANK_URL, Downloads.COLUMN_TITLE, item.mTitle);
                serializer.attribute(WindowAdapter2.BLANK_URL, "url", item.mUrl);
                serializer.attribute(WindowAdapter2.BLANK_URL, "image", item.mImage);
                serializer.endTag(WindowAdapter2.BLANK_URL, "quicklink");
            }
            serializer.endTag(WindowAdapter2.BLANK_URL, "quicklinks");
            serializer.endDocument();
            try {
                OutputStream os = this.mContext.openFileOutput("quicklink.xml", 0);
                OutputStreamWriter osw = new OutputStreamWriter(os);
                osw.write(writer.toString());
                osw.close();
                os.close();
                return true;
            } catch (FileNotFoundException e) {
                return false;
            } catch (IOException e2) {
                return false;
            }
        } catch (Exception e3) {
            throw new RuntimeException(e3);
        }
    }

    public boolean add(String title, String url, String image) {
        for (int i = 0; i < this.mItems.size(); i++) {
            QuickLinkItem item = this.mItems.get(i);
            Log.d("QuickLinkHelper", "QuickLink item - idx:" + i + ", url:" + item.mUrl);
            if (item.mUrl.equalsIgnoreCase(url)) {
                item.mFlag = 1;
                item.mTitle = title;
                item.mUrl = url;
                item.mImage = image;
                this.mItems.set(i, item);
                saveXML();
                return true;
            }
        }
        for (int i2 = 0; i2 < this.mItems.size(); i2++) {
            QuickLinkItem item2 = this.mItems.get(i2);
            if (item2.mFlag == 0) {
                item2.mFlag = 1;
                item2.mTitle = title;
                item2.mUrl = url;
                item2.mImage = image;
                this.mItems.set(i2, item2);
                saveXML();
                return true;
            }
        }
        return false;
    }

    public void update(int idx, int flag, String title, String url, String image) {
        for (int i = 0; i < this.mItems.size(); i++) {
            QuickLinkItem item = this.mItems.get(i);
            if (item.mIdx == idx) {
                item.mFlag = flag;
                item.mTitle = title;
                item.mUrl = url;
                item.mImage = image;
                this.mItems.set(i, item);
                saveXML();
                return;
            }
        }
    }

    public void onSnapshotCaptured(int idx, String image) {
        for (int i = 0; i < this.mItems.size(); i++) {
            QuickLinkItem item = this.mItems.get(i);
            if (item.mIdx == idx) {
                item.mImage = image;
                item.mFlag = 1;
                this.mItems.set(i, item);
                saveXML();
                return;
            }
        }
    }
}
