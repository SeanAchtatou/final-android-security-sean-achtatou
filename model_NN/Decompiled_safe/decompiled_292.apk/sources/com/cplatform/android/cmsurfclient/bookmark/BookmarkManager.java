package com.cplatform.android.cmsurfclient.bookmark;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;

public class BookmarkManager {
    private final String LOG_TAG = BookmarkManager.class.getSimpleName();
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public IBookmarkManager mHandler;

    public BookmarkManager(Context context, IBookmarkManager handler) {
        this.mContext = context;
        this.mHandler = handler;
    }

    public void addBookmark(String bookmarkTitle, String bookmarkUrl, boolean isShowCheckBox) {
        final View dialogview = LayoutInflater.from(this.mContext).inflate((int) R.layout.bookmark_add, (ViewGroup) null);
        ((EditText) dialogview.findViewById(R.id.bookmark_add_title)).setText(bookmarkTitle);
        ((EditText) dialogview.findViewById(R.id.bookmark_add_url)).setText(bookmarkUrl);
        CheckBox chkQuickLink = (CheckBox) dialogview.findViewById(R.id.bookmark_add_quicklink);
        if (!isShowCheckBox) {
            chkQuickLink.setVisibility(8);
        } else {
            chkQuickLink.setChecked(false);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this.mContext);
        builder.setTitle((int) R.string.bookmark_add_title);
        builder.setView(dialogview);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String title = ((EditText) dialogview.findViewById(R.id.bookmark_add_title)).getText().toString();
                String url = ((EditText) dialogview.findViewById(R.id.bookmark_add_url)).getText().toString();
                boolean isChecked = ((CheckBox) dialogview.findViewById(R.id.bookmark_add_quicklink)).isChecked();
                if (title.length() <= 0) {
                    Toast.makeText(BookmarkManager.this.mContext, (int) R.string.bookmark_prompt_title, 0).show();
                } else if (url.length() <= 0 || !URLUtil.isValidUrl(URLUtil.guessUrl(url))) {
                    Toast.makeText(BookmarkManager.this.mContext, (int) R.string.bookmark_prompt_url, 0).show();
                } else {
                    BookmarkItem item = new BookmarkItem(0, title, URLUtil.guessUrl(url), (String) null);
                    if (BookmarkDB.getInstance(BookmarkManager.this.mContext).isBookmarkExist(item)) {
                        Toast.makeText(BookmarkManager.this.mContext, (int) R.string.bookmark_url_exist, 0).show();
                        return;
                    }
                    BookmarkDB.getInstance(BookmarkManager.this.mContext).add(item);
                    Toast.makeText(BookmarkManager.this.mContext, (int) R.string.bookmark_add_success, 0).show();
                    if (isChecked && BookmarkManager.this.mHandler != null) {
                        BookmarkManager.this.mHandler.onAddQuickLink(item.title, item.url);
                    }
                }
            }
        });
        builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        builder.show();
    }
}
