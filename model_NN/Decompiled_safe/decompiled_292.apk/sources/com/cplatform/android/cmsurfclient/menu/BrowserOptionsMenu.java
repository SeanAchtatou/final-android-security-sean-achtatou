package com.cplatform.android.cmsurfclient.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import com.cplatform.android.cmsurfclient.R;
import java.util.ArrayList;
import java.util.HashMap;

public class BrowserOptionsMenu extends Activity {
    private static int[] mItemsIconCommonBrowser = {R.drawable.menu_refresh, R.drawable.menu_add_bookmark, R.drawable.menu_bookmark, R.drawable.menu_download, R.drawable.menu_share, R.drawable.menu_fullscreen, R.drawable.menu_settings, R.drawable.menu_quit};
    private static int[] mItemsIconCommonBrowserFS = {R.drawable.menu_refresh, R.drawable.menu_add_bookmark, R.drawable.menu_bookmark, R.drawable.menu_download, R.drawable.menu_share, R.drawable.menu_quit_fullscreen, R.drawable.menu_settings, R.drawable.menu_quit};
    private static int[] mItemsIconCommonChannel = {R.drawable.menu_refresh, R.drawable.menu_add_bookmark_grey, R.drawable.menu_bookmark, R.drawable.menu_download, R.drawable.menu_share_grey, R.drawable.menu_fullscreen, R.drawable.menu_settings, R.drawable.menu_quit};
    private static int[] mItemsIconCommonChannelFS = {R.drawable.menu_refresh, R.drawable.menu_add_bookmark_grey, R.drawable.menu_bookmark, R.drawable.menu_download, R.drawable.menu_share_grey, R.drawable.menu_quit_fullscreen, R.drawable.menu_settings, R.drawable.menu_quit};
    private static int[] mItemsIconCommonNavi = {R.drawable.menu_refresh_grey, R.drawable.menu_add_bookmark_grey, R.drawable.menu_bookmark, R.drawable.menu_download, R.drawable.menu_share_grey, R.drawable.menu_fullscreen, R.drawable.menu_settings, R.drawable.menu_quit};
    private static int[] mItemsIconCommonNaviFS = {R.drawable.menu_refresh_grey, R.drawable.menu_add_bookmark_grey, R.drawable.menu_bookmark, R.drawable.menu_download, R.drawable.menu_share_grey, R.drawable.menu_quit_fullscreen, R.drawable.menu_settings, R.drawable.menu_quit};
    private static int[] mItemsIconTools = {R.drawable.menu_nightmode, R.drawable.menu_lockscreen, R.drawable.menu_flow, R.drawable.menu_update, R.drawable.menu_selecttext_grey, R.drawable.menu_searchtext_grey, R.drawable.menu_help};
    private static int[] mItemsIconToolsBrowser = {R.drawable.menu_nightmode, R.drawable.menu_lockscreen, R.drawable.menu_flow, R.drawable.menu_update, R.drawable.menu_selecttext, R.drawable.menu_searchtext, R.drawable.menu_help};
    private static int[] mItemsLabelCommon = {R.string.menu_item_fresh, R.string.menu_item_addbookmark, R.string.menu_item_bookmark, R.string.menu_item_download, R.string.menu_item_share, R.string.menu_item_fullscreen, R.string.menu_item_settings, R.string.menu_item_quit};
    private static int[] mItemsLabelCommonFS = {R.string.menu_item_fresh, R.string.menu_item_addbookmark, R.string.menu_item_bookmark, R.string.menu_item_download, R.string.menu_item_share, R.string.menu_item_quit_fullscreen, R.string.menu_item_settings, R.string.menu_item_quit};
    private static int[] mItemsLabelTools = {R.string.menu_item_nightmode, R.string.menu_item_lockscreen, R.string.menu_item_flow, R.string.menu_item_update, R.string.menu_item_selecttext, R.string.menu_item_searchtext, R.string.menu_item_help};
    private static boolean[] mItemsStatusCommonBrowser = {true, true, true, true, true, true, true, true};
    private static boolean[] mItemsStatusCommonChannel = {true, false, true, true, false, true, true, true};
    private static boolean[] mItemsStatusCommonNavi = {false, false, true, true, false, true, true, true};
    private static boolean[] mItemsStatusTools = {true, true, true, true, true, true};
    private static String[] mItemsTagCommon = {"browser_options_refresh", "browser_options_addbookmark", "browser_options_bookmark", "browser_options_download", "browser_options_share", "browser_options_fullscreen", "browser_options_settings", "browser_options_quit"};
    private static String[] mItemsTagTools = {"browser_options_nightmode", "browser_options_lockscreen", "browser_options_flow", "browser_options_update", "browser_options_selecttext", "browser_options_searchtext", "browser_options_help"};
    /* access modifiers changed from: private */
    public Button mBtnCommon = null;
    /* access modifiers changed from: private */
    public Button mBtnTools = null;
    SimpleAdapter mCommonAdapter = null;
    /* access modifiers changed from: private */
    public GridView mGridView;
    /* access modifiers changed from: private */
    public boolean[] mItemsStatus = null;
    SimpleAdapter mToolsAdapter = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.menu_browser_options);
        Bundle b = getIntent().getExtras();
        String type = null;
        boolean isFullScreen = false;
        if (b != null) {
            type = b.getString("menu_type");
            isFullScreen = b.getBoolean("full_screen");
        }
        if (TextUtils.isEmpty(type)) {
            type = "menu_browser";
        }
        this.mBtnCommon = (Button) findViewById(R.id.menu_tab_common);
        this.mBtnCommon.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.menu_tab_selected);
                BrowserOptionsMenu.this.mBtnTools.setBackgroundResource(R.drawable.menu_tab);
                BrowserOptionsMenu.this.mGridView.setAdapter((ListAdapter) BrowserOptionsMenu.this.mCommonAdapter);
            }
        });
        this.mBtnTools = (Button) findViewById(R.id.menu_tab_tools);
        this.mBtnTools.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                v.setBackgroundResource(R.drawable.menu_tab_selected);
                BrowserOptionsMenu.this.mBtnCommon.setBackgroundResource(R.drawable.menu_tab);
                BrowserOptionsMenu.this.mGridView.setAdapter((ListAdapter) BrowserOptionsMenu.this.mToolsAdapter);
            }
        });
        this.mItemsStatus = new boolean[mItemsTagCommon.length];
        this.mGridView = (GridView) findViewById(R.id.gridview_browser_options_menu);
        ArrayList<HashMap<String, Object>> lstMenuItemCommon = new ArrayList<>();
        for (int i = 0; i < mItemsTagCommon.length; i++) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("ItemTag", mItemsTagCommon[i]);
            if (type.equalsIgnoreCase("menu_browser")) {
                if (!isFullScreen) {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonBrowser[i]));
                } else {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonBrowserFS[i]));
                }
                this.mItemsStatus[i] = mItemsStatusCommonBrowser[i];
            } else if (type.equalsIgnoreCase("menu_navi")) {
                if (!isFullScreen) {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonNavi[i]));
                } else {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonNaviFS[i]));
                }
                this.mItemsStatus[i] = mItemsStatusCommonNavi[i];
            } else if (type.equalsIgnoreCase("menu_channel")) {
                if (!isFullScreen) {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonChannel[i]));
                } else {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonChannelFS[i]));
                }
                this.mItemsStatus[i] = mItemsStatusCommonChannel[i];
            } else {
                if (!isFullScreen) {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonBrowser[i]));
                } else {
                    map.put("ItemImage", Integer.valueOf(mItemsIconCommonBrowserFS[i]));
                }
                this.mItemsStatus[i] = mItemsStatusCommonBrowser[i];
            }
            if (!isFullScreen) {
                map.put("ItemText", getString(mItemsLabelCommon[i]));
            } else {
                map.put("ItemText", getString(mItemsLabelCommonFS[i]));
            }
            lstMenuItemCommon.add(map);
        }
        this.mCommonAdapter = new SimpleAdapter(this, lstMenuItemCommon, R.layout.menu_options_item, new String[]{"ItemImage", "ItemText"}, new int[]{R.id.MenuItemImage, R.id.MenuItemText}) {
            public boolean areAllItemsEnabled() {
                return false;
            }

            public boolean isEnabled(int position) {
                return BrowserOptionsMenu.this.mItemsStatus[position];
            }
        };
        ArrayList<HashMap<String, Object>> lstMenuItemTools = new ArrayList<>();
        for (int i2 = 0; i2 < mItemsTagTools.length; i2++) {
            HashMap<String, Object> map2 = new HashMap<>();
            map2.put("ItemTag", mItemsTagTools[i2]);
            if (!type.equalsIgnoreCase("menu_browser")) {
                map2.put("ItemImage", Integer.valueOf(mItemsIconTools[i2]));
            } else {
                map2.put("ItemImage", Integer.valueOf(mItemsIconToolsBrowser[i2]));
            }
            map2.put("ItemText", getString(mItemsLabelTools[i2]));
            lstMenuItemTools.add(map2);
        }
        this.mToolsAdapter = new SimpleAdapter(this, lstMenuItemTools, R.layout.menu_options_item, new String[]{"ItemImage", "ItemText"}, new int[]{R.id.MenuItemImage, R.id.MenuItemText}) {
            public boolean areAllItemsEnabled() {
                return false;
            }

            public boolean isEnabled(int position) {
                return true;
            }
        };
        this.mGridView.setAdapter((ListAdapter) this.mCommonAdapter);
        this.mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                BrowserOptionsMenu.this.setResult(-1, new Intent().setAction((String) ((HashMap) ((GridView) parent).getItemAtPosition(position)).get("ItemTag")));
                BrowserOptionsMenu.this.finish();
            }
        });
    }

    public boolean onTouchEvent(MotionEvent event) {
        View v = findViewById(R.id.gridview_browser_options_menu);
        int[] location = {0, 0};
        v.getLocationOnScreen(location);
        float y = event.getY();
        if (y > ((float) (location[1] + v.getHeight())) || y < ((float) location[1])) {
            setResult(0, null);
            finish();
        }
        return true;
    }
}
