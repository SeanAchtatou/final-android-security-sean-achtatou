package com.cplatform.android.cmsurfclient;

public interface ISurfWebView {
    boolean isCurrent(SurfWebView surfWebView);

    boolean isHistoryTail(String str);

    void onGoBack();

    void onGoForward();

    void onLoadUrl(String str);

    void onPageFinished(SurfWebView surfWebView, String str, String str2);

    void onPageStarted(SurfWebView surfWebView, String str);

    void onPreloadUrl(String str);

    void onProgressChanged(SurfWebView surfWebView, int i);

    void onReceivedError(SurfWebView surfWebView, int i, String str, String str2);

    void onReceivedTitle(SurfWebView surfWebView, String str, boolean z);

    void onShareImage(String str);

    void onUpdateVisitedHistory(SurfWebView surfWebView, String str);
}
