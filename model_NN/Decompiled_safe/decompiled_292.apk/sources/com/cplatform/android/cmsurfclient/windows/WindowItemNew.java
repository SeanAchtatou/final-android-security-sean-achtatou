package com.cplatform.android.cmsurfclient.windows;

import android.graphics.Bitmap;
import com.cplatform.android.cmsurfclient.BrowserViewNew;

public class WindowItemNew {
    public static final int STATE_HOME_BACK = 1;
    public static final int STATE_HOME_BACKFORWARD = 3;
    public static final int STATE_HOME_FORWARD = 2;
    public static final int STATE_HOME_NONE = 0;
    public static final int STATE_WEBVIEW = 4;
    public BrowserViewNew browserView;
    public Bitmap snapshot;
    public int state;
    public int tab;
    public String title;
    public String url;

    public WindowItemNew() {
        this.state = 0;
        this.state = 0;
        this.tab = -1;
        this.title = null;
        this.url = null;
        this.browserView = null;
        this.snapshot = null;
    }
}
