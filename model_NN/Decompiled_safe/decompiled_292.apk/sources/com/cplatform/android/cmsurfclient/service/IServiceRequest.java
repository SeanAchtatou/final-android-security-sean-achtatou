package com.cplatform.android.cmsurfclient.service;

import com.cplatform.android.cmsurfclient.service.entry.Msb;

public interface IServiceRequest {
    void onServiceComplete(int i, Msb msb);
}
