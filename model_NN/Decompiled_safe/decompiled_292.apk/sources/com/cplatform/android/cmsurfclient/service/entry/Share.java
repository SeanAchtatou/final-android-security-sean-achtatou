package com.cplatform.android.cmsurfclient.service.entry;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Share {
    public ArrayList<ShareItem> mItems = null;
    public String mMessage;
    public String mRetCode;

    public Share(Element entry) {
        if (entry != null) {
            this.mRetCode = entry.getAttribute("code");
            this.mMessage = entry.getAttribute("msg");
            NodeList nlGroupItem = entry.getElementsByTagName("shareitem");
            if (nlGroupItem != null && nlGroupItem.getLength() > 0) {
                this.mItems = new ArrayList<>();
                int groupItemLen = nlGroupItem.getLength();
                for (int i = 0; i < groupItemLen; i++) {
                    this.mItems.add(new ShareItem((Element) nlGroupItem.item(i)));
                }
            }
        }
    }

    public String toString() {
        return "ReturnCode:" + this.mRetCode + " Message:" + this.mMessage;
    }
}
