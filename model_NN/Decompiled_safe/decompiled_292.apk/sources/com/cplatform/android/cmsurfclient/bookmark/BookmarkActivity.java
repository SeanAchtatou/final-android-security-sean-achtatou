package com.cplatform.android.cmsurfclient.bookmark;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;

public class BookmarkActivity extends Activity {
    public static BookmarkDB bmDB = null;
    private final String DEBUG_TAG = "BookmarkActivity";
    private TextView emptyNotify = null;
    /* access modifiers changed from: private */
    public BookmarkAdapter mBookmarkAdapter = null;
    /* access modifiers changed from: private */
    public BookmarkItem mBookmarkItem = null;
    /* access modifiers changed from: private */
    public BookmarkManager mBookmarkMgr = null;
    private ListView mBookmarkView;
    private LinearLayout mButtonAdd;
    private LinearLayout mButtonBack;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.bookmark);
        this.emptyNotify = (TextView) findViewById(R.id.bookmark_empty);
        this.mButtonAdd = (LinearLayout) findViewById(R.id.bookmark_add);
        this.mButtonBack = (LinearLayout) findViewById(R.id.bookmark_back);
        bmDB = BookmarkDB.getInstance(this);
        refreshNotifyTextVisible();
        this.mBookmarkAdapter = new BookmarkAdapter(this, bmDB.items);
        this.mBookmarkMgr = new BookmarkManager(this, null);
        this.mBookmarkView = (ListView) findViewById(R.id.listview_bookmark);
        this.mBookmarkView.setAdapter((ListAdapter) this.mBookmarkAdapter);
        this.mButtonBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                BookmarkActivity.this.getParent().setResult(-1, new Intent().setAction("none"));
                BookmarkActivity.this.finish();
            }
        });
        this.mButtonAdd.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (BookmarkActivity.this.mBookmarkMgr != null) {
                    BookmarkActivity.this.mBookmarkMgr.addBookmark(WindowAdapter2.BLANK_URL, "http://", false);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.mBookmarkView.invalidateViews();
    }

    public void onClick(BookmarkItem item) {
        getParent().setResult(-1, new Intent().setAction(item.url));
        finish();
    }

    public void onEdit(BookmarkItem item) {
        this.mBookmarkItem = item;
        final View dialogview = LayoutInflater.from(this).inflate((int) R.layout.bookmark_add, (ViewGroup) null);
        EditText txtUrl = (EditText) dialogview.findViewById(R.id.bookmark_add_url);
        if (txtUrl != null) {
            txtUrl.setText(item.url);
        }
        EditText txtTitle = (EditText) dialogview.findViewById(R.id.bookmark_add_title);
        if (txtTitle != null) {
            txtTitle.setText(item.title);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.bookmark_edit_title);
        builder.setView(dialogview);
        dialogview.findViewById(R.id.bookmark_add_quicklink).setVisibility(8);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String title = ((EditText) dialogview.findViewById(R.id.bookmark_add_title)).getText().toString();
                String url = ((EditText) dialogview.findViewById(R.id.bookmark_add_url)).getText().toString();
                if (TextUtils.isEmpty(title)) {
                    Toast.makeText(BookmarkActivity.this, (int) R.string.bookmark_prompt_title, 0).show();
                } else if (TextUtils.isEmpty(url) || !URLUtil.isValidUrl(URLUtil.guessUrl(url))) {
                    Toast.makeText(BookmarkActivity.this, (int) R.string.bookmark_prompt_url, 0).show();
                } else {
                    BookmarkActivity.this.mBookmarkItem.title = title;
                    BookmarkActivity.this.mBookmarkItem.url = URLUtil.guessUrl(url);
                    BookmarkActivity.bmDB.update(BookmarkActivity.this.mBookmarkItem);
                    BookmarkActivity.this.mBookmarkAdapter.notifyDataSetChanged();
                    Toast.makeText(BookmarkActivity.this, (int) R.string.bookmark_edit_success, 0).show();
                }
            }
        });
        builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        builder.show();
    }

    public void onShare(BookmarkItem item) {
    }

    public void onDelete(final BookmarkItem item) {
        View dialogView = LayoutInflater.from(this).inflate((int) R.layout.dialog_delete, (ViewGroup) null);
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle((int) R.string.warning);
        dialog.setView(dialogView);
        dialog.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                BookmarkActivity.bmDB.del(item);
                BookmarkActivity.this.mBookmarkAdapter.notifyDataSetChanged();
                BookmarkActivity.this.refreshNotifyTextVisible();
            }
        });
        dialog.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialog.show();
    }

    public void refreshNotifyTextVisible() {
        if (this.emptyNotify != null) {
            this.emptyNotify.setVisibility((bmDB == null || bmDB.items == null || bmDB.items.size() == 0) ? 0 : 8);
        }
    }
}
