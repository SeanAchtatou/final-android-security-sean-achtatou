package com.cplatform.android.cmsurfclient.cache;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.cplatform.android.cmsurfclient.SurfBrowser;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class CacheDB {
    private static final String DATABASE_NAME = "CMSurfClient";
    private static final String TABLE_CACHE_NAME = "cache";
    private static CacheDB instance = null;
    private static boolean tabcreated = false;
    private final String DEBUG_TAG = "CacheDB";
    public HashSet<String> assets = new HashSet<>();
    private Context context;
    private SQLiteDatabase db = null;
    public HashMap<String, CacheItem> items = new HashMap<>();

    public static CacheDB getInstance(Context context2) {
        if (instance == null) {
            instance = new CacheDB(context2);
            instance.load();
            instance.checkCacheVersion();
        }
        return instance;
    }

    private void loadAssets() {
        this.assets.clear();
        this.assets.add("icon_tab_bookmark_down.png");
        this.assets.add("icon_tab_bookmark_up.png");
        this.assets.add("icon_tab_funny_down.png");
        this.assets.add("icon_tab_funny_up.png");
        this.assets.add("icon_tab_navi_down.png");
        this.assets.add("icon_tab_navi_up.png");
        this.assets.add("icon_tab_news_down.png");
        this.assets.add("icon_tab_news_up.png");
        this.assets.add("icon_tab_personal_down.png");
        this.assets.add("icon_tab_personal_up.png");
        this.assets.add("icon_tab_photo_down.png");
        this.assets.add("icon_tab_photo_up.png");
        this.assets.add("navigator_icon_history.png");
        this.assets.add("navigator_icon_history_selected.png");
        this.assets.add("navigator_icon_hot.png");
        this.assets.add("navigator_icon_hot_selected.png");
        this.assets.add("navigator_icon_location.png");
        this.assets.add("navigator_icon_location_selected.png");
        this.assets.add("navigator_icon_remark.png");
        this.assets.add("navigator_icon_remark_selected.png");
        this.assets.add("navigator_icon_share.png");
        this.assets.add("navigator_icon_share_selcted.png");
        this.assets.add("navigator_icon_sort.png");
        this.assets.add("navigator_icon_sort_selected.png");
        this.assets.add("navigator_icon_tool.png");
        this.assets.add("navigator_icon_tool_selected.png");
        this.assets.add("navigator_icon_usual.png");
        this.assets.add("navigator_icon_usual_selected.png");
        this.assets.add("navigator_icon_www.png");
        this.assets.add("navigator_icon_www_selected.png");
        this.assets.add("search_baidu.png");
        this.assets.add("search_sogou.png");
    }

    private void checkCacheVersion() {
        CacheItem item = this.items.get("version");
        if (item == null || !SurfBrowser.CURRENT_VERSION.equals(item.file)) {
            clear();
        }
    }

    private CacheDB(Context context2) {
        this.context = context2;
    }

    public boolean openDB() {
        if (this.db == null || !this.db.isOpen()) {
            this.db = this.context.openOrCreateDatabase(DATABASE_NAME, 0, null);
            prepareTable();
        }
        return this.db != null && this.db.isOpen();
    }

    public void closeDB() {
        if (this.db != null) {
            if (this.db.isOpen()) {
                this.db.close();
            }
            this.db = null;
        }
    }

    private void prepareTable() {
        if (!tabcreated) {
            StringBuffer sql = new StringBuffer(100);
            sql.append("create table ").append(TABLE_CACHE_NAME).append("(_id integer primary key autoincrement, url text, file text, createtime integer);");
            try {
                tabcreated = true;
                this.db.execSQL(sql.toString());
            } catch (Exception e) {
            }
        }
    }

    public void load() {
        Date date;
        this.items.clear();
        loadAssets();
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_CACHE_NAME, new String[]{QueryApList.Carriers._ID, "url", "file", "createtime"}, null, null, null, null, null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                int id = cur.getInt(0);
                String url = cur.getString(1);
                String file = cur.getString(2);
                long milliseconds = cur.getLong(3) * 1000;
                if (milliseconds == 0) {
                    date = null;
                } else {
                    try {
                        Date date2 = new Date();
                        try {
                            date2.setTime(milliseconds);
                            date = date2;
                        } catch (Exception e) {
                            date = null;
                            this.items.put(url, new CacheItem((long) id, url, file, date));
                            cur.moveToNext();
                        }
                    } catch (Exception e2) {
                        date = null;
                        this.items.put(url, new CacheItem((long) id, url, file, date));
                        cur.moveToNext();
                    }
                }
                this.items.put(url, new CacheItem((long) id, url, file, date));
                cur.moveToNext();
            }
            cur.close();
            closeDB();
        }
    }

    public boolean add(CacheItem item) {
        boolean ret = false;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put("url", item.url);
                values.put("file", item.file);
                values.put("createtime", Long.valueOf(item.date == null ? 0 : item.date.getTime() / 1000));
                item.id = this.db.insert(TABLE_CACHE_NAME, null, values);
                this.items.put(item.url, item);
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean del(CacheItem item) {
        if (item == null) {
            return false;
        }
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_CACHE_NAME);
                sql.append(" where _id=").append(item.id).append(";");
                Log.i("CacheDB", sql.toString());
                this.db.execSQL(sql.toString());
                this.context.deleteFile(item.file);
                this.items.remove(item.url);
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean clear() {
        boolean ret = false;
        this.items.remove("version");
        for (Map.Entry<String, CacheItem> entry : this.items.entrySet()) {
            try {
                this.context.deleteFile(entry.getValue().file);
            } catch (Exception e) {
                Log.e(WindowAdapter2.BLANK_URL, WindowAdapter2.BLANK_URL, e);
            }
        }
        this.items.clear();
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_CACHE_NAME).append(";");
                Log.i("CacheDB", sql.toString());
                this.db.execSQL(sql.toString());
                ret = true;
            } catch (Exception e2) {
            }
            closeDB();
        }
        add(new CacheItem(-1, "version", SurfBrowser.CURRENT_VERSION, new Date()));
        return ret;
    }

    public boolean delUrl(String url) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_CACHE_NAME);
                sql.append(" where url='").append(url).append("';");
                Log.i("CacheDB", sql.toString());
                this.db.execSQL(sql.toString());
                CacheItem item = this.items.remove(url);
                if (item != null) {
                    this.context.deleteFile(item.file);
                }
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public String getFile(String url) {
        CacheItem cacheItem;
        if (url == null || !url.startsWith("http") || (cacheItem = this.items.get(url)) == null) {
            return url;
        }
        return cacheItem.file;
    }

    public String getAssetsFile(String url) {
        if (url != null && url.startsWith("http")) {
            int pos = url.lastIndexOf(47);
            if (pos < 0) {
                return null;
            }
            String file = url.substring(pos + 1);
            if (this.assets.contains(file)) {
                return "msb/" + file;
            }
        }
        return null;
    }
}
