package com.cplatform.android.cmsurfclient.wml2html;

import android.util.Log;
import com.cplatform.android.cmsurfclient.download.provider.Constants;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Wml2Html {
    private String[] mo_backward = null;
    private String[] mo_forward = null;
    private String mo_host = WindowAdapter2.BLANK_URL;
    private String mo_path = WindowAdapter2.BLANK_URL;
    private String[] mo_timer = null;
    private String mo_title = WindowAdapter2.BLANK_URL;

    private String UDecode(String src) {
        int parseInt;
        int len = src == null ? 0 : src.length();
        StringBuffer ret = new StringBuffer();
        if (len == 0) {
            return ret.toString();
        }
        Matcher m = Pattern.compile("&#x([A-Fa-f0-9]{4});|&#([\\d]{1,5});", 10).matcher(src);
        int beginIndex = 0;
        while (m.find()) {
            if (m.group(1) != null) {
                parseInt = Integer.parseInt(m.group(1), 16);
            } else {
                parseInt = Integer.parseInt(m.group(2));
            }
            ret.append(src.subSequence(beginIndex, m.start()));
            ret.append((char) parseInt);
            beginIndex = m.end();
        }
        if (beginIndex < len) {
            ret.append(src.subSequence(beginIndex, len));
        }
        return ret.toString();
    }

    private String formatURL(String strURL) {
        String ret;
        if (strURL.length() > 7 && "http://".equalsIgnoreCase(strURL.substring(0, 7))) {
            ret = strURL;
        } else if (strURL.length() > 7 && "wtai://".equalsIgnoreCase(strURL.substring(0, 7))) {
            ret = strURL;
        } else if ("/".equals(strURL.substring(0, 1))) {
            ret = "http://" + this.mo_host + strURL;
        } else {
            String tmp = this.mo_path;
            String ptr = strURL;
            Matcher arr = Pattern.compile("^\\.\\.\\/(.+)$").matcher(ptr);
            while (arr != null && arr.find()) {
                tmp = tmp.replace("[^\\/]+\\/$", WindowAdapter2.BLANK_URL);
                ptr = arr.group(1);
                arr = Pattern.compile("^\\.\\.\\/(.+)$").matcher(ptr);
            }
            Matcher arr2 = Pattern.compile("^\\.\\/(.+)$").matcher(ptr);
            if (arr2 != null && arr2.find()) {
                ptr = arr2.group(1);
            }
            ret = "http://" + this.mo_host + tmp + ptr;
        }
        String ret2 = ret.replaceAll("\\s", WindowAdapter2.BLANK_URL);
        Matcher arr3 = Pattern.compile("^(.+)#").matcher(ret2);
        if (arr3 == null || !arr3.find()) {
            return ret2;
        }
        return arr3.group(1);
    }

    private String HTMLDecode(String src) {
        return src.replaceAll("&quot;", "\"").replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&amp;", "&");
    }

    private String getAttr(String strData, String strName) {
        Matcher arr = Pattern.compile(strName + "=[\\\"\\']([^\\\"\\']+?)[\\\"\\']").matcher(strData);
        if (arr.find()) {
            return arr.group(1);
        }
        return WindowAdapter2.BLANK_URL;
    }

    private String encodeURIComponent(String src) {
        try {
            return URLEncoder.encode(src, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return src;
        }
    }

    private String getField(String strData) {
        Matcher arr = Pattern.compile("<postfield([^>]+)>", 8).matcher(strData);
        StringBuffer ret = null;
        while (arr.find()) {
            if (ret == null) {
                ret = new StringBuffer();
            } else {
                ret.append(",");
            }
            ret.append("'");
            ret.append(getAttr(arr.group(1), QueryApList.Carriers.NAME));
            ret.append("=");
            ret.append(HTMLDecode(encodeURIComponent(getAttr(arr.group(1), "value"))));
            ret.append("'");
        }
        return ret == null ? "''" : ret.toString();
    }

    private String getHost(String strURL) {
        Matcher arr = Pattern.compile("http:\\/\\/([^\\/]+)", 2).matcher(strURL);
        if (arr.find()) {
            return arr.group(1);
        }
        return WindowAdapter2.BLANK_URL;
    }

    private String getPath(String strURL) {
        String tmp = null;
        Matcher arr = Pattern.compile("http:\\/\\/[^\\/]+([^\\?]*)", 2).matcher(strURL);
        if (arr.find()) {
            tmp = arr.group(1);
        }
        if (tmp == null) {
            return "/";
        }
        int pos = tmp.lastIndexOf("/");
        if (pos != -1) {
            tmp = tmp.substring(0, pos + 1);
        }
        if (WindowAdapter2.BLANK_URL.equals(tmp)) {
            tmp = "/";
        }
        return tmp;
    }

    private String formatAnchor(String strData) {
        StringBuffer ret = new StringBuffer();
        int pos = 0;
        int len = strData == null ? 0 : strData.length();
        Matcher arr = Pattern.compile("<anchor[^>]*>([\\s\\S]*?)<go([^>]+?)>([\\s\\S]+?)<\\/go>([\\s\\S]*?)<\\/anchor>", 8).matcher(strData);
        while (arr.find()) {
            ret.append(strData.subSequence(pos, arr.start()));
            pos = arr.end();
            String strHref = getAttr(arr.group(2), "href");
            String strMethod = getAttr(arr.group(2), Constants.RETRY_AFTER_X_REDIRECT_COUNT);
            String strFiled = getField(arr.group(3));
            if (strMethod == WindowAdapter2.BLANK_URL) {
                strMethod = "get";
            }
            ret.append("<a href=\"javascript:mobiesurf_request('").append(strMethod).append("','").append(strHref).append("',").append(strFiled).append(")\" >").append(arr.group(1)).append(arr.group(4)).append("</a>");
        }
        ret.append(strData.subSequence(pos, len));
        return ret.toString();
    }

    private String[] getEvent(String strData, String strName) {
        String reg1 = "<onevent\\s+type=[\\\"\\']" + strName + "[\\\"\\']>[\\s\\S]*?<go([^>]+?)>([\\s\\S]*?)<\\/go>[\\s\\S]*?</onevent>";
        String reg2 = "<onevent\\s+type=[\\\"\\']" + strName + "[\\\"\\']>[\\s\\S]*?<go([^>]+?)>[\\s\\S]*?</onevent>";
        String reg3 = "<card.+?" + strName + "=[\\\"\\']([^\\\"\\']+)[\\\"\\'][^>]*>";
        String[] ret = null;
        Matcher matcher = Pattern.compile(reg1, 8).matcher(strData);
        Matcher arr = Pattern.compile(reg1, 8).matcher(strData);
        if (arr.find()) {
            ret = new String[]{getAttr(arr.group(1), Constants.RETRY_AFTER_X_REDIRECT_COUNT), getAttr(arr.group(1), "href"), getField(arr.group(2))};
            if (ret[0] == WindowAdapter2.BLANK_URL) {
                ret[0] = "get";
            }
            if (ret[1] != WindowAdapter2.BLANK_URL) {
                ret[1] = formatURL(ret[1]);
            }
        } else {
            Matcher arr2 = Pattern.compile(reg2, 8).matcher(strData);
            if (arr2.find()) {
                ret = new String[]{getAttr(arr2.group(1), Constants.RETRY_AFTER_X_REDIRECT_COUNT), getAttr(arr2.group(1), "href"), "''"};
                if (ret[0] == WindowAdapter2.BLANK_URL) {
                    ret[0] = "get";
                }
                if (ret[1] != WindowAdapter2.BLANK_URL) {
                    ret[1] = formatURL(ret[1]);
                }
            } else {
                Matcher arr3 = Pattern.compile(reg3, 8).matcher(strData);
                if (arr3.find()) {
                    ret = new String[]{"get", arr3.group(1), "''"};
                    if (ret[1] != WindowAdapter2.BLANK_URL) {
                        ret[1] = formatURL(ret[1]);
                    }
                }
            }
        }
        return ret;
    }

    private int getTimer(String s) {
        Matcher ret = Pattern.compile("<timer\\s+value=[\\\"\\'](\\d+)[\\\"\\'][^>]+>").matcher(s);
        if (ret == null || !ret.find()) {
            return 3000;
        }
        return Integer.parseInt(ret.group(1)) * 100;
    }

    public String parse(String strData, String strHost, String strPath, boolean enterforward) {
        Log.i("Wml2Html", "enterforward=" + enterforward);
        Log.e("Wml2Html", "Host=" + strHost + ", Path=" + strPath);
        if (strPath.lastIndexOf("/") >= 0) {
            strPath = strPath.substring(0, strPath.lastIndexOf("/") + 1);
        }
        String strData2 = UDecode(strData);
        StringBuffer retBuf = new StringBuffer();
        Matcher arr = Pattern.compile("<\\!\\-\\-mo\\.target:(.+?)\\-\\->").matcher(strData2);
        if (arr.find()) {
            this.mo_host = getHost(arr.group(1));
            this.mo_path = getPath(arr.group(1));
        } else {
            this.mo_host = strHost;
            this.mo_path = strPath;
        }
        Matcher arr2 = Pattern.compile("(href|src|onpick|onenterforward|onenterbackward|ontimer)[\\s]*=[\\s]*[\\\"\\']([^\\\"\\']+)[\\\"\\']", 8).matcher(strData2);
        int len = strData2 == null ? 0 : strData2.length();
        int beginIndex = 0;
        while (arr2.find()) {
            retBuf.append(strData2.subSequence(beginIndex, arr2.end(1)));
            retBuf.append("=\"");
            retBuf.append(formatURL(arr2.group(2)));
            retBuf.append("\"");
            beginIndex = arr2.end();
        }
        if (beginIndex < len) {
            retBuf.append(strData2.subSequence(beginIndex, len));
        }
        String ret = formatAnchor(retBuf.toString());
        this.mo_forward = getEvent(ret, "onenterforward");
        this.mo_backward = getEvent(ret, "onenterbackward");
        this.mo_timer = getEvent(ret, "ontimer");
        String ret2 = ret.replaceAll("<input(.+?)(name)=[\\\"\\']([^\\\"\\']+)[\\\"\\']", "<input$1id=\"$3\"").replaceAll("<a\\starget=", "<a href=");
        Matcher arr3 = Pattern.compile("<(card)[^>]*>([\\s\\S]+)<\\/\\1>").matcher(ret2);
        String body = arr3.find() ? arr3.group(2) : "unsupport document";
        Matcher arr4 = Pattern.compile("<card.+?title.*?=.*?[\\\"\\']([^\\\"\\']*?)[\\\"\\']").matcher(ret2);
        if (arr4.find()) {
            this.mo_title = HTMLDecode(arr4.group(1));
        } else {
            Matcher arr5 = Pattern.compile("<card.+?title=[\\\"\\']([^\\\"\\']+)[\\\"\\']").matcher(ret2);
            if (arr5.find()) {
                this.mo_title = HTMLDecode(arr5.group(1));
            } else {
                Matcher arr6 = Pattern.compile("<card.+?title.+?=.+?[\\\"\\']([^\\\"\\']+)[\\\"\\']").matcher(ret2);
                this.mo_title = arr6.find() ? HTMLDecode(arr6.group(1)) : WindowAdapter2.BLANK_URL;
            }
        }
        String onload = null;
        String onloadScript = null;
        if (this.mo_forward != null && this.mo_forward.length == 3 && !WindowAdapter2.BLANK_URL.equals(this.mo_forward[1])) {
            onload = "<body " + (enterforward ? "onload='mobiesurf_onload();'" : WindowAdapter2.BLANK_URL) + "><a href='javascript:mobiesurf_onload();'>请点击这里继续访问</a>";
            retBuf.setLength(0);
            retBuf.append("\tfunction mobiesurf_onload() {");
            retBuf.append("\t\tmobiesurf_request('").append(this.mo_forward[0]).append("','").append(this.mo_forward[1]).append("',").append(this.mo_forward[2]).append(");");
            retBuf.append("\t}");
            onloadScript = retBuf.toString();
        } else if (this.mo_backward != null && this.mo_backward.length == 3 && !WindowAdapter2.BLANK_URL.equals(this.mo_backward[1]) && !enterforward) {
            onload = "<body onload='mobiesurf_onload();'>";
            retBuf.setLength(0);
            retBuf.append("\tfunction mobiesurf_onload() {");
            retBuf.append("\t\tmobiesurf_request('").append(this.mo_backward[0]).append("','").append(this.mo_backward[1]).append("',").append(this.mo_backward[2]).append(");");
            retBuf.append("\t}");
            onloadScript = retBuf.toString();
        } else if (this.mo_timer != null && this.mo_timer.length == 3 && !WindowAdapter2.BLANK_URL.equals(this.mo_timer[1])) {
            onload = "<body onload='mobiesurf_onload();'>";
            retBuf.setLength(0);
            retBuf.append("\tfunction mobiesurf_onload() {");
            retBuf.append("\t\tmobiesurf_addInterval(mobiesurf_request, ").append(getTimer(ret2)).append(", '").append(this.mo_timer[0]).append("', '").append(this.mo_timer[1]).append("', ").append(this.mo_timer[2]).append(");");
            retBuf.append("\t}");
            retBuf.append("\tfunction mobiesurf_addInterval(func, ms) {");
            retBuf.append("\t\tvar argv = Array.prototype.slice.call(arguments, 2);");
            retBuf.append("\t\tvar newFunc = function() {");
            retBuf.append("\t\t\tfunc.apply(null, argv);");
            retBuf.append("\t\t};");
            retBuf.append("\t\treturn window.setInterval(newFunc, ms);");
            retBuf.append("\t}");
            onloadScript = retBuf.toString();
        }
        retBuf.setLength(0);
        retBuf.append("<html>");
        retBuf.append("<head>");
        retBuf.append("<title>").append(this.mo_title).append("</title>");
        retBuf.append("<meta name=\"viewport\" content=\"width=device-width; initial-scale=1.25; minimum-scale=1.25; maximum-scale=1.25; user-scalable=1;\"/>");
        retBuf.append("<style>");
        retBuf.append("body{line-height:150%;line-height:1.5em;}");
        retBuf.append("</style>");
        retBuf.append("</head>");
        retBuf.append("<script>");
        if (onloadScript != null) {
            retBuf.append(onloadScript);
        }
        retBuf.append("  function mobiesurf_getdata(strData) {");
        retBuf.append("      var reg = /\\$[\\(]{0,1}([\\w%]+)[\\)]{0,1}/g;");
        retBuf.append("      var arr;");
        retBuf.append("      var ret = '';");
        retBuf.append("      var pos = 0;");
        retBuf.append("      var obj, tmp;");
        retBuf.append("      if ((arr = reg.exec(strData)) != null) {");
        retBuf.append("          ret += strData.substring(pos, arr.index);");
        retBuf.append("          pos = arr.index + arr[0].length;");
        retBuf.append("          tmp = arr[1].match(/(.+?)%3A(n|e|u)/i);");
        retBuf.append("          obj = tmp?document.getElementById(decodeURIComponent(tmp[1])):document.getElementById(decodeURIComponent(arr[1]));");
        retBuf.append("          if (obj) ret += obj.value;");
        retBuf.append("      }");
        retBuf.append("      ret += strData.substring(pos);");
        retBuf.append("      return ret;");
        retBuf.append("  }");
        retBuf.append("\tfunction mobiesurf_request() {");
        retBuf.append("\t\tvar numargs = arguments.length;");
        retBuf.append("\t\tif (numargs < 2)");
        retBuf.append("\t\t\treturn;");
        retBuf.append("\t\tvar i, pos, param,paramarr,newElement;");
        retBuf.append("\t\tvar submitForm = document.createElement('FORM');");
        retBuf.append("\t\tdocument.body.appendChild(submitForm);");
        retBuf.append("\t\tsubmitForm.method = arguments[0];");
        retBuf.append("\t\tpos = arguments[1].indexOf('?');");
        retBuf.append("\t\tif (pos >= 0) {");
        retBuf.append("\t\t\tparam = arguments[1].substr(pos+1);");
        retBuf.append("\t\t\tparamarr = param.split('&');");
        retBuf.append("\t\t\tfor (i=0; i<paramarr.length; i++) {");
        retBuf.append("\t\t\t\tparam = mobiesurf_getdata(paramarr[i]);");
        retBuf.append("\t\t\t\tpos = param.indexOf('=');");
        retBuf.append("\t\t\t\tif (pos > -1) {");
        retBuf.append("\t\t\t\t\tnewElement = document.createElement('INPUT');");
        retBuf.append("\t\t\t\t\tnewElement.name=param.substring(0, pos);");
        retBuf.append("\t\t\t\t\tnewElement.value = param.substring(pos+1);");
        retBuf.append("\t\t\t\t\tnewElement.type='hidden';");
        retBuf.append("\t\t\t\t\tsubmitForm.appendChild(newElement);");
        retBuf.append("\t\t\t\t}");
        retBuf.append("\t\t\t}");
        retBuf.append("\t\t}");
        retBuf.append("\t\tfor (i = 2; i < numargs; i++) {");
        retBuf.append("\t\t\tparam = mobiesurf_getdata(arguments[i]);");
        retBuf.append("\t\t\tpos = param.indexOf('=');");
        retBuf.append("\t\t\tif (pos > -1) {");
        retBuf.append("\t\t\t\tvar newElement = document.createElement('INPUT');");
        retBuf.append("\t\t\t\tnewElement.name=param.substring(0, pos);");
        retBuf.append("\t\t\t\tnewElement.value = param.substring(pos+1);");
        retBuf.append("\t\t\t\tnewElement.type='hidden';");
        retBuf.append("\t\t\t\tsubmitForm.appendChild(newElement);");
        retBuf.append("\t\t\t}");
        retBuf.append("\t\t}");
        retBuf.append("\t  submitForm.action= arguments[1];");
        retBuf.append("\t  submitForm.submit();");
        retBuf.append("\t}");
        retBuf.append("</script>");
        retBuf.append(onload == null ? "<body>" : onload);
        retBuf.append(body.replaceAll("<(onevent)[^>]*>([\\s\\S]+)<\\/\\1>", WindowAdapter2.BLANK_URL).replaceAll("<timer\\s+value=[\\\"\\'](\\d+)[\\\"\\'][^>]+>", WindowAdapter2.BLANK_URL));
        retBuf.append("</body>");
        retBuf.append("</html>");
        Log.d("Wml2Html", retBuf.toString());
        return retBuf.toString();
    }
}
