package com.cplatform.android.cmsurfclient.navigator;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.navigator.NavigatorTabHost;

public class NavigatorTabWidget extends LinearLayout {
    private static float sScale;
    float mBackMoveFactor = 0.0f;
    private int mBackMoveWidth = 0;
    protected Context mContext;
    private int mInverseSliderWidth = 0;
    /* access modifiers changed from: private */
    public int mNumTabs = 0;
    private LinearLayout mOuterLayout;
    /* access modifiers changed from: private */
    public int mSelectedTab = 0;
    private OnTabSelectionChanged mSelectionChangedListener;
    private int mSliderMoveWidth = 0;
    /* access modifiers changed from: private */
    public FrameLayout mTabContent;

    interface OnTabSelectionChanged {
        void onTabSelectionChanged(int i, boolean z);
    }

    public NavigatorTabWidget(Context context) {
        super(context);
        initTabWidget(context);
    }

    public NavigatorTabWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTabWidget(context);
    }

    private final void initTabWidget(Context context) {
        this.mContext = context;
        sScale = this.mContext.getResources().getDisplayMetrics().density;
        setOrientation(0);
        inflate(context, R.layout.navigator_widget, this);
        this.mOuterLayout = (LinearLayout) findViewById(R.id.NavigatorTabWidgetLayoutOuter);
        setFocusable(true);
        setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() != 0) {
                    return true;
                }
                switch (keyCode) {
                    case SurfManagerActivity.CONTEXTMENU_HOMEVIEW_OPENNEW:
                        if (NavigatorTabWidget.this.mSelectedTab > 0) {
                            NavigatorTabWidget.this.snapTo(NavigatorTabWidget.this.mSelectedTab - 1);
                            NavigatorTabWidget.this.setCurrentTab(NavigatorTabWidget.this.mSelectedTab - 1);
                        }
                        return true;
                    case SurfManagerActivity.CONTEXTMENU_HOMEVIEW_BOOKMARK:
                        if (NavigatorTabWidget.this.mSelectedTab < NavigatorTabWidget.this.mNumTabs + 1) {
                            NavigatorTabWidget.this.snapTo(NavigatorTabWidget.this.mSelectedTab + 1);
                            NavigatorTabWidget.this.setCurrentTab(NavigatorTabWidget.this.mSelectedTab + 1);
                        }
                        return true;
                    default:
                        NavigatorTabWidget.this.mTabContent.requestFocus(2);
                        return NavigatorTabWidget.this.mTabContent.dispatchKeyEvent(event);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void addTab(NavigatorTabHost.NavigatorTabSpec newTab) {
        NavigatorTabItem tabItem = new NavigatorTabItem(this.mContext, this.mNumTabs, newTab);
        if (tabItem.getLayoutParams() == null) {
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-2, -2, 1.0f);
            lp.setMargins(0, 0, 0, 0);
            tabItem.setLayoutParams(lp);
        }
        tabItem.setFocusable(false);
        tabItem.setClickable(true);
        tabItem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NavigatorTabWidget.this.updateLayoutDimensions();
                NavigatorTabItem item = (NavigatorTabItem) v;
                if (item.getTabIndex() != NavigatorTabWidget.this.mSelectedTab) {
                    NavigatorTabWidget.this.snapTo(item.getTabIndex());
                    NavigatorTabWidget.this.setCurrentTab(item.getTabIndex());
                }
            }
        });
        addView(tabItem);
        this.mNumTabs++;
    }

    /* access modifiers changed from: package-private */
    public void moveTo(int tabIndex) {
        updateLayoutDimensions();
        NavigatorTabItem item = (NavigatorTabItem) getChildTabViewAt(tabIndex);
        if (item != null && tabIndex != this.mSelectedTab) {
            snapTo(item.getTabIndex());
            setCurrentTab(item.getTabIndex());
        }
    }

    private void moveSlider(int pos) {
        new LinearLayout.LayoutParams(-2, -2).setMargins(pos, 0, 0, 0);
    }

    private void moveBackground(int pos) {
        if (pos == 0 || this.mBackMoveFactor > 0.0f) {
        }
    }

    /* access modifiers changed from: private */
    public void snapTo(int tabIndex) {
        float snapWidth;
        if (this.mBackMoveFactor > 0.0f) {
            if (this.mNumTabs > 1) {
                snapWidth = ((float) this.mSliderMoveWidth) / ((float) (this.mNumTabs - 1));
            } else {
                snapWidth = (float) this.mSliderMoveWidth;
            }
            int i = -Math.round(((float) Math.round(((float) tabIndex) * snapWidth)) * this.mBackMoveFactor);
        }
    }

    /* access modifiers changed from: private */
    public void updateLayoutDimensions() {
        if (this.mSliderMoveWidth == 0) {
        }
        if (this.mBackMoveWidth == 0) {
            this.mBackMoveWidth = (this.mInverseSliderWidth - this.mOuterLayout.getWidth()) + this.mOuterLayout.getPaddingRight() + this.mOuterLayout.getPaddingLeft();
        }
        if (this.mBackMoveFactor == 0.0f) {
            this.mBackMoveFactor = ((float) this.mBackMoveWidth) / ((float) this.mSliderMoveWidth);
        }
    }

    public View getChildTabViewAt(int index) {
        return getChildAt(index + 1);
    }

    /* access modifiers changed from: package-private */
    public void setTabContent(FrameLayout layout) {
        this.mTabContent = layout;
    }

    public int getTabCount() {
        return getChildCount() - 1;
    }

    public void setCurrentTab(int tabIndex) {
        if (tabIndex >= 0 && tabIndex < getTabCount()) {
            ((NavigatorTabItem) getChildAt(this.mSelectedTab + 1)).selectItem(false);
            ((NavigatorTabItem) getChildAt(tabIndex + 1)).selectItem(true);
            this.mSelectionChangedListener.onTabSelectionChanged(tabIndex, true);
            this.mSelectedTab = tabIndex;
        }
    }

    public void focusCurrentTab2(int index) {
        int oldTab = this.mSelectedTab;
        setCurrentTab(index);
        if (oldTab != index) {
        }
    }

    /* access modifiers changed from: package-private */
    public void setTabSelectionListener(OnTabSelectionChanged listener) {
        this.mSelectionChangedListener = listener;
    }

    public void onFocusChange2(View v, boolean hasFocus) {
        if (v == this && hasFocus) {
            getChildTabViewAt(this.mSelectedTab).requestFocus();
        } else if (hasFocus) {
            int numTabs = getTabCount();
            for (int i = 0; i < numTabs; i++) {
                if (getChildTabViewAt(i) == v) {
                    setCurrentTab(i);
                    this.mSelectionChangedListener.onTabSelectionChanged(i, false);
                    return;
                }
            }
        }
    }
}
