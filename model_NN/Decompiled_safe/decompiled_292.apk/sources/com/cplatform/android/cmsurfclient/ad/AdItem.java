package com.cplatform.android.cmsurfclient.ad;

public class AdItem {
    public static final int TEXTCOLOR_NORMAL = -1;
    public static final int TEXTCOLOR_PRESSED = -11497280;
    public String title;
    public String url;

    public AdItem(String item_title, String item_url) {
        this.title = item_title;
        this.url = item_url;
    }
}
