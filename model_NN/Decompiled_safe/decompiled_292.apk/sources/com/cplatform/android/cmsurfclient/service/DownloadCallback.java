package com.cplatform.android.cmsurfclient.service;

public interface DownloadCallback {
    void onDownload(long j, long j2);
}
