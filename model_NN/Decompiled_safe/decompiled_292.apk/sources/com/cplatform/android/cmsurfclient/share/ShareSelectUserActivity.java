package com.cplatform.android.cmsurfclient.share;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Contacts;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;

public class ShareSelectUserActivity extends Activity {
    private static final int MAX_SHARE_USERNUMER = 5;
    private static final int MESSAGE_READCONTACTS = 6;
    private final String DEBUG_TAG = "ShareSelectUserActivity";
    private Button mBtnCancel;
    private Button mBtnOK;
    /* access modifiers changed from: private */
    public ArrayList<ShareContactItem> mContactList;
    /* access modifiers changed from: private */
    public ShareContactAdapter mContactListAdapter;
    /* access modifiers changed from: private */
    public ListView mContactListView;
    /* access modifiers changed from: private */
    public Handler mMainHandler = null;
    /* access modifiers changed from: private */
    public ArrayList<ShareContactItem> mMatchedUserList;
    /* access modifiers changed from: private */
    public ProgressDialog mReadProgress = null;
    /* access modifiers changed from: private */
    public AutoCompleteTextView mSearchEditView;
    private TextView mSelectTip;
    /* access modifiers changed from: private */
    public ArrayList<ShareContactItem> mSelectedUserList;
    private ShareContactAdapter mSelectedUserListAdapter;
    private ListView mSelectedUserListView;

    public void onCreate(Bundle savedInstanceState) {
        String targetUsers;
        String userName;
        String phoneNumber;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.share_contacts);
        this.mBtnOK = (Button) findViewById(R.id.share_contacts_btn_ok);
        this.mBtnCancel = (Button) findViewById(R.id.share_contacts_btn_cancel);
        this.mBtnOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String user;
                String selectedUsers = WindowAdapter2.BLANK_URL;
                for (int i = 0; i < ShareSelectUserActivity.this.mSelectedUserList.size(); i++) {
                    ShareContactItem item = (ShareContactItem) ShareSelectUserActivity.this.mSelectedUserList.get(i);
                    if (item.mUserName == null || item.mUserName.length() <= 0) {
                        user = item.mPhoneNumber;
                    } else {
                        user = item.mPhoneNumber + "(" + item.mUserName + ")";
                    }
                    selectedUsers = selectedUsers + user;
                    if (i != ShareSelectUserActivity.this.mSelectedUserList.size() - 1) {
                        selectedUsers = selectedUsers + ";";
                    }
                }
                ShareSelectUserActivity.this.setResult(-1, new Intent().setAction(selectedUsers));
                ShareSelectUserActivity.this.finish();
            }
        });
        this.mBtnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ShareSelectUserActivity.this.setResult(0, null);
                ShareSelectUserActivity.this.finish();
            }
        });
        this.mSelectTip = (TextView) findViewById(R.id.share_contacts_tip);
        this.mContactListView = (ListView) findViewById(R.id.share_listview_contacts);
        View viewHeader = View.inflate(this, R.layout.share_contacts_header, null);
        this.mContactListView.addHeaderView(viewHeader);
        this.mContactList = new ArrayList<>();
        this.mSelectedUserList = new ArrayList<>();
        this.mMatchedUserList = new ArrayList<>();
        Bundle b = getIntent().getExtras();
        if (!(b == null || (targetUsers = b.getString("share_targetusers")) == null || targetUsers.length() <= 0)) {
            String[] userArray = targetUsers.split(";");
            for (String userInfo : userArray) {
                int beginPos = userInfo.indexOf("(");
                int endPos = userInfo.indexOf(")");
                if (beginPos <= 0 || endPos <= 0) {
                    userName = WindowAdapter2.BLANK_URL;
                    phoneNumber = userInfo;
                } else {
                    phoneNumber = userInfo.substring(0, beginPos);
                    userName = userInfo.substring(beginPos + 1, endPos);
                }
                this.mSelectedUserList.add(new ShareContactItem(userName, phoneNumber, true));
            }
        }
        if (this.mSelectedUserList.size() > 0) {
            this.mSelectTip.setVisibility(8);
        } else {
            this.mSelectTip.setVisibility(0);
        }
        this.mSearchEditView = (AutoCompleteTextView) viewHeader.findViewById(R.id.share_edit_search);
        this.mSearchEditView.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String search = ShareSelectUserActivity.this.mSearchEditView.getText().toString();
                int strLen = search.length();
                if (strLen > 0) {
                    ShareSelectUserActivity.this.mMatchedUserList.clear();
                    for (int i = 0; i < ShareSelectUserActivity.this.mContactList.size(); i++) {
                        ShareContactItem item = (ShareContactItem) ShareSelectUserActivity.this.mContactList.get(i);
                        Log.d("ShareSelectUserActivity", "Username:" + item.mUserName + ", length:" + item.mUserName.length());
                        if (strLen <= item.mUserName.length() && search.equalsIgnoreCase(item.mUserName.substring(0, strLen))) {
                            ShareSelectUserActivity.this.mMatchedUserList.add(item);
                        } else if (strLen <= item.mPhoneNumber.length() && search.equalsIgnoreCase(item.mPhoneNumber.substring(0, strLen))) {
                            ShareSelectUserActivity.this.mMatchedUserList.add(item);
                        }
                    }
                    ShareSelectUserActivity.this.mContactListAdapter.notifyDataSetChanged();
                    return;
                }
                ShareSelectUserActivity.this.mMatchedUserList.clear();
                for (int i2 = 0; i2 < ShareSelectUserActivity.this.mContactList.size(); i2++) {
                    ShareSelectUserActivity.this.mMatchedUserList.add((ShareContactItem) ShareSelectUserActivity.this.mContactList.get(i2));
                }
                ShareSelectUserActivity.this.mContactListAdapter.notifyDataSetChanged();
            }
        });
        this.mSelectedUserListAdapter = new ShareContactAdapter(this, this.mSelectedUserList);
        this.mSelectedUserListView = (ListView) findViewById(R.id.share_listview_selected);
        this.mSelectedUserListView.setAdapter((ListAdapter) this.mSelectedUserListAdapter);
        this.mMainHandler = new Handler() {
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 6:
                        if (ShareSelectUserActivity.this.mReadProgress != null) {
                            ShareSelectUserActivity.this.mReadProgress.cancel();
                        }
                        ShareContactAdapter unused = ShareSelectUserActivity.this.mContactListAdapter = new ShareContactAdapter(ShareSelectUserActivity.this, ShareSelectUserActivity.this.mMatchedUserList);
                        ShareSelectUserActivity.this.mContactListView.setAdapter((ListAdapter) ShareSelectUserActivity.this.mContactListAdapter);
                        return;
                    default:
                        return;
                }
            }
        };
        this.mReadProgress = new ProgressDialog(this);
        this.mReadProgress.setIndeterminate(true);
        this.mReadProgress.setCancelable(false);
        this.mReadProgress.setTitle((int) R.string.ishare);
        this.mReadProgress.setMessage(getResources().getString(R.string.ishare_reading_contacts));
        this.mReadProgress.show();
        new Thread() {
            public void run() {
                ShareSelectUserActivity.this.readContacts();
                if (ShareSelectUserActivity.this.mMainHandler != null) {
                    ShareSelectUserActivity.this.mMainHandler.sendEmptyMessage(6);
                }
            }
        }.start();
    }

    public void readContacts() {
        this.mMatchedUserList.clear();
        String[] columns = {QueryApList.Carriers._ID, QueryApList.Carriers.NAME};
        Uri contacts = Contacts.People.CONTENT_URI;
        Cursor cur = managedQuery(contacts, columns, null, null, QueryApList.Carriers.DEFAULT_SORT_ORDER);
        if (cur.moveToFirst()) {
            Cursor newcur = null;
            do {
                String name = cur.getString(cur.getColumnIndex(QueryApList.Carriers.NAME));
                if (name != null) {
                    while (name.indexOf(" ") >= 0) {
                        name = name.replace(" ", WindowAdapter2.BLANK_URL);
                        Log.d("ShareSelectUserActivity", name);
                    }
                    long personId = cur.getLong(cur.getColumnIndex(QueryApList.Carriers._ID));
                    Cursor phonesCursor = managedQuery(Uri.withAppendedPath(ContentUris.withAppendedId(contacts, personId), "phones"), new String[]{QueryApList.Carriers._ID, QueryApList.Carriers.TYPE, "number"}, null, null, null);
                    if (phonesCursor.moveToFirst()) {
                        do {
                            int phoneType = phonesCursor.getInt(phonesCursor.getColumnIndex(QueryApList.Carriers.TYPE));
                            if (phoneType == 2 || phoneType == 3 || phoneType == 1) {
                                String phoneNo = phonesCursor.getString(phonesCursor.getColumnIndex("number"));
                                Log.i("ShareSelectUserActivity", "Contact info: id=" + personId + ", name=" + name + ", phone=" + phoneNo);
                                if (phoneNo.length() >= 11) {
                                    if (phoneNo.startsWith("+86")) {
                                        phoneNo = phoneNo.substring(3);
                                    }
                                    if (phoneNo.startsWith("1")) {
                                        ShareContactItem shareContactItem = new ShareContactItem(name, phoneNo, false);
                                        boolean isAlreadySelected = false;
                                        int i = 0;
                                        while (true) {
                                            if (i >= this.mSelectedUserList.size()) {
                                                break;
                                            }
                                            ShareContactItem item = this.mSelectedUserList.get(i);
                                            if (item.mUserName.equals(name) && item.mPhoneNumber.equals(phoneNo)) {
                                                isAlreadySelected = true;
                                                break;
                                            }
                                            i++;
                                        }
                                        if (!isAlreadySelected) {
                                            this.mContactList.add(shareContactItem);
                                            this.mMatchedUserList.add(shareContactItem);
                                        }
                                    }
                                }
                            }
                        } while (phonesCursor.moveToNext());
                    }
                    if (phonesCursor != null) {
                        phonesCursor.close();
                    }
                }
            } while (cur.moveToNext());
            if (newcur != null) {
                newcur.close();
            }
        }
        if (cur != null) {
            cur.close();
        }
    }

    public boolean onUserSelectionChanged(ShareContactItem item) {
        if (item.mIsSelected) {
            Log.i("ShareSelectUserActivity", "Selected user's name:" + item.mUserName + ", phonenumber:" + item.mPhoneNumber);
            if (this.mSelectedUserListAdapter.getCount() < 5) {
                int i = 0;
                while (true) {
                    if (i < this.mContactListAdapter.getCount()) {
                        ShareContactItem itemContact = (ShareContactItem) this.mContactListAdapter.getItem(i);
                        if (itemContact != null && itemContact.mUserName.equals(item.mUserName) && itemContact.mPhoneNumber.equals(item.mPhoneNumber)) {
                            this.mContactListAdapter.remove(itemContact);
                            break;
                        }
                        i++;
                    } else {
                        break;
                    }
                }
                int i2 = 0;
                while (true) {
                    if (i2 >= this.mContactList.size()) {
                        break;
                    }
                    ShareContactItem itemContact2 = this.mContactList.get(i2);
                    if (itemContact2.mUserName.equals(item.mUserName) && itemContact2.mPhoneNumber.equals(item.mPhoneNumber)) {
                        this.mContactList.remove(i2);
                        break;
                    }
                    i2++;
                }
                this.mSelectedUserListAdapter.add(item);
            } else {
                Toast.makeText(this, getResources().getString(R.string.ishare_max_friends_left) + 5 + getResources().getString(R.string.ishare_max_friends_right), 1).show();
                return false;
            }
        } else {
            Log.i("ShareSelectUserActivity", "Deselected user's name:" + item.mUserName + ", phonenumber:" + item.mPhoneNumber);
            int i3 = 0;
            while (true) {
                if (i3 >= this.mSelectedUserListAdapter.getCount()) {
                    break;
                }
                ShareContactItem itemContact3 = (ShareContactItem) this.mSelectedUserListAdapter.getItem(i3);
                if (itemContact3.mUserName.equals(item.mUserName) && itemContact3.mPhoneNumber.equals(item.mPhoneNumber)) {
                    this.mSelectedUserListAdapter.remove(itemContact3);
                    break;
                }
                i3++;
            }
            this.mContactListAdapter.add(item);
            this.mContactList.add(item);
        }
        if (this.mSelectedUserList.size() > 0) {
            this.mSelectTip.setVisibility(8);
        } else {
            this.mSelectTip.setVisibility(0);
        }
        return true;
    }
}
