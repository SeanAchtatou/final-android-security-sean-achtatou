package com.cplatform.android.cmsurfclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.download.ui.DownloadHomeTabActivity;
import com.cplatform.android.cmsurfclient.history.HistoryDB;
import com.cplatform.android.cmsurfclient.network.ConnectHelper;
import com.cplatform.android.cmsurfclient.network.NetworkManager;
import com.cplatform.android.cmsurfclient.preference.SurfBrowserSettings;
import com.cplatform.android.cmsurfclient.preference.SurfPreferenceActivity;
import com.cplatform.android.cmsurfclient.searchtip.SearchTipDB;
import com.cplatform.android.cmsurfclient.service.DownloadCallback;
import com.cplatform.android.cmsurfclient.service.IServiceRequest;
import com.cplatform.android.cmsurfclient.service.ServiceRequest;
import com.cplatform.android.cmsurfclient.service.entry.Msb;
import com.cplatform.android.cmsurfclient.service.entry.Share;
import com.cplatform.android.cmsurfclient.service.entry.Upgrade;
import com.cplatform.android.cmsurfclient.utils.PreferenceUtil;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import com.cplatform.android.cmsurfclient.windows.WindowItemNew;
import com.cplatform.android.cmsurfclient.windows.WindowManagerView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class SurfManagerActivity extends Activity implements IServiceRequest, DownloadCallback {
    protected static final Pattern ACCEPTED_URI_SCHEMA = Pattern.compile("(?i)((?:http|https|file):\\/\\/|(?:inline|data|about|content|javascript):)(.*)");
    public static final int CONTEXTMENU_BROWSERVIEW_ADDBOOKMARK = 39;
    public static final int CONTEXTMENU_BROWSERVIEW_BOOKMARK = 33;
    public static final int CONTEXTMENU_BROWSERVIEW_CLOSEPAGE = 38;
    public static final int CONTEXTMENU_BROWSERVIEW_DOWNLOADPICTURE = 36;
    public static final int CONTEXTMENU_BROWSERVIEW_OPEN = 31;
    public static final int CONTEXTMENU_BROWSERVIEW_OPENBACKGROUND = 40;
    public static final int CONTEXTMENU_BROWSERVIEW_OPENNEW = 32;
    public static final int CONTEXTMENU_BROWSERVIEW_PAGEPROPERTY = 37;
    public static final int CONTEXTMENU_BROWSERVIEW_SHARELINK = 34;
    public static final int CONTEXTMENU_BROWSERVIEW_SHAREPICTURE = 35;
    public static final int CONTEXTMENU_HOMEVIEW_BOOKMARK = 22;
    public static final int CONTEXTMENU_HOMEVIEW_OPENNEW = 21;
    public static final int CONTEXTMENU_HOMEVIEW_SHARELINK = 23;
    public static final int CONTEXTMENU_HOMEVIEW_SHAREPICTURE = 24;
    public static final int CONTEXTMENU_QUICKLINK_DELETE = 12;
    public static final int CONTEXTMENU_QUICKLINK_EDIT = 11;
    public static final int CONTEXTMENU_QUICKLINK_OPENNEW = 13;
    public static final FrameLayout.LayoutParams COVER_SCREEN_PARAMS = new FrameLayout.LayoutParams(-1, -1);
    private static final String CUSTOM_USERAGENT = "CMSurfClient";
    /* access modifiers changed from: private */
    public static final String LOG_TAG = SurfManagerActivity.class.getSimpleName();
    private static final char[] MAP_NUM;
    private static int MAX_WINDOWS_NUM = 9;
    private static final int MESSAGE_BLANK_LONGCLICK = 9;
    private static final int MESSAGE_CHECKUPGRADE = 5;
    private static final int MESSAGE_FOCUS_NODE_HREF = 8;
    private static final int MESSAGE_HOME = 3;
    private static final int MESSAGE_NETWORK = 7;
    private static final int MESSAGE_NETWORK_TIMEOUT = 10;
    private static final int MESSAGE_SHARE = 6;
    private static final int MESSAGE_SPLASH_PROGRESSREFRESH = 4;
    private static final int MESSAGE_UPGRADE = 1;
    private static final int MESSAGE_UPGRADEFAIL = 2;
    public static final String MODEL_DOPOD_A8188 = "A8188";
    public static final int NETWORK_STATUS_CONNECTED = 2;
    public static final int NETWORK_STATUS_CONNECTING = 1;
    public static final int NETWORK_STATUS_DISCONNECTED = 3;
    public static final int NETWORK_STATUS_UNKNOWN = 0;
    public static final int SHOW_VIEW_BROWSER = 2;
    public static final int SHOW_VIEW_HOME = 1;
    public static final int SHOW_VIEW_NONE = 0;
    public static final int SHOW_VIEW_WINDOWMANAGER = 3;
    public static Msb mMsb = null;
    public static NetworkManager mNetworkMgr = null;
    private static boolean showUpdateLog = false;
    public static HashSet<String> wapDomain = new HashSet<>();
    public static HashSet<String> whiteDomain = new HashSet<>();
    /* access modifiers changed from: private */
    public float brightness = -1.0f;
    public boolean isClickForward = false;
    private WindowManagerView mBrowserManager;
    public String mChannelID = WindowAdapter2.BLANK_URL;
    /* access modifiers changed from: private */
    public ProgressDialog mCheckUpgradeProgress = null;
    private int mCurControl = 0;
    public String mDeviceID = WindowAdapter2.BLANK_URL;
    private FrameLayout mFrameLayout;
    private HomeView mHomeView;
    public boolean mIsFullScreen = false;
    public WindowItemNew mLastWindowItem = null;
    public ArrayList<WindowItemNew> mListWebView = null;
    /* access modifiers changed from: private */
    public int mLockScreenSelection = R.id.lockscreen_none;
    /* access modifiers changed from: private */
    public Handler mMainHandler = null;
    /* access modifiers changed from: private */
    public int mNetworkStatus = 0;
    private TimerTask mNetworkTask = null;
    private Timer mNetworkTimer = null;
    public int[] mNewWindowIcon = null;
    public SurfBrowserSettings mSettings = null;
    /* access modifiers changed from: private */
    public ProgressDialog mSharePageProgress = null;
    /* access modifiers changed from: private */
    public ProgressDialog mUpgradeProgress = null;
    /* access modifiers changed from: private */
    public int mUpgradeProgressValue = -1;
    public String mUserAgent = WindowAdapter2.BLANK_URL;
    /* access modifiers changed from: private */
    public SeekBar progressSeekBar = null;
    /* access modifiers changed from: private */
    public int splashProgress = MESSAGE_NETWORK_TIMEOUT;

    static /* synthetic */ int access$608(SurfManagerActivity x0) {
        int i = x0.splashProgress;
        x0.splashProgress = i + 1;
        return i;
    }

    static /* synthetic */ int access$620(SurfManagerActivity x0, int x1) {
        int i = x0.splashProgress - x1;
        x0.splashProgress = i;
        return i;
    }

    static {
        char[] cArr = new char[MESSAGE_NETWORK_TIMEOUT];
        // fill-array-data instruction
        cArr[0] = 54;
        cArr[1] = 56;
        cArr[2] = 53;
        cArr[3] = 55;
        cArr[4] = 49;
        cArr[5] = 51;
        cArr[6] = 57;
        cArr[7] = 50;
        cArr[8] = 48;
        cArr[9] = 52;
        MAP_NUM = cArr;
    }

    /* access modifiers changed from: protected */
    public WebView getCurWebView() {
        BrowserViewNew browser;
        if (this.mLastWindowItem == null || (browser = this.mLastWindowItem.browserView) == null) {
            return null;
        }
        return browser.mWebViewMgr.getCurWebView();
    }

    public void onCreate(Bundle savedInstanceState) {
        Log.e(LOG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView((int) R.layout.surf_manager);
        this.mFrameLayout = (FrameLayout) getWindow().getDecorView().findViewById(16908290);
        loadSettings();
        showSplash();
        StringBuilder requestParams = new StringBuilder();
        requestParams.append(SurfBrowser.isOPhone() ? "ua=MOBILESURFING_OPHONE20" : "ua=MOBILESURFING_ANDROID16");
        mMsb = new ServiceRequest(this).readLocalMsb(requestParams.toString());
        this.mMainHandler = new Handler() {
            public void handleMessage(Message msg) {
                BrowserViewNew browser;
                WebView view;
                BrowserViewNew browser2;
                switch (msg.what) {
                    case 1:
                        boolean unused = SurfManagerActivity.this.onUpgrade();
                        return;
                    case 2:
                        new AlertDialog.Builder(SurfManagerActivity.this).setTitle((int) R.string.upgrade_fail).setMessage((int) R.string.upgrade_fail_message).setPositiveButton((int) R.string.common_ok, (DialogInterface.OnClickListener) null).create().show();
                        return;
                    case 3:
                        boolean unused2 = SurfManagerActivity.this.onReloadData();
                        SeekBar unused3 = SurfManagerActivity.this.progressSeekBar = null;
                        SurfManagerActivity.this.showUpdateLog();
                        return;
                    case 4:
                        if (SurfManagerActivity.this.progressSeekBar != null) {
                            SurfManagerActivity.this.progressSeekBar.setProgress(SurfManagerActivity.this.splashProgress);
                            return;
                        }
                        return;
                    case 5:
                        boolean unused4 = SurfManagerActivity.this.onCheckUpgrade();
                        return;
                    case 6:
                        boolean unused5 = SurfManagerActivity.this.onShareResult();
                        return;
                    case 7:
                        SurfManagerActivity.this.checkServer();
                        return;
                    case 8:
                        String url = (String) msg.getData().get("url");
                        String title = (String) msg.getData().get(Downloads.COLUMN_TITLE);
                        if (!TextUtils.isEmpty(url) && (view = (WebView) ((HashMap) msg.obj).get("webview")) != null && SurfManagerActivity.this.getCurWebView() == view && SurfManagerActivity.this.mLastWindowItem != null && (browser2 = SurfManagerActivity.this.mLastWindowItem.browserView) != null) {
                            browser2.onContextItemSelected(msg.arg1, url, title);
                            return;
                        }
                        return;
                    case 9:
                        if (SurfManagerActivity.this.mLastWindowItem != null && (browser = SurfManagerActivity.this.mLastWindowItem.browserView) != null) {
                            browser.onContextItemSelected(msg.arg1, browser.mItem.url, browser.mItem.title);
                            return;
                        }
                        return;
                    case SurfManagerActivity.MESSAGE_NETWORK_TIMEOUT /*10*/:
                        if (SurfManagerActivity.this.mNetworkStatus == 1) {
                            int unused6 = SurfManagerActivity.this.mNetworkStatus = 3;
                            for (int i = 0; i < SurfManagerActivity.this.mListWebView.size(); i++) {
                                BrowserViewNew browser3 = SurfManagerActivity.this.mListWebView.get(i).browserView;
                                if (browser3 != null) {
                                    browser3.onNetworkStatusChanged(SurfManagerActivity.this.mNetworkStatus);
                                }
                            }
                            Toast.makeText(SurfManagerActivity.this, (int) R.string.no_data_connection, 1).show();
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        };
        this.mListWebView = new ArrayList<>();
        this.mLastWindowItem = new WindowItemNew();
        this.mListWebView.add(this.mLastWindowItem);
        this.mNewWindowIcon = new int[9];
        this.mNewWindowIcon[0] = R.drawable.ico_tool_neww_1;
        this.mNewWindowIcon[1] = R.drawable.ico_tool_neww_2;
        this.mNewWindowIcon[2] = R.drawable.ico_tool_neww_3;
        this.mNewWindowIcon[3] = R.drawable.ico_tool_neww_4;
        this.mNewWindowIcon[4] = R.drawable.ico_tool_neww_5;
        this.mNewWindowIcon[5] = R.drawable.ico_tool_neww_6;
        this.mNewWindowIcon[6] = R.drawable.ico_tool_neww_7;
        this.mNewWindowIcon[7] = R.drawable.ico_tool_neww_8;
        this.mNewWindowIcon[8] = R.drawable.ico_tool_neww_9;
        this.mNetworkStatus = 1;
        mNetworkMgr = new NetworkManager(this, SurfBrowser.isOPhone());
        mNetworkMgr.connect();
        final String url = getUrlFromIntent(getIntent());
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (TextUtils.isEmpty(url)) {
                    SurfManagerActivity.this.showCurrentWindowItem();
                    SurfManagerActivity.this.showUpdateLog();
                    return;
                }
                SurfManagerActivity.this.browseInCurrentWindow(url, -1);
            }
        }, 1000);
    }

    private String getChannelID() {
        Element root;
        NodeList nodeList;
        Node node;
        NamedNodeMap attrMap;
        Node id;
        String channelID = WindowAdapter2.BLANK_URL;
        try {
            DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputStream is = getResources().getAssets().open("channel.xml");
            if (!(is == null || (root = docBuilder.parse(is).getDocumentElement()) == null || (nodeList = root.getElementsByTagName("channel")) == null || (node = nodeList.item(0)) == null || (attrMap = node.getAttributes()) == null || (id = attrMap.getNamedItem("id")) == null)) {
                channelID = id.getNodeValue() != null ? id.getNodeValue() : WindowAdapter2.BLANK_URL;
            }
        } catch (IOException e) {
        } catch (SAXException e2) {
        } catch (ParserConfigurationException e3) {
        } finally {
        }
        return channelID;
    }

    public void onConfigurationChanged(Configuration newConfig) {
        BrowserViewNew browser;
        BrowserViewNew browser2;
        Log.e(LOG_TAG, "onConfigurationChanged: " + newConfig.toString());
        if (newConfig.orientation == 2) {
            if (!this.mIsFullScreen && SurfBrowserSettings.getInstance().isAutoFullScreen()) {
                if (this.mCurControl == 2) {
                    if (!(this.mLastWindowItem == null || (browser2 = this.mLastWindowItem.browserView) == null)) {
                        browser2.setFullScreen(true);
                    }
                } else if (this.mCurControl == 1 && this.mHomeView != null) {
                    this.mHomeView.setFullScreen(true);
                    this.mHomeView.restartScrollMessage();
                }
            }
        } else if (newConfig.orientation == 1 && this.mIsFullScreen && SurfBrowserSettings.getInstance().isAutoFullScreen()) {
            if (this.mCurControl == 2) {
                if (!(this.mLastWindowItem == null || (browser = this.mLastWindowItem.browserView) == null)) {
                    browser.setFullScreen(false);
                }
            } else if (this.mCurControl == 1 && this.mHomeView != null) {
                this.mHomeView.setFullScreen(false);
                this.mHomeView.restartScrollMessage();
            }
        }
        super.onConfigurationChanged(newConfig);
    }

    public int getNetworkStatus() {
        return this.mNetworkStatus;
    }

    private void loadSettings() {
        SurfBrowserSettings.getInstance().loadFromDb(this);
        this.mSettings = SurfBrowserSettings.getInstance();
        this.mSettings.setSurfManager(this);
        whiteDomain.add(".139.com");
        whiteDomain.add(".10086.cn");
        whiteDomain.add(".mobi");
        whiteDomain.add(".monternet.com");
        whiteDomain.add(".i139.cn");
        whiteDomain.add("rtsp://");
        whiteDomain.add("android_asset");
        wapDomain.add(ConnectHelper.FEATURE_ENABLE_WAP);
        wapDomain.add("m");
        wapDomain.add("3g");
        DisplayMetrics metrics = new DisplayMetrics();
        try {
            ((WindowManager) getSystemService("window")).getDefaultDisplay().getMetrics(metrics);
        } catch (Exception e) {
            metrics.widthPixels = 320;
            metrics.heightPixels = 480;
            metrics.density = 1.0f;
        }
        this.mUserAgent = "CMSurfClient-" + (SurfBrowser.isOPhone() ? "Android" : "Android") + "-W" + String.valueOf(metrics.widthPixels) + (SurfBrowser.isOPhone() ? "/MOBILESURFING_OPHONE20/" : "/MOBILESURFING_ANDROID16/");
        this.mDeviceID = getDeviceID();
        this.mChannelID = getChannelID();
    }

    private String getDeviceID() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
            return telephonyManager.getDeviceId() != null ? telephonyManager.getDeviceId() : WindowAdapter2.BLANK_URL;
        } catch (Exception e) {
            return WindowAdapter2.BLANK_URL;
        }
    }

    private static String mapNum(String tmp) {
        int pos;
        if (tmp == null) {
            return null;
        }
        String ret = WindowAdapter2.BLANK_URL;
        int len = tmp.length();
        for (int i = 0; i < len; i++) {
            char c = tmp.charAt(i);
            if (c >= '0' && c <= '9' && (pos = c - '0') >= 0 && pos <= 9) {
                c = MAP_NUM[pos];
            }
            ret = ret + c;
        }
        return ret;
    }

    private void initTimer() {
        this.mNetworkTimer = new Timer();
        this.mNetworkTask = new TimerTask() {
            public void run() {
                Log.e(SurfManagerActivity.LOG_TAG, "Network connecting time out!!!");
                if (SurfManagerActivity.this.mMainHandler != null) {
                    SurfManagerActivity.this.mMainHandler.sendEmptyMessage(SurfManagerActivity.MESSAGE_NETWORK_TIMEOUT);
                }
            }
        };
        try {
            this.mNetworkTimer.schedule(this.mNetworkTask, 3000);
        } catch (Exception e) {
            Log.e(LOG_TAG, "exception: " + e.toString());
        }
    }

    private void showSplash() {
        this.progressSeekBar = (SeekBar) findViewById(R.id.progress_splash);
        if (this.progressSeekBar != null) {
            new Thread() {
                public void run() {
                    while (SurfManagerActivity.this.progressSeekBar != null) {
                        SurfManagerActivity.access$608(SurfManagerActivity.this);
                        if (SurfManagerActivity.this.splashProgress > 100) {
                            SurfManagerActivity.access$620(SurfManagerActivity.this, 100);
                        }
                        if (SurfManagerActivity.this.mMainHandler != null) {
                            SurfManagerActivity.this.mMainHandler.sendEmptyMessage(4);
                        }
                        try {
                            Thread.sleep(20);
                        } catch (InterruptedException e) {
                        }
                    }
                }
            }.start();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.e(LOG_TAG, "onResume");
        CookieSyncManager.getInstance().startSync();
        setAdapterCookies();
        if (mNetworkMgr != null) {
            mNetworkMgr.onResume();
        }
        if (this.mLastWindowItem != null) {
            BrowserViewNew browser = this.mLastWindowItem.browserView;
            if (browser != null) {
                browser.onResume();
            }
            if (this.mHomeView != null) {
                this.mHomeView.onResume(this.mLastWindowItem.tab);
            }
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Log.e(LOG_TAG, "onPause");
        CookieSyncManager.getInstance().stopSync();
        if (mNetworkMgr != null) {
            mNetworkMgr.onPause();
        }
        if (this.mHomeView != null) {
            this.mHomeView.removeScrollMessage();
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.e(LOG_TAG, "onDestory");
        if (mNetworkMgr != null) {
            mNetworkMgr.disconnect();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Log.e(LOG_TAG, "onNewIntent ~~~~~~~ intent = " + intent);
        String url = getUrlFromIntent(intent);
        if (TextUtils.isEmpty(url)) {
            showCurrentWindowItem();
        } else {
            browseInCurrentWindow(url, -1);
        }
        super.onNewIntent(intent);
    }

    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case CONTEXTMENU_QUICKLINK_EDIT /*11*/:
            case CONTEXTMENU_QUICKLINK_DELETE /*12*/:
            case CONTEXTMENU_QUICKLINK_OPENNEW /*13*/:
            case CONTEXTMENU_HOMEVIEW_OPENNEW /*21*/:
            case CONTEXTMENU_HOMEVIEW_BOOKMARK /*22*/:
            case CONTEXTMENU_HOMEVIEW_SHARELINK /*23*/:
            case CONTEXTMENU_HOMEVIEW_SHAREPICTURE /*24*/:
                if (this.mHomeView != null) {
                    this.mHomeView.onContextItemSelected(item);
                    break;
                }
                break;
            case CONTEXTMENU_BROWSERVIEW_OPEN /*31*/:
            case CONTEXTMENU_BROWSERVIEW_OPENNEW /*32*/:
            case CONTEXTMENU_BROWSERVIEW_BOOKMARK /*33*/:
            case CONTEXTMENU_BROWSERVIEW_SHARELINK /*34*/:
            case CONTEXTMENU_BROWSERVIEW_SHAREPICTURE /*35*/:
            case CONTEXTMENU_BROWSERVIEW_DOWNLOADPICTURE /*36*/:
            case CONTEXTMENU_BROWSERVIEW_OPENBACKGROUND /*40*/:
                if (this.mLastWindowItem != null) {
                    WebView webView = getCurWebView();
                    if (webView != null) {
                        int id = item.getItemId();
                        HashMap<String, WebView> hrefMap = new HashMap<>();
                        hrefMap.put("webview", webView);
                        webView.requestFocusNodeHref(this.mMainHandler.obtainMessage(8, id, 0, hrefMap));
                        break;
                    } else {
                        return false;
                    }
                }
                break;
            case CONTEXTMENU_BROWSERVIEW_PAGEPROPERTY /*37*/:
            case CONTEXTMENU_BROWSERVIEW_CLOSEPAGE /*38*/:
            case CONTEXTMENU_BROWSERVIEW_ADDBOOKMARK /*39*/:
                this.mMainHandler.sendMessage(this.mMainHandler.obtainMessage(9, item.getItemId(), 0));
                break;
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        BrowserViewNew browser;
        Log.e(LOG_TAG, "onKeyDown with keyCode=" + keyCode);
        if (keyCode != 4 && keyCode != 82 && keyCode != 24 && keyCode != 25) {
            return super.onKeyDown(keyCode, event);
        }
        switch (this.mCurControl) {
            case 1:
                if (this.mHomeView != null) {
                    this.mHomeView.onKeyDown(keyCode, event);
                    break;
                }
                break;
            case 2:
                if (!(this.mLastWindowItem == null || (browser = this.mLastWindowItem.browserView) == null)) {
                    browser.onKeyDown(keyCode, event);
                    break;
                }
            case 3:
                if (this.mBrowserManager != null) {
                    this.mBrowserManager.goBack();
                    break;
                }
                break;
        }
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        BrowserViewNew browser;
        BrowserViewNew browser2;
        if (resultCode != 0 && data != null) {
            String act = data.getAction();
            switch (requestCode) {
                case 1:
                    if (!TextUtils.isEmpty(act)) {
                        browseInCurrentWindow(act, -1);
                        return;
                    }
                    return;
                case 2:
                    if (!TextUtils.isEmpty(act)) {
                        browseInCurrentWindow(URLUtil.guessUrl(act), -1);
                        return;
                    }
                    return;
                case 3:
                default:
                    return;
                case 4:
                    if (!TextUtils.isEmpty(act)) {
                        switch (this.mCurControl) {
                            case 1:
                                if (this.mHomeView != null) {
                                    this.mHomeView.onOptionMenu(act);
                                    return;
                                }
                                return;
                            case 2:
                                if (this.mLastWindowItem != null && (browser = this.mLastWindowItem.browserView) != null) {
                                    browser.onOptionMenu(act);
                                    return;
                                }
                                return;
                            default:
                                return;
                        }
                    } else {
                        return;
                    }
                case 5:
                    if (!TextUtils.isEmpty(act) && !act.equalsIgnoreCase("none")) {
                        browseInCurrentWindow(act, -1, false);
                        return;
                    }
                    return;
                case 6:
                    this.mSettings.update();
                    setAdapterCookies();
                    if (SurfBrowserSettings.getInstance().isCMWAPPreferred() && !mNetworkMgr.getNetworkType().equalsIgnoreCase("CMWAP") && !mNetworkMgr.getNetworkType().equalsIgnoreCase("WIFI")) {
                        mNetworkMgr.connect();
                    }
                    if (this.mCurControl == 2 && this.mLastWindowItem != null && (browser2 = this.mLastWindowItem.browserView) != null) {
                        browser2.onResume();
                        return;
                    }
                    return;
                case 7:
                    if (!TextUtils.isEmpty(act) && act.equalsIgnoreCase("quit")) {
                        finish();
                        return;
                    }
                    return;
                case 8:
                case 9:
                    if (!TextUtils.isEmpty(act)) {
                        sharePage(act);
                        return;
                    }
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setAdapterCookies() {
        CookieManager cookieManager = CookieManager.getInstance();
        String adapterCookie = SurfBrowserSettings.getInstance().getAdapterCookies();
        cookieManager.setCookie("221.130.11.218:30328", adapterCookie);
        CookieSyncManager.getInstance().sync();
        Log.e(LOG_TAG, "setAdapterCookies: " + adapterCookie);
    }

    public void onNetworkToggle(boolean up) {
        Log.e(LOG_TAG, "onNetworkToggle: " + up);
        if (up) {
            this.mNetworkStatus = 2;
        } else {
            this.mNetworkStatus = 1;
            initTimer();
        }
        if (this.mListWebView != null) {
            for (int i = 0; i < this.mListWebView.size(); i++) {
                BrowserViewNew browser = this.mListWebView.get(i).browserView;
                if (browser != null) {
                    browser.onNetworkStatusChanged(this.mNetworkStatus);
                }
            }
        }
        if (up && this.mMainHandler != null) {
            this.mMainHandler.sendEmptyMessage(7);
        }
    }

    public void showLockScreen() {
        View dialogview = LayoutInflater.from(this).inflate((int) R.layout.dialog_lockscreen, (ViewGroup) null);
        RadioGroup selections = (RadioGroup) dialogview.findViewById(R.id.lockscreen_group);
        selections.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int unused = SurfManagerActivity.this.mLockScreenSelection = checkedId;
            }
        });
        selections.check(this.mLockScreenSelection);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.lockscreen_title);
        builder.setView(dialogview);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                switch (SurfManagerActivity.this.mLockScreenSelection) {
                    case R.id.lockscreen_none:
                        SurfManagerActivity.this.setRequestedOrientation(-1);
                        return;
                    case R.id.lockscreen_vertical:
                        SurfManagerActivity.this.setRequestedOrientation(1);
                        return;
                    case R.id.lockscreen_horizontal:
                        SurfManagerActivity.this.setRequestedOrientation(0);
                        return;
                    default:
                        return;
                }
            }
        });
        builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        builder.show();
    }

    public void onHomeClickBack() {
        if (this.mLastWindowItem != null) {
            this.mLastWindowItem.state = 4;
            showCurrentWindowItem();
        }
    }

    public void onHomeClickForward() {
        BrowserViewNew browserCtrl;
        if (this.mLastWindowItem != null) {
            if (this.mLastWindowItem.state == 3 && (browserCtrl = this.mLastWindowItem.browserView) != null) {
                browserCtrl.mWebViewMgr.goForward();
            }
            this.mLastWindowItem.state = 4;
            showCurrentWindowItem();
        }
    }

    public void showCurrentWindowItem() {
        if (this.mLastWindowItem != null) {
            switch (this.mLastWindowItem.state) {
                case 4:
                    BrowserViewNew browserCtrl = this.mLastWindowItem.browserView;
                    if (browserCtrl != null) {
                        browserCtrl.refreshButton(this.mListWebView.size());
                        browserCtrl.onResume();
                        this.mFrameLayout.removeAllViews();
                        this.mFrameLayout.addView(browserCtrl, COVER_SCREEN_PARAMS);
                        browserCtrl.requestFocus();
                        this.mCurControl = 2;
                        if (this.mHomeView != null) {
                            this.mHomeView.removeScrollMessage();
                            return;
                        }
                        return;
                    }
                    return;
                default:
                    if (this.mHomeView == null) {
                        this.mHomeView = new HomeView(this, this.mLastWindowItem.tab);
                    } else {
                        this.mHomeView.onResume(this.mLastWindowItem.tab);
                    }
                    int count = this.mListWebView.size();
                    switch (this.mLastWindowItem.state) {
                        case 1:
                            this.mHomeView.refreshButton(true, false, count);
                            break;
                        case 2:
                            this.mHomeView.refreshButton(false, true, count);
                            break;
                        case 3:
                            this.mHomeView.refreshButton(true, true, count);
                            break;
                        default:
                            this.mHomeView.refreshButton(false, false, count);
                            break;
                    }
                    this.mFrameLayout.removeAllViewsInLayout();
                    this.mFrameLayout.addView(this.mHomeView, COVER_SCREEN_PARAMS);
                    this.mHomeView.requestFocus();
                    this.mCurControl = 1;
                    return;
            }
        }
    }

    public void onBrowserClickHome(boolean canGoForward) {
        this.mLastWindowItem.state = canGoForward ? 3 : 1;
        showCurrentWindowItem();
    }

    public void onBrowserClickBack() {
        this.mLastWindowItem.state = 2;
        showCurrentWindowItem();
    }

    public WindowItemNew addBrowserItem(boolean isBackground) {
        WindowItemNew item = null;
        if (this.mListWebView.size() < MAX_WINDOWS_NUM) {
            item = new WindowItemNew();
            if (item != null) {
                this.mListWebView.add(item);
            }
            if (!isBackground) {
                this.mLastWindowItem = item;
            }
        } else {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle((int) R.string.information);
            dialog.setMessage(getResources().getString(R.string.too_many_windows) + MAX_WINDOWS_NUM);
            dialog.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    SurfManagerActivity.this.showWindowsManager();
                }
            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                }
            });
            dialog.show();
        }
        return item;
    }

    public void clearFormData() {
        if (this.mListWebView != null) {
            for (int i = 0; i < this.mListWebView.size(); i++) {
                BrowserViewNew browser = this.mListWebView.get(i).browserView;
                if (browser != null) {
                    browser.mWebViewMgr.clearFormData();
                }
            }
        }
    }

    public void clearCache(boolean isIncludeDiskFiles) {
        if (this.mListWebView != null) {
            for (int i = 0; i < this.mListWebView.size(); i++) {
                BrowserViewNew browser = this.mListWebView.get(i).browserView;
                if (browser != null) {
                    browser.mWebViewMgr.clearCache(Boolean.valueOf(isIncludeDiskFiles));
                }
            }
        }
    }

    public boolean browseInNewWindow(String url, int toolbarTabIdx) {
        if (addBrowserItem(false) != null) {
            return browseInCurrentWindow(url, toolbarTabIdx);
        }
        return false;
    }

    public boolean browseInBackground(String url) {
        WindowItemNew item = addBrowserItem(true);
        if (item == null) {
            return false;
        }
        item.title = url;
        item.url = url;
        item.browserView = new BrowserViewNew(this, item);
        item.state = 4;
        int count = this.mListWebView.size();
        if (this.mLastWindowItem == null || this.mLastWindowItem.browserView == null) {
            return false;
        }
        this.mLastWindowItem.browserView.refreshButton(count);
        return false;
    }

    public boolean browseInCurrentWindow(String url, int toolbarTabIdx) {
        return browseInCurrentWindow(url, toolbarTabIdx, true);
    }

    public boolean browseInCurrentWindow(String url, int toolbarTabIdx, boolean isFormatUrl) {
        String newUrl = isFormatUrl ? formatUrl(url) : URLUtil.guessUrl(url);
        BrowserViewNew browserView = this.mLastWindowItem.browserView;
        if (this.mLastWindowItem.state == 2) {
            this.mLastWindowItem.state = 0;
            if (browserView != null) {
                browserView.onClose();
                browserView = null;
            }
        }
        if (browserView != null) {
            browserView.loadUrl(newUrl, toolbarTabIdx, false);
            this.mLastWindowItem.state = 4;
            showCurrentWindowItem();
            return true;
        }
        if (toolbarTabIdx >= 0) {
            this.mLastWindowItem.tab = toolbarTabIdx;
        }
        if (url.equals(WindowAdapter2.BLANK_URL)) {
            this.mLastWindowItem.title = null;
            this.mLastWindowItem.url = null;
        } else {
            this.mLastWindowItem.title = newUrl;
            this.mLastWindowItem.url = newUrl;
        }
        this.mLastWindowItem.browserView = new BrowserViewNew(this, this.mLastWindowItem);
        this.mLastWindowItem.state = 4;
        showCurrentWindowItem();
        return true;
    }

    public void showWindowsManager() {
        if (this.mBrowserManager == null) {
            this.mBrowserManager = new WindowManagerView(this);
        } else {
            this.mBrowserManager.setFromWindowTag();
        }
        this.mFrameLayout.removeAllViews();
        this.mFrameLayout.addView(this.mBrowserManager, COVER_SCREEN_PARAMS);
        this.mBrowserManager.requestFocus();
        this.mCurControl = 3;
    }

    public void onCloseAll() {
        for (int i = 0; i < this.mListWebView.size(); i++) {
            BrowserViewNew browser = this.mListWebView.get(i).browserView;
            if (browser != null) {
                browser.onClose();
            }
        }
        this.mListWebView.clear();
        this.mLastWindowItem = new WindowItemNew();
        this.mListWebView.add(this.mLastWindowItem);
    }

    public void onCloseWebView(WindowItemNew item) {
        if (item != null) {
            this.mListWebView.remove(item);
            BrowserViewNew browser = item.browserView;
            if (browser != null) {
                browser.onClose();
            }
            if (this.mLastWindowItem != item) {
                return;
            }
            if (this.mListWebView.size() > 0) {
                this.mLastWindowItem = this.mListWebView.get(this.mListWebView.size() - 1);
            } else {
                this.mLastWindowItem = null;
            }
        }
    }

    public void onServiceComplete(int reqType, Msb msb) {
        switch (reqType) {
            case 2:
                if (msb != null) {
                    Log.e(LOG_TAG, "onServiceComplete() - msb: " + msb.toString());
                    mMsb = msb;
                    if (this.mMainHandler != null) {
                        this.mMainHandler.sendEmptyMessage(3);
                        if (msb.upg != null) {
                            Log.e(LOG_TAG, "\tupgrade:" + msb.upg);
                            this.mMainHandler.sendEmptyMessage(1);
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            case 3:
                if (!(msb == null || msb.upg == null)) {
                    if (mMsb == null) {
                        mMsb = new Msb();
                    }
                    mMsb.upg = msb.upg;
                    Log.e(LOG_TAG, "\tupgrade:" + msb.upg);
                }
                if (this.mMainHandler != null) {
                    this.mMainHandler.sendEmptyMessage(5);
                    return;
                }
                return;
            case 4:
            case 5:
            default:
                return;
            case 6:
                if (!(msb == null || msb.share == null)) {
                    if (mMsb == null) {
                        mMsb = new Msb();
                    }
                    mMsb.share = msb.share;
                    Log.e(LOG_TAG, "\tupgrade:" + msb.share);
                }
                if (this.mMainHandler != null) {
                    this.mMainHandler.sendEmptyMessage(6);
                    return;
                }
                return;
        }
    }

    /* access modifiers changed from: private */
    public boolean onReloadData() {
        if (this.mHomeView == null) {
            return true;
        }
        this.mHomeView.onReloadData();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean onUpgrade() {
        Upgrade upg = mMsb == null ? null : mMsb.upg;
        if (upg == null) {
            return false;
        }
        if (SurfBrowser.CURRENT_VERSION.equals(upg.sver) || upg.url == null || WindowAdapter2.BLANK_URL.equals(upg.url)) {
            return false;
        }
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle((int) R.string.upgrade);
        if (upg.txt == null || WindowAdapter2.BLANK_URL.equals(upg.txt)) {
            dialog.setMessage(getResources().getString(R.string.upgrade_newversion_found) + "\n" + getResources().getString(R.string.upgrade_confirm));
        } else {
            dialog.setMessage(upg.txt + "\n" + getResources().getString(R.string.upgrade_confirm));
        }
        dialog.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                int unused = SurfManagerActivity.this.mUpgradeProgressValue = -1;
                ProgressDialog unused2 = SurfManagerActivity.this.mUpgradeProgress = new ProgressDialog(SurfManagerActivity.this);
                SurfManagerActivity.this.mUpgradeProgress.setIndeterminate(true);
                SurfManagerActivity.this.mUpgradeProgress.setMax(100);
                SurfManagerActivity.this.mUpgradeProgress.setProgressStyle(1);
                SurfManagerActivity.this.mUpgradeProgress.setCancelable(false);
                SurfManagerActivity.this.mUpgradeProgress.setTitle((int) R.string.upgrade);
                SurfManagerActivity.this.mUpgradeProgress.setMessage(SurfManagerActivity.this.getResources().getString(R.string.upgrade_downloading));
                SurfManagerActivity.this.mUpgradeProgress.show();
                new Thread() {
                    public void run() {
                        Upgrade upg = SurfManagerActivity.mMsb == null ? null : SurfManagerActivity.mMsb.upg;
                        if (upg != null) {
                            try {
                                Log.e(WindowAdapter2.BLANK_URL, "upg.url=" + upg.url);
                                if (new ServiceRequest(SurfManagerActivity.this).downloadFile(upg.url, "upgrade.apk", 1, 0, SurfManagerActivity.this)) {
                                    Intent intent = new Intent("android.intent.action.VIEW");
                                    intent.setDataAndType(Uri.fromFile(SurfManagerActivity.this.getFileStreamPath("upgrade.apk")), "application/vnd.android.package-archive");
                                    SurfManagerActivity.this.startActivity(intent);
                                }
                            } catch (Exception e) {
                                Log.e(SurfManagerActivity.LOG_TAG, "download file fail", e);
                                if (SurfManagerActivity.this.mMainHandler != null) {
                                    SurfManagerActivity.this.mMainHandler.sendEmptyMessage(2);
                                }
                            }
                            SurfManagerActivity.this.mUpgradeProgress.cancel();
                        }
                    }
                }.start();
            }
        });
        if (upg.force != 1) {
            dialog.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                }
            });
        }
        dialog.show();
        return true;
    }

    public void sharePage(String params) {
        this.mSharePageProgress = new ProgressDialog(this);
        this.mSharePageProgress.setIndeterminate(true);
        this.mSharePageProgress.setCancelable(false);
        this.mSharePageProgress.setTitle((int) R.string.ishare);
        this.mSharePageProgress.setMessage(getResources().getString(R.string.ishare_sending));
        this.mSharePageProgress.setButton(getResources().getString(R.string.common_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SurfManagerActivity.this.mSharePageProgress.cancel();
                ProgressDialog unused = SurfManagerActivity.this.mSharePageProgress = null;
            }
        });
        this.mSharePageProgress.show();
        new ServiceRequest(this).requestMsb(6, this, params);
    }

    public boolean checkServer() {
        StringBuilder requestParams = new StringBuilder();
        requestParams.append(SurfBrowser.isOPhone() ? "ua=MOBILESURFING_OPHONE20" : "ua=MOBILESURFING_ANDROID16");
        requestParams.append("&uid=" + mapNum(this.mDeviceID));
        requestParams.append("&channelid=" + this.mChannelID);
        mMsb = new ServiceRequest(this).requestMsb(2, this, requestParams.toString());
        return true;
    }

    public boolean checkUpgrade() {
        this.mCheckUpgradeProgress = new ProgressDialog(this);
        this.mCheckUpgradeProgress.setIndeterminate(true);
        this.mCheckUpgradeProgress.setCancelable(false);
        this.mCheckUpgradeProgress.setTitle("软件升级");
        this.mCheckUpgradeProgress.setMessage("正在检测软件最新版本，请稍候");
        this.mCheckUpgradeProgress.setButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                SurfManagerActivity.this.mCheckUpgradeProgress.cancel();
                ProgressDialog unused = SurfManagerActivity.this.mCheckUpgradeProgress = null;
            }
        });
        this.mCheckUpgradeProgress.show();
        new ServiceRequest(this).requestMsb(3, this, SurfBrowser.isOPhone() ? "ua=MOBILESURFING_OPHONE20" : "ua=MOBILESURFING_ANDROID16");
        return true;
    }

    /* access modifiers changed from: private */
    public boolean onShareResult() {
        Share share;
        if (this.mSharePageProgress != null) {
            this.mSharePageProgress.cancel();
        }
        if (mMsb == null) {
            share = null;
        } else {
            share = mMsb.share;
        }
        Log.e(WindowAdapter2.BLANK_URL, "onShareResult:" + share);
        View dialogview = LayoutInflater.from(this).inflate((int) R.layout.share_result, (ViewGroup) null);
        ((TextView) dialogview.findViewById(R.id.share_result)).setText(share == null ? getResources().getString(R.string.ishare_failed) : share.mMessage);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.ishare);
        builder.setView(dialogview);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean onCheckUpgrade() {
        if (this.mCheckUpgradeProgress != null) {
            this.mCheckUpgradeProgress.cancel();
        }
        Upgrade upg = mMsb == null ? null : mMsb.upg;
        Log.e(WindowAdapter2.BLANK_URL, "onCheckUpgrade:" + upg);
        if (upg == null || SurfBrowser.CURRENT_VERSION.equals(upg.sver) || upg.url == null || WindowAdapter2.BLANK_URL.equals(upg.url)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setTitle("软件升级");
            dialog.setMessage(upg == null ? "升级检测发生异常，请稍候重试" : "软件已经是最新版本了");
            dialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            dialog.show();
            return false;
        }
        onUpgrade();
        return true;
    }

    public static boolean isWapSite(String url) {
        if (url.toLowerCase().contains("ismowsite")) {
            return true;
        }
        HashSet<String> wap = wapDomain;
        if (wap != null) {
            try {
                String host = new URL(URLUtil.guessUrl(url)).getHost().toLowerCase();
                int pos = host.indexOf(".");
                if (pos > 0) {
                    host = host.substring(0, pos);
                }
                if (wap.contains(host)) {
                    return true;
                }
            } catch (Exception e) {
                Log.e(WindowAdapter2.BLANK_URL, WindowAdapter2.BLANK_URL, e);
            }
        }
        return false;
    }

    public static String formatUrl(String url) {
        if (TextUtils.isEmpty(url)) {
            return url;
        }
        if (url.startsWith("file:///")) {
            return url;
        }
        String url2 = URLUtil.guessUrl(url);
        Log.e(LOG_TAG, "formatUrl: " + url2);
        String portal = null;
        if (mNetworkMgr.getNetworkType().equalsIgnoreCase("CMWAP") || mNetworkMgr.getNetworkType().equalsIgnoreCase("CMNET") || mNetworkMgr.getNetworkType().equalsIgnoreCase("WIFI")) {
            if (mMsb != null && !TextUtils.isEmpty(mMsb.domain)) {
                portal = mMsb.domain;
            }
        } else if (mMsb != null && !TextUtils.isEmpty(mMsb.domain2)) {
            portal = mMsb.domain2;
        }
        if (portal == null) {
            return url2;
        }
        if (url2.toLowerCase().contains("ismowsite")) {
            return url2;
        }
        HashSet<String> white = whiteDomain;
        HashSet<String> wap = wapDomain;
        if (white != null) {
            try {
                URL u = new URL(url2);
                Iterator<String> items = white.iterator();
                while (true) {
                    if (!items.hasNext()) {
                        break;
                    }
                    if (u.getHost().toLowerCase().contains(items.next())) {
                        portal = null;
                        break;
                    }
                }
            } catch (Exception e) {
            }
        }
        if (portal == null) {
            return url2;
        }
        if (wap != null) {
            try {
                String host = new URL(url2).getHost().toLowerCase();
                int pos = host.indexOf(".");
                if (pos > 0) {
                    host = host.substring(0, pos);
                }
                if (wap.contains(host)) {
                    portal = null;
                }
            } catch (Exception e2) {
            }
        }
        if (portal == null) {
            return url2;
        }
        if (mNetworkMgr.getNetworkType().equalsIgnoreCase("WIFI")) {
            if (!SurfBrowserSettings.getInstance().isWWW2WAPInWifi()) {
                return url2;
            }
        } else if (!SurfBrowserSettings.getInstance().isWWW2WAPInMobile()) {
            return url2;
        }
        return portal + url2.substring("http://".length());
    }

    public void settings() {
        startActivityForResult(new Intent(this, SurfPreferenceActivity.class), 6);
    }

    public void download() {
        Intent intent = new Intent(this, DownloadHomeTabActivity.class);
        intent.setFlags(268435456);
        startActivity(intent);
    }

    public void showQuitDialog() {
        final View dialogView = LayoutInflater.from(this).inflate((int) R.layout.dialog_quit, (ViewGroup) null);
        CheckBox chkClearHistory = (CheckBox) dialogView.findViewById(R.id.checkbox_clearhistory);
        if (chkClearHistory != null) {
            chkClearHistory.setChecked(false);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.quit_dialog_title);
        builder.setView(dialogView);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (((CheckBox) dialogView.findViewById(R.id.checkbox_clearhistory)).isChecked()) {
                    SurfManagerActivity.this.clearHistory();
                }
                SurfManagerActivity.this.quit();
            }
        });
        builder.setNegativeButton((int) R.string.common_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
            }
        });
        builder.show();
    }

    public void clearHistory() {
        HistoryDB.getInstance(this).clear();
        SearchTipDB.getInstance(this).clear();
        if (this.mListWebView != null) {
            for (int i = 0; i < this.mListWebView.size(); i++) {
                BrowserViewNew browser = this.mListWebView.get(i).browserView;
                if (browser != null) {
                    browser.mWebViewMgr.clearHistory();
                }
            }
        }
    }

    public void quit() {
        onCloseAll();
        finish();
    }

    public void setCurrentSnapshot(View view) {
        if (this.mLastWindowItem != null && view != null) {
            boolean enable = view.isDrawingCacheEnabled();
            if (!enable) {
                view.setDrawingCacheEnabled(true);
            }
            Bitmap cacheBmp = view.getDrawingCache();
            if (cacheBmp != null) {
                float width = (float) cacheBmp.getWidth();
                float height = (float) cacheBmp.getHeight();
                if (width > 0.0f && height > 0.0f) {
                    float dstWidth = getResources().getDisplayMetrics().density * 60.0f;
                    float dstHeight = dstWidth;
                    if (width > height) {
                        dstHeight = (height * dstWidth) / width;
                    } else {
                        dstWidth = (width * dstHeight) / height;
                    }
                    this.mLastWindowItem.snapshot = Bitmap.createScaledBitmap(cacheBmp, (int) dstWidth, (int) dstHeight, true);
                }
            }
            if (!enable) {
                view.setDrawingCacheEnabled(false);
            }
        }
    }

    public void onHomeTabChanged(int tabId) {
        if (this.mLastWindowItem != null && tabId >= 0) {
            this.mLastWindowItem.tab = tabId;
        }
    }

    private String smartUrlFilter(Uri inUri) {
        if (inUri != null) {
            return smartUrlFilter(inUri.toString());
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String smartUrlFilter(String url) {
        boolean hasSpace;
        String inUrl = url.trim();
        if (inUrl.indexOf(32) != -1) {
            hasSpace = true;
        } else {
            hasSpace = false;
        }
        Matcher matcher = ACCEPTED_URI_SCHEMA.matcher(inUrl);
        if (!matcher.matches()) {
            return URLUtil.guessUrl(inUrl);
        }
        String scheme = matcher.group(1);
        String lcScheme = scheme.toLowerCase();
        if (!lcScheme.equals(scheme)) {
            inUrl = lcScheme + matcher.group(2);
        }
        if (hasSpace) {
            inUrl = inUrl.replace(" ", "%20");
        }
        return inUrl;
    }

    private String getUrlFromIntent(Intent intent) {
        String mimeType;
        if (intent == null || !"android.intent.action.VIEW".equals(intent.getAction())) {
            return WindowAdapter2.BLANK_URL;
        }
        String url = smartUrlFilter(intent.getData());
        return (url == null || !url.startsWith("content:") || (mimeType = intent.resolveType(getContentResolver())) == null) ? url : url + "?" + mimeType;
    }

    /* access modifiers changed from: private */
    public void showUpdateLog() {
        if (!SurfBrowser.CURRENT_VERSION.equals(PreferenceUtil.getValue(this, "cmsurf", "cmsurf_version", (String) null)) && !showUpdateLog) {
            showUpdateLog = true;
            PreferenceUtil.saveValue(this, "cmsurf", "cmsurf_version", SurfBrowser.CURRENT_VERSION);
            try {
                InputStream in = getAssets().open("updatelog.txt");
                ByteArrayOutputStream buffer = new ByteArrayOutputStream();
                byte[] temp = new byte[1024];
                while (true) {
                    int read = in.read(temp, 0, temp.length);
                    if (read <= 0) {
                        break;
                    }
                    buffer.write(temp, 0, read);
                }
                in.close();
                String data = new String(buffer.toByteArray());
                Log.e("location", data);
                if (data != null) {
                    String data2 = data.trim();
                    if (WindowAdapter2.BLANK_URL.equals(data2)) {
                        data = null;
                    } else {
                        data = data2.replace("\r\n", "\n");
                    }
                }
                if (data != null) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                    dialog.setTitle((int) R.string.upgrade);
                    dialog.setMessage(data);
                    dialog.setNegativeButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                        }
                    });
                    dialog.show();
                }
            } catch (Exception e) {
            }
        }
    }

    public static void setBrightness(Activity activity, float bright) {
        if (activity != null) {
            if (bright < 0.02f) {
                bright = 0.02f;
            } else if (((double) bright) > 1.0d) {
                bright = 1.0f;
            }
            WindowManager.LayoutParams layoutparams = activity.getWindow().getAttributes();
            layoutparams.screenBrightness = bright;
            activity.getWindow().setAttributes(layoutparams);
        }
    }

    public void showBrightnessAdjust() {
        Log.e("bright", "bright" + getWindow().getAttributes().screenBrightness);
        View dialogview = LayoutInflater.from(this).inflate((int) R.layout.brightness_adjust, (ViewGroup) null);
        SeekBar seekBar = (SeekBar) dialogview.findViewById(R.id.brightness_seek);
        if (seekBar != null) {
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
                    float unused = SurfManagerActivity.this.brightness = ((float) progress) / 100.0f;
                    if (SurfManagerActivity.this.brightness < 0.02f) {
                        float unused2 = SurfManagerActivity.this.brightness = 0.02f;
                    } else if (SurfManagerActivity.this.brightness > 1.0f) {
                        float unused3 = SurfManagerActivity.this.brightness = 1.0f;
                    }
                    SurfManagerActivity.setBrightness(SurfManagerActivity.this, SurfManagerActivity.this.brightness);
                }

                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
            if (this.brightness >= 0.0f) {
                if (this.brightness < 0.02f) {
                    this.brightness = 0.02f;
                } else if (this.brightness > 1.0f) {
                    this.brightness = 1.0f;
                }
                seekBar.setProgress((int) (this.brightness * 100.0f));
            } else {
                seekBar.setProgress(50);
            }
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.brightness_adjust);
        builder.setView(dialogview);
        builder.setPositiveButton((int) R.string.common_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.show();
    }

    public static boolean isCMWap() {
        if (mNetworkMgr != null) {
            try {
                return "cmwap".equalsIgnoreCase(mNetworkMgr.getNetworkType());
            } catch (Exception e) {
            }
        }
        return false;
    }

    public void closeFind() {
    }

    public void onDownload(long totalSize, long downloadSize) {
        int tmp;
        if (downloadSize >= 0 && totalSize > 0 && this.mUpgradeProgressValue != (tmp = (int) ((100 * downloadSize) / totalSize))) {
            if (this.mUpgradeProgressValue < 0) {
                this.mUpgradeProgress.setIndeterminate(false);
                this.mUpgradeProgress.setMax(100);
            }
            this.mUpgradeProgressValue = tmp;
            this.mUpgradeProgress.setProgress(this.mUpgradeProgressValue);
        }
    }
}
