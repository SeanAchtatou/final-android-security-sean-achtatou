package com.cplatform.android.cmsurfclient.ad;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import com.cplatform.android.cmsurfclient.R;
import java.util.ArrayList;

public class AdAdapter extends ArrayAdapter<AdItem> {
    private Activity mActivity = null;

    public AdAdapter(Activity activity, ArrayList<AdItem> items) {
        super(activity, (int) R.layout.adlink_list_item, items);
        this.mActivity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate((int) R.layout.adlink_list_item, (ViewGroup) null);
        } else {
            row = convertView;
        }
        Button buttonItem = (Button) row.findViewById(R.id.adlink_item);
        buttonItem.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
        buttonItem.setText(((AdItem) getItem(position)).title);
        buttonItem.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                Button adLinkItem = (Button) v;
                if (event.getAction() == 0) {
                    adLinkItem.setTextColor(-1);
                    return false;
                } else if (event.getAction() == 1) {
                    adLinkItem.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                    return false;
                } else {
                    adLinkItem.setTextColor((int) AdItem.TEXTCOLOR_PRESSED);
                    return false;
                }
            }
        });
        row.setClickable(false);
        return row;
    }
}
