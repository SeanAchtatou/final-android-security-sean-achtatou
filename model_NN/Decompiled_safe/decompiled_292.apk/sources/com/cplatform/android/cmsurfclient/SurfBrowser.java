package com.cplatform.android.cmsurfclient;

import android.app.Application;
import android.net.NetworkInfo;
import android.util.Log;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import dalvik.system.VMRuntime;
import java.lang.reflect.Method;

public class SurfBrowser extends Application {
    public static final String CURRENT_VERSION = "1.1.5";
    private static final String LOG_TAG = SurfBrowser.class.getSimpleName();
    public static final String OMS_MIMETYPE = "application/vnd.oma.dd+xml";
    public static final int REQUEST_CODE_BOOKMARKHISTORY = 5;
    public static final int REQUEST_CODE_BROWSERMENU_OPTIONS = 4;
    public static final int REQUEST_CODE_HOME = 7;
    public static final int REQUEST_CODE_HOMEMENU_OPTIONS = 3;
    public static final int REQUEST_CODE_NAVIEDIT_SEARCH = 1;
    public static final int REQUEST_CODE_NAVIEDIT_URL = 2;
    public static final int REQUEST_CODE_SETTINGS = 6;
    public static final int REQUEST_CODE_SHARE_IMAGE = 9;
    public static final int REQUEST_CODE_SHARE_PAGE = 8;
    private static final float TARGET_HEAP_UTILIZATION = 0.75f;
    public static final String VERSION_NAME = "1.4 Beta";
    private static SurfBrowser mInstance = null;
    private static int mIsOPhoneChecked = 0;

    public SurfBrowser() {
        mInstance = this;
    }

    public static SurfBrowser getInstance() {
        return mInstance;
    }

    public void onCreate() {
        Log.d(LOG_TAG, "onCreate");
        VMRuntime.getRuntime().setTargetHeapUtilization((float) TARGET_HEAP_UTILIZATION);
        CookieSyncManager.createInstance(this);
        CookieManager.getInstance().removeExpiredCookie();
    }

    public static boolean useWML2HTML() {
        return !isOPhone();
    }

    public static boolean isOPhone() {
        switch (mIsOPhoneChecked) {
            case 0:
                mIsOPhoneChecked = 2;
                try {
                    Method method1 = NetworkInfo.class.getMethod("getApType", new Class[0]);
                    Method method2 = NetworkInfo.class.getMethod("getInterfaceName", new Class[0]);
                    if (!(method1 == null || method2 == null)) {
                        mIsOPhoneChecked = 1;
                        return true;
                    }
                } catch (Exception e) {
                }
                return false;
            case 1:
                return true;
            case 2:
                return false;
            default:
                return false;
        }
    }
}
