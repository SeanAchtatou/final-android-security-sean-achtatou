package com.cplatform.android.cmsurfclient.service.entry;

import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import org.w3c.dom.Element;

public class Weather {
    public String city = null;
    public String date = null;
    public String txt = null;
    public int type = 0;
    public String url = null;
    public String wind = null;

    public Weather() {
    }

    public Weather(Element entry) {
        if (entry != null) {
            this.city = entry.getAttribute("city");
            try {
                this.type = Integer.parseInt(entry.getAttribute(QueryApList.Carriers.TYPE));
            } catch (Exception e) {
                this.type = 0;
            }
            this.txt = entry.getAttribute("txt");
            this.url = entry.getAttribute("url");
            this.date = entry.getAttribute("date");
            this.wind = entry.getAttribute("wind");
        }
    }

    public String toString() {
        return "city:" + this.city + "\ttype:" + this.type + "\ttxt:" + this.txt + "\twind:" + this.wind + "\tdate:" + this.date + "\turl:" + this.url;
    }
}
