package com.cplatform.android.cmsurfclient.share;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import java.util.ArrayList;

public class ShareHistoryDB {
    private static final String DATABASE_NAME = "CMSurfClient";
    private static final String TABLE_SHAREBY_NAME = "shareby";
    private static final String TABLE_SHARETO_NAME = "shareto";
    private final String DEBUG_TAG = "ShareHistoryDB";
    private Context mContext;
    private SQLiteDatabase mDB = null;
    public ArrayList<String> mShareBy;
    public ArrayList<String> mShareTo;

    public ShareHistoryDB(Context context) {
        this.mContext = context;
        this.mShareBy = new ArrayList<>();
        this.mShareTo = new ArrayList<>();
    }

    public boolean openDB() {
        if (this.mDB == null || !this.mDB.isOpen()) {
            this.mDB = this.mContext.openOrCreateDatabase(DATABASE_NAME, 0, null);
            prepareTable();
        }
        return this.mDB != null && this.mDB.isOpen();
    }

    public void closeDB() {
        if (this.mDB != null) {
            if (this.mDB.isOpen()) {
                this.mDB.close();
            }
            this.mDB = null;
        }
    }

    private void prepareTable() {
        StringBuffer sql = new StringBuffer(100);
        sql.append("create table ").append(TABLE_SHAREBY_NAME).append("(_id integer primary key autoincrement, username text);");
        try {
            this.mDB.execSQL(sql.toString());
        } catch (Exception e) {
        }
        sql.delete(0, sql.length());
        sql.append("create table ").append(TABLE_SHARETO_NAME).append("(_id integer primary key autoincrement, phonenumber text, username text);");
        try {
            this.mDB.execSQL(sql.toString());
        } catch (Exception e2) {
        }
    }

    public void load() {
        this.mShareBy.clear();
        this.mShareTo.clear();
        if (openDB()) {
            Log.i("ShareHistoryDB", "openDB");
            Cursor cur = this.mDB.query(TABLE_SHAREBY_NAME, new String[]{QueryApList.Carriers._ID, "username"}, null, null, null, null, "_id DESC");
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                String username = cur.getString(1);
                this.mShareBy.add(username);
                Log.i("ShareHistoryDB", "ShareBy: username=" + username);
                cur.moveToNext();
            }
            Cursor cur2 = this.mDB.query(TABLE_SHARETO_NAME, new String[]{QueryApList.Carriers._ID, "phonenumber", "username"}, null, null, null, null, "_id DESC");
            cur2.moveToFirst();
            while (!cur2.isAfterLast()) {
                String phonenumber = cur2.getString(1);
                String username2 = cur2.getString(2);
                StringBuilder shareto = new StringBuilder(80);
                shareto.append(phonenumber);
                if (username2 != null && username2.length() > 0) {
                    shareto.append("(").append(username2).append(")");
                }
                this.mShareTo.add(shareto.toString());
                Log.i("ShareHistoryDB", "ShareTo: username=" + username2 + ", phonenumber=" + phonenumber);
                cur2.moveToNext();
            }
            cur2.close();
            closeDB();
        }
    }

    public boolean addShareBy(String username) {
        Log.i("ShareHistoryDB", "addShareBy:" + username);
        delShareBy(username);
        boolean ret = false;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put("username", username);
                this.mDB.insert(TABLE_SHAREBY_NAME, null, values);
                Log.i("ShareHistoryDB", "insert " + username);
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean addShareTo(String phonenumber, String username) {
        Log.i("ShareHistoryDB", "addShareTo:" + phonenumber);
        delShareTo(phonenumber);
        boolean ret = false;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put("phonenumber", phonenumber);
                values.put("username", username);
                this.mDB.insert(TABLE_SHARETO_NAME, null, values);
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean delShareBy(String username) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuilder sql = new StringBuilder(100);
                sql.append("delete from ").append(TABLE_SHAREBY_NAME);
                sql.append(" where username=").append("'").append(username).append("'").append(";");
                this.mDB.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean delShareTo(String phonenumber) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuilder sql = new StringBuilder(100);
                sql.append("delete from ").append(TABLE_SHARETO_NAME);
                sql.append(" where phonenumber=").append("'").append(phonenumber).append("'").append(";");
                this.mDB.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }
}
