package com.cplatform.android.cmsurfclient.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.WebView;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.utils.ReflectUtil;

public class NetworkManager {
    private static final String LOG_TAG = "NetworkManager";
    /* access modifiers changed from: private */
    public ConnectHelper mConnectHelper = null;
    private boolean mIsOPhone = false;
    private String mNetworkType = "UNKNOWN";
    public SurfManagerActivity mSurfMgr = null;

    public NetworkManager(SurfManagerActivity context, boolean isOPhone) {
        this.mSurfMgr = context;
        this.mIsOPhone = isOPhone;
        this.mConnectHelper = new ConnectHelper(this, this.mIsOPhone);
    }

    private boolean isDataConnectedOrConnecting() {
        TelephonyManager tm = (TelephonyManager) this.mSurfMgr.getSystemService("phone");
        if (tm == null) {
            return false;
        }
        int dataState = tm.getDataState();
        Log.e(LOG_TAG, "Current data state: " + dataState);
        switch (dataState) {
            case 0:
                return false;
            case 1:
                return true;
            case 2:
                return true;
            case 3:
                return false;
            default:
                return false;
        }
    }

    private void switchDataConnection(boolean isSwitchOn) {
        TelephonyManager tm = (TelephonyManager) this.mSurfMgr.getSystemService("phone");
        if (tm != null) {
            Class<?> ITelephonyClass = null;
            Object ITelephonyStub = ReflectUtil.invokeDeclaredMethodNoParams(TelephonyManager.class, tm, "getITelephony");
            try {
                ITelephonyClass = Class.forName(ITelephonyStub.getClass().getName());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            ReflectUtil.invokeDeclaredMethodNoParams(ITelephonyClass, ITelephonyStub, isSwitchOn ? "enableDataConnectivity" : "disableDataConnectivity");
        }
    }

    public boolean connect() {
        Log.e(LOG_TAG, "connect()...");
        logNetworkInfo();
        Log.e(LOG_TAG, "isDataConnectedOrConnecting: " + isDataConnectedOrConnecting());
        Log.e(LOG_TAG, "Build.MODEL: " + Build.MODEL);
        if (this.mIsOPhone || isNetworkConnected()) {
            if (!isWifiConnected()) {
                new Thread(new Runnable() {
                    public void run() {
                        NetworkManager.this.mConnectHelper.connect(NetworkManager.this.mSurfMgr.mSettings.isCMWAPPreferred());
                    }
                }).start();
            } else if (!this.mIsOPhone) {
                onNetworkStateChanged(true, "WIFI");
            } else {
                new Thread(new Runnable() {
                    public void run() {
                        NetworkManager.this.mConnectHelper.connect(false);
                    }
                }).start();
            }
        } else if (!isDataConnectedOrConnecting() && Build.MODEL.equalsIgnoreCase(SurfManagerActivity.MODEL_DOPOD_A8188)) {
            switchDataConnection(true);
        } else if (this.mSurfMgr != null) {
            this.mSurfMgr.onNetworkToggle(false);
        }
        return true;
    }

    public void disconnect() {
        this.mConnectHelper.disconnect();
    }

    public void onResume() {
        if (!this.mIsOPhone) {
            WebView.enablePlatformNotifications();
        }
    }

    public void onPause() {
        if (!this.mIsOPhone) {
            WebView.disablePlatformNotifications();
        }
    }

    public void onNetworkActive() {
        if (this.mSurfMgr.getNetworkStatus() == 3) {
            Log.e(LOG_TAG, "Network active, try reconnect network...");
            connect();
        }
    }

    public void onNetworkStateChanged(boolean isConnected, String networkType) {
        Log.e(LOG_TAG, "onNetworkStateChanged: connected - " + isConnected + ", type - " + networkType);
        this.mNetworkType = networkType;
        if (!isConnected) {
            if (this.mIsOPhone && this.mNetworkType.equalsIgnoreCase("WIFI")) {
                this.mConnectHelper.connect(this.mSurfMgr.mSettings.isCMWAPPreferred());
            }
            Log.e(LOG_TAG, "Wifi network connected: " + isWifiConnected());
            Log.e(LOG_TAG, "Mobile network connected: " + isMobileConnected());
        }
        if (!this.mIsOPhone) {
            WebView.disablePlatformNotifications();
            WebView.enablePlatformNotifications();
        }
        if (this.mSurfMgr != null) {
            this.mSurfMgr.onNetworkToggle(isConnected);
        }
    }

    public String getNetworkType() {
        return this.mNetworkType;
    }

    private boolean isWifiConnected() {
        return isNetworkConnected(this.mSurfMgr, 1);
    }

    private boolean isMobileConnected() {
        return isNetworkConnected(this.mSurfMgr, 0);
    }

    private boolean isNetworkConnected(Context context, int networkType) {
        NetworkInfo info;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService("connectivity");
        if (cm == null || (info = cm.getNetworkInfo(networkType)) == null) {
            return false;
        }
        return info.isConnectedOrConnecting();
    }

    private void logNetworkInfo() {
        ConnectivityManager cm = (ConnectivityManager) this.mSurfMgr.getSystemService("connectivity");
        for (NetworkInfo networkInfo : cm.getAllNetworkInfo()) {
            if (networkInfo != null) {
                Log.w(LOG_TAG, "NetworkInfo: " + networkInfo.toString());
            }
        }
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info != null) {
            Log.w(LOG_TAG, "ActiveNetworkInfo: " + info.toString());
        } else {
            Log.w(LOG_TAG, "Failed to get active network info");
        }
    }

    public boolean isNetworkConnected() {
        return isWifiConnected() || isMobileConnected();
    }
}
