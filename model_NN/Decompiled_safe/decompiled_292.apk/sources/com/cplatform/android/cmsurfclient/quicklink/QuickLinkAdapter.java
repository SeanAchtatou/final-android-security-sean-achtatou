package com.cplatform.android.cmsurfclient.quicklink;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.HomeView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;

public class QuickLinkAdapter extends ArrayAdapter<QuickLinkItem> {
    private static final int MAX_TITLE_LENGTH = 4;
    private Activity mActivity;

    public QuickLinkAdapter(Activity activity, ArrayList<QuickLinkItem> items) {
        super(activity, (int) R.layout.quicklink_item, items);
        this.mActivity = activity;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView == null) {
            row = this.mActivity.getLayoutInflater().inflate((int) R.layout.quicklink_item, (ViewGroup) null);
        } else {
            row = convertView;
        }
        QuickLinkItem item = (QuickLinkItem) getItem(position);
        RelativeLayout background = (RelativeLayout) row.findViewById(R.id.quicklink_bg);
        TextView title = (TextView) row.findViewById(R.id.quicklink_text);
        ImageView image = (ImageView) row.findViewById(R.id.quicklink_image);
        ProgressBar progress = (ProgressBar) row.findViewById(R.id.quicklink_progress);
        if (item.mFlag > 0) {
            title.setVisibility(0);
            title.setText(item.mTitle);
            title.setEllipsize(TextUtils.TruncateAt.END);
            if (item.mFlag == 1) {
                progress.setVisibility(4);
                Bitmap bmpImage = HomeView.getImageFromDataFile(this.mActivity, item.mImage);
                if (bmpImage == null) {
                    bmpImage = HomeView.getImageFromAssetFile(this.mActivity, item.mImage);
                }
                if (bmpImage != null) {
                    image.setImageBitmap(bmpImage);
                }
            } else if (item.mFlag == 2) {
                image.setImageBitmap(null);
                progress.setVisibility(0);
            }
            background.setBackgroundResource(R.drawable.quicklink_column_bg);
        } else {
            progress.setVisibility(4);
            title.setVisibility(4);
            title.setText(WindowAdapter2.BLANK_URL);
            image.setImageBitmap(null);
            background.setBackgroundResource(R.drawable.quicklink_column_none);
        }
        return row;
    }
}
