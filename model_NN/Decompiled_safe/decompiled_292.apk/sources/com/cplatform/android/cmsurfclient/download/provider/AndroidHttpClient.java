package com.cplatform.android.cmsurfclient.download.provider;

import android.util.Log;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpContext;

public final class AndroidHttpClient implements HttpClient {
    private static final String TAG = "AndroidHttpClient";
    /* access modifiers changed from: private */
    public static final ThreadLocal<Boolean> sThreadBlocked = new ThreadLocal<>();
    /* access modifiers changed from: private */
    public static final HttpRequestInterceptor sThreadCheckInterceptor = new HttpRequestInterceptor() {
        public void process(HttpRequest httprequest, HttpContext httpcontext) {
            if (AndroidHttpClient.sThreadBlocked.get() != null && ((Boolean) AndroidHttpClient.sThreadBlocked.get()).booleanValue()) {
                throw new RuntimeException("This thread forbids HTTP requests");
            }
        }
    };
    /* access modifiers changed from: private */
    public volatile LoggingConfiguration curlConfiguration;
    private final HttpClient delegate;
    private RuntimeException mLeakedException = new IllegalStateException("AndroidHttpClient created and never closed");

    public static AndroidHttpClient newInstance(String userAgent, boolean useProxy) {
        HttpParams params = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(params, false);
        HttpConnectionParams.setConnectionTimeout(params, 20000);
        HttpConnectionParams.setSoTimeout(params, 20000);
        HttpConnectionParams.setSocketBufferSize(params, 8192);
        HttpClientParams.setRedirecting(params, false);
        if (userAgent != null) {
            HttpProtocolParams.setUserAgent(params, userAgent);
        }
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        AndroidHttpClient httpClient = new AndroidHttpClient(new ThreadSafeClientConnManager(params, schemeRegistry), params);
        if (useProxy) {
            httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
        }
        return httpClient;
    }

    private AndroidHttpClient(ClientConnectionManager clientconnectionmanager, HttpParams httpparams) {
        this.delegate = new DefaultHttpClient(clientconnectionmanager, httpparams) {
            /* access modifiers changed from: protected */
            public HttpContext createHttpContext() {
                HttpContext context = new BasicHttpContext();
                context.setAttribute("http.authscheme-registry", getAuthSchemes());
                context.setAttribute("http.cookiespec-registry", getCookieSpecs());
                context.setAttribute("http.auth.credentials-provider", getCredentialsProvider());
                return context;
            }

            /* access modifiers changed from: protected */
            public BasicHttpProcessor createHttpProcessor() {
                BasicHttpProcessor processor = AndroidHttpClient.super.createHttpProcessor();
                processor.addRequestInterceptor(AndroidHttpClient.sThreadCheckInterceptor);
                processor.addRequestInterceptor(new CurlLogger());
                return processor;
            }
        };
        enableCurlLogging(TAG, 2);
    }

    public static void setThreadBlocked(boolean flag) {
        sThreadBlocked.set(Boolean.valueOf(flag));
    }

    public void close() {
        if (this.mLeakedException != null) {
            getConnectionManager().shutdown();
            this.mLeakedException = null;
        }
    }

    public <T> T execute(HttpHost httphost, HttpRequest httprequest, ResponseHandler<? extends T> responsehandler) throws IOException, ClientProtocolException {
        return this.delegate.execute(httphost, httprequest, responsehandler);
    }

    public <T> T execute(HttpHost httphost, HttpRequest httprequest, ResponseHandler<? extends T> responsehandler, HttpContext httpcontext) throws IOException, ClientProtocolException {
        return this.delegate.execute(httphost, httprequest, responsehandler, httpcontext);
    }

    public <T> T execute(HttpUriRequest httpurirequest, ResponseHandler<? extends T> responsehandler) throws IOException, ClientProtocolException {
        return this.delegate.execute(httpurirequest, responsehandler);
    }

    public <T> T execute(HttpUriRequest httpurirequest, ResponseHandler<? extends T> responsehandler, HttpContext httpcontext) throws IOException, ClientProtocolException {
        return this.delegate.execute(httpurirequest, responsehandler, httpcontext);
    }

    public HttpResponse execute(HttpHost httphost, HttpRequest httprequest) throws IOException {
        return this.delegate.execute(httphost, httprequest);
    }

    public HttpResponse execute(HttpHost httphost, HttpRequest httprequest, HttpContext httpcontext) throws IOException {
        return this.delegate.execute(httphost, httprequest, httpcontext);
    }

    public HttpResponse execute(HttpUriRequest httpurirequest) throws IOException {
        return this.delegate.execute(httpurirequest);
    }

    public HttpResponse execute(HttpUriRequest httpurirequest, HttpContext httpcontext) throws IOException {
        return this.delegate.execute(httpurirequest, httpcontext);
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        if (this.mLeakedException != null) {
            Log.e(TAG, "Leak found", this.mLeakedException);
            this.mLeakedException = null;
        }
    }

    public ClientConnectionManager getConnectionManager() {
        return this.delegate.getConnectionManager();
    }

    public HttpParams getParams() {
        return this.delegate.getParams();
    }

    private static class LoggingConfiguration {
        private final int level;
        private final String tag;

        private LoggingConfiguration(String tag2, int level2) {
            this.tag = tag2;
            this.level = level2;
        }

        /* access modifiers changed from: private */
        public boolean isLoggable() {
            return Log.isLoggable(this.tag, this.level);
        }

        /* access modifiers changed from: private */
        public boolean isAuthLoggable() {
            return false;
        }

        /* access modifiers changed from: private */
        public void println(String message) {
            Log.println(this.level, this.tag, message);
        }
    }

    public void enableCurlLogging(String name, int level) {
        if (name == null) {
            throw new NullPointerException(QueryApList.Carriers.NAME);
        } else if (level < 2 || level > 7) {
            throw new IllegalArgumentException("Level is out of range [2..7]");
        } else {
            this.curlConfiguration = new LoggingConfiguration(name, level);
        }
    }

    public void disableCurlLogging() {
        this.curlConfiguration = null;
    }

    private class CurlLogger implements HttpRequestInterceptor {
        private CurlLogger() {
        }

        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
            LoggingConfiguration configuration = AndroidHttpClient.this.curlConfiguration;
            if (configuration != null && configuration.isLoggable() && (request instanceof HttpUriRequest)) {
                configuration.println(AndroidHttpClient.toCurl((HttpUriRequest) request, configuration.isAuthLoggable()));
            }
        }
    }

    /* JADX INFO: Multiple debug info for r1v2 java.net.URI: [D('arr$' org.apache.http.Header[]), D('uri' java.net.URI)] */
    /* JADX INFO: Multiple debug info for r8v2 org.apache.http.HttpEntityEnclosingRequest: [D('entityRequest' org.apache.http.HttpEntityEnclosingRequest), D('request' org.apache.http.client.methods.HttpUriRequest)] */
    /* JADX INFO: Multiple debug info for r8v3 org.apache.http.HttpEntity: [D('entityRequest' org.apache.http.HttpEntityEnclosingRequest), D('entity' org.apache.http.HttpEntity)] */
    /* JADX INFO: Multiple debug info for r8v5 java.lang.String: [D('entity' org.apache.http.HttpEntity), D('entityString' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r3v7 int: [D('header' org.apache.http.Header), D('i$' int)] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ac  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String toCurl(org.apache.http.client.methods.HttpUriRequest r8, boolean r9) throws java.io.IOException {
        /*
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r1 = "curl "
            r2.append(r1)
            org.apache.http.Header[] r1 = r8.getAllHeaders()
            int r5 = r1.length
            r3 = 0
            r4 = r3
        L_0x0011:
            if (r4 >= r5) goto L_0x0049
            r3 = r1[r4]
            if (r9 != 0) goto L_0x0033
            java.lang.String r6 = r3.getName()
            java.lang.String r7 = "Authorization"
            boolean r6 = r6.equals(r7)
            if (r6 != 0) goto L_0x002f
            java.lang.String r6 = r3.getName()
            java.lang.String r7 = "Cookie"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x0033
        L_0x002f:
            int r3 = r4 + 1
            r4 = r3
            goto L_0x0011
        L_0x0033:
            java.lang.String r6 = "--header \""
            r2.append(r6)
            java.lang.String r3 = r3.toString()
            java.lang.String r3 = r3.trim()
            r2.append(r3)
            java.lang.String r3 = "\" "
            r2.append(r3)
            goto L_0x002f
        L_0x0049:
            java.net.URI r1 = r8.getURI()
            boolean r9 = r8 instanceof org.apache.http.impl.client.RequestWrapper
            if (r9 == 0) goto L_0x00b2
            r0 = r8
            org.apache.http.impl.client.RequestWrapper r0 = (org.apache.http.impl.client.RequestWrapper) r0
            r9 = r0
            org.apache.http.HttpRequest r9 = r9.getOriginal()
            boolean r3 = r9 instanceof org.apache.http.client.methods.HttpUriRequest
            if (r3 == 0) goto L_0x00b2
            org.apache.http.client.methods.HttpUriRequest r9 = (org.apache.http.client.methods.HttpUriRequest) r9
            java.net.URI r9 = r9.getURI()
        L_0x0063:
            java.lang.String r1 = "\""
            r2.append(r1)
            r2.append(r9)
            java.lang.String r9 = "\""
            r2.append(r9)
            boolean r9 = r8 instanceof org.apache.http.HttpEntityEnclosingRequest
            if (r9 == 0) goto L_0x00a7
            org.apache.http.HttpEntityEnclosingRequest r8 = (org.apache.http.HttpEntityEnclosingRequest) r8
            org.apache.http.HttpEntity r8 = r8.getEntity()
            if (r8 == 0) goto L_0x00a7
            boolean r9 = r8.isRepeatable()
            if (r9 == 0) goto L_0x00a7
            long r3 = r8.getContentLength()
            r5 = 1024(0x400, double:5.06E-321)
            int r9 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r9 >= 0) goto L_0x00ac
            java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream
            r9.<init>()
            r8.writeTo(r9)
            java.lang.String r8 = r9.toString()
            java.lang.String r9 = " --data-ascii \""
            java.lang.StringBuilder r9 = r2.append(r9)
            java.lang.StringBuilder r8 = r9.append(r8)
            java.lang.String r9 = "\""
            r8.append(r9)
        L_0x00a7:
            java.lang.String r8 = r2.toString()
            return r8
        L_0x00ac:
            java.lang.String r8 = " [TOO MUCH DATA TO INCLUDE]"
            r2.append(r8)
            goto L_0x00a7
        L_0x00b2:
            r9 = r1
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.AndroidHttpClient.toCurl(org.apache.http.client.methods.HttpUriRequest, boolean):java.lang.String");
    }
}
