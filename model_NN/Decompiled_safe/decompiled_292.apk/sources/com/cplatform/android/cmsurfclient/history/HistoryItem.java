package com.cplatform.android.cmsurfclient.history;

public class HistoryItem {
    public Long date;
    public long id;
    public String title;
    public String url;

    public HistoryItem(long id2, String title2, String url2, Long date2) {
        this.id = id2;
        this.title = title2;
        this.url = url2;
        this.date = date2;
    }
}
