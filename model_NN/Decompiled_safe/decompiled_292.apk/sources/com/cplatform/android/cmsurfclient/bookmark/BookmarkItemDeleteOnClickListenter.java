package com.cplatform.android.cmsurfclient.bookmark;

import android.app.Activity;
import android.view.View;

public class BookmarkItemDeleteOnClickListenter implements View.OnClickListener {
    public Activity activity;
    public BookmarkItem item;

    public BookmarkItemDeleteOnClickListenter(Activity activity2, BookmarkItem item2) {
        this.activity = activity2;
        this.item = item2;
    }

    public void onClick(View v) {
        ((BookmarkActivity) this.activity).onDelete(this.item);
    }
}
