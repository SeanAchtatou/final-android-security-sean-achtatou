package com.cplatform.android.cmsurfclient.quicklink;

public class QuickLinkItem {
    public int mFlag;
    public int mIdx;
    public String mImage;
    public String mTitle;
    public String mUrl;

    public QuickLinkItem(int idx, int flag, String title, String url, String image) {
        this.mIdx = idx;
        this.mFlag = flag;
        this.mTitle = title;
        this.mUrl = url;
        this.mImage = image;
    }
}
