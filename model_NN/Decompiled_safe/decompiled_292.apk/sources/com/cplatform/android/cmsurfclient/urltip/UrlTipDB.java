package com.cplatform.android.cmsurfclient.urltip;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UrlTipDB {
    private static final String DATABASE_NAME = "CMSurfClient";
    private static int MAX_ITEMS = 50;
    private static int MORE_ITEMS_LIMIT = 10;
    private static final String TABLE_URLTIP_NAME = "urltip";
    private static UrlTipDB instance = null;
    private final String DEBUG_TAG = "UrlTipDB";
    private Context context;
    private SQLiteDatabase db = null;
    private boolean isSort = true;
    public ArrayList<String> items = new ArrayList<>();

    private void clearIfNeed() {
        if (MAX_ITEMS > 0 && this.items.size() >= MAX_ITEMS + MORE_ITEMS_LIMIT) {
            this.isSort = false;
            this.items = load();
        }
    }

    public static UrlTipDB getInstance(Context context2) {
        if (instance == null) {
            instance = new UrlTipDB(context2);
        }
        return instance;
    }

    public UrlTipDB(Context context2) {
        this.context = context2;
        this.isSort = false;
        this.items = load();
    }

    public boolean openDB() {
        if (this.db == null || !this.db.isOpen()) {
            this.db = this.context.openOrCreateDatabase(DATABASE_NAME, 0, null);
            prepareTable();
        }
        return this.db != null && this.db.isOpen();
    }

    public void closeDB() {
        if (this.db != null) {
            if (this.db.isOpen()) {
                this.db.close();
            }
            this.db = null;
        }
    }

    private void prepareTable() {
        StringBuffer sql = new StringBuffer(100);
        sql.append("create table ").append(TABLE_URLTIP_NAME).append("(_id integer primary key autoincrement, url text, visitdate long);");
        try {
            this.db.execSQL(sql.toString());
        } catch (Exception e) {
        }
    }

    private ArrayList<String> load() {
        ArrayList<String> items2 = new ArrayList<>();
        int count = 0;
        long lastTime = 0;
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_URLTIP_NAME, new String[]{QueryApList.Carriers._ID, "url", "visitdate"}, null, null, null, null, "visitdate desc");
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                items2.add(cur.getString(1));
                lastTime = cur.getLong(2);
                cur.moveToNext();
                count++;
                if (MAX_ITEMS > 0 && count >= MAX_ITEMS) {
                    break;
                }
            }
            cur.close();
            closeDB();
        }
        if (MAX_ITEMS > 0 && count >= MAX_ITEMS) {
            clear(lastTime);
        }
        return items2;
    }

    public boolean clear(long visitdate) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_URLTIP_NAME);
                sql.append(" where visitdate<").append(visitdate).append(";");
                this.db.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public long add(String url, Long visitdate) {
        long id = 0;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put("url", url);
                values.put("visitdate", visitdate);
                id = this.db.insert(TABLE_URLTIP_NAME, null, values);
                if (id > 0) {
                    addItem(url);
                }
            } catch (Exception e) {
            }
            closeDB();
        }
        clearIfNeed();
        return id;
    }

    public void updateOrAdd(String url, Long visitdate) {
        if (update(url, visitdate) <= 0) {
            add(url, visitdate);
        }
    }

    public int update(String url, Long visitdate) {
        int rows = 0;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put("url", url);
                values.put("visitdate", visitdate);
                rows = this.db.update(TABLE_URLTIP_NAME, values, " (url='" + url + "') ", null);
            } catch (Exception e) {
            }
            closeDB();
        }
        return rows;
    }

    private void sort() {
        if (!this.isSort) {
            Collections.sort(this.items);
            this.isSort = true;
        }
    }

    public ArrayList<String> getItems() {
        sort();
        return this.items;
    }

    private void addItem(String item) {
        Log.i("UrlTipDB", "add item:" + item);
        if (!this.items.contains(item)) {
            this.isSort = false;
            this.items.add(item);
        }
    }

    private void removeItem(String item) {
        Log.i("UrlTipDB", "remove item:" + item);
        this.items.remove(item);
    }

    private ArrayList<UrlTipItem> guessUrl(String item) {
        ArrayList<UrlTipItem> ret = new ArrayList<>();
        if (item == null) {
            item = WindowAdapter2.BLANK_URL;
        }
        String lowerTmp = item.toLowerCase().trim();
        if (WindowAdapter2.BLANK_URL.equals(lowerTmp)) {
            ret.add(0, new UrlTipItem(null, "http://"));
        } else if ("http://".equals(lowerTmp)) {
            ret.add(0, new UrlTipItem(null, "http://wap."));
            ret.add(0, new UrlTipItem(null, "http://www."));
        } else if ("http://".startsWith(lowerTmp)) {
            ret.add(0, new UrlTipItem(null, "http://"));
        } else if (lowerTmp.indexOf(63) < 0 && lowerTmp.indexOf(35) < 0 && lowerTmp.indexOf(38) < 0 && lowerTmp.lastIndexOf("/") < lowerTmp.indexOf("://") + 3) {
            if (lowerTmp.endsWith(".")) {
                ret.add(0, new UrlTipItem(null, item + "net"));
                ret.add(0, new UrlTipItem(null, item + "com"));
            } else if (!lowerTmp.endsWith(".com") && !lowerTmp.endsWith(".net") && !lowerTmp.endsWith(".org") && !lowerTmp.endsWith(".cn")) {
                ret.add(0, new UrlTipItem(null, item + ".net"));
                ret.add(0, new UrlTipItem(null, item + ".com"));
            }
        }
        return ret;
    }

    private ArrayList<UrlTipItem> historyUrl(int max) {
        return new ArrayList<>();
    }

    public List<UrlTipItem> find(String item) {
        ArrayList<UrlTipItem> ret = guessUrl(item);
        Log.i("UrlTipDB", "find item:" + item);
        if (item == null) {
            ret.addAll(historyUrl(10));
        } else {
            String item2 = item.trim();
            if (WindowAdapter2.BLANK_URL.equals(item2)) {
                ret.addAll(historyUrl(10));
            } else {
                ArrayList<String> items2 = getItems();
                int startPos = items2.indexOf(item2);
                if (startPos < 0) {
                    items2.add(item2);
                    Collections.sort(items2);
                    startPos = items2.indexOf(item2);
                    items2.remove(startPos);
                }
                String tmp = item2 + 65535;
                int endPos = items2.indexOf(tmp);
                if (endPos < 0) {
                    items2.add(tmp);
                    Collections.sort(items2);
                    endPos = items2.indexOf(tmp);
                    items2.remove(endPos);
                }
                if (endPos >= startPos) {
                    List<String> foundList = items2.subList(startPos, endPos);
                    if (foundList != null && foundList.size() > 0) {
                        int count = 0;
                        for (String found : foundList) {
                            ret.add(new UrlTipItem(null, found));
                            count++;
                            if (count >= 20) {
                                break;
                            }
                        }
                    }
                } else {
                    ret.addAll(historyUrl(10));
                }
            }
        }
        return ret;
    }
}
