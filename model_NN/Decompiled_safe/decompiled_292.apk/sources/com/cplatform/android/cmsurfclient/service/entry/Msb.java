package com.cplatform.android.cmsurfclient.service.entry;

import android.util.Log;
import com.cplatform.android.cmsurfclient.download.provider.Constants;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Msb {
    public String domain = null;
    public String domain2 = null;
    public String dver = null;
    public boolean isAssets = false;
    public String rurl = null;
    public SearchEngines search = null;
    public Share share = null;
    public SNews snews = null;
    public Tabs tabs = null;
    public String uid = null;
    public Upgrade upg = null;
    public Weather weather = null;

    public Msb() {
    }

    public Msb(Element entryMsb) {
        if (entryMsb != null) {
            NodeList nlConf = entryMsb.getElementsByTagName("conf");
            if (nlConf != null && nlConf.getLength() > 0) {
                this.domain = ((Element) nlConf.item(0)).getAttribute("domain");
                if (this.domain != null) {
                    this.domain = this.domain.trim();
                    if (WindowAdapter2.BLANK_URL.equals(this.domain)) {
                        this.domain = null;
                    }
                }
                this.domain2 = ((Element) nlConf.item(0)).getAttribute("domain2");
                if (this.domain2 != null) {
                    this.domain2 = this.domain2.trim();
                    if (WindowAdapter2.BLANK_URL.equals(this.domain2)) {
                        this.domain2 = null;
                    }
                }
            }
            NodeList nlRedirect = entryMsb.getElementsByTagName("redirect");
            if (nlRedirect == null || nlRedirect.getLength() <= 0) {
                this.dver = entryMsb.getAttribute("dver");
                this.uid = entryMsb.getAttribute(Constants.UID);
                Log.i("Msb", "uid=" + this.uid.toString());
                NodeList nlUpgrade = entryMsb.getElementsByTagName("upgrade");
                if (nlUpgrade != null && nlUpgrade.getLength() > 0) {
                    this.upg = new Upgrade((Element) nlUpgrade.item(0));
                }
                NodeList nlShare = entryMsb.getElementsByTagName("share");
                if (nlShare != null && nlShare.getLength() > 0) {
                    this.share = new Share((Element) nlShare.item(0));
                }
                NodeList nlWeather = entryMsb.getElementsByTagName("weather");
                if (nlWeather != null && nlWeather.getLength() > 0) {
                    this.weather = new Weather((Element) nlWeather.item(0));
                }
                NodeList nlSnews = entryMsb.getElementsByTagName("snews");
                if (nlSnews != null && nlSnews.getLength() > 0) {
                    this.snews = new SNews((Element) nlSnews.item(0));
                }
                NodeList nlTabs = entryMsb.getElementsByTagName("tabs");
                if (nlTabs != null && nlTabs.getLength() > 0) {
                    this.tabs = new Tabs((Element) nlTabs.item(0));
                }
                NodeList nlSearchs = entryMsb.getElementsByTagName("search2");
                if (nlSearchs == null || nlSearchs.getLength() <= 0) {
                    NodeList nlSearchs2 = entryMsb.getElementsByTagName("search");
                    if (nlSearchs2 != null && nlSearchs2.getLength() > 0) {
                        this.search = new SearchEngines((Element) nlSearchs2.item(0));
                        return;
                    }
                    return;
                }
                this.search = new SearchEngines((Element) nlSearchs.item(0));
                return;
            }
            this.rurl = ((Element) nlRedirect.item(0)).getAttribute("rurl");
            if (this.rurl != null) {
                this.rurl = this.rurl.trim();
                if (WindowAdapter2.BLANK_URL.equals(this.rurl)) {
                    this.rurl = null;
                }
            }
        }
    }

    public String toString() {
        return "dver:" + this.dver + "\tuid:" + this.uid + "\trurl:" + this.rurl + "\tdomain:" + this.domain + " isAssets:" + this.isAssets;
    }
}
