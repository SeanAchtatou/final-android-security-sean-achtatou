package com.cplatform.android.cmsurfclient.naviedit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.cplatform.android.cmsurfclient.R;

public class NaviEditActivity extends Activity {
    View btnNaviSearch;
    View btnNaviUrl;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.naviedit);
        this.btnNaviSearch = findViewById(R.id.btn_search);
        this.btnNaviUrl = findViewById(R.id.btn_url);
        this.btnNaviSearch.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NaviEditActivity.this.startActivity(new Intent(NaviEditActivity.this, NaviEditSearchActivity.class));
            }
        });
        this.btnNaviUrl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                NaviEditActivity.this.startActivity(new Intent(NaviEditActivity.this, NaviEditUrlActivity.class));
            }
        });
    }
}
