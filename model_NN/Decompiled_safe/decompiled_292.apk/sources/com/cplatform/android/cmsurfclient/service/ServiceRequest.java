package com.cplatform.android.cmsurfclient.service;

import android.content.Context;
import android.net.Proxy;
import android.os.Handler;
import android.util.Log;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.cache.CacheDB;
import com.cplatform.android.cmsurfclient.cache.CacheItem;
import com.cplatform.android.cmsurfclient.download.provider.Constants;
import com.cplatform.android.cmsurfclient.network.android.APNUtil;
import com.cplatform.android.cmsurfclient.service.entry.Group;
import com.cplatform.android.cmsurfclient.service.entry.Label;
import com.cplatform.android.cmsurfclient.service.entry.Msb;
import com.cplatform.android.cmsurfclient.service.entry.SNews;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngines;
import com.cplatform.android.cmsurfclient.service.entry.Tab;
import com.cplatform.android.cmsurfclient.service.entry.Tabs;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ServiceRequest {
    public static final String LOG_TAG = "ServiceRequest";
    public static final int SERVICE_MSB = 2;
    public static final int SERVICE_SHARE = 6;
    public static final int SERVICE_SNEWS = 5;
    public static final int SERVICE_UPGRADE = 3;
    public static final int SERVICE_WEATHER = 4;
    private static boolean isFirstVisitCMWap = true;
    /* access modifiers changed from: private */
    public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss", Locale.CHINESE);
    /* access modifiers changed from: private */
    public Context context = null;
    /* access modifiers changed from: private */
    public IServiceRequest handler = null;
    /* access modifiers changed from: private */
    public int reqType = 0;
    /* access modifiers changed from: private */
    public String url = null;

    public class RequestThread extends Thread {
        Handler mChildHandler = null;

        public RequestThread() {
        }

        public void run() {
            int resFileId;
            int resFileId2;
            String baseFileName = ServiceRequest.sdf.format(Long.valueOf(new Date().getTime())) + ((int) (Math.random() * 10000.0d));
            StringBuilder append = new StringBuilder().append(baseFileName);
            int resFileId3 = Constants.MAX_DOWNLOADS + 1;
            String msbFile = append.append((int) Constants.MAX_DOWNLOADS).append(".che").toString();
            try {
                if (!ServiceRequest.this.downloadFile(ServiceRequest.this.url, msbFile, 0, 0)) {
                    if (ServiceRequest.this.handler != null) {
                        ServiceRequest.this.handler.onServiceComplete(ServiceRequest.this.reqType, null);
                    }
                    return;
                }
                Msb msb = ServiceRequest.this.parseFile(msbFile);
                if (msb == null) {
                    if (ServiceRequest.this.handler != null) {
                        ServiceRequest.this.handler.onServiceComplete(ServiceRequest.this.reqType, msb);
                    }
                    return;
                }
                if (msb.snews != null) {
                    SNews snews = msb.snews;
                    if (snews.items != null) {
                        int len = snews.items.size();
                        int i = 0;
                        while (i < len) {
                            ServiceRequest.this.requestFile(snews.items.get(i).img, baseFileName + resFileId3 + ".che");
                            i++;
                            resFileId3++;
                        }
                    }
                }
                int resFileId4 = resFileId3;
                if (msb.tabs != null) {
                    Tabs tabs = msb.tabs;
                    if (tabs.tabs != null) {
                        int len2 = tabs.tabs.size();
                        int i2 = 0;
                        while (true) {
                            resFileId = resFileId4;
                            if (i2 >= len2) {
                                break;
                            }
                            Tab tab = tabs.tabs.get(i2);
                            int resFileId5 = resFileId + 1;
                            ServiceRequest.this.requestFile(tab.imgUp, baseFileName + resFileId + ".che");
                            int resFileId6 = resFileId5 + 1;
                            ServiceRequest.this.requestFile(tab.imgDown, baseFileName + resFileId5 + ".che");
                            if (tab.labels != null) {
                                int labelLen = tab.labels.size();
                                int j = 0;
                                while (j < labelLen) {
                                    Label label = tab.labels.get(j);
                                    int resFileId7 = resFileId6 + 1;
                                    ServiceRequest.this.requestFile(label.imgUp, baseFileName + resFileId6 + ".che");
                                    int resFileId8 = resFileId7 + 1;
                                    ServiceRequest.this.requestFile(label.imgDown, baseFileName + resFileId7 + ".che");
                                    if (label.groups != null) {
                                        resFileId2 = resFileId8;
                                        for (int k = 0; k < label.groups.size(); k++) {
                                            Group group = label.groups.get(k);
                                            int resFileId9 = resFileId2 + 1;
                                            ServiceRequest.this.requestFile(group.img, baseFileName + resFileId2 + ".che");
                                            if (group.items != null) {
                                                int itemsLen = group.items.size();
                                                int m = 0;
                                                while (m < itemsLen) {
                                                    ServiceRequest.this.requestFile(group.items.get(m).img, baseFileName + resFileId9 + ".che");
                                                    m++;
                                                    resFileId9++;
                                                }
                                            }
                                            resFileId2 = resFileId9;
                                        }
                                    } else {
                                        resFileId2 = resFileId8;
                                    }
                                    j++;
                                    resFileId6 = resFileId2;
                                }
                            }
                            resFileId4 = resFileId6;
                            i2++;
                        }
                        resFileId4 = resFileId;
                    }
                }
                if (msb.search != null) {
                    SearchEngines search = msb.search;
                    if (search.items != null) {
                        int len3 = search.items.size();
                        int i3 = 0;
                        while (true) {
                            int resFileId10 = resFileId4;
                            if (i3 >= len3) {
                                break;
                            }
                            resFileId4 = resFileId10 + 1;
                            ServiceRequest.this.requestFile(search.items.get(i3).img, baseFileName + resFileId10 + ".che");
                            i3++;
                        }
                    }
                }
                if (ServiceRequest.this.handler != null) {
                    ServiceRequest.this.handler.onServiceComplete(ServiceRequest.this.reqType, msb);
                }
                if (ServiceRequest.this.reqType != 3) {
                    CacheDB cache = CacheDB.getInstance(ServiceRequest.this.context);
                    if (cache != null) {
                        CacheItem cacheItem = cache.items.get(ServiceRequest.this.url);
                        if (cacheItem != null) {
                            cache.del(cacheItem);
                        }
                        cache.add(new CacheItem(-1, ServiceRequest.this.url, msbFile, new Date()));
                        return;
                    }
                    return;
                }
                ServiceRequest.this.context.deleteFile(msbFile);
            } catch (Exception e) {
                if (ServiceRequest.this.handler != null) {
                    ServiceRequest.this.handler.onServiceComplete(ServiceRequest.this.reqType, null);
                }
            }
        }
    }

    public ServiceRequest(Context context2) {
        this.context = context2;
    }

    public void requestFile(String url2, String file) {
        if (url2 != null && url2.startsWith("http")) {
            CacheDB cache = CacheDB.getInstance(this.context);
            if (cache != null) {
                if (cache.items.get(url2) == null) {
                    String f = cache.getAssetsFile(url2);
                    if (f != null && copyAssetsFileToData(f, file)) {
                        cache.add(new CacheItem(-1, url2, file, new Date()));
                        return;
                    }
                } else {
                    return;
                }
            }
            try {
                if (downloadFile(url2, file, 0, 0)) {
                    cache.add(new CacheItem(-1, url2, file, new Date()));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "download file fail", e);
            }
        }
    }

    public Msb request(int reqType2, String url2, IServiceRequest handler2) {
        CacheItem item;
        this.url = url2;
        this.handler = handler2;
        this.reqType = reqType2;
        Msb msb = null;
        if (reqType2 == 2 || reqType2 == 5 || reqType2 == 4) {
            try {
                CacheDB cache = CacheDB.getInstance(this.context);
                if (!(cache == null || (item = cache.items.get(url2)) == null)) {
                    msb = parseFile(item.file);
                }
            } catch (Exception e) {
                msb = null;
                Log.e(LOG_TAG, "request disk file Exception", e);
            }
        }
        if (reqType2 == 2 && msb == null) {
            try {
                msb = parseAssetsFile("msb/msb.xml");
                msb.isAssets = true;
            } catch (Exception e2) {
                msb = null;
                Log.e(LOG_TAG, "request assets file Exception", e2);
            }
        }
        try {
            new RequestThread().start();
        } catch (Exception e3) {
            Log.e(LOG_TAG, "create request thread Exception", e3);
        }
        return msb;
    }

    public Msb parseFile(String f) {
        Msb msb;
        FileInputStream in = null;
        try {
            in = this.context.openFileInput(f);
            msb = parse(in);
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            msb = null;
            Log.e(LOG_TAG, "request file Exception", e2);
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        return msb;
    }

    public Msb parseAssetsFile(String f) {
        try {
            return parse(this.context.getAssets().open(f));
        } catch (Exception e) {
            Log.e(LOG_TAG, "request file Exception", e);
            return null;
        }
    }

    public Msb request(String u) {
        try {
            HttpURLConnection httpConnection = (HttpURLConnection) new URL(u).openConnection();
            httpConnection.setConnectTimeout(20000);
            if (httpConnection.getResponseCode() == 200) {
                return parse(httpConnection.getInputStream());
            }
            return null;
        } catch (Exception e) {
            Log.e(LOG_TAG, "request url Exception", e);
            return null;
        }
    }

    public Msb parse(InputStream in) {
        Msb msb = null;
        if (in == null) {
            return null;
        }
        try {
            NodeList nl = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(in).getDocumentElement().getElementsByTagName("msb");
            if (nl != null && nl.getLength() > 0) {
                msb = new Msb((Element) nl.item(0));
            }
        } catch (Exception e) {
            msb = null;
            Log.e(LOG_TAG, "parse msb Exception", e);
        }
        return msb;
    }

    public Msb readLocalMsb(String params) {
        CacheItem item;
        String url2 = "http://go.10086.cn/msb_server/" + "msb.do";
        if (params != null && params.length() > 0) {
            url2 = url2 + "?" + params;
        }
        Msb msb = null;
        try {
            CacheDB cache = CacheDB.getInstance(this.context);
            if (!(cache == null || (item = cache.items.get(url2)) == null)) {
                msb = parseFile(item.file);
            }
        } catch (Exception e) {
            msb = null;
            Log.e(LOG_TAG, "request disk file Exception in readLocalMsb()", e);
        }
        if (msb != null) {
            return msb;
        }
        try {
            Msb msb2 = parseAssetsFile("msb/msb.xml");
            msb2.isAssets = true;
            return msb2;
        } catch (Exception e2) {
            Log.e(LOG_TAG, "request assets file Exception in readLocalMsb()", e2);
            return null;
        }
    }

    public Msb requestMsb(int reqType2, IServiceRequest handler2, String params) {
        String url2 = null;
        switch (reqType2) {
            case 2:
                url2 = "http://go.10086.cn/msb_server/" + "msb.do";
                break;
            case 3:
                url2 = "http://go.10086.cn/msb_server/" + "upg.do";
                break;
            case 4:
                url2 = "http://go.10086.cn/msb_server/" + "weather.do";
                break;
            case 5:
                url2 = "http://go.10086.cn/msb_server/" + "snews.do";
                break;
            case 6:
                url2 = "http://go.10086.cn/msb_server/" + "share.do";
                break;
        }
        if (params != null && params.length() > 0) {
            url2 = url2 + "?" + params;
        }
        return request(reqType2, url2, handler2);
    }

    public boolean downloadFile(String u, String f, int mode, int timeout) throws Exception {
        Log.e("download", "u:" + u + " f:" + f);
        boolean ret = false;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        HttpClient httpClient = new DefaultHttpClient(basicHttpParams);
        if (SurfManagerActivity.isCMWap()) {
            httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
        }
        HttpResponse httpResponse = httpClient.execute(new HttpGet(u));
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        if (APNUtil.getInstance(this.context).mCurAPNType != APNUtil.APNType.CMWAP || !isFirstVisitCMWap) {
            if (responseCode == 200) {
                InputStream in = httpResponse.getEntity().getContent();
                FileOutputStream out = this.context.openFileOutput(f, mode);
                byte[] bytes = new byte[4096];
                while (true) {
                    int c = in.read(bytes);
                    if (c != -1) {
                        out.write(bytes, 0, c);
                    } else {
                        try {
                            break;
                        } catch (Exception e) {
                        }
                    }
                }
                in.close();
                try {
                    out.close();
                } catch (Exception e2) {
                }
                ret = true;
            }
            return ret;
        }
        isFirstVisitCMWap = false;
        return downloadFile(u, f, mode, 30000);
    }

    public boolean downloadFile(String u, String f, int mode, int timeout, DownloadCallback callback) throws Exception {
        Log.e("download", "u:" + u + " f:" + f);
        boolean ret = false;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 30000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 30000);
        HttpClient httpClient = new DefaultHttpClient(basicHttpParams);
        if (SurfManagerActivity.isCMWap()) {
            httpClient.getParams().setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
        }
        HttpResponse httpResponse = httpClient.execute(new HttpGet(u));
        int responseCode = httpResponse.getStatusLine().getStatusCode();
        if (APNUtil.getInstance(this.context).mCurAPNType != APNUtil.APNType.CMWAP || !isFirstVisitCMWap) {
            if (responseCode == 200) {
                long totalSize = -1;
                long downloadSize = 0;
                if (callback != null) {
                    try {
                        totalSize = Long.parseLong(httpResponse.getLastHeader("Content-Length").getValue());
                    } catch (Exception e) {
                        totalSize = -1;
                    }
                    callback.onDownload(totalSize, 0);
                }
                InputStream in = httpResponse.getEntity().getContent();
                FileOutputStream out = this.context.openFileOutput(f, mode);
                byte[] bytes = new byte[4096];
                while (true) {
                    int c = in.read(bytes);
                    if (c != -1) {
                        out.write(bytes, 0, c);
                        downloadSize += (long) c;
                        if (callback != null) {
                            callback.onDownload(totalSize, downloadSize);
                        }
                    } else {
                        try {
                            break;
                        } catch (Exception e2) {
                        }
                    }
                }
                in.close();
                try {
                    out.close();
                } catch (Exception e3) {
                }
                ret = true;
            }
            return ret;
        }
        isFirstVisitCMWap = false;
        return downloadFile(u, f, mode, 30000);
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public HttpURLConnection getURLConnection(String url2) throws Exception {
        if (Proxy.getDefaultHost() == null) {
            return (HttpURLConnection) new URL(url2).openConnection();
        }
        return (HttpURLConnection) new URL(url2).openConnection(new java.net.Proxy(Proxy.Type.HTTP, new InetSocketAddress(android.net.Proxy.getDefaultHost(), android.net.Proxy.getDefaultPort())));
    }

    private boolean copyAssetsFileToData(String assetsFile, String dataFile) {
        boolean ret = false;
        InputStream in = null;
        FileOutputStream out = null;
        try {
            InputStream in2 = this.context.getAssets().open(assetsFile);
            FileOutputStream out2 = this.context.openFileOutput(dataFile, 0);
            byte[] bytes = new byte[40960];
            while (true) {
                int c = in2.read(bytes);
                if (c == -1) {
                    break;
                }
                out2.write(bytes, 0, c);
            }
            ret = true;
            if (in2 != null) {
                try {
                    in2.close();
                } catch (IOException e) {
                }
            }
            if (out2 != null) {
                try {
                    out2.close();
                } catch (IOException e2) {
                }
            }
        } catch (Exception e3) {
            Log.e(LOG_TAG, "request file Exception", e3);
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e4) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e5) {
                }
            }
        } catch (Throwable th) {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e6) {
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e7) {
                }
            }
            throw th;
        }
        return ret;
    }
}
