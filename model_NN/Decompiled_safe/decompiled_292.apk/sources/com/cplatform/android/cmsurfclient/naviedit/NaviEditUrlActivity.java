package com.cplatform.android.cmsurfclient.naviedit;

import android.app.Activity;
import android.content.Intent;
import android.net.ParseException;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.download.util.FileNameUtils;
import com.cplatform.android.cmsurfclient.history.HistoryDB;
import com.cplatform.android.cmsurfclient.service.entry.Msb;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngine;
import com.cplatform.android.cmsurfclient.service.entry.SearchEngines;
import com.cplatform.android.cmsurfclient.urltip.UrlTipAdapter;
import com.cplatform.android.cmsurfclient.urltip.UrlTipItem;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class NaviEditUrlActivity extends Activity {
    /* access modifiers changed from: private */
    public TextView btnNaviGo;
    /* access modifiers changed from: private */
    public TextView clearView = null;
    /* access modifiers changed from: private */
    public EditText editNaviUrl;
    private ArrayList<UrlTipItem> mUrlTipItems;
    /* access modifiers changed from: private */
    public View mUrlTipSelector;
    private UrlTipAdapter mVisitHistoryAdapter;
    /* access modifiers changed from: private */
    public ListView mVisitHistoryView;
    public int top = 0;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean showUrl = false;
        int top2 = 40;
        CharSequence url = null;
        Bundle b = getIntent().getExtras();
        int background = R.drawable.naviedit_input_bg;
        if (b != null) {
            top2 = b.getInt("padding_top", 0);
            showUrl = b.getBoolean("show_url", false);
            url = b.getCharSequence("url");
            background = b.getInt("background", R.drawable.naviedit_input_bg);
        }
        requestWindowFeature(1);
        setContentView((int) R.layout.naviedit_browse);
        ((RelativeLayout) findViewById(R.id.naviedit_bar)).setBackgroundResource(background);
        View v = findViewById(R.id.naviedit_top);
        v.setPadding(v.getPaddingLeft(), top2, v.getPaddingRight(), (v.getPaddingBottom() + top2) - v.getPaddingTop());
        this.btnNaviGo = (TextView) findViewById(R.id.btn_go);
        this.editNaviUrl = (EditText) findViewById(R.id.edit_url);
        if (!showUrl || TextUtils.isEmpty(url)) {
            this.editNaviUrl.setText(WindowAdapter2.BLANK_URL);
            this.btnNaviGo.setText((int) R.string.naviedit_cancel);
        } else {
            this.editNaviUrl.setText(url);
            this.editNaviUrl.setSelectAllOnFocus(true);
            this.btnNaviGo.setText((int) R.string.naviedit_url_go);
        }
        this.mUrlTipSelector = findViewById(R.id.url_tip_selector);
        this.mUrlTipItems = new ArrayList<>();
        this.mVisitHistoryAdapter = new UrlTipAdapter(this, this.mUrlTipItems);
        this.mVisitHistoryView = (ListView) findViewById(R.id.listview_urltip);
        if (this.clearView == null && HistoryDB.getInstance(this).getCount() > 0) {
            this.clearView = new TextView(this);
            this.clearView.setSingleLine(true);
            this.clearView.setText(Html.fromHtml(getResources().getString(R.string.cleanvisitrecord)));
            this.clearView.setGravity(5);
            this.clearView.setTextColor(-14540118);
            this.clearView.setPadding(0, 0, 10, 0);
            this.clearView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    HistoryDB.getInstance(NaviEditUrlActivity.this).clear();
                    NaviEditUrlActivity.this.refreshUrlTip();
                    NaviEditUrlActivity.this.mVisitHistoryView.removeFooterView(NaviEditUrlActivity.this.clearView);
                    TextView unused = NaviEditUrlActivity.this.clearView = null;
                }
            });
            this.mVisitHistoryView.addFooterView(this.clearView);
        }
        this.mVisitHistoryView.setAdapter((ListAdapter) this.mVisitHistoryAdapter);
        this.editNaviUrl.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!NaviEditUrlActivity.this.editNaviUrl.isFocused() || NaviEditUrlActivity.this.mUrlTipSelector.getVisibility() == 0) {
                    NaviEditUrlActivity.this.mUrlTipSelector.setVisibility(8);
                } else {
                    NaviEditUrlActivity.this.refreshUrlTip();
                }
            }
        });
        this.editNaviUrl.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    NaviEditUrlActivity.this.btnNaviGo.setText((int) R.string.naviedit_url_go);
                } else {
                    NaviEditUrlActivity.this.btnNaviGo.setText((int) R.string.naviedit_cancel);
                }
                NaviEditUrlActivity.this.refreshUrlTip();
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        this.editNaviUrl.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode != 66) {
                    return false;
                }
                NaviEditUrlActivity.this.setResult(-1, new Intent().setAction(NaviEditUrlActivity.this.guessInputUrl(NaviEditUrlActivity.this.editNaviUrl.getText().toString())));
                NaviEditUrlActivity.this.finish();
                return true;
            }
        });
        ((InputMethodManager) this.editNaviUrl.getContext().getSystemService("input_method")).toggleSoftInput(0, 2);
        this.btnNaviGo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String url = NaviEditUrlActivity.this.editNaviUrl.getText().toString();
                if (!TextUtils.isEmpty(url)) {
                    NaviEditUrlActivity.this.setResult(-1, new Intent().setAction(NaviEditUrlActivity.this.guessInputUrl(url)));
                } else {
                    NaviEditUrlActivity.this.setResult(0, null);
                }
                NaviEditUrlActivity.this.finish();
            }
        });
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            View v = findViewById(R.id.naviedit_bar);
            int[] location = {0, 0};
            v.getLocationOnScreen(location);
            float y = event.getY();
            if (y > ((float) (location[1] + v.getHeight())) || y < ((float) location[1])) {
                setResult(0, null);
                finish();
            } else {
                float x = event.getX();
                this.btnNaviGo.getLocationOnScreen(location);
                if (x >= ((float) location[0])) {
                    this.btnNaviGo.performClick();
                }
            }
        }
        return true;
    }

    public void onClick(UrlTipItem item) {
        String url;
        if (item == null || item.url == null) {
            url = WindowAdapter2.BLANK_URL;
        } else {
            url = item.url;
        }
        this.editNaviUrl.setText(url);
        this.editNaviUrl.setSelection(url.length());
        refreshUrlTip();
    }

    private ArrayList<UrlTipItem> guessUrl(String item) {
        ArrayList<UrlTipItem> ret = new ArrayList<>();
        if (item == null) {
            item = WindowAdapter2.BLANK_URL;
        }
        String lowerTmp = item.toLowerCase().trim();
        if (WindowAdapter2.BLANK_URL.equals(lowerTmp)) {
            ret.add(0, new UrlTipItem(null, "http://"));
        } else if ("http://".equals(lowerTmp)) {
            ret.add(0, new UrlTipItem(null, "http://wap."));
            ret.add(0, new UrlTipItem(null, "http://www."));
        } else if ("http://".startsWith(lowerTmp)) {
            ret.add(0, new UrlTipItem(null, "http://"));
        } else if (lowerTmp.indexOf(63) < 0 && lowerTmp.indexOf(35) < 0 && lowerTmp.indexOf(38) < 0 && lowerTmp.lastIndexOf("/") < lowerTmp.indexOf("://") + 3) {
            if (lowerTmp.endsWith(".")) {
                ret.add(0, new UrlTipItem(null, item + "net"));
                ret.add(0, new UrlTipItem(null, item + "cn"));
                ret.add(0, new UrlTipItem(null, item + "com.cn"));
                ret.add(0, new UrlTipItem(null, item + "com"));
            } else if (!lowerTmp.endsWith(".com") && !lowerTmp.endsWith(".net") && !lowerTmp.endsWith(".org") && !lowerTmp.endsWith(".cn")) {
                ret.add(0, new UrlTipItem(null, item + ".net"));
                ret.add(0, new UrlTipItem(null, item + ".cn"));
                ret.add(0, new UrlTipItem(null, item + ".com.cn"));
                ret.add(0, new UrlTipItem(null, item + ".com"));
            }
        }
        ArrayList<UrlTipItem> historyItems = HistoryDB.getInstance(this).find(item, 20);
        if (historyItems != null && historyItems.size() > 0) {
            ret.addAll(historyItems);
        }
        return ret;
    }

    public void refreshUrlTip() {
        String tmp = this.editNaviUrl.getText().toString();
        List<UrlTipItem> ret = guessUrl(tmp);
        this.mUrlTipItems.clear();
        HashSet<String> urls = new HashSet<>();
        if (ret != null && ret.size() > 0) {
            for (UrlTipItem tipUrl : ret) {
                if (tipUrl != null && tipUrl.url != null && !tipUrl.url.equals(tmp) && !urls.contains(tipUrl.url)) {
                    urls.add(tipUrl.url);
                    this.mUrlTipItems.add(tipUrl);
                    Log.e("found_urltip", tipUrl.url);
                }
            }
        }
        this.mUrlTipSelector.setVisibility(this.mUrlTipItems.size() > 0 ? 0 : 8);
        this.mVisitHistoryAdapter.notifyDataSetChanged();
    }

    /* access modifiers changed from: private */
    public String guessInputUrl(String inUrl) {
        FileNameUtils.WebAddress webAddress;
        String key;
        String str;
        if (inUrl == null) {
            return WindowAdapter2.BLANK_URL;
        }
        String retVal = inUrl;
        if (inUrl.length() == 0) {
            return inUrl;
        }
        if (inUrl.startsWith("about:")) {
            return inUrl;
        }
        if (inUrl.startsWith("data:")) {
            return inUrl;
        }
        if (inUrl.startsWith("file:")) {
            return inUrl;
        }
        if (inUrl.startsWith("javascript:")) {
            return inUrl;
        }
        if (inUrl.endsWith(".")) {
            inUrl = inUrl.substring(0, inUrl.length() - 1);
        }
        try {
            webAddress = new FileNameUtils.WebAddress(inUrl);
            if (webAddress.mHost.indexOf(46) >= 0) {
                return webAddress.toString();
            }
        } catch (ParseException e) {
            webAddress = null;
        }
        String searchURL = null;
        String searchEncode = null;
        Msb msb = SurfManagerActivity.mMsb;
        if (!(msb == null || msb.search == null)) {
            SearchEngines search = msb.search;
            if (search.items != null) {
                int len = search.items.size();
                for (int i = 0; i < len; i++) {
                    SearchEngine se = search.items.get(i);
                    if (se.isDefault) {
                        searchURL = se.url;
                        searchEncode = se.encode;
                    } else if (searchURL == null) {
                        searchURL = se.url;
                        searchEncode = se.encode;
                    }
                }
            }
        }
        if (searchURL != null) {
            if (searchEncode != null) {
                try {
                    if (!WindowAdapter2.BLANK_URL.equals(searchEncode)) {
                        str = searchEncode;
                        key = URLEncoder.encode(retVal, str);
                        return searchURL + key;
                    }
                } catch (UnsupportedEncodingException e2) {
                    key = retVal;
                }
            }
            str = "gbk";
            key = URLEncoder.encode(retVal, str);
            return searchURL + key;
        } else if (webAddress == null) {
            return inUrl;
        } else {
            if (webAddress.mHost != null && webAddress.mHost.indexOf(46) == -1) {
                webAddress.mHost = "www." + webAddress.mHost + ".com";
            }
            return webAddress.toString();
        }
    }
}
