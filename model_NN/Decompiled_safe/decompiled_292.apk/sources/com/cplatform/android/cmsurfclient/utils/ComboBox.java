package com.cplatform.android.cmsurfclient.utils;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;

public class ComboBox extends LinearLayout {
    private ImageButton _button;
    /* access modifiers changed from: private */
    public AutoCompleteTextView _text;

    public ComboBox(Context context) {
        super(context);
        createChildControls(context);
    }

    public ComboBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        createChildControls(context);
    }

    private void createChildControls(Context context) {
        setOrientation(0);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this._text = new AutoCompleteTextView(context);
        this._text.setSingleLine();
        this._text.setInputType(114689);
        this._text.setRawInputType(128);
        addView(this._text, new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this._button = new ImageButton(context);
        this._button.setImageResource(17301506);
        this._button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ComboBox.this._text.showDropDown();
            }
        });
        addView(this._button, new LinearLayout.LayoutParams(-2, -2));
    }

    public void setSuggestionSource(Cursor source, String column) {
        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(getContext(), 17367050, source, new String[]{column}, new int[]{16908308});
        cursorAdapter.setStringConversionColumn(source.getColumnIndex(column));
        this._text.setAdapter(cursorAdapter);
    }

    public String getText() {
        return this._text.getText().toString();
    }

    public void setText(String text) {
        this._text.setText(text);
    }
}
