package com.cplatform.android.cmsurfclient;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.ClipboardManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;

public class SelectText extends RelativeLayout {
    private final String LOG_TAG = SelectText.class.getSimpleName();
    public final int SEARCHTEXT = 1;
    public final int SHARETEXT = 2;
    private ISelectText mBrowser = null;
    private Context mContext = null;
    private Button mCopy = null;
    Handler mHandler = new MessageHandler();
    private ImageView mImageDown = null;
    private int mImageHeight = 32;
    private ImageView mImageUp = null;
    private int mImageWidth = 29;
    private boolean mIsSelectTextMode = false;
    private RelativeLayout mPopup = null;
    private int mPopupHeight = 0;
    private int mPopupWidth = 0;
    private Button mSearch = null;
    private String mSelectedText = WindowAdapter2.BLANK_URL;
    private ImageView mSelectingImage = null;
    private Button mShare = null;
    /* access modifiers changed from: private */
    public SurfWebView mWebView = null;
    private ClickListener popClick = new ClickListener();

    public SelectText(Context context) {
        super(context);
        this.mContext = context;
        this.mBrowser = null;
        initChildView();
    }

    public SelectText(Context context, AttributeSet attributeset) {
        super(context, attributeset);
        this.mContext = context;
        this.mBrowser = null;
        initChildView();
    }

    public SelectText(Context context, AttributeSet attributeset, int i) {
        super(context, attributeset, i);
        this.mContext = context;
        this.mBrowser = null;
        initChildView();
    }

    private void actionDown(int x, int y) {
        if (this.mSelectingImage == null) {
            RelativeLayout.LayoutParams layoutParamsUp = (RelativeLayout.LayoutParams) this.mImageUp.getLayoutParams();
            layoutParamsUp.setMargins(x - (this.mImageWidth / 2), y, 0, 0);
            this.mImageUp.setLayoutParams(layoutParamsUp);
            RelativeLayout.LayoutParams layoutParamsDown = (RelativeLayout.LayoutParams) this.mImageDown.getLayoutParams();
            layoutParamsDown.setMargins(x - (this.mImageWidth / 2), y - this.mImageHeight, 0, 0);
            this.mImageDown.setLayoutParams(layoutParamsDown);
            this.mImageUp.setVisibility(0);
            this.mImageDown.setVisibility(0);
            this.mSelectingImage = this.mImageDown;
        }
        this.mPopup.setVisibility(4);
        setSelectedImage(x, y);
        highLightText(x, y);
    }

    private void actionMove(int x, int y) {
        setSelectedImage(x, y);
        highLightText(x, y);
    }

    private void actionUp(int x, int y) {
        highLightText(x, y);
        movePopup();
    }

    private void highLightText(int i, int j) {
        int l1;
        int i2;
        int i22;
        RelativeLayout.LayoutParams layoutparams = (RelativeLayout.LayoutParams) this.mImageUp.getLayoutParams();
        RelativeLayout.LayoutParams layoutparams1 = (RelativeLayout.LayoutParams) this.mImageDown.getLayoutParams();
        RelativeLayout.LayoutParams layoutparams2 = (RelativeLayout.LayoutParams) this.mSelectingImage.getLayoutParams();
        if (i < getWidth() - this.mImageWidth) {
            l1 = i - (this.mImageWidth / 2);
        } else {
            l1 = getWidth() - this.mImageWidth;
        }
        int i23 = 0;
        if (this.mSelectingImage != this.mImageDown) {
            if (this.mSelectingImage == this.mImageUp) {
                if (j > layoutparams1.topMargin + this.mImageHeight) {
                    i22 = j;
                } else {
                    i22 = layoutparams1.topMargin + this.mImageHeight;
                }
                i23 = Math.min(i22, getHeight() - this.mImageHeight);
            }
            layoutparams2.setMargins(l1, i23, 0, 0);
            this.mSelectingImage.setLayoutParams(layoutparams2);
            this.mWebView.highLightText(layoutparams1.leftMargin + (this.mImageWidth / 2), layoutparams1.topMargin + this.mImageHeight, layoutparams.leftMargin + (this.mImageWidth / 2), layoutparams.topMargin);
            return;
        }
        if (j < layoutparams.topMargin - this.mImageHeight) {
            i2 = j;
        } else {
            i2 = layoutparams.topMargin - this.mImageHeight;
        }
        layoutparams2.setMargins(l1, Math.max(i2, -(this.mImageHeight / 2)), 0, 0);
        this.mSelectingImage.setLayoutParams(layoutparams2);
        this.mWebView.highLightText(layoutparams1.leftMargin + (this.mImageWidth / 2), layoutparams1.topMargin + this.mImageHeight, layoutparams.leftMargin + (this.mImageWidth / 2), layoutparams.topMargin);
    }

    private void initChildView() {
        LayoutInflater.from(this.mContext).inflate((int) R.layout.select_text, this);
        this.mImageUp = (ImageView) findViewById(R.id.imageUp);
        this.mImageDown = (ImageView) findViewById(R.id.imageDown);
        this.mImageHeight = this.mImageDown.getLayoutParams().height;
        this.mImageWidth = this.mImageUp.getLayoutParams().width;
        this.mPopup = (RelativeLayout) findViewById(R.id.select_text_pop);
        LayoutInflater.from(this.mContext).inflate((int) R.layout.select_text_popup, this.mPopup);
        this.mPopup.measure(0, 0);
        this.mPopupHeight = this.mPopup.getMeasuredHeight();
        this.mPopupWidth = this.mPopup.getMeasuredWidth();
        this.mCopy = (Button) findViewById(R.id.btn_copy);
        this.mSearch = (Button) findViewById(R.id.btn_search);
        this.mShare = (Button) findViewById(R.id.btn_share);
        setListener();
    }

    private boolean isSelectedImage(int x, int y, ImageView imageView) {
        RelativeLayout.LayoutParams layoutparams = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
        int topMargin = layoutparams.topMargin;
        int leftMargin = layoutparams.leftMargin;
        if (imageView != this.mImageDown) {
            if (imageView != this.mImageUp || x <= leftMargin - 10 || x >= this.mImageWidth + leftMargin + 10 || y <= topMargin || y >= this.mImageHeight + topMargin + 20) {
                return false;
            }
            return true;
        } else if (x <= leftMargin - 10 || x >= this.mImageWidth + leftMargin + 10 || y <= topMargin - 20 || y >= this.mImageHeight + topMargin) {
            return false;
        } else {
            return true;
        }
    }

    private void movePopup() {
        int top;
        int left;
        RelativeLayout.LayoutParams layoutParamsImage = (RelativeLayout.LayoutParams) this.mSelectingImage.getLayoutParams();
        if (this.mPopup.getWidth() > 0) {
            this.mPopupWidth = this.mPopup.getWidth();
        }
        if (this.mPopup.getHeight() > 0) {
            this.mPopupHeight = this.mPopup.getHeight();
        }
        int k1 = layoutParamsImage.leftMargin + (this.mImageWidth / 2);
        if (this.mSelectingImage == this.mImageDown) {
            top = layoutParamsImage.topMargin;
        } else {
            top = layoutParamsImage.topMargin + this.mImageHeight;
        }
        if (k1 >= this.mPopupWidth / 2) {
            left = k1 - (this.mPopupWidth / 2);
        } else {
            left = 2;
        }
        int l2 = getWidth() - k1;
        if (l2 < this.mPopupWidth / 2) {
            left = (k1 - (this.mPopupWidth - l2)) - 2;
        }
        int j3 = 0;
        if (this.mSelectingImage == this.mImageDown) {
            if (top >= this.mPopupHeight) {
                j3 = top - this.mPopupHeight;
            } else {
                j3 = this.mImageHeight + top;
            }
        }
        if (this.mSelectingImage == this.mImageUp) {
            if (getHeight() - top < this.mPopupWidth / 2) {
                j3 = layoutParamsImage.topMargin - this.mPopupHeight;
            } else {
                j3 = top;
            }
        }
        RelativeLayout.LayoutParams layoutParamsPopup = (RelativeLayout.LayoutParams) this.mPopup.getLayoutParams();
        layoutParamsPopup.setMargins(left, j3, 0, 0);
        this.mPopup.setLayoutParams(layoutParamsPopup);
        this.mPopup.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void searchText() {
        this.mSelectedText = ((ClipboardManager) getContext().getSystemService("clipboard")).getText().toString();
        String searchText = this.mSelectedText;
        if (this.mBrowser != null) {
            this.mBrowser.onSearchText(searchText);
        }
    }

    private void setListener() {
        this.mCopy.setOnClickListener(this.popClick);
        this.mSearch.setOnClickListener(this.popClick);
        this.mShare.setOnClickListener(this.popClick);
    }

    private void setSelectedImage(int x, int y) {
        if (isSelectedImage(x, y, this.mImageDown)) {
            this.mSelectingImage = this.mImageDown;
        } else if (isSelectedImage(x, y, this.mImageUp)) {
            this.mSelectingImage = this.mImageUp;
        }
    }

    /* access modifiers changed from: private */
    public void shareText() {
        this.mSelectedText = ((ClipboardManager) getContext().getSystemService("clipboard")).getText().toString();
        if (this.mBrowser != null) {
            this.mBrowser.onShareText(this.mSelectedText);
        }
    }

    public void endSelectText() {
        this.mIsSelectTextMode = false;
        this.mSelectingImage = null;
        if (this.mImageUp != null) {
            this.mImageUp.setVisibility(4);
        }
        if (this.mImageDown != null) {
            this.mImageDown.setVisibility(4);
        }
        if (this.mPopup != null) {
            this.mPopup.setVisibility(4);
        }
        setVisibility(4);
        if (this.mWebView != null) {
            this.mWebView.cancelHighLightText();
        }
    }

    public WebView getWebView() {
        return this.mWebView;
    }

    public boolean isSelectTextMode() {
        return this.mIsSelectTextMode;
    }

    public boolean onTouchEvent(MotionEvent event) {
        boolean ret = true;
        if (!this.mIsSelectTextMode) {
            return super.onTouchEvent(event);
        }
        switch (event.getAction()) {
            case 0:
                actionDown((int) event.getX(), (int) event.getY());
                break;
            case 1:
                actionUp((int) event.getX(), (int) event.getY());
                break;
            case 2:
                actionMove((int) event.getX(), (int) event.getY());
                break;
            default:
                ret = super.onTouchEvent(event);
                break;
        }
        return ret;
    }

    public void setSelectTextMode(boolean flag) {
        this.mIsSelectTextMode = flag;
    }

    public void setInterface(ISelectText selectText) {
        this.mBrowser = selectText;
    }

    public void setWebView(SurfWebView webviewmodule) {
        this.mWebView = webviewmodule;
    }

    public void startSelectText() {
        Log.e(this.LOG_TAG, "start select text mode...");
        this.mIsSelectTextMode = true;
        this.mSelectingImage = null;
        this.mPopup.setVisibility(4);
        setVisibility(0);
    }

    public void stopSelectText() {
        Log.e(this.LOG_TAG, "stop select text mode...");
        this.mIsSelectTextMode = false;
        this.mSelectingImage = null;
        this.mPopup.setVisibility(4);
        setVisibility(4);
    }

    private class MessageHandler extends Handler {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    SelectText.this.searchText();
                    return;
                case 2:
                    SelectText.this.shareText();
                    return;
                default:
                    return;
            }
        }

        MessageHandler() {
        }
    }

    private class ClickListener implements View.OnClickListener {
        public void onClick(View view) {
            SelectText.this.mWebView.copyHighlightedText();
            switch (view.getId()) {
                case R.id.btn_search:
                    SelectText.this.mHandler.sendEmptyMessage(1);
                    break;
                case R.id.btn_share:
                    SelectText.this.mHandler.sendEmptyMessage(2);
                    break;
            }
            SelectText.this.endSelectText();
        }

        ClickListener() {
        }
    }
}
