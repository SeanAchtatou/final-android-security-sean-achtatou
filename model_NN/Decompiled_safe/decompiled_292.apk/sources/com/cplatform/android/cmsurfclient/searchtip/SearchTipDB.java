package com.cplatform.android.cmsurfclient.searchtip;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.preference.SurfBrowserSettings;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;

public class SearchTipDB {
    private static final String DATABASE_NAME = "CMSurfClient";
    private static int MAX_ITEMS = Downloads.STATUS_SUCCESS;
    private static int MORE_ITEMS_LIMIT = 50;
    private static final String TABLE_SEARCHTIP_NAME = "searchtip";
    private static SearchTipDB instance = null;
    private Context context;
    private SQLiteDatabase db = null;
    private boolean isTableCreated = false;
    private int itemCount = 0;

    private void clearIfNeed(long date) {
        if (MAX_ITEMS > 0 && this.itemCount >= MAX_ITEMS + MORE_ITEMS_LIMIT) {
            clear(date);
            this.itemCount = getCount();
        }
    }

    private void clearIfNeed() {
        if (MAX_ITEMS > 0 && this.itemCount >= MAX_ITEMS + MORE_ITEMS_LIMIT) {
            clear(getLastDate());
            this.itemCount = getCount();
        }
    }

    public static SearchTipDB getInstance(Context context2) {
        if (instance == null) {
            instance = new SearchTipDB(context2);
        }
        return instance;
    }

    public SearchTipDB(Context context2) {
        this.context = context2;
        this.itemCount = getCount();
    }

    public boolean openDB() {
        if (this.db == null || !this.db.isOpen()) {
            this.db = this.context.openOrCreateDatabase(DATABASE_NAME, 0, null);
            prepareTable();
        }
        return this.db != null && this.db.isOpen();
    }

    public void closeDB() {
        if (this.db != null) {
            if (this.db.isOpen()) {
                this.db.close();
            }
            this.db = null;
        }
    }

    private void prepareTable() {
        if (!this.isTableCreated) {
            StringBuffer sql = new StringBuffer(100);
            sql.append("create table ").append(TABLE_SEARCHTIP_NAME).append("(_id integer primary key autoincrement, title text, visitdate long);");
            try {
                this.db.execSQL(sql.toString());
            } catch (Exception e) {
            }
            this.isTableCreated = true;
        }
    }

    public ArrayList<SearchTipItem> load() {
        ArrayList<SearchTipItem> items = new ArrayList<>();
        long date = 0;
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_SEARCHTIP_NAME, new String[]{Downloads.COLUMN_TITLE, "visitdate"}, null, null, null, null, "visitdate desc");
            cur.moveToFirst();
            int count = 0;
            while (!cur.isAfterLast()) {
                date = cur.getLong(1);
                items.add(new SearchTipItem(cur.getString(0), date));
                cur.moveToNext();
                count++;
                if (MAX_ITEMS > 0 && count >= MAX_ITEMS) {
                    break;
                }
            }
            cur.close();
            closeDB();
        }
        clearIfNeed(date);
        return items;
    }

    public long getLastDate() {
        long date = 0;
        if (openDB()) {
            Cursor cur = this.db.query(TABLE_SEARCHTIP_NAME, new String[]{"visitdate"}, null, null, null, null, "visitdate desc");
            cur.moveToFirst();
            int count = 0;
            while (!cur.isAfterLast()) {
                date = cur.getLong(0);
                cur.moveToNext();
                count++;
                if (MAX_ITEMS > 0 && count >= MAX_ITEMS) {
                    break;
                }
            }
            cur.close();
            closeDB();
        }
        return date;
    }

    public ArrayList<SearchTipItem> find(String title, int max) {
        if (title != null) {
            title = title.trim();
        }
        if (WindowAdapter2.BLANK_URL.equals(title)) {
            title = null;
        }
        ArrayList<SearchTipItem> items = new ArrayList<>();
        try {
            if (openDB()) {
                int count = 0;
                if (title != null) {
                    Cursor cur = this.db.query(TABLE_SEARCHTIP_NAME, new String[]{Downloads.COLUMN_TITLE, "visitdate"}, " title like ? ", new String[]{title + "%"}, null, null, "visitdate desc");
                    cur.moveToFirst();
                    count = 0;
                    while (!cur.isAfterLast()) {
                        items.add(new SearchTipItem(cur.getString(0), cur.getLong(1)));
                        cur.moveToNext();
                        count++;
                        if (max > 0 && count >= max) {
                            break;
                        }
                    }
                    cur.close();
                }
                if (count < 1) {
                    int count2 = 0;
                    Cursor cur2 = this.db.query(TABLE_SEARCHTIP_NAME, new String[]{Downloads.COLUMN_TITLE, "visitdate"}, null, null, null, null, "visitdate desc");
                    cur2.moveToFirst();
                    while (!cur2.isAfterLast()) {
                        items.add(new SearchTipItem(cur2.getString(0), cur2.getLong(1)));
                        cur2.moveToNext();
                        count2++;
                        if (max > 0 && count2 >= max) {
                            break;
                        }
                    }
                    cur2.close();
                }
                closeDB();
            }
        } catch (Exception e) {
        }
        return items;
    }

    public boolean clear(long visitdate) {
        boolean ret = false;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("delete from ").append(TABLE_SEARCHTIP_NAME);
                sql.append(" where visitdate<").append(visitdate).append(";");
                this.db.execSQL(sql.toString());
                ret = true;
            } catch (Exception e) {
            }
            closeDB();
        }
        return ret;
    }

    public boolean clear() {
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("DROP TABLE IF EXISTS ").append(TABLE_SEARCHTIP_NAME);
                this.db.execSQL(sql.toString());
                this.itemCount = 0;
            } catch (Exception e) {
            }
            closeDB();
        }
        this.isTableCreated = false;
        return false;
    }

    public long add(String title, Long visitdate) {
        long id = 0;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put(Downloads.COLUMN_TITLE, title);
                values.put("visitdate", visitdate);
                id = this.db.insert(TABLE_SEARCHTIP_NAME, null, values);
                if (id > 0) {
                    this.itemCount++;
                }
            } catch (Exception e) {
            }
            closeDB();
        }
        clearIfNeed();
        return id;
    }

    public void updateOrAdd(String title, Long visitdate) {
        if (!SurfBrowserSettings.getInstance().isPrivacyBrowsing() && update(title, visitdate) <= 0) {
            add(title, visitdate);
        }
    }

    public int update(String title, Long visitdate) {
        int rows = 0;
        if (openDB()) {
            try {
                ContentValues values = new ContentValues();
                values.put("visitdate", visitdate);
                rows = this.db.update(TABLE_SEARCHTIP_NAME, values, " (title=?) ", new String[]{title});
            } catch (Exception e) {
            }
            closeDB();
        }
        return rows;
    }

    public int getCount() {
        int count = 0;
        if (openDB()) {
            try {
                StringBuffer sql = new StringBuffer(100);
                sql.append("select count(*) from ").append(TABLE_SEARCHTIP_NAME);
                Cursor cur = this.db.rawQuery(sql.toString(), null);
                if (cur != null) {
                    cur.moveToFirst();
                    if (!cur.isAfterLast()) {
                        count = cur.getInt(0);
                    }
                    cur.close();
                }
            } catch (Exception e) {
            }
            closeDB();
        }
        return count;
    }
}
