package com.cplatform.android.cmsurfclient.service.entry;

import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Tab {
    public String act = null;
    public String color = null;
    public String imgDown = null;
    public String imgUp = null;
    public boolean isDefault = false;
    public ArrayList<Label> labels = null;
    public String name = null;
    public String url = null;

    public Tab() {
    }

    public Tab(Element entry) {
        NodeList nlLabel;
        if (entry != null) {
            this.name = entry.getAttribute(QueryApList.Carriers.NAME);
            this.color = entry.getAttribute("color");
            this.imgUp = entry.getAttribute("img_up");
            this.imgDown = entry.getAttribute("img_down");
            this.act = entry.getAttribute("act");
            this.url = entry.getAttribute("url");
            this.isDefault = "1".equals(entry.getAttribute("default"));
            if (this.act != null && !"WEB".equals(this.act) && !WindowAdapter2.BLANK_URL.equals(this.act) && !"FAV".equals(this.act) && "NAV".equals(this.act) && (nlLabel = entry.getElementsByTagName("label")) != null && nlLabel.getLength() > 0) {
                this.labels = new ArrayList<>();
                int labelLen = nlLabel.getLength();
                for (int j = 0; j < labelLen; j++) {
                    this.labels.add(new Label((Element) nlLabel.item(j)));
                }
            }
        }
    }

    public String toString() {
        return "name:" + this.name + "\tcolor:" + this.color + "\timg_up:" + this.imgUp + "\timg_down:" + this.imgDown + "\tdefault:" + this.isDefault + "\tact:" + this.act + "\turl:" + this.url;
    }
}
