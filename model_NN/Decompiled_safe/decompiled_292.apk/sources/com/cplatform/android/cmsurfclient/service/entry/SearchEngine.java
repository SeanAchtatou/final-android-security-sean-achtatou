package com.cplatform.android.cmsurfclient.service.entry;

import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import org.w3c.dom.Element;

public class SearchEngine {
    public String encode;
    public String img;
    public boolean isDefault;
    public String name;
    public String url;

    public SearchEngine() {
    }

    public SearchEngine(Element entry) {
        if (entry != null) {
            this.name = entry.getAttribute(QueryApList.Carriers.NAME);
            this.img = entry.getAttribute("img");
            this.url = entry.getAttribute("url");
            this.encode = entry.getAttribute("enc");
            if (this.encode == null || WindowAdapter2.BLANK_URL.equals(this.encode)) {
                this.encode = "gbk";
            }
            this.isDefault = "1".equals(entry.getAttribute("default"));
        }
    }

    public String toString() {
        return "name:" + this.name + "\timg:" + this.img + "\tdefault:" + this.isDefault + "\turl:" + this.url + "\tencode:" + this.encode;
    }
}
