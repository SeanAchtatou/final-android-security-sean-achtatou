package com.cplatform.android.cmsurfclient.service.entry;

import java.util.ArrayList;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class SearchEngines {
    public ArrayList<SearchEngine> items = null;

    public SearchEngines() {
    }

    public SearchEngines(Element entry) {
        NodeList nlItem;
        if (entry != null && (nlItem = entry.getElementsByTagName("item")) != null && nlItem.getLength() > 0) {
            this.items = new ArrayList<>();
            int len = nlItem.getLength();
            for (int i = 0; i < len; i++) {
                this.items.add(new SearchEngine((Element) nlItem.item(i)));
            }
        }
    }
}
