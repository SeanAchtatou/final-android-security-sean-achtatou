package com.cplatform.android.cmsurfclient.download.provider;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.widget.RemoteViews;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.HashMap;

public class DownloadNotification {
    protected static final String LOGTAG = "download/provider/DownloadNotification";
    public static final String WHERE_COMPLETED = "status >= '200' AND visibility == '1'";
    public static final String WHERE_COMPLETED_ALL = "status >= '200'";
    public static final String WHERE_IN_QUEUE = "(status == '-100')";
    public static final String WHERE_RUNNING = "(status >= '100') AND (status <= '199') AND (visibility IS NULL OR visibility == '0' OR visibility == '1')";
    private Context mContext;
    private NotificationManager mNotificationMgr = ((NotificationManager) this.mContext.getSystemService("notification"));
    private HashMap<String, NotificationItem> mNotifications = new HashMap<>();

    class NotificationItem {
        String mDescription;
        int mId;
        String mPackageName;
        int mTitleCount = 0;
        String[] mTitles = new String[2];
        int mTotalCurrent = 0;
        int mTotalTotal = 0;

        /* access modifiers changed from: package-private */
        public void addItem(String title, int currentBytes, int totalBytes) {
            this.mTotalCurrent += currentBytes;
            if (totalBytes <= 0 || this.mTotalTotal == -1) {
                this.mTotalTotal = -1;
            } else {
                this.mTotalTotal += totalBytes;
            }
            if (this.mTitleCount < 2) {
                this.mTitles[this.mTitleCount] = title;
            }
            this.mTitleCount++;
        }

        NotificationItem() {
        }
    }

    public DownloadNotification(Context context) {
        this.mContext = context;
    }

    private String getDownloadingText(long totalBytes, long currentBytes) {
        if (totalBytes <= 0) {
            return WindowAdapter2.BLANK_URL;
        }
        StringBuilder sb = new StringBuilder();
        sb.append((100 * currentBytes) / totalBytes);
        sb.append('%');
        return sb.toString();
    }

    private void updateActiveNotification() {
        Cursor cursor = this.mContext.getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_NOTIFICATION_PACKAGE, Downloads.COLUMN_CONTROL}, WHERE_IN_QUEUE, null, QueryApList.Carriers._ID);
        if (cursor != null) {
            this.mNotifications.clear();
            HashMap<String, Integer> hashmap = new HashMap<>();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getInt(2) != 1) {
                    String notificationPackage = cursor.getString(1);
                    if (hashmap.containsKey(notificationPackage)) {
                        hashmap.put(notificationPackage, Integer.valueOf(((Integer) hashmap.get(notificationPackage)).intValue() + 1));
                    } else {
                        hashmap.put(notificationPackage, 1);
                    }
                }
                cursor.moveToNext();
            }
            cursor.close();
            Cursor cursor2 = this.mContext.getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, Downloads.COLUMN_DESCRIPTION, Downloads.COLUMN_NOTIFICATION_PACKAGE, Downloads.COLUMN_NOTIFICATION_CLASS, Downloads.COLUMN_CURRENT_BYTES, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_STATUS, Downloads._DATA, Downloads.COLUMN_CONTROL}, WHERE_RUNNING, null, QueryApList.Carriers._ID);
            if (cursor2 != null) {
                cursor2.moveToFirst();
                while (!cursor2.isAfterLast()) {
                    if (cursor2.getInt(9) == 1) {
                        String notificationPackage2 = cursor2.getString(3);
                        int totalBytes = cursor2.getInt(6);
                        int currentBytes = cursor2.getInt(5);
                        String title = cursor2.getString(1);
                        if (title == null || title.length() == 0) {
                            title = this.mContext.getResources().getString(R.string.download_unknown_title);
                        }
                        if (this.mNotifications.containsKey(notificationPackage2)) {
                            this.mNotifications.get(notificationPackage2).addItem(title, currentBytes, totalBytes);
                        } else {
                            NotificationItem notificationItem = new NotificationItem();
                            notificationItem.mId = cursor2.getInt(0);
                            notificationItem.mPackageName = notificationPackage2;
                            notificationItem.mDescription = cursor2.getString(2);
                            notificationItem.addItem(title, currentBytes, totalBytes);
                            this.mNotifications.put(notificationPackage2, notificationItem);
                        }
                    }
                    cursor2.moveToNext();
                }
                cursor2.close();
                for (NotificationItem item : this.mNotifications.values()) {
                    Notification notification = new Notification();
                    notification.icon = 17301633;
                    notification.flags |= 2;
                    RemoteViews remoteViews = new RemoteViews(DownloadSettings.getAppPackageName(), R.layout.download_status_bar_ongoing_event_progress_bar);
                    StringBuilder sb = new StringBuilder(item.mTitles[0]);
                    if (item.mTitleCount > 1) {
                        notification.number = item.mTitleCount;
                        sb.append(this.mContext.getString(R.string.download_notification_filename_extras, Integer.valueOf(item.mTitleCount - 1)));
                    } else {
                        remoteViews.setTextViewText(R.id.statusbar_downloader_description, item.mDescription);
                    }
                    remoteViews.setTextViewText(R.id.statusbar_downloader_title, sb);
                    remoteViews.setProgressBar(R.id.statusbar_downloader_progress_bar, item.mTotalTotal, item.mTotalCurrent, item.mTotalTotal == -1);
                    remoteViews.setTextViewText(R.id.statusbar_downloader_progress_text, getDownloadingText((long) item.mTotalTotal, (long) item.mTotalCurrent));
                    remoteViews.setImageViewResource(R.id.statusbar_downloader_appicon, 17301633);
                    notification.contentView = remoteViews;
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setClassName(DownloadSettings.getAppPackageName(), "com.cplatform.android.cmsurfclient.download.ui.DownloadHomeTabActivity");
                    intent.setFlags(268435456);
                    notification.contentIntent = PendingIntent.getActivity(this.mContext, 0, intent, 0);
                    this.mNotificationMgr.notify(item.mId, notification);
                }
                if (this.mNotifications.size() == 0) {
                    this.mNotificationMgr.cancelAll();
                }
            }
        }
    }

    private void updateCompletedNotification() {
        String text;
        Intent intent;
        Cursor cursor = this.mContext.getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, Downloads.COLUMN_DESCRIPTION, Downloads.COLUMN_NOTIFICATION_PACKAGE, Downloads.COLUMN_NOTIFICATION_CLASS, Downloads.COLUMN_CURRENT_BYTES, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_STATUS, Downloads._DATA, Downloads.COLUMN_LAST_MODIFICATION, Downloads.COLUMN_DESTINATION}, WHERE_COMPLETED, null, QueryApList.Carriers._ID);
        if (cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Notification notification = new Notification();
                notification.icon = 17301634;
                String title = cursor.getString(1);
                if (title == null || title.length() == 0) {
                    title = this.mContext.getResources().getString(R.string.download_unknown_title);
                }
                Uri uri = Uri.parse(DownloadSettings.getDownloadProviderContentUri() + "/" + cursor.getInt(0));
                if (Downloads.isStatusError(cursor.getInt(7))) {
                    text = this.mContext.getResources().getString(R.string.download_notification_download_failed);
                    intent = new Intent(Constants.ACTION_LIST);
                } else {
                    text = this.mContext.getResources().getString(R.string.download_notification_download_complete);
                    if (cursor.getInt(10) == 0) {
                        intent = new Intent(Constants.ACTION_OPEN);
                    } else {
                        intent = new Intent(Constants.ACTION_LIST);
                    }
                }
                intent.setClassName(DownloadSettings.getAppPackageName(), DownloadSettings.getDownloadReceiverClassName());
                intent.setData(uri);
                notification.setLatestEventInfo(this.mContext, title, text, PendingIntent.getBroadcast(this.mContext, 0, intent, 0));
                Intent intent2 = new Intent(Constants.ACTION_HIDE);
                intent2.setClassName(DownloadSettings.getAppPackageName(), DownloadSettings.getDownloadReceiverClassName());
                intent2.setData(uri);
                notification.deleteIntent = PendingIntent.getBroadcast(this.mContext, 0, intent2, 0);
                notification.when = cursor.getLong(9);
                this.mNotificationMgr.notify(cursor.getInt(0), notification);
                cursor.moveToNext();
            }
            cursor.close();
        }
    }

    public void cancel(int i) {
        this.mNotificationMgr.cancel(i);
    }

    public void cancelAll() {
        this.mNotificationMgr.cancelAll();
    }

    public void updateAll() {
        updateActiveNotification();
        updateCompletedNotification();
    }
}
