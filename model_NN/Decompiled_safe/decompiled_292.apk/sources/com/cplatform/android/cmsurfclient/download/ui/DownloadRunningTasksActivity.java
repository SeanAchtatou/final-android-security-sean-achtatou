package com.cplatform.android.cmsurfclient.download.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.download.provider.DownloadNotification;
import com.cplatform.android.cmsurfclient.download.provider.DownloadSettings;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;

public class DownloadRunningTasksActivity extends Activity implements MenuItem.OnMenuItemClickListener {
    private static final int CONTEXT_MENU_ID_DOWNLOAD_DELETE = 1;
    private TasksAdapter mContentAdapter;
    private ListView mContentView;
    /* access modifiers changed from: private */
    public boolean mHasRunnableTask;
    /* access modifiers changed from: private */
    public DownloadProviderContentObserver mObserver = new DownloadProviderContentObserver(new Handler());
    /* access modifiers changed from: private */
    public Cursor mTasksInQueue;

    class DownloadProviderContentObserver extends ContentObserver {
        public void onChange(boolean flag) {
            super.onChange(flag);
            DownloadRunningTasksActivity.this.refresh(true);
        }

        public DownloadProviderContentObserver(Handler handler) {
            super(handler);
        }
    }

    class TasksAdapter extends BaseAdapter {
        public int getCount() {
            return DownloadRunningTasksActivity.this.mTasksInQueue.getCount();
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return 0;
        }

        public View getView(int pos, View view, ViewGroup viewgroup) {
            DownloadRunningTasksActivity.this.mTasksInQueue.moveToPosition(pos);
            LinearLayout linearlayout = (LinearLayout) view;
            if (view == null) {
                linearlayout = (LinearLayout) LayoutInflater.from(DownloadRunningTasksActivity.this).inflate((int) R.layout.download_task_running_item, (ViewGroup) null);
            }
            String title = DownloadRunningTasksActivity.this.mTasksInQueue.getString(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_TITLE));
            if (TextUtils.isEmpty(title)) {
                title = "<Unknown>";
            }
            ((TextView) linearlayout.findViewById(R.id.download_task_item_title)).setText(title);
            long totalBytes = DownloadRunningTasksActivity.this.mTasksInQueue.getLong(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_TOTAL_BYTES));
            long currentBytes = DownloadRunningTasksActivity.this.mTasksInQueue.getLong(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_CURRENT_BYTES));
            long currentElapsedTime = DownloadRunningTasksActivity.this.mTasksInQueue.getLong(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_CURRENT_ELAPSED_TIME));
            int status = DownloadRunningTasksActivity.this.mTasksInQueue.getInt(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_STATUS));
            int control = DownloadRunningTasksActivity.this.mTasksInQueue.getInt(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_CONTROL));
            int percent = 0;
            if (totalBytes > 0 && currentBytes > 0) {
                percent = Long.valueOf((100 * currentBytes) / totalBytes).intValue();
            }
            ImageView imageview = (ImageView) linearlayout.findViewById(R.id.download_task_item_action_image);
            boolean flag = true;
            if (Downloads.isStatusSuspended(status) || control != 0) {
                ((TextView) linearlayout.findViewById(R.id.download_task_item_status_info)).setText(R.string.download_status_suspended);
                ((TextView) linearlayout.findViewById(R.id.download_task_item_action_text)).setText(R.string.download_action_start);
                imageview.setImageResource(R.drawable.download_icon_stopping);
            } else if (status == -100 || status == 190) {
                ((TextView) linearlayout.findViewById(R.id.download_task_item_status_info)).setText(R.string.download_status_pending);
                ((TextView) linearlayout.findViewById(R.id.download_task_item_action_text)).setText(R.string.download_action_waiting);
                imageview.setImageResource(R.drawable.download_icon_waiting);
                flag = false;
            } else {
                StringBuilder stringbuilder = new StringBuilder();
                stringbuilder.append(DownloadRunningTasksActivity.this.getText(R.string.download_status_downloading));
                stringbuilder.append(' ');
                stringbuilder.append(percent);
                stringbuilder.append("% ");
                if (totalBytes > 0 && currentBytes > 0) {
                    stringbuilder.append("(");
                    stringbuilder.append(Formatter.formatFileSize(DownloadRunningTasksActivity.this, currentBytes));
                    stringbuilder.append("/");
                    stringbuilder.append(Formatter.formatFileSize(DownloadRunningTasksActivity.this, totalBytes));
                    stringbuilder.append(")");
                } else if (totalBytes > 0) {
                    stringbuilder.append(Formatter.formatFileSize(DownloadRunningTasksActivity.this, totalBytes));
                }
                if (currentBytes > 0 && currentElapsedTime > 1000) {
                    stringbuilder.append(' ');
                    stringbuilder.append(Formatter.formatFileSize(DownloadRunningTasksActivity.this, currentBytes / (currentElapsedTime / 1000)));
                    stringbuilder.append("/s");
                }
                ((TextView) linearlayout.findViewById(R.id.download_task_item_status_info)).setText(stringbuilder);
                ((TextView) linearlayout.findViewById(R.id.download_task_item_action_text)).setText(R.string.download_action_pause);
                imageview.setImageResource(R.drawable.download_icon_playing);
            }
            if (flag) {
                final int i = status;
                final int i2 = control;
                final int i3 = DownloadRunningTasksActivity.this.mTasksInQueue.getInt(DownloadRunningTasksActivity.this.mTasksInQueue.getColumnIndex(QueryApList.Carriers._ID));
                linearlayout.findViewById(R.id.download_task_item_action).setOnClickListener(new View.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
                      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
                    public void onClick(View view2) {
                        if (Downloads.isStatusSuspended(i) || i2 != 0) {
                            ContentValues contentvalues = new ContentValues();
                            contentvalues.put(Downloads.COLUMN_CONTROL, (Integer) 0);
                            DownloadRunningTasksActivity.this.getContentResolver().update(Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(i3)), contentvalues, null, null);
                            return;
                        }
                        ContentValues contentvalues1 = new ContentValues();
                        contentvalues1.put(Downloads.COLUMN_CONTROL, (Integer) 1);
                        DownloadRunningTasksActivity.this.getContentResolver().update(Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(i3)), contentvalues1, null, null);
                    }
                });
            } else {
                linearlayout.findViewById(R.id.download_task_item_action).setOnClickListener(null);
            }
            ((ProgressBar) linearlayout.findViewById(R.id.download_task_item_progress)).setProgress(percent);
            return linearlayout;
        }

        private TasksAdapter() {
        }

        TasksAdapter(DownloadRunningTasksActivity downloadRunningTasksActivity, View.OnClickListener _pcls1) {
            this();
        }
    }

    private void comfirmAndDeleteAllTasks() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.app_name);
        builder.setMessage((int) R.string.download_delete_all_running_tasks);
        builder.setCancelable(false).setPositiveButton((int) R.string.download_save, new _cls4()).setNegativeButton((int) R.string.download_do_not_save, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialoginterface, int i) {
                dialoginterface.dismiss();
            }
        });
        builder.create().show();
    }

    /* access modifiers changed from: private */
    public void doDeleteAllTasks() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setIcon((int) R.drawable.icon);
        dialog.setMessage(getResources().getString(R.string.download_info_start_deleted_tasks));
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.show();
        getContentResolver().unregisterContentObserver(this.mObserver);
        new AsyncTask<Void, Integer, Integer>() {
            /* access modifiers changed from: protected */
            public Integer doInBackground(Void[] avoid) {
                Cursor cursor = DownloadRunningTasksActivity.this.getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_APP_DATA, Downloads._DATA}, String.format("(%s) OR (%s)", DownloadNotification.WHERE_IN_QUEUE, DownloadNotification.WHERE_RUNNING), null, QueryApList.Carriers._ID);
                if (cursor == null) {
                    return -1;
                }
                int i = 0;
                try {
                    cursor.moveToLast();
                    while (!cursor.isBeforeFirst() && !DownloadRunningTasksActivity.this.isFinishing()) {
                        DownloadHelper.deleteDownloadTask(DownloadRunningTasksActivity.this, Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), cursor.getString(cursor.getColumnIndex(QueryApList.Carriers._ID))), cursor.getString(cursor.getColumnIndex(Downloads.COLUMN_APP_DATA)), cursor.getString(cursor.getColumnIndex(Downloads._DATA)));
                        i++;
                        publishProgress(Integer.valueOf(i));
                        cursor.moveToPrevious();
                    }
                    cursor.close();
                    cursor.close();
                    return 0;
                } catch (Exception e) {
                    cursor.close();
                    return 0;
                }
            }

            /* access modifiers changed from: protected */
            public void onPostExecute(Integer integer) {
                if (!DownloadRunningTasksActivity.this.isFinishing()) {
                    DownloadRunningTasksActivity.this.getContentResolver().registerContentObserver(DownloadSettings.getDownloadProviderContentUri(), true, DownloadRunningTasksActivity.this.mObserver);
                    DownloadRunningTasksActivity.this.refresh(true);
                    dialog.dismiss();
                    if (integer.intValue() > 0) {
                        Toast.makeText(DownloadRunningTasksActivity.this, String.format(DownloadRunningTasksActivity.this.getResources().getText(R.string.download_info_count_deleted_tasks).toString(), integer), 0).show();
                    }
                }
            }

            /* access modifiers changed from: protected */
            public void onProgressUpdate(Integer[] ainteger) {
                if (!DownloadRunningTasksActivity.this.isFinishing()) {
                    super.onProgressUpdate((Object[]) ainteger);
                    dialog.setMessage(String.format(DownloadRunningTasksActivity.this.getResources().getText(R.string.download_info_count_deleted_tasks).toString(), ainteger[0]));
                }
            }
        }.execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void refresh(boolean autoChangeTab) {
        if (!isFinishing()) {
            this.mTasksInQueue.requery();
            if (!isFinishing()) {
                updateActionButtonStatus();
                this.mContentAdapter.notifyDataSetChanged();
            }
            if (autoChangeTab && this.mTasksInQueue.getCount() <= 0) {
                Activity activity = getParent();
                if (activity instanceof DownloadHomeTabActivity) {
                    TabHost tabHost = ((DownloadHomeTabActivity) activity).getTabHost();
                    if (tabHost.getCurrentTab() != 1) {
                        tabHost.setCurrentTab(1);
                    }
                }
            }
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 7 */
    private void updateActionButtonStatus() {
        boolean z;
        if (this.mTasksInQueue.getCount() == 0) {
            findViewById(R.id.downloadtasks_operate_button).setVisibility(4);
            findViewById(R.id.downloadtasks_operate_button).setEnabled(false);
            ((TextView) findViewById(R.id.downloadtasks_operate_button_text)).setText((int) R.string.download_stop_all_downloads_tasks);
            return;
        }
        this.mHasRunnableTask = false;
        findViewById(R.id.downloadtasks_operate_button).setVisibility(0);
        findViewById(R.id.downloadtasks_operate_button).setEnabled(true);
        this.mTasksInQueue.moveToFirst();
        while (!this.mTasksInQueue.isAfterLast()) {
            if (this.mTasksInQueue.getInt(this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_CONTROL)) != 1) {
                z = true;
            } else {
                z = false;
            }
            this.mHasRunnableTask = z;
            if (!this.mHasRunnableTask) {
                break;
            }
            this.mTasksInQueue.moveToNext();
        }
        if (this.mHasRunnableTask) {
            ((ImageView) findViewById(R.id.downloadtasks_operate_button_icon)).setImageResource(R.drawable.download_icon_small_stop);
            ((TextView) findViewById(R.id.downloadtasks_operate_button_text)).setText((int) R.string.download_stop_all_downloads_tasks);
            return;
        }
        ((ImageView) findViewById(R.id.downloadtasks_operate_button_icon)).setImageResource(R.drawable.download_icon_small_play);
        ((TextView) findViewById(R.id.downloadtasks_operate_button_text)).setText((int) R.string.download_start_all_downloads_tasks);
    }

    public boolean onContextItemSelected(MenuItem menuitem) {
        int i = ((AdapterView.AdapterContextMenuInfo) menuitem.getMenuInfo()).position;
        if (i < 0) {
            return false;
        }
        if (i >= this.mTasksInQueue.getCount()) {
            return false;
        }
        this.mTasksInQueue.moveToPosition(i);
        final int id = this.mTasksInQueue.getInt(this.mTasksInQueue.getColumnIndex(QueryApList.Carriers._ID));
        final String metaUri = this.mTasksInQueue.getString(this.mTasksInQueue.getColumnIndex(Downloads.COLUMN_APP_DATA));
        final String localPath = this.mTasksInQueue.getString(this.mTasksInQueue.getColumnIndex(Downloads._DATA));
        switch (menuitem.getItemId()) {
            case 1:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle((int) R.string.app_name);
                builder.setMessage((int) R.string.download_delete_one_running_task);
                builder.setCancelable(true).setPositiveButton((int) R.string.download_save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                        DownloadHelper.deleteDownloadTask(DownloadRunningTasksActivity.this, Uri.withAppendedPath(DownloadSettings.getDownloadProviderContentUri(), String.valueOf(id)), metaUri, localPath);
                        Toast.makeText(DownloadRunningTasksActivity.this, String.format(DownloadRunningTasksActivity.this.getString(R.string.download_info_count_deleted_tasks), 1), 0).show();
                    }
                }).setNegativeButton((int) R.string.download_do_not_save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialoginterface, int i) {
                        dialoginterface.dismiss();
                    }
                });
                builder.create().show();
                return true;
            default:
                return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.downloadtasks_listview);
        this.mTasksInQueue = getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, Downloads.COLUMN_STATUS, Downloads.COLUMN_CONTROL, Downloads.COLUMN_CURRENT_BYTES, Downloads.COLUMN_CURRENT_ELAPSED_TIME, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_APP_DATA, Downloads._DATA}, String.format("(%s) OR (%s)", DownloadNotification.WHERE_IN_QUEUE, DownloadNotification.WHERE_RUNNING), null, QueryApList.Carriers._ID);
        this.mContentView = (ListView) findViewById(R.id.download_tasks_listview);
        this.mContentAdapter = new TasksAdapter(this, null);
        this.mContentView.setAdapter((ListAdapter) this.mContentAdapter);
        this.mContentView.setOnCreateContextMenuListener(this);
        findViewById(R.id.downloadtasks_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DownloadRunningTasksActivity.this.finish();
            }
        });
        updateActionButtonStatus();
        findViewById(R.id.downloadtasks_operate_button).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ContentValues contentvalues = new ContentValues();
                contentvalues.put(Downloads.COLUMN_CONTROL, Integer.valueOf(DownloadRunningTasksActivity.this.mHasRunnableTask ? 1 : 0));
                DownloadRunningTasksActivity.this.getContentResolver().update(DownloadSettings.getDownloadProviderContentUri(), contentvalues, String.format("(%s) OR (%s)", DownloadNotification.WHERE_IN_QUEUE, DownloadNotification.WHERE_RUNNING), null);
            }
        });
    }

    public void onCreateContextMenu(ContextMenu contextmenu, View view, ContextMenu.ContextMenuInfo contextmenuinfo) {
        if (view == this.mContentView) {
            contextmenu.setHeaderTitle((int) R.string.app_name);
            contextmenu.add(0, 1, 0, (int) R.string.download_delete_task);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.mTasksInQueue.close();
        super.onDestroy();
    }

    public boolean onMenuItemClick(MenuItem menuitem) {
        switch (menuitem.getItemId()) {
            case 1:
                comfirmAndDeleteAllTasks();
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(this.mObserver);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        getContentResolver().registerContentObserver(DownloadSettings.getDownloadProviderContentUri(), true, this.mObserver);
        refresh(false);
    }

    private class _cls4 implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialoginterface, int i) {
            dialoginterface.dismiss();
            DownloadRunningTasksActivity.this.doDeleteAllTasks();
        }

        _cls4() {
        }
    }

    private class _cls3 implements DialogInterface.OnClickListener {
        public void onClick(DialogInterface dialoginterface, int i) {
            dialoginterface.dismiss();
        }

        _cls3() {
        }
    }
}
