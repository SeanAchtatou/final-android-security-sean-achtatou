package com.cplatform.android.cmsurfclient.naviedit;

public class SearchEngineItem {
    public String encode;
    public String icon;
    public String title;
    public String url;

    public SearchEngineItem(String icon2, String title2, String url2, String encode2) {
        this.icon = icon2;
        this.title = title2;
        this.url = url2;
        this.encode = encode2;
    }
}
