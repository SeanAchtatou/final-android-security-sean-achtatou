package com.cplatform.android.cmsurfclient.download.util;

import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.io.File;
import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.ParseException;

public final class FileNameUtils {
    private static final String LOG_TAG = "common/FileNameUtils";

    public static final class UriAndFileName {
        public String Filename;
        public String Mimetype;
        public URI Uri;
    }

    public static final class WebAddressParserException extends ParseException {
        private static final long serialVersionUID = 8180187199011858163L;

        public WebAddressParserException(String s) {
            super(s);
        }

        public WebAddressParserException(String s, Exception exception) {
            super(s + " 璇︾粏" + exception);
        }
    }

    public static class WebAddress {
        static final int MATCH_GROUP_AUTHORITY = 2;
        static final int MATCH_GROUP_HOST = 3;
        static final int MATCH_GROUP_PATH = 5;
        static final int MATCH_GROUP_PORT = 4;
        static final int MATCH_GROUP_SCHEME = 1;
        public String mAuthInfo;
        public String mHost;
        public String mPath;
        public int mPort;
        public String mScheme;
        final Pattern sAddressPattern = Pattern.compile("(?:(http|HTTP|https|HTTPS|file|FILE)\\:\\/\\/)?(?:([-A-Za-z0-9$_.+!*'(),;?&=]+(?:\\:[-A-Za-z0-9$_.+!*'(),;?&=]+)?)@)?([-A-Za-z0-9%]+(?:\\.[-A-Za-z0-9%]+)*)?(?:\\:([0-9]+))?(\\/?.*)?");

        public String toString() {
            String port = WindowAdapter2.BLANK_URL;
            if ((this.mPort != 443 && this.mScheme.equals("https")) || (this.mPort != 80 && this.mScheme.equals("http"))) {
                port = ":" + this.mPort;
            }
            String auth = WindowAdapter2.BLANK_URL;
            if (this.mAuthInfo.length() > 0) {
                auth = this.mAuthInfo + "@";
            }
            return this.mScheme + "://" + auth + this.mHost + port + this.mPath;
        }

        public WebAddress(String s) throws ParseException {
            if (s == null) {
                throw new NullPointerException();
            }
            this.mScheme = WindowAdapter2.BLANK_URL;
            this.mHost = WindowAdapter2.BLANK_URL;
            this.mPort = -1;
            this.mPath = "/";
            this.mAuthInfo = WindowAdapter2.BLANK_URL;
            Matcher matcher = this.sAddressPattern.matcher(s);
            if (!matcher.matches()) {
                throw new ParseException("Bad address");
            }
            String tmp = matcher.group(1);
            if (tmp != null) {
                this.mScheme = tmp;
            }
            String tmp2 = matcher.group(2);
            if (tmp2 != null) {
                this.mAuthInfo = tmp2;
            }
            String tmp3 = matcher.group(3);
            if (tmp3 != null) {
                this.mHost = tmp3;
            }
            String tmp4 = matcher.group(4);
            if (tmp4 != null) {
                try {
                    this.mPort = Integer.parseInt(tmp4);
                } catch (NumberFormatException e) {
                    throw new ParseException("Bad port");
                }
            }
            String tmp5 = matcher.group(5);
            if (tmp5 != null && tmp5.length() > 0) {
                if (tmp5.charAt(0) == '/') {
                    this.mPath = tmp5;
                } else {
                    this.mPath = "/" + tmp5;
                }
            }
            if (this.mScheme.equals(WindowAdapter2.BLANK_URL)) {
                if (this.mPort == 443) {
                    this.mScheme = "https";
                } else {
                    this.mScheme = "http";
                }
            }
            if (this.mPort != -1) {
                return;
            }
            if (this.mScheme.equals("https")) {
                this.mPort = 443;
            } else {
                this.mPort = 80;
            }
        }
    }

    public static String getFileExtension(String s) {
        int i = 0;
        String s1 = new File(s).getName();
        int i2 = s1.lastIndexOf(46);
        if (i2 > 0 && i2 < s1.length() - 1) {
            i = s1.substring(i2 + 1).toLowerCase();
        }
        return (String) i;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Throwable] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getFileExtensionFromUrl(java.lang.String r7) {
        /*
            r6 = 0
            com.cplatform.android.cmsurfclient.download.util.FileNameUtils$UriAndFileName r3 = guessFileName(r7)
            if (r3 != 0) goto L_0x0009
            r5 = r6
        L_0x0008:
            return r5
        L_0x0009:
            java.lang.String r5 = r3.Filename
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 == 0) goto L_0x0013
            r5 = r6
            goto L_0x0008
        L_0x0013:
            r1 = 0
            java.lang.String r2 = r3.Filename     // Catch:{ WebAddressParserException -> 0x0032 }
            r5 = 46
            int r0 = r2.lastIndexOf(r5)     // Catch:{ WebAddressParserException -> 0x0032 }
            if (r0 <= 0) goto L_0x0030
            int r5 = r2.length()     // Catch:{ WebAddressParserException -> 0x0032 }
            r6 = 1
            int r5 = r5 - r6
            if (r0 >= r5) goto L_0x0030
            int r5 = r0 + 1
            java.lang.String r5 = r2.substring(r5)     // Catch:{ WebAddressParserException -> 0x0032 }
            java.lang.String r1 = r5.toLowerCase()     // Catch:{ WebAddressParserException -> 0x0032 }
        L_0x0030:
            r5 = r1
            goto L_0x0008
        L_0x0032:
            r5 = move-exception
            r4 = r5
            java.lang.String r5 = "common/FileNameUtils"
            java.lang.String r6 = "Ingoring error"
            android.util.Log.e(r5, r6, r4)
            java.lang.String r5 = ""
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.util.FileNameUtils.getFileExtensionFromUrl(java.lang.String):java.lang.String");
    }

    /* JADX INFO: Multiple debug info for r0v13 java.lang.String: [D('type' java.lang.String), D('l' int)] */
    /* JADX WARN: Type inference failed for: r9v4, types: [java.lang.Throwable, com.cplatform.android.cmsurfclient.download.util.FileNameUtils$WebAddressParserException] */
    public static UriAndFileName guessFileName(String url) throws WebAddressParserException {
        String tmp;
        int l;
        if (TextUtils.isEmpty(url)) {
            throw new IllegalArgumentException("url");
        }
        Log.v(LOG_TAG, "Starting download: " + url);
        UriAndFileName uriandfilename = new UriAndFileName();
        String url2 = Uri.decode(url);
        if (TextUtils.isEmpty(url2)) {
            return null;
        }
        String sharpTxt = null;
        String queryTxt = null;
        WebAddress webAddress = new WebAddress(new String(URLUtil.decode(url2.getBytes())));
        String path = webAddress.mPath;
        if (path.length() > 0) {
            int pos = path.lastIndexOf(35);
            if (pos != -1) {
                sharpTxt = path.substring(pos + 1);
                path = path.substring(0, pos);
            }
            int pos2 = path.lastIndexOf(63);
            if (pos2 != -1) {
                queryTxt = path.substring(path.lastIndexOf(63) + 1);
                path = path.substring(0, pos2);
            }
        }
        try {
            uriandfilename.Uri = new URI(webAddress.mScheme, TextUtils.isEmpty(webAddress.mAuthInfo) ? null : webAddress.mAuthInfo, webAddress.mHost, webAddress.mPort, path, queryTxt, sharpTxt);
            int pos3 = path.lastIndexOf(47);
            if (!(-1 == pos3 || -1 == (l = (tmp = path.substring(pos3 + 1)).lastIndexOf(46)))) {
                uriandfilename.Mimetype = MimeTypeMap.getSingleton().getMimeTypeFromExtension(tmp.substring(l + 1));
                uriandfilename.Filename = URLUtil.guessFileName(url2, "attachment; filename=\"" + tmp + "\"", uriandfilename.Mimetype);
            }
            return uriandfilename;
        } catch (Exception e) {
            Log.e(LOG_TAG, "Could not parse url for download: " + url2, e);
            throw new WebAddressParserException(e.getMessage(), e);
        }
    }
}
