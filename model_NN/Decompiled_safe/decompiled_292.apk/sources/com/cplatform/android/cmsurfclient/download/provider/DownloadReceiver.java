package com.cplatform.android.cmsurfclient.download.provider;

import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import com.cplatform.android.cmsurfclient.R;
import java.io.File;

public class DownloadReceiver extends BroadcastReceiver {
    static {
        DownloadSettings.ensureAppSettingsInitialized();
    }

    public DownloadNotification getDownloadNotifier(Context context) {
        return new DownloadNotification(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public void onReceive(Context context, Intent intent) {
        Cursor cursor1;
        Uri uri;
        if (intent.getAction().equals(Downloads.ACTION_DOWNLOAD_COMPLETED) && (uri = intent.getData()) != null) {
            Log.v(Constants.TAG, "DownloadReceiver download complete. Uri: " + uri);
        }
        if (intent.getAction().equals(Downloads.ACTION_DOWNLOAD_ALL_PAUSED_WIFI_DISCONNECTED)) {
            Log.v(Constants.TAG, "Receiver onAllPausedWifiDisconnected");
            int j = intent.getIntExtra(Downloads.EXTRA_PAUSED_TASKS_COUNT, 0);
            if (j > 0) {
                Toast.makeText(context, String.format(context.getText(R.string.download_info_count_paused_tasks_wifi_connected).toString(), Integer.valueOf(j)), 0).show();
            }
        } else if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Intent intent1 = new Intent();
            intent1.setClassName(DownloadSettings.getAppPackageName(), DownloadSettings.getDownloadServiceClassName());
            context.startService(intent1);
        } else if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            NetworkInfo networkinfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            if (networkinfo != null && networkinfo.isConnected()) {
                Intent intent5 = new Intent();
                intent5.setClassName(DownloadSettings.getAppPackageName(), DownloadSettings.getDownloadServiceClassName());
                context.startService(intent5);
            }
        } else if (intent.getAction().equals(Constants.ACTION_RETRY)) {
            Intent intent8 = new Intent();
            intent8.setClassName(DownloadSettings.getAppPackageName(), DownloadSettings.getDownloadServiceClassName());
            context.startService(intent8);
        } else if (intent.getAction().equals(Constants.ACTION_OPEN) || intent.getAction().equals(Constants.ACTION_LIST)) {
            Cursor cursor = context.getContentResolver().query(intent.getData(), null, null, null, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    if (Downloads.isStatusCompleted(cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_STATUS))) && cursor.getInt(cursor.getColumnIndexOrThrow(Downloads.COLUMN_VISIBILITY)) == 1) {
                        ContentValues contentvalues = new ContentValues();
                        contentvalues.put(Downloads.COLUMN_VISIBILITY, (Integer) 0);
                        context.getContentResolver().update(intent.getData(), contentvalues, null, null);
                    }
                    if (intent.getAction().equals(Constants.ACTION_OPEN)) {
                        String s16 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads._DATA));
                        String s17 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_MIME_TYPE));
                        Uri uri4 = Uri.parse(s16);
                        if (uri4.getScheme() == null) {
                            uri4 = Uri.fromFile(new File(s16));
                        }
                        Intent intent2 = new Intent("android.intent.action.VIEW");
                        intent2.setDataAndType(uri4, s17);
                        intent2.setFlags(268435456);
                        try {
                            context.startActivity(intent2);
                        } catch (ActivityNotFoundException activitynotfoundexception) {
                            Log.d(Constants.TAG, "no activity for " + s17, activitynotfoundexception);
                        }
                    } else {
                        String s25 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NOTIFICATION_PACKAGE));
                        String s26 = cursor.getString(cursor.getColumnIndexOrThrow(Downloads.COLUMN_NOTIFICATION_CLASS));
                        if (!(s25 == null || s26 == null)) {
                            Intent intent3 = new Intent(Downloads.ACTION_NOTIFICATION_CLICKED);
                            intent3.setClassName(s25, s26);
                            if (intent.getBooleanExtra("multiple", true)) {
                                intent3.setData(DownloadSettings.getDownloadProviderContentUri());
                            } else {
                                intent3.setData(intent.getData());
                            }
                            context.sendBroadcast(intent3);
                        }
                    }
                }
                cursor.close();
            }
            getDownloadNotifier(context).cancel((int) ContentUris.parseId(intent.getData()));
        } else if (intent.getAction().equals(Constants.ACTION_HIDE) && (cursor1 = context.getContentResolver().query(intent.getData(), null, null, null, null)) != null) {
            if (cursor1.moveToFirst() && Downloads.isStatusCompleted(cursor1.getInt(cursor1.getColumnIndexOrThrow(Downloads.COLUMN_STATUS))) && cursor1.getInt(cursor1.getColumnIndexOrThrow(Downloads.COLUMN_VISIBILITY)) == 1) {
                ContentValues contentvalues3 = new ContentValues();
                contentvalues3.put(Downloads.COLUMN_VISIBILITY, (Integer) 0);
                context.getContentResolver().update(intent.getData(), contentvalues3, null, null);
            }
            cursor1.close();
        }
    }
}
