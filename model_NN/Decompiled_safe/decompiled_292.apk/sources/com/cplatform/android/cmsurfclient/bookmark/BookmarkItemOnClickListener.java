package com.cplatform.android.cmsurfclient.bookmark;

import android.app.Activity;
import android.view.View;

public class BookmarkItemOnClickListener implements View.OnClickListener {
    public Activity mActivity;
    public BookmarkItem mItem;

    public BookmarkItemOnClickListener(Activity activity, BookmarkItem item) {
        this.mActivity = activity;
        this.mItem = item;
    }

    public void onClick(View v) {
        ((BookmarkActivity) this.mActivity).onClick(this.mItem);
    }
}
