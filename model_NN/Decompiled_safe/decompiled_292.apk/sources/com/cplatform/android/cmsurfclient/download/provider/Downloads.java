package com.cplatform.android.cmsurfclient.download.provider;

import android.provider.BaseColumns;

public final class Downloads implements BaseColumns {
    public static final String ACTION_DOWNLOAD_ALL_PAUSED_WIFI_DISCONNECTED = "android.intent.action.DOWNLOAD_ALL_PAUSED_WIFI_DISCONNECTED";
    public static final String ACTION_DOWNLOAD_COMPLETED = "android.intent.action.DOWNLOAD_COMPLETED";
    public static final String ACTION_NOTIFICATION_CLICKED = "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED";
    public static final String COLUMN_APPOINT_NAME = "appointname";
    public static final String COLUMN_APP_DATA = "entity";
    public static final String COLUMN_CONTROL = "control";
    public static final String COLUMN_COOKIE_DATA = "cookiedata";
    public static final String COLUMN_CURRENT_BYTES = "current_bytes";
    public static final String COLUMN_CURRENT_ELAPSED_TIME = "current_elapsed_time";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DESTINATION = "destination";
    public static final String COLUMN_FILE_NAME_HINT = "hint";
    public static final String COLUMN_IF_RANGE_ID = "if_range_id";
    public static final String COLUMN_LAST_MODIFICATION = "lastmod";
    public static final String COLUMN_MIME_TYPE = "mimetype";
    public static final String COLUMN_NOTIFICATION_CLASS = "notificationclass";
    public static final String COLUMN_NOTIFICATION_EXTRAS = "notificationextras";
    public static final String COLUMN_NOTIFICATION_PACKAGE = "notificationpackage";
    public static final String COLUMN_NO_INTEGRITY = "no_integrity";
    public static final String COLUMN_OTHER_UID = "otheruid";
    public static final String COLUMN_REFERER = "referer";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_SUB_DIRECTORY = "subdirectory";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_TOTAL_BYTES = "total_bytes";
    public static final String COLUMN_URI = "uri";
    public static final String COLUMN_USER_AGENT = "useragent";
    public static final String COLUMN_VISIBILITY = "visibility";
    public static final int CONTROL_PAUSED = 1;
    public static final int CONTROL_RUN = 0;
    public static final int DESTINATION_CACHE_PARTITION = 1;
    public static final int DESTINATION_CACHE_PARTITION_NOROAMING = 3;
    public static final int DESTINATION_CACHE_PARTITION_PURGEABLE = 2;
    public static final int DESTINATION_EXTERNAL = 0;
    public static final String EXTRA_PAUSED_TASKS_COUNT = "download/provider/paused_task_count";
    public static final int STATUS_BAD_REQUEST = 400;
    public static final int STATUS_CANCELED = 490;
    public static final int STATUS_FILE_ERROR = 492;
    public static final int STATUS_HTTP_DATA_ERROR = 495;
    public static final int STATUS_HTTP_EXCEPTION = 496;
    public static final int STATUS_IN_QUEUE = 156;
    public static final int STATUS_LENGTH_REQUIRED = 411;
    public static final int STATUS_NOT_ACCEPTABLE = 406;
    public static final int STATUS_PENDING = 190;
    public static final int STATUS_PENDING_PAUSED = 191;
    public static final int STATUS_PRECONDITION_FAILED = 412;
    public static final int STATUS_RUNNING = 192;
    public static final int STATUS_RUNNING_PAUSED = 193;
    public static final int STATUS_SUCCESS = 200;
    public static final int STATUS_TOO_MANY_REDIRECTS = 497;
    public static final int STATUS_UNHANDLED_HTTP_CODE = 494;
    public static final int STATUS_UNHANDLED_REDIRECT = 493;
    public static final int STATUS_UNKNOWN_ERROR = 491;
    public static final int VISIBILITY_HIDDEN = 2;
    public static final int VISIBILITY_VISIBLE = 0;
    public static final int VISIBILITY_VISIBLE_NOTIFY_COMPLETED = 1;
    public static final String _DATA = "_data";

    private Downloads() {
    }

    public static boolean isStatusClientError(int i) {
        if (i < 400) {
            return false;
        }
        if (i >= 500) {
            return false;
        }
        return true;
    }

    public static boolean isStatusCompleted(int i) {
        if (i < 200 || i >= 300) {
            return i >= 400 && i < 600;
        }
        return true;
    }

    public static boolean isStatusError(int i) {
        if (i < 400) {
            return false;
        }
        if (i >= 600) {
            return false;
        }
        return true;
    }

    public static boolean isStatusInformational(int i) {
        if (i < 100) {
            return false;
        }
        if (i >= 200) {
            return false;
        }
        return true;
    }

    public static boolean isStatusServerError(int i) {
        if (i < 500) {
            return false;
        }
        if (i >= 600) {
            return false;
        }
        return true;
    }

    public static boolean isStatusSuccess(int i) {
        return i >= 200 && i < 300;
    }

    public static boolean isStatusSuspended(int i) {
        return i == 191 || i == 193;
    }
}
