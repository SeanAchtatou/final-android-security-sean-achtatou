package com.cplatform.android.cmsurfclient.utils;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.TreeSet;

public class PreferenceUtil {
    public static int getValue(Context context, String node, String key, int defaultValue) {
        return context.getSharedPreferences(node, 0).getInt(key, defaultValue);
    }

    public static String getValue(Context context, String node, String key, String defaultValue) {
        return context.getSharedPreferences(node, 0).getString(key, defaultValue);
    }

    public static boolean getValue(Context context, String node, String key, boolean defaultValue) {
        return context.getSharedPreferences(node, 0).getBoolean(key, defaultValue);
    }

    public static void saveValue(Context context, String node, String key, String value) {
        SharedPreferences.Editor sp = context.getSharedPreferences(node, 0).edit();
        sp.putString(key, value);
        sp.commit();
    }

    public static void saveValue(Context context, String node, String key, boolean value) {
        SharedPreferences.Editor sp = context.getSharedPreferences(node, 0).edit();
        sp.putBoolean(key, value);
        sp.commit();
    }

    public static void saveValue(Context context, String node, String key, int value) {
        SharedPreferences.Editor sp = context.getSharedPreferences(node, 0).edit();
        sp.putInt(key, value);
        sp.commit();
    }

    public static boolean isItemViewType(int param, TreeSet<Integer> treeSet) {
        if (treeSet.contains(Integer.valueOf(param))) {
            return true;
        }
        return false;
    }
}
