package com.cplatform.android.cmsurfclient.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.webkit.CookieManager;
import android.webkit.WebIconDatabase;
import android.webkit.WebSettings;
import android.webkit.WebViewDatabase;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import java.util.HashMap;
import java.util.Observable;

public class SurfBrowserSettings extends Observable {
    public static final String PREF_ADAPTER_FONTSIZE = "adapter_fontsize";
    public static final String PREF_ADAPTER_IMAGEQUALITY = "adapter_imagequality";
    public static final String PREF_ADAPTER_IMAGESIZE = "adapter_imagesize";
    public static final String PREF_ADAPTER_PAGESIZE = "adapter_pagesize";
    public static final String PREF_CLEAR_CACHE = "privacy_clear_cache";
    public static final String PREF_CLEAR_COOKIES = "privacy_clear_cookies";
    public static final String PREF_CLEAR_FORMDATA = "privacy_clear_formdata";
    public static final String PREF_CLEAR_HISTORY = "privacy_clear_history";
    public static final String PREF_CLEAR_PASSWORDS = "privacy_clear_passwords";
    public static final String PREF_CWMAP_PREFERRED = "network_cmwap_preferred";
    public static final String PREF_FONT_SIZE = "browser_font_size";
    public static final String PREF_RESET_DEFAULTS = "others_reset_defaults";
    private static int mAdapterFontSize = 1;
    private static int mAdapterImageQuality = 2;
    private static int mAdapterImageSize = 2;
    private static int mAdapterPageSize = 1;
    /* access modifiers changed from: private */
    public static WebSettings.TextSize mFontSize = WebSettings.TextSize.NORMAL;
    private static boolean mIsAutoFullScreen = true;
    private static boolean mIsCMWAPPreferred = true;
    private static boolean mIsFullScreen = false;
    /* access modifiers changed from: private */
    public static boolean mIsLoadPicture = true;
    private static boolean mIsPreload = true;
    private static boolean mIsPrivacyBrowsing = false;
    private static boolean mIsRememberZoomLevel = true;
    private static boolean mIsShowPageControls = false;
    private static boolean mIsShowZoomControls = false;
    private static boolean mIsUseVolumeKeys = true;
    private static boolean mIsWWW2WAPInMobile = true;
    private static boolean mIsWWW2WAPInWifi = false;
    private static SurfBrowserSettings sSingleton;
    private SurfManagerActivity mSurfMgr = null;
    private HashMap<WebSettings, Observer> mWebSettingsToObservers = new HashMap<>();

    public static class Observer implements java.util.Observer {
        private WebSettings mSettings;

        Observer(WebSettings w) {
            this.mSettings = w;
        }

        public void update(Observable o, Object arg) {
            WebSettings s = this.mSettings;
            s.setTextSize(SurfBrowserSettings.mFontSize);
            s.setLoadsImagesAutomatically(SurfBrowserSettings.mIsLoadPicture);
            ((SurfBrowserSettings) o).updateSurfMgrSettings();
        }
    }

    public void loadFromDb(Context ctx) {
        syncSharedPreferences(PreferenceManager.getDefaultSharedPreferences(ctx));
    }

    /* access modifiers changed from: package-private */
    public void syncSharedPreferences(SharedPreferences p) {
        mFontSize = WebSettings.TextSize.valueOf(p.getString(PREF_FONT_SIZE, mFontSize.name()));
        mIsLoadPicture = p.getBoolean("browser_load_picture", mIsLoadPicture);
        mIsShowZoomControls = p.getBoolean("browser_show_zoomcontrols", mIsShowZoomControls);
        mIsShowPageControls = p.getBoolean("browser_show_pagecontrols", mIsShowPageControls);
        mIsUseVolumeKeys = p.getBoolean("browser_use_volumekeys", mIsUseVolumeKeys);
        mIsWWW2WAPInMobile = p.getBoolean("browser_www2wap_in_mobile", mIsWWW2WAPInMobile);
        mIsWWW2WAPInWifi = p.getBoolean("browser_www2wap_in_wifi", mIsWWW2WAPInWifi);
        mIsAutoFullScreen = p.getBoolean("browser_auto_fullscreen", mIsAutoFullScreen);
        mIsRememberZoomLevel = p.getBoolean("browser_remember_zoomlevel", mIsRememberZoomLevel);
        mIsPrivacyBrowsing = p.getBoolean("browser_privacy_browsing", mIsPrivacyBrowsing);
        mIsPreload = p.getBoolean("browser_preload", mIsPreload);
        mIsCMWAPPreferred = p.getBoolean(PREF_CWMAP_PREFERRED, mIsCMWAPPreferred);
        mAdapterPageSize = Integer.parseInt(p.getString(PREF_ADAPTER_PAGESIZE, Integer.toString(mAdapterPageSize)));
        mAdapterImageQuality = Integer.parseInt(p.getString(PREF_ADAPTER_IMAGEQUALITY, Integer.toString(mAdapterImageQuality)));
        mAdapterImageSize = Integer.parseInt(p.getString(PREF_ADAPTER_IMAGESIZE, Integer.toString(mAdapterImageSize)));
        mAdapterFontSize = Integer.parseInt(p.getString(PREF_ADAPTER_FONTSIZE, Integer.toString(mAdapterFontSize)));
        update();
    }

    public boolean isFullScreen() {
        return mIsFullScreen;
    }

    public WebSettings.TextSize getFontSize() {
        return mFontSize;
    }

    public boolean isLoadPicture() {
        return mIsLoadPicture;
    }

    public boolean isShowZoomControls() {
        return mIsShowZoomControls;
    }

    public boolean isShowPageControls() {
        return mIsShowPageControls;
    }

    public boolean isUseVolumeKeys() {
        return mIsUseVolumeKeys;
    }

    public boolean isWWW2WAPInMobile() {
        return mIsWWW2WAPInMobile;
    }

    public boolean isWWW2WAPInWifi() {
        return mIsWWW2WAPInWifi;
    }

    public boolean isCMWAPPreferred() {
        return mIsCMWAPPreferred;
    }

    public boolean isAutoFullScreen() {
        return mIsAutoFullScreen;
    }

    public boolean isRememberZoomLevel() {
        return mIsRememberZoomLevel;
    }

    public boolean isPrivacyBrowsing() {
        return mIsPrivacyBrowsing;
    }

    public boolean isPreload() {
        return mIsPreload;
    }

    public String getAdapterCookies() {
        return ((("X-Msb-Option=" + "ps:" + mAdapterPageSize + "|") + "iq:" + mAdapterImageQuality + "|") + "is:" + mAdapterImageSize + "|") + "fs:" + mAdapterFontSize;
    }

    public Observer addObserver(WebSettings s) {
        Observer old = this.mWebSettingsToObservers.get(s);
        if (old != null) {
            super.deleteObserver(old);
        }
        Observer o = new Observer(s);
        this.mWebSettingsToObservers.put(s, o);
        super.addObserver(o);
        return o;
    }

    public void deleteObserver(WebSettings s) {
        Observer o = this.mWebSettingsToObservers.get(s);
        if (o != null) {
            this.mWebSettingsToObservers.remove(s);
            super.deleteObserver(o);
        }
    }

    public static SurfBrowserSettings getInstance() {
        if (sSingleton == null) {
            sSingleton = new SurfBrowserSettings();
        }
        return sSingleton;
    }

    public void setSurfManager(SurfManagerActivity surfMgr) {
        this.mSurfMgr = surfMgr;
        updateSurfMgrSettings();
    }

    public void update() {
        setChanged();
        notifyObservers();
    }

    /* access modifiers changed from: package-private */
    public void clearCache(Context context) {
        WebIconDatabase.getInstance().removeAllIcons();
        if (this.mSurfMgr != null) {
            this.mSurfMgr.clearCache(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void clearCookies(Context context) {
        CookieManager.getInstance().removeAllCookie();
    }

    /* access modifiers changed from: package-private */
    public void clearHistory(Context context) {
        if (this.mSurfMgr != null) {
            this.mSurfMgr.clearHistory();
        }
    }

    /* access modifiers changed from: package-private */
    public void clearFormData(Context context) {
        WebViewDatabase.getInstance(context).clearFormData();
        if (this.mSurfMgr != null) {
            this.mSurfMgr.clearFormData();
        }
    }

    /* access modifiers changed from: package-private */
    public void clearPasswords(Context context) {
        WebViewDatabase db = WebViewDatabase.getInstance(context);
        db.clearUsernamePassword();
        db.clearHttpAuthUsernamePassword();
    }

    /* access modifiers changed from: private */
    public void updateSurfMgrSettings() {
    }

    /* access modifiers changed from: package-private */
    public void resetDefaultPreferences(Context ctx) {
        reset();
        PreferenceManager.getDefaultSharedPreferences(ctx).edit().clear().commit();
        PreferenceManager.setDefaultValues(ctx, R.xml.preferences, true);
    }

    private SurfBrowserSettings() {
        reset();
    }

    private void reset() {
        mFontSize = WebSettings.TextSize.NORMAL;
        mIsLoadPicture = true;
        mIsShowZoomControls = false;
        mIsShowPageControls = false;
        mIsUseVolumeKeys = true;
        mIsWWW2WAPInMobile = true;
        mIsWWW2WAPInWifi = false;
        mIsAutoFullScreen = true;
        mIsRememberZoomLevel = true;
        mIsPrivacyBrowsing = false;
        mIsPreload = true;
        mIsCMWAPPreferred = true;
        mAdapterPageSize = 1;
        mAdapterImageQuality = 2;
        mAdapterImageSize = 2;
        mAdapterFontSize = 1;
    }
}
