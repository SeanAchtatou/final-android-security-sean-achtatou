package com.cplatform.android.cmsurfclient.searchtip;

public class SearchTipItem {
    public long date;
    public String title;

    public SearchTipItem(String title2, long date2) {
        this.title = title2;
        this.date = date2;
    }
}
