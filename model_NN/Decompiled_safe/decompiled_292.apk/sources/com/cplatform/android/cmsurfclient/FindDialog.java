package com.cplatform.android.cmsurfclient;

import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import com.cplatform.android.utils.ReflectUtil;

class FindDialog extends Dialog implements TextWatcher {
    private SurfManagerActivity mBrowserActivity;
    private ImageButton mBtnClose;
    private ImageButton mBtnNext;
    private ImageButton mBtnPrev;
    private EditText mEditText;
    private View.OnClickListener mFindCancelListener = new View.OnClickListener() {
        public void onClick(View v) {
            FindDialog.this.dismiss();
        }
    };
    private View.OnClickListener mFindNextListener = new View.OnClickListener() {
        public void onClick(View v) {
            FindDialog.this.findNext(true);
        }
    };
    private View.OnClickListener mFindPreviousListener = new View.OnClickListener() {
        public void onClick(View v) {
            FindDialog.this.findNext(false);
        }
    };
    private TextView mMatches;
    private WebView mWebView;

    private void hideSoftInput() {
        ((InputMethodManager) this.mBrowserActivity.getSystemService("input_method")).hideSoftInputFromWindow(this.mEditText.getWindowToken(), 0);
    }

    private void disableButtons() {
        this.mBtnPrev.setEnabled(false);
        this.mBtnNext.setEnabled(false);
        this.mBtnPrev.setFocusable(false);
        this.mBtnNext.setFocusable(false);
    }

    /* access modifiers changed from: package-private */
    public void setWebView(WebView webview) {
        this.mWebView = webview;
    }

    FindDialog(SurfManagerActivity context) {
        super(context, R.style.FindDialogTheme);
        this.mBrowserActivity = context;
        setCanceledOnTouchOutside(true);
    }

    /* access modifiers changed from: package-private */
    public void onConfigurationChanged(Configuration newConfig) {
        this.mEditText.getText().clear();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window theWindow = getWindow();
        theWindow.setGravity(87);
        setContentView((int) R.layout.browser_find);
        theWindow.setLayout(-1, -2);
        this.mEditText = (EditText) findViewById(R.id.edit);
        this.mBtnNext = (ImageButton) findViewById(R.id.next);
        this.mBtnNext.setOnClickListener(this.mFindNextListener);
        this.mBtnPrev = (ImageButton) findViewById(R.id.previous);
        this.mBtnPrev.setOnClickListener(this.mFindPreviousListener);
        this.mBtnClose = (ImageButton) findViewById(R.id.done);
        this.mBtnClose.setOnClickListener(this.mFindCancelListener);
        this.mMatches = (TextView) findViewById(R.id.matches);
        this.mMatches.setVisibility(4);
        disableButtons();
        theWindow.setSoftInputMode(4);
    }

    public void dismiss() {
        super.dismiss();
        this.mWebView.clearMatches();
        ReflectUtil.invokeMethodNoParams(WebView.class, this.mWebView, "notifyFindDialogDismissed");
        this.mBrowserActivity.closeFind();
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        int code = event.getKeyCode();
        boolean up = event.getAction() == 1;
        switch (code) {
            case SurfManagerActivity.CONTEXTMENU_HOMEVIEW_SHARELINK /*23*/:
            case 66:
                if (this.mEditText.hasFocus()) {
                    if (up) {
                        findNext(true);
                    }
                    return true;
                }
                break;
        }
        return super.dispatchKeyEvent(event);
    }

    /* access modifiers changed from: private */
    public void findNext(boolean isFindNext) {
        if (this.mWebView != null) {
            this.mWebView.findNext(isFindNext);
            hideSoftInput();
        }
    }

    public void show() {
        super.show();
        this.mEditText.requestFocus();
        this.mEditText.setText(WindowAdapter2.BLANK_URL);
        Spannable span = this.mEditText.getText();
        span.setSpan(this, 0, span.length(), 18);
        this.mMatches.setVisibility(4);
        setMatchesFound(0);
        disableButtons();
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (this.mWebView != null) {
            CharSequence find = this.mEditText.getText();
            if (find.length() == 0) {
                disableButtons();
                this.mWebView.clearMatches();
                this.mMatches.setVisibility(4);
                return;
            }
            this.mMatches.setVisibility(0);
            Class[] aClass = {Integer.TYPE};
            ReflectUtil.invokeDeclaredMethod(WebView.class, this.mWebView, "setFindDialogHeight", new Object[]{Integer.valueOf(getWindow().getDecorView().getHeight())}, aClass);
            int found = this.mWebView.findAll(find.toString());
            setMatchesFound(found);
            if (found < 2) {
                disableButtons();
                if (found == 0) {
                    setMatchesFound(0);
                    return;
                }
                return;
            }
            this.mBtnPrev.setFocusable(true);
            this.mBtnNext.setFocusable(true);
            this.mBtnPrev.setEnabled(true);
            this.mBtnNext.setEnabled(true);
        }
    }

    private void setMatchesFound(int found) {
        StringBuilder template = new StringBuilder();
        template.append(found);
        template.append(this.mBrowserActivity.getResources().getString(R.string.matches_found));
        this.mMatches.setText(template);
    }

    public void afterTextChanged(Editable s) {
    }
}
