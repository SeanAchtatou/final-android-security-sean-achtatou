package com.cplatform.android.cmsurfclient.download.provider;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

public class DownloadSettings {
    public static final String PREF_PAUSE_DOWNLOAD_ON_WIFI_DISCONNECTED = "pref_pause_download_on_wifi_disconnected";
    private static String appPackageName;
    private static String downloadProviderAuthorities;
    private static Uri downloadProviderContentUri;
    private static String downloadReceiverClassName;
    private static String downloadServiceClassName;
    private static int maxConcurrentDownloadingTasks = Integer.MAX_VALUE;
    private static int maxDownloadingTasksPerDomain = Integer.MAX_VALUE;
    private static boolean pauseDownloadWhenWifiDisconnected = false;
    private static String storageDir;

    public static final String getAppPackageName() {
        if (appPackageName != null && appPackageName.length() != 0) {
            return appPackageName;
        }
        throw new IllegalArgumentException("Empty app setting: appPackageName");
    }

    public static final String getDownloadProviderAuthorities() {
        if (downloadProviderAuthorities != null && downloadProviderAuthorities.length() != 0) {
            return downloadProviderAuthorities;
        }
        throw new IllegalArgumentException("Empty app setting: downloadProviderAuthorities");
    }

    public static final Uri getDownloadProviderContentUri() {
        if (downloadProviderContentUri != null) {
            return downloadProviderContentUri;
        }
        throw new IllegalArgumentException("Empty app setting: downloadProviderContentUri");
    }

    public static final String getDownloadReceiverClassName() {
        if (downloadReceiverClassName != null && downloadReceiverClassName.length() != 0) {
            return downloadReceiverClassName;
        }
        throw new IllegalArgumentException("Empty app setting: downloadReceiverClassName");
    }

    public static final String getDownloadServiceClassName() {
        if (downloadServiceClassName != null && downloadServiceClassName.length() != 0) {
            return downloadServiceClassName;
        }
        throw new IllegalArgumentException("Empty app setting: downloadServiceClassName");
    }

    protected static final int getMaxConcurrentDownloadingTasks() {
        return maxConcurrentDownloadingTasks;
    }

    protected static final int getMaxDownloadingTasksPerDomain() {
        return maxDownloadingTasksPerDomain;
    }

    public static final boolean getPauseDownloadWhenWifiDisconnected() {
        return pauseDownloadWhenWifiDisconnected;
    }

    public static final String getStorageDir() {
        if (storageDir != null && storageDir.length() != 0) {
            return storageDir;
        }
        throw new IllegalArgumentException("Empty app setting: storageDir");
    }

    public static final void initializeDownloadWhenWifiDisconnected(Context context) {
        pauseDownloadWhenWifiDisconnected = PreferenceManager.getDefaultSharedPreferences(context).getBoolean(PREF_PAUSE_DOWNLOAD_ON_WIFI_DISCONNECTED, true);
    }

    protected static final void setAppPackageName(String s) {
        appPackageName = s;
    }

    protected static final void setDownloadProviderAuthorities(String providerAuthorities) {
        downloadProviderAuthorities = providerAuthorities;
        if (TextUtils.isEmpty(storageDir)) {
            storageDir = "/" + providerAuthorities;
        }
        downloadProviderContentUri = Uri.parse("content://" + providerAuthorities + "/download");
    }

    protected static final void setDownloadReceiverClassName(String s) {
        downloadReceiverClassName = s;
    }

    protected static final void setDownloadServiceClassName(String s) {
        downloadServiceClassName = s;
    }

    protected static final void setMaxConcurrentDownloadingTasks(int i) {
        maxConcurrentDownloadingTasks = i;
    }

    protected static final void setMaxDownloadingTasksPerDomain(int i) {
        maxDownloadingTasksPerDomain = i;
    }

    public static final void setPauseDownloadWhenWifiDisconnected(Context context, boolean flag) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putBoolean(PREF_PAUSE_DOWNLOAD_ON_WIFI_DISCONNECTED, flag);
        editor.commit();
        pauseDownloadWhenWifiDisconnected = flag;
    }

    public static final void setStorageDir(String s) {
        if (s == null || s.length() == 0) {
            throw new IllegalArgumentException("Empty app setting: dirName");
        } else if (!s.startsWith("/")) {
            storageDir = "/" + s;
        } else {
            storageDir = s;
        }
    }

    static {
        Log.d(Constants.TAG, "Initializing AppSettings");
        setStorageDir("cmsurfclient/downloads");
        setAppPackageName("com.cplatform.android.cmsurfclient");
        setDownloadReceiverClassName("com.cplatform.android.cmsurfclient.download.provider.DownloadReceiver");
        setDownloadServiceClassName("com.cplatform.android.cmsurfclient.download.provider.DownloadService");
        setDownloadProviderAuthorities("com.cplatform.android.cmsurfclient.download.downloads");
        setMaxDownloadingTasksPerDomain(1);
        setMaxConcurrentDownloadingTasks(1);
    }

    public static void ensureAppSettingsInitialized() {
        Log.d(Constants.TAG, "AppSettings should has been Initialized");
    }
}
