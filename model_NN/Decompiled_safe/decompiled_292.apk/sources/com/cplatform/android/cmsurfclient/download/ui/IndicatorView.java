package com.cplatform.android.cmsurfclient.download.ui;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.download.provider.DownloadNotification;
import com.cplatform.android.cmsurfclient.download.provider.DownloadSettings;
import com.cplatform.android.cmsurfclient.download.provider.Downloads;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;

public class IndicatorView extends View {
    private static final int selectedTextStyle = 2131361801;
    private static final int unSelectedTextStyle = 2131361800;
    /* access modifiers changed from: private */
    public Context mContext;
    private Intent[] mIntents;
    private CharSequence[] mTabNames;
    private View mView;

    public IndicatorView(Context context, CharSequence[] tabNames, Intent[] intents) {
        super(context);
        this.mContext = context;
        this.mTabNames = tabNames;
        this.mIntents = intents;
    }

    private View getIndicatorView(CharSequence charsequence) {
        this.mView = LayoutInflater.from(this.mContext).inflate((int) R.layout.download_tabhost_tab, (ViewGroup) null);
        ((TextView) this.mView.findViewById(R.id.download_tab_name)).setText(charsequence);
        return this.mView;
    }

    public void setSelectAppearance() {
        ((TextView) this.mView.findViewById(R.id.download_tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_selected);
    }

    public void setTabHostAppearance(TabActivity tabactivity) {
        final TabHost tabHost = tabactivity.getTabHost();
        final TabWidget tabWidget = tabHost.getTabWidget();
        int tabNamesCount = this.mTabNames.length;
        for (int j = 0; j < tabNamesCount; j++) {
            tabHost.addTab(tabHost.newTabSpec(this.mTabNames[j].toString()).setIndicator(getIndicatorView(this.mTabNames[j])).setContent(this.mIntents[j]));
        }
        int tabCount = tabWidget.getChildCount();
        for (int j2 = 0; j2 < tabCount; j2 = j2 + 1 + 1) {
            ((TextView) tabWidget.getChildAt(j2).findViewById(R.id.download_tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_unselected);
        }
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            public void onTabChanged(String s) {
                int childCount = tabWidget.getChildCount();
                int i = 0;
                while (i < childCount) {
                    ((TextView) tabWidget.getChildAt(i).findViewById(R.id.download_tab_name)).setTextAppearance(IndicatorView.this.mContext, tabHost.getCurrentTab() == i ? R.style.download_tab_text_selected : R.style.download_tab_text_unselected);
                    i++;
                }
            }
        });
        int index = 0;
        if (tabNamesCount > 1) {
            index = getRunningTaskCount() > 0 ? 0 : 1;
        }
        tabHost.setCurrentTab(index);
        ((TextView) tabWidget.getChildAt(index).findViewById(R.id.download_tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_selected);
    }

    public void setUnSelectAppearance() {
        ((TextView) this.mView.findViewById(R.id.download_tab_name)).setTextAppearance(this.mContext, R.style.download_tab_text_selected);
    }

    public int getRunningTaskCount() {
        int count;
        Cursor tasksInQueue = null;
        try {
            tasksInQueue = this.mContext.getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), new String[]{QueryApList.Carriers._ID, Downloads.COLUMN_TITLE, Downloads.COLUMN_STATUS, Downloads.COLUMN_CONTROL, Downloads.COLUMN_CURRENT_BYTES, Downloads.COLUMN_CURRENT_ELAPSED_TIME, Downloads.COLUMN_TOTAL_BYTES, Downloads.COLUMN_APP_DATA, Downloads._DATA}, String.format("(%s) OR (%s)", DownloadNotification.WHERE_IN_QUEUE, DownloadNotification.WHERE_RUNNING), null, QueryApList.Carriers._ID);
            count = tasksInQueue.getCount();
            if (tasksInQueue != null) {
                try {
                    tasksInQueue.close();
                } catch (Exception e) {
                }
            }
        } catch (Exception e2) {
            count = 0;
            if (tasksInQueue != null) {
                try {
                    tasksInQueue.close();
                } catch (Exception e3) {
                }
            }
        } catch (Throwable th) {
            if (tasksInQueue != null) {
                try {
                    tasksInQueue.close();
                } catch (Exception e4) {
                }
            }
            throw th;
        }
        return count;
    }
}
