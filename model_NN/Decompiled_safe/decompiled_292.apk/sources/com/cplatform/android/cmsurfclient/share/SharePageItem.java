package com.cplatform.android.cmsurfclient.share;

import android.widget.EditText;

public class SharePageItem {
    public String mTag = String.valueOf(System.currentTimeMillis());
    public EditText mTxtInfo;
    public String mUserInfo;

    public SharePageItem(String userInfo) {
        this.mUserInfo = userInfo;
    }
}
