package com.cplatform.android.cmsurfclient.service.entry;

import org.w3c.dom.Element;

public class Upgrade {
    public int force = 2;
    public String sver = null;
    public String txt = null;
    public String url = null;

    public Upgrade() {
    }

    public Upgrade(Element entry) {
        if (entry != null) {
            this.sver = entry.getAttribute("sver");
            this.url = entry.getAttribute("url");
            try {
                this.force = Integer.parseInt(entry.getAttribute("force"));
            } catch (Exception e) {
                this.force = 2;
            }
            this.txt = entry.getAttribute("txt");
        }
    }

    public String toString() {
        return "sver:" + this.sver + "\turl:" + this.url + "\tforce:" + this.force + "\ttxt:" + this.txt;
    }
}
