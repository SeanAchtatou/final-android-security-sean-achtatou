package com.cplatform.android.cmsurfclient.windows;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.cplatform.android.cmsurfclient.R;
import java.util.ArrayList;

public class WindowAdapter2 extends ArrayAdapter<WindowItemNew> {
    public static final String BLANK_URL = "";
    private WindowManagerView mWindowManager;

    public WindowAdapter2(WindowManagerView windowManager, ArrayList<WindowItemNew> items) {
        super(windowManager.mSurfMgr, (int) R.layout.windows_manager_listitem, items);
        this.mWindowManager = windowManager;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        if (convertView == null) {
            row = this.mWindowManager.mSurfMgr.getLayoutInflater().inflate((int) R.layout.windows_manager_listitem, (ViewGroup) null);
        } else {
            row = convertView;
        }
        WindowItemNew item = (WindowItemNew) getItem(position);
        TextView title = (TextView) row.findViewById(R.id.window_title);
        TextView url = (TextView) row.findViewById(R.id.window_url);
        ImageView icon = (ImageView) row.findViewById(R.id.window_icon);
        ImageButton close = (ImageButton) row.findViewById(R.id.windows_manager_button_close);
        close.setVisibility(getCount() > 1 ? 0 : 4);
        String itemTitle = item.state == 4 ? item.title : this.mWindowManager.getResources().getString(R.string.app_name);
        String itemUrl = item.state == 4 ? item.url : BLANK_URL;
        if (itemTitle == null || BLANK_URL.equals(itemTitle)) {
            itemTitle = this.mWindowManager.getResources().getString(R.string.blank_page);
        }
        if (itemUrl == null) {
            itemUrl = BLANK_URL;
        }
        title.setText(itemTitle);
        url.setText(itemUrl);
        if (item.snapshot != null) {
            icon.setImageBitmap(item.snapshot);
        }
        close.setOnClickListener(new WindowItemDeleteOnClickListener(this.mWindowManager, item));
        row.setClickable(true);
        row.setOnClickListener(new WindowItemOnClickListener(this.mWindowManager, item));
        return row;
    }

    public class WindowItemDeleteOnClickListener implements View.OnClickListener {
        public WindowItemNew mItem = null;
        public WindowManagerView mManagerControl = null;

        public WindowItemDeleteOnClickListener(WindowManagerView manager, WindowItemNew item) {
            this.mManagerControl = manager;
            this.mItem = item;
        }

        public void onClick(View v) {
            if (this.mManagerControl != null) {
                this.mManagerControl.onDelete(this.mItem);
            }
        }
    }

    public class WindowItemOnClickListener implements View.OnClickListener {
        public WindowItemNew mItem;
        public WindowManagerView mManagerControl = null;

        public WindowItemOnClickListener(WindowManagerView manager, WindowItemNew item) {
            this.mManagerControl = manager;
            this.mItem = item;
        }

        public void onClick(View v) {
            this.mManagerControl.onSwitchTo(this.mItem);
        }
    }
}
