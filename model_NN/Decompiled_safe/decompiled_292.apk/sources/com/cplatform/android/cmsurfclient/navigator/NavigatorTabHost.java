package com.cplatform.android.cmsurfclient.navigator;

import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import com.cplatform.android.cmsurfclient.R;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.navigator.NavigatorTabWidget;
import java.util.ArrayList;
import java.util.List;

public class NavigatorTabHost extends FrameLayout {
    protected int mCurrentTab = -1;
    private View mCurrentView = null;
    protected LocalActivityManager mLocalActivityManager = null;
    private OnTabChangeListener mOnTabChangeListener;
    /* access modifiers changed from: private */
    public FrameLayout mTabContent;
    private List<NavigatorTabSpec> mTabSpecs = new ArrayList(2);
    private NavigatorTabWidget mTabWidget;

    private interface ContentStrategy {
        View getContentView();

        void tabClosed();
    }

    public interface OnTabChangeListener {
        void onTabChanged(String str);
    }

    public interface TabContentFactory {
        View createTabContent(String str);
    }

    public NavigatorTabHost(Context context) {
        super(context);
        initTabHost();
    }

    public NavigatorTabHost(Context context, AttributeSet attrs) {
        super(context, attrs);
        initTabHost();
    }

    public NavigatorTabHost(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initTabHost();
    }

    private final void initTabHost() {
        setFocusableInTouchMode(true);
        setDescendantFocusability(262144);
        this.mCurrentTab = -1;
        this.mCurrentView = null;
    }

    public NavigatorTabSpec newTabSpec(String tag, String label, Bitmap bmpActive, Bitmap bmpInactive) {
        return new NavigatorTabSpec(tag, label, bmpActive, bmpInactive);
    }

    public void setup() {
        this.mTabWidget = (NavigatorTabWidget) findViewById(R.id.navigatortabs);
        if (this.mTabWidget == null) {
            throw new RuntimeException("Your SlidingTabHost must have a SlidingTabWidget whose id attribute is 'R.id.slidingtabs'");
        }
        this.mTabWidget.setTabSelectionListener(new NavigatorTabWidget.OnTabSelectionChanged() {
            public void onTabSelectionChanged(int tabIndex, boolean clicked) {
                NavigatorTabHost.this.setCurrentTab(tabIndex);
                if (clicked) {
                    NavigatorTabHost.this.mTabContent.requestFocus(2);
                }
            }
        });
        this.mTabContent = (FrameLayout) findViewById(R.id.navigatortabcontent);
        if (this.mTabContent == null) {
            throw new RuntimeException("Your SlidingTabHost must have a FrameLayout whose id attribute is 'android.R.id.tabcontent'");
        }
        this.mTabWidget.setTabContent(this.mTabContent);
    }

    public void setup(LocalActivityManager activityGroup) {
        setup();
        this.mLocalActivityManager = activityGroup;
    }

    public void addTab(NavigatorTabSpec tabSpec) {
        if (tabSpec.mContentStrategy == null) {
            throw new IllegalArgumentException("you must specify a way to create the tab content");
        }
        this.mTabWidget.addTab(tabSpec);
        this.mTabSpecs.add(tabSpec);
        if (this.mCurrentTab == -1) {
            setCurrentTab(0);
        }
    }

    public void clearAllTabs() {
        this.mTabWidget.removeAllViews();
        initTabHost();
        this.mTabContent.removeAllViews();
        this.mTabSpecs.clear();
        requestLayout();
        invalidate();
    }

    public NavigatorTabWidget getTabWidget() {
        return this.mTabWidget;
    }

    public int getCurrentTab() {
        return this.mCurrentTab;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public String getCurrentTabTag() {
        if (this.mCurrentTab < 0 || this.mCurrentTab >= this.mTabSpecs.size()) {
            return null;
        }
        return this.mTabSpecs.get(this.mCurrentTab).getTag();
    }

    public View getCurrentTabView() {
        if (this.mCurrentTab < 0 || this.mCurrentTab >= this.mTabSpecs.size()) {
            return null;
        }
        return this.mTabWidget.getChildTabViewAt(this.mCurrentTab);
    }

    public View getCurrentView() {
        return this.mCurrentView;
    }

    public void setCurrentTabByTag(String tag) {
        for (int i = 0; i < this.mTabSpecs.size(); i++) {
            if (this.mTabSpecs.get(i).getTag().equals(tag)) {
                setCurrentTab(i);
                return;
            }
        }
    }

    public FrameLayout getTabContentView() {
        return this.mTabContent;
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        int tabIndex;
        int tabIndex2;
        switch (event.getKeyCode()) {
            case SurfManagerActivity.CONTEXTMENU_HOMEVIEW_OPENNEW:
                if (event.getAction() == 0 && (tabIndex2 = this.mCurrentTab - 1) >= 0) {
                    setCurrentTab(tabIndex2);
                    this.mTabWidget.moveTo(tabIndex2);
                }
                return true;
            case SurfManagerActivity.CONTEXTMENU_HOMEVIEW_BOOKMARK:
                if (event.getAction() == 0 && (tabIndex = this.mCurrentTab + 1) <= this.mTabWidget.getTabCount()) {
                    setCurrentTab(tabIndex);
                    this.mTabWidget.moveTo(tabIndex);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    public void dispatchWindowFocusChanged(boolean hasFocus) {
        if (this.mCurrentView != null) {
            this.mCurrentView.dispatchWindowFocusChanged(hasFocus);
        }
    }

    public void setCurrentTab(int index) {
        if (index >= 0 && index < this.mTabSpecs.size() && index != this.mCurrentTab) {
            if (this.mCurrentTab != -1) {
                this.mTabSpecs.get(this.mCurrentTab).mContentStrategy.tabClosed();
            }
            this.mCurrentTab = index;
            this.mTabWidget.setCurrentTab(this.mCurrentTab);
            this.mCurrentView = this.mTabSpecs.get(index).mContentStrategy.getContentView();
            if (this.mCurrentView.getParent() == null) {
                this.mTabContent.addView(this.mCurrentView, new ViewGroup.LayoutParams(-1, -1));
            }
            if (!this.mTabWidget.hasFocus()) {
                this.mCurrentView.requestFocus();
            }
            invokeOnTabChangeListener();
        }
    }

    public void setOnTabChangedListener(OnTabChangeListener l) {
        this.mOnTabChangeListener = l;
    }

    private void invokeOnTabChangeListener() {
        if (this.mOnTabChangeListener != null) {
            this.mOnTabChangeListener.onTabChanged(getCurrentTabTag());
        }
    }

    public class NavigatorTabSpec {
        private Bitmap mBmpActive;
        private Bitmap mBmpInactive;
        /* access modifiers changed from: private */
        public ContentStrategy mContentStrategy;
        private String mLabel;
        private String mTag;

        private NavigatorTabSpec(String tag, String label, Bitmap bmpActive, Bitmap bmpInactive) {
            this.mTag = tag;
            this.mLabel = label;
            this.mBmpActive = bmpActive;
            this.mBmpInactive = bmpInactive;
        }

        public NavigatorTabSpec setContent(int viewId) {
            this.mContentStrategy = new ViewIdContentStrategy(viewId);
            return this;
        }

        public NavigatorTabSpec setContent(TabContentFactory contentFactory) {
            this.mContentStrategy = new FactoryContentStrategy(this.mTag, contentFactory);
            return this;
        }

        public NavigatorTabSpec setContent(Intent intent) {
            this.mContentStrategy = new IntentContentStrategy(this.mTag, intent);
            return this;
        }

        public String getTag() {
            return this.mTag;
        }

        public String getLabel() {
            return this.mLabel;
        }

        public Bitmap getActiveIcon() {
            return this.mBmpActive;
        }

        public Bitmap getInactiveIcon() {
            return this.mBmpInactive;
        }
    }

    private class ViewIdContentStrategy implements ContentStrategy {
        private final View mView;

        private ViewIdContentStrategy(int viewId) {
            this.mView = NavigatorTabHost.this.mTabContent.findViewById(viewId);
            if (this.mView != null) {
                this.mView.setVisibility(8);
                return;
            }
            throw new RuntimeException("Could not create tab content because could not find view with id " + viewId);
        }

        public View getContentView() {
            this.mView.setVisibility(0);
            return this.mView;
        }

        public void tabClosed() {
            this.mView.setVisibility(8);
        }
    }

    private class FactoryContentStrategy implements ContentStrategy {
        private TabContentFactory mFactory;
        private View mTabContent;
        private final CharSequence mTag;

        public FactoryContentStrategy(CharSequence tag, TabContentFactory factory) {
            this.mTag = tag;
            this.mFactory = factory;
        }

        public View getContentView() {
            if (this.mTabContent == null) {
                this.mTabContent = this.mFactory.createTabContent(this.mTag.toString());
            }
            this.mTabContent.setVisibility(0);
            return this.mTabContent;
        }

        public void tabClosed() {
            this.mTabContent.setVisibility(4);
        }
    }

    private class IntentContentStrategy implements ContentStrategy {
        private final Intent mIntent;
        private View mLaunchedView;
        private final String mTag;

        private IntentContentStrategy(String tag, Intent intent) {
            this.mTag = tag;
            this.mIntent = intent;
        }

        public View getContentView() {
            if (NavigatorTabHost.this.mLocalActivityManager == null) {
                throw new IllegalStateException("Did you forget to call 'public void setup(LocalActivityManager activityGroup)'?");
            }
            Window w = NavigatorTabHost.this.mLocalActivityManager.startActivity(this.mTag, this.mIntent);
            View wd = w != null ? w.getDecorView() : null;
            if (!(this.mLaunchedView == wd || this.mLaunchedView == null || this.mLaunchedView.getParent() == null)) {
                NavigatorTabHost.this.mTabContent.removeView(this.mLaunchedView);
            }
            this.mLaunchedView = wd;
            if (this.mLaunchedView != null) {
                this.mLaunchedView.setVisibility(0);
                this.mLaunchedView.setFocusableInTouchMode(true);
                ((ViewGroup) this.mLaunchedView).setDescendantFocusability(262144);
            }
            return this.mLaunchedView;
        }

        public void tabClosed() {
            if (this.mLaunchedView != null) {
                this.mLaunchedView.setVisibility(8);
            }
        }
    }
}
