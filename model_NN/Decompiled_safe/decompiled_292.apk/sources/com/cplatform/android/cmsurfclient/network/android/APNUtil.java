package com.cplatform.android.cmsurfclient.network.android;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import com.cplatform.android.cmsurfclient.SurfManagerActivity;
import com.cplatform.android.cmsurfclient.network.ConnectHelper;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import com.cplatform.android.cmsurfclient.windows.WindowAdapter2;
import java.util.HashMap;

public class APNUtil {
    private static final String COLUMN_APN = "apn";
    private static final String COLUMN_APN_ID = "apn_id";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_TYPE = "type";
    private static final Uri CURRENT_APNS = Uri.parse("content://telephony/carriers/current");
    private static final String LOG_TAG = "APNUtil";
    private static final Uri PREFERRED_APN = Uri.parse("content://telephony/carriers/preferapn");
    private static final String[] PROJECTION = {"_id", "apn", "type"};
    public static APNUtil mInstance = null;
    private HashMap<String, APNNode> mAPNList = new HashMap<>();
    private int mCMNetID = -1;
    private int mCMWapID = -1;
    private Context mContext = null;
    private int mCurAPNID = -1;
    public APNType mCurAPNType = APNType.Unknow;
    private boolean mFixCMNet = false;
    private int mLastAPNID = -1;
    private boolean mUseCMWap = false;

    public enum APNType {
        CMWAP,
        CMNET,
        Unknow
    }

    public static APNUtil getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new APNUtil(context);
        }
        return mInstance;
    }

    public APNUtil(Context context) {
        this.mContext = context;
        if (mInstance == null) {
            mInstance = this;
        }
    }

    private boolean loadAPNList() {
        Log.e(LOG_TAG, "loadAPNList: begin...");
        this.mCurAPNID = -1;
        this.mLastAPNID = -1;
        this.mCMWapID = -1;
        this.mCMNetID = -1;
        this.mCurAPNType = APNType.Unknow;
        try {
            Cursor cursor = this.mContext.getContentResolver().query(PREFERRED_APN, PROJECTION, null, null, null);
            if (cursor == null || cursor.getCount() < 1) {
                Log.w("LOG_TAG", "loadAPNList: failed to get preferred APN");
                return false;
            }
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                this.mCurAPNID = cursor.getInt(0);
                String apn = cursor.getString(1);
                if (apn.equalsIgnoreCase("CMWAP")) {
                    this.mCurAPNType = APNType.CMWAP;
                } else if (apn.equalsIgnoreCase("CMNET")) {
                    this.mCurAPNType = APNType.CMNET;
                }
                Log.w(LOG_TAG, "loadAPNList: get preferred APN id - " + this.mCurAPNID);
            }
            cursor.close();
            try {
                Cursor cursor2 = this.mContext.getContentResolver().query(CURRENT_APNS, PROJECTION, null, null, null);
                if (cursor2 == null || cursor2.getCount() < 1) {
                    Log.e("LOG_TAG", "Failed to get current APN list");
                    return false;
                }
                cursor2.moveToFirst();
                while (!cursor2.isAfterLast()) {
                    int id = cursor2.getInt(0);
                    String apn2 = cursor2.getString(1);
                    if (apn2 == null) {
                        apn2 = WindowAdapter2.BLANK_URL;
                    }
                    String type = cursor2.getString(2);
                    if (type == null) {
                        type = WindowAdapter2.BLANK_URL;
                    }
                    this.mAPNList.put(String.valueOf(id), new APNNode(String.valueOf(id), apn2, type));
                    Log.w(LOG_TAG, "loadAPNList: id=" + id + ", apn=" + apn2 + ", type=" + type);
                    if (apn2.equalsIgnoreCase("CMWAP") && (type.toLowerCase().contains("default") || !type.toLowerCase().contains("mms"))) {
                        this.mCMWapID = id;
                    }
                    if (apn2.equalsIgnoreCase("CMNET") && type.toLowerCase().contains(ConnectHelper.FEATURE_ENABLE_INTERNET)) {
                        this.mCMNetID = id;
                    }
                    cursor2.moveToNext();
                }
                cursor2.close();
                return true;
            } catch (Exception e) {
                Exception ep = e;
                if (!(ep == null || ep.getMessage() == null)) {
                    Log.e("LOG_TAG", ep.getMessage());
                }
                return false;
            }
        } catch (Exception e2) {
            Exception ep2 = e2;
            if (!(ep2 == null || ep2.getMessage() == null)) {
                Log.e("LOG_TAG", "loadAPNList: failed to get preferred APN, error - " + ep2.getMessage());
            }
            return false;
        }
    }

    public boolean modifyAPN(boolean useCMWap, boolean fixCMNet) {
        ConnectivityManager cm;
        NetworkInfo info;
        Log.e(LOG_TAG, "modifyAPN: begin...");
        loadAPNList();
        if (useCMWap && getMCC().equals("460") && getMNC().equals("00") && this.mCMWapID == -1) {
            this.mCMWapID = addCMWAPAPN();
        }
        String model = Build.MODEL;
        if (!TextUtils.isEmpty(model) && model.equalsIgnoreCase(SurfManagerActivity.MODEL_DOPOD_A8188)) {
            Log.e(LOG_TAG, "Dopod A8188 found, we need check where preferred APN is matched with active network");
            if (this.mCurAPNType.equals(APNType.CMNET) && (cm = (ConnectivityManager) this.mContext.getSystemService("connectivity")) != null && (info = cm.getNetworkInfo(0)) != null && !TextUtils.isEmpty(info.getExtraInfo()) && info.getExtraInfo().equalsIgnoreCase("cmwap")) {
                Log.e(LOG_TAG, "Preferred APN is not matched with active network type");
                useCMWap = true;
            }
        }
        this.mUseCMWap = useCMWap;
        this.mFixCMNet = fixCMNet;
        if (!this.mUseCMWap || this.mCMWapID == -1 || this.mCMWapID == this.mCurAPNID) {
            this.mLastAPNID = this.mCurAPNID;
        } else {
            try {
                ContentResolver cr = this.mContext.getContentResolver();
                ContentValues values = new ContentValues();
                values.put(COLUMN_APN_ID, Integer.valueOf(this.mCMWapID));
                cr.update(PREFERRED_APN, values, null, null);
                Settings.System.putInt(cr, "network_manual_mode", 1);
                Settings.System.putString(cr, "network_manual_profile", String.valueOf(this.mCMWapID));
                this.mContext.sendBroadcast(new Intent("android.intent.action.NETWORK_MANUAL_MODE"));
                this.mLastAPNID = this.mCurAPNID;
                this.mCurAPNID = this.mCMWapID;
                this.mCurAPNType = APNType.CMWAP;
                Log.e(LOG_TAG, "modifyAPN: set current APN to " + this.mCMWapID);
            } catch (Exception e) {
                Exception ep = e;
                if (!(ep == null || ep.getMessage() == null)) {
                    Log.e(LOG_TAG, "modifyAPN: set current APN error - " + ep.getMessage());
                }
                return false;
            }
        }
        if (this.mFixCMNet && this.mCMNetID != -1) {
            APNNode node = this.mAPNList.get(String.valueOf(this.mCMNetID));
            if (node == null) {
                return false;
            }
            String[] args = {node.mID};
            try {
                ContentResolver cr2 = this.mContext.getContentResolver();
                ContentValues values2 = new ContentValues();
                values2.put("type", node.mType.replaceAll(ConnectHelper.FEATURE_ENABLE_INTERNET, WindowAdapter2.BLANK_URL));
                cr2.update(CURRENT_APNS, values2, "_id=?", args);
                Log.e(LOG_TAG, "modifyAPN: remove 'internet' in CMNET apn type");
            } catch (Exception e2) {
                Exception ep2 = e2;
                if (!(ep2 == null || ep2.getMessage() == null)) {
                    Log.e("APN", "modifyAPN: remove 'internet' in CMNET apn type error - " + ep2.getMessage());
                }
                return false;
            }
        }
        return true;
    }

    public boolean restoreAPN() {
        Log.e(LOG_TAG, "restoreAPN: begin...");
        if (!(!this.mUseCMWap || this.mLastAPNID == -1 || this.mLastAPNID == this.mCurAPNID)) {
            try {
                ContentResolver cr = this.mContext.getContentResolver();
                ContentValues values = new ContentValues();
                values.put(COLUMN_APN_ID, Integer.valueOf(this.mLastAPNID));
                cr.update(PREFERRED_APN, values, null, null);
                Settings.System.putInt(cr, "network_manual_mode", 1);
                Settings.System.putString(cr, "network_manual_profile", String.valueOf(this.mLastAPNID));
                this.mContext.sendBroadcast(new Intent("android.intent.action.NETWORK_MANUAL_MODE"));
                this.mCurAPNID = this.mLastAPNID;
                Log.e(LOG_TAG, "restoreAPN: set current APN to " + this.mLastAPNID);
            } catch (Exception e) {
                Exception ep = e;
                if (!(ep == null || ep.getMessage() == null)) {
                    Log.e(LOG_TAG, "restoreAPN: set current APN error - " + ep.getMessage());
                }
                return false;
            }
        }
        if (this.mFixCMNet && this.mCMNetID != -1) {
            APNNode node = this.mAPNList.get(String.valueOf(this.mCMNetID));
            if (node == null) {
                return false;
            }
            String[] args = {node.mID};
            try {
                ContentResolver cr2 = this.mContext.getContentResolver();
                ContentValues values2 = new ContentValues();
                values2.put("type", node.mType);
                cr2.update(CURRENT_APNS, values2, "_id=?", args);
                Log.e(LOG_TAG, "restoreAPN: add 'internet' in CMNET apn type");
            } catch (Exception e2) {
                Exception ep2 = e2;
                if (!(ep2 == null || ep2.getMessage() == null)) {
                    Log.e("APN", "restoreAPN: add 'internet' in CMNET apn type error - " + ep2.getMessage());
                }
                return false;
            }
        }
        return true;
    }

    private String getMCC() {
        String mcc = getSimOperator().substring(0, 3);
        Log.i("MCC is", mcc);
        return mcc;
    }

    private String getMNC() {
        String numeric = getSimOperator();
        String mnc = numeric.substring(3, numeric.length());
        Log.i("MNC is", mnc);
        return mnc;
    }

    private String getSimOperator() {
        return ((TelephonyManager) this.mContext.getSystemService("phone")).getSimOperator();
    }

    public int addCMWAPAPN() {
        int apnId = -1;
        ContentResolver resolver = this.mContext.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(QueryApList.Carriers.NAME, "CMWAP");
        values.put("apn", "cmwap");
        values.put(QueryApList.Carriers.PROXY, "10.0.0.172");
        values.put(QueryApList.Carriers.PORT, "80");
        values.put(QueryApList.Carriers.MCC, "460");
        values.put(QueryApList.Carriers.MNC, "00");
        values.put(QueryApList.Carriers.NUMERIC, getSimOperator());
        values.put("type", WindowAdapter2.BLANK_URL);
        Cursor c = null;
        try {
            Uri newRow = resolver.insert(Uri.parse("content://telephony/carriers"), values);
            if (newRow != null) {
                c = resolver.query(newRow, null, null, null, null);
                int id = c.getColumnIndex("_id");
                c.moveToFirst();
                apnId = c.getShort(id);
                Log.e(LOG_TAG, "CMWAP APN ID: " + apnId + ": Inserting new APN succeeded!");
            }
        } catch (Exception e) {
        }
        if (c != null) {
            c.close();
        }
        return apnId;
    }

    public class APNNode {
        public String mAPN;
        public String mID;
        public String mType;

        public APNNode(String id, String apn, String type) {
            this.mID = id;
            this.mAPN = apn;
            this.mType = type;
        }
    }
}
