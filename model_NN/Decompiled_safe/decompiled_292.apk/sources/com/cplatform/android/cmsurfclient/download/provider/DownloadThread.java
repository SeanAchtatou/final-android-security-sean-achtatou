package com.cplatform.android.cmsurfclient.download.provider;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import java.util.Locale;

public class DownloadThread extends Thread {
    private Context mContext;
    private DownloadInfo mInfo;

    public DownloadThread(Context context, DownloadInfo info) {
        this.mContext = context;
        this.mInfo = info;
    }

    private String userAgent() {
        String userAgent = this.mInfo.mUserAgent;
        if (userAgent != null) {
        }
        if (userAgent == null) {
            return "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.3 (KHTML, like Gecko) Chrome/6.0.464.0 Safari/534.3";
        }
        return userAgent;
    }

    /* JADX INFO: Multiple debug info for r31v7 java.io.IOException: [D('ex' java.lang.RuntimeException), D('ex' java.io.IOException)] */
    /* JADX INFO: Multiple debug info for r31v8 java.io.SyncFailedException: [D('ex' java.lang.RuntimeException), D('ex' java.io.SyncFailedException)] */
    /* JADX INFO: Multiple debug info for r31v9 java.io.FileNotFoundException: [D('ex' java.lang.RuntimeException), D('ex' java.io.FileNotFoundException)] */
    /* JADX INFO: Multiple debug info for r31v12 java.lang.RuntimeException: [D('ex' java.lang.RuntimeException), D('ex' java.io.FileNotFoundException)] */
    /* JADX INFO: Multiple debug info for r31v13 java.io.IOException: [D('ex' java.io.IOException), D('ex' java.io.FileNotFoundException)] */
    /* JADX INFO: Multiple debug info for r31v14 java.io.SyncFailedException: [D('ex' java.io.FileNotFoundException), D('ex' java.io.SyncFailedException)] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Code restructure failed: missing block: B:237:?, code lost:
        r62 = new android.content.ContentValues();
        r62.put(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_CURRENT_BYTES, java.lang.Integer.valueOf(r23));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x06ff, code lost:
        if (r41 != null) goto L_0x070e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:239:0x0701, code lost:
        r62.put(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_TOTAL_BYTES, java.lang.Integer.valueOf(r23));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x070e, code lost:
        r0.mContext.getContentResolver().update(r26, r62, null, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x0723, code lost:
        if (r41 == null) goto L_0x0a1d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x072c, code lost:
        if (r23 == java.lang.Integer.parseInt(r41)) goto L_0x0a1d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:245:0x0735, code lost:
        if (r0.mInfo.mNoIntegrity != false) goto L_0x088a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x0737, code lost:
        if (r42 != null) goto L_0x088a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x0739, code lost:
        android.util.Log.d(com.cplatform.android.cmsurfclient.download.provider.Constants.TAG, "mismatched content length for " + r0.mInfo.mId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x0758, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_LENGTH_REQUIRED;
        r12 = null;
        r9 = r51;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:286:0x0893, code lost:
        if (com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r0.mContext) != false) goto L_0x08a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:287:0x0895, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_RUNNING_PAUSED;
        r12 = null;
        r9 = r51;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:289:0x08a9, code lost:
        if (r0.mInfo.mNumFailed >= 5) goto L_0x08b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:290:0x08ab, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_RUNNING_PAUSED;
        r7 = true;
        r12 = null;
        r9 = r51;
        r8 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:291:0x08b6, code lost:
        android.util.Log.d(com.cplatform.android.cmsurfclient.download.provider.Constants.TAG, "closed socket for download " + r0.mInfo.mId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:292:0x08d5, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_HTTP_DATA_ERROR;
        r12 = null;
        r9 = r51;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:300:0x08fb, code lost:
        r23 = r23 + r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:302:?, code lost:
        r47 = java.lang.System.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x0905, code lost:
        if ((r23 - r21) <= 4096) goto L_0x093a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:305:0x090d, code lost:
        if ((r47 - r59) <= com.cplatform.android.cmsurfclient.download.provider.Constants.MIN_PROGRESS_TIME) goto L_0x093a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:306:0x090f, code lost:
        r62 = new android.content.ContentValues();
        r62.put(com.cplatform.android.cmsurfclient.download.provider.Downloads.COLUMN_CURRENT_BYTES, java.lang.Integer.valueOf(r23));
        r0.mContext.getContentResolver().update(r26, r62, null, null);
        r21 = r23;
        r59 = r47;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:0x093a, code lost:
        r5 = r0.mInfo;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:308:0x093f, code lost:
        monitor-enter(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:312:0x0948, code lost:
        if (r0.mInfo.mControl != 1) goto L_0x097c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:313:0x094a, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_RUNNING_PAUSED;
        r0.abort();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:314:0x094f, code lost:
        monitor-exit(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:315:0x0950, code lost:
        r12 = null;
        r9 = r51;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:325:?, code lost:
        monitor-exit(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:330:0x0988, code lost:
        android.util.Log.d(com.cplatform.android.cmsurfclient.download.provider.Constants.TAG, "canceled id " + r0.mInfo.mId);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:331:0x09a7, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_CANCELED;
        r12 = null;
        r9 = r51;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:355:0x0a1d, code lost:
        r6 = com.cplatform.android.cmsurfclient.download.provider.Downloads.STATUS_SUCCESS;
        r12 = null;
        r9 = r51;
        r8 = 0;
        r7 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:344:0x09f2  */
    /* JADX WARNING: Removed duplicated region for block: B:346:0x09f9  */
    /* JADX WARNING: Removed duplicated region for block: B:348:0x0a00 A[SYNTHETIC, Splitter:B:348:0x0a00] */
    /* JADX WARNING: Removed duplicated region for block: B:351:0x0a05  */
    /* JADX WARNING: Removed duplicated region for block: B:393:0x0c4b  */
    /* JADX WARNING: Removed duplicated region for block: B:395:0x0c52  */
    /* JADX WARNING: Removed duplicated region for block: B:397:0x0c59 A[SYNTHETIC, Splitter:B:397:0x0c59] */
    /* JADX WARNING: Removed duplicated region for block: B:400:0x0c5e  */
    /* JADX WARNING: Removed duplicated region for block: B:423:0x0d7e  */
    /* JADX WARNING: Removed duplicated region for block: B:425:0x0d85  */
    /* JADX WARNING: Removed duplicated region for block: B:427:0x0d8c A[SYNTHETIC, Splitter:B:427:0x0d8c] */
    /* JADX WARNING: Removed duplicated region for block: B:430:0x0d91  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x02d1  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x02d8  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x02df A[SYNTHETIC, Splitter:B:77:0x02df] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02e4  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:256:0x077a=Splitter:B:256:0x077a, B:107:0x039d=Splitter:B:107:0x039d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r64 = this;
            r5 = 10
            android.os.Process.setThreadPriority(r5)
            r6 = 491(0x1eb, float:6.88E-43)
            r28 = 0
            r54 = 0
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r0 = r5
            int r0 = r0.mRedirectCount
            r51 = r0
            r46 = 0
            r38 = 0
            r11 = 0
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            java.lang.String r5 = r5.mMimeType
            r0 = r64
            r1 = r5
            java.lang.String r17 = r0.sanitizeMimeType(r1)
            r57 = 0
            r24 = 0
            r63 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            android.net.Uri r7 = com.cplatform.android.cmsurfclient.download.provider.DownloadSettings.getDownloadProviderContentUri()
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r7 = "/"
            java.lang.StringBuilder r5 = r5.append(r7)
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r7 = r0
            int r7 = r7.mId
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r5 = r5.toString()
            android.net.Uri r26 = android.net.Uri.parse(r5)
            r27 = 0
            r40 = 0
            r15 = 0
            r41 = 0
            r16 = 0
            r42 = 0
            r43 = 0
            r5 = 4096(0x1000, float:5.74E-42)
            r0 = r5
            byte[] r0 = new byte[r0]     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r29 = r0
            r23 = 0
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r7 = "power"
            java.lang.Object r49 = r5.getSystemService(r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.os.PowerManager r49 = (android.os.PowerManager) r49     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = 1
            java.lang.String r7 = "download/provider"
            r0 = r49
            r1 = r5
            r2 = r7
            android.os.PowerManager$WakeLock r63 = r0.newWakeLock(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r63.acquire()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r11 = r5.mFileName     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r11 == 0) goto L_0x01fd
            java.lang.String r5 = ""
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isFilenameValid(r11, r5)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x01e1
            r6 = 492(0x1ec, float:6.9E-43)
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r12 = 0
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r13 = r5.mMimeType     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r64
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r7 = 0
            r5.mHasActiveThread = r7
            if (r63 == 0) goto L_0x00b8
            r63.release()
            r63 = 0
        L_0x00b8:
            if (r24 == 0) goto L_0x00bf
            r24.close()
            r24 = 0
        L_0x00bf:
            if (r57 == 0) goto L_0x00c4
            r57.close()     // Catch:{ IOException -> 0x0e9a }
        L_0x00c4:
            if (r11 == 0) goto L_0x00d5
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusError(r6)
            if (r5 == 0) goto L_0x00f1
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x00d5:
            r5 = r64
            r7 = r28
            r8 = r54
            r9 = r51
            r10 = r38
            r12 = r46
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
        L_0x00f0:
            return
        L_0x00f1:
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x00d5
            java.lang.String r5 = "android.os.FileUtils"
            java.lang.Class r37 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0fd2 }
            java.lang.String r5 = "setPermissions"
            r7 = 4
            java.lang.Class[] r7 = new java.lang.Class[r7]     // Catch:{ Exception -> 0x0fd2 }
            r8 = 0
            java.lang.Class<java.lang.String> r9 = java.lang.String.class
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r8 = 1
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0fd2 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r8 = 2
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0fd2 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r8 = 3
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0fd2 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r0 = r37
            r1 = r5
            r2 = r7
            java.lang.reflect.Method r55 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x0fd2 }
            r5 = 0
            r7 = 4
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x0fd2 }
            r8 = 0
            r7[r8] = r11     // Catch:{ Exception -> 0x0fd2 }
            r8 = 1
            r9 = 420(0x1a4, float:5.89E-43)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0fd2 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r8 = 2
            r9 = -1
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0fd2 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r8 = 3
            r9 = -1
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0fd2 }
            r7[r8] = r9     // Catch:{ Exception -> 0x0fd2 }
            r0 = r55
            r1 = r5
            r2 = r7
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0fd2 }
        L_0x0145:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0153, SyncFailedException -> 0x017d, IOException -> 0x01a7, RuntimeException -> 0x01d1 }
            r7 = 1
            r5.<init>(r11, r7)     // Catch:{ FileNotFoundException -> 0x0153, SyncFailedException -> 0x017d, IOException -> 0x01a7, RuntimeException -> 0x01d1 }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0153, SyncFailedException -> 0x017d, IOException -> 0x01a7, RuntimeException -> 0x01d1 }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0153, SyncFailedException -> 0x017d, IOException -> 0x01a7, RuntimeException -> 0x01d1 }
            goto L_0x00d5
        L_0x0153:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "file "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r8 = " not found: "
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r5, r7)
            goto L_0x00d5
        L_0x017d:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "file "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r8 = " sync failed: "
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r5, r7)
            goto L_0x00d5
        L_0x01a7:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "IOException trying to sync "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r11)
            java.lang.String r8 = ": "
            java.lang.StringBuilder r7 = r7.append(r8)
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)
            java.lang.String r7 = r7.toString()
            android.util.Log.w(r5, r7)
            goto L_0x00d5
        L_0x01d1:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.String r7 = "exception while syncing file: "
            r0 = r5
            r1 = r7
            r2 = r31
            android.util.Log.w(r0, r1, r2)
            goto L_0x00d5
        L_0x01e1:
            java.io.File r33 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r33
            r1 = r11
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            boolean r5 = r33.exists()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 == 0) goto L_0x01fd
            long r35 = r33.length()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r13 = 0
            int r5 = (r35 > r13 ? 1 : (r35 == r13 ? 0 : -1))
            if (r5 != 0) goto L_0x02fc
            r33.delete()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r11 = 0
        L_0x01fd:
            r21 = r23
            r59 = 0
            boolean r61 = com.cplatform.android.cmsurfclient.SurfManagerActivity.isCMWap()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r5 = r64.userAgent()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r5
            r1 = r61
            com.cplatform.android.cmsurfclient.download.provider.AndroidHttpClient r24 = com.cplatform.android.cmsurfclient.download.provider.AndroidHttpClient.newInstance(r0, r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            org.apache.http.client.methods.HttpGet r52 = new org.apache.http.client.methods.HttpGet     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r5 = r5.mUri     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r52
            r1 = r5
            r0.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r5 = r5.mCookies     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 == 0) goto L_0x0238
            java.lang.String r5 = "Cookie"
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7 = r0
            java.lang.String r7 = r7.mCookies     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r52
            r1 = r5
            r2 = r7
            r0.addHeader(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0238:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r5 = r5.mReferer     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 == 0) goto L_0x0251
            java.lang.String r5 = "Referer"
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7 = r0
            java.lang.String r7 = r7.mReferer     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r52
            r1 = r5
            r2 = r7
            r0.addHeader(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0251:
            if (r27 == 0) goto L_0x0284
            if (r42 == 0) goto L_0x025f
            java.lang.String r5 = "If-Match"
            r0 = r52
            r1 = r5
            r2 = r42
            r0.addHeader(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x025f:
            java.lang.String r5 = "Range"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "bytes="
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r7
            r1 = r23
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "-"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r52
            r1 = r5
            r2 = r7
            r0.addHeader(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0284:
            r0 = r24
            r1 = r52
            org.apache.http.HttpResponse r53 = r0.execute(r1)     // Catch:{ IllegalArgumentException -> 0x034f, IOException -> 0x038d }
            org.apache.http.StatusLine r5 = r53.getStatusLine()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            int r56 = r5.getStatusCode()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = 503(0x1f7, float:7.05E-43)
            r0 = r56
            r1 = r5
            if (r0 != r1) goto L_0x0405
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7 = 5
            if (r5 >= r7) goto L_0x0405
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            java.lang.String r5 = "Retry-After"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f85, RuntimeException -> 0x0f18, all -> 0x0ebc }
            if (r39 == 0) goto L_0x0fd9
            java.lang.String r5 = r39.getValue()     // Catch:{ NumberFormatException -> 0x0fca }
            int r8 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x0fca }
            if (r8 >= 0) goto L_0x03e9
            r8 = 0
        L_0x02be:
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f92, RuntimeException -> 0x0f25, all -> 0x0ec8 }
            r10 = r38
            r12 = r46
            r9 = r51
        L_0x02c7:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r63 == 0) goto L_0x02d6
            r63.release()
            r63 = 0
        L_0x02d6:
            if (r24 == 0) goto L_0x02dd
            r24.close()
            r24 = 0
        L_0x02dd:
            if (r57 == 0) goto L_0x02e2
            r57.close()     // Catch:{ IOException -> 0x0e9d }
        L_0x02e2:
            if (r11 == 0) goto L_0x02f3
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusError(r6)
            if (r5 == 0) goto L_0x0a29
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x02f3:
            r5 = r64
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            goto L_0x00f0
        L_0x02fc:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r5 = r5.mETag     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x031b
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            boolean r5 = r5.mNoIntegrity     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x031b
            java.lang.String r5 = "download/provider"
            java.lang.String r7 = "can't resume interrupted non-resumable download"
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r33.delete()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r11 = 0
            goto L_0x01fd
        L_0x031b:
            java.io.FileOutputStream r58 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = 1
            r0 = r58
            r1 = r11
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r35
            int r0 = (int) r0
            r23 = r0
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
            r5 = r0
            int r5 = r5.mTotalBytes     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
            r7 = -1
            if (r5 == r7) goto L_0x033f
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
            r5 = r0
            int r5 = r5.mTotalBytes     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
            java.lang.String r41 = java.lang.Integer.toString(r5)     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
        L_0x033f:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
            r5 = r0
            r0 = r5
            java.lang.String r0 = r0.mETag     // Catch:{ FileNotFoundException -> 0x0f74, RuntimeException -> 0x0f07, all -> 0x0eac }
            r42 = r0
            r27 = 1
            r57 = r58
            goto L_0x01fd
        L_0x034f:
            r31 = move-exception
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "Arg exception trying to execute request for "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = 400(0x190, float:5.6E-43)
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x038d:
            r31 = move-exception
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x03aa
            r6 = 193(0xc1, float:2.7E-43)
            r7 = r28
        L_0x039d:
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f85, RuntimeException -> 0x0f18, all -> 0x0ebc }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x02c7
        L_0x03aa:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7 = 5
            if (r5 >= r7) goto L_0x03b8
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            goto L_0x039d
        L_0x03b8:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "IOException trying to execute request for "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = 495(0x1ef, float:6.94E-43)
            r7 = r28
            goto L_0x039d
        L_0x03e9:
            r5 = 30
            if (r8 >= r5) goto L_0x03fc
            r8 = 30
        L_0x03ef:
            java.util.Random r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.sRandom     // Catch:{ NumberFormatException -> 0x0fcf }
            r9 = 31
            int r5 = r5.nextInt(r9)     // Catch:{ NumberFormatException -> 0x0fcf }
            int r8 = r8 + r5
            int r8 = r8 * 1000
            goto L_0x02be
        L_0x03fc:
            r5 = 86400(0x15180, float:1.21072E-40)
            if (r8 <= r5) goto L_0x03ef
            r8 = 86400(0x15180, float:1.21072E-40)
            goto L_0x03ef
        L_0x0405:
            r5 = 301(0x12d, float:4.22E-43)
            r0 = r56
            r1 = r5
            if (r0 == r1) goto L_0x0421
            r5 = 302(0x12e, float:4.23E-43)
            r0 = r56
            r1 = r5
            if (r0 == r1) goto L_0x0421
            r5 = 303(0x12f, float:4.25E-43)
            r0 = r56
            r1 = r5
            if (r0 == r1) goto L_0x0421
            r5 = 307(0x133, float:4.3E-43)
            r0 = r56
            r1 = r5
            if (r0 != r1) goto L_0x04c4
        L_0x0421:
            r5 = 5
            r0 = r51
            r1 = r5
            if (r0 < r1) goto L_0x0457
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "too many redirects for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = 497(0x1f1, float:6.96E-43)
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0457:
            java.lang.String r5 = "Location"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x04c4
            java.lang.String r45 = r39.getValue()     // Catch:{ URISyntaxException -> 0x0491 }
            java.net.URI r5 = new java.net.URI     // Catch:{ URISyntaxException -> 0x0491 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ URISyntaxException -> 0x0491 }
            r7 = r0
            java.lang.String r7 = r7.mUri     // Catch:{ URISyntaxException -> 0x0491 }
            r5.<init>(r7)     // Catch:{ URISyntaxException -> 0x0491 }
            java.net.URI r7 = new java.net.URI     // Catch:{ URISyntaxException -> 0x0491 }
            r0 = r7
            r1 = r45
            r0.<init>(r1)     // Catch:{ URISyntaxException -> 0x0491 }
            java.net.URI r5 = r5.resolve(r7)     // Catch:{ URISyntaxException -> 0x0491 }
            java.lang.String r12 = r5.toString()     // Catch:{ URISyntaxException -> 0x0491 }
            int r9 = r51 + 1
            r6 = 193(0xc1, float:2.7E-43)
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f9d, RuntimeException -> 0x0f30, all -> 0x0ed2 }
            r10 = r38
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0491:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "Couldn't resolve redirect URI for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = 400(0x190, float:5.6E-43)
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x04c4:
            if (r27 != 0) goto L_0x04cd
            r5 = 200(0xc8, float:2.8E-43)
            r0 = r56
            r1 = r5
            if (r0 != r1) goto L_0x04d6
        L_0x04cd:
            if (r27 == 0) goto L_0x0539
            r5 = 206(0xce, float:2.89E-43)
            r0 = r56
            r1 = r5
            if (r0 == r1) goto L_0x0539
        L_0x04d6:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "http error "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r7
            r1 = r56
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = " for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusError(r56)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 == 0) goto L_0x0519
            r6 = r56
        L_0x050a:
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0519:
            r5 = 300(0x12c, float:4.2E-43)
            r0 = r56
            r1 = r5
            if (r0 < r1) goto L_0x052a
            r5 = 400(0x190, float:5.6E-43)
            r0 = r56
            r1 = r5
            if (r0 >= r1) goto L_0x052a
            r6 = 493(0x1ed, float:6.91E-43)
            goto L_0x050a
        L_0x052a:
            if (r27 == 0) goto L_0x0536
            r5 = 200(0xc8, float:2.8E-43)
            r0 = r56
            r1 = r5
            if (r0 != r1) goto L_0x0536
            r6 = 412(0x19c, float:5.77E-43)
            goto L_0x050a
        L_0x0536:
            r6 = 494(0x1ee, float:6.92E-43)
            goto L_0x050a
        L_0x0539:
            if (r27 != 0) goto L_0x06d5
            java.lang.String r5 = "Accept-Ranges"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x054a
            java.lang.String r40 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x054a:
            java.lang.String r5 = "Content-Disposition"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x0559
            java.lang.String r15 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0559:
            java.lang.String r5 = "Content-Location"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x0568
            java.lang.String r16 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0568:
            if (r17 != 0) goto L_0x0580
            java.lang.String r5 = "Content-Type"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x0580
            java.lang.String r5 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            r1 = r5
            java.lang.String r17 = r0.sanitizeMimeType(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0580:
            java.lang.String r5 = "ETag"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x058f
            java.lang.String r42 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x058f:
            java.lang.String r5 = "Transfer-Encoding"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x059e
            java.lang.String r43 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x059e:
            if (r43 != 0) goto L_0x05af
            java.lang.String r5 = "Content-Length"
            r0 = r53
            r1 = r5
            org.apache.http.Header r39 = r0.getFirstHeader(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r39 == 0) goto L_0x05af
            java.lang.String r41 = r39.getValue()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x05af:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            boolean r5 = r5.mNoIntegrity     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x05df
            if (r41 != 0) goto L_0x05df
            if (r43 == 0) goto L_0x05c7
            java.lang.String r5 = "chunked"
            r0 = r43
            r1 = r5
            boolean r5 = r0.equalsIgnoreCase(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x05df
        L_0x05c7:
            java.lang.String r5 = "download/provider"
            java.lang.String r7 = "can't know size of download, giving up"
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = 411(0x19b, float:5.76E-43)
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x05df:
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r12 = r0
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r13 = r5.mUri     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r14 = r5.mHint     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            r0 = r5
            int r0 = r0.mDestination     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r18 = r0
            if (r41 == 0) goto L_0x0625
            int r5 = java.lang.Integer.parseInt(r41)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r19 = r5
        L_0x0604:
            java.lang.String r20 = ""
            com.cplatform.android.cmsurfclient.download.provider.DownloadFileInfo r34 = com.cplatform.android.cmsurfclient.download.provider.Helpers.generateSaveFile(r12, r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r34
            java.lang.String r0 = r0.mFileName     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            if (r5 != 0) goto L_0x0629
            r0 = r34
            int r0 = r0.mStatus     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = r0
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0625:
            r5 = 0
            r19 = r5
            goto L_0x0604
        L_0x0629:
            r0 = r34
            java.lang.String r0 = r0.mFileName     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r11 = r0
            r0 = r34
            java.io.FileOutputStream r0 = r0.mStream     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r57 = r0
            android.content.ContentValues r62 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r62.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r5 = "_data"
            r0 = r62
            r1 = r5
            r2 = r11
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r42 == 0) goto L_0x064e
            java.lang.String r5 = "etag"
            r0 = r62
            r1 = r5
            r2 = r42
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x064e:
            r25 = -1
            if (r41 == 0) goto L_0x0656
            int r25 = java.lang.Integer.parseInt(r41)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0656:
            java.lang.String r5 = "total_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r25)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r62
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            java.lang.String r5 = r5.mHint     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 == 0) goto L_0x06b4
            r5 = 47
            int r50 = r11.lastIndexOf(r5)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r44 = 0
            if (r50 < 0) goto L_0x0764
            int r5 = r50 + 1
            java.lang.String r44 = r11.substring(r5)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x0680:
            java.lang.String r5 = "hint"
            r0 = r62
            r1 = r5
            r2 = r44
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r5 = "title"
            r0 = r62
            r1 = r5
            r2 = r44
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            boolean r5 = android.text.TextUtils.isEmpty(r17)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x06a5
            java.lang.String r5 = "application/octet-stream"
            r0 = r5
            r1 = r17
            boolean r5 = r0.equalsIgnoreCase(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 == 0) goto L_0x06b4
        L_0x06a5:
            java.lang.String r32 = com.cplatform.android.cmsurfclient.download.util.FileNameUtils.getFileExtension(r11)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.webkit.MimeTypeMap r5 = android.webkit.MimeTypeMap.getSingleton()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r5
            r1 = r32
            java.lang.String r17 = r0.getMimeTypeFromExtension(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x06b4:
            if (r17 == 0) goto L_0x06c0
            java.lang.String r5 = "mimetype"
            r0 = r62
            r1 = r5
            r2 = r17
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x06c0:
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r26
            r2 = r62
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
        L_0x06d5:
            org.apache.http.HttpEntity r5 = r53.getEntity()     // Catch:{ IOException -> 0x0768 }
            java.io.InputStream r30 = r5.getContent()     // Catch:{ IOException -> 0x0768 }
            r10 = r38
        L_0x06df:
            r0 = r30
            r1 = r29
            int r22 = r0.read(r1)     // Catch:{ IOException -> 0x07c6 }
            r5 = -1
            r0 = r22
            r1 = r5
            if (r0 != r1) goto L_0x08e1
            android.content.ContentValues r62 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r62.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r5 = "current_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r23)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r62
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r41 != 0) goto L_0x070e
            java.lang.String r5 = "total_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r23)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r62
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
        L_0x070e:
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r26
            r2 = r62
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r41 == 0) goto L_0x0a1d
            int r5 = java.lang.Integer.parseInt(r41)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r23
            r1 = r5
            if (r0 == r1) goto L_0x0a1d
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            boolean r5 = r5.mNoIntegrity     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r5 != 0) goto L_0x088a
            if (r42 != 0) goto L_0x088a
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = "mismatched content length for "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r6 = 411(0x19b, float:5.76E-43)
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0764:
            r44 = r11
            goto L_0x0680
        L_0x0768:
            r5 = move-exception
            r31 = r5
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            if (r5 != 0) goto L_0x0787
            r6 = 193(0xc1, float:2.7E-43)
            r7 = r28
        L_0x077a:
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0f85, RuntimeException -> 0x0f18, all -> 0x0ebc }
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x02c7
        L_0x0787:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7 = 5
            if (r5 >= r7) goto L_0x0795
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            goto L_0x077a
        L_0x0795:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = "IOException getting entity for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x0f65, RuntimeException -> 0x0c0f, all -> 0x0d68 }
            r6 = 495(0x1ef, float:6.94E-43)
            r7 = r28
            goto L_0x077a
        L_0x07c6:
            r31 = move-exception
            android.content.ContentValues r62 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r62.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r5 = "current_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r23)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r62
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r26
            r2 = r62
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            boolean r5 = r5.mNoIntegrity     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r5 != 0) goto L_0x083b
            if (r42 != 0) goto L_0x083b
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = "download IOException for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r5 = "download/provider"
            java.lang.String r7 = "can't resume interrupted download with no ETag"
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r6 = 412(0x19c, float:5.77E-43)
            r7 = r28
        L_0x0830:
            r52.abort()     // Catch:{ FileNotFoundException -> 0x0fa8, RuntimeException -> 0x0f48, all -> 0x0ee8 }
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x02c7
        L_0x083b:
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r5 != 0) goto L_0x084b
            r6 = 193(0xc1, float:2.7E-43)
            r7 = r28
            goto L_0x0830
        L_0x084b:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7 = 5
            if (r5 >= r7) goto L_0x0859
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            goto L_0x0830
        L_0x0859:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = "download IOException for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = " : "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r7
            r1 = r31
            java.lang.StringBuilder r7 = r0.append(r1)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r6 = 495(0x1ef, float:6.94E-43)
            r7 = r28
            goto L_0x0830
        L_0x088a:
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.isNetworkAvailable(r5)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r5 != 0) goto L_0x08a1
            r6 = 193(0xc1, float:2.7E-43)
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x08a1:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            int r5 = r5.mNumFailed     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7 = 5
            if (r5 >= r7) goto L_0x08b6
            r6 = 193(0xc1, float:2.7E-43)
            r7 = 1
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x02c7
        L_0x08b6:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = "closed socket for download "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r6 = 495(0x1ef, float:6.94E-43)
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x08e1:
            r10 = 1
            r58 = r57
        L_0x08e4:
            if (r58 != 0) goto L_0x0fd5
            java.io.FileOutputStream r57 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x095a, FileNotFoundException -> 0x0fb3, RuntimeException -> 0x0f53, all -> 0x0ef2 }
            r5 = 1
            r0 = r57
            r1 = r11
            r2 = r5
            r0.<init>(r1, r2)     // Catch:{ IOException -> 0x095a, FileNotFoundException -> 0x0fb3, RuntimeException -> 0x0f53, all -> 0x0ef2 }
        L_0x08f0:
            r5 = 0
            r0 = r57
            r1 = r29
            r2 = r5
            r3 = r22
            r0.write(r1, r2, r3)     // Catch:{ IOException -> 0x0fc5 }
            int r23 = r23 + r22
            long r47 = java.lang.System.currentTimeMillis()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            int r5 = r23 - r21
            r7 = 4096(0x1000, float:5.74E-42)
            if (r5 <= r7) goto L_0x093a
            long r13 = r47 - r59
            r18 = 1500(0x5dc, double:7.41E-321)
            int r5 = (r13 > r18 ? 1 : (r13 == r18 ? 0 : -1))
            if (r5 <= 0) goto L_0x093a
            android.content.ContentValues r62 = new android.content.ContentValues     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r62.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r5 = "current_bytes"
            java.lang.Integer r7 = java.lang.Integer.valueOf(r23)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r62
            r1 = r5
            r2 = r7
            r0.put(r1, r2)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            android.content.ContentResolver r5 = r5.getContentResolver()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7 = 0
            r8 = 0
            r0 = r5
            r1 = r26
            r2 = r62
            r3 = r7
            r4 = r8
            r0.update(r1, r2, r3, r4)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r21 = r23
            r59 = r47
        L_0x093a:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            monitor-enter(r5)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ all -> 0x09b3 }
            r7 = r0
            int r7 = r7.mControl     // Catch:{ all -> 0x09b3 }
            r8 = 1
            if (r7 != r8) goto L_0x097c
            r6 = 193(0xc1, float:2.7E-43)
            r52.abort()     // Catch:{ all -> 0x09b3 }
            monitor-exit(r5)     // Catch:{ all -> 0x09b3 }
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x095a:
            r5 = move-exception
            r31 = r5
            r57 = r58
        L_0x095f:
            r0 = r64
            android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            r13 = 4096(0x1000, double:2.0237E-320)
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Helpers.discardPurgeableFiles(r5, r13)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            if (r5 != 0) goto L_0x0978
            r6 = 492(0x1ec, float:6.9E-43)
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0978:
            r58 = r57
            goto L_0x08e4
        L_0x097c:
            monitor-exit(r5)     // Catch:{ all -> 0x09b3 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r5 = r0
            int r5 = r5.mStatus     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7 = 490(0x1ea, float:6.87E-43)
            if (r5 != r7) goto L_0x06df
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r7.<init>()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r8 = "canceled id "
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r8 = r0
            int r8 = r8.mId     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            java.lang.String r7 = r7.toString()     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            android.util.Log.d(r5, r7)     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
            r6 = 490(0x1ea, float:6.87E-43)
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x09b3:
            r7 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x09b3 }
            throw r7     // Catch:{ FileNotFoundException -> 0x09b6, RuntimeException -> 0x0f3b, all -> 0x0edc }
        L_0x09b6:
            r5 = move-exception
            r31 = r5
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
        L_0x09c1:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0f00 }
            r13.<init>()     // Catch:{ all -> 0x0f00 }
            java.lang.String r14 = "FileNotFoundException for "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0f00 }
            java.lang.StringBuilder r13 = r13.append(r11)     // Catch:{ all -> 0x0f00 }
            java.lang.String r14 = " : "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0f00 }
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)     // Catch:{ all -> 0x0f00 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0f00 }
            android.util.Log.d(r5, r13)     // Catch:{ all -> 0x0f00 }
            r6 = 492(0x1ec, float:6.9E-43)
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r63 == 0) goto L_0x09f7
            r63.release()
            r63 = 0
        L_0x09f7:
            if (r24 == 0) goto L_0x09fe
            r24.close()
            r24 = 0
        L_0x09fe:
            if (r57 == 0) goto L_0x0a03
            r57.close()     // Catch:{ IOException -> 0x0ea0 }
        L_0x0a03:
            if (r11 == 0) goto L_0x0a14
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusError(r6)
            if (r5 == 0) goto L_0x0b1c
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x0a14:
            r5 = r64
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            goto L_0x00f0
        L_0x0a1d:
            r6 = 200(0xc8, float:2.8E-43)
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x02c7
        L_0x0a29:
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x02f3
            java.lang.String r5 = "android.os.FileUtils"
            java.lang.Class r37 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0fc2 }
            java.lang.String r5 = "setPermissions"
            r13 = 4
            java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x0fc2 }
            r14 = 0
            java.lang.Class<java.lang.String> r18 = java.lang.String.class
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r14 = 1
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0fc2 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r14 = 2
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0fc2 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r14 = 3
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0fc2 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r0 = r37
            r1 = r5
            r2 = r13
            java.lang.reflect.Method r55 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x0fc2 }
            r5 = 0
            r13 = 4
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x0fc2 }
            r14 = 0
            r13[r14] = r11     // Catch:{ Exception -> 0x0fc2 }
            r14 = 1
            r18 = 420(0x1a4, float:5.89E-43)
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0fc2 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r14 = 2
            r18 = -1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0fc2 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r14 = 3
            r18 = -1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0fc2 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0fc2 }
            r0 = r55
            r1 = r5
            r2 = r13
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0fc2 }
        L_0x0a7f:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0a8e, SyncFailedException -> 0x0ab8, IOException -> 0x0ae2, RuntimeException -> 0x0b0c }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0a8e, SyncFailedException -> 0x0ab8, IOException -> 0x0ae2, RuntimeException -> 0x0b0c }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0a8e, SyncFailedException -> 0x0ab8, IOException -> 0x0ae2, RuntimeException -> 0x0b0c }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0a8e, SyncFailedException -> 0x0ab8, IOException -> 0x0ae2, RuntimeException -> 0x0b0c }
            goto L_0x02f3
        L_0x0a8e:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "file "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x02f3
        L_0x0ab8:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "file "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x02f3
        L_0x0ae2:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "IOException trying to sync "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = ": "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x02f3
        L_0x0b0c:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r31
            android.util.Log.w(r0, r1, r2)
            goto L_0x02f3
        L_0x0b1c:
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x0a14
            java.lang.String r5 = "android.os.FileUtils"
            java.lang.Class r37 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0f62 }
            java.lang.String r5 = "setPermissions"
            r13 = 4
            java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x0f62 }
            r14 = 0
            java.lang.Class<java.lang.String> r18 = java.lang.String.class
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r14 = 1
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0f62 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r14 = 2
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0f62 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r14 = 3
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0f62 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r0 = r37
            r1 = r5
            r2 = r13
            java.lang.reflect.Method r55 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x0f62 }
            r5 = 0
            r13 = 4
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x0f62 }
            r14 = 0
            r13[r14] = r11     // Catch:{ Exception -> 0x0f62 }
            r14 = 1
            r18 = 420(0x1a4, float:5.89E-43)
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0f62 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r14 = 2
            r18 = -1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0f62 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r14 = 3
            r18 = -1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0f62 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f62 }
            r0 = r55
            r1 = r5
            r2 = r13
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0f62 }
        L_0x0b72:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0b81, SyncFailedException -> 0x0bab, IOException -> 0x0bd5, RuntimeException -> 0x0bff }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0b81, SyncFailedException -> 0x0bab, IOException -> 0x0bd5, RuntimeException -> 0x0bff }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0b81, SyncFailedException -> 0x0bab, IOException -> 0x0bd5, RuntimeException -> 0x0bff }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0b81, SyncFailedException -> 0x0bab, IOException -> 0x0bd5, RuntimeException -> 0x0bff }
            goto L_0x0a14
        L_0x0b81:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "file "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0a14
        L_0x0bab:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "file "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0a14
        L_0x0bd5:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "IOException trying to sync "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = ": "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0a14
        L_0x0bff:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r31
            android.util.Log.w(r0, r1, r2)
            goto L_0x0a14
        L_0x0c0f:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
        L_0x0c1c:
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x0f00 }
            r13.<init>()     // Catch:{ all -> 0x0f00 }
            java.lang.String r14 = "Exception for id "
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0f00 }
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo     // Catch:{ all -> 0x0f00 }
            r14 = r0
            int r14 = r14.mId     // Catch:{ all -> 0x0f00 }
            java.lang.StringBuilder r13 = r13.append(r14)     // Catch:{ all -> 0x0f00 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x0f00 }
            r0 = r5
            r1 = r13
            r2 = r31
            android.util.Log.d(r0, r1, r2)     // Catch:{ all -> 0x0f00 }
            r6 = 491(0x1eb, float:6.88E-43)
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r63 == 0) goto L_0x0c50
            r63.release()
            r63 = 0
        L_0x0c50:
            if (r24 == 0) goto L_0x0c57
            r24.close()
            r24 = 0
        L_0x0c57:
            if (r57 == 0) goto L_0x0c5c
            r57.close()     // Catch:{ IOException -> 0x0ea3 }
        L_0x0c5c:
            if (r11 == 0) goto L_0x0c6d
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusError(r6)
            if (r5 == 0) goto L_0x0c76
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x0c6d:
            r5 = r64
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            goto L_0x00f0
        L_0x0c76:
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x0c6d
            java.lang.String r5 = "android.os.FileUtils"
            java.lang.Class r37 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0f04 }
            java.lang.String r5 = "setPermissions"
            r13 = 4
            java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x0f04 }
            r14 = 0
            java.lang.Class<java.lang.String> r18 = java.lang.String.class
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r14 = 1
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0f04 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r14 = 2
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0f04 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r14 = 3
            java.lang.Class r18 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0f04 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r0 = r37
            r1 = r5
            r2 = r13
            java.lang.reflect.Method r55 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x0f04 }
            r5 = 0
            r13 = 4
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x0f04 }
            r14 = 0
            r13[r14] = r11     // Catch:{ Exception -> 0x0f04 }
            r14 = 1
            r18 = 420(0x1a4, float:5.89E-43)
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0f04 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r14 = 2
            r18 = -1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0f04 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r14 = 3
            r18 = -1
            java.lang.Integer r18 = java.lang.Integer.valueOf(r18)     // Catch:{ Exception -> 0x0f04 }
            r13[r14] = r18     // Catch:{ Exception -> 0x0f04 }
            r0 = r55
            r1 = r5
            r2 = r13
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0f04 }
        L_0x0ccc:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0cda, SyncFailedException -> 0x0d04, IOException -> 0x0d2e, RuntimeException -> 0x0d58 }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0cda, SyncFailedException -> 0x0d04, IOException -> 0x0d2e, RuntimeException -> 0x0d58 }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0cda, SyncFailedException -> 0x0d04, IOException -> 0x0d2e, RuntimeException -> 0x0d58 }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0cda, SyncFailedException -> 0x0d04, IOException -> 0x0d2e, RuntimeException -> 0x0d58 }
            goto L_0x0c6d
        L_0x0cda:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "file "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0c6d
        L_0x0d04:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "file "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0c6d
        L_0x0d2e:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "IOException trying to sync "
            java.lang.StringBuilder r13 = r13.append(r14)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r14 = ": "
            java.lang.StringBuilder r13 = r13.append(r14)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0c6d
        L_0x0d58:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r31
            android.util.Log.w(r0, r1, r2)
            goto L_0x0c6d
        L_0x0d68:
            r5 = move-exception
            r14 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
        L_0x0d74:
            r0 = r64
            com.cplatform.android.cmsurfclient.download.provider.DownloadInfo r0 = r0.mInfo
            r5 = r0
            r13 = 0
            r5.mHasActiveThread = r13
            if (r63 == 0) goto L_0x0d83
            r63.release()
            r63 = 0
        L_0x0d83:
            if (r24 == 0) goto L_0x0d8a
            r24.close()
            r24 = 0
        L_0x0d8a:
            if (r57 == 0) goto L_0x0d8f
            r57.close()     // Catch:{ IOException -> 0x0ea6 }
        L_0x0d8f:
            if (r11 == 0) goto L_0x0da0
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusError(r6)
            if (r5 == 0) goto L_0x0da8
            java.io.File r5 = new java.io.File
            r5.<init>(r11)
            r5.delete()
            r11 = 0
        L_0x0da0:
            r5 = r64
            r13 = r17
            r5.notifyDownloadCompleted(r6, r7, r8, r9, r10, r11, r12, r13)
            throw r14
        L_0x0da8:
            boolean r5 = com.cplatform.android.cmsurfclient.download.provider.Downloads.isStatusSuccess(r6)
            if (r5 == 0) goto L_0x0da0
            java.lang.String r5 = "android.os.FileUtils"
            java.lang.Class r37 = java.lang.Class.forName(r5)     // Catch:{ Exception -> 0x0ea9 }
            java.lang.String r5 = "setPermissions"
            r13 = 4
            java.lang.Class[] r13 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x0ea9 }
            r15 = 0
            java.lang.Class<java.lang.String> r16 = java.lang.String.class
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r15 = 1
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0ea9 }
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r15 = 2
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0ea9 }
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r15 = 3
            java.lang.Class r16 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0ea9 }
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r0 = r37
            r1 = r5
            r2 = r13
            java.lang.reflect.Method r55 = r0.getMethod(r1, r2)     // Catch:{ Exception -> 0x0ea9 }
            r5 = 0
            r13 = 4
            java.lang.Object[] r13 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x0ea9 }
            r15 = 0
            r13[r15] = r11     // Catch:{ Exception -> 0x0ea9 }
            r15 = 1
            r16 = 420(0x1a4, float:5.89E-43)
            java.lang.Integer r16 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x0ea9 }
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r15 = 2
            r16 = -1
            java.lang.Integer r16 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x0ea9 }
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r15 = 3
            r16 = -1
            java.lang.Integer r16 = java.lang.Integer.valueOf(r16)     // Catch:{ Exception -> 0x0ea9 }
            r13[r15] = r16     // Catch:{ Exception -> 0x0ea9 }
            r0 = r55
            r1 = r5
            r2 = r13
            r0.invoke(r1, r2)     // Catch:{ Exception -> 0x0ea9 }
        L_0x0dfe:
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0e0c, SyncFailedException -> 0x0e36, IOException -> 0x0e60, RuntimeException -> 0x0e8a }
            r13 = 1
            r5.<init>(r11, r13)     // Catch:{ FileNotFoundException -> 0x0e0c, SyncFailedException -> 0x0e36, IOException -> 0x0e60, RuntimeException -> 0x0e8a }
            java.io.FileDescriptor r5 = r5.getFD()     // Catch:{ FileNotFoundException -> 0x0e0c, SyncFailedException -> 0x0e36, IOException -> 0x0e60, RuntimeException -> 0x0e8a }
            r5.sync()     // Catch:{ FileNotFoundException -> 0x0e0c, SyncFailedException -> 0x0e36, IOException -> 0x0e60, RuntimeException -> 0x0e8a }
            goto L_0x0da0
        L_0x0e0c:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r15 = "file "
            java.lang.StringBuilder r13 = r13.append(r15)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r15 = " not found: "
            java.lang.StringBuilder r13 = r13.append(r15)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0da0
        L_0x0e36:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r15 = "file "
            java.lang.StringBuilder r13 = r13.append(r15)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r15 = " sync failed: "
            java.lang.StringBuilder r13 = r13.append(r15)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0da0
        L_0x0e60:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r15 = "IOException trying to sync "
            java.lang.StringBuilder r13 = r13.append(r15)
            java.lang.StringBuilder r13 = r13.append(r11)
            java.lang.String r15 = ": "
            java.lang.StringBuilder r13 = r13.append(r15)
            r0 = r13
            r1 = r31
            java.lang.StringBuilder r13 = r0.append(r1)
            java.lang.String r13 = r13.toString()
            android.util.Log.w(r5, r13)
            goto L_0x0da0
        L_0x0e8a:
            r5 = move-exception
            r31 = r5
            java.lang.String r5 = "download/provider"
            java.lang.String r13 = "exception while syncing file: "
            r0 = r5
            r1 = r13
            r2 = r31
            android.util.Log.w(r0, r1, r2)
            goto L_0x0da0
        L_0x0e9a:
            r5 = move-exception
            goto L_0x00c4
        L_0x0e9d:
            r5 = move-exception
            goto L_0x02e2
        L_0x0ea0:
            r5 = move-exception
            goto L_0x0a03
        L_0x0ea3:
            r5 = move-exception
            goto L_0x0c5c
        L_0x0ea6:
            r5 = move-exception
            goto L_0x0d8f
        L_0x0ea9:
            r5 = move-exception
            goto L_0x0dfe
        L_0x0eac:
            r5 = move-exception
            r14 = r5
            r57 = r58
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x0d74
        L_0x0ebc:
            r5 = move-exception
            r14 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x0d74
        L_0x0ec8:
            r5 = move-exception
            r14 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            goto L_0x0d74
        L_0x0ed2:
            r5 = move-exception
            r14 = r5
            r10 = r38
            r8 = r54
            r7 = r28
            goto L_0x0d74
        L_0x0edc:
            r5 = move-exception
            r14 = r5
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x0d74
        L_0x0ee8:
            r5 = move-exception
            r14 = r5
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x0d74
        L_0x0ef2:
            r5 = move-exception
            r14 = r5
            r57 = r58
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x0d74
        L_0x0f00:
            r5 = move-exception
            r14 = r5
            goto L_0x0d74
        L_0x0f04:
            r5 = move-exception
            goto L_0x0ccc
        L_0x0f07:
            r5 = move-exception
            r31 = r5
            r57 = r58
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x0c1c
        L_0x0f18:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x0c1c
        L_0x0f25:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            goto L_0x0c1c
        L_0x0f30:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r8 = r54
            r7 = r28
            goto L_0x0c1c
        L_0x0f3b:
            r5 = move-exception
            r31 = r5
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x0c1c
        L_0x0f48:
            r5 = move-exception
            r31 = r5
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x0c1c
        L_0x0f53:
            r5 = move-exception
            r31 = r5
            r57 = r58
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x0c1c
        L_0x0f62:
            r5 = move-exception
            goto L_0x0b72
        L_0x0f65:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x09c1
        L_0x0f74:
            r5 = move-exception
            r31 = r5
            r57 = r58
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x09c1
        L_0x0f85:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x09c1
        L_0x0f92:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r12 = r46
            r9 = r51
            goto L_0x09c1
        L_0x0f9d:
            r5 = move-exception
            r31 = r5
            r10 = r38
            r8 = r54
            r7 = r28
            goto L_0x09c1
        L_0x0fa8:
            r5 = move-exception
            r31 = r5
            r12 = r46
            r9 = r51
            r8 = r54
            goto L_0x09c1
        L_0x0fb3:
            r5 = move-exception
            r31 = r5
            r57 = r58
            r12 = r46
            r9 = r51
            r8 = r54
            r7 = r28
            goto L_0x09c1
        L_0x0fc2:
            r5 = move-exception
            goto L_0x0a7f
        L_0x0fc5:
            r5 = move-exception
            r31 = r5
            goto L_0x095f
        L_0x0fca:
            r5 = move-exception
            r8 = r54
            goto L_0x02be
        L_0x0fcf:
            r5 = move-exception
            goto L_0x02be
        L_0x0fd2:
            r5 = move-exception
            goto L_0x0145
        L_0x0fd5:
            r57 = r58
            goto L_0x08f0
        L_0x0fd9:
            r8 = r54
            goto L_0x02be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.cplatform.android.cmsurfclient.download.provider.DownloadThread.run():void");
    }

    private void notifyDownloadCompleted(int status, boolean countRetry, int retryAfter, int redirectCount, boolean gotData, String filename, String uri, String mimeType) {
        notifyThroughDatabase(status, countRetry, retryAfter, redirectCount, gotData, filename, uri, mimeType);
        if (Downloads.isStatusCompleted(status)) {
            notifyThroughIntent();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private void notifyThroughDatabase(int status, boolean countRetry, int retryAfter, int redirectCount, boolean gotData, String filename, String uri, String mimeType) {
        ContentValues values = new ContentValues();
        values.put(Downloads.COLUMN_STATUS, Integer.valueOf(status));
        values.put(Downloads._DATA, filename);
        if (uri != null) {
            values.put(Downloads.COLUMN_URI, uri);
        }
        values.put(Downloads.COLUMN_MIME_TYPE, mimeType);
        values.put(Downloads.COLUMN_LAST_MODIFICATION, Long.valueOf(System.currentTimeMillis()));
        values.put(Constants.RETRY_AFTER_X_REDIRECT_COUNT, Integer.valueOf((redirectCount << 28) + retryAfter));
        if (!countRetry) {
            values.put(Constants.FAILED_CONNECTIONS, (Integer) 0);
        } else if (gotData) {
            values.put(Constants.FAILED_CONNECTIONS, (Integer) 1);
        } else {
            values.put(Constants.FAILED_CONNECTIONS, Integer.valueOf(this.mInfo.mNumFailed + 1));
        }
        this.mContext.getContentResolver().update(ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), (long) this.mInfo.mId), values, null, null);
    }

    private void notifyThroughIntent() {
        this.mInfo.sendIntentIfRequested(Uri.parse(DownloadSettings.getDownloadProviderContentUri() + "/" + this.mInfo.mId), this.mContext);
    }

    private String sanitizeMimeType(String mimeType) {
        try {
            String mimeType2 = mimeType.trim().toLowerCase(Locale.ENGLISH);
            int semicolonIndex = mimeType2.indexOf(59);
            if (semicolonIndex != -1) {
                mimeType2 = mimeType2.substring(0, semicolonIndex);
            }
            return mimeType2;
        } catch (NullPointerException e) {
            return null;
        }
    }
}
