package com.cplatform.android.cmsurfclient.download.provider;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.cplatform.android.cmsurfclient.network.ophone.QueryApList;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Helpers {
    private static final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment;\\s*filename\\s*=\\s*\"([^\"]*)\"");
    public static Random sRandom = new Random(SystemClock.uptimeMillis());

    private static class Lexer {
        public static final int TOKEN_AND_OR = 3;
        public static final int TOKEN_CLOSE_PAREN = 2;
        public static final int TOKEN_COLUMN = 4;
        public static final int TOKEN_COMPARE = 5;
        public static final int TOKEN_END = 9;
        public static final int TOKEN_IS = 7;
        public static final int TOKEN_NULL = 8;
        public static final int TOKEN_OPEN_PAREN = 1;
        public static final int TOKEN_START = 0;
        public static final int TOKEN_VALUE = 6;
        private final Set<String> mAllowedColumns;
        private final char[] mChars;
        private int mCurrentToken = 0;
        private int mOffset = 0;
        private final String mSelection;

        private static final boolean isIdentifierStart(char c) {
            return c == '_' || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z');
        }

        private static final boolean isIdentifierChar(char c) {
            return c == '_' || (c >= 'A' && c <= 'Z') || ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'));
        }

        public int currentToken() {
            return this.mCurrentToken;
        }

        public Lexer(String selection, Set<String> allowedColumns) {
            this.mSelection = selection;
            this.mAllowedColumns = allowedColumns;
            this.mChars = new char[this.mSelection.length()];
            this.mSelection.getChars(0, this.mChars.length, this.mChars, 0);
            advance();
        }

        public void advance() {
            char[] chars = this.mChars;
            while (this.mOffset < chars.length && chars[this.mOffset] == ' ') {
                this.mOffset++;
            }
            if (this.mOffset == chars.length) {
                this.mCurrentToken = 9;
            } else if (chars[this.mOffset] == '(') {
                this.mOffset++;
                this.mCurrentToken = 1;
            } else if (chars[this.mOffset] == ')') {
                this.mOffset++;
                this.mCurrentToken = 2;
            } else if (chars[this.mOffset] == '?') {
                this.mOffset++;
                this.mCurrentToken = 6;
            } else if (chars[this.mOffset] == '=') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset < chars.length && chars[this.mOffset] == '=') {
                    this.mOffset++;
                }
            } else if (chars[this.mOffset] == '>') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset < chars.length && chars[this.mOffset] == '=') {
                    this.mOffset++;
                }
            } else if (chars[this.mOffset] == '<') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset >= chars.length) {
                    return;
                }
                if (chars[this.mOffset] == '=' || chars[this.mOffset] == '>') {
                    this.mOffset++;
                }
            } else if (chars[this.mOffset] == '!') {
                this.mOffset++;
                this.mCurrentToken = 5;
                if (this.mOffset >= chars.length || chars[this.mOffset] != '=') {
                    throw new IllegalArgumentException("Unexpected character after !");
                }
                this.mOffset++;
            } else if (isIdentifierStart(chars[this.mOffset])) {
                int startOffset = this.mOffset;
                this.mOffset++;
                while (this.mOffset < chars.length && isIdentifierChar(chars[this.mOffset])) {
                    this.mOffset++;
                }
                String word = this.mSelection.substring(startOffset, this.mOffset);
                if (this.mOffset - startOffset <= 4) {
                    if (word.equals("IS")) {
                        this.mCurrentToken = 7;
                        return;
                    } else if (word.equals("OR") || word.equals("AND")) {
                        this.mCurrentToken = 3;
                        return;
                    } else if (word.equals("NULL")) {
                        this.mCurrentToken = 8;
                        return;
                    }
                }
                if (this.mAllowedColumns.contains(word)) {
                    this.mCurrentToken = 4;
                    return;
                }
                throw new IllegalArgumentException("unrecognized column or keyword");
            } else if (chars[this.mOffset] == '\'') {
                this.mOffset++;
                while (this.mOffset < chars.length) {
                    if (chars[this.mOffset] == '\'') {
                        if (this.mOffset + 1 >= chars.length || chars[this.mOffset + 1] != '\'') {
                            break;
                        }
                        this.mOffset++;
                    }
                    this.mOffset++;
                }
                if (this.mOffset == chars.length) {
                    throw new IllegalArgumentException("unterminated string");
                }
                this.mOffset++;
                this.mCurrentToken = 6;
            } else {
                throw new IllegalArgumentException("illegal character");
            }
        }
    }

    private Helpers() {
    }

    private static String chooseExtensionFromFilename(String mimeType, int destination, String filename, int dotIndex) {
        String extension = null;
        if (mimeType != null) {
            String typeFromExt = MimeTypeMap.getSingleton().getMimeTypeFromExtension(filename.substring(filename.lastIndexOf(46) + 1));
            if ((typeFromExt == null || !typeFromExt.equalsIgnoreCase(mimeType)) && (extension = chooseExtensionFromMimeType(mimeType, false)) != null) {
            }
        }
        if (extension == null) {
            return filename.substring(dotIndex);
        }
        return extension;
    }

    private static String chooseExtensionFromMimeType(String mimeType, boolean useDefaults) {
        String extension = null;
        if (!(mimeType == null || (extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mimeType)) == null)) {
            extension = "." + extension;
        }
        if (extension != null) {
            return extension;
        }
        if (mimeType == null || !mimeType.toLowerCase().startsWith("text/")) {
            if (useDefaults) {
                return Constants.DEFAULT_DL_BINARY_EXTENSION;
            }
            return extension;
        } else if (mimeType.equalsIgnoreCase("text/html")) {
            return Constants.DEFAULT_DL_HTML_EXTENSION;
        } else {
            if (useDefaults) {
                return Constants.DEFAULT_DL_TEXT_EXTENSION;
            }
            return extension;
        }
    }

    private static String chooseFilename(String url, String hint, String contentDisposition, String contentLocation, int destination) {
        String decodedUrl;
        int index;
        String decodedContentLocation;
        int index2;
        String filename = null;
        if (0 == 0 && hint != null && !hint.endsWith("/")) {
            int index3 = hint.lastIndexOf(47) + 1;
            filename = index3 > 0 ? hint.substring(index3) : hint;
        }
        if (filename == null && contentDisposition != null && (filename = parseContentDisposition(contentDisposition)) != null && (index2 = filename.lastIndexOf(47) + 1) > 0) {
            filename = filename.substring(index2);
        }
        if (filename == null && contentLocation != null && (decodedContentLocation = Uri.decode(contentLocation)) != null && !decodedContentLocation.endsWith("/") && decodedContentLocation.indexOf(63) < 0) {
            int index4 = decodedContentLocation.lastIndexOf(47) + 1;
            if (index4 > 0) {
                filename = decodedContentLocation.substring(index4);
            } else {
                filename = decodedContentLocation;
            }
        }
        if (filename == null && (decodedUrl = Uri.decode(url)) != null && !decodedUrl.endsWith("/") && decodedUrl.indexOf(63) < 0 && (index = decodedUrl.lastIndexOf(47) + 1) > 0) {
            filename = decodedUrl.substring(index);
        }
        if (filename == null) {
            filename = Constants.DEFAULT_DL_FILENAME;
        }
        return filename.replaceAll("[^a-zA-Z0-9\\.\\-_]+", "_");
    }

    private static String chooseUniqueFilename(int destination, String filename, String extension, boolean recoveryDir) {
        String fullFilename = filename + extension;
        if (!new File(fullFilename).exists() && (!recoveryDir || (destination != 1 && destination != 2 && destination != 3))) {
            return fullFilename;
        }
        String filename2 = filename + Constants.FILENAME_SEQUENCE_SEPARATOR;
        int sequence = 1;
        for (int magnitude = 1; magnitude < 1000000000; magnitude *= 10) {
            for (int iteration = 0; iteration < 9; iteration++) {
                String fullFilename2 = filename2 + sequence + extension;
                if (!new File(fullFilename2).exists()) {
                    return fullFilename2;
                }
                sequence += sRandom.nextInt(magnitude) + 1;
            }
        }
        return null;
    }

    /* JADX INFO: finally extract failed */
    public static final boolean discardPurgeableFiles(Context context, long targetBytes) {
        Cursor cursor = context.getContentResolver().query(DownloadSettings.getDownloadProviderContentUri(), null, "( status = '200' AND destination = '2' )", null, Downloads.COLUMN_LAST_MODIFICATION);
        if (cursor == null) {
            return false;
        }
        long totalFreed = 0;
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast() && totalFreed < targetBytes) {
                File file = new File(cursor.getString(cursor.getColumnIndex(Downloads._DATA)));
                totalFreed += file.length();
                file.delete();
                context.getContentResolver().delete(ContentUris.withAppendedId(DownloadSettings.getDownloadProviderContentUri(), cursor.getLong(cursor.getColumnIndex(QueryApList.Carriers._ID))), null, null);
                cursor.moveToNext();
            }
            cursor.close();
            return totalFreed > 0;
        } catch (Throwable th) {
            cursor.close();
            throw th;
        }
    }

    /* JADX INFO: Multiple debug info for r5v3 int: [D('dotIndex' int), D('extension' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r1v1 java.io.File: [D('base' java.io.File), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r1v7 java.lang.String: [D('fullFilename' java.lang.String), D('filename' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r5v9 java.lang.String: [D('dotIndex' int), D('extension' java.lang.String)] */
    public static DownloadFileInfo generateSaveFile(Context context, String url, String hint, String contentDisposition, String contentLocation, String mimeType, int destination, int contentLength, String s5) throws FileNotFoundException {
        String contentDisposition2;
        String extension;
        String hint2;
        if ((destination == 0 || destination == 2) && mimeType == null) {
            Log.d(Constants.TAG, "external download with no mime type not allowed");
            return new DownloadFileInfo(null, null, Downloads.STATUS_NOT_ACCEPTABLE);
        }
        String filename = chooseFilename(url, hint, contentDisposition, contentLocation, destination);
        int dotIndex = filename.indexOf(46);
        if (dotIndex < 0) {
            hint2 = contentDisposition;
            contentDisposition2 = chooseExtensionFromMimeType(mimeType, true);
            extension = filename;
        } else {
            contentDisposition2 = chooseExtensionFromFilename(mimeType, destination, filename, dotIndex);
            extension = filename.substring(0, dotIndex);
            String tmp = hint;
            hint2 = url;
            url = tmp;
        }
        File base = getBaseFile(context, destination, contentLength, s5);
        if (base == null) {
            return new DownloadFileInfo(null, null, Downloads.STATUS_FILE_ERROR);
        }
        String filename2 = chooseUniqueFilename(destination, base.getPath() + File.separator + extension, contentDisposition2, Constants.RECOVERY_DIRECTORY.equalsIgnoreCase(hint2 + url));
        if (filename2 != null) {
            return new DownloadFileInfo(filename2, new FileOutputStream(filename2), 0);
        }
        return new DownloadFileInfo(null, null, Downloads.STATUS_FILE_ERROR);
    }

    public static DownloadFileInfo generateSaveFileWithAppointName(Context context, String s, int i, int j, String s1, String s2) throws FileNotFoundException {
        if ((i == 0 || i == 2) && s == null) {
            Log.d(Constants.TAG, "external download with no mime type not allowed");
            return new DownloadFileInfo(null, null, Downloads.STATUS_NOT_ACCEPTABLE);
        }
        File file = getBaseFile(context, i, j, s1);
        if (file == null) {
            return new DownloadFileInfo(null, null, Downloads.STATUS_FILE_ERROR);
        }
        return new DownloadFileInfo(s2, new FileOutputStream(file.getPath() + File.separator + s2), 0);
    }

    /* JADX INFO: Multiple debug info for r14v1 java.io.File: [D('base' java.io.File), D('s' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r0v2 java.lang.String: [D('base' java.io.File), D('root' java.lang.String)] */
    private static File getBaseFile(Context context, int destination, int contentLength, String s) {
        File base;
        if (destination == 1 || destination == 2 || destination == 3) {
            File base2 = Environment.getDownloadCacheDirectory();
            StatFs stat = new StatFs(base2.getPath());
            int blockSize = stat.getBlockSize();
            while (true) {
                int availableBlocks = stat.getAvailableBlocks();
                if (((long) blockSize) * (((long) availableBlocks) - 4) >= ((long) contentLength)) {
                    base = base2;
                    break;
                } else if (discardPurgeableFiles(context, ((long) contentLength) - (((long) 0) * (((long) availableBlocks) - 4))) == 0) {
                    Log.d(Constants.TAG, "download aborted - not enough free space in internal storage");
                    return null;
                } else {
                    stat.restat(base2.getPath());
                }
            }
        } else if (Environment.getExternalStorageState().equals("mounted")) {
            String root = Environment.getExternalStorageDirectory().getPath();
            if (s != null) {
                base = new File(root + DownloadSettings.getStorageDir() + File.separator + s);
            } else {
                base = new File(root + DownloadSettings.getStorageDir());
            }
            if (!base.isDirectory()) {
                String[] subPath = DownloadSettings.getStorageDir().split(File.separator);
                if (subPath != null && subPath.length > 1) {
                    int count = subPath.length;
                    String tmpPath = null;
                    int i = 0;
                    while (i < count) {
                        String tmpPath2 = i == 0 ? root + subPath[i] : tmpPath + File.separator + subPath[i];
                        File tmp = new File(tmpPath2);
                        if (tmp.isDirectory() || tmp.mkdir()) {
                            i++;
                            tmpPath = tmpPath2;
                        } else {
                            Log.d(Constants.TAG, "download aborted - can't create base directory " + tmp.getPath());
                            return null;
                        }
                    }
                } else if (!base.mkdir()) {
                    Log.d(Constants.TAG, "download aborted - can't create base directory " + base.getPath());
                    return null;
                }
            }
            StatFs stat2 = new StatFs(base.getPath());
            if (((long) stat2.getBlockSize()) * (((long) stat2.getAvailableBlocks()) - 4) < ((long) contentLength)) {
                Log.d(Constants.TAG, "download aborted - not enough free space");
                return null;
            }
        } else {
            Log.d(Constants.TAG, "download aborted - no external storage");
            return null;
        }
        return base;
    }

    public static boolean isFilenameValid(String s, String s1) {
        File file = new File(s).getParentFile();
        String s3 = Environment.getExternalStorageDirectory() + DownloadSettings.getStorageDir();
        if (file.equals(Environment.getDownloadCacheDirectory())) {
            return true;
        }
        if (TextUtils.isEmpty(s1)) {
            return file.equals(new File(s3));
        }
        return file.equals(new File(s3 + File.separator + s1));
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null) {
            Log.w(Constants.TAG, "couldn't get connectivity manager");
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (NetworkInfo state : info) {
                    if (state.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean isNetworkRoaming(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null) {
            Log.w(Constants.TAG, "couldn't get connectivity manager");
        } else {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info == null || info.getType() != 0 || !((TelephonyManager) context.getSystemService("phone")).isNetworkRoaming()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean isUsingWifiNetwork(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivity == null) {
            Log.w(Constants.TAG, "couldn't get connectivity manager");
        } else {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null && info.getType() == 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean isWifiAvailable(Context context) {
        ConnectivityManager connectivitymanager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivitymanager == null) {
            Log.w(Constants.TAG, "couldn't get connectivity manager");
        } else {
            NetworkInfo[] anetworkinfo = connectivitymanager.getAllNetworkInfo();
            if (anetworkinfo != null) {
                int len = anetworkinfo.length;
                for (int j = 0; j < len; j++) {
                    if (anetworkinfo[j].getType() == 1 && anetworkinfo[j].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static String parseContentDisposition(String contentDisposition) {
        try {
            Matcher m = CONTENT_DISPOSITION_PATTERN.matcher(contentDisposition);
            if (m.find()) {
                return m.group(1);
            }
        } catch (IllegalStateException e) {
        }
        return null;
    }

    private static void parseExpression(Lexer lexer) {
        while (true) {
            if (lexer.currentToken() == 1) {
                lexer.advance();
                parseExpression(lexer);
                if (lexer.currentToken() != 2) {
                    throw new IllegalArgumentException("syntax error, unmatched parenthese");
                }
                lexer.advance();
            } else {
                parseStatement(lexer);
            }
            if (lexer.currentToken() == 3) {
                lexer.advance();
            } else {
                return;
            }
        }
    }

    private static void parseStatement(Lexer lexer) {
        if (lexer.currentToken() != 4) {
            throw new IllegalArgumentException("syntax error, expected column name");
        }
        lexer.advance();
        if (lexer.currentToken() == 5) {
            lexer.advance();
            if (lexer.currentToken() != 6) {
                throw new IllegalArgumentException("syntax error, expected quoted string");
            }
            lexer.advance();
        } else if (lexer.currentToken() == 7) {
            lexer.advance();
            if (lexer.currentToken() != 8) {
                throw new IllegalArgumentException("syntax error, expected NULL");
            }
            lexer.advance();
        } else {
            throw new IllegalArgumentException("syntax error after column name");
        }
    }

    public static void validateSelection(String selection, Set<String> allowedColumns) {
        if (selection != null) {
            try {
                Lexer lexer = new Lexer(selection, allowedColumns);
                parseExpression(lexer);
                if (lexer.currentToken() != 9) {
                    throw new IllegalArgumentException("syntax error");
                }
            } catch (RuntimeException e) {
                RuntimeException ex = e;
                Log.d(Constants.TAG, "invalid selection triggered " + ex);
                throw ex;
            }
        }
    }
}
