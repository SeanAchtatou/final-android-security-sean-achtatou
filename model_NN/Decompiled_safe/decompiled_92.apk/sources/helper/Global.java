package helper;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;

public final class Global {
    public static synchronized boolean isSDCardAvailable() {
        boolean z;
        synchronized (Global.class) {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                z = true;
            } else {
                z = false;
            }
        }
        return z;
    }

    public static synchronized boolean hasInternetConnection(Context hContext) {
        boolean z;
        synchronized (Global.class) {
            try {
                Enumeration<NetworkInterface> enuNetwork = NetworkInterface.getNetworkInterfaces();
                while (true) {
                    if (!enuNetwork.hasMoreElements()) {
                        break;
                    }
                    Enumeration<InetAddress> enumInetAddress = enuNetwork.nextElement().getInetAddresses();
                    while (true) {
                        if (enumInetAddress.hasMoreElements()) {
                            InetAddress hInetAddressLoop = enumInetAddress.nextElement();
                            if (!hInetAddressLoop.isLoopbackAddress()) {
                                z = !TextUtils.isEmpty(hInetAddressLoop.getHostAddress().toString());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Exception e2 = e;
                if (Constants.debug()) {
                    throw new RuntimeException(e2);
                }
            }
            z = false;
        }
        return z;
    }

    public static synchronized void closeReader(Reader hReader) {
        synchronized (Global.class) {
            if (hReader != null) {
                try {
                    hReader.close();
                } catch (Exception e) {
                    if (Constants.debug()) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static synchronized void closeInputStream(InputStream hInputStream) {
        synchronized (Global.class) {
            if (hInputStream != null) {
                try {
                    hInputStream.close();
                } catch (Exception e) {
                    if (Constants.debug()) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public static synchronized void closeOutputStream(OutputStream hOutputStream) {
        synchronized (Global.class) {
            if (hOutputStream != null) {
                try {
                    hOutputStream.close();
                } catch (Exception e) {
                    if (Constants.debug()) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
