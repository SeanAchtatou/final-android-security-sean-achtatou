package res;

import android.content.Context;

public final class Res {
    private static final String RESOURCE_TYPE_BOOL = "bool";
    private static final String RESOURCE_TYPE_COLOR = "color";
    private static final String RESOURCE_TYPE_DIMEN = "dimen";
    private static final String RESOURCE_TYPE_DRAWABLE = "drawable";
    private static final String RESOURCE_TYPE_ID = "id";
    private static final String RESOURCE_TYPE_INTEGER = "integer";
    private static final String RESOURCE_TYPE_LAYOUT = "layout";
    private static final String RESOURCE_TYPE_RAW = "raw";
    private static final String RESOURCE_TYPE_STRING = "string";

    private static int getResourceId(Context hContext, String sResType, String sResName) {
        int iResult = hContext.getResources().getIdentifier(sResName, sResType, hContext.getPackageName());
        if (iResult != 0) {
            return iResult;
        }
        throw new RuntimeException("Resource " + sResName + " not found in " + hContext.getPackageName());
    }

    public static int bool(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_BOOL, sResName);
    }

    public static int color(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_COLOR, sResName);
    }

    public static int dimen(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_DIMEN, sResName);
    }

    public static int drawable(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_DRAWABLE, sResName);
    }

    public static int id(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_ID, sResName);
    }

    public static int integer(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_INTEGER, sResName);
    }

    public static int raw(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_RAW, sResName);
    }

    public static int string(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_STRING, sResName);
    }

    public static int layout(Context hContext, String sResName) {
        return getResourceId(hContext, RESOURCE_TYPE_LAYOUT, sResName);
    }
}
