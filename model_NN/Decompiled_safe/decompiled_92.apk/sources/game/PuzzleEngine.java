package game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import cfg.Option;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import view.PuzzlePieceContainer;

public final class PuzzleEngine {
    private static final int ALPHA_GROUP_CLEAR_HIGHTLIGHT = 255;
    private static final int ALPHA_GROUP_HIGHTLIGHT = 100;
    private final HashMap<Integer, ArrayList<PuzzlePiece>> m_arrAllGroup = new HashMap<>();
    private final HashMap<Integer, Bitmap> m_arrAllGroupBitmap = new HashMap<>();
    private PuzzlePieceContainer[][] m_arrMatrix;
    private final ArrayList<PuzzlePieceContainer> m_arrPuzzlePieceContainer = new ArrayList<>();
    private final HashMap<Integer, PuzzlePiece> m_arrPuzzlePieceSorted = new HashMap<>();
    private final Context m_hContext;
    private int m_iGridSize;
    private int m_iPuzzlePieceHeight;
    private int m_iPuzzlePieceWidth;

    public PuzzleEngine(Context hContext) {
        this.m_hContext = hContext;
    }

    public void init(int iGridSize) {
        this.m_iGridSize = iGridSize;
        this.m_arrPuzzlePieceContainer.clear();
        this.m_arrPuzzlePieceSorted.clear();
        this.m_arrMatrix = (PuzzlePieceContainer[][]) Array.newInstance(PuzzlePieceContainer.class, iGridSize, iGridSize);
        ungroupAll();
    }

    public void setImage(Bitmap hImage) {
        clearPuzzlePiece();
        int iImageWidth = hImage.getWidth();
        int iImageHeight = hImage.getHeight();
        this.m_iPuzzlePieceWidth = iImageWidth / this.m_iGridSize;
        this.m_iPuzzlePieceHeight = iImageHeight / this.m_iGridSize;
        int iFinalPosition = 0;
        for (int iRow = 0; iRow < this.m_iGridSize; iRow++) {
            for (int iColumn = 0; iColumn < this.m_iGridSize; iColumn++) {
                addPuzzlePiece(iFinalPosition, new PuzzlePiece(iFinalPosition, iColumn, iRow, Bitmap.createBitmap(hImage, this.m_iPuzzlePieceWidth * iColumn, this.m_iPuzzlePieceHeight * iRow, this.m_iPuzzlePieceWidth, this.m_iPuzzlePieceHeight)));
                iFinalPosition++;
            }
        }
        do {
            shuffle();
        } while (isSolved());
    }

    public int getPuzzlePieceHeight() {
        return this.m_iPuzzlePieceHeight;
    }

    public int getPuzzlePieceWidth() {
        return this.m_iPuzzlePieceWidth;
    }

    public ArrayList<PuzzlePieceContainer> getArray() {
        return this.m_arrPuzzlePieceContainer;
    }

    public void addContainer(PuzzlePieceContainer hContainer) {
        this.m_arrPuzzlePieceContainer.add(hContainer);
        this.m_arrMatrix[hContainer.getColumn()][hContainer.getRow()] = hContainer;
    }

    public PuzzlePieceContainer getContainer(int iIndex) {
        return this.m_arrPuzzlePieceContainer.get(iIndex);
    }

    public int size() {
        return this.m_arrPuzzlePieceContainer.size();
    }

    public void clearPuzzlePiece() {
        this.m_arrPuzzlePieceSorted.clear();
    }

    public void addPuzzlePiece(int iFinalPosition, PuzzlePiece hPuzzlePiece) {
        this.m_arrPuzzlePieceSorted.put(Integer.valueOf(iFinalPosition), hPuzzlePiece);
    }

    public void shuffle() {
        ArrayList<Integer> arrSortedOrder = new ArrayList<>();
        int iSize = this.m_arrPuzzlePieceSorted.size();
        for (int i = 0; i < iSize; i++) {
            arrSortedOrder.add(Integer.valueOf(i));
        }
        Random hRandom = new Random();
        ArrayList<Integer> arrRandomOrder = new ArrayList<>();
        while (!arrSortedOrder.isEmpty()) {
            arrRandomOrder.add((Integer) arrSortedOrder.remove(hRandom.nextInt(arrSortedOrder.size())));
        }
        fillContainer(arrRandomOrder);
    }

    private void fillContainer(ArrayList<Integer> arrFillOrder) {
        int iSize = arrFillOrder.size();
        for (int i = 0; i < iSize; i++) {
            this.m_arrPuzzlePieceContainer.get(i).setPuzzlePiece(this.m_arrPuzzlePieceSorted.get(arrFillOrder.get(i)));
        }
    }

    public boolean isSolved() {
        boolean bCorrectPlace;
        boolean bIsSolved = true;
        Iterator<PuzzlePieceContainer> it = this.m_arrPuzzlePieceContainer.iterator();
        while (it.hasNext()) {
            PuzzlePieceContainer hPieceContainer = it.next();
            PuzzlePiece hPiece = hPieceContainer.getPuzzlePiece();
            if (hPiece == null) {
                return false;
            }
            int iHintColor = Option.getHintColor(this.m_hContext);
            hPieceContainer.setHintColor(iHintColor);
            if (hPieceContainer.getId() == hPiece.getFinalPosition()) {
                bCorrectPlace = true;
            } else {
                bCorrectPlace = false;
            }
            if (-1 != iHintColor) {
                int iColumnContainer = hPieceContainer.getColumn();
                int iRowContainer = hPieceContainer.getRow();
                int iFinalColumnPiece = hPiece.getFinalColumn();
                boolean bLeft = false;
                boolean bTop = false;
                boolean bRight = false;
                boolean bBottom = false;
                if (iColumnContainer == 0) {
                    if (bCorrectPlace) {
                        bLeft = true;
                    }
                } else if (iColumnContainer > 0 && iFinalColumnPiece > 0) {
                    if (hPiece.getFinalPosition() - this.m_arrMatrix[iColumnContainer - 1][iRowContainer].getPuzzlePiece().getFinalPosition() == 1) {
                        bLeft = true;
                    }
                }
                if (iRowContainer == 0) {
                    if (bCorrectPlace) {
                        bTop = true;
                    }
                } else if (iRowContainer > 0) {
                    if (hPiece.getFinalPosition() == this.m_arrMatrix[iColumnContainer][iRowContainer - 1].getPuzzlePiece().getFinalPosition() + this.m_iGridSize) {
                        bTop = true;
                    }
                }
                int iIndex = this.m_iGridSize - 1;
                if (iColumnContainer == iIndex) {
                    if (bCorrectPlace) {
                        bRight = true;
                    }
                } else if (iColumnContainer < iIndex && iFinalColumnPiece < iIndex && this.m_arrMatrix[iColumnContainer + 1][iRowContainer].getPuzzlePiece().getFinalPosition() - hPiece.getFinalPosition() == 1) {
                    bRight = true;
                }
                if (iRowContainer == iIndex) {
                    if (bCorrectPlace) {
                        bBottom = true;
                    }
                } else if (iRowContainer < iIndex) {
                    if (hPiece.getFinalPosition() + this.m_iGridSize == this.m_arrMatrix[iColumnContainer][iRowContainer + 1].getPuzzlePiece().getFinalPosition()) {
                        bBottom = true;
                    }
                }
                hPieceContainer.correctNeightbors(bLeft, bTop, bRight, bBottom);
            }
            if (bIsSolved && !bCorrectPlace) {
                bIsSolved = false;
            }
        }
        return bIsSolved;
    }

    public int getNewGroupId() {
        int iGroupId = 0;
        while (this.m_arrAllGroup.containsKey(Integer.valueOf(iGroupId))) {
            iGroupId++;
        }
        this.m_arrAllGroup.put(Integer.valueOf(iGroupId), new ArrayList());
        return iGroupId;
    }

    public void highlightForGroup() {
        Iterator<PuzzlePieceContainer> it = this.m_arrPuzzlePieceContainer.iterator();
        while (it.hasNext()) {
            PuzzlePieceContainer hPieceContainer = it.next();
            hPieceContainer.setAlpha(100);
            hPieceContainer.update();
        }
    }

    public void clearHighlightForGroup() {
        Iterator<PuzzlePieceContainer> it = this.m_arrPuzzlePieceContainer.iterator();
        while (it.hasNext()) {
            PuzzlePieceContainer hPieceContainer = it.next();
            hPieceContainer.setAlpha(ALPHA_GROUP_CLEAR_HIGHTLIGHT);
            hPieceContainer.update();
        }
    }

    public void addPuzzlePieceToGroup(int iGroupId, PuzzlePiece hPuzzlePiece) {
        ArrayList<PuzzlePiece> arrGroup = this.m_arrAllGroup.get(Integer.valueOf(iGroupId));
        if (arrGroup != null && !arrGroup.contains(hPuzzlePiece)) {
            arrGroup.add(hPuzzlePiece);
            hPuzzlePiece.getContainer().setAlpha(ALPHA_GROUP_CLEAR_HIGHTLIGHT);
        }
    }

    public void createGroup(int iGroupId) {
        ArrayList<PuzzlePiece> arrNewGroup = getGroup(iGroupId);
        if (arrNewGroup == null || arrNewGroup.size() <= 0) {
            clearHighlightForGroup();
            return;
        }
        int iMinColumn = this.m_iGridSize - 1;
        int iMaxColumn = 0;
        int iMinRow = this.m_iGridSize - 1;
        int iMaxRow = 0;
        Iterator<PuzzlePieceContainer> it = this.m_arrPuzzlePieceContainer.iterator();
        while (it.hasNext()) {
            PuzzlePieceContainer hPuzzlePieceContainer = it.next();
            hPuzzlePieceContainer.setAlpha(ALPHA_GROUP_CLEAR_HIGHTLIGHT);
            hPuzzlePieceContainer.update();
            if (arrNewGroup.contains(hPuzzlePieceContainer.getPuzzlePiece())) {
                int iColumn = hPuzzlePieceContainer.getColumn();
                int iRow = hPuzzlePieceContainer.getRow();
                if (iMinColumn > iColumn) {
                    iMinColumn = iColumn;
                }
                if (iMaxColumn < iColumn) {
                    iMaxColumn = iColumn;
                }
                if (iMinRow > iRow) {
                    iMinRow = iRow;
                }
                if (iMaxRow < iRow) {
                    iMaxRow = iRow;
                }
            }
        }
        arrNewGroup.clear();
        Iterator<PuzzlePieceContainer> it2 = this.m_arrPuzzlePieceContainer.iterator();
        while (it2.hasNext()) {
            PuzzlePieceContainer hPuzzlePieceContainer2 = it2.next();
            int iColumn2 = hPuzzlePieceContainer2.getColumn();
            int iRow2 = hPuzzlePieceContainer2.getRow();
            if (iColumn2 >= iMinColumn && iColumn2 <= iMaxColumn && iRow2 >= iMinRow && iRow2 <= iMaxRow) {
                PuzzlePiece hPuzzlePiece = hPuzzlePieceContainer2.getPuzzlePiece();
                ungroup(hPuzzlePiece.getGroupId());
                hPuzzlePiece.setColumnInGrid(iColumn2);
                hPuzzlePiece.setRowInGrid(iRow2);
                hPuzzlePiece.setGroupId(iGroupId);
                arrNewGroup.add(hPuzzlePiece);
                hPuzzlePieceContainer2.update();
            }
        }
        int iWidth = (iMaxColumn - iMinColumn) + 1;
        int iHeight = (iMaxRow - iMinRow) + 1;
        Bitmap hGroupBitmap = Bitmap.createBitmap(this.m_iPuzzlePieceWidth * iWidth, this.m_iPuzzlePieceHeight * iHeight, Bitmap.Config.RGB_565);
        Canvas hCanvas = new Canvas(hGroupBitmap);
        Iterator<PuzzlePiece> it3 = arrNewGroup.iterator();
        while (it3.hasNext()) {
            PuzzlePiece hPuzzlePiece2 = it3.next();
            hPuzzlePiece2.setGroupDimension(iWidth, iHeight);
            int iColumn3 = hPuzzlePiece2.getColumnInGrid() - iMinColumn;
            int iRow3 = hPuzzlePiece2.getRowInGrid() - iMinRow;
            hPuzzlePiece2.setColumnInGroupBitmap(iColumn3);
            hPuzzlePiece2.setRowInGroupBitmap(iRow3);
            hCanvas.drawBitmap(hPuzzlePiece2.getBitmap(), (float) (this.m_iPuzzlePieceWidth * iColumn3), (float) (this.m_iPuzzlePieceHeight * iRow3), (Paint) null);
        }
        this.m_arrAllGroupBitmap.put(Integer.valueOf(iGroupId), hGroupBitmap);
    }

    public void ungroup(int iGroupId) {
        this.m_arrAllGroupBitmap.remove(Integer.valueOf(iGroupId));
        ArrayList<PuzzlePiece> arrGroup = this.m_arrAllGroup.remove(Integer.valueOf(iGroupId));
        if (arrGroup != null) {
            Iterator it = arrGroup.iterator();
            while (it.hasNext()) {
                PuzzlePiece hPuzzlePiece = (PuzzlePiece) it.next();
                hPuzzlePiece.ungroup();
                hPuzzlePiece.getContainer().update();
            }
            arrGroup.clear();
        }
    }

    public void ungroupAll() {
        this.m_arrAllGroupBitmap.clear();
        int iSize = this.m_arrAllGroup.size();
        for (int i = 0; i < iSize; i++) {
            ArrayList<PuzzlePiece> arrGroup = this.m_arrAllGroup.get(Integer.valueOf(i));
            if (this.m_arrAllGroup != null) {
                Iterator it = arrGroup.iterator();
                while (it.hasNext()) {
                    PuzzlePiece hPuzzlePiece = (PuzzlePiece) it.next();
                    hPuzzlePiece.ungroup();
                    hPuzzlePiece.getContainer().update();
                }
                arrGroup.clear();
            }
        }
        this.m_arrAllGroup.clear();
    }

    public boolean hasGroup() {
        return this.m_arrAllGroup.size() != 0;
    }

    public ArrayList<PuzzlePiece> getGroup(int iGroupId) {
        return this.m_arrAllGroup.get(Integer.valueOf(iGroupId));
    }

    public Bitmap getGroupBitmap(int iGroupId) {
        return this.m_arrAllGroupBitmap.get(Integer.valueOf(iGroupId));
    }
}
