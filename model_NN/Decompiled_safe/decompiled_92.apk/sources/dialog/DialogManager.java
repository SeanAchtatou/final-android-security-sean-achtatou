package dialog;

import ad.AdContainer;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import data.MyPuzzle;
import data.MyPuzzleImage;
import data.MyPuzzleList;
import data.Puzzle;
import dialog.DialogScreenHintColor;
import game.IGame;
import java.util.ArrayList;
import java.util.List;
import res.Res;
import screen.ScreenPlayBase;

public final class DialogManager extends Dialog {
    private final List<DialogScreenBase> m_arrScreen = new ArrayList();
    private boolean m_bScreenSwitchDismiss = false;
    private final Activity m_hActivity;
    private final AdContainer m_hAdContainer;
    /* access modifiers changed from: private */
    public DialogScreenBase m_hDialogScreenCurrent;
    private final DialogScreenHintColor m_hDialogScreenHintColor;
    private final DialogScreenLeavePuzzle m_hDialogScreenLeavePuzzle;
    private final DialogScreenMyPuzzleAdd m_hDialogScreenMyPuzzleAdd;
    private final DialogScreenMyPuzzleRemove m_hDialogScreenMyPuzzleRemove;
    private final DialogScreenPuzzleImageInfo m_hDialogScreenPuzzleImageInfo;
    private final DialogScreenPuzzleInfo m_hDialogScreenPuzzleInfo;
    private final DialogScreenPuzzleSolved m_hDialogScreenPuzzleSolved;
    private final DialogScreenQuit m_hDialogScreenQuit;
    private final DialogScreenRating m_hDialogScreenRating;
    private final DialogScreenMyPuzzleImageRemove m_hDialogScreenRemoveMyPuzzleImage;
    private final DialogScreenReshuffle m_hDialogScreenReshuffle;
    private final DialogScreenResult m_hDialogScreenResult;
    private final DialogScreenSaveImage m_hDialogScreenSaveImage;
    private final IGame m_hGame;

    public DialogManager(Activity hActivity, IGame hGame) {
        super(hActivity);
        this.m_hActivity = hActivity;
        this.m_hGame = hGame;
        setCancelable(false);
        requestWindowFeature(1);
        DialogRootLayout hDialogRootLayout = new DialogRootLayout(hActivity, hGame.getPuzzle());
        ViewGroup vgScreenContainer = hDialogRootLayout.getScreenContainer();
        this.m_hDialogScreenQuit = new DialogScreenQuit(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenQuit);
        this.m_hDialogScreenLeavePuzzle = new DialogScreenLeavePuzzle(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenLeavePuzzle);
        this.m_hDialogScreenPuzzleInfo = new DialogScreenPuzzleInfo(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenPuzzleInfo);
        this.m_hDialogScreenPuzzleImageInfo = new DialogScreenPuzzleImageInfo(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenPuzzleImageInfo);
        this.m_hDialogScreenReshuffle = new DialogScreenReshuffle(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenReshuffle);
        this.m_hDialogScreenResult = new DialogScreenResult(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenResult);
        this.m_hDialogScreenPuzzleSolved = new DialogScreenPuzzleSolved(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenPuzzleSolved);
        this.m_hDialogScreenHintColor = new DialogScreenHintColor(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenHintColor);
        this.m_hDialogScreenMyPuzzleRemove = new DialogScreenMyPuzzleRemove(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenMyPuzzleRemove);
        this.m_hDialogScreenRemoveMyPuzzleImage = new DialogScreenMyPuzzleImageRemove(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenRemoveMyPuzzleImage);
        this.m_hDialogScreenMyPuzzleAdd = new DialogScreenMyPuzzleAdd(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenMyPuzzleAdd);
        this.m_hDialogScreenSaveImage = new DialogScreenSaveImage(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenSaveImage);
        this.m_hDialogScreenRating = new DialogScreenRating(hActivity, this);
        addScreen(vgScreenContainer, this.m_hDialogScreenRating);
        Window hWindow = getWindow();
        hWindow.setBackgroundDrawable(new ColorDrawable(0));
        WindowManager.LayoutParams hLayoutParams = hWindow.getAttributes();
        hLayoutParams.flags |= 1024;
        hLayoutParams.width = -1;
        hLayoutParams.height = -1;
        hWindow.setAttributes(hLayoutParams);
        hWindow.setContentView(hDialogRootLayout);
        this.m_hAdContainer = hDialogRootLayout.getAdContainer();
    }

    private void addScreen(ViewGroup vgScreenContainer, DialogScreenBase hScreen) {
        hScreen.setVisibility(4);
        this.m_arrScreen.add(hScreen);
        vgScreenContainer.addView(hScreen);
    }

    public IGame getGame() {
        return this.m_hGame;
    }

    private void performShow(DialogScreenBase hDialogScreenNew) {
        this.m_bScreenSwitchDismiss = false;
        this.m_hAdContainer.check(this.m_hActivity);
        this.m_hDialogScreenCurrent = hDialogScreenNew;
        for (DialogScreenBase hScreenLoop : this.m_arrScreen) {
            hScreenLoop.setVisibility(4);
        }
        this.m_hDialogScreenCurrent.setVisibility(0);
        show();
    }

    public void showQuit(boolean bUnlockScreenOnDismiss) {
        this.m_hDialogScreenQuit.onShow(bUnlockScreenOnDismiss);
        performShow(this.m_hDialogScreenQuit);
    }

    public void showLeavePuzzle(int iScreenId) {
        this.m_hDialogScreenLeavePuzzle.onShow(iScreenId);
        performShow(this.m_hDialogScreenLeavePuzzle);
    }

    public void showHintColor(DialogScreenHintColor.OnColorChangedListener hOnColorChanged) {
        this.m_hDialogScreenHintColor.onShow(hOnColorChanged);
        performShow(this.m_hDialogScreenHintColor);
    }

    public void showPuzzleInfo(String sPuzzleInfo) {
        this.m_hDialogScreenPuzzleInfo.onShow(sPuzzleInfo);
        performShow(this.m_hDialogScreenPuzzleInfo);
    }

    public void showPuzzleImageInfo(String sImageInfo, ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hDialogScreenPuzzleImageInfo.onShow(sImageInfo, hResultData, hOnDismiss);
        performShow(this.m_hDialogScreenPuzzleImageInfo);
    }

    public void showReshuffle(DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hDialogScreenReshuffle.onShow(hOnDialogClick);
        performShow(this.m_hDialogScreenReshuffle);
    }

    public void showPuzzleSolved(ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hDialogScreenPuzzleSolved.onShow(hResultData, hOnDismiss);
        performShow(this.m_hDialogScreenPuzzleSolved);
    }

    public void showSaveImage(String sPuzzleId, int iImageIndex, ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hDialogScreenSaveImage.onShow(sPuzzleId, iImageIndex, hResultData, hOnDismiss);
        performShow(this.m_hDialogScreenSaveImage);
    }

    public void showResult(ScreenPlayBase.ResultData hResultData, DialogInterface.OnDismissListener hOnDismiss) {
        this.m_hDialogScreenResult.onShow(hResultData, hOnDismiss);
        performShow(this.m_hDialogScreenResult);
    }

    public void showMyPuzzleRemove(MyPuzzle hMyPuzzle, DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hDialogScreenMyPuzzleRemove.onShow(hMyPuzzle, hOnDialogClick);
        performShow(this.m_hDialogScreenMyPuzzleRemove);
    }

    public void showMyPuzzleImageRemove(MyPuzzleImage hMyPuzzleImage, DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hDialogScreenRemoveMyPuzzleImage.onShow(hMyPuzzleImage, hOnDialogClick);
        performShow(this.m_hDialogScreenRemoveMyPuzzleImage);
    }

    public void showMyPuzzleAdd(int iScreenId, MyPuzzleList hMyPuzzleList, MyPuzzle hMyPuzzle, DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hDialogScreenMyPuzzleAdd.onShow(iScreenId, hMyPuzzleList, hMyPuzzle, hOnDialogClick);
        performShow(this.m_hDialogScreenMyPuzzleAdd);
    }

    public void showRating() {
        performShow(this.m_hDialogScreenRating);
    }

    public void setMyPuzzleAddIcon(Bitmap hMyPuzzleIcon) {
        this.m_hDialogScreenMyPuzzleAdd.setDialogScreenMyPuzzleAddIcon(hMyPuzzleIcon);
    }

    public void screenSwitchDismiss() {
        if (isShowing()) {
            this.m_bScreenSwitchDismiss = true;
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (this.m_bScreenSwitchDismiss) {
            this.m_bScreenSwitchDismiss = false;
        } else if (this.m_hDialogScreenCurrent == null) {
            this.m_hGame.unlockScreen();
        } else {
            if (this.m_hDialogScreenCurrent.unlockScreenOnDismiss()) {
                this.m_hGame.unlockScreen();
            }
            this.m_hDialogScreenCurrent.post(new Runnable() {
                public void run() {
                    DialogManager.this.m_hDialogScreenCurrent.cleanUp();
                }
            });
        }
    }

    private static final class DialogRootLayout extends RelativeLayout {
        private final AdContainer m_hAdContainer;
        private final ViewGroup m_vgScreenContainer;

        public DialogRootLayout(Context hContext, Puzzle hPuzzle) {
            super(hContext);
            setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            ImageView ivBackground = new ImageView(hContext);
            ivBackground.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            ivBackground.setImageResource(Res.drawable(hContext, "background"));
            ivBackground.setScaleType(ImageView.ScaleType.FIT_XY);
            addView(ivBackground);
            LinearLayout vgContentRoot = new LinearLayout(hContext);
            vgContentRoot.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            vgContentRoot.setOrientation(1);
            vgContentRoot.setGravity(17);
            addView(vgContentRoot);
            FrameLayout vgContent = new FrameLayout(hContext);
            vgContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
            vgContentRoot.addView(vgContent);
            this.m_vgScreenContainer = new FrameLayout(hContext);
            this.m_vgScreenContainer.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            vgContent.addView(this.m_vgScreenContainer);
            this.m_hAdContainer = new AdContainer(hContext, hPuzzle);
            vgContentRoot.addView(this.m_hAdContainer);
        }

        public ViewGroup getScreenContainer() {
            return this.m_vgScreenContainer;
        }

        public AdContainer getAdContainer() {
            return this.m_hAdContainer;
        }
    }
}
