package dialog;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.TitleView;

public final class DialogScreenReshuffle extends DialogScreenBase {
    private static final int BUTTON_ID_CANCEL = 0;
    private static final int BUTTON_ID_OK = 1;
    private final View.OnClickListener m_hOnClick = new View.OnClickListener() {
        public void onClick(View v) {
            if (DialogScreenReshuffle.this.m_hOnDialogClick != null) {
                switch (v.getId()) {
                    case 0:
                        DialogScreenReshuffle.this.m_hOnDialogClick.onClick(DialogScreenReshuffle.this.m_hDialogManager, -2);
                        break;
                    case 1:
                        DialogScreenReshuffle.this.m_hOnDialogClick.onClick(DialogScreenReshuffle.this.m_hDialogManager, -1);
                        break;
                    default:
                        throw new RuntimeException("Invalid view id");
                }
            }
            DialogScreenReshuffle.this.m_hDialogManager.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener m_hOnDialogClick;

    public DialogScreenReshuffle(Activity hActivity, DialogManager hDialogManager) {
        super(hActivity, hDialogManager);
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        TitleView hTitle = new TitleView(hActivity, hMetrics);
        hTitle.setTitle(ResString.dialog_screen_shuffle_title);
        addView(hTitle);
        RelativeLayout vgTextContainer = new RelativeLayout(hActivity);
        vgTextContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgTextContainer);
        TextView tvText = new TextView(hActivity);
        RelativeLayout.LayoutParams hTextParams = new RelativeLayout.LayoutParams(-1, -2);
        hTextParams.addRule(13);
        tvText.setLayoutParams(hTextParams);
        tvText.setGravity(17);
        tvText.setTextColor(-1);
        tvText.setTextSize(1, 20.0f);
        tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        tvText.setTypeface(Typeface.DEFAULT, 1);
        int iDip20 = ResDimens.getDip(hMetrics, 20);
        tvText.setPadding(iDip20, iDip20, iDip20, iDip20);
        tvText.setText(ResString.dialog_screen_shuffle_text);
        vgTextContainer.addView(tvText);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_cancel", this.m_hOnClick);
        vgButtonContainer.createButton(1, "btn_img_ok", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public void onShow(DialogInterface.OnClickListener hOnDialogClick) {
        this.m_hOnDialogClick = hOnDialogClick;
    }

    public void cleanUp() {
        super.cleanUp();
        this.m_hOnDialogClick = null;
    }
}
