package data;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.simboly.puzzlebox.ActivityGame;
import helper.BitmapHelper;
import java.io.Serializable;

public class MorePuzzle implements Serializable {
    public static final int TYPE_INSTALLED = 0;
    public static final int TYPE_MARKET = 1;
    private static final long serialVersionUID = -6466868954056643923L;
    private transient Bitmap m_hIcon;
    private final int m_iType = 0;
    private final String m_sDeveloperName;
    private final String m_sIconPath;
    private final String m_sIconVersion;
    private final String m_sId;
    private final String m_sMarketUriApp;
    private final String m_sPuzzleName;

    public MorePuzzle(String sId, String sDeveloperName, String sPuzzleName) {
        this.m_sId = sId;
        this.m_sDeveloperName = sDeveloperName;
        this.m_sPuzzleName = sPuzzleName;
        this.m_sMarketUriApp = null;
        this.m_sIconVersion = null;
        this.m_sIconPath = null;
    }

    public MorePuzzle(String sId, String sDeveloperName, String sPuzzleName, String sMarketUriApp, String sIconVersion, String sIconPath) {
        this.m_sId = sId;
        this.m_sDeveloperName = sDeveloperName;
        this.m_sPuzzleName = sPuzzleName;
        this.m_sMarketUriApp = sMarketUriApp;
        this.m_sIconVersion = sIconVersion;
        this.m_sIconPath = sIconPath;
    }

    public int getType() {
        return this.m_iType;
    }

    public String getId() {
        return this.m_sId;
    }

    public String getDeveloperName() {
        return this.m_sDeveloperName;
    }

    public String getPuzzleName() {
        return this.m_sPuzzleName;
    }

    public String getIconVersion() {
        return this.m_sIconVersion;
    }

    public String getMarketUri() {
        return this.m_sMarketUriApp;
    }

    public String getIconPath() {
        return this.m_sIconPath;
    }

    public Bitmap getIcon(Context hContext) {
        if (this.m_hIcon == null) {
            if (this.m_sIconPath != null) {
                this.m_hIcon = BitmapFactory.decodeFile(this.m_sIconPath);
            } else {
                this.m_hIcon = BitmapHelper.createBitmapFromUri(hContext, PuzzleHelper.getIconUri(this.m_sId));
            }
        }
        return this.m_hIcon;
    }

    public boolean isInstalled(Context hContext) {
        PackageManager hPackageManager = hContext.getPackageManager();
        Intent hIntent = new Intent();
        hIntent.setClassName(this.m_sId, ActivityGame.class.getName());
        return hPackageManager.queryIntentActivities(hIntent, 32).size() > 0;
    }
}
