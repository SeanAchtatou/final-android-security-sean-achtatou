package screen;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import dialog.DialogManager;
import dialog.DialogScreenHintColor;
import game.IGame;
import game.PuzzleEngine;
import game.PuzzlePiece;
import java.util.ArrayList;
import java.util.Iterator;
import res.Res;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.CustomButton;
import view.PuzzlePieceContainer;

public abstract class ScreenPlayBase extends ScreenBase {
    private static final int BUTTON_ID_BACK = 0;
    private static final int BUTTON_ID_GROUP = 3;
    private static final int BUTTON_ID_HINT = 2;
    private static final int BUTTON_ID_SHUFFLE = 1;
    private static final int BUTTON_ID_VIBRATION = 4;
    private static final int GRID_SIZE_MIN = 3;
    private static final int MODE_DRAG = 0;
    private static final int MODE_GROUP = 1;
    public static final String PREF_KEY_PLAYER_NAME = "PREF_KEY_PLAYER_NAME";
    private static final int VIBRATE_DURATION = 25;
    private boolean m_bHasVibratePermission = false;
    private boolean m_bMoved = false;
    /* access modifiers changed from: private */
    public boolean m_bTimerStopped = false;
    private final CustomButton m_btnVibration;
    /* access modifiers changed from: private */
    public float m_fImageLeft = 0.0f;
    /* access modifiers changed from: private */
    public float m_fImageTop = 0.0f;
    /* access modifiers changed from: private */
    public boolean m_hCatchMoveEvent = false;
    protected final DialogManager m_hDialogManager;
    /* access modifiers changed from: private */
    public final DragAndDropView m_hDragAndDropView;
    /* access modifiers changed from: private */
    public final GameProgressView m_hGameProgress;
    /* access modifiers changed from: private */
    public Bitmap m_hImageCurrent;
    private final DialogInterface.OnClickListener m_hOnDialogReshuffleClick = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface hDialog, int iWhich) {
            if (iWhich != -1) {
                return;
            }
            if (ScreenPlayBase.this.m_hImageCurrent == null) {
                ScreenPlayBase.this.loadImage();
            } else {
                ScreenPlayBase.this.setImage(ScreenPlayBase.this.m_hImageCurrent);
            }
        }
    };
    private final View.OnTouchListener m_hOnDragAndDropDetectorTouch = new View.OnTouchListener() {
        public boolean onTouch(View vView, MotionEvent hEvent) {
            switch (hEvent.getAction()) {
                case 0:
                    ScreenPlayBase.this.m_vgPuzzleContainer.dispatchTouchEvent(hEvent);
                    if (ScreenPlayBase.this.m_hPuzzlePieceContainerDragged != null) {
                        ScreenPlayBase.this.m_hDragAndDropView.show();
                        break;
                    }
                    break;
                case 1:
                    ScreenPlayBase.this.m_hDragAndDropView.hide();
                    if (ScreenPlayBase.this.m_hPuzzlePieceContainerDragged != null) {
                        Iterator<PuzzlePieceContainer> it = ScreenPlayBase.this.m_hPuzzleEngine.getArray().iterator();
                        while (it.hasNext()) {
                            PuzzlePieceContainer hPieceContainer = it.next();
                            if (hPieceContainer.getVisibility() != 0) {
                                hPieceContainer.setVisibility(0);
                            }
                        }
                    }
                    MotionEvent hEventDown = MotionEvent.obtain(hEvent);
                    hEventDown.setAction(0);
                    ScreenPlayBase.this.m_hCatchMoveEvent = true;
                    ScreenPlayBase.this.m_vgPuzzleContainer.dispatchTouchEvent(hEventDown);
                    ScreenPlayBase.this.m_vgPuzzleContainer.dispatchTouchEvent(hEvent);
                    ScreenPlayBase.this.m_hCatchMoveEvent = false;
                    ScreenPlayBase.this.m_hPuzzlePieceContainerDragged = null;
                    ScreenPlayBase.this.hasMoved();
                    ScreenPlayBase.this.checkPuzzle();
                    break;
            }
            ScreenPlayBase.this.m_hDragAndDropView.draw((int) (hEvent.getRawX() - ScreenPlayBase.this.m_fImageLeft), (int) (hEvent.getRawY() - ScreenPlayBase.this.m_fImageTop));
            return true;
        }
    };
    private final View.OnTouchListener m_hOnGroupDetectorTouch = new View.OnTouchListener() {
        public boolean onTouch(View vView, MotionEvent hEvent) {
            switch (hEvent.getAction()) {
                case 0:
                    ScreenPlayBase.this.m_iNewGroupId = ScreenPlayBase.this.m_hPuzzleEngine.getNewGroupId();
                    return true;
                case 1:
                    ScreenPlayBase.this.m_hPuzzleEngine.createGroup(ScreenPlayBase.this.m_iNewGroupId);
                    ScreenPlayBase.this.switchMode(0);
                    return true;
                case 2:
                    MotionEvent hEventDown = MotionEvent.obtain(hEvent);
                    hEventDown.setAction(0);
                    ScreenPlayBase.this.m_vgPuzzleContainer.dispatchTouchEvent(hEventDown);
                    return true;
                default:
                    return true;
            }
        }
    };
    private final View.OnTouchListener m_hOnPuzzlePieceTouch = new View.OnTouchListener() {
        public boolean onTouch(View v, MotionEvent event) {
            PuzzlePieceContainer hPuzzlePieceContainerReleased = (PuzzlePieceContainer) v;
            PuzzlePiece hPuzzlePieceReleased = hPuzzlePieceContainerReleased.getPuzzlePiece();
            switch (event.getAction()) {
                case 0:
                    if (ScreenPlayBase.this.m_hCatchMoveEvent) {
                        ScreenPlayBase.this.m_hCatchMoveEvent = false;
                        return true;
                    }
                    switch (ScreenPlayBase.this.m_iCurrentMode) {
                        case 0:
                            ScreenPlayBase.this.m_fImageLeft = event.getX() + ((float) (hPuzzlePieceReleased.getColumnInGroupBitmap() * ScreenPlayBase.this.m_iPuzzlePieceWidth));
                            ScreenPlayBase.this.m_fImageTop = event.getY() + ((float) (hPuzzlePieceReleased.getRowInGroupBitmap() * ScreenPlayBase.this.m_iPuzzlePieceHeight));
                            ScreenPlayBase.this.m_hPuzzlePieceContainerDragged = hPuzzlePieceContainerReleased;
                            if (!hPuzzlePieceReleased.hasGroup()) {
                                ScreenPlayBase.this.m_hDragAndDropView.setBitmap(hPuzzlePieceReleased.getBitmap());
                                hPuzzlePieceContainerReleased.setVisibility(4);
                                break;
                            } else {
                                int iGroupId = hPuzzlePieceReleased.getGroupId();
                                Bitmap hBitmap = ScreenPlayBase.this.m_hPuzzleEngine.getGroupBitmap(iGroupId);
                                if (hBitmap != null) {
                                    ScreenPlayBase.this.m_hDragAndDropView.setBitmap(hBitmap);
                                    Iterator<PuzzlePieceContainer> it = ScreenPlayBase.this.m_hPuzzleEngine.getArray().iterator();
                                    while (it.hasNext()) {
                                        PuzzlePieceContainer hContainer = it.next();
                                        if (hContainer.getPuzzlePiece().getGroupId() == iGroupId) {
                                            hContainer.setVisibility(4);
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        case 1:
                            ScreenPlayBase.this.m_hPuzzleEngine.addPuzzlePieceToGroup(ScreenPlayBase.this.m_iNewGroupId, hPuzzlePieceReleased);
                            break;
                        default:
                            throw new RuntimeException("Invalide mode");
                    }
                case 1:
                    if (ScreenPlayBase.this.m_hPuzzlePieceContainerDragged == null) {
                        return false;
                    }
                    PuzzlePiece hPieceDragged = ScreenPlayBase.this.m_hPuzzlePieceContainerDragged.getPuzzlePiece();
                    boolean bHasGroupDragged = hPieceDragged.hasGroup();
                    boolean bHasGroupReleased = hPuzzlePieceReleased.hasGroup();
                    if (!bHasGroupDragged && bHasGroupReleased) {
                        if (hPuzzlePieceReleased.getGroupSize() == 1) {
                            ScreenPlayBase.this.switchPieces(hPuzzlePieceReleased, hPieceDragged);
                        }
                        return false;
                    } else if (!bHasGroupDragged && !bHasGroupReleased) {
                        ScreenPlayBase.this.switchPieces(hPuzzlePieceReleased, hPieceDragged);
                        return false;
                    } else if (bHasGroupDragged && !bHasGroupReleased) {
                        performSwitch(ScreenPlayBase.this.m_hPuzzlePieceContainerDragged, hPuzzlePieceContainerReleased);
                        return false;
                    } else if (bHasGroupDragged && bHasGroupReleased) {
                        int iGroupIdDragged = hPieceDragged.getGroupId();
                        int iGroupIdReleased = hPuzzlePieceReleased.getGroupId();
                        if (iGroupIdDragged == iGroupIdReleased) {
                            performSwitch(ScreenPlayBase.this.m_hPuzzlePieceContainerDragged, hPuzzlePieceContainerReleased);
                            return false;
                        }
                        if (hPieceDragged.hasSameGroupDimension(hPuzzlePieceReleased)) {
                            ArrayList<PuzzlePiece> arrDragged = ScreenPlayBase.this.m_hPuzzleEngine.getGroup(iGroupIdDragged);
                            ArrayList<PuzzlePiece> arrReleased = ScreenPlayBase.this.m_hPuzzleEngine.getGroup(iGroupIdReleased);
                            Iterator<PuzzlePiece> it2 = arrDragged.iterator();
                            while (it2.hasNext()) {
                                PuzzlePiece hDragged = it2.next();
                                Iterator<PuzzlePiece> it3 = arrReleased.iterator();
                                while (true) {
                                    if (it3.hasNext()) {
                                        PuzzlePiece hRelease = it3.next();
                                        if (hDragged.getColumnInGroupBitmap() == hRelease.getColumnInGroupBitmap() && hDragged.getRowInGroupBitmap() == hRelease.getRowInGroupBitmap()) {
                                            ScreenPlayBase.this.switchPieces(hRelease, hDragged);
                                        }
                                    }
                                }
                            }
                        }
                        return false;
                    }
                    break;
            }
            return false;
        }

        private void performSwitch(PuzzlePieceContainer hContainerDragged, PuzzlePieceContainer hContainerReleased) {
            int iTop;
            int iRight;
            int iGroupIdDragged = hContainerDragged.getPuzzlePiece().getGroupId();
            ArrayList<PuzzlePiece> arrPieceDragged = ScreenPlayBase.this.m_hPuzzleEngine.getGroup(iGroupIdDragged);
            PuzzlePieceContainer hPuzzlePieceContainerFirst = arrPieceDragged.get(0).getContainer();
            PuzzlePieceContainer hPuzzlePieceContainerLast = arrPieceDragged.get(arrPieceDragged.size() - 1).getContainer();
            int iColumnInGridFirst = hPuzzlePieceContainerFirst.getColumn();
            int iRowInGridFirst = hPuzzlePieceContainerFirst.getRow();
            int iColumnInGridLast = hPuzzlePieceContainerLast.getColumn();
            int iRowInGridLast = hPuzzlePieceContainerLast.getRow();
            int iColumnInGridDragged = hContainerDragged.getColumn();
            int iRowInGridDragged = hContainerDragged.getRow();
            int iColumnInGridReleased = hContainerReleased.getColumn();
            int iRowInGridReleased = hContainerReleased.getRow();
            int iLeft = (iColumnInGridFirst + iColumnInGridReleased) - iColumnInGridDragged;
            if (iLeft >= 0 && (iTop = (iRowInGridFirst + iRowInGridReleased) - iRowInGridDragged) >= 0 && (iRight = (iColumnInGridReleased + iColumnInGridLast) - iColumnInGridDragged) < ScreenPlayBase.this.m_iGridSizeCurrent) {
                int iBottom = (iRowInGridReleased + iRowInGridLast) - iRowInGridDragged;
                if (iBottom < ScreenPlayBase.this.m_iGridSizeCurrent) {
                    ArrayList<PuzzlePieceContainer> arrContainerReleased = new ArrayList<>();
                    for (int i = iTop; i <= iBottom; i++) {
                        for (int j = iLeft; j <= iRight; j++) {
                            Iterator<PuzzlePieceContainer> it = ScreenPlayBase.this.m_hPuzzleEngine.getArray().iterator();
                            while (true) {
                                if (!it.hasNext()) {
                                    break;
                                }
                                PuzzlePieceContainer hConReleased = it.next();
                                if (hConReleased.getColumn() == j && hConReleased.getRow() == i) {
                                    PuzzlePiece hPuzzlePieceReleased = hConReleased.getPuzzlePiece();
                                    if (!hPuzzlePieceReleased.hasGroup() || hPuzzlePieceReleased.getGroupId() == iGroupIdDragged) {
                                        arrContainerReleased.add(hConReleased);
                                    } else {
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    for (int i2 = 0; i2 < arrContainerReleased.size(); i2++) {
                        ScreenPlayBase.this.switchPieces(((PuzzlePieceContainer) arrContainerReleased.get(i2)).getPuzzlePiece(), arrPieceDragged.get(i2));
                    }
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public final PuzzleEngine m_hPuzzleEngine;
    /* access modifiers changed from: private */
    public PuzzlePieceContainer m_hPuzzlePieceContainerDragged;
    /* access modifiers changed from: private */
    public final Runnable m_hRunnableStartTimer = new Runnable() {
        public void run() {
            String sMin;
            String sSec;
            if (!ScreenPlayBase.this.m_bTimerStopped) {
                ScreenPlayBase.this.m_lTimeInMs = ScreenPlayBase.this.getTime() - ScreenPlayBase.this.m_lBaseTime;
                int iBestTimeInSec = (int) (ScreenPlayBase.this.m_lTimeInMs / 1000);
                int iMin = iBestTimeInSec / 60;
                int iSec = iBestTimeInSec % 60;
                if (iMin < 10) {
                    sMin = "0" + iMin;
                } else {
                    sMin = String.valueOf(iMin);
                }
                if (iSec < 10) {
                    sSec = "0" + iSec;
                } else {
                    sSec = String.valueOf(iSec);
                }
                ScreenPlayBase.this.m_hGameProgress.setTime(sMin, sSec);
                ScreenPlayBase.this.postDelayed(ScreenPlayBase.this.m_hRunnableStartTimer, 1000);
            }
        }
    };
    private final Vibrator m_hVibrator;
    /* access modifiers changed from: private */
    public int m_iCurrentMode = 0;
    /* access modifiers changed from: private */
    public int m_iGridSizeCurrent = 3;
    private int m_iMoves = 0;
    /* access modifiers changed from: private */
    public int m_iNewGroupId = 0;
    /* access modifiers changed from: private */
    public int m_iPuzzlePieceHeight;
    /* access modifiers changed from: private */
    public int m_iPuzzlePieceWidth;
    /* access modifiers changed from: private */
    public long m_lBaseTime = 0;
    /* access modifiers changed from: private */
    public long m_lTimeInMs = 0;
    private long m_lTimeStopAt = 0;
    protected final TextView m_tvImageState;
    private final View m_vDragAndDropDetector;
    private final View m_vGroupDetector;
    private final LinearLayout m_vgGrid;
    /* access modifiers changed from: private */
    public final RelativeLayout m_vgPuzzleContainer;

    /* access modifiers changed from: protected */
    public abstract int calculateNewGridSize();

    /* access modifiers changed from: protected */
    public abstract void dialogResultDismiss(ResultData resultData);

    /* access modifiers changed from: protected */
    public abstract String getDisplayLevel();

    /* access modifiers changed from: protected */
    public abstract ResultData getResultData();

    /* access modifiers changed from: protected */
    public abstract void onBackPress();

    public ScreenPlayBase(int iScreenId, Activity hActivity, IGame hGame) {
        super(iScreenId, hActivity, hGame);
        this.m_hDialogManager = hGame.getDialogManager();
        this.m_hPuzzleEngine = new PuzzleEngine(hActivity);
        this.m_hVibrator = (Vibrator) hActivity.getSystemService("vibrator");
        DisplayMetrics hMetrics = getResources().getDisplayMetrics();
        FrameLayout frameLayout = new FrameLayout(hActivity);
        frameLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        addView(frameLayout);
        LinearLayout linearLayout = new LinearLayout(hActivity);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        frameLayout.addView(linearLayout);
        int iDipGameProgressViewHeight = ResDimens.getDip(hMetrics, 17);
        this.m_hGameProgress = new GameProgressView(hActivity, iDipGameProgressViewHeight);
        linearLayout.addView(this.m_hGameProgress);
        FrameLayout frameLayout2 = new FrameLayout(hActivity);
        frameLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout.addView(frameLayout2);
        this.m_vgPuzzleContainer = new RelativeLayout(hActivity);
        this.m_vgPuzzleContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        frameLayout2.addView(this.m_vgPuzzleContainer);
        this.m_tvImageState = new TextView(hActivity);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        this.m_tvImageState.setLayoutParams(layoutParams);
        this.m_tvImageState.setGravity(17);
        this.m_tvImageState.setTextColor(-1);
        this.m_tvImageState.setTextSize(1, 25.0f);
        this.m_tvImageState.setTypeface(Typeface.DEFAULT, 1);
        this.m_tvImageState.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
        int iDip5 = ResDimens.getDip(hMetrics, 5);
        this.m_tvImageState.setPadding(iDip5, iDip5, iDip5, iDip5);
        this.m_vgPuzzleContainer.addView(this.m_tvImageState);
        RelativeLayout relativeLayout = new RelativeLayout(hActivity);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(hMetrics.widthPixels, hMetrics.widthPixels);
        layoutParams2.addRule(13);
        relativeLayout.setLayoutParams(layoutParams2);
        this.m_vgPuzzleContainer.addView(relativeLayout);
        this.m_vgGrid = new LinearLayout(hActivity);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        this.m_vgGrid.setLayoutParams(layoutParams3);
        this.m_vgGrid.setOrientation(1);
        this.m_vgGrid.setGravity(17);
        relativeLayout.addView(this.m_vgGrid);
        this.m_vDragAndDropDetector = new View(hActivity);
        this.m_vDragAndDropDetector.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m_vDragAndDropDetector.setOnTouchListener(this.m_hOnDragAndDropDetectorTouch);
        frameLayout2.addView(this.m_vDragAndDropDetector);
        this.m_vGroupDetector = new View(hActivity);
        this.m_vGroupDetector.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m_vGroupDetector.setOnTouchListener(this.m_hOnGroupDetectorTouch);
        frameLayout2.addView(this.m_vGroupDetector);
        int iDipButtonHeight = ResDimens.getDip(hMetrics, 48);
        int iDipAvaileableForButton = ((hMetrics.heightPixels - iDipGameProgressViewHeight) - hMetrics.widthPixels) - ResDimens.getDip(hMetrics, 52);
        iDipButtonHeight = iDipAvaileableForButton < iDipButtonHeight ? iDipAvaileableForButton : iDipButtonHeight;
        FrameLayout frameLayout3 = new FrameLayout(hActivity);
        frameLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, iDipButtonHeight));
        linearLayout.addView(frameLayout3);
        ButtonContainer buttonContainer = new ButtonContainer(hActivity, iDipButtonHeight);
        buttonContainer.createButton(0, "btn_img_back", this.m_hOnClick);
        buttonContainer.createButton(1, "btn_img_shuffle", this.m_hOnClick);
        buttonContainer.createButton(2, "btn_img_hint", this.m_hOnClick);
        buttonContainer.createButton(3, "btn_img_group", this.m_hOnClick);
        frameLayout3.addView(buttonContainer);
        ButtonContainer buttonContainer2 = new ButtonContainer(hActivity, iDipButtonHeight);
        this.m_btnVibration = buttonContainer2.createButton(4, "btn_img_back", this.m_hOnClick);
        frameLayout3.addView(buttonContainer2);
        setButtonContainer(buttonContainer, buttonContainer2);
        this.m_hDragAndDropView = new DragAndDropView(hActivity);
        this.m_hDragAndDropView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.m_hDragAndDropView.setVisibility(4);
        frameLayout.addView(this.m_hDragAndDropView);
        switchMode(0);
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        updateButtonVibrationState(Option.getVibration(this.m_hActivity));
        this.m_tvImageState.setVisibility(0);
        hideButtonContainerMore(false);
        updateVibratPermission();
        return true;
    }

    public void onShow() {
        super.onShow();
        this.m_vgPuzzleContainer.startAnimation(this.m_hAnimIn);
        startLevel();
    }

    public void onActivityPause() {
        super.onActivityPause();
        stopTimer();
    }

    public void onActivityResume() {
        super.onActivityResume();
        resumeTimer();
        updateVibratPermission();
    }

    public void onHide() {
        super.onHide();
        this.m_hHandler.removeCallbacks(this.m_hRunnableStartTimer);
    }

    public void onAfterHide() {
        super.onAfterHide();
        this.m_hImageCurrent = null;
        hideButtonContainerMore(false);
    }

    private void updateVibratPermission() {
        this.m_bHasVibratePermission = this.m_hActivity.getPackageManager().checkPermission("android.permission.VIBRATE", this.m_hActivity.getPackageName()) == 0;
    }

    /* access modifiers changed from: protected */
    public void startLevel() {
        System.gc();
        this.m_hGameProgress.setLevel(getDisplayLevel());
        resetData();
        generateGrid(calculateNewGridSize());
        loadImage();
    }

    /* access modifiers changed from: protected */
    public void loadImage() {
        this.m_tvImageState.setVisibility(0);
    }

    private void resetData() {
        this.m_iMoves = 0;
        this.m_hGameProgress.setMoves(this.m_iMoves);
        this.m_lTimeInMs = 0;
        this.m_hGameProgress.setTime("00", "00");
    }

    /* access modifiers changed from: private */
    public long getTime() {
        return System.currentTimeMillis();
    }

    private void startTimer() {
        this.m_lTimeStopAt = 0;
        this.m_lBaseTime = getTime();
        this.m_bTimerStopped = false;
        this.m_hHandler.removeCallbacks(this.m_hRunnableStartTimer);
        this.m_hHandler.post(this.m_hRunnableStartTimer);
    }

    private void stopTimer() {
        this.m_lTimeStopAt = getTime();
        this.m_bTimerStopped = true;
        this.m_hHandler.removeCallbacks(this.m_hRunnableStartTimer);
    }

    private void resumeTimer() {
        if (this.m_bTimerStopped) {
            this.m_bTimerStopped = false;
            this.m_lBaseTime += getTime() - this.m_lTimeStopAt;
            this.m_hHandler.removeCallbacks(this.m_hRunnableStartTimer);
            this.m_hHandler.post(this.m_hRunnableStartTimer);
        }
    }

    /* access modifiers changed from: private */
    public void switchMode(int iMode) {
        switch (iMode) {
            case 0:
                this.m_vgGrid.setBackgroundColor(0);
                this.m_vGroupDetector.setVisibility(8);
                this.m_vDragAndDropDetector.setVisibility(0);
                break;
            case 1:
                this.m_vgGrid.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
                this.m_hPuzzleEngine.highlightForGroup();
                this.m_hPuzzleEngine.ungroupAll();
                this.m_vGroupDetector.setVisibility(0);
                this.m_vDragAndDropDetector.setVisibility(8);
                break;
            default:
                throw new RuntimeException("Invalid mode");
        }
        this.m_iCurrentMode = iMode;
    }

    /* access modifiers changed from: protected */
    public void setImage(Bitmap hImage) {
        this.m_hImageCurrent = hImage;
        int iGridSize = calculateNewGridSize();
        if (this.m_iGridSizeCurrent != iGridSize) {
            generateGrid(iGridSize);
        }
        this.m_hPuzzlePieceContainerDragged = null;
        this.m_hPuzzleEngine.setImage(hImage);
        this.m_iPuzzlePieceWidth = this.m_hPuzzleEngine.getPuzzlePieceWidth();
        this.m_iPuzzlePieceHeight = this.m_hPuzzleEngine.getPuzzlePieceHeight();
        resetData();
        startTimer();
        this.m_tvImageState.setVisibility(8);
        this.m_hGame.unlockScreen();
    }

    private void generateGrid(int iGridSize) {
        int i = 3;
        if (iGridSize > 3) {
            i = iGridSize;
        }
        this.m_iGridSizeCurrent = i;
        this.m_hPuzzleEngine.init(iGridSize);
        this.m_vgGrid.removeAllViews();
        LinearLayout.LayoutParams hParamsRow = new LinearLayout.LayoutParams(-1, 0, 1.0f);
        LinearLayout.LayoutParams hParamsColumn = new LinearLayout.LayoutParams(0, -1, 1.0f);
        int iDipHintLineSize = ResDimens.getDip(getResources().getDisplayMetrics(), 1);
        int iCounter = 0;
        for (int iRow = 0; iRow < iGridSize; iRow++) {
            LinearLayout vgColumn = new LinearLayout(this.m_hActivity);
            vgColumn.setOrientation(0);
            vgColumn.setGravity(17);
            this.m_vgGrid.addView(vgColumn, hParamsRow);
            for (int iColumn = 0; iColumn < iGridSize; iColumn++) {
                PuzzlePieceContainer hPieceContainer = new PuzzlePieceContainer(this.m_hActivity, iColumn, iRow, iDipHintLineSize);
                vgColumn.addView(hPieceContainer, hParamsColumn);
                hPieceContainer.setId(iCounter);
                hPieceContainer.setOnTouchListener(this.m_hOnPuzzlePieceTouch);
                this.m_hPuzzleEngine.addContainer(hPieceContainer);
                iCounter++;
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkPuzzle() {
        if (this.m_hPuzzleEngine.isSolved()) {
            solved(this.m_iMoves, this.m_lTimeInMs);
        }
    }

    /* access modifiers changed from: protected */
    public void solved(int iMoves, long lTimeInMs) {
        this.m_hGame.lockScreen();
        stopTimer();
        generateGrid(calculateNewGridSize());
        this.m_hDragAndDropView.hide();
        this.m_hPuzzleEngine.ungroupAll();
        final ResultData hResultData = getResultData();
        hResultData.hImage = this.m_hImageCurrent;
        hResultData.iMovesNew = iMoves;
        hResultData.lTimeInMsNew = lTimeInMs;
        this.m_hHandler.post(new Runnable() {
            public void run() {
                DialogManager dialogManager = ScreenPlayBase.this.m_hDialogManager;
                ResultData resultData = hResultData;
                final ResultData resultData2 = hResultData;
                dialogManager.showPuzzleSolved(resultData, new DialogInterface.OnDismissListener() {
                    public void onDismiss(DialogInterface dialog2) {
                        Handler handler = ScreenPlayBase.this.m_hHandler;
                        final ResultData resultData = resultData2;
                        handler.post(new Runnable() {
                            public void run() {
                                if (ScreenPlayBase.this.m_bIsCurrentScreen) {
                                    ScreenPlayBase.this.dialogResultDismiss(resultData);
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public String getPlayerName() {
        return PreferenceManager.getDefaultSharedPreferences(this.m_hActivity).getString(PREF_KEY_PLAYER_NAME, ResString.default_user_name);
    }

    public static final class ResultData {
        public Bitmap hImage;
        public final int iGameMode;
        public int iImageIndex;
        public int iMovesNew;
        public int iMovesRecord;
        public long lTimeInMsNew;
        public long lTimeInMsRecord;
        public String sMovesNameRecord;
        public String sPuzzleId;
        public String sPuzzleImageInfo;
        public String sTimeInMsNameRecord;

        public ResultData(int iGameMode2) {
            this.iGameMode = iGameMode2;
        }
    }

    /* access modifiers changed from: private */
    public void switchPieces(PuzzlePiece hPieceFirst, PuzzlePiece hPieceSecond) {
        PuzzlePieceContainer hContainerFirst = hPieceFirst.getContainer();
        PuzzlePieceContainer hContainerSecond = hPieceSecond.getContainer();
        hContainerFirst.setPuzzlePiece(hPieceSecond);
        hContainerSecond.setPuzzlePiece(hPieceFirst);
        if (!hPieceFirst.equals(hPieceSecond)) {
            this.m_bMoved = true;
        }
    }

    /* access modifiers changed from: private */
    public void hasMoved() {
        if (this.m_bMoved) {
            this.m_bMoved = false;
            if (this.m_hPuzzleEngine.hasGroup()) {
                this.m_hPuzzleEngine.ungroupAll();
            }
            GameProgressView gameProgressView = this.m_hGameProgress;
            int i = this.m_iMoves + 1;
            this.m_iMoves = i;
            gameProgressView.setMoves(i);
            if (Option.getVibration(this.m_hActivity) && this.m_bHasVibratePermission) {
                this.m_hVibrator.vibrate(25);
                System.gc();
            }
        }
    }

    public boolean dispatchKeyEvent(KeyEvent hEvent) {
        switch (hEvent.getKeyCode()) {
            case 82:
                if (1 == hEvent.getAction()) {
                    if (isButtonContainerMoreVisible()) {
                        hideButtonContainerMore(true);
                    } else {
                        showButtonContainerMore();
                    }
                }
                return true;
            default:
                return super.dispatchKeyEvent(hEvent);
        }
    }

    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        boolean bVibrationSwitch;
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                onBackPress();
                return;
            case 1:
                this.m_hDialogManager.showReshuffle(this.m_hOnDialogReshuffleClick);
                return;
            case 2:
                this.m_hDialogManager.showHintColor(new DialogScreenHintColor.OnColorChangedListener() {
                    public void colorChanged(int iColor) {
                        Option.setHintColor(ScreenPlayBase.this.m_hActivity, iColor);
                        ScreenPlayBase.this.checkPuzzle();
                        ScreenPlayBase.this.invalidate();
                    }
                });
                return;
            case 3:
                if (this.m_hPuzzleEngine.hasGroup()) {
                    this.m_hPuzzleEngine.ungroupAll();
                    this.m_hGame.unlockScreen();
                    return;
                }
                if (this.m_iCurrentMode == 1) {
                    this.m_hPuzzleEngine.clearHighlightForGroup();
                    switchMode(0);
                } else {
                    switchMode(1);
                }
                this.m_hGame.unlockScreen();
                return;
            case 4:
                if (Option.getVibration(this.m_hActivity)) {
                    bVibrationSwitch = false;
                } else {
                    bVibrationSwitch = true;
                }
                Option.setVibration(this.m_hActivity, bVibrationSwitch);
                updateButtonVibrationState(bVibrationSwitch);
                this.m_hGame.unlockScreen();
                return;
            default:
                throw new RuntimeException("Invalid view id");
        }
    }

    private void updateButtonVibrationState(boolean bVibration) {
        if (bVibration) {
            this.m_btnVibration.setIcon(Res.drawable(this.m_hActivity, "btn_img_vibration_on"));
        } else {
            this.m_btnVibration.setIcon(Res.drawable(this.m_hActivity, "btn_img_vibration_off"));
        }
    }

    private static final class GameProgressView extends LinearLayout {
        private final TextView m_tvLevel;
        private final TextView m_tvMoves;
        private final TextView m_tvTime;

        public GameProgressView(Context hContext, int iHeight) {
            super(hContext);
            setLayoutParams(new LinearLayout.LayoutParams(-1, iHeight));
            setOrientation(0);
            setGravity(17);
            int iDip1 = ResDimens.getDip(getResources().getDisplayMetrics(), 1);
            this.m_tvLevel = createTextView(hContext, iDip1, 19);
            this.m_tvMoves = createTextView(hContext, iDip1, 17);
            this.m_tvTime = createTextView(hContext, iDip1, 21);
        }

        private TextView createTextView(Context hContext, int iPadding, int iGravity) {
            TextView tvText = new TextView(hContext);
            tvText.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
            tvText.setSingleLine(true);
            tvText.setTextColor(-1);
            tvText.setTextSize(1, 14.0f);
            tvText.setTypeface(Typeface.DEFAULT, 1);
            tvText.setPadding(iPadding, 0, iPadding, 0);
            tvText.setGravity(iGravity);
            addView(tvText);
            return tvText;
        }

        public void setLevel(String sText) {
            this.m_tvLevel.setText(sText);
        }

        public void setMoves(int iMoves) {
            this.m_tvMoves.setText(String.format(ResString.screen_puzzle_label_moves, Integer.valueOf(iMoves)));
        }

        public void setTime(String sMin, String sSec) {
            this.m_tvTime.setText("TIME: " + sMin + ":" + sSec);
        }
    }

    private static final class DragAndDropView extends View {
        private Bitmap m_hBitmap;
        private Rect m_hRectNewDes = new Rect();
        private Rect m_hRectOld = new Rect();
        private Rect m_hRectSrc = new Rect();

        public DragAndDropView(Context hContext) {
            super(hContext);
        }

        public void setBitmap(Bitmap hBitmap) {
            this.m_hBitmap = hBitmap;
            if (hBitmap != null) {
                this.m_hRectSrc.left = 0;
                this.m_hRectSrc.top = 0;
                this.m_hRectSrc.right = this.m_hBitmap.getWidth();
                this.m_hRectSrc.bottom = this.m_hBitmap.getHeight();
            }
        }

        public void show() {
            setVisibility(0);
        }

        public void hide() {
            this.m_hBitmap = null;
            setVisibility(4);
        }

        public void draw(int iLeft, int iTop) {
            if (this.m_hBitmap != null) {
                invalidate(this.m_hRectOld);
                this.m_hRectNewDes.left = iLeft;
                this.m_hRectNewDes.top = iTop;
                this.m_hRectNewDes.right = this.m_hBitmap.getWidth() + iLeft;
                this.m_hRectNewDes.bottom = this.m_hBitmap.getHeight() + iTop;
                invalidate(this.m_hRectNewDes);
            }
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas hCanvas) {
            super.onDraw(hCanvas);
            if (this.m_hBitmap != null) {
                hCanvas.drawBitmap(this.m_hBitmap, this.m_hRectSrc, this.m_hRectNewDes, (Paint) null);
                this.m_hRectOld.left = this.m_hRectNewDes.left;
                this.m_hRectOld.top = this.m_hRectNewDes.top;
                this.m_hRectOld.right = this.m_hRectNewDes.right;
                this.m_hRectOld.bottom = this.m_hRectNewDes.bottom;
            }
        }
    }
}
