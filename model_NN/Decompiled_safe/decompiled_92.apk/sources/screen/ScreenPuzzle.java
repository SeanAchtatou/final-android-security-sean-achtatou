package screen;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Process;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cfg.Option;
import data.Puzzle;
import data.PuzzleImage;
import data.PuzzleVerifier;
import dialog.DialogManager;
import game.IGame;
import java.util.List;
import res.ResDimens;
import res.ResString;
import view.ButtonContainer;
import view.CustomButton;
import view.TitleView;

public final class ScreenPuzzle extends ScreenPuzzleBase {
    private static final int BUTTON_ID_BACK = 0;
    private static final int BUTTON_ID_INFO = 1;
    private static final int BUTTON_ID_NEXT = 2;
    private boolean m_bReset;
    private final CustomButton m_btnInfo;
    /* access modifiers changed from: private */
    public final PuzzleDetailAdapter m_hAdapter;
    private final DialogManager m_hDialogManager;
    private final AdapterView.OnItemClickListener m_hOnItemClick = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View v, int iPos, long lId) {
            if (iPos >= 0 && ScreenPuzzle.this.m_bIsCurrentScreen && !ScreenPuzzle.this.m_hGame.isScreenLocked()) {
                ScreenPuzzle.this.play(iPos);
            }
        }
    };
    final Runnable m_hRunnableProgress = new Runnable() {
        public void run() {
            ScreenPuzzle.this.m_pbProgress.setVisibility(0);
        }
    };
    final Runnable m_hRunnableVerified = new Runnable() {
        public void run() {
            if (ScreenPuzzle.this.m_hGame.isScreenLocked()) {
                ScreenPuzzle.this.m_pbProgress.setVisibility(8);
                ScreenPuzzle.this.m_hAdapter.loadData(ScreenPuzzle.this.m_hPuzzle);
                ScreenPuzzle.this.m_hAdapter.notifyDataSetChanged();
                ScreenPuzzle.this.m_lvList.startAnimation(ScreenPuzzle.this.m_hAnimIn);
                ScreenPuzzle.this.m_hGame.unlockScreen();
            }
        }
    };
    private final TitleView m_hTitle;
    private int m_iImageIndexSelected;
    /* access modifiers changed from: private */
    public final ListView m_lvList;
    /* access modifiers changed from: private */
    public final View m_pbProgress;

    public ScreenPuzzle(Activity hActivity, IGame hGame) {
        super(2, hActivity, hGame);
        this.m_hDialogManager = hGame.getDialogManager();
        this.m_hTitle = new TitleView(hActivity, getResources().getDisplayMetrics());
        addView(this.m_hTitle);
        RelativeLayout vgContentContainer = new RelativeLayout(hActivity);
        vgContentContainer.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        addView(vgContentContainer);
        this.m_pbProgress = new ProgressBar(hActivity, null, 16842874);
        RelativeLayout.LayoutParams hProgressBarParams = new RelativeLayout.LayoutParams(-2, -2);
        hProgressBarParams.addRule(13);
        this.m_pbProgress.setLayoutParams(hProgressBarParams);
        vgContentContainer.addView(this.m_pbProgress);
        this.m_lvList = new ListView(hActivity);
        RelativeLayout.LayoutParams hListParams = new RelativeLayout.LayoutParams(-1, -1);
        hListParams.addRule(13);
        this.m_lvList.setLayoutParams(hListParams);
        this.m_lvList.setCacheColorHint(0);
        this.m_lvList.setOnItemClickListener(this.m_hOnItemClick);
        this.m_lvList.setFocusable(false);
        this.m_hAdapter = new PuzzleDetailAdapter();
        this.m_lvList.setAdapter((ListAdapter) this.m_hAdapter);
        vgContentContainer.addView(this.m_lvList);
        ButtonContainer vgButtonContainer = new ButtonContainer(hActivity);
        vgButtonContainer.createButton(0, "btn_img_back", this.m_hOnClick);
        this.m_btnInfo = vgButtonContainer.createButton(1, "btn_img_info", this.m_hOnClick);
        vgButtonContainer.createButton(2, "btn_img_next", this.m_hOnClick);
        addView(vgButtonContainer);
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        this.m_bReset = bReset;
        if (bReset) {
            this.m_hAdapter.loadData(null);
            this.m_hAdapter.notifyDataSetChanged();
            PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImageSelected();
            if (hPuzzleImage != null) {
                this.m_iImageIndexSelected = hPuzzleImage.getIndex();
            }
        } else {
            Animation hAnim = new AlphaAnimation(0.0f, 0.0f);
            hAnim.setDuration(0);
            hAnim.setFillAfter(true);
            this.m_lvList.startAnimation(hAnim);
        }
        this.m_pbProgress.setVisibility(8);
        if (TextUtils.isEmpty(this.m_hPuzzle.getInfo())) {
            this.m_btnInfo.setEnabled(false);
        } else {
            this.m_btnInfo.setEnabled(true);
        }
        this.m_hTitle.setTitle(this.m_hPuzzle.getName());
        return true;
    }

    public void onShow() {
        int iImageIndex;
        super.onShow();
        this.m_hGame.lockScreen();
        if (!this.m_bReset) {
            this.m_hAdapter.loadData(this.m_hPuzzle);
            this.m_hAdapter.notifyDataSetChanged();
            PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImageSelected();
            if (!(hPuzzleImage == null || (iImageIndex = hPuzzleImage.getIndex()) == this.m_iImageIndexSelected)) {
                this.m_lvList.setSelection(iImageIndex - 1);
            }
            this.m_lvList.startAnimation(this.m_hAnimIn);
            this.m_hGame.unlockScreen();
            return;
        }
        new Thread(new Runnable() {
            public void run() {
                Process.setThreadPriority(10);
                ScreenPuzzle.this.m_hHandler.postDelayed(ScreenPuzzle.this.m_hRunnableVerified, 5000);
                ScreenPuzzle.this.m_hHandler.postDelayed(ScreenPuzzle.this.m_hRunnableProgress, 500);
                boolean bVerified = PuzzleVerifier.verifiy(ScreenPuzzle.this.m_hActivity, ScreenPuzzle.this.m_hPuzzle);
                ScreenPuzzle.this.m_hHandler.removeCallbacks(ScreenPuzzle.this.m_hRunnableVerified);
                ScreenPuzzle.this.m_hHandler.removeCallbacks(ScreenPuzzle.this.m_hRunnableProgress);
                if (ScreenPuzzle.this.m_hPuzzle != null) {
                    if (bVerified) {
                        ScreenPuzzle.this.m_hHandler.post(ScreenPuzzle.this.m_hRunnableVerified);
                    } else {
                        ScreenPuzzle.this.m_hHandler.post(new Runnable() {
                            public void run() {
                                ScreenPuzzle.this.m_pbProgress.setVisibility(8);
                                ScreenPuzzle.this.m_hGame.switchScreen(13);
                            }
                        });
                    }
                }
            }
        }).start();
    }

    public void onHide() {
        this.m_hPuzzle.clearPuzzleImageTempData();
        super.onHide();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: game.IGame.switchScreen(int, boolean):void
     arg types: [int, int]
     candidates:
      game.IGame.switchScreen(int, java.lang.Object):void
      game.IGame.switchScreen(int, boolean):void */
    /* access modifiers changed from: protected */
    public void onClick(int iViewId) {
        super.onClick(iViewId);
        switch (iViewId) {
            case 0:
                this.m_hGame.switchScreen(9, true);
                return;
            case 1:
                this.m_hDialogManager.showPuzzleInfo(this.m_hPuzzle.getInfo());
                return;
            case 2:
                for (PuzzleImage hPuzzleImageLoop : this.m_hPuzzle.getPuzzleImage()) {
                    if (hPuzzleImageLoop.isNextUnsolved()) {
                        play(hPuzzleImageLoop.getIndex());
                        return;
                    }
                }
                if (this.m_hPuzzle.getPuzzleImage().size() > 0) {
                    play(0);
                    return;
                } else {
                    this.m_hGame.unlockScreen();
                    return;
                }
            default:
                throw new RuntimeException("Invalid view id");
        }
    }

    /* access modifiers changed from: private */
    public void play(int iPuzzleImageIndex) {
        PuzzleImage hPuzzleImage = this.m_hPuzzle.getPuzzleImage().get(iPuzzleImageIndex);
        if (hPuzzleImage != null && hPuzzleImage.isPlayable()) {
            this.m_iImageIndexSelected = iPuzzleImageIndex;
            this.m_hPuzzle.setPuzzleImageSelected(hPuzzleImage);
            this.m_hGame.switchScreen(3);
        }
    }

    private final class PuzzleDetailAdapter extends BaseAdapter {
        private Puzzle m_hPuzzleAdapter;
        private int m_iAdapterCount = 0;
        private final int m_iDipThumbnailSize;
        private final String m_sLevel;
        private final String m_sMoves;
        private final String m_sTime;

        public PuzzleDetailAdapter() {
            this.m_iDipThumbnailSize = ResDimens.getDip(ScreenPuzzle.this.getResources().getDisplayMetrics(), 96);
            this.m_sLevel = "Level ";
            this.m_sMoves = "Moves: ";
            this.m_sTime = "Time: ";
        }

        public void loadData(Puzzle hPuzzle) {
            if (hPuzzle == null) {
                this.m_hPuzzleAdapter = null;
                this.m_iAdapterCount = 0;
                return;
            }
            this.m_hPuzzleAdapter = hPuzzle;
            this.m_iAdapterCount = hPuzzle.getImageIndexNextUnsolved();
            if (this.m_iAdapterCount < hPuzzle.getPuzzleImage().size()) {
                this.m_iAdapterCount++;
            }
        }

        public int getCount() {
            return this.m_iAdapterCount;
        }

        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public PuzzleImage getItem(int iPos) {
            if (this.m_hPuzzleAdapter == null) {
                return null;
            }
            List<PuzzleImage> arrPuzzleImage = this.m_hPuzzleAdapter.getPuzzleImage();
            if (arrPuzzleImage.size() < iPos) {
                return null;
            }
            return arrPuzzleImage.get(iPos);
        }

        public long getItemId(int iId) {
            return (long) iId;
        }

        public View getView(int iPos, View vConvert, ViewGroup vgParent) {
            ListItem hListItem;
            if (vConvert == null) {
                hListItem = new ListItem(ScreenPuzzle.this.m_hActivity);
            } else {
                hListItem = (ListItem) vConvert;
            }
            PuzzleImage hPuzzleImage = getItem(iPos);
            hListItem.tvLevel.setText(String.valueOf(this.m_sLevel) + (iPos + 1));
            if (hPuzzleImage.getBestMoves() <= 0) {
                hListItem.tvBestMoves.setText(String.valueOf(this.m_sMoves) + "---");
                hListItem.tvBestMovesName.setText((CharSequence) null);
            } else {
                hListItem.tvBestMoves.setText(String.valueOf(this.m_sMoves) + hPuzzleImage.getBestMoves());
                hListItem.tvBestMovesName.setText(hPuzzleImage.getBestMovesName());
            }
            long lTimeInMS = hPuzzleImage.getBestTimeInMs();
            if (lTimeInMS <= 0) {
                hListItem.tvBestTime.setText(String.valueOf(this.m_sTime) + "---");
                hListItem.tvBestTimeName.setText((CharSequence) null);
            } else {
                hListItem.tvBestTime.setText(String.valueOf(this.m_sTime) + String.format("%.3f s", Float.valueOf(((float) lTimeInMS) / 1000.0f)));
                hListItem.tvBestTimeName.setText(hPuzzleImage.getBestTimeInMsName());
            }
            if (hPuzzleImage.isSolved()) {
                Bitmap hThumbnail = hPuzzleImage.getThumbnail(ScreenPuzzle.this.m_hActivity, this.m_hPuzzleAdapter, this.m_iDipThumbnailSize);
                hListItem.ivThumbnail.setImageBitmap(hThumbnail);
                hListItem.tvImageState.setText((CharSequence) null);
                if (hThumbnail == null) {
                    int iIndexClearTo = ScreenPuzzle.this.m_lvList.getFirstVisiblePosition() - 1;
                    int iIndexClearFrom = ScreenPuzzle.this.m_lvList.getLastVisiblePosition() + 1;
                    List<PuzzleImage> arrPuzzleImage = ScreenPuzzle.this.m_hPuzzle.getPuzzleImage();
                    for (int i = 0; i < iIndexClearTo; i++) {
                        PuzzleImage hImageDataNext = arrPuzzleImage.get(i);
                        hImageDataNext.setImage(null, null);
                        hImageDataNext.setThumbnail(null);
                    }
                    int iSize = arrPuzzleImage.size();
                    if (iIndexClearFrom < iSize) {
                        for (int i2 = iIndexClearFrom; i2 < iSize; i2++) {
                            PuzzleImage hImageDataNext2 = arrPuzzleImage.get(i2);
                            hImageDataNext2.setImage(null, null);
                            hImageDataNext2.setThumbnail(null);
                        }
                    }
                    notifyDataSetChanged();
                }
            } else {
                hListItem.tvImageState.setText(ResString.image_unsolved);
                hListItem.ivThumbnail.setImageBitmap(null);
            }
            return hListItem;
        }

        private final class ListItem extends LinearLayout {
            public final ImageView ivThumbnail;
            public final TextView tvBestMoves;
            public final TextView tvBestMovesName;
            public final TextView tvBestTime;
            public final TextView tvBestTimeName;
            public final TextView tvImageState;
            public final TextView tvLevel;

            public ListItem(Context hContext) {
                super(hContext);
                DisplayMetrics hMetrics = getResources().getDisplayMetrics();
                setLayoutParams(new AbsListView.LayoutParams(-1, -1));
                setOrientation(0);
                setGravity(17);
                int iDip10 = ResDimens.getDip(hMetrics, 10);
                setPadding(iDip10, iDip10, iDip10, iDip10);
                LinearLayout linearLayout = new LinearLayout(hContext);
                linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
                linearLayout.setGravity(17);
                addView(linearLayout);
                RelativeLayout relativeLayout = new RelativeLayout(hContext);
                int iDipThumbnailBorderSize = ResDimens.getDip(hMetrics, 100);
                relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(iDipThumbnailBorderSize, iDipThumbnailBorderSize));
                relativeLayout.setBackgroundColor(-1);
                linearLayout.addView(relativeLayout);
                this.tvImageState = new TextView(hContext);
                int iDipThumbnailSize = ResDimens.getDip(hMetrics, 96);
                RelativeLayout.LayoutParams hThumbnailBackgroundParams = new RelativeLayout.LayoutParams(iDipThumbnailSize, iDipThumbnailSize);
                hThumbnailBackgroundParams.addRule(13);
                this.tvImageState.setLayoutParams(hThumbnailBackgroundParams);
                this.tvImageState.setGravity(17);
                this.tvImageState.setBackgroundColor(Option.HINT_COLOR_DEFAULT);
                this.tvImageState.setTextColor(-1);
                this.tvImageState.setTextSize(1, 13.0f);
                this.tvImageState.setTypeface(Typeface.DEFAULT, 1);
                this.tvImageState.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
                int iDip5 = ResDimens.getDip(hMetrics, 5);
                this.tvImageState.setPadding(iDip5, iDip5, iDip5, iDip5);
                relativeLayout.addView(this.tvImageState);
                this.ivThumbnail = new ImageView(hContext);
                RelativeLayout.LayoutParams hThumbnailParams = new RelativeLayout.LayoutParams(iDipThumbnailSize, iDipThumbnailSize);
                hThumbnailParams.addRule(13);
                this.ivThumbnail.setLayoutParams(hThumbnailParams);
                relativeLayout.addView(this.ivThumbnail);
                LinearLayout linearLayout2 = new LinearLayout(hContext);
                linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(0, -1, 1.0f));
                linearLayout2.setOrientation(1);
                linearLayout2.setGravity(19);
                linearLayout2.setPadding(iDip10, 0, 0, 0);
                addView(linearLayout2);
                this.tvLevel = getTextView(hContext, 22);
                this.tvLevel.setPadding(0, 0, 0, iDip10);
                linearLayout2.addView(this.tvLevel);
                this.tvBestMoves = getTextView(hContext, 16);
                linearLayout2.addView(this.tvBestMoves);
                this.tvBestMovesName = getTextView(hContext, 16);
                linearLayout2.addView(this.tvBestMovesName);
                this.tvBestTime = getTextView(hContext, 16);
                linearLayout2.addView(this.tvBestTime);
                this.tvBestTimeName = getTextView(hContext, 16);
                linearLayout2.addView(this.tvBestTimeName);
            }

            private TextView getTextView(Context hContext, int iTextSize) {
                TextView tvText = new TextView(hContext);
                tvText.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
                tvText.setSingleLine();
                tvText.setTextColor(-1);
                tvText.setTextSize(1, (float) iTextSize);
                tvText.setTypeface(Typeface.DEFAULT, 1);
                tvText.setShadowLayer(0.5f, 0.0f, 0.0f, Option.HINT_COLOR_DEFAULT);
                return tvText;
            }
        }
    }
}
