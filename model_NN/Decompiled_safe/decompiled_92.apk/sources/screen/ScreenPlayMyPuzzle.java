package screen;

import android.app.Activity;
import android.view.KeyEvent;
import com.adwhirl.util.AdWhirlUtil;
import data.MyPuzzle;
import data.MyPuzzleImage;
import game.IGame;
import helper.BitmapHelper;
import helper.Constants;
import screen.ScreenPlayBase;

public class ScreenPlayMyPuzzle extends ScreenPlayBase {
    private MyPuzzle m_hMyPuzzle;
    private MyPuzzleImage m_hMyPuzzleImage;
    private final int m_iDisplayWidth = getResources().getDisplayMetrics().widthPixels;
    private int m_iMyPuzzleImageIndex;

    public ScreenPlayMyPuzzle(Activity hActivity, IGame hGame) {
        super(7, hActivity, hGame);
    }

    public boolean beforeShow(int iComeFromScreenId, boolean bReset, Object hData) {
        super.beforeShow(iComeFromScreenId, bReset, hData);
        Object[] arrData = (Object[]) hData;
        this.m_hMyPuzzle = (MyPuzzle) arrData[0];
        this.m_iMyPuzzleImageIndex = ((Integer) arrData[1]).intValue();
        return true;
    }

    /* access modifiers changed from: protected */
    public String getDisplayLevel() {
        return this.m_hMyPuzzle.getName();
    }

    /* access modifiers changed from: protected */
    public int calculateNewGridSize() {
        return this.m_hMyPuzzleImage.getGridSize();
    }

    /* access modifiers changed from: protected */
    public void startLevel() {
        this.m_hMyPuzzleImage = this.m_hMyPuzzle.get(this.m_iMyPuzzleImageIndex);
        super.startLevel();
    }

    /* access modifiers changed from: protected */
    public void loadImage() {
        super.loadImage();
        if (this.m_hMyPuzzleImage.getImage() == null) {
            this.m_hGame.switchScreen(11);
        } else {
            setImage(BitmapHelper.scaleBitmap(this.m_hMyPuzzleImage.getImage(), this.m_iDisplayWidth));
        }
    }

    /* access modifiers changed from: protected */
    public ScreenPlayBase.ResultData getResultData() {
        ScreenPlayBase.ResultData hResultData = new ScreenPlayBase.ResultData(3);
        hResultData.sPuzzleId = null;
        hResultData.iImageIndex = -1;
        hResultData.sMovesNameRecord = this.m_hMyPuzzleImage.getMovesName();
        hResultData.iMovesRecord = this.m_hMyPuzzleImage.getMoves();
        hResultData.sTimeInMsNameRecord = this.m_hMyPuzzleImage.getTimeInMsName();
        hResultData.lTimeInMsRecord = this.m_hMyPuzzleImage.getTimeInMs();
        return hResultData;
    }

    /* access modifiers changed from: protected */
    public void dialogResultDismiss(ScreenPlayBase.ResultData hResultData) {
        String sPlayerName = getPlayerName();
        boolean bSave = false;
        if (hResultData.lTimeInMsNew < this.m_hMyPuzzleImage.getTimeInMs() || this.m_hMyPuzzleImage.getTimeInMs() <= 0) {
            this.m_hMyPuzzleImage.setTimeInMs(sPlayerName, hResultData.lTimeInMsNew);
            bSave = true;
        }
        if (hResultData.iMovesNew < this.m_hMyPuzzleImage.getMoves() || this.m_hMyPuzzleImage.getMoves() <= 0) {
            this.m_hMyPuzzleImage.setMoves(sPlayerName, hResultData.iMovesNew);
            bSave = true;
        }
        if (bSave) {
            this.m_hMyPuzzle.saveToSD();
        }
        this.m_iMyPuzzleImageIndex = this.m_iMyPuzzleImageIndex < this.m_hMyPuzzle.size() - 1 ? this.m_iMyPuzzleImageIndex + 1 : 0;
        startLevel();
        this.m_hHandler.post(new Runnable() {
            public void run() {
                ScreenPlayMyPuzzle.this.m_hDialogManager.dismiss();
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onBackPress() {
        this.m_hDialogManager.showLeavePuzzle(11);
    }

    public boolean dispatchKeyEvent(KeyEvent event) {
        if (!Constants.debug()) {
            return super.dispatchKeyEvent(event);
        }
        switch (event.getKeyCode()) {
            case AdWhirlUtil.NETWORK_TYPE_ONERIOT:
                if (1 == event.getAction()) {
                    solved(10000, 1000000);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }
}
