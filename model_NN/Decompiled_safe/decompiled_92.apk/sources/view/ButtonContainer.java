package view;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import res.Res;
import res.ResDimens;

public final class ButtonContainer extends LinearLayout {
    private final Context m_hContext;
    private final int m_iDipButtonHeight;

    public ButtonContainer(Context hContext) {
        super(hContext);
        this.m_hContext = hContext;
        this.m_iDipButtonHeight = ResDimens.getDip(getResources().getDisplayMetrics(), 48);
        init(this.m_iDipButtonHeight);
    }

    public ButtonContainer(Context hContext, int iDipHeight) {
        super(hContext);
        this.m_hContext = hContext;
        this.m_iDipButtonHeight = iDipHeight;
        init(this.m_iDipButtonHeight);
    }

    private void init(int iDipHeight) {
        setLayoutParams(new LinearLayout.LayoutParams(-1, iDipHeight));
        setOrientation(0);
    }

    public CustomButton createButton(int iViewId, String sDrawableResName, View.OnClickListener hOnClick) {
        CustomButton btnButton = new CustomButton(this.m_hContext, iViewId);
        btnButton.setLayoutParams(new LinearLayout.LayoutParams(0, this.m_iDipButtonHeight, 1.0f));
        btnButton.setOnClickListener(hOnClick);
        btnButton.setIcon(Res.drawable(this.m_hContext, sDrawableResName));
        addView(btnButton);
        return btnButton;
    }
}
