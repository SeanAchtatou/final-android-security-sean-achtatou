package view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import cfg.Option;
import game.PuzzlePiece;

public final class PuzzlePieceContainer extends RelativeLayout {
    /* access modifiers changed from: private */
    public boolean m_bDrawHint = true;
    private final ImageContainer m_hImageContainer;
    private PuzzlePiece m_hPuzzlePiece;
    private final int m_iColumn;
    private final int m_iRow;

    public PuzzlePieceContainer(Context context, int iColumn, int iRow, int iHintLineSize) {
        super(context);
        this.m_iColumn = iColumn;
        this.m_iRow = iRow;
        this.m_hImageContainer = new ImageContainer(context, iHintLineSize);
        addView(this.m_hImageContainer);
    }

    public void setHintColor(int iHintColor) {
        this.m_hImageContainer.setHintColor(iHintColor);
    }

    public void correctNeightbors(boolean bLeft, boolean bTop, boolean bRight, boolean bBottom) {
        this.m_hImageContainer.correctNeightbors(bLeft, bTop, bRight, bBottom);
    }

    public void setPuzzlePiece(PuzzlePiece hPuzzlePiece) {
        this.m_hPuzzlePiece = hPuzzlePiece;
        this.m_hImageContainer.setImageBitmap(hPuzzlePiece.getBitmap());
        hPuzzlePiece.setContainer(this);
    }

    public void update() {
        if (this.m_hPuzzlePiece != null) {
            this.m_hImageContainer.setImageBitmap(this.m_hPuzzlePiece.getBitmap());
        }
    }

    public PuzzlePiece getPuzzlePiece() {
        return this.m_hPuzzlePiece;
    }

    public int getColumn() {
        return this.m_iColumn;
    }

    public int getRow() {
        return this.m_iRow;
    }

    public void setAlpha(int iAlpha) {
        this.m_hImageContainer.setAlpha(iAlpha);
    }

    private final class ImageContainer extends ImageView {
        private boolean m_bCorrectNeighborBottom = false;
        private boolean m_bCorrectNeighborLeft = false;
        private boolean m_bCorrectNeighborRight = false;
        private boolean m_bCorrectNeighborTop = false;
        private final Paint m_hPaintLine = new Paint();
        private final Rect m_hRectBottom = new Rect();
        private final Rect m_hRectLeft = new Rect();
        private final Rect m_hRectRight = new Rect();
        private final Rect m_hRectTop = new Rect();
        private final int m_iHintLinesSize;

        public ImageContainer(Context context, int iHintLineSize) {
            super(context);
            this.m_iHintLinesSize = iHintLineSize;
            this.m_hPaintLine.setColor((int) Option.HINT_COLOR_DEFAULT);
        }

        public void setHintColor(int iHintColor) {
            if (-1 == iHintColor) {
                PuzzlePieceContainer.this.m_bDrawHint = false;
                return;
            }
            PuzzlePieceContainer.this.m_bDrawHint = true;
            this.m_hPaintLine.setColor(iHintColor);
        }

        public void correctNeightbors(boolean bLeft, boolean bTop, boolean bRight, boolean bBottom) {
            this.m_bCorrectNeighborLeft = bLeft;
            this.m_bCorrectNeighborTop = bTop;
            this.m_bCorrectNeighborRight = bRight;
            this.m_bCorrectNeighborBottom = bBottom;
        }

        public void setImageBitmap(Bitmap hBitmap) {
            if (hBitmap != null) {
                int iWidth = hBitmap.getWidth();
                int iHeight = hBitmap.getHeight();
                this.m_hRectLeft.left = 0;
                this.m_hRectLeft.top = 0;
                this.m_hRectLeft.right = this.m_iHintLinesSize;
                this.m_hRectLeft.bottom = iHeight;
                this.m_hRectTop.left = 0;
                this.m_hRectTop.top = 0;
                this.m_hRectTop.right = iWidth;
                this.m_hRectTop.bottom = this.m_iHintLinesSize;
                this.m_hRectRight.left = iWidth - this.m_iHintLinesSize;
                this.m_hRectRight.top = 0;
                this.m_hRectRight.right = iWidth;
                this.m_hRectRight.bottom = iHeight;
                this.m_hRectBottom.left = 0;
                this.m_hRectBottom.top = iHeight - this.m_iHintLinesSize;
                this.m_hRectBottom.right = iWidth;
                this.m_hRectBottom.bottom = iHeight;
            }
            super.setImageBitmap(hBitmap);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas hCanvas) {
            super.onDraw(hCanvas);
            if (PuzzlePieceContainer.this.m_bDrawHint) {
                if (!this.m_bCorrectNeighborLeft) {
                    hCanvas.drawRect(this.m_hRectLeft, this.m_hPaintLine);
                }
                if (!this.m_bCorrectNeighborTop) {
                    hCanvas.drawRect(this.m_hRectTop, this.m_hPaintLine);
                }
                if (!this.m_bCorrectNeighborRight) {
                    hCanvas.drawRect(this.m_hRectRight, this.m_hPaintLine);
                }
                if (!this.m_bCorrectNeighborBottom) {
                    hCanvas.drawRect(this.m_hRectBottom, this.m_hPaintLine);
                }
            }
        }
    }
}
