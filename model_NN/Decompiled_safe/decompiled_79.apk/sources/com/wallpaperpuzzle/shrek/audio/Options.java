package com.wallpaperpuzzle.shrek.audio;

import android.content.Context;
import android.content.SharedPreferences;
import com.wallpaperpuzzle.shrek.WallpaperPuzzle;

public class Options {
    private static final String DEFENDCASTLE_OPTIONS = WallpaperPuzzle.class.getName();
    private static final String OPT_MUSIC = "music";
    private static final boolean OPT_MUSIC_DEF = true;

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(DEFENDCASTLE_OPTIONS, 0);
    }

    public static boolean getMusic(Context context) {
        return getPreferences(context).getBoolean(OPT_MUSIC, OPT_MUSIC_DEF);
    }

    public static boolean putMusic(Context context, boolean value) {
        return getPreferences(context).edit().putBoolean(OPT_MUSIC, value).commit();
    }
}
