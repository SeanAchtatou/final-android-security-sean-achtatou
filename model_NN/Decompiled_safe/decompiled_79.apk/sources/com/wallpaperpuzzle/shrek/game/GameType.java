package com.wallpaperpuzzle.shrek.game;

public enum GameType {
    x3(3),
    x4(4),
    x5(5),
    x6(6);
    
    private int quantity;

    private GameType(int quantity2) {
        this.quantity = quantity2;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity2) {
        this.quantity = quantity2;
    }

    public int getOrdinal() {
        return ordinal();
    }
}
