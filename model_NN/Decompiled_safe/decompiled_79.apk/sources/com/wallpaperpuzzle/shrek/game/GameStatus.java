package com.wallpaperpuzzle.shrek.game;

public enum GameStatus {
    beforeStart,
    started,
    completed,
    wallpaper
}
