package com.wallpaperpuzzle.shrek.game;

public interface PanelListener {
    void onComplete();

    void onNext();
}
