package com.zz.Ringtone.r007;

public final class R {

    public static final class array {
        public static final int local_music_option = 2131034115;
        public static final int local_ring_option = 2131034120;
        public static final int order_option = 2131034116;
        public static final int play_mode = 2131034114;
        public static final int ring_types = 2131034118;
        public static final int ring_types_a = 2131034117;
        public static final int select_share_methods = 2131034112;
        public static final int select_unset_ringtone = 2131034113;
        public static final int set_ring_option = 2131034119;
    }

    public static final class attr {
        public static final int adSize = 2130771968;
        public static final int adUnitId = 2130771969;
    }

    public static final class color {
        public static final int alpha00 = 2131165187;
        public static final int black = 2131165186;
        public static final int brown = 2131165184;
        public static final int contents_current = 2131165188;
        public static final int contents_dark_yellow = 2131165190;
        public static final int contents_light_yellow = 2131165189;
        public static final int green = 2131165191;
        public static final int light_brown = 2131165185;
        public static final int red_apple = 2131165192;
        public static final int transparent_background = 2131165193;
    }

    public static final class drawable {
        public static final int bk = 2130837504;
        public static final int delete = 2130837505;
        public static final int delete_s = 2130837506;
        public static final int ic_menu_add = 2130837507;
        public static final int ic_menu_home = 2130837508;
        public static final int ic_menu_mute = 2130837509;
        public static final int ic_menu_refresh = 2130837510;
        public static final int ic_menu_share = 2130837511;
        public static final int ic_menu_shortcut = 2130837512;
        public static final int icon = 2130837513;
        public static final int moreapk = 2130837514;
        public static final int mp3 = 2130837515;
        public static final int mylib = 2130837516;
        public static final int myring = 2130837517;
        public static final int next = 2130837518;
        public static final int next1 = 2130837519;
        public static final int next_press1 = 2130837520;
        public static final int next_s = 2130837521;
        public static final int no = 2130837522;
        public static final int pause = 2130837523;
        public static final int pause_s = 2130837524;
        public static final int play = 2130837525;
        public static final int play_s = 2130837526;
        public static final int prev = 2130837527;
        public static final int prev1 = 2130837528;
        public static final int prev_press1 = 2130837529;
        public static final int prev_s = 2130837530;
        public static final int previewnext = 2130837531;
        public static final int previewnext_s = 2130837532;
        public static final int previewprev = 2130837533;
        public static final int previewprev_s = 2130837534;
        public static final int search = 2130837535;
        public static final int set = 2130837536;
        public static final int set_s = 2130837537;
        public static final int star = 2130837538;
        public static final int stop = 2130837539;
        public static final int stop_s = 2130837540;
        public static final int type_music = 2130837541;
        public static final int type_notification = 2130837542;
        public static final int type_ringtone = 2130837543;
        public static final int xml_btn_delete = 2130837544;
        public static final int xml_btn_next = 2130837545;
        public static final int xml_btn_next1 = 2130837546;
        public static final int xml_btn_pause = 2130837547;
        public static final int xml_btn_play = 2130837548;
        public static final int xml_btn_prev = 2130837549;
        public static final int xml_btn_prev1 = 2130837550;
        public static final int xml_btn_previewnext = 2130837551;
        public static final int xml_btn_previewprev = 2130837552;
        public static final int xml_btn_set = 2130837553;
        public static final int xml_btn_stop = 2130837554;
    }

    public static final class id {
        public static final int BANNER = 2131099648;
        public static final int IAB_BANNER = 2131099650;
        public static final int IAB_LEADERBOARD = 2131099651;
        public static final int IAB_MRECT = 2131099649;
        public static final int ad = 2131099662;
        public static final int btn_previewnext = 2131099660;
        public static final int btn_previewprev = 2131099659;
        public static final int btnpause = 2131099671;
        public static final int btnplay = 2131099672;
        public static final int btnset = 2131099674;
        public static final int btnstop = 2131099673;
        public static final int home = 2131099686;
        public static final int mediaselect = 2131099676;
        public static final int mp3_ico = 2131099664;
        public static final int mylibraryweb = 2131099657;
        public static final int next = 2131099680;
        public static final int page_info = 2131099679;
        public static final int pbview = 2131099670;
        public static final int prev = 2131099678;
        public static final int progress_layout = 2131099677;
        public static final int refresh = 2131099685;
        public static final int rl1 = 2131099658;
        public static final int rl2 = 2131099661;
        public static final int rl3 = 2131099666;
        public static final int rlplayer = 2131099663;
        public static final int row_album = 2131099684;
        public static final int row_artist = 2131099683;
        public static final int row_display_name = 2131099655;
        public static final int row_icon = 2131099681;
        public static final int row_ringtone = 2131099653;
        public static final int row_starred = 2131099654;
        public static final int row_title = 2131099682;
        public static final int search_filter = 2131099652;
        public static final int tv_song_name = 2131099665;
        public static final int tvend = 2131099669;
        public static final int tvstart = 2131099668;
        public static final int tvtime = 2131099667;
        public static final int wv = 2131099675;
        public static final int wv_page = 2131099656;
    }

    public static final class layout {
        public static final int choose_contact = 2130903040;
        public static final int contact_row = 2130903041;
        public static final int file_web_view = 2130903042;
        public static final int main = 2130903043;
        public static final int mylibrary = 2130903044;
        public static final int ring_preview = 2130903045;
        public static final int ring_select = 2130903046;
        public static final int ring_select_row = 2130903047;
    }

    public static final class menu {
        public static final int apk_menu = 2131361792;
    }

    public static final class string {
        public static final int alert_ok_button = 2131230746;
        public static final int alert_title_failure = 2131230745;
        public static final int alertdialog_cancel = 2131230763;
        public static final int alertdialog_ok = 2131230762;
        public static final int alertdialog_select = 2131230742;
        public static final int alertdialog_share = 2131230743;
        public static final int app_name = 2131230721;
        public static final int btn_cancel = 2131230734;
        public static final int btn_ok = 2131230733;
        public static final int button_start_record_activity = 2131230744;
        public static final int choose_contact_title = 2131230752;
        public static final int confirm_delete_ringdroid = 2131230756;
        public static final int create_shortcut = 2131230736;
        public static final int delete_cancel_button = 2131230758;
        public static final int delete_failed = 2131230754;
        public static final int delete_ok_button = 2131230757;
        public static final int delete_ringtone = 2131230755;
        public static final int edit_intent = 2131230751;
        public static final int hello = 2131230720;
        public static final int input_title = 2131230776;
        public static final int mStreaming_message = 2131230773;
        public static final int mStreaming_title = 2131230772;
        public static final int menu__search = 2131230775;
        public static final int menu_share = 2131230737;
        public static final int menu_unset_ringtone = 2131230738;
        public static final int no_record_tip = 2131230750;
        public static final int no_sdcard = 2131230748;
        public static final int online_index_mb = 2131230725;
        public static final int page_info_head = 2131230780;
        public static final int preview_url = 2131230728;
        public static final int rb_menu_assign = 2131230782;
        public static final int rb_menu_delete = 2131230783;
        public static final int rb_menu_preview = 2131230784;
        public static final int rb_menu_set = 2131230781;
        public static final int ring_picker_title = 2131230761;
        public static final int sdcard_readonly = 2131230747;
        public static final int sdcard_shared = 2131230749;
        public static final int search_hint = 2131230777;
        public static final int search_url = 2131230726;
        public static final int stop = 2131230771;
        public static final int success_contact_ringtone = 2131230753;
        public static final int tip_delete_success = 2131230760;
        public static final int tip_delete_txt = 2131230759;
        public static final int tip_download_end = 2131230778;
        public static final int tip_set_alarm_failed = 2131230769;
        public static final int tip_set_alarm_success = 2131230768;
        public static final int tip_set_notification_failed = 2131230767;
        public static final int tip_set_notification_success = 2131230766;
        public static final int tip_set_ring_failed = 2131230765;
        public static final int tip_set_ring_success = 2131230764;
        public static final int tip_set_shortcut = 2131230770;
        public static final int title_moreapp = 2131230724;
        public static final int title_mylib = 2131230723;
        public static final int title_myring = 2131230722;
        public static final int todo = 2131230786;
        public static final int txt_choice = 2131230741;
        public static final int txt_domain = 2131230727;
        public static final int txt_home = 2131230779;
        public static final int txt_loading = 2131230735;
        public static final int txt_mailcontent = 2131230739;
        public static final int txt_mailsubject = 2131230740;
        public static final int txt_quit_body = 2131230732;
        public static final int txt_quit_title = 2131230731;
        public static final int txt_refresh = 2131230729;
        public static final int txt_tip_clickitem = 2131230774;
        public static final int txt_unknownfilesize = 2131230785;
        public static final int webapkurl = 2131230730;
    }

    public static final class style {
        public static final int AudioFileInfoOverlayText = 2131296256;
        public static final int ChooseContactBackground = 2131296258;
        public static final int HorizontalDividerBottom = 2131296262;
        public static final int HorizontalDividerTop = 2131296261;
        public static final int ToolbarBackground = 2131296257;
        public static final int Transparent = 2131296263;
        public static final int VerticalDividerLeft = 2131296259;
        public static final int VerticalDividerRight = 2131296260;
    }

    public static final class styleable {
        public static final int[] com_google_ads_AdView = {R.attr.adSize, R.attr.adUnitId};
        public static final int com_google_ads_AdView_adSize = 0;
        public static final int com_google_ads_AdView_adUnitId = 1;
    }

    public static final class xml {
        public static final int searchable = 2130968576;
    }
}
