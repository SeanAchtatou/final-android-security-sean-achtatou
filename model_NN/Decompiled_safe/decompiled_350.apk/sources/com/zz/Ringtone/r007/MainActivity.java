package com.zz.Ringtone.r007;

import android.app.AlertDialog;
import android.app.TabActivity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Browser;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.Toast;
import com.zzbook.util.FileUtil;
import com.zzbook.util.GenUtil;
import java.io.File;

public class MainActivity extends TabActivity {
    public static final int MENU_CREATESHORTCUT = 0;
    public static final int MENU_SHARE = 1;
    public static final int MENU_UNSETRINGTONE = 2;
    public static TabHost tabHost;
    /* access modifiers changed from: private */
    public int iChoice = 0;
    private int imNowTab = 0;
    private int imPreTab = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitPackage();
        initTab();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.widget.FrameLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initTab() {
        tabHost = getTabHost();
        TabWidget tabWidget = tabHost.getTabWidget();
        Resources resources = getResources();
        LayoutInflater.from(this).inflate((int) R.layout.main, (ViewGroup) tabHost.getTabContentView(), true);
        tabHost.addTab(tabHost.newTabSpec("MYRING").setIndicator(resources.getString(R.string.title_myring), resources.getDrawable(R.drawable.myring)).setContent(new Intent(this, RingdroidActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("MYLIB").setIndicator(resources.getString(R.string.title_mylib), resources.getDrawable(R.drawable.mylib)).setContent(new Intent(this, MyLibrary.class)));
        tabHost.addTab(tabHost.newTabSpec("MOREAPP").setIndicator(resources.getString(R.string.title_moreapp), resources.getDrawable(R.drawable.moreapk)).setContent(new Intent(this, MoreApk.class)));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu paramMenu) {
        super.onCreateOptionsMenu(paramMenu);
        paramMenu.add(0, 0, 0, (int) R.string.create_shortcut).setIcon((int) R.drawable.ic_menu_shortcut);
        paramMenu.add(0, 1, 0, (int) R.string.menu_share).setIcon((int) R.drawable.ic_menu_share);
        paramMenu.add(0, 2, 0, (int) R.string.menu_unset_ringtone).setIcon((int) R.drawable.ic_menu_mute);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
        super.onOptionsItemSelected(paramMenuItem);
        switch (paramMenuItem.getItemId()) {
            case 0:
                addShortCut();
                return true;
            case 1:
                AlertDialog.Builder localBuilder = new AlertDialog.Builder(this).setTitle((int) R.string.alertdialog_share);
                localBuilder.setItems((int) R.array.select_share_methods, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        switch (paramInt) {
                            case 0:
                                Intent intent1 = new Intent("android.intent.action.VIEW");
                                intent1.putExtra("sms_body", MainActivity.this.getResources().getString(R.string.txt_mailsubject) + " " + MainActivity.this.getResources().getString(R.string.txt_mailcontent));
                                intent1.setType("vnd.android-dir/mms-sms");
                                MainActivity.this.startActivity(intent1);
                                return;
                            case 1:
                                Intent intent2 = new Intent("android.intent.action.SEND");
                                intent2.setType("plain/text");
                                intent2.putExtra("android.intent.extra.SUBJECT", MainActivity.this.getResources().getString(R.string.txt_mailsubject));
                                intent2.putExtra("android.intent.extra.TEXT", MainActivity.this.getResources().getString(R.string.txt_mailcontent));
                                MainActivity.this.startActivity(Intent.createChooser(intent2, MainActivity.this.getResources().getString(R.string.txt_choice)));
                                return;
                            case MainActivity.MENU_UNSETRINGTONE /*2*/:
                                Browser.sendString(MainActivity.this, MainActivity.this.getResources().getString(R.string.txt_mailsubject) + MainActivity.this.getResources().getString(R.string.txt_mailcontent));
                                return;
                            default:
                                return;
                        }
                    }
                });
                localBuilder.create().show();
                return true;
            case MENU_UNSETRINGTONE /*2*/:
                AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(this).setIcon((int) R.drawable.type_notification).setTitle(getResources().getString(R.string.alertdialog_select));
                dialogbuilder.setSingleChoiceItems((int) R.array.select_unset_ringtone, 0, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        int unused = MainActivity.this.iChoice = paramInt;
                    }
                });
                dialogbuilder.setPositiveButton((int) R.string.btn_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        AudioManager audioManager = (AudioManager) MainActivity.this.getSystemService("audio");
                        switch (MainActivity.this.iChoice) {
                            case 0:
                                audioManager.setRingerMode(1);
                                return;
                            case 1:
                                audioManager.setRingerMode(0);
                                return;
                            default:
                                return;
                        }
                    }
                });
                dialogbuilder.setNegativeButton((int) R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
                dialogbuilder.create().show();
                return true;
            default:
                return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void addShortCut() {
        Intent shortcut = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
        shortcut.putExtra("android.intent.extra.shortcut.NAME", getString(R.string.app_name));
        shortcut.putExtra("duplicate", false);
        shortcut.putExtra("android.intent.extra.shortcut.INTENT", new Intent("android.intent.action.MAIN").setComponent(new ComponentName(getPackageName(), "." + getLocalClassName())));
        shortcut.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(this, R.drawable.icon));
        sendBroadcast(shortcut);
        Toast.makeText(this, (int) R.string.tip_set_shortcut, 1000).show();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045 A[SYNTHETIC, Splitter:B:20:0x0045] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004a A[SYNTHETIC, Splitter:B:23:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x004f A[SYNTHETIC, Splitter:B:26:0x004f] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0054 A[SYNTHETIC, Splitter:B:29:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0078 A[SYNTHETIC, Splitter:B:48:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x007d A[SYNTHETIC, Splitter:B:51:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0082 A[SYNTHETIC, Splitter:B:54:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0087 A[SYNTHETIC, Splitter:B:57:0x0087] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void releaseAssetRes(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            java.io.File r6 = new java.io.File
            r6.<init>(r14)
            boolean r11 = r6.exists()
            if (r11 == 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            r8 = 0
            r9 = 0
            r0 = 0
            r2 = 0
            android.content.res.AssetManager r11 = r12.getAssets()     // Catch:{ IOException -> 0x00ce }
            java.io.InputStream r8 = r11.open(r13)     // Catch:{ IOException -> 0x00ce }
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ IOException -> 0x00ce }
            r1.<init>(r8)     // Catch:{ IOException -> 0x00ce }
            java.io.FileOutputStream r10 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x00d2, all -> 0x00c2 }
            r10.<init>(r14)     // Catch:{ IOException -> 0x00d2, all -> 0x00c2 }
            java.io.BufferedOutputStream r3 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x00d7, all -> 0x00c5 }
            r3.<init>(r10)     // Catch:{ IOException -> 0x00d7, all -> 0x00c5 }
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r11]     // Catch:{ IOException -> 0x003b, all -> 0x00c9 }
            r7 = 0
        L_0x002c:
            int r7 = r1.read(r4)     // Catch:{ IOException -> 0x003b, all -> 0x00c9 }
            r11 = -1
            if (r7 == r11) goto L_0x005d
            r11 = 0
            r3.write(r4, r11, r7)     // Catch:{ IOException -> 0x003b, all -> 0x00c9 }
            r3.flush()     // Catch:{ IOException -> 0x003b, all -> 0x00c9 }
            goto L_0x002c
        L_0x003b:
            r11 = move-exception
            r5 = r11
            r2 = r3
            r0 = r1
            r9 = r10
        L_0x0040:
            r5.printStackTrace()     // Catch:{ all -> 0x0075 }
            if (r0 == 0) goto L_0x0048
            r0.close()     // Catch:{ IOException -> 0x009f }
        L_0x0048:
            if (r8 == 0) goto L_0x004d
            r8.close()     // Catch:{ IOException -> 0x00a4 }
        L_0x004d:
            if (r2 == 0) goto L_0x0052
            r2.close()     // Catch:{ IOException -> 0x00a9 }
        L_0x0052:
            if (r9 == 0) goto L_0x000b
            r9.close()     // Catch:{ IOException -> 0x0058 }
            goto L_0x000b
        L_0x0058:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x000b
        L_0x005d:
            if (r1 == 0) goto L_0x0062
            r1.close()     // Catch:{ IOException -> 0x00ae }
        L_0x0062:
            if (r8 == 0) goto L_0x0067
            r8.close()     // Catch:{ IOException -> 0x00b3 }
        L_0x0067:
            if (r3 == 0) goto L_0x006c
            r3.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x006c:
            if (r10 == 0) goto L_0x0071
            r10.close()     // Catch:{ IOException -> 0x00bd }
        L_0x0071:
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x000b
        L_0x0075:
            r11 = move-exception
        L_0x0076:
            if (r0 == 0) goto L_0x007b
            r0.close()     // Catch:{ IOException -> 0x008b }
        L_0x007b:
            if (r8 == 0) goto L_0x0080
            r8.close()     // Catch:{ IOException -> 0x0090 }
        L_0x0080:
            if (r2 == 0) goto L_0x0085
            r2.close()     // Catch:{ IOException -> 0x0095 }
        L_0x0085:
            if (r9 == 0) goto L_0x008a
            r9.close()     // Catch:{ IOException -> 0x009a }
        L_0x008a:
            throw r11
        L_0x008b:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x007b
        L_0x0090:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0080
        L_0x0095:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0085
        L_0x009a:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x008a
        L_0x009f:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0048
        L_0x00a4:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x004d
        L_0x00a9:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0052
        L_0x00ae:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0062
        L_0x00b3:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0067
        L_0x00b8:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x006c
        L_0x00bd:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x0071
        L_0x00c2:
            r11 = move-exception
            r0 = r1
            goto L_0x0076
        L_0x00c5:
            r11 = move-exception
            r0 = r1
            r9 = r10
            goto L_0x0076
        L_0x00c9:
            r11 = move-exception
            r2 = r3
            r0 = r1
            r9 = r10
            goto L_0x0076
        L_0x00ce:
            r11 = move-exception
            r5 = r11
            goto L_0x0040
        L_0x00d2:
            r11 = move-exception
            r5 = r11
            r0 = r1
            goto L_0x0040
        L_0x00d7:
            r11 = move-exception
            r5 = r11
            r0 = r1
            r9 = r10
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.zz.Ringtone.r007.MainActivity.releaseAssetRes(java.lang.String, java.lang.String):void");
    }

    private void InitPackage() {
        String strDir;
        String strSoftFolderDir;
        if (Environment.getExternalStorageState().equals("mounted")) {
            strDir = "/sdcard/popringtone/myRing/";
            strSoftFolderDir = "/sdcard/popringtone/softfolder/";
        } else {
            strDir = "/data/popringtone/myRing/";
            strSoftFolderDir = "/data/popringtone/softfolder/";
        }
        FileUtil.folderExistOrCreate(strSoftFolderDir);
        String strSoftMark = strSoftFolderDir + getResources().getString(R.string.app_name);
        if (!FileUtil.fileExist(strSoftMark)) {
            try {
                File file = new File(strDir);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String[] fsubFiles = getResources().getAssets().list("");
                for (int iIndex = 0; iIndex < fsubFiles.length; iIndex++) {
                    releaseAssetRes(fsubFiles[iIndex], strDir + fsubFiles[iIndex]);
                }
                FileUtil.createFile(strSoftMark, strSoftMark);
            } catch (Exception e) {
                e.printStackTrace();
            }
            GenUtil.systemPrint("sendBroadcast called");
            sendBroadcast(new Intent("android.intent.action.MEDIA_MOUNTED", Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }
}
