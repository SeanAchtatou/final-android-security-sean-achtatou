package com.zz.Ringtone.r007;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import com.zzbook.util.GenUtil;
import com.zzbook.util.OtherUtil;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class RingPreviewerActivity extends Activity {
    private ImageButton mBtnDelete;
    private ImageButton mBtnNext;
    /* access modifiers changed from: private */
    public ImageButton mBtnPlay;
    private ImageButton mBtnPrev;
    private ImageButton mBtnSet;
    /* access modifiers changed from: private */
    public ImageButton mBtnStop;
    /* access modifiers changed from: private */
    public MyHandler mMyHandler;
    /* access modifiers changed from: private */
    public ProgressBar mProgressBar;
    /* access modifiers changed from: private */
    public Thread mProgressThread = null;
    /* access modifiers changed from: private */
    public TextView mTvEnd;
    private TextView mTvName;
    /* access modifiers changed from: private */
    public TextView mTvTime;
    private WebView mWebView;
    /* access modifiers changed from: private */
    public boolean mbPause = false;
    /* access modifiers changed from: private */
    public int miCurIndex;
    /* access modifiers changed from: private */
    public int miSetType;
    /* access modifiers changed from: private */
    public ArrayList<HashMap<String, Object>> mlist = null;
    /* access modifiers changed from: private */
    public String mstrCurTotalTime;
    /* access modifiers changed from: private */
    public MediaPlayer previewPlayer;

    static /* synthetic */ int access$008(RingPreviewerActivity x0) {
        int i = x0.miCurIndex;
        x0.miCurIndex = i + 1;
        return i;
    }

    static /* synthetic */ int access$010(RingPreviewerActivity x0) {
        int i = x0.miCurIndex;
        x0.miCurIndex = i - 1;
        return i;
    }

    private void initView() {
        this.mBtnPrev = (ImageButton) findViewById(R.id.btn_previewprev);
        this.mBtnNext = (ImageButton) findViewById(R.id.btn_previewnext);
        this.mTvName = (TextView) findViewById(R.id.tv_song_name);
        this.mProgressBar = (ProgressBar) findViewById(R.id.pbview);
        this.mBtnPlay = (ImageButton) findViewById(R.id.btnplay);
        this.mBtnStop = (ImageButton) findViewById(R.id.btnstop);
        this.mBtnDelete = (ImageButton) findViewById(R.id.btnpause);
        this.mBtnSet = (ImageButton) findViewById(R.id.btnset);
        this.mBtnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RingPreviewerActivity.access$010(RingPreviewerActivity.this);
                if (RingPreviewerActivity.this.mbPause) {
                    boolean unused = RingPreviewerActivity.this.mbPause = false;
                }
                String unused2 = RingPreviewerActivity.this.mstrCurTotalTime = RingPreviewerActivity.this.getCurPlayerDuration();
                RingPreviewerActivity.this.mMyHandler.sendEmptyMessage(2);
                RingPreviewerActivity.this.updateView();
            }
        });
        this.mBtnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RingPreviewerActivity.access$008(RingPreviewerActivity.this);
                if (RingPreviewerActivity.this.mbPause) {
                    boolean unused = RingPreviewerActivity.this.mbPause = false;
                }
                String unused2 = RingPreviewerActivity.this.mstrCurTotalTime = RingPreviewerActivity.this.getCurPlayerDuration();
                RingPreviewerActivity.this.mMyHandler.sendEmptyMessage(2);
                RingPreviewerActivity.this.updateView();
            }
        });
        this.mBtnSet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RingPreviewerActivity.this.stopPlay();
                RingPreviewerActivity.this.onCreateDialog(1).show();
            }
        });
        this.mBtnDelete.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RingPreviewerActivity.this.stopPlay();
                RingPreviewerActivity.this.confirmDelete();
            }
        });
        this.mBtnPlay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RingPreviewerActivity.this.play();
            }
        });
        this.mBtnStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                RingPreviewerActivity.this.mBtnPlay.setVisibility(0);
                RingPreviewerActivity.this.mBtnStop.setVisibility(4);
                boolean unused = RingPreviewerActivity.this.mbPause = false;
                RingPreviewerActivity.this.stopPlay();
            }
        });
        this.previewPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer paramMediaPlayer) {
                try {
                    RingPreviewerActivity.this.mProgressBar.setMax(100);
                    RingPreviewerActivity.this.mProgressBar.setProgress(100);
                    RingPreviewerActivity.this.previewPlayer.reset();
                    RingPreviewerActivity.this.mBtnPlay.setVisibility(0);
                    RingPreviewerActivity.this.mBtnStop.setVisibility(4);
                    RingPreviewerActivity.this.mMyHandler.sendEmptyMessage(2);
                } catch (Exception e) {
                }
            }
        });
        this.previewPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
            public void onBufferingUpdate(MediaPlayer arg0, int arg1) {
                try {
                    GenUtil.systemPrintln("onBufferingUpdate: " + String.valueOf(arg1));
                    if (!RingPreviewerActivity.this.mbPause) {
                        RingPreviewerActivity.this.mProgressBar.setProgress(arg1);
                    }
                } catch (Exception e) {
                }
            }
        });
        this.mTvTime = (TextView) findViewById(R.id.tvtime);
        this.mTvTime.setText("0:00");
        this.mTvEnd = (TextView) findViewById(R.id.tvend);
        this.mstrCurTotalTime = getCurPlayerDuration();
        this.mTvEnd.setText(this.mstrCurTotalTime);
        this.mWebView = (WebView) findViewById(R.id.wv);
        if (isWifiConnected()) {
            this.mWebView.loadUrl(getString(R.string.preview_url));
        } else {
            this.mWebView.setVisibility(8);
        }
        this.mProgressThread = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    int iSeconds = RingPreviewerActivity.this.previewPlayer.getCurrentPosition();
                    RingPreviewerActivity.this.mProgressBar.setProgress(iSeconds);
                    Message message = new Message();
                    message.what = 1;
                    if (RingPreviewerActivity.this.mstrCurTotalTime.length() > 0) {
                        message.obj = OtherUtil.FormatDuration(iSeconds / 1000);
                    } else {
                        message.obj = OtherUtil.FormatDuration(iSeconds / 1000);
                    }
                    RingPreviewerActivity.this.mMyHandler.sendMessage(message);
                    try {
                        Thread.sleep(1000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        ((AdView) findViewById(R.id.ad)).loadAd(new AdRequest());
    }

    /* access modifiers changed from: private */
    public void stopPlay() {
        if (this.previewPlayer.isPlaying()) {
            this.previewPlayer.stop();
            this.previewPlayer.reset();
            this.mBtnPlay.setVisibility(0);
            this.mBtnStop.setVisibility(4);
        }
    }

    /* access modifiers changed from: private */
    public void updateView() {
        if (this.mlist == null || this.mlist.size() <= 0) {
            this.mBtnPrev.setVisibility(8);
            this.mBtnNext.setVisibility(8);
            return;
        }
        if (this.miCurIndex <= 0) {
            this.mBtnPrev.setVisibility(8);
        } else {
            this.mBtnPrev.setVisibility(0);
        }
        if (this.miCurIndex < this.mlist.size() - 1) {
            this.mBtnNext.setVisibility(0);
        } else {
            this.mBtnNext.setVisibility(8);
        }
        this.mTvName.setText((String) this.mlist.get(this.miCurIndex).get("_title"));
        play();
    }

    /* access modifiers changed from: private */
    public String getCurPlayerDuration() {
        String strRing = (String) this.mlist.get(this.miCurIndex).get("_dir");
        try {
            stopPlay();
            this.previewPlayer.setDataSource(strRing);
            this.previewPlayer.prepare();
            int iDuration = this.previewPlayer.getDuration();
            this.previewPlayer.reset();
            return OtherUtil.FormatDuration(iDuration / 1000);
        } catch (IllegalStateException e) {
            this.previewPlayer.reset();
        } catch (IOException e2) {
            IOException e3 = e2;
            GenUtil.systemPrintln(e3.getMessage());
            e3.printStackTrace();
        }
        return "";
    }

    /* access modifiers changed from: private */
    public void play() {
        this.mBtnPlay.setVisibility(4);
        this.mBtnStop.setVisibility(0);
        new Thread(new Runnable() {
            public void run() {
                try {
                    if (RingPreviewerActivity.this.mbPause) {
                        RingPreviewerActivity.this.previewPlayer.start();
                        boolean unused = RingPreviewerActivity.this.mbPause = false;
                        return;
                    }
                    RingPreviewerActivity.this.previewPlayer.setDataSource((String) ((HashMap) RingPreviewerActivity.this.mlist.get(RingPreviewerActivity.this.miCurIndex)).get("_dir"));
                    RingPreviewerActivity.this.previewPlayer.prepare();
                    RingPreviewerActivity.this.previewPlayer.start();
                    int iduration = RingPreviewerActivity.this.previewPlayer.getDuration();
                    if (iduration < 0) {
                        iduration = 0;
                    }
                    RingPreviewerActivity.this.mProgressBar.setMax(iduration);
                    String unused2 = RingPreviewerActivity.this.mstrCurTotalTime = OtherUtil.FormatDuration(iduration / 1000);
                    if (RingPreviewerActivity.this.mProgressThread != null) {
                        RingPreviewerActivity.this.mProgressThread.start();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void onCreate(Bundle paramBundle) {
        super.onCreate(paramBundle);
        setContentView((int) R.layout.ring_preview);
        Intent localintent = getIntent();
        this.miCurIndex = localintent.getIntExtra("cur_index", 0);
        this.mlist = (ArrayList) localintent.getSerializableExtra("cur_list");
        GenUtil.systemPrintln("RingBrowserActivity: mlist.size=" + this.mlist.size());
        this.previewPlayer = new MediaPlayer();
        this.mMyHandler = new MyHandler();
        this.mstrCurTotalTime = "";
        initView();
        updateView();
    }

    /* access modifiers changed from: private */
    public void confirmDelete() {
        if (((String) this.mlist.get(this.miCurIndex).get("_dir")).length() <= 0) {
            Toast.makeText(this, (int) R.string.delete_failed, 0).show();
        } else {
            new AlertDialog.Builder(this).setTitle((int) R.string.delete_ringtone).setMessage((int) R.string.confirm_delete_ringdroid).setPositiveButton((int) R.string.delete_ok_button, new DialogInterface.OnClickListener() {
                /* JADX WARNING: Removed duplicated region for block: B:13:0x006f A[Catch:{ Exception -> 0x00c1 }] */
                /* JADX WARNING: Removed duplicated region for block: B:16:0x0087 A[Catch:{ Exception -> 0x00c1 }] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void onClick(android.content.DialogInterface r8, int r9) {
                    /*
                        r7 = this;
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        java.util.ArrayList r4 = r4.mlist     // Catch:{ Exception -> 0x00c1 }
                        com.zz.Ringtone.r007.RingPreviewerActivity r5 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        int r5 = r5.miCurIndex     // Catch:{ Exception -> 0x00c1 }
                        java.lang.Object r2 = r4.get(r5)     // Catch:{ Exception -> 0x00c1 }
                        java.util.HashMap r2 = (java.util.HashMap) r2     // Catch:{ Exception -> 0x00c1 }
                        java.lang.String r4 = "_dir"
                        java.lang.Object r3 = r2.get(r4)     // Catch:{ Exception -> 0x00c1 }
                        java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x00c1 }
                        java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c1 }
                        r4.<init>()     // Catch:{ Exception -> 0x00c1 }
                        java.lang.String r5 = "confirmDelete: "
                        java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00c1 }
                        java.lang.StringBuilder r4 = r4.append(r3)     // Catch:{ Exception -> 0x00c1 }
                        java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00c1 }
                        com.zzbook.util.GenUtil.systemPrintln(r4)     // Catch:{ Exception -> 0x00c1 }
                        int r4 = r3.length()     // Catch:{ Exception -> 0x00c1 }
                        if (r4 <= 0) goto L_0x009d
                        java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x00c1 }
                        r1.<init>(r3)     // Catch:{ Exception -> 0x00c1 }
                        boolean r4 = r1.isFile()     // Catch:{ Exception -> 0x00ad }
                        if (r4 == 0) goto L_0x009e
                        boolean r4 = r1.exists()     // Catch:{ Exception -> 0x00ad }
                        if (r4 == 0) goto L_0x009e
                        r1.delete()     // Catch:{ Exception -> 0x00ad }
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00ad }
                        r5 = 2131230760(0x7f080028, float:1.8077582E38)
                        r6 = 1000(0x3e8, float:1.401E-42)
                        android.widget.Toast r4 = android.widget.Toast.makeText(r4, r5, r6)     // Catch:{ Exception -> 0x00ad }
                        r4.show()     // Catch:{ Exception -> 0x00ad }
                    L_0x0058:
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        java.util.ArrayList r4 = r4.mlist     // Catch:{ Exception -> 0x00c1 }
                        com.zz.Ringtone.r007.RingPreviewerActivity r5 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        int r5 = r5.miCurIndex     // Catch:{ Exception -> 0x00c1 }
                        r4.remove(r5)     // Catch:{ Exception -> 0x00c1 }
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        int r4 = r4.miCurIndex     // Catch:{ Exception -> 0x00c1 }
                        if (r4 >= 0) goto L_0x0075
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        r5 = 0
                        int unused = r4.miCurIndex = r5     // Catch:{ Exception -> 0x00c1 }
                    L_0x0075:
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        int r4 = r4.miCurIndex     // Catch:{ Exception -> 0x00c1 }
                        com.zz.Ringtone.r007.RingPreviewerActivity r5 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        java.util.ArrayList r5 = r5.mlist     // Catch:{ Exception -> 0x00c1 }
                        int r5 = r5.size()     // Catch:{ Exception -> 0x00c1 }
                        if (r4 < r5) goto L_0x0098
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        com.zz.Ringtone.r007.RingPreviewerActivity r5 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        java.util.ArrayList r5 = r5.mlist     // Catch:{ Exception -> 0x00c1 }
                        int r5 = r5.size()     // Catch:{ Exception -> 0x00c1 }
                        r6 = 1
                        int r5 = r5 - r6
                        int unused = r4.miCurIndex = r5     // Catch:{ Exception -> 0x00c1 }
                    L_0x0098:
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        r4.updateView()     // Catch:{ Exception -> 0x00c1 }
                    L_0x009d:
                        return
                    L_0x009e:
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00ad }
                        r5 = 2131230759(0x7f080027, float:1.807758E38)
                        r6 = 1000(0x3e8, float:1.401E-42)
                        android.widget.Toast r4 = android.widget.Toast.makeText(r4, r5, r6)     // Catch:{ Exception -> 0x00ad }
                        r4.show()     // Catch:{ Exception -> 0x00ad }
                        goto L_0x0058
                    L_0x00ad:
                        r4 = move-exception
                        r0 = r4
                        r0.printStackTrace()     // Catch:{ Exception -> 0x00c1 }
                        com.zz.Ringtone.r007.RingPreviewerActivity r4 = com.zz.Ringtone.r007.RingPreviewerActivity.this     // Catch:{ Exception -> 0x00c1 }
                        r5 = 2131230759(0x7f080027, float:1.807758E38)
                        r6 = 1000(0x3e8, float:1.401E-42)
                        android.widget.Toast r4 = android.widget.Toast.makeText(r4, r5, r6)     // Catch:{ Exception -> 0x00c1 }
                        r4.show()     // Catch:{ Exception -> 0x00c1 }
                        goto L_0x0058
                    L_0x00c1:
                        r4 = move-exception
                        r0 = r4
                        r0.printStackTrace()
                        goto L_0x009d
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.zz.Ringtone.r007.RingPreviewerActivity.AnonymousClass12.onClick(android.content.DialogInterface, int):void");
                }
            }).setNegativeButton((int) R.string.delete_cancel_button, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                }
            }).setCancelable(true).show();
        }
    }

    /* access modifiers changed from: protected */
    public AlertDialog onCreateDialog(int iParam) {
        if (iParam != 1) {
            return null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setIcon(17301543).setTitle((int) R.string.ring_picker_title);
        this.miSetType = 0;
        builder.setSingleChoiceItems((int) R.array.ring_types_a, this.miSetType, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                int unused = RingPreviewerActivity.this.miSetType = paramInt;
            }
        }).setPositiveButton((int) R.string.alertdialog_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                switch (RingPreviewerActivity.this.miSetType) {
                    case 0:
                        File file = new File((String) ((HashMap) RingPreviewerActivity.this.mlist.get(RingPreviewerActivity.this.miCurIndex)).get("_dir"));
                        if (!file.isFile() || !file.exists()) {
                            Toast.makeText(RingPreviewerActivity.this, (int) R.string.tip_set_ring_failed, 1000).show();
                            return;
                        }
                        boolean unused = RingPreviewerActivity.this.addAndSetRingtone(file, 1);
                        Toast.makeText(RingPreviewerActivity.this, (int) R.string.tip_set_ring_success, 1000).show();
                        GenUtil.systemPrint("ringtone file.getAbsolutePath() = " + file.getAbsolutePath());
                        return;
                    case 1:
                        File file2 = new File((String) ((HashMap) RingPreviewerActivity.this.mlist.get(RingPreviewerActivity.this.miCurIndex)).get("_dir"));
                        if (!file2.isFile() || !file2.exists()) {
                            Toast.makeText(RingPreviewerActivity.this, (int) R.string.tip_set_notification_failed, 1000).show();
                            return;
                        }
                        boolean unused2 = RingPreviewerActivity.this.addAndSetRingtone(file2, 2);
                        Toast.makeText(RingPreviewerActivity.this, (int) R.string.tip_set_notification_success, 1000).show();
                        GenUtil.systemPrint(RingPreviewerActivity.this.miSetType + "  ringtone file.getAbsolutePath() = " + file2.getAbsolutePath());
                        return;
                    case MainActivity.MENU_UNSETRINGTONE:
                        File file3 = new File((String) ((HashMap) RingPreviewerActivity.this.mlist.get(RingPreviewerActivity.this.miCurIndex)).get("_dir"));
                        if (!file3.isFile() || !file3.exists()) {
                            Toast.makeText(RingPreviewerActivity.this, (int) R.string.tip_set_alarm_failed, 1000).show();
                            return;
                        }
                        boolean unused3 = RingPreviewerActivity.this.addAndSetRingtone(file3, 4);
                        Toast.makeText(RingPreviewerActivity.this, (int) R.string.tip_set_alarm_success, 1000).show();
                        GenUtil.systemPrint(RingPreviewerActivity.this.miSetType + "  ringtone file.getAbsolutePath() = " + file3.getAbsolutePath());
                        return;
                    case 3:
                        RingPreviewerActivity.this.assingToContact();
                        return;
                    default:
                        return;
                }
            }
        }).setNegativeButton((int) R.string.alertdialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
            }
        });
        return builder.create();
    }

    /* access modifiers changed from: private */
    public boolean addAndSetRingtone(File file, int iType) {
        Uri localUri = addRingtone(file, iType);
        if (localUri == null) {
            return false;
        }
        RingtoneManager.setActualDefaultRingtoneUri(this, iType, localUri);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private Uri addRingtone(File file, int iType) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("_data", file.getAbsolutePath());
            contentValues.put("title", file.getName());
            contentValues.put("_size", Long.valueOf(file.length()));
            contentValues.put("mime_type", "audio/mp3");
            if (iType == 4) {
                contentValues.put("is_alarm", (Boolean) true);
                contentValues.put("is_notification", (Boolean) false);
                contentValues.put("is_ringtone", (Boolean) false);
                contentValues.put("is_music", (Boolean) false);
            } else if (iType == 2) {
                contentValues.put("is_alarm", (Boolean) false);
                contentValues.put("is_notification", (Boolean) true);
                contentValues.put("is_ringtone", (Boolean) false);
                contentValues.put("is_music", (Boolean) false);
            } else if (iType == 1) {
                contentValues.put("is_alarm", (Boolean) false);
                contentValues.put("is_notification", (Boolean) false);
                contentValues.put("is_ringtone", (Boolean) true);
                contentValues.put("is_music", (Boolean) false);
            }
            Uri uri = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            getContentResolver().delete(uri, "_data=\"" + file.getAbsolutePath() + "\"", null);
            return getContentResolver().insert(uri, contentValues);
        } catch (Exception e) {
            return null;
        }
    }

    class MyHandler extends Handler {
        MyHandler() {
        }

        public void handleMessage(Message paramMessage) {
            switch (paramMessage.what) {
                case 1:
                    if (RingPreviewerActivity.this.mTvTime != null) {
                        RingPreviewerActivity.this.mTvTime.setText((String) paramMessage.obj);
                        break;
                    }
                    break;
                case MainActivity.MENU_UNSETRINGTONE:
                    if (RingPreviewerActivity.this.mTvTime != null) {
                        RingPreviewerActivity.this.mTvTime.setText("0:00");
                        RingPreviewerActivity.this.mTvEnd.setText(RingPreviewerActivity.this.mstrCurTotalTime);
                        break;
                    }
                    break;
            }
            super.handleMessage(paramMessage);
        }
    }

    private boolean isWifiConnected() {
        WifiManager wifiManager = (WifiManager) getSystemService("wifi");
        if (!wifiManager.isWifiEnabled()) {
            return false;
        }
        if (wifiManager.getConnectionInfo().getIpAddress() <= 0) {
            return false;
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            this.mbPause = false;
            this.previewPlayer.stop();
            this.previewPlayer.reset();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void assingToContact1() {
        stopPlay();
        Intent intent1 = new Intent();
        String strFile = (String) this.mlist.get(this.miCurIndex).get("_dir");
        Uri localUri = Uri.parse(strFile);
        GenUtil.systemPrint("strFile = " + strFile);
        intent1.setData(localUri);
        intent1.setClass(this, ChooseContactActivity.class);
        startActivity(intent1);
    }

    public void assingToContact() {
        stopPlay();
        Intent intent = new Intent();
        String strFile = (String) this.mlist.get(this.miCurIndex).get("_dir");
        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_data like ?", new String[]{"%" + strFile}, null);
        GenUtil.systemPrint("strFile = " + strFile);
        GenUtil.systemPrint("cursor.getCount() = " + cursor.getCount());
        if (cursor.getCount() == 0) {
            GenUtil.systemPrint("inner cursor1.getCount() = " + getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, "_data=?", new String[]{"/mnt" + strFile}, null).getCount());
        }
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            int id = cursor.getInt(0);
            cursor.close();
            intent.setData(Uri.withAppendedPath(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, id + ""));
            intent.setClass(this, ChooseContactActivity.class);
            startActivity(intent);
            return;
        }
        GenUtil.systemPrint("error");
        Toast.makeText(this, "error", 1000).show();
    }
}
