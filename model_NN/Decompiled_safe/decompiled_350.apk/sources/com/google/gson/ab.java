package com.google.gson;

import java.io.IOException;

final class ab implements w {
    /* access modifiers changed from: private */
    public final int a;
    /* access modifiers changed from: private */
    public final int b;
    /* access modifiers changed from: private */
    public final int c;
    private final boolean d;

    private class a {
        private final Appendable b;
        private StringBuilder c = new StringBuilder();
        private int d = 0;

        a(Appendable appendable) {
            this.b = appendable;
        }

        private void a(int i) throws IOException {
            if (h().length() + i > ab.this.a - ab.this.c) {
                g();
            }
        }

        /* access modifiers changed from: private */
        public void g() throws IOException {
            if (this.c != null) {
                this.b.append(this.c).append("\n");
            }
            this.c = null;
        }

        private StringBuilder h() {
            if (this.c == null) {
                this.c = new StringBuilder();
                for (int i = 0; i < this.d; i++) {
                    for (int i2 = 0; i2 < ab.this.b; i2++) {
                        this.c.append(' ');
                    }
                }
            }
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public final void a() throws IOException {
            h().append(':');
            a(0);
        }

        /* access modifiers changed from: package-private */
        public final void a(String str) throws IOException {
            a(str.length() + 2);
            h().append('\"');
            h().append(str);
            h().append('\"');
        }

        /* access modifiers changed from: package-private */
        public final void b() throws IOException {
            h().append(',');
            a(0);
        }

        /* access modifiers changed from: package-private */
        public final void b(String str) throws IOException {
            a(str.length() + 2);
            h().append(str);
        }

        /* access modifiers changed from: package-private */
        public final void c() throws IOException {
            a(0);
            h().append('{');
            this.d++;
        }

        /* access modifiers changed from: package-private */
        public final void d() {
            h().append('}');
            this.d--;
        }

        /* access modifiers changed from: package-private */
        public final void e() throws IOException {
            a(0);
            h().append('[');
            this.d++;
        }

        /* access modifiers changed from: package-private */
        public final void f() {
            h().append(']');
            this.d--;
        }
    }

    private class b implements u {
        private final a b;
        private final h c;
        private final boolean d;

        b(a aVar, h hVar, boolean z) {
            this.b = aVar;
            this.c = hVar;
            this.d = z;
        }

        private String b(JsonPrimitive jsonPrimitive) throws IOException {
            StringBuilder sb = new StringBuilder();
            jsonPrimitive.a(sb, this.c);
            return sb.toString();
        }

        private void d(boolean z) throws IOException {
            if (!z) {
                this.b.b();
            }
        }

        public final void a() throws IOException {
            this.b.b("null");
        }

        public final void a(JsonPrimitive jsonPrimitive) throws IOException {
            this.b.b(b(jsonPrimitive));
        }

        public final void a(JsonPrimitive jsonPrimitive, boolean z) throws IOException {
            d(z);
            this.b.b(b(jsonPrimitive));
        }

        public final void a(String str, JsonPrimitive jsonPrimitive, boolean z) throws IOException {
            d(z);
            this.b.a(str);
            this.b.a();
            this.b.b(b(jsonPrimitive));
        }

        public final void a(String str, boolean z) throws IOException {
            d(z);
            this.b.a(str);
            this.b.a();
        }

        public final void a(boolean z) throws IOException {
            d(z);
        }

        public final void b() throws IOException {
            this.b.e();
        }

        public final void b(String str, boolean z) throws IOException {
            d(z);
            this.b.a(str);
            this.b.a();
        }

        public final void b(boolean z) throws IOException {
            d(z);
        }

        public final void c() {
            this.b.f();
        }

        public final void c(String str, boolean z) throws IOException {
            if (this.d) {
                b(str, z);
            }
        }

        public final void c(boolean z) throws IOException {
            d(z);
        }

        public final void d() throws IOException {
            this.b.c();
        }

        public final void e() {
            this.b.d();
        }
    }

    ab() {
        this(true, (byte) 0);
    }

    private ab(boolean z) {
        this.a = 80;
        this.b = 2;
        this.c = 4;
        this.d = z;
    }

    ab(boolean z, byte b2) {
        this(z);
    }

    public final void a(JsonElement jsonElement, Appendable appendable, boolean z) throws IOException {
        if (jsonElement != null) {
            a aVar = new a(appendable);
            new ae(new b(aVar, new h(this.d), z), z).a(jsonElement);
            aVar.g();
        }
    }
}
