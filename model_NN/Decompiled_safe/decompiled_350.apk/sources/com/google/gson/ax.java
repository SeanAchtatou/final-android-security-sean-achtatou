package com.google.gson;

import java.io.IOException;
import java.io.Reader;

final class ax {
    int a;
    int b;
    int c;
    public int d;
    protected int[] e;
    protected int[] f;
    protected int g;
    protected int h;
    protected boolean i;
    protected boolean j;
    protected Reader k;
    protected char[] l;
    protected int m;
    protected int n;
    protected int o;

    public ax(Reader reader) {
        this(reader, (byte) 0);
    }

    private ax(Reader reader, byte b2) {
        this.d = -1;
        this.g = 0;
        this.h = 1;
        this.i = false;
        this.j = false;
        this.m = 0;
        this.n = 0;
        this.o = 8;
        this.k = reader;
        this.h = 1;
        this.g = 0;
        this.a = 4096;
        this.b = 4096;
        this.l = new char[4096];
        this.e = new int[4096];
        this.f = new int[4096];
    }

    private void a(boolean z) {
        char[] cArr = new char[(this.a + 2048)];
        int[] iArr = new int[(this.a + 2048)];
        int[] iArr2 = new int[(this.a + 2048)];
        if (z) {
            try {
                System.arraycopy(this.l, this.c, cArr, 0, this.a - this.c);
                System.arraycopy(this.l, 0, cArr, this.a - this.c, this.d);
                this.l = cArr;
                System.arraycopy(this.e, this.c, iArr, 0, this.a - this.c);
                System.arraycopy(this.e, 0, iArr, this.a - this.c, this.d);
                this.e = iArr;
                System.arraycopy(this.f, this.c, iArr2, 0, this.a - this.c);
                System.arraycopy(this.f, 0, iArr2, this.a - this.c, this.d);
                this.f = iArr2;
                int i2 = this.d + (this.a - this.c);
                this.d = i2;
                this.m = i2;
            } catch (Throwable th) {
                throw new Error(th.getMessage());
            }
        } else {
            System.arraycopy(this.l, this.c, cArr, 0, this.a - this.c);
            this.l = cArr;
            System.arraycopy(this.e, this.c, iArr, 0, this.a - this.c);
            this.e = iArr;
            System.arraycopy(this.f, this.c, iArr2, 0, this.a - this.c);
            this.f = iArr2;
            int i3 = this.d - this.c;
            this.d = i3;
            this.m = i3;
        }
        this.a += 2048;
        this.b = this.a;
        this.c = 0;
    }

    public final char a() throws IOException {
        this.c = -1;
        char b2 = b();
        this.c = this.d;
        return b2;
    }

    public final void a(int i2) {
        this.n += i2;
        int i3 = this.d - i2;
        this.d = i3;
        if (i3 < 0) {
            this.d += this.a;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00da  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final char b() throws java.io.IOException {
        /*
            r8 = this;
            r2 = 2048(0x800, float:2.87E-42)
            r7 = -1
            r6 = 0
            r5 = 1
            int r0 = r8.n
            if (r0 <= 0) goto L_0x0021
            int r0 = r8.n
            int r0 = r0 - r5
            r8.n = r0
            int r0 = r8.d
            int r0 = r0 + 1
            r8.d = r0
            int r1 = r8.a
            if (r0 != r1) goto L_0x001a
            r8.d = r6
        L_0x001a:
            char[] r0 = r8.l
            int r1 = r8.d
            char r0 = r0[r1]
        L_0x0020:
            return r0
        L_0x0021:
            int r0 = r8.d
            int r0 = r0 + 1
            r8.d = r0
            int r1 = r8.m
            if (r0 < r1) goto L_0x009e
            int r0 = r8.m
            int r1 = r8.b
            if (r0 != r1) goto L_0x0043
            int r0 = r8.b
            int r1 = r8.a
            if (r0 != r1) goto L_0x007e
            int r0 = r8.c
            if (r0 <= r2) goto L_0x0071
            r8.m = r6
            r8.d = r6
            int r0 = r8.c
            r8.b = r0
        L_0x0043:
            java.io.Reader r0 = r8.k     // Catch:{ IOException -> 0x005f }
            char[] r1 = r8.l     // Catch:{ IOException -> 0x005f }
            int r2 = r8.m     // Catch:{ IOException -> 0x005f }
            int r3 = r8.b     // Catch:{ IOException -> 0x005f }
            int r4 = r8.m     // Catch:{ IOException -> 0x005f }
            int r3 = r3 - r4
            int r0 = r0.read(r1, r2, r3)     // Catch:{ IOException -> 0x005f }
            if (r0 != r7) goto L_0x0099
            java.io.Reader r0 = r8.k     // Catch:{ IOException -> 0x005f }
            r0.close()     // Catch:{ IOException -> 0x005f }
            java.io.IOException r0 = new java.io.IOException     // Catch:{ IOException -> 0x005f }
            r0.<init>()     // Catch:{ IOException -> 0x005f }
            throw r0     // Catch:{ IOException -> 0x005f }
        L_0x005f:
            r0 = move-exception
            int r1 = r8.d
            int r1 = r1 - r5
            r8.d = r1
            r8.a(r6)
            int r1 = r8.c
            if (r1 != r7) goto L_0x0070
            int r1 = r8.d
            r8.c = r1
        L_0x0070:
            throw r0
        L_0x0071:
            int r0 = r8.c
            if (r0 >= 0) goto L_0x007a
            r8.m = r6
            r8.d = r6
            goto L_0x0043
        L_0x007a:
            r8.a(r6)
            goto L_0x0043
        L_0x007e:
            int r0 = r8.b
            int r1 = r8.c
            if (r0 <= r1) goto L_0x0089
            int r0 = r8.a
            r8.b = r0
            goto L_0x0043
        L_0x0089:
            int r0 = r8.c
            int r1 = r8.b
            int r0 = r0 - r1
            if (r0 >= r2) goto L_0x0094
            r8.a(r5)
            goto L_0x0043
        L_0x0094:
            int r0 = r8.c
            r8.b = r0
            goto L_0x0043
        L_0x0099:
            int r1 = r8.m     // Catch:{ IOException -> 0x005f }
            int r0 = r0 + r1
            r8.m = r0     // Catch:{ IOException -> 0x005f }
        L_0x009e:
            char[] r0 = r8.l
            int r1 = r8.d
            char r0 = r0[r1]
            int r1 = r8.g
            int r1 = r1 + 1
            r8.g = r1
            boolean r1 = r8.j
            if (r1 == 0) goto L_0x00cd
            r8.j = r6
        L_0x00b0:
            int r1 = r8.h
            r8.g = r5
            int r1 = r1 + 1
            r8.h = r1
        L_0x00b8:
            switch(r0) {
                case 9: goto L_0x00e0;
                case 10: goto L_0x00dd;
                case 11: goto L_0x00bb;
                case 12: goto L_0x00bb;
                case 13: goto L_0x00da;
                default: goto L_0x00bb;
            }
        L_0x00bb:
            int[] r1 = r8.e
            int r2 = r8.d
            int r3 = r8.h
            r1[r2] = r3
            int[] r1 = r8.f
            int r2 = r8.d
            int r3 = r8.g
            r1[r2] = r3
            goto L_0x0020
        L_0x00cd:
            boolean r1 = r8.i
            if (r1 == 0) goto L_0x00b8
            r8.i = r6
            r1 = 10
            if (r0 != r1) goto L_0x00b0
            r8.j = r5
            goto L_0x00b8
        L_0x00da:
            r8.i = r5
            goto L_0x00bb
        L_0x00dd:
            r8.j = r5
            goto L_0x00bb
        L_0x00e0:
            int r1 = r8.g
            int r1 = r1 - r5
            r8.g = r1
            int r1 = r8.g
            int r2 = r8.o
            int r3 = r8.g
            int r4 = r8.o
            int r3 = r3 % r4
            int r2 = r2 - r3
            int r1 = r1 + r2
            r8.g = r1
            goto L_0x00bb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.gson.ax.b():char");
    }

    public final int c() {
        return this.f[this.d];
    }

    public final int d() {
        return this.e[this.d];
    }

    public final int e() {
        return this.f[this.c];
    }

    public final int f() {
        return this.e[this.c];
    }

    public final String g() {
        return this.d >= this.c ? new String(this.l, this.c, (this.d - this.c) + 1) : new String(this.l, this.c, this.a - this.c) + new String(this.l, 0, this.d + 1);
    }
}
