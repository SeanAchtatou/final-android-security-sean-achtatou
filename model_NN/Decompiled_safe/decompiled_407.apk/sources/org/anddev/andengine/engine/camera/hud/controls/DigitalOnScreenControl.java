package org.anddev.andengine.engine.camera.hud.controls;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class DigitalOnScreenControl extends BaseOnScreenControl {
    public DigitalOnScreenControl(int pX, int pY, Camera pCamera, TextureRegion pControlBaseTextureRegion, TextureRegion pControlKnobTextureRegion, float pTimeBetweenUpdates, BaseOnScreenControl.IOnScreenControlListener pOnScreenControlListener) {
        super(pX, pY, pCamera, pControlBaseTextureRegion, pControlKnobTextureRegion, pTimeBetweenUpdates, pOnScreenControlListener);
    }

    /* access modifiers changed from: protected */
    public void onUpdateControlKnob(float pRelativeX, float pRelativeY) {
        if (pRelativeX == Const.MOVE_LIMITED_SPEED && pRelativeY == Const.MOVE_LIMITED_SPEED) {
            super.onUpdateControlKnob(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
        }
        if (Math.abs(pRelativeX) > Math.abs(pRelativeY)) {
            if (pRelativeX > Const.MOVE_LIMITED_SPEED) {
                super.onUpdateControlKnob(0.5f, Const.MOVE_LIMITED_SPEED);
            } else if (pRelativeX < Const.MOVE_LIMITED_SPEED) {
                super.onUpdateControlKnob(-0.5f, Const.MOVE_LIMITED_SPEED);
            } else if (pRelativeX == Const.MOVE_LIMITED_SPEED) {
                super.onUpdateControlKnob(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
            }
        } else if (pRelativeY > Const.MOVE_LIMITED_SPEED) {
            super.onUpdateControlKnob(Const.MOVE_LIMITED_SPEED, 0.5f);
        } else if (pRelativeY < Const.MOVE_LIMITED_SPEED) {
            super.onUpdateControlKnob(Const.MOVE_LIMITED_SPEED, -0.5f);
        } else if (pRelativeY == Const.MOVE_LIMITED_SPEED) {
            super.onUpdateControlKnob(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
        }
    }
}
