package org.anddev.andengine.engine.handler.timer;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.engine.handler.IUpdateHandler;

public class TimerHandler implements IUpdateHandler {
    private boolean mAutoReset;
    private boolean mCallbackTriggered;
    private float mSecondsPassed;
    private final ITimerCallback mTimerCallback;
    private final float mTimerSeconds;

    public TimerHandler(float pTimerSeconds, ITimerCallback pTimerCallback) {
        this(pTimerSeconds, false, pTimerCallback);
    }

    public TimerHandler(float pTimerSeconds, boolean pAutoReset, ITimerCallback pTimerCallback) {
        this.mCallbackTriggered = false;
        this.mTimerSeconds = pTimerSeconds;
        this.mAutoReset = pAutoReset;
        this.mTimerCallback = pTimerCallback;
    }

    public boolean isAutoReset() {
        return this.mAutoReset;
    }

    public void setAutoReset(boolean pAutoReset) {
        this.mAutoReset = pAutoReset;
    }

    public void onUpdate(float pSecondsElapsed) {
        if (this.mAutoReset) {
            this.mSecondsPassed += pSecondsElapsed;
            while (this.mSecondsPassed >= this.mTimerSeconds) {
                this.mSecondsPassed -= this.mTimerSeconds;
                this.mTimerCallback.onTimePassed(this);
            }
        } else if (!this.mCallbackTriggered) {
            this.mSecondsPassed += pSecondsElapsed;
            if (this.mSecondsPassed >= this.mTimerSeconds) {
                this.mCallbackTriggered = true;
                this.mTimerCallback.onTimePassed(this);
            }
        }
    }

    public void reset() {
        this.mCallbackTriggered = false;
        this.mSecondsPassed = Const.MOVE_LIMITED_SPEED;
    }
}
