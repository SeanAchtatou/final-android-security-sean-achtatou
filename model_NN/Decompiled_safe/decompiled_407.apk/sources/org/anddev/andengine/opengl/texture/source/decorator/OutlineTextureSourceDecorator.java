package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Paint;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseShapeTextureSourceDecorator;

public class OutlineTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    protected final int mOutlineColor;

    public OutlineTextureSourceDecorator(ITextureSource pTextureSource, int pOutlineColor) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pOutlineColor);
    }

    public OutlineTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pOutlineColor) {
        this(pTextureSource, pTextureSourceDecoratorShape, pOutlineColor, false);
    }

    public OutlineTextureSourceDecorator(ITextureSource pTextureSource, int pOutlineColor, boolean pAntiAliasing) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pOutlineColor, pAntiAliasing);
    }

    public OutlineTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pOutlineColor, boolean pAntiAliasing) {
        super(pTextureSource, pTextureSourceDecoratorShape, pAntiAliasing);
        this.mOutlineColor = pOutlineColor;
        this.mPaint.setStyle(Paint.Style.STROKE);
        this.mPaint.setColor(pOutlineColor);
    }

    public OutlineTextureSourceDecorator clone() {
        return new OutlineTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mOutlineColor, this.mAntiAliasing);
    }
}
