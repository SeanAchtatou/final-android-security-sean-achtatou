package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Canvas;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.LinearGradientFillTextureSourceDecorator;

public class RectangleLinearGradientFillTextureSourceDecorator extends LinearGradientFillTextureSourceDecorator {
    public RectangleLinearGradientFillTextureSourceDecorator(ITextureSource pTextureSource, int pFromColor, int pToColor, LinearGradientFillTextureSourceDecorator.LinearGradientDirection pLinearGradientDirection) {
        super(pTextureSource, pFromColor, pToColor, pLinearGradientDirection);
    }

    public RectangleLinearGradientFillTextureSourceDecorator clone() {
        return new RectangleLinearGradientFillTextureSourceDecorator(this.mTextureSource, this.mFromColor, this.mToColor, this.mLinearGradientDirection);
    }

    /* access modifiers changed from: protected */
    public void onDecorateBitmap(Canvas pCanvas) {
        pCanvas.drawRect(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) (pCanvas.getWidth() - 1), (float) (pCanvas.getHeight() - 1), this.mPaint);
    }
}
