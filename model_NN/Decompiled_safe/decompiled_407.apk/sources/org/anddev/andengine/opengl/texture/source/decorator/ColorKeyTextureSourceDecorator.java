package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.AvoidXfermode;
import org.anddev.andengine.opengl.texture.source.ITextureSource;
import org.anddev.andengine.opengl.texture.source.decorator.BaseShapeTextureSourceDecorator;

public class ColorKeyTextureSourceDecorator extends BaseShapeTextureSourceDecorator {
    private static final int TOLERANCE_DEFAULT = 0;
    private final int mColorKeyColor;

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, int pColorKeyColor) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pColorKeyColor);
    }

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pColorKeyColor) {
        this(pTextureSource, pTextureSourceDecoratorShape, pColorKeyColor, 0);
    }

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, int pColorKeyColor, int pTolerance) {
        this(pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape.RECTANGLE, pColorKeyColor, pTolerance);
    }

    public ColorKeyTextureSourceDecorator(ITextureSource pTextureSource, BaseShapeTextureSourceDecorator.TextureSourceDecoratorShape pTextureSourceDecoratorShape, int pColorKeyColor, int pTolerance) {
        super(pTextureSource, pTextureSourceDecoratorShape);
        this.mColorKeyColor = pColorKeyColor;
        this.mPaint.setXfermode(new AvoidXfermode(pColorKeyColor, pTolerance, AvoidXfermode.Mode.TARGET));
        this.mPaint.setColor(0);
    }

    public ColorKeyTextureSourceDecorator clone() {
        return new ColorKeyTextureSourceDecorator(this.mTextureSource, this.mTextureSourceDecoratorShape, this.mColorKeyColor);
    }
}
