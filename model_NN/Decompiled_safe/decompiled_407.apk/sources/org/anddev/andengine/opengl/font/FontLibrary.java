package org.anddev.andengine.opengl.font;

import android.util.SparseArray;
import org.anddev.andengine.util.Library;

public class FontLibrary extends Library<Font> {
    public FontLibrary() {
    }

    public FontLibrary(int pInitialCapacity) {
        super(pInitialCapacity);
    }

    /* access modifiers changed from: package-private */
    public void loadFonts(FontManager pFontManager) {
        SparseArray<Font> items = this.mItems;
        for (int i = items.size() - 1; i >= 0; i--) {
            pFontManager.loadFont((Font) items.get(i));
        }
    }
}
