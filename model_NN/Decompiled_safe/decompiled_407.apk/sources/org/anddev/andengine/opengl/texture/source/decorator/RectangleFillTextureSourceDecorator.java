package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Canvas;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class RectangleFillTextureSourceDecorator extends FillTextureSourceDecorator {
    public RectangleFillTextureSourceDecorator(ITextureSource pTextureSource, int pFillColor) {
        super(pTextureSource, pFillColor);
    }

    public RectangleFillTextureSourceDecorator clone() {
        return new RectangleFillTextureSourceDecorator(this.mTextureSource, this.mFillColor);
    }

    /* access modifiers changed from: protected */
    public void onDecorateBitmap(Canvas pCanvas) {
        pCanvas.drawRect(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) (pCanvas.getWidth() - 1), (float) (pCanvas.getHeight() - 1), this.mPaint);
    }
}
