package org.anddev.andengine.opengl.texture.source.decorator;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public abstract class BaseShapeTextureSourceDecorator extends BaseTextureSourceDecorator {
    protected final TextureSourceDecoratorShape mTextureSourceDecoratorShape;

    public enum TextureSourceDecoratorShape {
        CIRCLE {
            public void onDecorateBitmap(Canvas pCanvas, Paint pPaint) {
                float widthHalf = (float) (pCanvas.getWidth() / 2);
                float heightHalf = (float) (pCanvas.getHeight() / 2);
                pCanvas.drawCircle(widthHalf - 0.5f, heightHalf - 0.5f, Math.min(widthHalf, heightHalf), pPaint);
            }
        },
        ELLIPSE {
            private final RectF mRectF = new RectF();

            public void onDecorateBitmap(Canvas pCanvas, Paint pPaint) {
                this.mRectF.set(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) (pCanvas.getWidth() - 1), (float) (pCanvas.getWidth() - 1));
                pCanvas.drawOval(this.mRectF, pPaint);
            }
        },
        RECTANGLE {
            public void onDecorateBitmap(Canvas pCanvas, Paint pPaint) {
                pCanvas.drawRect(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) (pCanvas.getWidth() - 1), (float) (pCanvas.getHeight() - 1), pPaint);
            }
        };

        public abstract void onDecorateBitmap(Canvas canvas, Paint paint);
    }

    public abstract BaseShapeTextureSourceDecorator clone();

    public BaseShapeTextureSourceDecorator(ITextureSource pTextureSource, TextureSourceDecoratorShape pTextureSourceDecoratorShape) {
        super(pTextureSource);
        this.mTextureSourceDecoratorShape = pTextureSourceDecoratorShape;
    }

    public BaseShapeTextureSourceDecorator(ITextureSource pTextureSource, TextureSourceDecoratorShape pTextureSourceDecoratorShape, boolean pAntiAliasing) {
        super(pTextureSource, pAntiAliasing);
        this.mTextureSourceDecoratorShape = pTextureSourceDecoratorShape;
    }

    /* access modifiers changed from: protected */
    public void onDecorateBitmap(Canvas pCanvas) {
        this.mTextureSourceDecoratorShape.onDecorateBitmap(pCanvas, this.mPaint);
    }
}
