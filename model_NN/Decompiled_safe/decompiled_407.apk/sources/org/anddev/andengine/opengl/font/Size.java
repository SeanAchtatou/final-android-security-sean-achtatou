package org.anddev.andengine.opengl.font;

import com.finger2finger.games.res.Const;

class Size {
    private float mHeight;
    private float mWidth;

    public Size() {
        this(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
    }

    public Size(float pWidth, float pHeight) {
        setWidth(pWidth);
        setHeight(pHeight);
    }

    public void setWidth(float width) {
        this.mWidth = width;
    }

    public float getWidth() {
        return this.mWidth;
    }

    public void setHeight(float height) {
        this.mHeight = height;
    }

    public float getHeight() {
        return this.mHeight;
    }

    public void set(int pWidth, int pHeight) {
        setWidth((float) pWidth);
        setHeight((float) pHeight);
    }
}
