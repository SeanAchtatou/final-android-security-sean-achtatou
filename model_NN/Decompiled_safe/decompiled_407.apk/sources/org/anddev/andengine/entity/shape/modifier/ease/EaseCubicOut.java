package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseCubicOut implements IEaseFunction {
    private static EaseCubicOut INSTANCE;

    private EaseCubicOut() {
    }

    public static EaseCubicOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseCubicOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (pSecondsElapsed / pDuration) - 1.0f;
        return (((pSecondsElapsed2 * pSecondsElapsed2 * pSecondsElapsed2) + 1.0f) * pMaxValue) + pMinValue;
    }
}
