package org.anddev.andengine.entity.layer;

import java.util.ArrayList;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.scene.Scene;

public abstract class BaseLayer extends Entity implements ILayer {
    private final ArrayList<Scene.ITouchArea> mTouchAreas = new ArrayList<>();

    public BaseLayer() {
    }

    public BaseLayer(int pZIndex) {
        super(pZIndex);
    }

    public void registerTouchArea(Scene.ITouchArea pTouchArea) {
        this.mTouchAreas.add(pTouchArea);
    }

    public void unregisterTouchArea(Scene.ITouchArea pTouchArea) {
        this.mTouchAreas.remove(pTouchArea);
    }

    public ArrayList<Scene.ITouchArea> getTouchAreas() {
        return this.mTouchAreas;
    }
}
