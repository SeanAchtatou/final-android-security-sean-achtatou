package org.anddev.andengine.entity.layer;

import java.util.Comparator;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.util.IEntityMatcher;

public class FixedCapacityLayer extends BaseLayer {
    private final int mCapacity;
    private final IEntity[] mEntities;
    private int mEntityCount = 0;

    public FixedCapacityLayer(int pCapacity) {
        this.mCapacity = pCapacity;
        this.mEntities = new IEntity[pCapacity];
    }

    public int getEntityCount() {
        return this.mEntityCount;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        IEntity[] entities = this.mEntities;
        int entityCount = this.mEntityCount;
        for (int i = 0; i < entityCount; i++) {
            entities[i].onDraw(pGL, pCamera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
        IEntity[] entities = this.mEntities;
        int entityCount = this.mEntityCount;
        for (int i = 0; i < entityCount; i++) {
            entities[i].onUpdate(pSecondsElapsed);
        }
    }

    public void reset() {
        super.reset();
        IEntity[] entities = this.mEntities;
        for (int i = this.mEntityCount - 1; i >= 0; i--) {
            entities[i].reset();
        }
    }

    public IEntity getEntity(int pIndex) {
        checkIndex(pIndex);
        return this.mEntities[pIndex];
    }

    public void clear() {
        IEntity[] entities = this.mEntities;
        for (int i = this.mEntityCount - 1; i >= 0; i--) {
            entities[i] = null;
        }
        this.mEntityCount = 0;
    }

    public void addEntity(IEntity pEntity) {
        if (this.mEntityCount < this.mCapacity) {
            this.mEntities[this.mEntityCount] = pEntity;
            this.mEntityCount++;
        }
    }

    public boolean removeEntity(IEntity pEntity) {
        return removeEntity(indexOfEntity(pEntity)) != null;
    }

    public IEntity removeEntity(int pIndex) {
        checkIndex(pIndex);
        IEntity[] entities = this.mEntities;
        IEntity out = entities[pIndex];
        int lastIndex = this.mEntityCount - 1;
        if (pIndex == lastIndex) {
            this.mEntities[lastIndex] = null;
        } else {
            entities[pIndex] = entities[lastIndex];
            entities[this.mEntityCount] = null;
        }
        this.mEntityCount = lastIndex;
        return out;
    }

    public boolean removeEntity(IEntityMatcher pEntityMatcher) {
        IEntity[] entities = this.mEntities;
        for (int i = entities.length - 1; i >= 0; i--) {
            if (pEntityMatcher.matches(entities[i])) {
                removeEntity(i);
                return true;
            }
        }
        return false;
    }

    public IEntity findEntity(IEntityMatcher pEntityMatcher) {
        IEntity[] entities = this.mEntities;
        for (int i = entities.length - 1; i >= 0; i--) {
            IEntity entity = entities[i];
            if (pEntityMatcher.matches(entity)) {
                return entity;
            }
        }
        return null;
    }

    private int indexOfEntity(IEntity pEntity) {
        IEntity[] entities = this.mEntities;
        for (int i = entities.length - 1; i >= 0; i--) {
            if (entities[i] == pEntity) {
                return i;
            }
        }
        return -1;
    }

    public void sortEntities() {
        ZIndexSorter.getInstance().sort(this.mEntities, 0, this.mEntityCount);
    }

    public void sortEntities(Comparator<IEntity> pEntityComparator) {
        ZIndexSorter.getInstance().sort(this.mEntities, 0, this.mEntityCount, pEntityComparator);
    }

    public IEntity replaceEntity(int pIndex, IEntity pEntity) {
        checkIndex(pIndex);
        IEntity[] entities = this.mEntities;
        IEntity oldEntity = entities[pIndex];
        entities[pIndex] = pEntity;
        return oldEntity;
    }

    public void setEntity(int pIndex, IEntity pEntity) {
        checkIndex(pIndex);
        if (pIndex == this.mEntityCount) {
            addEntity(pEntity);
        } else if (pIndex < this.mEntityCount) {
            this.mEntities[pIndex] = pEntity;
        }
    }

    public void swapEntities(int pEntityIndexA, int pEntityIndexB) {
        if (pEntityIndexA > this.mEntityCount) {
            throw new IndexOutOfBoundsException("pEntityIndexA was bigger than the EntityCount.");
        } else if (pEntityIndexA > this.mEntityCount) {
            throw new IndexOutOfBoundsException("pEntityIndexB was bigger than the EntityCount.");
        } else {
            IEntity[] entities = this.mEntities;
            IEntity tmp = entities[pEntityIndexA];
            entities[pEntityIndexA] = entities[pEntityIndexB];
            entities[pEntityIndexB] = tmp;
        }
    }

    private void checkIndex(int pIndex) {
        if (pIndex < 0 || pIndex >= this.mEntityCount) {
            throw new IndexOutOfBoundsException("Invalid index: " + pIndex + " (Size: " + this.mEntityCount + " | Capacity: " + this.mCapacity + ")");
        }
    }
}
