package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.BaseSingleValueChangeModifier;

public abstract class SingleValueChangeShapeModifier extends BaseSingleValueChangeModifier<IShape> implements IShapeModifier {
    public SingleValueChangeShapeModifier(float pDuration, float pValueChange) {
        super(pDuration, pValueChange);
    }

    public SingleValueChangeShapeModifier(float pDuration, float pValueChange, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pValueChange, pShapeModiferListener);
    }

    protected SingleValueChangeShapeModifier(SingleValueChangeShapeModifier pSingleValueChangeShapeModifier) {
        super(pSingleValueChangeShapeModifier);
    }
}
