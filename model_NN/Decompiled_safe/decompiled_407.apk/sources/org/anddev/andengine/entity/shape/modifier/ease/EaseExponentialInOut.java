package org.anddev.andengine.entity.shape.modifier.ease;

import com.finger2finger.games.res.Const;

public class EaseExponentialInOut implements IEaseFunction {
    private static EaseExponentialInOut INSTANCE;

    private EaseExponentialInOut() {
    }

    public static EaseExponentialInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseExponentialInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        if (pSecondsElapsed == Const.MOVE_LIMITED_SPEED) {
            return pMinValue;
        }
        if (pSecondsElapsed == pDuration) {
            return pMinValue + pMaxValue;
        }
        float pSecondsElapsed2 = pSecondsElapsed / (pDuration * 0.5f);
        if (pSecondsElapsed2 < 1.0f) {
            return (float) ((((double) (pMaxValue * 0.5f)) * Math.pow(2.0d, (double) (10.0f * (pSecondsElapsed2 - 1.0f)))) + ((double) pMinValue));
        }
        return (float) ((((double) (pMaxValue * 0.5f)) * ((-Math.pow(2.0d, (double) (-10.0f * (pSecondsElapsed2 - 1.0f)))) + 2.0d)) + ((double) pMinValue));
    }
}
