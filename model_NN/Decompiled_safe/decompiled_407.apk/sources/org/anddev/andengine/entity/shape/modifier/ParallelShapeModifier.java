package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.ParallelModifier;

public class ParallelShapeModifier extends ParallelModifier<IShape> implements IShapeModifier {
    public ParallelShapeModifier(IShapeModifier... pShapeModifiers) throws IllegalArgumentException {
        super(pShapeModifiers);
    }

    public ParallelShapeModifier(IShapeModifier.IShapeModifierListener pShapeModiferListener, IShapeModifier... pShapeModifiers) throws IllegalArgumentException {
        super(pShapeModiferListener, pShapeModifiers);
    }

    protected ParallelShapeModifier(ParallelShapeModifier pParallelShapeModifier) {
        super(pParallelShapeModifier);
    }

    public ParallelShapeModifier clone() {
        return new ParallelShapeModifier(this);
    }
}
