package org.anddev.andengine.entity.scene.background;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureManager;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class RepeatingSpriteBackground extends SpriteBackground {
    private Texture mTexture;

    public RepeatingSpriteBackground(float pCameraWidth, float pCameraHeight, TextureManager pTextureManager, ITextureSource pTextureSource) throws IllegalArgumentException {
        super(null);
        this.mEntity = loadSprite(pCameraWidth, pCameraHeight, pTextureManager, pTextureSource);
    }

    public RepeatingSpriteBackground(float pRed, float pGreen, float pBlue, float pCameraWidth, float pCameraHeight, TextureManager pTextureManager, ITextureSource pTextureSource) throws IllegalArgumentException {
        super(pRed, pGreen, pBlue, null);
        this.mEntity = loadSprite(pCameraWidth, pCameraHeight, pTextureManager, pTextureSource);
    }

    public Texture getTexture() {
        return this.mTexture;
    }

    private Sprite loadSprite(float pCameraWidth, float pCameraHeight, TextureManager pTextureManager, ITextureSource pTextureSource) throws IllegalArgumentException {
        this.mTexture = new Texture(pTextureSource.getWidth(), pTextureSource.getHeight(), TextureOptions.REPEATING_PREMULTIPLYALPHA);
        TextureRegion textureRegion = TextureRegionFactory.createFromSource(this.mTexture, pTextureSource, 0, 0);
        int width = Math.round(pCameraWidth);
        int height = Math.round(pCameraHeight);
        textureRegion.setWidth(width);
        textureRegion.setHeight(height);
        pTextureManager.loadTexture(this.mTexture);
        return new Sprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) width, (float) height, textureRegion);
    }
}
