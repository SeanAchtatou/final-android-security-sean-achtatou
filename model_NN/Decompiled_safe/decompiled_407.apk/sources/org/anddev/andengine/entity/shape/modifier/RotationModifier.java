package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;

public class RotationModifier extends SingleValueSpanShapeModifier {
    public RotationModifier(float pDuration, float pFromRotation, float pToRotation) {
        this(pDuration, pFromRotation, pToRotation, null, IEaseFunction.DEFAULT);
    }

    public RotationModifier(float pDuration, float pFromRotation, float pToRotation, IEaseFunction pEaseFunction) {
        this(pDuration, pFromRotation, pToRotation, null, pEaseFunction);
    }

    public RotationModifier(float pDuration, float pFromRotation, float pToRotation, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pFromRotation, pToRotation, pShapeModiferListener, IEaseFunction.DEFAULT);
    }

    public RotationModifier(float pDuration, float pFromRotation, float pToRotation, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromRotation, pToRotation, pShapeModiferListener, pEaseFunction);
    }

    protected RotationModifier(RotationModifier pRotationModifier) {
        super(pRotationModifier);
    }

    public RotationModifier clone() {
        return new RotationModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(IShape pShape, float pRotation) {
        pShape.setRotation(pRotation);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(IShape pShape, float pPercentageDone, float pRotation) {
        pShape.setRotation(pRotation);
    }
}
