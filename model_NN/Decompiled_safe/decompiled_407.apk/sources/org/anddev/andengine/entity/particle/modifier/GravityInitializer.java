package org.anddev.andengine.entity.particle.modifier;

import com.finger2finger.games.res.Const;

public class GravityInitializer extends AccelerationInitializer {
    public GravityInitializer() {
        super(Const.MOVE_LIMITED_SPEED, 9.80665f);
    }
}
