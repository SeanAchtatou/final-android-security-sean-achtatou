package org.anddev.andengine.entity.shape.modifier.ease;

public interface IEaseFunction {
    public static final IEaseFunction DEFAULT = EaseLinear.getInstance();

    float getPercentageDone(float f, float f2, float f3, float f4);
}
