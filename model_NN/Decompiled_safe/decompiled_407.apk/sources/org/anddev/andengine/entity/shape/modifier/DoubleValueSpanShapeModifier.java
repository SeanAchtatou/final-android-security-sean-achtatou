package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;
import org.anddev.andengine.util.modifier.BaseDoubleValueSpanModifier;

public abstract class DoubleValueSpanShapeModifier extends BaseDoubleValueSpanModifier<IShape> implements IShapeModifier {
    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB);
    }

    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pEaseFunction);
    }

    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pShapeModiferListener);
    }

    public DoubleValueSpanShapeModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pShapeModiferListener, pEaseFunction);
    }

    protected DoubleValueSpanShapeModifier(DoubleValueSpanShapeModifier pDoubleValueSpanModifier) {
        super(pDoubleValueSpanModifier);
    }
}
