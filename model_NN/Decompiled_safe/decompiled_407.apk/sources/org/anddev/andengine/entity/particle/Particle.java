package org.anddev.andengine.entity.particle;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public class Particle extends Sprite {
    boolean mDead = false;
    private float mDeathTime = -1.0f;
    private float mLifeTime = Const.MOVE_LIMITED_SPEED;

    public Particle(float pX, float pY, TextureRegion pTextureRegion) {
        super(pX, pY, pTextureRegion);
    }

    public Particle(float pX, float pY, TextureRegion pTextureRegion, RectangleVertexBuffer pRectangleVertexBuffer) {
        super(pX, pY, pTextureRegion, pRectangleVertexBuffer);
    }

    public float getLifeTime() {
        return this.mLifeTime;
    }

    public float getDeathTime() {
        return this.mDeathTime;
    }

    public void setDeathTime(float pDeathTime) {
        this.mDeathTime = pDeathTime;
    }

    public boolean isDead() {
        return this.mDead;
    }

    public void setDead(boolean pDead) {
        this.mDead = pDead;
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
        if (!this.mDead) {
            this.mLifeTime += pSecondsElapsed;
            super.onManagedUpdate(pSecondsElapsed);
            float deathTime = this.mDeathTime;
            if (deathTime != -1.0f && this.mLifeTime > deathTime) {
                setDead(true);
            }
        }
    }

    public void reset() {
        super.reset();
        this.mDead = false;
        this.mDeathTime = -1.0f;
        this.mLifeTime = Const.MOVE_LIMITED_SPEED;
    }
}
