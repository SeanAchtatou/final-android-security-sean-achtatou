package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseQuartOut implements IEaseFunction {
    private static EaseQuartOut INSTANCE;

    private EaseQuartOut() {
    }

    public static EaseQuartOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseQuartOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float pSecondsElapsed2 = (pSecondsElapsed / pDuration) - 1.0f;
        return ((-pMaxValue) * ((((pSecondsElapsed2 * pSecondsElapsed2) * pSecondsElapsed2) * pSecondsElapsed2) - 1.0f)) + pMinValue;
    }
}
