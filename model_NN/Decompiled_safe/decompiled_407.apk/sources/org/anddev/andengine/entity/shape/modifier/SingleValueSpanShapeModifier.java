package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;
import org.anddev.andengine.util.modifier.BaseSingleValueSpanModifier;

public abstract class SingleValueSpanShapeModifier extends BaseSingleValueSpanModifier<IShape> implements IShapeModifier {
    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue) {
        super(pDuration, pFromValue, pToValue);
    }

    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValue, pToValue, pEaseFunction);
    }

    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, pFromValue, pToValue, pShapeModiferListener);
    }

    public SingleValueSpanShapeModifier(float pDuration, float pFromValue, float pToValue, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValue, pToValue, pShapeModiferListener, pEaseFunction);
    }

    protected SingleValueSpanShapeModifier(SingleValueSpanShapeModifier pSingleValueSpanShapeModifier) {
        super(pSingleValueSpanShapeModifier);
    }
}
