package org.anddev.andengine.entity.layer;

import java.util.ArrayList;
import java.util.Comparator;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.util.IEntityMatcher;

public interface ILayer extends IEntity {
    void addEntity(IEntity iEntity);

    void clear();

    IEntity findEntity(IEntityMatcher iEntityMatcher);

    IEntity getEntity(int i);

    int getEntityCount();

    ArrayList<Scene.ITouchArea> getTouchAreas();

    void registerTouchArea(Scene.ITouchArea iTouchArea);

    IEntity removeEntity(int i);

    boolean removeEntity(IEntity iEntity);

    boolean removeEntity(IEntityMatcher iEntityMatcher);

    IEntity replaceEntity(int i, IEntity iEntity);

    void setEntity(int i, IEntity iEntity);

    void sortEntities();

    void sortEntities(Comparator<IEntity> comparator);

    void swapEntities(int i, int i2);

    void unregisterTouchArea(Scene.ITouchArea iTouchArea);
}
