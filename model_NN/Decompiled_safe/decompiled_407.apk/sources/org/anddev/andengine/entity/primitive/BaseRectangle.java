package org.anddev.andengine.entity.primitive;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.shape.RectangularShape;
import org.anddev.andengine.opengl.vertex.RectangleVertexBuffer;

public abstract class BaseRectangle extends RectangularShape {
    public BaseRectangle(float pX, float pY, float pWidth, float pHeight) {
        super(pX, pY, pWidth, pHeight, new RectangleVertexBuffer(35044));
        updateVertexBuffer();
    }

    public BaseRectangle(float pX, float pY, float pWidth, float pHeight, RectangleVertexBuffer pRectangleVertexBuffer) {
        super(pX, pY, pWidth, pHeight, pRectangleVertexBuffer);
    }

    public RectangleVertexBuffer getVertexBuffer() {
        return (RectangleVertexBuffer) super.getVertexBuffer();
    }

    /* access modifiers changed from: protected */
    public void onUpdateVertexBuffer() {
        getVertexBuffer().update(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mWidth, this.mHeight);
    }
}
