package org.anddev.andengine.entity.scene;

import android.util.SparseArray;
import com.finger2finger.games.res.Const;
import java.util.ArrayList;
import javax.microedition.khronos.opengles.GL10;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.UpdateHandlerList;
import org.anddev.andengine.engine.handler.runnable.RunnableHandler;
import org.anddev.andengine.entity.Entity;
import org.anddev.andengine.entity.layer.DynamicCapacityLayer;
import org.anddev.andengine.entity.layer.FixedCapacityLayer;
import org.anddev.andengine.entity.layer.ILayer;
import org.anddev.andengine.entity.layer.ZIndexSorter;
import org.anddev.andengine.entity.scene.background.ColorBackground;
import org.anddev.andengine.entity.scene.background.IBackground;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.util.GLHelper;

public class Scene extends Entity {
    private IBackground mBackground = new ColorBackground(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
    private boolean mBackgroundEnabled = true;
    protected Scene mChildScene;
    private boolean mChildSceneModalDraw;
    private boolean mChildSceneModalTouch;
    private boolean mChildSceneModalUpdate;
    private final int mLayerCount;
    private final ILayer[] mLayers;
    private IOnAreaTouchListener mOnAreaTouchListener;
    private boolean mOnAreaTouchTraversalBackToFront = true;
    private IOnSceneTouchListener mOnSceneTouchListener;
    protected Scene mParentScene;
    private final RunnableHandler mRunnableHandler = new RunnableHandler();
    private float mSecondsElapsedTotal;
    private boolean mTouchAreaBindingEnabled = false;
    private final SparseArray<ITouchArea> mTouchAreaBindings = new SparseArray<>();
    private final ArrayList<ITouchArea> mTouchAreas = new ArrayList<>();
    private final UpdateHandlerList mUpdateHandlers = new UpdateHandlerList();

    public interface IOnAreaTouchListener {
        boolean onAreaTouched(TouchEvent touchEvent, ITouchArea iTouchArea, float f, float f2);
    }

    public interface IOnSceneTouchListener {
        boolean onSceneTouchEvent(Scene scene, TouchEvent touchEvent);
    }

    public interface ITouchArea {
        boolean contains(float f, float f2);

        float[] convertLocalToSceneCoordinates(float f, float f2);

        float[] convertSceneToLocalCoordinates(float f, float f2);

        boolean onAreaTouched(TouchEvent touchEvent, float f, float f2);
    }

    public Scene(int pLayerCount) {
        this.mLayerCount = pLayerCount;
        this.mLayers = new ILayer[pLayerCount];
        createLayers();
    }

    public Scene(int pLayerCount, boolean pFixedCapacityLayers, int... pLayerCapacities) throws IllegalArgumentException {
        if (pLayerCount != pLayerCapacities.length) {
            throw new IllegalArgumentException("pLayerCount must be the same as the length of pLayerCapacities.");
        }
        this.mLayerCount = pLayerCount;
        this.mLayers = new ILayer[pLayerCount];
        createLayers(pFixedCapacityLayers, pLayerCapacities);
    }

    public float getSecondsElapsedTotal() {
        return this.mSecondsElapsedTotal;
    }

    public IBackground getBackground() {
        return this.mBackground;
    }

    public void setBackground(IBackground pBackground) {
        this.mBackground = pBackground;
    }

    public ILayer getLayer(int pLayerIndex) throws ArrayIndexOutOfBoundsException {
        return this.mLayers[pLayerIndex];
    }

    public int getLayerCount() {
        return this.mLayers.length;
    }

    public ILayer getBottomLayer() {
        return this.mLayers[0];
    }

    public ILayer getTopLayer() {
        return this.mLayers[this.mLayerCount - 1];
    }

    public void setLayer(int pLayerIndex, ILayer pLayer) {
        this.mLayers[pLayerIndex] = pLayer;
    }

    public void swapLayers(int pLayerIndexA, int pLayerIndexB) {
        ILayer[] layers = this.mLayers;
        ILayer tmp = layers[pLayerIndexA];
        layers[pLayerIndexA] = layers[pLayerIndexB];
        layers[pLayerIndexB] = tmp;
    }

    public ILayer replaceLayer(int pLayerIndex, ILayer pLayer) {
        ILayer[] layers = this.mLayers;
        ILayer oldLayer = layers[pLayerIndex];
        layers[pLayerIndex] = pLayer;
        return oldLayer;
    }

    public void sortLayers() {
        ZIndexSorter.getInstance().sort(this.mLayers);
    }

    public boolean isBackgroundEnabled() {
        return this.mBackgroundEnabled;
    }

    public void setBackgroundEnabled(boolean pEnabled) {
        this.mBackgroundEnabled = pEnabled;
    }

    public void clearTouchAreas() {
        this.mTouchAreas.clear();
    }

    public void registerTouchArea(ITouchArea pTouchArea) {
        this.mTouchAreas.add(pTouchArea);
    }

    public void unregisterTouchArea(ITouchArea pTouchArea) {
        this.mTouchAreas.remove(pTouchArea);
    }

    public void clearUpdateHandlers() {
        this.mUpdateHandlers.clear();
    }

    public void registerUpdateHandler(IUpdateHandler pUpdateHandler) {
        this.mUpdateHandlers.add(pUpdateHandler);
    }

    public void unregisterUpdateHandler(IUpdateHandler pUpdateHandler) {
        this.mUpdateHandlers.remove(pUpdateHandler);
    }

    public void setOnSceneTouchListener(IOnSceneTouchListener pOnSceneTouchListener) {
        this.mOnSceneTouchListener = pOnSceneTouchListener;
    }

    public IOnSceneTouchListener getOnSceneTouchListener() {
        return this.mOnSceneTouchListener;
    }

    public boolean hasOnSceneTouchListener() {
        return this.mOnSceneTouchListener != null;
    }

    public void setOnAreaTouchListener(IOnAreaTouchListener pOnAreaTouchListener) {
        this.mOnAreaTouchListener = pOnAreaTouchListener;
    }

    public IOnAreaTouchListener getOnAreaTouchListener() {
        return this.mOnAreaTouchListener;
    }

    public boolean hasOnAreaTouchListener() {
        return this.mOnAreaTouchListener != null;
    }

    private void setParentScene(Scene pParentScene) {
        this.mParentScene = pParentScene;
    }

    public boolean hasChildScene() {
        return this.mChildScene != null;
    }

    public Scene getChildScene() {
        return this.mChildScene;
    }

    public void setChildSceneModal(Scene pChildScene) {
        setChildScene(pChildScene, true, true, true);
    }

    public void setChildScene(Scene pChildScene) {
        setChildScene(pChildScene, false, false, false);
    }

    public void setChildScene(Scene pChildScene, boolean pModalDraw, boolean pModalUpdate, boolean pModalTouch) {
        pChildScene.setParentScene(this);
        this.mChildScene = pChildScene;
        this.mChildSceneModalDraw = pModalDraw;
        this.mChildSceneModalUpdate = pModalUpdate;
        this.mChildSceneModalTouch = pModalTouch;
    }

    public void clearChildScene() {
        this.mChildScene = null;
    }

    public void setOnAreaTouchTraversalBackToFront() {
        this.mOnAreaTouchTraversalBackToFront = true;
    }

    public void setOnAreaTouchTraversalFrontToBack() {
        this.mOnAreaTouchTraversalBackToFront = false;
    }

    public void setTouchAreaBindingEnabled(boolean pTouchAreaBindingEnabled) {
        this.mTouchAreaBindingEnabled = pTouchAreaBindingEnabled;
        if (!this.mTouchAreaBindingEnabled) {
            this.mTouchAreaBindings.clear();
        }
    }

    public boolean isTouchAreaBindingEnabled() {
        return this.mTouchAreaBindingEnabled;
    }

    /* access modifiers changed from: protected */
    public void onManagedDraw(GL10 pGL, Camera pCamera) {
        Scene childScene = this.mChildScene;
        if (childScene == null || !this.mChildSceneModalDraw) {
            if (this.mBackgroundEnabled) {
                pCamera.onApplyPositionIndependentMatrix(pGL);
                GLHelper.setModelViewIdentityMatrix(pGL);
                this.mBackground.onDraw(pGL, pCamera);
            }
            pCamera.onApplyMatrix(pGL);
            GLHelper.setModelViewIdentityMatrix(pGL);
            drawLayers(pGL, pCamera);
        }
        if (childScene != null) {
            childScene.onDraw(pGL, pCamera);
        }
    }

    /* access modifiers changed from: protected */
    public void onManagedUpdate(float pSecondsElapsed) {
        updateUpdateHandlers(pSecondsElapsed);
        this.mRunnableHandler.onUpdate(pSecondsElapsed);
        this.mSecondsElapsedTotal += pSecondsElapsed;
        Scene childScene = this.mChildScene;
        if (childScene == null || !this.mChildSceneModalUpdate) {
            this.mBackground.onUpdate(pSecondsElapsed);
            updateLayers(pSecondsElapsed);
        }
        if (childScene != null) {
            childScene.onUpdate(pSecondsElapsed);
        }
    }

    public boolean onSceneTouchEvent(TouchEvent pSceneTouchEvent) {
        boolean isDownAction;
        Boolean handled;
        Boolean handled2;
        Boolean handled3;
        Boolean handled4;
        SparseArray<ITouchArea> touchAreaBindings;
        ITouchArea boundTouchArea;
        int action = pSceneTouchEvent.getAction();
        if (action == 0) {
            isDownAction = true;
        } else {
            isDownAction = false;
        }
        if (this.mTouchAreaBindingEnabled && !isDownAction && (boundTouchArea = (touchAreaBindings = this.mTouchAreaBindings).get(pSceneTouchEvent.getPointerID())) != null) {
            float sceneTouchEventX = pSceneTouchEvent.getX();
            float sceneTouchEventY = pSceneTouchEvent.getY();
            switch (action) {
                case 1:
                case 3:
                    touchAreaBindings.remove(pSceneTouchEvent.getPointerID());
                    break;
            }
            Boolean handled5 = onAreaTouchEvent(pSceneTouchEvent, sceneTouchEventX, sceneTouchEventY, boundTouchArea);
            if (handled5 != null && handled5.booleanValue()) {
                return true;
            }
        }
        if (this.mChildScene != null) {
            if (onChildSceneTouchEvent(pSceneTouchEvent)) {
                return true;
            }
            if (this.mChildSceneModalTouch) {
                return false;
            }
        }
        float sceneTouchEventX2 = pSceneTouchEvent.getX();
        float sceneTouchEventY2 = pSceneTouchEvent.getY();
        int layerCount = this.mLayerCount;
        ILayer[] layers = this.mLayers;
        if (this.mOnAreaTouchTraversalBackToFront) {
            for (int i = 0; i < layerCount; i++) {
                ArrayList<ITouchArea> layerTouchAreas = layers[i].getTouchAreas();
                int layerTouchAreaCount = layerTouchAreas.size();
                if (layerTouchAreaCount > 0) {
                    int j = 0;
                    while (j < layerTouchAreaCount) {
                        ITouchArea layerTouchArea = layerTouchAreas.get(j);
                        if (!layerTouchArea.contains(sceneTouchEventX2, sceneTouchEventY2) || (handled4 = onAreaTouchEvent(pSceneTouchEvent, sceneTouchEventX2, sceneTouchEventY2, layerTouchArea)) == null || !handled4.booleanValue()) {
                            j++;
                        } else {
                            if (this.mTouchAreaBindingEnabled && isDownAction) {
                                this.mTouchAreaBindings.put(pSceneTouchEvent.getPointerID(), layerTouchArea);
                            }
                            return true;
                        }
                    }
                    continue;
                }
            }
        } else {
            for (int i2 = layerCount - 1; i2 >= 0; i2--) {
                ArrayList<ITouchArea> layerTouchAreas2 = layers[i2].getTouchAreas();
                int layerTouchAreaCount2 = layerTouchAreas2.size();
                if (layerTouchAreaCount2 > 0) {
                    int j2 = layerTouchAreaCount2 - 1;
                    while (j2 >= 0) {
                        ITouchArea layerTouchArea2 = layerTouchAreas2.get(j2);
                        if (!layerTouchArea2.contains(sceneTouchEventX2, sceneTouchEventY2) || (handled = onAreaTouchEvent(pSceneTouchEvent, sceneTouchEventX2, sceneTouchEventY2, layerTouchArea2)) == null || !handled.booleanValue()) {
                            j2--;
                        } else {
                            if (this.mTouchAreaBindingEnabled && isDownAction) {
                                this.mTouchAreaBindings.put(pSceneTouchEvent.getPointerID(), layerTouchArea2);
                            }
                            return true;
                        }
                    }
                    continue;
                }
            }
        }
        ArrayList<ITouchArea> touchAreas = this.mTouchAreas;
        int touchAreaCount = touchAreas.size();
        if (touchAreaCount > 0) {
            if (this.mOnAreaTouchTraversalBackToFront) {
                int i3 = 0;
                while (i3 < touchAreaCount) {
                    ITouchArea touchArea = touchAreas.get(i3);
                    if (!touchArea.contains(sceneTouchEventX2, sceneTouchEventY2) || (handled3 = onAreaTouchEvent(pSceneTouchEvent, sceneTouchEventX2, sceneTouchEventY2, touchArea)) == null || !handled3.booleanValue()) {
                        i3++;
                    } else {
                        if (this.mTouchAreaBindingEnabled && isDownAction) {
                            this.mTouchAreaBindings.put(pSceneTouchEvent.getPointerID(), touchArea);
                        }
                        return true;
                    }
                }
            } else {
                int i4 = touchAreaCount - 1;
                while (i4 >= 0) {
                    ITouchArea touchArea2 = touchAreas.get(i4);
                    if (!touchArea2.contains(sceneTouchEventX2, sceneTouchEventY2) || (handled2 = onAreaTouchEvent(pSceneTouchEvent, sceneTouchEventX2, sceneTouchEventY2, touchArea2)) == null || !handled2.booleanValue()) {
                        i4--;
                    } else {
                        if (this.mTouchAreaBindingEnabled && isDownAction) {
                            this.mTouchAreaBindings.put(pSceneTouchEvent.getPointerID(), touchArea2);
                        }
                        return true;
                    }
                }
            }
        }
        if (this.mOnSceneTouchListener != null) {
            return this.mOnSceneTouchListener.onSceneTouchEvent(this, pSceneTouchEvent);
        }
        return false;
    }

    private Boolean onAreaTouchEvent(TouchEvent pSceneTouchEvent, float sceneTouchEventX, float sceneTouchEventY, ITouchArea touchArea) {
        float[] touchAreaLocalCoordinates = touchArea.convertSceneToLocalCoordinates(sceneTouchEventX, sceneTouchEventY);
        float touchAreaLocalX = touchAreaLocalCoordinates[0];
        float touchAreaLocalY = touchAreaLocalCoordinates[1];
        if (touchArea.onAreaTouched(pSceneTouchEvent, touchAreaLocalX, touchAreaLocalY)) {
            return Boolean.TRUE;
        }
        if (this.mOnAreaTouchListener != null) {
            return Boolean.valueOf(this.mOnAreaTouchListener.onAreaTouched(pSceneTouchEvent, touchArea, touchAreaLocalX, touchAreaLocalY));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean onChildSceneTouchEvent(TouchEvent pSceneTouchEvent) {
        return this.mChildScene.onSceneTouchEvent(pSceneTouchEvent);
    }

    public void reset() {
        super.reset();
        clearChildScene();
        ILayer[] layers = this.mLayers;
        for (int i = this.mLayerCount - 1; i >= 0; i--) {
            layers[i].reset();
        }
    }

    public void postRunnable(Runnable pRunnable) {
        this.mRunnableHandler.postRunnable(pRunnable);
    }

    public void back() {
        clearChildScene();
        if (this.mParentScene != null) {
            this.mParentScene.clearChildScene();
            this.mParentScene = null;
        }
    }

    private void createLayers() {
        ILayer[] layers = this.mLayers;
        for (int i = this.mLayerCount - 1; i >= 0; i--) {
            layers[i] = new DynamicCapacityLayer();
        }
    }

    private void createLayers(boolean pFixedCapacityLayers, int[] pLayerCapacities) {
        ILayer[] layers = this.mLayers;
        if (pFixedCapacityLayers) {
            for (int i = this.mLayerCount - 1; i >= 0; i--) {
                layers[i] = new FixedCapacityLayer(pLayerCapacities[i]);
            }
            return;
        }
        for (int i2 = this.mLayerCount - 1; i2 >= 0; i2--) {
            layers[i2] = new DynamicCapacityLayer(pLayerCapacities[i2]);
        }
    }

    private void updateLayers(float pSecondsElapsed) {
        ILayer[] layers = this.mLayers;
        int layerCount = this.mLayerCount;
        for (int i = 0; i < layerCount; i++) {
            layers[i].onUpdate(pSecondsElapsed);
        }
    }

    private void drawLayers(GL10 pGL, Camera pCamera) {
        ILayer[] layers = this.mLayers;
        int layerCount = this.mLayerCount;
        for (int i = 0; i < layerCount; i++) {
            layers[i].onDraw(pGL, pCamera);
        }
    }

    private void updateUpdateHandlers(float pSecondsElapsed) {
        if (this.mChildScene == null || !this.mChildSceneModalUpdate) {
            this.mUpdateHandlers.onUpdate(pSecondsElapsed);
        }
        if (this.mChildScene != null) {
            this.mChildScene.updateUpdateHandlers(pSecondsElapsed);
        }
    }
}
