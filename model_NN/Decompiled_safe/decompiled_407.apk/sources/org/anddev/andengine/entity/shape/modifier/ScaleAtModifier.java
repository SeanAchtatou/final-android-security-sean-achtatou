package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;

public class ScaleAtModifier extends ScaleModifier {
    private final float mScaleCenterX;
    private final float mScaleCenterY;

    public ScaleAtModifier(float pDuration, float pFromScale, float pToScale, float pScaleCenterX, float pScaleCenterY) {
        this(pDuration, pFromScale, pToScale, pScaleCenterX, pScaleCenterY, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float pDuration, float pFromScale, float pToScale, float pScaleCenterX, float pScaleCenterY, IEaseFunction pEaseFunction) {
        this(pDuration, pFromScale, pToScale, pScaleCenterX, pScaleCenterY, (IShapeModifier.IShapeModifierListener) null, pEaseFunction);
    }

    public ScaleAtModifier(float pDuration, float pFromScale, float pToScale, float pScaleCenterX, float pScaleCenterY, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        this(pDuration, pFromScale, pToScale, pScaleCenterX, pScaleCenterY, pShapeModiferListener, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float pDuration, float pFromScale, float pToScale, float pScaleCenterX, float pScaleCenterY, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        this(pDuration, pFromScale, pToScale, pFromScale, pToScale, pScaleCenterX, pScaleCenterY, pShapeModiferListener, pEaseFunction);
    }

    public ScaleAtModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, float pScaleCenterX, float pScaleCenterY) {
        this(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pScaleCenterX, pScaleCenterY, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, float pScaleCenterX, float pScaleCenterY, IEaseFunction pEaseFunction) {
        this(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pScaleCenterX, pScaleCenterY, null, pEaseFunction);
    }

    public ScaleAtModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, float pScaleCenterX, float pScaleCenterY, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        this(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pScaleCenterX, pScaleCenterY, pShapeModiferListener, IEaseFunction.DEFAULT);
    }

    public ScaleAtModifier(float pDuration, float pFromScaleX, float pToScaleX, float pFromScaleY, float pToScaleY, float pScaleCenterX, float pScaleCenterY, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromScaleX, pToScaleX, pFromScaleY, pToScaleY, pShapeModiferListener, pEaseFunction);
        this.mScaleCenterX = pScaleCenterX;
        this.mScaleCenterY = pScaleCenterY;
    }

    protected ScaleAtModifier(ScaleAtModifier pScaleAtModifier) {
        super(pScaleAtModifier);
        this.mScaleCenterX = pScaleAtModifier.mScaleCenterX;
        this.mScaleCenterY = pScaleAtModifier.mScaleCenterY;
    }

    public ScaleAtModifier clone() {
        return new ScaleAtModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onManagedInitialize(IShape pShape) {
        super.onManagedInitialize((Object) pShape);
        pShape.setScaleCenter(this.mScaleCenterX, this.mScaleCenterY);
    }
}
