package org.anddev.andengine.entity.shape.modifier.ease;

import android.util.FloatMath;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.util.constants.MathConstants;

public class EaseElasticOut implements IEaseFunction, MathConstants {
    private static EaseElasticOut INSTANCE;

    private EaseElasticOut() {
    }

    public static EaseElasticOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseElasticOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float s;
        float p = Const.MOVE_LIMITED_SPEED;
        float a = Const.MOVE_LIMITED_SPEED;
        if (pSecondsElapsed == Const.MOVE_LIMITED_SPEED) {
            return pMinValue;
        }
        float pSecondsElapsed2 = pSecondsElapsed / pDuration;
        if (pSecondsElapsed2 == 1.0f) {
            return pMinValue + pMaxValue;
        }
        if (Const.MOVE_LIMITED_SPEED == Const.MOVE_LIMITED_SPEED) {
            p = pDuration * 0.3f;
        }
        if (Const.MOVE_LIMITED_SPEED == Const.MOVE_LIMITED_SPEED || ((pMaxValue > Const.MOVE_LIMITED_SPEED && Const.MOVE_LIMITED_SPEED < pMaxValue) || (pMaxValue < Const.MOVE_LIMITED_SPEED && Const.MOVE_LIMITED_SPEED < (-pMaxValue)))) {
            a = pMaxValue;
            s = p / 4.0f;
        } else {
            s = (float) (((double) (p / 6.2831855f)) * Math.asin((double) (pMaxValue / Const.MOVE_LIMITED_SPEED)));
        }
        return (float) ((((double) a) * Math.pow(2.0d, (double) (-10.0f * pSecondsElapsed2)) * ((double) FloatMath.sin((((pSecondsElapsed2 * pDuration) - s) * 6.2831855f) / p))) + ((double) pMaxValue) + ((double) pMinValue));
    }
}
