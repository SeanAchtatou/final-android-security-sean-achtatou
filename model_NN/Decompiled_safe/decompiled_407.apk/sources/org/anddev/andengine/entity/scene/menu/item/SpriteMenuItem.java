package org.anddev.andengine.entity.scene.menu.item;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class SpriteMenuItem extends Sprite implements IMenuItem {
    private final int mID;

    public SpriteMenuItem(int pID, TextureRegion pTextureRegion) {
        super(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, pTextureRegion);
        this.mID = pID;
    }

    public int getID() {
        return this.mID;
    }

    public void onSelected() {
    }

    public void onUnselected() {
    }
}
