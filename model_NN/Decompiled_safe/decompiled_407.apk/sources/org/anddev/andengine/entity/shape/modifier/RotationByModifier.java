package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;

public class RotationByModifier extends SingleValueChangeShapeModifier {
    public RotationByModifier(float pDuration, float pRotation) {
        super(pDuration, pRotation);
    }

    protected RotationByModifier(RotationByModifier pRotationByModifier) {
        super(pRotationByModifier);
    }

    public RotationByModifier clone() {
        return new RotationByModifier(this);
    }

    /* access modifiers changed from: protected */
    public void onChangeValue(IShape pShape, float pValue) {
        pShape.setRotation(pShape.getRotation() + pValue);
    }
}
