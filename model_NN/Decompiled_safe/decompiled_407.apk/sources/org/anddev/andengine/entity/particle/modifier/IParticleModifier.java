package org.anddev.andengine.entity.particle.modifier;

import org.anddev.andengine.entity.particle.Particle;

public interface IParticleModifier extends IParticleInitializer {
    void onUpdateParticle(Particle particle);
}
