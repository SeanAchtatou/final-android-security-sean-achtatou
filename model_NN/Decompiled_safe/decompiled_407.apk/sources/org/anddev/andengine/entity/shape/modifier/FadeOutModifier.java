package org.anddev.andengine.entity.shape.modifier;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;

public class FadeOutModifier extends AlphaModifier {
    public FadeOutModifier(float pDuration) {
        super(pDuration, 1.0f, (float) Const.MOVE_LIMITED_SPEED, IEaseFunction.DEFAULT);
    }

    public FadeOutModifier(float pDuration, IEaseFunction pEaseFunction) {
        super(pDuration, 1.0f, (float) Const.MOVE_LIMITED_SPEED, pEaseFunction);
    }

    public FadeOutModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModiferListener) {
        super(pDuration, 1.0f, Const.MOVE_LIMITED_SPEED, pShapeModiferListener, IEaseFunction.DEFAULT);
    }

    public FadeOutModifier(float pDuration, IShapeModifier.IShapeModifierListener pShapeModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, 1.0f, Const.MOVE_LIMITED_SPEED, pShapeModiferListener, pEaseFunction);
    }

    protected FadeOutModifier(FadeOutModifier pFadeOutModifier) {
        super(pFadeOutModifier);
    }

    public FadeOutModifier clone() {
        return new FadeOutModifier(this);
    }
}
