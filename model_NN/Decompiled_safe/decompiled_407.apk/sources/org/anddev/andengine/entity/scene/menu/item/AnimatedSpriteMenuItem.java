package org.anddev.andengine.entity.scene.menu.item;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class AnimatedSpriteMenuItem extends AnimatedSprite implements IMenuItem {
    private final int mID;

    public AnimatedSpriteMenuItem(int pID, TiledTextureRegion pTiledTextureRegion) {
        super(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, pTiledTextureRegion);
        this.mID = pID;
    }

    public int getID() {
        return this.mID;
    }

    public void onSelected() {
    }

    public void onUnselected() {
    }
}
