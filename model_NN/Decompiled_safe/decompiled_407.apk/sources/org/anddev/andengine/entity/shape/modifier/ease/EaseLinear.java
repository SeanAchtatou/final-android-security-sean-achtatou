package org.anddev.andengine.entity.shape.modifier.ease;

public class EaseLinear implements IEaseFunction {
    private static EaseLinear INSTANCE;

    private EaseLinear() {
    }

    public static EaseLinear getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseLinear();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        return ((pMaxValue * pSecondsElapsed) / pDuration) + pMinValue;
    }
}
