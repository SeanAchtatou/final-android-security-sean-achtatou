package org.anddev.andengine.entity.shape.modifier.ease;

import com.finger2finger.games.res.Const;

public class EaseBounceIn implements IEaseFunction {
    private static EaseBounceIn INSTANCE;

    private EaseBounceIn() {
    }

    public static EaseBounceIn getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceIn();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        return (pMaxValue - EaseBounceOut.getInstance().getPercentageDone(pDuration - pSecondsElapsed, pDuration, Const.MOVE_LIMITED_SPEED, pMaxValue)) + pMinValue;
    }
}
