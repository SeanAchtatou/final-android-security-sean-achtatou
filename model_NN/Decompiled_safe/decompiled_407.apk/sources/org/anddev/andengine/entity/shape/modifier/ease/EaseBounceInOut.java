package org.anddev.andengine.entity.shape.modifier.ease;

import com.finger2finger.games.res.Const;

public class EaseBounceInOut implements IEaseFunction {
    private static EaseBounceInOut INSTANCE;

    private EaseBounceInOut() {
    }

    public static EaseBounceInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseBounceInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        if (((double) pSecondsElapsed) < ((double) pDuration) * 0.5d) {
            return (EaseBounceIn.getInstance().getPercentageDone(pSecondsElapsed * 2.0f, pDuration, Const.MOVE_LIMITED_SPEED, pMaxValue) * 0.5f) + pMinValue;
        }
        return (EaseBounceOut.getInstance().getPercentageDone((pSecondsElapsed * 2.0f) - pDuration, pDuration, Const.MOVE_LIMITED_SPEED, pMaxValue) * 0.5f) + (pMaxValue * 0.5f) + pMinValue;
    }
}
