package org.anddev.andengine.entity.shape.modifier.ease;

import android.util.FloatMath;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.util.constants.MathConstants;

public class EaseElasticInOut implements IEaseFunction, MathConstants {
    private static EaseElasticInOut INSTANCE;

    private EaseElasticInOut() {
    }

    public static EaseElasticInOut getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EaseElasticInOut();
        }
        return INSTANCE;
    }

    public float getPercentageDone(float pSecondsElapsed, float pDuration, float pMinValue, float pMaxValue) {
        float s;
        float p = Const.MOVE_LIMITED_SPEED;
        float a = Const.MOVE_LIMITED_SPEED;
        if (pSecondsElapsed == Const.MOVE_LIMITED_SPEED) {
            return pMinValue;
        }
        float pSecondsElapsed2 = (float) (((double) pSecondsElapsed) / (((double) pDuration) * 0.5d));
        if (pSecondsElapsed2 == 2.0f) {
            return pMinValue + pMaxValue;
        }
        if (Const.MOVE_LIMITED_SPEED == Const.MOVE_LIMITED_SPEED) {
            p = pDuration * 0.45000002f;
        }
        if (Const.MOVE_LIMITED_SPEED == Const.MOVE_LIMITED_SPEED || ((pMaxValue > Const.MOVE_LIMITED_SPEED && Const.MOVE_LIMITED_SPEED < pMaxValue) || (pMaxValue < Const.MOVE_LIMITED_SPEED && Const.MOVE_LIMITED_SPEED < (-pMaxValue)))) {
            a = pMaxValue;
            s = p / 4.0f;
        } else {
            s = (float) (((double) (p / 6.2831855f)) * Math.asin((double) (pMaxValue / Const.MOVE_LIMITED_SPEED)));
        }
        if (pSecondsElapsed2 < 1.0f) {
            float pSecondsElapsed3 = pSecondsElapsed2 - 1.0f;
            return (float) ((-0.5d * ((double) a) * Math.pow(2.0d, (double) (10.0f * pSecondsElapsed3)) * ((double) FloatMath.sin((((pSecondsElapsed3 * pDuration) - s) * 6.2831855f) / p))) + ((double) pMinValue));
        }
        float pSecondsElapsed4 = pSecondsElapsed2 - 1.0f;
        return (float) ((((double) a) * Math.pow(2.0d, (double) (-10.0f * pSecondsElapsed4)) * ((double) FloatMath.sin((((pSecondsElapsed4 * pDuration) - s) * 6.2831855f) / p)) * 0.5d) + ((double) pMaxValue) + ((double) pMinValue));
    }
}
