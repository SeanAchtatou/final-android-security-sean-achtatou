package org.anddev.andengine.entity.shape.modifier;

import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.util.modifier.SequenceModifier;

public class SequenceShapeModifier extends SequenceModifier<IShape> implements IShapeModifier {

    public interface ISubSequenceShapeModifierListener extends SequenceModifier.ISubSequenceModifierListener<IShape> {
    }

    public SequenceShapeModifier(IShapeModifier... pShapeModifiers) throws IllegalArgumentException {
        super(pShapeModifiers);
    }

    public SequenceShapeModifier(IShapeModifier.IShapeModifierListener pShapeModiferListener, IShapeModifier... pShapeModifiers) throws IllegalArgumentException {
        super(pShapeModiferListener, pShapeModifiers);
    }

    public SequenceShapeModifier(IShapeModifier.IShapeModifierListener pShapeModiferListener, ISubSequenceShapeModifierListener pSubSequenceShapeModifierListener, IShapeModifier... pShapeModifiers) throws IllegalArgumentException {
        super(pShapeModiferListener, pSubSequenceShapeModifierListener, pShapeModifiers);
    }

    protected SequenceShapeModifier(SequenceShapeModifier pSequenceShapeModifier) {
        super(pSequenceShapeModifier);
    }

    public SequenceShapeModifier clone() {
        return new SequenceShapeModifier(this);
    }
}
