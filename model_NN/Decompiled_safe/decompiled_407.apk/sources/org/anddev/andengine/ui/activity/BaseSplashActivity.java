package org.anddev.andengine.ui.activity;

import android.app.Activity;
import android.content.Intent;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.engine.handler.timer.ITimerCallback;
import org.anddev.andengine.engine.handler.timer.TimerHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.IResolutionPolicy;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.scene.SplashScene;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureFactory;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public abstract class BaseSplashActivity extends BaseGameActivity {
    private Camera mCamera;
    private TextureRegion mLoadingScreenTextureRegion;
    private ITextureSource mSplashTextureSource;

    /* access modifiers changed from: protected */
    public abstract Class<? extends Activity> getFollowUpActivity();

    /* access modifiers changed from: protected */
    public abstract EngineOptions.ScreenOrientation getScreenOrientation();

    /* access modifiers changed from: protected */
    public abstract float getSplashDuration();

    /* access modifiers changed from: protected */
    public abstract ITextureSource onGetSplashTextureSource();

    /* access modifiers changed from: protected */
    public float getSplashScaleFrom() {
        return 1.0f;
    }

    /* access modifiers changed from: protected */
    public float getSplashScaleTo() {
        return 1.0f;
    }

    public void onLoadComplete() {
    }

    public Engine onLoadEngine() {
        this.mSplashTextureSource = onGetSplashTextureSource();
        int width = this.mSplashTextureSource.getWidth();
        int height = this.mSplashTextureSource.getHeight();
        this.mCamera = getSplashCamera(width, height);
        return new Engine(new EngineOptions(true, getScreenOrientation(), getSplashResolutionPolicy(width, height), this.mCamera));
    }

    public void onLoadResources() {
        Texture loadingScreenTexture = TextureFactory.createForTextureSourceSize(this.mSplashTextureSource, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mLoadingScreenTextureRegion = TextureRegionFactory.createFromSource(loadingScreenTexture, this.mSplashTextureSource, 0, 0);
        getEngine().getTextureManager().loadTexture(loadingScreenTexture);
    }

    public Scene onLoadScene() {
        float splashDuration = getSplashDuration();
        SplashScene splashScene = new SplashScene(this.mCamera, this.mLoadingScreenTextureRegion, splashDuration, getSplashScaleFrom(), getSplashScaleTo());
        splashScene.registerUpdateHandler(new TimerHandler(splashDuration, new ITimerCallback() {
            public void onTimePassed(TimerHandler pTimerHandler) {
                BaseSplashActivity.this.startActivity(new Intent(BaseSplashActivity.this, BaseSplashActivity.this.getFollowUpActivity()));
                BaseSplashActivity.this.finish();
            }
        }));
        return splashScene;
    }

    /* access modifiers changed from: protected */
    public Camera getSplashCamera(int pSplashwidth, int pSplashHeight) {
        return new Camera(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) pSplashwidth, (float) pSplashHeight);
    }

    /* access modifiers changed from: protected */
    public IResolutionPolicy getSplashResolutionPolicy(int pSplashwidth, int pSplashHeight) {
        return new RatioResolutionPolicy((float) pSplashwidth, (float) pSplashHeight);
    }
}
