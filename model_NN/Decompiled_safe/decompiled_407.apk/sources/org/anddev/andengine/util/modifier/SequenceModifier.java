package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;
import org.anddev.andengine.util.modifier.util.ModifierUtils;

public class SequenceModifier<T> extends BaseModifier<T> {
    private int mCurrentSubSequenceModifier;
    private final float mDuration;
    private ISubSequenceModifierListener<T> mSubSequenceModiferListener;
    private final IModifier<T>[] mSubSequenceModifiers;

    public interface ISubSequenceModifierListener<T> {
        void onSubSequenceFinished(IModifier<T> iModifier, T t, int i);
    }

    public SequenceModifier(IModifier... pModifiers) throws IllegalArgumentException {
        this(null, pModifiers);
    }

    public SequenceModifier(IModifier.IModifierListener<T> pModiferListener, IModifier<T>... pModifiers) throws IllegalArgumentException {
        this(pModiferListener, null, pModifiers);
    }

    public SequenceModifier(IModifier.IModifierListener<T> pModiferListener, ISubSequenceModifierListener<T> pSubSequenceModifierListener, IModifier<T>... pModifiers) throws IllegalArgumentException {
        super(pModiferListener);
        if (pModifiers.length == 0) {
            throw new IllegalArgumentException("pModifiers must not be empty!");
        }
        this.mSubSequenceModiferListener = pSubSequenceModifierListener;
        this.mSubSequenceModifiers = pModifiers;
        this.mDuration = ModifierUtils.getSequenceDurationOfModifier(pModifiers);
        pModifiers[0].setModifierListener(new InternalModifierListener(this, null));
    }

    protected SequenceModifier(SequenceModifier sequenceModifier) {
        super(sequenceModifier.mModifierListener);
        this.mSubSequenceModiferListener = sequenceModifier.mSubSequenceModiferListener;
        this.mDuration = sequenceModifier.mDuration;
        IModifier[] otherModifiers = sequenceModifier.mSubSequenceModifiers;
        this.mSubSequenceModifiers = new IModifier[otherModifiers.length];
        IModifier[] shapeModifiers = this.mSubSequenceModifiers;
        for (int i = shapeModifiers.length - 1; i >= 0; i--) {
            shapeModifiers[i] = otherModifiers[i].clone();
        }
        shapeModifiers[0].setModifierListener(new InternalModifierListener(this, null));
    }

    public SequenceModifier<T> clone() {
        return new SequenceModifier<>(this);
    }

    public ISubSequenceModifierListener<T> getSubSequenceModiferListener() {
        return this.mSubSequenceModiferListener;
    }

    public void setSubSequenceModiferListener(ISubSequenceModifierListener<T> pSubSequenceModiferListener) {
        this.mSubSequenceModiferListener = pSubSequenceModiferListener;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public void onUpdate(float pSecondsElapsed, T pItem) {
        if (!this.mFinished) {
            this.mSubSequenceModifiers[this.mCurrentSubSequenceModifier].onUpdate(pSecondsElapsed, pItem);
        }
    }

    public void reset() {
        this.mCurrentSubSequenceModifier = 0;
        this.mFinished = false;
        IModifier[] shapeModifiers = this.mSubSequenceModifiers;
        for (int i = shapeModifiers.length - 1; i >= 0; i--) {
            shapeModifiers[i].reset();
        }
    }

    /* access modifiers changed from: private */
    public void onHandleModifierFinished(SequenceModifier<T>.InternalModifierListener pInternalModifierListener, IModifier<T> pModifier, T pItem) {
        this.mCurrentSubSequenceModifier++;
        if (this.mCurrentSubSequenceModifier < this.mSubSequenceModifiers.length) {
            this.mSubSequenceModifiers[this.mCurrentSubSequenceModifier].setModifierListener(pInternalModifierListener);
            if (this.mSubSequenceModiferListener != null) {
                this.mSubSequenceModiferListener.onSubSequenceFinished(pModifier, pItem, this.mCurrentSubSequenceModifier);
                return;
            }
            return;
        }
        this.mFinished = true;
        if (this.mModifierListener != null) {
            this.mModifierListener.onModifierFinished(this, pItem);
        }
    }

    private class InternalModifierListener implements IModifier.IModifierListener<T> {
        private InternalModifierListener() {
        }

        /* synthetic */ InternalModifierListener(SequenceModifier sequenceModifier, InternalModifierListener internalModifierListener) {
            this();
        }

        public void onModifierFinished(IModifier<T> pModifier, T pItem) {
            SequenceModifier.this.onHandleModifierFinished(this, pModifier, pItem);
        }
    }
}
