package org.anddev.andengine.util.modifier;

import org.anddev.andengine.util.modifier.IModifier;

public class LoopModifier<T> extends BaseModifier<T> {
    public static final int LOOP_CONTINUOUS = -1;
    private final float mDuration;
    private final int mInitialLoopCount;
    private int mLoopCount;
    private ILoopModifierListener<T> mLoopModifierListener;
    private final IModifier<T> mModifier;

    public interface ILoopModifierListener<T> {
        void onLoopFinished(LoopModifier<T> loopModifier, int i);
    }

    public LoopModifier(IModifier iModifier) {
        this(null, -1, iModifier);
    }

    public LoopModifier(int pLoopCount, IModifier<T> pModifier) {
        this(null, pLoopCount, pModifier);
    }

    public LoopModifier(IModifier.IModifierListener<T> pModiferListener, int pLoopCount, IModifier<T> pModifier) {
        this(pModiferListener, pLoopCount, null, pModifier);
    }

    public LoopModifier(IModifier.IModifierListener<T> pModiferListener, int pLoopCount, ILoopModifierListener<T> pLoopModifierListener, IModifier<T> pModifier) {
        super(pModiferListener);
        this.mLoopModifierListener = pLoopModifierListener;
        this.mModifier = pModifier;
        this.mInitialLoopCount = pLoopCount;
        this.mLoopCount = pLoopCount;
        this.mDuration = pLoopCount == -1 ? Float.POSITIVE_INFINITY : pModifier.getDuration() * ((float) pLoopCount);
        pModifier.setModifierListener(new InternalModifierListener(this, null));
    }

    protected LoopModifier(LoopModifier loopModifier) {
        this(loopModifier.mModifierListener, loopModifier.mInitialLoopCount, loopModifier.mModifier.clone());
    }

    public LoopModifier<T> clone() {
        return new LoopModifier<>(this);
    }

    public ILoopModifierListener<T> getLoopModifierListener() {
        return this.mLoopModifierListener;
    }

    public void setLoopModifierListener(ILoopModifierListener<T> pLoopModifierListener) {
        this.mLoopModifierListener = pLoopModifierListener;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public void onUpdate(float pSecondsElapsed, T pItem) {
        if (!this.mFinished) {
            this.mModifier.onUpdate(pSecondsElapsed, pItem);
        }
    }

    public void reset() {
        this.mLoopCount = this.mInitialLoopCount;
        this.mModifier.reset();
    }

    public void onHandleLoopFinished(T pItem) {
        if (this.mLoopModifierListener != null) {
            this.mLoopModifierListener.onLoopFinished(this, this.mLoopCount);
        }
        if (this.mInitialLoopCount == -1) {
            this.mModifier.reset();
            return;
        }
        this.mLoopCount--;
        if (this.mLoopCount < 0) {
            this.mFinished = true;
            if (this.mModifierListener != null) {
                this.mModifierListener.onModifierFinished(this, pItem);
                return;
            }
            return;
        }
        this.mModifier.reset();
    }

    private class InternalModifierListener implements IModifier.IModifierListener<T> {
        private InternalModifierListener() {
        }

        /* synthetic */ InternalModifierListener(LoopModifier loopModifier, InternalModifierListener internalModifierListener) {
            this();
        }

        public void onModifierFinished(IModifier<T> iModifier, T pItem) {
            LoopModifier.this.onHandleLoopFinished(pItem);
        }
    }
}
