package org.anddev.andengine.util.modifier;

import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;
import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseDoubleValueSpanModifier<T> extends BaseSingleValueSpanModifier<T> {
    private final float mFromValueB;
    private final float mValueSpanB;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValues(Object obj, float f, float f2);

    /* access modifiers changed from: protected */
    public abstract void onSetValues(Object obj, float f, float f2, float f3);

    public BaseDoubleValueSpanModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB) {
        this(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, null, IEaseFunction.DEFAULT);
    }

    public BaseDoubleValueSpanModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IEaseFunction pEaseFunction) {
        this(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, null, pEaseFunction);
    }

    public BaseDoubleValueSpanModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IModifier.IModifierListener iModifierListener) {
        this(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, iModifierListener, IEaseFunction.DEFAULT);
    }

    public BaseDoubleValueSpanModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, IModifier.IModifierListener<T> pModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pModiferListener, pEaseFunction);
        this.mFromValueB = pFromValueB;
        this.mValueSpanB = pToValueB - pFromValueB;
    }

    protected BaseDoubleValueSpanModifier(BaseDoubleValueSpanModifier<T> pBaseDoubleValueSpanModifier) {
        super(pBaseDoubleValueSpanModifier);
        this.mFromValueB = pBaseDoubleValueSpanModifier.mFromValueB;
        this.mValueSpanB = pBaseDoubleValueSpanModifier.mValueSpanB;
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValue(T pItem, float pValueA) {
        onSetInitialValues(pItem, pValueA, this.mFromValueB);
    }

    /* access modifiers changed from: protected */
    public void onSetValue(T pItem, float pPercentageDone, float pValueA) {
        onSetValues(pItem, pPercentageDone, pValueA, this.mFromValueB + (this.mValueSpanB * pPercentageDone));
    }
}
