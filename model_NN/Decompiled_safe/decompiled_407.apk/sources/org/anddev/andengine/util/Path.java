package org.anddev.andengine.util;

import android.util.FloatMath;
import com.finger2finger.games.res.Const;

public class Path {
    private final float[] mCoordinatesX;
    private final float[] mCoordinatesY;
    private int mIndex;
    private float mLength;
    private boolean mLengthChanged = false;

    public Path(int pLength) {
        this.mCoordinatesX = new float[pLength];
        this.mCoordinatesY = new float[pLength];
        this.mIndex = 0;
        this.mLengthChanged = false;
    }

    public Path(float[] pCoordinatesX, float[] pCoordinatesY) throws IllegalArgumentException {
        if (pCoordinatesX.length != pCoordinatesY.length) {
            throw new IllegalArgumentException("Coordinate-Arrays must have the same length.");
        }
        this.mCoordinatesX = pCoordinatesX;
        this.mCoordinatesY = pCoordinatesY;
        this.mIndex = pCoordinatesX.length;
        this.mLengthChanged = true;
    }

    public Path(Path pPath) {
        int size = pPath.getSize();
        this.mCoordinatesX = new float[size];
        this.mCoordinatesY = new float[size];
        System.arraycopy(pPath.mCoordinatesX, 0, this.mCoordinatesX, 0, size);
        System.arraycopy(pPath.mCoordinatesY, 0, this.mCoordinatesY, 0, size);
        this.mIndex = pPath.mIndex;
        this.mLengthChanged = pPath.mLengthChanged;
        this.mLength = pPath.mLength;
    }

    public Path clone() {
        return new Path(this);
    }

    public Path to(float pX, float pY) {
        this.mCoordinatesX[this.mIndex] = pX;
        this.mCoordinatesY[this.mIndex] = pY;
        this.mIndex++;
        this.mLengthChanged = true;
        return this;
    }

    public float[] getCoordinatesX() {
        return this.mCoordinatesX;
    }

    public float[] getCoordinatesY() {
        return this.mCoordinatesY;
    }

    public int getSize() {
        return this.mCoordinatesX.length;
    }

    public float getLength() {
        if (this.mLengthChanged) {
            updateLength();
        }
        return this.mLength;
    }

    public float getSegmentLength(int pSegmentIndex) {
        float[] coordinatesX = this.mCoordinatesX;
        float[] coordinatesY = this.mCoordinatesY;
        int nextSegmentIndex = pSegmentIndex + 1;
        float dx = coordinatesX[pSegmentIndex] - coordinatesX[nextSegmentIndex];
        float dy = coordinatesY[pSegmentIndex] - coordinatesY[nextSegmentIndex];
        return FloatMath.sqrt((dx * dx) + (dy * dy));
    }

    private void updateLength() {
        float length = Const.MOVE_LIMITED_SPEED;
        for (int i = this.mIndex - 2; i >= 0; i--) {
            length += getSegmentLength(i);
        }
        this.mLength = length;
    }
}
