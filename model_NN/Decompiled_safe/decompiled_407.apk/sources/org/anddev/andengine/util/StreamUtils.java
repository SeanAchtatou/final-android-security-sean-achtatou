package org.anddev.andengine.util;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Scanner;

public class StreamUtils {
    public static final int IO_BUFFER_SIZE = 8192;

    public static final String readFully(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
        Scanner sc = new Scanner(in);
        while (sc.hasNextLine()) {
            sb.append(sc.nextLine());
        }
        return sb.toString();
    }

    public static byte[] streamToBytes(InputStream in) throws IOException {
        return streamToBytes(in, -1);
    }

    public static byte[] streamToBytes(InputStream in, int pReadLimit) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream(Math.min(pReadLimit, (int) IO_BUFFER_SIZE));
        copy(in, os, (long) pReadLimit);
        return os.toByteArray();
    }

    public static void copy(InputStream in, OutputStream out) throws IOException {
        copy(in, out, -1);
    }

    public static boolean copyAndClose(InputStream in, OutputStream out) {
        try {
            copy(in, out, -1);
            closeStream(in);
            closeStream(out);
            return true;
        } catch (IOException e) {
            closeStream(in);
            closeStream(out);
            return false;
        } catch (Throwable th) {
            closeStream(in);
            closeStream(out);
            throw th;
        }
    }

    public static void copy(InputStream in, OutputStream out, long pByteLimit) throws IOException {
        byte[] b = new byte[IO_BUFFER_SIZE];
        long pBytesLeftToRead = pByteLimit;
        if (pByteLimit >= 0) {
            while (true) {
                int read = in.read(b);
                if (read != -1) {
                    if (pBytesLeftToRead <= ((long) read)) {
                        out.write(b, 0, (int) pBytesLeftToRead);
                        break;
                    } else {
                        out.write(b, 0, read);
                        pBytesLeftToRead -= (long) read;
                    }
                } else {
                    break;
                }
            }
        } else {
            while (true) {
                int read2 = in.read(b);
                if (read2 == -1) {
                    break;
                }
                out.write(b, 0, read2);
            }
        }
        out.flush();
    }

    public static void closeStream(Closeable pStream) {
        if (pStream != null) {
            try {
                pStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void flushCloseStream(OutputStream pStream) {
        if (pStream != null) {
            try {
                pStream.flush();
                closeStream(pStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void flushCloseWriter(Writer pWriter) {
        if (pWriter != null) {
            try {
                pWriter.flush();
                closeStream(pWriter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
