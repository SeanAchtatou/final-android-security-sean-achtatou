package org.anddev.andengine.util.modifier;

import org.anddev.andengine.entity.shape.modifier.ease.IEaseFunction;
import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseTripleValueSpanModifier<T> extends BaseDoubleValueSpanModifier<T> {
    private final float mFromValueC;
    private final float mValueSpanC;

    /* access modifiers changed from: protected */
    public abstract void onSetInitialValues(Object obj, float f, float f2, float f3);

    /* access modifiers changed from: protected */
    public abstract void onSetValues(Object obj, float f, float f2, float f3, float f4);

    public BaseTripleValueSpanModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, float pFromValueC, float pToValueC, IEaseFunction pEaseFunction) {
        this(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pFromValueC, pToValueC, null, pEaseFunction);
    }

    public BaseTripleValueSpanModifier(float pDuration, float pFromValueA, float pToValueA, float pFromValueB, float pToValueB, float pFromValueC, float pToValueC, IModifier.IModifierListener<T> pModiferListener, IEaseFunction pEaseFunction) {
        super(pDuration, pFromValueA, pToValueA, pFromValueB, pToValueB, pModiferListener, pEaseFunction);
        this.mFromValueC = pFromValueC;
        this.mValueSpanC = pToValueC - pFromValueC;
    }

    protected BaseTripleValueSpanModifier(BaseTripleValueSpanModifier<T> pBaseTripleValueSpanModifier) {
        super(pBaseTripleValueSpanModifier);
        this.mFromValueC = pBaseTripleValueSpanModifier.mFromValueC;
        this.mValueSpanC = pBaseTripleValueSpanModifier.mValueSpanC;
    }

    /* access modifiers changed from: protected */
    public void onSetInitialValues(T pItem, float pValueA, float pValueB) {
        onSetInitialValues(pItem, pValueA, pValueB, this.mFromValueC);
    }

    /* access modifiers changed from: protected */
    public void onSetValues(T pItem, float pPercentageDone, float pValueA, float pValueB) {
        onSetValues(pItem, pPercentageDone, pValueA, pValueB, this.mFromValueC + (this.mValueSpanC * pPercentageDone));
    }
}
