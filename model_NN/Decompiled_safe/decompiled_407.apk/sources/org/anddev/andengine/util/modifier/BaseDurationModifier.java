package org.anddev.andengine.util.modifier;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.util.modifier.IModifier;

public abstract class BaseDurationModifier<T> extends BaseModifier<T> {
    protected final float mDuration;
    private float mTotalSecondsElapsed;

    /* access modifiers changed from: protected */
    public abstract void onManagedInitialize(Object obj);

    /* access modifiers changed from: protected */
    public abstract void onManagedUpdate(float f, Object obj);

    public BaseDurationModifier() {
        this(-1.0f, null);
    }

    public BaseDurationModifier(float pDuration) {
        this(pDuration, null);
    }

    public BaseDurationModifier(float pDuration, IModifier.IModifierListener<T> pModiferListener) {
        super(pModiferListener);
        this.mDuration = pDuration;
    }

    protected BaseDurationModifier(BaseDurationModifier baseDurationModifier) {
        this(baseDurationModifier.mDuration, baseDurationModifier.mModifierListener);
    }

    /* access modifiers changed from: protected */
    public float getTotalSecondsElapsed() {
        return this.mTotalSecondsElapsed;
    }

    public float getDuration() {
        return this.mDuration;
    }

    public final void onUpdate(float pSecondsElapsed, T pItem) {
        float secondsToElapse;
        if (!this.mFinished) {
            if (this.mTotalSecondsElapsed == Const.MOVE_LIMITED_SPEED) {
                onManagedInitialize(pItem);
            }
            if (this.mTotalSecondsElapsed + pSecondsElapsed < this.mDuration) {
                secondsToElapse = pSecondsElapsed;
            } else {
                secondsToElapse = this.mDuration - this.mTotalSecondsElapsed;
            }
            this.mTotalSecondsElapsed += secondsToElapse;
            onManagedUpdate(secondsToElapse, pItem);
            if (this.mDuration != -1.0f && this.mTotalSecondsElapsed >= this.mDuration) {
                this.mTotalSecondsElapsed = this.mDuration;
                this.mFinished = true;
                if (this.mModifierListener != null) {
                    this.mModifierListener.onModifierFinished(this, pItem);
                }
            }
        }
    }

    public void reset() {
        this.mFinished = false;
        this.mTotalSecondsElapsed = Const.MOVE_LIMITED_SPEED;
    }
}
