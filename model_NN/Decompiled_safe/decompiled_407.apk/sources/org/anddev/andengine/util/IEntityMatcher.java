package org.anddev.andengine.util;

import org.anddev.andengine.entity.IEntity;

public interface IEntityMatcher {
    boolean matches(IEntity iEntity);
}
