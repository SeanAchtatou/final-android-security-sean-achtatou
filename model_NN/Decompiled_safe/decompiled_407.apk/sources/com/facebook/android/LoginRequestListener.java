package com.facebook.android;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.facebook.android.Facebook;

public class LoginRequestListener implements Facebook.DialogListener {
    /* access modifiers changed from: private */
    public Context mContext;
    private boolean mEnableShowMsg = false;
    private String mErrorMsg = "";

    public LoginRequestListener(Context pContext, boolean pEnableShowMsg, String pErrorHeadMsg) {
        this.mContext = pContext;
        this.mEnableShowMsg = pEnableShowMsg;
        this.mErrorMsg = pErrorHeadMsg;
    }

    public void showMsg(final String pErrorMsg) {
        if (this.mEnableShowMsg && this.mContext != null && pErrorMsg != null && !pErrorMsg.equals("")) {
            ((Activity) this.mContext).runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(LoginRequestListener.this.mContext, pErrorMsg, 1).show();
                }
            });
        }
    }

    public void onCancel() {
    }

    public void onComplete(Bundle values) {
        Log.e("f2f", "Login onComplete");
    }

    public void onError(DialogError e) {
        Log.e("f2f", "Login onError:" + e.getMessage());
        showMsg(this.mErrorMsg + " net Error.");
    }

    public void onFacebookError(FacebookError e) {
        Log.e("f2f", "Login onFacebookError:" + e.getMessage());
        showMsg(this.mErrorMsg + " Facebook Error.");
    }

    public String getMErrorMsg() {
        return this.mErrorMsg;
    }

    public void setMErrorMsg(String errorMsg) {
        this.mErrorMsg = errorMsg;
    }

    public boolean isMEnableShowMsg() {
        return this.mEnableShowMsg;
    }

    public void setMEnableShowMsg(boolean enableShowMsg) {
        this.mEnableShowMsg = enableShowMsg;
    }

    public Context getMContext() {
        return this.mContext;
    }

    public void setMContext(Context context) {
        this.mContext = context;
    }
}
