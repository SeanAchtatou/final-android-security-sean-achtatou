package com.facebook.android;

public class FriendEntity {
    private String fID;
    private String fIconUrl;
    private String fName;
    private String fSex;

    public String getFID() {
        return this.fID;
    }

    public void setFID(String fid) {
        this.fID = fid;
    }

    public String getFName() {
        return this.fName;
    }

    public void setFName(String name) {
        this.fName = name;
    }

    public String getFIconUrl() {
        return this.fIconUrl;
    }

    public void setFIconUrl(String iconUrl) {
        this.fIconUrl = iconUrl;
    }

    public String getFSex() {
        return this.fSex;
    }

    public void setFSex(String sex) {
        this.fSex = sex;
    }
}
