package com.facebook.android;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;
import com.facebook.android.AsyncFacebookRunner;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class AsyncRequestListener implements AsyncFacebookRunner.RequestListener {
    /* access modifiers changed from: private */
    public Context mContext;
    private boolean mEnableShowMsg = false;
    private String mErrorMsg = "";

    public abstract void onComplete(JSONObject jSONObject);

    public AsyncRequestListener(Context pContext, boolean pEnableShowMsg, String pErrorHeadMsg) {
        this.mContext = pContext;
        this.mEnableShowMsg = pEnableShowMsg;
        this.mErrorMsg = pErrorHeadMsg;
    }

    public void showMsg(final String pErrorMsg) {
        if (this.mEnableShowMsg && this.mContext != null && pErrorMsg != null && !pErrorMsg.equals("")) {
            ((Activity) this.mContext).runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(AsyncRequestListener.this.mContext, pErrorMsg, 1).show();
                }
            });
        }
    }

    public void onComplete(String response) {
        try {
            onComplete(Util.parseJson(response));
        } catch (JSONException e) {
            JSONException e2 = e;
            e2.printStackTrace();
            Log.e("f2f", "JSON Error:" + e2.getMessage());
        } catch (FacebookError e3) {
            Log.e("f2f", "Facebook Error:" + e3.getMessage());
        }
    }

    public void onFacebookError(FacebookError e, Object state) {
        Log.e("f2f", "Facebook Error:" + e.getMessage());
        showMsg(this.mErrorMsg + " Facebook Error");
    }

    public void onFileNotFoundException(FileNotFoundException e, Object state) {
        Log.e("f2f", "Resource not found:" + e.getMessage());
        showMsg(this.mErrorMsg + " Resource not found.");
    }

    public void onIOException(IOException e, Object state) {
        Log.e("f2f", "Network Error:" + e.getMessage());
        showMsg(this.mErrorMsg + " Network Error.");
    }

    public void onMalformedURLException(MalformedURLException e, Object state) {
        Log.e("f2f", " Invalid URL.");
        showMsg(this.mErrorMsg + "Invalid URL");
    }

    public void onComplete(String response, Object state) {
        Log.d("f2f", "onComplete:" + response.toString());
    }

    public String getMErrorMsg() {
        return this.mErrorMsg;
    }

    public void setMErrorMsg(String errorMsg) {
        this.mErrorMsg = errorMsg;
    }

    public boolean isMEnableShowMsg() {
        return this.mEnableShowMsg;
    }

    public void setMEnableShowMsg(boolean enableShowMsg) {
        this.mEnableShowMsg = enableShowMsg;
    }

    public Context getMContext() {
        return this.mContext;
    }

    public void setMContext(Context context) {
        this.mContext = context;
    }
}
