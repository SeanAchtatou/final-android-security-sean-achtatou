package com.millennialmedia.android;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;

public class MMAdViewOverlayActivity extends Activity implements AccelerometerListener {
    private static final String TAG = "MillennialMediaSDK";
    private static Activity context;
    private MMAdViewWebOverlay mmOverlay;
    protected Boolean shouldAccelerate;
    private MediaPlayer videoPlayer;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long time = 600;
        String transition = null;
        int padding = 0;
        boolean titlebar = false;
        String title = null;
        boolean navbar = true;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            time = extras.getLong("transitionTime", 600);
            transition = extras.getString("overlayTransition");
            padding = extras.getInt("shouldResizeOverlay", 0);
            titlebar = extras.getBoolean("shouldShowTitlebar");
            title = extras.getString("overlayTitle");
            navbar = extras.getBoolean("shouldShowNavbar");
        }
        Log.i(TAG, "Path: " + getIntent().getData().getLastPathSegment());
        this.mmOverlay = new MMAdViewWebOverlay(this, padding, time, transition, titlebar, title, navbar);
        setContentView(this.mmOverlay);
        this.mmOverlay.loadWebContent(getIntent().getDataString());
        this.shouldAccelerate = Boolean.valueOf(getIntent().getBooleanExtra("canAccelerate", false));
        setRequestedOrientation(1);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.i(TAG, "Accelerometer passed?: " + this.shouldAccelerate);
        super.onResume();
        if (AccelerometerManager.isSupported() && this.shouldAccelerate.booleanValue()) {
            AccelerometerManager.startListening(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.videoPlayer != null) {
            this.videoPlayer.release();
            this.videoPlayer = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.videoPlayer != null) {
            this.videoPlayer.stop();
        }
        if (AccelerometerManager.isListening()) {
            Log.i(TAG, "Accelerometer stopped");
            AccelerometerManager.stopListening();
        }
        setResult(0);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public static Context getContext() {
        return context;
    }

    public void didShake(float force) {
        Log.d(TAG, "Phone shaken: " + force);
        this.mmOverlay.injectJS("javascript:didShake(" + force + ")");
    }

    public void didAccelerate(float x, float y, float z) {
        Log.d(TAG, "Accelerometer x:" + x + " y:" + y + " z:" + z);
        this.mmOverlay.injectJS("javascript:didAccelerate(" + x + "," + y + "," + z + ")");
    }
}
