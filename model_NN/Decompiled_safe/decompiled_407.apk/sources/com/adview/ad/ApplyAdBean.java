package com.adview.ad;

public class ApplyAdBean {
    private String appId = null;
    private int isTestMode = 0;
    private int system;

    public ApplyAdBean() {
        setSystem(1);
    }

    public String getAppId() {
        return this.appId;
    }

    public void setAppId(String str) {
        this.appId = new String(str);
    }

    public int getTestMode() {
        return this.isTestMode;
    }

    public void setTestMode(int val) {
        this.isTestMode = val;
    }

    public int getSystem() {
        return this.system;
    }

    public void setSystem(int val) {
        this.system = val;
    }
}
