package com.adview.adapters;

import android.graphics.Color;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.wiyun.ad.AdView;

public class WiyunAdapter extends AdViewAdapter {
    public WiyunAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Wiyun");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            AdView ad = new AdView(adViewLayout.getContext());
            ad.setBackgroundColor(bgColor);
            ad.setTextColor(fgColor);
            ad.setResId(this.ration.key);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                ad.setTestMode(true);
            } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                ad.setTestMode(false);
            } else {
                ad.setTestMode(false);
            }
            ad.setTransitionType(5);
            ad.setRefreshInterval(0);
            ad.requestAd();
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, ad));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
