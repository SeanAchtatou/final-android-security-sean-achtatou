package com.adview.adapters;

import android.util.Log;
import android.view.Display;
import com.adchina.android.ads.AdManager;
import com.adchina.android.ads.AdView;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;

public class AdChinaAdapter extends AdViewAdapter {
    public AdChinaAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into AdChina");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Display display = adViewLayout.activityReference.get().getWindowManager().getDefaultDisplay();
            AdManager.setResolution(String.valueOf(display.getWidth()) + TMXConstants.TAG_OBJECT_ATTRIBUTE_X + display.getHeight());
            AdManager.setAdspaceId(this.ration.key);
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                AdManager.setDebugMode(true);
            } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                AdManager.setDebugMode(false);
            } else {
                AdManager.setDebugMode(false);
            }
            AdView mAdView = new AdView(adViewLayout.getContext());
            mAdView.start();
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, mAdView));
            adViewLayout.rotateThreadedDelayed();
        }
    }
}
