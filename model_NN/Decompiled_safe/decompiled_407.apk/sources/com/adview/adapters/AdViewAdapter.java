package com.adview.adapters;

import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.finger2finger.games.common.CommonConst;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;

public abstract class AdViewAdapter {
    protected final WeakReference<AdViewLayout> adViewLayoutReference;
    protected Ration ration;

    public abstract void handle();

    public AdViewAdapter(AdViewLayout adViewLayout, Ration ration2) {
        this.adViewLayoutReference = new WeakReference<>(adViewLayout);
        this.ration = ration2;
    }

    private static AdViewAdapter getAdapter(AdViewLayout adViewLayout, Ration ration2) {
        try {
            switch (ration2.type) {
                case 1:
                    if (Class.forName("com.google.ads.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.AdMobAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 2:
                case 3:
                case 4:
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case CommonConst.F2FALERT_MSGID.SMS_SEND_FAIL /*18*/:
                case 19:
                case 20:
                case 23:
                case 27:
                default:
                    return unknownAdNetwork(adViewLayout, ration2);
                case 6:
                    if (Class.forName("com.millennialmedia.android.MMAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.MillennialAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 21:
                    if (Class.forName("com.wooboo.adlib_android.WoobooAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.WoobooAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 22:
                    if (Class.forName("net.youmi.android.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.YoumiAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 24:
                    if (Class.forName("com.casee.adsdk.CaseeAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.CaseeAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 25:
                    if (Class.forName("com.wiyun.ad.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.WiyunAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_ADCHINA /*26*/:
                    if (Class.forName("com.adchina.android.ads.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.AdChinaAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 28:
                    if (Class.forName("com.adview.ad.KyAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.AdViewHouseAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_SMARTAD /*29*/:
                    if (Class.forName("com.madhouse.android.ads.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.SmartAdAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 30:
                    if (Class.forName("cn.domob.android.ads.DomobAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.DomobAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_VPON /*31*/:
                    if (Class.forName("com.vpon.adon.android.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.VponAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_ADTOUCH /*32*/:
                    if (Class.forName("com.energysource.szj.embeded.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.AdTouchAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_ADWO /*33*/:
                    if (Class.forName("com.adwo.adsdk.AdwoAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.AdwoAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_AIRAD /*34*/:
                    if (Class.forName("com.mt.airad.AirAD") != null) {
                        return getNetworkAdapter("com.adview.adapters.AirAdAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case 35:
                    if (Class.forName("com.wqmobile.sdk.widget.ADView") != null) {
                        return getNetworkAdapter("com.adview.adapters.WqAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_APPMEDIA /*36*/:
                    if (Class.forName("cn.appmedia.ad.BannerAdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.AppMediaAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
                case AdViewUtil.NETWORK_TYPE_TINMOO /*37*/:
                    if (Class.forName("com.ignitevision.android.ads.AdView") != null) {
                        return getNetworkAdapter("com.adview.adapters.TinmooAdapter", adViewLayout, ration2);
                    }
                    return unknownAdNetwork(adViewLayout, ration2);
            }
        } catch (ClassNotFoundException e) {
            return unknownAdNetwork(adViewLayout, ration2);
        } catch (VerifyError e2) {
            VerifyError e3 = e2;
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                Log.e("AdView", "YYY - Caught VerifyError", e3);
            }
            return unknownAdNetwork(adViewLayout, ration2);
        }
    }

    private static AdViewAdapter getNetworkAdapter(String networkAdapter, AdViewLayout adViewLayout, Ration ration2) {
        try {
            return (AdViewAdapter) Class.forName(networkAdapter).getConstructor(AdViewLayout.class, Ration.class).newInstance(adViewLayout, ration2);
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | NoSuchMethodException | SecurityException | InvocationTargetException e) {
            return null;
        }
    }

    private static AdViewAdapter unknownAdNetwork(AdViewLayout adViewLayout, Ration ration2) {
        if (AdViewTargeting.getRunMode() != AdViewTargeting.RunMode.TEST) {
            return null;
        }
        Log.w(AdViewUtil.ADVIEW, "Unsupported ration type: " + ration2.type);
        return null;
    }

    public static void handle(AdViewLayout adViewLayout, Ration ration2) throws Throwable {
        AdViewAdapter adapter = getAdapter(adViewLayout, ration2);
        if (adapter != null) {
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                Log.d(AdViewUtil.ADVIEW, "Valid adapter, calling handle()");
            }
            adapter.handle();
            return;
        }
        throw new Exception("Invalid adapter");
    }
}
