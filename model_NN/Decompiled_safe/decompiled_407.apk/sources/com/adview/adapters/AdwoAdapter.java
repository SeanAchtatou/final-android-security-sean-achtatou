package com.adview.adapters;

import android.app.Activity;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.adwo.adsdk.AdwoAdView;

public class AdwoAdapter extends AdViewAdapter {
    public AdwoAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        AdwoAdView adView;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Adwo");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null && adViewLayout.activityReference.get() != null) {
            Extra extra = adViewLayout.extra;
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                adView = new AdwoAdView((Activity) adViewLayout.getContext(), this.ration.key, 4194432, 16711680, true, extra.cycleTime);
            } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                adView = new AdwoAdView((Activity) adViewLayout.getContext(), this.ration.key, 4194432, 16711680, false, extra.cycleTime);
            } else {
                adView = new AdwoAdView((Activity) adViewLayout.getContext(), this.ration.key, 4194432, 16711680, false, extra.cycleTime);
            }
            adViewLayout.adViewManager.resetRollover();
            adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
            adViewLayout.rotateThreadedDelayed();
            adView.finalize();
        }
    }
}
