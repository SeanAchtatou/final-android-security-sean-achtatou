package com.adview.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewManager;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import net.youmi.android.AdManager;
import net.youmi.android.AdView;

public class YoumiAdapter extends AdViewAdapter {
    public YoumiAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
        String key = new String(ration.key);
        String key2 = new String(ration.key2);
        int internal = adViewLayout.extra.cycleTime;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            if (AdViewManager.getYoumiInit()) {
                AdManager.init(key, key2, internal, true, 2.2d);
                AdViewManager.setYoumiInit(false);
            }
        } else if (AdViewManager.getYoumiInit()) {
            AdManager.init(key, key2, internal, false, 2.2d);
            AdViewManager.setYoumiInit(false);
        }
    }

    public void handle() {
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into Youmi");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            int bgColor = Color.rgb(extra.bgRed, extra.bgGreen, extra.bgBlue);
            int fgColor = Color.rgb(extra.fgRed, extra.fgGreen, extra.fgBlue);
            Activity activity = adViewLayout.activityReference.get();
            if (activity != null) {
                AdView adView = new AdView(activity, bgColor, fgColor, AdViewUtil.VERSION);
                adViewLayout.adViewManager.resetRollover();
                adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, adView));
                adViewLayout.rotateThreadedDelayed();
            }
        }
    }
}
