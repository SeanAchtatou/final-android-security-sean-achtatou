package com.adview.adapters;

import android.app.Activity;
import android.util.AttributeSet;
import android.util.Log;
import com.adview.AdViewLayout;
import com.adview.AdViewTargeting;
import com.adview.obj.Extra;
import com.adview.obj.Ration;
import com.adview.util.AdViewUtil;
import com.madhouse.android.ads.AdListener;
import com.madhouse.android.ads.AdManager;
import com.madhouse.android.ads.AdView;

public class SmartAdAdapter extends AdViewAdapter implements AdListener {
    public SmartAdAdapter(AdViewLayout adViewLayout, Ration ration) {
        super(adViewLayout, ration);
    }

    public void handle() {
        AdView ad;
        if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
            Log.d(AdViewUtil.ADVIEW, "Into SmartAd");
        }
        AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
        if (adViewLayout != null) {
            Extra extra = adViewLayout.extra;
            Activity activity = adViewLayout.activityReference.get();
            if (activity != null) {
                AdManager.setApplicationId(activity, this.ration.key);
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    ad = new AdView(activity, (AttributeSet) null, 0, this.ration.key2, extra.cycleTime, 0, true);
                } else if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.NORMAL) {
                    ad = new AdView(activity, (AttributeSet) null, 0, this.ration.key2, extra.cycleTime, 0, false);
                } else {
                    ad = new AdView(activity, (AttributeSet) null, 0, this.ration.key2, extra.cycleTime, 0, false);
                }
                ad.setListener(this);
            }
        }
    }

    public void onAdEvent(AdView arg0, int arg1) {
        switch (arg1) {
            case 1:
            case 3:
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    Log.d(AdViewUtil.ADVIEW, "SmartAd new Ad");
                }
                AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
                if (adViewLayout != null) {
                    adViewLayout.adViewManager.resetRollover();
                    adViewLayout.handler.post(new AdViewLayout.ViewAdRunnable(adViewLayout, arg0));
                    adViewLayout.rotateThreadedDelayed();
                    arg0.setListener((AdListener) null);
                    return;
                }
                return;
            case 2:
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    Log.d(AdViewUtil.ADVIEW, "SmartAd success");
                }
                arg0.setListener((AdListener) null);
                return;
            case 4:
                if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                    Log.d(AdViewUtil.ADVIEW, "SmartAd invalid ad");
                }
                arg0.setListener((AdListener) null);
                AdViewLayout adViewLayout2 = (AdViewLayout) this.adViewLayoutReference.get();
                if (adViewLayout2 != null) {
                    adViewLayout2.adViewManager.resetRollover_pri();
                    adViewLayout2.rollover_pri();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onAdStatus(int arg0) {
        if (arg0 != 200) {
            if (AdViewTargeting.getRunMode() == AdViewTargeting.RunMode.TEST) {
                Log.d(AdViewUtil.ADVIEW, "SmartAd fail ad");
            }
            AdViewLayout adViewLayout = (AdViewLayout) this.adViewLayoutReference.get();
            if (adViewLayout != null) {
                adViewLayout.adViewManager.resetRollover_pri();
                adViewLayout.rollover_pri();
            }
        }
    }
}
