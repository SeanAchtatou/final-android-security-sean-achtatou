package com.finger2finger.games.scene;

import android.content.res.Resources;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.MainMenuButtons;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class MainMenuScene extends F2FScene {
    private Font mFontPlay;
    private MainMenuButtons mMainMenuButtons;
    private String mStrPlay;
    private String mStrTile1;
    private String mStrTile2;
    private Font mTitleFont;

    public MainMenuScene(F2FGameActivity pContext) {
        super(4, pContext);
        loadScene();
    }

    public void loadScene() {
        loadResources();
        initBackgroudScene(this);
        initPlayButton(this, 1);
        initMenuSprites(this, 1);
    }

    private void loadResources() {
        loadFont();
        loadStringsFromXml();
    }

    private void loadFont() {
        this.mFontPlay = this.mContext.mResource.getBaseFontByKey(Resource.FONT.MAINMENU_PLAY.mKey);
        this.mTitleFont = this.mContext.mResource.getBaseFontByKey(Resource.FONT.GAME_TITLE_FONT.mKey);
    }

    private void loadStringsFromXml() {
        Resources res = this.mContext.getResources();
        this.mStrPlay = res.getString(R.string.str_startGame);
        this.mStrTile1 = res.getString(R.string.title_1);
        this.mStrTile2 = res.getString(R.string.title_2);
    }

    private void initBackgroudScene(Scene pScene) {
        pScene.getBottomLayer().addEntity(new Sprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.MAINMENU_BACKGROUND.mKey)));
    }

    private void initMenuSprites(Scene pScene, int pLayerIndex) {
        this.mMainMenuButtons = new MainMenuButtons(this.mContext, this, 2);
        this.mMainMenuButtons.createButtons();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
     arg types: [int, int]
     candidates:
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], int):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long[], boolean):org.anddev.andengine.entity.sprite.AnimatedSprite
      org.anddev.andengine.entity.sprite.AnimatedSprite.animate(long, boolean):org.anddev.andengine.entity.sprite.AnimatedSprite */
    private void initPlayButton(Scene pScene, int pLayerIndex) {
        TiledTextureRegion crabRegion = this.mContext.mResource.getTiledTextureRegionByKey(Resource.TILEDTURE.MAIN_MENU_CRAB.mKey);
        AnimatedSprite mSpriteCrab = new AnimatedSprite(((float) (crabRegion.getTileWidth() * 2)) * 0.2f * CommonConst.RALE_SAMALL_VALUE, ((float) CommonConst.CAMERA_HEIGHT) - ((((float) (crabRegion.getTileHeight() * 2)) * 1.4f) * CommonConst.RALE_SAMALL_VALUE), ((float) (crabRegion.getTileWidth() * 2)) * CommonConst.RALE_SAMALL_VALUE, ((float) (crabRegion.getTileHeight() * 2)) * CommonConst.RALE_SAMALL_VALUE, crabRegion.clone());
        mSpriteCrab.animate(500L, true);
        getLayer(pLayerIndex).addEntity(mSpriteCrab);
        Text mTextTitle1 = new Text(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mTitleFont, this.mStrTile1);
        float titleWidth = mTextTitle1.getWidth();
        float titleHeight = mTextTitle1.getHeight();
        float titlePX = (((float) CommonConst.CAMERA_WIDTH) - titleWidth) / 2.0f;
        float titlePY = (((float) CommonConst.CAMERA_HEIGHT) - titleHeight) / 4.0f;
        mTextTitle1.setPosition(titlePX, titlePY);
        getLayer(pLayerIndex).addEntity(mTextTitle1);
        Text mTextTitle2 = new Text(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mTitleFont, this.mStrTile2);
        float titleWidth2 = mTextTitle2.getWidth();
        float titleHeight2 = mTextTitle2.getHeight();
        float titlePY2 = titlePY + titleHeight2;
        mTextTitle2.setPosition((((float) CommonConst.CAMERA_WIDTH) - titleWidth2) / 2.0f, titlePY2);
        getLayer(pLayerIndex).addEntity(mTextTitle2);
        TextureRegion startGameRegion = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.MAINMENU_PLAY_BLANK.mKey);
        float bntPlayWidth = ((float) startGameRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float bntPlayHeight = ((float) startGameRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float bntPlayPX = (((float) CommonConst.CAMERA_WIDTH) - bntPlayWidth) / 2.0f;
        float bntPlayPY = (30.0f * CommonConst.RALE_SAMALL_VALUE) + titlePY2 + titleHeight2;
        Sprite startGame = new Sprite(bntPlayPX, bntPlayPY, bntPlayWidth, bntPlayHeight, startGameRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                MainMenuScene.this.mContext.setStatus(F2FGameActivity.Status.LEVEL_OPTION);
                return true;
            }
        };
        pScene.getLayer(pLayerIndex).addEntity(startGame);
        pScene.getLayer(pLayerIndex).registerTouchArea(startGame);
        Text mTextPlay = new Text(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mFontPlay, this.mStrPlay);
        mTextPlay.setPosition((((((float) startGameRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f) + bntPlayPX) - (mTextPlay.getWidth() / 2.0f), (((((float) startGameRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f) + bntPlayPY) - (mTextPlay.getHeight() / 2.0f));
        getLayer(pLayerIndex).addEntity(mTextPlay);
    }
}
