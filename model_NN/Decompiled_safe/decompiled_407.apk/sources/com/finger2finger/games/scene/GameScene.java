package com.finger2finger.games.scene;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.RemovedObject;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.scene.ContextMenuScene;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.common.scene.dialog.ImageDialog;
import com.finger2finger.games.common.scene.dialog.PortraitGameOverDialog;
import com.finger2finger.games.common.scene.dialog.PortraitTextDialog;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.engine.camera.hud.HUD;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.AlphaModifier;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.MoveModifier;
import org.anddev.andengine.entity.shape.modifier.ParallelShapeModifier;
import org.anddev.andengine.entity.shape.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.MathUtils;
import org.anddev.andengine.util.modifier.IModifier;

public class GameScene extends F2FScene {
    private float CLOUD1_LEFT_BORDER;
    private float CLOUD1_RIGHT_BORDER;
    private float CLOUD2_LEFT_BORDER;
    private float CLOUD2_RIGHT_BORDER;
    private float CLOUD_SCALE = 0.6f;
    private float CLOUD_SPEED1 = 20.0f;
    private float CLOUD_SPEED2 = 20.0f;
    private int Layer_Background = 0;
    /* access modifiers changed from: private */
    public int Layer_Bubble = 2;
    private int Layer_Picture = 1;
    private String TIME_FORMART = "%02d:%02d";
    private long gameupdateTime;
    private int insideIndex = 0;
    private boolean isChildSceneClick = false;
    private boolean isFirstClear = false;
    public boolean isFirstPlayGame = true;
    private boolean isFirstTouch = false;
    private boolean isFirstUpdateUnlimitedTime = true;
    private boolean isFirstXTime = true;
    public boolean isGameOver = false;
    private boolean isNeedCheckClear = true;
    private boolean isRemainTimeFirst = true;
    boolean isReplayGame = false;
    public boolean isTimeLimitedPlaying = false;
    private int level = 0;
    private ChildSceneStatus mChildStatus = ChildSceneStatus.UNDIFINE;
    private int[][] mClear = ((int[][]) Array.newInstance(Integer.TYPE, Const.MAP_HEIGHT, Const.MAP_WIDTH));
    private int mClearTimes = 0;
    private Sprite mCloud1;
    private Sprite mCloud2;
    private ContextMenuScene mContextMenuScene;
    private Font mFontHud;
    private Font mFontScoreHint;
    private int mGameMode = 0;
    private HUD mHud;
    private F2FVector mOriginalVector = null;
    private float[][][] mPosition = ((float[][][]) Array.newInstance(Float.TYPE, Const.MAP_HEIGHT, Const.MAP_WIDTH, 2));
    private ChangeableText mRemainSecond;
    /* access modifiers changed from: private */
    public int mScore = 0;
    private int mScoreIncrease = 5;
    private Sprite mSpriteArrow;
    private AnimatedSprite mSpriteCrab;
    /* access modifiers changed from: private */
    public Sprite mSpriteFrame;
    private MySprite[][] mSpritePictures = ((MySprite[][]) Array.newInstance(MySprite.class, Const.MAP_HEIGHT, Const.MAP_WIDTH));
    private Sprite mSpriteRemainTime;
    private Sprite mSpriteScoreBlank;
    private Sprite mSpriteXTimeBar;
    private int[] mStarLevel = new int[CommonConst.STAR_NUM];
    private float mStartX = Const.MOVE_LIMITED_SPEED;
    private float mStartY = Const.MOVE_LIMITED_SPEED;
    private int[][] mStatus = ((int[][]) Array.newInstance(Integer.TYPE, Const.MAP_HEIGHT, Const.MAP_WIDTH));
    private TextureRegion mTRArrow;
    private TextureRegion mTRBG;
    private TextureRegion mTRFrame;
    private TiledTextureRegion[] mTRPic = new TiledTextureRegion[6];
    private TextureRegion mTRRemainTimeBG;
    private TextureRegion mTRScoreBG;
    private TextureRegion mTRXTimeBar;
    private TiledTextureRegion mTTRCrab;
    private float mTargetScoreX = Const.MOVE_LIMITED_SPEED;
    private float mTargetScoreY = Const.MOVE_LIMITED_SPEED;
    /* access modifiers changed from: private */
    public ChangeableText mTextScore;
    private long mTimeIncreaseUpdate = 0;
    private long mTimeScoreX = 0;
    private Rectangle mXTimeProgress;
    private int mXTimeRemain = 0;
    private long mXTimeThrough = 0;
    private float mXTime_Height;
    private float mXTime_Width;
    private int remainTime = 0;
    private int remainUnlimitedTime = 0;
    /* access modifiers changed from: private */
    public Queue<RemovedObject> removedSpriteList = new LinkedList();
    private ArrayList<F2FVector> sameArrayList = new ArrayList<>();
    float[] sceneTouchdata_down = new float[3];
    private int subLevel = 0;

    public enum ChildSceneStatus {
        UNDIFINE,
        GAME_HELP_DIALOG,
        GAME_START_DIALOG,
        GAME_OVER_FAIL,
        GAME_OVER_OK,
        GAME_NEXLLEVEL,
        GAME_PASSALLLEVEL,
        GAME_PASSALLLEVEL_LITE
    }

    public boolean isChildSceneClick() {
        return this.isChildSceneClick;
    }

    public void setChildSceneClick(boolean isChildSceneClick2) {
        this.isChildSceneClick = isChildSceneClick2;
    }

    public GameScene(F2FGameActivity pContext) {
        super(4, pContext);
        enableMultiTouch();
        enableSceneTouch();
        this.level = this.mContext.getMGameInfo().getMLevelIndex();
        this.subLevel = this.mContext.getMGameInfo().getMSubLevelIndex();
        this.insideIndex = this.mContext.getMGameInfo().getMInsideIndex();
        if (this.level == 0) {
            this.mGameMode = 0;
        } else if (this.level == 1) {
            this.mGameMode = 1;
        }
        loadResources();
        startGame();
    }

    public void loadScene() {
    }

    private void loadResources() {
        loadTextures();
        loadFont();
        loadHelpPreferences();
    }

    private void loadFont() {
        this.mFontScoreHint = this.mContext.mResource.getBaseFontByKey(Resource.FONT.XSCORE.mKey);
        this.mFontHud = this.mContext.mResource.getBaseFontByKey(Resource.FONT.HUD_CONTEXT.mKey);
    }

    public void loadHelpPreferences() {
        this.isFirstPlayGame = CommonConst.GAME_HELP_SHOW;
        if (this.isFirstPlayGame) {
            CommonConst.GAME_HELP_SHOW = false;
        }
        this.mContext.setGameInfo();
    }

    private void loadTextures() {
        this.mTRPic = this.mContext.mResource.getArrayTiledTextureRegionByKey(Resource.TILEDTURE.GAME_SEASTAR.mKey);
        this.mTRFrame = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_FRAME.mKey);
        this.mTRArrow = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_ARROW.mKey);
        this.mTTRCrab = this.mContext.mResource.getTiledTextureRegionByKey(Resource.TILEDTURE.MAIN_MENU_CRAB.mKey);
        this.mTRBG = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_BACKGROUND.mKey);
        this.mTRXTimeBar = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_TIMEBAR.mKey);
        this.mTRScoreBG = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_SCORE.mKey);
        this.mTRRemainTimeBG = this.mContext.mResource.getTextureRegionByKey(Resource.TEXTURE.GAME_REMAIN.mKey);
    }

    private void startGame() {
        initializeBackGroud();
        initializeGameInfo();
        initializeSeaStar();
        initializeEntity();
        showStartHint();
        this.mContext.getEngine().enableVibrator(this.mContext);
        registerUpdateHandler(new IUpdateHandler() {
            public void onUpdate(float pSecondsElapsed) {
                if (!GameScene.this.isGameOver) {
                    GameScene.this.updateUnlimitedTime();
                    GameScene.this.updateXTime();
                    GameScene.this.updateRemainTime();
                    GameScene.this.onUpdateSceneTouchEvent_Down();
                    GameScene.this.resetSeaStar();
                    GameScene.this.doMoveUp();
                    GameScene.this.doMoveDown();
                    GameScene.this.doCheckTheSame();
                    GameScene.this.doRemoveEntity();
                    GameScene.this.updateCloud();
                }
            }

            public void reset() {
            }
        });
        if (this.mGameMode == 1) {
            this.mTimeIncreaseUpdate = System.currentTimeMillis();
        }
    }

    /* access modifiers changed from: private */
    public void updateUnlimitedTime() {
        if (this.mGameMode == 1) {
            if (this.isFirstUpdateUnlimitedTime) {
                this.remainUnlimitedTime = 60;
                this.mTimeIncreaseUpdate = System.currentTimeMillis();
                this.isFirstUpdateUnlimitedTime = false;
            }
            if (System.currentTimeMillis() - this.mTimeIncreaseUpdate >= 1000) {
                this.mTimeIncreaseUpdate = System.currentTimeMillis();
                this.remainUnlimitedTime--;
            }
            if (this.remainUnlimitedTime <= 0) {
                this.isFirstUpdateUnlimitedTime = true;
                this.mScoreIncrease++;
                showAccelerationHint();
            }
        }
    }

    private void getTimeBonus(int bonus) {
        if (this.mGameMode == 1) {
            this.remainTime += bonus;
            this.mRemainSecond.setText(String.valueOf(this.remainTime));
            this.mRemainSecond.addShapeModifier(new SequenceShapeModifier(new ScaleModifier(0.3f, 1.0f, 1.5f), new ScaleModifier(0.3f, 1.5f, 1.0f)));
        } else if (this.mGameMode == 0) {
            this.remainTime += bonus;
            this.mRemainSecond.setText(getTimeFormart(this.remainTime));
            this.mRemainSecond.addShapeModifier(new SequenceShapeModifier(new ScaleModifier(0.3f, 1.0f, 1.5f), new ScaleModifier(0.3f, 1.5f, 1.0f)));
        }
    }

    public void showStartHint() {
        String msg = "";
        if (this.mGameMode == 1) {
            msg = this.mContext.getResources().getString(R.string.str_caption_start_unlimited);
        } else if (this.mGameMode == 0) {
            msg = this.mContext.getResources().getString(R.string.str_caption_start);
        }
        PortraitTextDialog dialog = new PortraitTextDialog(2, this.mContext.getEngine().getCamera(), this.mContext, msg);
        this.mChildStatus = ChildSceneStatus.GAME_START_DIALOG;
        setChildScene(dialog, false, true, true);
    }

    public void showAccelerationHint() {
        String msg = this.mContext.getResources().getString(R.string.str_acceleration_hint);
        PortraitTextDialog dialog = new PortraitTextDialog(2, this.mContext.getEngine().getCamera(), this.mContext, String.format(msg, Integer.valueOf(this.mScoreIncrease)));
        this.mChildStatus = ChildSceneStatus.GAME_HELP_DIALOG;
        setChildScene(dialog, false, true, true);
    }

    public HUD getHud() {
        return this.mHud;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.anddev.andengine.util.MathUtils.random(float, float):float
     arg types: [int, int]
     candidates:
      org.anddev.andengine.util.MathUtils.random(int, int):int
      org.anddev.andengine.util.MathUtils.random(float, float):float */
    private void initializeBackGroud() {
        getBottomLayer().addEntity(new Sprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mTRBG));
        this.CLOUD_SPEED1 = MathUtils.random(10.0f, 30.0f);
        this.CLOUD_SPEED2 = MathUtils.random(10.0f, 30.0f);
        TextureRegion[] clouds = this.mContext.mResource.getArrayTextureRegionByKey(Resource.TEXTURE.GAME_CLOUD.mKey);
        this.mCloud1 = new Sprite(((float) clouds[0].getWidth()) * this.CLOUD_SCALE * 0.1f * CommonConst.RALE_SAMALL_VALUE, ((float) clouds[0].getHeight()) * 1.5f * this.CLOUD_SCALE * CommonConst.RALE_SAMALL_VALUE, ((float) clouds[0].getWidth()) * CommonConst.RALE_SAMALL_VALUE * this.CLOUD_SCALE, ((float) clouds[0].getHeight()) * CommonConst.RALE_SAMALL_VALUE * this.CLOUD_SCALE, clouds[0]);
        this.mCloud1.setVelocityX(this.CLOUD_SPEED1 * CommonConst.RALE_SAMALL_VALUE);
        this.mCloud2 = new Sprite(((float) CommonConst.CAMERA_WIDTH) - (((((float) clouds[1].getWidth()) * this.CLOUD_SCALE) * 1.1f) * CommonConst.RALE_SAMALL_VALUE), ((float) clouds[1].getHeight()) * this.CLOUD_SCALE * 0.8f * CommonConst.RALE_SAMALL_VALUE, ((float) clouds[1].getWidth()) * CommonConst.RALE_SAMALL_VALUE * this.CLOUD_SCALE, ((float) clouds[1].getHeight()) * CommonConst.RALE_SAMALL_VALUE * this.CLOUD_SCALE, clouds[1]);
        this.mCloud2.setVelocityX(this.CLOUD_SPEED2 * CommonConst.RALE_SAMALL_VALUE);
        getLayer(this.Layer_Background).addEntity(this.mCloud1);
        getLayer(this.Layer_Background).addEntity(this.mCloud2);
        this.CLOUD1_LEFT_BORDER = this.mCloud1.getWidth() * 0.5f;
        this.CLOUD2_LEFT_BORDER = this.mCloud1.getWidth() * 0.8f;
        this.CLOUD1_RIGHT_BORDER = ((float) CommonConst.CAMERA_WIDTH) - (this.mCloud1.getWidth() * 1.5f);
        this.CLOUD2_RIGHT_BORDER = ((float) CommonConst.CAMERA_WIDTH) - this.mCloud2.getWidth();
    }

    /* access modifiers changed from: private */
    public void updateCloud() {
        if (this.mCloud1.getX() < this.CLOUD1_LEFT_BORDER) {
            this.mCloud1.setVelocityX(this.CLOUD_SPEED1 * CommonConst.RALE_SAMALL_VALUE);
            this.mCloud1.setPosition(this.CLOUD1_LEFT_BORDER, this.mCloud1.getY());
        } else if (this.mCloud1.getX() > this.CLOUD1_RIGHT_BORDER) {
            this.mCloud1.setVelocityX((-this.CLOUD_SPEED1) * CommonConst.RALE_SAMALL_VALUE);
            this.mCloud1.setPosition(this.CLOUD1_RIGHT_BORDER, this.mCloud1.getY());
        }
        if (this.mCloud2.getX() < this.CLOUD2_LEFT_BORDER) {
            this.mCloud2.setVelocityX(this.CLOUD_SPEED2 * CommonConst.RALE_SAMALL_VALUE);
            this.mCloud2.setPosition(this.CLOUD2_LEFT_BORDER, this.mCloud2.getY());
        } else if (this.mCloud2.getX() > this.CLOUD2_RIGHT_BORDER) {
            this.mCloud2.setVelocityX((-this.CLOUD_SPEED2) * CommonConst.RALE_SAMALL_VALUE);
            this.mCloud2.setPosition(this.CLOUD2_RIGHT_BORDER, this.mCloud2.getY());
        }
    }

    private void initializeGameInfo() {
        this.mSpriteScoreBlank = new Sprite(CommonConst.RALE_SAMALL_VALUE * 5.0f, CommonConst.RALE_SAMALL_VALUE * 20.0f, ((float) this.mTRScoreBG.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRScoreBG.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRScoreBG);
        this.mTextScore = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mFontHud, getScoreFormartString(this.mScore, 5), 5);
        this.mTextScore.setPosition(this.mSpriteScoreBlank.getX() + (((((float) this.mTRScoreBG.getWidth()) * CommonConst.RALE_SAMALL_VALUE) * 2.1f) / 5.0f), (this.mSpriteScoreBlank.getY() + (((((float) this.mTRScoreBG.getHeight()) * CommonConst.RALE_SAMALL_VALUE) * 2.0f) / 5.0f)) - (this.mTextScore.getHeight() / 2.0f));
        this.mTargetScoreX = this.mTextScore.getX();
        this.mTargetScoreY = this.mTextScore.getY() + this.mTextScore.getHeight();
        getLayer(1).addEntity(this.mSpriteScoreBlank);
        getLayer(2).addEntity(this.mTextScore);
        float remainTimeBlank_x = ((float) CommonConst.CAMERA_WIDTH) - (((float) (this.mTRRemainTimeBG.getWidth() + 5)) * CommonConst.RALE_SAMALL_VALUE);
        this.mSpriteRemainTime = new Sprite(remainTimeBlank_x, 20.0f * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRRemainTimeBG.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRRemainTimeBG.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRRemainTimeBG);
        if (this.mGameMode == 1) {
            this.mRemainSecond = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mFontHud, String.valueOf(100), 5);
        } else if (this.mGameMode == 0) {
            this.mRemainSecond = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mFontHud, getTimeFormart(Const.INITIAL_NOMARL_TIME), 5);
        }
        this.mRemainSecond.setPosition(((this.mSpriteRemainTime.getWidth() * 7.0f) / 15.0f) + remainTimeBlank_x, (this.mSpriteRemainTime.getY() + (this.mSpriteRemainTime.getHeight() / 2.0f)) - (this.mRemainSecond.getHeight() / 2.0f));
        getLayer(1).addEntity(this.mSpriteRemainTime);
        getLayer(2).addEntity(this.mRemainSecond);
        this.mSpriteCrab = new AnimatedSprite(this.mSpriteScoreBlank.getX() + this.mSpriteScoreBlank.getWidth() + (10.0f * CommonConst.RALE_SAMALL_VALUE), 10.0f * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTTRCrab.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTTRCrab.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTTRCrab);
        getLayer(2).addEntity(this.mSpriteCrab);
        float timesPX = ((float) CommonConst.CAMERA_WIDTH) - (((float) (this.mTRXTimeBar.getWidth() + 5)) * CommonConst.RALE_SAMALL_VALUE);
        float timesPY = 120.0f * CommonConst.RALE_SAMALL_VALUE;
        this.mSpriteXTimeBar = new Sprite(timesPX, timesPY, ((float) this.mTRXTimeBar.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRXTimeBar.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRXTimeBar.clone());
        this.mXTime_Height = this.mSpriteXTimeBar.getHeight() - (55.0f * CommonConst.RALE_SAMALL_VALUE);
        this.mXTime_Width = 5.0f * CommonConst.RALE_SAMALL_VALUE;
        this.mXTimeProgress = new Rectangle(((this.mSpriteXTimeBar.getWidth() - this.mXTime_Width) / 2.0f) + timesPX + (2.0f * CommonConst.RALE_SAMALL_VALUE), timesPY + (40.0f * CommonConst.RALE_SAMALL_VALUE), this.mXTime_Width * CommonConst.RALE_SAMALL_VALUE, this.mXTime_Height * CommonConst.RALE_SAMALL_VALUE);
        this.mXTimeProgress.setColor(1.0f, Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, 0.8f);
        getLayer(2).addEntity(this.mSpriteXTimeBar);
        getLayer(2).addEntity(this.mXTimeProgress);
        showTimeProgress(false);
    }

    /* access modifiers changed from: private */
    public void updateXTime() {
        if (this.isFirstClear && this.mSpriteXTimeBar.isVisible()) {
            if (this.isFirstXTime) {
                this.mXTimeRemain = 80;
                this.mTimeScoreX = System.currentTimeMillis();
                this.mXTimeThrough = 0;
                updateProgress(Const.MOVE_LIMITED_SPEED, 8000.0f);
                this.isFirstXTime = false;
            }
            if (System.currentTimeMillis() - this.mTimeScoreX >= 100) {
                this.mTimeScoreX = System.currentTimeMillis();
                this.mXTimeRemain--;
                this.mXTimeThrough += 100;
                updateProgress((float) this.mXTimeThrough, 8000.0f);
            }
            if (this.mXTimeRemain <= 0) {
                this.isFirstXTime = true;
                this.mClearTimes = 0;
                this.isFirstClear = false;
                showTimeProgress(false);
            }
        }
    }

    private void showTimeProgress(boolean isVisible) {
        this.mSpriteXTimeBar.setVisible(isVisible);
        this.mXTimeProgress.setVisible(isVisible);
    }

    private void updateProgress(float pVariable1, float pVariable2) {
        if (this.mXTimeProgress.isVisible()) {
            float pecentRale = 1.0f - (((pVariable1 * 1.0f) / pVariable2) * 1.0f);
            if (pecentRale > Const.MOVE_LIMITED_SPEED) {
                this.mXTimeProgress.setHeight(this.mXTime_Height * pecentRale);
            } else {
                this.mXTimeProgress.setHeight(Const.MOVE_LIMITED_SPEED);
            }
        }
    }

    private String getTimeFormart(int pTime) {
        return String.format(this.TIME_FORMART, Integer.valueOf(pTime / 60), Integer.valueOf(pTime % 60));
    }

    /* access modifiers changed from: private */
    public String getScoreFormartString(int pValue, int pLenght) {
        String str = String.valueOf(pValue);
        int length = str.length();
        for (int i = 0; i < pLenght - length; i++) {
            str = "0" + str;
        }
        return str;
    }

    private void createTextXScore(float pX, float pY, int pXScale) {
        ChangeableText mXText = new ChangeableText(pX, pY, this.mFontScoreHint, String.format(Const.X_SCORE_FORMAT, Integer.valueOf(pXScale)), String.format(Const.X_SCORE_FORMAT, Integer.valueOf(pXScale)).length());
        mXText.addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                ChangeableText ct = (ChangeableText) arg1;
                ct.setVisible(false);
                synchronized (GameScene.this.removedSpriteList) {
                    GameScene.this.removedSpriteList.offer(new RemovedObject(ct, GameScene.this.Layer_Bubble));
                }
            }
        }, new ParallelShapeModifier(new AlphaModifier(0.5f, 1.0f, Const.MOVE_LIMITED_SPEED), new ScaleModifier(0.5f, 1.0f, 3.0f))));
        getLayer(this.Layer_Bubble).addEntity(mXText);
    }

    private void createTextAddScore(float pX, float pY, int pScore) {
        float moveTime = ((float) ((int) ((pY - this.mTargetScoreY) / Const.MAP_TILE_HEIGHT))) * 0.05f;
        ChangeableText mGetScore = new ChangeableText(pX, pY, this.mFontScoreHint, String.format(Const.ADD_SCORE_FORMAT, Integer.valueOf(pScore)), String.format(Const.ADD_SCORE_FORMAT, Integer.valueOf(pScore)).length());
        mGetScore.addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                ChangeableText ct = (ChangeableText) arg1;
                ct.setVisible(false);
                synchronized (GameScene.this.removedSpriteList) {
                    GameScene.this.removedSpriteList.offer(new RemovedObject(ct, GameScene.this.Layer_Bubble));
                }
                GameScene.this.mTextScore.setText(GameScene.this.getScoreFormartString(GameScene.this.mScore, 5));
            }
        }, new ParallelShapeModifier(new AlphaModifier(moveTime, 1.0f, 0.5f), new MoveModifier(moveTime, pX, this.mTargetScoreX, pY, this.mTargetScoreY))));
        getLayer(this.Layer_Bubble).addEntity(mGetScore);
    }

    /* access modifiers changed from: private */
    public void updateRemainTime() {
        if (!this.isGameOver) {
            if (this.isRemainTimeFirst) {
                if (this.mGameMode == 1) {
                    this.remainTime = 100;
                } else if (this.mGameMode == 0) {
                    this.remainTime = Const.INITIAL_NOMARL_TIME;
                }
                this.gameupdateTime = System.currentTimeMillis();
                this.isRemainTimeFirst = false;
            }
            if (System.currentTimeMillis() - this.gameupdateTime >= 1000) {
                this.gameupdateTime = System.currentTimeMillis();
                this.remainTime--;
                if (this.remainTime >= 0) {
                    if (this.remainTime <= 10 && !this.isTimeLimitedPlaying) {
                        Sound timeLimited = this.mContext.mResource.getSoundByKey(Resource.SOUNDTURE.SOUND_LIMITED_TIME.mKey);
                        if (CommonConst.GAME_MUSIC_ON && this.mContext.mResource != null) {
                            this.isTimeLimitedPlaying = true;
                            timeLimited.setLooping(true);
                            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
                        }
                    } else if (this.mGameMode == 1 && this.remainTime > 10) {
                        this.isTimeLimitedPlaying = false;
                        this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
                    }
                    if (this.mGameMode == 1) {
                        this.mRemainSecond.setText(String.valueOf(this.remainTime));
                    } else if (this.mGameMode == 0) {
                        this.mRemainSecond.setText(getTimeFormart(this.remainTime));
                    }
                } else {
                    if (CommonConst.GAME_MUSIC_ON) {
                        this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
                        this.isTimeLimitedPlaying = false;
                    }
                    setGameOver();
                }
            }
        }
    }

    private void setGameOver() {
        this.isGameOver = true;
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            if (this.isTimeLimitedPlaying) {
                this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
            }
            this.mContext.mResource.playSound(Resource.SOUNDTURE.GAMEOVER_SOUND);
        }
        this.mContext.getEngine().vibrate(200);
        saveGameInfo(false);
        loadGameOverTips();
    }

    private void initializeEntity() {
        this.mSpriteFrame = new Sprite(((float) (-this.mTRFrame.getWidth())) * CommonConst.RALE_SAMALL_VALUE, (float) CommonConst.CAMERA_HEIGHT, ((float) this.mTRFrame.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRFrame.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRFrame);
        this.mSpriteFrame.setVisible(false);
        this.mSpriteArrow = new Sprite(((float) (-this.mTRArrow.getWidth())) * CommonConst.RALE_SAMALL_VALUE, (float) CommonConst.CAMERA_HEIGHT, ((float) this.mTRArrow.getWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRArrow.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRArrow);
        this.mSpriteArrow.setVisible(false);
        getLayer(this.Layer_Bubble).addEntity(this.mSpriteFrame);
        getLayer(this.Layer_Bubble).addEntity(this.mSpriteArrow);
    }

    private int getRandomType() {
        return new Random().nextInt(6);
    }

    private void initializeSeaStar() {
        Const.MAP_HEIGHT = 10;
        this.mStartY = ((float) CommonConst.CAMERA_HEIGHT) - (10.0f * CommonConst.RALE_SAMALL_VALUE);
        this.mStartX = (((float) CommonConst.CAMERA_WIDTH) - ((((float) this.mTRPic[0].getTileWidth()) * CommonConst.RALE_SAMALL_VALUE) * ((float) Const.MAP_WIDTH))) / 2.0f;
        Const.MAP_TILE_WIDTH = ((float) this.mTRPic[0].getTileWidth()) * CommonConst.RALE_SAMALL_VALUE;
        Const.MAP_TILE_HEIGHT = ((float) this.mTRPic[0].getTileHeight()) * CommonConst.RALE_SAMALL_VALUE;
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            float pX = this.mStartX;
            float pY = this.mStartY - (((float) ((Const.MAP_HEIGHT - i) * this.mTRPic[0].getTileHeight())) * CommonConst.RALE_SAMALL_VALUE);
            for (int j = 0; j < Const.MAP_WIDTH; j++) {
                this.mPosition[i][j][0] = pX;
                this.mPosition[i][j][1] = pY;
                createSeaStar(i, j, pX, pY);
                pX += ((float) this.mTRPic[0].getTileWidth()) * CommonConst.RALE_SAMALL_VALUE;
            }
        }
    }

    private void createSeaStar(int i, int j, float pX, float pY) {
        int type = getRandomType();
        this.mSpritePictures[i][j] = new MySprite(type, pX, pY, ((float) this.mTRPic[type].getTileWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRPic[type].getTileHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRPic[type].clone());
        getLayer(this.Layer_Picture).addEntity(this.mSpritePictures[i][j]);
        this.isNeedCheckClear = true;
    }

    private int getScore(int pClearCount, int i, int j, int pType) {
        if (!this.isFirstTouch) {
            return 0;
        }
        if (pClearCount <= 0) {
            return 0;
        }
        if (!this.isFirstClear) {
            this.isFirstClear = true;
            showTimeProgress(true);
            this.mTimeScoreX = System.currentTimeMillis();
        }
        this.mClearTimes++;
        int score = Const.Score[pType] * pClearCount * this.mClearTimes;
        this.mScore += score;
        createTextAddScore(this.mPosition[i][j][0], this.mPosition[i][j][1], score);
        if (this.mClearTimes > 1) {
            createTextXScore(this.mPosition[i][j][0], this.mPosition[i][j][1], this.mClearTimes);
        }
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_CLEAR);
        }
        return score;
    }

    private int checkEnableMoveUp() {
        int pMoveUpCount = 0;
        boolean flag = true;
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            for (int j = 0; j < Const.MAP_WIDTH; j++) {
                if (this.mSpritePictures[i][j] != null) {
                    flag = false;
                }
            }
            if (!flag) {
                break;
            }
            pMoveUpCount++;
        }
        return pMoveUpCount;
    }

    /* access modifiers changed from: private */
    public void doMoveUp() {
        int pMoveUpCount = checkEnableMoveUp();
        if (pMoveUpCount > 0 && checkStatus()) {
            this.mSpriteCrab.animate(200, 5);
            for (int i = 0; i < Const.MAP_HEIGHT; i++) {
                for (int j = 0; j < Const.MAP_WIDTH; j++) {
                    if (i + pMoveUpCount >= Const.MAP_HEIGHT) {
                        createSeaStar(i, j, this.mPosition[i][j][0], this.mPosition[i][j][1]);
                        this.mSpritePictures[i][j].addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
                            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
                            }

                            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                                GameScene.this.resetStatus(arg1);
                            }
                        }, new MoveModifier(0.2f, this.mPosition[i][j][0], this.mPosition[i][j][0], this.mPosition[i][j][1] + (((float) pMoveUpCount) * Const.MAP_TILE_HEIGHT), this.mPosition[i][j][1])));
                    } else {
                        doChange(i, j, i + pMoveUpCount, j);
                    }
                }
            }
        }
    }

    private void doChange(int pFirstRow, int pFirstColumn, int pSecondRow, int pSecondColumn) {
        MySprite firstSprite = this.mSpritePictures[pFirstRow][pFirstColumn];
        MySprite secondSprite = this.mSpritePictures[pSecondRow][pSecondColumn];
        float firstX = this.mPosition[pFirstRow][pFirstColumn][0];
        float firstY = this.mPosition[pFirstRow][pFirstColumn][1];
        float secondX = this.mPosition[pSecondRow][pSecondColumn][0];
        float secondY = this.mPosition[pSecondRow][pSecondColumn][1];
        this.mSpritePictures[pSecondRow][pSecondColumn] = firstSprite;
        this.mSpritePictures[pFirstRow][pFirstColumn] = secondSprite;
        if (this.mSpritePictures[pSecondRow][pSecondColumn] != null) {
            this.mStatus[pSecondRow][pSecondColumn] = 1;
            this.mSpritePictures[pSecondRow][pSecondColumn].addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
                public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                    onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
                }

                public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                    GameScene.this.resetStatus(arg1);
                }
            }, new MoveModifier(0.2f, firstX, secondX, firstY, secondY)));
        }
        if (this.mSpritePictures[pFirstRow][pFirstColumn] != null) {
            this.mStatus[pFirstRow][pFirstColumn] = 1;
            this.mSpritePictures[pFirstRow][pFirstColumn].addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
                public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                    onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
                }

                public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                    GameScene.this.resetStatus(arg1);
                }
            }, new MoveModifier(0.2f, secondX, firstX, secondY, firstY)));
        }
        this.isNeedCheckClear = true;
    }

    /* access modifiers changed from: private */
    public void resetStatus(IShape pIShape) {
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            for (int j = 0; j < Const.MAP_WIDTH; j++) {
                if (this.mSpritePictures[i][j] != null && this.mSpritePictures[i][j].equals(pIShape)) {
                    this.mSpritePictures[i][j].setPosition(this.mPosition[i][j][0], this.mPosition[i][j][1]);
                    this.mStatus[i][j] = 0;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void resetStatusAndRemove(IShape pIShape) {
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            int j = 0;
            while (j < Const.MAP_WIDTH) {
                if (this.mSpritePictures[i][j] == null || !this.mSpritePictures[i][j].equals(pIShape)) {
                    j++;
                } else {
                    this.mStatus[i][j] = 0;
                    this.mSpritePictures[i][j].setVisible(false);
                    synchronized (this.removedSpriteList) {
                        this.removedSpriteList.offer(new RemovedObject(this.mSpritePictures[i][j], this.Layer_Picture));
                    }
                    this.mSpritePictures[i][j] = null;
                    return;
                }
            }
        }
    }

    private boolean checkStatus() {
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            for (int j = 0; j < Const.MAP_WIDTH; j++) {
                if (this.mSpritePictures[i][j] == null) {
                    this.mStatus[i][j] = 0;
                } else if (this.mStatus[i][j] == 1) {
                    return false;
                }
            }
        }
        return true;
    }

    private void checkThereSame(int i, int j, int type) {
        if (i >= 0 && i < Const.MAP_HEIGHT && j >= 0 && j < Const.MAP_WIDTH) {
            if (j - 1 >= 0 && this.mClear[i][j - 1] == 0 && this.mSpritePictures[i][j - 1] != null && this.mSpritePictures[i][j - 1].isVisible() && this.mSpritePictures[i][j - 1].mType == type) {
                this.mClear[i][j - 1] = 1;
                this.sameArrayList.add(new F2FVector(i, j - 1));
                checkThereSame(i, j - 1, type);
            }
            if (j + 1 < Const.MAP_WIDTH && this.mClear[i][j + 1] == 0 && this.mSpritePictures[i][j + 1] != null && this.mSpritePictures[i][j + 1].isVisible() && this.mSpritePictures[i][j + 1].mType == type) {
                this.mClear[i][j + 1] = 1;
                this.sameArrayList.add(new F2FVector(i, j + 1));
                checkThereSame(i, j + 1, type);
            }
            if (i - 1 >= 0 && this.mClear[i - 1][j] == 0 && this.mSpritePictures[i - 1][j] != null && this.mSpritePictures[i - 1][j].isVisible() && this.mSpritePictures[i - 1][j].mType == type) {
                this.mClear[i - 1][j] = 1;
                this.sameArrayList.add(new F2FVector(i - 1, j));
                checkThereSame(i - 1, j, type);
            }
            if (i + 1 < Const.MAP_HEIGHT && this.mClear[i + 1][j] == 0 && this.mSpritePictures[i + 1][j] != null && this.mSpritePictures[i + 1][j].isVisible() && this.mSpritePictures[i + 1][j].mType == type) {
                this.mClear[i + 1][j] = 1;
                this.sameArrayList.add(new F2FVector(i + 1, j));
                checkThereSame(i + 1, j, type);
            }
        }
    }

    /* access modifiers changed from: private */
    public void doMoveDown() {
        if (checkStatus()) {
            int lastRow = 0;
            int lastColumn = 0;
            for (int i = Const.MAP_HEIGHT - 2; i >= 0; i--) {
                int j = 0;
                while (j < Const.MAP_WIDTH) {
                    if (this.mSpritePictures[i][j] != null) {
                        int moveDistance = 0;
                        int k = i + 1;
                        while (k < Const.MAP_HEIGHT && this.mSpritePictures[k][j] == null) {
                            lastRow = k;
                            lastColumn = j;
                            moveDistance++;
                            k++;
                        }
                        if (moveDistance > 0) {
                            doChange(i, j, lastRow, lastColumn);
                        }
                    }
                    j++;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void doCheckTheSame() {
        int totalScore = 0;
        boolean isClear = false;
        if (checkStatus()) {
            if (this.isNeedCheckClear) {
                for (int i = 0; i < Const.MAP_HEIGHT; i++) {
                    for (int j = 0; j < Const.MAP_WIDTH; j++) {
                        if (this.mSpritePictures[i][j] != null && this.mSpritePictures[i][j].isVisible()) {
                            int type = this.mSpritePictures[i][j].mType;
                            int clearCount = clearSame(i, j, type);
                            if (clearCount > 0 && !isClear) {
                                isClear = true;
                            }
                            totalScore += getScore(clearCount, i, j, type);
                            if (this.isNeedCheckClear) {
                                this.isNeedCheckClear = false;
                            }
                        }
                    }
                }
                clearCheckSameArray();
            }
            if (this.mGameMode == 1) {
                getTimeBonusByScore(totalScore);
            }
        }
    }

    private void getTimeBonusByScore(int pScore) {
        if (pScore >= this.mScoreIncrease) {
            int mTimeBonus = pScore / this.mScoreIncrease;
            if (mTimeBonus > 100) {
                mTimeBonus = 100;
            }
            getTimeBonus(mTimeBonus);
        }
    }

    private int clearSame(int pRow, int pColumn, int pType) {
        if (this.mClear[pRow][pColumn] == 1) {
            return 0;
        }
        this.sameArrayList.clear();
        this.sameArrayList.add(new F2FVector(pRow, pColumn));
        this.mClear[pRow][pColumn] = 1;
        checkThereSame(pRow, pColumn, pType);
        if (this.sameArrayList.size() < 3) {
            return 0;
        }
        for (int i = 0; i < this.sameArrayList.size(); i++) {
            setClearModifier(this.sameArrayList.get(i).mRow, this.sameArrayList.get(i).mColumn);
        }
        return this.sameArrayList.size();
    }

    private void setClearModifier(int row, int column) {
        if (this.mSpritePictures[row][column] != null) {
            this.mStatus[row][column] = 1;
            this.mSpritePictures[row][column].setCurrentTileIndex(1);
            this.mSpritePictures[row][column].addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
                public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                    onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
                }

                public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                    GameScene.this.resetStatusAndRemove(arg1);
                }
            }, new AlphaModifier(0.2f, 1.0f, Const.MOVE_LIMITED_SPEED)));
        }
    }

    /* access modifiers changed from: private */
    public void doRemoveEntity() {
        synchronized (this.removedSpriteList) {
            RemovedObject rs = this.removedSpriteList.poll();
            if (rs != null) {
                getLayer(rs._layer).removeEntity(rs._sprite);
            }
        }
    }

    private void clearCheckSameArray() {
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            for (int j = 0; j < Const.MAP_WIDTH; j++) {
                this.mClear[i][j] = 0;
            }
        }
    }

    private void replayGame() {
        this.isTimeLimitedPlaying = false;
        this.isRemainTimeFirst = true;
        this.isFirstTouch = false;
        this.isFirstClear = false;
        this.isFirstXTime = true;
        showTimeProgress(false);
        this.mScore = 0;
        this.mTextScore.setText(getScoreFormartString(this.mScore, 5));
        this.remainTime = 100;
        if (this.mGameMode == 1) {
            this.isFirstUpdateUnlimitedTime = true;
            this.mTimeIncreaseUpdate = System.currentTimeMillis();
            this.mScoreIncrease = 5;
            this.remainTime = 100;
            this.mRemainSecond.setText(String.valueOf(this.remainTime));
        } else if (this.mGameMode == 0) {
            this.remainTime = Const.INITIAL_NOMARL_TIME;
            this.mRemainSecond.setText(getTimeFormart(this.remainTime));
        }
        this.mTimeScoreX = System.currentTimeMillis();
        this.mClearTimes = 0;
        this.mSpriteArrow.setVisible(false);
        this.mSpriteFrame.setVisible(false);
        GoogleAnalyticsUtils.setTracker("/replay");
        if (CommonConst.GAME_MUSIC_ON && this.mContext.mResource != null) {
            this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_BEGINE);
        }
        this.isGameOver = false;
        this.isReplayGame = true;
    }

    /* access modifiers changed from: private */
    public void resetSeaStar() {
        if (this.isReplayGame) {
            this.mStatus = (int[][]) Array.newInstance(Integer.TYPE, Const.MAP_HEIGHT, Const.MAP_WIDTH);
            for (int i = 0; i < Const.MAP_HEIGHT; i++) {
                for (int j = 0; j < Const.MAP_WIDTH; j++) {
                    if (this.mSpritePictures[i][j] != null) {
                        this.mSpritePictures[i][j].clearShapeModifiers();
                        getLayer(this.Layer_Picture).removeEntity(this.mSpritePictures[i][j]);
                        this.mSpritePictures[i][j] = null;
                    }
                    createSeaStar(i, j, this.mPosition[i][j][0], this.mPosition[i][j][1]);
                }
            }
            this.isReplayGame = false;
        }
    }

    private void showFrame(float x, float y) {
        this.mSpriteFrame.clearShapeModifiers();
        this.mSpriteFrame.setAlpha(1.0f);
        this.mSpriteFrame.setPosition(x, y);
        this.mSpriteFrame.setVisible(true);
        this.mSpriteFrame.addShapeModifier(new SequenceShapeModifier(new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape arg1) {
                GameScene.this.mSpriteFrame.setVisible(false);
            }
        }, new AlphaModifier(0.2f, 1.0f, 1.0f), new AlphaModifier(0.2f, 1.0f, Const.MOVE_LIMITED_SPEED)));
    }

    private class F2FVector {
        int mColumn;
        int mRow;

        F2FVector(int pRow, int pColumn) {
            this.mRow = pRow;
            this.mColumn = pColumn;
        }
    }

    private F2FVector getSelectedSeaStar(float pX, float pY) {
        for (int i = 0; i < Const.MAP_HEIGHT; i++) {
            for (int j = 0; j < Const.MAP_WIDTH; j++) {
                if (this.mSpritePictures[i][j] != null && this.mSpritePictures[i][j].isVisible() && pX - this.mSpritePictures[i][j].getX() >= Const.MOVE_LIMITED_SPEED && pX - this.mSpritePictures[i][j].getX() <= Const.MAP_TILE_WIDTH && pY - this.mSpritePictures[i][j].getY() >= Const.MOVE_LIMITED_SPEED && pY - this.mSpritePictures[i][j].getY() <= Const.MAP_TILE_HEIGHT) {
                    return new F2FVector(i, j);
                }
            }
        }
        return null;
    }

    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        if (this.isGameOver) {
            return true;
        }
        if (pSceneTouchEvent.getAction() == 0) {
            if (!this.isFirstTouch) {
                this.isFirstTouch = true;
                this.mTimeScoreX = System.currentTimeMillis();
            }
            saveSceneTouchEvent_Down(pSceneTouchEvent.getX(), pSceneTouchEvent.getY());
        } else if (pSceneTouchEvent.getAction() == 1 && this.isChildSceneClick) {
            this.isChildSceneClick = false;
            return true;
        }
        return true;
    }

    private void saveSceneTouchEvent_Down(float x1, float y1) {
        if (this.sceneTouchdata_down[2] != 2.0f) {
            this.sceneTouchdata_down[0] = x1;
            this.sceneTouchdata_down[1] = y1;
            this.sceneTouchdata_down[2] = 1.0f;
        }
    }

    /* access modifiers changed from: private */
    public void onUpdateSceneTouchEvent_Down() {
        if (this.sceneTouchdata_down[2] == 1.0f) {
            this.sceneTouchdata_down[2] = 2.0f;
            this.mOriginalVector = getSelectedSeaStar(this.sceneTouchdata_down[0], this.sceneTouchdata_down[1]);
            if (this.mOriginalVector != null) {
                showFrame(this.mPosition[this.mOriginalVector.mRow][this.mOriginalVector.mColumn][0], this.mPosition[this.mOriginalVector.mRow][this.mOriginalVector.mColumn][1]);
                setClearModifier(this.mOriginalVector.mRow, this.mOriginalVector.mColumn);
            }
        }
        this.sceneTouchdata_down[2] = 0.0f;
    }

    private class MySprite extends AnimatedSprite {
        int mType = 0;

        public MySprite(int pType, float pX, float pY, float pWidth, float pHeight, TiledTextureRegion pTextureRegion) {
            super(pX, pY, pWidth, pHeight, pTextureRegion);
            this.mType = pType;
        }
    }

    public ContextMenuScene getmContextMenuScene() {
        return this.mContextMenuScene;
    }

    public void setNextLevel() {
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
        }
        GoogleAnalyticsUtils.setTracker("play_start_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/previous_level"));
        this.mContext.getMGameInfo().setMInsideIndex(this.insideIndex + 1);
        this.mContext.startNextLevel();
    }

    private void saveGameInfo(boolean isPassLevel) {
        if (isPassLevel) {
            if (this.mContext.getMGameInfo().updateScoreByIndex(this.level, this.subLevel, this.insideIndex, this.mScore)) {
                this.mContext.getMGameInfo().updateStarByIndex(this.level, this.subLevel, this.insideIndex, this.mContext.getMGameInfo().getStarClassification(this.mStarLevel, this.mScore));
            }
            if (this.insideIndex + 1 == PortConst.LevelInfo[this.level][0]) {
                this.mContext.getMGameInfo().updateEnableByIndex(this.level, this.subLevel + 1, this.insideIndex, true);
            } else if (this.insideIndex + 1 < PortConst.LevelInfo[this.level][0]) {
                this.mContext.getMGameInfo().updateEnableByIndex(this.level, this.subLevel, this.insideIndex + 1, true);
            }
            this.mContext.setGameInfo();
        }
    }

    public void loadGameOverTips() {
        if (this.mScore <= CommonConst.GAME_MAX_SCORE) {
            this.mChildStatus = ChildSceneStatus.GAME_OVER_FAIL;
            setChildScene(new PortraitGameOverDialog(this.mContext.getEngine().getCamera(), this.mContext, false, false, "", CommonConst.GAME_MAX_SCORE, this.mScore, false), false, true, true);
            return;
        }
        CommonConst.GAME_MAX_SCORE = this.mScore;
        this.mContext.getMGameInfo().saveGameInfo(this.mContext);
        this.mChildStatus = ChildSceneStatus.GAME_OVER_OK;
        setChildScene(new PortraitGameOverDialog(this.mContext.getEngine().getCamera(), this.mContext, false, true, "", CommonConst.GAME_MAX_SCORE, this.mScore, false), false, true, true);
    }

    public void showHelpDialog() {
        ImageDialog dialog2 = new ImageDialog(2, this.mContext.getEngine().getCamera(), null, 1.0f, this.mContext);
        disableHud();
        setChildScene(dialog2, false, true, true);
        this.mChildStatus = ChildSceneStatus.GAME_HELP_DIALOG;
    }

    public void onMenuBtn() {
        this.isChildSceneClick = true;
        backToUpMenu();
    }

    public void onReplayBtn() {
        this.isChildSceneClick = true;
        rePlayGame();
    }

    public void onNextLevelBtn() {
        this.isChildSceneClick = true;
        setNextLevel();
    }

    public void onImageDialogCancelBtn() {
        this.isChildSceneClick = true;
        if (this.isFirstPlayGame) {
            showStartHint();
            this.isFirstPlayGame = false;
        }
    }

    public void onTextDialogOKBtn() {
        this.isChildSceneClick = true;
        switch (this.mChildStatus) {
            case GAME_START_DIALOG:
                if (CommonConst.GAME_MUSIC_ON && this.mContext.mResource != null) {
                    this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_BEGINE);
                }
                enableHud();
                return;
            case GAME_OVER_FAIL:
                rePlayGame();
                return;
            case GAME_OVER_OK:
                rePlayGame();
                return;
            case GAME_NEXLLEVEL:
                setNextLevel();
                return;
            case GAME_PASSALLLEVEL:
                rePlayGame();
                return;
            case GAME_PASSALLLEVEL_LITE:
                rePlayGame();
                return;
            default:
                return;
        }
    }

    public void onTextDialogCancelBtn() {
        this.isChildSceneClick = true;
        switch (this.mChildStatus) {
            case GAME_OVER_FAIL:
                backToUpMenu();
                return;
            case GAME_OVER_OK:
            default:
                return;
            case GAME_NEXLLEVEL:
                backToUpMenu();
                return;
            case GAME_PASSALLLEVEL:
                backToUpMenu();
                return;
            case GAME_PASSALLLEVEL_LITE:
                backToUpMenu();
                return;
        }
    }

    public void rePlayGame() {
        replayGame();
    }

    public void clearContextMenu() {
        this.mContextMenuScene.clearContextMenu(F2FGameActivity.Status.GAME);
    }

    public void enableHud() {
        if (this.mHud != null) {
            this.mHud.setVisible(true);
        }
        if (CommonConst.GAME_MUSIC_ON && this.isTimeLimitedPlaying) {
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
        }
    }

    public void disableHud() {
        if (this.mHud != null) {
            this.mHud.setVisible(false);
        }
        if (this.isTimeLimitedPlaying) {
            this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
        }
    }

    public void showContextMenu() {
        disableHud();
        if (this.mContextMenuScene == null) {
            this.mContextMenuScene = new ContextMenuScene(this.mContext.getEngine().getCamera(), this.mContext);
            setChildScene(this.mContextMenuScene, false, true, true);
            return;
        }
        this.mContextMenuScene.loadScene(false);
        setChildScene(this.mContextMenuScene, false, true, true);
    }

    public void backToUpMenu() {
        disableHud();
        if (CommonConst.GAME_MUSIC_ON) {
            this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
        }
        if (PortConst.enableSubLevelOption) {
            this.mContext.setStatus(F2FGameActivity.Status.SUBLEVEL_OPTION);
        } else {
            this.mContext.setStatus(F2FGameActivity.Status.LEVEL_OPTION);
        }
        this.mContext.resetGameScene();
    }

    public void operSound(boolean isPlay) {
        if (!isPlay) {
            this.mContext.mResource.pauseSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
        } else if (CommonConst.GAME_MUSIC_ON && !CommonConst.IS_GAMEOVER && this.isTimeLimitedPlaying && this.remainTime <= Const.REMAIN_TIME) {
            this.mContext.mResource.playSound(Resource.SOUNDTURE.SOUND_LIMITED_TIME);
        }
    }
}
