package com.finger2finger.games.res;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseFontFactory;
import com.finger2finger.games.common.base.BaseStrokeFont;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.youmi.android.AdView;
import org.anddev.andengine.audio.music.Music;
import org.anddev.andengine.audio.music.MusicFactory;
import org.anddev.andengine.audio.sound.Sound;
import org.anddev.andengine.audio.sound.SoundFactory;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXLoadException;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class Resource {
    public F2FGameActivity _context;
    public Engine _engine;
    public Resource _instance;
    public boolean enableReleaseResource = false;
    public boolean loadCommonResourceReady = false;
    public boolean loadGameResourceReady = false;
    public boolean loadLevelOptionResourceReady = false;
    public boolean loadLoadingResourceReady = false;
    public boolean loadLogoResourceReady = false;
    public boolean loadMainMenuResourceReady = false;
    public boolean loadSubLevelOptionResourceReady = false;
    private HashMap<Integer, TextureRegion[]> msArrayTextureRegions = new HashMap<>();
    private HashMap<Integer, TiledTextureRegion[]> msArrayTiledTextureRegions = new HashMap<>();
    private HashMap<Integer, Bitmap> msBitmap = new HashMap<>();
    private HashMap<Integer, Drawable> msDrawable = new HashMap<>();
    private HashMap<Integer, BaseFont> msFont = new HashMap<>();
    private HashMap<Integer, Music> msMusic = new HashMap<>();
    private HashMap<Integer, Sound> msSound = new HashMap<>();
    private HashMap<Integer, BaseStrokeFont> msStrokeFont = new HashMap<>();
    private HashMap<Integer, TextureRegion> msTextureRegions = new HashMap<>();
    private HashMap<Integer, TiledTextureRegion> msTiledTextureRegions = new HashMap<>();
    private HashMap<Integer, TMXTiledMap> msTmxTiledMap = new HashMap<>();

    public static final class drawable {
        public static final int facebook_icon = 2130837508;
    }

    public static final class string {
        public static final int exit_game_dialog_content = 2131034121;
        public static final int exit_game_dialog_no = 2131034115;
        public static final int exit_game_dialog_title = 2131034219;
        public static final int exit_game_dialog_yes = 2131034114;
        public static final int language = 2131034212;
        public static final int str_dilaog_fail = 2131034130;
        public static final int str_dilaog_highscore = 2131034127;
        public static final int str_dilaog_passlevel = 2131034128;
        public static final int str_dilaog_score = 2131034129;
    }

    public Resource(Engine engine, F2FGameActivity context) {
        this._engine = engine;
        this._context = context;
    }

    public void loadMainMenuResource() {
        if (!this.loadMainMenuResourceReady) {
            loadMenuTextures();
            this.loadMainMenuResourceReady = true;
        }
    }

    public void loadGameResource() {
        if (!this.loadGameResourceReady) {
            loadGameTextures();
            this.loadGameResourceReady = true;
        }
    }

    public void loadCommonResource() {
        if (!this.loadCommonResourceReady) {
            loadFont();
            loadStrokeFont();
            loadMusic();
            loadSound();
            loadBitMap();
            this.loadCommonResourceReady = true;
        }
    }

    public void startLoad() {
        new Thread(new Runnable() {
            public void run() {
                Resource.this.loadCommonResource();
                Resource.this.loadMainMenuResource();
                Resource.this.loadGameResource();
            }
        }).start();
    }

    public void startLoadCommonResource() {
        if (this.enableReleaseResource) {
            new Thread(new Runnable() {
                public void run() {
                    Resource.this.loadCommonResource();
                }
            }).start();
        }
    }

    public void startLoadMainMenuResource() {
        if (this.enableReleaseResource) {
            new Thread(new Runnable() {
                public void run() {
                    Resource.this.releaseGameResource();
                    Resource.this.loadMainMenuResource();
                }
            }).start();
        }
    }

    public void startLoadGameResource() {
        if (this.enableReleaseResource) {
            new Thread(new Runnable() {
                public void run() {
                    Resource.this.releaseMainMenuResource();
                    Resource.this.releaseGameResource();
                    Resource.this.loadGameResource();
                }
            }).start();
        }
    }

    public void releaseLogoResource() {
    }

    public void releaseMainMenuResource() {
    }

    public void releaseGameResource() {
    }

    public void releaseResource() {
        this.msTextureRegions = null;
        this.msArrayTextureRegions = null;
        this.msTiledTextureRegions = null;
        this.msArrayTiledTextureRegions = null;
        this.msMusic = null;
        this.msSound = null;
        this.msFont = null;
        this.msStrokeFont = null;
        this.msBitmap = null;
        this.msDrawable = null;
        this.msTmxTiledMap = null;
        this._instance = null;
    }

    public BaseFont getBaseFontByKey(int key) {
        return this.msFont.get(Integer.valueOf(key));
    }

    public BaseStrokeFont getStrokeFontByKey(int key) {
        return this.msStrokeFont.get(Integer.valueOf(key));
    }

    public TextureRegion getTextureRegionByKey(int key) {
        return this.msTextureRegions.get(Integer.valueOf(key));
    }

    public TiledTextureRegion getTiledTextureRegionByKey(int key) {
        return this.msTiledTextureRegions.get(Integer.valueOf(key));
    }

    public TiledTextureRegion[] getArrayTiledTextureRegionByKey(int key) {
        return this.msArrayTiledTextureRegions.get(Integer.valueOf(key));
    }

    public TextureRegion[] getArrayTextureRegionByKey(int key) {
        return this.msArrayTextureRegions.get(Integer.valueOf(key));
    }

    public Bitmap getBitmapByKey(int key) {
        return this.msBitmap.get(Integer.valueOf(key));
    }

    public Drawable getDrawableByKey(int key) {
        return this.msDrawable.get(Integer.valueOf(key));
    }

    public Sound getSoundByKey(int key) {
        return this.msSound.get(Integer.valueOf(key));
    }

    public Music getMusicByKey(int key) {
        return this.msMusic.get(Integer.valueOf(key));
    }

    public TMXTiledMap getTmxTiledMapByKey(int key) {
        return this.msTmxTiledMap.get(Integer.valueOf(key));
    }

    public void loadMenuTextures() {
        Texture texture = new Texture(512, 1024, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.MAINMENU_BACKGROUND.mKey), TextureRegionFactory.createFromAsset(texture, this._context, TEXTURE.MAINMENU_BACKGROUND.mValue, 0, 0));
        Texture gameTitleTexture = new Texture(256, 256, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.MAINMENU_PLAY_BLANK.mKey), TextureRegionFactory.createFromAsset(gameTitleTexture, this._context, TEXTURE.MAINMENU_PLAY_BLANK.mValue, 0, 0));
        if (this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey)) == null || this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey)).getTexture() == null) {
            Texture mCrabTexture = new Texture(256, 128, TextureOptions.DEFAULT);
            this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey), TextureRegionFactory.createTiledFromAsset(mCrabTexture, this._context, TILEDTURE.MAIN_MENU_CRAB.mValue, 0, 0, 2, 1));
            this._engine.getTextureManager().loadTextures(mCrabTexture);
        }
        this._engine.getTextureManager().loadTextures(texture, gameTitleTexture);
    }

    public void unloadMenuTextures() {
        BufferObjectManager.getActiveInstance().clear();
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.MAINMENU_BACKGROUND.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.MAINMENU_PLAY_BLANK.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey)).getTexture());
        this.msTiledTextureRegions.remove(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey));
    }

    public void unloadGameTextures() {
        BufferObjectManager.getActiveInstance().clear();
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_BACKGROUND.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msArrayTiledTextureRegions.get(Integer.valueOf(TILEDTURE.GAME_SEASTAR.mKey))[0].getTexture());
        for (int i = 0; i < 2; i++) {
            this._engine.getTextureManager().unloadTexture(this.msArrayTextureRegions.get(Integer.valueOf(TEXTURE.GAME_CLOUD.mKey))[i].getTexture());
        }
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_FRAME.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_TIMEBAR.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTextureRegions.get(Integer.valueOf(TEXTURE.GAME_SCORE.mKey)).getTexture());
        this._engine.getTextureManager().unloadTexture(this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey)).getTexture());
        this.msTiledTextureRegions.remove(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey));
    }

    public void loadGameTextures() {
        Texture texture = new Texture(512, 1024, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_BACKGROUND.mKey), TextureRegionFactory.createFromAsset(texture, this._context, TEXTURE.GAME_BACKGROUND.mValue, 0, 0));
        Texture mSeastarTexture = new Texture(1024, 64, TextureOptions.DEFAULT);
        TiledTextureRegion[] seastars = new TiledTextureRegion[6];
        for (int i = 0; i < 6; i++) {
            seastars[i] = TextureRegionFactory.createTiledFromAsset(mSeastarTexture, this._context, String.format(TILEDTURE.GAME_SEASTAR.mValue, Integer.valueOf(i + 1)), i * 128, 0, 2, 1);
        }
        this.msArrayTiledTextureRegions.put(Integer.valueOf(TILEDTURE.GAME_SEASTAR.mKey), seastars);
        Texture mCloudTexture = new Texture(512, 256, TextureOptions.DEFAULT);
        TextureRegion[] clouds = new TextureRegion[2];
        for (int i2 = 0; i2 < 2; i2++) {
            clouds[i2] = TextureRegionFactory.createFromAsset(mCloudTexture, this._context, String.format(TEXTURE.GAME_CLOUD.mValue, Integer.valueOf(i2 + 1)), i2 * AdView.DEFAULT_BACKGROUND_TRANS, 0);
        }
        this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.GAME_CLOUD.mKey), clouds);
        Texture texture2 = new Texture(256, 64, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_FRAME.mKey), TextureRegionFactory.createFromAsset(texture2, this._context, TEXTURE.GAME_FRAME.mValue, 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_ARROW.mKey), TextureRegionFactory.createFromAsset(texture2, this._context, TEXTURE.GAME_ARROW.mValue, 64, 0));
        Texture texture3 = new Texture(64, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_TIMEBAR.mKey), TextureRegionFactory.createFromAsset(texture3, this._context, TEXTURE.GAME_TIMEBAR.mValue, 0, 0));
        Texture texture4 = new Texture(256, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_SCORE.mKey), TextureRegionFactory.createFromAsset(texture4, this._context, TEXTURE.GAME_SCORE.mValue, 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_REMAIN.mKey), TextureRegionFactory.createFromAsset(texture4, this._context, TEXTURE.GAME_REMAIN.mValue, 0, 60));
        if (this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey)) == null || this.msTiledTextureRegions.get(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey)).getTexture() == null) {
            Texture mCrabTexture = new Texture(256, 128, TextureOptions.DEFAULT);
            this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.MAIN_MENU_CRAB.mKey), TextureRegionFactory.createTiledFromAsset(mCrabTexture, this._context, TILEDTURE.MAIN_MENU_CRAB.mValue, 0, 0, 2, 1));
            this._engine.getTextureManager().loadTextures(mCrabTexture);
        }
        this._engine.getTextureManager().loadTextures(texture, mCloudTexture, mSeastarTexture, texture2, texture3, texture4);
    }

    public void loadTmxTiledMap() {
        try {
            TMXTiledMap mTMXTiledMap = new TMXLoader(this._context, this._context.getEngine().getTextureManager(), TextureOptions.DEFAULT).loadFromAsset(this._context, String.format(TILEDMAP.TILEDMAP.mValue, Integer.valueOf(this._context.getMGameInfo().getMLevelIndex() + 1), Integer.valueOf(this._context.getMGameInfo().getMInsideIndex() + 1)));
            this.msTmxTiledMap.put(Integer.valueOf(TILEDMAP.TILEDMAP.mKey), mTMXTiledMap);
            this.msTmxTiledMap.put(Integer.valueOf(TILEDMAP.TILEDMAP.mKey), mTMXTiledMap);
        } catch (TMXLoadException e) {
            Log.e("Resource", e.getMessage());
        }
    }

    public void loadMusic() {
        try {
            this.msMusic.put(Integer.valueOf(MUSICTURE.BACKGROUD_MUSIC.mKey), MusicFactory.createMusicFromAsset(this._engine.getMusicManager(), this._context, MUSICTURE.BACKGROUD_MUSIC.mValue));
            this.msMusic.get(Integer.valueOf(MUSICTURE.BACKGROUD_MUSIC.mKey)).setLooping(true);
        } catch (Exception e) {
            Log.e("Resource", e.toString());
        }
    }

    public void loadSound() {
        try {
            this.msSound.put(Integer.valueOf(SOUNDTURE.GAMEOVER_SOUND.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.GAMEOVER_SOUND.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_CLEAR.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_CLEAR.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_BEGINE.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_BEGINE.mValue));
            this.msSound.put(Integer.valueOf(SOUNDTURE.SOUND_LIMITED_TIME.mKey), SoundFactory.createSoundFromAsset(this._engine.getSoundManager(), this._context, SOUNDTURE.SOUND_LIMITED_TIME.mValue));
        } catch (Exception e) {
            Log.e("Resource", e.toString());
        }
    }

    public void loadBitMap() {
        try {
            this.msBitmap.put(Integer.valueOf(BITMAP.FACEBOOK.mKey), BitmapFactory.decodeStream(this._context.getAssets().open(BITMAP.FACEBOOK.mValue)));
        } catch (IOException e) {
            Log.e("Resource", e.toString());
        }
    }

    public void loadFont() {
        BaseFontFactory.setAssetBasePath(Const.FONT_PATH);
        Texture texture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont bf = new BaseFont(texture, Typeface.create(Typeface.DEFAULT, 1), 20.0f, true, -65536, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.SUBLEVEL_SCORE.mKey), bf);
        Texture texture2 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontTextDialogTitle = new BaseFont(texture2, Typeface.create(Typeface.DEFAULT, 1), 30.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.DIALOG_TITLE.mKey), mFontTextDialogTitle);
        Texture texture3 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontHud = new BaseFont(texture3, Typeface.create(Typeface.DEFAULT, 1), 25.0f, true, -16711936, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.HUD_CONTEXT.mKey), mFontHud);
        Texture texture4 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont defaultFont = new BaseFont(texture4, Typeface.create(Typeface.DEFAULT, 1), 28.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.DEFOUT_FONT.mKey), defaultFont);
        this.msFont.put(Integer.valueOf(FONT.DIALOG_CONTEXT.mKey), defaultFont);
        this._engine.getTextureManager().loadTextures(texture, texture3, texture2, texture4);
        this._engine.getFontManager().loadFonts(bf, mFontHud, mFontTextDialogTitle, defaultFont);
        Texture texture5 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontTitle = BaseFontFactory.createFromAsset(texture5, this._context, FontConst.GAMETITLE_FONT_TYPE, 80.0f, true, FontConst.GAMETITLE_FONT_COLOR, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.GAME_TITLE_FONT.mKey), mFontTitle);
        Texture texture6 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontAbout_Title = BaseFontFactory.createFromAsset(texture6, this._context, FontConst.START_FONT_ABOUT_TITLE, 30.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.MAINMENU_ABOUTTITLE.mKey), mFontAbout_Title);
        Texture texture7 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontAbout = BaseFontFactory.createFromAsset(texture7, this._context, FontConst.START_FONT_ABOUT, 18.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.MAINMENU_ABOUTCONTENT.mKey), mFontAbout);
        Texture texture8 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontPlay = BaseFontFactory.createFromAsset(texture8, this._context, FontConst.START_FONT_PLAY, 40.0f, true, -1, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.MAINMENU_PLAY.mKey), mFontPlay);
        Texture texture9 = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontLevelDescrib = BaseFontFactory.createFromAsset(texture9, this._context, FontConst.LEVEL_DESCRIB_FONT, 48.0f, true, -65536, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.LEVELDESCRIB.mKey), mFontLevelDescrib);
        Texture mFontTextureScoreHint = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseFont mFontScoreHint = new BaseFont(mFontTextureScoreHint, Typeface.create(Typeface.DEFAULT, 1), 40.0f, true, -65536, CommonConst.RALE_SAMALL_VALUE);
        this.msFont.put(Integer.valueOf(FONT.XSCORE.mKey), mFontScoreHint);
        this._engine.getTextureManager().loadTextures(texture7, texture6, texture8, texture9, mFontTextureScoreHint, texture5);
        this._engine.getFontManager().loadFonts(mFontAbout, mFontAbout_Title, mFontPlay, mFontLevelDescrib, mFontScoreHint, mFontTitle);
    }

    public void loadStrokeFont() {
        BaseFontFactory.setAssetBasePath(Const.FONT_PATH);
        Texture dialogTitleTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont dialogTitle = BaseFontFactory.createStrokeFromAsset(dialogTitleTexture, this._context, FontConst.DIALOG_TITLE_FONT, 40.0f, true, FontConst.DIALOG_TITLE_IN_COLOR, 2, -16777216, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.DIALOG_TITLE.mKey), dialogTitle);
        this._engine.getTextureManager().loadTexture(dialogTitleTexture);
        this._engine.getFontManager().loadFont(dialogTitle);
        Texture dialogTipTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont dialogTip = BaseFontFactory.createStrokeFromAsset(dialogTipTexture, this._context, FontConst.DIALOG_TIP_FONT, 35.0f, true, -16777216, 2, -1, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.DIALOG_TIP.mKey), dialogTip);
        this._engine.getTextureManager().loadTexture(dialogTipTexture);
        this._engine.getFontManager().loadFont(dialogTip);
        Texture dialogContentTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont dialogContent = BaseFontFactory.createStrokeFromAsset(dialogContentTexture, this._context, FontConst.DIALOG_CONTENT_FONT, 45.0f, true, -1, 2, -16777216, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.DIALOG_CONTENT.mKey), dialogContent);
        this._engine.getTextureManager().loadTexture(dialogContentTexture);
        this._engine.getFontManager().loadFont(dialogContent);
        Texture mGameOverPointFontTexture = new Texture(512, 512, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BaseStrokeFont mGameOverPointFont = BaseFontFactory.createStrokeFromAsset(mGameOverPointFontTexture, this._context, FontConst.GAMEOVER_POINT_FONT, 55.0f, true, -1, 1, -16777216, false, CommonConst.RALE_SAMALL_VALUE);
        this.msStrokeFont.put(Integer.valueOf(STROKEFONT.GAME_POINT_FONT.mKey), mGameOverPointFont);
        this._engine.getFontManager().loadFont(mGameOverPointFont);
        this._engine.getTextureManager().loadTexture(mGameOverPointFontTexture);
    }

    public enum MUSICTURE {
        BACKGROUD_MUSIC(0, "audio/music/backmusic.mp3");
        
        public final int mKey;
        public final String mValue;

        private MUSICTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum SOUNDTURE {
        GAMEOVER_SOUND(1, "audio/sound/gameover.mp3"),
        SOUND_BEGINE(2, "audio/sound/begine.mp3"),
        SOUND_LIMITED_TIME(3, "audio/sound/time.mp3"),
        SOUND_CLEAR(4, "audio/sound/clear.mp3");
        
        public final int mKey;
        public final String mValue;

        private SOUNDTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public void playMusic(MUSICTURE pMusicType) {
        if (this.msMusic.get(Integer.valueOf(pMusicType.mKey)) != null && !this.msMusic.get(Integer.valueOf(pMusicType.mKey)).isPlaying()) {
            this.msMusic.get(Integer.valueOf(pMusicType.mKey)).resume();
        }
    }

    public void pauseMusic(MUSICTURE pMusicType) {
        if (this.msMusic.get(Integer.valueOf(pMusicType.mKey)) != null && this.msMusic.get(Integer.valueOf(pMusicType.mKey)).isPlaying()) {
            this.msMusic.get(Integer.valueOf(pMusicType.mKey)).pause();
        }
    }

    public void realseMusic() {
        for (Map.Entry<Integer, Music> entry : this.msMusic.entrySet()) {
            ((Music) entry.getValue()).stop();
            ((Music) entry.getValue()).release();
        }
    }

    public void realseSound() {
        for (Map.Entry<Integer, Sound> entry : this.msSound.entrySet()) {
            ((Sound) entry.getValue()).stop();
            ((Sound) entry.getValue()).release();
        }
    }

    public void playSound(SOUNDTURE pSoundType) {
        if (this.msSound.get(Integer.valueOf(pSoundType.mKey)) != null) {
            this.msSound.get(Integer.valueOf(pSoundType.mKey)).play();
        }
    }

    public void pauseSound(SOUNDTURE pSoundType) {
        if (this.msSound.get(Integer.valueOf(pSoundType.mKey)) != null) {
            this.msSound.get(Integer.valueOf(pSoundType.mKey)).pause();
        }
    }

    public enum FONT {
        SUBLEVEL_SCORE(0),
        DIALOG_TITLE(1),
        DIALOG_CONTEXT(2),
        HUD_CONTEXT(3),
        MAINMENU_ABOUTTITLE(4),
        MAINMENU_ABOUTCONTENT(5),
        MAINMENU_PLAY(6),
        LEVELDESCRIB(7),
        XSCORE(8),
        DEFOUT_FONT(9),
        GAME_TITLE_FONT(10);
        
        public final int mKey;

        private FONT(int key) {
            this.mKey = key;
        }
    }

    public enum STROKEFONT {
        MAINMENU_TITLE(0),
        DIALOG_TITLE(1),
        DIALOG_TIP(2),
        DIALOG_CONTENT(3),
        GAME_POINT_FONT(4);
        
        public final int mKey;

        private STROKEFONT(int key) {
            this.mKey = key;
        }
    }

    public enum TILEDTURE {
        MAIN_MENU_CRAB(100, "textures/mainmenu/menu_crab.png"),
        GAME_SEASTAR(101, "textures/gameentity/food%d.png");
        
        public final int mKey;
        public final String mValue;

        private TILEDTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum TEXTURE {
        MAINMENU_BACKGROUND(1, "textures/background/mainmenu_bg.png"),
        GAME_BACKGROUND(2, "textures/background/game_bg.png"),
        MAINMENU_PLAY_BLANK(40, "textures/mainmenu/menu_blank.png"),
        GAME_CLOUD(100, "textures/gameentity/cloud%d.png"),
        GAME_ARROW(201, "textures/gameentity/arrow.png"),
        GAME_FRAME(203, "textures/gameentity/frame.png"),
        GAME_REMAIN(205, "textures/gameentity/remain.png"),
        GAME_SCORE(206, "textures/gameentity/score.png"),
        GAME_TIMEBAR(207, "textures/gameentity/timebar.png"),
        GAME_NEW_RECORD(302, "textures/background/new_record.png");
        
        public final int mKey;
        public final String mValue;

        private TEXTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum TILEDMAP {
        TILEDMAP(0, "config/tmx/tmxlevel%d_%d.tmx");
        
        public final int mKey;
        public final String mValue;

        private TILEDMAP(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum BITMAP {
        FACEBOOK(0, "CommonResource/buttons/facebook.png");
        
        public final int mKey;
        public final String mValue;

        private BITMAP(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }
}
