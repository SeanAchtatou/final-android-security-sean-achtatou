package com.finger2finger.games.res;

import com.finger2finger.games.common.CommonConst;

public class Const extends CommonConst {
    public static final int ACTIVE_LEVEL = 0;
    public static final String ADD_SCORE_FORMAT = "+ %d";
    public static final long ADVIEW_INTERVAL = 30000;
    public static final float ALPHA_MODIFIER_TIME = 0.2f;
    public static final int ARROW_LEFT = 0;
    public static final int ARROW_RIGHT = 1;
    public static final String AUDIO_PATH = "audio/";
    public static final String BACKGROUND_PATH = "textures/background.png";
    public static final int BASIC_SPEEDY = 25;
    public static final int BG_ENTITY_COUNT = 10;
    public static final int BG_ENTITY_TYPE_COUNT = 6;
    public static final int BG_LIGHT_COUNT = 20;
    public static final int BRICK_TYPE_COUNT = 7;
    public static float BTN_SPEED = 0.5f;
    public static final float BUTTON_BIG_VALUE = 1.2f;
    public static final float BUTTON_DISTANCE_X = 20.0f;
    public static final float BUTTON_DISTANCE_Y = 15.0f;
    public static final int BUTTON_FINAL_VALUE = 2;
    public static final int CLICK_FINISH = 2;
    public static final int CLICK_RED_NUM = 5;
    public static final int CLOUD_TYPE_COUNT = 2;
    public static final boolean DEBUG_MODE = false;
    public static final float DIALOG_BIG_VALUE = 1.0f;
    public static float DOWN_SPEED_Y_SCALE = 50.0f;
    public static final boolean ENABLE_HUD = false;
    public static final String FONT_PATH = "fonts/";
    public static final boolean FPSLOGGER_DEBUG_MODE = true;
    public static final float FRAME_THICKNESS = 2.0f;
    public static final String GAMEINFO_PREFERENCES = "MonkeyClear";
    public static final float GAMEIN_CAPTION_TIME = 10.0f;
    public static final float GAMEIN_MENU_MOVETIME = 0.5f;
    public static final float GAMEIN_MENU_SCALE = 0.33f;
    public static final float GAME_ABOUT_MOVETIME = 0.5f;
    public static final int GAME_LIFE = 5;
    public static int GAME_MODEID = 0;
    public static final String GAME_NAME = "MonkeyClear";
    public static final float GAME_PASSLEVEL_SCALE = 0.5f;
    public static final float GAME_STARTHINT_SCALE = 0.7f;
    public static final String GFX_BACKGROUND_PATH = "gfx/background/";
    public static final String GFX_BUTTON_PATH = "gfx/button/";
    public static final String GFX_DOLG_PATH = "gfx/dolg/";
    public static final String GFX_HELP_PATH = "gfx/help/";
    public static final String GFX_SNS_PATH = "gfx/sns/";
    public static final int HELP_SIZE_HEIGHT = 256;
    public static final int HELP_SIZE_WIDTH = 256;
    public static float HORIZON_POSITION_X = MOVE_LIMITED_SPEED;
    public static float HORIZON_POSITION_Y = MOVE_LIMITED_SPEED;
    public static float HORIZON_POSITION_Z = MOVE_LIMITED_SPEED;
    public static final int INITIAL_LIMITED_BONUS = 100;
    public static final int INITIAL_NOMARL_TIME = 180;
    public static final int INITIAL_SCORE_INCREASE = 5;
    public static final int INITIAL_TIME_INCREASE = 60000;
    public static final int INITIAL_UNLIMITED_TIME = 100;
    public static int LEVEL_INDEX = 0;
    public static final float LIMITED_SPEED = 5.0f;
    public static final float LINE_BORDER = 5.0f;
    public static final String LOADING_BG_PATH = "textures/gfx/background/loading_bg.png";
    public static final long LOADING_SIMULATE_TIME = 100;
    public static final String LOADING_TEXT_PATH = "textures/gfx/background/loading_text.png";
    public static int MAP_HEIGHT = 10;
    public static float MAP_TILE_HEIGHT = 64.0f;
    public static float MAP_TILE_WIDTH = 64.0f;
    public static final float MAP_UNIT_LENGTH = 32.0f;
    public static int MAP_WIDTH = 6;
    public static final float MENU_CRAB_NORMAL_SPEED = 100.0f;
    public static final float MENU_GHOST_NORMAL_SPEED = 80.0f;
    public static final float MENU_TURTLE_NORMAL_SPEED = 60.0f;
    public static final float MODE_PICTURE_HEIGHT = 400.0f;
    public static final float MODE_PICTURE_WIDHT = 400.0f;
    public static final float MOVE_LIMITED_SPEED = 0.0f;
    public static final float MOVE_MODIFIER_TIME = 0.2f;
    public static final String NAME_PREFERENCES = "MonkeyClear";
    public static float NET_SHIP_DISTANCE = 40.0f;
    public static final boolean ONCE_LOAD_ALL_RESOURES = false;
    public static long PEARL_SHINING_INTERVAL = 1000;
    public static final String PERFERANCE_GAME_SETTINGS = "GameSettings";
    public static final String PERFERANCE_MUSIC_SETTINGS = "MusicSettings";
    public static final String PERFERANCE_SENSOR_X = "Sensor_X";
    public static final String PERFERANCE_SENSOR_Y = "Sensor_Y";
    public static final String PERFERANCE_SENSOR_Z = "Sensor_Z";
    public static final int RANKINGLIST_COUNT = 10;
    public static final String RANKINGLIST_DATA = "RankingListData";
    public static final float[] SCORE_LEVEL_SCALE = {0.8f, 0.9f, 1.0f};
    public static final long SCORE_X_DURATION = 8000;
    public static final boolean SCREEN_LANDSCAPE = false;
    public static final String SEASTAR_PNG_PATH = "seastar/star%d.png";
    public static final int SEASTAR_TYPE_COUNT = 6;
    public static final float SENSOR_MENU_MOVETIME = 0.5f;
    public static final float SENSOR_SAVEHINT_SCALE = 0.6f;
    public static final float SHIP_Y_SCALE = 0.16f;
    public static int[] SMS_LEVEL = {1};
    public static final float SPEACH_ACTIVE_AREA = 100.0f;
    public static final int SPEACH_COUNT = 1;
    public static final long SPEACH_INTERVAL_TIME = 10000;
    public static final float START_ABOUT_MOVESPEED = -0.5f;
    public static final float START_MENUSTONE_MOVETIME = 0.5f;
    public static final float START_MENU_MOVETIME = 1.0f;
    public static final float START_MENU_SCALETIME = 0.8f;
    public static final float START_MENU_SCALE_FROM = 1.0f;
    public static final float START_MENU_SCALE_TO = 1.2f;
    public static int SUBLEVEL_SINGLE_UNIT = 10;
    public static float SUBMARINE_SCALE = 2.0f;
    public static final int[] Score = {1, 1, 2, 2, 3, 3};
    public static final String TEXTURES_PATH = "textures/";
    public static final int TICKER_CHARACTERS_PERSECOND = 5;
    public static final int TICKER_DIALOGBOX_PERSECOND = 10;
    public static final int TIME_LAST_VALUE = 100;
    public static final int TIME_PASS_STEP = 50;
    public static final int TIME_RED_VALUE = 50;
    public static final String URL_FACEBOOK = "http://www.facebook.com/apps/application.php?id=117576181637869&v=wall";
    public static final String URL_REFERANCE_LEVELS = "http://www.finger2finger.com";
    public static final String URL_TWITTER = "https://twitter.com/#!/Finger2finger";
    public static final String URL_TWITTER_LITE = "https://twitter.com/#!/Finger2finger";
    public static final String VERSION_PREFERENCES = "Version";
    public static final long VIBRATE_DURATION = 100;
    public static final String Version = "1.0.102";
    public static final float WAVE_LEFT_Y_SCALE = 0.2f;
    public static final float WAVE_RIGHT_Y_SCALE = 0.2f;
    public static final int WORD_LESS_WIDTH = 50;
    public static final String X_SCORE_FORMAT = "X %d";
    public static final boolean enableGoogleAnalytices = true;
    public static final boolean enableHorizontalDirection = true;
    public static boolean enableSMS = PortConst.enableSMS;
    public static final boolean enableVerticalDirection = true;

    public static class GAME_MODE {
        public static final int GAME_MODE_SHOP = 6;
        public static final int GAME_MODE_STORE = 7;
        public static final int NORMAL = 0;
        public static final int UNLIMITED = 1;
    }

    public static class Music {
        public static final String BACKGROUND = "background.mp3";
        public static final String SOUND_BEGINE = "begine.mp3";
        public static final String SOUND_CRABDEAD = "crabdead.mp3";
        public static final String SOUND_GET_STAR = "star.mp3";
        public static final String SOUND_GET_STONE = "stone.mp3";
        public static final String SOUND_GHOSTDEAD = "ghostdead.mp3";
        public static final String SOUND_LIMITED_TIME = "time.mp3";
        public static final String SOUND_NOPASS = "nopass.mp3";
        public static final String SOUND_PASSLEVEL = "passlevel.mp3";
        public static final String SOUND_TURTLE = "turtle.mp3";
    }

    public static class RectBlankColor {
        public static float alpha = 0.6f;
        public static float blue = 1.0f;
        public static float green = 1.0f;
        public static float red = 1.0f;
    }

    public static class RectDarkColor {
        public static float alpha = 1.0f;
        public static float blue = 0.8392f;
        public static float green = 0.6549f;
        public static float red = 0.0824f;
    }

    public static class SNS {
        public static final String FACEBOOK_APP_ID = "117576181637869";
        public static final String FACEBOOK_FIELD = "uid,name,sex,pic_square";
    }

    public static class Type {
        public static final int BALL_ADD = 13;
        public static final int BALL_POWER = 9;
        public static final int BALL_SPEED_DOWN = 11;
        public static final int BALL_SPEED_UP = 10;
        public static final int BOMB = 8;
        public static final int[] COLLISION_COUNT = {0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0};
        public static final int CRAB_ENLARGE = 12;
        public static final int GOLD = 14;
        public static final int[] GOLD_TYPE = {0, 0, 0, 0, 1, 1, 2, 2, 0, 0, 0, 0, 0, 0};
        public static final int NOTHING = 0;
        public static final int[] SCORE = {0, 10, 10, 10, 20, 20, 30, 30, 0, 0, 0, 0, 0, 0};
        public static final int SEASTAR0 = 1;
        public static final int SEASTAR1 = 2;
        public static final int SEASTAR2 = 3;
        public static final int SEASTAR3 = 4;
        public static final int SEASTAR4 = 5;
        public static final int SEASTAR5 = 6;
        public static final int SEASTAR6 = 7;
    }
}
