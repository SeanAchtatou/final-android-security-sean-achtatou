package com.finger2finger.games.res;

import com.finger2finger.games.common.CommonPortConst;
import com.finger2finger.games.horseclickclear.lite.R;

public class PortConst extends CommonPortConst {
    public static final String AdviewId = "SDK201114110405285w052lb43ibpxq0";
    public static final String GoogleAnalytics = "UA-22417598-14";
    public static final int[][] LevelInfo = {new int[]{10}, new int[]{10}};
    public static final int[] LevelTitle = {R.string.game_mode_01, R.string.game_mode_02};
    public static final boolean enableAds = true;
    public static final boolean enableFaceBook = true;
    public static final boolean enableGoogleAnalytics = true;
    public static final boolean enableInviteFriend = true;
    public static final boolean enableLevelOption = true;
    public static final boolean enableOperatedBySensor = true;
    public static boolean enablePromotion = true;
    public static final boolean enableTwitter = true;
}
