package com.finger2finger.games.common.scene.dialog;

import android.util.Log;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.ShopButton;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class TextDialog extends CameraScene {
    private static int layerOutCount = 3;
    private float btnRale = 1.0f;
    float cancel_x;
    float cancel_y;
    private int enableNextIndex = 0;
    private int index = 0;
    float indistinctLayer_original_x;
    float indistinctLayer_original_y;
    float indistinctLayer_target_x;
    float indistinctLayer_target_y;
    private ChangeableText[] mChangeableText;
    /* access modifiers changed from: private */
    public F2FGameActivity mContext;
    float mDistanceY = 50.0f;
    /* access modifiers changed from: private */
    public F2FScene mF2FScene;
    private float mParentHeight;
    private float mParentWidth;
    private Sprite mSpriteCancel;
    private Sprite mSpriteOK;
    private Rectangle mSpriteOutsideIndistinctLayer;
    private Sprite mSpriteTextRect;
    private String mStrTitle;
    private String[] mStreams;
    private TextureRegion mTRBG;
    private TextureRegion mTRButton1;
    /* access modifiers changed from: private */
    public TextureRegion mTRButton2;
    private String[] mText;
    private Font[] mTextFont;
    private ChangeableText mTextTitle;
    private Font mTitleFont;
    float ok_x;
    float ok_y;
    private int pageCount = 0;
    float rect_x;
    float rect_y;
    float textRect_height;
    float textRect_width;
    float text_x;
    float text_y;

    public TextDialog(int layerCount, Camera camera, String pTitle, String[] pText, Font pTitleFont, Font[] pTextFont, TextureRegion pTRButton1, TextureRegion pTRButton2, F2FGameActivity pContext) {
        super(layerCount, camera);
        initialize(pTitle, pText, pTitleFont, pTextFont, pTRButton1, pTRButton2, pContext);
        loadScene();
    }

    public TextDialog(int layerCount, Camera camera, String pTitle, String[] pText, Font pTitleFont, Font[] pTextFont, TextureRegion pTRButton1, TextureRegion pTRButton2, F2FGameActivity pContext, F2FScene pF2FScene) {
        super(layerCount, camera);
        this.mF2FScene = pF2FScene;
        initialize(pTitle, pText, pTitleFont, pTextFont, pTRButton1, pTRButton2, pContext);
        loadScene();
    }

    public TextDialog(Camera camera, String pTitle, String[] pText, Font pTitleFont, Font[] pTextFont, String pTRButton1, String pTRButton2, F2FGameActivity pContext, F2FScene pF2FScene) {
        super(layerOutCount, camera);
        this.mF2FScene = pF2FScene;
        initialize(pTitle, pText, pTitleFont, pTextFont, pTRButton1, pTRButton2, pContext);
        loadTextScene(pTRButton1, pTRButton2);
    }

    public void initialize(String pTitle, String[] pText, Font pTitleFont, Font[] pTextFont, TextureRegion pTRButton1, TextureRegion pTRButton2, F2FGameActivity pContext) {
        this.mContext = pContext;
        this.mStrTitle = pTitle;
        this.mText = pText;
        if (!(pText == null || pText[this.enableNextIndex] == null)) {
            this.mStreams = pText[this.enableNextIndex].split(CommonConst.SPLIT_CHAPTER);
            this.mChangeableText = new ChangeableText[pText.length];
        }
        if (this.mStreams == null || this.mStreams.length <= 0) {
            this.pageCount = 1;
            this.mStreams = new String[1];
            this.mStreams[0] = pText[0];
        } else {
            this.pageCount = this.mStreams.length;
        }
        this.mTitleFont = pTitleFont;
        this.mTextFont = pTextFont;
        this.mParentWidth = (float) CommonConst.CAMERA_WIDTH;
        this.mParentHeight = (float) CommonConst.CAMERA_HEIGHT;
        this.mTRBG = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey);
        this.mTRButton1 = pTRButton1;
        this.mTRButton2 = pTRButton2;
        if (this.mTRButton2 == null) {
            this.btnRale = 0.8f;
            this.mTRButton1 = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey).clone();
        }
    }

    public void initialize(String pTitle, String[] pText, Font pTitleFont, Font[] pTextFont, String pTRButton1, String pTRButton2, F2FGameActivity pContext) {
        this.mContext = pContext;
        this.mStrTitle = pTitle;
        this.mText = pText;
        if (!(pText == null || pText[this.enableNextIndex] == null)) {
            this.mStreams = pText[this.enableNextIndex].split(CommonConst.SPLIT_CHAPTER);
            this.mChangeableText = new ChangeableText[pText.length];
        }
        if (this.mStreams == null || this.mStreams.length <= 0) {
            this.pageCount = 1;
            this.mStreams = new String[1];
            this.mStreams[0] = pText[0];
        } else {
            this.pageCount = this.mStreams.length;
        }
        this.mTitleFont = pTitleFont;
        this.mTextFont = pTextFont;
        this.mParentWidth = (float) CommonConst.CAMERA_WIDTH;
        this.mParentHeight = (float) CommonConst.CAMERA_HEIGHT;
        this.mTRBG = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey);
        this.btnRale = 0.8f;
        this.mTRButton1 = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey).clone();
        this.mTRButton2 = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey).clone();
    }

    public void loadScene() {
        String txt;
        iniPosition();
        this.mSpriteOutsideIndistinctLayer = new Rectangle(this.indistinctLayer_target_x, this.indistinctLayer_target_y, this.mParentWidth, this.mParentHeight);
        this.mSpriteOutsideIndistinctLayer.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue, CommonConst.GrayColor.alpha);
        this.mSpriteTextRect = new Sprite(this.rect_x, this.rect_y, this.textRect_width, this.textRect_height, this.mTRBG);
        this.mSpriteOK = new Sprite(this.ok_x, this.ok_y, ((float) this.mTRButton1.getWidth()) * CommonConst.RALE_SAMALL_VALUE * this.btnRale, ((float) this.mTRButton1.getHeight()) * CommonConst.RALE_SAMALL_VALUE * this.btnRale, this.mTRButton1) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                if (TextDialog.this.mTRButton2 == null) {
                    TextDialog.this.next();
                    return true;
                }
                TextDialog.this.mContext.getEngine().getScene().clearChildScene();
                TextDialog.this.mContext.getmGameScene().onTextDialogOKBtn();
                return true;
            }
        };
        if (this.mTRButton2 != null) {
            this.mSpriteCancel = new Sprite(this.cancel_x, this.cancel_y, ((float) this.mTRButton2.getWidth()) * 1.0f * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRButton2.getHeight()) * 1.0f * CommonConst.RALE_SAMALL_VALUE, this.mTRButton2) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    TextDialog.this.mContext.getEngine().getScene().clearChildScene();
                    if (TextDialog.this.mF2FScene == null) {
                        TextDialog.this.mContext.getmGameScene().onTextDialogCancelBtn();
                        return true;
                    }
                    TextDialog.this.mF2FScene.onTextDialogCancelBtn();
                    return true;
                }
            };
        }
        this.mTextTitle = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mTitleFont, this.mStrTitle);
        this.text_x = (this.rect_x + (this.textRect_width / 2.0f)) - (this.mTextTitle.getWidth() / 2.0f);
        this.text_y = this.rect_y + (22.0f * CommonConst.RALE_SAMALL_VALUE);
        this.mTextTitle.setPosition(this.text_x, this.text_y);
        this.text_y = this.text_y + (this.mDistanceY * CommonConst.RALE_SAMALL_VALUE) + this.mTextTitle.getHeight();
        if (!(this.mText == null || this.mTextFont == null)) {
            for (int i = 0; i < this.mText.length; i++) {
                if (!(this.mText[i] == null || this.mTextFont[i] == null)) {
                    if (i != this.enableNextIndex || this.index >= this.mStreams.length) {
                        txt = this.mText[i];
                    } else {
                        txt = this.mStreams[this.index];
                        this.index = this.index + 1;
                    }
                    String autoLineMsg = Utils.getAutoLine(this.mTextFont[i], txt, (int) (this.textRect_width - (50.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
                    this.text_x = this.rect_x + ((this.textRect_width - ((float) Utils.getMaxCharacterLens(this.mTextFont[i], autoLineMsg))) / 2.0f);
                    this.mChangeableText[i] = new ChangeableText(this.text_x, this.text_y, this.mTextFont[i], autoLineMsg, this.mTextFont[i].getStringWidth(autoLineMsg + "00000000"));
                    Log.d("f2f", autoLineMsg);
                    getLayer(2).addEntity(this.mChangeableText[i]);
                    this.text_y = this.text_y + (20.0f * CommonConst.RALE_SAMALL_VALUE) + this.mChangeableText[i].getHeight();
                }
            }
        }
        getLayer(1).addEntity(this.mSpriteTextRect);
        getLayer(0).addEntity(this.mSpriteOutsideIndistinctLayer);
        getLayer(1).addEntity(this.mTextTitle);
        getLayer(1).addEntity(this.mSpriteOK);
        if (this.mTRButton2 != null) {
            getLayer(1).addEntity(this.mSpriteCancel);
            registerTouchArea(this.mSpriteCancel);
        } else {
            ChangeableText changeableText = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey), "OK");
            changeableText.setPosition(this.ok_x + ((this.mSpriteOK.getWidth() - changeableText.getWidth()) / 2.0f), this.ok_y + ((this.mSpriteOK.getHeight() - changeableText.getHeight()) / 2.0f));
            getTopLayer().addEntity(changeableText);
        }
        ShopButton shopButton = new ShopButton();
        shopButton.enableMoreGame = false;
        shopButton.showShop(this.mContext, this, getTopLayer().getZIndex(), "Dialog");
        registerTouchArea(this.mSpriteOK);
        setBackgroundEnabled(false);
    }

    public void loadTextScene(String pBtnName1, String pBtnName2) {
        String txt;
        iniPosition();
        this.mSpriteOutsideIndistinctLayer = new Rectangle(this.indistinctLayer_target_x, this.indistinctLayer_target_y, this.mParentWidth, this.mParentHeight);
        this.mSpriteOutsideIndistinctLayer.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue, CommonConst.GrayColor.alpha);
        this.mSpriteTextRect = new Sprite(this.rect_x, this.rect_y, this.textRect_width, this.textRect_height, this.mTRBG);
        float okWidth = ((float) this.mTRButton1.getWidth()) * CommonConst.RALE_SAMALL_VALUE * this.btnRale;
        float okHeight = ((float) this.mTRButton1.getHeight()) * CommonConst.RALE_SAMALL_VALUE * this.btnRale;
        this.mSpriteOK = new Sprite(this.ok_x, this.ok_y, okWidth, okHeight, this.mTRButton1) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                if (TextDialog.this.mTRButton2 == null) {
                    TextDialog.this.next();
                    return true;
                }
                TextDialog.this.mContext.getEngine().getScene().clearChildScene();
                if (TextDialog.this.mF2FScene == null) {
                    TextDialog.this.mContext.getmGameScene().onTextDialogOKBtn();
                    return true;
                }
                TextDialog.this.mF2FScene.onTextDialogOKBtn();
                return true;
            }
        };
        this.mSpriteCancel = new Sprite(this.cancel_x, this.cancel_y, okWidth, okHeight, this.mTRButton2) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                TextDialog.this.mContext.getEngine().getScene().clearChildScene();
                if (TextDialog.this.mF2FScene == null) {
                    TextDialog.this.mContext.getmGameScene().onTextDialogCancelBtn();
                    return true;
                }
                TextDialog.this.mF2FScene.onTextDialogCancelBtn();
                return true;
            }
        };
        this.mSpriteCancel.setPosition(((this.rect_x + this.textRect_width) - this.mSpriteCancel.getWidth()) - (15.0f * CommonConst.RALE_SAMALL_VALUE), this.ok_y);
        this.mTextTitle = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mTitleFont, this.mStrTitle);
        this.text_x = (this.rect_x + (this.textRect_width / 2.0f)) - (this.mTextTitle.getWidth() / 2.0f);
        this.text_y = this.rect_y + (22.0f * CommonConst.RALE_SAMALL_VALUE);
        this.mTextTitle.setPosition(this.text_x, this.text_y);
        this.text_y = this.text_y + (this.mDistanceY * CommonConst.RALE_SAMALL_VALUE) + this.mTextTitle.getHeight();
        if (!(this.mText == null || this.mTextFont == null)) {
            for (int i = 0; i < this.mText.length; i++) {
                if (!(this.mText[i] == null || this.mTextFont[i] == null)) {
                    if (i != this.enableNextIndex || this.index >= this.mStreams.length) {
                        txt = this.mText[i];
                    } else {
                        txt = this.mStreams[this.index];
                        this.index = this.index + 1;
                    }
                    String autoLineMsg = Utils.getAutoLine(this.mTextFont[i], txt, (int) (this.textRect_width - (50.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
                    this.text_x = this.rect_x + ((this.textRect_width - ((float) Utils.getMaxCharacterLens(this.mTextFont[i], autoLineMsg))) / 2.0f);
                    this.mChangeableText[i] = new ChangeableText(this.text_x, this.text_y, this.mTextFont[i], autoLineMsg, this.mTextFont[i].getStringWidth(autoLineMsg + "00000000"));
                    Log.d("f2f", autoLineMsg);
                    getLayer(2).addEntity(this.mChangeableText[i]);
                    this.text_y = this.text_y + (20.0f * CommonConst.RALE_SAMALL_VALUE) + this.mChangeableText[i].getHeight();
                }
            }
        }
        getLayer(1).addEntity(this.mSpriteTextRect);
        getLayer(0).addEntity(this.mSpriteOutsideIndistinctLayer);
        getLayer(1).addEntity(this.mTextTitle);
        getLayer(1).addEntity(this.mSpriteOK);
        getLayer(1).addEntity(this.mSpriteCancel);
        Font mFontCaption = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey);
        ChangeableText changeableText = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, mFontCaption, pBtnName1);
        ChangeableText changeableText2 = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, mFontCaption, pBtnName2);
        changeableText.setPosition(this.mSpriteOK.getX() + ((this.mSpriteOK.getWidth() - changeableText.getWidth()) / 2.0f), this.mSpriteOK.getY() + ((this.mSpriteOK.getHeight() - changeableText.getHeight()) / 2.0f));
        changeableText2.setPosition(this.mSpriteCancel.getX() + ((this.mSpriteCancel.getWidth() - changeableText2.getWidth()) / 2.0f), this.mSpriteCancel.getY() + ((this.mSpriteCancel.getHeight() - changeableText2.getHeight()) / 2.0f));
        getTopLayer().addEntity(changeableText);
        getTopLayer().addEntity(changeableText2);
        ShopButton shopButton = new ShopButton();
        shopButton.enableMoreGame = false;
        shopButton.showShop(this.mContext, this, getTopLayer().getZIndex(), "Dialog");
        registerTouchArea(this.mSpriteOK);
        registerTouchArea(this.mSpriteCancel);
        setBackgroundEnabled(false);
    }

    private void iniPosition() {
        this.indistinctLayer_original_x = Const.MOVE_LIMITED_SPEED - this.mParentWidth;
        this.indistinctLayer_original_y = Const.MOVE_LIMITED_SPEED;
        this.indistinctLayer_target_x = Const.MOVE_LIMITED_SPEED;
        this.indistinctLayer_target_y = Const.MOVE_LIMITED_SPEED;
        this.textRect_width = ((float) this.mTRBG.getWidth()) * CommonConst.RALE_SAMALL_VALUE * 1.0f;
        this.textRect_height = ((float) this.mTRBG.getHeight()) * CommonConst.RALE_SAMALL_VALUE * 1.0f;
        this.rect_x = (this.mParentWidth - this.textRect_width) / 2.0f;
        this.rect_y = (this.mParentHeight - this.textRect_height) / 2.0f;
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
        }
        this.text_x = this.rect_x + 10.0f;
        this.text_y = this.rect_y + 10.0f;
        if (this.mTRButton2 == null) {
            this.ok_x = this.rect_x + (this.textRect_width / 2.0f) + (((((float) this.mTRButton1.getWidth()) * this.btnRale) * CommonConst.RALE_SAMALL_VALUE) / 2.0f);
            this.ok_y = this.rect_y + this.textRect_height;
            return;
        }
        this.ok_x = this.rect_x + (CommonConst.RALE_SAMALL_VALUE * 15.0f);
        this.ok_y = this.rect_y + this.textRect_height;
        this.cancel_x = ((this.rect_x + this.textRect_width) - ((float) this.mTRButton2.getWidth())) - (CommonConst.RALE_SAMALL_VALUE * 15.0f);
        this.cancel_y = this.rect_y + this.textRect_height;
    }

    public void setPosition(int pTextIndex, float pX, float pY) {
        if (pTextIndex >= 0 && pTextIndex < this.mText.length && this.mChangeableText[pTextIndex] != null) {
            this.mChangeableText[pTextIndex].setPosition(pX, pY);
        }
    }

    /* access modifiers changed from: private */
    public void next() {
        if (this.pageCount == 1 || this.index >= this.pageCount) {
            this.mContext.getEngine().getScene().clearChildScene();
            if (this.mF2FScene == null) {
                this.mContext.getmGameScene().onTextDialogOKBtn();
            } else {
                this.mF2FScene.onTextDialogOKBtn();
            }
        } else {
            String autoLineMsg = Utils.getAutoLine(this.mTextFont[this.enableNextIndex], this.mStreams[this.index], (int) (this.textRect_width - (50.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
            this.mChangeableText[this.enableNextIndex].setText(this.mStreams[this.index]);
            this.text_x = this.rect_x + ((this.textRect_width - ((float) Utils.getMaxCharacterLens(this.mTextFont[this.enableNextIndex], autoLineMsg))) / 2.0f);
            this.text_y = this.rect_y + (this.mDistanceY * CommonConst.RALE_SAMALL_VALUE) + this.mTextTitle.getHeight();
            this.mChangeableText[this.enableNextIndex].setPosition(this.text_x, this.text_y);
            this.index++;
        }
    }
}
