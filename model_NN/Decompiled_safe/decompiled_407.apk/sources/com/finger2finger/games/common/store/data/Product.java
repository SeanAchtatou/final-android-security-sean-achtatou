package com.finger2finger.games.common.store.data;

public class Product {
    public static final int LIST_ITEM_COUNT = 6;
    private String comment;
    private int fingerprice = 0;
    private int goldprice = 0;
    private String icon = "";
    private String id = "";
    private String name = "";

    public int getGoldprice() {
        return this.goldprice;
    }

    public int getFingerprice() {
        return this.fingerprice;
    }

    public void setGoldprice(int goldprice2) {
        this.goldprice = goldprice2;
    }

    public void setFingerprice(int fingerprice2) {
        this.fingerprice = fingerprice2;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public String getComment() {
        return this.comment;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public Product(String[] data) throws Exception {
        if (data == null || data.length != 6) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.name = data[1];
        this.icon = data[2];
        this.goldprice = Integer.parseInt(data[3]);
        this.fingerprice = Integer.parseInt(data[4]);
        this.comment = data[5];
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.name).append(TablePath.SEPARATOR_ITEM).append(this.icon).append(TablePath.SEPARATOR_ITEM).append(this.goldprice).append(TablePath.SEPARATOR_ITEM).append(this.fingerprice).append(TablePath.SEPARATOR_ITEM).append(this.comment);
        return sb.toString();
    }
}
