package com.finger2finger.games.common.res;

import android.content.pm.PackageInfo;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseStrokeFont;
import com.finger2finger.games.common.base.ResourceTextureBitMapSource;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.youmi.android.AdView;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.opengl.buffer.BufferObjectManager;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class CommonResource {
    public F2FGameActivity _context;
    public Engine _engine;
    public boolean enableReleaseResource = false;
    private boolean isLoadNumber = false;
    public boolean loadCommonResourceReady = false;
    private HashMap<Integer, TextureRegion[]> msArrayTextureRegions = new HashMap<>();
    private HashMap<Integer, TiledTextureRegion[]> msArrayTiledTextureRegions = new HashMap<>();
    private HashMap<Integer, BaseFont> msFont = new HashMap<>();
    private HashMap<Integer, BaseStrokeFont> msStrokeFont = new HashMap<>();
    private HashMap<Integer, TextureRegion> msTextureRegions = new HashMap<>();
    private HashMap<Integer, TiledTextureRegion> msTiledTextureRegions = new HashMap<>();
    public String pIconPath = "";
    public boolean pIsSDCard = false;

    public CommonResource(Engine engine, F2FGameActivity context) {
        this._engine = engine;
        this._context = context;
    }

    public void loadCommonResource() {
        if (!this.loadCommonResourceReady) {
            loadUnLoadResource();
            this.loadCommonResourceReady = true;
        }
    }

    public void loadUnLoadResource() {
        if (PortConst.enableMoreGame) {
            MoreGameConst.enableInstallGame.clear();
            ArrayList<String> installGameList = getGameInstallName();
            for (int i = 0; i < MoreGameConst.MoreGameInfo.length; i++) {
                if (!checkInstallGame(MoreGameConst.MoreGameInfo[i][2], installGameList)) {
                    MoreGameConst.enableInstallGame.add(new String[]{MoreGameConst.MoreGameInfo[i][0], MoreGameConst.MoreGameInfo[i][1]});
                }
            }
        }
        Texture buttonTexture = new Texture(512, 128, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_BLANK.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_BLANK.mValue, 0, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_CLOSE.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_CLOSE.mValue, 186, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_PLAY.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_PLAY.mValue, 250, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_NEXTLEVEL.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_NEXTLEVEL.mValue, 314, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.STAR_LINGHT.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.STAR_LINGHT.mValue, 378, 0));
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.STAR_UNLINGHT.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.STAR_UNLINGHT.mValue, 424, 64));
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.BUTTON_UP_PAGE.mKey), TextureRegionFactory.createTiledFromAsset(buttonTexture, this._context, TILEDTURE.BUTTON_UP_PAGE.mValue, 232, 64, 2, 1));
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.BUTTON_DOWN_PAGE.mKey), TextureRegionFactory.createTiledFromAsset(buttonTexture, this._context, TILEDTURE.BUTTON_DOWN_PAGE.mValue, 360, 64, 2, 1));
        if (Const.enableRankingList) {
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_RANKINGLIST.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_RANKINGLIST.mValue, 424, 64));
        }
        this._engine.getTextureManager().loadTextures(buttonTexture);
        Texture backgroundTexture = new Texture(1024, 1024, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.LOADING_BG.mKey), TextureRegionFactory.createFromAsset(backgroundTexture, this._context, TEXTURE.LOADING_BG.mValue, 0, 0));
        Texture textTexture = new Texture(512, 128, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.LOADING_TEXT.mKey), TextureRegionFactory.createFromAsset(textTexture, this._context, TEXTURE.LOADING_TEXT.mValue, 0, 0));
        this._engine.getTextureManager().loadTextures(backgroundTexture, textTexture);
        if (!PortConst.enableMoreGame) {
        }
        Texture texturePoint = new Texture(32, 32, TextureOptions.DEFAULT);
        this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.TILED_POINT.mKey), TextureRegionFactory.createTiledFromAsset(texturePoint, this._context, TILEDTURE.TILED_POINT.mValue, 0, 0, 2, 1));
        this._engine.getTextureManager().loadTextures(texturePoint);
        loadCommonProduct();
        Texture newRecordTexture = new Texture(128, 128, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_NEW_RECORD.mKey), TextureRegionFactory.createFromAsset(newRecordTexture, this._context, TEXTURE.GAME_NEW_RECORD.mValue, 0, 0));
        this._engine.getTextureManager().loadTextures(newRecordTexture);
        Texture dialogTexture = new Texture(512, 512, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_DIALOG_BG.mKey), TextureRegionFactory.createFromAsset(dialogTexture, this._context, TEXTURE.GAME_DIALOG_BG.mValue, 0, 0));
        this._engine.getTextureManager().loadTextures(dialogTexture);
    }

    public void loadBtnResource() {
        if (this.msTextureRegions.get(Integer.valueOf(TEXTURE.BUTTON_ABOUT.mKey)) == null || this.msTextureRegions.get(Integer.valueOf(TEXTURE.BUTTON_ABOUT.mKey)).getTexture() == null) {
            Texture buttonTexture = new Texture(512, 256, TextureOptions.DEFAULT);
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_ABOUT.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_ABOUT.mValue, 0, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_REPLAY.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_REPLAY.mValue, 64, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_CLOSE.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_CLOSE.mValue, 128, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_FACEBOOK.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_FACEBOOK.mValue, 192, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_HELP.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_HELP.mValue, 256, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_LEFT.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_LEFT.mValue, 320, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_MENU.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_MENU.mValue, 384, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_INVITE.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_INVITE.mValue, 448, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_TWITTER.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_TWITTER.mValue, 0, 64));
            if (PortConst.enableStoreMode) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.SHOP_ICON.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.SHOP_ICON.mValue, 64, 64));
            }
            this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.BUTTON_MUSIC.mKey), TextureRegionFactory.createTiledFromAsset(buttonTexture, this._context, TILEDTURE.BUTTON_MUSIC.mValue, 128, 64, 2, 1));
            this.msTiledTextureRegions.put(Integer.valueOf(TILEDTURE.BUTTON_LEFT_RIGHT.mKey), TextureRegionFactory.createTiledFromAsset(buttonTexture, this._context, TILEDTURE.BUTTON_LEFT_RIGHT.mValue, 256, 64, 2, 1));
            if (PortConst.enableMoreGame) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.MOREGAME_ICON.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.MOREGAME_ICON.mValue, 384, 64));
            }
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_PAUSE.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_PAUSE.mValue, 448, 64));
            if (Const.enableQQ) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_QQ.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_QQ.mValue, 0, 192));
            }
            if (Const.enableSina) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_SINA.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_SINA.mValue, 64, 192));
            }
            if (Const.enableRenRen) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.BUTTON_RENREN.mKey), TextureRegionFactory.createFromAsset(buttonTexture, this._context, TEXTURE.BUTTON_RENREN.mValue, 128, 192));
            }
            this._engine.getTextureManager().loadTextures(buttonTexture);
        }
    }

    public void unLoadBtnResource() {
        if (this.msTextureRegions.get(Integer.valueOf(TEXTURE.BUTTON_ABOUT.mKey)) != null && this.msTextureRegions.get(Integer.valueOf(TEXTURE.BUTTON_ABOUT.mKey)).getTexture() != null) {
            this._engine.getTextureManager().unloadTexture(getTextureRegionByKey(TEXTURE.BUTTON_ABOUT.mKey).getTexture());
            this.msTextureRegions.remove(Integer.valueOf(TEXTURE.BUTTON_ABOUT.mKey));
        }
    }

    public void loadCommonProduct() {
        if (PortConst.enableStoreMode) {
            Texture[] mProductTexture = new Texture[ShopConst.DATA_PRODUCT.length];
            TextureRegion[] mTRProp = new TextureRegion[ShopConst.DATA_PRODUCT.length];
            for (int i = 0; i < ShopConst.DATA_PRODUCT.length; i++) {
                mProductTexture[i] = new Texture(64, 64, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
                mTRProp[i] = TextureRegionFactory.createFromAsset(mProductTexture[i], this._context, ShopConst.DATA_PRODUCT[i][1], 0, 0);
                this._engine.getTextureManager().loadTexture(mProductTexture[i]);
            }
            this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.GAME_PRODUCT_PIC.mKey), mTRProp);
        }
    }

    public void loadLogoTextures() {
        Texture logoTexture = new Texture(512, 512, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.LOGO_SELF.mKey), TextureRegionFactory.createFromAsset(logoTexture, this._context, TEXTURE.LOGO_SELF.mValue, 0, 0));
        this._engine.getTextureManager().loadTexture(logoTexture);
    }

    public void loadPartnerLogoTextures() {
        if (PortConst.PARTERNERLOGO_PATH.length > 0) {
            Texture parterLogoTexture = new Texture(512, 1024, TextureOptions.DEFAULT);
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.PARTERLOGO.mKey), TextureRegionFactory.createFromAsset(parterLogoTexture, this._context, TEXTURE.PARTERLOGO.mValue, 0, 0));
            this._engine.getTextureManager().loadTexture(parterLogoTexture);
        }
    }

    public void unloadLogoTextures() {
        BufferObjectManager.getActiveInstance().clear();
        this._engine.getTextureManager().unloadTexture(getTextureRegionByKey(TEXTURE.LOGO_SELF.mKey).getTexture());
        if (PortConst.PARTERNERLOGO_PATH.length > 0) {
            this._engine.getTextureManager().unloadTexture(getTextureRegionByKey(TEXTURE.PARTERLOGO.mKey).getTexture());
        }
    }

    public void loadMenuResource() {
        loadBtnResource();
        loadPromotionIcon();
    }

    public void unLoadMenuResource() {
        BufferObjectManager.getActiveInstance().clear();
        unLoadBtnResource();
        unLoadPromotionIcon();
    }

    public void loadGameResource() {
        if (PortConst.enableHelp) {
            Texture helpsTexture = new Texture(512, 512, TextureOptions.DEFAULT);
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.GAME_HELP.mKey), TextureRegionFactory.createFromAsset(helpsTexture, this._context, String.format(TEXTURE.GAME_HELP.mValue, 1), 0, 0));
            this._engine.getTextureManager().loadTextures(helpsTexture);
        }
        loadBtnResource();
    }

    public void unloadGameResource() {
        TextureRegion region;
        BufferObjectManager.getActiveInstance().clear();
        unLoadBtnResource();
        if (PortConst.enableHelp && (region = getTextureRegionByKey(TEXTURE.GAME_HELP.mKey)) != null) {
            this._engine.getTextureManager().unloadTexture(region.getTexture());
        }
    }

    public void loadNumber() {
        if (this.isLoadNumber) {
        }
        if (PortConst.enableSubLevelOption) {
            Texture texture = new Texture(512, 64, TextureOptions.DEFAULT);
            TextureRegion[] numbers = new TextureRegion[10];
            for (int i = 0; i < 10; i++) {
                numbers[i] = TextureRegionFactory.createFromAsset(texture, this._context, String.format(TEXTURE.NUMBER.mValue, Integer.valueOf(i)), i * 44, 0);
            }
            this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.NUMBER.mKey), numbers);
            this._engine.getTextureManager().loadTextures(texture);
        }
        this.isLoadNumber = true;
    }

    public void loadLevelResource() {
        loadBtnResource();
        Texture textureLevelBg = new Texture(1024, 1024, TextureOptions.DEFAULT);
        this.msTextureRegions.put(Integer.valueOf(TEXTURE.LEVEL_BG.mKey), TextureRegionFactory.createFromAsset(textureLevelBg, this._context, TEXTURE.LEVEL_BG.mValue, 0, 0));
        this._engine.getTextureManager().loadTextures(textureLevelBg);
        int textureLength = PortConst.LevelInfo.length;
        Texture[] textures = new Texture[textureLength];
        TextureRegion[] mode = new TextureRegion[textureLength];
        for (int i = 0; i < PortConst.LevelInfo.length; i++) {
            if (!Const.enableDynamicPic) {
                textures[i] = new Texture(512, 512, TextureOptions.DEFAULT);
                mode[i] = TextureRegionFactory.createFromAsset(textures[i], this._context, String.format(Const.levelModePic, Integer.valueOf(i + 1)), 0, 0);
            } else {
                textures[i] = new Texture(1024, 512, TextureOptions.DEFAULT);
                mode[i] = Const.setLevelModeRegion(this._context, textures[i], i);
            }
            this._engine.getTextureManager().loadTexture(textures[i]);
        }
        this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.LEVEL_MODE.mKey), mode);
    }

    public void unloadLevelResource() {
        BufferObjectManager.getActiveInstance().clear();
        unLoadBtnResource();
        TextureRegion region = getTextureRegionByKey(TEXTURE.LEVEL_BG.mKey);
        if (region != null) {
            this._engine.getTextureManager().unloadTexture(region.getTexture());
        }
        TextureRegion[] regionArray = getArrayTextureRegionByKey(TEXTURE.LEVEL_MODE.mKey);
        if (regionArray != null && regionArray.length > 0) {
            int nCount = regionArray.length;
            for (int i = 0; i < nCount; i++) {
                if (regionArray[i] != null) {
                    this._engine.getTextureManager().unloadTexture(regionArray[i].getTexture());
                }
            }
        }
    }

    public void loadSubLevelResource() {
        if (PortConst.enableSubLevelOption) {
            loadBtnResource();
            Texture bg = new Texture(1024, 1024, TextureOptions.DEFAULT);
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.SUBLEVEL_BG.mKey), TextureRegionFactory.createFromAsset(bg, this._context, TEXTURE.SUBLEVEL_BG.mValue, 0, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.SUBLEVEL_ENABLE.mKey), TextureRegionFactory.createFromAsset(bg, this._context, TEXTURE.SUBLEVEL_ENABLE.mValue, 800, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.SUBLEVEL_DISABLE.mKey), TextureRegionFactory.createFromAsset(bg, this._context, TEXTURE.SUBLEVEL_DISABLE.mValue, 900, 0));
            this._engine.getTextureManager().loadTextures(bg);
            loadNumber();
        }
    }

    public void unLoadSubLevelResource() {
        TextureRegion region;
        BufferObjectManager.getActiveInstance().clear();
        if (PortConst.enableSubLevelOption) {
            unLoadBtnResource();
            TextureRegion region2 = getTextureRegionByKey(TEXTURE.SUBLEVEL_BG.mKey);
            if (region2 != null) {
                this._engine.getTextureManager().unloadTexture(region2.getTexture());
            }
            if (getArrayTextureRegionByKey(TEXTURE.NUMBER.mKey) != null && getArrayTextureRegionByKey(TEXTURE.NUMBER.mKey).length > 0 && (region = getArrayTextureRegionByKey(TEXTURE.NUMBER.mKey)[0]) != null) {
                this._engine.getTextureManager().unloadTexture(region.getTexture());
            }
        }
    }

    public void loadMoreGameResource() {
        if (PortConst.enableMoreGame) {
            loadBtnResource();
            Texture moreGameTexture = new Texture(1024, 1024, TextureOptions.DEFAULT);
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.MOREGAME_BG.mKey), TextureRegionFactory.createFromAsset(moreGameTexture, this._context, TEXTURE.MOREGAME_BG.mValue, 0, 0));
            this._engine.getTextureManager().loadTexture(moreGameTexture);
            if (MoreGameConst.enableInstallGame.size() > 0) {
                Texture[] textureMoreGame = new Texture[MoreGameConst.enableInstallGame.size()];
                TextureRegion[] moreGame = new TextureRegion[MoreGameConst.enableInstallGame.size()];
                for (int i = 0; i < MoreGameConst.enableInstallGame.size(); i++) {
                    textureMoreGame[i] = new Texture(128, 128, TextureOptions.DEFAULT);
                    moreGame[i] = TextureRegionFactory.createFromAsset(textureMoreGame[i], this._context, MoreGameConst.enableInstallGame.get(i)[0], 0, 0);
                    this._engine.getTextureManager().loadTexture(textureMoreGame[i]);
                }
                this.msArrayTextureRegions.put(Integer.valueOf(TEXTURE.MORE_GAME.mKey), moreGame);
            }
        }
    }

    public void unloadMoreGameResource() {
        BufferObjectManager.getActiveInstance().clear();
        unLoadBtnResource();
        TextureRegion bgRegion = getTextureRegionByKey(TEXTURE.MOREGAME_BG.mKey);
        if (bgRegion != null) {
            this._engine.getTextureManager().unloadTexture(bgRegion.getTexture());
        }
        TextureRegion[] region = getArrayTextureRegionByKey(TEXTURE.MORE_GAME.mKey);
        if (region != null && region.length > 0) {
            int nCount = region.length;
            for (int i = 0; i < nCount; i++) {
                if (region[i] != null) {
                    this._engine.getTextureManager().unloadTexture(region[i].getTexture());
                }
            }
        }
    }

    public void startLoad() {
        new Thread(new Runnable() {
            public void run() {
                CommonResource.this.loadCommonResource();
            }
        }).start();
    }

    public void releaseResource() {
        this.msTextureRegions = null;
        this.msArrayTextureRegions = null;
        this.msTiledTextureRegions = null;
        this.msArrayTiledTextureRegions = null;
        this.msFont = null;
        this.msStrokeFont = null;
    }

    public BaseFont getBaseFontByKey(int key) {
        return this.msFont.get(Integer.valueOf(key));
    }

    public BaseStrokeFont getStrokeFontByKey(int key) {
        return this.msStrokeFont.get(Integer.valueOf(key));
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public TextureRegion getTextureRegionByKey(int key) {
        if (this.msTextureRegions.get(Integer.valueOf(key)) != null) {
            return this.msTextureRegions.get(Integer.valueOf(key)).clone();
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public TiledTextureRegion getTiledTextureRegionByKey(int key) {
        if (this.msTiledTextureRegions.get(Integer.valueOf(key)) != null) {
            return this.msTiledTextureRegions.get(Integer.valueOf(key));
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public TiledTextureRegion[] getArrayTiledTextureRegionByKey(int key) {
        if (this.msArrayTiledTextureRegions.get(Integer.valueOf(key)) != null) {
            return this.msArrayTiledTextureRegions.get(Integer.valueOf(key));
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 2 */
    public TextureRegion[] getArrayTextureRegionByKey(int key) {
        if (this.msArrayTextureRegions.get(Integer.valueOf(key)) != null) {
            return this.msArrayTextureRegions.get(Integer.valueOf(key));
        }
        return null;
    }

    public enum TILEDTURE {
        BUTTON_MUSIC(0, "CommonResource/buttons/musicOnOff.png"),
        BUTTON_LEFT_RIGHT(1, "CommonResource/buttons/left_right.png"),
        TILED_POINT(2, "CommonResource/background/tiledpoint.png"),
        BUTTON_UP_PAGE(12, "CommonResource/buttons/uppage.png"),
        BUTTON_DOWN_PAGE(13, "CommonResource/buttons/downpage.png");
        
        public final int mKey;
        public final String mValue;

        private TILEDTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public enum TEXTURE {
        LOGO_SELF(0, "CommonResource/logo/self/logo.png"),
        LOADING_BG(1, "CommonResource/loading/loading_bg.png"),
        LOADING_TEXT(2, "CommonResource/loading/loading_text.png"),
        GAME_BACKGROUND(3, "CommonResource/background/background.png"),
        BUTTON_ABOUT(8, "CommonResource/buttons/about.png"),
        BUTTON_REPLAY(9, "CommonResource/buttons/replay.png"),
        BUTTON_CLOSE(10, "CommonResource/buttons/close.png"),
        BUTTON_HELP(11, "CommonResource/buttons/help.png"),
        BUTTON_LEFT(12, "CommonResource/buttons/left.png"),
        BUTTON_MENU(13, "CommonResource/buttons/menu.png"),
        BUTTON_NO(14, "CommonResource/buttons/no.png"),
        BUTTON_PLAY(15, "CommonResource/buttons/play.png"),
        BUTTON_TWITTER(16, "CommonResource/buttons/twitter.png"),
        BUTTON_YES(17, "CommonResource/buttons/yes.png"),
        BUTTON_FACEBOOK(18, "CommonResource/buttons/facebook.png"),
        BUTTON_NEXTLEVEL(21, "CommonResource/buttons/next_btn.png"),
        NUMBER(22, "CommonResource/number/number%02d.png"),
        PARTERLOGO(23, "CommonResource/logo/partner/frame1.png"),
        STAR_LINGHT(24, "CommonResource/buttons/star_light.png"),
        STAR_UNLINGHT(25, "CommonResource/buttons/star_nolight.png"),
        BUTTON_INVITE(26, "CommonResource/buttons/invitefriend.png"),
        BUTTON_BLANK(27, "CommonResource/buttons/menu_blank.png"),
        BUTTON_PAUSE(28, "CommonResource/buttons/pause.png"),
        BUTTON_QQ(29, "CommonResource/buttons/qq.png"),
        BUTTON_RENREN(30, "CommonResource/buttons/renren.png"),
        BUTTON_SINA(31, "CommonResource/buttons/sina.png"),
        BUTTON_RANKINGLIST(32, "CommonResource/buttons/rankinglist.png"),
        LEVEL_BG(50, "CommonResource/background/levelbg.png"),
        LEVEL_MODE(51, "CommonResource/mode/mode_%d.png"),
        SHOP_ICON(55, "CommonResource/buttons/shopbtn.png"),
        MOREGAME_ICON(56, "CommonResource/buttons/moregame.png"),
        SUBLEVEL_BG(100, "CommonResource/background/sublevelbg.png"),
        SUBLEVEL_ENABLE(101, "CommonResource/background/sublevelpic_enable.png"),
        SUBLEVEL_DISABLE(102, "CommonResource/background/sublevelpic_disable.png"),
        MOREGAME_BG(103, "CommonResource/background/sublevelbg.png"),
        HUD_LIFE(AdView.DEFAULT_BACKGROUND_TRANS, "CommonResource/hud/life_bg.png"),
        HUD_SCORE(201, "CommonResource/hud/score_bg.png"),
        HUD_TIME_LEVEL(202, "CommonResource/hud/time_level_bg.png"),
        GAME_DIALOG_BG(300, "CommonResource/background/dolg.png"),
        GAME_HELP(302, CommonConst.HELP_FORMART_PICPATH),
        GAME_NEW_RECORD(301, "CommonResource/background/new_record.png"),
        MORE_GAME(401, ""),
        GAME_PRODUCT_PIC(501, ""),
        PROMOTION_LINKPIC(601, ""),
        PROMOTION_BG(602, ""),
        PROMOTION_ICON(603, "");
        
        public final int mKey;
        public final String mValue;

        private TEXTURE(int key, String value) {
            this.mKey = key;
            this.mValue = value;
        }
    }

    public ArrayList<String> getGameInstallName() {
        ArrayList<String> retValue = new ArrayList<>();
        List<PackageInfo> packs = this._context.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packs.size(); i++) {
            if (packs.get(i).packageName.toLowerCase().startsWith(CommonConst.F2F_GAMES_PACKAGE_NAME)) {
                retValue.add(packs.get(i).packageName.toLowerCase());
            }
        }
        return retValue;
    }

    public boolean checkInstallGame(String packLastName, ArrayList<String> installGameList) {
        if (installGameList == null) {
            return false;
        }
        for (int i = 0; i < installGameList.size(); i++) {
            if (installGameList.get(i).startsWith(CommonConst.F2F_GAMES_PACKAGE_NAME + "." + packLastName)) {
                return true;
            }
        }
        return false;
    }

    public void loadPromotionSource(boolean pIsSDStage, boolean pIsPortScreen, String pBgPath, String pActionPath) {
        Texture bgTexture = new Texture(1024, 1024, TextureOptions.DEFAULT);
        if (pIsSDStage) {
            ResourceTextureBitMapSource textureSource = new ResourceTextureBitMapSource(this._context, pBgPath);
            ResourceTextureBitMapSource textureactionSource = new ResourceTextureBitMapSource(this._context, pActionPath);
            if (!pIsPortScreen) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_BG.mKey), TextureRegionFactory.createFromSource(bgTexture, textureSource, 0, 0));
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_LINKPIC.mKey), TextureRegionFactory.createFromSource(bgTexture, textureactionSource, 0, 480));
            } else {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_BG.mKey), TextureRegionFactory.createFromSource(bgTexture, textureSource, 0, 0));
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_LINKPIC.mKey), TextureRegionFactory.createFromSource(bgTexture, textureactionSource, 480, 0));
            }
        } else if (!pIsPortScreen) {
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_BG.mKey), TextureRegionFactory.createFromAsset(bgTexture, this._context, pBgPath, 0, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_LINKPIC.mKey), TextureRegionFactory.createFromAsset(bgTexture, this._context, pActionPath, 0, 480));
        } else {
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_BG.mKey), TextureRegionFactory.createFromAsset(bgTexture, this._context, pBgPath, 0, 0));
            this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_LINKPIC.mKey), TextureRegionFactory.createFromAsset(bgTexture, this._context, pActionPath, 480, 0));
        }
        this._engine.getTextureManager().loadTextures(bgTexture);
    }

    public void unLoadPromotionSource() {
        TextureRegion resoureRegion = getTextureRegionByKey(TEXTURE.PROMOTION_BG.mKey);
        if (resoureRegion != null && resoureRegion.getTexture() != null) {
            this._engine.getTextureManager().unloadTexture(resoureRegion.getTexture());
        }
    }

    public void loadPromotionIcon() {
        if (this.pIconPath != null && !this.pIconPath.equals("")) {
            Texture iconTexture = new Texture(128, 128, TextureOptions.DEFAULT);
            if (!this.pIsSDCard) {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_ICON.mKey), TextureRegionFactory.createFromAsset(iconTexture, this._context, this.pIconPath, 0, 0));
            } else {
                this.msTextureRegions.put(Integer.valueOf(TEXTURE.PROMOTION_ICON.mKey), TextureRegionFactory.createFromSource(iconTexture, new ResourceTextureBitMapSource(this._context, this.pIconPath), 0, 0));
            }
            this._engine.getTextureManager().loadTextures(iconTexture);
        }
    }

    public void unLoadPromotionIcon() {
        TextureRegion resoureRegion = getTextureRegionByKey(TEXTURE.PROMOTION_ICON.mKey);
        if (resoureRegion != null && resoureRegion.getTexture() != null) {
            this._engine.getTextureManager().unloadTexture(resoureRegion.getTexture());
        }
    }
}
