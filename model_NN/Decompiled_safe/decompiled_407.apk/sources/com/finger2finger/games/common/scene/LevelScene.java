package com.finger2finger.games.common.scene;

import android.content.res.Resources;
import android.widget.Toast;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.ShopButton;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseAnimatedSprite;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.res.SMSConst;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class LevelScene extends F2FScene {
    private static final float modeDifPicHei = 250.0f;
    private static final float modePicHei = 300.0f;
    private static final float modePicWid = 300.0f;
    private final int Layer_Zero = 0;
    private int clickLevelIndex = -1;
    private float curretPX = Const.MOVE_LIMITED_SPEED;
    private float curretPY = Const.MOVE_LIMITED_SPEED;
    /* access modifiers changed from: private */
    public long gameupdateTime;
    private boolean isFirstShowSMSDialog = true;
    /* access modifiers changed from: private */
    public boolean isMove = true;
    /* access modifiers changed from: private */
    public boolean isRemainTimeFirst = true;
    private float mDistanceModePic = Const.MOVE_LIMITED_SPEED;
    private ArrayList<Integer> mEffectLevelIndex = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean mEnableOprate = false;
    private Font mFont;
    private int mLevelCount = 0;
    private Text[] mModeDescribe;
    private float mModeStartPositionX = Const.MOVE_LIMITED_SPEED;
    private float mModeStartPositionY = Const.MOVE_LIMITED_SPEED;
    private BaseSprite mSpriteBack;
    private BaseSprite[] mSpriteMode;
    private BaseAnimatedSprite[] mSpritePoint;
    private String[] mStrModeDescrib;
    private TextureRegion[] mTextureRegionMode;
    /* access modifiers changed from: private */
    public int remainTime = 120;
    private float targetPX = Const.MOVE_LIMITED_SPEED;
    private float targetPY = Const.MOVE_LIMITED_SPEED;

    public LevelScene(F2FGameActivity pContext) {
        super(2, pContext);
        loadResource();
        iniBackGroud();
        iniBackButton();
        new ShopButton().showShop(pContext, this, 1, "LevelScene");
        iniModeSprite();
        iniStatusPoint();
        enableSceneTouch();
        registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.finger2finger.games.common.scene.LevelScene.access$102(com.finger2finger.games.common.scene.LevelScene, boolean):boolean
             arg types: [com.finger2finger.games.common.scene.LevelScene, int]
             candidates:
              com.finger2finger.games.common.scene.F2FScene.access$102(com.finger2finger.games.common.scene.F2FScene, android.view.MotionEvent):android.view.MotionEvent
              com.finger2finger.games.common.scene.LevelScene.access$102(com.finger2finger.games.common.scene.LevelScene, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.finger2finger.games.common.scene.LevelScene.access$502(com.finger2finger.games.common.scene.LevelScene, boolean):boolean
             arg types: [com.finger2finger.games.common.scene.LevelScene, int]
             candidates:
              com.finger2finger.games.common.scene.F2FScene.access$502(com.finger2finger.games.common.scene.F2FScene, org.anddev.andengine.input.touch.TouchEvent):org.anddev.andengine.input.touch.TouchEvent
              com.finger2finger.games.common.scene.LevelScene.access$502(com.finger2finger.games.common.scene.LevelScene, boolean):boolean */
            public void onUpdate(float pSecondsElapsed) {
                LevelScene.this.checkSMSStatus();
                if (!LevelScene.this.mEnableOprate) {
                    if (LevelScene.this.isRemainTimeFirst) {
                        int unused = LevelScene.this.remainTime = 1;
                        long unused2 = LevelScene.this.gameupdateTime = System.currentTimeMillis();
                        boolean unused3 = LevelScene.this.isRemainTimeFirst = false;
                    }
                    if (System.currentTimeMillis() - LevelScene.this.gameupdateTime >= 200) {
                        long unused4 = LevelScene.this.gameupdateTime = System.currentTimeMillis();
                        int unused5 = LevelScene.this.remainTime = LevelScene.this.remainTime - 1;
                    }
                    if (LevelScene.this.remainTime <= 0) {
                        boolean unused6 = LevelScene.this.mEnableOprate = true;
                        boolean unused7 = LevelScene.this.isMove = false;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void checkSMSStatus() {
        if (!Const.enableSMS) {
            return;
        }
        if (this.isSMSSendCanle) {
            this.isSMSSendCanle = false;
            this.clickLevelIndex = -1;
            GoogleAnalyticsUtils.setTracker("/Send_SMS/cancle");
        } else if (this.clickLevelIndex != -1) {
            if (this.isFirstShowSMSDialog) {
                this.mContext.showMsgDialog(19);
                GoogleAnalyticsUtils.setTracker("/Send_SMS/send_in_level_" + String.valueOf(this.clickLevelIndex + 1));
                this.isFirstShowSMSDialog = false;
            }
            if (SMSConst.SMS_STATUS == 0) {
                this.mContext.saveSMSSettings(String.valueOf(this.clickLevelIndex));
                this.mContext.showMsgDialog(20);
            } else if (SMSConst.SMS_STATUS == 1) {
                this.isFirstShowSMSDialog = true;
                this.mContext.showMsgDialog(18);
            }
        }
    }

    public void setSMSNextProcess() {
        moveToNext(this.clickLevelIndex);
    }

    public void loadScene() {
    }

    private void LoadExtras() {
        this.mLevelCount = this.mTextureRegionMode.length;
        for (int i = 0; i < this.mLevelCount; i++) {
            if (i >= PortConst.LevelInfo.length) {
                this.mEffectLevelIndex.add(Integer.valueOf(i));
            } else if (PortConst.LevelInfo[i][0] > 0) {
                this.mEffectLevelIndex.add(Integer.valueOf(i));
            }
        }
        this.mLevelCount = this.mEffectLevelIndex.size();
        this.mSpriteMode = new BaseSprite[this.mLevelCount];
        this.mModeDescribe = new Text[this.mLevelCount];
        this.mStrModeDescrib = new String[this.mLevelCount];
        this.mSpritePoint = new BaseAnimatedSprite[this.mLevelCount];
    }

    private void loadResource() {
        loadTextrue();
        LoadExtras();
        loadFont();
        loadStringsFromXml();
    }

    private void loadFont() {
        this.mFont = this.mContext.mResource.getBaseFontByKey(Resource.FONT.LEVELDESCRIB.mKey);
    }

    private void loadStringsFromXml() {
        Resources res = this.mContext.getResources();
        for (int i = 0; i < this.mLevelCount; i++) {
            this.mStrModeDescrib[i] = res.getString(PortConst.LevelTitle[this.mEffectLevelIndex.get(i).intValue()]);
        }
    }

    private void loadTextrue() {
        this.mTextureRegionMode = this.mContext.commonResource.getArrayTextureRegionByKey(CommonResource.TEXTURE.LEVEL_MODE.mKey);
    }

    private void iniBackGroud() {
        getLayer(0).addEntity(new Sprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.LEVEL_BG.mKey)));
    }

    private void iniBackButton() {
        TextureRegion mTRBack = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_LEFT.mKey);
        this.mSpriteBack = new BaseSprite(((float) mTRBack.getWidth()) * CommonConst.RALE_SAMALL_VALUE * 0.2f, ((float) CommonConst.CAMERA_HEIGHT) - ((((float) mTRBack.getHeight()) * CommonConst.RALE_SAMALL_VALUE) * 1.2f), CommonConst.RALE_SAMALL_VALUE, mTRBack, false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                LevelScene.this.backToMenu();
                return true;
            }
        };
        registerTouchArea(this.mSpriteBack);
        getTopLayer().addEntity(this.mSpriteBack);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    private void iniModeSprite() {
        if (this.mLevelCount > 0) {
            float start_X = (((float) CommonConst.CAMERA_WIDTH) - (CommonConst.RALE_SAMALL_VALUE * 300.0f)) / 2.0f;
            float start_Y = (((float) CommonConst.CAMERA_HEIGHT) - (CommonConst.RALE_SAMALL_VALUE * 300.0f)) / 2.0f;
            this.mModeStartPositionY = start_Y;
            this.mDistanceModePic = (float) (CommonConst.CAMERA_HEIGHT / 2);
            for (int i = 0; i < this.mLevelCount; i++) {
                this.mSpriteMode[i] = new BaseSprite(start_X, start_Y, CommonConst.RALE_SAMALL_VALUE, this.mTextureRegionMode[this.mEffectLevelIndex.get(i).intValue()], false);
                if (Const.enableDynamicPic && !(((float) this.mTextureRegionMode[this.mEffectLevelIndex.get(i).intValue()].getWidth()) == 300.0f && ((float) this.mTextureRegionMode[this.mEffectLevelIndex.get(i).intValue()].getHeight()) == 300.0f)) {
                    this.mSpriteMode[i].setWidth(CommonConst.RALE_SAMALL_VALUE * 300.0f);
                    this.mSpriteMode[i].setHeight(modeDifPicHei * CommonConst.RALE_SAMALL_VALUE);
                }
                this.mModeDescribe[i] = new Text(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mFont, this.mStrModeDescrib[i]);
                float describeY = this.mSpriteMode[i].getY() + ((this.mSpriteMode[i].getHeight() * 3.5f) / 5.0f);
                if (Const.enableDynamicPic) {
                    describeY = this.mSpriteMode[i].getY() + this.mSpriteMode[i].getHeight();
                }
                this.mModeDescribe[i].setPosition((this.mSpriteMode[i].getX() + (this.mSpriteMode[i].getWidth() / 2.0f)) - (this.mModeDescribe[i].getWidth() / 2.0f), describeY);
                start_Y += (float) (CommonConst.CAMERA_HEIGHT / 2);
                getTopLayer().addEntity(this.mSpriteMode[i]);
                getTopLayer().addEntity(this.mModeDescribe[i]);
            }
        }
    }

    private void iniStatusPoint() {
        TiledTextureRegion mTTRPoint = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.TILED_POINT.mKey);
        float distance_X = 20.0f * CommonConst.RALE_SAMALL_VALUE;
        float start_X = (((float) CommonConst.CAMERA_WIDTH) - (((((float) mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE) * ((float) this.mLevelCount)) + (((float) (this.mLevelCount - 1)) * distance_X))) / 2.0f;
        float start_Y = ((float) CommonConst.CAMERA_HEIGHT) - ((((float) mTTRPoint.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) * 2.0f);
        for (int i = 0; i < this.mLevelCount; i++) {
            this.mSpritePoint[i] = new BaseAnimatedSprite(start_X, start_Y, CommonConst.RALE_SAMALL_VALUE, mTTRPoint.clone(), false);
            start_X = start_X + distance_X + ((float) mTTRPoint.getTileWidth());
            getTopLayer().addEntity(this.mSpritePoint[i]);
            if (i != 0) {
                this.mSpritePoint[i].setCurrentTileIndex(1);
            }
        }
    }

    private boolean checkInTouch(BaseSprite pBaseSprite, float pTouchX, float pTouchY) {
        if (pBaseSprite == null) {
            return false;
        }
        return pTouchX >= pBaseSprite.getX() && pTouchX <= pBaseSprite.getX() + pBaseSprite.getWidth() && pTouchY >= pBaseSprite.getY() && pTouchY <= pBaseSprite.getY() + pBaseSprite.getHeight();
    }

    public void handleTouchEvent(ArrayList<TouchEvent> touchEvents) {
        for (int i = 0; i < touchEvents.size(); i++) {
            TouchEvent pSceneTouchEvent = touchEvents.get(i);
            if (pSceneTouchEvent != null) {
                if (pSceneTouchEvent.getAction() == 0) {
                    this.targetPX = pSceneTouchEvent.getX();
                    this.targetPY = pSceneTouchEvent.getY();
                } else if (pSceneTouchEvent.getAction() != 1) {
                    continue;
                } else if (this.isChildSceneClick) {
                    this.isChildSceneClick = false;
                    return;
                } else if (!this.isMove) {
                    this.curretPX = pSceneTouchEvent.getX();
                    this.curretPY = pSceneTouchEvent.getY();
                    if (Math.abs(this.targetPY - this.curretPY) < CommonConst.MIN_TOUCH_DEFAUT_Y * CommonConst.RALE_SAMALL_VALUE) {
                        int j = 0;
                        while (j < this.mLevelCount) {
                            if (this.mSpriteMode[j] == null || !checkInTouch(this.mSpriteMode[j], this.targetPX, this.targetPY) || !checkInTouch(this.mSpriteMode[j], this.curretPX, this.curretPY)) {
                                j++;
                            } else {
                                onAreaTouch(this.mEffectLevelIndex.get(j).intValue());
                                return;
                            }
                        }
                        continue;
                    } else if (Math.abs(this.targetPY - this.curretPY) >= CommonConst.MIN_TOUCH_DEFAUT_Y * CommonConst.RALE_SAMALL_VALUE) {
                        move(this.targetPX, this.targetPY, this.curretPX, this.curretPY);
                        updateStatusPoint();
                    }
                } else {
                    return;
                }
            }
        }
    }

    private boolean checkIsNeedSMS(int index) {
        int i = 0;
        while (i < Const.SMS_LEVEL.length) {
            if (index != Const.SMS_LEVEL[i]) {
                i++;
            } else if (!this.mContext.loadSMSPreferences(String.valueOf(index))) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void onAreaTouch(int i) {
        if (Const.enableSMS && checkIsNeedSMS(i)) {
            this.isSMSSendCanle = false;
            this.mContext.showMsgDialog(17);
            this.clickLevelIndex = i;
        } else if (!CommonConst.GAME_STORAGE_MODE_ON || i != CommonConst.GAME_STORAGE_MODE_LEVEL) {
            moveToNext(i);
        } else {
            this.mContext.setStatus(F2FGameActivity.Status.GAME_STORAGE);
        }
    }

    private void moveToNext(int i) {
        if (PortConst.LevelInfo != null && i < PortConst.LevelInfo.length) {
            if (PortConst.LevelInfo[i][0] > 0) {
                this.mContext.commonResource.unloadLevelResource();
                Const.GAME_MODEID = i;
                this.mContext.getMGameInfo().setMLevelIndex(i);
                if (PortConst.enableSubLevelOption) {
                    this.mContext.setStatus(F2FGameActivity.Status.SUBLEVEL_OPTION);
                } else {
                    this.mContext.setStatus(F2FGameActivity.Status.GAME);
                }
            } else {
                Toast.makeText(this.mContext, this.mContext.getResources().getString(R.string.Barrier_shielding), 1).show();
            }
        }
    }

    private void move(float orginal_x, float orginal_y, float target_x, float target_y) {
        float distance;
        boolean isUp = target_y - orginal_y <= Const.MOVE_LIMITED_SPEED;
        if (!checkOutSide(isUp)) {
            if (isUp) {
                distance = (float) ((-CommonConst.CAMERA_HEIGHT) / 2);
            } else {
                distance = (float) (CommonConst.CAMERA_HEIGHT / 2);
            }
            for (int i = 0; i < this.mLevelCount; i++) {
                this.mSpriteMode[i].setPosition(this.mSpriteMode[i].getX(), this.mSpriteMode[i].getY() + distance);
                this.mModeDescribe[i].setPosition(this.mModeDescribe[i].getX(), this.mSpriteMode[i].getY() + ((this.mSpriteMode[i].getHeight() * 3.5f) / 5.0f));
            }
            updateStatusPoint();
        }
    }

    private void updatePositon() {
        boolean isUp;
        if (this.mLevelCount > 0) {
            float distance = this.mSpriteMode[0].getY() - this.mModeStartPositionY;
            if (distance < Const.MOVE_LIMITED_SPEED) {
                isUp = true;
            } else {
                isUp = false;
            }
            int m = (int) (Math.abs(distance) / this.mDistanceModePic);
            if ((Math.abs(distance) % this.mDistanceModePic) / this.mDistanceModePic >= 0.5f) {
                m++;
            }
            setPositionFromBefore(isUp ? this.mModeStartPositionY - (((float) m) * this.mDistanceModePic) : this.mModeStartPositionY + (((float) m) * this.mDistanceModePic));
        }
    }

    private boolean checkOutSide(boolean isLeft) {
        if (!isLeft && this.mSpriteMode != null && this.mSpriteMode[0] != null && this.mSpriteMode[0].getY() + (this.mSpriteMode[0].getHeight() / 2.0f) + ((float) (CommonConst.CAMERA_HEIGHT / 2)) >= ((float) CommonConst.CAMERA_HEIGHT)) {
            return true;
        }
        if (!isLeft || this.mSpriteMode == null || this.mSpriteMode[this.mLevelCount - 1] == null || this.mSpriteMode[this.mLevelCount - 1].getY() - ((float) (CommonConst.CAMERA_HEIGHT / 2)) > Const.MOVE_LIMITED_SPEED) {
            return false;
        }
        return true;
    }

    private void updateStatusPoint() {
        boolean isFind = false;
        for (int i = 0; i < this.mLevelCount; i++) {
            if (this.mSpriteMode[i].getY() <= Const.MOVE_LIMITED_SPEED || this.mSpriteMode[i].getY() + this.mSpriteMode[i].getHeight() >= ((float) CommonConst.CAMERA_HEIGHT)) {
                this.mSpritePoint[i].setCurrentTileIndex(1);
            } else if (!isFind) {
                this.mSpritePoint[i].setCurrentTileIndex(0);
                isFind = true;
            } else {
                this.mSpritePoint[i].setCurrentTileIndex(1);
            }
        }
    }

    private void setPositionFromBefore(float position) {
        float start_X = position;
        float start_Y = position;
        for (int i = 0; i < this.mLevelCount; i++) {
            this.mSpriteMode[i].setPosition(this.mSpriteMode[i].getX(), start_Y);
            this.mModeDescribe[i].setPosition(this.mSpriteMode[i].getX(), this.mSpriteMode[i].getY() + ((this.mSpriteMode[i].getHeight() * 3.5f) / 5.0f));
            start_X += this.mDistanceModePic;
        }
    }

    public void backToMenu() {
        this.isMove = true;
        this.mContext.setStatus(F2FGameActivity.Status.MAINMENU);
    }
}
