package com.finger2finger.games.common.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.adview.AdViewLayout;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.F2FMailSender;
import com.finger2finger.games.common.GameInfo;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.message.F2FMessage;
import com.finger2finger.games.common.res.CommonRankingList;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.res.MoreGameConst;
import com.finger2finger.games.common.res.PromotionHandler;
import com.finger2finger.games.common.res.SMSConst;
import com.finger2finger.games.common.res.ShopConst;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.common.scene.LevelScene;
import com.finger2finger.games.common.scene.LoadingScene;
import com.finger2finger.games.common.scene.LogoScene;
import com.finger2finger.games.common.scene.MoreGameScene;
import com.finger2finger.games.common.scene.SubLevelScene;
import com.finger2finger.games.common.scene.dialog.F2FAlertDialog;
import com.finger2finger.games.common.store.activity.ShopActivity;
import com.finger2finger.games.common.store.data.PersonalAccount;
import com.finger2finger.games.common.store.data.PersonalAccountTable;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.common.store.io.TableLoad;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import com.finger2finger.games.scene.GameScene;
import com.finger2finger.games.scene.MainMenuScene;
import java.util.ArrayList;
import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.FixedStepEngine;
import org.anddev.andengine.engine.camera.SmoothCamera;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.opengl.view.RenderSurfaceView;

public class F2FGameActivity extends F2FBaseGameActivity {
    private static TableLoad tableLoad;
    AdViewLayout adViewLayout;
    public CommonResource commonResource = null;
    public boolean enableShowWireLess = true;
    private boolean isFirstPlayMusic = true;
    public boolean isLoadingCompleted = false;
    private boolean isUnloadLogo = false;
    RelativeLayout.LayoutParams layoutParams;
    private long loadingDelay = 0;
    private BroadcastReceiver mBroadcastReceiver;
    private GameInfo mGameInfo;
    public boolean mGamePaused = false;
    public GameScene mGameScene;
    public long mGameStartMillis = 0;
    public F2FScene mGameStorageScene;
    private boolean mIsloadScene = false;
    public Status mLastStatus = Status.MAINMENU;
    private LevelScene mLevelOptionScene;
    private LoadingScene mLoadingScene;
    public MainMenuScene mMainMenuScene;
    public ArrayList<F2FMessage> mMessageQueue = new ArrayList<>();
    private MoreGameScene mMoreGameScene;
    public Resource mResource = null;
    private long mSetGameSceneDelay = 0;
    public Status mState = Status.UNDEFINED;
    private SubLevelScene mSubLevelOptionScene;
    public GestureDetector myGesture;
    public PromotionHandler promotionHandler;
    public Handler rankingListHandler = new Handler() {
        public void handleMessage(Message msg) {
            F2FGameActivity.this.addToRankingList(msg.what);
        }
    };

    public enum Status {
        MAINMENU,
        GAME,
        GAME_STORAGE,
        LEVEL_OPTION,
        SUBLEVEL_OPTION,
        MORE_GAME,
        UNDEFINED,
        SHOP,
        LOADING,
        GAME_ERROR
    }

    public void loadPromotionInfo() {
        if (PortConst.enablePromotion) {
            this.promotionHandler = new PromotionHandler();
            try {
                this.promotionHandler.showPromtion(this);
            } catch (Exception e) {
                Exception e2 = e;
                PortConst.enablePromotion = false;
                PortConst.showPromtionIcon = false;
                Log.e("f2f_F2FGameActivity_loadPromotionInfo_showPromtion_error", e2.getMessage());
                GoogleAnalyticsUtils.setTracker("f2f_F2FGameActivity_loadPromotionInfo_showPromtion_error,detail is " + e2.getMessage());
            }
        } else {
            PortConst.showPromtionIcon = false;
        }
    }

    public void cutPromotionFile() {
        if (!PortConst.enablePromotion && this.promotionHandler != null && this.promotionHandler.enableCopy) {
            try {
                this.promotionHandler.cutFile();
            } catch (Exception e) {
                Exception e2 = e;
                Log.e("f2f_F2FGameActivity_cutPromotionFile_cutFile_error", e2.getMessage());
                GoogleAnalyticsUtils.setTracker("f2f_F2FGameActivity_cutPromotionFile_cutFile_error,detail is " + e2.getMessage());
            }
        }
    }

    public void loadGameInfo() {
        this.mGameInfo = new GameInfo();
        if (this.mGameInfo != null) {
            this.mGameInfo.initialize(this);
            this.mGameInfo.loadGameInfo(this);
        }
        CommonRankingList.InitializeRankingList(this);
    }

    public void setGameInfo() {
        if (this.mGameInfo != null) {
            this.mGameInfo.saveGameInfo(this);
        }
    }

    public GameInfo getMGameInfo() {
        return this.mGameInfo;
    }

    public void setMGameInfo(GameInfo gameInfo) {
        this.mGameInfo = gameInfo;
    }

    public static TableLoad getTableLoad() {
        return tableLoad;
    }

    public void onLoadComplete() {
    }

    public Engine getEngine() {
        return this.mEngine;
    }

    public GameScene getmGameScene() {
        return this.mGameScene;
    }

    public void resetGameScene() {
        this.mGameScene = null;
    }

    public void resetSubLevelScene() {
        this.mSubLevelOptionScene = null;
    }

    public Engine onLoadEngine() {
        TablePath.initialize(this);
        Const.packageName = getApplication().getPackageName();
        Const.enableSDCard = Utils.checkSDCardEnable();
        loadSceneSize();
        loadLanguage();
        loadGameInfo();
        return new FixedStepEngine(new EngineOptions(true, EngineOptions.ScreenOrientation.PORTRAIT, new RatioResolutionPolicy((float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT), new SmoothCamera(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, CommonConst.RALE_SAMALL_VALUE * ((float) CommonConst.CAMERA_WIDTH), CommonConst.RALE_SAMALL_VALUE * ((float) CommonConst.CAMERA_HEIGHT), 1.0f)).setNeedsSound(true).setNeedsMusic(true), 30);
    }

    public void onLoadResources() {
    }

    public void onGameResumed() {
        super.onResume();
        if (!(!PortConst.enableStoreMode || this.mGameInfo == null || this.mGameInfo.getMGameInfo() == null || this.mGameInfo.getMGameInfo().getMGameInfo() == null)) {
            if (this.mGameInfo.getMGameInfo().getMGameInfo().get_foreverClearAds() != -1 && CommonConst.FOREVER_CLEAR_ADS_DEFAUT == -1) {
                removeAdView();
                this.mGameInfo.getMGameInfo().getMGameInfo().set_foreverClearAds(-1);
                getTableLoad().plusProp(ShopConst.PropInfo[3][0], ShopConst.PropInfo[3][1]);
            } else if (this.mGameInfo.getMGameInfo().getMGameInfo().get_dialyClearAds() <= 0 && CommonConst.DIALY_CLEAR_ADS_DEFAUT > 0) {
                removeAdView();
                this.mGameInfo.getMGameInfo().getMGameInfo().set_dialyClearAds(System.currentTimeMillis() + 86400000);
                getTableLoad().plusProp(ShopConst.PropInfo[2][0], ShopConst.PropInfo[2][1]);
            }
        }
        if (CommonConst.GAME_MUSIC_ON && this.mGamePaused) {
            this.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            this.mGamePaused = false;
        }
        if (this.mMainMenuScene != null) {
            this.mMainMenuScene.downloadImage();
        }
        updatePurchaseProp();
    }

    public void updatePurchaseProp() {
        if (this.mState == Status.GAME && this.mGameScene != null && CommonConst.FROM_GAMESCENE_TO_SHOP) {
            this.mGameScene.isGameOver = false;
            if (CommonConst.LAST_F2FDIALOG_TYPE == 11) {
                this.mGameScene.checkLife();
            } else if (CommonConst.LAST_F2FDIALOG_TYPE == 10) {
                this.mGameScene.checkTime();
            }
            CommonConst.LAST_F2FDIALOG_TYPE = -1;
            CommonConst.FROM_GAMESCENE_TO_SHOP = false;
        }
    }

    public void onGamePaused() {
        if (this.mGameScene != null && this.mState == Status.GAME && !this.mGameScene.hasChildScene()) {
            this.mGameScene.showContextMenu();
        }
        if (CommonConst.GAME_MUSIC_ON) {
            this.mGamePaused = true;
            if (this.mResource != null) {
                this.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            }
            if (this.mGameScene != null) {
                this.mGameScene.operSound(false);
            }
        }
    }

    public Scene onLoadScene() {
        loadResources();
        loadPromotionInfo();
        checkSMS();
        this.loadingDelay = System.currentTimeMillis();
        this.mEngine.registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            public void onUpdate(float pSecondsElapsed) {
                F2FGameActivity.this.checkResource();
                F2FGameActivity.this.startMusic();
                F2FGameActivity.this.updateSceneStatus();
                F2FGameActivity.this.checkMessageQueue();
                F2FGameActivity.this.cutPromotionFile();
            }
        });
        return new LogoScene(this);
    }

    /* access modifiers changed from: private */
    public void checkMessageQueue() {
        if (this.mMessageQueue.size() > 0 && !getEngine().getScene().hasChildScene() && this.mState != Status.LOADING) {
            int i = 0;
            while (i < this.mMessageQueue.size()) {
                F2FMessage message = this.mMessageQueue.get(i);
                if (message == null || !(message.showInSceneStatus == Status.UNDEFINED || message.showInSceneStatus == this.mState)) {
                    i++;
                } else {
                    showMsgDialog(100, message.messageId);
                    this.mMessageQueue.remove(i);
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkResource() {
        if (this.mLoadingScene == null && this.commonResource != null && this.commonResource.loadCommonResourceReady && !this.isLoadingCompleted) {
            this.mLoadingScene = new LoadingScene(this);
            this.isLoadingCompleted = true;
        }
    }

    private void loadResources() {
        if (PortConst.enableMoreGame) {
            MoreGameConst.loadMoreGame(this);
        }
        this.mResource = new Resource(this.mEngine, this);
        this.commonResource = new CommonResource(this.mEngine, this);
        this.commonResource.startLoad();
        if (PortConst.enableStoreMode) {
            tableLoad = new TableLoad(this);
            try {
                tableLoad.initialize();
            } catch (Exception e) {
                Exception e2 = e;
                Log.e("f2f_F2FGameActivity_onLoadScene_tableLoad_initialize_error", e2.getMessage());
                GoogleAnalyticsUtils.setTracker("f2f_F2FGameActivity_onLoadScene_tableLoad_initialize_error,detail is " + e2.getMessage());
            }
        }
        this.myGesture = new GestureDetector(new MyGestureListener());
        this.mResource.startLoad();
    }

    private void checkSMS() {
        if (Const.enableSMS) {
            this.mBroadcastReceiver = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    String actionName = intent.getAction();
                    int resultCode = getResultCode();
                    if (actionName.equals(SMSConst.ACTION_SMS_SEND)) {
                        if (resultCode == -1) {
                            SMSConst.SMS_STATUS = 0;
                            GoogleAnalyticsUtils.setTracker("/Send_SMS/success");
                        } else {
                            SMSConst.SMS_STATUS = 1;
                            GoogleAnalyticsUtils.setTracker("/Send_SMS/fail");
                        }
                        F2FGameActivity.this.mEngine.getScene().clearChildScene();
                    }
                }
            };
            registerReceiver(this.mBroadcastReceiver, new IntentFilter(SMSConst.ACTION_SMS_SEND));
        }
    }

    /* access modifiers changed from: private */
    public void updateSceneStatus() {
        switch (this.mState) {
            case MAINMENU:
                if (this.mIsloadScene) {
                    return;
                }
                if (!PortConst.enablePromotion || System.currentTimeMillis() - this.loadingDelay >= PortConst.loadingTime) {
                    this.commonResource.unLoadPromotionSource();
                    PortConst.enablePromotion = false;
                    setResource();
                    this.mMainMenuScene = new MainMenuScene(this);
                    this.mEngine.setScene(this.mMainMenuScene);
                    setHud(Status.MAINMENU);
                    this.isLoadingCompleted = false;
                    this.mIsloadScene = true;
                    GoogleAnalyticsUtils.setTracker("/MainMenuScene");
                    if (CommonConst.errorList.size() > 0 || CommonConst.errorLevel == CommonConst.ERROR_LEVEL.UNREPAIR) {
                        setGameErrorMsg();
                        return;
                    }
                    if (PortConst.enableStoreMode && PortConst.enableAddGold) {
                        if (CommonConst.IS_FIRST_PLAY_GAME) {
                            CommonConst.IS_ADD_EVERYDAY_GOLDEN_MONEY = false;
                            CommonConst.IS_FIRST_PLAY_GAME = false;
                            showMsgDialog(0);
                        } else if (CommonConst.IS_ADD_EVERYDAY_GOLDEN_MONEY) {
                            CommonConst.IS_ADD_EVERYDAY_GOLDEN_MONEY = false;
                            CommonConst.IS_FIRST_PLAY_GAME = false;
                            showMsgDialog(1);
                        }
                    }
                    if (Const.enableCheckWire) {
                        showWireMsg();
                        return;
                    }
                    return;
                }
                return;
            case LEVEL_OPTION:
                if (this.mIsloadScene) {
                    return;
                }
                if (this.commonResource.loadCommonResourceReady) {
                    setResource();
                    this.mLevelOptionScene = new LevelScene(this);
                    this.mEngine.setScene(this.mLevelOptionScene);
                    setHud(Status.LEVEL_OPTION);
                    this.isLoadingCompleted = false;
                    this.mIsloadScene = true;
                    GoogleAnalyticsUtils.setTracker("/LevelOptionScene");
                    return;
                } else if (this.isLoadingCompleted) {
                    this.mEngine.setScene(this.mLoadingScene);
                    setHud(Status.LOADING);
                    return;
                } else {
                    return;
                }
            case SHOP:
                if (!this.mIsloadScene) {
                    startActivity(new Intent(this, ShopActivity.class));
                    this.mState = Status.LEVEL_OPTION;
                    setHud(Status.LOADING);
                    this.mIsloadScene = true;
                    GoogleAnalyticsUtils.setTracker("/ShopScene");
                    return;
                }
                return;
            case SUBLEVEL_OPTION:
                if (this.mIsloadScene) {
                    return;
                }
                if (this.commonResource.loadCommonResourceReady) {
                    setResource();
                    this.mSubLevelOptionScene = new SubLevelScene(this);
                    this.mEngine.setScene(this.mSubLevelOptionScene);
                    setHud(Status.SUBLEVEL_OPTION);
                    this.isLoadingCompleted = false;
                    this.mIsloadScene = true;
                    GoogleAnalyticsUtils.setTracker("/SubLevelOptionScene");
                    return;
                } else if (this.isLoadingCompleted) {
                    this.mEngine.setScene(this.mLoadingScene);
                    setHud(Status.LOADING);
                    return;
                } else {
                    return;
                }
            case GAME:
                if (!this.mIsloadScene) {
                    if (this.mResource.loadGameResourceReady) {
                    }
                    if (System.currentTimeMillis() - this.mSetGameSceneDelay >= 500) {
                        setResource();
                        this.mGameScene = new GameScene(this);
                        this.mEngine.setScene(this.mGameScene);
                        this.mGameScene.setChildSceneClick(true);
                        this.mGameStartMillis = System.currentTimeMillis();
                        this.isLoadingCompleted = false;
                        this.mIsloadScene = true;
                        return;
                    }
                    return;
                }
                return;
            case GAME_STORAGE:
                if (!this.mIsloadScene) {
                    this.mGameStorageScene = Const.createCustomozedScene(this);
                    if (this.mGameStorageScene != null) {
                        this.mEngine.setScene(this.mGameStorageScene);
                        this.isLoadingCompleted = false;
                        this.mIsloadScene = true;
                        return;
                    } else if (this.isLoadingCompleted) {
                        this.mEngine.setScene(this.mLoadingScene);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            case MORE_GAME:
                if (this.mIsloadScene) {
                    return;
                }
                if (this.commonResource.loadCommonResourceReady) {
                    setResource();
                    this.mMoreGameScene = new MoreGameScene(this);
                    this.mEngine.setScene(this.mMoreGameScene);
                    setHud(Status.MORE_GAME);
                    this.isLoadingCompleted = false;
                    this.mIsloadScene = true;
                    GoogleAnalyticsUtils.setTracker("/MoreGameScene");
                    return;
                } else if (this.isLoadingCompleted) {
                    this.mEngine.setScene(this.mLoadingScene);
                    setHud(Status.LOADING);
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void startMusic() {
        if (this.mIsloadScene && this.isFirstPlayMusic && this.mResource != null && this.mResource.loadCommonResourceReady && CommonConst.GAME_MUSIC_ON) {
            this.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
            this.isFirstPlayMusic = false;
        }
    }

    public boolean isAdViewEnable() {
        if (this.adViewLayout == null || this.adViewLayout.getVisibility() == 4) {
            return false;
        }
        return true;
    }

    public void removeAdView() {
        CommonConst.ENABLE_SHOW_ADVIEW = true;
        if (this.layoutParams != null) {
            this.layoutParams.height = -1;
            this.mRenderSurfaceView.setLayoutParams(this.layoutParams);
        }
        if (this.adViewLayout != null) {
            this.adViewLayout.setVisibility(4);
        }
    }

    /* access modifiers changed from: protected */
    public void onSetContentView() {
        RelativeLayout layout = new RelativeLayout(this);
        setContentView(layout);
        checkAdViewEnable();
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
            this.adViewLayout = new AdViewLayout(this, PortConst.AdviewId);
            this.adViewLayout.setMaxHeight((int) (((float) CommonConst.CAMERA_HEIGHT) - (CommonConst.AdViewHeight * CommonConst.RALE_SAMALL_VALUE)));
            this.adViewLayout.setMaxWidth(CommonConst.CAMERA_WIDTH);
            RelativeLayout.LayoutParams adViewLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
            adViewLayoutParams.addRule(12);
            adViewLayoutParams.addRule(14, -1);
            layout.addView(this.adViewLayout, adViewLayoutParams);
            layout.invalidate();
        }
        this.layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        this.layoutParams.addRule(6);
        this.layoutParams.addRule(14, -1);
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
            this.layoutParams.height = (int) (((float) CommonConst.CAMERA_HEIGHT) - (CommonConst.AdViewHeight * CommonConst.RALE_SAMALL_VALUE));
        } else {
            this.layoutParams.height = -1;
        }
        this.mRenderSurfaceView = new RenderSurfaceView(this);
        this.mRenderSurfaceView.setEGLConfigChooser(false);
        this.mRenderSurfaceView.setRenderer(this.mEngine);
        layout.addView(this.mRenderSurfaceView, this.layoutParams);
        GoogleAnalyticsUtils.getInstance(this);
    }

    public void checkAdViewEnable() {
        if (this.mGameInfo != null && this.mGameInfo.getMGameInfo() != null && this.mGameInfo.getMGameInfo().getMGameInfo() != null) {
            if (this.mGameInfo.getMGameInfo().getMGameInfo().get_foreverClearAds() == -1) {
                CommonConst.ENABLE_SHOW_ADVIEW = true;
            } else if (this.mGameInfo.getMGameInfo().getMGameInfo().get_dialyClearAds() - System.currentTimeMillis() <= 0 || this.mGameInfo.getMGameInfo().getMGameInfo().get_dialyClearAds() - System.currentTimeMillis() >= 86400000) {
                this.mGameInfo.getMGameInfo().getMGameInfo().set_dialyClearAds(CommonConst.ADS_DEFAUT_VALUE);
                CommonConst.DIALY_CLEAR_ADS_DEFAUT = CommonConst.ADS_DEFAUT_VALUE;
            } else {
                CommonConst.ENABLE_SHOW_ADVIEW = true;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        GoogleAnalyticsUtils.stopTracker();
        Process.killProcess(Process.myPid());
    }

    public void finish() {
        super.finish();
        release();
        GoogleAnalyticsUtils.dispatchTracker();
    }

    private void release() {
        this.mMainMenuScene = null;
        this.mGameScene = null;
        this.mLevelOptionScene = null;
        this.mSubLevelOptionScene = null;
        this.mLoadingScene = null;
        if (this.mResource != null) {
            this.mResource.releaseResource();
            this.mResource = null;
        }
        this.commonResource = null;
        this.myGesture = null;
        this.layoutParams = null;
        this.adViewLayout = null;
        this.mGameInfo = null;
        tableLoad = null;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        switch (this.mState) {
            case MAINMENU:
                if (this.mMainMenuScene != null && !this.mMainMenuScene.hasChildScene()) {
                    gameFinish();
                    break;
                }
            case LEVEL_OPTION:
                if (this.mLevelOptionScene == null) {
                    setStatus(Status.MAINMENU);
                    break;
                } else {
                    this.mLevelOptionScene.backToMenu();
                    break;
                }
            case SUBLEVEL_OPTION:
                setStatus(Status.LEVEL_OPTION);
                break;
            case GAME:
                if (this.mGameScene != null) {
                    if (this.mGameScene.hasChildScene()) {
                        if (this.mGameScene.getChildScene().equals(this.mGameScene.getmContextMenuScene())) {
                            this.mGameScene.clearContextMenu();
                            break;
                        }
                    } else {
                        this.mGameScene.showContextMenu();
                        break;
                    }
                }
                break;
            case GAME_STORAGE:
                if (this.mGameStorageScene != null) {
                    if (this.mGameStorageScene.hasChildScene()) {
                        if (this.mGameStorageScene.getChildScene().equals(this.mGameStorageScene.getmContextMenuScene())) {
                            this.mGameStorageScene.clearContextMenu();
                            break;
                        }
                    } else {
                        this.mGameStorageScene.showContextMenu();
                        break;
                    }
                }
                break;
            case MORE_GAME:
                if (this.mMoreGameScene != null) {
                    this.mMoreGameScene.backToMenu();
                    break;
                }
                break;
        }
        return true;
    }

    public void startNextLevel() {
        this.mGameScene = null;
        setStatus(Status.GAME);
    }

    public void gameFinish() {
        showMsgDialog(15);
    }

    public void showMsgDialog(int dialogTypeID) {
        if (this.mGameScene != null && getEngine().getScene().equals(this.mGameScene)) {
            this.mGameScene.disableHud();
        }
        F2FAlertDialog alertDialog = new F2FAlertDialog(this);
        alertDialog.showDialog(dialogTypeID);
        getEngine().getScene().setChildScene(alertDialog, false, true, true);
    }

    public void showMsgDialog(int dialogTypeID, int messageID) {
        if (this.mGameScene != null && getEngine().getScene().equals(this.mGameScene)) {
            this.mGameScene.disableHud();
        }
        F2FAlertDialog alertDialog = new F2FAlertDialog(this);
        alertDialog.showDialog(dialogTypeID, getResources().getString(messageID));
        getEngine().getScene().setChildScene(alertDialog, false, true, true);
    }

    public void setGameErrorMsg() {
        if (CommonConst.errorLevel != CommonConst.ERROR_LEVEL.UNREPAIR) {
            showMsgDialog(14);
        } else {
            showMsgDialog(13);
        }
    }

    public void operateErrorBtn() {
        try {
            if (CommonConst.errorLevel == CommonConst.ERROR_LEVEL.UNREPAIR) {
                exitGame(false);
            } else if (CommonConst.errorList.size() > 0) {
                int nCount = CommonConst.errorList.size();
                for (int i = 0; i < nCount; i++) {
                    switch (CommonConst.errorList.get(i)) {
                        case GAMEINFO_REPAIR:
                            getMGameInfo().rectGameInfo(this);
                            break;
                        default:
                            getTableLoad().rectInitialData();
                            break;
                    }
                }
            } else {
                exitGame(false);
            }
        } catch (Exception e) {
            Log.e("f2f_operateErrorBtn_error", e.toString());
            exitGame(false);
        } finally {
            CommonConst.errorList.clear();
            CommonConst.errorLevel = CommonConst.ERROR_LEVEL.CORRECT;
        }
    }

    public void exitGame(boolean pIsSaveGameInfo) {
        if (pIsSaveGameInfo) {
            setGameInfo();
        }
        CommonConst.errorList.clear();
        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.CORRECT;
        if (CommonConst.GAME_MUSIC_ON && this.mResource.getMusicByKey(Resource.MUSICTURE.BACKGROUD_MUSIC.mKey).isPlaying()) {
            this.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
        }
        finish();
    }

    public void setHud(Status pStatus) {
        if (this.mGameScene == null) {
            return;
        }
        if (pStatus != Status.GAME) {
            this.mGameScene.disableHud();
        } else {
            this.mGameScene.enableHud();
        }
    }

    public void resetCamera() {
        SmoothCamera mBoundCamera = (SmoothCamera) getEngine().getCamera();
        mBoundCamera.setZoomFactor(1.0f);
        mBoundCamera.reset();
        mBoundCamera.setChaseShape(null);
        mBoundCamera.setBounds(Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_HEIGHT);
        mBoundCamera.setCenter((float) (CommonConst.CAMERA_WIDTH / 2), (float) (CommonConst.CAMERA_HEIGHT / 2));
    }

    public void setStatus(Status pStatus) {
        this.mLastStatus = this.mState;
        this.mState = pStatus;
        this.mIsloadScene = false;
        resetCamera();
        if (pStatus != Status.SHOP && this.mLoadingScene != null) {
            this.mLoadingScene = null;
            this.mLoadingScene = new LoadingScene(this);
            this.mEngine.setScene(this.mLoadingScene);
            setHud(Status.LOADING);
            this.mSetGameSceneDelay = System.currentTimeMillis();
        }
    }

    public void setResource() {
        unLoadResource();
        if (!this.isUnloadLogo) {
            this.isUnloadLogo = true;
            this.commonResource.unloadLogoTextures();
        }
        loadResource();
    }

    public void unLoadResource() {
        switch (this.mLastStatus) {
            case MAINMENU:
                this.commonResource.unLoadMenuResource();
                this.mResource.unloadMenuTextures();
                return;
            case LEVEL_OPTION:
                this.commonResource.unloadLevelResource();
                return;
            case SHOP:
            case GAME_STORAGE:
            default:
                return;
            case SUBLEVEL_OPTION:
                this.commonResource.unLoadSubLevelResource();
                return;
            case GAME:
                this.commonResource.unloadGameResource();
                this.mResource.unloadGameTextures();
                return;
            case MORE_GAME:
                this.commonResource.unloadMoreGameResource();
                return;
        }
    }

    public void loadResource() {
        switch (this.mState) {
            case MAINMENU:
                this.commonResource.loadMenuResource();
                this.mResource.loadMenuTextures();
                return;
            case LEVEL_OPTION:
                this.commonResource.loadLevelResource();
                return;
            case SHOP:
            case GAME_STORAGE:
            default:
                return;
            case SUBLEVEL_OPTION:
                this.commonResource.loadSubLevelResource();
                return;
            case GAME:
                this.commonResource.loadGameResource();
                this.mResource.loadGameTextures();
                return;
            case MORE_GAME:
                this.commonResource.loadMoreGameResource();
                return;
        }
    }

    private void loadSceneSize() {
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        CommonConst.CAMERA_HEIGHT = Math.max(display.getWidth(), display.getHeight());
        CommonConst.CAMERA_WIDTH = Math.min(display.getWidth(), display.getHeight());
        CommonConst.RALE_SAMALL_VALUE = ((float) CommonConst.CAMERA_HEIGHT) / ((float) CommonConst.CAMERA_MAX_HEIGHT_PORTAIT);
    }

    public void loadLanguage() {
        Utils.LANGUAGE = getResources().getString(R.string.language);
    }

    public class MyGestureListener implements GestureDetector.OnGestureListener {
        final float mSwipeMinDistance = 10.0f;

        public MyGestureListener() {
        }

        public boolean onDown(MotionEvent e) {
            return false;
        }

        public boolean onFling(MotionEvent pMotionEventStart, MotionEvent pMotionEventEnd, float pVelocityX, float pVelocityY) {
            if (!(F2FGameActivity.this.getEngine().getScene() == null || pMotionEventStart == null || pMotionEventEnd == null)) {
                ((F2FScene) F2FGameActivity.this.getEngine().getScene()).onFling(pMotionEventStart, pMotionEventEnd, pVelocityX, pVelocityY);
            }
            return true;
        }

        public void onLongPress(MotionEvent e) {
        }

        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            return false;
        }

        public void onShowPress(MotionEvent e) {
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }
    }

    public void addToRankingList(final int pScore) {
        if (CommonRankingList.getmRankingListInfo().checkIsTopScore(pScore)) {
            View saveGameResult = LayoutInflater.from(this).inflate((int) R.layout.rankinglist_save, (ViewGroup) null);
            final EditText name = (EditText) saveGameResult.findViewById(R.id.username_edit);
            String label_postiveButton = getResources().getString(R.string.str_button_yes);
            new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.rankinglist_lable)).setView(saveGameResult).setPositiveButton(label_postiveButton, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    F2FGameActivity.this.addToRankingList(name.getText().toString(), pScore);
                }
            }).setNegativeButton(getResources().getString(R.string.str_button_cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    F2FGameActivity.this.addToRankingList("", pScore);
                }
            }).show();
        }
    }

    public void addToRankingList(String pName, int pScore) {
        CommonRankingList.getmRankingListInfo().addTopValue(pName, pScore);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == F2FMailSender.SEND_IN_MAINMENU) {
            if (PortConst.enableStoreMode && PortConst.enableAddGold) {
                if (this.mGameInfo != null) {
                    this.mGameInfo.inviteFriend(this);
                }
                setGoldSore(CommonConst.INVITE_FRIEND_GOLD_NUMBER);
                showMsgDialog(5);
            }
        } else if (requestCode != F2FMailSender.SEND_IN_GAME) {
        } else {
            if (PortConst.enableStoreMode && PortConst.enableAddGold) {
                setGoldSore(CommonConst.PASS_LEVEL_GOLD_NUMBER);
                this.mGameInfo.saveGameInfo(this);
                showMsgDialog(5);
            } else if (this.mGameScene != null) {
                this.mGameScene.showOKDialog();
            }
        }
    }

    public void sendMailInGame() {
        F2FMailSender.setMail(this, F2FMailSender.SEND_IN_GAME);
    }

    private void setGoldSore(int pGoldenCount) {
        if (PortConst.enableStoreMode) {
            PersonalAccountTable personalAccountTable = getTableLoad().getmPersonalAccountTable();
            PersonalAccount accout = personalAccountTable.getPersonalAccountList().get(0);
            accout.setGolden_count(accout.getGolden_count() + pGoldenCount);
            try {
                personalAccountTable.write();
            } catch (Exception e) {
                Log.e("f2fError", e.toString());
            }
        }
    }

    public void showWireMsg() {
        if (!Utils.checkNetWork(this) && this.enableShowWireLess) {
            this.enableShowWireLess = false;
            F2FAlertDialog alertDialog = new F2FAlertDialog(this);
            alertDialog.showDialog(16);
            getEngine().getScene().setChildScene(alertDialog, false, true, true);
        }
    }

    public void setWireless() {
        startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
    }

    public boolean loadSMSPreferences(String id) {
        return getSharedPreferences(CommonConst.NAME_PREFERENCES, 1).getBoolean(id, false);
    }

    public void saveSMSSettings(String id) {
        SharedPreferences.Editor editor = getSharedPreferences(CommonConst.NAME_PREFERENCES, 1).edit();
        editor.putBoolean(id, true);
        editor.commit();
    }
}
