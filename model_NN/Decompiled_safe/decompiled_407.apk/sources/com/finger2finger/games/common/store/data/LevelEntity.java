package com.finger2finger.games.common.store.data;

public class LevelEntity {
    public static final int LIST_ITEM_COUNT = 6;
    private int index = -1;
    private boolean isEnable = false;
    private int levelIndex = -1;
    private String levelName = "";
    private int levelScoreMax = 0;
    private int star = 0;
    private int subLevelIndex = -1;

    public LevelEntity(String[] data) throws Exception {
        boolean z;
        if (data == null || data.length != 6) {
            throw new IllegalArgumentException();
        }
        this.index = Integer.parseInt(data[0]);
        this.levelIndex = Integer.parseInt(data[1]);
        this.subLevelIndex = Integer.parseInt(data[2]);
        this.levelScoreMax = Integer.parseInt(data[3]);
        if (Integer.parseInt(data[4]) == 0) {
            z = true;
        } else {
            z = false;
        }
        this.isEnable = z;
        this.star = Integer.parseInt(data[5]);
        this.levelName = String.format("SubLevel%02d", Integer.valueOf(this.levelIndex));
    }

    public LevelEntity(int levelIndex2, int subLevelIndex2, int index2, int levelScoreMax2, boolean isEnable2, int star2) {
        this.levelIndex = levelIndex2;
        this.subLevelIndex = subLevelIndex2;
        this.index = index2;
        this.levelScoreMax = levelScoreMax2;
        this.isEnable = isEnable2;
        this.levelName = String.format("SubLevel%02d", Integer.valueOf(levelIndex2));
        this.star = star2;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.index).append(TablePath.SEPARATOR_ITEM).append(this.levelIndex).append(TablePath.SEPARATOR_ITEM).append(this.subLevelIndex).append(TablePath.SEPARATOR_ITEM).append(this.levelScoreMax).append(TablePath.SEPARATOR_ITEM).append(this.isEnable ? 0 : 1).append(TablePath.SEPARATOR_ITEM).append(this.star);
        return sb.toString();
    }

    public int getLevelIndex() {
        return this.levelIndex;
    }

    public void setLevelIndex(int levelIndex2) {
        this.levelIndex = levelIndex2;
    }

    public String getLevelName() {
        return this.levelName;
    }

    public void setLevelName(String levelName2) {
        this.levelName = levelName2;
    }

    public int getLevelScoreMax() {
        return this.levelScoreMax;
    }

    public void setLevelScoreMax(int levelScoreMax2) {
        this.levelScoreMax = levelScoreMax2;
    }

    public int getSubLevelIndex() {
        return this.subLevelIndex;
    }

    public void setSubLevelIndex(int subLevelIndex2) {
        this.subLevelIndex = subLevelIndex2;
    }

    public void setIsEnable(boolean isEnable2) {
        this.isEnable = isEnable2;
    }

    public boolean getIsEnable() {
        return this.isEnable;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index2) {
        this.index = index2;
    }

    public int getStar() {
        return this.star;
    }

    public void setStar(int star2) {
        this.star = star2;
    }
}
