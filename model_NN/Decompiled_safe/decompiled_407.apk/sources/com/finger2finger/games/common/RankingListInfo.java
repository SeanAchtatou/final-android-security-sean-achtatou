package com.finger2finger.games.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.finger2finger.games.common.store.data.RankingList;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.common.store.io.OriginalDataProcess;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.PortConst;

public class RankingListInfo {
    private Context mContext;
    private String mNameAnonymous = "Anonymous";
    private RankingList[] mRankingList = new RankingList[10];

    public RankingList[] getmRankingList() {
        return this.mRankingList;
    }

    public void setmRankingList(RankingList[] mRankingList2) {
        this.mRankingList = mRankingList2;
    }

    public RankingListInfo(Context pContext) {
        this.mContext = pContext;
    }

    public void initialize() {
        this.mNameAnonymous = this.mContext.getString(R.string.rankinglist_unknown);
        for (int i = 0; i < 10; i++) {
            try {
                this.mRankingList[i] = new RankingList(new String[]{"0", ""});
            } catch (Exception e) {
                Log.e("Initialize_RankingList_Error", e.getMessage());
            }
        }
    }

    public void loadPreferences() {
        String[] data;
        try {
            String stream = this.mContext.getSharedPreferences("MonkeyClear", 1).getString("RankingListData", "");
            if (stream != null && !stream.equals("") && (data = OriginalDataProcess.AES1282Strings(stream)) != null && data.length > 0) {
                for (int i = 0; i < data.length; i++) {
                    this.mRankingList[i] = new RankingList(data[i].split(TablePath.SEPARATOR_ITEM));
                }
            }
        } catch (Exception e) {
            Log.e("Load_RankingList_Error", e.getMessage());
        }
    }

    public void writePreferences() {
        try {
            String[] data = new String[10];
            for (int i = 0; i < 10; i++) {
                data[i] = this.mRankingList[i].toString();
            }
            if (data != null && data.length > 0) {
                if (!PortConst.enableSDCard || CommonPortConst.isNoSDCard) {
                    SharedPreferences.Editor editor = this.mContext.getSharedPreferences("MonkeyClear", 1).edit();
                    editor.putString("RankingListData", OriginalDataProcess.Strings2AES128(data));
                    editor.commit();
                }
            }
        } catch (Exception e) {
            Log.e("write_RankingList_Error", e.getMessage());
        }
    }

    public boolean checkIsTopScore(int pScore) {
        if (pScore > this.mRankingList[9].getScore()) {
            return true;
        }
        return false;
    }

    public boolean addTopValue(String pName, int pScore) {
        if (pName == null || pName.equals("")) {
            pName = this.mNameAnonymous;
        }
        int index = -1;
        int i = 0;
        while (true) {
            if (i >= 10) {
                break;
            } else if (pScore > this.mRankingList[i].getScore()) {
                index = i;
                break;
            } else {
                i++;
            }
        }
        if (index >= 0) {
            for (int i2 = 9; i2 >= index; i2--) {
                if (index == i2) {
                    try {
                        this.mRankingList[i2] = new RankingList(new String[]{String.valueOf(pScore), pName});
                    } catch (Exception e) {
                    }
                    writePreferences();
                    return true;
                }
                if (i2 - 1 >= 0) {
                    this.mRankingList[i2] = this.mRankingList[i2 - 1];
                }
            }
        }
        return false;
    }
}
