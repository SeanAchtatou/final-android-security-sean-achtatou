package com.finger2finger.games.common;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import com.finger2finger.games.common.activity.F2FGameActivity;
import java.util.List;
import java.util.regex.Pattern;
import org.anddev.andengine.opengl.font.Font;

public class Utils {
    public static String LANGUAGE = "";

    public static int getMaxCharacterLens(Font font, String str) {
        int width = 0;
        int index = 0;
        String[] arrs = str.split("\n");
        for (int i = 0; i < arrs.length; i++) {
            if (width < arrs[i].length()) {
                width = arrs[i].length();
                index = i;
            }
        }
        return font.getStringWidth(arrs[index]);
    }

    /* JADX INFO: Multiple debug info for r5v1 java.lang.String: [D('font' org.anddev.andengine.opengl.font.Font), D('outStr' java.lang.String)] */
    public static String getAutoLine(Font font, String pInput, int pLineWidth, boolean pIsAddSpce, int pSpaceLength) {
        if (pInput == null || pInput.equals("")) {
            return "";
        }
        String pInput2 = pInput.replaceAll("\n", "").replaceAll("\r", "");
        if (pIsAddSpce) {
            pInput2 = addSpace(pInput2, pSpaceLength, true);
        }
        int strLen = pInput2.length();
        int strWidth = font.getStringWidth(pInput2);
        if (strWidth <= pLineWidth) {
            return pInput2 + "\n";
        }
        int lineCharNum = (strWidth / pLineWidth) + (strWidth % pLineWidth > 0 ? 1 : 0);
        if (lineCharNum != 1) {
            String outStr = "";
            int temp = 0;
            for (int i = 0; i < lineCharNum - 1; i++) {
                String outStr2 = outStr + pInput2.substring(temp, (((i + 1) * strLen) / lineCharNum) + 1);
                temp = (((i + 1) * strLen) / lineCharNum) + 1;
                if (temp < pInput2.length()) {
                    outStr2 = increaseExpansion(outStr2, pInput2.charAt(temp));
                }
                outStr = outStr2 + "\n";
            }
            return strWidth % pLineWidth != 0 ? outStr + pInput2.substring(temp, strLen) + "\n" : outStr;
        } else if (strWidth % pLineWidth == 0) {
            return pInput2 + "\n";
        } else {
            return (increaseExpansion(pInput2.substring(0, ((strLen * 2) / 3) + 1), pInput2.charAt(((strLen * 2) / 3) + 1)) + "\n") + pInput2.substring(((strLen * 2) / 3) + 1, strLen) + "\n";
        }
    }

    public static int getLineHight(Font font, String pInput) {
        int lineHight = font.getLineHeight();
        if (pInput == null || pInput.equals("")) {
            return lineHight;
        }
        int lineHight2 = pInput.split("\n").length - 1;
        if (lineHight2 > 0) {
            return lineHight2 * font.getLineHeight();
        }
        return lineHight2;
    }

    public static String addZero(String pInput, int pLength, boolean pIsAddLeft) {
        if (pLength <= pInput.length()) {
            return pInput;
        }
        if (pInput == null || pInput.equals("")) {
            pInput = "0";
        }
        String retValue = pInput;
        for (int i = 0; i <= pLength - retValue.length(); i++) {
            if (pIsAddLeft) {
                retValue = "0" + retValue;
            } else {
                retValue = retValue + "0";
            }
        }
        return retValue;
    }

    public static String addSpace(String pInput, int pLength, boolean pIsAddFixLegth) {
        if (pInput == null || pInput.equals("")) {
            pInput = " ";
        }
        String retValue = pInput;
        if (!pIsAddFixLegth) {
            for (int i = 0; i <= pLength - retValue.length(); i++) {
                retValue = " " + retValue;
            }
        } else {
            for (int i2 = 0; i2 < pLength; i2++) {
                retValue = " " + retValue;
            }
        }
        return retValue;
    }

    public static String getSencondTimeFormat(String pInput) {
        String pInput2 = addZero(pInput, 5, true);
        return pInput2.substring(0, 3) + "." + pInput2.substring(3);
    }

    public static String getSecondIniFormat(String pInput) {
        int len = pInput.length();
        for (int i = 0; i <= 3 - len; i++) {
            pInput = "0" + pInput;
        }
        return pInput + ".00";
    }

    public static String increaseExpansion(String pCurrentInput, char pNextChar) {
        String crrentInput = pCurrentInput;
        if (pCurrentInput == null || pCurrentInput.equals("")) {
            return "";
        }
        if (!isLetter(pCurrentInput.charAt(pCurrentInput.length() - 1)) || !isLetter(pNextChar)) {
            return crrentInput;
        }
        return pCurrentInput + "-";
    }

    public static boolean isLetter(char pChar) {
        return ('a' <= pChar && pChar <= 'z') || ('A' <= pChar && pChar <= 'Z');
    }

    public static String getImageName(String name) {
        if (LANGUAGE == null || LANGUAGE.equals("")) {
            return name;
        }
        try {
            return ((name.substring(0, name.indexOf(".png")) + "_") + LANGUAGE) + ".png";
        } catch (Exception e) {
            return name;
        }
    }

    public static void setFullScreen(Activity mActivity) {
        mActivity.requestWindowFeature(1);
        mActivity.getWindow().addFlags(1024);
        mActivity.getWindow().clearFlags(2048);
    }

    public static void link(Context mContext, String url) {
        mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public static String getOmissionString(String pString, int pLength) {
        if (pString.equals("") || pString == null || pString.length() <= pLength) {
            return pString;
        }
        return pString.substring(0, pLength) + "...";
    }

    public static boolean checkNetWork(Context mContext) {
        NetworkInfo networkInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isAvailable() || !networkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static boolean checkGameInstall(Context pContext, String pageName) {
        List<PackageInfo> packs = pContext.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < packs.size(); i++) {
            if (packs.get(i).packageName.toLowerCase().contains(pageName)) {
                return true;
            }
        }
        return false;
    }

    public static void vote(Context pContext) {
        pContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + pContext.getPackageName())));
    }

    public static void goPage(Context pContext, String pUrl) {
        pContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(pUrl)));
    }

    public static boolean checkSDCardEnable() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean checkEmailEnable(Context pContext) {
        Account[] accounts = AccountManager.get(pContext).getAccountsByType("com.google");
        if (accounts == null || accounts.length <= 0) {
            return false;
        }
        return true;
    }

    public static boolean isNumeric(String str) {
        if (str == null || str.equals("")) {
            return false;
        }
        if (!Pattern.compile("[0-9]*").matcher(str).matches()) {
            return false;
        }
        return true;
    }

    public static void setGoogleAnalyticsTime(F2FGameActivity mContext, int pStatus) {
        setGoogleAnalytics(mContext, pStatus, -1, -1);
    }

    public static void setGoogleAnalyticsScore(F2FGameActivity mContext, int pStatus, int pScore) {
        setGoogleAnalytics(mContext, pStatus, pScore, -1);
    }

    public static void setGoogleAnalyticsMeter(F2FGameActivity mContext, int pStatus, int pMeter) {
        setGoogleAnalytics(mContext, pStatus, -1, pMeter);
    }

    public static void setGoogleAnalyticsScoreAndMeter(F2FGameActivity mContext, int pStatus, int pScore, int pMeter) {
        setGoogleAnalytics(mContext, pStatus, pScore, pMeter);
    }

    public static void setGoogleAnalytics(F2FGameActivity mContext, int pStatus, int pScore, int pMeter) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("play_end");
        if (mContext.getMGameInfo() != null) {
            if (mContext.getMGameInfo().getMLevelIndex() >= 0) {
                buffer.append("_");
                buffer.append(String.valueOf(mContext.getMGameInfo().getMLevelIndex()));
            }
            if (mContext.getMGameInfo().getMInsideIndex() >= 0) {
                buffer.append("_");
                buffer.append(String.valueOf(mContext.getMGameInfo().getMInsideIndex()));
            }
        }
        switch (pStatus) {
            case 0:
                buffer.append("/replay/");
                break;
            case 1:
                buffer.append("/back/");
                break;
            case 2:
                buffer.append("/success/");
                break;
            case 3:
                buffer.append("/fail/");
                break;
        }
        long playTime = (System.currentTimeMillis() - mContext.mGameStartMillis) / 1000;
        buffer.append("time_");
        buffer.append(playTime);
        if (pScore > 0) {
            buffer.append("_");
            buffer.append("score_");
            buffer.append(playTime);
        }
        if (pMeter > 0) {
            buffer.append("_");
            buffer.append("meter_");
            buffer.append(pMeter);
        }
        GoogleAnalyticsUtils.setTracker(buffer.toString());
    }
}
