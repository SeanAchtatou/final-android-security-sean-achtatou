package com.finger2finger.games.common.scene.dialog;

import android.content.res.Resources;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.ShopButton;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.base.BaseStrokeFont;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class GameOverDialog extends CameraScene {
    private F2FGameActivity mContext;
    private boolean mEnableShowStar = true;

    private enum BtnType {
        MENU_BTN,
        REPLAY_BTN,
        NEXTLEVEL_BTN
    }

    public GameOverDialog(Camera camera, F2FGameActivity pContext, boolean pIsThreeBtn, boolean pIsSucceed, String plevelText, int pHightScore, int pScore, boolean pHaveShopBtn, int pStarNum, int pMaxStarNum) {
        super(1, camera);
        this.mContext = pContext;
        loadScene(pIsThreeBtn, pHaveShopBtn, plevelText, pHightScore, pScore, pStarNum, pIsSucceed, pMaxStarNum);
        setBackgroundEnabled(false);
    }

    public GameOverDialog(Camera camera, F2FGameActivity pContext, boolean pIsThreeBtn, boolean pIsSucceed, String plevelText, int pHightScore, int pScore, boolean pHaveShopBtn, int pStarNum, int pMaxStarNum, boolean pEnableShowStar) {
        super(1, camera);
        this.mEnableShowStar = pEnableShowStar;
        this.mContext = pContext;
        loadScene(pIsThreeBtn, pHaveShopBtn, plevelText, pHightScore, pScore, pStarNum, pIsSucceed, pMaxStarNum);
        setBackgroundEnabled(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [?, ?, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    public void loadScene(boolean pIsThreeBtn, boolean pHaveShopBtn, String plevelText, int pHightScore, int pScore, int pStarNum, boolean pIsSucceed, int pMaxStarNum) {
        Rectangle rectangle = new Rectangle(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        rectangle.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue, CommonConst.GrayColor.alpha);
        getTopLayer().addEntity(rectangle);
        float stepValue = 55.0f * CommonConst.RALE_SAMALL_VALUE;
        AnonymousClass1 r5 = new BaseSprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_MENU.mKey), false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                GameOverDialog.this.closeBtn(BtnType.MENU_BTN);
                return true;
            }
        };
        BaseSprite backgroudSpr = new BaseSprite((float) Const.MOVE_LIMITED_SPEED, (float) Const.MOVE_LIMITED_SPEED, CommonConst.RALE_SAMALL_VALUE * 1.0f, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey), false);
        float backgroudSprWidth = backgroudSpr.getWidth();
        float backgroudSprHeight = backgroudSpr.getHeight();
        float backgroudSprPX = (((float) CommonConst.CAMERA_WIDTH) - backgroudSprWidth) / 2.0f;
        float backgroudSprPY = (((float) CommonConst.CAMERA_HEIGHT) - backgroudSprHeight) / 2.0f;
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
        }
        backgroudSpr.setPosition(backgroudSprPX, backgroudSprPY);
        getTopLayer().addEntity(backgroudSpr);
        ShopButton shopButton = new ShopButton();
        shopButton.enableMoreGame = false;
        shopButton.showShop(this.mContext, this, getTopLayer().getZIndex(), "FromDialog");
        if (pHightScore < pScore && pIsSucceed) {
            pHightScore = pScore;
            BaseSprite mNewRecordSpr = new BaseSprite((float) Const.MOVE_LIMITED_SPEED, (float) Const.MOVE_LIMITED_SPEED, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_NEW_RECORD.mKey), false);
            mNewRecordSpr.setPosition(((backgroudSprPX + backgroudSprWidth) - mNewRecordSpr.getWidth()) - (5.0f * CommonConst.RALE_SAMALL_VALUE), backgroudSprPY + (68.0f * CommonConst.RALE_SAMALL_VALUE));
            getTopLayer().addEntity(mNewRecordSpr);
        }
        r5.setPosition(backgroudSprPX + stepValue, (backgroudSprPY + backgroudSprHeight) - (r5.getHeight() / 2.0f));
        registerTouchArea(r5);
        getTopLayer().addEntity(r5);
        AnonymousClass2 r8 = new BaseSprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_REPLAY.mKey), false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                GameOverDialog.this.closeBtn(BtnType.REPLAY_BTN);
                return true;
            }
        };
        float spriteReplayWidth = r8.getWidth();
        float spriteReplayHeight = r8.getHeight();
        float spriteReplaySprPX = backgroudSprPX + ((backgroudSprWidth - spriteReplayWidth) / 2.0f);
        float spriteReplaySprPY = (backgroudSprPY + backgroudSprHeight) - (spriteReplayHeight / 2.0f);
        if (!pIsThreeBtn) {
            spriteReplaySprPX = ((backgroudSprPX + backgroudSprWidth) - spriteReplayWidth) - stepValue;
            spriteReplaySprPY = (backgroudSprPY + backgroudSprHeight) - (spriteReplayHeight / 2.0f);
        } else {
            AnonymousClass3 r9 = new BaseSprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, CommonConst.RALE_SAMALL_VALUE, this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_NEXTLEVEL.mKey), false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    GameOverDialog.this.closeBtn(BtnType.NEXTLEVEL_BTN);
                    return true;
                }
            };
            r9.setPosition(((backgroudSprPX + backgroudSprWidth) - r9.getWidth()) - stepValue, (backgroudSprPY + backgroudSprHeight) - (r9.getHeight() / 2.0f));
            registerTouchArea(r9);
            getTopLayer().addEntity(r9);
        }
        r8.setPosition(spriteReplaySprPX, spriteReplaySprPY);
        registerTouchArea(r8);
        getTopLayer().addEntity(r8);
        BaseStrokeFont dialogTitleFont = this.mContext.mResource.getStrokeFontByKey(Resource.STROKEFONT.DIALOG_TITLE.mKey);
        BaseStrokeFont dialogTipFont = this.mContext.mResource.getStrokeFontByKey(Resource.STROKEFONT.DIALOG_TIP.mKey);
        BaseStrokeFont dialogContentFont = this.mContext.mResource.getStrokeFontByKey(Resource.STROKEFONT.DIALOG_CONTENT.mKey);
        if (plevelText != null && !plevelText.equals("")) {
            getTopLayer().addEntity(new ChangeableText(backgroudSprPX + (45.0f * CommonConst.RALE_SAMALL_VALUE), backgroudSprPY + (25.0f * CommonConst.RALE_SAMALL_VALUE), dialogTitleFont, plevelText));
        }
        Resources res = this.mContext.getResources();
        String topScoreTxt = res.getString(R.string.str_dilaog_highscore);
        float scoreTxtPX = ((backgroudSprPX + backgroudSprWidth) - ((float) dialogTipFont.getStringWidth(topScoreTxt))) - (45.0f * CommonConst.RALE_SAMALL_VALUE);
        float scoreTxtPY = backgroudSprPY + (8.0f * CommonConst.RALE_SAMALL_VALUE);
        getTopLayer().addEntity(new ChangeableText(scoreTxtPX, scoreTxtPY, dialogTipFont, topScoreTxt));
        getTopLayer().addEntity(new ChangeableText(((backgroudSprPX + backgroudSprWidth) - ((float) dialogTitleFont.getStringWidth(String.valueOf(pHightScore)))) - (45.0f * CommonConst.RALE_SAMALL_VALUE), (((float) dialogTipFont.getLineHeight()) + scoreTxtPY) - (8.0f * CommonConst.RALE_SAMALL_VALUE), dialogTitleFont, String.valueOf(pHightScore)));
        if (pIsSucceed) {
            String passLevelText = res.getString(R.string.str_dilaog_passlevel);
            float passLevelPX = backgroudSprPX + (60.0f * CommonConst.RALE_SAMALL_VALUE);
            float passLevelPY = backgroudSprPY + (85.0f * CommonConst.RALE_SAMALL_VALUE);
            getTopLayer().addEntity(new ChangeableText(passLevelPX, passLevelPY, dialogContentFont, passLevelText));
            if (this.mEnableShowStar) {
                setStar(pStarNum, CommonConst.STAR_NUM, this, 0, passLevelPX + (100.0f * CommonConst.RALE_SAMALL_VALUE), ((float) dialogContentFont.getLineHeight()) + passLevelPY + (15.0f * CommonConst.RALE_SAMALL_VALUE), 1.0f, true);
            }
            String scoreText = res.getString(R.string.str_dilaog_score);
            float scorePY = passLevelPY + (110.0f * CommonConst.RALE_SAMALL_VALUE);
            getTopLayer().addEntity(new ChangeableText(passLevelPX, scorePY, dialogTipFont, scoreText));
            getTopLayer().addEntity(new ChangeableText(((float) dialogTipFont.getStringWidth(scoreText)) + passLevelPX + (20.0f * CommonConst.RALE_SAMALL_VALUE), scorePY, dialogContentFont, String.valueOf(pScore)));
            if (this.mContext.getmGameScene() != null) {
                Utils.setGoogleAnalytics(this.mContext, 2, this.mContext.getmGameScene().mScore, this.mContext.getmGameScene().mMeter);
                return;
            }
            return;
        }
        String failText = res.getString(R.string.str_dilaog_fail);
        getTopLayer().addEntity(new ChangeableText(backgroudSprPX + (120.0f * CommonConst.RALE_SAMALL_VALUE), backgroudSprPY + (150.0f * CommonConst.RALE_SAMALL_VALUE), dialogContentFont, failText));
        if (this.mContext.getmGameScene() != null) {
            Utils.setGoogleAnalytics(this.mContext, 3, this.mContext.getmGameScene().mScore, this.mContext.getmGameScene().mMeter);
        }
    }

    /* access modifiers changed from: private */
    public void closeBtn(BtnType pbtnType) {
        this.mContext.getEngine().getScene().clearChildScene();
        switch (pbtnType) {
            case MENU_BTN:
                this.mContext.getmGameScene().onMenuBtn();
                return;
            case REPLAY_BTN:
                this.mContext.getmGameScene().onReplayBtn();
                return;
            case NEXTLEVEL_BTN:
                this.mContext.getmGameScene().onNextLevelBtn();
                return;
            default:
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [?, ?, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    private float setStar(int pstarValue, int pStarMax, Scene pScene, int pLayerIndex, float pStartPX, float pStartPY, float pRaleValue, boolean pIsleftAdd) {
        TextureRegion region;
        float width = Const.MOVE_LIMITED_SPEED;
        if (!pIsleftAdd) {
            pStartPX -= 30.0f * CommonConst.RALE_SAMALL_VALUE;
        }
        for (int i = 1; i <= pStarMax; i++) {
            if (pstarValue >= i) {
                region = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.STAR_LINGHT.mKey).clone();
            } else {
                region = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.STAR_UNLINGHT.mKey).clone();
            }
            BaseSprite startSprite = new BaseSprite((float) Const.MOVE_LIMITED_SPEED, (float) Const.MOVE_LIMITED_SPEED, CommonConst.RALE_SAMALL_VALUE * pRaleValue, region, false);
            if (pIsleftAdd) {
                startSprite.setPosition((((float) (i - 1)) * (startSprite.getWidth() + 2.0f)) + pStartPX, pStartPY);
            } else {
                startSprite.setPosition(pStartPX - (((float) (pStarMax - i)) * (startSprite.getWidth() + 2.0f)), pStartPY);
            }
            width += startSprite.getWidth();
            pScene.getLayer(pLayerIndex).addEntity(startSprite);
        }
        return width;
    }
}
