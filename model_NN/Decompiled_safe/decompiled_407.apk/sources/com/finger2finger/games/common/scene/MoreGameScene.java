package com.finger2finger.games.common.scene;

import android.content.Intent;
import android.net.Uri;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.F2FVector;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.ShopButton;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.res.MoreGameConst;
import com.finger2finger.games.res.Const;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class MoreGameScene extends F2FScene {
    private final int Layer_Zero = 0;
    private int columnCount = 4;
    private float curretPX = Const.MOVE_LIMITED_SPEED;
    private float curretPY = Const.MOVE_LIMITED_SPEED;
    /* access modifiers changed from: private */
    public long gameupdateTime;
    private int insideLevelCount = 0;
    /* access modifiers changed from: private */
    public boolean isMove = true;
    /* access modifiers changed from: private */
    public boolean isRemainTimeFirst = true;
    private int mCurrentIndex = 0;
    /* access modifiers changed from: private */
    public boolean mEnableOprate = false;
    private float mLeftButtonX = ((float) (CommonConst.CAMERA_WIDTH / 2));
    private AnimatedSprite mNextSprite;
    private int mPageCount = 0;
    private float mPointCenterY = Const.MOVE_LIMITED_SPEED;
    private float[][] mPosition = ((float[][]) Array.newInstance(Float.TYPE, Const.SUBLEVEL_SINGLE_UNIT, 2));
    private float mRightButtonX = ((float) (CommonConst.CAMERA_WIDTH / 2));
    private Sprite mSpriteBack;
    private AnimatedSprite[] mSpritePoint;
    private Sprite[] mSpriteSubLevels;
    private TextureRegion mTRBG;
    private TextureRegion mTRBack;
    private TiledTextureRegion mTRDownPage;
    private TextureRegion[] mTRMoreGames;
    private TiledTextureRegion mTRUpPage;
    private TiledTextureRegion mTTRPoint;
    private AnimatedSprite mUpSprite;
    /* access modifiers changed from: private */
    public int remainTime = 120;
    private float tagetPX = Const.MOVE_LIMITED_SPEED;
    private float tagetPY = Const.MOVE_LIMITED_SPEED;

    public MoreGameScene(F2FGameActivity pContext) {
        super(2, pContext);
        loadResource();
        initializeBackGroud();
        initializeSubLevelInfo();
        ShopButton shopButton = new ShopButton();
        shopButton.enableMoreGame = false;
        shopButton.showShop(pContext, this, 1, "MoreGameScene");
        initializeButton();
        initializePoint();
        initializeUpNextButton();
        updateStatus();
        enableSceneTouch();
        registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.finger2finger.games.common.scene.MoreGameScene.access$102(com.finger2finger.games.common.scene.MoreGameScene, boolean):boolean
             arg types: [com.finger2finger.games.common.scene.MoreGameScene, int]
             candidates:
              com.finger2finger.games.common.scene.F2FScene.access$102(com.finger2finger.games.common.scene.F2FScene, android.view.MotionEvent):android.view.MotionEvent
              com.finger2finger.games.common.scene.MoreGameScene.access$102(com.finger2finger.games.common.scene.MoreGameScene, boolean):boolean */
            public void onUpdate(float pSecondsElapsed) {
                if (!MoreGameScene.this.mEnableOprate) {
                    if (MoreGameScene.this.isRemainTimeFirst) {
                        int unused = MoreGameScene.this.remainTime = 1;
                        long unused2 = MoreGameScene.this.gameupdateTime = System.currentTimeMillis();
                        boolean unused3 = MoreGameScene.this.isRemainTimeFirst = false;
                    }
                    if (System.currentTimeMillis() - MoreGameScene.this.gameupdateTime >= 200) {
                        long unused4 = MoreGameScene.this.gameupdateTime = System.currentTimeMillis();
                        int unused5 = MoreGameScene.this.remainTime = MoreGameScene.this.remainTime - 1;
                    }
                    if (MoreGameScene.this.remainTime <= 0) {
                        boolean unused6 = MoreGameScene.this.mEnableOprate = true;
                        boolean unused7 = MoreGameScene.this.isMove = false;
                    }
                }
            }
        });
    }

    public void loadScene() {
    }

    private void LoadExtras() {
        this.insideLevelCount = MoreGameConst.enableInstallGame.size();
        this.mSpriteSubLevels = new Sprite[this.insideLevelCount];
        this.mSpritePoint = new AnimatedSprite[this.insideLevelCount];
        this.mPageCount = this.insideLevelCount % Const.SUBLEVEL_SINGLE_UNIT != 0 ? (this.insideLevelCount / Const.SUBLEVEL_SINGLE_UNIT) + 1 : this.insideLevelCount / Const.SUBLEVEL_SINGLE_UNIT;
    }

    private void loadResource() {
        loadTextrue();
        LoadExtras();
    }

    private void loadTextrue() {
        this.mTRBG = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.MOREGAME_BG.mKey).clone();
        this.mTRBack = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_LEFT.mKey);
        this.mTRUpPage = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_UP_PAGE.mKey);
        this.mTRDownPage = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_DOWN_PAGE.mKey);
        this.mTTRPoint = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.TILED_POINT.mKey);
        this.mTRMoreGames = this.mContext.commonResource.getArrayTextureRegionByKey(CommonResource.TEXTURE.MORE_GAME.mKey);
    }

    private void initializeSubLevelInfo() {
        float unitLength_x = 100.0f * CommonConst.RALE_SAMALL_VALUE;
        float unitLength_y = 91.0f * CommonConst.RALE_SAMALL_VALUE;
        float offset_x = unitLength_x / 4.0f;
        float x = offset_x;
        float y = 100.0f * CommonConst.RALE_SAMALL_VALUE;
        int i = 0;
        while (true) {
            if (i >= Const.SUBLEVEL_SINGLE_UNIT) {
                break;
            }
            x = x + unitLength_x + offset_x;
            if (x > ((float) CommonConst.CAMERA_WIDTH)) {
                this.columnCount = i - 1;
                break;
            }
            i++;
        }
        float rowLength = (((float) this.columnCount) * (unitLength_x + offset_x)) - offset_x;
        float x2 = (((float) CommonConst.CAMERA_WIDTH) - rowLength) / 2.0f;
        int column = 0;
        for (int i2 = 0; i2 < this.insideLevelCount; i2++) {
            if (column + 1 > this.columnCount) {
                y += 1.5f * unitLength_y;
                x2 = (((float) CommonConst.CAMERA_WIDTH) - rowLength) / 2.0f;
                column = 0;
            }
            savePosition(i2, x2, y);
            float x3 = getPosition(i2).getX();
            y = getPosition(i2).getY();
            this.mSpriteSubLevels[i2] = new Sprite(x3, y, ((float) this.mTRMoreGames[i2].getWidth()) * 1.4f * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTRMoreGames[i2].getHeight()) * 1.4f * CommonConst.RALE_SAMALL_VALUE, this.mTRMoreGames[i2].clone());
            getLayer(0).addEntity(this.mSpriteSubLevels[i2]);
            x2 = x3 + unitLength_x + offset_x;
            column++;
        }
    }

    private void savePosition(int pIndex, float pX, float pY) {
        if (pIndex >= 0 && pIndex < Const.SUBLEVEL_SINGLE_UNIT) {
            this.mPosition[pIndex][0] = pX;
            this.mPosition[pIndex][1] = pY;
        }
    }

    private F2FVector getPosition(int pIndex) {
        int index = pIndex % Const.SUBLEVEL_SINGLE_UNIT;
        return new F2FVector(this.mPosition[index][0], this.mPosition[index][1]);
    }

    private void initializeBackGroud() {
        getLayer(0).addEntity(new Sprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mTRBG));
    }

    private void initializeButton() {
        this.mSpriteBack = new Sprite(((float) this.mTRBack.getWidth()) * CommonConst.RALE_SAMALL_VALUE * 0.2f, ((float) CommonConst.CAMERA_HEIGHT) - ((((float) this.mTRBack.getHeight()) * CommonConst.RALE_SAMALL_VALUE) * 1.2f), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRBack.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRBack.getHeight()), this.mTRBack) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                MoreGameScene.this.backToMenu();
                return true;
            }
        };
        getLayer(0).addEntity(this.mSpriteBack);
        registerTouchArea(this.mSpriteBack);
    }

    private void initializePoint() {
        if (this.mPageCount > 0) {
            float offset = 20.0f * CommonConst.RALE_SAMALL_VALUE;
            float pointLength = Const.MOVE_LIMITED_SPEED;
            for (int i = 0; i < this.mPageCount; i++) {
                pointLength = pointLength + (((float) this.mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
            }
            float y = ((float) CommonConst.CAMERA_HEIGHT) - ((((float) this.mTTRPoint.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) * 2.0f);
            float x = (((float) CommonConst.CAMERA_WIDTH) - (pointLength - offset)) / 2.0f;
            this.mLeftButtonX = x - (((float) this.mTRUpPage.getTileWidth()) * 1.5f);
            this.mLeftButtonX -= offset;
            this.mPointCenterY = ((float) (this.mTTRPoint.getTileHeight() / 2)) + y;
            for (int i2 = 0; i2 < this.mPageCount; i2++) {
                this.mSpritePoint[i2] = new AnimatedSprite(x, y, ((float) this.mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTTRPoint.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTTRPoint.clone());
                x = x + (((float) this.mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
                if (i2 == this.mCurrentIndex) {
                    this.mSpritePoint[i2].setCurrentTileIndex(0);
                } else {
                    this.mSpritePoint[i2].setCurrentTileIndex(1);
                }
                getLayer(0).addEntity(this.mSpritePoint[i2]);
            }
            this.mRightButtonX = (((float) this.mTRUpPage.getTileWidth()) * 0.5f) + x;
        }
    }

    private void initializeUpNextButton() {
        float up_next_y = this.mPointCenterY - ((((float) this.mTRUpPage.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f);
        this.mUpSprite = new AnimatedSprite(this.mLeftButtonX, up_next_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRUpPage.getTileWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRUpPage.getTileHeight()), this.mTRUpPage.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                MoreGameScene.this.doUp();
                return true;
            }
        };
        getLayer(0).addEntity(this.mUpSprite);
        registerTouchArea(this.mUpSprite);
        this.mNextSprite = new AnimatedSprite(this.mRightButtonX, up_next_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRDownPage.getTileWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRDownPage.getTileHeight()), this.mTRDownPage.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                MoreGameScene.this.doNext();
                return true;
            }
        };
        getLayer(0).addEntity(this.mNextSprite);
        registerTouchArea(this.mNextSprite);
    }

    private boolean checkIsLastPage(int pIndex) {
        if (pIndex + 1 >= this.mPageCount) {
            return true;
        }
        return false;
    }

    private void updateStatus() {
        if (this.mCurrentIndex == 0) {
            this.mUpSprite.setCurrentTileIndex(1);
        } else {
            this.mUpSprite.setCurrentTileIndex(0);
        }
        if (checkIsLastPage(this.mCurrentIndex)) {
            this.mNextSprite.setCurrentTileIndex(1);
        } else {
            this.mNextSprite.setCurrentTileIndex(0);
        }
        for (int i = 0; i < this.insideLevelCount; i++) {
            if (i < this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT || i >= (this.mCurrentIndex + 1) * Const.SUBLEVEL_SINGLE_UNIT) {
                this.mSpriteSubLevels[i].setVisible(false);
            } else {
                this.mSpriteSubLevels[i].setVisible(true);
            }
        }
        for (int i2 = 0; i2 < this.mPageCount; i2++) {
            if (i2 == this.mCurrentIndex) {
                this.mSpritePoint[i2].setCurrentTileIndex(0);
            } else {
                this.mSpritePoint[i2].setCurrentTileIndex(1);
            }
        }
    }

    /* access modifiers changed from: private */
    public void doUp() {
        if (this.mCurrentIndex - 1 >= 0) {
            this.mCurrentIndex--;
            updateStatus();
        }
    }

    /* access modifiers changed from: private */
    public void doNext() {
        if (!checkIsLastPage(this.mCurrentIndex)) {
            this.mCurrentIndex++;
            updateStatus();
        }
    }

    public void backToMenu() {
        this.isMove = true;
        this.mContext.setStatus(this.mContext.mLastStatus);
    }

    public void handleTouchEvent(ArrayList<TouchEvent> touchEvents) {
        for (int i = 0; i < touchEvents.size(); i++) {
            TouchEvent pSceneTouchEvent = touchEvents.get(i);
            if (pSceneTouchEvent != null) {
                if (pSceneTouchEvent.getAction() == 0) {
                    this.tagetPX = pSceneTouchEvent.getX();
                    this.tagetPY = pSceneTouchEvent.getY();
                } else if (pSceneTouchEvent.getAction() != 1) {
                    continue;
                } else if (!this.isMove) {
                    this.curretPX = pSceneTouchEvent.getX();
                    this.curretPY = pSceneTouchEvent.getY();
                    if (Math.abs(this.tagetPX - this.curretPX) < CommonConst.MIN_TOUCH_DEFAUT * CommonConst.RALE_SAMALL_VALUE) {
                        int j = 0;
                        while (j < this.insideLevelCount) {
                            if (this.mSpriteSubLevels[j] == null || !checkInTouch(this.mSpriteSubLevels[j], this.tagetPX, this.tagetPY) || !checkInTouch(this.mSpriteSubLevels[j], this.curretPX, this.curretPY)) {
                                j++;
                            } else {
                                onAreaTouch(j);
                                return;
                            }
                        }
                        continue;
                    } else if (Math.abs(this.tagetPX - this.curretPX) >= CommonConst.MIN_TOUCH_DEFAUT * CommonConst.RALE_SAMALL_VALUE) {
                        doUpDown(this.tagetPX, this.curretPX);
                    }
                } else {
                    return;
                }
            }
        }
    }

    private boolean checkInTouch(Sprite pBaseSprite, float pTouchX, float pTouchY) {
        if (pBaseSprite == null) {
            return false;
        }
        return pTouchX >= pBaseSprite.getX() && pTouchX <= pBaseSprite.getX() + pBaseSprite.getWidth() && pTouchY >= pBaseSprite.getY() && pTouchY <= pBaseSprite.getY() + pBaseSprite.getHeight();
    }

    private void onAreaTouch(int i) {
        int n = (this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT) + i;
        GoogleAnalyticsUtils.setTracker("/GoMarket/" + MoreGameConst.enableInstallGame.get(n)[1]);
        Intent installIntent = new Intent("android.intent.action.VIEW");
        installIntent.setData(Uri.parse(MoreGameConst.enableInstallGame.get(n)[1]));
        this.mContext.startActivity(installIntent);
    }

    private void doUpDown(float orginal_x, float target_x) {
        if (target_x - orginal_x > Const.MOVE_LIMITED_SPEED) {
            doUp();
        } else {
            doNext();
        }
    }
}
