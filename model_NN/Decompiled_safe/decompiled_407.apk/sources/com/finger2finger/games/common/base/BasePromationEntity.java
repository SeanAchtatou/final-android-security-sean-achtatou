package com.finger2finger.games.common.base;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.res.Const;

public class BasePromationEntity {
    public String apkUrlPath0 = "";
    public String apkUrlPath1 = "";
    public String apkUrlPath2 = "";
    public int duration;
    public String packageName = "";
    public String promationActionInfo;
    public float promationActionPicPX;
    public float promationActionPicPY;
    public CommonConst.PROMATION_TYPE promationType = CommonConst.PROMATION_TYPE.SHOW;
    public String promationaction;
    public String promotionIcon = "";
    public String promotionPortBg;
    public long promotionPortSize;
    public String promotionactionpic = "promotion_action.png";
    public long promotionactionpicsize;
    public String promotionbgPath = "promotion_bg.png";
    public long promotionbgsize;
    public long promotioniconSize;
    public long updatetime;

    public BasePromationEntity(String pPackageName, String pUpdatetime, String pDuration, String pPromotionbg, String pPromotionPortBg, String pPromotionbgsize, String pPromotionPortBgsize, String pPromotionactionpic, String pPromotionactionpicsize, String pPromtionactionpicposion, String pActionPortPostion, String pPromationaction, String pApkUrlPath0, String pApkUrlPath1, String pApkUrlPath2, String pIconName, String pIconSize) {
        String[] actionInfo;
        String[] posion;
        this.packageName = pPackageName;
        if (Utils.isNumeric(pUpdatetime)) {
            this.updatetime = Long.parseLong(pUpdatetime);
        } else {
            this.updatetime = 0;
        }
        if (Utils.isNumeric(pDuration)) {
            this.duration = Integer.parseInt(pDuration);
        } else {
            this.duration = 0;
        }
        this.promotionbgPath = pPromotionbg;
        this.promotionPortBg = pPromotionPortBg;
        if (Utils.isNumeric(pPromotionbgsize)) {
            this.promotionbgsize = Long.parseLong(pPromotionbgsize);
        } else {
            this.promotionbgsize = 0;
        }
        if (Utils.isNumeric(pPromotionPortBgsize)) {
            this.promotionPortSize = Long.parseLong(pPromotionPortBgsize);
        } else {
            this.promotionPortSize = 0;
        }
        this.promotionactionpic = pPromotionactionpic;
        if (Utils.isNumeric(pPromotionactionpicsize)) {
            this.promotionactionpicsize = Long.parseLong(pPromotionactionpicsize);
        } else {
            this.promotionactionpicsize = 0;
        }
        if (pActionPortPostion != null && !pActionPortPostion.equals("") && (posion = pActionPortPostion.split(Const.SEPARATOR_ITEM_COMMA)) != null && posion.length == 2) {
            this.promationActionPicPX = Float.parseFloat(posion[0]);
            this.promationActionPicPY = Float.parseFloat(posion[1]);
        }
        if (pPromationaction != null && !pPromationaction.equals("") && (actionInfo = pPromationaction.split(Const.SEPARATOR_ITEM_BRANCH)) != null && actionInfo.length == 2) {
            this.promationType = Const.getPromationType(actionInfo[0]);
            if (this.promationType == CommonConst.PROMATION_TYPE.ZOOM) {
                this.promationActionInfo = actionInfo[1];
            }
        }
        this.apkUrlPath0 = pApkUrlPath0;
        this.apkUrlPath1 = pApkUrlPath1;
        this.apkUrlPath2 = pApkUrlPath2;
        this.promotionIcon = pIconName;
        if (Utils.isNumeric(pIconSize)) {
            this.promotioniconSize = Long.parseLong(pIconSize);
        } else {
            this.promotioniconSize = 0;
        }
    }
}
