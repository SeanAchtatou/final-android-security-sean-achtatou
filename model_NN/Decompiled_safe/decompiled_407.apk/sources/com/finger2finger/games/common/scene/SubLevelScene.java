package com.finger2finger.games.common.scene;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.F2FVector;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.ShopButton;
import com.finger2finger.games.common.SubLevelUnit;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.res.SMSConst;
import com.finger2finger.games.common.store.data.LevelEntity;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import java.lang.reflect.Array;
import java.util.ArrayList;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.font.Font;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class SubLevelScene extends F2FScene {
    private final int Layer_One = 1;
    private final int Layer_Zero = 0;
    private int clickLevelIndex = -1;
    private int columnCount = 4;
    private float curretPX = Const.MOVE_LIMITED_SPEED;
    private float curretPY = Const.MOVE_LIMITED_SPEED;
    private String format_score;
    /* access modifiers changed from: private */
    public long gameupdateTime;
    private int insideLevelCount = 0;
    private boolean isFirstShowSMSDialog = true;
    /* access modifiers changed from: private */
    public boolean isMove = true;
    /* access modifiers changed from: private */
    public boolean isRemainTimeFirst = true;
    private ArrayList<LevelEntity> levelList = new ArrayList<>();
    private int mCurrentIndex = 0;
    /* access modifiers changed from: private */
    public boolean mEnableOprate = false;
    private Font mFont;
    private float mLeftButtonX = ((float) (CommonConst.CAMERA_WIDTH / 2));
    private SubLevelUnit[] mLevelProperty;
    private AnimatedSprite mNextSprite;
    private int mPageCount = 0;
    private float mPointCenterY = Const.MOVE_LIMITED_SPEED;
    private float[][] mPosition = ((float[][]) Array.newInstance(Float.TYPE, Const.SUBLEVEL_SINGLE_UNIT, 2));
    private float mRightButtonX = ((float) (CommonConst.CAMERA_WIDTH / 2));
    private Sprite mSpriteBack;
    private AnimatedSprite[] mSpritePoint;
    private Sprite[] mSpriteSubLevels;
    private TextureRegion mTRBG;
    private TextureRegion mTRBack;
    private TiledTextureRegion mTRDownPage;
    private TextureRegion[] mTRLevelIndex = new TextureRegion[10];
    private TextureRegion mTRStarGray;
    private TextureRegion mTRStarLight;
    private TextureRegion mTRSubLevelDisable;
    private TextureRegion mTRSubLevelEnable;
    private TiledTextureRegion mTRUpPage;
    private TiledTextureRegion mTTRPoint;
    private AnimatedSprite mUpSprite;
    /* access modifiers changed from: private */
    public int remainTime = 120;
    private int sublevel = 0;
    private float tagetPX = Const.MOVE_LIMITED_SPEED;
    private float tagetPY = Const.MOVE_LIMITED_SPEED;

    public SubLevelScene(F2FGameActivity pContext) {
        super(2, pContext);
        loadResource();
        initializeBackGroud();
        new ShopButton().showShop(this.mContext, this, 1, "SubLevelScene");
        initializeSubLevelInfo();
        initializeButton();
        initializePoint();
        initializeUpNextButton();
        updateStatus();
        enableSceneTouch();
        registerUpdateHandler(new IUpdateHandler() {
            public void reset() {
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.finger2finger.games.common.scene.SubLevelScene.access$102(com.finger2finger.games.common.scene.SubLevelScene, boolean):boolean
             arg types: [com.finger2finger.games.common.scene.SubLevelScene, int]
             candidates:
              com.finger2finger.games.common.scene.F2FScene.access$102(com.finger2finger.games.common.scene.F2FScene, android.view.MotionEvent):android.view.MotionEvent
              com.finger2finger.games.common.scene.SubLevelScene.access$102(com.finger2finger.games.common.scene.SubLevelScene, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.finger2finger.games.common.scene.SubLevelScene.access$502(com.finger2finger.games.common.scene.SubLevelScene, boolean):boolean
             arg types: [com.finger2finger.games.common.scene.SubLevelScene, int]
             candidates:
              com.finger2finger.games.common.scene.F2FScene.access$502(com.finger2finger.games.common.scene.F2FScene, org.anddev.andengine.input.touch.TouchEvent):org.anddev.andengine.input.touch.TouchEvent
              com.finger2finger.games.common.scene.SubLevelScene.access$502(com.finger2finger.games.common.scene.SubLevelScene, boolean):boolean */
            public void onUpdate(float pSecondsElapsed) {
                SubLevelScene.this.checkSMSStatus();
                if (!SubLevelScene.this.mEnableOprate) {
                    if (SubLevelScene.this.isRemainTimeFirst) {
                        int unused = SubLevelScene.this.remainTime = 1;
                        long unused2 = SubLevelScene.this.gameupdateTime = System.currentTimeMillis();
                        boolean unused3 = SubLevelScene.this.isRemainTimeFirst = false;
                    }
                    if (System.currentTimeMillis() - SubLevelScene.this.gameupdateTime >= 200) {
                        long unused4 = SubLevelScene.this.gameupdateTime = System.currentTimeMillis();
                        int unused5 = SubLevelScene.this.remainTime = SubLevelScene.this.remainTime - 1;
                    }
                    if (SubLevelScene.this.remainTime <= 0) {
                        boolean unused6 = SubLevelScene.this.mEnableOprate = true;
                        boolean unused7 = SubLevelScene.this.isMove = false;
                    }
                }
            }
        });
    }

    public void loadScene() {
    }

    private void LoadExtras() {
        this.insideLevelCount = PortConst.LevelInfo[Const.GAME_MODEID][this.sublevel];
        this.mSpriteSubLevels = new Sprite[this.insideLevelCount];
        this.mLevelProperty = new SubLevelUnit[this.insideLevelCount];
        this.mSpritePoint = new AnimatedSprite[this.insideLevelCount];
        this.mPageCount = this.insideLevelCount % Const.SUBLEVEL_SINGLE_UNIT != 0 ? (this.insideLevelCount / Const.SUBLEVEL_SINGLE_UNIT) + 1 : this.insideLevelCount / Const.SUBLEVEL_SINGLE_UNIT;
    }

    private void loadResource() {
        loadTextrue();
        LoadExtras();
        loadSubLevelInfos();
        loadStringsFromXml();
    }

    private void loadStringsFromXml() {
        this.format_score = this.mContext.getResources().getString(R.string.str_score_formart);
    }

    private void loadTextrue() {
        this.mTRBG = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_BG.mKey);
        this.mTRBack = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_LEFT.mKey);
        this.mTRUpPage = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_UP_PAGE.mKey);
        this.mTRDownPage = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_DOWN_PAGE.mKey);
        this.mTRStarLight = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.STAR_LINGHT.mKey);
        this.mTRStarGray = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.STAR_UNLINGHT.mKey);
        this.mTTRPoint = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.TILED_POINT.mKey);
        this.mTRSubLevelEnable = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_ENABLE.mKey);
        this.mTRSubLevelDisable = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_DISABLE.mKey);
        this.mTRLevelIndex = this.mContext.commonResource.getArrayTextureRegionByKey(CommonResource.TEXTURE.NUMBER.mKey);
        this.mFont = this.mContext.mResource.getBaseFontByKey(Resource.FONT.SUBLEVEL_SCORE.mKey);
    }

    private void loadSubLevelInfos() {
        this.levelList = this.mContext.getMGameInfo().getLevelInfoByModeID(Const.GAME_MODEID);
    }

    private void initializeSubLevelInfo() {
        TextureRegion textureRegion;
        this.mTRSubLevelEnable = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_ENABLE.mKey);
        this.mTRSubLevelDisable = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_DISABLE.mKey);
        float unitLength_x = ((float) this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_ENABLE.mKey).getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float unitLength_y = ((float) this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.SUBLEVEL_ENABLE.mKey).getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float offset_x = unitLength_x / 4.0f;
        float x = offset_x;
        float y = 100.0f * CommonConst.RALE_SAMALL_VALUE;
        int i = 0;
        while (true) {
            if (i >= Const.SUBLEVEL_SINGLE_UNIT) {
                break;
            }
            x = x + unitLength_x + offset_x;
            if (x > ((float) CommonConst.CAMERA_WIDTH)) {
                this.columnCount = i - 1;
                break;
            }
            i++;
        }
        float rowLength = (((float) this.columnCount) * (unitLength_x + offset_x)) - offset_x;
        float x2 = (((float) CommonConst.CAMERA_WIDTH) - rowLength) / 2.0f;
        int column = 0;
        for (int i2 = 0; i2 < this.insideLevelCount; i2++) {
            if (column + 1 > this.columnCount) {
                y += 1.5f * unitLength_y;
                x2 = (((float) CommonConst.CAMERA_WIDTH) - rowLength) / 2.0f;
                column = 0;
            }
            savePosition(i2, x2, y);
            float x3 = getPosition(i2).getX();
            y = getPosition(i2).getY();
            boolean isEnable = this.levelList.get(i2).getIsEnable();
            Sprite[] spriteArr = this.mSpriteSubLevels;
            float width = ((float) this.mTRSubLevelEnable.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
            float height = ((float) this.mTRSubLevelEnable.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
            if (isEnable) {
                textureRegion = this.mTRSubLevelEnable;
            } else {
                textureRegion = this.mTRSubLevelDisable;
            }
            spriteArr[i2] = new Sprite(x3, y, width, height, textureRegion);
            getLayer(0).addEntity(this.mSpriteSubLevels[i2]);
            this.mLevelProperty[i2] = new SubLevelUnit();
            float center_x = x3 + (unitLength_x / 2.0f);
            float center_y = y + (unitLength_y / 2.0f);
            float star_x = x3 + (17.5f * CommonConst.RALE_SAMALL_VALUE);
            float star_y = ((this.mSpriteSubLevels[i2].getHeight() + y) - ((((float) this.mTRStarLight.getHeight()) * CommonConst.RALE_SMALL_STAR) * CommonConst.RALE_SAMALL_VALUE)) - (13.5f * CommonConst.RALE_SAMALL_VALUE);
            float under_center_x = x3 + (unitLength_x / 2.0f);
            float under_center_y = y + unitLength_y + (10.0f * CommonConst.RALE_SAMALL_VALUE);
            if (isEnable) {
                this.mLevelProperty[i2].setmNumberEntity(i2, this, 1, center_x, center_y, this.mTRLevelIndex);
                this.mLevelProperty[i2].setmStarEntity(this.levelList.get(i2).getStar(), this, 1, star_x, star_y, this.mTRStarLight, this.mTRStarGray);
                this.mLevelProperty[i2].setmExplainEntity(this, 1, under_center_x, under_center_y, String.format(this.format_score, Integer.valueOf(this.levelList.get(i2).getLevelScoreMax())), this.mFont, isEnable);
                this.mLevelProperty[i2].getmNumberEntity().addLevelIndex();
                this.mLevelProperty[i2].getmStarEntity().addStars();
                this.mLevelProperty[i2].getmExplainEntity().addExplain();
            }
            x2 = x3 + unitLength_x + offset_x;
            column++;
        }
    }

    private void savePosition(int pIndex, float pX, float pY) {
        if (pIndex >= 0 && pIndex < Const.SUBLEVEL_SINGLE_UNIT) {
            this.mPosition[pIndex][0] = pX;
            this.mPosition[pIndex][1] = pY;
        }
    }

    private F2FVector getPosition(int pIndex) {
        int index = pIndex % Const.SUBLEVEL_SINGLE_UNIT;
        return new F2FVector(this.mPosition[index][0], this.mPosition[index][1]);
    }

    private void initializeBackGroud() {
        getLayer(0).addEntity(new Sprite(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT, this.mTRBG));
    }

    private void initializeButton() {
        this.mSpriteBack = new Sprite(((float) this.mTRBack.getWidth()) * CommonConst.RALE_SAMALL_VALUE * 0.2f, ((float) CommonConst.CAMERA_HEIGHT) - ((((float) this.mTRBack.getHeight()) * CommonConst.RALE_SAMALL_VALUE) * 1.2f), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRBack.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRBack.getHeight()), this.mTRBack) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                SubLevelScene.this.backToMenu();
                return true;
            }
        };
        getLayer(0).addEntity(this.mSpriteBack);
        registerTouchArea(this.mSpriteBack);
    }

    private void initializePoint() {
        if (this.mPageCount > 0) {
            float offset = 20.0f * CommonConst.RALE_SAMALL_VALUE;
            float pointLength = Const.MOVE_LIMITED_SPEED;
            for (int i = 0; i < this.mPageCount; i++) {
                pointLength = pointLength + (((float) this.mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
            }
            float y = ((float) CommonConst.CAMERA_HEIGHT) - ((((float) this.mTTRPoint.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) * 2.0f);
            float x = (((float) CommonConst.CAMERA_WIDTH) - (pointLength - offset)) / 2.0f;
            this.mLeftButtonX = x - (((float) this.mTRUpPage.getTileWidth()) * 1.5f);
            this.mLeftButtonX -= offset;
            this.mPointCenterY = ((float) (this.mTTRPoint.getTileHeight() / 2)) + y;
            for (int i2 = 0; i2 < this.mPageCount; i2++) {
                this.mSpritePoint[i2] = new AnimatedSprite(x, y, ((float) this.mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE, ((float) this.mTTRPoint.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTTRPoint.clone());
                x = x + (((float) this.mTTRPoint.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset;
                if (i2 == this.mCurrentIndex) {
                    this.mSpritePoint[i2].setCurrentTileIndex(0);
                } else {
                    this.mSpritePoint[i2].setCurrentTileIndex(1);
                }
                getLayer(0).addEntity(this.mSpritePoint[i2]);
            }
            this.mRightButtonX = (((float) this.mTRUpPage.getTileWidth()) * 0.5f) + x;
        }
    }

    private void initializeUpNextButton() {
        float up_next_y = this.mPointCenterY - ((((float) this.mTRUpPage.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) / 2.0f);
        this.mUpSprite = new AnimatedSprite(this.mLeftButtonX, up_next_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRUpPage.getTileWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRUpPage.getTileHeight()), this.mTRUpPage.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                SubLevelScene.this.doUp();
                return true;
            }
        };
        getLayer(0).addEntity(this.mUpSprite);
        registerTouchArea(this.mUpSprite);
        this.mNextSprite = new AnimatedSprite(this.mRightButtonX, up_next_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRDownPage.getTileWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTRDownPage.getTileHeight()), this.mTRDownPage.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                SubLevelScene.this.doNext();
                return true;
            }
        };
        getLayer(0).addEntity(this.mNextSprite);
        registerTouchArea(this.mNextSprite);
    }

    private boolean checkIsLastPage(int pIndex) {
        if (pIndex + 1 >= this.mPageCount) {
            return true;
        }
        return false;
    }

    private void updateStatus() {
        if (this.mCurrentIndex == 0) {
            this.mUpSprite.setCurrentTileIndex(1);
        } else {
            this.mUpSprite.setCurrentTileIndex(0);
        }
        if (checkIsLastPage(this.mCurrentIndex)) {
            this.mNextSprite.setCurrentTileIndex(1);
        } else {
            this.mNextSprite.setCurrentTileIndex(0);
        }
        for (int i = 0; i < this.insideLevelCount; i++) {
            if (i < this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT || i >= (this.mCurrentIndex + 1) * Const.SUBLEVEL_SINGLE_UNIT) {
                this.mSpriteSubLevels[i].setVisible(false);
                this.mLevelProperty[i].disable();
            } else {
                this.mSpriteSubLevels[i].setVisible(true);
                this.mLevelProperty[i].enable();
            }
        }
        for (int i2 = 0; i2 < this.mPageCount; i2++) {
            if (i2 == this.mCurrentIndex) {
                this.mSpritePoint[i2].setCurrentTileIndex(0);
            } else {
                this.mSpritePoint[i2].setCurrentTileIndex(1);
            }
        }
    }

    /* access modifiers changed from: private */
    public void doUp() {
        if (this.mCurrentIndex - 1 >= 0) {
            this.mCurrentIndex--;
            updateStatus();
        }
    }

    /* access modifiers changed from: private */
    public void doNext() {
        if (!checkIsLastPage(this.mCurrentIndex)) {
            this.mCurrentIndex++;
            updateStatus();
        }
    }

    /* access modifiers changed from: private */
    public void backToMenu() {
        this.isMove = true;
        this.mContext.commonResource.unLoadSubLevelResource();
        this.mContext.setStatus(F2FGameActivity.Status.LEVEL_OPTION);
    }

    public void handleTouchEvent(ArrayList<TouchEvent> touchEvents) {
        for (int i = 0; i < touchEvents.size(); i++) {
            TouchEvent pSceneTouchEvent = touchEvents.get(i);
            if (pSceneTouchEvent != null) {
                if (pSceneTouchEvent.getAction() == 0) {
                    this.tagetPX = pSceneTouchEvent.getX();
                    this.tagetPY = pSceneTouchEvent.getY();
                } else if (pSceneTouchEvent.getAction() != 1) {
                    continue;
                } else if (!this.isMove) {
                    this.curretPX = pSceneTouchEvent.getX();
                    this.curretPY = pSceneTouchEvent.getY();
                    if (Math.abs(this.tagetPX - this.curretPX) < CommonConst.MIN_TOUCH_DEFAUT * CommonConst.RALE_SAMALL_VALUE) {
                        int j = 0;
                        while (j < this.insideLevelCount) {
                            if (this.mSpriteSubLevels[j] == null || !checkInTouch(this.mSpriteSubLevels[j], this.tagetPX, this.tagetPY) || !checkInTouch(this.mSpriteSubLevels[j], this.curretPX, this.curretPY)) {
                                j++;
                            } else {
                                onAreaTouch(j);
                                return;
                            }
                        }
                        continue;
                    } else if (Math.abs(this.tagetPX - this.curretPX) >= CommonConst.MIN_TOUCH_DEFAUT * CommonConst.RALE_SAMALL_VALUE) {
                        doUpDown(this.tagetPX, this.curretPX);
                    }
                } else {
                    return;
                }
            }
        }
    }

    private boolean checkInTouch(Sprite pBaseSprite, float pTouchX, float pTouchY) {
        if (pBaseSprite == null) {
            return false;
        }
        return pTouchX >= pBaseSprite.getX() && pTouchX <= pBaseSprite.getX() + pBaseSprite.getWidth() && pTouchY >= pBaseSprite.getY() && pTouchY <= pBaseSprite.getY() + pBaseSprite.getHeight();
    }

    private void onAreaTouch(int i) {
        if (this.levelList.get((this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT) + i).getIsEnable() && (this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT) + i < PortConst.LevelInfo[Const.GAME_MODEID][0]) {
            this.mContext.getMGameInfo().setMSubLevelIndex(this.sublevel);
            this.mContext.getMGameInfo().setMInsideIndex((this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT) + i);
            if (!Const.enableSMS || !checkIsNeedSMS(this.mContext.getMGameInfo().getMLevelIndex(), (i - 1) + (this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT))) {
                this.mContext.setStatus(F2FGameActivity.Status.GAME);
                GoogleAnalyticsUtils.setTracker("play_start_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/from_menu"));
                return;
            }
            this.isSMSSendCanle = false;
            this.mContext.showMsgDialog(17);
            this.clickLevelIndex = (i - 1) + (this.mCurrentIndex * Const.SUBLEVEL_SINGLE_UNIT);
        }
    }

    private void doUpDown(float orginal_x, float target_x) {
        if (target_x - orginal_x > Const.MOVE_LIMITED_SPEED) {
            doUp();
        } else {
            doNext();
        }
    }

    /* access modifiers changed from: private */
    public void checkSMSStatus() {
        if (!Const.enableSMS) {
            return;
        }
        if (this.isSMSSendCanle) {
            this.clickLevelIndex = -1;
            GoogleAnalyticsUtils.setTracker("/Send_SMS/cancle");
        } else if (this.clickLevelIndex != -1) {
            if (this.isFirstShowSMSDialog) {
                this.isFirstShowSMSDialog = false;
                this.mContext.showMsgDialog(19);
                GoogleAnalyticsUtils.setTracker("/Send_SMS/send_in_game_" + String.valueOf(this.clickLevelIndex + 1));
            }
            if (SMSConst.SMS_STATUS == 0) {
                this.mContext.saveSMSSettings(String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.clickLevelIndex));
                this.mContext.showMsgDialog(20);
                this.clickLevelIndex = -1;
                this.isFirstShowSMSDialog = true;
            } else if (SMSConst.SMS_STATUS == 1) {
                this.isFirstShowSMSDialog = true;
                this.mContext.showMsgDialog(18);
            }
        }
    }

    private boolean checkIsNeedSMS(int levelIndex, int sublevelIndex) {
        int i = 0;
        while (i < Const.SMS_SUB_LEVEL.length) {
            if (levelIndex != Const.SMS_SUB_LEVEL[i][0] || sublevelIndex != Const.SMS_SUB_LEVEL[i][1]) {
                i++;
            } else if (!this.mContext.loadSMSPreferences(String.valueOf(levelIndex) + "_" + String.valueOf(sublevelIndex))) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public void setSMSNextProcess() {
        this.mContext.setStatus(F2FGameActivity.Status.GAME);
        GoogleAnalyticsUtils.setTracker("play_start_" + String.valueOf(this.mContext.getMGameInfo().getMLevelIndex()) + "_" + String.valueOf(this.mContext.getMGameInfo().getMInsideIndex() + "/from_menu"));
    }
}
