package com.finger2finger.games.common.res;

import android.app.PendingIntent;
import android.content.Intent;
import android.telephony.SmsManager;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.horseclickclear.lite.R;

public class SMSConst {
    public static final String ACTION_SMS_DELIVERY = "lab.sodino.sms.delivery";
    public static final String ACTION_SMS_SEND = "lab.sodino.sms.send";
    private static int CHARGEITEM_TYPE = -1;
    private static String[] ChargeItems = {"idlt020001", "idlt020002", "idlt020003", "idlt020004", "idlt020006", "idlt020007", "idlt020005"};
    private static final String[] GameName = {"Tropical Fruits", "Easter Chicken", "Crab Revenge III", "Halloween", "Fish Fever", "11", "F2FPuzzle"};
    private static final String NO = "9388";
    public static int SMS_STATUS = -1;
    private static final String TXT_CONTENT = "REG BAHAGIA GM001 %s";

    public static class SMSStatus {
        public static final int FAIL = 1;
        public static final int OK = 0;
        public static final int UNKNOWN = -1;
    }

    private static void setChargeItemType(F2FGameActivity mContext) {
        String gameName;
        if (CHARGEITEM_TYPE == -1 && (gameName = mContext.getResources().getString(R.string.app_name)) != null && !gameName.equals("")) {
            for (int i = 0; i < GameName.length; i++) {
                if (gameName.equals(GameName[i])) {
                    CHARGEITEM_TYPE = i;
                    return;
                }
            }
        }
    }

    public static void sendMSM(F2FGameActivity mContext) {
        SMS_STATUS = -1;
        setChargeItemType(mContext);
        if (CHARGEITEM_TYPE == -1) {
            SMS_STATUS = 1;
            return;
        }
        SmsManager.getDefault().sendTextMessage(NO, null, String.format(TXT_CONTENT, ChargeItems[CHARGEITEM_TYPE]), PendingIntent.getBroadcast(mContext, 0, new Intent(ACTION_SMS_SEND), 0), null);
    }
}
