package com.finger2finger.games.common.store.data;

public class HonorRecordTable {
    public static final int LIST_ITEM_COUNT = 2;
    public int hardWorkDay = 0;
    public int inviteFriendsNum = 0;

    public HonorRecordTable(String[] data) throws Exception {
        if (data == null || data.length != 2) {
            throw new IllegalArgumentException();
        }
        this.hardWorkDay = Integer.parseInt(data[0]);
        this.inviteFriendsNum = Integer.parseInt(data[1]);
    }

    public HonorRecordTable(int pHardWorkDay, int pInviteFriendsNum) {
        this.hardWorkDay = pHardWorkDay;
        this.inviteFriendsNum = pInviteFriendsNum;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.hardWorkDay).append(TablePath.SEPARATOR_ITEM).append(this.inviteFriendsNum);
        return sb.toString();
    }
}
