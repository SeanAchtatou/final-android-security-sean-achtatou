package com.finger2finger.games.common;

import com.finger2finger.games.res.Const;

public class CommonPortConst {
    public static String AdviewId = "SDK20112204370536e71ud9ql2zeq2q1";
    public static String AppSupserRewardsHParameter = "pqmjbzmcsep.711089327275";
    public static String GoogleAnalytics = "UA-22467042-2";
    public static String MARKET_LINK = "market://details?id=";
    public static String MMAdviewID = "28911";
    public static String[] PARTERNERLOGO_PATH = new String[0];
    public static long PARTERNERLOGO_TIME = 3000;
    public static boolean enableAddGold = false;
    public static boolean enableAds = false;
    public static boolean enableCheckWire = false;
    public static boolean enableDownPic = false;
    public static boolean enableDynamicPic = false;
    public static boolean enableFaceBook = false;
    public static boolean enableGameNightMode = false;
    public static boolean enableGoogleAnalytics = false;
    public static boolean enableHelp = false;
    public static boolean enableHoner = false;
    public static boolean enableInviteFriend = false;
    public static boolean enableLevelOption = false;
    public static boolean enableLiteVersion = false;
    public static boolean enableMMAdView = false;
    public static boolean enableMoreGame = false;
    public static boolean enablePinchZoom = false;
    public static boolean enablePromotion = false;
    public static boolean enableProp = false;
    public static boolean enableQQ = false;
    public static boolean enableReloadResource = false;
    public static boolean enableRenRen = false;
    public static boolean enableSDCard = false;
    public static boolean enableSMS = false;
    public static boolean enableScoreGame = false;
    public static boolean enableShowMarquee = false;
    public static boolean enableSina = false;
    public static boolean enableStoreMode = false;
    public static boolean enableSubLevelOption = false;
    public static boolean enableTwitter = false;
    public static boolean isNoSDCard = false;
    public static String loadLinkUrl = (MARKET_LINK + "com.f2fgames.games.kongfumonk.lite");
    public static long loadingTime = Const.SCORE_X_DURATION;
    public static boolean showPromtionIcon = false;
}
