package com.finger2finger.games.common.store.data;

public class HonorTable {
    public static final int LIST_ITEM_COUNT = 2;
    private int honorNum = 0;
    private String id = "";

    public HonorTable(String[] data) throws Exception {
        if (data == null || data.length != 2) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.honorNum = Integer.parseInt(data[1]);
    }

    public HonorTable(String pID, int pNum) {
        this.id = pID;
        this.honorNum = pNum;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.honorNum);
        return sb.toString();
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public int getHonorNum() {
        return this.honorNum;
    }

    public void setHonorNum(int honorNum2) {
        this.honorNum = honorNum2;
    }
}
