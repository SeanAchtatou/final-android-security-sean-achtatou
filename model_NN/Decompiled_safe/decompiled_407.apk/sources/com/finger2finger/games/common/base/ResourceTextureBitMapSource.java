package com.finger2finger.games.common.base;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import java.io.File;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class ResourceTextureBitMapSource implements ITextureSource {
    public final Context mContext;
    public Bitmap mDrawableResourceMap;
    public String mFilePath;
    public final int mHeight;
    public final int mWidth;

    public ResourceTextureBitMapSource(Context pContext, Bitmap pDrawableResourceMap) {
        this.mContext = pContext;
        this.mDrawableResourceMap = pDrawableResourceMap;
        new BitmapFactory.Options().inJustDecodeBounds = true;
        this.mWidth = pDrawableResourceMap.getWidth();
        this.mHeight = pDrawableResourceMap.getHeight();
    }

    public ResourceTextureBitMapSource(Context pContext, String pAssetPath) {
        this.mContext = pContext;
        this.mFilePath = pAssetPath;
        File file = new File(Environment.getExternalStorageDirectory(), this.mFilePath);
        this.mFilePath = file.getPath();
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        if (file.exists()) {
            decodeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            this.mDrawableResourceMap = BitmapFactory.decodeFile(file.getPath(), decodeOptions);
        }
        this.mWidth = decodeOptions.outWidth;
        this.mHeight = decodeOptions.outHeight;
    }

    protected ResourceTextureBitMapSource(Context pContext, String pFilePath, int pWidth, int pHeight) {
        this.mContext = pContext;
        this.mFilePath = pFilePath;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
    }

    public ResourceTextureBitMapSource clone() {
        return new ResourceTextureBitMapSource(this.mContext, this.mFilePath, this.mWidth, this.mHeight);
    }

    public int getHeight() {
        return this.mHeight;
    }

    public int getWidth() {
        return this.mWidth;
    }

    public Bitmap onLoadBitmap() {
        File file = new File(this.mFilePath);
        if (!file.exists()) {
            return null;
        }
        BitmapFactory.Options decodeOptions = new BitmapFactory.Options();
        decodeOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(file.getPath(), decodeOptions);
    }

    public String toString() {
        return getClass().getSimpleName();
    }
}
