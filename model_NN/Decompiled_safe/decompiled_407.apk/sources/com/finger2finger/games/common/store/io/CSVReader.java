package com.finger2finger.games.common.store.io;

import com.finger2finger.games.common.store.data.TablePath;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    public static final char DEFAULT_QUOTE_CHARACTER = '\"';
    public static final int DEFAULT_SKIP_LINES = 0;
    private BufferedReader br;
    private boolean hasNext;
    private boolean linesSkiped;
    private char quotechar;
    private char separator;
    private int skipLines;

    public CSVReader(Reader reader) {
        this(reader, TablePath.SEPARATOR_ITEM.charAt(0), '\"', 0);
    }

    public CSVReader(Reader reader, String pSepartorItem) {
        this(reader, pSepartorItem.charAt(0), '\"', 0);
    }

    public CSVReader(Reader reader, char separator2, char quotechar2, int line) {
        this.hasNext = true;
        this.br = new BufferedReader(reader);
        this.separator = separator2;
        this.quotechar = quotechar2;
        this.skipLines = line;
    }

    public String[] readNext(String pSeed) throws Exception {
        String nextLine = getNextLine(pSeed);
        if (this.hasNext) {
            return parseLine(nextLine, pSeed);
        }
        return null;
    }

    private String getNextLine(String pSeed) throws Exception {
        if (!this.linesSkiped) {
            for (int i = 0; i < this.skipLines; i++) {
                this.br.readLine();
            }
            this.linesSkiped = true;
        }
        String nextLine = this.br.readLine();
        if (nextLine == null) {
            this.hasNext = false;
        } else {
            try {
                nextLine = F2FCrypto.decrypt(pSeed, nextLine).replaceAll("\n", "");
            } catch (Exception e) {
                throw e;
            }
        }
        if (this.hasNext) {
            return nextLine;
        }
        return null;
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    private String[] parseLine(String nextLine, String pSeed) throws Exception {
        if (nextLine == null) {
            return null;
        }
        List<String> tokensOnThisLine = new ArrayList<>();
        StringBuffer sb = new StringBuffer();
        boolean inQuotes = false;
        do {
            if (inQuotes) {
                sb.append("\n");
                nextLine = getNextLine(pSeed);
                if (nextLine == null) {
                    break;
                }
            }
            int i = 0;
            while (i < nextLine.length()) {
                char c = nextLine.charAt(i);
                if (c == this.quotechar) {
                    if (!inQuotes || nextLine.length() <= i + 1 || nextLine.charAt(i + 1) != this.quotechar) {
                        inQuotes = !inQuotes;
                        if (i > 2 && nextLine.charAt(i - 1) != this.separator && nextLine.length() > i + 1 && nextLine.charAt(i + 1) != this.separator) {
                            sb.append(c);
                        }
                    } else {
                        sb.append(nextLine.charAt(i + 1));
                        i++;
                    }
                } else if (c != this.separator || inQuotes) {
                    sb.append(c);
                } else {
                    tokensOnThisLine.add(sb.toString());
                    sb = new StringBuffer();
                }
                i++;
            }
        } while (inQuotes);
        tokensOnThisLine.add(sb.toString());
        return (String[]) tokensOnThisLine.toArray(new String[0]);
    }

    public void close() throws IOException {
        this.br.close();
    }
}
