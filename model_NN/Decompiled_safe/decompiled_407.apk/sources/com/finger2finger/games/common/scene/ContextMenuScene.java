package com.finger2finger.games.common.scene;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

public class ContextMenuScene extends CameraScene {
    private TextureRegion mBackButtonRegion;
    private Sprite mBackSprite;
    private final float mButtonDistance = 20.0f;
    F2FGameActivity mContext;
    private boolean mEnableHelp = true;
    private boolean mEnableReplay = true;
    private TextureRegion mHelpPicRegion;
    private Sprite mHelpPicSprite;
    private TextureRegion mLogoffRegion;
    private Sprite mLogoffSprite;
    private AnimatedSprite mMusicSprite;
    private TiledTextureRegion mMusicTextureRegion;
    private Rectangle mRect1;
    private Rectangle mRectBlank;
    private Rectangle mRectDarkGreen;
    private TextureRegion mReplayRegion;
    private Sprite mReplaySprite;
    /* access modifiers changed from: private */
    public F2FScene mScene;

    public ContextMenuScene(Camera pCamera, F2FGameActivity pContext) {
        super(1, pCamera);
        this.mContext = pContext;
        intRegion();
        loadScene(true);
    }

    public ContextMenuScene(Camera pCamera, F2FGameActivity pContext, F2FScene pScene, boolean enableReplay, boolean enableHelp) {
        super(1, pCamera);
        this.mContext = pContext;
        this.mEnableReplay = enableReplay;
        this.mEnableHelp = enableHelp;
        this.mScene = pScene;
        intRegion();
        loadScene(true);
    }

    private void intRegion() {
        this.mBackButtonRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_PLAY.mKey);
        this.mLogoffRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_MENU.mKey);
        this.mReplayRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_REPLAY.mKey);
        this.mHelpPicRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_HELP.mKey);
        this.mMusicTextureRegion = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_MUSIC.mKey);
    }

    public void loadScene(boolean pIsNew) {
        float f;
        this.mRect1 = new Rectangle(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        this.mRect1.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue);
        this.mRect1.setAlpha(CommonConst.GrayColor.alpha);
        float f2 = (float) CommonConst.CAMERA_WIDTH;
        float aboutPicHeight = ((float) CommonConst.CAMERA_HEIGHT) / 4.0f;
        float aboutPY = ((float) CommonConst.CAMERA_HEIGHT) - aboutPicHeight;
        float BackPicWidth = ((float) this.mBackButtonRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float BackPicHeight = ((float) this.mBackButtonRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float f3 = (((float) CommonConst.CAMERA_WIDTH) - BackPicWidth) / 2.0f;
        float f4 = aboutPY - (BackPicHeight / 2.0f);
        float logoffPicWidth = ((float) this.mLogoffRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float logoffPicHeight = ((float) this.mLogoffRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float logoffPX = (((((float) CommonConst.CAMERA_WIDTH) / 2.0f) + Const.MOVE_LIMITED_SPEED) - logoffPicWidth) - (20.0f * CommonConst.RALE_SAMALL_VALUE);
        float logoffPY = aboutPY + ((aboutPicHeight - logoffPicHeight) / 2.0f);
        float replayWidth = ((float) this.mReplayRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float replayHeight = ((float) this.mReplayRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float replayPX = (logoffPX - replayWidth) - (20.0f * CommonConst.RALE_SAMALL_VALUE);
        float replayPY = logoffPY;
        float musicPicWidth = ((float) this.mMusicTextureRegion.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float musicPicHeight = ((float) this.mMusicTextureRegion.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float musicPX = logoffPX + logoffPicWidth + (20.0f * CommonConst.RALE_SAMALL_VALUE);
        float helpPicWidth = ((float) this.mHelpPicRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float helpPicHeight = ((float) this.mHelpPicRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float helpPX = musicPX + musicPicWidth + (20.0f * CommonConst.RALE_SAMALL_VALUE);
        float helpPY = logoffPY;
        float totalWidth = (20.0f * CommonConst.RALE_SAMALL_VALUE) + logoffPicWidth + replayWidth + (20.0f * CommonConst.RALE_SAMALL_VALUE) + musicPicWidth + (PortConst.enableHelp ? (20.0f * CommonConst.RALE_SAMALL_VALUE) + helpPicWidth : Const.MOVE_LIMITED_SPEED);
        float f5 = (20.0f * CommonConst.RALE_SAMALL_VALUE) + logoffPicHeight + replayHeight + (20.0f * CommonConst.RALE_SAMALL_VALUE) + musicPicHeight;
        if (PortConst.enableHelp) {
            f = (20.0f * CommonConst.RALE_SAMALL_VALUE) + helpPicHeight;
        } else {
            f = Const.MOVE_LIMITED_SPEED;
        }
        float f6 = f5 + f;
        float aboutPicWidth = (float) CommonConst.CAMERA_WIDTH;
        float aboutPicHeight2 = (((float) CommonConst.CAMERA_HEIGHT) * 0.85f) / 6.0f;
        float aboutPY2 = ((float) CommonConst.CAMERA_HEIGHT) - aboutPicHeight2;
        float BackPicWidth2 = ((float) this.mBackButtonRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float BackPicHeight2 = ((float) this.mBackButtonRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float backPX = (((float) CommonConst.CAMERA_WIDTH) - BackPicWidth2) / 2.0f;
        float backPY = aboutPY2 - (BackPicHeight2 / 2.0f);
        float startX = (((float) CommonConst.CAMERA_WIDTH) - totalWidth) / 2.0f;
        if (this.mEnableReplay) {
            replayWidth = ((float) this.mReplayRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
            replayHeight = ((float) this.mReplayRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
            replayPX = startX;
            replayPY = (((float) CommonConst.CAMERA_HEIGHT) - replayHeight) - (4.0f * CommonConst.RALE_SAMALL_VALUE);
            startX = startX + replayWidth + (20.0f * CommonConst.RALE_SAMALL_VALUE);
        }
        float logoffPicWidth2 = ((float) this.mLogoffRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float logoffPicHeight2 = ((float) this.mLogoffRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float logoffPX2 = startX;
        float logoffPY2 = (((float) CommonConst.CAMERA_HEIGHT) - logoffPicHeight2) - (4.0f * CommonConst.RALE_SAMALL_VALUE);
        float startX2 = startX + logoffPicWidth2 + (20.0f * CommonConst.RALE_SAMALL_VALUE);
        float musicPicWidth2 = ((float) this.mMusicTextureRegion.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE;
        float musicPicHeight2 = ((float) this.mMusicTextureRegion.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE;
        float musicPX2 = startX2;
        float musicPY = logoffPY2;
        float startX3 = startX2 + musicPicWidth2 + (20.0f * CommonConst.RALE_SAMALL_VALUE);
        if (PortConst.enableHelp && this.mEnableHelp) {
            helpPicWidth = ((float) this.mHelpPicRegion.getWidth()) * CommonConst.RALE_SAMALL_VALUE;
            helpPicHeight = ((float) this.mHelpPicRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE;
            helpPX = startX3;
            helpPY = logoffPY2;
            float f7 = (20.0f * CommonConst.RALE_SAMALL_VALUE) + startX3 + musicPicWidth2;
        }
        if (this.mRectBlank == null || this.mRectDarkGreen == null) {
            this.mRectBlank = new Rectangle(Const.MOVE_LIMITED_SPEED, aboutPY2, aboutPicWidth, aboutPicHeight2);
            this.mRectBlank.setColor(Const.RectBlankColor.red, Const.RectBlankColor.green, Const.RectBlankColor.blue);
            this.mRectDarkGreen = new Rectangle((3.0f * CommonConst.RALE_SAMALL_VALUE) + Const.MOVE_LIMITED_SPEED, (3.0f * CommonConst.RALE_SAMALL_VALUE) + aboutPY2, aboutPicWidth - (6.0f * CommonConst.RALE_SAMALL_VALUE), aboutPicHeight2 - (6.0f * CommonConst.RALE_SAMALL_VALUE));
            this.mRectDarkGreen.setColor(Const.RectDarkColor.red, Const.RectDarkColor.green, Const.RectDarkColor.blue);
        }
        if (this.mBackSprite == null) {
            this.mBackSprite = new Sprite(backPX, backPY, BackPicWidth2, BackPicHeight2, this.mBackButtonRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    ContextMenuScene.this.clearContextMenu(F2FGameActivity.Status.GAME);
                    return true;
                }
            };
            this.mBackSprite.setRotation(90.0f);
        }
        if (this.mLogoffSprite == null) {
            this.mLogoffSprite = new Sprite(logoffPX2, logoffPY2, logoffPicWidth2, logoffPicHeight2, this.mLogoffRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 0) {
                        ContextMenuScene.this.removePauseScene();
                        ContextMenuScene.this.mContext.getEngine().getScene().clearChildScene();
                        if (ContextMenuScene.this.mScene != null) {
                            Utils.setGoogleAnalytics(ContextMenuScene.this.mContext, 1, ContextMenuScene.this.mScene.mScore, ContextMenuScene.this.mScene.mMeter);
                            ContextMenuScene.this.mScene.backToUpMenu();
                        } else {
                            Utils.setGoogleAnalytics(ContextMenuScene.this.mContext, 1, ContextMenuScene.this.mContext.getmGameScene().mScore, ContextMenuScene.this.mContext.getmGameScene().mMeter);
                            ContextMenuScene.this.mContext.getmGameScene().backToUpMenu();
                        }
                    }
                    return true;
                }
            };
        }
        if (this.mReplaySprite == null && this.mEnableReplay) {
            this.mReplaySprite = new Sprite(replayPX, replayPY, replayWidth, replayHeight, this.mReplayRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    ContextMenuScene.this.clearContextMenu(F2FGameActivity.Status.GAME);
                    if (ContextMenuScene.this.mScene != null) {
                        Utils.setGoogleAnalytics(ContextMenuScene.this.mContext, 0, ContextMenuScene.this.mScene.mScore, ContextMenuScene.this.mScene.mMeter);
                        ContextMenuScene.this.mScene.rePlayGame();
                        return true;
                    }
                    Utils.setGoogleAnalytics(ContextMenuScene.this.mContext, 0, ContextMenuScene.this.mContext.getmGameScene().mScore, ContextMenuScene.this.mContext.getmGameScene().mMeter);
                    ContextMenuScene.this.mContext.getmGameScene().rePlayGame();
                    return true;
                }
            };
        }
        if (this.mMusicSprite == null) {
            this.mMusicSprite = new AnimatedSprite(musicPX2, musicPY, musicPicWidth2, musicPicHeight2, this.mMusicTextureRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() == 0 && ContextMenuScene.this.mContext.mResource != null) {
                        if (CommonConst.GAME_MUSIC_ON) {
                            setCurrentTileIndex(1);
                            CommonConst.GAME_MUSIC_ON = false;
                            ContextMenuScene.this.mContext.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
                        } else {
                            setCurrentTileIndex(0);
                            CommonConst.GAME_MUSIC_ON = true;
                            ContextMenuScene.this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
                        }
                    }
                    return true;
                }
            };
        }
        if (CommonConst.GAME_MUSIC_ON) {
            this.mMusicSprite.setCurrentTileIndex(0);
        } else {
            this.mMusicSprite.setCurrentTileIndex(1);
        }
        if (PortConst.enableHelp && this.mEnableHelp && this.mHelpPicSprite == null) {
            this.mHelpPicSprite = new Sprite(helpPX, helpPY, helpPicWidth, helpPicHeight, this.mHelpPicRegion) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    if (ContextMenuScene.this.mScene != null) {
                        ContextMenuScene.this.mScene.showHelpDialog();
                        return true;
                    }
                    ContextMenuScene.this.mContext.getmGameScene().showHelpDialog();
                    return true;
                }
            };
        }
        if (pIsNew) {
            this.mRect1.setPosition(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
            getTopLayer().addEntity(this.mRect1);
            this.mRectBlank.setPosition(Const.MOVE_LIMITED_SPEED, aboutPY2);
            this.mRectDarkGreen.setPosition((3.0f * CommonConst.RALE_SAMALL_VALUE) + Const.MOVE_LIMITED_SPEED, (3.0f * CommonConst.RALE_SAMALL_VALUE) + aboutPY2);
            getTopLayer().addEntity(this.mRectBlank);
            getTopLayer().addEntity(this.mRectDarkGreen);
            getTopLayer().addEntity(this.mBackSprite);
            registerTouchArea(this.mBackSprite);
            getTopLayer().addEntity(this.mLogoffSprite);
            registerTouchArea(this.mLogoffSprite);
            if (this.mEnableReplay) {
                registerTouchArea(this.mReplaySprite);
                getTopLayer().addEntity(this.mReplaySprite);
            }
            getTopLayer().addEntity(this.mMusicSprite);
            registerTouchArea(this.mMusicSprite);
            if (PortConst.enableHelp && this.mEnableHelp) {
                getTopLayer().addEntity(this.mHelpPicSprite);
                registerTouchArea(this.mHelpPicSprite);
            }
        } else {
            this.mRect1.setPosition(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED);
            this.mRectBlank.setPosition(Const.MOVE_LIMITED_SPEED, aboutPY2);
            this.mRectDarkGreen.setPosition((3.0f * CommonConst.RALE_SAMALL_VALUE) + Const.MOVE_LIMITED_SPEED, (3.0f * CommonConst.RALE_SAMALL_VALUE) + aboutPY2);
            this.mBackSprite.setPosition(backPX, backPY);
            this.mLogoffSprite.setPosition(logoffPX2, logoffPY2);
            if (this.mEnableReplay) {
                this.mReplaySprite.setPosition(replayPX, replayPY);
            }
            this.mMusicSprite.setPosition(musicPX2, musicPY);
            if (PortConst.enableHelp && this.mEnableHelp) {
                this.mHelpPicSprite.setPosition(helpPX, helpPY);
            }
        }
        setBackgroundEnabled(false);
    }

    /* access modifiers changed from: private */
    public void removePauseScene() {
        this.mMusicSprite.setPosition((float) (CommonConst.CAMERA_WIDTH + 30), Const.MOVE_LIMITED_SPEED);
        this.mLogoffSprite.setPosition((float) (CommonConst.CAMERA_WIDTH + 30), Const.MOVE_LIMITED_SPEED);
        this.mBackSprite.setPosition((float) (CommonConst.CAMERA_WIDTH + 30), Const.MOVE_LIMITED_SPEED);
        this.mRectBlank.setPosition((float) (CommonConst.CAMERA_WIDTH + 30), Const.MOVE_LIMITED_SPEED);
        this.mRectDarkGreen.setPosition(((float) (CommonConst.CAMERA_WIDTH + 30)) + (5.0f * CommonConst.RALE_SAMALL_VALUE), Const.MOVE_LIMITED_SPEED);
        if (PortConst.enableHelp && this.mEnableHelp) {
            this.mHelpPicSprite.setPosition((float) (CommonConst.CAMERA_WIDTH + 30), Const.MOVE_LIMITED_SPEED);
        }
        if (this.mEnableReplay) {
            this.mReplaySprite.setPosition((float) (CommonConst.CAMERA_WIDTH + 30), Const.MOVE_LIMITED_SPEED);
        }
    }

    public void clearContextMenu(F2FGameActivity.Status pStatus) {
        removePauseScene();
        this.mContext.getEngine().getScene().clearChildScene();
        if (this.mScene == null) {
            this.mContext.getmGameScene().settagetPosition();
            this.mContext.getmGameScene().setChildSceneClick(true);
            this.mContext.getmGameScene().enableHud();
        }
    }
}
