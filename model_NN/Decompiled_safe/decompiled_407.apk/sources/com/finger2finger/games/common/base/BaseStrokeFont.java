package com.finger2finger.games.common.base;

import android.graphics.Typeface;
import org.anddev.andengine.opengl.font.StrokeFont;
import org.anddev.andengine.opengl.texture.Texture;

public class BaseStrokeFont extends StrokeFont {
    public BaseStrokeFont(Texture texture, Typeface typeface, float size, boolean antiAlias, int color, float strokeWidth, int strokeColor, float praleValue) {
        super(texture, typeface, size * praleValue, antiAlias, color, strokeWidth, strokeColor);
    }

    public BaseStrokeFont(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, float pStrokeWidth, int pStrokeColor, boolean pStrokeOnly, float praleValue) {
        super(pTexture, pTypeface, pSize * praleValue, pAntiAlias, pColor, pStrokeWidth, pStrokeColor, pStrokeOnly);
    }
}
