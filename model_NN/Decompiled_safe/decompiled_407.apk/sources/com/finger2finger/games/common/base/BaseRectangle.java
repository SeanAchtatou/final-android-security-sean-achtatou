package com.finger2finger.games.common.base;

import org.anddev.andengine.entity.primitive.Rectangle;

public class BaseRectangle extends Rectangle {
    public BaseRectangle(float px, float py, float width, float height, float praleValue) {
        super(px, py, width * praleValue, height * praleValue);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BaseRectangle(float r5, float r6, float r7, float r8, float r9, boolean r10) {
        /*
            r4 = this;
            r3 = 1
            if (r10 != r3) goto L_0x0015
            float r0 = r5 * r9
        L_0x0005:
            if (r10 != r3) goto L_0x0017
            float r1 = r6 * r9
        L_0x0009:
            if (r10 != r3) goto L_0x0019
            float r2 = r7 * r9
        L_0x000d:
            if (r10 != r3) goto L_0x001b
            float r3 = r8 * r9
        L_0x0011:
            r4.<init>(r0, r1, r2, r3)
            return
        L_0x0015:
            r0 = r5
            goto L_0x0005
        L_0x0017:
            r1 = r6
            goto L_0x0009
        L_0x0019:
            r2 = r7
            goto L_0x000d
        L_0x001b:
            r3 = r8
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.base.BaseRectangle.<init>(float, float, float, float, float, boolean):void");
    }
}
