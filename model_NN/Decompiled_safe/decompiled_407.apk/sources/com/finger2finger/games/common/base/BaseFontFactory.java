package com.finger2finger.games.common.base;

import android.content.Context;
import android.graphics.Typeface;
import org.anddev.andengine.opengl.texture.Texture;

public class BaseFontFactory {
    private static String sAssetBasePath = "";

    public static void setAssetBasePath(String pAssetBasePath) {
        if (pAssetBasePath.endsWith("/") || pAssetBasePath.length() == 0) {
            sAssetBasePath = pAssetBasePath;
            return;
        }
        throw new IllegalStateException("pAssetBasePath must end with '/' or be lenght zero.");
    }

    public static BaseFont create(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, float praleValue) {
        return new BaseFont(pTexture, pTypeface, pSize, pAntiAlias, pColor, praleValue);
    }

    public static BaseStrokeFont createStroke(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor, float praleValue) {
        return new BaseStrokeFont(pTexture, pTypeface, pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor, praleValue);
    }

    public static BaseStrokeFont createStroke(Texture pTexture, Typeface pTypeface, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor, boolean pStrokeOnly, float praleValue) {
        return new BaseStrokeFont(pTexture, pTypeface, pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor, pStrokeOnly, praleValue);
    }

    public static BaseFont createFromAsset(Texture pTexture, Context pContext, String pAssetPath, float pSize, boolean pAntiAlias, int pColor, float praleValue) {
        return new BaseFont(pTexture, Typeface.createFromAsset(pContext.getAssets(), sAssetBasePath + pAssetPath), pSize, pAntiAlias, pColor, praleValue);
    }

    public static BaseStrokeFont createStrokeFromAsset(Texture pTexture, Context pContext, String pAssetPath, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor, float praleValue) {
        return new BaseStrokeFont(pTexture, Typeface.createFromAsset(pContext.getAssets(), sAssetBasePath + pAssetPath), pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor, praleValue);
    }

    public static BaseStrokeFont createStrokeFromAsset(Texture pTexture, Context pContext, String pAssetPath, float pSize, boolean pAntiAlias, int pColor, int pStrokeWidth, int pStrokeColor, boolean pStrokeOnly, float praleValue) {
        return new BaseStrokeFont(pTexture, Typeface.createFromAsset(pContext.getAssets(), sAssetBasePath + pAssetPath), pSize, pAntiAlias, pColor, (float) pStrokeWidth, pStrokeColor, pStrokeOnly, praleValue);
    }
}
