package com.finger2finger.games.common.scene.dialog;

import com.finger2finger.games.res.Const;

public class DynamicDialog {
    private boolean isDisplayed = false;
    private float mHeight = Const.MOVE_LIMITED_SPEED;
    private int mType;
    private float mWidth = Const.MOVE_LIMITED_SPEED;
    private float mX = Const.MOVE_LIMITED_SPEED;
    private float mY = Const.MOVE_LIMITED_SPEED;

    public boolean isDisplayed() {
        return this.isDisplayed;
    }

    public void setDisplayed(boolean isDisplayed2) {
        this.isDisplayed = isDisplayed2;
    }

    public int getmType() {
        return this.mType;
    }

    public void setmType(int mType2) {
        this.mType = mType2;
    }

    public float getmX() {
        return this.mX;
    }

    public float getmY() {
        return this.mY;
    }

    public float getmWidth() {
        return this.mWidth;
    }

    public float getmHeight() {
        return this.mHeight;
    }

    public void setmX(float mX2) {
        this.mX = mX2;
    }

    public void setmY(float mY2) {
        this.mY = mY2;
    }

    public void setmWidth(float mWidth2) {
        this.mWidth = mWidth2;
    }

    public void setmHeight(float mHeight2) {
        this.mHeight = mHeight2;
    }

    public DynamicDialog(int pType, float pX, float pY, float pWidth, float pHeight) {
        this.mX = pX;
        this.mY = pY;
        this.mWidth = pWidth;
        this.mHeight = pHeight;
        this.mType = pType;
    }
}
