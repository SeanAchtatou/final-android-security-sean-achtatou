package com.finger2finger.games.common;

import android.content.Context;
import android.util.Log;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.store.data.GameInformation;
import com.finger2finger.games.common.store.data.LevelEntity;
import com.finger2finger.games.common.store.data.StoreGameInfo;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.common.store.io.Utils;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import com.finger2finger.games.res.VersionControl;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class GameInfo {
    private StoreGameInfo mGameInfo = null;
    private int mInsideIndex = 0;
    private int mLevelIndex = 0;
    private int mSubLevelIndex = 0;
    private int maxSore = 0;
    private int score = 0;

    public void initialize(Context mContext) {
        CommonConst.IS_ADD_EVERYDAY_GOLDEN_MONEY = false;
        CommonConst.IS_FIRST_PLAY_GAME = false;
        boolean createFile = false;
        if (!PortConst.enableSDCard || CommonPortConst.isNoSDCard) {
            this.mGameInfo = new StoreGameInfo();
            String stream = this.mGameInfo.checkPreferences(mContext);
            if (stream == null || stream.equals("")) {
                createFile = true;
            }
        } else {
            try {
                Utils.createDir(TablePath.ROOT_PATH);
                Utils.createDir(TablePath.GAME_PATH);
                Utils.createDir(TablePath.PERSONAL_PATH);
                createFile = Utils.createFile(TablePath.INFO_GAME_PATH);
            } catch (Exception e) {
                Log.e("f2f_GameInfo_createDir_error", e.toString());
                CommonPortConst.isNoSDCard = true;
                this.mGameInfo = new StoreGameInfo();
                String stream2 = this.mGameInfo.checkPreferences(mContext);
                if (stream2 == null || stream2.equals("")) {
                    createFile = true;
                }
            }
        }
        if (createFile) {
            try {
                rectGameInfo(mContext);
            } catch (Exception e2) {
                Log.e("f2f_GameInfo_setGameInfo_error", e2.toString());
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAMEINFO_REPAIR);
            }
        } else {
            this.mGameInfo = new StoreGameInfo();
            if (!VersionControl.upgrade(this.mGameInfo.checkVersionPreferences(mContext), Const.Version)) {
                Log.e("f2f_version_check_error", "version_check_fail");
                CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAMEINFO_REPAIR);
            }
            try {
                this.mGameInfo.load(mContext);
            } catch (Exception e3) {
                Exception e4 = e3;
                if (!PortConst.enableSDCard) {
                    try {
                        rectGameInfo(mContext);
                        return;
                    } catch (Exception e5) {
                        Log.e("f2f_GameInfo_StoreGameInfo_rectGameInfo_error", e4.toString());
                        CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAMEINFO_REPAIR);
                        return;
                    }
                } else {
                    Log.e("f2f_GameInfo_StoreGameInfo_load_error", e4.toString());
                    CommonConst.errorList.add(CommonConst.ERROR_LEVEL.GAMEINFO_REPAIR);
                }
            }
            if (CommonConst.errorList.size() == 0 && CommonConst.errorLevel != CommonConst.ERROR_LEVEL.UNREPAIR) {
                String nowDay = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
                if (Integer.parseInt(nowDay) > Integer.parseInt(this.mGameInfo.getMGameInfo().get_loadGameDay())) {
                    CommonConst.GAME_LOAD_DAY = nowDay;
                    CommonConst.INVITE_FRIEND_NUM = 0;
                    this.mGameInfo.getMGameInfo().set_InviteFriendNum(CommonConst.INVITE_FRIEND_NUM);
                    this.mGameInfo.getMGameInfo().set_loadGameDay(CommonConst.GAME_LOAD_DAY);
                    CommonConst.ENBALE_SHOW_INVITE_DIALOG = true;
                    CommonConst.ENABLE_INVITE_FRIEND = true;
                    this.mGameInfo.getMGameInfo().set_enableInviteDilaog(0);
                    this.mGameInfo.getMGameInfo().set_enableInviteFriend(0);
                    try {
                        this.mGameInfo.write(mContext);
                    } catch (Exception e6) {
                        Log.e("f2f_StoreGameInfo_write_error", e6.toString());
                        CommonConst.errorLevel = CommonConst.ERROR_LEVEL.UNREPAIR;
                    }
                }
            }
            try {
                addLevelInfo(mContext);
            } catch (Exception e7) {
                Log.e("f2f_StoreGameInfo_addLevelInfo_error", e7.toString());
                CommonConst.errorLevel = CommonConst.ERROR_LEVEL.GAMEINFO_REPAIR;
            }
        }
    }

    public void rectGameInfo(Context mContext) throws Exception {
        int i;
        int i2;
        int i3;
        int i4;
        boolean isEnable;
        this.mGameInfo = new StoreGameInfo();
        if (CommonConst.GAME_MUSIC_ON) {
            i = 0;
        } else {
            i = 1;
        }
        String str = CommonConst.GAME_VERSION;
        int i5 = CommonConst.GAME_GOLD;
        int i6 = CommonConst.GAME_EXCHANGE;
        if (CommonConst.GAME_HELP_SHOW) {
            i2 = 0;
        } else {
            i2 = 1;
        }
        int i7 = CommonConst.GAME_MAX_SCORE;
        long j = CommonConst.DIALY_CLEAR_ADS_DEFAUT;
        long j2 = CommonConst.FOREVER_CLEAR_ADS_DEFAUT;
        int i8 = CommonConst.INVITE_FRIEND_NUM;
        String str2 = CommonConst.GAME_LOAD_DAY;
        if (CommonConst.ENBALE_SHOW_INVITE_DIALOG) {
            i3 = 0;
        } else {
            i3 = 1;
        }
        if (CommonConst.ENABLE_INVITE_FRIEND) {
            i4 = 0;
        } else {
            i4 = 1;
        }
        GameInformation gameInfo = new GameInformation(i, str, i5, i6, i2, i7, j, j2, i8, str2, i3, i4);
        int subLevelCount = PortConst.LevelInfo.length;
        int i9 = PortConst.LevelInfo[Const.GAME_MODEID][0];
        for (int i10 = 0; i10 < subLevelCount; i10++) {
            int insideLevelCount = PortConst.LevelInfo[i10][0];
            for (int j3 = 0; j3 < insideLevelCount; j3++) {
                if (j3 == 0) {
                    isEnable = true;
                } else {
                    isEnable = false;
                }
                this.mGameInfo.getMLevelEntity().add(new LevelEntity(i10, 0, j3, 0, isEnable, 0));
            }
        }
        this.mGameInfo.setMGameInfo(gameInfo);
        this.mGameInfo.write(mContext);
    }

    private void addLevelInfo(Context mContext) throws Exception {
        boolean isEnable;
        int levelCount = PortConst.LevelInfo.length;
        int oldLevelCount = geOldtLevelCount();
        for (int i = 0; i < oldLevelCount; i++) {
            if (i < PortConst.LevelInfo.length) {
                int insideLevelCount = PortConst.LevelInfo[i][0];
                int oldLevelValue = getOldLevelInfoByInsideLevel(i);
                if (insideLevelCount - oldLevelValue > 0) {
                    int countNum = insideLevelCount - oldLevelValue;
                    for (int j = 0; j < countNum; j++) {
                        this.mGameInfo.getMLevelEntity().add(new LevelEntity(i, 0, j + oldLevelValue, 0, false, 0));
                    }
                }
            }
        }
        if (levelCount - oldLevelCount > 0) {
            for (int i2 = oldLevelCount; i2 < levelCount; i2++) {
                int insideLevelCount2 = PortConst.LevelInfo[i2][0];
                for (int j2 = 0; j2 < insideLevelCount2; j2++) {
                    if (j2 == 0) {
                        isEnable = true;
                    } else {
                        isEnable = false;
                    }
                    this.mGameInfo.getMLevelEntity().add(new LevelEntity(i2, 0, j2, 0, isEnable, 0));
                }
            }
        }
        this.mGameInfo.write(mContext);
    }

    private int geOldtLevelCount() {
        ArrayList<LevelEntity> levelEntity = this.mGameInfo.getMLevelEntity();
        Map<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < levelEntity.size(); i++) {
            if (hashMap.get(Integer.valueOf(levelEntity.get(i).getLevelIndex())) == null) {
                hashMap.put(Integer.valueOf(levelEntity.get(i).getLevelIndex()), 1);
            } else {
                hashMap.put(Integer.valueOf(levelEntity.get(i).getLevelIndex()), Integer.valueOf(((Integer) hashMap.get(Integer.valueOf(levelEntity.get(i).getLevelIndex()))).intValue() + 1));
            }
        }
        return hashMap.size();
    }

    private int getOldLevelInfoByInsideLevel(int subLevelCount) {
        int retValue = 0;
        ArrayList<LevelEntity> levelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < levelEntity.size(); i++) {
            if (levelEntity.get(i).getLevelIndex() == subLevelCount) {
                retValue++;
            }
        }
        return retValue;
    }

    public boolean updateScoreByIndex(int levelIndex, int subLevelIndex, int index, int maxScore) {
        ArrayList<LevelEntity> mLevelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < mLevelEntity.size(); i++) {
            LevelEntity levelInfo = mLevelEntity.get(i);
            if (levelInfo.getIndex() == index && levelInfo.getLevelIndex() == levelIndex && levelInfo.getSubLevelIndex() == subLevelIndex && levelInfo.getLevelScoreMax() < maxScore) {
                levelInfo.setLevelScoreMax(maxScore);
                return true;
            }
        }
        return false;
    }

    public int getStarByIndex(int levelIndex, int subLevelIndex, int index) {
        ArrayList<LevelEntity> mLevelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < mLevelEntity.size(); i++) {
            LevelEntity levelInfo = mLevelEntity.get(i);
            if (levelInfo.getIndex() == index && levelInfo.getLevelIndex() == levelIndex && levelInfo.getSubLevelIndex() == subLevelIndex) {
                return levelInfo.getStar();
            }
        }
        return 0;
    }

    public boolean updateStarByIndex(int levelIndex, int subLevelIndex, int index, int star) {
        ArrayList<LevelEntity> mLevelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < mLevelEntity.size(); i++) {
            LevelEntity levelInfo = mLevelEntity.get(i);
            if (levelInfo.getIndex() == index && levelInfo.getLevelIndex() == levelIndex && levelInfo.getSubLevelIndex() == subLevelIndex && levelInfo.getStar() < star) {
                levelInfo.setStar(star);
                return true;
            }
        }
        return false;
    }

    public int getStarClassification(int[] pScoreLevel, int pScore) {
        if (pScoreLevel == null || pScore <= 0) {
            return 0;
        }
        for (int i = 0; i < pScoreLevel.length; i++) {
            if (pScore < pScoreLevel[i]) {
                return i;
            }
        }
        return pScoreLevel.length;
    }

    public int getStarClassification4Time(int[] pTimeLevel, int pTime) {
        if (pTimeLevel == null) {
            return 0;
        }
        int length = pTimeLevel.length;
        for (int i = 0; i < length; i++) {
            if (pTime <= pTimeLevel[i]) {
                return length - i;
            }
        }
        return 0;
    }

    public int getMaxLevelScoreByIndex(int levelIndex, int subLevelIndex, int index) {
        ArrayList<LevelEntity> mLevelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < mLevelEntity.size(); i++) {
            LevelEntity levelInfo = mLevelEntity.get(i);
            if (levelInfo.getIndex() == index && levelInfo.getLevelIndex() == levelIndex && levelInfo.getSubLevelIndex() == subLevelIndex) {
                return levelInfo.getLevelScoreMax();
            }
        }
        return 0;
    }

    public void updateEnableByIndex(int levelIndex, int subLevelIndex, int index, boolean pIsEnabled) {
        ArrayList<LevelEntity> mLevelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < mLevelEntity.size(); i++) {
            LevelEntity levelInfo = mLevelEntity.get(i);
            if (levelInfo.getIndex() == index && levelInfo.getLevelIndex() == levelIndex && levelInfo.getSubLevelIndex() == subLevelIndex) {
                levelInfo.setIsEnable(pIsEnabled);
                return;
            }
        }
    }

    public void saveGameInfo(Context mContext) {
        int i;
        int i2;
        int i3;
        int i4;
        try {
            GameInformation gameInfo = this.mGameInfo.getMGameInfo();
            if (CommonConst.GAME_MUSIC_ON) {
                i = 0;
            } else {
                i = 1;
            }
            gameInfo.set_gameMusicOn(i);
            gameInfo.set_gameVersion(CommonConst.GAME_VERSION);
            gameInfo.set_gameGold(CommonConst.GAME_GOLD);
            gameInfo.set_gameExchangeRatio(CommonConst.GAME_EXCHANGE);
            if (CommonConst.GAME_HELP_SHOW) {
                i2 = 0;
            } else {
                i2 = 1;
            }
            gameInfo.set_gameHelpShow(i2);
            gameInfo.set_maxSore(CommonConst.GAME_MAX_SCORE);
            gameInfo.set_loadGameDay(CommonConst.GAME_LOAD_DAY);
            gameInfo.set_InviteFriendNum(CommonConst.INVITE_FRIEND_NUM);
            if (CommonConst.ENBALE_SHOW_INVITE_DIALOG) {
                i3 = 0;
            } else {
                i3 = 1;
            }
            gameInfo.set_enableInviteDilaog(i3);
            if (CommonConst.ENABLE_INVITE_FRIEND) {
                i4 = 0;
            } else {
                i4 = 1;
            }
            gameInfo.set_enableInviteFriend(i4);
            this.mGameInfo.write(mContext);
        } catch (Exception e) {
            Log.d("F2FError", e.toString());
        }
    }

    public ArrayList<LevelEntity> getLevelInfoByModeID(int pModeID) {
        ArrayList<LevelEntity> levelArrayList = new ArrayList<>();
        ArrayList<LevelEntity> mLevelEntity = this.mGameInfo.getMLevelEntity();
        for (int i = 0; i < mLevelEntity.size(); i++) {
            LevelEntity levelInfo = mLevelEntity.get(i);
            if (levelInfo.getLevelIndex() == pModeID) {
                levelArrayList.add(levelInfo);
            }
        }
        return levelArrayList;
    }

    public StoreGameInfo getMGameInfo() {
        return this.mGameInfo;
    }

    public void setMGameInfo(StoreGameInfo gameInfo) {
        this.mGameInfo = gameInfo;
    }

    public void loadGameInfo(Context mContext) {
        GameInformation gameInfo;
        boolean z;
        boolean z2;
        if (this.mGameInfo != null && (gameInfo = this.mGameInfo.getMGameInfo()) != null) {
            CommonConst.GAME_MUSIC_ON = gameInfo.get_gameMusicOn() == 0;
            CommonConst.GAME_VERSION = gameInfo.get_gameVersion();
            CommonConst.GAME_GOLD = gameInfo.get_gameGold();
            CommonConst.GAME_EXCHANGE = gameInfo.get_gameExchangeRatio();
            if (gameInfo.get_gameHelpShow() == 0) {
                z = true;
            } else {
                z = false;
            }
            CommonConst.GAME_HELP_SHOW = z;
            CommonConst.GAME_MAX_SCORE = gameInfo.get_maxSore();
            CommonConst.FOREVER_CLEAR_ADS_DEFAUT = gameInfo.get_foreverClearAds();
            CommonConst.DIALY_CLEAR_ADS_DEFAUT = gameInfo.get_dialyClearAds();
            CommonConst.GAME_LOAD_DAY = gameInfo.get_loadGameDay();
            CommonConst.INVITE_FRIEND_NUM = gameInfo.get_InviteFriendNum();
            CommonConst.ENBALE_SHOW_INVITE_DIALOG = gameInfo.get_enableInviteDilaog() == 0;
            if (gameInfo.get_enableInviteFriend() == 0) {
                z2 = true;
            } else {
                z2 = false;
            }
            CommonConst.ENABLE_INVITE_FRIEND = z2;
        }
    }

    public boolean inviteFriend(Context pContext) {
        boolean retValue = false;
        String nowDay = String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())));
        String oldDay = this.mGameInfo.getMGameInfo().get_loadGameDay();
        int friendNum = this.mGameInfo.getMGameInfo().get_InviteFriendNum();
        if (Integer.parseInt(nowDay) == Integer.parseInt(oldDay)) {
            if (friendNum < CommonConst.INVITE_FRIEND_MAX_NAM - 1) {
                CommonConst.INVITE_FRIEND_NUM++;
                this.mGameInfo.getMGameInfo().set_InviteFriendNum(CommonConst.INVITE_FRIEND_NUM);
                retValue = true;
            } else {
                retValue = false;
            }
        } else if (Integer.parseInt(nowDay) >= Integer.parseInt(oldDay)) {
            CommonConst.INVITE_FRIEND_NUM = 1;
            this.mGameInfo.getMGameInfo().set_InviteFriendNum(1);
            CommonConst.GAME_LOAD_DAY = nowDay;
            this.mGameInfo.getMGameInfo().set_loadGameDay(nowDay);
            retValue = true;
        }
        if (retValue) {
            CommonConst.ENABLE_INVITE_FRIEND = true;
        } else {
            CommonConst.ENABLE_INVITE_FRIEND = false;
        }
        try {
            saveGameInfo(pContext);
        } catch (Exception e) {
            Log.e("f2f_GameInfo_setGameInfo_inviteFriend", e.toString());
        }
        return retValue;
    }

    public int getMLevelIndex() {
        return this.mLevelIndex;
    }

    public void setMLevelIndex(int levelIndex) {
        this.mLevelIndex = levelIndex;
    }

    public int getMSubLevelIndex() {
        return this.mSubLevelIndex;
    }

    public void setMSubLevelIndex(int subLevelIndex) {
        this.mSubLevelIndex = subLevelIndex;
    }

    public int getMInsideIndex() {
        return this.mInsideIndex;
    }

    public void setMInsideIndex(int insideIndex) {
        this.mInsideIndex = insideIndex;
    }

    public int getMaxSore() {
        return this.maxSore;
    }

    public void setMaxSore(int maxSore2) {
        this.maxSore = maxSore2;
    }

    public int getScore() {
        return this.score;
    }

    public void setScore(int score2) {
        this.score = score2;
    }
}
