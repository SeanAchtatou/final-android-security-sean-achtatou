package com.finger2finger.games.common.res;

import android.util.Log;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.DownLoadFile;
import com.finger2finger.games.common.GoogleAnalyticsUtils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BasePromationEntity;
import com.finger2finger.games.common.store.data.TablePath;
import com.finger2finger.games.common.store.io.Utils;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.PortConst;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import org.anddev.andengine.level.LevelLoader;
import org.anddev.andengine.util.SAXUtils;
import org.anddev.andengine.util.constants.TimeConstants;
import org.xml.sax.Attributes;

public class PromotionHandler {
    public static final String PROPERTY_FILE_PATH = "CommonResource/promotiongame/promotion.xml";
    public static final String TAG_PROMOTION = "promotion";
    public static final String TAG_PROMOTION_APKURL0 = "apkurl0";
    public static final String TAG_PROMOTION_APKURL1 = "apkurl1";
    public static final String TAG_PROMOTION_APKURL2 = "apkurl2";
    public static final String TAG_PROMOTION_COUNTRY = "country";
    public static final String TAG_PROMOTION_DURATION = "duration";
    public static final String TAG_PROMOTION_ENTITY = "promotioninfo";
    public static final String TAG_PROMOTION_GAMENAME = "gamename";
    public static final String TAG_PROMOTION_PACKAGE = "package";
    public static final String TAG_PROMOTION_PORT_PROMOTIONBG = "promotionportbg";
    public static final String TAG_PROMOTION_PROMATIONACTION = "promationaction";
    public static final String TAG_PROMOTION_PROMOTIONACTIONPIC = "promotionactionpic";
    public static final String TAG_PROMOTION_PROMOTIONACTIONPICSIZE = "promotionactionpicsize";
    public static final String TAG_PROMOTION_PROMOTIONBG = "promotionbg";
    public static final String TAG_PROMOTION_PROMOTIONBGSIZE = "promotionbgsize";
    public static final String TAG_PROMOTION_PROMOTIONPORTBGSIZE = "promotionportbgsize";
    public static final String TAG_PROMOTION_PROMTIONACTIONPICPOSION = "promtionactionpicposion";
    public static final String TAG_PROMOTION_PROMTIONACTIONPORTPICPOSION = "promtionactionportpicposion";
    public static final String TAG_PROMOTION_PROMTIONICON = "promotionicon";
    public static final String TAG_PROMOTION_PROMTIONICON_SIZE = "promotioniconcsize";
    public static final String TAG_PROMOTION_UPTATETIME = "updatetime";
    public boolean enableCopy = false;
    public boolean isSDStage = false;
    public ArrayList<BasePromationEntity> promationInfo = new ArrayList<>();

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0063 A[SYNTHETIC, Splitter:B:27:0x0063] */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e A[SYNTHETIC, Splitter:B:8:0x002e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadInfo(com.finger2finger.games.common.activity.F2FGameActivity r9, java.lang.String r10) throws java.io.IOException {
        /*
            r8 = this;
            org.anddev.andengine.level.LevelLoader r4 = new org.anddev.andengine.level.LevelLoader
            r4.<init>()
            r2 = 0
            java.lang.String r5 = "promotion"
            com.finger2finger.games.common.res.PromotionHandler$1 r6 = new com.finger2finger.games.common.res.PromotionHandler$1
            r6.<init>()
            r4.registerEntityLoader(r5, r6)
            java.lang.String r5 = "promotioninfo"
            com.finger2finger.games.common.res.PromotionHandler$2 r6 = new com.finger2finger.games.common.res.PromotionHandler$2
            r6.<init>()
            r4.registerEntityLoader(r5, r6)
            if (r10 == 0) goto L_0x0024
            java.lang.String r5 = ""
            boolean r5 = r10.equals(r5)     // Catch:{ IOException -> 0x0054 }
            if (r5 == 0) goto L_0x0033
        L_0x0024:
            r5 = 0
            r8.isSDStage = r5     // Catch:{ IOException -> 0x0054 }
            java.lang.String r5 = "CommonResource/promotiongame/promotion.xml"
            r4.loadLevelFromAsset(r9, r5)     // Catch:{ IOException -> 0x0054 }
        L_0x002c:
            if (r2 == 0) goto L_0x0032
            r2.close()     // Catch:{ IOException -> 0x0049 }
            r2 = 0
        L_0x0032:
            return
        L_0x0033:
            r5 = 1
            r8.isSDStage = r5     // Catch:{ IOException -> 0x0054 }
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0054 }
            java.io.File r5 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x0054 }
            r1.<init>(r5, r10)     // Catch:{ IOException -> 0x0054 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0054 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0054 }
            r4.loadLevelFromStream(r3)     // Catch:{ IOException -> 0x0076, all -> 0x0073 }
            r2 = r3
            goto L_0x002c
        L_0x0049:
            r0 = move-exception
            java.lang.String r5 = "PromotionHandler_loadInfo_instream_error"
            java.lang.String r6 = r0.toString()
            android.util.Log.e(r5, r6)
            goto L_0x0032
        L_0x0054:
            r5 = move-exception
            r0 = r5
        L_0x0056:
            java.lang.String r5 = "PromotionHandler_loadInfo_error"
            java.lang.String r6 = r0.toString()     // Catch:{ all -> 0x0060 }
            android.util.Log.e(r5, r6)     // Catch:{ all -> 0x0060 }
            throw r0     // Catch:{ all -> 0x0060 }
        L_0x0060:
            r5 = move-exception
        L_0x0061:
            if (r2 == 0) goto L_0x0067
            r2.close()     // Catch:{ IOException -> 0x0068 }
            r2 = 0
        L_0x0067:
            throw r5
        L_0x0068:
            r0 = move-exception
            java.lang.String r6 = "PromotionHandler_loadInfo_instream_error"
            java.lang.String r7 = r0.toString()
            android.util.Log.e(r6, r7)
            goto L_0x0067
        L_0x0073:
            r5 = move-exception
            r2 = r3
            goto L_0x0061
        L_0x0076:
            r5 = move-exception
            r0 = r5
            r2 = r3
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.res.PromotionHandler.loadInfo(com.finger2finger.games.common.activity.F2FGameActivity, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadDownPicInfo(com.finger2finger.games.common.activity.F2FGameActivity r8, java.lang.String r9, final boolean r10, final java.lang.String r11) throws java.io.IOException {
        /*
            r7 = this;
            org.anddev.andengine.level.LevelLoader r4 = new org.anddev.andengine.level.LevelLoader
            r4.<init>()
            r2 = 0
            java.lang.String r5 = "promotion"
            com.finger2finger.games.common.res.PromotionHandler$3 r6 = new com.finger2finger.games.common.res.PromotionHandler$3
            r6.<init>()
            r4.registerEntityLoader(r5, r6)
            java.lang.String r5 = "promotioninfo"
            com.finger2finger.games.common.res.PromotionHandler$4 r6 = new com.finger2finger.games.common.res.PromotionHandler$4
            r6.<init>(r10, r11)
            r4.registerEntityLoader(r5, r6)
            java.io.File r1 = new java.io.File     // Catch:{ IOException -> 0x0032 }
            java.io.File r5 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ IOException -> 0x0032 }
            r1.<init>(r5, r9)     // Catch:{ IOException -> 0x0032 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0032 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0032 }
            r4.loadLevelFromStream(r3)     // Catch:{ IOException -> 0x0049, all -> 0x0046 }
            if (r3 == 0) goto L_0x004d
            r3.close()
            r2 = 0
        L_0x0031:
            return
        L_0x0032:
            r5 = move-exception
            r0 = r5
        L_0x0034:
            java.lang.String r5 = "PromotionHandler_loadInfo_error"
            java.lang.String r6 = r0.toString()     // Catch:{ all -> 0x003e }
            android.util.Log.e(r5, r6)     // Catch:{ all -> 0x003e }
            throw r0     // Catch:{ all -> 0x003e }
        L_0x003e:
            r5 = move-exception
        L_0x003f:
            if (r2 == 0) goto L_0x0045
            r2.close()
            r2 = 0
        L_0x0045:
            throw r5
        L_0x0046:
            r5 = move-exception
            r2 = r3
            goto L_0x003f
        L_0x0049:
            r5 = move-exception
            r0 = r5
            r2 = r3
            goto L_0x0034
        L_0x004d:
            r2 = r3
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.res.PromotionHandler.loadDownPicInfo(com.finger2finger.games.common.activity.F2FGameActivity, java.lang.String, boolean, java.lang.String):void");
    }

    public void loadWebXmlInfo(final F2FGameActivity mContext, String pUrlPath, final long pUpdateTime) throws Exception {
        LevelLoader levelLoader = new LevelLoader();
        InputStream instream = null;
        levelLoader.registerEntityLoader(TAG_PROMOTION, new LevelLoader.IEntityLoader() {
            public void onLoadEntity(String pEntityName, Attributes pAttributes) {
            }
        });
        levelLoader.registerEntityLoader(TAG_PROMOTION_ENTITY, new LevelLoader.IEntityLoader() {
            public void onLoadEntity(String pEntityName, Attributes pAttributes) {
                String attributeOrThrow = SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_GAMENAME);
                String country = SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_COUNTRY);
                if (pUpdateTime != Long.parseLong(SAXUtils.getAttributeOrThrow(pAttributes, PromotionHandler.TAG_PROMOTION_UPTATETIME))) {
                    try {
                        if (DownLoadFile.downLoadFile(Const.PROMOTION_URL + country + TablePath.SEPARATOR_PATH + Const.PROMOTION_XML_NAME, TablePath.T_PROMOTION_TEMP_PATH + Const.PROMOTION_XML_NAME)) {
                            PromotionHandler.this.loadDownPicInfo(mContext, TablePath.T_PROMOTION_TEMP_PATH + Const.PROMOTION_XML_NAME, true, country);
                        }
                    } catch (Exception e) {
                        Log.e("PromotionHandler_loadWebXmlInfo_downLoadFile_error", e.toString());
                    }
                }
            }
        });
        try {
            instream = getWebStream(pUrlPath);
            levelLoader.loadLevelFromStream(instream);
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e) {
                    Log.e("PromotionHandler_loadInfo_instream_error", e.toString());
                }
            }
        } catch (IOException e2) {
            IOException e3 = e2;
            Log.e("PromotionHandler_loadInfo_error", e3.toString());
            throw e3;
        } catch (Throwable th) {
            if (instream != null) {
                try {
                    instream.close();
                } catch (IOException e4) {
                    Log.e("PromotionHandler_loadInfo_instream_error", e4.toString());
                }
            }
            throw th;
        }
    }

    /* Debug info: failed to restart local var, previous not found, register: 9 */
    public void showPromtion(F2FGameActivity pContext) throws Exception {
        String pBgPath;
        if (PortConst.enablePromotion) {
            String promtionXmlPath = TablePath.T_PROMOTION_USE_PATH + Const.PROMOTION_XML_NAME;
            boolean fileExist = Utils.isFileExist(promtionXmlPath);
            if (!Const.enableSDCard || !fileExist) {
                loadAssetXml(pContext);
            } else {
                loadSDXml(pContext, promtionXmlPath);
            }
            checkWebInfo(pContext, this.promationInfo.get(0).updatetime);
            if (this.promationInfo == null || this.promationInfo.size() <= 0) {
                PortConst.enablePromotion = false;
                PortConst.showPromtionIcon = false;
            } else if (!com.finger2finger.games.common.Utils.checkGameInstall(pContext, this.promationInfo.get(0).packageName)) {
                String pBgPath2 = this.promationInfo.get(0).promotionPortBg;
                String pActionPath = Const.PROMOTION_FILE_PATH + this.promationInfo.get(0).promotionactionpic;
                String iconPath = Const.PROMOTION_FILE_PATH + this.promationInfo.get(0).promotionIcon;
                if (this.isSDStage) {
                    pActionPath = TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(0).promotionactionpic;
                    pBgPath = TablePath.T_PROMOTION_USE_PATH + pBgPath2;
                    iconPath = TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(0).promotionIcon;
                } else {
                    pBgPath = Const.PROMOTION_FILE_PATH + pBgPath2;
                }
                pContext.commonResource.pIsSDCard = this.isSDStage;
                pContext.commonResource.pIconPath = iconPath;
                pContext.commonResource.loadPromotionSource(this.isSDStage, true, pBgPath, pActionPath);
                PortConst.loadingTime = (long) ((this.promationInfo.get(0).duration + 3) * TimeConstants.MILLISECONDSPERSECOND);
            } else {
                PortConst.enablePromotion = false;
                PortConst.showPromtionIcon = false;
            }
        } else {
            PortConst.showPromtionIcon = false;
        }
    }

    public void checkWebInfo(final F2FGameActivity pContext, final long pUpdateTime) {
        if (Const.enableSDCard && com.finger2finger.games.common.Utils.checkNetWork(pContext)) {
            new Thread(new Runnable() {
                public void run() {
                    try {
                        PromotionFile.inizillie();
                        PromotionHandler.this.loadWebXmlInfo(pContext, Const.PROMOTION_URL + Const.PROMOTION_INDEX_XML_NAME, pUpdateTime);
                    } catch (Exception e) {
                        Exception e2 = e;
                        Log.e("PromotionHandler_checkWebInfo_loadWebXmlInfo_error", e2.toString());
                        GoogleAnalyticsUtils.setTracker("PromotionHandler_checkWebInfo_loadWebXmlInfo_error,detail is " + e2.toString());
                    }
                }
            }).start();
        }
    }

    public InputStream getWebStream(String pUrlPath) throws IOException {
        return ((HttpURLConnection) new URL(pUrlPath).openConnection()).getInputStream();
    }

    public void cutFile() throws IOException {
        this.enableCopy = false;
        PromotionFile.copyFileFromTemp(TablePath.T_PROMOTION_TEMP_PATH, TablePath.T_PROMOTION_USE_PATH);
    }

    public void loadAssetXml(F2FGameActivity pContext) {
        try {
            loadInfo(pContext, "");
        } catch (Exception e) {
            Exception e2 = e;
            PortConst.enablePromotion = false;
            PortConst.showPromtionIcon = false;
            Log.e("PromotionHandler_loadAssetXml_loadInfo_error", e2.toString());
            GoogleAnalyticsUtils.setTracker("PromotionHandler_loadAssetXml_loadInfo_error,detail is " + e2.toString());
        }
    }

    public void loadSDXml(F2FGameActivity pContext, String pFilePath) {
        try {
            loadInfo(pContext, pFilePath);
            if (PromotionFile.checkFileInfo(TablePath.T_PROMOTION_USE_PATH + Const.PROMOTION_XML_NAME, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(0).promotionbgPath, this.promationInfo.get(0).promotionbgsize, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(0).promotionPortBg, this.promationInfo.get(0).promotionPortSize, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(0).promotionactionpic, this.promationInfo.get(0).promotionactionpicsize, TablePath.T_PROMOTION_USE_PATH + this.promationInfo.get(0).promotionIcon, this.promationInfo.get(0).promotioniconSize, true) != CommonConst.PROMATION_FILE_STATUS.ENABLE_USE) {
                loadAssetXml(pContext);
            }
        } catch (Exception e) {
            loadAssetXml(pContext);
            Log.e("PromotionHandler_loadSDXml_loadInfo_error", e.toString());
            GoogleAnalyticsUtils.setTracker("PromotionHandler_loadSDXml_loadInfo_error,detail is " + e.toString());
        }
    }
}
