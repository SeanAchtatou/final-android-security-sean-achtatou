package com.finger2finger.games.common;

import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.Text;

public class RemovedObject {
    public IShape _ishape = null;
    public int _layer = 0;
    public Scene _scene = null;
    public Sprite _sprite = null;
    public Text _text = null;

    public RemovedObject(IShape pIshape, int pLayer) {
        this._ishape = pIshape;
        this._layer = pLayer;
    }

    public RemovedObject(Sprite sprite, Scene scene, int layer) {
        this._layer = layer;
        this._scene = scene;
        this._sprite = sprite;
    }

    public RemovedObject(Text text, Scene scene, int layer) {
        this._layer = layer;
        this._scene = scene;
        this._text = text;
    }
}
