package com.finger2finger.games.common.store.data;

public class Store {
    public static final int LIST_ITEM_COUNT = 7;
    private long arrival_date = 0;
    private int daily_limit = 0;
    private String id = "";
    private String product_id = "";
    private int saled_count = 0;
    private int term = 0;
    private int total_count = 0;

    public String getId() {
        return this.id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public int getDaily_limit() {
        return this.daily_limit;
    }

    public int getTotal_count() {
        return this.total_count;
    }

    public long getArrival_date() {
        return this.arrival_date;
    }

    public int getTerm() {
        return this.term;
    }

    public int getSaled_count() {
        return this.saled_count;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setProduct_id(String productId) {
        this.product_id = productId;
    }

    public void setDaily_limit(int dailyLimit) {
        this.daily_limit = dailyLimit;
    }

    public void setTotal_count(int totalCount) {
        this.total_count = totalCount;
    }

    public void setArrival_date(long arrivalDate) {
        this.arrival_date = arrivalDate;
    }

    public void setTerm(int term2) {
        this.term = term2;
    }

    public void setSaled_count(int saledCount) {
        this.saled_count = saledCount;
    }

    public Store(String[] data) throws Exception {
        if (data == null || data.length != 7) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.product_id = data[1];
        this.daily_limit = Integer.parseInt(data[2]);
        this.total_count = Integer.parseInt(data[3]);
        this.arrival_date = Long.parseLong(data[4]);
        this.term = Integer.parseInt(data[5]);
        this.saled_count = Integer.parseInt(data[6]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.product_id).append(TablePath.SEPARATOR_ITEM).append(this.daily_limit).append(TablePath.SEPARATOR_ITEM).append(this.total_count).append(TablePath.SEPARATOR_ITEM).append(this.arrival_date).append(TablePath.SEPARATOR_ITEM).append(this.term).append(TablePath.SEPARATOR_ITEM).append(this.saled_count);
        return sb.toString();
    }
}
