package com.finger2finger.games.common.scene.dialog;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.F2FMailSender;
import com.finger2finger.games.common.Utils;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.base.BaseFont;
import com.finger2finger.games.common.base.BaseSprite;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class F2FRecommendationDialog extends CameraScene {
    private TextureRegion bg;
    private float bg_height;
    private float bg_width;
    private float bg_x;
    private float bg_y;
    private TextureRegion close;
    private float close_x;
    private float close_y;
    private TextureRegion facebook;
    private float facebook_target_x;
    private float facebook_target_y;
    private TextureRegion gmail;
    private float gmail_target_x;
    private float gmail_target_y;
    /* access modifiers changed from: private */
    public F2FGameActivity mContext;
    private TextureRegion qq;
    private float qq_target_x;
    private float qq_target_y;
    private TextureRegion renren;
    private float renren_target_x;
    private float renren_target_y;
    private TextureRegion sina;
    private float sina_target_x;
    private float sina_target_y;
    private TextureRegion twitter;
    private float twitter_target_x;
    private float twitter_target_y;

    public F2FRecommendationDialog(Camera camera, F2FGameActivity pContext) {
        super(2, camera);
        this.mContext = pContext;
        initialButtonPosition();
        loadScene();
        setBackgroundEnabled(false);
    }

    private void initialButtonPosition() {
        float width = Const.MOVE_LIMITED_SPEED;
        float offset = 10.0f * CommonConst.RALE_SAMALL_VALUE;
        this.bg = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_DIALOG_BG.mKey);
        this.bg_width = ((float) this.bg.getWidth()) * CommonConst.RALE_SAMALL_VALUE * 1.0f;
        this.bg_height = ((float) this.bg.getHeight()) * CommonConst.RALE_SAMALL_VALUE * 1.0f;
        this.bg_x = (((float) CommonConst.CAMERA_WIDTH) - this.bg_width) / 2.0f;
        float original_x = this.bg_x + offset;
        this.bg_y = (((float) CommonConst.CAMERA_HEIGHT) - this.bg_height) / 2.0f;
        float original_y = this.bg_y + this.bg_height + (5.0f * CommonConst.RALE_SAMALL_VALUE);
        this.close = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_CLOSE.mKey);
        float close_width = ((float) this.close.getWidth()) * CommonConst.RALE_SAMALL_VALUE * 0.8f;
        float close_height = ((float) this.close.getHeight()) * CommonConst.RALE_SAMALL_VALUE * 0.8f;
        this.close_x = (this.bg_x + this.bg_width) - (1.5f * close_width);
        this.close_y = this.bg_y + (0.1f * close_height);
        float offset2 = getOffset(this.bg_width, offset);
        if (Const.enableFaceBook) {
            this.facebook = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_FACEBOOK.mKey);
            this.facebook_target_x = original_x + Const.MOVE_LIMITED_SPEED;
            this.facebook_target_y = original_y;
            width = Const.MOVE_LIMITED_SPEED + (((float) this.facebook.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset2;
        }
        if (Const.enableTwitter) {
            this.twitter = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_TWITTER.mKey);
            this.twitter_target_x = original_x + width;
            this.twitter_target_y = original_y;
            width = width + (((float) this.twitter.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset2;
        }
        if (Const.enableGmail) {
            this.gmail = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_INVITE.mKey);
            this.gmail_target_x = original_x + width;
            this.gmail_target_y = original_y;
            width = width + (((float) this.gmail.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset2;
        }
        if (Const.enableQQ) {
            this.qq = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_QQ.mKey);
            this.qq_target_x = original_x + width;
            this.qq_target_y = original_y;
            width = width + (((float) this.qq.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset2;
        }
        if (Const.enableSina) {
            this.sina = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_SINA.mKey);
            this.sina_target_x = original_x + width;
            this.sina_target_y = original_y;
            width = width + (((float) this.sina.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset2;
        }
        if (Const.enableRenRen) {
            this.renren = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_RENREN.mKey);
            this.renren_target_x = original_x + width;
            this.renren_target_y = original_y;
            float width2 = width + (((float) this.renren.getWidth()) * CommonConst.RALE_SAMALL_VALUE) + offset2;
        }
    }

    private float getOffset(float width, float offset) {
        int enableCount = 0;
        if (Const.enableFaceBook) {
            enableCount = 0 + 1;
        }
        if (Const.enableTwitter) {
            enableCount++;
        }
        if (Const.enableGmail) {
            enableCount++;
        }
        if (Const.enableQQ) {
            enableCount++;
        }
        if (Const.enableSina) {
            enableCount++;
        }
        if (Const.enableRenRen) {
            enableCount++;
        }
        return ((width - (((float) (enableCount * 64)) * CommonConst.RALE_SAMALL_VALUE)) - (2.0f * offset)) / ((float) (enableCount - 1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void
     arg types: [float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, int]
     candidates:
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion):void
      com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void */
    public void loadScene() {
        Rectangle rectangle = new Rectangle(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        rectangle.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue);
        rectangle.setAlpha(CommonConst.GrayColor.alpha);
        getLayer(0).addEntity(rectangle);
        getLayer(0).addEntity(new BaseSprite(this.bg_x, this.bg_y, CommonConst.RALE_SAMALL_VALUE * 1.0f, this.bg, false));
        AnonymousClass1 r6 = new BaseSprite(this.close_x, this.close_y, CommonConst.RALE_SAMALL_VALUE * 0.8f, this.close, false) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 0) {
                    F2FRecommendationDialog.this.mContext.getEngine().getScene().clearChildScene();
                    CommonConst.IS_GAMEOVER = true;
                    if (F2FRecommendationDialog.this.mContext.getmGameScene() != null) {
                        F2FRecommendationDialog.this.mContext.getmGameScene().showOKDialog();
                    }
                }
                return true;
            }
        };
        registerTouchArea(r6);
        getLayer(0).addEntity(r6);
        BaseFont mDefaultFontTitle = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_TITLE.mKey);
        String msgTitleStr = this.mContext.getResources().getString(R.string.str_tips);
        float textTitlePX = this.bg_x + ((this.bg_width - ((float) Utils.getMaxCharacterLens(mDefaultFontTitle, msgTitleStr))) / 2.0f);
        float textTitlePY = this.bg_y + (25.0f * CommonConst.RALE_SAMALL_VALUE);
        ChangeableText changeableText = new ChangeableText(textTitlePX, textTitlePY, mDefaultFontTitle, msgTitleStr);
        changeableText.setPosition(textTitlePX, textTitlePY);
        getLayer(0).addEntity(changeableText);
        BaseFont mDefaultFont = this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey);
        String autoLineMsg = Utils.getAutoLine(mDefaultFont, this.mContext.getResources().getString(R.string.F2F_recommendation_content), (int) (this.bg_width - (50.0f * CommonConst.RALE_SAMALL_VALUE)), false, 0);
        ChangeableText mStartContent = new ChangeableText(this.bg_x + ((this.bg_width - ((float) Utils.getMaxCharacterLens(mDefaultFont, autoLineMsg))) / 2.0f), textTitlePY + (((this.bg_height - ((textTitlePY - this.bg_y) + ((float) mDefaultFont.getLineHeight()))) - ((float) Utils.getLineHight(mDefaultFont, autoLineMsg))) / 2.0f), mDefaultFont, autoLineMsg, mDefaultFont.getStringWidth(autoLineMsg + "0000"));
        mStartContent.setScale(0.85f);
        getLayer(0).addEntity(mStartContent);
        if (Const.enableFaceBook) {
            AnonymousClass2 r12 = new BaseSprite(this.facebook_target_x, this.facebook_target_y, CommonConst.RALE_SAMALL_VALUE, this.facebook, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    Utils.link(F2FRecommendationDialog.this.mContext, Const.FACEBOOK_LINK + F2FRecommendationDialog.this.mContext.getApplication().getPackageName());
                    return true;
                }
            };
            registerTouchArea(r12);
            getTopLayer().addEntity(r12);
        }
        if (Const.enableTwitter) {
            AnonymousClass3 r13 = new BaseSprite(this.twitter_target_x, this.twitter_target_y, CommonConst.RALE_SAMALL_VALUE, this.twitter, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    Utils.link(F2FRecommendationDialog.this.mContext, Const.TWITTER_LINK + F2FRecommendationDialog.this.mContext.getApplication().getPackageName());
                    return true;
                }
            };
            registerTouchArea(r13);
            getTopLayer().addEntity(r13);
        }
        if (Const.enableGmail) {
            AnonymousClass4 r14 = new BaseSprite(this.gmail_target_x, this.gmail_target_y, CommonConst.RALE_SAMALL_VALUE, this.gmail, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    F2FMailSender.setMail(F2FRecommendationDialog.this.mContext, F2FMailSender.SEND_IN_MAINMENU);
                    return true;
                }
            };
            registerTouchArea(r14);
            getTopLayer().addEntity(r14);
        }
        if (Const.enableQQ) {
            AnonymousClass5 r15 = new BaseSprite(this.qq_target_x, this.qq_target_y, CommonConst.RALE_SAMALL_VALUE, this.qq, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    Utils.link(F2FRecommendationDialog.this.mContext, Const.QQ_LINK);
                    return true;
                }
            };
            registerTouchArea(r15);
            getTopLayer().addEntity(r15);
        }
        if (Const.enableSina) {
            AnonymousClass6 r16 = new BaseSprite(this.sina_target_x, this.sina_target_y, CommonConst.RALE_SAMALL_VALUE, this.sina, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    Utils.link(F2FRecommendationDialog.this.mContext, Const.SINA_LINK + F2FRecommendationDialog.this.mContext.getResources().getString(R.string.F2F_recommendation_sina));
                    return true;
                }
            };
            registerTouchArea(r16);
            getTopLayer().addEntity(r16);
        }
        if (Const.enableRenRen) {
            AnonymousClass7 r17 = new BaseSprite(this.renren_target_x, this.renren_target_y, CommonConst.RALE_SAMALL_VALUE, this.renren, false) {
                public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                    if (pSceneTouchEvent.getAction() != 0) {
                        return true;
                    }
                    Utils.link(F2FRecommendationDialog.this.mContext, Const.RENREN_LINK);
                    return true;
                }
            };
            registerTouchArea(r17);
            getTopLayer().addEntity(r17);
        }
    }
}
