package com.finger2finger.games.common.store.data;

import com.finger2finger.games.common.store.io.Utils;
import java.util.ArrayList;

public class GameTable {
    private ArrayList<Game> gameList = new ArrayList<>();

    public ArrayList<Game> getgameList() {
        return this.gameList;
    }

    public void setgameList(ArrayList<Game> gameList2) {
        this.gameList = gameList2;
    }

    public void createFile() throws Exception {
        Utils.createFile(TablePath.T_GAME_PATH);
    }

    public String[] readFile() throws Exception {
        return Utils.readFile(TablePath.T_GAME_PATH);
    }

    public void serlize(String[] data) throws Exception {
        Exception e;
        Game store = null;
        if (data != null && data.length > 0) {
            int i = 0;
            while (i < data.length) {
                try {
                    store = new Game(data[i].split(TablePath.SEPARATOR_ITEM));
                    try {
                        this.gameList.add(store);
                        i++;
                    } catch (Exception e2) {
                        e = e2;
                        throw e;
                    }
                } catch (Exception e3) {
                    e = e3;
                    throw e;
                }
            }
        }
    }

    public void write() throws Exception {
        String[] data = new String[this.gameList.size()];
        for (int i = 0; i < this.gameList.size(); i++) {
            data[i] = this.gameList.get(i).toString();
        }
        if (data != null && data.length > 0) {
            Utils.writeFile(TablePath.T_GAME_PATH, data);
        }
    }
}
