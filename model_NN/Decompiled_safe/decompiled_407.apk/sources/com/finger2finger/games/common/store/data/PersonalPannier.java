package com.finger2finger.games.common.store.data;

public class PersonalPannier {
    public static final int LIST_ITEM_COUNT = 4;
    private String id = "";
    private int product_count = 0;
    private String product_id = "";
    private String user_id = "";

    public String getId() {
        return this.id;
    }

    public String getUser_id() {
        return this.user_id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public int getProduct_count() {
        return this.product_count;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setUser_id(String userId) {
        this.user_id = userId;
    }

    public void setProduct_id(String productId) {
        this.product_id = productId;
    }

    public void setProduct_count(int productCount) {
        this.product_count = productCount;
    }

    public PersonalPannier(String[] data) throws Exception {
        if (data == null || data.length != 4) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.user_id = data[1];
        this.product_id = data[2];
        this.product_count = Integer.parseInt(data[3]);
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.user_id).append(TablePath.SEPARATOR_ITEM).append(this.product_id).append(TablePath.SEPARATOR_ITEM).append(this.product_count);
        return sb.toString();
    }
}
