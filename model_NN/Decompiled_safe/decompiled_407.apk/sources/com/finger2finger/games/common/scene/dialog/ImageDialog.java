package com.finger2finger.games.common.scene.dialog;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.entity.text.ChangeableText;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;

public class ImageDialog extends CameraScene {
    ChangeableText btn1Txt;
    private float btnRale = 1.0f;
    float image_x;
    float image_y;
    private int index = 0;
    float indistinctLayer_original_x;
    float indistinctLayer_original_y;
    float indistinctLayer_target_x;
    float indistinctLayer_target_y;
    private F2FGameActivity mContext;
    private Texture mImageTexture;
    private int mPageCount;
    private float mParentHeight;
    private float mParentWidth;
    private String mPathFormat;
    private float mSize;
    private Sprite mSpriteImage;
    private Rectangle mSpriteInsideIndistinctLayer;
    private Sprite mSpriteNext;
    private Rectangle mSpriteOutsideIndistinctLayer;
    private TextureRegion mTRImage;
    private TextureRegion mTROk;
    float next_x;
    float next_y;

    public ImageDialog(int layerCount, Camera camera, TextureRegion pTRImage, float pSize, F2FGameActivity pContext) {
        super(layerCount, camera);
        this.mContext = pContext;
        this.mSize = pSize;
        this.mTRImage = pTRImage;
        this.mPageCount = Const.GAME_HELP_COUNT;
        this.mPathFormat = CommonConst.HELP_FORMART_PICPATH;
        this.btnRale = 0.75f;
        this.index = 0;
        loadScene();
        setBackgroundEnabled(false);
    }

    public void loadRegion() {
        if (this.mTRImage == null) {
            this.mTRImage = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.GAME_HELP.mKey);
        }
        this.mImageTexture = this.mTRImage.getTexture();
        TextureRegionFactory.createFromAsset(this.mImageTexture, this.mContext, String.format(this.mPathFormat, Integer.valueOf(this.index + 1)), 0, 0);
        this.mTROk = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_BLANK.mKey);
        iniPosition();
    }

    public void loadScene() {
        loadRegion();
        this.mSpriteOutsideIndistinctLayer = new Rectangle(this.indistinctLayer_target_x, this.indistinctLayer_target_y, this.mParentWidth, this.mParentHeight) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                ImageDialog.this.setCurrentHelp(false);
                return true;
            }
        };
        this.mSpriteOutsideIndistinctLayer.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue, CommonConst.GrayColor.alpha);
        getLayer(0).addEntity(this.mSpriteOutsideIndistinctLayer);
        this.mSpriteImage = new Sprite(this.image_x, this.image_y, this.mSize * ((float) this.mTRImage.getWidth()) * CommonConst.RALE_SAMALL_VALUE, this.mSize * ((float) this.mTRImage.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTRImage.clone()) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                ImageDialog.this.setCurrentHelp(false);
                return true;
            }
        };
        this.mSpriteNext = new Sprite(this.next_x, this.next_y, this.btnRale * ((float) this.mTROk.getWidth()) * CommonConst.RALE_SAMALL_VALUE, this.btnRale * ((float) this.mTROk.getHeight()) * CommonConst.RALE_SAMALL_VALUE, this.mTROk) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                ImageDialog.this.setCurrentHelp(false);
                return true;
            }
        };
        getLayer(1).addEntity(this.mSpriteImage);
        getLayer(1).addEntity(this.mSpriteNext);
        registerTouchArea(this.mSpriteNext);
        this.btn1Txt = new ChangeableText(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, this.mContext.mResource.getBaseFontByKey(Resource.FONT.DIALOG_CONTEXT.mKey), "OK");
        this.btn1Txt.setPosition(this.next_x + ((this.mSpriteNext.getWidth() - this.btn1Txt.getWidth()) / 2.0f), this.next_y + ((this.mSpriteNext.getHeight() - this.btn1Txt.getHeight()) / 2.0f));
        getTopLayer().addEntity(this.btn1Txt);
    }

    private void iniPosition() {
        this.mParentWidth = (float) CommonConst.CAMERA_WIDTH;
        this.mParentHeight = (float) CommonConst.CAMERA_HEIGHT;
        this.indistinctLayer_original_x = Const.MOVE_LIMITED_SPEED - this.mParentWidth;
        this.indistinctLayer_original_y = Const.MOVE_LIMITED_SPEED;
        this.indistinctLayer_target_x = Const.MOVE_LIMITED_SPEED;
        this.indistinctLayer_target_y = Const.MOVE_LIMITED_SPEED;
        this.image_x = (this.mParentWidth - ((((float) this.mTRImage.getWidth()) * CommonConst.RALE_SAMALL_VALUE) * this.mSize)) / 2.0f;
        this.image_y = (this.mParentHeight - ((((float) this.mTRImage.getHeight()) * CommonConst.RALE_SAMALL_VALUE) * this.mSize)) / 2.0f;
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
            this.image_y = (this.mParentHeight - ((((float) this.mTRImage.getHeight()) * CommonConst.RALE_SAMALL_VALUE) * this.mSize)) - (70.0f * CommonConst.RALE_SAMALL_VALUE);
        }
        this.next_x = ((this.image_x + ((((float) this.mTRImage.getWidth()) * CommonConst.RALE_SAMALL_VALUE) * this.mSize)) - ((((float) this.mTROk.getWidth()) * CommonConst.RALE_SAMALL_VALUE) * this.btnRale)) - (15.0f * CommonConst.RALE_SAMALL_VALUE);
        this.next_y = this.image_y + (((float) this.mTRImage.getHeight()) * CommonConst.RALE_SAMALL_VALUE * this.mSize);
    }

    /* access modifiers changed from: private */
    public void setCurrentHelp(boolean pIsClose) {
        if (pIsClose) {
            this.mContext.getEngine().getScene().clearChildScene();
            this.mContext.getmGameScene().onImageDialogCancelBtn();
            return;
        }
        this.index++;
        if (this.index >= this.mPageCount) {
            this.mContext.getEngine().getScene().clearChildScene();
            this.mContext.getmGameScene().onImageDialogCancelBtn();
            return;
        }
        TextureRegionFactory.createFromAsset(this.mImageTexture, this.mContext, String.format(this.mPathFormat, Integer.valueOf(this.index + 1)), 0, 0);
    }

    public void setImageSize(float pWidth, float pHeight) {
        this.image_x = (this.mParentWidth - pWidth) / 2.0f;
        this.image_y = (this.mParentHeight - pHeight) / 2.0f;
        this.mSpriteImage.setWidth(pWidth);
        this.mSpriteImage.setHeight(pHeight);
        if (!CommonConst.ENABLE_SHOW_ADVIEW) {
        }
        this.mSpriteImage.setPosition(this.image_x, this.image_y);
        float pX = ((this.mSpriteImage.getX() + this.mSpriteImage.getWidth()) - this.mSpriteNext.getWidth()) - (15.0f * CommonConst.RALE_SAMALL_VALUE);
        float pY = this.mSpriteImage.getY() + this.mSpriteImage.getHeight();
        this.mSpriteNext.setPosition(pX, pY);
        this.btn1Txt.setPosition(pX + ((this.mSpriteNext.getWidth() - this.btn1Txt.getWidth()) / 2.0f), pY + ((this.mSpriteNext.getHeight() - this.btn1Txt.getHeight()) / 2.0f));
    }
}
