package com.finger2finger.games.common;

import android.content.Context;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.source.ITextureSource;

public class TextureOperate {
    public static void changeTextureSource(Texture pTexture, ITextureSource pTextrueSource, int pTexturePositionX, int pTexturePositionY) {
        if (pTexture != null) {
            pTexture.clearTextureSources();
            TextureRegionFactory.createFromSource(pTexture, pTextrueSource, pTexturePositionX, pTexturePositionY);
        }
    }

    public static void changeTextureAsset(Texture pTexture, Context pContext, String pAssetPath, int pTexturePositionX, int pTexturePositionY) {
        if (pTexture != null) {
            pTexture.clearTextureSources();
            TextureRegionFactory.createFromAsset(pTexture, pContext, pAssetPath, pTexturePositionX, pTexturePositionY);
        }
    }
}
