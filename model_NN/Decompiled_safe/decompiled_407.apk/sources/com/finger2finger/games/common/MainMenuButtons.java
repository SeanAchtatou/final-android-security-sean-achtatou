package com.finger2finger.games.common;

import android.widget.Toast;
import com.finger2finger.games.common.activity.F2FGameActivity;
import com.finger2finger.games.common.res.CommonResource;
import com.finger2finger.games.common.scene.AboutScene;
import com.finger2finger.games.common.scene.F2FScene;
import com.finger2finger.games.horseclickclear.lite.R;
import com.finger2finger.games.res.Const;
import com.finger2finger.games.res.Resource;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.shape.IShape;
import org.anddev.andengine.entity.shape.modifier.IShapeModifier;
import org.anddev.andengine.entity.shape.modifier.LoopShapeModifier;
import org.anddev.andengine.entity.shape.modifier.MoveModifier;
import org.anddev.andengine.entity.shape.modifier.ScaleModifier;
import org.anddev.andengine.entity.shape.modifier.SequenceShapeModifier;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.input.touch.TouchEvent;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.modifier.IModifier;

public class MainMenuButtons {
    float AboutFrameHeight = ((float) CommonConst.CAMERA_HEIGHT);
    float AboutFrameWidth = (((float) CommonConst.CAMERA_WIDTH) * 0.5f);
    private float about_original_x;
    private float about_original_y;
    private float about_target_x;
    private float about_target_y;
    private float facebook_original_x;
    private float facebook_original_y;
    private float facebook_target_x;
    private float facebook_target_y;
    private float friend_original_x;
    private float friend_original_y;
    private float friend_target_x;
    private float friend_target_y;
    /* access modifiers changed from: private */
    public boolean isEnable = true;
    private TextureRegion mAboutRegion;
    private AboutScene mAboutScene;
    private Sprite mAboutSprite;
    /* access modifiers changed from: private */
    public AnimatedSprite mArrowSprite;
    /* access modifiers changed from: private */
    public F2FGameActivity mContext;
    private TextureRegion mFaceBookRegion;
    private Sprite mFaceBookSprite;
    private TextureRegion mFriendRegion;
    private Sprite mFriendSprite;
    private int mLayer = 2;
    private TiledTextureRegion mMusicRegion;
    private AnimatedSprite mMusicSprite;
    private F2FScene mScene;
    private TiledTextureRegion mTTRArrowRegion;
    private TextureRegion mTwitterRegion;
    private Sprite mTwitterSprite;
    private float menu_Height;
    private float menu_Width;
    private float menu_original_x;
    /* access modifiers changed from: private */
    public float menu_original_y;
    private float menu_target_x;
    /* access modifiers changed from: private */
    public float menu_target_y;
    private float music_original_x;
    private float music_original_y;
    private float music_target_x;
    private float music_target_y;
    private float twitter_original_x;
    private float twitter_original_y;
    private float twitter_target_x;
    private float twitter_target_y;

    public MainMenuButtons(F2FGameActivity pContext, F2FScene pScene, int pLayer) {
        this.mContext = pContext;
        this.mScene = pScene;
        this.mLayer = pLayer;
    }

    public void createButtons() {
        initMenuSprites(this.mScene, this.mLayer);
    }

    private void initMenuSprites(Scene pScene, int pLayerIndex) {
        setSpritePosition();
        initArrowSprite(pScene, pLayerIndex);
        initMusicSprite(pScene, pLayerIndex);
        initAboutSprite(pScene, pLayerIndex);
        initFacebookSprite(pScene, pLayerIndex);
        initTwitterSprite(pScene, pLayerIndex);
        initFriendSprite(pScene, pLayerIndex);
        ShopButton shopButton = new ShopButton();
        shopButton.enableShowPromtion = true;
        shopButton.showShop(this.mContext, pScene, pLayerIndex, "MainMenu");
    }

    private void initArrowSprite(Scene pScene, int pLayerIndex) {
        this.mArrowSprite = new AnimatedSprite(this.menu_original_x, this.menu_original_y, this.menu_Width, this.menu_Height, this.mTTRArrowRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 0) {
                    if (!MainMenuButtons.this.isEnable) {
                        return true;
                    }
                    if (getY() == MainMenuButtons.this.menu_original_y) {
                        clearShapeModifiers();
                        MainMenuButtons.this.mArrowSprite.setScale(1.0f);
                        MainMenuButtons.this.showMenuItmesMoveAnimation(true);
                    } else if (getY() == MainMenuButtons.this.menu_target_y) {
                        MainMenuButtons.this.showMenuItmesMoveAnimation(false);
                    }
                }
                return true;
            }
        };
        this.mArrowSprite.setCurrentTileIndex(0);
        menuItmeAnimation();
        this.mArrowSprite.setRotation(90.0f);
        pScene.getLayer(pLayerIndex).addEntity(this.mArrowSprite);
        pScene.getLayer(pLayerIndex).registerTouchArea(this.mArrowSprite);
    }

    /* access modifiers changed from: private */
    public void showMenuItmesMoveAnimation(final boolean isOn) {
        float f;
        AnimatedSprite animatedSprite = this.mArrowSprite;
        if (isOn) {
            f = this.menu_original_x;
        } else {
            f = this.menu_target_x;
        }
        animatedSprite.addShapeModifier(new MoveModifier(0.65f, f, isOn ? this.menu_target_x : this.menu_original_x, isOn ? this.menu_original_y : this.menu_target_y, isOn ? this.menu_target_y : this.menu_original_y, new IShapeModifier.IShapeModifierListener() {
            public /* bridge */ /* synthetic */ void onModifierFinished(IModifier x0, Object x1) {
                onModifierFinished((IModifier<IShape>) x0, (IShape) x1);
            }

            public void onModifierFinished(IModifier<IShape> iModifier, IShape pItem) {
                if (isOn) {
                    MainMenuButtons.this.mArrowSprite.setCurrentTileIndex(1);
                    return;
                }
                MainMenuButtons.this.mArrowSprite.setCurrentTileIndex(0);
                MainMenuButtons.this.menuItmeAnimation();
            }
        }));
        this.mAboutSprite.addShapeModifier(new MoveModifier(0.65f, isOn ? this.about_original_x : this.about_target_x, isOn ? this.about_target_x : this.about_original_x, isOn ? this.about_original_y : this.about_target_y, isOn ? this.about_target_y : this.about_original_y));
        this.mMusicSprite.addShapeModifier(new MoveModifier(0.65f, isOn ? this.music_original_x : this.music_target_x, isOn ? this.music_target_x : this.music_original_x, isOn ? this.music_original_y : this.music_target_y, isOn ? this.music_target_y : this.music_original_y));
        this.mFaceBookSprite.addShapeModifier(new MoveModifier(0.65f, isOn ? this.facebook_original_x : this.facebook_target_x, isOn ? this.facebook_target_x : this.facebook_original_x, isOn ? this.facebook_original_y : this.facebook_target_y, isOn ? this.facebook_target_y : this.facebook_original_y));
        this.mTwitterSprite.addShapeModifier(new MoveModifier(0.65f, isOn ? this.twitter_original_x : this.twitter_target_x, isOn ? this.twitter_target_x : this.twitter_original_x, isOn ? this.twitter_original_y : this.twitter_target_y, isOn ? this.twitter_target_y : this.twitter_original_y));
        this.mFriendSprite.addShapeModifier(new MoveModifier(0.65f, isOn ? this.friend_original_x : this.friend_target_x, isOn ? this.friend_target_x : this.friend_original_x, isOn ? this.friend_original_y : this.friend_target_y, isOn ? this.friend_target_y : this.friend_original_y));
    }

    /* access modifiers changed from: private */
    public void menuItmeAnimation() {
        this.mArrowSprite.addShapeModifier(new LoopShapeModifier(new SequenceShapeModifier(new ScaleModifier(0.8f, 1.0f, 1.2f), new ScaleModifier(0.8f, 1.2f, 1.0f))));
    }

    private void initMusicSprite(Scene pScene, int layoutIndex) {
        this.mMusicSprite = new AnimatedSprite(this.music_original_x, this.music_original_y, CommonConst.RALE_SAMALL_VALUE * (((float) this.mMusicRegion.getWidth()) / 2.0f), CommonConst.RALE_SAMALL_VALUE * ((float) this.mMusicRegion.getHeight()), this.mMusicRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 0 && MainMenuButtons.this.mContext.mResource != null) {
                    if (CommonConst.GAME_MUSIC_ON) {
                        setCurrentTileIndex(1);
                        CommonConst.GAME_MUSIC_ON = false;
                        MainMenuButtons.this.mContext.mResource.pauseMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
                    } else {
                        setCurrentTileIndex(0);
                        CommonConst.GAME_MUSIC_ON = true;
                        MainMenuButtons.this.mContext.mResource.playMusic(Resource.MUSICTURE.BACKGROUD_MUSIC);
                    }
                }
                return true;
            }
        };
        if (CommonConst.GAME_MUSIC_ON) {
            this.mMusicSprite.setCurrentTileIndex(0);
        } else {
            this.mMusicSprite.setCurrentTileIndex(1);
        }
        pScene.getLayer(layoutIndex).addEntity(this.mMusicSprite);
        pScene.getLayer(layoutIndex).registerTouchArea(this.mMusicSprite);
    }

    private void initAboutSprite(Scene pScene, int layoutIndex) {
        final Scene scene = pScene;
        this.mAboutSprite = new Sprite(this.about_original_x, this.about_original_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mAboutRegion.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mAboutRegion.getHeight()), this.mAboutRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                MainMenuButtons.this.showAboutScene(scene);
                return true;
            }
        };
        pScene.getLayer(layoutIndex).addEntity(this.mAboutSprite);
        pScene.getLayer(layoutIndex).registerTouchArea(this.mAboutSprite);
    }

    public void showAboutScene(Scene pScene) {
        if (this.mAboutScene == null) {
            this.mAboutScene = new AboutScene(this.mContext.getEngine().getCamera(), this.mContext);
        }
        pScene.setChildScene(this.mAboutScene, false, true, true);
        GoogleAnalyticsUtils.setTracker("/AboutScene");
        this.mAboutScene.showAboutSceneMoveAnimation(true);
    }

    private void initTwitterSprite(Scene pScene, int layoutIndex) {
        this.mTwitterSprite = new Sprite(this.twitter_original_x, this.twitter_original_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mTwitterRegion.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mTwitterRegion.getHeight()), this.mTwitterRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                Utils.link(MainMenuButtons.this.mContext, Const.TWITTER_LINK + MainMenuButtons.this.mContext.getApplication().getPackageName());
                return true;
            }
        };
        pScene.getLayer(layoutIndex).registerTouchArea(this.mTwitterSprite);
        pScene.getLayer(layoutIndex).addEntity(this.mTwitterSprite);
    }

    private void initFriendSprite(Scene pScene, int layoutIndex) {
        this.mFriendSprite = new Sprite(this.friend_original_x, this.friend_original_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mFriendRegion.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mFriendRegion.getHeight()), this.mFriendRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == 0) {
                    if (CommonConst.ENABLE_INVITE_FRIEND) {
                        F2FMailSender.setMail(MainMenuButtons.this.mContext, F2FMailSender.SEND_IN_MAINMENU);
                    } else {
                        Toast.makeText(MainMenuButtons.this.mContext, MainMenuButtons.this.mContext.getResources().getString(R.string.Enough_to_invite), 1).show();
                    }
                }
                return true;
            }
        };
        pScene.getLayer(layoutIndex).registerTouchArea(this.mFriendSprite);
        pScene.getLayer(layoutIndex).addEntity(this.mFriendSprite);
    }

    private void initFacebookSprite(Scene pScene, int layoutIndex) {
        this.mFaceBookSprite = new Sprite(this.facebook_original_x, this.facebook_original_y, CommonConst.RALE_SAMALL_VALUE * ((float) this.mFaceBookRegion.getWidth()), CommonConst.RALE_SAMALL_VALUE * ((float) this.mFaceBookRegion.getHeight()), this.mFaceBookRegion) {
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() != 0) {
                    return true;
                }
                Utils.link(MainMenuButtons.this.mContext, Const.FACEBOOK_LINK + MainMenuButtons.this.mContext.getApplication().getPackageName());
                return true;
            }
        };
        pScene.getLayer(layoutIndex).registerTouchArea(this.mFaceBookSprite);
        pScene.getLayer(layoutIndex).addEntity(this.mFaceBookSprite);
    }

    private void setSpritePosition() {
        intRegion();
        this.menu_Width = ((float) this.mTTRArrowRegion.getTileWidth()) * CommonConst.RALE_SAMALL_VALUE;
        this.menu_Height = ((float) this.mTTRArrowRegion.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE;
        this.menu_original_x = ((float) CommonConst.CAMERA_WIDTH) - (this.menu_Width * 1.3f);
        this.menu_original_y = ((float) CommonConst.CAMERA_HEIGHT) - this.menu_Height;
        float f = this.menu_original_y + (this.menu_Height / 2.0f);
        float original_y = this.menu_original_y;
        float width = Const.MOVE_LIMITED_SPEED + (((float) this.mTTRArrowRegion.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) + 10.0f;
        this.about_original_x = this.menu_original_x;
        this.about_original_y = original_y + width;
        float width2 = width + (((float) this.mAboutRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE) + 10.0f;
        this.music_original_x = this.menu_original_x;
        this.music_original_y = original_y + width2;
        float width3 = width2 + (((float) this.mMusicRegion.getTileHeight()) * CommonConst.RALE_SAMALL_VALUE) + 10.0f;
        this.facebook_original_x = this.menu_original_x;
        this.facebook_original_y = original_y + width3;
        float width4 = width3 + (((float) this.mFaceBookRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE) + 10.0f;
        this.twitter_original_x = this.menu_original_x;
        this.twitter_original_y = original_y + width4;
        float width5 = width4 + (((float) this.mTwitterRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE) + 10.0f;
        this.friend_original_x = this.menu_original_x;
        this.friend_original_y = original_y + width5;
        float width6 = ((((width5 + (((float) this.mFriendRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE)) + 10.0f) - 10.0f) - (((float) this.mAboutRegion.getHeight()) * CommonConst.RALE_SAMALL_VALUE)) + 10.0f;
        this.about_target_x = this.menu_original_x;
        this.about_target_y = this.about_original_y - width6;
        this.music_target_x = this.menu_original_x;
        this.music_target_y = this.music_original_y - width6;
        this.facebook_target_x = this.menu_original_x;
        this.facebook_target_y = this.facebook_original_y - width6;
        this.twitter_target_x = this.menu_original_x;
        this.twitter_target_y = this.twitter_original_y - width6;
        this.friend_target_x = this.menu_original_x;
        this.friend_target_y = this.friend_original_y - width6;
        this.menu_target_x = this.menu_original_x;
        this.menu_target_y = this.menu_original_y - width6;
    }

    private void intRegion() {
        this.mTTRArrowRegion = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_LEFT_RIGHT.mKey);
        this.mMusicRegion = this.mContext.commonResource.getTiledTextureRegionByKey(CommonResource.TILEDTURE.BUTTON_MUSIC.mKey);
        this.mFaceBookRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_FACEBOOK.mKey);
        this.mTwitterRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_TWITTER.mKey);
        this.mAboutRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_ABOUT.mKey);
        this.mFriendRegion = this.mContext.commonResource.getTextureRegionByKey(CommonResource.TEXTURE.BUTTON_INVITE.mKey);
    }
}
