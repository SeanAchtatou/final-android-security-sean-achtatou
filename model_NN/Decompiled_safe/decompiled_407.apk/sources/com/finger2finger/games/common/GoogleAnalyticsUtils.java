package com.finger2finger.games.common;

import android.content.Context;
import android.util.Log;
import com.finger2finger.games.res.PortConst;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;

public class GoogleAnalyticsUtils {
    private static boolean analyStarted = false;
    public static GoogleAnalyticsTracker tracker;

    public static void getInstance(Context pContext) {
        try {
            if (!analyStarted) {
                analyStarted = true;
            }
            tracker = GoogleAnalyticsTracker.getInstance();
            tracker.start(PortConst.GoogleAnalytics, pContext);
        } catch (Exception e) {
            Log.e("GoogleAnalyticsUtils_getInstance", e.getMessage());
        }
    }

    public static void stopTracker() {
        try {
            if (tracker != null) {
                tracker.stop();
                tracker = null;
                analyStarted = false;
            }
        } catch (Exception e) {
            Log.e("GoogleAnalyticsUtils_stopTracker", e.getMessage());
        }
    }

    public static void dispatchTracker() {
        try {
            if (tracker != null) {
                tracker.dispatch();
            }
        } catch (Exception e) {
            Log.e("GoogleAnalyticsUtils_dispatchTracker", e.getMessage());
        }
    }

    public static void setTracker(String pMsg) {
        try {
            if (tracker != null) {
                tracker.trackPageView(pMsg);
            }
        } catch (Exception e) {
            Log.e("GoogleAnalyticsUtils_setTracker", e.getMessage());
        }
    }
}
