package com.finger2finger.games.common.store.io;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.common.store.data.TablePath;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

public class Utils {
    public static boolean isFileExist(String name) {
        try {
            if (new File(Environment.getExternalStorageDirectory(), name).exists()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDirectoryExist(String name) {
        try {
            if (new File(Environment.getExternalStorageDirectory(), name).exists()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean createDir(String name) throws Exception {
        try {
            File destDir = new File(Environment.getExternalStorageDirectory(), name);
            if (destDir.exists()) {
                return false;
            }
            destDir.mkdirs();
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean createFile(String name) throws Exception {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), name);
            if (file.exists()) {
                return false;
            }
            file.createNewFile();
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public static boolean deleteFile(String name) throws Exception {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), name);
            if (!file.exists()) {
                return false;
            }
            file.delete();
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public static void writeFile(String name, String[] data) throws Exception {
        try {
            createFile(name);
            File file = new File(Environment.getExternalStorageDirectory(), name);
            if (!file.exists()) {
                throw new FileNotFoundException(Environment.getExternalStorageDirectory() + "/" + name);
            }
            FileWriter fw = new FileWriter(file);
            CSVWriter cw = new CSVWriter(fw);
            cw.writeNext(data, CommonConst.MASTER_PASSWORD);
            cw.close();
            fw.close();
        } catch (Exception e) {
            throw e;
        }
    }

    public static String[] readFile(String name) throws Exception {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), name);
            if (!file.exists()) {
                throw new FileNotFoundException(Environment.getExternalStorageDirectory() + "/" + name);
            }
            CSVReader reader = new CSVReader(new FileReader(file));
            String[] data = reader.readNext(CommonConst.MASTER_PASSWORD);
            if (data != null && data.length > 0 && !data[0].contains(TablePath.SEPARATOR_ITEM)) {
                reader.close();
                reader = new CSVReader(new FileReader(file), TablePath.SEPARATOR_ITEM_0);
                data = reader.readNext(CommonConst.MASTER_PASSWORD);
            }
            reader.close();
            return data;
        } catch (Exception e) {
            throw e;
        }
    }

    public static Bitmap loadBitmapFromSDCard(String path) throws Exception {
        try {
            return BitmapFactory.decodeStream(new BufferedInputStream(new FileInputStream(new File(Environment.getExternalStorageDirectory(), path))));
        } catch (Exception e) {
            throw e;
        }
    }

    public static Bitmap loadBitmapFromAssets(Context mContext, String path) throws Exception {
        try {
            return BitmapFactory.decodeStream(mContext.getAssets().open(path));
        } catch (Exception e) {
            throw e;
        }
    }

    public static String[] getSplitData(String pData, int pLenght) {
        String[] retValue = pData.split(TablePath.SEPARATOR_ITEM);
        if (retValue == null || retValue.length == pLenght) {
            return retValue;
        }
        return pData.split(TablePath.SEPARATOR_ITEM_0);
    }
}
