package com.finger2finger.games.common.scene.dialog;

import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.res.Const;
import org.anddev.andengine.engine.camera.Camera;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.CameraScene;

public class EmptyDialog extends CameraScene {
    float mDistanceY = 30.0f;
    private Rectangle mSpriteOutsideIndistinctLayer;

    public EmptyDialog(int layerCount, Camera camera) {
        super(layerCount, camera);
        loadScene();
    }

    public void loadScene() {
        this.mSpriteOutsideIndistinctLayer = new Rectangle(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) CommonConst.CAMERA_WIDTH, (float) CommonConst.CAMERA_HEIGHT);
        this.mSpriteOutsideIndistinctLayer.setColor(CommonConst.GrayColor.red, CommonConst.GrayColor.green, CommonConst.GrayColor.blue, CommonConst.GrayColor.alpha);
        getLayer(0).addEntity(this.mSpriteOutsideIndistinctLayer);
        setBackgroundEnabled(false);
    }
}
