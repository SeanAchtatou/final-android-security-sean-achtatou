package com.finger2finger.games.common.base;

import com.finger2finger.games.res.Const;
import org.anddev.andengine.entity.sprite.Sprite;
import org.anddev.andengine.opengl.texture.region.TextureRegion;

public class BaseSprite extends Sprite {
    protected boolean mEnableMove;
    protected float mspeedX;
    protected float mspeedY;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BaseSprite(float r8, float r9, float r10, org.anddev.andengine.opengl.texture.region.TextureRegion r11, boolean r12) {
        /*
            r7 = this;
            r2 = 1
            r6 = 0
            if (r12 != r2) goto L_0x0027
            float r0 = r8 * r10
            r1 = r0
        L_0x0007:
            if (r12 != r2) goto L_0x0029
            float r0 = r9 * r10
            r2 = r0
        L_0x000c:
            int r0 = r11.getWidth()
            float r0 = (float) r0
            float r3 = r0 * r10
            int r0 = r11.getHeight()
            float r0 = (float) r0
            float r4 = r0 * r10
            r0 = r7
            r5 = r11
            r0.<init>(r1, r2, r3, r4, r5)
            r0 = 0
            r7.mEnableMove = r0
            r7.mspeedX = r6
            r7.mspeedY = r6
            return
        L_0x0027:
            r1 = r8
            goto L_0x0007
        L_0x0029:
            r2 = r9
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BaseSprite(float r8, float r9, float r10, float r11, float r12, org.anddev.andengine.opengl.texture.region.TextureRegion r13, boolean r14) {
        /*
            r7 = this;
            r2 = 1
            r6 = 0
            if (r14 != r2) goto L_0x001d
            float r0 = r8 * r12
            r1 = r0
        L_0x0007:
            if (r14 != r2) goto L_0x001f
            float r0 = r9 * r12
            r2 = r0
        L_0x000c:
            float r3 = r10 * r12
            float r4 = r11 * r12
            r0 = r7
            r5 = r13
            r0.<init>(r1, r2, r3, r4, r5)
            r0 = 0
            r7.mEnableMove = r0
            r7.mspeedX = r6
            r7.mspeedY = r6
            return
        L_0x001d:
            r1 = r8
            goto L_0x0007
        L_0x001f:
            r2 = r9
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.finger2finger.games.common.base.BaseSprite.<init>(float, float, float, float, float, org.anddev.andengine.opengl.texture.region.TextureRegion, boolean):void");
    }

    public BaseSprite(float px, float py, float width, float height, TextureRegion textureRegion) {
        super(px, py, width, height, textureRegion);
        this.mEnableMove = false;
        this.mspeedX = Const.MOVE_LIMITED_SPEED;
        this.mspeedY = Const.MOVE_LIMITED_SPEED;
    }

    public void setMEnableMove(boolean pEnableMove) {
        this.mEnableMove = pEnableMove;
    }

    public boolean isMEnableMove() {
        return this.mEnableMove;
    }

    public float getMspeedX() {
        return this.mspeedX;
    }

    public void setMspeedX(float mspeedX2) {
        this.mspeedX = mspeedX2;
    }

    public float getMspeedY() {
        return this.mspeedY;
    }

    public void setMspeedY(float mspeedY2) {
        this.mspeedY = mspeedY2;
    }
}
