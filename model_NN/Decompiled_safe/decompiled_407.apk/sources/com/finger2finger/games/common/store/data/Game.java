package com.finger2finger.games.common.store.data;

public class Game {
    public static final int LIST_ITEM_COUNT = 9;
    private int IsInstalled = 0;
    private String comment = "";
    private String icon = "";
    private String id = "";
    private String name = "";
    private String product_id = "";
    private String store_id = "";
    private String uri = "";
    private String version = "";

    public String getId() {
        return this.id;
    }

    public String getProduct_id() {
        return this.product_id;
    }

    public String getName() {
        return this.name;
    }

    public String getVersion() {
        return this.version;
    }

    public String getIcon() {
        return this.icon;
    }

    public String getComment() {
        return this.comment;
    }

    public String getUri() {
        return this.uri;
    }

    public int getIsInstalled() {
        return this.IsInstalled;
    }

    public String getStore_id() {
        return this.store_id;
    }

    public void setId(String id2) {
        this.id = id2;
    }

    public void setProduct_id(String productId) {
        this.product_id = productId;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public void setVersion(String version2) {
        this.version = version2;
    }

    public void setIcon(String icon2) {
        this.icon = icon2;
    }

    public void setComment(String comment2) {
        this.comment = comment2;
    }

    public void setUri(String uri2) {
        this.uri = uri2;
    }

    public void setIsInstalled(int isInstalled) {
        this.IsInstalled = isInstalled;
    }

    public void setStore_id(String storeId) {
        this.store_id = storeId;
    }

    public Game(String[] data) throws Exception {
        if (data == null || data.length != 9) {
            throw new IllegalArgumentException();
        }
        this.id = data[0];
        this.product_id = data[1];
        this.name = data[2];
        this.version = data[3];
        this.icon = data[4];
        this.comment = data[5];
        this.uri = data[6];
        this.IsInstalled = Integer.parseInt(data[7]);
        this.store_id = data[8];
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.id).append(TablePath.SEPARATOR_ITEM).append(this.product_id).append(TablePath.SEPARATOR_ITEM).append(this.name).append(TablePath.SEPARATOR_ITEM).append(this.version).append(TablePath.SEPARATOR_ITEM).append(this.icon).append(TablePath.SEPARATOR_ITEM).append(this.comment).append(TablePath.SEPARATOR_ITEM).append(this.uri).append(TablePath.SEPARATOR_ITEM).append(this.IsInstalled).append(TablePath.SEPARATOR_ITEM).append(this.store_id);
        return sb.toString();
    }
}
