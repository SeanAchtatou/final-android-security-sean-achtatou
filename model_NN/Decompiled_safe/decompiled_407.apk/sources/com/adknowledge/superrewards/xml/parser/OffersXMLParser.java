package com.adknowledge.superrewards.xml.parser;

import android.sax.Element;
import android.sax.EndElementListener;
import android.sax.EndTextElementListener;
import android.sax.RootElement;
import android.sax.StartElementListener;
import android.util.Log;
import android.util.Xml;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SROfferType;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.model.SRPricePoint;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;

public class OffersXMLParser {
    /* access modifiers changed from: private */
    public SROffer currentSROffer;
    /* access modifiers changed from: private */
    public SRPricePoint currentSRPricePoint;

    public List<SROffer> parse(String xml) {
        Log.i("SR", "In Offers Parser");
        this.currentSROffer = new SROffer();
        RootElement rootElement = new RootElement(SROffer.SRXML);
        List<SROffer> messages = new ArrayList<>();
        rootElement.setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                boolean z;
                boolean z2;
                SRParams.srv = Integer.parseInt(arg0.getValue(SRParams.SRV));
                if (arg0.getValue(SRParams.DIRECTPAY_AVAILABLE).toString().equals("1")) {
                    z = true;
                } else {
                    z = false;
                }
                SRParams.directpay_available = z;
                Log.i("SR", "Set directpay_available " + SRParams.directpay_available);
                if (arg0.getValue(SRParams.OFFERS_AVAILABLE).toString().equals("1")) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                SRParams.offers_available = z2;
                Log.i("SR", "Set srv " + SRParams.srv);
            }
        });
        rootElement.getChild(SRParams.PARAMS).getChild(SRParams.LINKS).getChild(SRParams.HELP).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.help = body;
                Log.i("SR", "Set HELP " + SRParams.help);
            }
        });
        rootElement.getChild(SRParams.PARAMS).getChild(SRParams.APPINFO).setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                SRParams.name = arg0.getValue("name").toString();
                Log.i("SR", "Set Name " + SRParams.name);
            }
        });
        rootElement.getChild(SRParams.PARAMS).getChild(SRParams.APPINFO).getChild("currency").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.currency = body;
                Log.i("SR", "Set CURRENCY " + SRParams.currency);
            }
        });
        rootElement.getChild(SRParams.PARAMS).getChild(SRParams.APPINFO).getChild(SRParams.CURRENCY_ICON).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.currency_icon = body;
                Log.i("SR", "Set CURRENCY ICON " + SRParams.currency_icon);
            }
        });
        rootElement.getChild(SRParams.DIRECTPAY).getChild(SRParams.LOCAL_RATE).getChild(SRParams.RATE).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.rate = BigDecimal.valueOf((double) Float.parseFloat(body));
                Log.i("SR", "Set RATE " + SRParams.rate);
            }
        });
        rootElement.getChild(SRParams.DIRECTPAY).getChild(SRParams.LOCAL_RATE).getChild(SRParams.CURRENCY_CODE).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.currency_code = body;
                Log.i("SR", "Set CURRENCY CODE " + SRParams.currency_code);
            }
        });
        rootElement.getChild(SRParams.DIRECTPAY).getChild(SRParams.LOCAL_RATE).getChild(SRParams.CURRENCY_SYMBOL).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.currency_symbol = body;
                Log.i("SR", "Set CURRENCY SYMBOL " + SRParams.currency_symbol);
            }
        });
        rootElement.getChild(SRParams.DIRECTPAY).getChild(SRParams.LOCAL_RATE).getChild(SRParams.TO_US_TEXT).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                SRParams.to_us_text = body;
                Log.i("SR", "Set US TEXT " + SRParams.to_us_rate);
            }
        });
        Element dpayoffer = rootElement.getChild(SRParams.DIRECTPAY).getChild(SRParams.PROVIDERS).getChild(SRParams.PROVIDER);
        dpayoffer.setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                OffersXMLParser.this.currentSROffer.setId(arg0.getValue("id").toString());
                Log.i("SR", "Set Offer with ID of " + arg0.getValue("id").toString());
                OffersXMLParser.this.currentSROffer.setOfferType(SROfferType.DIRECT);
                Log.i("SR", "Set Offer Type: " + SROfferType.DIRECT);
            }
        });
        final List<SROffer> list = messages;
        dpayoffer.setEndElementListener(new EndElementListener() {
            public void end() {
                list.add(OffersXMLParser.this.currentSROffer);
                OffersXMLParser.this.currentSROffer = new SROffer(SRParams.currency, "0", SRParams.help);
            }
        });
        dpayoffer.getChild("name").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setName(body);
                Log.i("SR", "Set Offer with Name of " + body);
            }
        });
        dpayoffer.getChild(SROffer.DESCRIPTION).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setDescription(body);
                Log.i("SR", "Set Offer with Description of " + body);
            }
        });
        dpayoffer.getChild(SROffer.SHORT_DESCRIPTION).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setShortDescription(body);
                Log.i("SR", "Set Offer with short Description of " + body);
            }
        });
        dpayoffer.getChild(SROffer.LONG_DESCRIPTION).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setLongDescription(body);
                Log.i("SR", "Set Offer with long Description of " + body);
            }
        });
        dpayoffer.getChild(SROffer.GEO_AVAILABILITY).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setGeoAvailability(body);
                Log.i("SR", "Set Offer with geo availability of " + body);
            }
        });
        dpayoffer.getChild(SROffer.CLICK_URL).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setUrl(body);
                Log.i("SR", "Set Offer with URL of " + body);
            }
        });
        dpayoffer.getChild("image").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setImage(body);
                Log.i("SR", "Set Offer with Image of " + body);
            }
        });
        Element pricePoints = dpayoffer.getChild(SROffer.PRICE_POINTS);
        this.currentSRPricePoint = new SRPricePoint();
        Element pricePoint = pricePoints.getChild(SRPricePoint.PRICE);
        pricePoint.setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                OffersXMLParser.this.currentSRPricePoint.setAmount(arg0.getValue(SRPricePoint.AMOUNT));
                Log.i("SR", "Set Amount: " + arg0.getValue(SRPricePoint.AMOUNT));
                OffersXMLParser.this.currentSRPricePoint.setPoints(arg0.getValue(SRPricePoint.POINTS));
                Log.i("SR", "Set Points: " + arg0.getValue(SRPricePoint.POINTS));
                OffersXMLParser.this.currentSRPricePoint.setUsAmount(arg0.getValue(SRPricePoint.US_AMOUNT));
                Log.i("SR", "Set US Amount: " + arg0.getValue(SRPricePoint.US_AMOUNT));
                OffersXMLParser.this.currentSRPricePoint.setLocalAmount(arg0.getValue(SRPricePoint.LOCAL_AMOUNT));
                Log.i("SR", "Set LocalAmount: " + arg0.getValue(SRPricePoint.LOCAL_AMOUNT));
                OffersXMLParser.this.currentSRPricePoint.setUsText(arg0.getValue(SRPricePoint.US_TEXT));
                Log.i("SR", "Set US Text: " + arg0.getValue(SRPricePoint.US_TEXT));
                OffersXMLParser.this.currentSRPricePoint.setLocalText(arg0.getValue(SRPricePoint.LOCAL_TEXT));
                Log.i("SR", "Set local Text: " + arg0.getValue(SRPricePoint.LOCAL_TEXT));
                if (arg0.getValue(SRPricePoint.ENTRYPOINTURL) != null) {
                    OffersXMLParser.this.currentSRPricePoint.setEntrypointurl(arg0.getValue(SRPricePoint.ENTRYPOINTURL));
                    Log.i("SR", "Set Entrypointurl: " + arg0.getValue(SRPricePoint.ENTRYPOINTURL));
                }
            }
        });
        pricePoint.setEndElementListener(new EndElementListener() {
            public void end() {
                if (OffersXMLParser.this.currentSROffer.getPricePoints() == null) {
                    OffersXMLParser.this.currentSROffer.setPricePoints(new ArrayList());
                }
                OffersXMLParser.this.currentSROffer.getPricePoints().add(OffersXMLParser.this.currentSRPricePoint);
                OffersXMLParser.this.currentSRPricePoint = new SRPricePoint();
            }
        });
        Element otheroffer = rootElement.getChild(SROffer.OFFERS);
        Element testoffer = otheroffer.getChild(SROffer.TESTOFFER);
        testoffer.setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                OffersXMLParser.this.currentSROffer.setId(arg0.getValue("id").toString());
                Log.i("SR", "Set Test Offer with ID of " + arg0.getValue("id").toString());
                OffersXMLParser.this.currentSROffer.setOfferType(SROfferType.OFFER);
                Log.i("SR", "Set Offer Type: " + arg0.getValue(SROffer.OFFER_TYPE));
            }
        });
        final List<SROffer> list2 = messages;
        testoffer.setEndElementListener(new EndElementListener() {
            public void end() {
                OffersXMLParser.this.currentSROffer.setFreeImage();
                list2.add(OffersXMLParser.this.currentSROffer);
                OffersXMLParser.this.currentSROffer = new SROffer(SRParams.currency, "0", SRParams.help);
            }
        });
        testoffer.getChild(SROffer.TITLE).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setName(body);
                Log.i("SR", "Set Offer with Name of " + body);
            }
        });
        testoffer.getChild(SROffer.DESCRIPTION).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setDescription(body);
                Log.i("SR", "Set Offer with Description of " + body);
            }
        });
        testoffer.getChild(SROffer.REQUIREMENTS).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setRequirements(body);
                Log.i("SR", "Set Offer with Requirements of " + body);
            }
        });
        testoffer.getChild(SROffer.PAYOUT).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setPayout(body);
                Log.i("SR", "Set Offer with Payout of " + body);
            }
        });
        testoffer.getChild(SROffer.CLICK_URL).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setUrl(body);
                Log.i("SR", "Set Offer with Url of " + body);
            }
        });
        testoffer.getChild("image").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setImage(body);
                Log.i("SR", "Set Offer with Image of " + body);
            }
        });
        Element offer = otheroffer.getChild(SROffer.OFFER);
        offer.setStartElementListener(new StartElementListener() {
            public void start(Attributes arg0) {
                OffersXMLParser.this.currentSROffer.setId(arg0.getValue("id").toString());
                Log.i("SR", "Set Offer with ID of " + arg0.getValue("id").toString());
                if (arg0.getValue(SROffer.OFFER_TYPE).equals(SROffer.OFFER)) {
                    OffersXMLParser.this.currentSROffer.setOfferType(SROfferType.OFFER);
                    Log.i("SR", "Set Offer Type: " + arg0.getValue(SROffer.OFFER_TYPE));
                } else if (arg0.getValue(SROffer.OFFER_TYPE).equals("install")) {
                    OffersXMLParser.this.currentSROffer.setOfferType(SROfferType.INSTALL);
                    Log.i("SR", "Set Offer Type: " + arg0.getValue(SROffer.OFFER_TYPE));
                } else {
                    OffersXMLParser.this.currentSROffer.setOfferType(SROfferType.OTHER);
                }
            }
        });
        final List<SROffer> list3 = messages;
        offer.setEndElementListener(new EndElementListener() {
            public void end() {
                list3.add(OffersXMLParser.this.currentSROffer);
                OffersXMLParser.this.currentSROffer = new SROffer(SRParams.currency, "0", SRParams.help);
            }
        });
        offer.getChild(SROffer.TITLE).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setName(body);
                Log.i("SR", "Set Offer with Name of " + body);
            }
        });
        offer.getChild(SROffer.DESCRIPTION).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setDescription(body);
                Log.i("SR", "Set Offer with Description of " + body);
            }
        });
        offer.getChild(SROffer.REQUIREMENTS).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setRequirements(body);
                Log.i("SR", "Set Offer with Requirements of " + body);
            }
        });
        offer.getChild(SROffer.PAYOUT).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setPayout(body);
                Log.i("SR", "Set Offer with Payout of " + body);
            }
        });
        offer.getChild(SROffer.CLICK_URL).setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setUrl(body);
                Log.i("SR", "Set Offer with Url of " + body);
            }
        });
        offer.getChild("image").setEndTextElementListener(new EndTextElementListener() {
            public void end(String body) {
                OffersXMLParser.this.currentSROffer.setImage(body);
                Log.i("SR", "Set Offer with Image of " + body);
            }
        });
        try {
            Xml.parse(xml, rootElement.getContentHandler());
            return messages;
        } catch (Exception e) {
            Exception e2 = e;
            Log.i("SR", e2.getMessage());
            throw new RuntimeException(e2);
        }
    }
}
