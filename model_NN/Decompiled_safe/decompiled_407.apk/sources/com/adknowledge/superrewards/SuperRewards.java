package com.adknowledge.superrewards;

import android.app.Activity;
import android.content.Context;
import com.adknowledge.superrewards.model.SROffer;
import java.util.List;

public interface SuperRewards {
    List<SROffer> getOffers(String str, Context context);

    List<SROffer> getOffers(String str, String str2);

    List<SROffer> getOffers(String str, String str2, String str3, String str4, String str5, String str6, String str7, Context context);

    void showOffers(Activity activity, String str);

    void showOffers(Activity activity, String str, String str2);

    void showOffers(Activity activity, String str, String str2, String str3);
}
