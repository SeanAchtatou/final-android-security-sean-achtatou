package com.adknowledge.superrewards.model;

public enum SROfferType {
    OFFER("Offer"),
    INSTALL("install"),
    DIRECT("Direct"),
    OTHER("Other"),
    MOBILE("Mobile"),
    PARAMS("Params"),
    LOCALRATE("Localrate");
    
    private String displayName;

    private SROfferType(String displayName2) {
        this.displayName = displayName2;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
