package com.adknowledge.superrewards.model;

public enum SRType {
    FREE("Free"),
    PAY("Pay"),
    MOBILE("Mobile"),
    OTHER("Other");
    
    private String displayName;

    private SRType(String displayName2) {
        this.displayName = displayName2;
    }

    public String getDisplayName() {
        return this.displayName;
    }
}
