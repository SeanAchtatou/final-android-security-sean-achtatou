package com.adknowledge.superrewards.ui.activities;

import android.app.ProgressDialog;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TextView;
import com.adknowledge.superrewards.SRResources;
import com.adknowledge.superrewards.SuperRewardsImpl;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.model.SRImageByName;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.model.SROfferType;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.model.SRRequestBuilder;
import com.adknowledge.superrewards.tracking.SRAppInstallTracker;
import com.adknowledge.superrewards.ui.adapter.SRDirectPaymentAdapter;
import com.adknowledge.superrewards.ui.adapter.SRInstallPaymentAdapter;
import com.adknowledge.superrewards.ui.adapter.SROfferPaymentAdapter;
import com.adknowledge.superrewards.ui.adapter.item.SRDirectPaymentItem;
import com.adknowledge.superrewards.ui.adapter.item.SROfferPaymentItem;
import com.zong.android.engine.activities.ZongPaymentRequest;
import com.zong.android.engine.web.ZongWebView;
import java.util.ArrayList;
import java.util.List;
import org.anddev.andengine.level.util.constants.LevelConstants;

public class SRPaymentMethodsActivity extends TabActivity {
    /* access modifiers changed from: private */
    public List<SROffer> directPaymentList;
    /* access modifiers changed from: private */
    public List<SROffer> installPaymentList;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mDirectTabSpec;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public TabHost.TabSpec mInstallTabSpec;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mOfferTabSpec;
    /* access modifiers changed from: private */
    public TabHost.TabSpec mSupportTabSpec;
    /* access modifiers changed from: private */
    public ProgressDialog myProgressDialog;
    /* access modifiers changed from: private */
    public List<SROffer> offerPaymentList;
    /* access modifiers changed from: private */
    public List<SROffer> paymentMethodList;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("SR", "Setting up Payment Methods");
        final boolean customTitleSupported = requestWindowFeature(7);
        setContentView(SRResources.layout.sr_payment_methods_activity_layout);
        Bundle extras = getIntent().getExtras();
        final String h = extras.getString("h");
        final String uid = extras.getString(LevelConstants.TAG_LEVEL_ATTRIBUTE_UID);
        final String cc = extras.getString("cc");
        final String xml = extras.getString("xml");
        final String nOffers = extras.getString("nOffers");
        String string = extras.getString("mode");
        final SROfferPaymentAdapter sROfferPaymentAdapter = new SROfferPaymentAdapter(this);
        final SRDirectPaymentAdapter sRDirectPaymentAdapter = new SRDirectPaymentAdapter(this);
        final SRInstallPaymentAdapter sRInstallPaymentAdapter = new SRInstallPaymentAdapter(this);
        ListView directListView = (ListView) findViewById(SRResources.id.SRPaymentMethodsActivityDirectListView);
        directListView.setAdapter((ListAdapter) sRDirectPaymentAdapter);
        ((ListView) findViewById(SRResources.id.SRPaymentMethodsActivityOfferListView)).setAdapter((ListAdapter) sROfferPaymentAdapter);
        ((ListView) findViewById(SRResources.id.SRPaymentMethodsActivityInstallListView)).setAdapter((ListAdapter) sRInstallPaymentAdapter);
        if (!Utils.checkRunFlag(getApplicationContext())) {
            SRAppInstallTracker.getInstance(getApplicationContext(), h).track();
        }
        this.myProgressDialog = ProgressDialog.show(this, "Please wait...", "Loading data from Super Rewards", false);
        final TabHost tabHost = getTabHost();
        tabHost.setup();
        this.mInstallTabSpec = tabHost.newTabSpec("INSTALL").setIndicator("install", getResources().getDrawable(SRResources.drawable.sr_install_tab_icon)).setContent(SRResources.id.SRPaymentMethodsActivityInstallListView);
        tabHost.addTab(this.mInstallTabSpec);
        this.mDirectTabSpec = tabHost.newTabSpec("DIRECT").setIndicator("buy", getResources().getDrawable(SRResources.drawable.sr_buy_tab_icon)).setContent(SRResources.id.SRPaymentMethodsActivityDirectListView);
        tabHost.addTab(this.mDirectTabSpec);
        this.mOfferTabSpec = tabHost.newTabSpec("OFFER").setIndicator("earn", getResources().getDrawable(SRResources.drawable.sr_earn_tab_icon)).setContent(SRResources.id.SRPaymentMethodsActivityOfferListView);
        tabHost.addTab(this.mOfferTabSpec);
        new Thread(new Runnable() {
            public void run() {
                Log.i("SR", "In Payment Methods parent thread");
                SRPaymentMethodsActivity.this.paymentMethodList = new SuperRewardsImpl().getOffers(h, uid, cc, xml, SRParams.v, nOffers, null, SRPaymentMethodsActivity.this.getApplicationContext());
                SRPaymentMethodsActivity.this.directPaymentList = SRPaymentMethodsActivity.this.filterOffers(SRPaymentMethodsActivity.this.paymentMethodList, SROfferType.DIRECT);
                SRPaymentMethodsActivity.this.offerPaymentList = SRPaymentMethodsActivity.this.filterOffers(SRPaymentMethodsActivity.this.paymentMethodList, SROfferType.OFFER);
                SRPaymentMethodsActivity.this.installPaymentList = SRPaymentMethodsActivity.this.filterOffers(SRPaymentMethodsActivity.this.paymentMethodList, SROfferType.INSTALL);
                final List<SRDirectPaymentItem> directItemList = new ArrayList<>(SRPaymentMethodsActivity.this.directPaymentList.size());
                final List<SROfferPaymentItem> offerItemList = new ArrayList<>(SRPaymentMethodsActivity.this.offerPaymentList.size());
                final List<SROfferPaymentItem> installItemList = new ArrayList<>(SRPaymentMethodsActivity.this.installPaymentList.size());
                for (SROffer srDirect : SRPaymentMethodsActivity.this.directPaymentList) {
                    if (SRImageByName.getDirectPayImageByName(srDirect.getName()) != 0) {
                        directItemList.add(new SRDirectPaymentItem(SRPaymentMethodsActivity.this.getResources().getDrawable(SRImageByName.getDirectPayImageByName(srDirect.getName())), srDirect.getName()));
                    } else {
                        directItemList.add(new SRDirectPaymentItem(Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srDirect.getIcon()), srDirect.getName()));
                    }
                    Log.i("SR", "Set Direct Payment Offer: " + srDirect.getName());
                }
                for (SROffer srOffer : SRPaymentMethodsActivity.this.offerPaymentList) {
                    offerItemList.add(new SROfferPaymentItem(Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer.getIcon()), Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer.getImage()), srOffer));
                    Log.i("SR", "Set Offer: " + srOffer.getName());
                }
                for (SROffer srOffer2 : SRPaymentMethodsActivity.this.installPaymentList) {
                    installItemList.add(new SROfferPaymentItem(Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer2.getIcon()), Utils.getImage(SRPaymentMethodsActivity.this.getApplicationContext(), srOffer2.getImage()), srOffer2));
                    Log.i("SR", "Set Offer: " + srOffer2.getName());
                }
                Handler access$9 = SRPaymentMethodsActivity.this.mHandler;
                final boolean z = customTitleSupported;
                final TabHost tabHost = tabHost;
                final SRDirectPaymentAdapter sRDirectPaymentAdapter = sRDirectPaymentAdapter;
                final SROfferPaymentAdapter sROfferPaymentAdapter = sROfferPaymentAdapter;
                final SRInstallPaymentAdapter sRInstallPaymentAdapter = sRInstallPaymentAdapter;
                access$9.post(new Runnable() {
                    public void run() {
                        SRPaymentMethodsActivity.this.customTitleBar("get more " + SRParams.currency.toLowerCase(), false, Boolean.valueOf(z));
                        tabHost.clearAllTabs();
                        if (installItemList.size() != 0) {
                            tabHost.addTab(SRPaymentMethodsActivity.this.mInstallTabSpec);
                        }
                        if (directItemList.size() != 0) {
                            tabHost.addTab(SRPaymentMethodsActivity.this.mDirectTabSpec);
                        }
                        if (offerItemList.size() != 0) {
                            tabHost.addTab(SRPaymentMethodsActivity.this.mOfferTabSpec);
                        }
                        Intent i = new Intent(SRPaymentMethodsActivity.this.getApplicationContext(), SRWebViewActivity.class);
                        i.putExtra(SRWebViewActivity.URL, SRParams.help);
                        SRPaymentMethodsActivity.this.mSupportTabSpec = tabHost.newTabSpec("SUPPORT").setIndicator("support", SRPaymentMethodsActivity.this.getResources().getDrawable(SRResources.drawable.sr_support_tab_icon)).setContent(i);
                        tabHost.addTab(SRPaymentMethodsActivity.this.mSupportTabSpec);
                        SRPaymentMethodsActivity.this.getTabWidget().setCurrentTab(0);
                        sRDirectPaymentAdapter.setOffers(directItemList);
                        sROfferPaymentAdapter.setOffers(offerItemList);
                        sRInstallPaymentAdapter.setOffers(installItemList);
                        sRDirectPaymentAdapter.notifyDataSetChanged();
                        sROfferPaymentAdapter.notifyDataSetChanged();
                        sRInstallPaymentAdapter.notifyDataSetChanged();
                        SRPaymentMethodsActivity.this.myProgressDialog.dismiss();
                    }
                });
            }
        }).start();
        Log.i("SR", "Finished Setting up Payment Methods");
        directListView.setOnItemClickListener(getDirectPaymentOnItemClickListener());
    }

    /* access modifiers changed from: private */
    public List<SROffer> filterOffers(List<SROffer> offers, SROfferType type) {
        List<SROffer> filteredOffers = new ArrayList<>();
        for (SROffer srOffer : offers) {
            if (srOffer.getOfferType().equals(type)) {
                filteredOffers.add(srOffer);
            }
        }
        return filteredOffers;
    }

    /* access modifiers changed from: protected */
    public AdapterView.OnItemClickListener getDirectPaymentOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
             arg types: [java.lang.String, com.zong.android.engine.activities.ZongPaymentRequest]
             candidates:
              ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
              ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent} */
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                SROffer selectedOffer = (SROffer) SRPaymentMethodsActivity.this.directPaymentList.get(position);
                if (selectedOffer.getName().equalsIgnoreCase("zong")) {
                    ZongPaymentRequest paymentRequest = new SRRequestBuilder(SRPaymentMethodsActivity.this.getApplicationContext(), selectedOffer).getRequest();
                    Intent intent = new Intent(SRPaymentMethodsActivity.this.getApplicationContext(), ZongWebView.class);
                    intent.putExtra("com.zong.intent.Request", (Parcelable) paymentRequest);
                    SRPaymentMethodsActivity.this.startActivityForResult(intent, 1);
                    return;
                }
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.putExtra(SROffer.OFFER, selectedOffer);
                intent2.setClassName(arg0.getContext(), SRDirectPaymentActivity.class.getName());
                SRPaymentMethodsActivity.this.startActivity(intent2);
            }
        };
    }

    public void customTitleBar(String blurb, boolean spinner, Boolean customTitleSupported) {
        if (customTitleSupported.booleanValue()) {
            getWindow().setFeatureInt(7, SRResources.layout.sr_custom_title);
            TextView SRCustomTitleLeft = (TextView) findViewById(SRResources.id.SRCustomTitleLeft);
            SRCustomTitleLeft.setText(blurb);
            ProgressBar titleProgressBar = (ProgressBar) findViewById(SRResources.id.SRTitleProgressBar);
            titleProgressBar.setVisibility(8);
            if (spinner) {
                SRCustomTitleLeft.setText("Loading...");
                titleProgressBar.setVisibility(0);
            }
        }
    }
}
