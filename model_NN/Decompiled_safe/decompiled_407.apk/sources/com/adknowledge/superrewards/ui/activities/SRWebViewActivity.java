package com.adknowledge.superrewards.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class SRWebViewActivity extends Activity {
    public static final String URL = "http://www.srpoints.com";
    private WebView browser;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        this.browser = new WebView(this);
        setContentView(this.browser);
        this.browser.getSettings().setJavaScriptEnabled(true);
        this.browser.getSettings().setSupportZoom(true);
        this.browser.getSettings().setBuiltInZoomControls(true);
        this.browser.setInitialScale(1);
        this.browser.loadUrl(getIntent().getStringExtra(URL));
    }
}
