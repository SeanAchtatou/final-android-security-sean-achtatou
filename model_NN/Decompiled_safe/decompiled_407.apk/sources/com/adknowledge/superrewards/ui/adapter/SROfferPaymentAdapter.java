package com.adknowledge.superrewards.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.adknowledge.superrewards.SRResources;
import com.adknowledge.superrewards.model.SROffer;
import com.adknowledge.superrewards.ui.activities.SROfferPaymentActivity;
import com.adknowledge.superrewards.ui.adapter.item.SROfferPaymentItem;
import java.util.ArrayList;
import java.util.List;

public class SROfferPaymentAdapter extends BaseAdapter {
    /* access modifiers changed from: private */
    public Context context;
    private List<SROfferPaymentItem> offers = new ArrayList();

    public SROfferPaymentAdapter() {
    }

    public SROfferPaymentAdapter(Context context2) {
        this.context = context2;
    }

    public List<SROfferPaymentItem> getOffers() {
        return this.offers;
    }

    public void setOffers(List<SROfferPaymentItem> offers2) {
        this.offers = offers2;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public int getCount() {
        return this.offers.size();
    }

    public SROfferPaymentItem getItem(int position) {
        try {
            return this.offers.get(position);
        } catch (Exception e) {
            return null;
        }
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(SRResources.layout.sr_list_offer_item_template, (ViewGroup) null);
        }
        SROfferPaymentItem srOfferItem = getItem(position);
        if (srOfferItem != null) {
            TextView srTextView = (TextView) v.findViewById(SRResources.id.SROfferListItemTextView);
            TextView srTextViewPoints = (TextView) v.findViewById(SRResources.id.SROfferListItemTextViewPoints);
            ImageView srImageView = (ImageView) v.findViewById(SRResources.id.SROfferListItemImageView);
            TextView srTextViewCurrency = (TextView) v.findViewById(SRResources.id.SROfferListItemTextViewCurrency);
            TextView srTextViewDescription = (TextView) v.findViewById(SRResources.id.SROfferPaymentDescription);
            RelativeLayout relativelayout = (RelativeLayout) v.findViewById(SRResources.id.SROfferList);
            if (srTextView != null) {
                srTextView.setText(Html.fromHtml(srOfferItem.getOffer().getName().trim()));
            }
            if (srTextViewPoints != null) {
                srTextViewPoints.setText(srOfferItem.getOffer().getPayout());
            }
            if (srTextViewCurrency != null) {
                srTextViewCurrency.setText(srOfferItem.getOffer().getCurrency());
            }
            if (srImageView != null) {
                srImageView.setImageDrawable(srOfferItem.getImage());
            }
            if (srTextViewDescription != null) {
                String StringDescription = Html.fromHtml(srOfferItem.getOffer().getDescription()).toString().trim();
                if (StringDescription.length() > 55) {
                    StringDescription = String.valueOf(StringDescription.substring(0, 55)) + "(...)";
                }
                srTextViewDescription.setText(StringDescription);
            }
            relativelayout.setOnClickListener(getClickListener(srOfferItem.getOffer()));
        }
        return v;
    }

    private View.OnClickListener getClickListener(final SROffer srOffer) {
        return new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.putExtra(SROffer.OFFER, srOffer);
                intent.setClassName(SROfferPaymentAdapter.this.context, SROfferPaymentActivity.class.getName());
                SROfferPaymentAdapter.this.context.startActivity(intent);
            }
        };
    }
}
