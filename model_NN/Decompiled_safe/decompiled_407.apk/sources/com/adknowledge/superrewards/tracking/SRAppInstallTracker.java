package com.adknowledge.superrewards.tracking;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import com.adknowledge.superrewards.Utils;
import com.adknowledge.superrewards.model.SRParams;
import com.adknowledge.superrewards.web.SRClient;
import com.adknowledge.superrewards.web.SRRequest;
import java.util.HashMap;
import java.util.Locale;

public class SRAppInstallTracker {
    public static String h;
    private static SRAppInstallTracker instance = null;
    public static HashMap<String, String> map = new HashMap<>();
    String androidID;
    String countryCode;
    Context ctx;
    String deviceBuildModel;
    String deviceID;
    String deviceOSBuildVersion;
    String hparam;
    Boolean isSet = false;
    String qs;
    String simSerial;
    boolean toast = false;

    public static SRAppInstallTracker getInstance(Context context, String h2) {
        if (instance == null) {
            instance = new SRAppInstallTracker(context, h2);
        }
        return instance;
    }

    protected SRAppInstallTracker(Context context, String h2) {
        this.ctx = context;
        this.hparam = h2;
        TelephonyManager tm = (TelephonyManager) this.ctx.getSystemService("phone");
        this.deviceID = tm.getDeviceId();
        this.simSerial = tm.getSimSerialNumber();
        this.deviceBuildModel = Build.MODEL;
        this.androidID = Settings.Secure.getString(this.ctx.getContentResolver(), "android_id");
        this.deviceOSBuildVersion = Build.VERSION.RELEASE;
        this.countryCode = Locale.getDefault().getCountry();
        map = getDeviceParams();
        setHparam(this.hparam);
        this.isSet = true;
    }

    public static Void setHparam(String h2) {
        h = h2;
        return null;
    }

    public boolean hasRunBefore() {
        return Utils.checkRunFlag(this.ctx);
    }

    public void setShowToast(boolean show) {
        this.toast = show;
    }

    public boolean getShowToast() {
        return this.toast;
    }

    public void track(boolean showToast) {
        if (!hasRunBefore()) {
            setShowToast(showToast);
            new AppInstallTask(this, null).execute(new Void[0]);
            Utils.setRunFlag(this.ctx);
        }
    }

    public void track() {
        if (!hasRunBefore()) {
            setShowToast(false);
            new AppInstallTask(this, null).execute(new Void[0]);
            Utils.setRunFlag(this.ctx);
        }
    }

    private class AppInstallTask extends AsyncTask<Void, Void, String> {
        private AppInstallTask() {
        }

        /* synthetic */ AppInstallTask(SRAppInstallTracker sRAppInstallTracker, AppInstallTask appInstallTask) {
            this();
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Void... params) {
            SRRequest request = SRClient.getInstance().createRequest();
            request.setCommand(SRRequest.Command.METHOD);
            request.setCall(SRRequest.Call.INSTALL);
            request.execute(SRAppInstallTracker.this.ctx, SRAppInstallTracker.this.hparam);
            Log.i("SR", "Result is " + request.getResult());
            return request.getResult();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String result) {
            if (SRAppInstallTracker.this.getShowToast()) {
                Toast.makeText(SRAppInstallTracker.this.ctx, result, 1).show();
            }
        }
    }

    public void set() {
        TelephonyManager tm = (TelephonyManager) this.ctx.getSystemService("phone");
        this.deviceID = tm.getDeviceId();
        this.simSerial = tm.getSimSerialNumber();
        this.deviceBuildModel = Build.MODEL;
        this.androidID = Settings.Secure.getString(this.ctx.getContentResolver(), "android_id");
        this.deviceOSBuildVersion = Build.VERSION.RELEASE;
        this.countryCode = Locale.getDefault().getCountry();
        map = getDeviceParams();
        this.isSet = true;
    }

    public HashMap<String, String> getDeviceParams() {
        HashMap<String, String> SRMap = new HashMap<>();
        SRMap.put("type", "android");
        SRMap.put("device_id", this.deviceID);
        SRMap.put("alt_id", this.androidID);
        SRMap.put("model", this.deviceBuildModel);
        SRMap.put("os", "Android");
        SRMap.put("os_version", this.deviceOSBuildVersion);
        SRMap.put("cc", this.countryCode);
        SRMap.put("ip", Utils.getLocalIpAddress());
        SRMap.put("h", this.hparam);
        SRMap.put("sim_serial", this.simSerial);
        SRMap.put("version", SRParams.version_number);
        return SRMap;
    }
}
