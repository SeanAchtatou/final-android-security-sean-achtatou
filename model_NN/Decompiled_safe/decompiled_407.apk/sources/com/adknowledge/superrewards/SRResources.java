package com.adknowledge.superrewards;

public class SRResources {

    public static class attr {
    }

    public static class drawable {
        public static int sr_buy_tab_icon;
        public static int sr_creditcard_directpay_logo;
        public static int sr_earn_tab_icon;
        public static int sr_free_offer_image;
        public static int sr_googlecheckout_directpay_logo;
        public static int sr_install_tab_icon;
        public static int sr_list_item_background;
        public static int sr_logo;
        public static int sr_next_button;
        public static int sr_paypal_directpay_logo;
        public static int sr_points_green_button;
        public static int sr_purchase_offer_image;
        public static int sr_support_tab_icon;
        public static int sr_zong_directpay_logo;
    }

    public static class id {
        public static int SRCustomTitleLayout;
        public static int SRCustomTitleLeft;
        public static int SRDirectListItemImageView;
        public static int SRDirectListItemTextView;
        public static int SRDirectPaymentBuyButton;
        public static int SRDirectPaymentDescription;
        public static int SRDirectPaymentLargeIcon;
        public static int SRDirectPaymentName;
        public static int SRDirectPaymentPricePoints;
        public static int SRHeaderPathTextViewFirst;
        public static int SRHeaderPathTextViewSecond;
        public static int SROfferIcon;
        public static int SROfferList;
        public static int SROfferListItemButton;
        public static int SROfferListItemImageView;
        public static int SROfferListItemTextView;
        public static int SROfferListItemTextViewCurrency;
        public static int SROfferListItemTextViewPoints;
        public static int SROfferName;
        public static int SROfferPaymentBuyButton;
        public static int SROfferPaymentDescription;
        public static int SROfferPaymentDescriptionScrollView;
        public static int SROfferPaymentRequirements;
        public static int SROfferPaymentRequirementsScrollView;
        public static int SRPaymentMethodsActivityDirectListView;
        public static int SRPaymentMethodsActivityInstallListView;
        public static int SRPaymentMethodsActivityOfferListView;
        public static int SRPaymentMethodsCloseButton;
        public static int SRPaymentMethodsHeader;
        public static int SRSupport;
        public static int SRSupportTextView;
        public static int SRTitleProgressBar;
        public static int SRUpperContainer;
        public static int SRWebView;
        public static int SRlogo;
    }

    public static class layout {
        public static int sr_custom_title;
        public static int sr_direct_payment_activity_layout;
        public static int sr_list_direct_item_template;
        public static int sr_list_offer_item_template;
        public static int sr_offer_payment_activity_layout;
        public static int sr_payment_methods_activity_layout;
        public static int sr_support_layout;
        public static int sr_tab_indicator_layout;
        public static int sr_webview_layout;
    }
}
