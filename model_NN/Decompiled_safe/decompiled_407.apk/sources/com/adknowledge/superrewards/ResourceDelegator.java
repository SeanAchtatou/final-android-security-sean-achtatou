package com.adknowledge.superrewards;

import java.lang.reflect.Field;

public class ResourceDelegator {
    public static void delegateValues(Class<?> fromClass, Class<?> toClass) {
        for (Class<?> cls : toClass.getClasses()) {
            for (Class<?> cls2 : fromClass.getClasses()) {
                if (cls.getSimpleName().equals(cls2.getSimpleName())) {
                    for (Field toSubClassField : cls.getDeclaredFields()) {
                        Field fromSubClassField = null;
                        try {
                            fromSubClassField = cls2.getField(toSubClassField.getName());
                        } catch (NoSuchFieldException | SecurityException e) {
                        }
                        if (fromSubClassField != null && fromSubClassField.getType().equals(toSubClassField.getType())) {
                            try {
                                toSubClassField.set(toSubClassField, fromSubClassField.get(cls2));
                            } catch (IllegalAccessException | IllegalArgumentException e2) {
                            }
                        }
                    }
                }
            }
        }
    }
}
