package net.youmi.android;

import android.app.Activity;
import com.finger2finger.games.common.CommonConst;
import com.finger2finger.games.res.Const;
import java.util.Date;
import org.anddev.andengine.entity.layer.tiled.tmx.util.constants.TMXConstants;
import org.json.JSONObject;

class a {
    static long a = 0;
    static long b = 0;
    static boolean c = false;
    static ax d;

    a() {
    }

    static String a(Activity activity, int i, String str) {
        try {
            StringBuilder sb = new StringBuilder(512);
            sb.append("adid=");
            sb.append(i);
            t.a(sb, "sd", str);
            String str2 = "0";
            if (e.i(activity)) {
                str2 = "1";
            }
            t.a(sb, "ts", str2);
            Date date = new Date(System.currentTimeMillis());
            t.a(sb, activity, date);
            t.a(sb, "sig", as.c(sb.toString(), String.valueOf(n.c()) + t.a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int a2 = ag.a(Integer.MAX_VALUE);
            return String.valueOf(ar.a(sb2, a2)) + "&k=" + a2;
        } catch (Exception e) {
            return "";
        }
    }

    static String a(Activity activity, int i, String str, int i2) {
        try {
            StringBuilder sb = new StringBuilder(512);
            sb.append("adid=");
            sb.append(i);
            t.a(sb, "et", new StringBuilder(String.valueOf(i2)).toString());
            t.a(sb, "eid", str);
            String str2 = "0";
            if (e.i(activity)) {
                str2 = "1";
            }
            t.a(sb, "ts", str2);
            Date date = new Date(System.currentTimeMillis());
            t.a(sb, activity, date);
            t.a(sb, "sig", as.c(sb.toString(), String.valueOf(n.c()) + t.a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int a2 = ag.a(Integer.MAX_VALUE);
            return String.valueOf(ar.a(sb2, a2)) + "&k=" + a2;
        } catch (Exception e) {
            return "";
        }
    }

    static String a(Activity activity, AdView adView) {
        try {
            StringBuilder sb = new StringBuilder(512);
            sb.append("adw=");
            int adWidth = adView.getAdWidth();
            if (adWidth <= 0) {
                adWidth = adView.a().a();
            }
            sb.append(adWidth);
            t.a(sb, "adh", new StringBuilder(String.valueOf(adView.getAdHeight())).toString());
            String str = "0";
            if (e.i(activity)) {
                str = "1";
            }
            t.a(sb, "ts", str);
            Date date = new Date(System.currentTimeMillis());
            t.a(sb, activity, date);
            t.a(sb, "sig", as.c(sb.toString(), String.valueOf(n.c()) + t.a().substring(0, (date.getSeconds() % 16) + 1)));
            String sb2 = sb.toString();
            int a2 = ag.a(Integer.MAX_VALUE);
            return String.valueOf(ar.a(sb2, a2)) + "&k=" + a2;
        } catch (Exception e) {
            return "";
        }
    }

    static z a(Activity activity, String str) {
        if (str == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            int a2 = bd.a(bd.a(jSONObject, "result", (JSONObject) null), "code", -999);
            if (a2 == -999) {
                g.b("Unable to connect to the server, please check your network configuration!");
                return null;
            }
            g.a(activity, a2, 3);
            if (a2 == 0) {
                return z.a(bd.a(jSONObject, TMXConstants.TAG_DATA, (JSONObject) null));
            }
            return null;
        } catch (Exception e) {
        }
    }

    static void a(AdView adView) {
        try {
            if (d != null) {
                adView.a(d);
            }
        } catch (Exception e) {
        }
    }

    static boolean a() {
        if (!c) {
            return false;
        }
        if (System.currentTimeMillis() - b < CommonConst.UPDATE_USER_POINT_DURATION) {
            return true;
        }
        c = false;
        return false;
    }

    static ax b(Activity activity, AdView adView) {
        try {
            JSONObject d2 = cg.d(activity, t.a(cr.b(), a(activity, adView)));
            int a2 = bd.a(bd.a(d2, "result", (JSONObject) null), "code", -999);
            if (a2 == -999) {
                g.b("Unable to connect to the server, please check your network configuration!");
                return null;
            }
            g.a(activity, a2, 1);
            JSONObject a3 = bd.a(d2, TMXConstants.TAG_DATA, (JSONObject) null);
            if (a3 == null) {
                return null;
            }
            int a4 = bd.a(a3, "adid", -1);
            String a5 = bd.a(a3, "sd", (String) null);
            String a6 = bd.a(a3, "text", "");
            String a7 = bd.a(a3, "img", "");
            String a8 = bd.a(a3, "url", "");
            int a9 = bd.a(a3, "type", 1);
            int a10 = bd.a(a3, "opentype", 1);
            ch.a(bd.a(a3, "rqivsec", 30));
            ax axVar = new ax();
            if (!axVar.a(activity, a4, a5, a6, a8, a7, a9, a10)) {
                return null;
            }
            return axVar;
        } catch (Exception e) {
        }
    }

    static void b(Activity activity, int i, String str, int i2) {
        try {
            new Thread(new x(activity, i, str, i2)).start();
        } catch (Exception e) {
        }
    }

    static boolean b() {
        return System.currentTimeMillis() >= a;
    }

    static boolean b(Activity activity, int i, String str) {
        try {
            int a2 = bd.a(bd.a(cg.d(activity, t.a(cr.c(), a(activity, i, str))), "result", (JSONObject) null), "code", -999);
            if (a2 == -999) {
                g.b("Unable to connect to the server, please check your network configuration!");
                return false;
            }
            g.a(activity, a2, 2);
            return a2 == 0;
        } catch (Exception e) {
            return false;
        }
    }

    static long c() {
        long currentTimeMillis = System.currentTimeMillis();
        if (a > currentTimeMillis) {
            return a - currentTimeMillis;
        }
        return 100;
    }

    static long c(Activity activity, AdView adView) {
        try {
            if (!b()) {
                a(adView);
                cd.a(activity);
                return c();
            } else if (a()) {
                a(adView);
                cd.a(activity);
                return d();
            } else {
                c = true;
                b = System.currentTimeMillis();
                try {
                    ax b2 = b(activity, adView);
                    if (b2 != null) {
                        d = b2;
                        a = d() + System.currentTimeMillis();
                        b2.k();
                        if (!b(activity, b2.f(), b2.e())) {
                            b2.j();
                        }
                    } else {
                        a = System.currentTimeMillis() + Const.SPEACH_INTERVAL_TIME;
                        try {
                            adView.d();
                        } catch (Exception e) {
                        }
                    }
                } catch (Exception e2) {
                }
                c = false;
                a(adView);
                cd.a(activity);
                return c();
            }
        } catch (Exception e3) {
        }
    }

    static String c(Activity activity, int i, String str) {
        try {
            return t.a(cr.e(), a(activity, i, str));
        } catch (Exception e) {
            return null;
        }
    }

    static boolean c(Activity activity, int i, String str, int i2) {
        try {
            int a2 = bd.a(bd.a(cg.d(activity, t.a(cr.d(), a(activity, i, str, i2))), "result", (JSONObject) null), "code", -999);
            if (a2 == -999) {
                g.b("Unable to connect to the server, please check your network configuration!");
                return false;
            }
            g.a(activity, a2, 4);
            return a2 == 0;
        } catch (Exception e) {
            return false;
        }
    }

    static long d() {
        long d2 = (long) h.d();
        if (n.d() > d2) {
            d2 = n.d();
        }
        return ch.a() > d2 ? ch.a() : d2;
    }

    static boolean d(Activity activity, int i, String str) {
        try {
            int a2 = bd.a(bd.a(cg.d(activity, c(activity, i, str)), "result", (JSONObject) null), "code", -999);
            if (a2 == -999) {
                g.b("Unable to connect to the server, please check your network configuration!");
                return false;
            }
            g.a(activity, a2, 3);
            return a2 == 0;
        } catch (Exception e) {
            return false;
        }
    }
}
