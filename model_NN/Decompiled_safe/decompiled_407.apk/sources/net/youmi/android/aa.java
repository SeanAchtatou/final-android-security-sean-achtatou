package net.youmi.android;

import android.content.Context;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

class aa {
    aa() {
    }

    static int a(Properties properties, String str, int i) {
        try {
            String a = a(properties, str, (String) null);
            return a == null ? i : Integer.parseInt(a);
        } catch (Exception e) {
            return i;
        }
    }

    static String a(Properties properties, String str, String str2) {
        try {
            return properties.getProperty(str);
        } catch (Exception e) {
            return str2;
        }
    }

    static Properties a(Context context, String str) {
        Properties properties = new Properties();
        try {
            FileInputStream openFileInput = context.openFileInput(str);
            if (openFileInput != null) {
                properties.load(openFileInput);
            }
        } catch (Exception e) {
        }
        return properties;
    }

    static boolean a(Context context, Properties properties, String str) {
        try {
            FileOutputStream openFileOutput = context.openFileOutput(str, 0);
            if (openFileOutput != null) {
                properties.store(openFileOutput, (String) null);
                try {
                    openFileOutput.close();
                } catch (Exception e) {
                }
                return true;
            }
        } catch (Exception e2) {
        }
        return false;
    }
}
