package net.youmi.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;

class ci extends TableLayout {
    ImageView a;
    ImageView b;
    ImageView c;
    ImageView d;
    ImageView e;
    Bitmap f;
    Bitmap g;
    Bitmap h;
    Bitmap i;
    Bitmap j;
    Bitmap k;
    Bitmap l;
    Bitmap m;
    Bitmap n;
    Bitmap o;
    Bitmap p;
    Bitmap q;
    Bitmap r;
    Bitmap s;
    boolean t = false;
    boolean u = false;
    boolean v = false;
    j w;
    ak x;

    ci(Activity activity, ak akVar, j jVar) {
        super(activity);
        this.w = jVar;
        this.x = akVar;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        layoutParams2.addRule(13);
        layoutParams3.addRule(11);
        layoutParams3.addRule(15);
        this.c = new ImageView(activity);
        Bitmap c2 = c();
        if (c2 != null) {
            this.c.setImageBitmap(c2);
            layoutParams.width = this.x.a(c2.getWidth());
            layoutParams.height = this.x.a(c2.getHeight());
        }
        this.c.setOnTouchListener(new bj(this));
        this.c.setOnClickListener(new bk(this));
        this.d = new ImageView(activity);
        Bitmap f2 = f();
        if (f2 != null) {
            this.d.setImageBitmap(f());
            layoutParams3.width = f2.getWidth();
            layoutParams3.height = f2.getHeight();
            layoutParams3.width = this.x.a(layoutParams3.width);
            layoutParams3.height = this.x.a(layoutParams3.height);
        }
        this.d.setOnTouchListener(new bh(this));
        this.d.setOnClickListener(new bi(this));
        this.a = new ImageView(activity);
        Bitmap h2 = h();
        if (h2 != null) {
            this.a.setImageBitmap(h2);
            layoutParams2.width = this.x.a(h2.getWidth());
            layoutParams2.height = this.x.a(h2.getHeight());
        }
        this.a.setOnClickListener(new bo(this));
        this.a.setOnTouchListener(new bq(this));
        this.b = new ImageView(activity);
        Bitmap k2 = k();
        if (k2 != null) {
            this.b.setImageBitmap(k2);
        }
        this.b.setOnClickListener(new bm(this));
        this.b.setOnTouchListener(new bn(this));
        this.e = new ImageView(activity);
        Bitmap j2 = j();
        if (j2 != null) {
            this.e.setImageBitmap(j2);
        }
        TableRow tableRow = new TableRow(activity);
        int a2 = this.x.a(56);
        int a3 = this.x.a(39);
        TableRow.LayoutParams layoutParams4 = new TableRow.LayoutParams(a2, a3);
        layoutParams4.column = 0;
        TableRow.LayoutParams layoutParams5 = new TableRow.LayoutParams(a2, a3);
        layoutParams5.column = 0;
        TableRow.LayoutParams layoutParams6 = new TableRow.LayoutParams(a2, a3);
        layoutParams6.column = 0;
        TableRow.LayoutParams layoutParams7 = new TableRow.LayoutParams(a2, a3);
        layoutParams7.column = 0;
        TableRow.LayoutParams layoutParams8 = new TableRow.LayoutParams(a2, a3);
        layoutParams8.column = 0;
        setStretchAllColumns(true);
        tableRow.addView(this.c, layoutParams4);
        tableRow.addView(this.d, layoutParams5);
        tableRow.addView(this.a, layoutParams6);
        tableRow.addView(this.e, layoutParams7);
        tableRow.addView(this.b, layoutParams8);
        addView(tableRow);
        setGravity(1);
        setBackgroundDrawable(new BitmapDrawable(a()));
    }

    /* access modifiers changed from: package-private */
    public Bitmap a() {
        if (this.s != null) {
            return this.s;
        }
        try {
            this.s = a("a4.png");
        } catch (Exception e2) {
        }
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public Bitmap a(String str) {
        try {
            return BitmapFactory.decodeStream(getClass().getResourceAsStream(str));
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.t = z;
        if (z) {
            if (this.c != null) {
                this.c.setImageBitmap(b());
            }
        } else if (this.c != null) {
            this.c.setImageBitmap(c());
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap b() {
        if (this.f != null) {
            return this.f;
        }
        try {
            this.f = a("a11.png");
        } catch (Exception e2) {
        }
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.u = z;
        if (z) {
            if (this.d != null) {
                this.d.setImageBitmap(e());
            }
        } else if (this.d != null) {
            this.d.setImageBitmap(f());
        }
    }

    /* access modifiers changed from: package-private */
    public Bitmap c() {
        if (this.g != null) {
            return this.g;
        }
        this.g = a("a9.png");
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public Bitmap d() {
        if (this.h != null) {
            return this.h;
        }
        this.h = a("a10.png");
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public Bitmap e() {
        if (this.i != null) {
            return this.i;
        }
        this.i = a("a14.png");
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public Bitmap f() {
        if (this.k != null) {
            return this.k;
        }
        this.k = a("a12.png");
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public Bitmap g() {
        if (this.j != null) {
            return this.j;
        }
        this.j = a("a13.png");
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public Bitmap h() {
        if (this.l != null) {
            return this.l;
        }
        this.l = a("a18.png");
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public Bitmap i() {
        if (this.m != null) {
            return this.m;
        }
        this.m = a("a17.png");
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public Bitmap j() {
        if (this.r != null) {
            return this.r;
        }
        this.r = a("a19.png");
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public Bitmap k() {
        if (this.p != null) {
            return this.p;
        }
        this.p = a("a16.png");
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public Bitmap l() {
        if (this.q != null) {
            return this.q;
        }
        this.q = a("a15.png");
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public Bitmap m() {
        if (this.n != null) {
            return this.n;
        }
        this.n = a("a6.png");
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public Bitmap n() {
        if (this.o != null) {
            return this.o;
        }
        this.o = a("a5.png");
        return this.o;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            Bitmap a2 = a();
            if (a2 != null) {
                getLayoutParams().height = this.x.a(a2.getHeight());
            }
        } catch (Exception e2) {
        }
    }
}
