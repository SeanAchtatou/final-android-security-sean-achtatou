package net.youmi.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

class cx {
    String a = null;
    Activity b;
    byte[] c;
    private int d = -1;
    private int e = 0;
    private String f = "";

    cx(Activity activity) {
        this.b = activity;
    }

    /* access modifiers changed from: private */
    public void a(bp bpVar) {
        a(bpVar, 2, "加载完成");
    }

    /* access modifiers changed from: private */
    public void a(bp bpVar, int i) {
        int i2 = this.e;
        if (i > 0) {
            if (this.e + i < 100) {
                this.e += i;
            } else if (this.d == 2) {
                this.e = 100;
            } else {
                this.e = 99;
            }
        }
        if (i2 != this.e && bpVar != null) {
            try {
                bpVar.a(this);
            } catch (Exception e2) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(bp bpVar, int i, String str) {
        if (this.d != i) {
            if (i == 0 || i == 1 || i == 2 || i == -1) {
                this.f = str;
                this.d = i;
                if (this.d == 2) {
                    this.e = 100;
                }
                if (bpVar != null) {
                    try {
                        bpVar.a(this);
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    static String b(String str) {
        String lowerCase;
        int indexOf;
        int indexOf2 = str.indexOf("<meta");
        String str2 = str;
        while (indexOf2 > -1) {
            int indexOf3 = str2.substring(indexOf2).indexOf(">");
            if (indexOf2 > -1 && indexOf3 > -1 && (indexOf = (lowerCase = str2.substring(indexOf2, indexOf2 + indexOf3).toLowerCase()).indexOf("charset")) > -1) {
                return lowerCase.substring(indexOf + 7, indexOf3).replace("=", "").replace("/", "").replace("\"", "").replace("'", "").replace(" ", "");
            }
            String substring = str2.substring(indexOf3);
            str2 = substring;
            indexOf2 = substring.indexOf("<meta");
        }
        return "utf-8";
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        if (this.c != null) {
            String str2 = null;
            if (this.a == null) {
                try {
                    str2 = new String(this.c, "UTF-8");
                } catch (Exception e2) {
                }
                if (str2 == null) {
                    return new String(this.c);
                }
                this.a = b(str2);
                if (this.a != null) {
                    this.a = this.a.trim().toLowerCase();
                }
                if (this.a.equals("utf-8")) {
                    return str2;
                }
            }
            try {
                return new String(this.c, this.a);
            } catch (Exception e3) {
            }
        }
        return str;
    }

    /* access modifiers changed from: package-private */
    public void a(HttpURLConnection httpURLConnection) {
        int indexOf;
        try {
            Map<String, List<String>> headerFields = httpURLConnection.getHeaderFields();
            for (String next : headerFields.keySet()) {
                String lowerCase = headerFields.get(next).toString().toLowerCase();
                if (next != null && next.trim().equals("content-type") && (indexOf = lowerCase.indexOf("charset=")) > -1) {
                    this.a = lowerCase.substring(indexOf + 8).replace("]", "");
                    return;
                }
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(bp bpVar, String str) {
        a(bpVar, (ay) null, str);
    }

    /* access modifiers changed from: package-private */
    public void a(bp bpVar, ay ayVar, String str) {
        new Thread(new bt(this, bpVar, ayVar, str)).start();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.d == 2;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, ay ayVar) {
        InputStream inputStream;
        boolean z;
        InputStream inputStream2 = null;
        if (ayVar != null) {
            try {
                inputStream2 = ayVar.c(str);
            } catch (Exception e2) {
            }
        }
        if (inputStream2 != null) {
            inputStream = inputStream2;
            z = true;
        } else {
            HttpURLConnection a2 = cg.a(this.b, str);
            if (a2 == null) {
                return false;
            }
            try {
                a2.setRequestMethod("GET");
                a2.setDoInput(true);
                cg.a(a2);
                a2.connect();
                a(a2);
                inputStream = a2.getInputStream();
                z = false;
            } catch (Exception e3) {
                inputStream = inputStream2;
                z = false;
            }
        }
        if (inputStream != null) {
            try {
                if (inputStream.available() > 0) {
                    byte[] bArr = new byte[1024];
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    while (true) {
                        int read = inputStream.read(bArr, 0, bArr.length);
                        if (read <= 0) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr, 0, read);
                    }
                    this.c = byteArrayOutputStream.toByteArray();
                    try {
                        inputStream.close();
                    } catch (Exception e4) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (Exception e5) {
                    }
                    if (ayVar != null && !z) {
                        ayVar.a(str, this.c);
                    }
                    this.d = 2;
                    return true;
                }
            } catch (Exception e6) {
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Bitmap b() {
        if (this.c != null) {
            try {
                return BitmapFactory.decodeByteArray(this.c, 0, this.c.length);
            } catch (Exception e2) {
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public byte[] c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return this.d;
    }
}
