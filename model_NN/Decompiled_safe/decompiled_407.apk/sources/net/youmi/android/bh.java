package net.youmi.android;

import android.view.MotionEvent;
import android.view.View;

class bh implements View.OnTouchListener {
    final /* synthetic */ ci a;

    bh(ci ciVar) {
        this.a = ciVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (!this.a.u) {
                    return false;
                }
                try {
                    this.a.d.setImageBitmap(this.a.g());
                    return false;
                } catch (Exception e) {
                    return false;
                }
            default:
                try {
                    if (this.a.u) {
                        this.a.d.setImageBitmap(this.a.e());
                        return false;
                    }
                    this.a.d.setImageBitmap(this.a.f());
                    return false;
                } catch (Exception e2) {
                    return false;
                }
        }
    }
}
