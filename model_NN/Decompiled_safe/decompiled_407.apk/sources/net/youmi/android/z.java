package net.youmi.android;

import com.adknowledge.superrewards.model.SROffer;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

class z {
    private boolean a = false;
    private aw b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private ArrayList h;
    private ArrayList i;
    private ArrayList j;
    private ArrayList k;
    private at l;

    z() {
    }

    static z a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        try {
            z zVar = new z();
            zVar.c = bd.a(jSONObject, "adid", 1);
            zVar.d = bd.a(jSONObject, "header", "");
            zVar.e = bd.a(jSONObject, SROffer.TITLE, "");
            zVar.f = bd.a(jSONObject, "imgurl", "");
            zVar.g = bd.a(jSONObject, "content", "");
            JSONArray a2 = bd.a(jSONObject, "contactways", (JSONArray) null);
            if (a2 != null && a2.length() > 0) {
                for (int i2 = 0; i2 < a2.length(); i2++) {
                    JSONObject a3 = bd.a(a2, i2, (JSONObject) null);
                    if (a3 != null) {
                        av avVar = new av();
                        avVar.a(bd.a(a3, "num", (String) null));
                        if (avVar.a() != null) {
                            avVar.a(bd.a(a3, "type", 0));
                            avVar.b(bd.a(a3, "smsdf", ""));
                            if (zVar.h == null) {
                                zVar.h = new ArrayList(a2.length());
                            }
                            zVar.h.add(avVar);
                        }
                    }
                }
            }
            JSONObject a4 = bd.a(jSONObject, "extracontactways", (JSONObject) null);
            if (a4 != null) {
                if (zVar.b == null) {
                    zVar.b = new aw();
                }
                zVar.b.c(bd.a(a4, "email", (String) null));
                zVar.b.a(bd.a(a4, "msn", (String) null));
                zVar.b.b(bd.a(a4, "qq", (String) null));
                zVar.b.d(bd.a(a4, "fax", (String) null));
            }
            JSONArray a5 = bd.a(jSONObject, "locations", (JSONArray) null);
            if (a5 != null && a5.length() > 0) {
                for (int i3 = 0; i3 < a5.length(); i3++) {
                    JSONObject a6 = bd.a(a5, i3, (JSONObject) null);
                    if (a6 != null) {
                        be beVar = new be();
                        beVar.a(bd.a(a6, "lcid", ""));
                        beVar.b(bd.a(a6, "lat", ""));
                        beVar.c(bd.a(a6, "lon", ""));
                        beVar.d(bd.a(a6, SROffer.TITLE, ""));
                        beVar.e(bd.a(a6, "url", ""));
                        if (zVar.j == null) {
                            zVar.j = new ArrayList(a5.length());
                        }
                        zVar.j.add(beVar);
                    }
                }
            }
            JSONArray a7 = bd.a(jSONObject, "downloads", (JSONArray) null);
            if (a7 != null && a7.length() > 0) {
                for (int i4 = 0; i4 < a7.length(); i4++) {
                    JSONObject a8 = bd.a(a7, i4, (JSONObject) null);
                    if (a8 != null) {
                        cj cjVar = new cj();
                        cjVar.c(bd.a(a8, "len", ""));
                        cjVar.a(bd.a(a8, "dlid", ""));
                        cjVar.d(bd.a(a8, SROffer.TITLE, ""));
                        cjVar.b(bd.a(a8, "url", ""));
                        if (zVar.i == null) {
                            zVar.i = new ArrayList(a7.length());
                        }
                        zVar.i.add(cjVar);
                    }
                }
            }
            JSONArray a9 = bd.a(jSONObject, "waps", (JSONArray) null);
            if (a9 != null && a9.length() > 0) {
                for (int i5 = 0; i5 < a9.length(); i5++) {
                    JSONObject a10 = bd.a(a9, i5, (JSONObject) null);
                    if (a10 != null) {
                        co coVar = new co();
                        coVar.c(bd.a(a10, "wapid", ""));
                        coVar.b(bd.a(a10, SROffer.TITLE, ""));
                        coVar.a(bd.a(a10, "url", ""));
                        if (zVar.k == null) {
                            zVar.k = new ArrayList(a9.length());
                        }
                        zVar.k.add(coVar);
                    }
                }
            }
            JSONObject a11 = bd.a(jSONObject, "search", (JSONObject) null);
            if (a11 == null) {
                return zVar;
            }
            if (zVar.l == null) {
                zVar.l = new at();
            }
            zVar.l.c(bd.a(a11, "shid", (String) null));
            zVar.l.b(bd.a(a11, "logourl", (String) null));
            zVar.l.d(bd.a(a11, "url", (String) null));
            zVar.l.a(bd.a(a11, SROffer.TITLE, (String) null));
            zVar.l.e(bd.a(a11, "keyword", (String) null));
            return zVar;
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.d == null ? "" : this.d;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.e == null ? "" : this.e;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.g == null ? "" : this.g;
    }

    /* access modifiers changed from: package-private */
    public ArrayList e() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public ArrayList f() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public ArrayList g() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public ArrayList h() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public aw i() {
        return this.b;
    }
}
