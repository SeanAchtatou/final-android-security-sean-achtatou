package net.youmi.android;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import com.adview.util.AdViewUtil;
import com.finger2finger.games.res.Const;

class ah {
    static final int a = Color.argb(160, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION);
    static final int b = Color.argb(50, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION);
    static final int c = Color.argb(128, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION, (int) AdViewUtil.VERSION);
    static final int d = Color.argb(128, 0, 0, 0);
    static final int e = Color.argb(80, 0, 0, 0);
    static final int f = Color.argb(150, 135, 206, 250);
    private static LinearGradient g;

    ah() {
    }

    static Bitmap a(int i, int i2, int i3, int i4) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            Bitmap createBitmap2 = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
            paint.reset();
            Canvas canvas2 = new Canvas(createBitmap2);
            canvas2.drawColor(i3);
            paint.reset();
            paint.setShader(a(i2));
            Rect rect = new Rect();
            rect.top = 0;
            rect.left = 0;
            rect.bottom = i2 / 2;
            rect.right = i;
            canvas2.drawRect(rect, paint);
            paint.reset();
            paint.setColor(d);
            canvas2.drawLine(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, (float) i, Const.MOVE_LIMITED_SPEED, paint);
            paint.reset();
            paint.setColor(c);
            canvas2.drawLine(Const.MOVE_LIMITED_SPEED, 1.0f, (float) i, 1.0f, paint);
            paint.reset();
            paint.setColor(e);
            canvas2.drawLine(Const.MOVE_LIMITED_SPEED, (float) (i2 - 1), (float) i, (float) (i2 - 1), paint);
            paint.reset();
            paint.reset();
            paint.setAlpha(i4);
            canvas.drawBitmap(createBitmap2, (float) Const.MOVE_LIMITED_SPEED, (float) Const.MOVE_LIMITED_SPEED, paint);
            try {
                if (!createBitmap2.isRecycled()) {
                    createBitmap2.recycle();
                }
            } catch (Exception e2) {
            }
            return createBitmap;
        } catch (Exception e3) {
            return null;
        }
    }

    static LinearGradient a(int i) {
        if (g == null) {
            g = new LinearGradient((float) Const.MOVE_LIMITED_SPEED, (float) Const.MOVE_LIMITED_SPEED, (float) Const.MOVE_LIMITED_SPEED, 0.5f * ((float) i), a, b, Shader.TileMode.CLAMP);
        }
        return g;
    }
}
