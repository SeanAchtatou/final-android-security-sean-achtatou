package net.youmi.android;

import android.app.Activity;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import org.anddev.andengine.util.constants.TimeConstants;

class q extends RelativeLayout implements ac {
    ak a;
    Activity b;
    bs c;
    ce d;
    ImageView e;
    final /* synthetic */ bb f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q(bb bbVar, Activity activity, ak akVar, int i) {
        super(activity);
        this.f = bbVar;
        this.a = akVar;
        this.b = activity;
        a(activity, i);
    }

    private void a(Activity activity, int i) {
        if (this.c == null) {
            this.c = new bs(this.f, activity, i, this.a);
            this.c.setVisibility(0);
        }
        if (this.d == null) {
            this.d = new ce(this.f, activity, this.a, true);
            this.d.setVisibility(0);
        }
        if (this.e == null) {
            this.e = new ImageView(activity);
        }
        this.d.setId(TimeConstants.MILLISECONDSPERSECOND);
        this.c.setId(1001);
        this.e.setId(1002);
        this.e.setScaleType(ImageView.ScaleType.FIT_XY);
        RelativeLayout.LayoutParams b2 = ap.b();
        RelativeLayout.LayoutParams b3 = ap.b();
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, this.a.d());
        b2.addRule(9);
        b2.addRule(15);
        int f2 = this.a.f();
        b2.setMargins(f2, f2, f2, f2);
        layoutParams.addRule(11);
        b3.addRule(0, this.e.getId());
        b3.addRule(1, this.d.getId());
        b3.addRule(15);
        b3.setMargins(f2, 0, 0, 0);
        addView(this.e, layoutParams);
        addView(this.d, b2);
        addView(this.c, b3);
    }

    public void a() {
        setVisibility(8);
    }

    public void a(Animation animation) {
        try {
            startAnimation(animation);
        } catch (Exception e2) {
        }
    }

    public boolean a(ax axVar) {
        if (axVar == null) {
            return false;
        }
        try {
            if (axVar.d() == null) {
                return false;
            }
            if (!this.d.a(axVar)) {
                return false;
            }
            return this.c.a(axVar);
        } catch (Exception e2) {
            return false;
        }
    }

    public void b() {
        removeAllViews();
    }

    public void c() {
        this.d.c();
        this.c.c();
    }

    public void d() {
        setVisibility(0);
    }

    public void e() {
        this.d.e();
        this.c.e();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            new cx(this.b).a(new b(this), af.a, this.a.g());
        } catch (Exception e2) {
        }
    }
}
