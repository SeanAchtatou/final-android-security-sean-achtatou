package net.youmi.android;

import android.app.Activity;
import java.text.SimpleDateFormat;
import java.util.Date;

class t {
    private static String a = "5047020c035e433d015b6641755941170d03";
    private static String b = "51440503525a433d515f67472551151b055f";
    private static String c;
    private static final SimpleDateFormat d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    t() {
    }

    static String a() {
        if (c == null) {
            c = v.a(a, b);
        }
        return c;
    }

    static String a(String str, String str2) {
        int i = 32;
        if (str != null) {
            i = 32 + str.length();
        }
        if (str2 != null) {
            i += str2.length();
        }
        StringBuilder sb = new StringBuilder(i);
        sb.append(str);
        sb.append("?da=");
        sb.append(str2);
        sb.append("&src=");
        sb.append(h.a());
        return sb.toString();
    }

    private static String a(Date date) {
        return d.format(date);
    }

    static void a(StringBuilder sb, Activity activity, Date date) {
        ak a2 = l.a(activity);
        String b2 = n.b();
        String e = n.e();
        String b3 = cg.b(activity);
        String d2 = e.d(activity);
        String f = e.f(activity);
        String e2 = e.e(activity);
        String b4 = e.b();
        String e3 = e.f();
        String g = e.g(activity);
        String c2 = e.c();
        String a3 = a(date);
        String num = Integer.toString(a2.c());
        String h = e.h(activity);
        String a4 = h.a();
        String c3 = h.c();
        String num2 = Integer.toString(a2.b());
        String b5 = h.b();
        a(sb, "aid", b2);
        a(sb, "av", e);
        a(sb, "apn", b3);
        a(sb, "bd", f);
        a(sb, "cid", d2);
        a(sb, "cn", e2);
        a(sb, "dd", b4);
        a(sb, "dv", e3);
        a(sb, "ei", g);
        a(sb, "out", "0");
        a(sb, "pn", "");
        a(sb, "po", c2);
        a(sb, "rt", a3);
        a(sb, "sh", num);
        a(sb, "si", h);
        a(sb, "src", a4);
        a(sb, "sv", c3);
        a(sb, "sw", num2);
        a(sb, "ver", b5);
    }

    static void a(StringBuilder sb, String str, String str2) {
        if (sb != null && str != null) {
            try {
                sb.append("&");
                sb.append(str.trim());
                sb.append("=");
                if (str2 == null) {
                    sb.append("");
                    return;
                }
                try {
                    String b2 = as.b(str2.trim());
                    if (b2 != null) {
                        sb.append(b2);
                    }
                } catch (Exception e) {
                }
            } catch (Exception e2) {
            }
        }
    }
}
