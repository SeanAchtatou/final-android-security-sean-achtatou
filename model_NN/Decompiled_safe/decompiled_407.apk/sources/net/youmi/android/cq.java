package net.youmi.android;

import android.app.Activity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;

class cq extends RelativeLayout implements j {
    WebView a;
    ak b;
    ci c;
    int d = 1005;
    int e = 1006;
    AdActivity f;

    public cq(AdActivity adActivity, ak akVar) {
        super(adActivity);
        this.f = adActivity;
        this.b = akVar;
        a(adActivity);
    }

    public void a() {
        if (this.f != null) {
            this.f.finish();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        setBackgroundColor(-1);
        this.a = new WebView(activity);
        this.a.setId(this.d);
        this.c = new ci(activity, this.b, this);
        this.c.setId(this.e);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(2, this.c.getId());
        addView(this.a, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(12);
        addView(this.c, layoutParams2);
        a(this.a, this.c);
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView, ci ciVar) {
        WebView.enablePlatformNotifications();
        WebSettings settings = webView.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setCacheMode(1);
        settings.setAllowFileAccess(true);
        webView.setWebViewClient(new cb(this));
        webView.setDownloadListener(new cc(this));
        webView.setWebChromeClient(new ca(this));
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        try {
            if (this.a != null) {
                this.a.loadUrl(str);
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, String str3, String str4) {
        try {
            if (this.a != null) {
                this.a.loadDataWithBaseURL(str, str2, str3, str4, str);
            }
        } catch (Exception e2) {
        }
    }

    public void b() {
        if (this.a != null) {
            this.a.reload();
        }
    }

    public void c() {
        if (this.a != null && this.a.canGoBack()) {
            this.a.goBack();
            if (this.c != null) {
                this.c.a(this.a.canGoBack());
                this.c.b(this.a.canGoForward());
            }
        }
    }

    public void d() {
        if (this.a != null && this.a.canGoForward()) {
            this.a.goForward();
            if (this.c != null) {
                this.c.a(this.a.canGoBack());
                this.c.b(this.a.canGoForward());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        try {
            return this.a != null && this.a.canGoBack();
        } catch (Exception e2) {
        }
    }
}
