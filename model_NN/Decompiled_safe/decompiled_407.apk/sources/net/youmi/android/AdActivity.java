package net.youmi.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;

public class AdActivity extends Activity {
    ax a;
    cp b;
    Handler c = new Handler();
    ak d;

    private void a() {
        try {
            new Thread(new az(this)).start();
        } catch (Exception e) {
        }
    }

    static void a(Intent intent) {
        intent.putExtra("A79004B391FE457D956486F565DFC3B9", 2);
    }

    static void a(Intent intent, String str) {
        intent.putExtra("A79004B391FE457D956486F565DFC3B9", 1);
        intent.putExtra("A7D1721B9508405D8271AB82AC6D9B3C", str);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        try {
            this.d = l.a(this);
            Intent intent = getIntent();
            int intExtra = intent.getIntExtra("A79004B391FE457D956486F565DFC3B9", 0);
            try {
                str = intent.getStringExtra("A7D1721B9508405D8271AB82AC6D9B3C");
            } catch (Exception e) {
                str = null;
            }
            if (intExtra == 0) {
                finish();
            }
            if (intExtra == 1) {
                if (str == null) {
                    finish();
                }
                String trim = str.trim();
                if (trim.length() == 0) {
                    finish();
                }
                requestWindowFeature(2);
                setProgressBarIndeterminate(false);
                bw bwVar = new bw(this, this.d);
                this.b = bwVar;
                setContentView(bwVar);
                bwVar.a(trim);
            } else if (intExtra == 2) {
                this.a = aj.a();
                if (this.a == null) {
                    finish();
                }
                switch (this.a.a()) {
                    case 1:
                        if (this.a.n() == null) {
                            finish();
                        }
                        requestWindowFeature(2);
                        setProgressBarIndeterminate(false);
                        bw bwVar2 = new bw(this, this.d);
                        this.b = bwVar2;
                        setContentView(bwVar2);
                        bwVar2.a(this.a.g(), this.a.n());
                        a();
                        return;
                    case 2:
                        if (this.a.h() == null) {
                            finish();
                        }
                        requestWindowFeature(2);
                        setProgressBarIndeterminate(false);
                        bw bwVar3 = new bw(this, this.d);
                        this.b = bwVar3;
                        setContentView(bwVar3);
                        bwVar3.a(this.a.g(), bc.a(this.a.h()));
                        return;
                    case 3:
                        if (this.a.m() == null) {
                            finish();
                        }
                        requestWindowFeature(1);
                        getWindow().setFlags(1024, 1024);
                        ba baVar = new ba(this, this.d, this.a.m());
                        this.b = baVar;
                        setContentView(baVar);
                        a();
                        return;
                    default:
                        finish();
                        return;
                }
            }
        } catch (Exception e2) {
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.b == null) {
            return super.onKeyDown(i, keyEvent);
        }
        this.b.a();
        return true;
    }
}
