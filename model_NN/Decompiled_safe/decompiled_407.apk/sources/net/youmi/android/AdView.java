package net.youmi.android;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class AdView extends RelativeLayout {
    public static final int DEFAULT_BACKGROUND_COLOR = -16777216;
    public static final int DEFAULT_BACKGROUND_TRANS = 200;
    public static final int DEFAULT_TEXT_COLOR = -1;
    bb a;
    boolean b = false;
    boolean c = false;
    boolean d = false;
    AdListener e;
    private int f = DEFAULT_BACKGROUND_TRANS;
    private int g = -1;
    private int h = -16777216;
    private int i = -1;
    /* access modifiers changed from: private */
    public Activity j;
    private ak k;

    public AdView(Activity activity) {
        super(activity);
        a(activity);
    }

    public AdView(Activity activity, int i2, int i3, int i4) {
        super(activity);
        a(activity, i2, i3, i4);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a((Activity) context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a((Activity) context, attributeSet, i2);
    }

    /* access modifiers changed from: package-private */
    public ak a() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        a(activity, null, 0, -16777216, -1, DEFAULT_BACKGROUND_TRANS);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, int i2, int i3, int i4) {
        a(activity, null, 0, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AttributeSet attributeSet) {
        a(activity, attributeSet, 0, -16777216, -1, DEFAULT_BACKGROUND_TRANS);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AttributeSet attributeSet, int i2) {
        a(activity, attributeSet, i2, -16777216, -1, DEFAULT_BACKGROUND_TRANS);
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, AttributeSet attributeSet, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int attributeUnsignedIntValue;
        this.j = activity;
        this.k = l.a(activity);
        if (attributeSet != null) {
            try {
                String str = "http://schemas.android.com/apk/res/" + activity.getPackageName();
                i9 = attributeSet.getAttributeUnsignedIntValue(str, "textColor", i4);
                try {
                    attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", i3);
                } catch (Exception e2) {
                    i10 = i3;
                    i8 = i9;
                    i7 = i10;
                    i6 = i5;
                    this.g = i8;
                    this.h = i7;
                    this.f = i6;
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, this.k.d());
                    this.a = new bb(activity, this.k, this, this.h, this.g, this.f);
                    addView(this.a, layoutParams);
                    this.a.setVisibility(8);
                }
                try {
                    i6 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundTransparent", i5);
                    int i11 = attributeUnsignedIntValue;
                    i8 = i9;
                    i7 = i11;
                } catch (Exception e3) {
                    i10 = attributeUnsignedIntValue;
                    i8 = i9;
                    i7 = i10;
                    i6 = i5;
                    this.g = i8;
                    this.h = i7;
                    this.f = i6;
                    RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, this.k.d());
                    this.a = new bb(activity, this.k, this, this.h, this.g, this.f);
                    addView(this.a, layoutParams2);
                    this.a.setVisibility(8);
                }
            } catch (Exception e4) {
                i10 = i3;
                i9 = i4;
                i8 = i9;
                i7 = i10;
                i6 = i5;
                this.g = i8;
                this.h = i7;
                this.f = i6;
                RelativeLayout.LayoutParams layoutParams22 = new RelativeLayout.LayoutParams(-2, this.k.d());
                this.a = new bb(activity, this.k, this, this.h, this.g, this.f);
                addView(this.a, layoutParams22);
                this.a.setVisibility(8);
            }
        } else {
            i6 = i5;
            i7 = i3;
            i8 = i4;
        }
        if (i6 > 255) {
            i6 = 255;
        }
        if (i6 < 0) {
            i6 = 0;
        }
        this.g = i8;
        this.h = i7;
        this.f = i6;
        RelativeLayout.LayoutParams layoutParams222 = new RelativeLayout.LayoutParams(-2, this.k.d());
        this.a = new bb(activity, this.k, this, this.h, this.g, this.f);
        addView(this.a, layoutParams222);
        this.a.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void a(ax axVar) {
        if (axVar != null) {
            try {
                if (axVar.f() != this.i) {
                    this.i = axVar.f();
                    e();
                    this.a.a(axVar);
                }
            } catch (Exception e2) {
            }
        }
    }

    public void addView(View view) {
        if (view != null && view == this.a) {
            super.addView(view);
        }
    }

    public void addView(View view, int i2) {
        if (view != null && view == this.a) {
            super.addView(view, i2);
        }
    }

    public void addView(View view, int i2, int i3) {
        if (view != null && view == this.a) {
            super.addView(view, i2, i3);
        }
    }

    public void addView(View view, int i2, ViewGroup.LayoutParams layoutParams) {
        if (view != null && view == this.a) {
            super.addView(view, i2, layoutParams);
        }
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        if (view != null && view == this.a) {
            super.addView(view, layoutParams);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        try {
            this.b = true;
            Thread thread = new Thread(new am(this));
            thread.setDaemon(true);
            thread.start();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        try {
            this.b = false;
            removeAllViews();
            this.a.a();
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        try {
            getHandler().post(new aq(this));
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        try {
            getHandler().post(new ao(this));
        } catch (Exception e2) {
        }
    }

    public int getAdHeight() {
        return this.k.d();
    }

    public int getAdWidth() {
        return getWidth();
    }

    public int getBackgroundColor() {
        return this.h;
    }

    public int getBackgroundTransparent() {
        return this.f;
    }

    public int getTextColor() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            if (!(layoutParams == null || layoutParams.height == -2)) {
                layoutParams.height = -2;
            }
        } catch (Exception e2) {
        }
        b();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        c();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        this.c = !z;
    }

    public void setAdListener(AdListener adListener) {
        this.e = adListener;
    }
}
