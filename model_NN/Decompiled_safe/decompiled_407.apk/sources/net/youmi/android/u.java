package net.youmi.android;

import com.adview.util.AdViewUtil;

class u {
    static final int[] a = {AdViewUtil.VERSION, 65280, 16711680, -16777216};

    u() {
    }

    static final int a(byte b, int i, int i2) {
        return i + i2 > 8 ? b : ((b << ((8 - i) - i2)) & AdViewUtil.VERSION) >>> (8 - i2);
    }

    static final int a(byte[] bArr, int i, int i2) {
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            i4 |= (bArr[i + i5] << i3) & a[i5];
            i3 += 8;
        }
        return i4;
    }
}
