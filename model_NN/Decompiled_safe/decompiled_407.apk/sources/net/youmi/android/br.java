package net.youmi.android;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import com.finger2finger.games.res.Const;
import java.util.Random;

class br extends View {
    ak a;
    Bitmap b;
    Bitmap c;
    Bitmap d;
    Paint e = new Paint();
    RectF f = new RectF(Const.MOVE_LIMITED_SPEED, Const.MOVE_LIMITED_SPEED, 240.0f, 50.0f);
    float g = Const.MOVE_LIMITED_SPEED;
    int h = 0;
    int i = 0;
    float j = Const.MOVE_LIMITED_SPEED;
    Rect k = new Rect(0, 0, 1, 8);
    Rect l = new Rect();
    RectF m = new RectF();
    Random n = new Random();
    int o;
    int p;
    int q = 8;
    int r = 6;

    public br(Activity activity, ak akVar, int i2) {
        super(activity);
        this.a = akVar;
        this.d = BitmapFactory.decodeStream(getClass().getResourceAsStream("a1.png"));
        this.c = BitmapFactory.decodeStream(getClass().getResourceAsStream("a3.png"));
        this.b = BitmapFactory.decodeStream(getClass().getResourceAsStream("a2.png"));
        setBackgroundDrawable(new BitmapDrawable(this.b));
        this.o = i2;
        this.p = akVar.a(this.q);
        this.g = (float) i2;
        this.h = 0;
        this.j = this.g / 100.0f;
        this.i = 0;
    }

    public void a(int i2) {
        int i3 = i2 < 0 ? 0 : i2;
        if (i3 > 100) {
            i3 = 100;
        }
        this.i = i3;
        postInvalidate();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        try {
            getLayoutParams().height = this.p;
            getLayoutParams().width = this.o;
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float f2 = this.j * ((float) this.i);
        this.k.set(0, 0, 10, this.r);
        this.m.set(Const.MOVE_LIMITED_SPEED, 1.0f, f2, 7.0f);
        canvas.drawBitmap(this.d, this.k, this.m, this.e);
        this.k.set(0, 0, 34, this.q);
        this.m.set(f2, Const.MOVE_LIMITED_SPEED, ((float) this.a.a(34)) + f2, (float) this.q);
        canvas.drawBitmap(this.c, this.k, this.m, this.e);
    }
}
