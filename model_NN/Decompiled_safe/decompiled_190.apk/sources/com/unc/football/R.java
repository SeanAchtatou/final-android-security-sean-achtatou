package com.unc.football;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bbg = 2130837504;
        public static final int close_icon = 2130837505;
        public static final int close_icon1 = 2130837506;
        public static final int home_icon = 2130837507;
        public static final int icon = 2130837508;
        public static final int scrollbar_vertical_thumb = 2130837509;
        public static final int scrollbar_vertical_track = 2130837510;
        public static final int year_icon = 2130837511;
    }

    public static final class id {
        public static final int close = 2131099650;
        public static final int home = 2131099649;
        public static final int selyear = 2131099648;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int main1 = 2130903041;
    }

    public static final class menu {
        public static final int option_menu = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
