package com.waps.ads;

import android.app.Activity;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

class c implements Runnable {
    private WeakReference a;
    private String b;

    public c(AdGroupLayout adGroupLayout, String str) {
        this.a = new WeakReference(adGroupLayout);
        this.b = str;
    }

    public void run() {
        Activity activity;
        AdGroupLayout adGroupLayout = (AdGroupLayout) this.a.get();
        if (adGroupLayout != null && (activity = (Activity) adGroupLayout.a.get()) != null) {
            if (adGroupLayout.j == null) {
                adGroupLayout.j = new AdGroupManager(new WeakReference(activity.getApplicationContext()), this.b);
            }
            if (!adGroupLayout.n) {
                boolean unused = adGroupLayout.o = false;
                return;
            }
            adGroupLayout.j.fetchConfig();
            adGroupLayout.d = adGroupLayout.j.getExtra();
            if (adGroupLayout.d == null) {
                adGroupLayout.c.schedule(this, 30, TimeUnit.SECONDS);
                int unused2 = AdGroupLayout.s = adGroupLayout.d.k;
                return;
            }
            adGroupLayout.rotateAd();
            int unused3 = AdGroupLayout.s = adGroupLayout.d.k;
        }
    }
}
