package com.waps;

import android.os.AsyncTask;
import android.os.Environment;
import java.io.File;

class j extends AsyncTask {
    final /* synthetic */ AppConnect a;

    private j(AppConnect appConnect) {
        this.a = appConnect;
    }

    /* synthetic */ j(AppConnect appConnect, d dVar) {
        this(appConnect);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        boolean z = false;
        String access$100 = this.a.L;
        if (!this.a.M.equals("")) {
            access$100 = access$100 + "&" + this.a.M;
        }
        String a2 = AppConnect.v.a("http://app.waps.cn/action/connect/active?", access$100);
        if (a2 != null) {
            z = this.a.handleConnectResponse(a2);
        }
        return Boolean.valueOf(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            if (AppConnect.S && this.a.R != null && !"".equals(this.a.R) && this.a.R.compareTo(this.a.E) > 0) {
                this.a.UpdateDialog("http://app.waps.cn/action/app/update?" + this.a.L);
            }
            if (AppConnect.T && Environment.getExternalStorageState().equals("mounted")) {
                File file = new File(Environment.getExternalStorageDirectory().toString() + "/Android/Package.dat");
                if (file.exists()) {
                    file.delete();
                }
            }
            if (AppConnect.aa == null || "".equals(AppConnect.aa)) {
                this.a.loadApps();
                h unused = this.a.Z = new h(this.a, null);
                this.a.Z.execute(new Void[0]);
            }
        } catch (Exception e) {
        } finally {
            boolean unused2 = AppConnect.T = false;
        }
    }
}
